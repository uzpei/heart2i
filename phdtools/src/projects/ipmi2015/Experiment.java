package projects.ipmi2015;

import heart.Heart;
import heart.HeartTools;
import heart.HeartTools.DamageType;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.sun.java.swing.plaf.gtk.GTKConstants.ExpanderStyle;

import projects.inpainting.reconstruction.FrameFieldReconstruction;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import system.object.Pair;
import system.object.Triplet;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;

public class Experiment {

	public static void main(String[] args) {
		new Experiment();
	}
	
	public Experiment() {
		Heart heart = HeartTools.heartSample();

		// 1) take wall normals
		// 2) diffusion vectors
		// 3) make diffused vectors orthonormal to the wall normals
		
//		heart.computeWallNormals()
		
//		FittingMethod method = FittingMethod.Direct; OptimizerType type = OptimizerType.DIRECT;
		FittingMethod method = FittingMethod.Optimized; OptimizerType type = OptimizerType.SIMPLEX;
//		List<IntensityVolume> cijk_true = DifferentialOneForm.connectionFormsDefault(heart.getFrameField(), method, type);

		List<Triplet<String, double[], double[]>> results = new LinkedList<Triplet<String, double[], double[]>>();
		
		int k;
//		for (int experiment_type : new int[] { 1, 2, 3 }) {
		for (int experiment_type : new int[] { 4 }) {
			ReconstructionMode mode = null;
			
			// perforation
			if (experiment_type == 4 || experiment_type == 5) {
				switch (experiment_type) {
				case 4:
					mode = ReconstructionMode.VectorInterpolation;
					break;
				case 5:
					mode = ReconstructionMode.VectorDiffusion;
					break;
				}

				k = 0;
//				int pradius = 10;
				double density = 5;
				int n = 6;
				double[] results_exp = new double[n];
				double[] params_exp = new double[n];
				for (int pradius = 0; pradius < n; pradius++) {
					VoxelFrameField frameField = HeartTools.applyDamage(heart.getFrameField(), DamageType.Perforate, new double[] { density, pradius });
					results_exp[k] = compute(heart, frameField, method, type, mode);
					params_exp[k] = pradius;
					k++;
				}
				
				String name = "longaxis_" + mode.toString().toLowerCase();
				results.add(new Triplet<String, double[], double[]>(name, results_exp, params_exp));

			}
			
			// long-axis sampling, vector diffusion
			if (experiment_type == 1 || experiment_type == 2 || experiment_type == 3) {
				// Sampling setup for long-axis experiments
				int minSubset = 1;
				int maxSubset = 5;
				int[] samplingSpace = MathToolset.linearIntegerSpace(minSubset, maxSubset, (maxSubset - minSubset)+1);
				double[] results_exp = new double[samplingSpace.length];
				double[] params_exp = new double[samplingSpace.length];

				switch (experiment_type) {
				case 1:
					mode = ReconstructionMode.VectorInterpolation;
					break;
				case 2:
					mode = ReconstructionMode.VectorDiffusion;
					break;
				case 3:
					mode = ReconstructionMode.AffineDiffusion;
					break;
				}

				k = 0;
				for (int sampling : samplingSpace) {
					VoxelFrameField frameField = HeartTools.applyDamage(heart.getFrameField(), DamageType.LongAxis, new double[] { sampling });
					results_exp[k] = compute(heart, frameField, method, type, mode);
					params_exp[k] = sampling;
					k++;
				}
				
				String name = "longaxis_" + mode.toString().toLowerCase();
				results.add(new Triplet<String, double[], double[]>(name, results_exp, params_exp));
			}
		}
		
		for (Triplet<String, double[], double[]> result : results) {
			System.out.println(result.getLeft() + " = " + Arrays.toString(result.getCenter()) + Arrays.toString(result.getRight()));
		}
	}
	
	public double compute(Heart heart, VoxelFrameField damaged, FittingMethod method, OptimizerType type, ReconstructionMode mode) {
		List<IntensityVolume> cijk_data = DifferentialOneForm.connectionFormsDefault(damaged, method, type);
		
		FrameFieldReconstruction.verbose = true;

		// compute reconstruction
		CartanParameterNode[][][] nodes = asArray(cijk_data, damaged.getMask());
		VoxelFrameField frame = FrameFieldReconstruction.reconstruct(mode, heart.getFrameField(), damaged, nodes);
		
		// compute error in the reconstructed regions only
		IntensityVolumeMask currentReconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getFrameField().getMask(), damaged.getMask());
		IntensityVolume error = FrameFieldReconstruction.error(heart.getFrameField().getF1(), frame.getF1(), currentReconstructionMask);
		double e_mean = error.getMean(currentReconstructionMask);
		return e_mean;
	}
	
	public CartanParameterNode[][][] asArray(final List<IntensityVolume> cijks, IntensityVolumeMask mask) {
		int[] dims = cijks.get(0).getDimension();
		final CartanParameterNode[][][] nodes = new CartanParameterNode[dims[0]][dims[1]][dims[2]];
		new VoxelBox(mask).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				nodes[x][y][z] = new CartanParameterNode();
				for (int i = 0; i < 9; i++)
					nodes[x][y][z].set(i, cijks.get(i).get(x, y, z));
			}
		});
		return nodes;
	}
}
