package projects.ipmi2015;

import gl.geometry.FancyAxis;
import gl.geometry.GLSimpleGeometry;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.HeartTools;
import heart.HeartTools.DamageType;
import heart.Tractography;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import math.coloring.ColorMap;
import math.coloring.JColorizer;
import projects.inpainting.reconstruction.FrameFieldReconstruction;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelFrameField;
import app.dti.DiffusionSplitter;
import app.pami2013.cartan.CartanParameter;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer.OptimizerSetting;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
/**
 * TODO:
 * 1) Run tractography after reconstruction to better show long-axis results
 * 2) Plot random perturbation noise VS reconstruction error
 * 3) use 6-neighbor interpolation + other method on islands (specify in text)
 * 4) fix rule-based inpainting seeds
 * 5) Interpolation does *not* work, produces nans
 * 6) Diffusion is too irregular... something is fishy
 * 7) CartanClosedEstimation: pick one vector in neighborhood and set direction of others based on it...
 *    might help handle some noise/flips
 */
public class SimpleReconVisu {
	static Heart heart;
	static VoxelFrameField frameField, frameFieldCorrupted;
	static IntensityVolumeMask connectionDisplayMask;
	static JColorizer colorizer = new JColorizer(ColorMap.Map.HOT);
	static EnumComboBox<CartanParameter.Parameter> ecb = new EnumComboBox<CartanParameter.Parameter>(CartanParameter.Parameter.C121);
	static EnumComboBox<ReconstructionMode> ecbRecon = new EnumComboBox<ReconstructionMode>(ReconstructionMode.Inpainting);
	static ReconstructionMode mode = ReconstructionMode.Inpainting;
	static BooleanParameter drawFieldTrue = new BooleanParameter("draw truth", true);
	static BooleanParameter drawCijk = new BooleanParameter("draw Cijk", false);
	static BooleanParameter drawField = new BooleanParameter("draw field", true);
	static BooleanParameter drawCells= new BooleanParameter("draw cells", false);
	static BooleanParameter drawBox = new BooleanParameter("draw box", true);
	static BooleanParameter drawWorld = new BooleanParameter("draw world", true);
	static BooleanParameter highDef = new BooleanParameter("high def", false);
	static VectorColumn column = new VectorColumn();

	public static void main(String[] args) {
		NodeScene scene = new NodeScene();
//		heart = HeartTools.heartSample();
//		heart = HeartTools.heart();
//		heart = HeartTools.heartHuman();
		heart = HeartTools.helixHeart();
//		heart = HeartTools.torusHeart();
		
//		heart = HeartTools.heartSynthetic();
		frameField = heart.getFrameField();
		heart.getFrameField().fixAlignment();
		
		// Enforce neighbor direction consistency
//		frameField.fixAlignment();
		
		final IntensityVolume volume = heart.getFrameField().getF1().getX();
		volume.createDisplayMask(heart.getMask());
		scene.addNode(new GLSimpleGeometry() {
			@Override
			public void display(GLAutoDrawable drawable) {
				staticDisplay(drawable);
			}
		});
		
		ecbRecon.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				mode = ecbRecon.getSelected();
			}
		});
		
		scene.addControl(getControls());
		scene.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				
				if (e.getKeyCode() == KeyEvent.VK_P) {
					applyDamage();
				}
				else if (e.getKeyCode() == KeyEvent.VK_D) {
					diffuse();
				}
				else if (e.getKeyCode() == KeyEvent.VK_T) {
					trace();
				}
				else if (e.getKeyCode() == KeyEvent.VK_N) {
					applyNoise();
				}
				else if (e.getKeyCode() == KeyEvent.VK_F) {
					frameField.fixAlignment();
				}
				else if (e.getKeyCode() == KeyEvent.VK_C) {
					computeConnections();
				}
				else if (e.getKeyCode() == KeyEvent.VK_R) {
					reconstruct();
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
					switchMode();
				}
				else if (e.getKeyCode() == KeyEvent.VK_L) {
					reload();
				}
			}
		});
		
		applyDamage();
//		computeConnections();
	}

	public static JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		vfp.add(colorizer);
		
		JPanel bpanel = new JPanel(new GridLayout(5,2));
		bpanel.add(drawField.getControls());
		bpanel.add(drawFieldTrue.getControls());
		bpanel.add(drawCijk.getControls());
		bpanel.add(drawBox.getControls());
		bpanel.add(drawCells.getControls());
		bpanel.add(drawWorld.getControls());
		bpanel.add(highDef.getControls());
		vfp.add(bpanel);
		
		// buttons
		JPanel btpanel = new JPanel(new GridLayout(2,3));
		List<String> dstr = Arrays.asList("x hole", "y hole", "cuthole", "perforate", "longaxis", "subsample");
		JButton[] dbtns = new JButton[dstr.size()];
		for (int i = 0; i < dstr.size(); i++) {
			dbtns[i] = new JButton(dstr.get(i));
			
			final int fi = i;
			dbtns[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					applyDamage(fi);
				}
			});
			btpanel.add(dbtns[i]);
		}
		vfp.add(btpanel);
		
		vfp.add(tractography.getControls());
		
		vfp.add(ecbRecon.getControls());
		vfp.add(ecb.getControls());
		
		ParameterListener pl = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateColoring();
				updateHistograms();
			}
		};
		ecb.addParameterListener(pl);
		drawFieldTrue.addParameterListener(pl);

		highDef.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				frameFieldCorrupted.getF1().setHighDef(highDef.getValue());
				heart.getFrameField().getF1().setHighDef(highDef.getValue());
			}
		});
		
		histogramData = new JHistogram();
		histogramTrue = new JHistogram();
		vfp.add(histogramData);
		vfp.add(histogramTrue);
		
		return vfp.getPanel();
	}
	
	static void updateColoring() {
		
		IntensityVolume v = getSelectedVolume();
		if (v != null) {
			v.createDisplayMask(connectionDisplayMask);
			v.setColorMap(colorizer);
			
			// set color based on true data
			int index = ecb.getSelected().ordinal();
			double[] mm = cijk_true.get(index).getminMax();
			colorizer.setMinMax(mm[0], mm[1]);
		}
	}
	
	static JHistogram histogramTrue, histogramData;

	static void updateHistograms() {
		if (cijk_true == null || cijk_data == null || frameField == null)
			return;
		
		int index = ecb.getSelected().ordinal();
		
		histogramTrue.setHistogram(new Histogram(cijk_true.get(index).flattenCompact(heart.getMask())));
		histogramData.setHistogram(new Histogram(cijk_data.get(index).flattenCompact(frameField.getMask())));
	}
	
	/**
	 * Display the scene
	 * @param drawable
	 */
	public static void staticDisplay(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glPushMatrix();
		heart.getVoxelBox().applyHeartNormalization(gl);
		
//		VoxelBox boxh = new VoxelBox(heart.getVoxelBox());
//		boxh.setDrawCells(drawCells.getValue());
//		boxh.setVisible(false);
//		boxh.display(drawable);
//		if (reconstructionMask != null) {
//			VoxelBox box = new VoxelBox(reconstructionMask);
//			box.setDrawCells(drawCells.getValue());
//			box.setVisible(false);
//			box.display(drawable);
//		}

		column.display(drawable);

		VoxelFrameField field = null;
		if (drawFieldTrue.getValue()) {
			field = heart.getFrameField();
		}
		else {
			field = frameField;
		}
		
		if (drawField.getValue()) {

			field.getF1().setHighDef(highDef.getValue());
			field.getF1().setVisible(true);
//			field.getF1().setVisible(false);
			field.getF3().setVisible(false);
			field.getF1().setColor(new double[] { 0, 0, 1 });
			field.display(drawable);
			field.getF1().setDrawRadius(0.2);
		}
		
		if (drawBox.getValue()) {
			field.getF1().getVoxelBox().setVisible(true);
			field.getF1().getVoxelBox().display(drawable);
			
			// Create a voxelbox only for the reconstructed region
			if (reconstructionMask != null) {
				VoxelBox box = new VoxelBox(reconstructionMask);
				box.setDrawCells(drawCells.getValue());
				box.setVisible(false);
				box.display(drawable);
			}
		}
		
		tractography.display(drawable);
		
		if (drawCijk.getValue()){ 
			IntensityVolume v = getSelectedVolume();
			if (v != null)
				v.displayAsVoxels(drawable);
			
		}
		
		if (drawWorld.getValue()) {
			gl.glPushMatrix();
			Point3d c = heart.getVoxelBox().getOrigin3d();
			gl.glTranslated(c.x, c.y, c.z);
			
			double fs = 2;
			gl.glScaled(fs,fs,fs);
			new FancyAxis().display(drawable);
			gl.glPopMatrix();
		}
		
		gl.glPopMatrix();
		
//		System.out.println(Arrays.toString(v.getminMax()));
	}
	
	static IntensityVolume getSelectedVolume() {
		int index = ecb.getSelected().ordinal();
		
		if (drawFieldTrue.getValue())
			return cijk_true == null ? null : cijk_true.get(index);
		else
			return cijk_data == null ? null : cijk_data.get(index);
	}
	
	static List<IntensityVolume> cijk_true, cijk_data;
	
	static void computeConnections() {
		if (frameField == null || frameField.getMask() == null)
			applyDamage();
		
		System.out.println("Computing connections...");
		
//		FittingMethod method = FittingMethod.Direct; OptimizerType type = OptimizerType.DIRECT;
		FittingMethod method = FittingMethod.Optimized; OptimizerType type = OptimizerType.SIMPLEX;
		
//		cijk_true = DifferentialOneForm.connectionFormsDefault(heart.getFrameField(), method, type);
//		cijk_data = DifferentialOneForm.connectionFormsDefault(frameField, method, type);
		cijk_data = DifferentialOneForm.connectionFormsIpmi(frameField);
		cijk_true = DifferentialOneForm.connectionFormsIpmi(frameField);

		connectionDisplayMask = new IntensityVolumeMask(frameField.getMask());
		connectionDisplayMask.cutThreeQuarter();

		// Compute damaged connections 
//		cijk_data = DifferentialOneForm.connectionFormsDirect(frameField);
		System.out.println("Done.");
		
		updateColoring();
		updateHistograms();
	}

	static void diffuse() {
		// Test post diffusion
		for (int i = 0; i < 1; i++) {
			VoxelBox box = frameField.getF1().getVoxelBox();
			frameField = FrameFieldReconstruction.reconstructVectorDiffusion(frameField, reconstructionMask, reconstructionMask, 3);
			frameField.setVoxelBox(box);
		}
		frameField.getF1().setColor(new double[] { 0, 0, 1 });
		IntensityVolume error = FrameFieldReconstruction.error(heart.getFrameField().getF1(), frameField.getF1(), reconstructionMask);
		System.out.println("Mean error = " + error.getMean(reconstructionMask));
		System.out.println("Mean std = " + error.getStandardDeviation(reconstructionMask));
	}
	
	static void switchMode() {
		mode = ReconstructionMode.values()[(mode.ordinal() + 1) % ReconstructionMode.values().length];
		System.out.println("---> Reconstruction mode: " + mode + " (next: " + ReconstructionMode.values()[(mode.ordinal() + 1) % ReconstructionMode.values().length] + ")");
		ecbRecon.setSelected(mode);
	}
	
	static IntensityVolumeMask reconstructionMask = null;
	
	static void reconstruct() {
		reconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getMask(), frameField.getMask());
		
		OptimizerSetting optimizer = new OptimizerSetting(FittingMethod.Optimized, OptimizerType.SIMPLEX);
//		OptimizerSetting optimizer = new OptimizerSetting(FittingMethod.Direct, OptimizerType.DIRECT);
//		OptimizerSetting optimizer = new OptimizerSetting(FittingMethod.ClosedForm, OptimizerType.DIRECT);
		Heart ruled;
		if (mode == ReconstructionMode.Inpainting || mode == ReconstructionMode.RuleBased) {
			try {
				ruled = HeartTools.computeRuleBased(heart);
			}
			catch (NullPointerException e) {
				ruled = null;
			}
			
		}
		else
			ruled = null;
		
		FrameFieldReconstruction.verbose = false;
		frameField = FrameFieldReconstruction.reconstruct(mode, heart.getFrameField(), frameField, optimizer, ruled);
		frameField.getF1().setColor(new double[] { 0, 0, 1 });
		frameField.getF1().setVoxelBox(heart.getVoxelBox());

		IntensityVolume error = FrameFieldReconstruction.error(heart.getFrameField().getF1(), frameField.getF1(), reconstructionMask);
		System.out.println("# voxels = " + frameField.getF1().getVoxelBox().countVoxels());
		System.out.println("Mean error = " + error.getMean(reconstructionMask));
		System.out.println("Mean std = " + error.getStandardDeviation(reconstructionMask));
	}
	
	static void reload() {
		frameField = frameFieldCorrupted.voxelCopy();
		double c = 0.2;
//		frameField.getF1().setColor(new double[] { c, c, c });
		frameField.getF1().setColor(new double[] { 1, 0, 0 });
		frameField.getF1().setVoxelBox(heart.getVoxelBox());

//		VoxelBox box = new VoxelBox(frameField.getMask());
//		box.cut(CuttingPlane.TRANSVERSE);
//		frameField.setVoxelBox(box);
	}
	
	static int currentDamageId = 0;

	static void applyDamage() {
		applyDamage(currentDamageId);
	}
	
	static void applyDamage(int id) {
		currentDamageId = id;
		
//		heart = HeartTools.torusHeart();
//		heart = HeartTools.heartSample();
//		heart = HeartTools.torusHeart();
//		heart = HeartTools.heart();
//		heart = HeartTools.torusHeart();
//		heart = HeartTools.helixHeart();
//		heart.getFrameField().fixAlignment();

		/*
		 * Apply voxel box
		 */
		heart.getVoxelBox().cut(CuttingPlane.ALL);
		
//		frameFieldCorrupted = heart.getFrameField();

		boolean xhole = id == 0;
		boolean yhole = id == 1;
		boolean cuthole = id == 2;
		boolean perforate = id == 3;
		boolean longAxis = id == 4;
		boolean subsample = id == 5;

		System.out.println("Applying corruption...");
//		frameFieldCorrupted = HeartTools.applySampling(heart.getFrameField(), SamplingType.Perforate, new double[] { 5, 10 });
//		frameFieldCorrupted = HeartTools.applySampling(heart.getFrameField(), SamplingType.LongAxis, new double[] { 10 });
//		frameFieldCorrupted = HeartTools.applySampling(heart.getFrameField(), SamplingType.Perforate, new double[] { 5, 2 });
		
		IntensityVolumeMask availableMask = new IntensityVolumeMask(heart.getMask());
		
		int[] dims = availableMask.getDimension();
		if (xhole) {
//			double holeSize = 0.3;
			double holeSize = 0.4;
			availableMask = new IntensityVolumeMask(heart.getMask());
			for (Point3i pt : new VoxelBox(heart.getMask()).getVolume()) {
				if (pt.y < dims[1] / 1.5 && pt.x > holeSize * dims[0] && pt.x < (1-holeSize) * dims[0]) {
					availableMask.set(pt, 0);
				}
			}
		}
		
		if (cuthole) {
			double holeSize = 0.75;
			
			Point3i origin = new Point3i(MathToolset.roundInt(0.5 * holeSize * dims[0]), 0, 0);
			Point3i span = new Point3i(dims[0], MathToolset.roundInt((1-holeSize) * dims[1] * 1.2), dims[2]);
			span.x -= MathToolset.roundInt(0.5 * span.x);
			heart.getVoxelBox().setOrigin(origin);
			heart.getVoxelBox().setSpan(span);

			for (Point3i pt : new VoxelBox(availableMask).getVolume()) {
				if (pt.x > (holeSize-0.2) * dims[0] && pt.y < (1-holeSize-0.05) * dims[1]) {
					availableMask.set(pt, 0);
				}
			}
		}

		if (yhole) {
			double holeSize = 0.44;
			for (Point3i pt : new VoxelBox(heart.getMask()).getVolume()) {
				if (pt.z > holeSize * dims[2] && pt.z < (1-holeSize) * dims[2]) {
					availableMask.set(pt, 0);
				}
			}
		}

		/*
		 * Set only available data
		 */
		frameFieldCorrupted = new VoxelFrameField(heart.getDimension());
		for (Point3i p : new VoxelBox(availableMask).getVolume()) {
			frameFieldCorrupted.set(p, heart.getFrameField().get(p));
		}
		frameFieldCorrupted.setMask(availableMask);
		/*
		 * These are pre-computed types
		 */
		if (perforate)
//			frameFieldCorrupted = HeartTools.applySampling(heart.getFrameField(), SamplingType.Perforate, new double[] { 6, 10 });
			frameFieldCorrupted = HeartTools.applyDamage(heart.getFrameField(), DamageType.Perforate, new double[] { 7, 5 });
//			frameFieldCorrupted = HeartTools.applySampling(frameFieldCorrupted, SamplingType.Perforate, new double[] { 8, 6 });

		if (subsample)
			frameFieldCorrupted = HeartTools.applyDamage(heart.getFrameField(), DamageType.Sampled, new double[] { 11 });
		
		if (longAxis) {
			int interlacing = 10;
//			frameFieldCorrupted = HeartTools.applyDamage(heart.getFrameField(), DamageType.LongAxis, new double[] { 4 });
			frameFieldCorrupted = HeartTools.applyDamage(heart.getFrameField(), DamageType.LongAxis, new double[] { 10 });
			List<Integer> slices = DiffusionSplitter.generateSlices(heart.getVoxelBox(), interlacing);
			column.update(new Vector3d(0,4,0), new Point3i(dims[0]/2,0,0), slices);
		}
		else {
			column.clear();
		}

		frameFieldCorrupted.getF1().setColor(new double[] { 0, 0, 0 });
		frameFieldCorrupted.getF1().setVoxelBox(heart.getVoxelBox());

		int count1 = new VoxelBox(frameFieldCorrupted.getMask()).countVoxels();
		int count2 = new VoxelBox(heart.getMask()).countVoxels();
		System.out.println("Full / Damaged / Reconstructed = " + count2 + " / " + count1 + " / " + (count2-count1));
		System.out.println("Destruction percentage = " + (count2 - count1) / (double) count2 * 100);
		System.out.println("Available percentage = " + count1 / (double) count2 * 100);
		
		reload();
	}
	
	public static void applyNoise() {
		boolean addNoise = true;
		boolean randomFlips = false;
		
		// gaussian noise
		if (addNoise) {
			frameFieldCorrupted = HeartTools.applyDamage(frameFieldCorrupted, DamageType.Noise, new double[] { 0.7853981633974483 });
		}
//		frameFieldCorrupted.perturb(Math.PI / 32);
		
		if (randomFlips) {
			final Vector3d v = new Vector3d();
			final Random rand = new Random();
			final double flipProb = 0.2;
			new VoxelBox(frameFieldCorrupted.getMask()).voxelProcess(new PerVoxelMethodUnchecked() {
				@Override
				public void process(int x, int y, int z) {
					if (rand.nextDouble() < flipProb) {
						// flip F1 + F2 (preserve right-handedness)
						frameFieldCorrupted.getF1().getVector3d(x, y, z, v);
						v.negate();
						frameFieldCorrupted.getF1().set(x, y, z, v);
						
						frameFieldCorrupted.getF2().getVector3d(x, y, z, v);
						v.negate();
						frameFieldCorrupted.getF2().set(x, y, z, v);
					}
				}
			});
		}
		
		frameField = frameFieldCorrupted;
	}
	
	static Tractography tractography = new Tractography();
	static void trace() {
		heart = HeartTools.helixHeart();

		frameField.fixAlignment();
		
		// Set transverse voxel box
		VoxelBox box = new VoxelBox(heart.getMask());
		Point3i origin = frameField.getF1().getVoxelBox().getOrigin();
		Point3i span = frameField.getF1().getVoxelBox().getSpan();
		int[] dims = box.getDimension();
//		origin.x = MathToolset.roundInt(dims[0] / 2);
//		origin.y = MathToolset.roundInt(dims[1] / 2);
//		origin.z = MathToolset.roundInt(dims[2] / 2);
//		span.z = 1;
		box.setOrigin(origin);
		box.setSpan(span);

		
		VoxelFrameField field = null;
		if (drawFieldTrue.getValue()) {
			field = heart.getFrameField();
		}
		else {
			field = frameField;
		}
		System.out.println("Tracing...");
		tractography.update(field, box);
		tractography.trace();
		System.out.println("Done.");
	}
}
