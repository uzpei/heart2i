package projects.ipmi2015;

import heart.Heart;
import heart.HeartTools;
import heart.HeartTools.DamageType;

import java.awt.Color;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import math.coloring.ColorMap.Map;
import projects.inpainting.reconstruction.FrameFieldReconstruction;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import visualization.HeartVolumeSnapshot;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelFrameField;
import diffgeom.fitting.core.CartanOptimizer.OptimizerSetting;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import extension.matlab.MatlabScript;

public class ExperimentNoise {

	public static void main(String[] args) {
		new ExperimentNoise();
	}
	
	public ExperimentNoise() {
//		Heart heart = HeartTools.heartSample();
		Heart heart = HeartTools.heart();
//		Heart heart = HeartTools.torusHeart(false);
		
//		Heart heartRuleBased = 	HeartTools.computeRuleBased(heart);
		Heart heartRuleBased = null;

		FrameFieldReconstruction.verbose = false;

		float scale = 12;
		boolean visualOutput = false;
		boolean initialOutput = false;
		
		// number of realizations per experiment
		int numRealizations = 100;
		
		// how many slices to skip between samples
		DamageType sampleStyle = DamageType.Noise;
		List<String> methods = new LinkedList<String>();
		
		// Sample in [0,1]
		double[] experiments = MathToolset.linearSpace(1.2, 2, 6);
		double thetaMax = Math.PI / 16;
		
		// Remap 
		for (int i = 0; i < experiments.length; i++) {
			// Remap to [0,1]
			experiments[i]--;
			
			// Remap to max noise
			experiments[i] *= thetaMax;
			
		}
		System.out.println("Noise levels = " + Arrays.toString(experiments));
		
		int experiment_id = -1;
		int numMethods = 4;
		double[][][] results_a = new double[numMethods][2][experiments.length];
		for (double noiseNormalized : experiments) {
			experiment_id++;
			
			// Normalize noise from 0 to pi/4
			System.out.println("Noise = " + noiseNormalized);
			
			for (int realization = 0; realization < numRealizations; realization++) {
				System.out.println(realization + "/" + numRealizations);
				
				VoxelFrameField frameFieldDamaged = HeartTools.applyDamage(heart.getFrameField(), DamageType.Noise, new double[] { noiseNormalized });
				frameFieldDamaged = HeartTools.applyDamage(frameFieldDamaged, DamageType.Perforate, new double[] { 4, 6 });

				if (visualOutput || (realization == 0 && initialOutput))
					HeartVolumeSnapshot.save(frameFieldDamaged, null, scale, 0, 1, String.format("field_damaged_%.2f", noiseNormalized) +".png", true);
							
				IntensityVolumeMask reconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getFrameField().getMask(), frameFieldDamaged.getMask());
				IntensityVolume resultVolume;
				IntensityVolumeMask errorMask = reconstructionMask;
				errorMask.removeTopFront();
				ReconstructionMode mode;
				Double mean, std;
				boolean helical = false;
				String outFile = "./result_" + sampleStyle + "_";
				OptimizerSetting optimizer;
				int method_id = 0;
				
				mode = ReconstructionMode.VectorInterpolation;
				System.out.println(mode.toString());
				resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
				mean = resultVolume.getMean(reconstructionMask);
				std = resultVolume.getStandardDeviation(reconstructionMask);
				results_a[method_id][0][experiment_id] += mean / numRealizations;
				results_a[method_id][1][experiment_id] += std / numRealizations;		
				method_id++;
				if (methods.size() < method_id)
					methods.add(mode.toString());
				if (visualOutput || (realization == 0 && initialOutput))
					HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outFile + mode + "_" + String.format("%.4f", noiseNormalized) + ".png", true);
		
				mode = ReconstructionMode.VectorDiffusion;
				System.out.println(mode.toString());
				resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
				mean = resultVolume.getMean(reconstructionMask);
				std = resultVolume.getStandardDeviation(reconstructionMask);
				results_a[method_id][0][experiment_id] += mean / numRealizations;
				results_a[method_id][1][experiment_id] += std / numRealizations;		
				method_id++;
				if (methods.size() < method_id)
					methods.add(mode.toString());
				if (visualOutput || (realization == 0 && initialOutput))
					HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outFile + mode + "_" + String.format("%.4f", noiseNormalized) + ".png", true);
		
				mode = ReconstructionMode.RuleBased;
				System.out.println(mode.toString());
				resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
				mean = resultVolume.getMean(reconstructionMask);
				std = resultVolume.getStandardDeviation(reconstructionMask);
				results_a[method_id][0][experiment_id] += mean / numRealizations;
				results_a[method_id][1][experiment_id] += std / numRealizations;		
				method_id++;
				if (methods.size() < method_id)
					methods.add(mode.toString());
				if (visualOutput || (realization == 0 && initialOutput))
					HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outFile + mode + "_" + String.format("%.4f", noiseNormalized) + ".png", true);
		
				mode = ReconstructionMode.Inpainting; 
				optimizer = new OptimizerSetting(FittingMethod.Direct, OptimizerType.DIRECT);
				resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, optimizer, mode);
				mean = resultVolume.getMean(reconstructionMask);
				std = resultVolume.getStandardDeviation(reconstructionMask);
				results_a[method_id][0][experiment_id] += mean / numRealizations;
				results_a[method_id][1][experiment_id] += std / numRealizations;		
				method_id++;
				if (methods.size() < method_id)
					methods.add(mode.toString());
				if (visualOutput || (realization == 0 && initialOutput))
					HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outFile + mode + "_" + String.format("%.4f", noiseNormalized) + ".png", true);
		
				
				print_all(methods, results_a, experiments);
			}
		}

//		MatlabScript script = new MatlabScript();
//		
//		List<Double> plotx = new LinkedList<Double>();
//		List<Double> ploty = new LinkedList<Double>();
//		List<String> labels = new LinkedList<String>();
//		for (double data : experiments) {
//			plotx.add(data);
//			labels.add(""+data);
//		}
//
//		script.hold(true);
//		for (int i = 0; i < results_a.length; i++) {
//			String s_mean = methods.get(i) + "_mean " + "= [";
//			String s_std = methods.get(i) + "_std " + "= [";
//			for (int j = 0; j < experiments.length; j += 1) {
//				s_mean += results_a[i][0][j] + " ";
//				s_std += results_a[i][1][j] + " ";
//			}
//			s_mean += "];";
//			s_std += "];";
//			System.out.println(s_mean);
//			System.out.println(s_std);
//
//			ploty.clear();
//			for (double data : results_a[i][0])
//				ploty.add(data);
//			
////			script.variable(methods.get(i) + "_mean", results_a[i][0]);
////			script.plot(plotx, ploty, 1, Color.red, false);
//			script.plot(ploty, 1, MathToolset.getColors(3).get(i), false);
//		}
//		script.hold(false);
//		script.setAxisLabels(FrameAxis.X, labels, 24);
//		script.legend(methods, 18);
//		script.writeLine("current = daspect");
//		script.writeLine("daspect([current(1)/2 current(2) current(3)])");
//		script.setAxisFontSize(18);
//		script.setXLabel("Sampling sparsity", 24);
//		script.setYLabel("Mean error (degrees)", 24);
//		script.writeLine("set(legendHandle, 'location', 'northeast')");
//		System.out.println(script.toString());
//		
//		System.exit(0);
	}
	
	public static void print_all(List<String> methods, double[][][] results_a, double[] experiments) {
		for (int i = 0; i < results_a.length; i++) {
			String s_mean = methods.get(i) + "_mean " + "= [";
			String s_std = methods.get(i) + "_std " + "= [";
			for (int j = 0; j < experiments.length; j += 1) {
				s_mean += results_a[i][0][j] + " ";
				s_std += results_a[i][1][j] + " ";
			}
			s_mean += "];";
			s_std += "];";
			System.out.println(s_mean);
//			System.out.println(s_std);
		}
	}
	
	public IntensityVolume compute(Heart heart, Heart heartRuleBased, VoxelFrameField damaged, OptimizerSetting optimizer, ReconstructionMode mode) {

		// compute reconstruction
		IntensityVolumeMask currentReconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getFrameField().getMask(), damaged.getMask());
		
//		HeartVolumeSnapshot.save(cijk_data.get(2), currentReconstructionMask, Map.HOT, -0.5, 0.5, 50, false, "c123" + optimizer.method + ".png", true);

		VoxelFrameField frame;
		if (mode == ReconstructionMode.RuleBased) {
			frame = FrameFieldReconstruction.reconstructRuleBased(heart, currentReconstructionMask);
		}
		else {
			frame = FrameFieldReconstruction.reconstruct(mode, heart.getFrameField(), damaged, optimizer, heartRuleBased);
		}
		
		// compute error in the reconstructed regions only
		IntensityVolume error = FrameFieldReconstruction.error(heart.getFrameField().getF1(), frame.getF1(), currentReconstructionMask);
		return error;
	}
}
