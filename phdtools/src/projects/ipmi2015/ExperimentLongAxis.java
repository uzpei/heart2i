package projects.ipmi2015;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;
import heart.HeartTools;
import heart.HeartTools.DamageType;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import app.dti.DiffusionSplitter;
import math.coloring.ColorMap.Map;
import projects.inpainting.reconstruction.FrameFieldReconstruction;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import visualization.HeartVolumeSnapshot;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import diffgeom.fitting.core.CartanOptimizer.OptimizerSetting;

public class ExperimentLongAxis {

	public static void main(String[] args) {
		new ExperimentLongAxis();
	}
	
	public ExperimentLongAxis() {
		
		List<HeartDefinition> hearts = new LinkedList<HeartDefinition>();
		hearts.add(new HeartDefinition("rat07052008", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat17122007/registered_clean/"));
//		hearts.add(new HeartDefinition("rat21012008", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat21012008/registered_clean/"));
//		hearts.add(new HeartDefinition("rat22012008", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat22012008/registered_clean/"));
//		hearts.add(new HeartDefinition("rat24012008", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat24012008/registered_clean/"));
//		hearts.add(new HeartDefinition("rat24022008_17", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat24022008_17/registered_clean/"));
//		hearts.add(new HeartDefinition("rat24022008_17", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat24022008_17/registered_clean/"));

		// how many slices to skip between samples
//		int[] skips = new int[] { 2, 5, 20 };
//		int[] skips = new int[] { 2, 5, 8, 10 };
//		int[] experiment_parameters = new int[] { 2, 3, 4, 5, 7, 10 };
//		int[] experiment_parameters = new int[] { 10 };
//		int[] experiment_parameters = new int[] { 2, 7, 10 };
		int[] experiment_parameters = new int[] { 5 };
		float scale = 40;
		boolean visualOutput = true;

		double[][][] allData = new double[ReconstructionMode.values().length][experiment_parameters.length][hearts.size()];
		
		int heartId = 0;
		int voxelCount = 0;
		for (HeartDefinition def : hearts) {
			Heart heart = new Heart(def);
			Heart heartRuleBased = 	HeartTools.computeRuleBased(heart);
			heart.getFrameField().fixAlignment();
//			Heart heart = HeartTools.heartSample();
//			Heart heart = HeartTools.heart();
			
			int experimentId = 0;
			for (int nskip : experiment_parameters) {
				VoxelFrameField frameFieldDamaged = HeartTools.applyDamage(heart.getFrameField(), DamageType.LongAxis, new double[] { nskip });
//				if (visualOutput) {
//					frameFieldDamaged.getF1().setHighDef(true);				
//					frameFieldDamaged.getF1().setDrawRadius(0.2);				
//					HeartVolumeSnapshot.save(frameFieldDamaged, null, scale, 0, 1, "field_damaged_" + nskip + "_" + def.id + ".png", true);
//				}
			
			IntensityVolumeMask reconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getFrameField().getMask(), frameFieldDamaged.getMask());
			voxelCount += new VoxelBox(reconstructionMask).countVoxels();
			IntensityVolume resultVolume;
			IntensityVolumeMask errorMask = reconstructionMask;
			errorMask.removeTopFront();
			ReconstructionMode mode;
			Double mean, std;
			boolean helical = false;
			String outFile = "./result_";
			String outStr;
			OptimizerSetting optimizer;

			List<Integer> slices = DiffusionSplitter.generateSlices(heart.getVoxelBox(), nskip);
			VectorColumn column = new VectorColumn();
			column.update(new Vector3d(0,4,0), new Point3i(frameFieldDamaged.getDimension()[0]/2,-3,0), slices);

			System.out.println("Computing this experiment...");
			
//			mode = ReconstructionMode.VectorInterpolation;
//			resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
//			mean = resultVolume.getMean(reconstructionMask);
//			allData[mode.ordinal()][experimentId][heartId] = mean;
//			System.out.println("mean = " + mean);
//			outStr = outFile + mode + "_" + def.id + "_" + nskip +  ".png";
//			if (visualOutput)
//				HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outStr, true, column);
//
//			mode = ReconstructionMode.RuleBased;
//			resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
//			mean = resultVolume.getMean(reconstructionMask);
//			std = resultVolume.getStandardDeviation(reconstructionMask);
//			allData[mode.ordinal()][experimentId][heartId] = mean;
//			outStr = outFile + mode + "_" + def.id + "_" + nskip +  ".png";
//			if (visualOutput)
//				HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outStr, true, column);
//
//			mode = ReconstructionMode.VectorDiffusion;
//			resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
//			mean = resultVolume.getMean(reconstructionMask);
//			std = resultVolume.getStandardDeviation(reconstructionMask);
//			allData[mode.ordinal()][experimentId][heartId] = mean;
//			outStr = outFile + mode + "_" + def.id + "_" + nskip +  ".png";
//			if (visualOutput)
//				HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outStr, true, column);

			mode = ReconstructionMode.Inpainting; 
			resultVolume = compute(heart, heartRuleBased, frameFieldDamaged, null, mode);
			mean = resultVolume.getMean(reconstructionMask);
			std = resultVolume.getStandardDeviation(reconstructionMask);
			allData[mode.ordinal()][experimentId][heartId] = mean;
			outStr = outFile + mode + "_" + def.id + "_" + nskip +  ".png";
			if (visualOutput)
				HeartVolumeSnapshot.save(resultVolume, errorMask, Map.HOT, 0, 25, scale, helical, outStr, true, column);
			
			
			// Average out current data over third dimension (all hearts)
			System.out.println();
			System.out.println(heartId + "\\" + hearts.size());
			System.out.println(voxelCount + " voxels processed.");
			for (ReconstructionMode rmode : ReconstructionMode.values()) {
				if (rmode == ReconstructionMode.None)
					continue;
				
				double[] meanv = new double[allData[rmode.ordinal()].length];
				for (int k = 0; k < experiment_parameters.length; k++) {
					for (int h = 0; h < (heartId + 1); h++) {
						meanv[k] += allData[rmode.ordinal()][k][h] / (heartId + 1);
					}
				}
				System.out.println(rmode + "=" + Arrays.toString(meanv));
			}
			
//			TextTransfer.setClipboardContents(sb.toString());
			
			experimentId++;
			}

//			System.exit(0);			
			
			heartId++;
		}
		
		/*
		 *  Print out all data
		 */
		System.out.println();
		System.out.println("[All data]");
		for (int h = 0; h < hearts.size(); h++) {
			System.out.println();
			System.out.println(hearts.get(h).id);
			for (ReconstructionMode rmode : ReconstructionMode.values()) {
				double[] data = new double[experiment_parameters.length];
				for (int k = 0; k < experiment_parameters.length; k++) {
					data[k] = allData[rmode.ordinal()][k][h];
				}
				System.out.println(rmode + "{" + h +"}" + "=" + Arrays.toString(data) + ";");
			}
		}
	}
	
	public IntensityVolume compute(Heart heart, Heart heartRuleBased, VoxelFrameField damaged, OptimizerSetting optimizer, ReconstructionMode mode) {
		
		// For testing, make sure there is no data within the damaged regions
		FrameFieldReconstruction.verbose = false;

		// compute reconstruction
		IntensityVolumeMask currentReconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getFrameField().getMask(), damaged.getMask());
		
//		HeartVolumeSnapshot.save(cijk_data.get(2), currentReconstructionMask, Map.HOT, -0.5, 0.5, 50, false, "c123" + optimizer.method + ".png", true);

		VoxelFrameField frame;
		if (mode == ReconstructionMode.RuleBased) {
			frame = FrameFieldReconstruction.reconstructRuleBased(heart, currentReconstructionMask);
		}
		else {
			frame = FrameFieldReconstruction.reconstruct(mode, heart.getFrameField(), damaged, optimizer, heartRuleBased);
		}
		
		// compute error in the reconstructed regions only
		IntensityVolume error = FrameFieldReconstruction.error(heart.getFrameField().getF1(), frame.getF1(), currentReconstructionMask);
		return error;
	}
}
