package projects.ipmi2015;

import gl.geometry.GLSimpleGeometry;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.HeartTools;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

/**
 * Compute a rule-based fiber orientation volume based only on the B0
 * @author epiuze
 *
 */
public class RuleBasedFibers {

	static Heart heart;
	static Heart heartRule;
	static boolean displayFlag = true;
	static boolean highDefFlag = false;
	
	public static void main(String[] args) {
		NodeScene scene = new NodeScene();
//		heart = HeartTools.heartSample();
		heart = HeartTools.heart();
		heartRule = HeartTools.computeRuleBased(heart);

		scene.addNode(new GLSimpleGeometry() {
			@Override
			public void display(GLAutoDrawable drawable) {
				staticDisplay(drawable);
			}
		});
		
		scene.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				
				if (e.getKeyCode() == KeyEvent.VK_S) {
					displayFlag = !displayFlag;
				}
				if (e.getKeyCode() == KeyEvent.VK_D) {
					highDefFlag = !highDefFlag;
				}
				else if (e.getKeyCode() == KeyEvent.VK_C) {
					recomputeRule();
				}
			}
		});
		
		scene.addInteractor(heart.getVoxelBox());
	}
	
	public static void staticDisplay(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glPushMatrix();
//		heart.getVoxelBox().applyHeartNormalization(gl);
		
		if (highDefFlag) {
			double scale = 5;
			heartRule.getFrameField().getF1().setDrawScale(scale);
			heartRule.getFrameField().getF1().setHighDef(highDefFlag);
			heart.getFrameField().getF1().setDrawScale(scale);
			heart.getFrameField().getF1().setHighDef(highDefFlag);
			
//			heart.getFrameField().fixAlignment();
		}
		
		if (displayFlag)
			heartRule.display(drawable);
		else
			heart.display(drawable);
		
		gl.glPopMatrix();
	}
	
	public static void recomputeRule() {
		heartRule = HeartTools.computeRuleBased(heart);
	}
}
