package projects.ipmi2015;

import gl.geometry.GLSimpleGeometry;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.HeartTools;
import heart.HeartTools.DamageType;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import math.coloring.ColorMap;
import math.coloring.JColorizer;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
/**
 * 3) compute connections everywhere
 * 4) compare with ground truth connections
 * 5) test different methods: closed-form + optimized on boundary?
 * @author epiuze
 *
 */
public class PartialCijkComp {
	static Heart heart;
	static IntensityVolumeMask seedMask;
	static VoxelFrameField frameField;
	static JColorizer colorizer = new JColorizer(ColorMap.Map.HOT);
	static EnumComboBox<CartanParameter.Parameter> ecb = new EnumComboBox<CartanParameter.Parameter>(CartanParameter.Parameter.C121);
	static BooleanParameter drawCijkTrue = new BooleanParameter("draw truth", false);
	static BooleanParameter drawFieldTrue = new BooleanParameter("draw field true", true);
	static BooleanParameter drawCijk = new BooleanParameter("draw Cijk", true);
	static BooleanParameter drawField = new BooleanParameter("draw field", true);
	

	public static void main(String[] args) {
		NodeScene scene = new NodeScene();
//		heart = HeartTools.heartSample();
//		heart = HeartTools.heart();
//		heart = HeartTools.heartHuman();
		heart = HeartTools.helixHeart();

//		heart = HeartTools.heartSynthetic();
		frameField = heart.getFrameField();
		
		final IntensityVolume volume = heart.getFrameField().getF1().getX();
		volume.createDisplayMask(heart.getMask());
		scene.addNode(new GLSimpleGeometry() {
			@Override
			public void display(GLAutoDrawable drawable) {
				staticDisplay(drawable);
			}
		});
		
		scene.addControl(getControls());
		scene.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				
				if (e.getKeyCode() == KeyEvent.VK_P) {
					perforate();
				}
				else if (e.getKeyCode() == KeyEvent.VK_C) {
					computeConnections();
				}
			}
		});
		
		perforate();
		computeConnections();
	}

	public static JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(colorizer);
		vfp.add(drawField.getControls());
		vfp.add(drawFieldTrue.getControls());
		vfp.add(drawCijk.getControls());
		vfp.add(drawCijkTrue.getControls());
		vfp.add(ecb.getControls());
		
		ParameterListener pl = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateColoring();
				updateHistograms();
			}
		};
		ecb.addParameterListener(pl);
		drawCijkTrue.addParameterListener(pl);

		histogramData = new JHistogram();
		histogramTrue = new JHistogram();
		vfp.add(histogramData);
		vfp.add(histogramTrue);
		
		return vfp.getPanel();
	}
	
	static void updateColoring() {
		
		IntensityVolume v = getSelectedVolume();
		IntensityVolumeMask mask;
//		if (drawCijkTrue.getValue())
//			mask = heart.getMask();
//		else
//			mask = seedMask;
		// always use the seedmask
		mask = seedMask;
		
		if (v != null) {
			v.createDisplayMask(mask);
			v.setColorMap(colorizer);
			
			// set color based on true data
			int index = ecb.getSelected().ordinal();
			double[] mm = cijk_true.get(index).getminMax();
			colorizer.setMinMax(mm[0], mm[1]);
		}
	}
	
	static JHistogram histogramTrue, histogramData;

	static void updateHistograms() {
		if (cijk_true == null || cijk_data == null || seedMask == null)
			return;
		
		int index = ecb.getSelected().ordinal();
		
		histogramTrue.setHistogram(new Histogram(cijk_true.get(index).flattenCompact(heart.getMask())));
		histogramData.setHistogram(new Histogram(cijk_data.get(index).flattenCompact(seedMask)));
	}
	
	public static void staticDisplay(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glPushMatrix();
		heart.getVoxelBox().applyHeartNormalization(gl);
		
		if (drawField.getValue()) {
			if (drawFieldTrue.getValue())
				heart.getFrameField().display(drawable);
			else
				frameField.display(drawable);
		}
		
		if (drawCijk.getValue()){ 
			IntensityVolume v = getSelectedVolume();
			if (v != null)
				v.displayAsVoxels(drawable);
			
		}
		
		gl.glPopMatrix();
		
//		System.out.println(Arrays.toString(v.getminMax()));
	}
	
	static IntensityVolume getSelectedVolume() {
		int index = ecb.getSelected().ordinal();
		
		if (drawCijkTrue.getValue())
			return cijk_true == null ? null : cijk_true.get(index);
		else
			return cijk_data == null ? null : cijk_data.get(index);
	}
	
	static List<IntensityVolume> cijk_true, cijk_data;
	
	static void computeConnections() {
		if (seedMask == null)
			perforate();
		
		System.out.println("Computing connections...");
		
		FittingMethod method = FittingMethod.ClosedForm; OptimizerType type = OptimizerType.DIRECT;
//		FittingMethod method = FittingMethod.Optimized; OptimizerType type = OptimizerType.SIMPLEX;
		
		cijk_true = DifferentialOneForm.connectionFormsDefault(heart.getFrameField(), method, type);
		cijk_data = DifferentialOneForm.connectionFormsDefault(frameField, method, type);

		// Compute damaged connections 
//		cijk_data = DifferentialOneForm.connectionFormsDirect(frameField);
		System.out.println("Done.");
		
		updateColoring();
		updateHistograms();
	}

	static void perforate() {
		heart = HeartTools.helixHeart();
		frameField = heart.getFrameField();

		System.out.println("Perforating...");
//		frameField = HeartTools.applySampling(heart.getFrameField(), SamplingType.Perforate, new double[] { 5, 10 });
//		frameField = HeartTools.applySampling(heart.getFrameField(), SamplingType.LongAxis, new double[] { 10 });
		frameField = HeartTools.applyDamage(heart.getFrameField(), DamageType.Perforate, new double[] { 5, 1 });
		
		seedMask = frameField.getMask();
		System.out.println("Done.");

	}
}
