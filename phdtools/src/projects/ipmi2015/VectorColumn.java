package projects.ipmi2015;

import gl.geometry.FancyArrow;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import tools.geom.MathToolset;

public class VectorColumn {

	public VectorColumn() {
		
	}

	public List<FancyArrow> arrows = new LinkedList<FancyArrow>();
	public Point3d origin = new Point3d();

	public void update(Vector3d arrow, Point3i origin, List<Integer> slices) {
		Color3f color3f = new Color3f(Color.BLUE);
		double size = 1;
		
		arrows.clear();
		for (Integer slice : slices) {
			Point3d p0 = new Point3d(origin.x,origin.y,origin.z+slice);
			p0.z += 1;
			
			Point3d p1 = new Point3d(p0);
			p1.add(arrow);
			
			arrows.add(new FancyArrow(p0, p1, color3f, size));
		}
		
	}
	
	public void update(Vector3d arrow, Point3d origin, Vector3d samplingDirection, double length, double numSamples) {
		double dl = length / numSamples;
		samplingDirection = new Vector3d(samplingDirection);
		samplingDirection.normalize();
		samplingDirection.scale(dl);
		
		Color3f color3f = new Color3f(Color.BLUE);
		double size = 0.1;
		
		arrows.clear();
		Point3d p0 = new Point3d(origin);
		Point3d p1 = new Point3d();
		for (int i = 0; i < numSamples; i++) {
			p0.add(samplingDirection);
			p1.set(p0);
			p1.add(arrow);
			arrows.add(new FancyArrow(p0, p1, color3f, size));
		}
	}
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		for (FancyArrow arrow : arrows) {
			arrow.draw(gl);
		}
	}

	public void clear() {
		arrows.clear();
	}
}
