package projects.ipmi2015;

import gl.geometry.VertexBufferObject;
import gl.renderer.JoglScene;
import heart.Heart;
import heart.HeartTools;
import heart.HeartTools.DamageType;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import math.coloring.ColorMap;
import math.coloring.JColorizer;
import math.space.VolumeDiffuser;
import projects.inpainting.reconstruction.FrameFieldReconstruction;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import tools.TextToolset;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;
import visualization.helical.HelicalSlicerControl;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.dti.DiffusionSplitter;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer.OptimizerSetting;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;

public class ExpReconstructionApp extends JoglScene {

	public static void main(String[] args) {
		new ExpReconstructionApp();
	}
	
	private Heart heart, heartRuleBased;
	
	private IntParameter nIts = new IntParameter("iterations", 10, 1, 1000);
	private DoubleParameter gamma = new DoubleParameter("delta", 0.2, 0, 10);
	private BooleanParameter randomized = new BooleanParameter("random seed", false);
	private BooleanParameter identity = new BooleanParameter("identity seed", false);
	private BooleanParameter normal = new BooleanParameter("epinormal seed", false);
	private DoubleParameter seedingContour = new DoubleParameter("contour seed <", 1.5, 0, 20);
	private IntParameter seedingAxial = new IntParameter("axial seed " + GreekToolset.GreekLetter.DELTA.toCapital(), 10, 0, 20);
	private DoubleParameter perforate = new DoubleParameter("perforation seed", 6, 0, 10);
	private IntParameter perforateRadius = new IntParameter("perforation radius", 10, 0, 10);
	
	private BooleanParameter orthogonalize = new BooleanParameter("orthogonalize", false);
	private BooleanParameter highdef = new BooleanParameter("high def", false);
	private BooleanParameter drawframes = new BooleanParameter("draw frames", true);
	private BooleanParameter drawtext = new BooleanParameter("draw text", false);
	
	private JColorizer colorMap;
	
	private boolean restoreSlicerVisibility = false;
	
	private VoxelFrameField framesSeed;
	
//	private VoxelFrameField framesReconstructedAffine, framesReconstructedVector, framesReconstructedInterpolation;
//	private List<Pair<ReconstructionMode, VoxelFrameField>> framesReconstructed = new LinkedList<Pair<ReconstructionMode, VoxelFrameField>>();
//	private List<ReconstructionMode> modes = Arrays.asList(ReconstructionMode.VectorDiffusion, ReconstructionMode.Inpainting);
	private List<ReconstructionMode> modes = Arrays.asList(ReconstructionMode.Inpainting);
	private VoxelFrameField[] framesReconstructed = new VoxelFrameField[modes.size()];
	
	private int displayMode = 0;
	
	private IntensityVolumeMask seedMask;
	private IntensityVolumeMask currentReconstructionMask;

	private IntensityVolume[] errorVolumes = new IntensityVolume[modes.size()];
	
//	private EnumComboBox<ReconstructionMode> ecbReconstructionMode = new EnumComboBox<FrameFieldReconstruction.ReconstructionMode>(ReconstructionMode.AffineDiffusion);
	
	private List<IntensityVolume> cartanVolumesTruth, cartanVolumesReconstructed;

	/**
	 * TODO: add frame field slicer
	 */
	private HelicalSlicerControl slicer = new HelicalSlicerControl();
	
	private boolean displaySeedVoxels = false;
	private VertexBufferObject vbo = null;
	private boolean groundTruthSwitch = false;
	private boolean connectionSwitch = false;
	private boolean seedSwitch = false;
	
	private JHistogram histogram;
	
	protected ExpReconstructionApp() {
		super("Frame Reconstruction");

		// Add histogram controls
		VerticalFlowPanel vfp = new VerticalFlowPanel();		
		histogram = new JHistogram();
		vfp.add(histogram);
		super.renderer.getControlFrame().add("Histograms", vfp.getPanel());

		slicer.setVisible(false);
		slicer.setDrawAll(true);

		// Load heart
		heart = HeartTools.heartSample();
//		heart = HeartTools.heart();
		heartRuleBased = HeartTools.computeRuleBased(heart);
		
		heart.prepareDataIfNecessary(true);
		
		heart.addDataLoadedCallback(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				prepareHeart();
			}
		});
		prepareHeart();
		
		colorMap = new JColorizer(ColorMap.Map.HOT);
		colorMap.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				updateColorMaps();
			}
		});

		histogram.addThresholdedIntervalListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				final IntensityVolumeMask histomask = new IntensityVolumeMask(heart.getDimension());
				VoxelBox box = new VoxelBox(heart.getMask());
				final double[][] regions = histogram.getThresholdedIntervals();
				
				if (regions == null)
					return;
				
				box.voxelProcess(new PerVoxelMethodUnchecked() {
					
					@Override
					public void process(int x, int y, int z) {
						double value = errorVolumes[displayMode].get(x, y, z);
						
						// Update volume with histogram region
						for (double[] region : regions) {
							double min = region[0];
							double max = region[1];
							
							if (value >= min && value <= max) {
								histomask.set(x, y, z, 1);
								break;
							}
						}
					}
				});
				
				// Finally, update our visualization mask
//				framesReconstructedAffine.setMask(histomask);
				for (VoxelFrameField frameField : framesReconstructed) {
					frameField.setMask(histomask);
				}
			}
		});
		
		super.render();
	}
	
	private void updateColorMaps() {
		if (cartanVolumesTruth != null) {
			for (IntensityVolume volume : cartanVolumesTruth) {
//				IntensityVolumeMask mask = framesSeed.getMask().extractPreserveBounds(heart.getVoxelBox());
//				volume.createDisplayMask(mask);
				volume.setColorMap(this.colorMap);
			}
			double[] minMax = cartanVolumesTruth.get(2).getminMax();
			colorMap.setMinMax(minMax[0], minMax[1]);
		}
	}
	
	private void prepareHeart() {
		// Compute the ground truth connections
		cartanVolumesTruth = DifferentialOneForm.connectionFormsDirect(heart.getFrameField());

		// Allow interaction with the heart's voxel box
		addInteractor(heart.getVoxelBox());
		
		// Add parameter listeners
		heart.getVoxelBox().addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateVoxelBoxes();
			}
		});
		
		reconstruct();
	}

	private void reset() {
		heart.reloadData();
	}
	
	private void updateVoxelBoxes() {
		for (VoxelFrameField frameField : framesReconstructed) {
			frameField.setRegion(heart.getVoxelBox());
		}
		
		if (framesSeed != null) {
			framesSeed.getF1().getVoxelBox().setRegion(heart.getVoxelBox());
			framesSeed.getF2().getVoxelBox().setRegion(heart.getVoxelBox());
			framesSeed.getF3().getVoxelBox().setRegion(heart.getVoxelBox());
		}

//		System.out.println(heart.getVoxelBox().getOrigin() + " | " + heart.getVoxelBox().getSpan());

		if (slicer != null ) {
//			slicer.updateVolumes(slicer.getVolume(), new IntensityVolumeMask(reconstructed.getMask().hpOut(heart.getMask().extractPreserveBounds(heart.getVoxelBox()))), new VoxelBox(heart.getVoxelBox()));
//			slicer.getVoxelBox().cut(heart.getVoxelBox().getCuttingPlane());
//			updateSlicer();
			slicer.precomputeDisplayList();
		}
		
		if (cartanVolumesTruth != null) {
			for (IntensityVolume volume : cartanVolumesTruth) {
				IntensityVolumeMask mask = framesSeed.getMask().extractPreserveBounds(heart.getVoxelBox());
				volume.createDisplayMask(mask);
			}
		}
		
	}
	
	private void seed() {
//		seed = heart.getFrameField().voxelCopy();
		framesSeed = new VoxelFrameField(heart.getFrameField().getDimension());
		framesSeed.getF2().setVisible(false);
		framesSeed.getF3().setVisible(false);

		System.out.println("Constructing seed frame field...");
		
		if (identity.getValue()) {
			framesSeed.voxelProcess(new PerVoxelMethodUnchecked() {
				@Override
				public void process(int x, int y, int z) {
					framesSeed.set(new Point3i(x,y,z), new CoordinateFrame(x, y, z));
				}
			});
		}
		
		if (normal.getValue()) {
			framesSeed.voxelProcess(new PerVoxelMethodUnchecked() {
				@Override
				public void process(int x, int y, int z) {
					Point3i p = new Point3i(x,y,z);
					Vector3d[] frameAxes = framesSeed.get(p);
					frameAxes[0].set(0,0,0);
					frameAxes[1].set(0,0,0);
					framesSeed.set(p, frameAxes);
				}
			});
		}
		if (randomized.getValue()) {
			VoxelBox box = framesSeed.getF1().getVoxelBox();
			framesSeed = VoxelFrameField.randomize(framesSeed.getDimension());
			framesSeed.setVoxelBox(box);
		}
		
		// Now set the actual available seeds
		seedMask = new IntensityVolumeMask(framesSeed.getDimension());

		if (seedingContour.isChecked()) {
			final IntensityVolume epicardium = heart.getDistanceTransformEpi();
			final IntensityVolume endocardium = heart.getDistanceTransformEndo();
			final Vector3d[] frameAxes = new Vector3d[3];
			for (int i = 0; i < 3; i++)
				frameAxes[i] = new Vector3d();
			
			heart.getVoxelBox().voxelProcess(new PerVoxelMethodUnchecked() {
				@Override
				public void process(int x, int y, int z) {
					Point3i p = new Point3i(x,y,z);

//					for (int i = 0; i < 3; i++)
//						frameAxes[i].set(0, 0, 0);
					
					if (Math.abs(epicardium.get(p)) <= seedingContour.getValue() || Math.abs(endocardium.get(p)) <= seedingContour.getValue()) {
						Vector3d[] axes = heart.getFrameField().get(p);
						for (int i = 0; i < 3; i++)
							frameAxes[i].set(axes[i]);
						seedMask.set(x, y, z, 1);
					}
					
					framesSeed.set(p, frameAxes);
				}
			});
		}
		
		if (seedingAxial.isChecked()) {
			framesSeed = HeartTools.applyDamage(heart.getFrameField(), DamageType.LongAxis, new double[] { seedingAxial.getValue() });
			seedMask = framesSeed.getMask();
		}
		
		if (perforate.isChecked()) {
			framesSeed = HeartTools.applyDamage(heart.getFrameField(), DamageType.Perforate, new double[] { perforate.getValue(), perforateRadius.getValue() });
			seedMask = framesSeed.getMask();
		}
				
		
		/*
		 * Prepare for reconstruction
		 */
		framesSeed.setMask(seedMask);

		int ri = 0;
		for (ReconstructionMode mode : modes) {
			framesReconstructed[ri++] = framesSeed.voxelCopy();
		}

		// Set the reconstruction mask
		currentReconstructionMask = FrameFieldReconstruction.getReconstructionTargets(heart.getFrameField().getMask(), framesSeed.getMask());

		for (VoxelFrameField frameField : framesReconstructed) {
			frameField.getF1().getVoxelBox().applyCut();
			frameField.getF2().getVoxelBox().applyCut();
			frameField.getF3().getVoxelBox().applyCut();
		}

		
		restoreSlicerVisibility = slicer.isVisible();
		
		vbo = null;
		
		System.out.println("Done.");
	}
	
	private void reconstruct() {
			
		if (framesSeed == null)
			seed();

		restoreSlicerVisibility = slicer.isVisible();
		
		/*
		 * Reconstruct the connection forms
		 */
		System.out.println("Computing connection reconstruction...");
		
		/*
		 * Prepare data
		 */
		VoxelFrameField fieldInput = heart.getFrameField();
		VoxelFrameField fieldDamaged = fieldInput.extractField(new VoxelBox(seedMask));

//		heartRuleBased = HeartTools.computeRuleBased(heart);

		
		/*
		 * Reconstruct the frame field 
		 */
		System.out.println("Computing frame field reconstruction...");
		FrameFieldReconstruction.verbose = true;

		int ri = 0;
		for (ReconstructionMode mode : modes) {
			
			
			System.out.println(TextToolset.box("Computing [" + mode.toString() + "] reconstruction"));
			
			// compute reconstruction
			OptimizerSetting optimizer = new OptimizerSetting(FittingMethod.ClosedForm, OptimizerType.DIRECT);
			VoxelFrameField frame = FrameFieldReconstruction.reconstruct(mode, heart.getFrameField(), fieldDamaged, optimizer, heartRuleBased);
			framesReconstructed[ri] = frame;
			
			// compute error in the reconstructed regions only
			IntensityVolume error = FrameFieldReconstruction.error(heart.getFrameField().getF1(), frame.getF1(), currentReconstructionMask);
			errorVolumes[ri++] = error;
		}
		
		// Update histogram and error volume
		updateDisplayId();
		
		// Update drawing components
		updateVoxelBoxes();
				
		seedSwitch = false;
	}
	
	private void updateDisplayId() {
		updateSlicer();
		IntensityVolume errorVolume = errorVolumes[displayMode];
		double[] values = errorVolume.flattenCompact(currentReconstructionMask);
		histogram.setHistogram(new Histogram(values));
	}
	
	private void updateSlicer() {
		slicer.updateVolumes(errorVolumes[displayMode], currentReconstructionMask , heart.getVoxelBox());
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		List<String> text = new ArrayList<String>();
		text.add("");
		
		boolean helixColor = false;
		
//		slicer.setVisible(true);

		if (framesSeed == null) {
			groundTruthSwitch = true;
			displaySeedVoxels = false;
		}

		if (displaySeedVoxels) {
			if (vbo == null) {
				vbo = new VertexBufferObject(seedMask);
				vbo.setWireframe(true, Color.white);
			}
			
			gl.glPushMatrix();
			heart.getVoxelBox().applyHeartNormalization(gl);
			vbo.display(drawable);
			gl.glPopMatrix();
			text.set(0, "displaying seed voxels");
		}
		else {
			if (groundTruthSwitch) {
				if (connectionSwitch && cartanVolumesTruth != null) {
					gl.glPushMatrix();
//					heart.getVoxelBox().applyHeartNormalization(gl);
//					cartanVolumesTruth.get(2).displayAsVoxels(drawable);
//					cartanVolumesTruth.get(2).displayAsVoxels(drawable, heart.getMask());
					cartanVolumesTruth.get(2).displayAsVoxels(drawable);
					gl.glPopMatrix();
				}
				else {
					if (highdef.getValue()) 
					{
						heart.getFrameField().getF1().setHighDef(true);
						heart.getFrameField().getF1().setHelixColoring(helixColor);
					}

					if (drawframes.getValue())
						heart.display(drawable);
				}
				text.set(0, "ground truth");
			}
			else if (framesSeed != null) {
				slicer.display(drawable);

				gl.glPushMatrix();
				heart.getVoxelBox().applyHeartNormalization(gl);
				
				if (drawframes.getValue())
					heart.getVoxelBox().display(drawable);
				
//				currentReconstructionMask.display(drawable);
//				errorVolumes.get(ReconstructionMode.None).display(gl, heart.getVoxelBox(), 1);

				if (connectionSwitch && cartanVolumesReconstructed != null) {
					gl.glPushMatrix();
					heart.getVoxelBox().applyHeartNormalization(gl);
					cartanVolumesReconstructed.get(2).displayAsVoxels(drawable);
					gl.glPopMatrix();
					text.set(0, "reconstructed");
				}
				else {
					if (seedSwitch) {
						if (highdef.getValue()) 
						{
							framesSeed.getF1().setHighDef(true);
							framesSeed.getF1().setHelixColoring(helixColor);
						}
						
						if (drawframes.getValue())
							framesSeed.display(drawable);
						text.set(0, "seed");
					}
					else {
						VoxelFrameField field = null;

						field = framesReconstructed[displayMode];
						text.set(0,  "reconstructed (" + modes.get(displayMode).toString().toLowerCase() + ")");
						
						text.add("Mean error = " + errorVolumes[displayMode].getMean(currentReconstructionMask));

						if (field != null) {
							if (highdef.getValue()) 
							{
								field.getF1().setHighDef(true);
								field.getF1().setHelixColoring(helixColor);
							}
							if (drawframes.getValue())
								field.display(drawable); 
						}
					}
				}
					
				gl.glPopMatrix();
			}			
		}
//		super.renderer.getTextRenderer().drawTopLeft("[" + text + "]", drawable);
		if (drawtext.getValue())
			super.renderer.getTextRenderer().drawTopLeft(text, drawable);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(seedingContour.getSliderControlsExtended("use", false));
		vfp.add(seedingAxial.getSliderControlsExtended("use", false));
		vfp.add(perforate.getSliderControlsExtended("use", true));
		vfp.add(perforateRadius.getSliderControls());
		vfp.add(highdef.getControls());
		vfp.add(drawtext.getControls());
		vfp.add(drawframes.getControls());
		vfp.add(colorMap);
//		vfp.add(orthogonalize.getControls());
//		vfp.add(normal.getControls());
//		vfp.add(identity.getControls());
//		vfp.add(randomized.getControls());
//		ParameterListener l = new ParameterListener() {
//			@Override
//			public void parameterChanged(Parameter parameter) {
//				seed();
//			}
//		};
//		normal.addParameterListener(l);
//		identity.addParameterListener(l);
//		randomized.addParameterListener(l);
//		vfp.add(gamma.getSliderControlsExtended("markov", true));
		
		vfp.add(nIts.getSliderControls());
		
		VerticalFlowPanel slicerPanel = new VerticalFlowPanel();
		slicerPanel.createBorder("Error map");
		slicerPanel.add(slicer.getControls());
		vfp.add(slicerPanel);
		
		// Add heart controls
		vfp.add(heart.getControls());
		
		return vfp.getPanel();
	}

	@Override
	public void attach(Component component) {
		this.component = component;
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// Handle events
				if (e.getKeyCode() == KeyEvent.VK_R) {
					reconstruct();
				}
				else if (e.getKeyCode() == KeyEvent.VK_I) {
					displayMode = (displayMode + 1) % modes.size();	
					updateDisplayId();
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
					seed();
//					reconstruct();
				}
				else if (e.getKeyCode() == KeyEvent.VK_P) {
					seed();
				}
				else if (e.getKeyCode() == KeyEvent.VK_C) {
					reset();
				}
				else if (e.getKeyCode() == KeyEvent.VK_E) {
					groundTruthSwitch ^= true;
				}
				else if (e.getKeyCode() == KeyEvent.VK_W) {
					seedSwitch ^= true;
				}
				else if (e.getKeyCode() == KeyEvent.VK_B) {
					displaySeedVoxels ^= true;
				}
				else if (e.getKeyCode() == KeyEvent.VK_M) {
					connectionSwitch ^= true;
				}
			}
		});
	}

}
