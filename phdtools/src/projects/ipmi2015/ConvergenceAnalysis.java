package projects.ipmi2015;

import heart.Heart;
import heart.HeartTools;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import system.interfaces.TextTransfer;
import system.object.Triplet;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.core.experiments.PamiExperimentRunner;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;
import extension.matlab.MatlabScript;

public class ConvergenceAnalysis {

	public static void main(String[] args) {
		Heart heart;
		boolean random = false;
		
		if (random) {
//			int[] dims = new int[] { 16, 16, 16 };
			int[] dims = new int[] { 5, 5, 5};
			heart = Heart.randomize(dims);
			heart.prepareDataIfNecessary();
//			VoxelFrameField field = VoxelFrameField.randomize(heart.getDimension());
//			field.setMask(heart.getMask());
//			heart.setVoxelFrameField(field);
		}
		else {
//			HeartDefinition def = new HeartDefinition("rat07052008_sub", Species.Rat, "./data/heart/rat/sampleSubset/");	
//			HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/");
//			HeartDefinition def = new HeartDefinition("rat07052008_sub", Species.Rat, "./data/heart/rat/sampleSmall/");	
//			heart = new Heart(def);
//			heart = HeartTools.heartSample();
			heart = HeartTools.heart();
			
//			heart.setSmoothing(new IterativeFilter(5, 0.2));
//			heart.setSmoothing(null);
			heart.prepareDataIfNecessary();

			VoxelBox box = heart.getVoxelBox();
			box.cut(CuttingPlane.FIT);
			box.sliceAxis(FrameAxis.Z, 0.7);
			box.computeVoxelListUpdate();
		}
		
		runConvergenceAnalysis(heart);
	}
	
	/**
	 * TODO: subclass PamiExperiment
	 * @param heart
	 */
	public static void runConvergenceAnalysis(Heart heart) {
		/*
		 * Use 6-neighbors int this experiment to get a fair comparison (both for computation time and accuracy) to the direct computations
		 */
		NeighborhoodShape neighborhoodShape = NeighborhoodShape.SixNeighbors;
		
		// Construct space of iterations
		int minIterations = 0;
		int maxIterations = 200;
		int numPoints = 40;
//		int numPoints = 4;
		int nsize = 3;
		int numThreads = Runtime.getRuntime().availableProcessors();
		double regularization = 0;
		double errorThreshold = 0;
		boolean validate = true;
		
		int[] iterationSpace = MathToolset.linearIntegerSpace(minIterations, maxIterations, numPoints);
		System.out.println(Arrays.toString(iterationSpace));
		String output = "./data/PAMI/images/computations/";
		
		CartanModel model = CartanModel.FULL;

		// Available methods
		List<Triplet<FittingMethod, OptimizerType, Boolean>> methods = new LinkedList<Triplet<FittingMethod, OptimizerType, Boolean>>();
		methods.add(new Triplet<FittingMethod, OptimizerType, Boolean>(FittingMethod.Direct, OptimizerType.DIRECT, new Boolean(false)));
		methods.add(new Triplet<FittingMethod, OptimizerType, Boolean>(FittingMethod.ClosedForm, OptimizerType.DIRECT, new Boolean(false)));
		methods.add(new Triplet<FittingMethod, OptimizerType, Boolean>(FittingMethod.Optimized, OptimizerType.SIMPLEX, new Boolean(false)));
		methods.add(new Triplet<FittingMethod, OptimizerType, Boolean>(FittingMethod.Optimized, OptimizerType.SIMPLEX, new Boolean(true)));
		methods.add(new Triplet<FittingMethod, OptimizerType, Boolean>(FittingMethod.Optimized, OptimizerType.BOBYQA, new Boolean(false)));
		methods.add(new Triplet<FittingMethod, OptimizerType, Boolean>(FittingMethod.Optimized, OptimizerType.BOBYQA, new Boolean(true)));
		
		// This will store results
		HashMap<Integer, HashMap<String, double[]>> results = new HashMap<Integer, HashMap<String, double[]>>();
		
		// Create back-end list
		for (int iterations : iterationSpace) {
			results.put(iterations, new HashMap<String, double[]>());
		}
		
		/*
		 *  Compute
		 */
		for (Triplet<FittingMethod, OptimizerType, Boolean> method : methods) {

			// parse method and optimizers
			FittingMethod fittingMethod = method.getLeft();
			OptimizerType optimizer = method.getCenter();
			boolean useSeeds = method.getRight();

			CartanFitter fitter = new CartanFitter(fittingMethod);
			fitter.setOptimizerType(optimizer);
			
			double[] resultArray = null;
			for (int iterations : iterationSpace) {
				// Only process additional iterations for the simplex optimizer
				if (iterations == iterationSpace[0] || optimizer == OptimizerType.SIMPLEX) {
					System.out.println("\n[ITERATION " + iterations + "] for " + methodLabel(method));
					resultArray = new double[9 + 3 + 1];
					
					CartanFittingParameter fittingParameter = new CartanFittingParameter(null, fittingMethod, useSeeds, numThreads, iterations, errorThreshold, 0, regularization, model, neighborhoodShape, nsize);
					// Set bounds for BOBYQA optimizer
					if (method.getCenter() == OptimizerType.BOBYQA) 
						fittingParameter.setBounds(PamiExperimentRunner.getTheoreticalBounds(fittingParameter));
					else 
						fittingParameter.setBounds(null);
					
					FittingData fittingData = new FittingData(fittingParameter, heart.getFrameField(), heart.getVoxelBox());
					FittingExperimentResult fittingResult = new FittingExperimentResult(fittingData);
					CartanFitter.setVerboseLevel(3);
					
					// Launch fit
					fitter.launchFit(fittingResult, true);

					// Compute mean
					CartanParameterNode mean = PostProcess(fittingResult, validate);
					
					for (int i = 0; i < 9; i++)
						resultArray[i] = mean.get(i);
					for (int i = 0; i < 3; i++)
						resultArray[9+i] = mean.getError()[i];
					
					// measure in ms per node
//					resultArray[9+3] = fittingResult.getElapsedTime();
					resultArray[9+3] = 1000 * 1000 * fittingResult.getElapsedTime() / heart.getVoxelBox().countVoxels();

				}
				
				results.get(iterations).put(methodLabel(method), resultArray);
//				results.get(iterations).add(new Pair<String, double[]>(method.getLeft().toString() + method.getRight(), resultArray));
			}			
		}

		// Create list of measurement labels
		// Index of the label should correspond to the data ordering
		List<String> measurementLabels = new ArrayList<String>();
		for (int parameterIndex = 0; parameterIndex < 9; parameterIndex++)
			measurementLabels.add(CartanParameter.CartanModel.FULL.getAvailableConnections().get(parameterIndex).toString());
		for (int errorIndex = 0; errorIndex < 3; errorIndex++)
			measurementLabels.add("\\epsilon_" + (errorIndex+1));
		String time_label = "time (ms per kilovoxels)";
		measurementLabels.add(time_label);

		// Map from a measurement type (string) to all measurements from different methods (string) for all iterations
		HashMap<String, HashMap<String, double[]>> allData = new HashMap<String, HashMap<String, double[]>>();
		
		for (String label : measurementLabels)
			allData.put(label, new HashMap<String, double[]>());

		// Fill-in data
		for (Triplet<FittingMethod, OptimizerType, Boolean> method : methods) {
			
			// Create data structure to hold all measurements for this method
			double[][] methodData = new double[measurementLabels.size()][iterationSpace.length];

			// For each iteration
			for (int iterationIndex = 0; iterationIndex < iterationSpace.length; iterationIndex++) {
				double[] iterationData = results.get(iterationSpace[iterationIndex]).get(methodLabel(method));
				
				// Fetch and fill-in measurements
				for (int i = 0; i < measurementLabels.size(); i++) {
					methodData[i][iterationIndex] = iterationData[i];
				}
			}
			
			// Add to hashmap of method results
			for (int i = 0; i < measurementLabels.size(); i++) {
				allData.get(measurementLabels.get(i)).put(methodLabel(method), methodData[i]);
			}
			
		}			
		
		/*
		 *  Combine all results into a Matlab script
		 */
		MatlabScript script = new MatlabScript();
		
		// Create the iteration space variable (X)
		script.variable("iterations", iterationSpace);

		/*
		 *  Plot connection form evolution as a function of the number of iterations
		 */
		int lineWidth = 2;
		
		// For each measurement
		int measureIndex = 0;
		script.figure(1);
		for (String measureLabel : measurementLabels) {
//		for (String measureLabel : Arrays.asList(measurementLabels.get(0), measurementLabels.get(measurementLabels.size() - 2))) {
//		for (String measureLabel : Arrays.asList(measurementLabels.get(0))) {
			// Get all data for this measure
			HashMap<String, double[]> dataMethods = allData.get(measureLabel);

			// Prepare plot
//			script.figure(1+measureIndex);
			script.writeLine("clf");
			script.hold(true);

			// Fetch per-method data
			int methodIndex = 0;
			List<String> lineStyles = Arrays.asList("-", "--");
			for (Triplet<FittingMethod, OptimizerType, Boolean> method : methods) {
				double[] y = dataMethods.get(methodLabel(method));
//				script.variable(measureLabel, y);
				String varName = "data" + methodIndex;
				script.variable(varName, y);
//				script.writeLine("plotHandle = plot(" + "iterations" + "," + "rad2deg(" + measureLabel + ")" + ", '-o','LineWidth', " + lineWidth + ",'color', " + script.toString(MathToolset.getColors(methods.size()).get(methodIndex)) + ")");
//				String style = "'-o'";
//				// change style
//				if (method.getLeft() == OptimizerType.BOBYQA || method.getLeft() == OptimizerType.DIRECT)
//					style = "'-'";
				String style = "'" + lineStyles.get(methodIndex % lineStyles.size()) + (method.getRight() && method.getCenter() == OptimizerType.SIMPLEX ? "o" : "") + "'";
				
				script.writeLine("plotHandle = plot(" + "iterations" + "," + varName + ", " + style + ",'LineWidth', " + lineWidth + ",'color', " + script.toString(MathToolset.getColors(methods.size()).get(methodIndex)) + ")");
				methodIndex++;
			}
			
			// Done plotting
			script.hold(false);
			script.setXLabel("iterations", 24);
			script.setYLabel(measureLabel, 24);
			script.setAxisFontSize(24);

			List<String> methodLegend = new LinkedList<String>();
			for (Triplet<FittingMethod, OptimizerType, Boolean> method : methods)
				methodLegend.add(methodLabel(method));
			
			script.legend(methodLegend, 18, "NorthEast");
			script.export_fig(new File(output + measureLabel + ".pdf").getAbsolutePath(), true, true);
			measureIndex++;
		}
		
		/*
		 * Compare c_ijks and errors at max iteration for all methods
		 */
		script.figure(1);
		script.writeLine("clf");
		
		// For each method
		script.hold(true);
		int methodIndex = 0;
		for (Triplet<FittingMethod, OptimizerType, Boolean> method : methods) {
			// Plot connection and errors lifeline
			double[] values = new double[measurementLabels.size()];
			int valueIndex = 0;
			for (String measureLabel : measurementLabels) {
				// Skip timings
				if (measureLabel.toLowerCase().contains(time_label))
					continue;
				
				// Get all data for this measure
				HashMap<String, double[]> dataMethods = allData
						.get(measureLabel);
				double[] y = dataMethods.get(methodLabel(method));
				values[valueIndex++] = y[y.length - 1];
			}
			String varName = "data" + methodIndex;
			script.variable(varName, values);
			script.writeLine("plot(" + varName
					+ ", 's', 'MarkerSize', 15, 'LineWidth', 2,'color', "
					+ script.toString(MathToolset.getColors(methods.size())
							.get(methodIndex++)) + ")");
		}

		List<String> methodLegend = new LinkedList<String>();
		for (Triplet<FittingMethod,OptimizerType, Boolean> method : methods)
			methodLegend.add(methodLabel(method));

		// Done plotting
		script.hold(false);
		
		script.legend(methodLegend, 24, "SouthEast");
		
		script.writeLine("set(gca,'XTick',1:12)");
		script.writeLine("xlim([0 13])");
		script.writeLine("xangle = 45");
		script.setAxisFontSize(24);
		script.writeLine("[hx, hy] = format_ticks(gca, {'$c_{121}$', '$c_{122}$', '$c_{123}$', '$c_{131}$', '$c_{132}$', '$c_{133}$', '$c_{231}$', '$c_{232}$', '$c_{233}$', '$\\varepsilon_1$', '$\\varepsilon_2$', '$\\varepsilon_3$'}, [], [], [], xangle, [], [])");
		script.writeLine("set(hx, 'FontSize', 28)");

		script.export_fig(new File(output + "allparameters.pdf").getAbsolutePath(), true, true);

		/*
		 * Print buffer
		 */
		System.out.println(script.toString());
		
		TextTransfer.setClipboardContents(script.toString());
	}
	
	private static String methodLabel(Triplet<FittingMethod, OptimizerType, Boolean> method) {
		String s;

		if (method.getLeft() == FittingMethod.Optimized) {
			s = method.getCenter().toString();
			s += method.getRight() ? "-S" : "-NS"; 
		}
		else
			s = method.getLeft().toString();
			
		return s;
	}

	private static CartanParameterNode PostProcess(FittingExperimentResult result, boolean validate) {

		PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(
				result.getFittingData());

		if (validate)
			PamiExperimentRunner.validate(result);
		
		// Recompute error in Omega_n
		result.getFittingData().setNeighborhoodShape(NeighborhoodShape.Isotropic);
//		result.getFittingData().setNeighborhoodShape(NeighborhoodShape.SixNeighbors);
		
		for (CartanParameterNode node : result.getResultNodes()) {
			// System.out.println(node.toStringShort());
			node.setError(fittingError.evaluate(node, result.getFittingData().getNeighborhood(node.getPosition())));
		}

		CartanParameterNode mean = CartanParameterNode.computeMean(
				result.getResultNodes(), CartanModel.FULL);
		System.out.println("Mean = " + mean.toStringShort());

		return mean;
	}
	
}
