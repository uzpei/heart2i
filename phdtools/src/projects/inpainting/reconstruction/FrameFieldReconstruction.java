package projects.inpainting.reconstruction;

import heart.Heart;
import heart.HeartTools;

import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import tools.TextToolset;
import tools.frame.VolumeSampler;
import tools.geom.MathToolset;
import tools.interpolation.GaussianInterpolator;
import tools.interpolation.GaussianInterpolator.METRIC;
import tools.interpolation.ParameterVector;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer.OptimizerSetting;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;

public class FrameFieldReconstruction {
	
	public static boolean verbose = false;
	
	public enum ReconstructionMode { None, AffineDiffusion, VectorDiffusion, VectorInterpolation, RuleBased, Inpainting }

	/**
	 * Compute the intersection of ~damaged_mask and mask 
	 * @param maskInput
	 * @param maskDamaged
	 * @return
	 */
	public static IntensityVolumeMask getReconstructionTargets(IntensityVolumeMask maskInput, IntensityVolumeMask maskDamaged) {

		// Get missing voxels (including background)
		IntensityVolumeMask maskReconstruction = new IntensityVolumeMask(maskDamaged.notOut().getData());

		// Intersect with full mask
		maskReconstruction.hp(maskInput);
		
		return maskReconstruction;
//		return maskInput;
	}

	/**
	 * @param mode the type of reconstruction method
	 * @param fieldInput the original frame field
	 * @param fieldDamaged the damaged frame field (to be reconstructed)
	 * @param connectionsDamaged the computed connection forms within the damaged volume
	 */
	public static VoxelFrameField reconstruct(ReconstructionMode mode, VoxelFrameField fieldInput, VoxelFrameField fieldDamaged, OptimizerSetting optimizer, Heart heart) {
		// Use a max of x iterations (could be passed as parameter). 
		// shoud be at least as large as the inter-slice spacing
		int numIterations = 40;

		if (mode == ReconstructionMode.None)
			numIterations = 0; 
			
		// Construct an exclusive mask of the voxels that have to be reconstructed
		IntensityVolumeMask maskReconstruction = getReconstructionTargets(fieldInput.getMask(), fieldDamaged.getMask());

		return FrameFieldReconstruction.reconstruct(fieldDamaged, maskReconstruction, optimizer, 3, numIterations, mode, heart);
	}

	/**
	 * @param damagedFrameField the frame field to reconstruct
	 * @param maskAvailable the corresponding nodes within this frame field that are marked as available (or valid). This can be different than damagedFrameField.getMask() but should at least be contained within in.
	 * @param maskToReconstruct the nodes that need to be reconstructed within this frame field.
	 * @param connectionsData 
	 * @param reconstructionRadius
	 * @param maxIterations
	 * @param mode
	 * @return
	 */
	public static VoxelFrameField reconstruct(VoxelFrameField damagedFrameField, IntensityVolumeMask maskToReconstruct, OptimizerSetting optimizer, int reconstructionRadius, int maxIterations, ReconstructionMode mode, final Heart ruleBasedHeart) {
				
		// Start working with a copy of the original damaged frame field
		VoxelFrameField reconstructed = damagedFrameField.voxelCopy();

		if (verbose)
			System.out.println("Available / target voxels = " + new VoxelBox(reconstructed.getMask()).countVoxels() + " / " + new VoxelBox(maskToReconstruct).countVoxels());

		/*
		 * This mask contains an up-to-date volume of voxels that have to be reconstructed.
		 * It should get updated each iteration and eventually get to zero.
		 */
		IntensityVolumeMask targetMaskUpdated = new IntensityVolumeMask(maskToReconstruct);
		
		int iteration = 0;
		
		while (stillRunning(iteration, maxIterations, targetMaskUpdated)) {
			if (verbose)
				System.out.println(TextToolset.box("Running reconstruction iteration #" + iteration));
			int numTargets = new VoxelBox(targetMaskUpdated).countVoxels();
//			System.out.println(numTargets + " voxels are marked for reconstruction.");

			switch (mode) {
			case None:
				// Do nothing, will return original
				break;
			case RuleBased:
//				reconstructed = ruleBasedHeart.getFrameField();
				targetMaskUpdated = reconstructed.getMask();
				for (Point3i p : new VoxelBox(maskToReconstruct).getVolume()) {
					reconstructed.set(p, ruleBasedHeart.getFrameField().get(p));
					targetMaskUpdated.set(p, 1);
				}
				reconstructed.setMask(targetMaskUpdated);
				iteration = maxIterations;
				break;
			case AffineDiffusion:
				/*
				 *  Reconstruct by diffusing affine connections
				 */
				reconstructed = reconstructAffineDiffusion(reconstructed, optimizer, targetMaskUpdated, reconstructionRadius);
				break;
			case Inpainting:
				/*
				 *  Reconstruct by combining affine diffusion, rule-based info, and interpolation
				 */
								
				// Whether vector boundaries should be padded (diffused before computing the connection forms)
				boolean preSmoothing = false;
				boolean useRuleBased = true;
				boolean useFinalRegularization = true;
				boolean useAffine = true;
				int numRegs = 1;
				
				// first iteration seed with rule-based
//				if (useRuleBased && iteration == 0) {
//					reconstructed = reconstruct(reconstructed, maskToReconstruct, optimizer, reconstructionRadius, maxIterations, ReconstructionMode.RuleBased, ruleBasedHeart);					
//					// make sure we actually run the diffusion
//					reconstructed.setMask(damagedFrameField.getMask());
//				}
				
				// smooth out the entire field before doing any computations
				if (preSmoothing && iteration == 0) {
//					reconstructed = reconstructVectorDiffusion(reconstructed, reconstructed.getMask(), maskToReconstruct, reconstructionRadius);
					IntensityVolumeMask mask = new IntensityVolumeMask(reconstructed.getMask());
					for (int i = 0; i < 1; i++) {
						reconstructed = reconstructVectorDiffusion(reconstructed, reconstructed.getMask(), reconstructed.getMask(), reconstructionRadius);
					}
					reconstructed.setMask(mask);
				}
				
				if (useAffine)
					reconstructed = reconstructAffineDiffusion(reconstructed, optimizer, targetMaskUpdated, reconstructionRadius);
//					reconstructed = reconstructAffineInpainting(reconstructed, optimizer, targetMaskUpdated, reconstructionRadius);

				if (useRuleBased && ruleBasedHeart != null) {
//					rulebasedProject(reconstructed, maskToReconstruct, ruleBasedHeart);
					rulebasedProject(reconstructed, targetMaskUpdated, ruleBasedHeart);
				}
				
				// update mask preemptively for this method such that we can test termination
				targetMaskUpdated = new IntensityVolumeMask(maskToReconstruct.hpOut(reconstructed.getMask().notOut()));
				if (!stillRunning(iteration, maxIterations,  targetMaskUpdated)) {
					/*
					 * When we are done, run a few iterations of diffusion to regularize result
					 * (basically neighbor voting)
					 */
					if (useFinalRegularization) {
						for (int i = 0; i < numRegs; i++)
							reconstructed = reconstructVectorDiffusion(reconstructed, maskToReconstruct, maskToReconstruct, reconstructionRadius);
					}
				}
				
				break;
			case VectorDiffusion:
				/*
				 *  Reconstruct by diffusing vector themselves
				 */
//				reconstructed = reconstructVectorDiffusion(reconstructed, targetMaskUpdated, maskToReconstruct, reconstructionRadius);
				for (int i = 0; i < 1; i++) {
					reconstructed = reconstructVectorDiffusion(reconstructed, targetMaskUpdated, maskToReconstruct, reconstructionRadius);
				}
				break;
			case VectorInterpolation:
				/*
				 * Reconstruct via a pure distance-weighted interpolation
				 */
//				reconstructed = reconstructVectorInterpolation(reconstructed, targetMaskUpdated, reconstructionRadius);
				reconstructed = reconstructVectorInterpolation(reconstructed, maskToReconstruct, reconstructionRadius);
				break;
			default:
				break;
			}

			// Update the targetMask as the intersection between the initial target and the NOT of the reconstructed voxels
			targetMaskUpdated = new IntensityVolumeMask(maskToReconstruct.hpOut(reconstructed.getMask().notOut()));

			// Output info
			int newTargets = new VoxelBox(targetMaskUpdated).countVoxels();
			int delta = numTargets - newTargets;
			
			if (verbose)
				System.out.println(delta + "/" + numTargets + " were effectively reconstructed. Leftover = " + newTargets);
			
			iteration++;
		}
		
		if (iteration > 0) {
			if (iteration == maxIterations && verbose)
				System.out.println("Terminating since max number of iterations was reached (" + iteration + ")");
			else if (verbose)
				System.out.println("Terminating since reconstruction converged at iteration " + iteration);
		}
		else {
			System.out.println("Nothing to reconstruct.");
		}
		
		
		return reconstructed;
	}
	
	private static void rulebasedProject(final VoxelFrameField frameField, IntensityVolumeMask target, final Heart ruleBasedHeart) {
		
		double phi1 = 0.1;
		double phi3 = 0.3;
//		double phi1 = 1;
//		double phi3 = 1;
		
		// Replace third frame (local wall normal) axis rule-based model of real data
		// (only uses wall geometry info)
		for (Point3i p : new VoxelBox(target).getVolume()) {
//		for (Point3i p : new VoxelBox(target.getDimension()).getVolume()) {
			// Get current
			Vector3d[] f = frameField.get(p.x, p.y, p.z);
			
			// Get rule
			Vector3d[] frule = ruleBasedHeart.getFrameField().get(p.x, p.y,  p.z);
			
			// Mix f1 with rule-based prior
			f[0].scale(1-phi1);
			f[0].scaleAdd(phi1, frule[0], f[0]);
			f[0].normalize();
			
			// Mix f3 with rule-based prior
			f[2].scale(1-phi3);
			f[2].scaleAdd(phi3, frule[2], f[2]);

			// Orthogonalize f3 w.r.t f1
			f[2].scaleAdd(-f[2].dot(f[0]), f[0], f[2]);
			f[2].normalize();
			
//			f[0].scale(0);
//			f[1].scale(0);
//			f[2].scale(0);
			
			// Orthogonalize f1 w.r.t f3
//			f[0].scaleAdd(-f[0].dot(f[2]), f[2], f[0]);
//			f[0].normalize();

			// Generate f1 based on new estimate
			f[1].cross(f[2], f[0]);
			frameField.set(p.x, p.y, p.z, f);
			
			// update mask
//			frameField.getMask().set(p, 1);
		}

		// Make sure to update sub-masks (framefield -> f1, f2, f3)
//		frameField.setMask(frameField.getMask());
	}

	private static boolean stillRunning(int iteration, int maxIterations, IntensityVolumeMask mask) {
		return new VoxelBox(mask).countVoxels() > 0 && iteration < maxIterations;
	}

	public static VoxelFrameField reconstructRuleBased(final Heart heart, final IntensityVolumeMask target) {

		// Construct rule-based heart
		final Heart ruled = HeartTools.computeRuleBased(heart);
		
		// Replace available input data
		IntensityVolumeMask mask = new IntensityVolumeMask(heart.getMask());
		mask.sub(target);
		
		final Point3i pt = new Point3i();
		new VoxelBox().voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				pt.set(x,y,z);
				Vector3d[] f = heart.getFrameField().get(pt);
				ruled.getFrameField().set(x, y, z, f);
			}
		});
		
		return ruled.getFrameField();
	}

	private static IntensityVolumeMask getComputationMask(final VoxelBox boxTarget, final VoxelBox boxData, final List<Point3i> neighbors) {
		
		final IntensityVolumeMask mask = new IntensityVolumeMask(boxTarget.getDimension());
		final Point3i p = new Point3i();		
		final Point3i q = new Point3i();
		boxTarget.voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				// Process this reconstructed voxel
				p.set(x, y, z);
				// Visit neighbors in a ball around p
				boxData.neighborhoodProcess(p, neighbors, new PerVoxelMethodUnchecked() {
					@Override
					public void process(int x, int y, int z) {
						// Process this neighbor
						q.set(x, y, z);
						mask.set(q, 1);;
					}});
				
			}});
				
		return mask;
	}
	
	/**
	 * @param data the current reconstructed frame field
	 * @param connectionsData the corresponding connection forms for the available frame field nodes 
	 * @param target the target region to reconstruct
	 * @param reconstructionRadius the length of isotropic diffusion to perform the reconstruction for in voxel neighbors
	 * @return
	 */
	public static VoxelFrameField reconstructAffineDiffusion(final VoxelFrameField data, OptimizerSetting optimizer, final IntensityVolumeMask target, int reconstructionRadius) {

		// Initialize the reconstruction voxel frame field
		VoxelFrameField reconstructed = data.voxelCopy();
//		final VoxelFrameField reconstructed = new VoxelFrameField(data.getDimension());

		boolean interpolateBoundary = false;
		boolean doAffine = true;
		VoxelBox boxTarget = new VoxelBox(target);
		VoxelBox boxData = new VoxelBox(data.getMask());
		final List<Point3i> neighbors = VolumeSampler.sample(NeighborhoodShape.Isotropic, reconstructionRadius);
		boolean useDefault = false;

		if (interpolateBoundary) {
			reconstructed = reconstructVectorInterpolation(reconstructed, target, reconstructionRadius);
//			targetMaskUpdated = new IntensityVolumeMask(maskToReconstruct.hpOut(reconstructed.getMask().notOut()));
		}
		
		if (!doAffine)
			return reconstructed;
		
		
		/*
		 *  IPMI2015: recompute connection forms in current available data.
		 *  Multi-step pass, identify the following:
		 *  1) island nodes -> dF = 0
		 *  2) nonsingular -> dF = closed-form
		 *  3) singular -> dF = optimized with direct seeds
		 */
				
		List<IntensityVolume> cijk_data;
		if (useDefault) {
			cijk_data = DifferentialOneForm.connectionFormsDefault(data, optimizer.method, optimizer.type);
			
		}
		else {
			// Create a diffused mask of voxels for which connections have to be computed
			IntensityVolumeMask computationMask = getComputationMask(boxTarget, boxData, neighbors);
			cijk_data = DifferentialOneForm.connectionFormsIpmi(data, computationMask);
		}
		final CartanParameterNode[][][] connectionsData = DifferentialOneForm.asArray(cijk_data, reconstructed.getMask());
//		final CartanParameterNode[][][] connectionsData = DifferentialOneForm.connectionFormParametersDirect(reconstructed);
		
		// Current voxel
		reconstructed.setMask(data.getMask());
//		final Point3i p = new Point3i();
		
		// Data voxel
		final Point3i q = new Point3i();
		
		final IntensityVolumeMask reconstructedMask = new IntensityVolumeMask(data.getMask());

		final Point3i v = new Point3i();
		final Vector3d[] Fpe = new Vector3d[3];
		Fpe[0] = new Vector3d();
		Fpe[1] = new Vector3d();
		Fpe[2] = new Vector3d();

		final Vector3d[] fsum = new Vector3d[3];
		fsum[0] = new Vector3d();
		fsum[1] = new Vector3d();
		fsum[2] = new Vector3d();
		
		final VoxelFrameField reconstructedRef = reconstructed;
		
		// For each voxel that is to be reconstructed
		for (final Point3i p : boxTarget.getVolume()) {
			
			// Initialize unnormalized frame expansion to zero
			for (int fi = 0; fi < 3; fi++)
				fsum[fi].set(0, 0, 0);

//				Vector3d[] fp = data.get(p);
//				for (int fi = 0; fi < 3; fi++)
//					fsum[fi] = fp[fi];
			
			// Visit neighbors in a ball around p
			boxData.neighborhoodProcess(p, neighbors, new PerVoxelMethodUnchecked() {
				@Override
				public void process(int x, int y, int z) {
					// Process this neighbor
					q.set(x, y, z);
					
					// Fetch neighboring's F_i(q)
					Vector3d[] Fpv = data.get(q);

					// Fetch connection form at neighbor
					CartanParameterNode cp = connectionsData[q.x][q.y][q.z];
					
					// Skip if this neighbor has no connection form set
					if (cp == null) {
						System.err.println("AffineDiffusion: Null connection form.");
					}

					// Extrapolate from the neighbor q to p (not normalized)
					v.sub(p, q);
					Vector3d dv = MathToolset.tuple3iToVector3d(v);
					DifferentialOneForm.extrapolate(cp, dv, Fpv, Fpe, false);

					// Accumulate
					for (int fi = 0; fi < 3; fi++)
						if (!MathToolset.isNan(Fpe[fi]))
							fsum[fi].add(Fpe[fi]);
				}
			});

			// Done processing neighbor. Now normalize.
			boolean notEnoughData = false;
			for (int fi = 0; fi < 3; fi++) {
				if (!MathToolset.isNan(fsum[fi]) && fsum[fi].length() > 1e-8)
					fsum[fi].normalize();
				else {
					notEnoughData = true;
					break;
//						fsum[fi].set(0, 0, 0);
				}
			}

			if (!notEnoughData) {
				// Set reconstructed frames
				reconstructedRef.set(p, fsum);

				// Mark this voxel as processed
				reconstructedMask.set(p, 1);
			}
		}
				
		reconstructedRef.setMask(reconstructedMask);
		reconstructedRef.setVoxelBox(new VoxelBox(reconstructedMask));

		return reconstructedRef;
	}
	
	/**
	 * Reconstruct by diffusing vectors within the boundary. 
	 * @param data the available frame field nodes
	 * @param connectionsData the corresponding connection forms for the available frame field nodes 
	 * @param currentTarget the target region to reconstruct
	 * @param reconstructionRadius the length of isotropic diffusion to perform the reconstruction for in voxel neighbors
	 * @return
	 */
	public static VoxelFrameField reconstructVectorDiffusion(final VoxelFrameField data, final IntensityVolumeMask currentTarget, final IntensityVolumeMask allTargets, int reconstructionRadius) {

		// Initialize the reconstruction voxel frame field
		final VoxelFrameField reconstructed = data.voxelCopy();
//		final VoxelFrameField reconstructed = new VoxelFrameField(data.getDimension());
		
		// Current voxel
		final Point3i p = new Point3i();
		
		// Data voxel
		final Point3i q = new Point3i();
		
		// Construct the voxel box for traversing the volume
//		final VoxelBox boxTarget = new VoxelBox(currentTarget);
		final VoxelBox boxTarget = new VoxelBox(allTargets);
		final VoxelBox boxData = new VoxelBox(data.getMask());

		final List<Point3i> neighbors = VolumeSampler.sample(NeighborhoodShape.Isotropic, reconstructionRadius);
		final IntensityVolumeMask reconstructedMask = new IntensityVolumeMask(data.getMask());

		final Vector3d[] fsum = new Vector3d[3];
		fsum[0] = new Vector3d();
		fsum[1] = new Vector3d();
		fsum[2] = new Vector3d();
		
		// Take all target voxels to reconstruct
		boxTarget.voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				// Process this reconstructed voxel
				p.set(x, y, z);
				
				// Initialize unnormalized frame expansion to zero
				for (int fi = 0; fi < 3; fi++)
					fsum[fi].set(0, 0, 0);
				
				// Visit neighbors in a ball around the current voxel being diffused to
				boxData.neighborhoodProcess(p, neighbors, new PerVoxelMethodUnchecked() {
					@Override
					public void process(int x, int y, int z) {
						// Process this neighbor
						q.set(x, y, z);
						
						// Fetch neighbor's frame and accumulate
						Vector3d[] Fpv = data.get(q);
						
						for (int fi = 0; fi < 3; fi++)
							if (!MathToolset.isNan(Fpv[fi]))
								fsum[fi].add(Fpv[fi]);
					}
				});

				// Done processing neighbor. Now normalize.
				boolean notEnoughData = false;
				for (int fi = 0; fi < 3; fi++) {
					if (!MathToolset.isNan(fsum[fi]) && fsum[fi].length() > 1e-8)
						fsum[fi].normalize();
					else {
						notEnoughData = true;
						break;
//						fsum[fi].set(0, 0, 0);
					}
				}

				if (!notEnoughData) {
					// Set reconstructed frames
					reconstructed.set(p, fsum);

					// Mark this voxel as processed
					reconstructedMask.set(x, y, z, 1);
				}
			}
		});
				
		reconstructed.setMask(reconstructedMask);
		reconstructed.setVoxelBox(new VoxelBox(reconstructedMask));

		return reconstructed;
	}
	
	/**
	 * @param currentDamagedField the available frame field nodes
	 * @param target the target region to reconstruct
	 * @param reconstructionRadius the length of isotropic diffusion to perform the reconstruction for in voxel neighbors
	 * @return
	 */
	public static VoxelFrameField reconstructVectorInterpolation(final VoxelFrameField currentDamagedField, final IntensityVolumeMask target, int reconstructionRadius) {

		// FIXME: need to set this explicitly
		// if EXP is used, must be extra careful with sigma
		METRIC interpolationMetric = METRIC.SPATIAL;
		
		final VoxelFrameField reconstructed = currentDamagedField.voxelCopy();

		IntensityVolumeMask maskData = currentDamagedField.getMask();

		/*
		 *  Diffuse the current mask
		 *  then extract the 1-boundary of this mask
		 *  These will be our reconstruction targets for this iteration
		 */
		if (verbose)
			System.out.println("Original voxels = " + new VoxelBox(maskData).getVolume().size());
		IntensityVolumeMask maskBoundary = new IntensityVolumeMask(maskData).erode(reconstructionRadius);
		
		if (verbose)
			System.out.println("after erosion = " + new VoxelBox(maskBoundary).getVolume().size());
		
		maskBoundary = new IntensityVolumeMask(maskBoundary.hpOut(target));
		
		// List of points to interpolate
		List<Point3i> toInterpolate = new VoxelBox(maskBoundary).getVolume();

		if (verbose)
			System.out.println(toInterpolate.size() + " voxels marked for interpolation");
		
		// Stores a coordinate frame
		Vector3d[] frames = new Vector3d[3];
		frames[0] = new Vector3d();
		frames[1] = new Vector3d();
		frames[2] = new Vector3d();

		// Setup interpolation
		GaussianInterpolator interpolator = new GaussianInterpolator(interpolationMetric);
		LinkedList<ParameterVector> controlPoints = new LinkedList<ParameterVector>();
		
		List<Point3i> interpolants = VolumeSampler.sample(NeighborhoodShape.Isotropic, 5);

		Point3i pinterp = new Point3i();
		
		// Go through every point that needs to be interpolated
		for (Point3i pt : toInterpolate) {
		
			// Assemble control points: 9 components for f1, f2, f3 vectors
			controlPoints.clear();
			for (Point3i pi : interpolants) {
				pinterp.add(pt, pi);
				
				// Only use valid interpolants
				if (!maskData.contains(pinterp.x, pinterp.y, pinterp.z) || maskData.isMasked(pinterp))
					continue;
				
				Vector3d[] f = currentDamagedField.get(pinterp);
				// For now only interpolate f1
//				double[] f1 = new double[] { f[0].x, f[0].y, f[0].z, f[1].x, f[1].y, f[1].z, f[2].x, f[2].y, f[2].z };
				double[] f1 = new double[] { f[0].x, f[0].y, f[0].z };
				controlPoints.add(new ParameterVector(pinterp.x, pinterp.y, pinterp.z, f1));
			}
			
			// Interpolate
			if (controlPoints.size() > 0) {
				ParameterVector interpolated = interpolator.interpolate(MathToolset.tuple3iToPoint3d(pt), controlPoints);
				
				// Set result
				frames[0].set(interpolated.data[0], interpolated.data[1], interpolated.data[2]);

				if (Double.isNaN(frames[0].x)) {
					System.out.println("Vector interpolation: nan found at " + pt + " :" + frames[0]);
				}
				
				frames[0].normalize();
//				frames[1].set(interpolated.data[3], interpolated.data[4], interpolated.data[5]);
//				frames[2].set(interpolated.data[6], interpolated.data[7], interpolated.data[8]);
//				System.out.println(frames[0].length());
				reconstructed.set(pt.x, pt.y, pt.z, frames);
				maskData.set(pt.x, pt.y, pt.z, 1);
			}
		}
		
		reconstructed.setMask(maskData);
		
//		System.out.println("Updated count = " + new VoxelBox(maskData).getVolume().size());

		return reconstructed;
	}
	
	/**
	 * Compute the per-voxel angular error (orientation) between two vector fields.
	 * This error has a maximum of pi/2 since we always pick the absolute value of the dot product
	 * Assume the two vector fields have the same dimensions.
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static IntensityVolume error(final VoxelVectorField v1, final VoxelVectorField v2, IntensityVolumeMask computationMask) {
		final IntensityVolume errorVolume = new IntensityVolume(v1.getDimensions()); 

		// Process both volumes
		VoxelBox box = new VoxelBox(computationMask);
		
		final Vector3d vv1 = new Vector3d();
		final Vector3d vv2 = new Vector3d();
		
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				v1.getVector3d(x, y, z, vv1);
				v2.getVector3d(x, y, z, vv2);
				
				// It's not our job to normalize.
//				vv1.normalize();
//				vv2.normalize();
				
				double proj = Math.abs(vv1.dot(vv2));
//				vv1.negate();
//				proj = Math.max(proj, vv1.dot(vv2));
				proj = MathToolset.clamp(proj, 0, 1);
				double theta = Math.acos(Math.abs(proj));
				
				// Take smallest angle (i.e. compare orientation only)
				double error = theta / Math.PI * 180;
				errorVolume.set(x, y, z, error);
//				errorVolume.set(x, y, z, 45);
				if (Double.isNaN(error)) {
//					System.nanoTime();
					System.out.println("Error computations: projection = " + proj + " | NaN error between vectors: " + vv1 + "," + vv2);
				}
			}
		});
		
		return errorVolume;
	}
}

