package projects.inpainting.reconstruction;

import heart.Heart;
import heart.HeartTools;

import java.util.Random;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import diffgeom.DifferentialOneForm;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameterNode;

public class ExpIntrinsic {

	private enum DamageType { Slices, Holes }
	
	public static void main(String[] args) {
		new ExpIntrinsic();
	}
	
	public ExpIntrinsic() {
		// Input: frame field, mask, reconstruction mask
		
		DamageType damageType = DamageType.Holes;
		
		// This is our original volume
		Heart heart = HeartTools.heart();
		IntensityVolumeMask mask = heart.getMask();
		VoxelFrameField field = heart.getFrameField();
		
		// Compute our damaged and target reconstruction masks
		IntensityVolumeMask maskAvailable = null, maskTarget = null;
		if (damageType == DamageType.Slices) {
			int numSamples = 10;
			IntensityVolumeMask[] slices = HeartTools.sliceLongitudinally(mask, numSamples);
			maskAvailable = slices[0];
			maskTarget = slices[1];
		}
		else if (damageType == DamageType.Holes) {
			IntensityVolumeMask[] slices = HeartTools.perforate(mask, 7);
			maskAvailable = slices[0];
			maskTarget = slices[1];
		}
		
		/*
		 *  Construct the computation frame fields
		 */
		// This field contains only the available data (longitudinal slices)
		VoxelFrameField fieldData = field.extractField(new VoxelBox(maskAvailable));
		VoxelFrameField fieldReconstructed;
		
		// Compute connection forms within the available volume
		// FIXME: for now we are cheating: connections are computed within the complete volume
		CartanParameterNode[][][] nodes = DifferentialOneForm.connectionFormParametersDirect(field);
		
		int numIterations = 10;

		/*
		 *  Reconstruct by diffusing affine connections
		 */
		fieldReconstructed = fieldData.voxelCopy();
		fieldReconstructed = FrameFieldReconstruction.reconstruct(nodes, fieldReconstructed, maskAvailable, maskTarget, 3, numIterations, ReconstructionMode.AffineDiffusion);
		IntensityVolume errorVolume1 = FrameFieldReconstruction.error(field.getF1(), fieldReconstructed.getF1(), maskTarget);
		System.out.println("Affine: mean error for " + new VoxelBox(maskTarget).countVoxels() + " voxels = " + errorVolume1.getMean(maskTarget));

		/*
		 *  Reconstruct by diffusing affine connections
		 */
		fieldReconstructed = fieldData.voxelCopy();
		fieldReconstructed = FrameFieldReconstruction.reconstruct(nodes, fieldReconstructed, maskAvailable, maskTarget, 3, numIterations, ReconstructionMode.VectorDiffusion);
		IntensityVolume errorVolume2 = FrameFieldReconstruction.error(field.getF1(), fieldReconstructed.getF1(), maskTarget);
		System.out.println("Vector: mean error for " + new VoxelBox(maskTarget).countVoxels() + " voxels = " + errorVolume2.getMean(maskTarget));

		/*
		 * Reconstruct via distance-weighted interpolation
		 */
	}
}
