package projects.inpainting.experiment;

import heart.Heart;
import heart.HeartTools;

import java.util.Arrays;
import java.util.List;

import math.coloring.ColorMap;
import math.coloring.ColorMap.Map;
import projects.inpainting.reconstruction.FrameFieldReconstruction;
import projects.inpainting.reconstruction.FrameFieldReconstruction.ReconstructionMode;
import tools.TextToolset;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import visualization.HeartVolumeSnapshot;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.dti.DiffusionSplitter;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;

public class InpaintingPipeline {

	public enum HoleType { Perforated, Invivo }
	
	public static void main(String[] args) {
		new InpaintingPipeline();
	}
	
	public InpaintingPipeline() {

		/*
		 * Obtain a frame field which will serve as the ground truth
		 * Fetch the field and the mask
		 */
//		VoxelFrameField fieldInput = HeartTools.heartSample().getFrameField();
		Heart heart = HeartTools.heart();
		VoxelFrameField fieldInput = heart.getFrameField();
		IntensityVolumeMask maskInput = fieldInput.getMask();
		
		/*
		 * Compute the "true" connection forms for all available voxels
		 */
		CartanParameterNode[][][] connectionNodesInput = DifferentialOneForm.connectionFormParametersDirect(fieldInput);

		/*
		 * Visualization properties
		 */
		double zoom = 30;
		boolean closeWhenFinished = true;

		/*
		 * Create holes in the input volume.
		 * The parameter vector will have a different meaning depending on the hole type used.
		 */
		HoleType type = HoleType.Invivo;
		double[] params = null;
		switch (type) {
		case Invivo:
			// In-vivo simulates typical acquisitions by sampling the volume longitudinally
			int numSlices = 10;
			params = new double[] { numSlices };
			break;
			//
		case Perforated:
			// Perforated simulates damaged volumes by poisson sampling ellipsoidal holes within the volume
			double perforation = 12;
			params = new double[] { perforation };
			break;
			default:
		}
		
		// Generate a damaged volume
		// i.e. the original with some structured data missing
		IntensityVolumeMask maskDamaged = generateHoles(maskInput, type, params);
		VoxelFrameField fieldDamaged = fieldInput.extractField(new VoxelBox(maskDamaged));

		// This mask contains only the voxels that should be reconstructed
		// it is the AND of maskInput and not-maskDamaged
		IntensityVolumeMask maskTargetOnly = FrameFieldReconstruction.getReconstructionTargets(maskInput, maskDamaged);
		int voxelCountTarget = new VoxelBox(maskTargetOnly).countVoxels();
		int voxelCountTotal = new VoxelBox(maskInput).countVoxels();

		/*
		 *  Display the available nodes as a frame field and save a snapshot
		 */
//		double[] minMax = heart.getDistanceTransformEpi().getminMax();
//		HeartVolumeSnapshot.save(heart.getFrameField(), heart.getDistanceTransformEpi(), zoom, minMax[0], minMax[1], "./inpainting" + type + ".png", false);
//		if (true) 
//			return;
		
		// Estimate connection forms within this partial volume
		CartanParameterNode[][][] connectionNodesDamaged = DifferentialOneForm.connectionFormParametersDirect(fieldInput);
		
		/*
		 * Reconstruct the damaged frame field
		 */
		List<ReconstructionMode> modes = Arrays.asList(ReconstructionMode.None, ReconstructionMode.AffineDiffusion, ReconstructionMode.VectorDiffusion, ReconstructionMode.VectorInterpolation);
//		List<ReconstructionMode> modes = Arrays.asList(ReconstructionMode.VectorDiffusion);
		FrameFieldReconstruction.verbose = false;
		double minError = 1e-6;
		double maxError = 10;
		ColorMap.Map map = Map.JET;
		IntensityVolumeMask showMask = new IntensityVolumeMask(maskTargetOnly);
//		IntensityVolumeMask showMask = new IntensityVolumeMask(maskInput);
		
		for (ReconstructionMode mode : modes) {
			System.out.println(TextToolset.box(mode + ""));
			VoxelFrameField fieldReconstructed = FrameFieldReconstruction.reconstruct(mode, fieldInput, fieldDamaged, connectionNodesDamaged);
			IntensityVolume errorVolume = FrameFieldReconstruction.error(fieldInput.getF1(), fieldReconstructed.getF1(), maskInput);

			// FIXME: For testing purposes
//			IntensityVolume errorVolume = IntensityVolume.createRandomizedVolume(maskInput.getDimension()[0], maskInput.getDimension()[1], maskInput.getDimension()[2]);
//			IntensityVolume errorVolume = fieldReconstructed.getFieldX().getX();
			
			HeartVolumeSnapshot.save(errorVolume, showMask, map, minError, maxError, zoom, "./inpainting" + mode + type + ".png", closeWhenFinished);

			System.out.println(mode + ": mean error (degrees) for " + voxelCountTarget + " voxels (" + MathToolset.toString(100d * voxelCountTarget / voxelCountTotal, 2) + "%) = " + errorVolume.getMean(maskTargetOnly));
		}
		
		System.exit(0);
	}
	
	/**
	 * @param input the input mask to create holes for
	 * @param type the type of simulated holes, currently either invivo or perforated
	 * @param parameters an array of values containing hole type specific parameters
	 * @return a mask containing the original data but with holes in it
	 */
	private IntensityVolumeMask generateHoles(IntensityVolumeMask input, HoleType type, double[] parameters) {
		IntensityVolumeMask holeMask = null;

		if (type == HoleType.Invivo) {
	    	// Compute split mask
			IntensityVolumeMask[] vols = DiffusionSplitter.splitMask(input, FrameAxis.Z, MathToolset.roundInt(parameters[0]));
			holeMask = vols[0];
		}
		else if (type == HoleType.Perforated) {
			IntensityVolumeMask[] perforation = HeartTools.perforate(input, parameters[0]);
			holeMask = perforation[0];
		}
		
		return holeMask;
	}

}
