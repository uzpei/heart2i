package projects.inpainting.sandbox.closedform;

import java.awt.event.KeyAdapter;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;

import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import heart.Heart;
import heart.HeartTools;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import visualization.FrameFieldVisualizer;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.core.experiments.PamiExperimentRunner;

public class ClosedFormComputationDataTest {

	public static void main(String[] args) {
		new ClosedFormComputationDataTest();
	}
	
	public ClosedFormComputationDataTest() {
		final Heart heart = HeartTools.heartSynthetic();
		int voxelCount = heart.getVoxelBox().countVoxels();

		System.out.println("Using " + voxelCount + " voxels.");
		
		FrameFieldVisualizer v = new FrameFieldVisualizer(heart.getFrameField());
		v.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_C){
					compute(heart.getFrameField());
				}
			}
		});

		compute(heart.getFrameField());
	}
	
	private void compute(VoxelFrameField field) {
		int nsize = 3;

		CartanParameterNode resultDirect = computeMeanError(field, FittingMethod.Direct, nsize);
		CartanParameterNode resultClosed = computeMeanError(field, FittingMethod.ClosedForm, nsize);
//		CartanParameterNode resultOptimized = computeMeanError(heart.getFrameField(), FittingMethod.Optimized, nsize);
		
		System.out.println("Direct: " + resultDirect.toString());
		System.out.println("Closed: " + resultClosed.toString());
//		System.out.println(resultOptimized.toString());
	}
	
	private CartanParameterNode computeMeanError(VoxelFrameField frameField, FittingMethod method, int nsize) {
		System.out.println("Computing direct...");
		FittingData dataDirect = new FittingData(new CartanFittingParameter(null, CartanModel.FULL, method, nsize), frameField, new VoxelBox(frameField.getMask()));
		FittingExperimentResult result = new FittingExperimentResult(dataDirect);

		CartanFitter fitter = new CartanFitter(method);
		fitter.launchFit(result, true);

		// Compute the error
		PamiExperimentRunner.validate(result);
		PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(result.getFittingData());
		
		for (CartanParameterNode node : result.getResultNodes()) 
			node.setError(fittingError.evaluate(node, result.getFittingData().getNeighborhood(node.getPosition())));
		
		return CartanParameterNode.computeMean(result.getResultNodes(), dataDirect.getFittingParameter().getCartanModel());
		
	}
}
