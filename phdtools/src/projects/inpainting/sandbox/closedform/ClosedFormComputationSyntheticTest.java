package projects.inpainting.sandbox.closedform;

import gl.geometry.FancyAxis;
import gl.geometry.GLGeometry;
import gl.geometry.primitive.Plane;
import gl.material.GLMaterial;
import gl.renderer.NodeScene;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import diffgeom.DifferentialOneForm.MovingAxis;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import projects.inpainting.sandbox.FrameFieldGenerator;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import system.object.Triplet;
import tools.TextToolset;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterControl;
import app.pami2013.cartan.CartanParameter.CartanModel;

public class ClosedFormComputationSyntheticTest {

	static boolean uiBased = true;
	
	static boolean applyNormalization = true;
	static NodeScene scene;
	static OrientationFrame frame;
	static VoxelFrameField field;
	static VoxelFrameField fieldCenter;
	static Plane plane;
	static CartanParameterControl cpc = new CartanParameterControl(CartanModel.FULL);
	static CartanParameter estimated = new CartanParameter();
	static EnumComboBox<NeighborhoodShape> ecbShape = new EnumComboBox<CartanFittingParameter.NeighborhoodShape>(NeighborhoodShape.IsotropicXY);
	static IntParameter nsize = new IntParameter("size", 1, 1, 5);
	static FancyAxis fa = new FancyAxis();
	static VoxelBox box;
	static int[] dims;
	
	public static void main(String[] args) {
		
		int dimension = 2 * (nsize.getValue()+1) + 1;
		dims = new int[] { dimension, dimension, dimension };
		
//		NeighborhoodShape shape = NeighborhoodShape.Isotropic;
//		CartanParameter parameter = new CartanParameter();parameter.setc123(0.2);parameter.setc232(0.1);
		CartanParameter parameter = new CartanParameter();parameter.setc123(0.2);
//		parameter.randomize(1);
		cpc.set(parameter);
		
		ParameterListener experimentListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				constructExperiment();
			}
		};
		cpc.addParameterListener(experimentListener);
		ecbShape.addParameterListener(experimentListener);
		nsize.addParameterListener(experimentListener);

		if (uiBased) {
			// Create a node scene to show the generated frame field
			scene = new NodeScene(500);
			scene.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
//					if (e.getKeyCode() == KeyEvent.VK_R) {
//						frame = generateFrame(false);
//						runExperiment();		
//					}
					if (e.getKeyCode() == KeyEvent.VK_R) {
						frame = generateFrame(true);
						constructExperiment();
					}
					if (e.getKeyCode() == KeyEvent.VK_C) {
						frame = generateFrame(false);
						constructExperiment();
					}
				}
			});
			
			// TODO: add CartanParamControl, add neighborhood size and shape control
			VerticalFlowPanel vfp = new VerticalFlowPanel();
			vfp.add(cpc.getControls());
			vfp.add(ecbShape.getControls());
			vfp.add(nsize.getSliderControls());
			scene.addControl(vfp.getPanel());

			scene.clearNodes();
			GLGeometry geom = new GLGeometry() {
				
				@Override
				public void init(GLAutoDrawable drawable) {
				}
				
				@Override
				public String getName() {
					return null;
				}
				
				@Override
				public void display(GLAutoDrawable drawable) {
					GL2 gl = drawable.getGL().getGL2();
					gl.glScaled(dims[0], dims[1], dims[2]);
					gl.glPushMatrix();
					if (box != null && field != null) {
						gl.glPushMatrix();
						box.applyViewTransformation(gl);
						for (MovingAxis f : MovingAxis.values()) {
							field.getField(f).setHighDef(true);
							field.getField(f).setDrawTip(true);
							field.getField(f).setDrawRadius(0.02);

							fieldCenter.getField(f).setHighDef(true);
							fieldCenter.getField(f).setDrawTip(true);
							fieldCenter.getField(f).setDrawRadius(0.04);
						}
						
						field.display(drawable);
						fieldCenter.display(drawable);
						
						gl.glPopMatrix();
					}
					
//					fa.display(drawable, 0.5d / dims[0]);
					gl.glPopMatrix();
					List<String> connections = new LinkedList<String>();
//					connections.add(cpc.toStringShort());
//					connections.add(estimated.toStringShort());
					DecimalFormat format = new DecimalFormat(".###");
					connections.add("Original | Estimated");
					for (CartanParameter.Parameter parameter : CartanParameter.CartanModel.FULL.getAvailableConnections()) {
						connections.add(parameter.toString() + ": " + format.format(cpc.get(parameter)) + " | " + format.format(estimated.get(parameter)));
					}
					
					connections.add("SSE = " + format.format(cpc.SSE(estimated)));
					
					scene.getRenderer().getTextRenderer().drawTopLeft(connections, drawable);
				}
			};
			scene.addNode(geom);
		}

		frame = generateFrame(false);
		constructExperiment();
//		runExperiment();
	}
	
	private static OrientationFrame generateFrame(boolean randomize) {
		frame = new OrientationFrame();
		if (randomize)
			frame.randomize();
		
//		OrientationFrame frame = new OrientationFrame(new Vector3d(0.5, -0.5, 0.5));
		System.out.println("Frame = \n" + frame.toString());
		System.out.println("Vectors = ");
		Vector3d f1 = new Vector3d(frame.getFrame().getElement(0, 0), frame.getFrame().getElement(1, 0), frame.getFrame().getElement(2, 0));
		Vector3d f2 = new Vector3d(frame.getFrame().getElement(0, 1), frame.getFrame().getElement(1, 1), frame.getFrame().getElement(2, 1));
		Vector3d f3 = new Vector3d(frame.getFrame().getElement(0, 2), frame.getFrame().getElement(1, 2), frame.getFrame().getElement(2, 2));
		System.out.println(f1 + " norm = " + f1.length());
		System.out.println(f2 + " norm = " + f2.length());
		System.out.println(f3 + " norm = " + f3.length());
		
		return frame;
	}

	protected static void constructExperiment() {
		
		// Generate a field that contains at least twice the size we use for computations
		field = FrameFieldGenerator.generate(dims, frame, cpc, applyNormalization);
		fieldCenter = new VoxelFrameField(dims);
		fieldCenter.set(frame);
		
		box = new VoxelBox(dims);
		box.set(ecbShape.getSelected());
		
		field.setVoxelBox(box);
		field.getF1().setVisible(true);
		field.getF2().setVisible(true);
		field.getF3().setVisible(true);

		fieldCenter.setVoxelBox(box);
		fieldCenter.getF1().setVisible(true);
		fieldCenter.getF2().setVisible(true);
		fieldCenter.getF3().setVisible(true);

		if (uiBased && ecbShape.getSelected() == NeighborhoodShape.IsotropicXY || ecbShape.getSelected() == NeighborhoodShape.IsotropicXZ || ecbShape.getSelected() == NeighborhoodShape.IsotropicYZ) {
			// Set display parameters with respect to neighborhood
			double c = 0.5 + dims[0] / 2;
			Vector3d v1 = null, v2 = null;
			if (ecbShape.getSelected() == NeighborhoodShape.IsotropicXY) {
				v1 = FrameAxis.Y.getAxis();
				v2 = FrameAxis.X.getAxis();
			}
			else if (ecbShape.getSelected() == NeighborhoodShape.IsotropicXZ) {
				v1 = FrameAxis.Z.getAxis();
				v2 = FrameAxis.X.getAxis();
			}
			else if (ecbShape.getSelected() == NeighborhoodShape.IsotropicYZ) {
				v1 = FrameAxis.Y.getAxis();
				v2 = FrameAxis.Z.getAxis();
			}
			
			plane = new Plane(new Point3d(c, c, c), v1, v2);
			plane.setDrawGrid(true);
			plane.setMaterial(GLMaterial.SOFTWHITE);
			plane.setSize(dims[0], dims[1], dims[2]);
		}
		
		// Check whether our frame field is everywhere orthogonal
		final double[] orthoSum = new double[] { 0 };
		new VoxelBox(field.getDimension()).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				Vector3d[] f = field.get(x, y, z);
				orthoSum[0] += f[0].dot(f[1]) + f[0].dot(f[2]) + f[1].dot(f[2]);
			}
		});
		
		System.out.println("Ortho sum = " + orthoSum[0]);
		
		runExperiment();
	}

	public static void runExperiment() {
				
		int numRealizations = 1;
		
//		double perturbation = 0.1d;
		double perturbation = 0d;
		
		int sampleSize = 2 * nsize.getValue() + 1;
		
		Point3i center = new Point3i(dims[0] / 2, dims[1] / 2, dims[2] / 2);
		System.out.println("Frame field volume has dimensions = " + Arrays.toString(dims));
		System.out.println("Computing from center = " + center.toString());

		for (int i = 0; i < numRealizations; i++) {

			// Perturb frame field
			VoxelFrameField perturbed = field;
			if (i > 0)
				perturbed = field.perturb(perturbation);

//			System.out.println("Frame field = " + new OrientationFrame(perturbed.get(center)).toString());
//			System.out.println("Frame field = " + frame.toString());
			System.out.println("Original connections = " +  "[ " + cpc.toStringShort() + "]\n");

			// Estimate parameters
			List<Triplet<FittingMethod,CartanParameter, String>> result = compare(perturbed, center, ecbShape.getSelected(), sampleSize);
			
			System.out.println("\n=== Realization " + i);
			for (Triplet<FittingMethod, CartanParameter, String> estimate : result) {
				System.out.println(estimate.getLeft() + " ("+ estimate.getRight() + "): " + new CartanParameter(estimate.getCenter()).toStringShort());
				System.out.println("SSE = " + cpc.SSE(estimate.getCenter()));
				System.out.println();
			}
		}
	}
	
	/**
	 * @param field
	 * @param computationCenter
	 * @return
	 */
	public static List<Triplet<FittingMethod, CartanParameter, String>> compare(VoxelFrameField field, Point3i computationCenter, NeighborhoodShape nshape, int nsize) {
		List<Triplet<FittingMethod,CartanParameter, String>> result = new LinkedList<Triplet<FittingMethod,CartanParameter, String>>();

		estimated = DifferentialOneForm.computeClosedFormParameter(field, computationCenter, nshape, nsize);
		result.add(new Triplet<CartanFittingParameter.FittingMethod, CartanParameter, String>(FittingMethod.ClosedForm, estimated, nshape.toString() + "-" + nsize));
//		result.add(new Triplet<CartanFittingParameter.FittingMethod, CartanParameter, String>(FittingMethod.ClosedForm, DifferentialOneForm.computeClosedFormParameter(field, computationCenter, NeighborhoodShape.Isotropic, nsize), "isotropic-" + nsize));
//		result.add(new Triplet<CartanFittingParameter.FittingMethod, CartanParameter, String>(FittingMethod.ClosedForm, DifferentialOneForm.computeClosedFormParameter(field, computationCenter, NeighborhoodShape.Isotropic, nsize), "isotropic-" + nsize));
//		result.add(new Triplet<CartanFittingParameter.FittingMethod, CartanParameter, String>(FittingMethod.ClosedForm, DifferentialOneForm.computeClosedFormParameter(field, computationCenter, NeighborhoodShape.SixNeighbors, 3), "6-neighbors"));
//
//		CartanParameter pDirect = DifferentialOneForm.computeOneFormParameter(field, computationCenter);
//		result.add(new Triplet<CartanFittingParameter.FittingMethod, CartanParameter, String>(FittingMethod.Direct, pDirect, ""));
//		
//		CartanFitter.setVerboseLevel(0);
//		CartanParameter pOpti = DifferentialOneForm.computeOptimizedOneFormParameters(field, computationCenter, nshape, nsize);
//		result.add(new Triplet<CartanFittingParameter.FittingMethod, CartanParameter, String>(FittingMethod.Optimized, pOpti, nshape.toString() + "-" + nsize));
		return result;
	}
}
