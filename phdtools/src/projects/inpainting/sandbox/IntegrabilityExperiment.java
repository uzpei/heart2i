package projects.inpainting.sandbox;

import diffgeom.DifferentialOneForm;
import math.matrix.Matrix3d;
import tools.frame.OrientationFrame;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;

public class IntegrabilityExperiment {
	
	public static void main(String[] args) {
		new IntegrabilityExperiment();
	}

	public IntegrabilityExperiment() {

		/*
		 *  Set experiment parameters
		 */
		// volume dimension
		int n = 5;
		
		// whether frame axes should be normalized after expansion
		boolean normalize = true;
		
		// the connection forms to apply
		CartanParameter cp = new CartanParameter();
		cp.randomize(0.5);
		
		// the initial coordinate frame
		OrientationFrame frame = OrientationFrame.identity();
		
		/*
		 *  Extrapolate the frame field
		 */
		VoxelFrameField f = FrameFieldGenerator.generate(new int[] { n, n, n}, frame, cp, normalize);
		
	
		/*
		 * Recompute the connection forms for all voxels
		 */
//		CartanParameterNode[][][] cijks = DifferentialOneForm.computeOneFormParameters(f);
		Matrix3d[] cijks = DifferentialOneForm.computeOneForm(f, CartanModel.FULL);
		Matrix3d[] c12 = { cijks[0], cijks[1], cijks[2] }; 
		Matrix3d[] c13 = { cijks[3], cijks[4], cijks[5] }; 
		Matrix3d[] c23 = { cijks[6], cijks[7], cijks[8] }; 
		
		/*
		 * Given a voxel and a direction v,
		 * compute dc_ijk/dx_l for k=1...3, l=1...3 
		 * Connections are enumerated as c121, c122, c123, c131, c132, c133, c231, c232, c233
		 */
		Matrix3d[][] dc12 = new Matrix3d[3][3];
		Matrix3d[][] dc13 = new Matrix3d[3][3];
		Matrix3d[][] dc23 = new Matrix3d[3][3];
		int[][] dx = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		for (int k = 0; k < 3; k++) {
			for (int l = 0; l < 3; l++) {
				dc12[k][l] = c12[k].d(dx[l]);
				dc13[k][l] = c13[k].d(dx[l]);
				dc23[k][l] = c23[k].d(dx[l]);
			}
		}
		
		
		/*
		 * Compute connection differentials (dC)
		 */
		
	}
}
