package projects.inpainting.sandbox;

import diffgeom.DifferentialOneForm.MovingAxis;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import visualization.helical.HelicalSlicerControl;
import volume.IntensityVolume;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelVectorField;
import gl.geometry.VertexArrayObject;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.HeartTools;

public class OrthogonalityB0 extends NodeScene {

	public static void main(String[] args) {
		new OrthogonalityB0();
	}
	
	private JHistogram histogram;
	
	public OrthogonalityB0() {
		super(true);
		
		Heart heart = HeartTools.heart();
		
		// Compute f1 dot grad(b0)
		final VoxelVectorField f = heart.getFrameField().getField(MovingAxis.F1);
		
		// Compute the heart wall normals and normalize them
		final VoxelVectorField n = heart.computeWallNormals();
		n.normalize();
		
		final IntensityVolume fn = new IntensityVolume(f.getDimensions());
		new VoxelBox(f.getDimensions()).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				double dot = Math.abs(f.getVector3d(x,y,z).dot(n.getVector3d(x,y,z)));
				fn.set(x, y, z, dot);
			}
		});

    	HelicalSlicerControl slicer = new HelicalSlicerControl(fn, heart.getMask());
		addControlNode(slicer);
		addNode(slicer);
		
		histogram = new JHistogram();
		histogram.setHistogram(new Histogram(fn.flattenCompact(heart.getMask())));
		addControlNode(histogram);
	}
}
