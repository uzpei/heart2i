package projects.inpainting.sandbox;

import gl.material.Colour;
import gl.material.GLMaterial;
import gl.renderer.JoglScene;
import gl.renderer.JoglTextRenderer;
import heart.Tractography;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.CoordinateFrameSample;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.MathToolset;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterControl;

import com.jogamp.opengl.util.gl2.GLUT;

import diffgeom.DifferentialOneForm;

public class FrameFieldGenerator extends JoglScene {

	public static void main(String[] args) {
		new FrameFieldGenerator();
	}
	
//	private IntParameter neighborhood = new IntParameter("neighborhood", 9, 1, 19);
	private IntDiscreteParameter neighborhood = new IntDiscreteParameter("neighborhood", 9, new int[] { 1,3,5,7,9,11,13,15,17,19,21,23 });
	
	private EnumComboBox<SamplingStyle> sampling = new EnumComboBox<SamplingStyle>(SamplingStyle.NP_NP_NP);
	
	private DoubleParameter magnitude = new DoubleParameter("connection magnitude", 0.2, 0, 1);
	private CartanParameterControl parameter;
	
	private VoxelFrameField frameField;
	
	private BooleanParameter orthogonalize = new BooleanParameter("orthogonalize", false);
	
	private VoxelBox tractoBox;
	
	private Tractography tracto = new Tractography();
	
	private GLUT glut = new GLUT();
	
	protected FrameFieldGenerator() {
		super("Field generator");
		
		ParameterListener updateListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				createFrameField();
			}
		};
		neighborhood.addParameterListener(updateListener);
		sampling.addParameterListener(updateListener);
		magnitude.addParameterListener(updateListener);

		parameter = new CartanParameterControl();

		// Set helicoidal wrapping
		parameter.setc123(3);
		parameter.setc132(-3);
		parameter.setc122(0.5);
		
		createTractography();
		createFrameField();
		
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(orthogonalize.getControls());
		parameter.addParameterListener(updateListener);
		orthogonalize.addParameterListener(updateListener);

		vfp.add(parameter.getControls());
		super.renderer.controlFrame.add("Connections", vfp.getPanel());
		super.render();
	}

	private void createTractography() {
		tracto.setNumSteps(50);
		tracto.setEnabled(true);
		tracto.setHighDef(true);
	}
	
	private void createFrameField() {
		// Get volume dimensions
		int n = neighborhood.getValue();
		int[] dims = sampling.getSelected().getSamplingDimension((n-1) / 2);
		dims[0] += 1;
		dims[1] += 1;
		dims[2] += 1;
		
		// Create frame field and create storage
		boolean[] drawCache = new boolean[] { true, true, true };

		if (frameField != null) {
			drawCache[0] = frameField.getF1().isVisible();
			drawCache[1] = frameField.getF2().isVisible();
			drawCache[2] = frameField.getF3().isVisible();
		}
		
		frameField = new VoxelFrameField(dims);
		frameField.getF1().setVisible(drawCache[0]);
		frameField.getF2().setVisible(drawCache[1]);
		frameField.getF3().setVisible(drawCache[2]);

		// Create the tractography voxel box
		VoxelBox tractoBox = new VoxelBox(dims);
		tractoBox.setDrawCells(true);
		frameField.setVoxelBox(tractoBox);
		
		if (this.tractoBox == null) {
			// Set voxel box span and origin
//			Point3i span = tractoBox.getSpan();
//			tractoBox.setOrigin(new Point3i(span.x / 2, 0, 0));
//			tractoBox.setSpan(new Point3i(0, span.y, span.z));
			tractoBox.setOrigin(new Point3i(4,2,2));
			tractoBox.setSpan(new Point3i(1,5,5));
		}
		else {
			tractoBox.setOrigin(this.tractoBox.getOrigin());
			tractoBox.setSpan(this.tractoBox.getSpan());
			
			// FIXME: update the tractobox UI
			tractoBox.getControls().invalidate();
			tractoBox.getControls().validate();
		}
		this.tractoBox = tractoBox;
		
		tractoBox.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				tracto.trace();
			}
		});
		
		updateFrameFieldUI();
		
		// Get sampling bounds and set extrapolation center
		Point3i center = computeCenter();
		CoordinateFrame cframe = new CoordinateFrame(new OrientationFrame(), MathToolset.tuple3iToPoint3d(center));
		extrapolate(frameField, cframe, parameter, sampling.getSelected(), n, orthogonalize.getValue());

		double stepSize = 0.2;
		
		tracto.hold(true);
//		tracto.excludeOrigin(computeCenter());
		tracto.excludeOrigin(null);
		tracto.setStepSize(stepSize);
		tracto.setOrthogonalization(orthogonalize.getValue());
		tracto.update(frameField, tractoBox);
		tracto.setConnectionParameters(center, parameter);
		tracto.hold(false);
		tracto.trace();
	}
	

	/**
	 * Generate a frame field to first order from the center of the given dimensions using the given frame as an expansion point.
	 * The resulting frame field will in general not be orthogonal.
	 * @param dims
	 * @param parameter
	 * @param normalize whether frame axes should be normalized after expansion
	 * @return
	 */
	public static VoxelFrameField generate(int[] dims, OrientationFrame frame, CartanParameter parameter, boolean normalize) {
		
		VoxelFrameField frameField = new VoxelFrameField(dims);

		Vector3d v = new Vector3d();
		
		// Extrapolate neighborhood
		Point3i center = new Point3i(dims[0] / 2, dims[1] / 2, dims[2] / 2);
		for (int x = 0; x < dims[0]; x++) {
			for (int y = 0; y < dims[1]; y++) {
				for (int z = 0; z < dims[2]; z++) {
					v.x = x - center.x;
					v.y = y - center.y;
					v.z = z - center.z;
					
					Vector3d[] extrapolated = DifferentialOneForm.extrapolate(frame, parameter, v, normalize);
					frameField.set(x, y, z, extrapolated);
				}
			}
		}
		
		IntensityVolumeMask mask = new IntensityVolumeMask(frameField.getDimension());
		mask.fillWith(1);
		frameField.setMask(mask);
		
		return frameField;
	}
	
	public static void extrapolate(VoxelFrameField frameField, CoordinateFrame frame, CartanParameter parameter, SamplingStyle sampling, int neighborhoodSize, boolean orthogonalize) {
		
		Point3i center = MathToolset.tuple3dTo3i(frame.getTranslation());
		CoordinateFrameSample origin = new CoordinateFrameSample();
		origin.presampleInteger(neighborhoodSize, sampling, true);
		
		// Extrapolate neighborhood
		List<Point3i> neighbors = origin.getIntegerSampling();
		Point3i p = new Point3i();
		Vector3d v = new Vector3d();
		for (Point3i neighbor : neighbors) {
			p.add(center, neighbor);
			v.set(neighbor.x, neighbor.y, neighbor.z);
			
			Vector3d[] extrapolated = DifferentialOneForm.extrapolate(frame, parameter, v);
			
			// Orthogonalize the frame
			if (orthogonalize) {
				OrientationFrame oFrame = new OrientationFrame(extrapolated[0], extrapolated[1], extrapolated[2]);
				MathToolset.orthogonalize(oFrame.getFrame());
				oFrame.updateFrameVectors(); 
				
//				System.out.println();
//				System.out.println(extrapolated.getAxis(FrameAxis.X));
				extrapolated[0] = oFrame.getAxis(FrameAxis.X);
				extrapolated[1] = oFrame.getAxis(FrameAxis.Y);
				extrapolated[2] = oFrame.getAxis(FrameAxis.Z);
				
//				System.out.println(extrapolated.getAxis(FrameAxis.X));
			}
			
			frameField.set(p, extrapolated);
		}
	}
	
	private Point3i computeCenter() {
		int[][] bounds = sampling.getSelected().getSamplingBounds((neighborhood.getValue()-1)/2);
		return new Point3i(
				MathToolset.roundInt((bounds[0][1] - bounds[0][0]) / 2d),
				MathToolset.roundInt((bounds[1][1] - bounds[1][0]) / 2d),
				MathToolset.roundInt((bounds[2][1] - bounds[2][0]) / 2d));

	}
	
	private JoglTextRenderer textRenderer = null;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		if (textRenderer == null) {
			Font font = new Font("Consolas", Font.PLAIN, 32);
			textRenderer = new JoglTextRenderer(font, true, false);
//			Color3f color = Colour.grey1;
			Color3f color = Colour.white;
			textRenderer.setColor(new Color(color.x, color.y, color.z));
		}
//		Font font = new Font("Consolas", Font.PLAIN, 32);
//		textRenderer = new JoglTextRenderer(font, true, false);
//		Color3f color = Colour.grey1;
//		textRenderer.setColor(new Color(color.x, color.y, color.z));
		
		gl.glPushMatrix();
		Point3i center = computeCenter();
		int scale = neighborhood.getValue();
		gl.glScaled(1d / scale, 1d/ scale, 1d/ scale);
//		gl.glTranslated(-scale, - scale, -scale);
		gl.glTranslated(-center.x - 0.5, - center.y - 0.5, -center.z - 0.5);
		tractoBox.display(drawable);
		frameField.display(drawable);

		gl.glTranslated(0.5, 0.5, 0.5);
		tracto.display(drawable);
		gl.glPopMatrix();
		
		// Display origin as solid cube
		gl.glEnable(GL2.GL_LIGHTING);
		GLMaterial.TRANSPARENT_LIGHT.apply(gl);
		glut.glutSolidCube(1f / scale);
		
		// Display coordinate system
//		FancyAxis fa = new FancyAxis(0.3);
//		fa.draw(gl);
		
//		GLMaterial.BRASS.apply(gl);
//		Cylinder cyl = new Cylinder(6, new Point3f(), new Vector3f(1, 1, 0), 0.05f, 0.5f);
//		cyl.display(gl);
//		List<Point3f[]> pts = cyl.getGeometry();
//		gl.glBegin(GL2.GL_TRIANGLE_STRIP);
//		for (Point3f[] p : pts) {
//			gl.glNormal3f(p[2].x, p[2].y, p[2].z);
//			gl.glVertex3f(p[0].x, p[0].y, p[0].z);
//			gl.glVertex3f(p[1].x, p[1].y, p[1].z);
//		}
//		gl.glEnd();
		
		// Display connection parameters
		List<String> pstring = new LinkedList<String>();
		for (int i = 0; i < 6; i++) {
			pstring.add(CartanParameter.Parameter.FromIndex(i) + ": "+ new BigDecimal(parameter.get(i)).setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue());
		}
//		textRenderer.drawTopLeft(parameter.toStringShort(), drawable);
//		textRenderer.drawBottomLeft(pstring, drawable);
		textRenderer.drawTopLeft(pstring, drawable);
	}

	private VerticalFlowPanel frameFieldComponent;
	
	private void updateFrameFieldUI() {
		if (frameFieldComponent == null)
			frameFieldComponent = new VerticalFlowPanel();

		frameFieldComponent.removeAll();
		frameFieldComponent.add(tractoBox.getControls());
		frameFieldComponent.add(frameField.getControls());
		
		super.renderer.controlFrame.getContentPanel().validate();
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(neighborhood.getSliderControls());
		vfp.add(sampling.getControls());
		vfp.add(magnitude.getSliderControls());
		vfp.add(tracto.getControls());

		if (frameFieldComponent == null)
			frameFieldComponent = new VerticalFlowPanel();
		
		vfp.add(frameFieldComponent);
		
		return vfp.getPanel();
	}

	@Override
	public void attach(Component component) {
		this.component = component;
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					// Generate a random connection form
					parameter.randomize(magnitude.getValue());
					parameter.updateSliders();
					createFrameField();
				}
				else if (e.getKeyCode() == KeyEvent.VK_O) {
					orthogonalize.setValue(!orthogonalize.getValue());
				}
			}
		});
	}

}
