package projects.experiments;

import gl.geometry.GLGeometry;
import gl.renderer.JoglRenderer;
import gl.renderer.NodeScene;

import java.nio.FloatBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.glsl.ShaderState;

public class VoxelRenderingExperiment implements GLGeometry {

	final static String vertex = "void main(void) {\n gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex; }\n";
	
	final static String fragment = "void main(void) {\n gl_FragColor = vec4(0.0,1.0,1.0,1.0); }\n";
	
	public VoxelRenderingExperiment() {
		NodeScene scene = new NodeScene(false);
		scene.addNode(this);
		scene.start();
	}
	
	@Override
	public void display(GLAutoDrawable drawable)
	{
		GL2 gl = drawable.getGL().getGL2();
		
	    /* Loop our display increasing the number of shown vertexes each time.
	     * Start with 2 vertexes (a line) and increase to 3 (a triangle) and 4 (a diamond) */
//	    for (int i = 2; i <= 4; i++)
//	    {
//	        /* Make our background black */
	        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//	        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
//	 
//	        /* Invoke glDrawArrays telling that our data is a line loop and we want to draw 2-4 vertexes */
//	        gl.glDrawArrays(GL2.GL_LINE_LOOP, 0, i);
//	    }
	 
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
	    
	    // Create a simple diamond objet
	    float[][] diamond2d = {
	    {  0.0f,  1.0f  }, /* Top point */
	    {  1.0f,  0.0f  }, /* Right point */
	    {  0.0f, -1.0f  }, /* Bottom point */
	    { -1.0f,  0.0f  } }; /* Left point */
	    float[][] colors2d = {
	    {  1.0f,  0.0f,  0.0f  }, /* Red */
	    {  0.0f,  1.0f,  0.0f  }, /* Green */
	    {  0.0f,  0.0f,  1.0f  }, /* Blue */
	    {  1.0f,  1.0f,  1.0f  } }; /* White */
	    
	    float[] diamond = new float[diamond2d.length * diamond2d[0].length];
	    float[] colors = new float[colors2d.length * colors2d[0].length];
	    FloatBuffer bufferDiamond = FloatBuffer.wrap(diamond);
	    FloatBuffer bufferColors = FloatBuffer.wrap(colors);
	    
	    // Allocate and bind a Vertex Array Object to our handle
	    int[] vao = new int[1];
	    gl.glGenVertexArrays(1, vao, 0);
//	    gl.glBindVertexArray(vao[0]);
	 
	    // Allocate two Vertex Buffer Objects to our handle
	    int[] vbo = new int[2];
//	    gl.glGenBuffers(2, vbo, 0);
	 
	    // Bind the first VBO 
	    // This one will store vertex attributes (coordinates)
//	    gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbo[0]);
	 
	    // Assign the coordinate data
//        gl.glBufferData( GL2.GL_ARRAY_BUFFER, diamond.length * Buffers.SIZEOF_FLOAT, bufferDiamond, GL2.GL_STATIC_DRAW );
//        
//	    // Set coordinate data into attribute index 0
//        // and indicate that it contains two floats per vertex
//        int indexCoordinate = 0;
//	    gl.glVertexAttribPointer(indexCoordinate, 2, Buffers.SIZEOF_FLOAT, false, 0, 0);
//	 
//	    // Enable coordinate attribute index 
//	    gl.glEnableVertexAttribArray(indexCoordinate);
//	 
//	    // Bind our second VBO for colors)
//	    gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbo[1]);
//	 
//	    // Assign the color data
//	    gl.glBufferData(GL2.GL_ARRAY_BUFFER, colors.length * Buffers.SIZEOF_FLOAT, bufferColors, GL2.GL_STATIC_DRAW);
//	    
//	    // Set color data into attribute index 1
//        // and indicate that it contains three floats per vertex
//	    gl.glVertexAttribPointer(1, 3, Buffers.SIZEOF_FLOAT, false, 0, 0);
//	 
//	    // Enable color attribute index
//	    gl.glEnableVertexAttribArray(1);
	 
	    // Read the shaders into the appropriate buffers
	    ShaderState program = JoglRenderer.createProgram(gl, vertex, fragment);

	    /* Bind attribute index 0 (coordinates) to in_Position and attribute index 1 (color) to in_Color */
	    /* Attribute locations must be setup before calling glLinkProgram. */
//	    program.bindAttribLocation(gl, 0, "in_Position");
//	    program.bindAttribLocation(gl, 1, "in_Color");
	    program.useProgram(gl, true);
	}
	
	private void cleanup() {
	    /* Cleanup all the things we bound and allocated */
//	    gl.glUseProgram(0);
//	    program.destroy(gl);
//	    gl.glDisableVertexAttribArray(0);
//	    gl.glDisableVertexAttribArray(1);
//	    gl.glDeleteBuffers(2, vbo, 0);
//	    gl.glDeleteVertexArrays(1, vao, 0);
	}
	@Override
	public String getName() {
		return "Voxel rendering experiment";
	}

	public static void main(String [] args) {
		new VoxelRenderingExperiment();
	}
}
