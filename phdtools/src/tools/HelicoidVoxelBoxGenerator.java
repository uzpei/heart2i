package tools;

import helicoid.Helicoid;
import helicoid.modeling.HelicoidGenerator;
import helicoid.parameter.HelicoidParameter;

import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.ParameterListener;
import swing.parameters.ParameterManager;
import volume.PerVoxelMethod;
import voxel.VoxelBox;

/**
 * Class used for extrapolating helicoids inside a voxel box, from a single
 * helicoid for which the parameter is known.
 * 
 * @author epiuze
 * 
 */
public class HelicoidVoxelBoxGenerator extends ParameterManager implements
		PerVoxelMethod {

	private DoubleParameter oscale = new DoubleParameter("offset scale", 50, 0,
			10);

	private VoxelBox box = new VoxelBox();

	private HelicoidGenerator generator;

	private HelicoidParameter helicoidParameter = new HelicoidParameter();

	private List<Helicoid> helicoids = new LinkedList<Helicoid>();

	public HelicoidVoxelBoxGenerator() {
		this(new HelicoidGenerator());
	}
	
	public HelicoidVoxelBoxGenerator(HelicoidGenerator generator) {
		this.generator = generator;

		super.addParameter(oscale);
	}

	public void setOffsetScale(double v) {
		oscale.setValue(v);
	}

	/**
	 * @param p
	 * @return a helicoid voxel box around p.
	 */
	public List<Helicoid> generateNeighborhood(VoxelBox box,
			HelicoidParameter helicoidParameter) {
		if (super.isHolding())
			return null;

		helicoids.clear();
		this.helicoidParameter = helicoidParameter;
		this.box = box;

		box.voxelProcess(this);

		return helicoids;
	}

	@Override
	public void process(int x, int y, int z) {

		Point3d center = box.getCenter();
		Point3d p = new Point3d(x, y, z);
		Point3d offset = new Point3d();
		offset.sub(p, center);
		offset.scale(oscale.getValue());

		generator.getParameterControl().setOffset(offset);

		Helicoid h = generator.generate(helicoidParameter, p);
		helicoids.add(h);
	}

	public JPanel getControls() {
		VerticalFlowPanel wp = new VerticalFlowPanel();
		wp.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Helicoid Neighborhood"));

		wp.add(oscale.getSliderControls());
		// wp.add(box.getControls());

		return wp.getPanel();
	}

	@Override
	public void addParameterListener(ParameterListener l) {
		super.addParameterListener(l);
	}

	@Override
	public boolean isValid(Point3d origin, Vector3d span) {
		return true;
	}
}
