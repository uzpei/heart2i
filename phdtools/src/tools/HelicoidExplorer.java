package tools;

import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameter.ParameterType;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;


import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.ParameterManager;

public class HelicoidExplorer {
	
	private IntParameter sleep = new IntParameter("sleep (ms)", 10, 0, 1000);	
	private DoubleParameter stepsize1 = new DoubleParameter("step size 1", 0.01, 0, 0.1);	
	private DoubleParameter stepsize2 = new DoubleParameter("step size 2", 0.01, 0, 0.1);
	
	private HelicoidParameter current = new HelicoidParameter();
	private List<HelicoidParameter> min = new LinkedList<HelicoidParameter>();
	private List<HelicoidParameter> max = new LinkedList<HelicoidParameter>();
	
	private List<ParameterType> types = new LinkedList<ParameterType>(); 
	private boolean running = false;
	
	public void explore(ParameterType ptype, HelicoidParameter hpmin, HelicoidParameter hpmax) {
		explore(ptype, null, hpmin, hpmax, null, null);
	}
	
	public void explore(ParameterType ptype1, ParameterType ptype2, HelicoidParameter hpmin1, HelicoidParameter hpmax1, HelicoidParameter hpmin2, HelicoidParameter hpmax2) {
		types.clear();
		min.clear();
		max.clear();
		
		types.add(ptype1);
		if (ptype2 != null && hpmin2 != null && hpmax2 != null) types.add(ptype2);
		
		min.add(hpmin1);
		if (ptype2 != null && hpmin2 != null && hpmax2 != null) min.add(hpmin2);
		
		max.add(hpmax1);
		if (ptype2 != null && hpmin2 != null && hpmax2 != null) max.add(hpmax2);
		
		reset();
	}

	public void explore(HelicoidParameter.ParameterType parameterType) {
		double ktmag = 0.55;
		
		double kbmin = -0.5;
		double kbmax = 0.5;
		
		double knmin = -0.05;
		double knmax = 0.7;
		
		double amin = -1;
		double amax = 0;
		
		if (parameterType.equals(ParameterType.HKT)) {
			setDefault(new HelicoidParameter(0, 0, 0, 0));
			explore(ParameterType.HKT, new HelicoidParameter(-ktmag, 0, 0, 0), new HelicoidParameter(ktmag, 0, 0, 0));
		}
		else if (parameterType.equals(ParameterType.HKN)) {
			setDefault(new HelicoidParameter(-ktmag, 0, 0, 0));
//			explore(ParameterType.HKN, new HelicoidParameter(0, -knmag, 0, 0), new HelicoidParameter(knmag, 0, 0, 0));
			explore(ParameterType.HKN, new HelicoidParameter(0, knmin, 0, 0), new HelicoidParameter(0, knmax, 0, 0));
		}
		else if (parameterType.equals(ParameterType.HKB)) {
			setDefault(new HelicoidParameter(0, 0, 0, 0));
//			explore(ParameterType.HKB, ParameterType.HALPHA, new HelicoidParameter(0, 0, kbmin, 0), new HelicoidParameter(0, 0, kbmax, 0), new HelicoidParameter(0, 0, 0, amin), new HelicoidParameter(0, 0, 0, amax));
			explore(ParameterType.HKB, new HelicoidParameter(0, 0, kbmin, 0), new HelicoidParameter(0, 0, kbmax, 0));
		}
	}
	
	private void reset() {
		// Set the minimum or max value depending on direction
		for (int i = 0; i < types.size(); i++) {
			current.set(types.get(i), types.get(i).extractValue(forward ? min.get(i) : max.get(i)));
		}
		
		running = true;
	}
	
	public void launch() {
		new Thread() {
			@Override
			public void run() {
				super.run();
				
				running = true;
				while (step()) {
					
					try {
						Thread.sleep(sleep.getValue());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				running = false;
			}
		}.run();
	}
	
	public boolean forward = true;
	
	public boolean step() {
		if (!running) return false;
		
		boolean one = true;
		int d = forward ? 1 : -1;
		
		boolean firststop = false;
		
		// Set the minimum value
		for (int i = 0; i < types.size(); i++) {
			ParameterType type = types.get(i);
			HelicoidParameter hpmin = min.get(i);
			HelicoidParameter hpmax = max.get(i);
			
			current.add(type, one ? d*stepsize1.getValue() : d*stepsize2.getValue());
			one = !one;
			
//			System.out.println(current.toString());
			
			if (type.extractValue(current) < type.extractValue(hpmin)) {
				if (types.size() == 1) {
					stopRunning();
					return false;
				}
				else {
					if (firststop) {
						stopRunning();
						return false;
					}
					firststop = true;
				}
			}
			if (type.extractValue(current) > type.extractValue(hpmax)) {
				if (types.size() == 1) {
					stopRunning();
					return false;
				}
				else {
					if (firststop) {
						stopRunning();
						return false;
					}
					firststop = true;
				}
			}
		}
		
		return true;
	}
	
	private void stopRunning() {
		if (!forward) {
			running = false;
		}
		
		forward = !forward;
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
				
		JPanel bpanel = new JPanel(new GridLayout(1, 3));
		// Build some sample runs
		for (final ParameterType type : HelicoidParameter.ParameterType.values()) {
			if (type.equals(ParameterType.ALL))
				continue;
			
			JButton btnParam = new JButton("run " + type.toString());
			btnParam.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					explore(type);
				}
			});
			bpanel.add(btnParam);
		}

		vfp.add(bpanel);
		
		vfp.add(sleep.getSliderControls());
		vfp.add(stepsize1.getSliderControls());
		vfp.add(stepsize2.getSliderControls());
		return vfp.getPanel();
	}

	protected void setDefault(HelicoidParameter helicoidParameter) {
		current.set(helicoidParameter);
	}

	public HelicoidParameter getCurrentParameter() {
		return current;
	}
	
	public boolean isRunning() {
		return running;
	}
	
}
