package tools.interpolation;

import java.util.Arrays;

import javax.vecmath.Point3d;

/**
 * Wrapper around hyperdata represented as a point in 3d space
 * and an arbitrary number of components.
 * @author piuze
 */
public class ParameterVector {

	public Point3d p = new Point3d();
	public double[] data;

	public ParameterVector() {
		// Data and location will need to be set later 
	}
	
	/**
	 * Construct a parameter vector data at the location p.
	 * @param p The location of the vector.
	 * @param data The components of the vector.
	 */
	public ParameterVector(Point3d p, double[] data) {
		this(p.x, p.y, p.z, data);
	}

	public ParameterVector(double x, double y, double z, double[] data) {
		this.p.set(x, y, z);
		this.data = data;
	}

	public ParameterVector(double x, double y, double z, double data) {
		this(x, y, z, new double[] { data });
	}

	/**
	 * @param p2
	 * @param count
	 */
	public ParameterVector(Point3d p, double data) {
		this(p, new double[] { data });
	}

	public ParameterVector(ParameterVector other) {
		this.p = other.p;
		this.data = other.data;
	}

	@Override
	public String toString() {
		return "[P= " + p.toString() + "] [data = " + Arrays.toString(data) + "]";
	}

	public void abs() {
		for (int i = 0; i < data.length; i++) {
			data[i] = Math.abs(data[i]);
		}
	}
}
