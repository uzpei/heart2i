/**
 * 
 */
package tools.interpolation;

import java.util.List;

import javax.vecmath.Point3d;
import javax.vecmath.Quat4d;


/**
 * @author piuze
 * 
 */
public class FrameInterpolator extends GaussianInterpolator {
	@Override
	public ParameterVector interpolate(Point3d p,
			List<ParameterVector> controlPoints, boolean normalize) {

		// There is a way to know for sure we are dealing with a quaternion
		// but for efficiency let's just assume a parameter of length 4 is a quaternion.
		if (controlPoints.get(0).data.length == 4) {

			// Find the first non-zero quaternion
			Quat4d q0 = new Quat4d();
			Quat4d q1 = new Quat4d();
			for (ParameterVector v : controlPoints) {
				q1.set(controlPoints.get(0).data);
				
				// Loop until its non-zero.
				if (q0.epsilonEquals(q1, 1e-6))
					continue;
				q0.set(q1);
				break;
			}
			
			// Then make sure all quaternions are oriented consistently with respect to the first non-zero one.
			for (ParameterVector v : controlPoints) {
				q1.set(v.data);

				double t = q0.x * q1.x + q0.y * q1.y + q0.z * q1.z + q0.w
						* q1.w;

				// Negate to make sure the orientation is preserved
				if (t < 0)
					for (int i = 0; i < 4; i++)
						v.data[i] *= -1;

			}
		}

		// proceed to the interpolation as usual
		return super.interpolate(p, controlPoints, true);
	}
}
