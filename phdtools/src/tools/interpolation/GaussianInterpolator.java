package tools.interpolation;

import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;


import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

/**
 * @author piuze
 */
public class GaussianInterpolator implements Interpolator {
    
    public enum METRIC { LOG, EXP, SPATIAL };

    private DoubleParameter power = new DoubleParameter("power", 2, 0.1,
            5);

    private DoubleParameter sigma = new DoubleParameter("sigma", 0.3, 1e-12,
            5);

    private DoubleParameter epsilon = new DoubleParameter("weight epsilon", 1e-12, 1e-18, 1e-8);

    private METRIC metric = METRIC.SPATIAL;

    private EnumComboBox<METRIC> ecb = new EnumComboBox<METRIC>(metric);

    public double getSigma() {
        return sigma.getValue();
    }
        
    public GaussianInterpolator() {
        this(METRIC.SPATIAL);
    }
    
    public GaussianInterpolator(METRIC m) {
        ecb.addParameterListener(new ParameterListener() {
            
            @Override
            public void parameterChanged(Parameter parameter) {
                metric = ecb.getSelected();
                
                switch (metric) {
                case SPATIAL:
                    sigma.setEnabled(false);
                    break;
                case LOG:
                    sigma.setEnabled(false);
                    break;
                case EXP:
                    sigma.setEnabled(true);
                    break;
                }
            }
        });
        setNorm(m);

    }
    public void setNorm(METRIC m) {
        metric = m;
        ecb.setSelected(metric);
    }

    /**
     * Use an exponential norm.
     */
    private double exp(double d) {
        return Math.exp(-d / (sigma.getValue() * sigma.getValue()));
    }
    /**
     * Use a spatial norm.
     */
    private double spat(double d) {
        return 1/d;
    }

    /**
     * Use a log norm.
     */
    private double log(double d) {
        return 1/Math.log(1+d);
    }

    @Override
    public String toString() {
        return "Metric = " + metric.toString();
    }
    
    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("RBF Interpolation");

        vfp.add(epsilon.getSliderControls());
        vfp.add(power.getSliderControls());
        vfp.add(sigma.getSliderControls());
        vfp.add(ecb.getControls());
                
        return vfp.getPanel();

    }

    @Override
    public ParameterVector interpolate(Point3d p,
            List<ParameterVector> controlPoints, boolean normalize) {
        
        if (controlPoints.size() == 0) return null;
        
        
        int m = controlPoints.get(0).data.length;
        int n = controlPoints.size();
        
        double[] weights = new double[n];
        double norm = 0;

        double[] value = new double[m];
        ParameterVector iv = new ParameterVector(p, value);

        double pow = power.getValue();
        double eps = epsilon.getValue();
        int i = 0;
        switch (metric) {
        case SPATIAL:
            for (ParameterVector v : controlPoints) {
                double d = p.distance(v.p);
                if (d < eps) {
                    for (int j = 0; j < n; j++) {
                        weights[j] = 0;
                    }
                    weights[i] = 1;
                    norm = 1;
                    break;
                }
                else {
                    weights[i] = Math.pow(spat(d), pow);
                    norm += weights[i];
                    i++;
                }
            }
            break;
        case LOG:
            for (ParameterVector v : controlPoints) {
                double d = p.distance(v.p);
                if (d < eps) {
                    for (int j = 0; j < n; j++) {
                        weights[j] = 0;
                    }
                    weights[i] = 1;
                    norm = 1;
                    break;
                }
                else {
                    weights[i] = Math.pow(log(d), pow);
                    norm += weights[i];
                    i++;
                }
            }
            break;
        case EXP:
            for (ParameterVector v : controlPoints) {
                double d = p.distance(v.p);
                if (d < eps) {
                    for (int j = 0; j < n; j++) {
                        weights[j] = 0;
                    }
                    weights[i] = 1;
                    norm = 1;
                    break;
                }
                else {
                    weights[i] = Math.pow(exp(d), pow);
                    norm += weights[i];
                    i++;
                }
            }
            break;
        }

        if (norm > epsilon.getValue()) {
            for (int j = 0; j < n; j++) {
                double weight = weights[j] / norm;
                ParameterVector v = controlPoints.get(j);

                for (int k= 0; k < m; k++) {
                    iv.data[k] += weight * v.data[k];
                }
            }
        }

//        if (Double.isNaN(iv.data[0])) {
//        	System.nanoTime();
//        }
        
        return iv;
    }

    @Override
    public ParameterVector interpolate(Point3d p,
            List<ParameterVector> controlPoints) {
        return interpolate(p, controlPoints, true);
    }

    private List<ParameterListener> listeners = new LinkedList<ParameterListener>();

    @Override
    public void addListener(ParameterListener l) {
        listeners.add(l);
        sigma.addParameterListener(l);
        power.addParameterListener(l);
        epsilon.addParameterListener(l);
        ecb.addParameterListener(l);
    }

	public void setSigma(double v) {
		sigma.setValue(v);
		
	}

	public METRIC getNorm() {
		return metric;
	}

	public void setPower(double v) {
		power.setValue(v);
	}
}
