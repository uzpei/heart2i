package tools.interpolation;


import java.util.List;

import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.parameters.ParameterListener;


public interface Interpolator {

	/**
	 * Interpolate the local frames at this location and normalize the weights by default.
	 * @param p
	 * @param controlPoints
	 * @return an interpolated local frame.
	 */
	public ParameterVector interpolate(Point3d p, List<ParameterVector> controlPoints);

	   /**
     * Interpolate the local frames at this location and normalize the weights
     * if necessary.
     * @param p
     * @param controlPoints
     * @param normalize if weights should be normalized.
     * @return an interpolated local frame.
     */
    public ParameterVector interpolate(Point3d p, List<ParameterVector> controlPoints, boolean normalize);

    /**
     * Add a parameter listener.
     * @param l
     */
	public void addListener(ParameterListener l);
	
	/**
	 * @return UI controls for this interpolator.
	 */
    public JPanel getControls();
    
    /**
     * 
     * @return the name of this interpolator.
     */
    public String toString();
}
