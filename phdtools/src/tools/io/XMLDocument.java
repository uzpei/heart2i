package tools.io;

import java.io.File;
import java.io.IOException;
import java.util.Stack;

import tools.TextToolset;
import tools.loader.LineWriter;

public class XMLDocument {
	
	private String filename;

	private String buffer;

	public XMLDocument(String filename, boolean createHeader) {
		this.filename = filename;

		createBuffer(createHeader);
	}

	public XMLDocument(boolean createHeader) {
		this(null, createHeader);
	}
	
	public XMLDocument(String filename) {
		this(filename, true);
	}

	private void createBuffer(boolean createHeader) {
		buffer = createHeader ? "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" : "";
		
		level = 0;
		
		listStack = new Stack<String>();
		openNodeStack = new Stack<Boolean>();
	}
	
	private int level;
	
	private Stack<String> listStack;

	private Stack<Boolean> openNodeStack;

	private void pushLevel() {
		level++;
	}
	
	private void popLevel() {
		level--;
	}

	public void addAttribute(String name, String value) {
		addNode(name, value, true);
	}

	public void addNode(String name, String value) {
		addNode(name, value, false);
	}
	
	public void addXMLFormattedText(String text) {
		buffer += text;
	}
	
	/**
	 * TODO: Might be able to detect automatically if this is an attribute or not by peeking at the list and open stacks. 
	 * @param name
	 * @param value
	 * @param asAttribute
	 */
	public void addNode(String name, String value, boolean asAttribute) {
		pushLevel();
		if (asAttribute)
			buffer += beginLine() + name + "=\"" + value + "\"";
		else
			buffer += beginLine() + "<" + name + " value=\"" + value + "\"/>";
		popLevel();
	}
	
	public void pushNodeList(String listName) {
		pushNodeList(listName, false);
	}
	
	public void pushNodeList(String listName, boolean open) {
		pushLevel();
		buffer += newLine + beginLine() + (open ? "<" + listName :  "<" + listName + ">");
		listStack.push(listName);
		openNodeStack.push(open);
	}
	
	public void popNodeList() {
		boolean open = openNodeStack.pop();
		String name = listStack.pop();
		
		buffer += beginLine() + (open ? "/>" : "</" + name + ">") + newLine;
		popLevel();
	}

	private String beginLine() {
		return newLine + TextToolset.fill("\t", level);
	}
	
	private String newLine = "\n";
	
	public void save() {
		if (filename == null)
			return;
		
		try {
			LineWriter.write(filename, buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return buffer;
	}
}
