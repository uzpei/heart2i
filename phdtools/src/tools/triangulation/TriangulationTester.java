package tools.triangulation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Line2D;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.vecmath.Point2d;


/* 
 * This is like the FontDemo applet in volume 1, except that it 
 * uses the Java 2D APIs to define and render the graphics and text.
 */

public class TriangulationTester extends JApplet {
    final static int maxCharHeight = 15;

    final static int minFontSize = 6;

    final static Color bg = Color.white;

    final static Color fg = Color.black;

    final static Color red = Color.red;

    final static Color white = Color.white;

    final static BasicStroke stroke = new BasicStroke(2.0f);

    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = { 10.0f };

    final static BasicStroke dashed = new BasicStroke(1.0f,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

    Dimension totalSize;

    FontMetrics fontMetrics;

    private List<Point2d> points = new LinkedList<Point2d>();
    private List<Point2d> convexHull = new LinkedList<Point2d>();
    private List<Point2d> boundingBox = new LinkedList<Point2d>();

    private Font font1;
    private Font font2;
    
    public void init() {
        //Initialize drawing colors
        setBackground(bg);
        setForeground(fg);
        
        // Initialize font
        String [] fonts = getToolkit().getFontList();
        font1 = new Font(fonts[22], Font.BOLD, 30);
        font2 = new Font(fonts[22], Font.BOLD, 15);
    }

    private void addPoint(Point2d p) {
        points.add(p);
        
        refresh();
    }

    private void refresh() {
//        System.out.println("Refreshing scene...");

        PointCloud2D pc = new PointCloud2D(points);
        
        convexHull = pc.getConvexHull();
        boundingBox = pc.getBoundingBox();
        
        init();
        repaint();
    }

    private void reset() {
        points.clear();
        
        refresh();
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        Dimension d = getSize();

        Color fg3D = Color.lightGray;

        // First clear the screen
        g2.clearRect(0, 0, d.width, d.height);

        g.setFont(font1);

//        String [] fonts = getToolkit().getFontList();
//        Font font;
//        int font_size = 20;
//        int x = 20;
//        int y = 25;
//        int line_spacing = 25;
//        for (int i = 0; i < 50; i++) {
//           font = new Font(fonts[i], Font.BOLD, font_size);
//           g.setFont(font);
//           g.drawString(i + ") " + fonts[i] + " 123456789", x, y);
//           y += line_spacing;
//        }
        
        g2.setPaint(fg3D);
        g2.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g2.draw3DRect(3, 3, d.width - 7, d.height - 7, false);
        g2.setPaint(fg);

        BasicStroke spoint = new BasicStroke(5);
        BasicStroke stline = new BasicStroke(1);
        BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
        
        g2.setPaint(Color.red);
        g2.setFont(font1);
        g2.drawString("" + points.size(), 5, font1.getSize());
        
        // draw the list of points
        g2.setStroke(spoint);
        if (points.size() > 1) {
            Point2d p1 = new Point2d();

            for (int i = 0; i < points.size(); i++) {
                p1.set(points.get(i));

                g2.setPaint(Color.black);

                // Draw a line connecting the next point
                // g2.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));

                // Draw the point
                g2.draw(new Line2D.Double(p1.x, p1.y, p1.x + 1, p1.y + 1));

                // Draw the point label
                g2.setPaint(Color.red);
                g2.setFont(font2);
                g2.drawString("" + i, (int) p1.x, (int) p1.y - 5);
            }
        } 
        else if (points.size() == 1) {
            g2.setPaint(Color.black);
            g2.draw(new Line2D.Double(points.get(0).x, points.get(0).y, points.get(0).x, points.get(0).y + 1));

            g2.setPaint(Color.red);
            g2.setFont(font2);
            g2.drawString("" + points.size(), (int) points.get(0).x,
                    (int) points.get(0).y - 5);
        }

        // draw the convex hull of the list of points
        g2.setStroke(stline);
        if (convexHull.size() > 1) {
            Point2d p1 = new Point2d(convexHull.get(0));
            Point2d p2 = new Point2d(convexHull.get(1));
            
            for (int i = 1; i < convexHull.size(); i++) {
                p2.set(convexHull.get(i));
                
                g2.setPaint(Color.blue);
                g2.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
                
                p1.set(p2);
            }
            g2.draw(new Line2D.Double(p2.x, p2.y, convexHull.get(0).x, convexHull.get(0).y));

        }

        // draw the bounding box of the list of points
        g2.setStroke(stline);
        if (boundingBox.size() > 1) {
            Point2d p1 = new Point2d(boundingBox.get(0));
            Point2d p2 = new Point2d(boundingBox.get(1));
            
            for (int i = 1; i < boundingBox.size(); i++) {
                p2.set(boundingBox.get(i));
                
                g2.setPaint(Color.green);
                g2.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
                
                p1.set(p2);
            }
            g2.draw(new Line2D.Double(p2.x, p2.y, boundingBox.get(0).x, boundingBox.get(0).y));

            // draw the subdivision of the bounding box
//          g2.setStroke(dashed);
          Point2d p3, p4;
          p1 = boundingBox.get(0);
          p2 = boundingBox.get(1);
          p3 = boundingBox.get(2);
          p4 = boundingBox.get(3);
          g2.setPaint(Color.black);
          double dx = 10;
          double dy = 10;
          for (double x = p1.x; x <= p4.x; x += dx) {
              for (double y = p1.y; y <= p2.y; y += dy) {
                  g2.draw(new Line2D.Double(x, y, x + 1, y + 1));
              }            
          }

        }

    }

    FontMetrics pickFont(Graphics2D g2, String longString, int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();

        while (!fontFits) {
            if ((fontMetrics.getHeight() <= maxCharHeight)
                    && (fontMetrics.stringWidth(longString) <= xSpace)) {
                fontFits = true;
            } else {
                if (size <= minFontSize) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name, style, --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

    public static void main(String s[]) {
        final JFrame f = new JFrame("Convex Hull Test");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        
        final JApplet applet = new TriangulationTester();

        applet.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.print(e.getX() + ",");
                System.out.println(e.getY());
                
                Point2d p = new Point2d(e.getX(), e.getY());
                
                ((TriangulationTester) applet).addPoint(p);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub
                
            }
            
        });

        applet.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_R) {
                    ((TriangulationTester) applet).reset();
                }
                else {
                    ((TriangulationTester) applet).refresh();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void keyTyped(KeyEvent e) {
                // TODO Auto-generated method stub
                
            }
            
        });
        
        f.getContentPane().add("Center", applet);
        applet.init();
        f.setSize(new Dimension(700, 700));
//        f.pack();
        f.setVisible(true);
        
        // Start the update timer
        timer = new Timer() ;
        timer.schedule(new ToDoTask(((TriangulationTester) applet)), 0, 500);
    }
    
    static Timer timer;

    static class ToDoTask extends TimerTask {
        private TriangulationTester applet;
        
        public ToDoTask(TriangulationTester tester) {
            this.applet = tester;
        }
        
        public void run () {
            applet.refresh();
        }
    }
}
