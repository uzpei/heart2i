package tools.triangulation;



import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

import tools.geom.BoundingBox;
import tools.geom.ConvexHull;
import tools.geom.ConvexHull.ConvexHullAlgorithm;




/**
 * Class representing a 2D point cloud involving some computational features
 * such as finding a convex hull, interpolating, clockwise angles, etc.
 * @author piuze
 */
public class PointCloud2D {
    private List<Point2d> points = new LinkedList<Point2d>();
    
    /**
     * @param points
     */
    public PointCloud2D(Point2d[] pts) {
        for (Point2d p : pts) points.add(p);
    }
    
    public PointCloud2D(List<Point2d> points2) {
        points.addAll(points2);
    }

    /**
     * @return The set of points in this cloud.
     */
    public List<Point2d> getPoints() {
        return points;
    }
    
    public List<Point2d> getConvexHull() {
        return ConvexHull.getHull(this, ConvexHullAlgorithm.GRAHAM);
    }

    public List<Point2d> getBoundingBox() {
        return BoundingBox.getBox(this);
    }

    /**
     * @param p The pivot point for comparing angles.
     */
    public void sortByPolarAngle(Point2d p) {
        Point2dPolarAngleComparator c = new Point2dPolarAngleComparator();
        c.setPivot(p);
        
        Collections.sort(points, c);
    }

    public List<Point2d> getSortedByX() {
        Collections.sort(points, new Point2dXcoordComparator());
        
        return points;
    }

    public List<Point2d> getSortedByY() {
        Collections.sort(points, new Point2dYcoordComparator());
        
        return points;
    }

    /**
     * Sort points by X coordinate.
     * @author piuze
     */
    static class Point2dXcoordComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            Point2d p1 = (Point2d) o1;
            Point2d p2 = (Point2d) o2;
            
            return (int) Math.signum(p1.x - p2.x);
        }
    }

    /**
     * Sort points by y coordinate.
     * @author piuze
     */
    static class Point2dYcoordComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            Point2d p1 = (Point2d) o1;
            Point2d p2 = (Point2d) o2;
            
            return (int) Math.signum(p1.y - p2.y);
        }
    }

    public int size() {
        return points.size();
    }
    
    public static void sortByX(List<Point2d> points) {
        Collections.sort(points, new Point2dXcoordComparator());
    }

    public static void sortByY(List<Point2d> points) {
        Collections.sort(points, new Point2dYcoordComparator());
    }

    /**
     * Swap points by reference, slower.
     * @param index1
     * @param index2
     */
    public void swapPointsByRef(int index1, int index2) {
        Point2d p1 = points.get(index1);
        Point2d p2 = points.get(index2);
        
        // Put p1 in place of p2
        points.remove(index2);
        points.add(index2, p1);

        // Put p2 in place of p1
        points.remove(index1);
        points.add(index1, p2);
        
    }

    /**
     * Swap points by value, faster.
     * @param index1
     * @param index2
     */
    public void swapPointsByVal(int index1, int index2) {
        // Temp placeholder for p1
        Point2d temp = new Point2d(points.get(index1));
        Point2d p2 = points.get(index2);
        
        // Set p2 at p1
        points.get(index1).set(p2);

        // Set p1 at p2
        points.get(index2).set(temp);
    }
    
    /**
     * Sort points by polar angle with a pivot.
     * @author piuze
     */
    static class Point2dPolarAngleComparator implements Comparator {

        private Point2d pivot = new Point2d();
        
        public void setPivot(Point2d p) {
            pivot.set(p);
        }
        
        @Override
        public int compare(Object o1, Object o2) {
            Point2d p1 = (Point2d) o1;
            Point2d p2 = (Point2d) o2;

            Vector2d v1 = new Vector2d();
            v1.sub(p1, pivot);

            Vector2d v2 = new Vector2d();
            v2.sub(p2, pivot);

            double a1 = Math.atan2(v1.y, v1.x);
            double a2 =  Math.atan2(v2.y, v2.x);
            
            return (int) Math.signum(a1 - a2);
        }
    }


}

