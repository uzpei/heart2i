package tools.geom;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

import tools.triangulation.PointCloud2D;



public class BoundingBox {
	private List<Point3d> box = new LinkedList<Point3d>();
	
    private static List<Point2d> pts;
    
    /**
     * Determine a 2D bounding box (square) in the XZ plane. 
     * @param cloud
     * @return
     */
    public static List<Point2d> getBox(List<Point3d> cloud) {
        double xmin = Double.MAX_VALUE;
        double xmax = -Double.MAX_VALUE;

        double zmin = Double.MAX_VALUE;
        double zmax = -Double.MAX_VALUE;

        for (Point3d p : cloud) {
            
            if (p.x < xmin) xmin = p.x;
            if (p.x > xmax) xmax = p.x;
            
            if (p.z < zmin) zmin = p.z;
            if (p.z > zmax) zmax = p.z;
        }
        
        Point2d p1 = new Point2d(xmin, zmin);
        Point2d p2 = new Point2d(xmin, zmax);
        Point2d p3 = new Point2d(xmax, zmax);
        Point2d p4 = new Point2d(xmax, zmin);
        
        List<Point2d> pts = new LinkedList<Point2d>();
        pts.add(p1);
        pts.add(p2);
        pts.add(p3);
        pts.add(p4);
        
        return pts;
    }
    
	public static double getSamplingDistance(List<Point3d> cloud, int density) {
        List<Point2d> boundingBox = getBox(cloud);
        Point2d p1, p2, p3, p4;
        p1 = boundingBox.get(0);
        p2 = boundingBox.get(1);
        p3 = boundingBox.get(2);
        p4 = boundingBox.get(3);
        
        double dex = (p4.x - p1.x);
        double dey = (p2.y - p1.y);
        
        int nx = density;
        int ny = density;

        double dx = dex / nx;
        double dy = dey / ny;

        return 0.5 * (dx + dy);
	}

    public static List<Point2d> getGridSampling(int density, List<Point3d> cloud) {
    	return getGridSampling(density, cloud, true, 1);
    }

    public static List<Point2d> getGridSampling(int density, List<Point3d> cloud, boolean useEpsilon) {
    	return getGridSampling(density, cloud, useEpsilon, 1);
    }

    /**
     * If the grid is nearly linear (loss of one degree of freedom) the sampling
     * is done on a line.
     * @param density
     * @param cloud
     * @return the 2d bounding box thresholded at d
     */
    public static List<Point2d> getGridSampling(int density, List<Point3d> cloud, boolean useEpsilon, double eps) {
        List<Point2d> sampling = new ArrayList<Point2d>();
    	if (density < 1) return sampling;
        
        // Use noisy sampling?
        boolean noisy = false;

        // Get the initial bounding box
        List<Point2d> boundingBox = getBox(cloud);

        Random rand = new Random();
        
        // Perform the sampling
        Point2d p1, p2, p3, p4;
        p1 = boundingBox.get(0);
        p2 = boundingBox.get(1);
        p3 = boundingBox.get(2);
        p4 = boundingBox.get(3);
        
        double dex = (p4.x - p1.x);
        double dey = (p2.y - p1.y);
        
        int nx = density;
        int ny = density;
        
        double dx = dex / nx;
        double dy = dey / ny;
        double noisex = 0.5 * dx;
        double noisey = 0.5 * dy;

//        System.out.println(nx + ", " + dex + " --- " + ny + ", " + dey + ". EPS = " + eps);

        if (useEpsilon) {
            if (dex <= eps) {
            	nx = 0;
            	dx = 0;
            	p1.x = 0;
            }
            else if (dey <= eps) {
            	ny = 0;
            	dy = 0;
            	p1.y = 0;
            }
        }
        
        if (!noisy) {
            for (int i = 0; i <= nx; i++) {
                for (int j = 0; j <= ny; j++) {
                    Point2d p = new Point2d(p1.x + i * dx, p1.y + j * dy);
                    sampling.add(p);
                }
            }
        }
        else {
        	for (Point2d p : sampling) {
        		p.x += noisex*rand.nextDouble(); 
        		p.y += noisey*rand.nextDouble(); 
        	}
        }
        
        return sampling;
    }

    public static void display(GLAutoDrawable drawable, List<Point3d> points) {
		GL2 gl = drawable.getGL().getGL2();

        pts = getBox(points);

        gl.glDisable(GL2.GL_LIGHTING);
        gl.glColor4f(0.5f,0.5f,0.5f,0.5f);
        gl.glLineWidth(4);
        gl.glBegin(GL2.GL_LINE_STRIP);
        for (Point2d p : pts) {
            gl.glVertex3d(p.x, 0, p.y);
        }
        Point2d p = pts.get(0);
        gl.glVertex3d(p.x, 0, p.y);

        gl.glEnd();

    }

    public static List<Point2d> getBox(PointCloud2D pointCloud2D) {
        double xmin = Double.MAX_VALUE;
        double xmax = Double.MIN_VALUE;

        double ymin = Double.MAX_VALUE;
        double ymax = Double.MIN_VALUE;

        for (Point2d p : pointCloud2D.getPoints()) {
            
            if (p.x < xmin) xmin = p.x;
            if (p.x > xmax) xmax = p.x;
            
            if (p.y < ymin) ymin = p.y;
            if (p.y > ymax) ymax = p.y;
        }
        
        Point2d p1 = new Point2d(xmin, ymin);
        Point2d p2 = new Point2d(xmin, ymax);
        Point2d p3 = new Point2d(xmax, ymax);
        Point2d p4 = new Point2d(xmax, ymin);
        
        List<Point2d> pts = new LinkedList<Point2d>();
        pts.add(p1);
        pts.add(p2);
        pts.add(p3);
        pts.add(p4);
        
        return pts;
    }

	public static List<Point2d> get2DGridSampling(int value, List<Point2d> cloud,
			boolean useEpsilon) {
		return get2DGridSampling(value, cloud, useEpsilon, 1);
	}

	public static List<Point2d> get2DGridSampling(int value, List<Point2d> cloud,
			boolean useEpsilon, double eps) {
		List<Point3d> pts = new LinkedList<Point3d>();
		for (Point2d p : cloud) {
			pts.add(new Point3d(p.x, 0, p.y));
		}
		
		return getGridSampling(value, pts, useEpsilon, eps);
	}
    
}
