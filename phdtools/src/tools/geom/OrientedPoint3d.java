package tools.geom;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

public class OrientedPoint3d extends Point3d {

	private Matrix4d frame = new Matrix4d();
	{
		frame.setIdentity();
	}

	public OrientedPoint3d(Point3d p) {
		set(p);
		frame.setIdentity();
	}
	
	public OrientedPoint3d(Point3d p, Matrix4d frame) {
		set(p);
		this.frame.set(frame);
	}
	
	public OrientedPoint3d(Matrix4d frame) {
		this.frame.set(frame);
	}
	
	public Matrix4d getFrame() {
		return frame;
	}
	
	public void setFrame(Matrix4d frame) {
		this.frame.set(frame);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5399670437233935456L;

}
