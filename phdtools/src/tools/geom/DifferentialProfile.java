package tools.geom;

import gl.geometry.WorldAxis;

import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import tools.computations.MotionTools;

/**
 * 
 * @author piuze
 */
public class DifferentialProfile {

	private double epsilon;
	private List<Point3d> pts;
	private Vector3d T = new Vector3d();
	private Vector3d N = new Vector3d();
	private Vector3d B = new Vector3d();
	private Vector3d T1 = new Vector3d();
	private Vector3d T2 = new Vector3d();
	int n;

	public DifferentialProfile(List<Point3d> pts) {
		this.pts = pts;
		n = pts.size();
		epsilon = 1e-18;
	}

	/**
	 * @param pts
	 *            A list of 3d points.
	 * @param k
	 *            The index (starting from zero) of the point at which the
	 *            Frenet frame is to be computed.
	 * @return the Frenet frame at this index;
	 */
	public Matrix4d getFrenetFrame(int k) {

		k = k < 0 ? 0 : k;
		k = k > n - 2 ? n - 2 : k;

		Matrix4d frame = computeFrame(k);
		return frame;
	}

	/**
	 * @return the last Frenet frame;
	 */
	public Matrix4d getLastFrenetFrame() {
		return getFrenetFrame(n);
	}

	/**
	 * @return the first Frenet frame;
	 */
	public Matrix4d getFirstFrenetFrame() {
		return getFrenetFrame(0);
	}

	public Matrix4d getFirstFrenetFrameInv() {
		return invert(getFirstFrenetFrame());
	}

	public Matrix4d invert(Matrix4d m) {
		return MotionTools.invertRigidTransformation(m, m);
	}

	private boolean isCollinear(Point3d p1, Point3d p2, Point3d p3) {
		T1.sub(p2, p1);
		T2.sub(p3, p2);
		T1.cross(T1, T2);
		return T1.lengthSquared() <= epsilon;
	}

	/**
	 * Compute the Frenet frame along the hair strand and returns a tuple
	 * containing the curvature (x), torsion (y), and euclidiean delta (z) at
	 * this point index.
	 * 
	 * @param k
	 * @return a tuple containing the curvature (x), torsion (y) and delta (z).
	 */
	private Matrix4d computeFrame(int k) {
		Matrix4d frame = new Matrix4d();
		Point3d p1, p2, p3;
		Point3d o = new Point3d();

		if (k < 1) {
//		    System.out.println("Computing first frame");
			int fn = 0;
			p1 = pts.get(fn);
			p2 = pts.get(fn + 1);
			p3 = pts.get(fn + 2);
			o.set(p1);
			k = 0;
			
			if (isCollinear(p1, p2, p3)) {
				frame.setIdentity();
				frame.setColumn(3, p2.x, p2.y, p2.z, 1);
				return frame;
			}

			T.sub(p2, p1);
			T.normalize();

			B.sub(p3, p2);
			B.cross(T, B);
			B.normalize();

			N.cross(B, T);
			N.normalize();
			
//			N.negate();
//			B.negate();
			
			return buildFrame(T, N, B, p1);

		}
		// If this is the last frame, swap p2 and p1
		else if (k >= n - 1) {
			int fn = n - 3;
			p1 = pts.get(fn);
			p2 = pts.get(fn + 1);
			p3 = pts.get(fn + 2);
			o.set(p1);
			k = n - 1;
		} else {
			p1 = pts.get(k - 1);
			p2 = pts.get(k);
			p3 = pts.get(k + 1);
			o.set(p2);
		}

      System.out.println("Computing another frame");

		if (isCollinear(p1, p2, p3)) {
			frame.setIdentity();
			frame.setColumn(3, p2.x, p2.y, p2.z, 1);
			return frame;
		}

		return buildFrame(p1, p2, p3);
	}

	/**
	 * Build a frame from an orthogonal basis T, N, B corresponding to x=T, y=N,
	 * z=B with origin located at o
	 * 
	 * @param T
	 * @param N
	 * @param B
	 * @param o
	 * @return the frame
	 */
	private Matrix4d buildFrame(Vector3d T, Vector3d N, Vector3d B, Point3d o) {
		Matrix4d frame = new Matrix4d();

		frame.setColumn(0, T.x, T.y, T.z, 0);
		frame.setColumn(1, N.x, N.y, N.z, 0);
		frame.setColumn(2, B.x, B.y, B.z, 0);
		frame.setColumn(3, o.x, o.y, o.z, 1);

		return frame;
	}

	private Vector4d getT(Matrix4d frame) {
		Vector4d T = new Vector4d();
		frame.getColumn(0, T);
		return T;
	}

	private Vector4d getN(Matrix4d frame) {
		Vector4d N = new Vector4d();
		frame.getColumn(0, N);
		return N;
	}

	private Vector4d getB(Matrix4d frame) {
		Vector4d B = new Vector4d();
		frame.getColumn(0, B);
		return B;
	}

	private Matrix4d buildFrame(Point3d p0, Point3d p1, Point3d p2) {

		T.sub(p2, p1);
		T.normalize();

		B.sub(p1, p0);
		B.cross(B, T);
		B.normalize();

		N.cross(B, T);
		N.normalize();

		return buildFrame(T, N, B, p1);
	}

	public void displayFrenet(GLAutoDrawable drawable) {
		int skip = 10;

		Matrix4d Mk = new Matrix4d();

		GL2 gl = drawable.getGL().getGL2();

		// First find the length of the point list
		// slow... but no choice for now
		double length = 0;
		for (int k = 0; k < n - 1; k += skip) {
			length += pts.get(k).distance(pts.get(+1));
		}

		double scale = 0.005 * length * skip;
		for (int k = 0; k < n - 1; k += skip) {

			Mk.set(getFrenetFrame(k));
			WorldAxis.display(gl, Mk, (float) scale);
		}

	}
}
