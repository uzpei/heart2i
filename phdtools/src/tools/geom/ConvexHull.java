package tools.geom;



import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

import tools.triangulation.PointCloud2D;


public class ConvexHull {
    
    public enum ConvexHullAlgorithm { GRAHAM, INCREMENTAL };
    
    public static List<Point2d> getHull(PointCloud2D cloud, ConvexHullAlgorithm a) {
        
        List<Point2d> points = new LinkedList<Point2d>();
        
        if (cloud.size() < 2) return points;
        
        switch (a) {
        case GRAHAM:
            points = grahamScan(cloud);
            
            break;
        case INCREMENTAL:
            // TODO
            break;
        default:
            // TODO
        }
        
        return points;
    }
    
    private static List<Point2d> grahamScan(PointCloud2D cloud) {
        
        Stack<Point2d> output = new Stack<Point2d>();
        
        // The number of points in the point cloud
        int n = cloud.size();
        
        if (n < 4) return cloud.getPoints();
        
        // Find the point with lowest y-coord
        double lowPv = Double.MAX_VALUE;
        int lowYindex = 0;
        Point2d p;
        Point2d p0 = null;
        for (int i = 0; i < cloud.getPoints().size(); i++) {
            p = cloud.getPoints().get(i);
            if (p.y < lowPv) {
                lowYindex = i;
                lowPv = p.y;
                p0 = p;
            }
        }
        cloud.swapPointsByVal(0, lowYindex);

        // Sort the remaining points by their polar angle
        // with p0
        List<Point2d> pts = new LinkedList<Point2d>();
        for (int i = 1; i < n; i++) {
            pts.add(cloud.getPoints().get(i));
        }
        
        Point2dPolarAngleComparator c = new Point2dPolarAngleComparator();
        c.setPivot(p0);
        Collections.sort(pts, c);
        
        // Add p0 at the beginning of the list
        pts.add(0, p0);
        
        // Push the first three points
        output.push(pts.get(0));
        output.push(pts.get(1));
        output.push(pts.get(2));
        
        // m denotes the number of points in the convex hull
        for (int i = 3; i < n; i++) {
            // Find the next valid point on the convex hull
            while (ccw(output.get(output.size() - 2), output.peek(), pts.get(i)) <= 0) {
                output.pop();
            }

            output.push(pts.get(i));
        }
        
        return output.subList(0, output.size());
    }
    
    /**
     * @param p1
     * @param p2
     * @param p3
     * @return The determinant that gives the signed area of the 
     * triangle formed by p1, p2, and p3;
     */
    private static double ccw(Point2d p1, Point2d p2, Point2d p3) {
        return (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
    }

    /**
     * Sort points by polar angle with a pivot.
     * @author piuze
     */
    static class Point2dPolarAngleComparator implements Comparator {

        private Point2d pivot = new Point2d();
        
        public void setPivot(Point2d p) {
            pivot.set(p);
        }
        
        @Override
        public int compare(Object o1, Object o2) {
            Point2d p1 = (Point2d) o1;
            Point2d p2 = (Point2d) o2;

            Vector2d v1 = new Vector2d();
            v1.sub(p1, pivot);

            Vector2d v2 = new Vector2d();
            v2.sub(p2, pivot);

            double a1 = Math.atan2(v1.y, v1.x);
            double a2 =  Math.atan2(v2.y, v2.x);
            
            return (int) Math.signum(a1 - a2);
        }
    }
    
}
