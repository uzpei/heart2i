package tools.geom;


import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;


import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;

/**
 * Class for computing the Fr�chet distance between two polygonal curves.
 * 
 * @author piuze
 */
public class FrechetCoupler {

    private double[][] ca;

    private List<Point3d> P;

    private List<Point3d> Q;

    private int p, q;

    private double weight = 0;

    private double scale = 1;

    /**
     * Weights the polyline vertices unequally (favors end matchings)
     */
    public static DoubleParameter weighting = new DoubleParameter(
            "Frechet weighting", 10, 0, 20);

    /**
     * Whether the Frechet distance measure is a cumulative measure over curves
     * or simply a global measure.
     */
    public static BooleanParameter cumulative = new BooleanParameter("cumulative measure",
            true);

    public FrechetCoupler() {
    }

    public double evaluate(Polyline h1, Polyline h2) {
        return evaluate(h1, h2, FrechetCoupler.cumulative.getValue());
    }
    
    public double evaluate(Polyline h1, Polyline h2, boolean cumulative) {
        double[][] result = compute(h1.getPoints(), h2.getPoints(), 1, cumulative);

        if (Double.isNaN(result[h1.getCount() - 1][h2.getCount() - 1])) {
            System.nanoTime();
        }
        
        return result[h1.getCount() - 1][h2.getCount() - 1];
    }

    public double[][] compute(Polyline h1, Polyline h2, boolean cumulative) {
        return compute(h1.getPoints(), h2.getPoints(), 1, cumulative);

    }

    /**
     * 
     * @param l1
     * @param m
     * @param l2
     * @param n
     * @param cumulative
     * @return the Frechet distance between the first m points of l1 and n
     *         points of l2
     */
    private double[][] compute(List<Point3d> l1, List<Point3d> l2,
            double lengthScale, boolean cumulative) {

        // Might eventually want to change that...
        scale = lengthScale;

        P = l1;
        Q = l2;

        p = l1.size();
        q = l2.size();

        ca = new double[p][q];

        for (int i = 0; i < p; i++) {
            for (int j = 0; j < q; j++) {
                ca[i][j] = Double.NEGATIVE_INFINITY;
            }
        }

        if (cumulative) {
            // Adjust based on weight
            if (weighting.isChecked()) {
                weight = weighting.getValue();
                cweighted(p - 1, q - 1);
            } else {
                csum(p - 1, q - 1);
            }
        } else {
            c(p - 1, q - 1);
        }

        return ca;
    }

    private double c(int i, int j) {
        if (ca[i][j] > -1)
            return ca[i][j];
        else if (i == 0 && j == 0)
            ca[i][j] = d(0, 0);
        else if (i > 0 && j == 0)
            ca[i][j] = max(c(i - 1, 0), d(i, 0));
        else if (i == 0 && j > 0)
            ca[i][j] = max(c(0, j - 1), d(0, j));
        else if (i > 0 && j > 0)
            ca[i][j] = max(min(c(i - 1, j), c(i - 1, j - 1), c(i, j - 1)), d(i,
                    j));
        else
            ca[i][j] = Double.POSITIVE_INFINITY;

        return ca[i][j];
    }

    private double csum(int i, int j) {
        if (ca[i][j] > -1)
            return ca[i][j];
        else if (i == 0 && j == 0)
            ca[i][j] = d(0, 0);
        else if (i > 0 && j == 0)
            ca[i][j] = max(csum(i - 1, 0), d(i, 0));
        else if (i == 0 && j > 0)
            ca[i][j] = max(csum(0, j - 1), d(0, j));
        else if (i > 0 && j > 0)
            ca[i][j] = min(csum(i - 1, j), csum(i - 1, j - 1), csum(i, j - 1))
                    + d(i, j);
        else
            ca[i][j] = Double.POSITIVE_INFINITY;

        return ca[i][j];
    }

    private double cweighted(int i, int j) {
        if (ca[i][j] > -1)
            return ca[i][j];
        else if (i == 0 && j == 0)
            ca[i][j] = dw(0, 0);
        else if (i > 0 && j == 0)
            ca[i][j] = max(cweighted(i - 1, 0), dw(i, 0));
        else if (i == 0 && j > 0)
            ca[i][j] = max(cweighted(0, j - 1), dw(0, j));
        else if (i > 0 && j > 0)
            ca[i][j] = min(cweighted(i - 1, j), cweighted(i - 1, j - 1),
                    cweighted(i, j - 1))
                    + dw(i, j);
        else
            ca[i][j] = Double.POSITIVE_INFINITY;

        return ca[i][j];
    }

    private double d(int i, int j) {
        return P.get(i).distance(Q.get(j)) / scale;
    }

    /**
     * Weighted distance between two vertices.
     * End vertices are given more weight to increase
     * tip matching.
     * @param i
     * @param j
     * @return
     */
    private double dw(int i, int j) {
        double w = 1;
        if (i >= p - 2 && j >= q - 2) {
            w = weight;
        }
//        int prev = 3;
//        if (i >= p - prev && j >= q - prev) {
//            w = (1.0-((double)(i+j)) / (p+q)) * weight;
//        }

        return w * d(i, j);
    }

    private double max(double a, double b) {
        return Math.max(a, b);
    }

    private double min(double a, double b) {
        return Math.min(a, b);
    }

    private double min(double a, double b, double c) {
        return Math.min(Math.min(a, b), Math.min(b, c));
    }
    
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Frechet Coupler"));
        vfp.add(weighting.getSliderControlsExtended());
        vfp.add(cumulative.getControls());
        
        return vfp.getPanel();
    }

}
