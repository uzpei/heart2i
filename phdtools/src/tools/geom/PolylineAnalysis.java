package tools.geom;



import gl.math.FlatMatrix4d;

import java.io.Serializable;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import tools.computations.MotionTools;

/**
 * @author piuze
 */
public class PolylineAnalysis implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9154893112313161239L;

    private IntParameter skip = new IntParameter("skip", 2, 1, 100);
    
    private DoubleParameter scale = new DoubleParameter("scale", 0.3, 0, 10);
    private BooleanParameter display = new BooleanParameter("display", true);
    private static Vector3d up = new Vector3d(0, 0, 1);

    private Vector3d e1 = new Vector3d();

    private Vector3d e2 = new Vector3d();

    private Vector3d e3 = new Vector3d();

    private Vector3d T = new Vector3d();

    private Vector3d N = new Vector3d();

    private Vector3d B = new Vector3d();

//    private Point3d o = new Point3d();

    private Matrix4d frame = new Matrix4d();

    private Matrix4d inverseFrame = new Matrix4d();

    private Matrix4d torsion = new Matrix4d();

    private Matrix4d curvature = new Matrix4d();

    private FlatMatrix4d fm = new FlatMatrix4d();
    
    private void clearMatrices() {
        frame = new Matrix4d();
        frame.setIdentity();

        inverseFrame = new Matrix4d();
        inverseFrame.setIdentity();

        torsion = new Matrix4d();

        curvature = new Matrix4d();

        fm = new FlatMatrix4d();

    }

    /**
     * Compute the Frenet frame along the hair strand and returns a tuple
     * containing the curvature (x), torsion (y), and euclidiean delta (z) at
     * this point index.
     * 
     * @param h
     * @param point
     * @return a tuple containing the curvature (x), torsion (y) and delta (z).
     */
    public Matrix4d computeFrame(Polyline hair, int k) {
        Point3d p0, p1, p2;
        int n = hair.getCount();
        Point3d o = new Point3d();

        // If the hair has a single point, return the identity matrix
        if (hair.getCount() < 2) {
            frame = new Matrix4d();
            frame.setIdentity();
            o.set(hair.getPoint(0));
            frame.setColumn(3, o.x, o.y, o.z, 1);
            return frame;
        }
        // If the hair has only 2 points, use these
        // 2 points for the tangent and use another arbitrary vector as binormal
        else if (hair.getCount() == 2) {
            p0 = hair.getPoint(0);
            p1 = hair.getPoint(1);
            o.set(p0);

            T.sub(p1, p0);
            T.normalize();

            int c = getMinCoordinate(T);
            B.set(c == 0 ? 1 : 0, c == 1 ? 1 : 0, c == 2 ? 1 : 0);
            N.cross(B, T);
            
        }
        // otherwise we can use three points, with the frame centered on the second one
        else {
            // If this is the first frame, swap p1 and p0, or p2 and p1
            if (k < 1) {
                p0 = hair.getPoint(2);
                p1 = hair.getPoint(0);
                p2 = hair.getPoint(1);
                o.set(p1);
                k = 0;
            }
            // If this is the last frame, swap p2 and p1
            else if (k >= n - 1) {
                p0 = hair.getPoint(n-3);
                p1 = hair.getPoint(n-2);
                p2 = hair.getPoint(n-1);
                o.set(p2);
                k = n - 1;
            }
            else {
                p0 = hair.getPoint(k - 1);
                p1 = hair.getPoint(k);
                p2 = hair.getPoint(k + 1);
                o.set(p1);
            }
            
            T.sub(p2, p1);
            T.normalize();

            B.sub(p1, p0);
            B.cross(B, T);
            
            // If we are not dealing with a straight line, proceed as usual
            if (B.length() > 1e-12) {
                double or = getOrientation(p0, p1, p2);
                B.normalize();
                B.scale(or);
                N.cross(B, T);
                N.normalize();
            }
            // In this case we are dealing with colinear points
            // i.e. how to get around a zero binormal vector
            else {
                
//                double r = 1;
//                double s = 1;
//                double t = 1;
//                if (T.z > 0) {
//                    t = (r * T.x + s * T.y) / T.z;
//                } else if (T.y > 0) {
//                    t = (r * T.x + t * T.z) / T.y;
//                } else if (T.x > 0) {
//                    t = (s * T.y + t * T.z) / T.x;
//                }
//
//                B.set(r, s, t);
//                B.normalize();
//                N.cross(B, T);
//                N.normalize();
//                B.cross(T, N);
//                B.normalize();
                if (k >= 1) {
                    Matrix4d frame = computeFrame(hair, k - 1);
                    frame.setColumn(3, o.x, o.y, o.z, 1);
                    return frame;
                }
                else {
                	// The only information we have is T so find 2 other arbitrary orthogonal vectors
//                    Matrix4d frame = new Matrix4d();
//                    frame.setIdentity();
//                    return frame;
                	N.set(T.x+1, T.y+1, T.z+1);
                	N.cross(T, N);
                	N.normalize();
                	B.cross(T, N);
                	B.normalize();
                }

            } 

        }
        
        return getFrame(T, B, N, o);
    }
    
    public Matrix4d getFrame(Vector3d T, Vector3d B, Vector3d N, Point3d o) {
        Matrix4d frame = new Matrix4d();
        
        frame.setColumn(0, T.x, T.y, T.z, 0);
        frame.setColumn(1, N.x, N.y, N.z, 0);
        frame.setColumn(2, B.x, B.y, B.z, 0);
        frame.setColumn(3, o.x, o.y, o.z, 1);
        
        return frame;
    }
    
    private void displayFrame(GLAutoDrawable drawable, Matrix4d frame) {
    	
		GL2 gl = drawable.getGL().getGL2();
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        
        Vector4d T = new Vector4d();
        Vector4d N = new Vector4d();
        Vector4d B = new Vector4d();
        Vector4d O = new Vector4d();
        frame.getColumn(0, T);
        frame.getColumn(1, N);
        frame.getColumn(2, B);
        frame.getColumn(3, O);
//        T.normalize();
        double s = scale.getValue();
        T.scale(s);
        N.scale(s);
        B.scale(s);
        T.add(O);
        N.add(O);
        B.add(O);
        
        gl.glPointSize(10);
        gl.glBegin(GL2.GL_POINTS);
        gl.glVertex3d(O.x, O.y, O.z);
        gl.glEnd();
        
        gl.glPushMatrix();
        
        gl.glLineWidth(2.0f);
        gl.glBegin(GL2.GL_LINES);
        
        // T
        gl.glColor3f(1, 0.5f, 0.5f);
        gl.glVertex3d(O.x, O.y, O.z);
        gl.glVertex3d(T.x, T.y, T.z);
        
        // N
        gl.glColor3f(0.5f, 1, 0.5f);
        gl.glVertex3d(O.x, O.y, O.z);
        gl.glVertex3d(N.x, N.y, N.z);

        // B
        gl.glColor3f(0.5f, 0.5f, 1);
        gl.glVertex3d(O.x, O.y, O.z);
        gl.glVertex3d(B.x, B.y, B.z);

        gl.glEnd();
        
        gl.glPopMatrix();

        // T
//        gl.glColor3f(1, 0, 0);
//        gl.glRasterPos3d(T.x, T.y, T.z); 
//        ViewerHelper.glut.glutBitmapString(GLUT.BITMAP_9_BY_15, "T");

        // N
//        gl.glColor3f(0, 1, 0);
//        JoglTextRenderer.print3dTextLines("N", new Point3d(N.x, N.y, N.z));

        // B
//        gl.glColor3f(0, 0, 1);
//        JoglTextRenderer.print3dTextLines("B", new Point3d(B.x, B.y, B.z));
        
        gl.glEnable(GL2.GL_LIGHTING);

    }

    /**
     * Makes sure we are aligned with the world frame on the first frame.
     */
    public Matrix4d computeFrameShift(Polyline hair, Matrix4d world) {
        frame = computeFirstFrame(hair);

        return frame;
    }

    /**
     * @param i
     *            index centered in a 3-tuple.
     * @return the signed curvature at this hair's vertex index.
     */
    public double getCurvature(Polyline hair, int i) {
        if (hair.getCount() < 3) return 0;

        clearMatrices();
        
        if (i < 1) {
            return getCurvature(hair, 1);
        } else if (i >= hair.getCount() - 1) {
            return getCurvature(hair, hair.getCount() - 2);
        }

        Point3d p1 = hair.getPoint(i - 1);
        Point3d p2 = hair.getPoint(i);
        Point3d p3 = hair.getPoint(i + 1);

        Vector3d t1 = new Vector3d();
        Vector3d t2 = new Vector3d();
        t1.sub(p2, p1);
        t2.sub(p3, p2);

        Vector3d Q = new Vector3d();
        Q.sub(p3, p1);

        double alpha = Math.acos(t1.dot(t2) / (t1.length() * t2.length()));

        double k = 2 * Math.sin(alpha) / Q.length();

        double s = getOrientation(p1, p2, p3);

        // if (s != 0)
        // k = k * -Math.signum(s);
        k *= s;
        // System.out.println(s);

        return k;
    }

    /**
     * @param point
     * @return the signed torsion at this hair's vertex index.
     */
    public double getTorsion(Polyline hair, int i) {
        if (hair.getCount() < 5) return 0;

        clearMatrices();

        if (i <= 1) {
            return getTorsion(hair, 2);
        } else if (i >= hair.getCount() - 2) {
            return getTorsion(hair, hair.getCount() - 3);
        }

        boolean symm = true;

        if (symm) {
            double Tip1 = getAsymTorsion(hair, i);
            double Ti = getAsymTorsion(hair, i + 1);

            Vector3d Li = new Vector3d();
            Vector3d Lip1 = new Vector3d();
            Li.sub(hair.getPoint(i), hair.getPoint(i - 1));
            Lip1.sub(hair.getPoint(i + 1), hair.getPoint(i));
            double Lis = Li.length();
            double Lip1s = Lip1.length();

            double Tsym = (Lis * Tip1 + Lip1s * Ti) / (Lis + Lip1s);

            return Tsym;
        } else {
            return getAsymTorsion(hair, i);
        }

    }

    private double getAsymTorsion(Polyline hair, int i) {
        Point3d pim2 = hair.getPoint(i - 2);
        Point3d pim1 = hair.getPoint(i - 1);
        Point3d pi = hair.getPoint(i);
        Point3d pip1 = hair.getPoint(i + 1);

        Vector3d Lim1 = new Vector3d();
        Vector3d Li = new Vector3d();
        Vector3d Lip1 = new Vector3d();

        Lim1.sub(pim1, pim2);
        Li.sub(pi, pim1);
        Lip1.sub(pip1, pi);

        Vector3d Vim1 = new Vector3d();
        Vector3d Vi = new Vector3d();

        Vim1.cross(Lim1, Li);
        Vi.cross(Li, Lip1);

        double Vim1s = Vim1.length();
        double Vis = Vi.length();

        if (Vim1s == 0 || Vis == 0) {
            return 0;
        }

        Vim1.scale(1 / Vim1s);
        Vi.scale(1 / Vis);

        double beta = Math.acos(Vim1.dot(Vi));

        double t = Math.sin(beta) / Li.length();

        // return s * t;
        return t;
    }

    /**
     * See http://mathworld.wolfram.com/TriangleArea.html
     * http://books.google.ca/
     * books?id=wCfWkc_E3GkC&pg=PA265&lpg=PA265&dq=3d+triangle
     * +signed+area&source
     * =bl&ots=zIXVUCbeFL&sig=AtiynDfcyKbAiMsdwVM7zB7SzNI&hl=en
     * &ei=_4NPS-KuGM6n8AbA7r2mCg
     * &sa=X&oi=book_result&ct=result&resnum=6&ved=0CCAQ6AEwBQ
     * #v=onepage&q=3d%20triangle%20signed%20area&f=false
     * 
     * @param l
     * @param k
     * @param r
     * @return
     */
    private double getOrientation(Point3d l, Point3d k, Point3d r) {

        // First turn the 3D triangle in barycentric coordinates
        // i.e. by projecting it into a 2D plane. We drop the coordinate
        // for which the normal has the largest value (in absolute value)

        Vector3d kr = new Vector3d();
        Vector3d kl = new Vector3d();
        Vector3d n = new Vector3d();
        kr.sub(r, k);
        kl.sub(l, k);
        n.cross(kr, kl);
        n.absolute();
        
        // If the three points are colinear, return 0
        if (n.length() < 1e-12) return 0;
        
        int c = getMaxCoordinate(n);
        double[] p1 = new double[] { l.x, l.y, l.z };
        double[] p2 = new double[] { k.x, k.y, k.z };
        double[] p3 = new double[] { r.x, r.y, r.z };

        // Reassign the coordinates based on the largest (absolute) entry in the
        // normal vector
        int x, y;
        if (c == 0) {
            x = 1;
            y = 2;
        } else if (c == 1) {
            x = 0;
            y = 2;
        } else {
            x = 0;
            y = 1;
        }

        return Math.signum(0.5 * ((p3[x] - p2[x]) * p1[y] + (p1[x] - p3[x])
                * p2[y] + (p2[x] - p1[x]) * p3[y]));

    }

    /**
     * @param n
     * @return the largest (in absolute value) coordinate of this vector. 
     */
    private int getMaxCoordinate(Tuple3d n) {
        double max = Math.max(Math.max(n.x, n.y), Math.max(n.x, n.z));
        return n.x == max ? 0 : n.y == max ? 1 : 2;
    }
    
    /**
     * @param n
     * @return the minimum (in absolute value) coordinate of this vector. 
     */
    private int getMinCoordinate(Tuple3d n) {
        double max = Math.min(Math.min(n.x, n.y), Math.min(n.x, n.z));
        return n.x == max ? 0 : n.y == max ? 1 : 2;
    }

    public Matrix4d computeFrameInverse(Polyline hair, int i) {
        Matrix4d m = computeFrame(hair, i);
        // try {
        // m.invert();
        // } catch (Exception e) {
        // m.setIdentity();
        // }
        MotionTools.invertRigidTransformation(m, m);

        return m;
    }

    /**
     * Invert this frame in place.
     * 
     * @param m
     * @return The matrix, inversed.
     */
    public static Matrix4d invertFrame(Matrix4d m) {
        // try {
        // m.invert();
        // } catch (Exception e) {
        // m.setIdentity();
        // }
        MotionTools.invertRigidTransformation(m, m);

        return m;
    }

    /**
     * @param hair
     * @return the last frame of this hair.
     */
    public Matrix4d computeLastFrame(Polyline hair) {
        return computeFrame(hair, hair.getCount() - 1);
    }

    /**
     * @param hair
     * @return the last frame of this hair.
     */
    public Matrix4d computeLastFrameInverse(Polyline hair) {
        return computeFrameInverse(hair, hair.getCount() - 1);
    }

    /**
     * @param hair
     * @return the last frame of this hair.
     */
    public Matrix4d computeFirstFrame(Polyline hair) {
        return computeFrame(hair, 0);
    }

    /**
     * @param hair
     * @return the last frame of this hair.
     */
    public Matrix4d computeFirstFrameInverse(Polyline hair) {
        return computeFrameInverse(hair, 0);
    }

    public void displayFrenet(Polyline h, GLAutoDrawable drawable) {
    	if (!display.getValue()) return;

        GL gl = drawable.getGL();

        Matrix4d Mk = new Matrix4d();

        Matrix4d M = new Matrix4d();
        Matrix4d Minv = new Matrix4d();

        Matrix4d flip = new Matrix4d();
        flip.setIdentity();
        flip.setRotation(new AxisAngle4d(1, 0, 0, Math.PI));

        double minc = getCurvatureMinima(h, 5);

        double[] curvs = new double[h.getCount() - 1];
        double[] tors = new double[h.getCount() - 1];

        for (int k = 0; k < h.getCount() - 1; k += skip.getValue()) {
//        for (int k = 0; k < 1; k += skip) {

            double curv = getCurvature(h, k);
            curvs[k] = curv;

            double tor = getTorsion(h, k);
            tors[k] = tor;

            Mk.set(computeFrame(h, k));

//            displayFrame(drawable, Mk);
//            new CoordinateFrame(Mk).display(drawable, 0.1, false);
        }

         float[] curvColor = new float[] { 0, 0, 1, 1 };
         float[] torColor = new float[] { 1, 0, 0, 1 };
         PlottingTools.display(drawable, curvs, 0, 0.3, 0.2, curvColor);
         PlottingTools.display(drawable, tors, 0, 1, 0.2, torColor);

    }

    /**
     * 
     * @param h
     * @param n
     *            the n-th minima will be used as a threshold
     * @return
     */
    private double getCurvatureMinima(Polyline h, int n) {
        double[] min = new double[n];
        for (int i = 0; i < n; i++) {
            min[i] = Double.POSITIVE_INFINITY;
        }

        int lasti = -Integer.MIN_VALUE;

        int mindist = h.getCount() / 5;

        for (int i = 1; i < h.getCount() - 1; i++) {

            // if (i - lasti < mindist)
            // continue;

            double k = getCurvature(h, i);

            for (int j = 0; j < n; j++) {
                if (k < min[j]) {
                    for (int l = n - 1; l > j; l--) {
                        min[l] = min[l - 1];
                    }
                    min[j] = k;
                    break;
                }
            }
        }

        return min[n - 1];
    }

    public static void displayCurvature(Polyline h, GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        // Display plot
        PolylineAnalysis ha = new PolylineAnalysis();

        double[] curvs = new double[h.getCount() - 2];
        double[] tors = new double[h.getCount() - 2];

        double minc = Double.POSITIVE_INFINITY;
        double mint = Double.POSITIVE_INFINITY;
        double maxc = Double.NEGATIVE_INFINITY;
        double maxt = Double.NEGATIVE_INFINITY;
        for (int k = 1; k < h.getCount() - 1; k += 1) {
            double curv = ha.getCurvature(h, k);
            double tor = ha.getTorsion(h, k);

            curvs[k - 1] = curv;
            tors[k - 1] = tor;

            if (curv > maxc)
                maxc = curv;

            if (tor > maxt)
                maxt = tor;

            if (curv < minc)
                minc = curv;

            if (tor < mint)
                mint = tor;
        }

        // if (maxc > Double.MAX_VALUE)
        // maxc = 1;
        //
        // if (maxt > Double.MAX_VALUE)
        // maxt = 1;
        //
        // if (mint < -Double.MAX_VALUE)
        // mint = -1;

        float[] cColor = new float[] { 0, 0, 1, 1 };
        PlottingTools.display(drawable, curvs, minc, maxc, 0.2, cColor);

        float[] tColor = new float[] { 1, 0, 1, 1 };
        // PlottingTools.display(drawable, tors, mint, maxt, 0.2, tColor);

    }

    /**
     * Finds the index of the point of highest curvature in this strand.
     * 
     * @param h
     * @param targetMidSection
     *            if we enforce the maximum to be found around the midsection
     * @return the index of the point of highest curvature in this strand.
     */
    public int getCurvatureMaximum(Polyline h, boolean targetMidSection) {
        double maxk = Double.NEGATIVE_INFINITY;
        int index = -1;

        int buffer = 2;

        double mid = 0.3;

        for (int i = 1; i < h.getCount() - 1; i++) {
            double k = getCurvature(h, i);

            if (targetMidSection && i < mid * h.getCount())
                continue;

            if (targetMidSection && i > (1 - mid) * h.getCount())
                break;

            if (i <= buffer)
                continue;

            if (i >= h.getCount() - buffer - 1)
                break;

            if (k > maxk) {
                maxk = k;
                index = i;
            }
        }

        return index;
    }

    /**
     * @param target
     * @return the differential (curvature and torsion) profile of this strand
     *         for each vertex. the profile is accessed using profile[i][j] with
     *         i=vertex, and j={0=curvature, 1=torsion}.
     */
    public double[][] getDifferentialProfile(Polyline target) {
        return getDifferentialProfile(target, 0, target.getCount());
    }

    /**
     * @param target
     * @param start
     * @param end
     * @return the differential (curvature and torsion) profile of this strand
     *         for each vertex. the profile is accessed using profile[i][j] with
     *         i=vertex, and j={0=curvature, 1=torsion}.
     */
    public double[][] getDifferentialProfile(Polyline target, int start, int end) {
        double[][] profile = new double[end - start][2];

        for (int k = start; k < end; k++) {
            double curv = getCurvature(target, k);
            double tor = getTorsion(target, k);

            profile[k][0] = curv;
            profile[k][1] = tor;
        }

        return profile;
    }
    
    public JPanel getControls() {
    	VerticalFlowPanel vfp = new VerticalFlowPanel();
    	vfp.add(skip.getSliderControls());
    	vfp.add(scale.getSliderControls());
    	vfp.add(display.getControls());
    	
    	return vfp.getPanel();
    }

}
