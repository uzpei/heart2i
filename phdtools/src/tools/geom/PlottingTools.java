package tools.geom;

import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.text.NumberFormat;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import swing.text.GreekToolset.GreekLetter;

import com.jogamp.opengl.util.gl2.GLUT;

public class PlottingTools {
    public static void display(GLAutoDrawable drawable, double[] values,
            double minvalue, double maxvalue, double scale, float[] color) {
		GL2 gl = drawable.getGL().getGL2();

        double maxthreshold = maxvalue;

        if (minvalue < -maxthreshold)
            minvalue = -maxthreshold;
        if (maxvalue > maxthreshold)
            maxvalue = maxthreshold;

        // Angle plotter
        if (values != null) {
            int size = values.length;

            int h = drawable.getHeight();
            int w = drawable.getWidth();
            double xend = w - 5;
            double yend = 20;

            int sx = (int) (scale * w);
            int sy = (int) (scale * h - yend);

            double xstep = sx / ((double) size);
            double ystep = sy / (maxvalue - minvalue);

            // Map to right corner
            // This is the lower left corner of the plot.
            double x0 = (1 - scale) * w;
            double y0 = scale * h;

            JoglRenderer.beginOverlay(drawable);

            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(1);

            JoglTextRenderer.printTextLines(drawable, "k = " + nf.format(minvalue)
                    + GreekLetter.THETA.code, x0 - 90, y0, new float[] { 0, 0,
                    0 }, GLUT.BITMAP_9_BY_15);
            JoglTextRenderer.printTextLines(drawable, "k = " + nf.format(maxvalue)
                    + GreekLetter.THETA.code, x0 - 90, yend, new float[] { 0,
                    0, 0 }, GLUT.BITMAP_9_BY_15);

            gl.glLineWidth(1);
            gl.glEnable(GL2.GL_LINE_STIPPLE);
            gl.glLineStipple(1, (short) 0x00FF);
            gl.glBegin(GL2.GL_LINES);

            // min value
            gl.glVertex2d(x0, y0);
            gl.glVertex2d(xend, y0);

            // max value
            gl.glVertex2d(x0, yend);
            gl.glVertex2d(xend, yend);

            gl.glEnd();
            gl.glDisable(GL2.GL_LINE_STIPPLE);

            boolean drawline = false;
            gl.glLineWidth(2);
            gl.glEnable(GL2.GL_POINT_SMOOTH);
            gl.glPointSize(2);
            gl.glBegin(drawline ? GL2.GL_LINE_STRIP : GL2.GL_POINTS);

            double v;
            float r = 0.8f;
            for (int i = 0; i < size - 1; i++) {
                v = values[i];
                gl.glColor4f(color[0], color[1], color[2], color[3]);

                if (Math.abs(values[i]) > maxthreshold) {
                    v = Math.signum(values[i]) * maxthreshold;

                    gl.glColor4f(r + color[0], r + color[1], r + color[2],
                            color[3]);
                }
                gl.glVertex2d(x0 + i * xstep, y0 - v * sy / maxvalue);
            }
            gl.glEnd();

            JoglRenderer.endOverlay(drawable);

        }

    }

}
