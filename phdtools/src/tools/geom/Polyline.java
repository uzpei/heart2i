package tools.geom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;


/**
 * Interface for a geometrical hair strand.
 * 
 * @author piuze
 */
public class Polyline implements Iterable<Point3d>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4091960029926735301L;

	private List<Point3d> points;

	private double length = -1;

	/**
	 * The orientation frame of this polyline.
	 */
	private Matrix4d localFrame = new Matrix4d();
	{
		localFrame.setIdentity();
	}

	/**
	 * The world frame of this polyline.
	 */
	private Matrix4d rotationFrame = new Matrix4d();
	{
		rotationFrame.setIdentity();
	}

	/**
	 * Create a new strand with these points.
	 * 
	 * @param pts
	 */
	public Polyline(List<Point3d> pts) {
		set(pts, false);
	}

	public Polyline() {
		this(new LinkedList<Point3d>());
	}

	/**
	 * @param pts
	 * @param byref
	 *            set to true if this Polyline should share the same vertices.
	 */
	public Polyline(List<Point3d> pts, boolean byref) {
		set(pts, byref);
	}

	public void set(List<Point3d> pts) {
		set(pts, false);
	}
	public void set(List<Point3d> pts, boolean byref) {
		if (byref) {
			points = pts;
		} else {
			points = new LinkedList<Point3d>();

			for (Point3d p : pts) {
				points.add(new Point3d(p));
			}
		}
		computeLength();
	}

	/**
	 * @param other
	 * @param byref
	 *            set to true if this Polyline should share the same vertices.
	 */
	public Polyline(Polyline other, boolean byref) {
		this(other.getPoints(), byref);
		setLocalFrame(other.getLocalFrame());
	}

	/**
	 * Creates a new polyline having the same geometry, but allocating new
	 * memory for the vertices (i.e. vertices are not copied by reference).
	 * 
	 * @param other
	 */
	public Polyline(Polyline other) {
		this(other, false);
	}

	/**
	 * Builds a HairStrand from these points, starting at start and ending at
	 * end, inclusively.
	 * 
	 * @param pts
	 * @param start
	 * @param end
	 */
	public Polyline(List<Point3d> pts, int start, int end) {
		points = new LinkedList<Point3d>();
		for (int i = start; i <= end; i++) {
			points.add(new Point3d(pts.get(i)));
		}

		computeLength();
	}

	/**
	 * Full orientation frame = this.local frame * this.rotation.
	 * 
	 * @return the world frame at the root of this polyline.
	 */
	public Matrix4d getRotationFrame() {
		return rotationFrame;
	}

	/**
	 * Set the world frame of this polyline. Full orientation frame = this.local
	 * frame * this.rotation.
	 * 
	 * @param m
	 */
	public void setRotationFrame(Matrix4d m) {
		rotationFrame.set(m);
	}

	/**
	 * Full orientation frame = this.local frame * this.rotation.
	 * 
	 * @return the orientation frame at the root of this polyline.
	 */
	public Matrix4d getLocalFrame() {
		return localFrame;
	}

	/**
	 * Set the local frame of this polyline. Full orientation frame = this.local
	 * frame * this.rotation.
	 * 
	 * @param m
	 */
	public void setLocalFrame(Matrix4d m) {
		localFrame.set(m);
//		rotationFrame.set(m);
	}

	/**
	 * @return The list of points contained making this polyline.
	 */
	public List<Point3d> getPoints() {
		return points;
	}

	/**
	 * @param i
	 * @return the i-th control vertex of this polyline.
	 */
	public Point3d getPoint(int i) {
		return points.get(i);
	}

	/**
	 * @return The origin of this polyline.
	 */
	public Point3d getOrigin() {
		return points.get(0);
	}

	private double computeLength() {
		// Compute the length of the strand
		length = 0;

		if (points.size() == 0)
			return 0;

		Point3d p1 = points.get(0);
		for (Point3d p : points) {
			if (p == null) continue;
			
			length += p.distance(p1);

			p1 = p;
		}
		
		return length;
	}

	/**
	 * @return The Euclidian length of this polyline, root-to-end.
	 */
	public double getLength() {
		return computeLength();
	}

	/**
	 * @return The number of points in this polyline.
	 */
	public int getCount() {
		return points.size();
	}

	/**
	 * Moves this polyline to a new location.
	 * 
	 * @param newOrigin
	 */
	public void moveTo(Tuple3d newOrigin) {
		// Subtract the current origin
		// and move to a new offset.

		for (int i = 1; i < points.size(); i++) {
			Point3d p = points.get(i);
			p.sub(points.get(0));
			p.add(newOrigin);
		}

		points.get(0).set(newOrigin);
	}

	/**
	 * Display this polyline in an opengl canvas.
	 * 
	 * @param drawable
	 */
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);

		// First draw the strand
		boolean strip = true;
		if (strip) {
			gl.glBegin(GL.GL_LINE_STRIP);
			for (Point3d p : points) {
				gl.glVertex3d(p.x, p.y, p.z);
			}
			gl.glEnd();
		} else {
			gl.glBegin(GL.GL_LINES);
			Point3d p0 = new Point3d(points.get(0));
			for (Point3d p : points) {
				gl.glVertex3d(p0.x, p0.y, p0.z);
				gl.glVertex3d(p.x, p.y, p.z);

				p0.set(p);
			}
			gl.glEnd();
		}
	}

	/**
	 * 
	 * @return The last point of this polyline.
	 */
	public Point3d getTip() {
		return points.get(points.size() - 1);
	}

	/**
	 * Appply a transformation on each vertex of this polyline. The local
	 * orientation frame is also transformed
	 * 
	 * @param m
	 */
	public void transform(Matrix4d m) {
		for (Point3d p : this) {
			m.transform(p);
		}

		getLocalFrame().mul(m, getLocalFrame());
	}

	/**
	 * Scale each point of this polyline by a factor.
	 * 
	 * @param d
	 */
	public void scale(double d) {
		// For move to origin
		Point3d p0 = new Point3d(getOrigin());
		moveTo(new Point3d());
		// Then scale
		for (Point3d p : points) {
			p.scale(d);
		}
		moveTo(p0);

		computeLength();
	}

	/**
	 * Normalize this polyline. (Set the length of this polyline to 1)
	 */
	public void normalize() {
		scale(1 / getLength());
		length = 1;
	}

	/**
	 * Concatenate another polyline to the end of this one.
	 * 
	 * @param residual
	 */
	public void concatenate(Polyline other) {
		getPoints().addAll(other.getPoints());
		computeLength();
	}

	@Override
	public Iterator<Point3d> iterator() {
		return getPoints().iterator();
	}

	/**
	 * @param pts
	 * @param numcycles
	 * @param smoothm
	 */
	public static void smooth(List<Point3d> ptsStore, int numcycles, double smoothm) {

		for (int c = 0; c < numcycles; c++) {
			// Create a copy of the list to smooth
			List<Point3d> pts = new ArrayList<Point3d>();
			for (Point3d pt : ptsStore) {
				pts.add(new Point3d(pt));
			}
			
			// Clear old list
			ptsStore.clear();

			int size = pts.size();
			// Treat first and last point differently
			// i.e. preserve their position
			ptsStore.add(pts.get(0));

			for (int i = 1; i < size-1; i++) {

				Point3d p1 = pts.get(i-1);
				Point3d p2 = pts.get(i);
				Point3d p3 = pts.get(i + 1);

				Point3d ptn = new Point3d();
				ptn.scaleAdd(smoothm / 2, p1, ptn);
				ptn.scaleAdd(1 - smoothm, p2, ptn);
				ptn.scaleAdd(smoothm / 2, p3, ptn);
				
				ptsStore.add(ptn);
			}
			ptsStore.add(pts.get(pts.size()-1));
		}
	}

	public Polyline tesselate(int targetCount) {
		
		if (targetCount > getCount()) {
			System.err.println("Cannot subsample, maximum reached.");
			return this;
		}
		if (targetCount <= 0) {
			System.err.println("Cannot subsample, minimum reached.");
			return this;
		}

		double skip = ((double)getCount()) / targetCount;
//		skip = 1.5;

		List<Point3d> pts = new LinkedList<Point3d>();
		
		// Add the first point
		pts.add(getPoint(0));

		// Reserve the tip until the end
		for (double i = skip; i < getCount() - 2; i += skip) {
			pts.add(getPoint((int)i));
		}

		// Add the tip
		pts.add(getTip());

		set(pts);
		
		return this;
	}

	/**
	 * Reduces the number of vertices in that polyline.
	 * @param per the percentage of the total vertex count to use in the tesselation.
	 * @return this polyline.
	 */
	public Polyline tesselate(double per) {
		
		int targetCount = (int) Math.ceil(getCount() * per);
		
		if (targetCount > getCount()) {
			System.err.println("Cannot subsample, maximum reached.");
			return this;
		}
		if (targetCount <= 0) {
			System.err.println("Cannot subsample, minimum reached.");
			return this;
		}

		double skip = ((double)getCount()) / targetCount;
//		skip = 1.5;

		List<Point3d> pts = new LinkedList<Point3d>();
		
		// Add the first point
		pts.add(getPoint(0));

		// Reserve the tip until the end
		for (double i = skip; i < getCount() - 2; i += skip) {
			pts.add(getPoint((int)i));
		}

		// Add the tip
		pts.add(getTip());

		set(pts);
		
		return this;
	}
	
	/**
	 * Average these two polylines together and store the result in this polyline. Only considers the indices that are available in both polylines.
	 * @param poly
	 */
	public void average(Polyline poly) {
		Point3d o1 = this.getOrigin();
		Point3d o2 = poly.getOrigin();
		
		// TODO: Could also consider weighted averaging (change w)
		double w = 0.5;
		for (int i = 0; i < Math.min(this.getCount(), poly.getCount()); i++) {
			Point3d p1 = this.getPoint(i);
			Point3d p2 = poly.getPoint(i);
			
			Point3d p1s = new Point3d();
			p1s.sub(p1, o1);
			p1s.scale(w);
			
			Point3d p2s = new Point3d();
			p2s.sub(p2, o2);
			p2s.scale(1 - w);
			
			p1.add(p1s, p2s);
		}
	}

}
