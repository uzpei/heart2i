package tools.geom;

import gl.geometry.primitive.Plane;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3i;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;

import no.uib.cipr.matrix.DenseLU;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.Matrix.Norm;
import no.uib.cipr.matrix.MatrixSingularException;
import no.uib.cipr.matrix.NotConvergedException;
import no.uib.cipr.matrix.SVD;

import org.apache.commons.math3.distribution.LogNormalDistribution;

public class MathToolset {
	private static Random rand = new Random();
	
	public final static double epsilon = 1e-12;

	public final static double epsilonEighth = 1e-8;

	public static Vector3d getRandomVector3d(double scale) {
		scale *= 2;
		return new Vector3d(scale * (-0.5 + rand.nextDouble()), scale * (-0.5 + rand.nextDouble()), scale * (-0.5 + rand.nextDouble())); 
	}
	
	public static Vector3d getRandomUnitVector3d() {
		Vector3d v = MathToolset.getRandomVector3d(1);
		v.normalize();
		
		return v;
	}
	
	public static double getMaxComponent(Vector3d v) {
		return Math.max(Math.max(v.x, v.y), v.z);
	}

	/**
	 * Find an orthogonal vector from the smallest component of v
	 * @param v
	 * @return
	 */
	public static Vector3f getOrthogonal(Vector3f v) {
		
		Vector3f e2 = new Vector3f();
		
		e2.set(v);
		e2.absolute();

		// This set of tests produces a right handed coordinate system
		// in the canonical basis X, Y, Z if v = (1, 0, 0)
		
		// x = y = z
		if (e2.x == e2.y && e2.y == e2.z)
			e2.x = 0;
		// x <= y | z
		else if (e2.x <= e2.y && e2.x <= e2.z)
			e2.x = Math.max(e2.y, e2.z);
		// y <= x | z
		else if (e2.y <= e2.x && e2.y <= e2.z)
			e2.y = Math.max(e2.x, e2.z);
		// z, x | y
		else if (e2.z <= e2.x && e2.z <= e2.y)
			e2.z = Math.max(e2.x, e2.y);

		// e2 is then found by cross product of this vector with v
		e2.cross(v, e2);
		e2.normalize();
		return e2;
//		// First find smallest component
//		int vi = -1;
//		
//		if (v.x <= v.y) {
//			if (v.x <= v.z){
//				vi = 0;
//			}
//			else {
//				vi = 2;
//			}
//		}
//		else if (v.y <= v.z) {
//			vi = 1;
//		}
//		else
//			vi = 2;
//		
//		float[] vma = new float[3];
//		vma[vi] = 1;
//		Vector3f vm = new Vector3f(vma);
//		
//		// Find orthogonal vector 
//		vm.scaleAdd(-vm.dot(v), v, vm);
//		
//		// Normalize
//		vm.normalize();
//		
//		return vm;
	}

	/**
	 * Round each component of this Tuple3d to the nearest integer and returns a Point3i containing them.
	 * @param t
	 * @return
	 */
	public static Point3i tuple3dTo3i(Tuple3d t) {
		return new Point3i(roundInt(t.x), roundInt(t.y), roundInt(t.z));
	}

	public static Point3f tuple3dTo3f(Tuple3d t) {
		return new Point3f((float) t.x, (float) t.y, (float) t.z);
	}

	public static Vector3f tuple3dToVector3f(Tuple3d t) {
		return new Vector3f((float) t.x, (float) t.y, (float) t.z);
	}

	public static Vector3d tuple3iToVector3d(Tuple3i t) {
		return new Vector3d(t.x, t.y, t.z);
	}

	public static Point3d tuple3iToPoint3d(Tuple3i t) {
		return new Point3d(t.x, t.y, t.z);
	}

	/*
	 * Scalar operations
	 */
	
	/**
	 * @param v
	 * @return the nearest integer number to v
	 */
	public static int roundInt(double v) {
		return (int) Math.round(v);
	}

	public static int symmetricRound(double v) {
		return (int) (Math.signum(v) * Math.ceil(Math.abs(v)));
	}

	/**
	 * @param v
	 * @return the nearest integer number to v
	 */
	public static int roundInt(float v) {
		return Math.round(v);
	}

	/**
	 * @param v
	 * @return the symmetrical floor of v (i.e. absFloor(-1.5) = 1 = absFloor(1.5))
	 */
	public static int absFloor(double v) {
		return (int) (Math.signum(v) * Math.floor(Math.abs(v)));
	}

	public static List<Point3d> tuple3iListTo3d(List<Point3i> pts) {
		List<Point3d> ptsd = new LinkedList<Point3d>();
		
		for (Point3i p : pts) {
			ptsd.add(MathToolset.tuple3iToPoint3d(p));
		}
		
		return ptsd;
	}

	/*
	 * Tuple list conversions
	 */

	public static List<Point3i> tuple3dListTo3i(List<Point3d> pts) {
		List<Point3i> ptsd = new LinkedList<Point3i>();
		
		for (Point3d p : pts) {
			ptsd.add(MathToolset.tuple3dTo3i(p));
		}
		
		return ptsd;
	}

	/*
	 * Tuple classes extensions
	 */
	public static double dist(Point3i p1, Point3i p2) {
		return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y) + (p1.z - p2.z) * (p1.z - p2.z));
	}
	
	/*
	 * Mathematical basis functions
	 */
	
	/**
	 * @param pts
	 * @param values
	 * @param x
	 * @return the Lagrange basis evaluated at {@code x} from a set of node pairs (Point3d, Double)
	 */
	public static double lagrangeBasis(List<Point3d> pts, List<Double> values, Point3d x) {
		int n = pts.size();

		// Precompute pairwise distance
		double[][] ijdist = new double[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				ijdist[i][j] = pts.get(i).distance(pts.get(j));
				ijdist[j][i] = ijdist[i][j];
			}
		}
		
		// Precompute point-control distances
		double[] idist = new double[n];
		for (int i = 0; i< n; i++) {
			idist[i] = pts.get(i).distance(x);
		}

		// For each control point
		double f = 0;
		for (int j = 0; j< n; j++) {
			double l = 1;
			
			// Compute Lagrange basis
			for (int i = 0; i < n; i++) {
				if (i == j) continue;
				l *= idist[i] / ijdist[i][j];
			}
			
			f += l * values.get(j);
		}
		
		return f;
	}

	public static int[] linearIntegerSpace(int v1, int v2) {
		return linearIntegerSpace(v1, v2, v2 - v1 + 1);
	}
	
	public static int[] linearIntegerSpace(int v1, int v2, int n) {

		// Eventually replace by HashSet
		int[] space = new int[n];
	
		double[] spaced = linearSpace(v1, v2, n);
		
		for (int i = 0; i < spaced.length; i++) {
			space[i] = roundInt(spaced[i]);
		}
		
		return space;
	}

	/**
	 * Compute linear <code>n</code>-integer sampling from <code>v1</code> to <code>v2</code> (both inclusive)
	 * @param v1
	 * @param v2
	 * @param n
	 * @return an n-integer sampling from v1 to v2 (v2-v1+1 / n points in total)
	 */
	public static double[] linearSpace(double v1, double v2, int n) {
		double[] space = new double[n];
		
		double span = v2 - v1;
		double delta = span / (n-1);

		for (int i = 0; i < n; i++) {
			space[i] = v1 + i * delta;
		}
		
		return space;
	}

	public static double clampUp(double value, double clamp) {
		return value > clamp ? clamp : value;
	}

	public static double clampDown(double value, double clamp) {
		return value < clamp ? clamp : value;
	}
	
	/**
	 * Clamp a value in the inclusive range [clampDown, clampUp]
	 * @param value
	 * @param clampDown
	 * @param clampUp
	 * @return
	 */
	public static double clamp(double value, double clampDown, double clampUp) {
		return clampDown(clampUp(value, clampUp), clampDown);
	}
	
	/*
	 * Number theory
	 */
	
	/**
	 * Pack three numbers occupying a maximum of 10 bits (magnitude ~1000) into a single 32 bits integer.
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public static int pack(int a, int b, int c)
	{
	    return (a << 20) | (b << 10) | c;
	}

	public double[] logNormal(double mean, double sigma, double[] x) {
		LogNormalDistribution distrib = new LogNormalDistribution(mean, sigma);
		double[] density = new double[x.length];
		int i = 0;
		for (double xi : x)
			density[i++] = distrib.density(xi);
	
		return density;
	}
	
	/**
	 * Unpack three numbers containing 10 bits from a 32 bits int.
	 * @param combined
	 */
	public static int[] unpack(int packed)
	{
		return new int[] { packed >> 20, (packed >> 10) & 0x3ff, packed & 0x3ff };
	}
	
	/*
	 * Sampling
	 */
	
	/**
	 * @param n
	 *            the number of jittered points.
	 * @param size
	 *            the size of one side of the sampling grid.
	 * @return a stratified jittered sampling consisting of n = (m * m) points
	 *         in a grid of size <code>size</code>.
	 */
	public static Point3d[] jitterStratified(int n, double size, Plane plane) {
		Point3d[] jitter = new Point3d[n * n];

		// Determine grid dimensions
		double step = size / (n - 1);

		for (int p = 0; p < n * n; p++) {
			int i = p / n;
			int j = p % n;

			Point3d pt = new Point3d();
			pt.add(plane.getCorner());
//			pt.add(p1);
			pt.scaleAdd(i * step, plane.getS(), pt);
			pt.scaleAdd(j * step, plane.getT(), pt);
			jitter[p] = pt;
		}

		return jitter;
	}

	/**
	 * @param n
	 *            the number of jittered points.
	 * @param size
	 *            the size of one side of the sampling grid.
	 * @return a randomized stratified jittered sampling consisting of n = (m *
	 *         m) points in a grid of size <code>size</code>.
	 */
	public static Point3d[] jitter(int n, double size, Plane plane) {
		Point3d[] jitter = new Point3d[n * n];

		// Determine grid dimensions
		double step = size / (n - 1);
		Random rand = new Random();
		double rmag = 0.5 * step;
		double rmag2 = rmag / 2;
		double ri, rj;
		int i, j;

		for (int p = 0; p < n * n; p++) {
			i = p / n;
			j = p % n;

			ri = -rmag2 + rmag * rand.nextDouble();
			rj = -rmag2 + rmag * rand.nextDouble();

			Point3d pt = new Point3d();
			pt.add(plane.getCorner());
			pt.scaleAdd(i * step + ri, plane.getS(), pt);
			pt.scaleAdd(j * step + rj, plane.getT(), pt);
			jitter[p] = pt;
		}

		return jitter;
	}

	/**
	 * Poisson disc sampling in 3D (n=3) using Bridson's Fast Poisson Disk
	 * Sampling in Arbitrary Dimensions.
	 * @param r the density (inversely proportional)
	 */
	public static Point3d[] poissonSampling3D(double r, double size) {

		// Initialize active and background list
		List<Integer> activeList = new ArrayList<Integer>();
		List<Point3d> backgroundList = new ArrayList<Point3d>();

		// Center of region
		Point3d c = new Point3d(0, 0, 0);
		
		// Initial sample x0
		Random rand = new Random();
//		Point2d x0 = new Point2d(rand.nextDouble() * xs, rand.nextDouble() * ys);
		activeList.add(0);
		backgroundList.add(c);

		// Maximal number of iterations allowed
		int maxits = 50000;
		int it = 0;
		
		int k = 10;

		while (activeList.size() > 0 && it < maxits) {
			boolean found = false;

			// Select random index in active list
			int i = rand.nextInt(activeList.size());
			Point3d xi = backgroundList.get(activeList.get(i));

			for (double thetaSample = 0; thetaSample <= 2 * Math.PI; thetaSample += 2
					* Math.PI / k) {
				for (double phiSample = -Math.PI/2; phiSample <= Math.PI/2; phiSample += Math.PI / k) {
					// Generate point in spherical shell between radius r and 2r around xi
			
					Point3d p = new Point3d();
					double rSample = r * (1 + rand.nextDouble());
//					double thetaSample = rand.nextDouble() * 2 * Math.PI;
//					double phiSample = (-0.5 + rand.nextDouble()) * Math.PI;
					
					p.x = xi.x + rSample * Math.cos(phiSample)
							* Math.cos(thetaSample);
					p.y = xi.y + rSample * Math.cos(phiSample)
							* Math.sin(thetaSample);
					p.z = xi.z + rSample * Math.sin(phiSample);

					// Check if new point lies within distance r of existing
					// samples.
					// If it is sufficiently far, emit it as the next sample and
					// add
					// it to the active list.
					if (verifyMinimumDistance(r, backgroundList, p)
							&& p.distance(c) < size) {
						activeList.add(backgroundList.size());
						backgroundList.add(p);
						found = true;
					}
					if (found)
						break;
				}
				if (found)
					break;
			}

			 if (found == false)
				 activeList.remove(i);

			it++;
		}

		Point3d[] jitter = new Point3d[backgroundList.size()];
		int ji = 0;
		for (Point3d pt : backgroundList) {
			jitter[ji++] = pt;
		}

		return jitter;	
		
	}
	/**
	 * Poisson disc sampling in 2D (n=2) using Bridson's Fast Poisson Disk
	 * Sampling in Arbitrary Dimensions.
	 * @param r the density (inversely proportional)
	 */
	public static Point3d[] poissonSampling2D(double r, Plane plane) {

		int n = 2;
		int k = 30;

		// Cell size (for accelerating search... to implement later)
		double csize = r / Math.sqrt(n);

		// Domain size
		double xs = plane.getS().length();
		double ys = plane.getT().length();
		double size = Math.min(xs, ys) / 2;

		// Initialize active and background lists
		List<Integer> activeList = new ArrayList<Integer>();
		List<Point3d> backgroundList = new ArrayList<Point3d>();

		// Center of region
		Point3d c = new Point3d(0, 0, 0);
		
		// Initial sample x0
		Random rand = new Random();
//		Point2d x0 = new Point2d(rand.nextDouble() * xs, rand.nextDouble() * ys);
		activeList.add(0);
		backgroundList.add(c);

		int maxits = 100000;
		int it = 0;
		while (activeList.size() > 0 && it < maxits) {
			boolean found = false;

			// Select random index in active list
			int i = rand.nextInt(activeList.size());
			Point3d xi = backgroundList.get(activeList.get(i));

			double theta = 0;
			for (int j = 0; j < k; j++) {
				// Generate point in spherical annulus between radius r and 2r
				// around xi
				Point3d p = new Point3d();
				double ar = r * (1 + rand.nextDouble());

				p.x = xi.x + ar * Math.cos(theta);
				p.y = xi.y + ar * Math.sin(theta);

				// Check if new point lies within distance r of existing
				// samples.
				// If it is sufficiently far, emit it as the next sample and add
				// it to the active list.
				if (verifyMinimumDistance(r, backgroundList, p) && p.distance(c) < size) {
					activeList.add(backgroundList.size());
					backgroundList.add(p);
					found = true;
				}

				theta += 2 * Math.PI / k;
			}

			 if (found == false)
				 activeList.remove(i);

			it++;
		}

		// Map to 3d coordinates inside the plane
		Point3d[] jitter = new Point3d[backgroundList.size()];
		int ji = 0;
		for (Point3d pt : backgroundList) {
			Point3d pt3 = new Point3d();
			pt3.scaleAdd(pt.x, plane.getS(), pt3);
			pt3.scaleAdd(pt.y, plane.getT(), pt3);
			pt3.add(plane.getOrigin());
			jitter[ji++] = pt3;
		}

		return jitter;
	}
	
	/**
	 * Poisson disc sampling in 2D (n=2) using Bridson's Fast Poisson Disk
	 * Sampling in Arbitrary Dimensions.
	 */
	public static Vector4d[] poissonSamplingWeighted(double r, Plane plane) {
		Point3d[] jitter = poissonSampling2D(r, plane);
		
		double l = plane.getSmallestSide() / 2;
		
		Vector4d[] weighted = new Vector4d[jitter.length];
		int i = 0;
		for (Point3d p : jitter) {
			weighted[i++] = new Vector4d(p.x, p.y, p.z, 1 - p.distance(plane.getOrigin()) / l);
		}

		return weighted;
	}

	/*
	 * 
	 * Matrix computations
	 * 
	 */
	
	public static DenseMatrix kronecker(DenseMatrix A, DenseMatrix B) {
		int m = A.numRows();
		int n = A.numColumns();
		int p = B.numRows();
		int q = B.numColumns();
		
		DenseMatrix product = new DenseMatrix(m * p, n * q);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				double aij = A.get(i, j);
				for (int k = 0; k < p; k++) {
					for (int l = 0; l < q; l++) {
						product.set(i * p + k, j * q + l, aij * B.get(k, l));
					}
				}
			}
		}
		
		return product;
	}
	
    /**
     * Compute v1 * v2'
     * @param v1
     * @param v2
     * @return
     */
    public static DenseMatrix outerProduct(Vector3d v1, Vector3d v2) {
    	DenseMatrix prod = new DenseMatrix(3, 3);
    	prod.set(0, 0, v1.x * v2.x);
    	prod.set(0, 1, v1.x * v2.y);
    	prod.set(0, 2, v1.x * v2.z);

    	prod.set(1, 0, v1.y * v2.x);
    	prod.set(1, 1, v1.y * v2.y);
    	prod.set(1, 2, v1.y * v2.z);

    	prod.set(2, 0, v1.z * v2.x);
    	prod.set(2, 1, v1.z * v2.y);
    	prod.set(2, 2, v1.z * v2.z);
    	
    	return prod;
    }
    /**
     * Compute v1 * v2'
     * @param v1
     * @param v2
     * @return
     */
    public static DenseMatrix outerProduct(Point3i v1, Point3i v2) {
    	DenseMatrix prod = new DenseMatrix(3, 3);
    	prod.set(0, 0, v1.x * v2.x);
    	prod.set(0, 1, v1.x * v2.y);
    	prod.set(0, 2, v1.x * v2.z);

    	prod.set(1, 0, v1.y * v2.x);
    	prod.set(1, 1, v1.y * v2.y);
    	prod.set(1, 2, v1.y * v2.z);

    	prod.set(2, 0, v1.z * v2.x);
    	prod.set(2, 1, v1.z * v2.y);
    	prod.set(2, 2, v1.z * v2.z);
    	
    	return prod;
    }
       
    public static DenseMatrix outerProduct(Vector3d v, DenseVector v2) {
    	DenseMatrix prod = new DenseMatrix(3, v2.size());
    	
    	for (int j = 0; j < v2.size(); j++) {
    		prod.set(0, j, v.x * v2.get(j));
    		prod.set(1, j, v.y * v2.get(j));
    		prod.set(2, j, v.z * v2.get(j));
    	}
    	
    	return prod;
    }
    
    public static DenseMatrix outerProduct(Point3i v, DenseVector v2) {
    	DenseMatrix prod = new DenseMatrix(3, v2.size());
    	int[] va = new int[3];
    	v.get(va);
    	
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
    			prod.set(i, j, va[i] * v2.get(j));
    	
    	return prod;
    }

    
    /**
     * Vectorize row-major (looping through rows first, for each column)
     * @param m
     * @return
     */
    public static DenseVector vectorize(DenseMatrix m) {
    	DenseVector v = new DenseVector(m.numRows() * m.numColumns());
    	
    	int k = 0;
    	for (int j = 0; j < m.numColumns(); j++) {
        	for (int i = 0; i < m.numRows(); i++) {
        		v.set(k++, m.get(i, j));
        	}    		
    	}
    	
    	return v;
    }
    
    /**
     * Thin-plate spline radial basis function (RBF).
     * phi = | p - p_0 | ^ 3
     * @param p
     * @param p0
     * @return
     */
	public static double thinPlateBasisFunction(double px, double py, double pz, double p0x, double p0y, double p0z) {

        // Compute L2 norm
        double l = Math.sqrt((px - p0x) * (px - p0x) + (py - p0y) * (py - p0y) + (pz - p0z) * (pz - p0z));

        // Check for threshold
        if (l <= epsilon) return 0;
        
//        // Thin-plate radial basis function |x-c| ^3
//        // Coefficient = gamma(n / 2 - 2) / (16 pi^(n/2)) where n = dimensionality
//        // for n != 2 or 4
//        // here n = 3
//        // such that coef = gamma(3/2 - 2) / (16 pi^(3/2)) = -0.0398
//        return -0.0398 * (l * l * l);
//    	return l*l*Math.log(l);
        return l * l * l;
    }

	/**
	 * Compute the condition number of a matrix given a matrix norm.
	 */
	public static double conditionNumber(DenseMatrix A, Norm norm) {
		// Empty matrix
		if (A.getData().length == 0)
			return Double.MAX_VALUE;
		
		Matrix Ainv = MathToolset.pseudoInverse(A, 1e-18);
		double k = Ainv.norm(norm) * A.norm(norm);
		return k;
	}
	
	/**
	 * Compute the pseudoinverse of A using and SVD decomposition.
	 * @param A
	 * @param b
	 * @param singularThreshold
	 */
    public static Matrix pseudoInverse(DenseMatrix A, double singularThreshold) {
//    	System.out.println("Computing SVD pseudoinverse...");
    	int m = A.numRows();
    	int n = A.numColumns();
        SVD svd = new SVD(m, n);
        try {
            svd = SVD.factorize( A);
		} catch (NotConvergedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        // Extract the decomposition
        // U is mxm
        // V is nxn
        // S is mxn;
		DenseMatrix U = svd.getU();
		DenseMatrix Vt = svd.getVt();
//		TridiagMatrix S = new TridiagMatrix(svd.getS().length);
//		System.arraycopy(svd.getS(), 0, S.getDiagonal(), 0, svd.getS().length);
		double[] S = svd.getS();
		
		// Transpose U
		DenseMatrix Ut = new DenseMatrix(U.numColumns(), U.numRows());
		U.transpose(Ut);

		// Transpose Vt
		DenseMatrix V = new DenseMatrix(Vt.numColumns(), Vt.numRows());
		Vt.transpose(V);

		// Invert S
		DenseMatrix Sinv = new DenseMatrix(n, m);
//		double v;
//		for (MatrixEntry e : S) {
//			v = e.get();
//			e.set(v > singularThreshold ? 1 / v : 0);
//		}
		for (int i = 0; i < S.length; i++) {
			Sinv.set(i,i,S[i] > singularThreshold ? 1 / S[i] : 0);
		}

		// Compute V*S-1*Ut
		Matrix SinvUt = Sinv.mult(Ut, new DenseMatrix(n,m));
		Matrix pseudoInverse = new DenseMatrix(n, m);
		V.mult(SinvUt,pseudoInverse);

		// Test the pseudoinverse
//		DenseMatrix I = new DenseMatrix(n,n);
//		pseudoInverse.mult(A, I);
		
		return pseudoInverse;
	}
    
	/**
	 * Solve for x in Ax = b using an SVD pseudoinverse.
	 * @param A
	 * @param b
	 * @param singularThreshold
	 */
    public static DenseVector SolveSVD(DenseMatrix A, DenseVector b, double singularThreshold) {
    	System.out.println("Computing SVD pseudoinverse...");
    	Matrix pseudoInverse = pseudoInverse(A, singularThreshold);
		DenseVector x = new DenseVector(A.numColumns());
		pseudoInverse.mult(b, x);

		return x;
	}

	/**
	 * Solve for X in AX = B using an SVD pseudoinverse.
	 * @param A
	 * @param B
	 * @param singularThreshold
	 */
    public static DenseMatrix SolveSVD(DenseMatrix A, DenseMatrix B, double singularThreshold) {
//    	System.out.println("Computing SVD pseudoinverse...");
    	Matrix pseudoInverse = pseudoInverse(A, singularThreshold);
		DenseMatrix X = new DenseMatrix(A.numColumns(), B.numColumns());
		pseudoInverse.mult(B, X);

		return X;
	}

	public static DenseVector SolveLU(DenseMatrix A, DenseVector b) {
		System.out.println("Computing LU factorization...");
		
		int n = A.numColumns();
		DenseVector weights = new DenseVector(n);

		try {

			DenseLU lu = DenseLU.factorize(A);
			DenseMatrix B = new DenseMatrix(b);
			System.out.println("Solving...");
			lu.solve(B);
			weights.set(new DenseVector(B.getData()));
		} catch (MatrixSingularException e) {
			// If the matrix is singular, use SVD
			System.err.println("Matrix is singular. Using SVD.");
			return SolveSVD(A, b, epsilon);
		}

		return weights;
	}

	/**
	 * Compute the nearest rotation matrix to this matrix and set it in-place.
	 */
	public static void orthogonalize(Matrix4d F) {
		javax.vecmath.Matrix3d m1 = new javax.vecmath.Matrix3d();
		F.get(m1);
		F.setRotationScale(m1);
	}
	
	/**
	 * Compute the nearest rotation matrix to this matrix by solving the orthogonal procrustes 
	 * (better to use the SVD renormalization)
	 */
//	public static javax.vecmath.Matrix3d orthogonalize(javax.vecmath.Matrix3d F) {
//		
//		javax.vecmath.Matrix3d Ft = new javax.vecmath.Matrix3d ();
//		Ft.transpose(F);
//		javax.vecmath.Matrix3d  Q = new javax.vecmath.Matrix3d (F);
//
//		javax.vecmath.Matrix3d  Qinv = new javax.vecmath.Matrix3d();
//		javax.vecmath.Matrix3d  QinvF = new javax.vecmath.Matrix3d();
//		javax.vecmath.Matrix3d  FtQ = new javax.vecmath.Matrix3d();
//		javax.vecmath.Matrix3d  mtemp1 = new javax.vecmath.Matrix3d();
//		javax.vecmath.Matrix3d  mtemp2 = new javax.vecmath.Matrix3d();
//		
//		// Number of iteration for Babylonian square root approximation
//		int n = 10;
//		for (int i = 0; i < n; i++) {
//			Qinv.invert(Q);
//			MotionTools.invertMatrix3d(Q, Qinv);
//
//			QinvF.mul(Qinv, F);
//			FtQ.add(Ft, Q);
//			
//			mtemp1.add(QinvF, FtQ);
//			MotionTools.invertMatrix3d(mtemp1, mtemp2);
//			
//			Q.mul(F, mtemp2);
//			Q.mul(2);
//		}
//		
//		return Q;
//	}

	private static boolean verifyMinimumDistance(double r, List<Point3d> points, Point3d p) {
		for (Point3d pt : points) {
			if (pt.distance(p) < r)
				return false;
		}

		return true;
	}
	
	public static void main(String[] args) {
		Vector3f v = new Vector3f(1,1,1);
		v.normalize();
		Vector3f vp = getOrthogonal(v);
		
		System.out.println(vp + " at " + vp.dot(v));
	}

	public static Vector3d randomDirection() {
		Vector3d v = new Vector3d(-1 + 2 * rand.nextDouble(), -1 + 2 * rand.nextDouble(), -1 + 2 * rand.nextDouble());
		v.normalize();
		return v;
	}

	public static Point3d randomPosition(double mag) {
		return new Point3d(-mag + 2 * rand.nextDouble() * mag, -mag + 2 * rand.nextDouble() * mag, -mag + 2 * rand.nextDouble() * mag);
	}

	/**
	 * Compute the mean value of this double array.
	 * @param array
	 * @return
	 */
	public static double mean(double[] array) {
		double sum = 0;
		
		for (double value : array)
			sum += value;
		
		return sum / array.length;
	}
	
	/**
	 * Compute the standard deviation (sqrt(variance)) of this double array.
	 * @param array
	 * @return
	 */
	public static double std(double[] array) {
		double sum = 0;
		
		double mean = mean(array);
		
		for (double value : array)
			sum += (mean - value) * (mean - value);
		
		return Math.sqrt(sum / array.length);
	}

	public static double polyevaluate(double x, double[] coefs) {
		double value = 0;
		for (int coef = 0; coef < coefs.length; coef++) {
			value += coefs[coef] * Math.pow(x, coefs.length - coef - 1);
		}
		
		return value;
	}

	public static boolean epsilonEqual(double v1, double v2, double epsilon) {
		return Math.abs(v1 - v2) < epsilon;
	}

	public static String asCommaDelimited(double[] array) {
		String text = "";
		for (double value : array)
			text += value + ",";
		
		// Remove last comma
		text = text.substring(0, text.length() - 1);	
		
		return text;
	}

	public static String asCommaDelimited(int[] array) {
		String text = "";
		for (double value : array)
			text += value + ",";
		
		// Remove last comma
		text = text.substring(0, text.length() - 1);	
		
		return text;
	}

	/**
	 * Generate n colors by sampling the HSB spectrum.
	 * @param n
	 * @return
	 */
	public static List<Color> getColors(int n) {
		List<Color> colors = new ArrayList<Color>();
		float deltaHue = 1f / n;
		for (int i = 0; i < n; i++) {
		    colors.add(Color.getHSBColor(i * deltaHue, 1, 1));
		}
		
		return colors;
	}

	public static boolean isNan(Vector3d v) {
		return Double.isNaN(v.x) || Double.isNaN(v.y) || Double.isNaN(v.z);
	}

	/**
	 * @param integer
	 * @return whether integer is an even number
	 */
	public static boolean isEven(int integer) {
		return integer % 2 == 0;
	}
	
	/**
	 * @param integer
	 * @return whether integer is an odd number
	 */
	public static boolean isOdd(int integer) {
		return !isEven(integer);
	}
	
	/**
	 * Make a number even (if necessary) by incrementing it
	 * @param integer
	 */
	public static int makeEvenUp(int integer) {
		if (isEven(integer))
			// nothing to do
			return integer;
		else
			return integer + 1;
	}
	
	/**
	 * Make a number odd (if necessary) by incrementing it
	 * @param integer
	 */
	public static int makeOddUp(int integer) {
		if (isOdd(integer))
			// nothing to do
			return integer;
		else
			return integer + 1;
	}

	public static String toString(double d, int digits) {
        DecimalFormat f = new DecimalFormat();
		// Set max fraction digits so that truncation does not occur. Setting
		// the max to Integer.MAX_VALUE may cause problems with some JDK's.
		f.setMaximumFractionDigits(digits);
		f.setMinimumFractionDigits(0);
		f.setMinimumIntegerDigits(1);
		f.setGroupingUsed(false);
		return f.format(d);
	}

	public static double[] threshold(double[] list, double min, double max) {
		int count = 0;
		for (double v : list) {
			if (v >= min && v <= max) 
				count++;
		}
		double[] thresholded = new double[count];
		count = 0;
		for (double v : list) {
			if (v >= min && v <= max) {
//				System.out.println(v);
				thresholded[count++] = v;
			}
		}
		
		return thresholded;
	}

}
