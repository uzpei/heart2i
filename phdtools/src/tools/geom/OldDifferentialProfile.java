package tools.geom;

import gl.math.FlatMatrix4d;
import gl.renderer.JoglTextRenderer;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import tools.computations.MotionTools;

/**
 * 
 * @author piuze
 */
public class OldDifferentialProfile implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7485310364405387955L;
    
    private double epsilon;
    private List<Integer> inflections;
    private int inflectionMax;
    private List<Matrix4d> inflectionFrames;
    private List<Point3d> pts;
    private Matrix4d localFrame;
    Vector3d T = new Vector3d();
    Vector3d N = new Vector3d();
    Vector3d B = new Vector3d();
    int n;
    int[] flips;
    
    public OldDifferentialProfile(List<Point3d> pts) {
        this.pts = pts;
        n = pts.size();
        inflections = new LinkedList<Integer>();
        inflections.add(0);
        epsilon = 1e-12;
        inflectionMax = 1;
        flips = new int[n];
        
        for (int i = 0; i < n;i ++) {
            flips[i] = 1;
        }
        
        // Compute the first frame
        localFrame = computeFrame(0);
        MotionTools.invertRigidTransformation(localFrame, localFrame);

    }
    /**
     * @param pts A list of 3d points.
     * @param k The index (starting from zero) of the point at which
     * the Frenet frame is to be computed.
     * @return the Frenet frame at this index;
     */
    public Matrix4d getFrenetFrame(int k) {
        
        k = k < 0 ? 0 : k;
        k = k > n - 2 ? n - 2 : k;
        
        // If the query point lies in the range of the inflections points that have already been computed
        // else we have to determine if other inflection points exist beyond this point
        if (k > inflectionMax) {
            computeProfile(k);
        }
        
    	Matrix4d frame = computeFrame(k);
    	frame.mul(localFrame);
        return frame;
    }

    /**
     * @return the last Frenet frame;
     */
    public Matrix4d getLastFrenetFrame() {
        return getFrenetFrame(n);
    }

    /**
     * @return the first Frenet frame;
     */
    public Matrix4d getFirstFrenetFrame() {
        return getFrenetFrame(0);
    }
    
    public Matrix4d getFirstFrenetFrameInv() {
        return invert(getFirstFrenetFrame());
    }
 
    public Matrix4d invert(Matrix4d m) {
        return MotionTools.invertRigidTransformation(m, m);
    }
     
    /**
     * Compute the differential profile up to index k
     * @param k
     */
    private void computeProfile(int k) {
//        System.out.println("Computing profile at " + k + " with a current max of " + inflectionMax);
        
        // If it has already been computed, return
        if (k <= inflectionMax) return;
        
//        System.out.println("maximum inf = " + inflectionMax);
        
        int imax = k;
        boolean clp = true;
        
        // The first point is used as an inflection point
        int i = inflectionMax;
        boolean collinear = false;
        boolean inflection = false;
        
        double pcurv = Math.signum(getCurvature(i));
        double curv = 0;
        while (i <= imax) {
            collinear = false;
            inflection = false;
            
            Point3d p1 = pts.get(i-1);
            Point3d p2 = pts.get(i);
            Point3d p3 = pts.get(i+1);

            curv = Math.signum(getCurvature(i));
            
            boolean cl = isCollinear(p1, p2, p3);

            // The point we store is actually
            // the point just before the inflection.
            if (!clp && cl) {
                collinear = true;
            }
            
            if (curv != pcurv) {
//                System.out.println(curv + " VS " + pcurv);
//                inflection = true;
            }
            
            if (collinear || inflection) {
                inflections.add(i - 1);
                inflectionMax = i;
                
                // Increase the range such as to reach the last inflection point
                // (i.e. in the case of a long, straight line)
                if (i == imax && imax < n - 2) {
                    imax++;
                }
                
                // Update the sign flips for all points beyond this one
                for (int j = i; j < n; j++) {
                    flips[j] *= -1;
                }
            }
                        
            clp = cl;
            pcurv = curv;
            i++;
        }
        
        inflectionMax = imax;
        
//        System.out.println(inflections.size());
    }
    
    private Vector3d T1 = new Vector3d();
    private Vector3d T2 = new Vector3d();
    public boolean isCollinear(Point3d p1, Point3d p2, Point3d p3) {
        T1.sub(p2, p1);
        T2.sub(p3, p2);
        T1.cross(T1, T2);
        epsilon = 1e-18;
        return T1.lengthSquared() <= epsilon;
    }

    /**
     * Compute the Frenet frame along the hair strand and returns a tuple
     * containing the curvature (x), torsion (y), and euclidiean delta (z) at
     * this point index.
     * 
     * @param k
     * @return a tuple containing the curvature (x), torsion (y) and delta (z).
     */
    private Matrix4d computeFrame(int k) {
        Matrix4d frame = new Matrix4d();
        Point3d p1, p2, p3;
        Point3d o = new Point3d();

        // If the hair has a single point, return the identity matrix
        if (n < 2) {
            frame.setIdentity();
            o.set(pts.get(0));
            frame.setColumn(3, o.x, o.y, o.z, 1);
            return frame;
        }
        // If the hair has only 2 points, use these
        // 2 points for the tangent and use another arbitrary vector as binormal
        else if (n == 2) {
            p1 = pts.get(0);
            p2 = pts.get(1);
            o.set(p1);

            T.sub(p2, p1);
            T.normalize();

            int c = getMinCoordinate(T);
            B.set(c == 0 ? 1 : 0, c == 1 ? 1 : 0, c == 2 ? 1 : 0);
            N.cross(B, T);
            return buildFrame(T, N, B, o);
        }
        // otherwise we can use three points, with the frame centered on the second one
        else {
            // If this is the first frame, swap p1 and p0, or p2 and p1
            if (k < 1) {
//                p1 = pts.get(2);
//                p2 = pts.get(0);
//                p3 = pts.get(1);
//                o.set(p2);
            	int fn = 5;
                p1 = pts.get(fn);
                p2 = pts.get(fn+1);
                p3 = pts.get(fn+2);
                o.set(p1);
                k = 0;
            }
            // If this is the last frame, swap p2 and p1
            else if (k >= n - 1) {
                p1 = pts.get(n-3);
                p2 = pts.get(n-2);
                p3 = pts.get(n-1);
                o.set(p3);
                k = n - 1;
            }
            else {
                p1 = pts.get(k - 1);
                p2 = pts.get(k);
                p3 = pts.get(k + 1);
                o.set(p2);
            }
            
            if (isCollinear(p1, p2, p3)) {
//                System.out.println("Collinear at " + k);
                if (k > 0) {
                    return closestFrame(k);
                }
                else {
                    frame.setIdentity();
                    frame.setColumn(3, p2.x, p2.y, p2.z, 1);
                    return frame;
                }
            }

//            int sign = getOrientation(p1, p2, p3);
            int sign = flips[k];
            return buildFrame(p1, p2, p3, sign);
        }        
    }

    
    private Matrix4d closestFrame(int k) {
        int found = 0;
        for (int fxpt : inflections) {
//            System.out.println("Searching for inflections... found " + fxpt);
            if (fxpt > k) break;
            found = fxpt;
        }
        
//        System.out.println("closest inflection to " + k + " is " + found);
        
        Matrix4d frame = computeFrame(found);
        Point3d o = pts.get(k);
        frame.setColumn(3, o.x, o.y, o.z, 1);
//        System.out.println(frame);
        return frame;
    }
    /**
     * Build a frame from an orthogonal basis T, N, B 
     * corresponding to x=T, y=N, z=B
     * with origin located at o
     * @param T
     * @param N
     * @param B
     * @param o
     * @return the frame
     */
    private Matrix4d buildFrame(Vector3d T, Vector3d N, Vector3d B, Point3d o) {
        Matrix4d frame = new Matrix4d();
        
        frame.setColumn(0, T.x, T.y, T.z, 0);
        frame.setColumn(1, N.x, N.y, N.z, 0);
        frame.setColumn(2, B.x, B.y, B.z, 0);
        frame.setColumn(3, o.x, o.y, o.z, 1);
        
        return frame;
    }
    
    public Vector4d getT(Matrix4d frame) {
        Vector4d T = new Vector4d();
        frame.getColumn(0, T);
        return T;
    }
    public Vector4d getN(Matrix4d frame) {
        Vector4d N = new Vector4d();
        frame.getColumn(0, N);
        return N;
    }
    public Vector4d getB(Matrix4d frame) {
        Vector4d B = new Vector4d();
        frame.getColumn(0, B);
        return B;
    }
    
    private Matrix4d buildFrame(Point3d p0, Point3d p1, Point3d p2, int signum) {
        
        T.sub(p2, p1);
        T.normalize();

        B.sub(p1, p0);
        B.cross(B, T);
        B.normalize();
        
        N.cross(B, T);
        N.normalize();
        
        B.scale(signum);
        N.scale(signum);
        
        return buildFrame(T, N, B, p1);
    }

    /**
     * @param k
     * @return the signed curvature at this vertex index.
     */
    public double getCurvature(int k) {
        
        if (k < 1) {
            return getCurvature(1);
        } else if (k >= n - 1) {
            return getCurvature(n - 2);
        }

        Point3d p1 = pts.get(k - 1);
        Point3d p2 = pts.get(k);
        Point3d p3 = pts.get(k + 1);

        Vector3d t1 = new Vector3d();
        Vector3d t2 = new Vector3d();
        t1.sub(p2, p1);
        t2.sub(p3, p2);

        Vector3d Q = new Vector3d();
        Q.sub(p3, p1);

        double alpha = Math.acos(t1.dot(t2) / (t1.length() * t2.length()));

        double curvature = 2 * Math.sin(alpha) / Q.length();

        double s = getOrientation(p1, p2, p3);

        // if (s != 0)
        // k = k * -Math.signum(s);
        curvature *= s;
        // System.out.println(s);

        return curvature; 
    }

    /**
     * @param point
     * @return the signed torsion at this hair's vertex index.
     */
    public double getTorsion(int i) {
        if (n < 5) return 0;

        if (i <= 1) {
            return getTorsion(2);
        } else if (i >= n - 2) {
            return getTorsion(n - 3);
        }

        boolean symm = false;

        if (symm) {
            double Tip1 = getAsymTorsion(i);
            double Ti = getAsymTorsion(i + 1);

            Vector3d Li = new Vector3d();
            Vector3d Lip1 = new Vector3d();
            Li.sub(pts.get(i), pts.get(i - 1));
            Lip1.sub(pts.get(i + 1), pts.get(i));
            double Lis = Li.length();
            double Lip1s = Lip1.length();

            double Tsym = (Lis * Tip1 + Lip1s * Ti) / (Lis + Lip1s);

            return Tsym;
        } else {
            return getAsymTorsion(i);
        }

    }

    private double getAsymTorsion(int i) {
        Point3d pim2 = pts.get(i - 2);
        Point3d pim1 = pts.get(i - 1);
        Point3d pi = pts.get(i);
        Point3d pip1 = pts.get(i + 1);

        Vector3d Lim1 = new Vector3d();
        Vector3d Li = new Vector3d();
        Vector3d Lip1 = new Vector3d();

        Lim1.sub(pim1, pim2);
        Li.sub(pi, pim1);
        Lip1.sub(pip1, pi);

        Vector3d Vim1 = new Vector3d();
        Vector3d Vi = new Vector3d();

        Vim1.cross(Lim1, Li);
        Vi.cross(Li, Lip1);

        double Vim1s = Vim1.length();
        double Vis = Vi.length();

        if (Vim1s == 0 || Vis == 0) {
            return 0;
        }

        Vim1.scale(1 / Vim1s);
        Vi.scale(1 / Vis);

        double beta = Math.acos(Vim1.dot(Vi));

        double t = Math.sin(beta) / Li.length();

        // return s * t;
        return t;
    }

    /**
     * See http://mathworld.wolfram.com/TriangleArea.html
     * http://books.google.ca/
     * books?id=wCfWkc_E3GkC&pg=PA265&lpg=PA265&dq=3d+triangle
     * +signed+area&source
     * =bl&ots=zIXVUCbeFL&sig=AtiynDfcyKbAiMsdwVM7zB7SzNI&hl=en
     * &ei=_4NPS-KuGM6n8AbA7r2mCg
     * &sa=X&oi=book_result&ct=result&resnum=6&ved=0CCAQ6AEwBQ
     * #v=onepage&q=3d%20triangle%20signed%20area&f=false
     * 
     * @param l
     * @param k
     * @param r
     * @return
     */
    private int getOrientation(Point3d l, Point3d k, Point3d r) {

        // First turn the 3D triangle in barycentric coordinates
        // i.e. by projecting it into a 2D plane. We drop the coordinate
        // for which the normal has the largest value (in absolute value)

        Vector3d kr = new Vector3d();
        Vector3d kl = new Vector3d();
        Vector3d n = new Vector3d();
        kr.sub(r, k);
        kl.sub(l, k);
        n.cross(kr, kl);
        n.absolute();
        
        // If the three points are colinear, return 0
        if (isCollinear(l, k, r)) return 0;
        
        int c = getMaxCoordinate(n);
        double[] p1 = new double[] { l.x, l.y, l.z };
        double[] p2 = new double[] { k.x, k.y, k.z };
        double[] p3 = new double[] { r.x, r.y, r.z };

        // Reassign the coordinates based on the largest (absolute) entry in the
        // normal vector
        int x, y;
        if (c == 0) {
            x = 1;
            y = 2;
        } else if (c == 1) {
            x = 0;
            y = 2;
        } else {
            x = 0;
            y = 1;
        }

        return (int) Math.signum(0.5 * ((p3[x] - p2[x]) * p1[y] + (p1[x] - p3[x])
                * p2[y] + (p2[x] - p1[x]) * p3[y]));

    }

    /**
     * @param n
     * @return the largest (in absolute value) coordinate of this vector. 
     */
    private int getMaxCoordinate(Tuple3d n) {
        double max = Math.max(Math.max(n.x, n.y), Math.max(n.x, n.z));
        return n.x == max ? 0 : n.y == max ? 1 : 2;
    }

    /**
     * @param n
     * @return the minimum (in absolute value) coordinate of this vector. 
     */
    private int getMinCoordinate(Tuple3d n) {
        double min = Math.min(Math.min(n.x, n.y), Math.min(n.x, n.z));
        return n.x == min ? 0 : n.y == min ? 1 : 2;
    }
    
    public void displayFrenet(GLAutoDrawable drawable) {
        int skip = 10;

        FlatMatrix4d glm = new FlatMatrix4d();

        Matrix4d Mk = new Matrix4d();

		GL2 gl = drawable.getGL().getGL2();
        

        // First find the length of the point list
        // slow... but no choice for now
        double length = 0;
        for (int k = 0; k < n-1; k += skip) {
            length += pts.get(k).distance(pts.get(+1));
        }

        double scale = 0.005*length * skip;
        for (int k = 0; k < n - 1; k += skip) {

            Mk.set(getFrenetFrame(k));
            glm.setBackingMatrix(Mk);

            displayFrame(drawable, Mk, scale);
        }
        
        gl.glPointSize(10);
        gl.glBegin(GL2.GL_POINTS);
        for (int fxpt : inflections) {
            Point3d p = pts.get(fxpt);
            gl.glVertex3d(p.x, p.y, p.z);
        }
        gl.glEnd();

    }

    public void displayTorsion(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

        int skip = 1;
        Vector4d N = new Vector4d();
        Vector4d o = new Vector4d();

        List<Integer> inflectionPoints = new LinkedList<Integer>();
        
        int sign = 0;
        for (int k = 0; k < n - 1; k += skip) {
            Matrix4d frame = getFrenetFrame(k);
            frame.getColumn(2, N);
            frame.getColumn(3, o);
            
            double curv = getTorsion(k);
            if (Math.signum(curv) != sign) {
                inflectionPoints.add(k);
            }
            sign = (int) Math.signum(curv);
            N.scale(Math.abs(curv));
//            N.scale(1 / Math.abs(curv));
            N.add(o);
     
            gl.glLineWidth(2.0f);
            gl.glBegin(GL2.GL_LINES);
            gl.glVertex3d(o.x, o.y, o.z);
            gl.glVertex3d(N.x, N.y, N.z);
            gl.glEnd();
            
        }

        gl.glPointSize(8);

        for (int i = 0; i < n; i++) {
            double curv = getTorsion(i);
            sign = (int) Math.signum(curv);

            Point3d p = pts.get(i);
            if (sign <= 0)
                gl.glColor3d(0, 0, 0);
            else if (sign > 0)
                gl.glColor3d(1, 0, 0);
            gl.glBegin(GL2.GL_POINTS);
            gl.glVertex3d(p.x, p.y, p.z);
            gl.glEnd();
        }
        
        // Show inflection points
        gl.glColor3d(1, 0, 0);
        gl.glBegin(GL2.GL_POINTS);
        for (int i : inflectionPoints) {
            Point3d p = pts.get(i);
            gl.glVertex3d(p.x, p.y, p.z);
        }
        gl.glEnd();
        
    }

    public void displayCurvature(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

        int skip = 1;
        Vector4d N = new Vector4d();
        Vector4d o = new Vector4d();

        List<Integer> inflectionPoints = new LinkedList<Integer>();
        
        int sign = 0;
        for (int k = 0; k < n - 1; k += skip) {
            Matrix4d frame = getFrenetFrame(k);
            frame.getColumn(1, N);
            frame.getColumn(3, o);
            
            double curv = getCurvature(k);
            if (Math.signum(curv) != sign) {
                inflectionPoints.add(k);
            }
            sign = (int) Math.signum(curv);
            N.scale(Math.abs(curv) * flips[k]);
//            N.scale(1 / Math.abs(curv));
            N.add(o);
     
            gl.glLineWidth(2.0f);
            gl.glBegin(GL2.GL_LINES);
            gl.glVertex3d(o.x, o.y, o.z);
            gl.glVertex3d(N.x, N.y, N.z);
            gl.glEnd();
            
        }

        gl.glPointSize(7);

        for (int i = 0; i < n; i++) {
            double curv = getCurvature(i);
            sign = (int) Math.signum(curv);

            Point3d p = pts.get(i);
            if (sign < 0)
                gl.glColor3d(0, 0, 0);
            else if (sign > 0)
                gl.glColor3d(1, 0, 0);
            gl.glBegin(GL2.GL_POINTS);
            gl.glVertex3d(p.x, p.y, p.z);
            gl.glEnd();
        }
        
        // Show inflection points
        gl.glColor3d(1, 0, 0);
        gl.glBegin(GL2.GL_POINTS);
        for (int i : inflectionPoints) {
            Point3d p = pts.get(i);
            gl.glVertex3d(p.x, p.y, p.z);
        }
        gl.glEnd();
        
    }

    private void displayFrame(GLAutoDrawable drawable, Matrix4d frame) {
        displayFrame(drawable, frame, 0.3);
    }

    private void displayFrame(GLAutoDrawable drawable, Matrix4d frame, double scale) {
		GL2 gl = drawable.getGL().getGL2();
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LINE_SMOOTH);

        boolean dtext = false;
        double offset = 5;
        
        Vector4d T = new Vector4d();
        Vector4d N = new Vector4d();
        Vector4d B = new Vector4d();
        Vector4d o = new Vector4d();
        frame.getColumn(0, T);
        frame.getColumn(1, N);
        frame.getColumn(2, B);
        frame.getColumn(3, o);
        T.scale(scale);
        N.scale(scale);
        B.scale(scale);
        T.add(o);
        N.add(o);
        B.add(o);
        
//        gl.glPointSize(7);
//        gl.glBegin(GL2.GL_POINTS);
//        gl.glVertex3d(o.x, o.y, o.z);
//        gl.glEnd();
        
        gl.glPushMatrix();
        
        gl.glLineWidth(2.0f);
        gl.glBegin(GL2.GL_LINES);
        
        // T
        gl.glColor3f(1, 0.5f, 0.5f);
        gl.glVertex3d(o.x, o.y, o.z);
        gl.glVertex3d(T.x, T.y, T.z);
        
        // N
        gl.glColor3f(0.5f, 1, 0.5f);
        gl.glVertex3d(o.x, o.y, o.z);
        gl.glVertex3d(N.x, N.y, N.z);
//        System.out.println(N);

        // B
        gl.glColor3f(0.5f, 0.5f, 1);
        gl.glVertex3d(o.x, o.y, o.z);
        gl.glVertex3d(B.x, B.y, B.z);

        gl.glEnd();
        
        gl.glPopMatrix();

        if (dtext) {
            gl.glColor3f(1, 0, 0);
            gl.glRasterPos3d(T.x, T.y, T.z); 
            JoglTextRenderer.print3dTextLines("T", new Point3d(T.x, T.y, T.z));

            gl.glColor3f(0, 1, 0);
            JoglTextRenderer.print3dTextLines("N", new Point3d(N.x, N.y, N.z));

            gl.glColor3f(0, 0, 1);
            JoglTextRenderer.print3dTextLines("B", new Point3d(B.x, B.y, B.z));
        }
        
        gl.glEnable(GL2.GL_LIGHTING);

    }
    
}
