package tools.frame;

import gl.geometry.WorldAxis;
import gl.math.FlatMatrix4d;

import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;

public class OrientationFrame {

	private static final double EPSILON = 1e-8;

	public enum SamplingStyle {
//		NEG_NEG_NEG(AxisSampling.NEG, AxisSampling.NEG, AxisSampling.NEG),
//		NEG_NEG_ZERO(AxisSampling.NEG, AxisSampling.NEG, AxisSampling.ZERO),
//		NEG_NEG_POS(AxisSampling.NEG, AxisSampling.NEG, AxisSampling.POS),
//		
//		NEG_ZERO_NEG(AxisSampling.NEG, AxisSampling.ZERO, AxisSampling.NEG),
//		NEG_ZERO_ZERO(AxisSampling.NEG, AxisSampling.ZERO, AxisSampling.ZERO),
//		NEG_ZERO_POS(AxisSampling.NEG, AxisSampling.ZERO, AxisSampling.POS),
//
//		NEG_POS_NEG(AxisSampling.NEG, AxisSampling.POS, AxisSampling.NEG),
//		NEG_POS_ZERO(AxisSampling.NEG, AxisSampling.POS, AxisSampling.ZERO),
//		NEG_POS_POS(AxisSampling.NEG, AxisSampling.POS, AxisSampling.POS);
		ZERO(AxisSampling.ZERO, AxisSampling.ZERO, AxisSampling.ZERO),
		P_Z_Z(AxisSampling.POS, AxisSampling.ZERO, AxisSampling.ZERO),
		NP_Z_Z(AxisSampling.NEG_POS, AxisSampling.ZERO, AxisSampling.ZERO),
		P_NP_Z(AxisSampling.POS, AxisSampling.NEG_POS, AxisSampling.ZERO),
		Z_NP_Z(AxisSampling.ZERO, AxisSampling.NEG_POS, AxisSampling.ZERO),
		P_NP_NP(AxisSampling.POS, AxisSampling.NEG_POS, AxisSampling.NEG_POS), 
		N_NP_NP(AxisSampling.NEG, AxisSampling.NEG_POS, AxisSampling.NEG_POS), 
		NP_NP_NP(AxisSampling.NEG_POS, AxisSampling.NEG_POS, AxisSampling.NEG_POS),
		NP_Z_NP(AxisSampling.NEG_POS, AxisSampling.ZERO, AxisSampling.NEG_POS),
		NP_NP_Z(AxisSampling.NEG_POS, AxisSampling.NEG_POS, AxisSampling.ZERO),
		Z_NP_NP(AxisSampling.ZERO, AxisSampling.NEG_POS, AxisSampling.NEG_POS),
		Z_Z_NP(AxisSampling.ZERO, AxisSampling.ZERO, AxisSampling.NEG_POS);
		
		private SamplingStyle(AxisSampling x, AxisSampling y, AxisSampling z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		/**
		 * @param axisScales
		 * @param samplingStyle
		 * @return axis bounds based on the scale of the axis, and of its sampling style
		 */
		public static double[][] getSamplingBounds(double[] axisScales, SamplingStyle samplingStyle) {
			double[] extei = new double[] { samplingStyle.x.getMin() * axisScales[0], samplingStyle.x.getMax() * axisScales[0] };
			double[] extej = new double[] { samplingStyle.y.getMin() * axisScales[1], samplingStyle.y.getMax() * axisScales[1] };
			double[] extek = new double[] { samplingStyle.z.getMin() * axisScales[2], samplingStyle.z.getMax() * axisScales[2] };
			double[][] extes = new double[][] { extei, extej, extek };

			return extes;
		}
		
		public int[][] getSamplingBounds(int n) {
			// Fetch bounds
			double[][] bounds = getSamplingBounds(new double[] { n, n, n}, this);
			
			// Convert to integer
			int[][] ibounds = new int[3][2];
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 2; j++)
					ibounds[i][j] = MathToolset.roundInt(bounds[i][j]);
			
			return ibounds;
		}


		public int[] getSamplingDimension(int n) {
			int[] extei = new int[] { x.getMin() * n, x.getMax() * n };
			int[] extej = new int[] { y.getMin() * n, y.getMax() * n };
			int[] extek = new int[] { z.getMin() * n, z.getMax() * n };
			return new int[] { extei[1] - extei[0], extej[1] - extej[0], extek[1] - extek[0] };
		}

		public AxisSampling x, y, z;
		
		/**
		 * @param i
		 * @return the corresponding axis: x=1, y=2, z=3
		 */
		public AxisSampling get(int i) {
			
			if (i == 0) return x;
			else if (i == 1) return y;
			else if (i == 2) return z;
			else return z;
		}

		/**
		 * @param i
		 * @return the magnitude for this axis where x=1, y=2, z=3
		 */
		public double getMagnitude(int i) {
			return get(i).getMagnitude();
		}

		public void set(AxisSampling x, AxisSampling y, AxisSampling z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		public static SamplingStyle make(AxisSampling x, AxisSampling y, AxisSampling z) {
    		SamplingStyle samplingStyle = SamplingStyle.ZERO;
    		samplingStyle.set(x, y, z);
    		
    		return samplingStyle;

		}
		
		private EnumComboBox<AxisSampling> sx;
		private EnumComboBox<AxisSampling> sy;
		private EnumComboBox<AxisSampling> sz;
		
		private VerticalFlowPanel vfp = null;
		
		public JPanel getControls() {
			if (vfp == null) {
				sx = new EnumComboBox<AxisSampling>(
						"x style", x);
				sy = new EnumComboBox<AxisSampling>(
						"y style", y);
				sz = new EnumComboBox<AxisSampling>(
						"z style", z);
				
				ParameterListener l = new ParameterListener() {
					
					@Override
					public void parameterChanged(Parameter parameter) {
						x = sx.getSelected();
						y = sy.getSelected();
						z = sz.getSelected();
					}
				};
				
				sx.addParameterListener(l);
				sy.addParameterListener(l);
				sz.addParameterListener(l);

				vfp = new VerticalFlowPanel();
				vfp.add(sx.getControls());
				vfp.add(sy.getControls());
				vfp.add(sz.getControls());
			}
			
			return vfp.getPanel();

		}
		
		public void addParameterListener(ParameterListener listener) {
			getControls();
			
			sx.addParameterListener(listener);
			sy.addParameterListener(listener);
			sz.addParameterListener(listener);
		}
		
		@Override
		public String toString() {
			return x.toString() + ", " + y.toString() + ", " + z.toString();
		}

		public static int getCount(SamplingStyle style, int samples) {
			return VolumeSampler.generateCanonicalSampleInteger(new int[] { samples, samples, samples }, style).size();
		}
	}
	
	public enum AxisSampling {
		/**
		 * Only zero
		 */
		ZERO(0, 0),
		/**
		 * Negative, including 0
		 */
		NEG(-1, 0),
		/**
		 * Positive, including 0
		 */
		POS(0, 1),
		/**
		 * Negative and positive
		 */
		NEG_POS(-1, 1);
		
		private int min, max;
		
		public int getMin() {
			return min;
		}
		public int getMax() {
			return max;
		}
		
		private AxisSampling(int min, int max) {
			this.min = min;
			this.max = max;
		}
		public int getMagnitude() {
			return max - min;
		}
	};
	
	public enum FrameAxis { 
		X(1), Y(2), Z(3);
		
		private final int[] axisArray;
		private final Vector3d axis;
		private final int index;

		private FrameAxis(int e) {
			this.index = e;
			
			switch (index) {
			case (1):
				axisArray = new int[] { 1, 0, 0 };
				break;
			case (2):
				axisArray = new int[] { 0, 1, 0 };
				break;
			case (3):
				axisArray = new int[] { 0, 0, 1 };
				break;
				default:
					axisArray = new int[] { 1, 1, 1 };
			}
			
			axis = new Vector3d(axisArray[0], axisArray[1], axisArray[2]);
		}
		
		public final Vector3d getAxis() {
			return axis;
		}
		
		public final int[] getAxisArray() {
			return axisArray;
		}

		public int get(int[] v) {
			return v[index - 1];
		}
		
		public int getIndex() {
			return index - 1;
		}

		public int[] getNot(int[] v) {
			int[] ninds = getOrthogonalIndices();
			return new int[] { v[ninds[0]], v[ninds[1]] };
		}

		public int[] getOrthogonalIndices() {
			if (index == 1)
				return new int[] { 1, 2 };
			else if (index == 2)
				return new int[] { 0, 2 };
			else if (index == 3)
				return new int[] { 0, 1 };
			else return null;
		}
		
		@Override
		public String toString() {
			String[] s = new String[] { "x", "y", "z" };
			return s[getIndex()];
		}
	
	};

	protected Vector3d e1;
	protected Vector3d e2;
	protected Vector3d e3;
	protected Matrix4d frame;
	protected FlatMatrix4d flatFrame;

	/**
	 * Angle in radians
	 * @param axis
	 * @param theta
	 * @return
	 */
	public static OrientationFrame GetRotationMatrix(FrameAxis axis, double theta) {
		Vector3d e1 = new Vector3d(), e2 = new Vector3d(), e3 = new Vector3d();
		switch (axis) {
		case X:
			e1 = new Vector3d(1, 0, 0);
			e2 = new Vector3d(0, Math.cos(theta), -Math.sin(theta));
			e3 = new Vector3d(0, Math.sin(theta), Math.cos(theta));
			break;
		case Y:
			e1 = new Vector3d(Math.cos(theta), 0, -Math.sin(theta));
			e2 = new Vector3d(0, 1, 0);
			e3 = new Vector3d(Math.sin(theta), 0 , Math.cos(theta));
			break;
		case Z:
			e1 = new Vector3d(Math.cos(theta), -Math.sin(theta), 0);
			e2 = new Vector3d(Math.sin(theta), Math.cos(theta), 0);
			e3 = new Vector3d(0, 0, 1);
			break;
		}
		
		OrientationFrame frame = new OrientationFrame(e1, e2, e3);
		return frame;
	}

	/**
	 * Construct an orientation frame using this vector as part of the canonical basis. Two other arbitrary are found automatically, and consistently.
	 * @param e1
	 */
	public OrientationFrame(Vector3d e1) {
		this(OrientationFrame.frameFromVector(e1));
	}

	/**
	 * Construct an orientation frame using three vectors as the canonical basis.
	 * @param e1
	 * @param e2
	 * @param e3
	 */
	public OrientationFrame(Vector3d e1, Vector3d e2, Vector3d e3) {
		this.e1 = new Vector3d(e1);
		this.e2 = new Vector3d(e2);
		this.e3 = new Vector3d(e3);

		this.e1.normalize();
		this.e2.normalize();
		this.e3.normalize();
				
		buildFrame();
	}
	
	/**
	 * Construct an orientation frame as a deep copy of another orientation frame.
	 * @param other
	 */
	public OrientationFrame(OrientationFrame other) {
		this(other.e1, other.e2, other.e3);
	}

	/**
	 * Construct an orientation frame using the natural basis x<code>(1, 0, 0)</code>, y=<code>(0, 1, 0)</code>, z=<code>(0, 0, 1)</code>.
	 */
	public OrientationFrame() {
		this(FrameAxis.X.getAxis(), FrameAxis.Y.getAxis(), FrameAxis.Z.getAxis());
	}
	
	public OrientationFrame(Matrix4d m) {
		set(m);
	}
	
	public OrientationFrame(Vector3d[] v) {
		this(v[0], v[1], v[2]);
	}

	public void set(Matrix4d m) {
		double[] dv = new double[4];
		
		m.getColumn(0, dv);
		e1 = new Vector3d(dv[0], dv[1], dv[2]);
		e1.normalize();
		
		m.getColumn(1, dv);
		e2 = new Vector3d(dv[0], dv[1], dv[2]);
		e2.normalize();
		
		m.getColumn(2, dv);
		e3 = new Vector3d(dv[0], dv[1], dv[2]);
		e3.normalize();
		
		buildFrame();
	}

	private void set(OrientationFrame other) {
		e1 = new Vector3d(other.e1);
		e2 = new Vector3d(other.e2);
		e3 = new Vector3d(other.e3);
		
		buildFrame();
	}

	public OrientationFrame randomize() {
    	set(frameFromVector(MathToolset.randomDirection()));
    	return this;
	}
	

	/**
	 * Flip e1
	 * @return
	 */
	public OrientationFrame e1Flip() {
		e1.scale(-1);
		e3.scale(-1);
		buildFrame();
		return this;
	}

	protected void buildFrame() {
		frame = OrientationFrame.buildFrame(e1, e2, e3);
		updateFlatFrame();
	}
	
	protected void updateFlatFrame() {
		if (flatFrame == null) {
			flatFrame = new FlatMatrix4d(getFrame());
		}
//		flatFrame = new FlatMatrix4d(getFrame());
		flatFrame.setBackingMatrix(getFrame());
	}

	public static Matrix4d buildFrame(Vector3d e1, Vector3d e2, Vector3d e3) {
		Matrix4d frame = new Matrix4d();

		frame.setColumn(0, e1.x, e1.y, e1.z, 0);
		frame.setColumn(1, e2.x, e2.y, e2.z, 0);
		frame.setColumn(2, e3.x, e3.y, e3.z, 0);
		frame.setColumn(3, 0, 0, 0, 1);

		return frame;
	}
	
	public Matrix4d getFrame() {
		return frame;
	}
	
	/**
	 * Transform a point to this orientation frame
	 * @param p
	 * @return the transformed point
	 */
	public Point3d transform(Point3d p) {
		frame.transform(p);
		return p;
	}
	
	
	/**
	 * Transform a vector to this orientation frame.
	 * @param v
	 */
	public Vector3d transform(Vector3d v) {
		frame.transform(v);
		return v;
	}


	/**
	 * Premultiply this frame by a matrix <code>m</code>: this = m * this
	 * @param m
	 * @return
	 */
	public OrientationFrame premultiply(Matrix4d m) {
		m.transform(e1);
		m.transform(e2);
		m.transform(e3);
		
		buildFrame();
		
		return this;
	}

	/**
	 * Premultiply this frame by a matrix <code>m</code>: this = m * this
	 * @param m
	 * @return
	 */
	public OrientationFrame premultiply(OrientationFrame f) {
		f.getFrame().transform(e1);
		f.getFrame().transform(e2);
		f.getFrame().transform(e3);
		
		buildFrame();
		
		return this;
	}

	private double[][] rows = new double[3][4];

	public void postmultiply(Matrix4d rotframe) {
		frame.mul(frame, rotframe);
		frame.getColumn(0, rows[0]);
		frame.getColumn(1, rows[1]);
		frame.getColumn(2, rows[2]);
		e1.set(rows[0]);
		e2.set(rows[1]);
		e3.set(rows[2]);
		
		updateFlatFrame();
	}
	
	/**
	 * Find two vectors perpendicular this this one.
	 * @param v an arbitrary vector which forms e1
	 * @return an orientation having <code>v</code> as e1 and two arbitrary, perpendicular vectors as <code>e2,e3</code>
	 */
	public static OrientationFrame frameFromVector(Vector3d v) {
		Vector3d e1 = new Vector3d(v);
		Vector3d e2 = new Vector3d();
		Vector3d e3 = new Vector3d();
		
		if (v.length() < EPSILON)
			throw new RuntimeException(
					"Error while computing the linear span of an orientation frame: e1 must be non-zero");

		// In order to determine e2, find the smallest component of v (in magnitude) and set it to a non-zero value
		e2.set(e1);
		e2.absolute();

		// This set of tests produces a right handed coordinate system
		// in the canonical basis X, Y, Z if v = (1, 0, 0)
		
		// x = y = z
		if (e2.x == e2.y && e2.y == e2.z)
			e2.x = 0;
		// x <= y | z
		else if (e2.x <= e2.y && e2.x <= e2.z)
			e2.x = Math.max(e2.y, e2.z);
		// y <= x | z
		else if (e2.y <= e2.x && e2.y <= e2.z)
			e2.y = Math.max(e2.x, e2.z);
		// z, x | y
		else if (e2.z <= e2.x && e2.z <= e2.y)
			e2.z = Math.max(e2.x, e2.y);

		// e2 is then found by cross product of this vector with v
		e2.normalize();
		e3.cross(e1, e2);

		// e3 = e1 x e2
		e2.cross(e3, e1);
		
		e1.normalize();
		e2.normalize();
		e3.normalize();
		
		return new OrientationFrame(e1, e2, e3);
	}

	public void display(GLAutoDrawable drawable) {
		display(drawable, 1.0, false);
	}
	
	public void display(GLAutoDrawable drawable, double scale, boolean drawMore) {
		GL2 gl = drawable.getGL().getGL2();
	
		gl.glPushMatrix();
		gl.glMultMatrixd(flatFrame.asArray(), 0);
		WorldAxis.display(gl, scale, drawMore, drawMore);
		gl.glPopMatrix();
	}


	public void setAxis(FrameAxis frameAxis, Vector3d v) {
		switch (frameAxis) {
		case X:
			e1 = v;;
		case Y:
			e2 = v;
		case Z:
			e3 = v;
		}
	}

	public Vector3d getAxis(FrameAxis frameAxis) {
		switch (frameAxis) {
		case X:
			return e1;
		case Y:
			return e2;
		case Z:
			return e3;
		}
		return null;
	}
	
	public void applyTransform(GL2 gl) {
		gl.glMultMatrixd(flatFrame.asArray(), 0);
	}

	public static void main(String[] args) {
		// Initialize matrix to identity and perturb
		OrientationFrame frame = new OrientationFrame();

		System.out.println(frame.getFrame());
		Random rand = new Random();
		double mag = 0.05;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				double mij = frame.getFrame().getElement(i, j);
				frame.getFrame().setElement(i, j, mij + mag * (-1 + 2 * rand.nextDouble()));
			}
		}
		System.out.println(frame.getFrame());
		MathToolset.orthogonalize(frame.getFrame());
		System.out.println(frame.getFrame());
	}

	public static OrientationFrame identity() {
		return new OrientationFrame(FrameAxis.X.axis, FrameAxis.Y.axis, FrameAxis.Z.axis);
	}

	/**
	 * Update the precompute frame vectors based on the back Matrix4d.
	 */
	public void updateFrameVectors() {
		set(getFrame());
	}
	
	@Override
	public String toString() {
		return flatFrame.toString();
	}
}
