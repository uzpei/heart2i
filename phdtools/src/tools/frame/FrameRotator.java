package tools.frame;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;

import tools.frame.OrientationFrame.FrameAxis;



/**
 * Class describing a coordinate frame along with rotated frames passing through it.
 * @author epiuze
 *
 */
public class FrameRotator {

	/**
	 * Rotate a frame <code>n</code> times about its x-axis in a range of PI.
	 * @param frame the coordinate frame to rotate
	 * @param n the number of rotation increments
	 * @return a list of rotated coordinate frames
	 */
	public static List<OrientationFrame> rotate(OrientationFrame frame, int n) {
		return rotate(frame, FrameAxis.X, Math.PI, n);
	}

	public static List<OrientationFrame> rotateAndFlipE1(OrientationFrame frame, int n) {
		return rotateAndFlipE1(frame, FrameAxis.X, Math.PI, n);
	}

	/**
	 * Rotate a frame <code>n</code> times about its <code>frameAxis</code>-axis in a range of <code>angle</code>.
	 * @param frame the orientation frame to rotate
	 * @param frameAxis the axis or rotation
	 * @param angle the total angle of rotation
	 * @param n the number of rotation increments
	 * @return a list of rotated coordinate frames
	 */
	public static List<OrientationFrame> rotate(OrientationFrame frame, FrameAxis frameAxis, double angle, int n) {
		if (n <= 0)
			n = 1;
			
		List<OrientationFrame> frames = new ArrayList<OrientationFrame>(n);
		
		Matrix4d rotation = new Matrix4d();
		rotation.setIdentity();
		for (int i = 1; i <= n; i++) {
			rotation.setRotation(new AxisAngle4d(frame.getAxis(frameAxis), -i * (angle / n)));
			frames.add(new OrientationFrame(frame).premultiply(rotation));

		}
		
		return frames;
	}

	/**
	 * Create <code>n</code> rotated versions of <code>frame</code>
	 * about <code>frameAxis</code> and then creates one additional frame per
	 * rotation with a flip in the e1 vector (right handed coordinates are
	 * preserved by also flipping e3). The list thus first contains n frames
	 * (rotated versions) followed by <code>n</code> flipped versions. For frame index i, corresponding rotations /
	 * flips are (i, n + i).
	 * 
	 * @param frame
	 * @param frameAxis
	 * @param angle
	 * @param n
	 * @return
	 */
	public static List<OrientationFrame> rotateAndFlipE1(
			OrientationFrame frame, FrameAxis frameAxis, double angle, int n) {
		List<OrientationFrame> frames = FrameRotator.rotate(frame, frameAxis,
				angle, n);

		List<OrientationFrame> moreFrames = new LinkedList<OrientationFrame>();
		for (OrientationFrame of : frames) {
			moreFrames.add(new OrientationFrame(of).e1Flip());
		}

		frames.addAll(moreFrames);

		return frames;

	}

}
