package tools.frame;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import tools.frame.OrientationFrame.AxisSampling;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;

public class FrameRotatorTest implements GLObject, Interactor {

	private IntParameter dsamples = new IntParameter("frame samples", 3, 0, 100);
	private IntParameter rots = new IntParameter("frame rotations", 4, 0, 20);
	private BooleanParameter bint = new BooleanParameter("integer", false);
	
    public static void main(String[] args) {
        new FrameRotatorTest();
    }
    
    private JoglRenderer ev;
    
    public FrameRotatorTest() {
    	initViewer();
    }
    
    private void initViewer() {
    	Vector3d v = new Vector3d(1, 0, 0);
    	frame = new OrientationFrame(v);
    	
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("",
                this, new Dimension(winsize), new Dimension(680,
                        winsize.height + 90), true);
        
        ev.addInteractor(this);
        
        ev.start();
        
//        ev.getRoom().setVisible(false);
        
        ev.getCamera().zoom(500f);
    }
    
    private OrientationFrame frame;
    private SamplingStyle samplingStyle = SamplingStyle.make(AxisSampling.POS, AxisSampling.NEG_POS, AxisSampling.ZERO);
    
    @Override
    public void display(GLAutoDrawable drawable) {
        
		GL2 gl = drawable.getGL().getGL2();

//        WorldAxis.display(gl);
        
        // Test frame rotation
        int nrot = rots.getValue();
        List<OrientationFrame> frames;
        if (nrot > 0) {
            frames = FrameRotator.rotateAndFlipE1(frame, FrameAxis.X, Math.PI, nrot);
        }
        else {
        	frames = new LinkedList<OrientationFrame>();
        	frames.add(frame);
        }
//        List<OrientationFrame> frames = FrameRotator.rotate(frame, FrameAxis.X, Math.PI, nrot);
        
    	double[][] cs = new double[][] { 
    			{ 1, 0, 0 },
    			{ 0, 1, 0 }, 
    			{ 0, 0, 1 },
    			{ 1, 1, 0 },
    			{ 1, 0, 1 }, 
    			{ 1, 1, 1 }, 
    			{ 0, 1, 1 } 
    			};
        
    	int fi = 0;
    	int pc = 0;
        for (OrientationFrame f : frames) {
        	f.display(drawable);
        	
            // Test frame sampling
        	int nsamples = dsamples.getValue();
        	boolean integer = bint.getValue();
        	
        	double as = 1;
    		double[] axisScales = new double[] { as, as, as };
    		int[] axisSamples = new int[] { nsamples, nsamples, nsamples};
    		
//    		gl.glColor3d(0, 1, 1);
    		gl.glColor3d(cs[fi][0], cs[fi][1], cs[fi][2]);
    		gl.glPointSize(10.0f);
    		gl.glPushMatrix();
    		
    		if (integer) {
    			double scale = 2 * as / ((nsamples % 2 == 0 ? nsamples : nsamples - 1));
    			if (nsamples == 1) 
    				scale = 1;
    			
        		gl.glScaled(scale, scale, scale);
        		gl.glBegin(GL2.GL_POINTS);
        		for (Point3i p : VolumeSampler.sampleInteger(f, axisSamples, samplingStyle)) {
        			gl.glVertex3d(p.x, p.y, p.z);
        			pc++;
        		}
        		gl.glEnd();
    		}
    		else {
        		gl.glBegin(GL2.GL_POINTS);
        		for (Point3d p : VolumeSampler.sample(f, axisSamples,
        				axisScales, samplingStyle)) {
        			gl.glVertex3d(p.x, p.y, p.z);
        			pc++;
        		}
        		gl.glEnd();
    		}
    		
    		gl.glPopMatrix();
    		fi = (fi + 1) % cs.length;
        }
        
        ev.getTextRenderer().drawTopLeft("count = " + pc + " (" +rots.getValue() + " rots x " + dsamples.getValue() + " x " + + dsamples.getValue() +")", drawable);
    }

    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        
        vfp.add(dsamples.getSliderControls());
        vfp.add(rots.getSliderControls());
        vfp.add(samplingStyle.getControls());
        
        vfp.add(bint.getControls());
        
        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }

	@Override
	public void attach(Component component) {

        component.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
            	if (e.getKeyCode() == KeyEvent.VK_F) {
            		frame.randomize();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
	}

	@Override
	public void reload(GLViewerConfiguration config) {
			// TODO Auto-generated method stub
	}
}
