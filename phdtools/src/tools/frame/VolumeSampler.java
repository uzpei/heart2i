package tools.frame;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.ParameterListener;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.MathToolset;


public class VolumeSampler {

	private IntDiscreteParameter samples;

	private EnumComboBox<SamplingStyle> style = new EnumComboBox<SamplingStyle>(SamplingStyle.NP_NP_NP);
	
	public VolumeSampler(int samples, SamplingStyle style) {
		int maxSamples = 1000;
		int[] sdata = new int[maxSamples];
		for (int i = 0; i < maxSamples; i++) {
			sdata[i] = 2 * (i) + 1;
		}
		
		this.samples = new IntDiscreteParameter("num samples", 1, sdata);
		
		this.samples.setValue(samples);
		this.style.setSelected(style);
	}
	
	public void addParameterListener(ParameterListener pl) {
		samples.addParameterListener(pl);
		style.addParameterListener(pl);
	}

	public List<Point3d> getSamples(CoordinateFrame frame, boolean includeOrigin) {
		return VolumeSampler.sample(frame, samples.getValue(), 1, style.getSelected(), !includeOrigin);
	}

	public List<Point3i> getIntegerSamples(CoordinateFrame frame) {
		return VolumeSampler.sampleInteger(frame, samples.getValue(), style.getSelected());
	}
	
	/**
	 * Make sure samples are even, adjust if necessary
	 * @param samples
	 * @param samplingStyle 
	 * @return
	 */
	private static int[] getAdjustedSamples(int[] samples, SamplingStyle samplingStyle) {
		int[] nsamples = new int[3];
		
		for (int i = 0; i < 3; i++) {
			if (samples[i] % 2 != 0) nsamples[i] = samples[i] - 1;
			else nsamples[i] = samples[i];
			
			if (samples[i] == 0) nsamples[i] = 1;
			
			nsamples[i] *= Math.signum(samplingStyle.getMagnitude(i));
		}
		
		return nsamples;
	}
	
	private static double[] getSamplingIncrements(double[][] maxima, int[] axisSamples) {
		double[] incs = new double[3];
		for (int i = 0; i <= 2; i++)
			incs[i] = axisSamples[i] > 0 ? Math.abs(maxima[i][1] - maxima[i][0]) / (axisSamples[i]) : 0; 
		
		return incs;
	}

	public static List<Point3d> sample(OrientationFrame frame, int axisSamples, double axisScales, SamplingStyle samplingStyle, boolean excludeOrigin) {
		return VolumeSampler.sample(frame, new int[] { axisSamples, axisSamples, axisSamples }, new double[] { axisScales, axisScales, axisScales}, samplingStyle, excludeOrigin);
	}
	/**
	 * Sample the volume spanned by this coordinate frame. Note that the <code>axisSamples</code> must be odd numbers for the sampling to fall on the origin
	 * @param axisSamples
	 * @param axisScales
	 * @param samplingStyle
	 * @return
	 */
	public static List<Point3d> sample(OrientationFrame frame, int[] axisSamples, double[] axisScales, SamplingStyle samplingStyle, boolean excludeOrigin) {
		List<Point3d> sampling = new LinkedList<Point3d>();
		
		// Volume bounds
		double[][] extes = SamplingStyle.getSamplingBounds(axisScales, samplingStyle);
		
		// Increments
		int[] samples = getAdjustedSamples(axisSamples, samplingStyle);
		double[] incs = getSamplingIncrements(extes, samples);
		
		Point3d shift = new Point3d(-incs[0] * (samples[0]) / 2, -incs[1] * (samples[1]) / 2, -incs[2] * (samples[2]) / 2);
		
		// LE for the origin
		Point3d pzero = new Point3d();
		for (int i = 0; i <= samples[0]; i++) {
			for (int j = 0; j <= samples[1]; j++) {
				for (int k = 0; k <= samples[2]; k++) {
					Point3d p = new Point3d();
					p.scaleAdd(i * incs[0] + shift.x + (extes[0][0] + extes[0][1]) / 2, frame.getAxis(FrameAxis.X), p);
					p.scaleAdd(j * incs[1] + shift.y + (extes[1][0] + extes[1][1]) / 2, frame.getAxis(FrameAxis.Y), p);
					p.scaleAdd(k * incs[2] + shift.z + (extes[2][0] + extes[2][1]) / 2, frame.getAxis(FrameAxis.Z), p);

					// Omit origin if necessary
					if (excludeOrigin && p.distanceSquared(pzero) <= 1e-8)
						continue;
					
					sampling.add(p);
				}		
			}
		}
		
		return sampling;
	}

	public static List<Point3i> sampleInteger(OrientationFrame frame, int axisSamples, SamplingStyle samplingStyle) {
		return sampleInteger(frame, new int[] { axisSamples, axisSamples, axisSamples }, samplingStyle, false);
	}

	public static List<Point3i> sampleInteger(OrientationFrame frame, int axisSamples, SamplingStyle samplingStyle, boolean excludeOrigin) {
		return sampleInteger(frame, new int[] { axisSamples, axisSamples, axisSamples }, samplingStyle, excludeOrigin);
	}
	
	public static List<Point3i> sample(NeighborhoodShape neighborhoodShape, int neighborhoodSize) {
		List<Point3i> neighborhood = null;
		switch (neighborhoodShape) {
		case Isotropic:
			neighborhood = VolumeSampler.sampleInteger(new CoordinateFrame(), neighborhoodSize, SamplingStyle.NP_NP_NP, true);
			break;
		case IsotropicXY:
			neighborhood = VolumeSampler.sampleInteger(new CoordinateFrame(), neighborhoodSize, SamplingStyle.NP_NP_Z, true);
			break;
		case IsotropicXZ:
			neighborhood = VolumeSampler.sampleInteger(new CoordinateFrame(), neighborhoodSize, SamplingStyle.NP_Z_NP, true);
			break;
		case IsotropicYZ:
			neighborhood = VolumeSampler.sampleInteger(new CoordinateFrame(), neighborhoodSize, SamplingStyle.Z_NP_NP, true);
			break;
		case HemisphericalForward:
			neighborhood = VolumeSampler.sampleInteger(new CoordinateFrame(),
					neighborhoodSize, SamplingStyle.P_NP_NP, true);
			break;
		case HemisphericalBackwards:
			neighborhood = VolumeSampler.sampleInteger(new CoordinateFrame(),
					neighborhoodSize, SamplingStyle.N_NP_NP, true);
			break;
		case SixNeighbors:
			neighborhood = new LinkedList<Point3i>();
			for (FrameAxis axis : Arrays.asList(FrameAxis.X, FrameAxis.Y, FrameAxis.Z)) {
				for (int direction : Arrays.asList(-1, 1)) {
					Point3i p = new Point3i(axis.getAxisArray());
					p.scale(direction);
					neighborhood.add(p);
				}
			}
			break;
		}
		
		return neighborhood;
	}

	public static List<Point3i> sampleInteger(OrientationFrame frame, int[] axisSamples, SamplingStyle samplingStyle, boolean excludeOrigin) {
		
		int[] samples = getAdjustedSamples(axisSamples, samplingStyle);
		double[] axisScales = new double[] { samples[0] / 2, samples[1] / 2, samples[2] / 2};

		List<Point3d> sampling = sample(frame, axisSamples, axisScales, samplingStyle, excludeOrigin);
		
		// Convert to List<Point3i> and return;
		List<Point3i> pts = MathToolset.tuple3dListTo3i(sampling);
		
		return pts;
	}
	
	public static List<Point3i> generateCanonicalSampleInteger(int[] axisSamples, SamplingStyle samplingStyle) {
		OrientationFrame of = new OrientationFrame(FrameAxis.X.getAxis(), FrameAxis.Y.getAxis(), FrameAxis.Z.getAxis());
		return sampleInteger(of, axisSamples, samplingStyle, false);
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Frame Sampling");
		vfp.add(samples.getSliderControls());
		vfp.add(style.getControls());
		return vfp.getPanel();
	}

}
