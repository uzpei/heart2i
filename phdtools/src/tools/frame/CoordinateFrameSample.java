package tools.frame;

import java.util.List;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

public class CoordinateFrameSample extends CoordinateFrame {

	private List<Point3d> dsample;
	
	private List<Point3i> isample;
	
	private int numsamples;
	
	public CoordinateFrameSample(Vector3d v, Point3d p) {
		super(v, p);
	}

	public CoordinateFrameSample() {
		super();
	}

	public CoordinateFrameSample(OrientationFrame orientationFrame, Point3i pt) {
		super(orientationFrame, pt);
	}

	public CoordinateFrameSample(OrientationFrame orientationFrame, Point3d pt) {
		super(orientationFrame, pt);
	}

	public CoordinateFrameSample(CoordinateFrame coordinateFrame) {
		super(coordinateFrame);
	}
	
	public CoordinateFrameSample(Matrix4d frame) {
		super(frame);
	}

	public List<Point3d> presampleDouble(int[] axisSamples, double[] axisScales, SamplingStyle samplingStyle) {
		dsample = VolumeSampler.sample(this, axisSamples, axisScales, samplingStyle, false);
		return dsample;
	}
	
	private final static CoordinateFrame naturalFrame = new CoordinateFrame();
	
	public List<Point3i> presampleInteger(int[] axisSamples, SamplingStyle samplingStyle, boolean useNaturalFrameSampling) {
		if (useNaturalFrameSampling) {
			isample = VolumeSampler.sampleInteger(naturalFrame, axisSamples, samplingStyle, false);
		}
		else {
			isample = VolumeSampler.sampleInteger(this, axisSamples, samplingStyle, false);
			throw new RuntimeException("This is broken! Each axis is length one and is sampled in integer space... Doesn't make sense!");
		}
		
		return isample;
	}

	public List<Point3i> presampleInteger(int axisSamples, SamplingStyle samplingStyle, boolean useNaturalFrameSampling) {
		return presampleInteger(new int[] { axisSamples, axisSamples, axisSamples }, samplingStyle, useNaturalFrameSampling);
	}

	public List<Point3d> getDoubleSampling() {
		return dsample;
	}
	
	public List<Point3i> getIntegerSampling() {
		return isample;
	}

	public boolean checkOrthogonality() {
		double v = frame.determinant();
		System.out.println(v);
		
	    return v == 1 ? true : Math.abs(v - 1) < 1e-9;
//		if (e1.dot(e2) == 0 && e1.dot(e3) == 0 && e2.dot(e3) == 0) return true;
//		else return false;
	}
}
