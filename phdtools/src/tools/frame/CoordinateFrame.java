package tools.frame;

import gl.math.FlatMatrix4d;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 * An extension of an orientation frame with an origin.
 * @author epiuze
 *
 */
public class CoordinateFrame extends OrientationFrame {
	private Point3d origin = new Point3d();

	public CoordinateFrame(Vector3d e1, Vector3d e2, Vector3d e3, Point3d origin) {
		this(e1, e2, e3, origin.x, origin.y, origin.z);
	}
	
	public CoordinateFrame(Vector3d e1, Vector3d e2, Vector3d e3, double x, double y, double z) {
		super(e1, e2, e3);
		this.origin = new Point3d(x, y, z);
		
		buildCoordinateFrame();
	}

	public CoordinateFrame(Vector3d e1, Vector3d e2, Vector3d e3) {
		this(e1, e2, e3, new Point3d());
	}

	public CoordinateFrame(Vector3d e1, Vector3d e2, Vector3d e3, int x, int y,
			int z) {
		this(e1, e2, e3, new Point3d(x, y, z));
	}

	public CoordinateFrame(Vector3d v, Point3d p) {
		this(new OrientationFrame(v), p);
	}

	public CoordinateFrame(Vector3d v) {
		this(new OrientationFrame(v), new Point3d());
	}

	public CoordinateFrame(OrientationFrame frame, Point3d p) {
		this(frame.e1, frame.e2, frame.e3, p);
	}

	public CoordinateFrame(int x, int y, int z) {
		super();
		this.origin = new Point3d(x, y, z);
	}

	public CoordinateFrame() {
		this(new Vector3d(1, 0, 0), new Vector3d(0, 1, 0), new Vector3d(0, 0, 1), new Point3d());
	}

	public CoordinateFrame(OrientationFrame frame, Point3i pt) {
		this(frame, new Point3d(pt.x, pt.y, pt.z));
	}

	public CoordinateFrame(CoordinateFrame other) {
		this(other, other.origin);
	}

	public CoordinateFrame(Matrix4d m) {
		super(m);
		
		this.origin = new Point3d(m.getElement(0, 3), m.getElement(1, 3), m.getElement(2, 3));
		
		buildCoordinateFrame();
	}
	
	private double[] dv = new double[4];
	
	public void set(Matrix4d m) {
		
		m.getColumn(0, dv);
		e1.set(dv[0], dv[1], dv[2]);
		e1.normalize();
		
		m.getColumn(1, dv);
		e2.set(dv[0], dv[1], dv[2]);
		e2.normalize();
		
		m.getColumn(2, dv);
		e3.set(dv[0], dv[1], dv[2]);
		e3.normalize();

		origin = new Point3d(m.getElement(0, 3), m.getElement(1, 3), m.getElement(2, 3));

		frame.set(m);
		
		if (flatFrame == null)
			flatFrame = new FlatMatrix4d(frame);
		else
			flatFrame.setBackingMatrix(frame);
	}

	private void buildCoordinateFrame() {
		frame = CoordinateFrame.buildFrame(e1, e2, e3, origin);
		updateFlatFrame();
	}

	public static Matrix4d buildFrame(Vector3d e1, Vector3d e2, Vector3d e3, Point3d origin) {
		Matrix4d frame = OrientationFrame.buildFrame(e1, e2, e3);
		frame.setColumn(3, origin.x, origin.y, origin.z, 1);

		return frame;
	}

	public Point3d getTranslation() {
		return origin;
	}
	
	public Matrix4d getOrientation() {
		return OrientationFrame.buildFrame(e1, e2, e3);
	}
	
	@Override
	public CoordinateFrame premultiply(Matrix4d m) {
		super.premultiply(m);
		buildFrame();
		return this;
	}
	
	@Override
	public void display(GLAutoDrawable drawable, double scale, boolean drawMore) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glPushMatrix();
//		gl.glTranslated(origin.x, origin.y, origin.z);
		super.display(drawable, scale, drawMore);
		gl.glPopMatrix();
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		display(drawable, 1.0, false);
	}

	@Override
	public void applyTransform(GL2 gl) {
		gl.glMultMatrixd(flatFrame.asArray(), 0);
	}
	
	public Matrix4d getRotation(Matrix4d m) {
		m.set(getFrame());
		m.setColumn(3, 0, 0, 0, 1);
		
		return m;
	}

	public void setRotation(Matrix4d m) {
		m.setColumn(3, origin.x, origin.y, origin.z, 1);
		set(m);
	}

	public void setTranslation(Tuple3d p) {
		frame.setColumn(3, p.x, p.y, p.z, 1);
		set(frame);
	}

	/**
	 * Invert a matrix in-place;
	 * TODO: r and t3 are two dummy objects used in computations i.e. they should not be null.
	 * @param frame
	 */
	public static void invert(Matrix4d frame, Matrix3d r, Vector3d t3) {
			t3.set(frame.getElement(0, 3), frame.getElement(1, 3), frame.getElement(2, 3));
			
			// Transpose rotational part
			frame.getRotationScale(r);
			r.transpose();
			
			r.transform(t3);

			frame.set(r);
			frame.setColumn(3, -t3.x, -t3.y, -t3.z, 1);
	}

	public void translate(FrameAxis axis, Vector3d dv) {
		Vector3d v = getAxis(axis);
		v.add(dv);
		setAxis(axis, v);
		buildCoordinateFrame();
	}

	public void translate(Vector3d e1) {
		origin.add(e1);
		buildCoordinateFrame();
	}
	
	@Override
	public String toString() {
		return origin.toString() + ": " + e1.toString() + " | " + e2.toString() + " | " + e3.toString();
	}
}
