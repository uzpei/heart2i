package tools;

import java.awt.GraphicsEnvironment;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class TextToolset {
	public static final String space = "\u0020";
	public static final String tab = "\t";
	public static final String newLine = String.format("%n");
//	public static final String newLine = "\n";
	public static final String carriageReturn = "\r";
	public static final String formFeed = "\f";

	public static final char spaceChar = '\u0020';
	public static final char tabChar = '\t';
	public static final char newLineChar = '\n';
	public static final char carriageReturnChar = '\r';
	public static final char formFeedChar = '\f';


	/**
	 * Fill this string until it has length <code>length</code> by appending white spaces (\u0020).
	 * @param s
	 * @param length
	 * @return
	 */
	public static String fillSpace(String s, int length) {
		return fill(s, space, length);
	}
	
	/**
	 * Fill this string until it has length <code>length</code> by appending <code>filling</code>.
	 * @param s
	 * @param filling
	 * @param length
	 * @return
	 */
	public static String fill(String s, String filling, int length) {
		if (filling == "" || filling == null)
			filling = space;
		
		if (s.length() >= length) {
			return s;
		}

		while (s.length() < length)
			s += filling;

		
		return s;
	}

	public static String fill(String filling, int length) {
		return fill("", filling, length);
	}

	/**
	 * Find the longest field in a multiline string with a "field <code>separator</code> value" format.
	 * @param string
	 * @param separator
	 * @return the longest field in that multiline string.
	 */
	public static int getMaxFieldLengthFromMultilineString(String string, String separator) {
		StringTokenizer st = new StringTokenizer(string, "\n");
		List<String> text = new LinkedList<String>();
		while (st.hasMoreTokens()) {
			text.add(st.nextToken());
		}
		
		return getMaxFieldLengthFromMultilineString(text, separator);
	}

	/**
	 * Find the longest field in a multiline string with a "field <code>separator</code> value" format.
	 * @param string
	 * @param separator
	 * @return the longest field in that multiline string.
	 */
	public static int getMaxFieldLengthFromMultilineString(List<String> text, String separator) {
		int maxFieldLength = 0;
		String maxField = "";
		
		// Get the longest line
		for (String line : text) {
			// Parse the line using the separator
			StringTokenizer stsep = new StringTokenizer(line, separator);

			// Only parse lines that have the form: FIELD SEPARATOR VALUE
			if (stsep.countTokens() != 2)
				continue;

			// Parse the field and extract length
			maxField = stsep.nextToken();
			int fieldLength = maxField.length();

			if (fieldLength > maxFieldLength)
				maxFieldLength = fieldLength;
		}
		
		return maxFieldLength;
	}

	/**
	 * Find the longest line in a multiline string.
	 * @param string
	 * @param separator
	 * @return the number of characters in the longest line of that multiline string.
	 */
	public static int getMaxLineLengthFromMultilineString(List<String> string) {
		int maxLineLength = 0;
		
		// Find the longest line
		for (String s : string) {
			// Parse the field and extract length
			int length = s.length();

			if (length > maxLineLength)
				maxLineLength = length;
		}
		
		return maxLineLength;
	}

	/**
	 * Find the longest line in a multiline string.
	 * @param string
	 * @param separator
	 * @return the number of characters in the longest line of that multiline string.
	 */
	public static int getMaxLineLengthFromMultilineString(String string) {
		int maxLineLength = 0;
		
		// Find the longest line
		StringTokenizer st = new StringTokenizer(string, "\n");
		while (st.hasMoreTokens()) {
			// Parse the field and extract length
			int length = st.nextToken().length();

			if (length > maxLineLength)
				maxLineLength = length;
		}
		
		return maxLineLength;
	}

	/**
	 * Find the available system fonts.
	 * @return a set containing available system fonts.
	 */
	public static HashSet<String> getFonts() {
		HashSet<String> fontset = new HashSet<String>();

		GraphicsEnvironment gEnv = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		String envfonts[] = gEnv.getAvailableFontFamilyNames();
		for (int i = 1; i < envfonts.length; i++) {
			// System.out.println(envfonts[i]);
			fontset.add(envfonts[i]);
		}

		return fontset;
	}

	/**
	 * Count the number of lines in <code>string</code>.
	 * @param string
	 * @return the number of lines in <code>string</code>
	 */
	public static int countLines(String string) {
		StringTokenizer st = new StringTokenizer(string, "\n");
		int lineCount = 0;
		while (st.hasMoreTokens()) {
			st.nextToken();
			lineCount++;
		}

		return lineCount;
	}

	/**
	 * @param filling
	 * @param targetString
	 * @return a string filled with {@code filling} that has the length of {@code targetString}
	 */
	public static String fill(String filling, String targetString) {
		return fill("", filling, getMaxLineLengthFromMultilineString(targetString));
	}

	/**
	 * Fill {@code stringToFill} with {@code filling} until it reaches the length of {@code targetString}
	 * @param stringToFill
	 * @param filling
	 * @param targetString
	 * @return
	 */
	public static String fill(String stringToFill, String filling, String targetString) {
		return fill(stringToFill, filling, getMaxLineLengthFromMultilineString(targetString));
	}

	public static String fill(String stringToFill, String filling, List<String> targetString) {
		return fill(stringToFill, filling, getMaxLineLengthFromMultilineString(targetString));
	}

	public static String box(String s) {
		return box(s, '-');
	}
	
	/**
	 * Create a nicely formatted box around a string. 
	 * @param directory
	 * @return
	 */
	public static String box(String s, char filling) {
		// Spaces to the left and to the right
		int sideoff = 4 + 1;
		
		// Spaces up and down
		int vertoff = 1;
		
		int n = s.length();
		
//		String box = "\n";
		String box = "";
		
		String fullLength = fill("" + filling, n + 2 * sideoff);
		
		box +=  fullLength + "\n";
		
		for (int i = 0; i < vertoff; i++) {
			box += filling + fill(space, n - 2 + 2 * sideoff) + filling + "\n";
		}

		box += filling + fill(space, sideoff - 1) + s + fill(space, sideoff - 1) + filling + "\n";

		for (int i = 0; i < vertoff; i++) {
			box += filling + fill(space, n - 2 + 2 * sideoff) + filling + "\n";
		}

		box +=  fullLength + "\n";
		
		return box;
	}

	/**
	 * Remove the file extension (if any is found) from this string.
	 * Assumes the format [filename.extension]
	 * @param string
	 */
	public static String removeExtension(String filename) {
		// Find the last occurence of "."
		String extensionSeparator = ".";
		
		int index = filename.lastIndexOf(extensionSeparator);
		
		if (index <= 0) {
			return filename;
		}
		else {
			return filename.substring(0, index);
		}
	}

	/**
	 * @param i
	 * @return a character letter representation, where 0 = A, 1 = B, ..., Z = 26
	 */
	public static char asLetter(int i) {
		return (char) ('a' + i);
	}

	public static String join(double[] data, String separator) {
		List<Double> list = new LinkedList<Double>();
		for (double v : data)
			list.add(v);
		
		return join(list, separator);
	}
	
	public static <T> String join(List<T> data, String separator) {
		Iterator<T> iter = data.iterator();
		StringBuilder sb = new StringBuilder();
		if (iter.hasNext()) {
		  sb.append(iter.next());
		  while (iter.hasNext()) {
		    sb.append(separator).append(iter.next());
		  }
		}
		return sb.toString();
	}

	public static <T> String toString(List<T> data, String separator) {
		return join(data, separator);
	}

}
