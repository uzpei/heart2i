package tools;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Simple timer to monitor timings in blocks of code.
 * @author piuze
 *
 */
public class SimpleTimer {
    private long t0 = 0;

    private long t1 = 0;

    private double delta;

    private boolean stopped = true;
    
    public SimpleTimer() {
    	tick();
    }
    
    /**
     * Formatter, in seconds
     */
    private DecimalFormat sformatter = new DecimalFormat("#.##");

    /**
     * Formatter, in milliseconds
     */
    private DecimalFormat msformatter = new DecimalFormat("#.####");
    
    /**
     * @return a string containing the number of seconds since the last tick  and reset the clock.
     */
    public String tick_s() {
        return sformatter.format(tick()) + "s";
    }

    /**
     * @return a string containing the number of milliseconds since the last tick  and reset the clock.
     */
    public String tick_ms() {
        return msformatter.format(tick() * 1000) + "ms";
    }

    /**
     * @return the number of seconds since the last tick and reset the clock.
     */
    public double tick() {
    	return tick(true);
    }
    
    public double tick(boolean reset) {
    	stopped = false;
        t1 = System.nanoTime();
        delta = (t1 - t0) * 1e-9;

        if (reset) t0 = t1;
        
        return delta;
    }


    /**
     * @return observe the number of seconds since the last tick but don't reset the clock.
     */
    public double observeTick() {
    	boolean stopped = this.stopped;
    	
    	if (stopped) return delta;
    	
    	return tick(false);
    }

	public Object observe() {
		return delta;
	}

    /**
     * @return observe the number of milliseconds since the last tick but don't reset the clock.
     */
    public double observeTick_ms() {
        return observeTick() * 1000;
    }

	public void stop() {
		tick();
		stopped = true;
	}
	
	public boolean isRunning() {
		return !stopped;
	}
}
