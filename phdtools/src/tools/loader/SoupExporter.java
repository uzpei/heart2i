/**
 * 
 */
package tools.loader;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.List;

/**
 * @author piuze
 *
 */
public class SoupExporter {
	
	public static void export(List<Vertex> vertices, List<int[]> faces, String filename) throws IOException {
		System.out.println("Exporting to " + filename);
		PrintWriter out = new PrintWriter(new FileWriter(filename));

		// Formatting for floats
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(4);
		
		int m = faces.size();
		int n = vertices.size();
		
		out.println("# " + n + " vertices");
		for (Vertex v : vertices) {
			out.println("v " + nf.format(v.p.x) + " " + nf.format(v.p.y) + " " + nf.format(v.p.z));
		}
		
		out.println("# " + m + " faces");
		out.println("g all");
		String s;
		for (int[] face : faces) {
			s = "f ";
			for (int i : face) {
				s += (i+1) + " ";
			}
			out.println(s.trim());
		}
		out.println("g");
		
		out.close();
		System.out.println("Done writing to " + filename);
	}


	
}
