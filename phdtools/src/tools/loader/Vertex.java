package tools.loader;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Simple vertex class
 */
public class Vertex {
    
    public Vertex() {
        // TODO
    }
    
    public Vertex(Point3d p, int index) {
        this.p.set(p);
        this.index = index;
    }
    
    public Vertex(Vertex other) {
        this.index = other.index;
        this.p.set(other.p);
        this.n.set(other.n);
        this.child = other.child;
    }

    public int index = 0;
    
    public Point3d p = new Point3d();

    public Vector3d n = new Vector3d();

    Vertex child;
    
    @Override
    public String toString() {
        return "" + index;
    }
}
