package tools.loader;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import swing.component.FileChooser;
import swing.parameters.BooleanParameter;
import tools.loader.obj.ObjModel.PolygonFace;

/**
 * @author piuze
 */
public abstract class PolygonModel {
    
    private BooleanParameter draw = new BooleanParameter("draw", true);
    
    /**
     * @return the centroid of this model.
     */
    public Point3f getCentroid() {
        Point3d c = new Point3d();
        Point3d cp = new Point3d();
        
        for (PolyVertex pv : getVertices()) {
            cp.set(pv.p);
            c.add(cp);
        }
        c.scale(1.0 / getVertices().length);
        
        return new Point3f((float) c.x, (float) c.y, (float) c.z);
    }
    
    /**
     * @return the faces composing the mesh.
     */
    public abstract PolygonFace[] getFaces();

    /**
     * @return the mesh vertices.
     */
    public abstract PolyVertex[] getVertices();

    /**
     * Models can add extra controls through this method.
     * @return extra UI controls.
     */
    protected abstract JPanel getSpecificControls();

    private FileChooser fc = new FileChooser();
    
    /**
     * @return an optional panel (i.e. can be null) for hand-tuning the model.
     */
    public JPanel getControls() {
        
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Polygonal Model"));
        panel.setLayout(new GridBagLayout());

        fc.addOpenActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    load(fc.getCurrentFile().getAbsolutePath());
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
            
        });
        
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.NORTHWEST;
        
        c.gridx = 0;
        c.gridy = 0;
        panel.add(fc, c);

        fc.setPath(toString());
        
        c.gridx = 0;
        c.ipadx = 0;
        c.gridy = 2;
        panel.add(draw.getControls(), c);

        JPanel mc = getSpecificControls();
        if (mc != null) {
            c.gridx = 0;
            c.gridy = 2;
            panel.add(mc);
        }

        return panel;
    }
    /**
     * Models know how to draw themselves.
     * @param drawable
     */
    protected abstract void draw(GLAutoDrawable drawable);

    /**
     * Display this model to a GL canvas.
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
        if (draw.getValue()) draw(drawable);
    }

	public void setVisible(boolean b) {
		draw.setValue(b);
	}

    public void drawVertex(GLAutoDrawable drawable, PolyVertex v) {
    }
    /**
     * Fancy drawing of this vertex.
     * @param drawable
     * @param v 
     * @param alternateColor 
     */
    public void drawVertex(GLAutoDrawable drawable, PolyVertex v, float[] color) {
		GL2 gl = drawable.getGL().getGL2();

        Point3f p;
        Vector3f n;
        
        gl.glEnable(GL2.GL_BLEND);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        // Draw the vertex as a point
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPointSize(10f);
        float c = 0.4f;
        gl.glBegin(GL2.GL_POINTS);
        gl.glColor4f(c, c, c, 1f);
        n = getVertices()[v.index].n;
        p = getVertices()[v.index].p;
        float scale = 0.02f;
        gl.glVertex3f(p.x + scale * n.x, p.y + scale * n.y, p.z + scale * n.z);
        gl.glEnd();

        // Draw the normal
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glLineWidth(3);
        
//        gl.glColor4f(0, 0, 0.7f, 0.7f);
//        gl.glColor4f(0.7f, 0, 0, 0.7f);
        gl.glColor4f(color[0], color[1], color[2], color[3]);
        
        gl.glBegin(GL2.GL_LINES);
        p = getVertices()[v.index].p;
        n = getVertices()[v.index].n;
        gl.glVertex3f(p.x, p.y, p.z);
        scale = 1f;
        gl.glVertex3f(p.x + scale * n.x, p.y + scale * n.y, p.z + scale * n.z);
        gl.glEnd();
    }

    protected abstract void loadModel(String filename);
    
    public PolygonModel load(String filename) {
        loadModel(filename);
        
        fc.setPath(toString());
        
        return this;
    }
    
    
    @Override
	public abstract String toString();
    
}
