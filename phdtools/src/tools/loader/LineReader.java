package tools.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import sun.awt.datatransfer.DataTransferer.IndexOrderComparator;

/**
 * Simple wrapper class for reading lines.
 * @author piuze
 */
public class LineReader {
    private FileInputStream fis = null;
    private InputStreamReader isr = null;
    private BufferedReader reader = null;
    private File file = null;

    /**
     * @param filename The file from which we are reading sequential lines.
     */
    public LineReader(String filename) {
        this(new File(filename));
    }
    
    /**
     * @param file The file from which we are reading sequential lines.
     */
    public LineReader(File file) {
        this.file = file;
        
        try {
            fis = new FileInputStream(this.file);
            isr = new InputStreamReader(fis);
            reader = new BufferedReader(isr);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * Reset the reader to the beginning of the file.
     * @throws IOException 
     */
    public void reset() throws IOException {
        close();
        
        try {
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            reader = new BufferedReader(isr);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * @return The next line from the file or null if the EOF was reached.
     * @throws IOException
     */
    public String readLine() throws IOException {
        return reader.readLine();
    }

    public String readLineQuick() {
    	try {
            return reader.readLine();
    	}
    	catch (IOException e) {
    		return "";
    	}
    }

    /**
     * Close this line reader.
     * @throws IOException
     */
    public void close() throws IOException {
        fis.close();
        isr.close();
        reader.close();
    }

	public int getLineCount() {
		LineNumberReader lnr;
		int lineCount = 0;
		try {
			lnr = new LineNumberReader(new FileReader(file));
			try {
				lnr.skip(Long.MAX_VALUE);
				lineCount = lnr.getLineNumber();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lineCount;
	}
}
