package tools.loader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class LineWriter {
	public static void write(String filename, String buffer) throws IOException {
		write(new File(filename), buffer);
	}
	public static void write(File file, String buffer) throws IOException {
		// Create the path if necessary
		File parent = file.getParentFile();
		if (parent != null) {
			parent.mkdirs();
		}
		
		final PrintWriter out = new PrintWriter(new FileWriter(file.getAbsolutePath()));
		out.print(buffer);
		out.close();
	}

}
