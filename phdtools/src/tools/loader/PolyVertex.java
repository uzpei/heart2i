package tools.loader;

import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

/**
 * Holder class for a polygon vertex, its normal and its index.
 * @author piuze
 */
public class PolyVertex {
    /**
     * The position of this vertex.
     */
    public Point3f p = new Point3f();

    /**
     * The normal of this vertex.
     */
    public Vector3f n = new Vector3f();

    /**
     * The texture coordinate of this vertex [OPTIONAL].
     */
    public Point3f t = new Point3f();

    /**
     * Whether this vertex is texture-mapped.
     */
    public boolean useTexture;
    
    /**
     * Whether this vertex has a normal defined.
     */
    public boolean useNormal;
    
    /**
     * The index of this normal.
     */
    public int index;
    
    /**
     * @param index
     * @param p position coordinate.
     * @param n normal vector.
     */
    public PolyVertex(int index, Point3f p, Vector3f n) {
        this(index, p, n, null);
    }
    
    /**
     * @param index
     * @param p position coordinate.
     * @param n normal vector.
     * @param t texture coordinate.
     */
    public PolyVertex(int index, Point3f p, Vector3f n, Point3f t) {
        this.index = index;
        this.p.set(p);
        
        if (n != null) {
            this.n.set(n);
            useNormal = true;
        }
        else {
            useNormal = false;
        }
        
        if (t != null) {
            this.t.set(t);
            useTexture = true;
        }
        else {
            useTexture = false;
        }
    }

    /**
     * @param other
     */
    public PolyVertex(PolyVertex other) {
        index = other.index;
        p.set(other.p);
        n.set(other.n);
        t.set(other.t);
        useTexture = other.useTexture;
    }

    public PolyVertex(int index, Point3d p, Vector3d n) {
        this.index = index;
        this.p.set((float) p.x, (float) p.y, (float) p.z);
        this.n.set((float) n.x, (float) n.y, (float) n.z);
        
        useTexture = false;
    }
}
