package tools.loader.obj;

import java.io.IOException;
import java.io.PrintStream;
import java.util.StringTokenizer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import tools.loader.LineReader;
import tools.loader.PolyVertex;
import tools.loader.PolygonModel;

public class ObjModel extends PolygonModel {

    public enum Obj {
        VERTEX, NORMAL, TEXTURE, FACE, MTL_LIB, MTL_USE
    };

    private int vertexCount;

    private int normalCount;

    private int texCoordCount;

    private int faceCount;

    private PolyVertex[] polyVertices;
    
    private Point3f[] vertices;

    private Vector3f[] normals;

    private Point3f[] texCoords;

    private PolygonFace[] faces;

    private String filename;

    private float scale;
    
    private boolean noNormals = false;
    
    private boolean noTextures = false;
    
    private Point3f centroid;

    private boolean useNormals;
    
    private boolean useTextures;
    
    /**
     * Loads an OBJ model into this object.
     * @param filename
     * @param scale
     * @throws IOException
     */
    public PolygonModel load(String filename, float scale) {
        this.filename = filename;
        this.scale = scale;

        //com.sun.j3d.loaders.objectfile.ObjectFile
//        ObjectFile test = new ObjectFile();

        loadModel(filename);
        
        return this;
    }

    @Override
    protected void loadModel(String filename) {
        this.filename = filename;

        System.out.println("Reading " + filename + "...");

        LineReader reader = new LineReader(filename);

        try {
            allocateMemory(reader);

            reader.reset();

            parseObj(reader);

            reader.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Point3f p, t;
        Vector3f n;
        for (int i = 0; i < vertexCount; i++) {
            p = vertices[i];
            n = null;
            t = null;
            
            if (!noNormals) {
               n = normals[i]; 
            }
            if (!noTextures) {
                t = texCoords[i];
            }
            
            polyVertices[i] = new PolyVertex(i, p, n, t);
        }

        System.out.println("Found " + vertexCount + " vertices.");
        
        if (normalCount == vertexCount) useNormals = true;
        else useNormals = false;
        if (texCoordCount == vertexCount) useTextures = true;
        else useTextures = false;
        
        PrintStream nps = useNormals ? System.out : System.err;
        nps.println("Found " + normalCount + " normals.");
        PrintStream tps = useTextures ? System.out : System.err;
        tps.println("Found " + texCoordCount + " texture coordinates.");

        subtractCentroid();
        
    }

    private void subtractCentroid() {
        // Bring back the model to the origin
        centroid = getCentroid();
        System.out.println("Centroid is at " + centroid);
        for (int i = 0; i < vertexCount; i++) {
            vertices[i].sub(centroid);
            if (useTextures) texCoords[i].sub(centroid);
            polyVertices[i].p.sub(centroid);
            polyVertices[i].t.sub(centroid);
        }
    }

    private boolean checkType(String s, Obj t) {
        if (s.length() == 0)
            return false;

        s.trim();

        if (s.startsWith("v ") && t == Obj.VERTEX)
            return true;
        if (s.startsWith("vn ") && t == Obj.NORMAL)
            return true;
        if (s.startsWith("vt ") && t == Obj.TEXTURE)
            return true;
        if (s.startsWith("f ") && t == Obj.FACE)
            return true;
        if (s.startsWith("mtllib") && t == Obj.MTL_LIB)
            return true;
        if (s.startsWith("usemtl") && t == Obj.MTL_USE)
            return true;

        return false;
    }

    private void allocateMemory(LineReader reader) throws IOException {
        String line;

        // First fetch vertex/normal/textcoord count
        vertexCount = 0;
        normalCount = 0;
        texCoordCount = 0;
        faceCount = 0;
        while ((line = reader.readLine()) != null) {
            if (checkType(line, Obj.VERTEX))
                vertexCount++;
            else if (checkType(line, Obj.NORMAL))
                normalCount++;
            else if (checkType(line, Obj.TEXTURE))
                texCoordCount++;
            else if (checkType(line, Obj.FACE))
                faceCount++;
        }

        if (normalCount != vertexCount) noNormals = true;
        if (texCoordCount != vertexCount) noTextures = true;
        
        
        // Allocate data based on counters
        vertices = new Point3f[vertexCount];
        polyVertices = new PolyVertex[vertexCount];
        normals = new Vector3f[normalCount];
        texCoords = new Point3f[texCoordCount];
        faces = new PolygonFace[faceCount];

    }

    private void parseObj(LineReader reader) throws IOException {
        String line;

        vertexCount = 0;
        normalCount = 0;
        texCoordCount = 0;
        faceCount = 0;
        while ((line = reader.readLine()) != null) {
            if (checkType(line, Obj.VERTEX))
                parseVertex(line);
            else if (checkType(line, Obj.NORMAL))
                parseNormal(line);
            else if (checkType(line, Obj.TEXTURE))
                parseTex(line);
            else if (checkType(line, Obj.FACE))
                parseFace(line);
            else if (checkType(line, Obj.MTL_LIB))
                parseMtl(line);
            else if (checkType(line, Obj.MTL_USE))
                parseMtlUse(line);
            // else
            // System.out.println(line);
        }

    }

    private void parseVertex(String newline) {
        float coords[] = new float[4];

        // Remove the tag "v "
        newline = newline.substring(2, newline.length());

        StringTokenizer st = new StringTokenizer(newline, " ");
        for (int i = 0; st.hasMoreTokens(); i++)
            coords[i] = Float.parseFloat(st.nextToken());

        vertices[vertexCount] = new Point3f(coords);
        vertices[vertexCount].scale(scale);
        vertexCount++;
    }

    private void parseNormal(String newline) {
        float coords[] = new float[4];

        // Remove the tag "vn "
        newline = newline.substring(3, newline.length());

        StringTokenizer st = new StringTokenizer(newline, " ");
        for (int i = 0; st.hasMoreTokens(); i++)
            coords[i] = Float.parseFloat(st.nextToken());

        normals[normalCount] = new Vector3f(coords);
        normals[normalCount].scale(scale);
        normals[normalCount].normalize();
        normalCount++;
    }

    private void parseTex(String newline) {
        float coords[] = new float[4];

        // Remove the tag "vt "
        newline = newline.substring(3, newline.length());

        StringTokenizer st = new StringTokenizer(newline, " ");
        for (int i = 0; st.hasMoreTokens(); i++)
            coords[i] = Float.parseFloat(st.nextToken());

        texCoords[texCoordCount] = new Point3f(coords);
        texCoords[texCoordCount].scale(scale);
        texCoordCount++;
    }

    private void parseFace(String newline) {
        // Remove the tag "f "
        newline = newline.substring(2, newline.length());

        // Triangles are separated by a space.
        StringTokenizer st = new StringTokenizer(newline, " ");

        // Count the number of triangles in this face.
        int count = st.countTokens();

        // Allocate index arrays.
        int v[] = new int[count];
        int vt[] = new int[count];
        int vn[] = new int[count];
        for (int i = 0; i < count; i++) {
            char chars[] = st.nextToken().toCharArray();
            StringBuffer sb = new StringBuffer();
            char lc = 'x';
            for (int k = 0; k < chars.length; k++) {
                if (chars[k] == '/' && lc == '/')
                    sb.append('0');
                lc = chars[k];
                sb.append(lc);
            }

            StringTokenizer st2 = new StringTokenizer(sb.toString(), "/");
            int num = st2.countTokens();
            v[i] = Integer.parseInt(st2.nextToken());
            if (num > 1)
                vt[i] = Integer.parseInt(st2.nextToken());
            else
                vt[i] = 0;
            if (num > 2)
                vn[i] = Integer.parseInt(st2.nextToken());
            else
                vn[i] = 0;
        }

        faces[faceCount] = new PolygonFace(count, v, vn, vt);
        faceCount++;
    }

    /**
     * @param line
     */
    private void parseMtl(String line) {
        // TODO
    }

    /**
     * @param line
     */
    private void parseMtlUse(String line) {
        // TODO
    }

    @Override
    protected void draw(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

        float[] colorSkin = {245f/255f, 208f/255f, 207f/255f, 1f};
        float r = 0f;
//        final float[] shinycolour = new float[] {r,r,r,1};
        final float[] shinycolour = new float[] {r*colorSkin[0], r*colorSkin[1], r*colorSkin[2], 1};
        
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glDisable( GL2.GL_CULL_FACE );
        gl.glMaterialfv( GL2.GL_FRONT_AND_BACK,GL2.GL_AMBIENT_AND_DIFFUSE, colorSkin, 0 );
        gl.glMaterialfv( GL2.GL_FRONT_AND_BACK,GL2.GL_SPECULAR, shinycolour, 0 );
        gl.glMateriali( GL2.GL_FRONT_AND_BACK,GL2.GL_SHININESS, 0 );

        Vector3f v;
        Point3f p;

        gl.glPushMatrix();
//        gl.glRotated(-90, 0, 0, 1); 
        
        for (int i = 0; i < faceCount; i++) {

            // Should set materials here

            int[] facev = faces[i].getVertices();
            int[] facevn = faces[i].getNormals();
            int[] facevt = faces[i].getTexCoords();

            // // Quad Begin Header ////
            int polytype;
            if (facev.length == 3) {
                polytype = GL2.GL_TRIANGLES;
            } else if (facev.length == 4) {
                polytype = GL2.GL_QUADS;
            } else {
                polytype = GL2.GL_POLYGON;
            }
            gl.glBegin(polytype);
            // //////////////////////////

            for (int w = 0; w < facev.length; w++) {
                
                if (facevn[w] != 0) {
                    v = normals[facevn[w] - 1];
                    gl.glNormal3f(v.x, v.y, v.z);
                }

                // if (facevt[w] != 0) {
                // p = texCoords[facevt[w] - 1];
                // gl.glTexCoord3f(p.x, p.y, p.z);
                // }

                p = vertices[facev[w] - 1];
//                gl.glColor4f(1, 1, 1, 1f);
                gl.glVertex3f(p.x, p.y, p.z);
            }

            gl.glEnd();
        }
        
        gl.glPopMatrix();
    }

    /**
     * Class representing an obj mesh face.
     * @author piuze
     */
    public class PolygonFace {
        private int[] fvertices;
        private int[] fnormals;
        private int[] ftexCoords;
        private int fdim;
        
        /**
         * Create a face out of a set of vertices along with their normals
         * and texture coordinates.
         * @param dim
         * @param vertices
         * @param normals
         * @param texCoords
         */
        public PolygonFace(int dim, int[] vertices, int[] normals, int[] texCoords) {
            fdim = dim;
            this.fvertices = vertices;
            this.fnormals = normals;
            this.ftexCoords = texCoords;        
        }
        
        /**
         * @return the face vertices.
         */
        public int[] getVertices() {
            return fvertices;
        }

        /**
         * @return the face normals.
         */
        public int[] getNormals() {
            return fnormals;
        }

        /**
         * @return the face texture coordinates.
         */
        public int[] getTexCoords() {
            return ftexCoords;
        }

        /**
         * @return the face dimensionality (i.e. 3 for a triangular mesh).
         */
        public int getDimension() {
            return fdim;
        }
    }

    @Override
    public PolygonFace[] getFaces() {
        return faces;
    }

    @Override
    public PolyVertex[] getVertices() {
        return polyVertices;
    }
    
    @Override
    protected JPanel getSpecificControls() {

        return null;
    }
    
    @Override
    public String toString() {
        return filename;
    }

}
