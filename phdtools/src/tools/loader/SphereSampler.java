package tools.loader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.vecmath.Vector3d;

public class SphereSampler {

	public static List<Vector3d> load(String filename) {
		
		LineReader reader = new LineReader(filename);		
		
		List<Vector3d> dirs = new LinkedList<Vector3d>();
		
		String line;
		String es1, es2, es3;
		double e1, e2, e3;
        try {
			while ((line = reader.readLine()) != null) {
				StringTokenizer tokenizer = new StringTokenizer(line);
				if (tokenizer.countTokens() != 3) continue;
				
				es1 = tokenizer.nextToken();
				es2 = tokenizer.nextToken();
				es3 = tokenizer.nextToken();
				
				e1 = Double.parseDouble(es1);
				e2 = Double.parseDouble(es2);
				e3 = Double.parseDouble(es3);
				
				Vector3d v = new Vector3d(e1, e2, e3);
				
				dirs.add(v);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        System.out.println("Loaded " + dirs.size() + " orientation samples on the hemisphere.");
        
		return dirs;
	}
}
