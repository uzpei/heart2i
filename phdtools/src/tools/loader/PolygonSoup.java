package tools.loader;

import implicit.implicit3d.interpolation.Constraint;
import implicit.rendering.marching.ImplicitGrid3D;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.FileSaver;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import tools.geom.MathToolset;
import app.hair.tests.ThinPlateSurface;

/**
 * Simple implementation of a loader for a triangular polygon soup
 */
public class PolygonSoup {

	public enum Polytype { TRIANGLE, QUAD };
	
    private BooleanParameter draw = new BooleanParameter("draw", true);

    /**
     * List of vertex objects used in the mesh.
     */
    private List<Vertex> vertexList = new ArrayList<Vertex>();

    /**
     * List of faces, where each face is a list indices into the vertex list.
     */
    private List<int[]> faceList = new ArrayList<int[]>();

    private Point3d centroid = new Point3d();

    private BooleanParameter flatshading = new BooleanParameter(
            "flat shading", false);
    
    private String filename = "";
    
    protected Matrix4d tMatrix = new Matrix4d();

    private Polytype polytype;
    
    
    /**
     * Create an empty polygon soup. A load call should follow this declaration.
     */
    public PolygonSoup() {
        //
    }
    /**
     * Creates a polygon soup by loading an OBJ file
     * 
     * @param file
     */
    public PolygonSoup(String file) {
        load(file);
    }
    
    public PolygonSoup(String file, Matrix4d t) {
        load(file, t);
    }

    public PolygonSoup(String file, double scale) {
    	tMatrix.setIdentity();
    	tMatrix.setScale(scale);
        load(file, tMatrix);
    }

	public void reload() {
        load(filename);
    }
	
	public Matrix4d getTransformationMatrix() {
		return tMatrix;
	}

    public void load(String file) {
        tMatrix.setIdentity();
        load(file, tMatrix);
    }
    
    public void load(String file, Matrix4d transformation) {
        tMatrix.set(transformation);
        
        filename = file;
        
        vertexList.clear();
        faceList.clear();

        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader reader = new BufferedReader(isr);
            String line;
            int vindex = 0;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("v ")) {
                    Vertex v = parseVertex(line);
                    v.index = vindex;
                    vertexList.add(v);
                    
                    vindex++;
                } else if (line.startsWith("f ")) {
                    faceList.add(parseFace(line));
                }
            }
            reader.close();
            isr.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    	computeCentroid();

        centerAndTransform();
        
        switch (faceList.get(0).length) {
        case 3:
        	polytype = Polytype.TRIANGLE;
        	break;
        case 4:
        	polytype = Polytype.QUAD;
        	break;
        default:
        	return;
        }
        
        System.out.println("-=Detected a " + polytype + " mesh=-");
        
        computeNormals();
    }
    
    private void computeCentroid() {
    	centroid = new Point3d();

        for (Vertex v : vertexList) {
        	centroid.add(v.p);
        }
        centroid.scale(1.0 / vertexList.size());
        
        System.out.println("Centroid located at " + centroid);
    }
    
    private void centerAndTransform() {
    	        
        // Determine the bounding volume
        double max = 0;
        Point3d zero  = new Point3d();
        for (Vertex v : vertexList) {
        	v.p.sub(centroid);
            double d = v.p.distance(zero);
            if (d > max) max = d;
        }

//        // Update the translation with the centroid
//        Vector4d trans = new Vector4d();
//        tMatrix.getColumn(3, trans);
//        trans.x -= centroid.x;
//        trans.y -= centroid.y;
//        trans.z -= centroid.z;
//        
//        // Transform before applying the translation (i.e. rotate the centroid)
//        tMatrix.transform(centroid);
//        
//        // Incorporate the centroid
//        tMatrix.setTranslation(new Vector3d(trans.x,trans.y,trans.z));
//
//        // Set the bounding volume to a sphere of radius 1
//        // times the size of the model.
//        System.out.println("Scaling before normalization = " + tMatrix.getScale());
        tMatrix.setScale(tMatrix.getScale() / max);
//        System.out.println("after = " + tMatrix.getScale());

        // Apply transformation
        for (Vertex v : vertexList) {
        	
        	tMatrix.transform(v.p);
        }
    }
    
    private double scaling = 0;

    /**
     * @return the absolute scaling that was applied to the model vertices.
     */
    public double getScale() {
    	return scaling;
    }
    
    public void computeNormals() {
    	System.out.println("Recomputing normals...");
        if (polytype.equals(Polytype.TRIANGLE)) {
            computeTriNormals();
        }
        else {
        	computeArbNormals();
        }
    }
    
    private void computeTriNormals() {
        int[] ncount = new int[vertexList.size()];
        
        for (Vertex v : vertexList) {
            v.n.set(0, 0, 0);
        }

        Vector3d v1 = new Vector3d();
        Vector3d v2 = new Vector3d();
        Vector3d n = new Vector3d();

        for (int[] face : faceList) {
            int i0  = face[0];
            int i1  = face[1];
            int i2  = face[2];

            Point3d p0 = vertexList.get(i0).p;
            Point3d p1 = vertexList.get(i1).p;
            Point3d p2 = vertexList.get(i2).p;

            v1.sub(p1, p0);
            v2.sub(p2, p1);
            n.cross(v1, v2);
            n.normalize();

            vertexList.get(i0).n.add(n);
            vertexList.get(i1).n.add(n);
            vertexList.get(i2).n.add(n);
            
            ncount[i0]++;
            ncount[i1]++;
            ncount[i2]++;
        }
        
        for (Vertex v : vertexList) {
            if (ncount[v.index] != 0) {
                v.n.scale(1.0 / ncount[v.index]);
            	v.n.normalize();
            }
            else {
                System.err.println("Warning: missing face information. Using zero normal.");
            }
            System.nanoTime();
        }

    }
    
    private void computeArbNormals() {
        int[] ncount = new int[vertexList.size()];
        
        for (Vertex v : vertexList) {
            v.n.set(0, 0, 0);
        }

        Vector3d ref = new Vector3d(0, 0, 1);
        
        Vector3d v1 = new Vector3d();
        Vector3d v2 = new Vector3d();
        Vector3d n = new Vector3d();

        for (int[] face : faceList) {
        	int fc = face.length;

        	int[] ind = new int[fc];
        	for (int i = 0; i < fc; i++) {
        		ind[i] = face[i];
        	}

            // Only need to use three points to compute the normal
            // since they all are coplanar.
            Point3d p0 = vertexList.get(ind[0]).p;
            Point3d p1 = vertexList.get(ind[1]).p;
            Point3d p2 = vertexList.get(ind[2]).p;

            v1.sub(p1, p0);
            v2.sub(p2, p1);
            n.cross(v1, v2);
            n.normalize();
            
//            n.scale(-Math.signum(n.dot(ref)));

        	for (int i = 0; i < fc; i++) {
                vertexList.get(ind[i]).n.add(n);
                ncount[ind[i]]++;
        	}
        }
        
        for (Vertex v : vertexList) {
            if (ncount[v.index] != 0) {
                v.n.scale(1.0 / ncount[v.index]);
            	v.n.normalize();
            }
            else {
                System.err.println("Warning: missing face information. Using zero normal.");
            }
            System.nanoTime();
        }

    }

    private void computeQuadNormals() {
        int[] ncount = new int[vertexList.size()];
        
        for (Vertex v : vertexList) {
            v.n.set(0, 0, 0);
        }

        Vector3d v1 = new Vector3d();
        Vector3d v2 = new Vector3d();
        Vector3d n = new Vector3d();

        int i = 0;
        for (int[] face : faceList) {
        	System.out.println("Face #" + i++ + " has " + face.length + " vertices.");

            int i0  = face[0];
            int i1  = face[1];
            int i2  = face[2];
            int i3  = face[3];

            // Only need to use three points to compute the normal
            // since they all are coplanar.
            Point3d p0 = vertexList.get(i0).p;
            Point3d p1 = vertexList.get(i1).p;
            Point3d p2 = vertexList.get(i2).p;
            Point3d p3 = vertexList.get(i3).p;

            v1.sub(p1, p0);
            v2.sub(p2, p1);
            n.cross(v1, v2);
            n.normalize();

            vertexList.get(i0).n.add(n);
            vertexList.get(i1).n.add(n);
            vertexList.get(i2).n.add(n);
            vertexList.get(i3).n.add(n);
            
            ncount[i0]++;
            ncount[i1]++;
            ncount[i2]++;
            ncount[i3]++;
        }
        
        for (Vertex v : vertexList) {
            if (ncount[v.index] != 0) {
                v.n.scale(1.0 / ncount[v.index]);
            	v.n.normalize();
            	v.n.scale(Math.signum(v.n.dot(new Vector3d(0, 0, 1))));
            }
            else {
                System.err.println("Warning: missing face information. Using zero normal.");
            }
            System.nanoTime();
        }

    }

    private FileSaver fs = new FileSaver("./data/");
    

    /**
     * @return an optional panel (i.e. can be null) for hand-tuning the model.
     */
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Polygonal model " +  filename));
        vfp.add(flatshading.getControls());
        vfp.add(draw.getControls());
        vfp.add(fs);
        fs.addSaveActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					SoupExporter.export(vertexList, faceList, fs.getPath());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
        
        return vfp.getPanel();
    }

    public Point3d getCentroid() {
    	return centroid;
    }
    /**
     * Parses a vertex definition from a line in an obj file. Assumes that there
     * are three components.
     * 
     * @param newline
     * @return a new vertex object
     */
    private Vertex parseVertex(String newline) {
        // Remove the tag "v "
        newline = newline.substring(2, newline.length());
        StringTokenizer st = new StringTokenizer(newline, " ");
        Vertex v = new Vertex();
        v.p.x = Double.parseDouble(st.nextToken());
        v.p.y = Double.parseDouble(st.nextToken());
        v.p.z = Double.parseDouble(st.nextToken());
        return v;
    }

    /**
     * Gets the list of indices for a face from a string in an obj file. Simply
     * ignores texture and normal information for simplicity
     * 
     * @param newline
     * @return list of indices
     */
    private int[] parseFace(String newline) {
        // Remove the tag "f "
        newline = newline.substring(2, newline.length());
        // vertex/texture/normal tuples are separated by a spaces.
        StringTokenizer st = new StringTokenizer(newline, " ");
        int count = st.countTokens();
        int v[] = new int[count];
        for (int i = 0; i < count; i++) {
            // first token is vertex index... we'll ignore the rest
            StringTokenizer st2 = new StringTokenizer(st.nextToken(), "/");
            v[i] = Integer.parseInt(st2.nextToken()) - 1; // want zero indexed
            // vertices!
        }
        return v;
    }

    /**
     * Draw the polygon soup
     * 
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
        if (!draw.getValue()) return;
        
		GL2 gl = drawable.getGL().getGL2();
        
        gl.glEnable(GL2.GL_DEPTH_TEST);
//        gl.glDisable(GL2.GL_DEPTH_TEST);
        gl.glDepthFunc(GL2.GL_LEQUAL);
        gl.glClearDepth(1);
		gl.glDepthMask(true);
        gl.glDisable(GL2.GL_CULL_FACE);

        // float[] colorSkin = { 245f / 255f, 208f / 255f, 207f / 255f, 1f };
        float[] colorSkin = { 239f / 255f, 208f / 255f, 207f / 255f, 1f };
        float r = 0f;
        // final float[] shinycolour = new float[] {r,r,r,1};
        final float[] shinycolour = new float[] { r * colorSkin[0],
                r * colorSkin[1], r * colorSkin[2], 1 };

        // Leave this enabled
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE,
                colorSkin, 0);
        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, shinycolour, 0);
        gl.glMateriali(GL2.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 0);

        Vector3d n = new Vector3d();

        int ptype;
        if (polytype.equals(Polytype.TRIANGLE)) {
        	ptype = GL2.GL_TRIANGLES;
        } else if (polytype.equals(Polytype.QUAD)) {
        	ptype = GL2.GL_QUADS;
        } else {
        	ptype = GL2.GL_POLYGON;
        }
        gl.glBegin(ptype);

        Point3d p;
        for (int[] faceVertex : faceList) {
            if (flatshading.getValue()) {
                for (int vertex : faceVertex) {
                    p = vertexList.get(vertex).p;
                    gl.glNormal3d(0,0,1);
                    gl.glVertex3d(p.x, p.y, p.z);
                }
            }
            else {
                for (int vertex : faceVertex) {
                    p = vertexList.get(vertex).p;
                    n = vertexList.get(vertex).n;
                    gl.glNormal3d(n.x, n.y, n.z);
                    gl.glVertex3d(p.x, p.y, p.z);
                }
            }
        }
        gl.glEnd();
//        gl.glBegin(GL2.GL_TRIANGLES);
//        for (int[] faceVertex : faceList) {
//            Point3d p0 = vertexList.get(faceVertex[0]).p;
//            Point3d p1 = vertexList.get(faceVertex[1]).p;
//            Point3d p2 = vertexList.get(faceVertex[2]).p;
//            
//            if (flatshading.getValue()) {
//                v1.sub(p1, p0);
//                v2.sub(p2, p1);
//                n.cross(v1, v2);
//                n.normalize();
//                gl.glNormal3d(n.x, n.y, n.z);
//                gl.glVertex3d(p0.x, p0.y, p0.z);
//                gl.glVertex3d(p1.x, p1.y, p1.z);
//                gl.glVertex3d(p2.x, p2.y, p2.z);
//            }
//            else {
//                Vector3d n0 = vertexList.get(faceVertex[0]).n;
//                Vector3d n1 = vertexList.get(faceVertex[1]).n;
//                Vector3d n2 = vertexList.get(faceVertex[2]).n;
//
//                gl.glNormal3d(n0.x, n0.y, n0.z);
//                gl.glVertex3d(p0.x, p0.y, p0.z);
//                
//                gl.glNormal3d(n1.x, n1.y, n1.z);
//                gl.glVertex3d(p1.x, p1.y, p1.z);
//                
//                gl.glNormal3d(n2.x, n2.y, n2.z);
//                gl.glVertex3d(p2.x, p2.y, p2.z);
//            }
//        }
//        gl.glEnd();
    }
    public List<Vertex> getVertices() {
        return vertexList;
    }
    public void drawVertex(GLAutoDrawable drawable, Vertex v, float[] color) {
		GL2 gl = drawable.getGL().getGL2();

        Point3d p;
        Vector3d n;
        
        gl.glEnable(GL2.GL_BLEND);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        // Draw the vertex as a point
//        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPointSize(15f);
        float c = 0.4f;
//        gl.glColor4f(c, c, c, 1f);
        gl.glColor4f(color[0], color[1], color[2], 0.7f*color[3]);
        n = getVertices().get(v.index).n;
        p = getVertices().get(v.index).p;
        float scale = 0f;
        
        gl.glBegin(GL2.GL_POINTS);
        gl.glVertex3d(p.x + scale * n.x, p.y + scale * n.y, p.z + scale * n.z);
        gl.glEnd();

        // Draw the normal
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glLineWidth(3);
        
//        gl.glColor4f(0, 0, 0.7f, 0.7f);
//        gl.glColor4f(0.7f, 0, 0, 0.7f);
        gl.glColor4f(color[0], color[1], color[2], color[3]);
        
        gl.glBegin(GL2.GL_LINES);
        p = getVertices().get(v.index).p;
        n = getVertices().get(v.index).n;
        gl.glVertex3d(p.x, p.y, p.z);
        scale = 1f;
        gl.glVertex3d(p.x + scale * n.x, p.y + scale * n.y, p.z + scale * n.z);
        gl.glEnd();
    }
    public void drawVertices(GLAutoDrawable drawable, int psize) {
		GL2 gl = drawable.getGL().getGL2();
        
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_BLEND);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        gl.glInitNames();
        int i = 0;
        Vector3d normal;
        gl.glPointSize(psize);
        gl.glColor4f(1, 1, 1, 0.5f);
        for (Vertex pv : getVertices()) {

            gl.glPushName(i);
            gl.glBegin(GL2.GL_POINTS);
            gl.glNormal3d(pv.n.x, pv.n.y, pv.n.z);
            gl.glVertex3d(pv.p.x, pv.p.y, pv.p.z);
            gl.glEnd();
            gl.glPopName();

            i++;
        }
    }
	public String getName() {
		File f = new File(filename);
		return f.getName().substring(0, f.getName().length() - 4);
	}

	public static List<Point3d> sampleThinPlate(ThinPlateSurface tps, ImplicitGrid3D grid, PolygonSoup polygon, int numSamples) {
		// Sample the polygon
		List<Vertex> vertices = polygon.getVertices();
		int count = 0;
		HashSet<Vertex> samples = new HashSet<Vertex>();
		Random rand = new Random();
		while (count < numSamples) {
			if (samples.add(vertices.get(rand.nextInt(vertices.size()))))
				count++;
		}
		
		// TODO: use mean distance to centroid to set scale
		tps.clear();
		tps.addConstraint(new Constraint(polygon.getCentroid(), Constraint.Orientation.INTERIOR));
		double scale = 0.05;
		double radius = scale / numSamples;
		List<Point3d> psamples = new ArrayList<Point3d>();
		for (Vertex vertex : samples) {
//			tps.addNormalConstraint(vertex.p, vertex.n, scale);
			tps.addConstraint(new Constraint(vertex.p, Constraint.Orientation.BOUNDARY));

			Vector3d v = MathToolset.randomDirection();
			psamples.add(vertex.p);
		}
		
		return psamples;
	}
	
}
