package tools.loader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.vecmath.Vector3d;

import volume.IntensityVolume;

public class TractoDatLoader {

	private static void parse(LineReader reader, double[] array)
			throws NumberFormatException, IOException {
		int i = 0;
		String line;
		while ((line = reader.readLine()) != null) {
			array[i++] = Double.parseDouble(line);
		}
		reader.close();
	}

	public static IntensityVolume[] load(String filename, String fe1, String flambda, String ffa, int[] dim) {

		try {
			// principal direction
			File fex = new File(filename + fe1 + "x.txt");
			File fey = new File(filename + fe1 + "y.txt");
			File fez = new File(filename + fe1 + "z.txt");
			LineReader rex = new LineReader(fex);
			LineReader rey = new LineReader(fey);
			LineReader rez = new LineReader(fez);
			double[] arrayx = new double[rex.getLineCount()];
			double[] arrayy = new double[rey.getLineCount()];
			double[] arrayz = new double[rez.getLineCount()];
			parse(rex, arrayx);
			parse(rey, arrayy);
			parse(rez, arrayz);

			// eigenvalues
			if (flambda != null) {
//				File flx = new File(filename + flambda + "y.txt");
//				File fly = new File(filename + flambda + "z.txt");
//				File flz = new File(filename + flambda + "y.txt");
//				LineReader rlx = new LineReader(flx);
//				LineReader rly = new LineReader(fly);
//				LineReader rlz = new LineReader(flz);
//				double[] arraylx = new double[rlx.getLineCount()];
//				double[] arrayly = new double[rly.getLineCount()];
//				double[] arraylz = new double[rlz.getLineCount()];
//				parse(rlx, arraylx);
//				parse(rly, arraylx);
//				parse(rlz, arraylx);
			}

			if (ffa != null) {
				// Fractional anisotropy
				File fa = new File(filename + ffa + ".txt");
				LineReader rf = new LineReader(fa);
				double[] arrayfa = new double[rf.getLineCount()];
				parse(rf, arrayfa);
			}
			
			System.out.println("X dimension is " + arrayx.length);
			System.out.println("Y dimension is " + arrayy.length);
			System.out.println("Z dimension is " + arrayz.length);

			double[][][] datax = new double[dim[0]][dim[1]][dim[2]];
			double[][][] datay = new double[dim[0]][dim[1]][dim[2]];
			double[][][] dataz = new double[dim[0]][dim[1]][dim[2]];

			int i = 0;
			Vector3d vl = new Vector3d();
			Vector3d v = new Vector3d();
			// NO YZX
			// NO YXZ
			// NO XYZ
			
						for (int z = 0; z < dim[2]; z++) {
							for (int x = 0; x < dim[0]; x++) {
							for (int y = 0; y < dim[1]; y++) {

//						vl.set(arraylx[i], arrayly[i], arraylz[i]);
//						double fav = arrayfa[i];

//						if (Math.abs(vl.x) > 0.9 || Math.abs(vl.y) > 0.9 || Math.abs(vl.z) > 0.9) {
							// System.out.println(vl.length());
//						if (fav < 0.2) {
							v.set(arrayx[i], arrayy[i], arrayz[i]);
							v.normalize();

							datax[x][y][z] = v.x;
							datay[x][y][z] = v.y;
							dataz[x][y][z] = v.z;
//						}
//						}

						i++;
					}
				}
			}

			IntensityVolume vx = new IntensityVolume(datax);
			IntensityVolume vy = new IntensityVolume(datay);
			IntensityVolume vz = new IntensityVolume(dataz);

			return new IntensityVolume[] { vx, vy, vz };
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static IntensityVolume loadMask(String filename, int[] dim) {

		try {
			// principal direction
			File maskfile = new File(filename);
			LineReader maskreader = new LineReader(maskfile);
			double[] maskarray = new double[maskreader.getLineCount()];
			parse(maskreader, maskarray);

			double[][][] data = new double[dim[0]][dim[1]][dim[2]];

			int i = 0;
			for (int z = 0; z < dim[2]; z++) {
				for (int x = 0; x < dim[0]; x++) {
					for (int y = 0; y < dim[1]; y++) {
							data[x][y][z] = maskarray[i];
						i++;
					}
				}
			}

			return new IntensityVolume(data);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
