package tools;


import helicoid.Helicoid;
import helicoid.PiecewiseHelicoid;
import helicoid.modeling.PHelicoidGenerator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import tools.geom.Polyline;

public class PolylineExporter {

	public enum GEOTYPE {
		POLY, BEZIER, NURB
	};
	public enum OBJTYPE {
		BEZIER, BSPLINE, CARDINAL
	};

	public static void main(String[] args) throws IOException {
		new PolylineExporter();
	}

	public PolylineExporter() throws IOException {
//		List<Point3d> pts = new LinkedList<Point3d>();
//
//		boolean helix = false;
//		
//		// Generate a sample circle or helix
//		int turns = helix ? 5 : 1;
//		int n = 40;
//		double res = turns * Math.PI / n;
//		double r = 3;
//		double z = 0, dz = 0.3;
//		for (double t = 0; t < turns * Math.PI; t += res) {
//			Point3d p = new Point3d();
//			p.x = r * Math.cos(t);
//			p.y = r * Math.sin(t);
//			p.z = z;
//			if (helix) z += dz * r;
//
//			pts.add(p);
//		}
//
//		Polyline pl = new Polyline(pts);
//		exportOBJ(pl, "./data/testBspline.obj", OBJTYPE.BSPLINE);
		
		PHelicoidGenerator gen = new PHelicoidGenerator();
		PiecewiseHelicoid ph = gen.generateRandomPHelicoids(4);
		List<PiecewiseHelicoid> phs = new LinkedList<PiecewiseHelicoid>();
		phs.add(ph);		
		exportOBJhelicoids(phs, "./data/testBspline.obj", OBJTYPE.BSPLINE, false);
		
		
	}

	/**
	 * Videoscape GEO file format. From:
	 * 
	 * @param p
	 * @throws IOException
	 */
	public static void exportGEO(Polyline p, String filename, GEOTYPE type)
			throws IOException {
		List<Polyline> list = new LinkedList<Polyline>();
		p.moveTo(new Point3d(-1, 0, 0));
		list.add(p);
		
		// Create a translated version
		Polyline p2 = new Polyline(p);
		p2.moveTo(new Point3d(1, 0, 0));
		list.add(p2);
		
		exportGEO(list, filename, type);
	}

	public static void exportGEO(List<Polyline> list, String filename,
			GEOTYPE type) throws IOException {
		PrintWriter out = new PrintWriter(new FileWriter(filename));

		// GEO file
		out.println("3DG3");

		// Curve tag
		out.println("0");

		// Number of curves
		out.println(list.size() + "");

		// Extrude number
		out.println("0 0");

		// Orientation matrix
		// 1.0 0.0 0.0 0.0
		// 0.0 1.0 0.0 0.0
		// 0.0 0.0 1.0 0.0
		// 0.0 0.0 0.0 1.0
		Matrix4d identity = new Matrix4d();
		identity.setIdentity();
		String ms = identity.toString();
		// ms.replaceAll(",(?!,)","\n");

		ms = ms.replaceAll(",", "");
		ms = ms.substring(0, ms.length() - 1);
		out.println(ms);

		// Curve type:
		// 0 = poly, 1 = bezier, 4 = nurb
		int ctype = 0;
		switch (type) {
		case POLY:
			ctype = 0;
			break;
		case BEZIER:
			ctype = 1;
			break;
		case NURB:
			ctype = 4;
			break;
		}

		// Define parameters here
		// Number of vertices in u and v directions
		// by default use all vertices as geometry
		int pntsu = 1, pntsv = 1;
		// Resolution of u and v direction
		int resolu = 10, resolv = 10;
		// Order of u and v directtion
		int orderu = 0, orderv = 0;
		// Cyclic flag in u and v direction
		// 0 = not cyclic, 1 = cyclic
		int flagu = 0, flagv = 0;

		for (Polyline l : list) {
			out.println(ctype);
			out.println(pntsu + " " + pntsv);
			out.println(resolu + " " + resolv);
			out.println(orderu + " " + orderv);
			out.println(flagu + " " + flagv);

			if (type.equals(GEOTYPE.BEZIER)) {
				throw new IOException("NOT YET SUPPORTED.");
				// for (int i = 0; i < l.getPoints().size();) {
				// // Write points
				// Point3d p1 = l.getPoint(i++);
				// Point3d p2 = l.getPoint(i++);
				// Point3d p3 = l.getPoint(i++);
				// out.println(p1.x + " " + p1.y + " " + p1.z);
				// out.println(p2.x + " " + p2.y + " " + p2.z);
				// out.println(p3.x + " " + p3.y + " " + p3.z);
				// // handle type
				// // 0 = free, 1 = auto, 2 = vector, 3 = aligned
				// out.println("3 3");
				// }
			} else {
				// Write points
				for (Point3d p : l) {
					out.println(p.x + " " + p.y + " " + p.z);
				}
				// Write knots
				for (Point3d p : l) {
					out.print("1 ");
				}
				out.println();
			}
		}
		out.close();

	}

	/**
	 * Wavefront obj file format.
	 * 
	 * @param p
	 * @throws IOException
	 */
	public static void exportOBJ(Polyline p, String filename, OBJTYPE type)
			throws IOException {
		
		List<Polyline> list = new LinkedList<Polyline>();
		list.add(p);

		exportOBJ(list, filename, type);
	}

	/**
	 * TODO: add the choice of either sampling the vertex count, or selecting a precise target vertex count.
	 * @param list
	 * @param filename
	 * @param type
	 * @param separatePieces if different p-helicoid pieces should be stored in different files.
	 * @throws IOException
	 */
	public static void exportOBJ(List<Polyline> list, String filename,
			OBJTYPE type) throws IOException {
		throw new RuntimeException("This feature is not implemented for polylines. Use the p-helicoid exporter instead.");
	}	
	
	/**
	 * TODO: add the choice of either sampling the vertex count, or selecting a precise target vertex count.
	 * TODO: add the option to separate pieces. Export all first pieces of all p-helicoids first, then second, etc
	 * @param list
	 * @param filename
	 * @param type
	 * @param separatePieces if different p-helicoid pieces should be stored in different files.
	 * @throws IOException
	 */
	public static void exportOBJhelicoids(List<PiecewiseHelicoid> list, String filename,
			OBJTYPE type, boolean separatePieces) throws IOException {		

		// Prepare writing
		PrintWriter out = new PrintWriter(new FileWriter(filename));
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(4);
		List<Point3d> nodes = new LinkedList<Point3d>();

		// Degree of the B-spline
		int deg = 2;
		
		// Number of segments per p-helicoid piece
		int psegments = 3;
		
//		System.out.println("Exporting " + type + " to " + filename + " with " + samplingPercentage*100 + "% sampling.");
		
		// Parameterization of the curve
		// usually t0 = 0, tend = 1;
		double t0 = 0;
		double tend = 1;
				
		int index = 0;
		for (PiecewiseHelicoid ph: list) {
//			System.out.println("P-helicoid has " + ph.getCount() + " pieces and " + ph.getPoints().size() + " vertices.");

			nodes.clear();
			
			// Specifies the type of curve
			out.println("# " + type.toString().toLowerCase() + " curve");

			// Add first node (need to be added twice, this is the first)
			nodes.add(ph.getOrigin());
			
			// Add the origin and the interior control nodes for each piece
			for (Helicoid h : ph) {
				// Determine the delta
				int dn = h.getCount() / psegments;
				
				// Add first and second
				nodes.add(h.getPoint(0));
				
				// Add the interior control points
				for (int i = dn; i < h.getCount() - 1; i+= dn) {
					nodes.add(h.getPoint(i-1));
				}
			}
			
			// Add the last node (need to be added twice)
			nodes.add(ph.getTip());
			nodes.add(ph.getTip());
			
			// List the vertices
			for (Point3d p : nodes) {
				out.println("v " + nf.format(p.x) + " " + nf.format(p.y) + " " + nf.format(p.z));
			}
			
			// Comment
			out.println("# " + nodes.size() + " vertices");
			
			// Group
			out.println("g Curve" + index++);
			
			// Type of freeform geometry
			out.println("cstype " + type.toString().toLowerCase());

			// The degree of the interpolating polynomial
			out.println("deg " + deg);

			// Curve approximation technique
			// either 
			// 1) ctech cparm res (uses constant parameteric subdivision. Number of subdivisions = res * degree of curve. Use 0 for a single line segment per polynomial curve segment.)
			// 2) ctech cspace maxlength (uses constant spatial subdivision)
			out.println("ctech cparm 0.0");

			// Define the curve by listing parameterization and vertices
			out.print("curv " + t0 + " " + tend + " ");
			for (int i = 1; i <= nodes.size(); i++) {
				out.print(-i + " ");
			}
			
//			System.out.println("Writing " + nodes.size() + " vertices.");

			// Evaluation points
//			out.print("parm u ");
			// This is the increment for the evaluation points.
			// The parameter "u" will vary from t0 to tend with increments of "du"
//			double du = samplingPercentage * (tend-t0);
//			double du = 1.0 / nodes.size();
//			for (double i = t0; i <= tend; i += du)
//				out.print(nf.format(i) + " ");
//
//			// TODO: maybe only add tip if it's not already added?
////			if (tend - i > eps) out.print(nf.format(tend) + " ");
//			out.print(nf.format(tend) + " ");
			
			out.println();
			
			out.println("end");
		}
		
		out.close();
		System.out.println("Done writing to " + filename);
	}

}
