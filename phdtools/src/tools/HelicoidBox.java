package tools;

import gl.geometry.primitive.Cube;
import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameterNode;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import tools.geom.MathToolset;


public class HelicoidBox {

	private static double[] colorKT = new double[] { 1, 0, 0, 1 };
	private static double[] colorKN = new double[] { 0, 1, 0, 1 };
	private static double[] colorKB = new double[] { 0, 0, 1, 1 };
	private static double[] colorError = new double[] { 1, 1, 0, 1 };
	private static double lineWidth = 1;
	
	public static void display(GLAutoDrawable drawable, double transparency, HelicoidParameterNode hp, HelicoidParameter min, HelicoidParameter max, double maxError, Point3d origin, Vector3d[] span, boolean[] show) {
				
//		HelicoidParameter htemp = new HelicoidParameter(mag);
//		mag = new HelicoidParameter(1, 1, 1, 1);
		
		// Set RGB for KT
		colorKT[0] = 0.1 + Math.abs(hp.KT / max.KT);
		colorKT[2] = 1 - hp.KT / max.KT;
		colorKT[0] = colormap(colorKT[0]);

		// Set RGB for KN
		colorKN[1] = 0.1 + Math.abs(hp.KN / max.KN);
		colorKN[0] = 1 - hp.KN / max.KN;
		colorKN[1] = colormap(colorKN[1]);

		// Set RGB for KB
		// Add negative scaling
		colorKB[2] = 0.1 + Math.abs(hp.KB / max.KB);
//		colorKB[1] = 0;
		colorKB[1] = hp.KB > 0 ? hp.KB / max.KB : 0;
		colorKB[2] = colormap(colorKB[2]);

		// Set RGB for error
		colorError[0] = Math.abs(hp.getError() / maxError);
		colorError[1] = Math.abs(hp.getError() / maxError);
		colorError[0] = colormap(colorError[0]);
		colorError[1] = colormap(colorError[1]);
		
		// All nodes have same transparency
		colorKT[3] = transparency - 0.1;
		colorKN[3] = transparency - 0.1;
		colorKB[3] = transparency - 0.1;
		colorError[3] = transparency - 0.1;

		GL2 gl = drawable.getGL().getGL2();
		gl.glPushMatrix();
		gl.glTranslated(origin.x + 0.5, origin.y + 0.5, origin.z + 0.5);
		
		if (show[0] && hp.KT >= min.KT) {
			drawCube(drawable, colorKT, MathToolset.getMaxComponent(span[0]));
		}
		if (show[1] && hp.KN >= min.KN) {
			drawCube(drawable, colorKN, MathToolset.getMaxComponent(span[1]));
		}
		if (show[2] && hp.KB >= min.KB) {
			drawCube(drawable, colorKB, MathToolset.getMaxComponent(span[2]));
		}
		if (show[3]) {
//			System.out.println("Drawing error, span = " + span[3] + ", color = " + Arrays.toString(colorError));
			drawCube(drawable, colorError, MathToolset.getMaxComponent(span[3]));
		}

		gl.glPopMatrix();
		
//		if (show[0]) Box.display(drawable, origin, span[0], colorKT, lineWidth, false);
//		if (show[1]) Box.display(drawable, origin, span[1], colorKN, lineWidth, false);
//		if (show[2]) Box.display(drawable, origin, span[2], colorKB, lineWidth, false);
//		if (show[3]) Box.display(drawable, origin, span[3], colorAlpha, lineWidth, false);
		
//		mag.set(htemp);
	}
	
	public static void drawCube(GLAutoDrawable drawable, double[] color, double size) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glColor4d(color[0], color[1], color[2], color[3]);
//		gl.glColor4d(1, 1, 1, 1);
		Cube.draw(drawable, size);
//		gl.glColor4d(0, 0, 0, 1);
//		Cube.drawWireframe(drawable, size);

	}
	
	public static double colormap(double v) {
		double base = 0;
//		return base + Math.log(1 + Math.max(v - base, 0));
		return base + v;
	}
}
