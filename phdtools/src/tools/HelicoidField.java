package tools;

import gl.geometry.GLPickable;
import gl.geometry.primitive.Cube;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import helicoid.parameter.HelicoidParameter.ParameterType;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.PerVoxelMethod;
import volume.VolumeRenderer;
import voxel.VoxelBox;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;

/**
 * @author epiuze
 */
public class HelicoidField implements GLPickable, PerVoxelMethod {

//	protected static final int[] KT_COLOR = new int[] { 255, 0, 0 };
//	protected static final int[] KN_COLOR = new int[] { 0, 255, 0 };
//	protected static final int[] KB_COLOR = new int[] { 0, 0, 255 };
//	protected static final int[] ERROR_COLOR = new int[] { 255, 255, 0 };

	private BooleanParameter display = new BooleanParameter("display", true);

	private DoubleParameter opacity = new DoubleParameter("opacity", 0.8, 0, 1);

	private DoubleParameter errorThreshold = new DoubleParameter("error threshold", 0, 0, Double.POSITIVE_INFINITY);
	
	private DoubleParameter contrast = new DoubleParameter("contrast", 0.7, 0, 1);
	
	private BooleanParameter overlay = new BooleanParameter("box overlay", false);

	private VoxelBox voxelBox;

	private BooleanParameter[] showCurvatures = new BooleanParameter[4];
//	private BooleanParameter showAlpha = new BooleanParameter(GreekToolset
//			.toString(GreekLetter.ALPHA)
//			+ " (show)", false);
	
	private BooleanParameter interpolateFit = new BooleanParameter("fit interpolation", false);
	
	private List<CartanParameterNode> nodes;

	private List<CartanParameterNode> thresholdedNodes;

	private CartanParameterNode[][][] nodeArray;
	
	private CartanParameter min = new HelicoidParameterControl();
	private CartanParameter max = new CartanParameterControl();

	private CartanParameterControl displayMax = new CartanParameterControl();
	private CartanParameterControl displayMin = new CartanParameterControl();

	{
		double vdef = 0.15;
		displayMax.set(new CartanParameter(vdef, vdef, vdef, vdef));
	}
	
	private Vector3d[] span;

	private int[] dimension;

	private double currentMeanError, currentStandardDeviation, currentMin, currentMaxError;
	private CartanParameter meanParameter;
	private CartanParameter stdParameter;

	private BooleanParameter useVolumeRenderer = new BooleanParameter("use volume renderer)", true);

	private VolumeRenderer renderer;
	
	private IntensityVolume[] renderingVolumes = new IntensityVolume[4];

	public IntensityVolume[] getParameterVolumes() {
		return renderingVolumes;
	}
	
	public void createRenderer(JoglRenderer jrenderer) {
		renderer = jrenderer.createVolumeRendererInstance();
	}
	
	/**
	 * Create an empty helicoid box field. Fields will need to be initialized later using the {@code set} method.
	 */
	public HelicoidField() {
		this(null, null, false);
	}

	public HelicoidField(int[] dim, List<CartanParameterNode> nodes, boolean createBlender) {

		ParameterListener displayListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
//				System.out.println("Parameter changed");
				update();
			}
		};
		
		displayMin.addParameterListener(displayListener);
		displayMax.addParameterListener(displayListener);
		contrast.addParameterListener(displayListener);
		errorThreshold.addParameterListener(displayListener);
		
		// Default for empty constructor
		if (dim == null || nodes == null) {
			dim = new int[] { 1, 1, 1 };
			nodes = new LinkedList<CartanParameterNode>();
			nodes.add(new CartanParameterNode(new CartanParameter(),
					new Point3d()));
		}

		set(dim, nodes, ParameterType.ALL, createBlender);
		
		showCurvatures[0] = new BooleanParameter("KT (show)", true);
		showCurvatures[1] = new BooleanParameter("KN (show)", false);
		showCurvatures[2] = new BooleanParameter("KB (show)", false);
		showCurvatures[3] = new BooleanParameter("Error (show)", false);

	}

	/**
	 * Combo box for selecting the parameter type that is displayed.
	 */
	private EnumComboBox<ParameterType> ecbHelicoid = new EnumComboBox<ParameterType>(
			"helicoid parameter", ParameterType.HKT);
	{
		ecbHelicoid.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
	           	setType(ecbHelicoid.getSelected());
			}
		});
	}

	private void computeDisplayMagnitude() {

		min.set(Double.MAX_VALUE);
		max.set(-Double.MAX_VALUE);
		
		for (CartanParameterNode node : thresholdedNodes) {
			if (node.isGarbage()) continue;
			if (node.getc121() < min.getc121())
				min.getc121() = node.getc121();
			if (node.getc122() < min.getc122())
				min.getc122() = node.getc122();
			if (node.getc123() < min.getc123())
				min.getc123() = node.getc123();
			
//			if (node.alpha < min.alpha)
//				min.alpha = node.alpha;
			if (node.getError() < min.alpha)
				min.alpha = node.getError();

			if (node.getc121() > max.getc121())
				max.getc121() = node.getc121();
			if (node.getc122() > max.getc122())
				max.getc122() = node.getc122();
			if (node.getc123() > max.getc123())
				max.getc123() = node.getc123();
//			if (node.alpha > max.alpha)
//				max.alpha = node.alpha;
			if (node.getError() > max.alpha)
				max.alpha = node.getError();
		}
		
		displayMax.updateSliders();
	}

	private void update() {

		Vector3d spanKT = new Vector3d(1, 1, 1);
		Vector3d spanKN = new Vector3d(spanKT);
		Vector3d spanKB = new Vector3d(spanKT);
//		Vector3d spanAlpha = new Vector3d(spanKT);
		Vector3d spanError = new Vector3d(spanKT);
		
		span = new Vector3d[] { spanKT, spanKN, spanKB, spanError };

		updateThresholdedNodes(null);

		computeDisplayMagnitude();

		updateError();

		if (gridBlender != null)
			gridBlender.set(thresholdedNodes, displayMin, displayMax);

		fillHelicoidVolumes();
		updateRenderer();
	}
	
	/**
	 * Set the error threshold to the mean + 3 x sigma
	 */
	public void setAutomaticErrorThreshold() {
		setAutomaticErrorThreshold(false);
	}

	/**
	 * @param quiet to prevent a system update
	 */
	private void setAutomaticErrorThreshold(boolean quiet) {
		if (quiet) errorThreshold.hold(true);
		double threshold = Math.max(0, getMeanError(thresholdedNodes) + 1 * getStandardDeviation(thresholdedNodes));
		errorThreshold.setValue(threshold);
		if (quiet) errorThreshold.hold(false);
	}

	public List<String> getInfo() {
		text.clear();

		text.add("Mean parameter = " + meanParameter.toStringShort() + " +- "
				+ stdParameter.toStringShort());
		text.add("Min/max parameter = " + min.toStringShort() + ", "
				+ max.toStringShort());

		text.add("Mean error (deg) = " + toDegrees(currentMeanError) + " +- "
				+ toDegrees(currentStandardDeviation));
		text.add("Mean error (rad) = " + currentMeanError + " +- "
				+ currentStandardDeviation);
		text.add("Max error (deg) = " + toDegrees(currentMin) + ", "
				+ toDegrees(currentMaxError));
		text.add("# nodes = "
				+ thresholdedNodes.size()
				+ " ("
				+ (nodes.size() - thresholdedNodes.size())
				+ " ~ "
				+ 100
				* Math.round(10 * (nodes.size() - thresholdedNodes.size())
						/ ((double) nodes.size())) / 10d + "% thresholded)");

		return text;
	}
	
	private double toDegrees(double angle) {
		return angle / Math.PI * 180;
	}

	/**
	 * Compute stats using only the thresholded nodes.
	 */
	private void updateError() {
		
		currentMeanError = getMeanError(thresholdedNodes);
		currentStandardDeviation = getStandardDeviation(thresholdedNodes);
		currentMin = getMinError(thresholdedNodes);
		currentMaxError = getMaxError(thresholdedNodes);
		
		meanParameter = getMeanParameter(thresholdedNodes);
		stdParameter = getParameterStandardDeviation(thresholdedNodes);
	}
	
	private CartanParameter getMeanParameter(List<CartanParameterNode> nodes) {
		CartanParameter hp = new CartanParameter();
		
		int i = 0;
		for (CartanParameterNode node : nodes) {
			if (!Double.isNaN(node.getError())) {
				hp.add(node);
				i++;
			}
		}
		
		hp.divide(i);
		
		return hp;
	}

	private CartanParameter getParameterStandardDeviation(List<CartanParameterNode> nodes) {
		CartanParameter hp = new CartanParameter();
		CartanParameter mean = getMeanParameter(nodes);
		
		int i = 0;
		for (CartanParameterNode node : nodes) {
			if (!Double.isNaN(node.getError())) {
				CartanParameter nodep = new CartanParameter(node);
				nodep.add(-1, mean);
				nodep.exp(2);

				hp.add(nodep);
				i++;
			}
		}
		
		hp.divide(i);
		
		hp.exp(0.5);
		
		return hp;
	}

	private void updateThresholdedNodes(VoxelBox box) {
		double errorThreshold = this.errorThreshold.getValue();
		
		// Update thresholded nodes
		thresholdedNodes.clear();
		for (CartanParameterNode node : nodes) {
			CartanParameterNode tn = new CartanParameterNode(node);

			double ne = node.getError();
			
			if (Double.isNaN(ne)) {
				tn.setGarbage(true);
			}
			else if (this.errorThreshold.isChecked() && node.getError() > errorThreshold) {
				tn.setGarbage(true);
			}
			else if (box != null && !box.contains(tn.getPosition())) {
				tn.setGarbage(true);
			}
			
			if (!tn.isGarbage()){
				thresholdedNodes.add(tn);
			}
			
		}
	}
	
	public void updateRenderer() {
		updateRenderer(voxelBox);
	}
	
	public void updateRenderer(VoxelBox voxelBox) {
		this.voxelBox = voxelBox;
		fillHelicoidVolumes();
		setRenderingVolume();
	}

	private void fillHelicoidVolumes() {
		fillHelicoidVolumes(voxelBox);
	}
	
	private void fillHelicoidVolumes(VoxelBox box) {
		if (box == null) return;
		
		voxelBox = box;
		
		// Set volume renderer
		int[] dims = box.getDimension();

		// Parameters
		for (int i = 0; i < 3; i++) {
			renderingVolumes[i] = new IntensityVolume(dims[0], dims[1], dims[2]);
			IntensityVolume.fillWith(Double.NaN, renderingVolumes[i].getData());
		}
		
		// Error
		renderingVolumes[3] = new IntensityVolume(dims[0], dims[1], dims[2]);
		IntensityVolume.fillWith(Double.NaN, renderingVolumes[3].getData());
		
		for (CartanParameterNode node : thresholdedNodes) {
			Point3i pt = MathToolset.tuple3dTo3i(node.getPosition());
			
			for (int i = 0; i < 3; i++)
				renderingVolumes[i].set(pt.x, pt.y, pt.z, node.get(i));
			
			renderingVolumes[3].set(pt.x, pt.y, pt.z, node.getError());
		}
	}

	/**
	 * Set the volume dimensions and fill it with {@code helicoidNodes}.
	 * @param dim
	 * @param helicoidNodes
	 */
	public void set(int[] dim, List<CartanParameterNode> helicoidNodes) {
		set(dim, helicoidNodes, ecbHelicoid.getSelected(), true);
	}

	public void set(int[] dim, List<CartanParameterNode> helicoidNodes, ParameterType type) {
		set(dim, helicoidNodes, type, true);
	}
	
	public void set(int[] dim, List<CartanParameterNode> helicoidNodes, ParameterType type, boolean createBlender) {

		// Init arrays if data has changed
		if (nodes == null || nodeArray == null || dimension == null 
				|| !Arrays.equals(dimension, dim)) {
			dimension = new int[] { dim[0], dim[1], dim[2] };
			nodeArray = new CartanParameterNode[dim[0]][dim[1]][dim[2]];
			nodes = new LinkedList<CartanParameterNode>();
			thresholdedNodes = new LinkedList<CartanParameterNode>();
		}

		if (createBlender) {
			gridBlender = new GridBlender(dimension);
		}
		
		nodes.clear();
		thresholdedNodes.clear();
		int i = 0;
		for (CartanParameterNode node : helicoidNodes) {
			
			// Keep a copy of this node
			CartanParameterNode hpn = new CartanParameterNode(node);
			hpn.setIndex(i);
//			hpn.set(type, node);

			// Only keep clean nodes
			if (Double.isNaN(node.getError())) {
				hpn.setGarbage(true);
//				nodeArray[pt.x][pt.y][pt.z] = null;
//				continue;
			}
			
			nodes.add(hpn);

			// Update the volumetric array
			Point3i pt = dtoi(node.getPosition());
			nodeArray[pt.x][pt.y][pt.z] = hpn;

			// Update thresholded nodes
//			thresholdedNodes.add(new CartanParameterNode(hpn));
			
			i++;
		}

		double e = getMaxError(nodes);
		errorThreshold.hold(true);
		errorThreshold.setMaximum(e);
		errorThreshold.hold(false);
		
		// This will take care of the update
//		setAutomaticErrorThreshold(false);
		errorThreshold.setValue(e);
		update();
		
		CartanParameter mean = getMeanParameter(thresholdedNodes);
		mean.add(getParameterStandardDeviation(thresholdedNodes));
		mean.getc121() *= 2;
		mean.getc122() *= 2;
		mean.getc123() *= 15;
		mean.alpha = getMeanError();
		mean.abs();
		displayMax.set(mean);		
	}

	public void setType(ParameterType type) {
		for (int i = 0; i < 4; i++)
			showCurvatures[i].setValue(false);
		
		showCurvatures[type.ordinal()].setValue(true);
	}

	private Point3i dtoi(Point3d p) {
		Point3i pt = new Point3i();
		pt.x = (int) Math.round(p.x);
		pt.y = (int) Math.round(p.y);
		pt.z = (int) Math.round(p.z);

		return pt;
	}

	private boolean isValidPoint(Point3i p) {
		if (p.x >= dimension[0] || p.y >= dimension[1] || p.z >= dimension[2]
				|| p.x < 0 || p.y < 0 || p.z < 0) {
			return false;
		}

		return true;
	}

	private void setRenderingVolume() {
		if (voxelBox == null) return;
		
		for (int i = 0; i < 4; i++) {
			if (showCurvatures[i].getValue()) {
//				System.out.println("Setting rendering volume to " + ParameterType.values()[i].toString());
				renderer.setVolume(renderingVolumes[i], voxelBox.getMask(), voxelBox);
				break;
			}
		}
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		String title = "Helicoid Box";
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), title));
		
		vfp.add(display.getControls());

		vfp.add(useVolumeRenderer.getControls());
		vfp.add(renderer.getControls());
		
//        vfp.add(ecbHelicoid.getControls());

		JPanel parampanel = new JPanel(new GridLayout(2, 3));
		ParameterListener cl = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				setRenderingVolume();
			}
		};
		for (int i = 0; i < 4; i++) {
			parampanel.add(showCurvatures[i].getControls());
			showCurvatures[i].addParameterListener(cl);
		}
		
		vfp.add(parampanel);
		
		// vfp.add(showAlpha.getControls());

//		vfp.add(overlay.getControls());
		vfp.add(opacity.getSliderControls());
//		vfp.add(contrast.getSliderControls());
		vfp.add(interpolateFit.getControls());

		// vfp.add(cutoff_min.getControls());
		// vfp.add(cutoff_max.getControls());

		VerticalFlowPanel vfpmag = new VerticalFlowPanel();
		
//		Jpanel cbp = new CollapsibleBorderPanel(vfpmag,
//				"Parameter limits");

		vfpmag.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Parameter Limits", TitledBorder.LEFT, TitledBorder.CENTER));

		JPanel pmin = displayMin.getControls();
		JPanel pmax = displayMax.getControls();
		
		pmin.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "min"));
		pmax.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "max"));

		vfpmag.add(errorThreshold.getSliderControlsExtended());
		
		JButton btnMeanError = new JButton("to mean + " + GreekToolset.GreekLetter.SIGMA2);
		vfpmag.add(btnMeanError);
		btnMeanError.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setAutomaticErrorThreshold();
			}
		});
		
//		vfpmag.add(pmin);
		vfpmag.add(pmax);

		vfp.add(vfpmag);

		CollapsiblePanel cp = new CollapsiblePanel(vfp.getPanel(), title);
//		cp.collapse();
		return cp;
	}

	private void holdDisplay(boolean b) {
		for (int i = 0; i < 4; i++)
			showCurvatures[i].hold(b);
	}
	
	public void clear() {
		clear(ecbHelicoid.getSelected());
	}
	
	public void clear(ParameterType type) {
//		for (CartanParameterNode node : nodes) {
//			node.set(type, 0);
//		}
		nodes.clear();
		thresholdedNodes.clear();
		
		nodeArray = new CartanParameterNode[dimension[0]][dimension[1]][dimension[2]];

		set(dimension, nodes, ParameterType.ALL, gridBlender != null);
	}

	private Point mousePoint = new Point();

	private boolean mouseDown = false;
	
	public void doPick(Point p, boolean mouseDown) {
		mousePoint = p;
		this.mouseDown = mouseDown;
	}

	public CartanParameter getMax() {
		return new CartanParameter(max);
	}

	public CartanParameter getMin() {
		return new CartanParameter(min);
	}

	public int size() {
		return nodes.size();
	}

	private CartanParameterNode pickedNode = null;

	private GridBlender gridBlender = null;

	private GLAutoDrawable drawable = null;
	private GL2 gl = null;
	private boolean[] show;
	private boolean selectionMode = false;
	private JoglTextRenderer textRenderer;
	
	public void display(GLAutoDrawable drawable, VoxelBox box) {
		display(drawable, false, box);
	}

	public void renderVolume(GLAutoDrawable drawable) {
		if (useVolumeRenderer.getValue()) {
			gl = drawable.getGL().getGL2();
			renderer.display(drawable);
		}
	}
	
	private void display(final GLAutoDrawable drawable, final boolean selectionMode, VoxelBox box) {

		if (!display.getValue()) 
			return;
		
		voxelBox = box;
		
		this.drawable = drawable;
		this.selectionMode = selectionMode;

		gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);

		show = new boolean[] { showCurvatures[0].getValue(),
				showCurvatures[1].getValue(), showCurvatures[2].getValue(),
				showCurvatures[3].getValue() };

		// mag.scale(1 - contrast.getValue());
		// mag.scale(Math.log(2 - contrast.getValue()));

		if (interpolateFit.getValue() && !selectionMode && gridBlender != null) {
			gridBlender.display(drawable, box, opacity.getValue(),
					1 - Math.pow(1 - contrast.getValue(), 2));
		}

//		gl.glDisable(GL2.GL_LIGHTING);
//		gl.glPointSize(15f);
//		gl.glColor4d(1, 1, 1, 1);
//		gl.glBegin(GL2.GL_POINTS);
//		for (CartanParameterNode node : nodes) {
//			if (node.isGarbage()) {
//				gl.glVertex3d(node.getPosition().x, node.getPosition().y, node.getPosition().z);
//			}
//		}
//		gl.glEnd();
		
		if (selectionMode || !useVolumeRenderer.getValue()) {
			box.voxelProcess(this);
		}

		if (!selectionMode && isPicking())
			ScenePicker.pick(drawable, mousePoint, this);

	}
	
	private List<String> text = new LinkedList<String>();
	
	public void drawPickedNode(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		if (!display.getValue()) return;
		
		// Draw text info
		if (textRenderer == null) {
			Font font = new Font("Consolas", Font.PLAIN, 14);
			textRenderer = new JoglTextRenderer(font, true, false);
		}

		text = getInfo();

		// Add delimiter
		String delimiter = "-";
		String fill = TextToolset.fill("", delimiter, text);
		text.add(0, fill);

		textRenderer.setColor(0.5f, 0.9f, 0f, 1f);
		textRenderer.drawTopLeft(text, drawable);

		if (pickedNode != null ) {
			double pcs = 0.5;
			double[] color = new double[] { pcs, pcs, pcs, 0.6 };
			double size = 1.05;

			gl.glEnable(GL2.GL_CULL_FACE);
			gl.glDisable(GL2.GL_LIGHTING);
			gl.glPushMatrix();
			Point3d o = pickedNode.getPosition();
			gl.glTranslated(o.x + 0.5, o.y + 0.5, o.z + 0.5);
			gl.glColor4d(color[0], color[1], color[2], color[3] - 0.1);
			Cube.draw(drawable, size);
			gl.glColor4d(0, 0, 0, 1);
//			Cube.drawWireframe(drawable, size);

			gl.glPopMatrix();
			
//			s += "[Target selected - #" + pickedNode.getIndex() + "]\n";
//			s += "voxel = " + dtoi(pickedNode.getPosition()) + "\n";
//			text.clear();
			int lc = text.size();
			text.clear();
			for (int i = 0; i < lc; i++)
				text.add("");
			
			text.add("[Target # " + pickedNode.getIndex() + " selected - " + dtoi(pickedNode.getPosition()) + "]");
			text.add("fit : " + new CartanParameter(pickedNode).toString());
			text.add("mag = " + pickedNode.length());
			text.add("err = " + pickedNode.getError());
			text.add("min = " + getMin().toStringShort());
			text.add("max = " + getMax().toStringShort());
			
			// Add delimiter
			fill = TextToolset.fill("", delimiter, text);
			text.add(lc, fill);

			textRenderer.setColor(1f, 1f, 0f, 1f);
			
//			String filling = TextToolset.fill("", " \n", 2*TextToolset.countLines(ms));
			textRenderer.drawTopLeft(text, drawable);
		}
	}
	@Override
	public void processHit(Integer name) {
		if (name != null) {
			// System.out.println(mousePoint + ": picked name = " + name);
			pickedNode = new CartanParameterNode(nodes.get(name));
		} else {
			// Do nothing, keep previous valid
			pickedNode = null;
		}
	}
	
	public void pickNext() {
		if (pickedNode == null) return;
		
		int ind = pickedNode.getIndex() + 1;
		
		if (ind < nodes.size()) {
			pickedNode = nodes.get(ind);
		}
		else {
			pickedNode = nodes.get(0);
		}
	}

	public CartanParameterNode getPickedNode() {
		return pickedNode;
	}

	@Override
	public void renderInSelectionMode(GLAutoDrawable drawable) {
		display(drawable, true, voxelBox);
	}

	private boolean isPicking = false;
	
	public void setPicking(boolean picking) {
		isPicking = picking;
	}
	
	public boolean isPicking() {
		return isPicking;
	}

	@Override
	public void process(int x, int y, int z) {
		CartanParameterNode node = nodeArray[x][y][z];
		
		if (node == null || node.isGarbage()) return;

		if (selectionMode) {
//				if (showKT.getValue() || showKN.getValue() || showKB.getValue()) {
						gl.glPushName(node.getIndex());
						gl.glPushMatrix();
						Point3d o = node.getPosition();
						gl.glTranslated(o.x, o.y, o.z);
						Cube.draw(drawable, 1);
						gl.glPopMatrix();
						gl.glPopName();
						
//			}
		} 
		else if (!interpolateFit.getValue()) {
				HelicoidBox.display(drawable, !overlay.getValue() ? opacity
						.getValue() : 1, node, min, displayMax, max.alpha, node.getPosition(),
						span, show);
		}
	}

	@Override
	public boolean isValid(Point3d origin, Vector3d span) {
		if (nodes.size() == 0 || dimension[0] < origin.x + span.x || dimension[1] < origin.y + span.y || dimension[2] < origin.z + span.z)
			return false;
		
		return true;
	}

	public double getMeanError(List<CartanParameterNode> nodes) {
		double error = 0;
		int i = 0;
		for (CartanParameterNode node : nodes) {
			if (node.isGarbage()) continue;
			
			double ne = node.getError();

			if (Double.isNaN(ne)) {
				continue;
			}
			
			error += ne;
			i++;
		}
		
		return error / i;
	}

	public double getMeanError() {
		return getMeanError(thresholdedNodes);
	}
	
	private double getStandardDeviation(List<CartanParameterNode> nodes) {
		double mean = getMeanError(nodes);
		
		double var = 0;
		int i = 0;
		for (CartanParameterNode node : nodes) {
			if (node.isGarbage()) continue;
			
			double ne = node.getError();

			if (Double.isNaN(ne)) {
				continue;
			}

			var += (ne - mean) * (ne - mean);
			i++;
		}
		
		return Math.sqrt(var / i);
	}
	
	private double getMaxError(List<CartanParameterNode> nodes) {
		double max = 0;
		
		for (CartanParameterNode node : nodes) {
			if (node.isGarbage()) continue;

			double nl = node.getError();
			
			if (Double.isNaN(nl)) {
				continue;
			}
			
			if (nl > max) max = nl;
		}
		
		return max;
	}

	private double getMinError(List<CartanParameterNode> nodes) {
		double min = 0;
		
		for (CartanParameterNode node : nodes) {
			double nl = node.getError();
			if (nl < min) min = nl;
		}
		
		return min;
	}

	/**
	 * @return the current selected helicoid parameter type.
	 */
	public ParameterType getSelectedParameterType() {
		return ecbHelicoid.getSelected();
	}
	
	public List<CartanParameterNode> getThresholdedNodes() {
		return thresholdedNodes;
	}
	
	public void setVisible(boolean b) {
		display.setValue(b);
	}

	public boolean usingVolumeRenderer() {
		return useVolumeRenderer.getValue();
	}
	
	public void init(GLAutoDrawable drawable) {
		renderer.init(drawable);
	}
}

