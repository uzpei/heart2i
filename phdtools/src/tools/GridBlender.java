package tools;

import gl.geometry.WorldAxis;
import gl.geometry.primitive.Cube;
import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameter.ParameterType;
import helicoid.parameter.HelicoidParameterNode;

import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import volume.PerVoxelMethod;
import voxel.VoxelBox;


public class GridBlender implements PerVoxelMethod {

	private double[][][][] colors;
	
	private int[] dimension;

	public GridBlender(int[] dimension) {
		// Create the color volume
		colors = new double[dimension[0] + 1][dimension[1] + 1][dimension[2] + 1][4];

		this.dimension = dimension;
	}
	
	public void set(List<HelicoidParameterNode> nodes, HelicoidParameter min, HelicoidParameter max) {

		HelicoidParameter div = new HelicoidParameter();
		for (int i = 0; i < 4; i++) {
			div.set(i, Math.min(Math.abs(max.get(i)), ParameterType.fromIndex(i).getMaximumMagnitude()));
			if (div.get(i) == 0)
				div.set(i, 1);
		}
		
		// First clear color buffer
		for (int i = 0; i < dimension[0]; i++)
			for (int j = 0; j < dimension[1]; j++)
				for (int k = 0; k < dimension[2]; k++)
					for (int l = 0; l < 4; l++)
						colors[i][j][k][l] = 0;
		
		Point3i p = new Point3i();
		for (HelicoidParameterNode node : nodes) {
			Point3d pt = node.getPosition();
			p.x = (int) Math.round(pt.x);
			p.y = (int) Math.round(pt.y);
			p.z = (int) Math.round(pt.z);

//			colors[p.x][p.y][p.z][0] = 0;
//			colors[p.x][p.y][p.z][1] = 0;
//			colors[p.x][p.y][p.z][2] = 0;
//			colors[p.x][p.y][p.z][3] = 0;
			
			if (node.KT > min.KT) {
				colors[p.x][p.y][p.z][0] = Math.abs(node.KT / max.KT);
			}

			if (node.KN > min.KN) {
				colors[p.x][p.y][p.z][1] = Math.abs(node.KN / max.KN);
			}

			if (node.KB > min.KB) {
				colors[p.x][p.y][p.z][2] = Math.abs(node.KB / max.KB);
			}

		}
	}

	public void display(GLAutoDrawable drawable, VoxelBox box, double opacity, double contrast) {
		this.drawable = drawable;
		gl = drawable.getGL().getGL2();

		
		WorldAxis.display(gl);
		
		this.opacity = opacity;
		this.contrast = contrast;
		
		box.voxelProcess(this);
	}

	@Override
	public boolean isValid(Point3d origin, Vector3d span) {
		if (dimension[0] < origin.x + span.x || dimension[1] < origin.y + span.y || dimension[2] < origin.z + span.z)
			return false;
		
		return true;
	}

	private GL2 gl;
	private GLAutoDrawable drawable;
	private Point3i voxel = new Point3i();

	private int[] v0 = new int[] { 1, 1, 1 };
	private int[] v1 = new int[] { 1, 0, 1 };
	private int[] v2 = new int[] { 1, 0, 0 };
	private int[] v3 = new int[] { 1, 1, 0 };
	private int[] v4 = new int[] { 0, 1, 0 };
	private int[] v5 = new int[] { 0, 1, 1 };
	private int[] v6 = new int[] { 0, 0, 1 };
	private int[] v7 = new int[] { 0, 0, 0 };

	@Override
	public void process(int x, int y, int z) {
		
//		if (x != 0 || y != 0 || z != 0) return;
		
		gl.glPushMatrix();

		gl.glTranslated(x + 0.5, y + 0.5, z + 0.5);
		double ws = 0.2;
		gl.glColor4d(ws, ws, ws, 1);
		Cube.drawWireframe(drawable, 1);

		gl.glTranslated(-0.5, -0.5, -0.5);

		gl.glBegin(GL2.GL_QUADS);

		voxel.set(x, y, z);

		// Right face (+Y)
		gl.glNormal3i(0, 1, 0);
		gl.glColor4dv(getColor(voxel, v3), 0);
		gl.glVertex3iv(v3, 0);
		gl.glColor4dv(getColor(voxel, v4), 0);
		gl.glVertex3iv(v4, 0);
		gl.glColor4dv(getColor(voxel, v5), 0);
		gl.glVertex3iv(v5, 0);
		gl.glColor4dv(getColor(voxel, v0), 0);
		gl.glVertex3iv(v0, 0);

		// Left Face (-Y)
		gl.glNormal3i(0, -1, 0);
		gl.glColor4dv(getColor(voxel, v1), 0);
		gl.glVertex3iv(v1, 0);
		gl.glColor4dv(getColor(voxel, v6), 0);
		gl.glVertex3iv(v6, 0);
		gl.glColor4dv(getColor(voxel, v7), 0);
		gl.glVertex3iv(v7, 0);
		gl.glColor4dv(getColor(voxel, v2), 0);
		gl.glVertex3iv(v2, 0);

		// Front Face (+X)
		gl.glNormal3i(1, 0, 0);
		gl.glColor4dv(getColor(voxel, v2), 0);
		gl.glVertex3iv(v2, 0);
		gl.glColor4dv(getColor(voxel, v3), 0);
		gl.glVertex3iv(v3, 0);
		gl.glColor4dv(getColor(voxel, v0), 0);
		gl.glVertex3iv(v0, 0);
		gl.glColor4dv(getColor(voxel, v1), 0);
		gl.glVertex3iv(v1, 0);

		// Back Face (-X)
		gl.glNormal3i(1, 0, 0);
		gl.glColor4dv(getColor(voxel, v4), 0);
		gl.glVertex3iv(v4, 0);
		gl.glColor4dv(getColor(voxel, v7), 0);
		gl.glVertex3iv(v7, 0);
		gl.glColor4dv(getColor(voxel, v6), 0);
		gl.glVertex3iv(v6, 0);
		gl.glColor4dv(getColor(voxel, v5), 0);
		gl.glVertex3iv(v5, 0);

		// Top Face (+Y)
		gl.glNormal3i(0, 1, 0);
		gl.glColor4dv(getColor(voxel, v1), 0);
		gl.glVertex3iv(v1, 0);
		gl.glColor4dv(getColor(voxel, v0), 0);
		gl.glVertex3iv(v0, 0);
		gl.glColor4dv(getColor(voxel, v5), 0);
		gl.glVertex3iv(v5, 0);
		gl.glColor4dv(getColor(voxel, v6), 0);
		gl.glVertex3iv(v6, 0);

		// Bottom Face (-Y)
		gl.glNormal3i(0, -1, 0);
		gl.glColor4dv(getColor(voxel, v7), 0);
		gl.glVertex3iv(v7, 0);
		gl.glColor4dv(getColor(voxel, v4), 0);
		gl.glVertex3iv(v4, 0);
		gl.glColor4dv(getColor(voxel, v3), 0);
		gl.glVertex3iv(v3, 0);
		gl.glColor4dv(getColor(voxel, v2), 0);
		gl.glVertex3iv(v2, 0);

		gl.glEnd();
		
		gl.glPopMatrix();
	}

	private double contrast = 1.0;

	private double[] color = new double[4];
	
	private double opacity = 0;
	
	private double[] getColor(Point3i voxel, int[] v) {
		double[] c = colors[voxel.x + v[0]][voxel.y + v[1]][voxel.z + v[2]];
		for (int i = 0; i < 3; i++)
			color[i] = contrast * c[i];
		
		color[3] = opacity;
		return color;
	}

}
