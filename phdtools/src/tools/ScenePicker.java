package tools;

import gl.geometry.GLPickable;

import java.awt.Point;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

import com.jogamp.common.nio.Buffers;

/**
 * @author epiuze
 */
public class ScenePicker {
    private static GLU glu = new GLU();

    private static final int pickbufferSize = 32;

    private static int pickSize = 1;
    
//    /**
//     * Hits are stored in this selection buffer.
//     */
//    private static IntBuffer selectionBuffer = ByteBuffer.allocateDirect(
//            BufferUtil.SIZEOF_INT * pickbufferSize).order(
//            ByteOrder.nativeOrder()).asIntBuffer();

    /**
     * Perform picking on this drawing pass
     * @param drawable
     * @param model 
     * @pre size must have been set
     */
    private static void initPicking(GLAutoDrawable drawable, int x, int y, IntBuffer selectionBuffer) {
		GL2 gl = drawable.getGL().getGL2();
        
        int[] viewport = new int[4];
        double[] currentProjectionMatrix = new double[16];
        gl.glGetDoublev(GL2.GL_PROJECTION_MATRIX, currentProjectionMatrix, 0);

        /*
         * Enter SELECT MODE
         */
        
        gl.glSelectBuffer(selectionBuffer.capacity(), selectionBuffer);
        gl.glRenderMode(GL2.GL_SELECT);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();

        gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
        glu.gluPickMatrix(x, viewport[3] - y,
                pickSize, pickSize, viewport, 0);

        gl.glMultMatrixd(currentProjectionMatrix, 0);

        gl.glMatrixMode(GL2.GL_MODELVIEW);

        gl.glInitNames();

    }

    private static Integer stopPicking(GLAutoDrawable drawable, IntBuffer selectionBuffer) {
		GL2 gl = drawable.getGL().getGL2();
        
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glFlush();

        // Restore rendering mode
        int hits = gl.glRenderMode(GL2.GL_RENDER);

        Integer name = processHits(hits, selectionBuffer);
        
        return name;
    }

    private static Integer processHits(int hits, IntBuffer selectionBuffer) {

    	Integer name = null;
    	
    	if (hits == 0) return null;

    	/*
		 * Each hit record contains the following information stored as unsigned
		 * int: 
		 * 
		 * Number of names in the name stack for this hit record 
		 * 
		 * Minimum depth value of primitives (range 0 to 232-1) 
		 * 
		 * Maximum depth value of primitives (range 0 to 232-1)
		 * 
		 * Name stack contents (one name for each unsigned int).
		 */
    	
        int mindepth = Integer.MAX_VALUE;
        
//        for (int i = 0; i < selectionBuffer.limit(); i++) {
//        	System.out.print(selectionBuffer.get(i) + ", ");
//        }
//        if (hits > 0) System.out.println();
        
        int zmin, zmax;
        for (int i = 0; i < hits; i++) {
        	zmin = selectionBuffer.get(4 * i + 1);
        	zmax = selectionBuffer.get(4 * i + 2);
        	
            if (zmin < mindepth) {
            	name = selectionBuffer.get(4 * i + 3);
                mindepth = zmin;
            }
        }

        return name;
    }

    public static Integer pick(GLAutoDrawable drawable, Point p, GLPickable pickable) {
        IntBuffer selectionBuffer = ByteBuffer.allocateDirect(
                Buffers.SIZEOF_INT * pickbufferSize).order(
                ByteOrder.nativeOrder()).asIntBuffer();

        initPicking(drawable, p.x, p.y, selectionBuffer);
        pickable.renderInSelectionMode(drawable);
        Integer name = stopPicking(drawable, selectionBuffer);
        
        pickable.processHit(name);
        
        return name;
    }
}
