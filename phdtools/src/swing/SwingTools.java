package swing;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class SwingTools {

	public static Component encapsulate(Component c) {
		JPanel p = new JPanel(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.weightx = 0;
		gbc.weighty = 0;
		p.add(c, gbc);

		// Horizontal filling
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.weightx = 1;
		JPanel fillingPanel = new JPanel();
		fillingPanel.setOpaque(false);
		p.add(fillingPanel, gbc);

		p.setOpaque(false);

		return p;
	}

	public static void createBorder(JComponent component, String string) {
		component.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), string, TitledBorder.LEFT,
				TitledBorder.CENTER));
	}

	/**
	 * Get a screen shot from a component.
	 * 
	 * @param component
	 * @return
	 */
	public static BufferedImage getScreenShot(Component component) {

		BufferedImage image = new BufferedImage(component.getWidth(),
				component.getHeight(), BufferedImage.TYPE_INT_RGB);
		// call the Component's paint method, using
		// the Graphics object of the image.
		component.paint(image.getGraphics()); // alternately use .printAll(..)
		return image;
	}

	/**
	 * Get a screen shot from a component and paint it to a label.
	 * 
	 * @param component
	 * @return
	 */
	public static JLabel getScreenShotLabel(Component component) {
		BufferedImage img = getScreenShot(component);
		JLabel label = new JLabel(new ImageIcon(img.getScaledInstance(
				img.getWidth(null) / 2, img.getHeight(null) / 2,
				Image.SCALE_SMOOTH)));

		return label;
	}
	
	public static void saveScreenShotPNG(Component component, String filepath) {
		try {
			BufferedImage image = getScreenShot(component);
			File outputfile = new File(filepath);
			ImageIO.write(image, "png", outputfile);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
