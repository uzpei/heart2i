package swing.event;

import java.awt.Component;

/**
 * Interaction interface to allow mouse and key listeners to be attached to 
 * a component.  
 * 
 * @author epiuze
 */
public interface Interactor
{
    /**
     * Attach a listener to the provided component.
     * @param component
     */
    public void attach(Component component);

    /**
     * Detach a listener from the provided component.
     * @param component
     */
    public void detach(Component component);

}