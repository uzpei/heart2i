package swing.parameters;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import swing.component.EnumComboBox;
import swing.component.HorizontalFlowPanel;
import tools.TextToolset;
import tools.geom.MathToolset;

public class PrimitiveControl extends Parameter implements PropertyChangeListener, ChangeListener {

	public enum PrimitiveType { BOOLEAN, COLOR, DOUBLE, FLOAT, INTEGER, VECTOR }
	
	protected final static boolean DefaultChecked = true;

	private PrimitiveType type;
    private double valueD, defaultValueD, minValueD, maxValueD;
    private float valueF, defaultValueF, minValueF, maxValueF;
    private int valueI, defaultValueI, minValueI, maxValueI;
    private boolean valueB, defaultValueB;
    
    private BooleanParameter booleanp;
    private ParameterListener booleanListener;
    
    /**
     * The slider that displays/controls the parameter.
     */
    private JSlider slider;
    
    /**
     * Whether this slider is a logarithmic scale slider.
     */
    private boolean isLogarithmic;

    /**
     * Default number of ticks for number sliders.
     */
    public static int DEFAULT_SLIDER_TICKS = 20000;
    
    /**
     * Default floating point format as a <code>NumberFormat</code>.
     */
    public static NumberFormat DEFAULT_FLOATING_POINT_NUMBER_FORMAT = new DecimalFormat("0.0000000");

	/**
	 * Default floating point format as a <code>NumberFormat</code>.
	 */
	public static NumberFormat DEFAULT_INTEGER_NUMBER_FORMAT = new DecimalFormat("0");

    /**
     * Default width in pixels of the label to the left of a slider in a
     * slideNText panel. If the label doesn't fit in this width a tool tip with
     * the parameter name will be set.
     */
    public static int DEFAULT_SLIDER_LABEL_SIZE = 140;
    
    /**
     * The text field that displays this parameter's value.
     */
    private JFormattedTextField textFieldValue;
    
    /**
     * If the "checked" property was changed before creating the controls, make sure we use that boolean.
     */
    private boolean[] earlyChecked = new boolean[] { false, DefaultChecked };
    
    private JPanel panel = null;
    private JLabel label;

    public PrimitiveControl(PrimitiveType type, String name, float defaultValue, float minValue, float maxValue) {
        super(name);
    	checkNumberType(type);
        initialize(type, defaultValue, minValue, maxValue);
    }

    public PrimitiveControl(PrimitiveType type, String name, int defaultValue, int minValue, int maxValue) {
        super(name);
    	checkNumberType(type);
        initialize(type, defaultValue, minValue, maxValue);
    }

    public PrimitiveControl(PrimitiveType type, String name, boolean defaultValue) {
        super(name);
        checkBooleanType(type);
        initialize(type, defaultValue ? 1 : 0, 0, 1);
    }
    
    public PrimitiveControl(PrimitiveType type, String name, double defaultValue, double minValue, double maxValue) {
    	super(name);
    	checkNumberType(type);
    	initialize(type, defaultValue, minValue, maxValue);
    }
    
    private void checkNumberType(PrimitiveType type) {
    	List<PrimitiveType> typeExclusion = Arrays.asList(PrimitiveType.BOOLEAN, PrimitiveType.VECTOR);
    	for (PrimitiveType excluded : typeExclusion) {
        	if (type == excluded) {
        		throw new RuntimeException("This constructor should not be used with an " + excluded + " primitive type. Available types are " + TextToolset.toString(typeExclusion, " "));
        	}
    	}
    }
    
    private void checkBooleanType(PrimitiveType type) {
    	PrimitiveType typeValidation = PrimitiveType.BOOLEAN;
    	if (type != typeValidation) {
    		throw new RuntimeException("This constructor should be used with a " + typeValidation + " primitive type.");
    	}
    }

    private void initialize(PrimitiveType type, double defaultValue, double minValue, double maxValue) {        
        booleanp = new BooleanParameter("", DefaultChecked);
        
        switch (type) {
        case BOOLEAN:
            textFieldValue = null;
            this.defaultValueB = defaultValue > 0;
            this.valueB = this.defaultValueB;

        	break;
        case DOUBLE:
            textFieldValue = new JFormattedTextField(DEFAULT_FLOATING_POINT_NUMBER_FORMAT);
            this.defaultValueD = defaultValue;
            if (defaultValueD < minValueD) this.defaultValueD = minValue;
            if (defaultValueD > maxValueD) this.defaultValueD = maxValue;
            this.minValueD = minValue;
            this.maxValueD = maxValue;
            this.valueD = defaultValue;
        	break;
        case FLOAT:
            textFieldValue = new JFormattedTextField(DEFAULT_FLOATING_POINT_NUMBER_FORMAT);
            this.defaultValueF = (float) defaultValue;
            if (defaultValueF < minValueF) this.defaultValueF = (float) minValue;
            if (defaultValueF > maxValueF) this.defaultValueF = (float) maxValue;
            this.minValueF = (float) minValue;
            this.maxValueF = (float) maxValue;
            this.valueF = (float) defaultValue;
        	break;
        case INTEGER:
            textFieldValue = new JFormattedTextField(DEFAULT_INTEGER_NUMBER_FORMAT);
            this.defaultValueI = MathToolset.roundInt(defaultValue);
            if (defaultValueI < minValueI) this.defaultValueI = MathToolset.roundInt(minValue);
            if (defaultValueI > maxValueI) this.defaultValueI = MathToolset.roundInt(maxValue);
            this.minValueI = MathToolset.roundInt(minValue);
            this.maxValueI = MathToolset.roundInt(maxValue);
            this.valueI = MathToolset.roundInt(defaultValue);
        	break;
        case VECTOR:
           	throw new NotImplementedException();
        case COLOR:
        	throw new NotImplementedException();
        }
    }

    private boolean beginSetting() {
    	return !isBeingSet();
    }
    
    @Override
	protected void finishSetting() {
        updateView();
        super.finishSetting();
        notifyListeners();
    }

	/**
     * Sets the value of this parameter. Registered
     * <code>ParameterListener</code> s are notified of the change. The value
     * is clamped to [minValue, maxValue] where minValue and maxValue are the
     * parameter's minimum and maximum allowable values respectively.
     * @param value
     *        the new value of this parameter
     */
    private void setNumber(double value) {
        if (!beginSetting()) return;
        
        switch (type) {
        case BOOLEAN:
           	this.valueB = value > 0;
        case DOUBLE:
            this.valueD = MathToolset.clamp(value, minValueD, maxValueD);
        	break;
        case FLOAT:
            this.valueF = (float) MathToolset.clamp(value, minValueF, maxValueF);
        	break;
        case INTEGER:
            this.valueI = MathToolset.roundInt(MathToolset.clamp(value, minValueI, maxValueI));
        	break;
        case VECTOR:
           	throw new NotImplementedException();
        case COLOR:
        	throw new NotImplementedException();
        }
        
        finishSetting();
    }
    
    public void setMinimum(double minValue) {
       if (!beginSetting()) return;
        
        switch (type) {
        case BOOLEAN:
        	throw new RuntimeException();
        case DOUBLE:
        	this.minValueD = minValue;
            this.valueD = MathToolset.clamp(valueD, minValueD, maxValueD);
        	break;
        case FLOAT:
        	this.minValueF = (float) minValue;
            this.valueF = (float) MathToolset.clamp(valueF, minValueF, maxValueF);
        	break;
        case INTEGER:
        	this.minValueI = MathToolset.roundInt(minValue);
            this.valueI = MathToolset.roundInt(MathToolset.clamp(valueI, minValueI, maxValueI));
        	break;
        case VECTOR:
           	throw new NotImplementedException();
        case COLOR:
        	throw new NotImplementedException();
        }
        
        finishSetting();
    }

    public void setValue(double value) {
    	setNumber(value);
    }

    public void setValue(boolean value) {
    	setNumber(value ? 1 : 0);
    }

    /**
     * Gets the minimum value that this parameter may take on.
     * @return the minimum value of this parameter
     */
    public double getMinimum() {
        return minValue;
    }

    /**
     * Gets the maximum value of this parameter.
     * @return the maximum value that this parameter may take on
     */
    public double getMaximum() {
        return maxValue;
    }

    /**
     * Gets the current value of this parameter
     * @return the current value.
     */
    public double getValue() {
        return value;
    }
    
    
    public JPanel getSliderControls() {
        return getSliderControls("", false, false, false, false);
    }
    
    public void setLabelText(String text) {
    	label.setText(text);
    }
    
    public JPanel getSliderControls(boolean isLog) {
    	return getSliderControls("", isLog, false, DefaultChecked, false);
    }
    

    /**
     * Gets a slider and text box control for this double parameter.
     * @param checkboxText 
     * @return controls
     */
    public JPanel getSliderControls(String checkboxText, boolean isLog, boolean extended, boolean checked, boolean vertical) {
        if (panel != null) return panel;
        
        // Create the panel that holds the ui component
		final PrimitiveControl me = this;
		panel = new JPanel() {
			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				me.setEnabled(enabled);
			}
		};
		
		
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);

        NumberFormat format = DEFAULT_FLOATING_POINT_NUMBER_FORMAT;
        format.setMaximumFractionDigits(extended ? 4 : 5);
        format.setMinimumFractionDigits(extended ? 4 : 5);
        
        this.isLogarithmic = isLog;

        slider = new JSlider(vertical? SwingConstants.VERTICAL : SwingConstants.HORIZONTAL, 0, DEFAULT_SLIDER_TICKS, DEFAULT_SLIDER_TICKS / 2);
        slider.addChangeListener(this);
        
        // Set the text field dimensions
        Dimension dtxt = new Dimension(textFieldValue.getPreferredSize());
//        dtxt.width = extended ? DEFAULT_SLIDER_LABEL_WIDTH / 2 : DEFAULT_SLIDER_LABEL_WIDTH;
        dtxt.width = DEFAULT_SLIDER_LABEL_SIZE / 2 - 10;
        textFieldValue.setPreferredSize(dtxt);
        textFieldValue.setMinimumSize(dtxt);
        textFieldValue = new JFormattedTextField(format);
        textFieldValue.setText( "" + value );
        textFieldValue.addPropertyChangeListener("value", this);
        
        // Check the width of the text label for truncation. Display a tooltip text
        // if the width of the label goes beyond the defined threshold.
        label = new JLabel(getName(), JLabel.CENTER);
        Dimension dlbl = new Dimension(label.getPreferredSize());
        if (dlbl.width > DEFAULT_SLIDER_LABEL_SIZE) {
            label.setToolTipText( getName() );
        }
        // Use a fixed width for the text label
        dlbl.width = Math.min(dlbl.width, DEFAULT_SLIDER_LABEL_SIZE);
        if (getName() == "") {
        	dlbl.width = 0;
        }
        label.setPreferredSize(dlbl);
        label.setMinimumSize(dlbl);
    	label.setMaximumSize(dlbl);

        if (!vertical) {
            if (extended) {
            	booleanp.setName(checkboxText);
                // Set the value but don't notify the listeners
                boolean notifyListeners = false;
                booleanp.setValue(earlyChecked[0] ? earlyChecked[1] : checked, notifyListeners);
        		booleanp.setName(checkboxText);
            }
                        
            // Create the ui components


            /*
             * Set component dimensions.
             */
                    
            // Use a fixed minimum width for the slider
            Dimension dslider = new Dimension(slider.getPreferredSize());
            dslider.width = DEFAULT_SLIDER_LABEL_SIZE / 2;
            slider.setMinimumSize(dslider);
            slider.setPreferredSize(dslider);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.anchor = GridBagConstraints.FIRST_LINE_START;
            gbc.anchor = GridBagConstraints.NORTHWEST;
            
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridheight = 1;
            gbc.gridwidth = 1;
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.ipadx = 0;
            gbc.ipady = 0;
            gbc.weightx = 0;
            gbc.weighty = 0;

            // Text label (fixed width)
            gbc.weightx = 0;
            gbc.insets = new Insets(8, 4, 2, 4);
            panel.add(label, gbc);

            // Slider
            gbc.gridx++;
            gbc.weightx = 1;
            gbc.insets = new Insets(2, 0, 2, 0); // top, left, bottom, right
            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel.add(slider, gbc);

            // Text field
            gbc.gridx++;
//            gbc.weightx = extended ? 0 : 0;
            gbc.weightx = 0;
            gbc.insets = new Insets(2, 0, 2, 0);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel.add(textFieldValue, gbc);

            // If extended, a boolean parameter
            if (extended) {
                gbc.gridx++;
                gbc.weightx = 0;
                gbc.insets = new Insets(0, 0, 0, 0);
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.anchor = GridBagConstraints.NORTHEAST;
                gbc.fill = GridBagConstraints.NONE;
                panel.add(booleanp.getControls(2 * DEFAULT_SLIDER_LABEL_SIZE / 3), gbc);
                
            }
        }
        else {
            // Use a fixed minimum width for the slider
            Dimension dslider = new Dimension(slider.getPreferredSize());
//            dslider.width = 10;
            dslider.height = DEFAULT_SLIDER_LABEL_SIZE / 2;
            slider.setMinimumSize(dslider);
            slider.setPreferredSize(dslider);
            slider.setMaximumSize(dslider);
                        
            GridBagConstraints gbc = new GridBagConstraints();

            // Add text label
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 0;
            gbc.weighty = 0;
            gbc.fill = GridBagConstraints.NONE;
            gbc.ipadx = 1;
            gbc.insets = new Insets(8, 4, 2, 4);
            gbc.anchor = GridBagConstraints.FIRST_LINE_START;
            panel.add(label, gbc);
            gbc.ipadx = 0;

            // Add text field
            gbc.gridx = 0;
            gbc.gridy = 1;
//            gbc.insets = new Insets(2, 0, 2, 0);
//            gbc.anchor = GridBagConstraints.LINE_START;
            gbc.anchor = GridBagConstraints.FIRST_LINE_END;
            panel.add(textFieldValue, gbc);

            // Add slider
            gbc.gridx = 2;
            gbc.gridy = 0;
            gbc.gridheight = 2;
            gbc.anchor = GridBagConstraints.PAGE_START;
            gbc.fill = GridBagConstraints.NONE;
//            gbc.insets = new Insets(2, 0, 2, 0); // top, left, bottom, right
//            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel.add(slider, gbc);
            label.setHorizontalAlignment(JLabel.RIGHT);
            textFieldValue.setHorizontalAlignment(JFormattedTextField.RIGHT);
            
            // Add empty panel for filling
            gbc.gridx = 3;
            gbc.gridy = 0;
            gbc.gridheight = 2;
            gbc.weightx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
//            gbc.insets = new Insets(2, 0, 2, 0);
//            gbc.anchor = GridBagConstraints.LINE_START;
            gbc.anchor = GridBagConstraints.NORTHEAST;
            panel.add(new JPanel(), gbc);
//            panel.add(new JButton("fill"), gbc);
        }
        
        updateView();

        return panel;
    }

    
     /**
     * Gets a  text box control for this double parameter.
     * @return controls
     */
    public JPanel getControls() {
        if ( panel != null ) return panel;
        this.isLogarithmic = false;
        // Create the ui components
        label = new JLabel( getName() );
        
        textFieldValue.setText( "" + value );

        textFieldValue.addPropertyChangeListener("value", this);
        
        Dimension d;
        
        d = new Dimension(textFieldValue.getPreferredSize());
        d.width = DEFAULT_SLIDER_LABEL_SIZE;
        textFieldValue.setPreferredSize(d);
        textFieldValue.setMinimumSize(d);

        d = new Dimension(label.getPreferredSize());
        if (d.width > DEFAULT_SLIDER_LABEL_SIZE) {
            label.setToolTipText( getName() );
        }
        d.width = DEFAULT_SLIDER_LABEL_SIZE;
        label.setPreferredSize(d);
        label.setMinimumSize(d);
        
        // Create the panel that holds the ui component
        HorizontalFlowPanel hfp = new HorizontalFlowPanel();
        hfp.add( label );
        hfp.add( textFieldValue );
        panel = hfp.getPanel();
        
        updateView();

        return panel;
    }

    
    /**
     * Called by the text field when a new value is committed. The updated value
     * of the text field is passed down to the represented parameter.
     * 
     * @param evt
     *        ignored
     * @see java.beans.PropertyChangeListener#propertyChange(PropertyChangeEvent)
     */
    @Override
	public void propertyChange(PropertyChangeEvent evt) {
        Object valueO = textFieldValue.getValue();
        if (valueO instanceof Number) {
            double v = ((Number)valueO).doubleValue();
            setValue(v);
            return;
        }
    }
    
    /**
     * Prevent excessive changes by using time intervals
     */
    
    private long t0 = 0, t1 = 0;
    private long dt = (long) (10 * 1e6);
    
    /**
     * Called whenever the state of this slider's <code>JSlider</code> is
     * changed. The change in state is passed down to this slider's
     * <code>Parameter</code>
     * 
     * @param event
     *        the change event (ignored)
     */
    @Override
	public void stateChanged(ChangeEvent event) {
    	
    	t1 = System.nanoTime();
    	if (t1 - t0 < dt) {
    		return;
    	}
    	t0 = t1;
    	
        int ivalue = slider.getValue();
        int sliderRange = slider.getMaximum() - slider.getMinimum();
        double min = getMinimum();
        double max = getMaximum();
        double v;
        if (isLogarithmic) {
            min = Math.log(min);
            max = Math.log(max);
        }
        v = min + ivalue * (max - min) / sliderRange;
        if (isLogarithmic) {
            v = Math.exp(v);
        }        
        setValue(v);
    }
   
    /**
     * Updates this parameter's <code>JSlider</code> and <code>JFormattedTextField</code>
     * to reflect the range and
     * current value of this slider's parameter.
     */
    private void updateView() {
        boolean isHolding = isHolding();
        hold(true);
        
        if ( slider != null ) {
            int sliderRange = slider.getMaximum() - slider.getMinimum();
            int ivalue;
            double min = getMinimum();
            double max = getMaximum();
            double v = getValue();
            if (isLogarithmic)
            {
                min = Math.log(min);
                max = Math.log(max);
                v = Math.log(v);
            }
            ivalue = slider.getMinimum()
                     + (int)(Math.round((v - min) * sliderRange / (max - min)));
            slider.setValue(ivalue);
        }
        // update the text field
        textFieldValue.setValue(getValue());        

        hold(isHolding);
    }
 
    /**
     * Check or uncheck the checkbox linked to this parameter.
     * @param b
     */
    public void setChecked(boolean b) {
//    	if (booleanp == null) defaultChecked = b;
//    	else booleanp.setValue(b);
//        booleanp.setEnabled(b);
    	booleanp.setValue(b);
    	
    	if (panel == null) {
    		earlyChecked[0] = true;
    		earlyChecked[1] = b;
    	}
    }
    
    /**
     * @return whether this control is enabled.
     */
    public boolean isChecked() {
        return booleanp.getValue();
    }
    
    /**
     * @return the default value of this parameter.
     */
    public double getDefaultValue() {
        return defaultValue;
    }

    /**
     * Set the default value for this parameter.
     * @param v
     */
    public void setDefaultValue(double v) {
        defaultValue = v;
    }

    public void setEnabled(boolean b) {
    	if (panel == null) return;
    	
        label.setEnabled(b);
        textFieldValue.setEnabled(b);
        slider.setEnabled(b);
        booleanp.setEnabled(b);
    }

	/**
	 * 
	 */
	public void invalidate() {
		notifyListeners();
	}

    @Override
    public void addParameterListener(ParameterListener listener) {
    	booleanp.addParameterListener(listener);
    	super.addParameterListener(listener);
    }

    @Override
    public void addParameterListeners(List<ParameterListener> listeners) {
    	booleanp.addParameterListeners(listeners);
        super.addParameterListeners(listeners);
    }

	public float getFloatValue() {
		return (float) getValue();
	}
}
