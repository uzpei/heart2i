package swing.parameters;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import swing.component.VerticalFlowPanel;

public class IntParameterPoint3i extends ParameterManager {

	private IntParameter px, py, pz;
	private String name;
	
	public IntParameterPoint3i(String name, Point3i p0, Point3i pmin, Point3i pmax) {
		this.name = name;
		
		px = new IntParameter("x", p0.x, pmin.x, pmax.x);
		py = new IntParameter("y", p0.y, pmin.y, pmax.y);
		pz = new IntParameter("z", p0.z, pmin.z, pmax.z);
		
		addParameter(px);
		addParameter(py);
		addParameter(pz);
	}

	public IntParameterPoint3i(String name) {
		this(name, new Point3i(), new Point3i(), new Point3i());
	}

	public void set(Point3i p0, Point3i pmin, Point3i pmax) {
		hold(true);
		px.setValue(p0.x);
		py.setValue(p0.y);
		pz.setValue(p0.z);
		
		px.setMinimum(pmin.x);
		py.setMinimum(pmin.y);
		pz.setMinimum(pmin.z);
		
		px.setMaximum(pmax.x);
		py.setMaximum(pmax.y);
		pz.setMaximum(pmax.z);
		hold(false);
	}

	public void setBounds(Point3i pmin, Point3i pmax) {
		hold(true);
		px.setMinimum(pmin.x);
		py.setMinimum(pmin.y);
		pz.setMinimum(pmin.z);
		
		px.setMaximum(pmax.x);
		py.setMaximum(pmax.y);
		pz.setMaximum(pmax.z);
		hold(false);
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), name, TitledBorder.LEFT, TitledBorder.CENTER));
		
		vfp.add(px.getSliderControls());
		vfp.add(py.getSliderControls());
		vfp.add(pz.getSliderControls());
		
		return vfp.getPanel();
	}
	
	public Point3i getPoint3i() {
		return new Point3i(px.getValue(), py.getValue(), pz.getValue());
	}

	public void addParameterLister(ParameterListener p) {
		px.addParameterListener(p);
		py.addParameterListener(p);
		pz.addParameterListener(p);
	}

	public int[] getPoint() {
		return new int[] { px.getValue(), py.getValue(), pz.getValue() };
	}

	public void setValue(int[] array) {
		px.setValue(array[0]);
		py.setValue(array[1]);
		pz.setValue(array[2]);
	}
}
