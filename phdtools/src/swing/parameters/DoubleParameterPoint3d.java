package swing.parameters;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;

public class DoubleParameterPoint3d extends ParameterManager  {

	private final DoubleParameter px, py, pz;
	private final String name;
	
	public DoubleParameterPoint3d(String name, Point3d p0, Point3d pmin, Point3d pmax) {
		this.name = name;
		
		px = new DoubleParameter("x", p0.x, pmin.x, pmax.x);
		py = new DoubleParameter("y", p0.y, pmin.y, pmax.y);
		pz = new DoubleParameter("z", p0.z, pmin.z, pmax.z);
		
		addParameter(px);
		addParameter(py);
		addParameter(pz);
	}
	
	public DoubleParameterPoint3d(String name) {
		this(name, new Point3d(), new Point3d(), new Point3d(1, 1, 1));
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), name));
		
		vfp.add(px.getSliderControls());
		vfp.add(py.getSliderControls());
		vfp.add(pz.getSliderControls());
		
		return vfp.getPanel();
	}
	
	public void setMin(Point3d min) {
		px.setMinimum(min.x);
		py.setMinimum(min.y);
		pz.setMinimum(min.z);
	}

	public void setMax(Point3d max) {
		px.setMaximum(max.x);
		py.setMaximum(max.y);
		pz.setMaximum(max.z);
	}

	/**
	 * Set the point3d. 
	 * @param p
	 */
	public void setPoint3d(Point3d p) {
		px.setValue(p.x);
		py.setValue(p.y);
		pz.setValue(p.z);
	}

	public void setPoint3d(double ox, double oy, double oz) {
		px.setValue(ox);
		py.setValue(oy);
		pz.setValue(oz);
	}

	public Point3f getPoint3f() {
		return new Point3f((float)px.getValue(), (float)py.getValue(), (float)pz.getValue());
	}
	
	public Point3d getPoint3d() {
		return new Point3d(px.getValue(), py.getValue(), pz.getValue());
	}

	public Point3d getMax() {
		return new Point3d(px.getMaximum(), py.getMaximum(), pz.getMaximum());
	}

	public Point3d getMin() {
		return new Point3d(px.getMinimum(), py.getMinimum(), pz.getMinimum());
	}

	public Vector3d getSpan() {
		Vector3d v = new Vector3d();
		
		v.set(getMax());
		v.sub(getMin());
		
		return v;
	}
}
