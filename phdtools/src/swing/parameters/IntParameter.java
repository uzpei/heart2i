/*******************************************************************************
 * IntParameter.java
 * 
 * Created on 21-Sep-2003
 * Created by tedmunds
 * 
 ******************************************************************************/
package swing.parameters;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import swing.component.HorizontalFlowPanel;

/**
 * This {@link Parameter}subclass represents an int-valued parameter that can
 * take on values within a specified range.
 * 
 * @author epiuze (adapted from tedmunds)
 */
public class IntParameter extends Parameter implements PropertyChangeListener,
		ChangeListener {

	private int defaultValue;
	// private boolean defaultChecked = true;
	protected int value;
	protected int minValue;
	protected int maxValue;
	private BooleanParameter booleanp;
	
	private ParameterListener booleanListener;

	/**
	 * If the "checked" property was changed before creating the controls, make
	 * sure we use that boolean.
	 */
	private boolean[] earlyChecked = new boolean[] { false, DefaultChecked };

	/**
	 * Creates a new <code>IntParameter</code> with the specified name, default
	 * value, minimum value, and maximum value.
	 * 
	 * @param name
	 *            the name of this parameter
	 * @param defaultValue
	 *            the default value of this parameter
	 * @param minValue
	 *            the minimum value of this parameter
	 * @param maxValue
	 *            the maximum value of this parameter
	 */
	public IntParameter(String name, int defaultValue, int minValue,
			int maxValue) {
		super(name);
		// Make sure default is within range
		this.defaultValue = defaultValue;
		if (defaultValue < minValue)
			this.defaultValue = minValue;
		if (defaultValue > maxValue)
			this.defaultValue = maxValue;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.value = defaultValue;

		booleanp = new BooleanParameter("", DefaultChecked);
	}
	
	public IntParameter(IntParameter other) {
		super(other.name);
		// Make sure default is within range

		this.defaultValue = other.defaultValue;
		this.minValue = other.minValue;
		this.maxValue = other.maxValue;
		
		this.value = other.value;

		booleanp = new BooleanParameter(other.booleanp);
		
		addParameterListeners(other.getParameterListeners());
	}


	/**
	 * Sets the value of this parameter. Registered
	 * <code>ParameterListener</code> s are notified of the change. The value is
	 * clamped to [minValue, maxValue] where minValue and maxValue are the
	 * parameter's minimum and maximum allowable values respectively.
	 * 
	 * @param value
	 *            the new value of this parameter
	 */
	public void setValue(int value) {
		if (isBeingSet()) {
			return;
		}
		this.value = value;
		if (this.value < minValue)
			this.value = minValue;
		if (this.value > maxValue)
			this.value = maxValue;
		updateView();
		this.finishSetting();
		
		notifyListeners();
	}

	/**
	 * Set the minimum and maximum value to +-clamp and clamp those! 
	 * @param clamp
	 */
	public void setClamp(int clamp) {
		setMaximum(Math.abs(clamp));
		setMinimum(-Math.abs(clamp));
	}

	/**
	 * Sets the minimum allowed value of this parameter. The default and current
	 * value may be adjusted to comply.
	 * 
	 * @param minValue
	 */
	public void setMinimum(int minValue) {
		if (isBeingSet())
			return;
		this.minValue = minValue;
		if (defaultValue < minValue)
			this.defaultValue = minValue;
		if (value < minValue)
			value = minValue;
		updateUI();
		updateView();
		notifyListeners();
		this.finishSetting();
	}

	/**
	 * Sets the maximum allowed value of this parameter. The default and current
	 * value may be adjusted to comply.
	 * 
	 * @param maxValue
	 */
	public void setMaximum(int maxValue) {
		if (isBeingSet())
			return;
		this.maxValue = maxValue;
		if (defaultValue > maxValue)
			this.defaultValue = maxValue;
		if (value > maxValue)
			value = maxValue;
		updateUI();
		updateView();
		this.finishSetting();
		notifyListeners();
	}

	private void updateUI() {
		if (slider == null)
			return;

		slider.setMinimum(getMinimum());
		slider.setMaximum(getMaximum());
	}

	/**
	 * Gets the minimum value that this parameter may take on.
	 * 
	 * @return the minimum value of this parameter
	 */
	public int getMinimum() {
		return minValue;
	}

	/**
	 * Gets the maximum value of this parameter.
	 * 
	 * @return the maximum value that this parameter may take on
	 */
	public int getMaximum() {
		return maxValue;
	}

	/**
	 * Gets the current value of this parameter
	 * 
	 * @return the current value.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * The slider that displays/controls the parameter.
	 */
	protected JSlider slider;

	private final static String DEFAULT_EXTENDED_TEXT = "";

	/**
	 * Default floating point format for an integer text field.
	 */
	public static String DEFAULT_INTEGER_FORMAT = "0";

	/**
	 * Default floating point format as a <code>NumberFormat</code>.
	 */
	public static NumberFormat DEFAULT_INTEGER_NUMBER_FORMAT = new DecimalFormat(
			DEFAULT_INTEGER_FORMAT);

	/**
	 * Default width in pixels of the label to the left of a slider in a
	 * slideNText panel. If the label doesn't fit in this width a tool tip with
	 * the parameter name will be set.
	 */
	public static int DEFAULT_SLIDER_LABEL_WIDTH = 140;

	/**
	 * The text field that displays this parameter's value.
	 */
	protected JFormattedTextField textField = new JFormattedTextField(
			DEFAULT_INTEGER_NUMBER_FORMAT);

	private JLabel label;
	
	private JPanel panel = null;

	/**
	 * Gets a slider and text box control for this int parameter.
	 * 
	 * @return controls
	 */
	public JPanel getSliderControls() {
		return getSliderControls(false, "", DefaultChecked);
	}

	/**
	 * Gets a slider, a text box and checkbox control for this double parameter.
	 * Use "enabled" as the check box label.
	 * 
	 * @return controls
	 */
	public Component getSliderControlsExtended() {
		return getSliderControls(true, DEFAULT_EXTENDED_TEXT, DefaultChecked);
	}

	public Component getSliderControlsExtended(String checkboxText) {
		return getSliderControls(true, checkboxText, DefaultChecked);
	}

	public Component getSliderControlsExtended(String checkboxText, boolean checked) {
		return getSliderControls(true, checkboxText, checked);
	}

	
	/**
	 * Gets a slider, a text box and checkbox control for this double parameter.
	 * 
	 * @param checkboxText
	 * @return controls
	 */
	public JPanel getSliderControls(boolean extended, String checkboxText, boolean checked) {

		if (panel != null)
			return panel;
		
		isBeingSet();

        // Set the value but don't notify the listeners
        boolean notifyListeners = false;
		booleanp.setValue(earlyChecked[0] ? earlyChecked[1] : checked, notifyListeners);
		booleanp.setName(checkboxText);

		// Create the ui components
		label = new JLabel(getName());
		slider = new JSlider(SwingConstants.HORIZONTAL, 0, getMaximum(), 0);
		textField = new JFormattedTextField(DEFAULT_INTEGER_NUMBER_FORMAT);

		textField.setText("" + value);

		slider.addChangeListener(this);
		textField.addPropertyChangeListener("value", this);

		/*
		 * Set component dimensions.
		 */

		// Check the width of the text label for truncation. Display a tooltip
		// text
		// if the width of the label goes beyond the defined threshold.
		Dimension dlbl = new Dimension(label.getPreferredSize());
		if (dlbl.width > DEFAULT_SLIDER_LABEL_WIDTH) {
			label.setToolTipText(getName());
		}
		// Use a fixed width for the text label
		dlbl.width = DEFAULT_SLIDER_LABEL_WIDTH;
//		label.setPreferredSize(dlbl);
//		label.setMinimumSize(dlbl);
		label.setMaximumSize(dlbl);

		// Use a fixed minimum width for the slider
		Dimension dslider = new Dimension(slider.getPreferredSize());
		dslider.width = DEFAULT_SLIDER_LABEL_WIDTH / 2;
//		slider.setMinimumSize(dslider);
		slider.setPreferredSize(dslider);

		// Set the text field dimensions
		Dimension dtxt = new Dimension(textField.getPreferredSize());
		// dtxt.width = extended ? DEFAULT_SLIDER_LABEL_WIDTH / 2 :
		// DEFAULT_SLIDER_LABEL_WIDTH;
		dtxt.width = DEFAULT_SLIDER_LABEL_WIDTH / 2;
//		textField.setMinimumSize(dtxt);
		textField.setPreferredSize(dtxt);
		// textField.setMaximumSize(dtxt);

		// Create the panel that holds the ui component
		final IntParameter me = this;
		panel = new JPanel() {
			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				me.setEnabled(enabled);
			}
		};
		
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.weightx = 0;
		gbc.weighty = 0;

		// Text label (fixed width)
		gbc.weightx = 0;
		gbc.insets = new Insets(8, 4, 2, 8);
		panel.add(label, gbc);

		// Slider
		gbc.gridx++;
		gbc.weightx = 2;
		gbc.insets = new Insets(2, 0, 2, 2); // top, left, bottom, right
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(slider, gbc);

		// Text field
		gbc.gridx++;
		gbc.weightx = extended ? 0 : 1;
		gbc.insets = new Insets(2, 0, 2, 0);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(textField, gbc);

		// If extended, a boolean parameter
		if (extended) {
			gbc.gridx++;
			gbc.weightx = 0;
			gbc.insets = new Insets(0, 0, 0, 0);
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.anchor = GridBagConstraints.NORTHEAST;
			gbc.fill = GridBagConstraints.NONE;

			panel.add(booleanp.getControls(2 * DEFAULT_SLIDER_LABEL_WIDTH / 3),
					gbc);

		}

		updateView();

		finishSetting();
		return panel;
	}
	
	public void setTextJustification(int JTextFieldAlignment) {
		if (label == null) return;
		
		label.setHorizontalAlignment(JTextFieldAlignment);
		label.invalidate();
	}

	/**
	 * Gets a text box control for this int parameter.
	 * 
	 * @return controls
	 */
	public JPanel getControls() {
		if (panel != null)
			return panel;
		// Create the ui components
		label = new JLabel(getName());

		textField.setText("" + value);

		textField.addPropertyChangeListener("value", this);

		Dimension d;

		d = new Dimension(textField.getPreferredSize());
		d.width = DEFAULT_SLIDER_LABEL_WIDTH;
		textField.setPreferredSize(d);
//		textField.setMinimumSize(d);

		d = new Dimension(label.getPreferredSize());
		if (d.width > DEFAULT_SLIDER_LABEL_WIDTH) {
			label.setToolTipText(getName());
		}
		d.width = DEFAULT_SLIDER_LABEL_WIDTH;
		label.setPreferredSize(d);
//		label.setMinimumSize(d);

		// Create the panel that holds the ui component
		HorizontalFlowPanel hfp = new HorizontalFlowPanel();
		hfp.add(label);
		hfp.add(textField);
		panel = hfp.getPanel();

		updateView();

		return panel;
	}

	/**
	 * Called by the text field when a new value is committed. The updated value
	 * of the text field is passed down to the represented parameter.
	 * 
	 * @param evt
	 *            ignored
	 * @see java.beans.PropertyChangeListener#propertyChange(PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		Object valueO = textField.getValue();
		if (valueO instanceof Number) {
			int v = ((Number) valueO).intValue();
			setValue(v);
			return;
		}
	}

    /**
     * Prevent excessive changes by using time intervals
     */
    private long t0 = 0, t1 = 0;
    private long dt = (long) (10 * 1e6);

	/**
	 * Called whenever the state of this slider's <code>JSlider</code> is
	 * changed. The change in state is passed down to this slider's
	 * <code>Parameter</code>
	 * 
	 * @param event
	 *            the change event (ignored)
	 */
	@Override
	public void stateChanged(ChangeEvent event) {
		
    	t1 = System.nanoTime();
    	if (t1 - t0 < dt) {
    		return;
    	}
    	t0 = t1;

		if (isBlocking())
			return;
		setBlocking(true);

		int ivalue = slider.getValue();
		// int sliderRange = slider.getMaximum() - slider.getMinimum();
		// double min = getMinimum();
		// double max = getMaximum();
		// double v;
		// if (isLogarithmic) {
		// min = Math.log(min);
		// max = Math.log(max);
		// }
		// v = min + ivalue * (max - min) / sliderRange;
		// if (isLogarithmic) {
		// v = Math.exp(v);
		// }
		//        
		// System.out.println("========1=========");
		// System.out.println("Slider value = " + ivalue);
		// // System.out.println("Previous = " + previous);
		// System.out.println("int Value = " + v);
		//
		// if (v - previous > 0) {
		// previous = v;
		// v = Math.ceil(v);
		// }
		// else {
		// previous = v;
		// v = Math.floor(v);
		// }
		// // System.out.println("Previous = " + previous);
		//        
		// // setValue((int) v);
		// setValue((int) Math.round(v));
		setValue(ivalue);

		setBlocking(false);
	}

	/**
	 * Updates this parameter's <code>JSlider</code> and
	 * <code>JFormattedTextField</code> to reflect the range and current value
	 * of this slider's parameter.
	 */
	protected void updateView() {
		if (slider != null) {
			slider.setValue(getValue());
		}

		textField.setValue(getValue());
	}

	private boolean blocking = false;

	private void setBlocking(boolean b) {
		blocking = b;
	}

	private boolean isBlocking() {
		return blocking;
	}

	private double previous = 0;

	/**
	 * Check or uncheck the checkbox linked to this parameter.
	 * 
	 * @param b
	 */
	public void setChecked(boolean b) {
		// if (booleanp == null) defaultChecked = b;
		// else booleanp.setValue(b);
		// booleanp.setEnabled(b);
		booleanp.setValue(b);

		if (panel == null) {
			earlyChecked[0] = true;
			earlyChecked[1] = b;
		}
	}

	/**
	 * @return whether this control is enabled.
	 */
	public boolean isChecked() {
		return booleanp.getValue();
	}

	/**
	 * Enables or disables the slider linked to this parameter.
	 * 
	 * @param b
	 */
	public void setEnabled(boolean b) {
		if (panel == null)
			return;
		
		slider.setEnabled(b);
		textField.setEnabled(b);
		booleanp.setEnabled(b);
	}
	
	/**
	 * @return the default value of this parameter.
	 */
	public int getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Set the default value for this parameter.
	 * 
	 * @param v
	 */
	public void setDefaultValue(int v) {
		defaultValue = v;
	}

	@Override
	public void addParameterListener(ParameterListener listener) {
		booleanp.addParameterListener(listener);
		super.addParameterListener(listener);
	}

	@Override
	public void addParameterListeners(List<ParameterListener> listeners) {
		booleanp.addParameterListeners(listeners);
		super.addParameterListeners(listeners);
	}

	public void set(IntParameter other) {
		hold(true);
		setMinimum(other.getMinimum());
		setMaximum(other.getMaximum());
		setValue(other.getValue());
		hold(false);
	}
}
