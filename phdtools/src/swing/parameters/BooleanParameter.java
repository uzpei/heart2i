package swing.parameters;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import swing.component.ButtonSwitch;
import swing.component.ButtonSwitch.SwitchType;


/**
 * Simple boolean parameter class
 * @author kry
 */
public class BooleanParameter extends Parameter implements ItemListener {
    /**
     * The current value of this parameter.
     */
    protected boolean value;

    /**
     * The default value of this parameter.
     */
    protected boolean defaultValue;

    /**
     * Whether a change of this parameter occured and the sender specifically prevented a notification to the listeners.
     */
    protected boolean valueChangedWithNotificationDisabled;
    
    /**
     * Creates a new <code>BooleanParameter</code> with the specified name and
     * default value.
     * @param name the name of this parameter
     * @param defaultValue the default value of this parameter
     */
    public BooleanParameter(String name, SwitchType type, boolean defaultValue) {
        super(name);
        this.defaultValue = defaultValue;
        this.value = defaultValue;
        
        button = new ButtonSwitch(type, defaultValue);
        button.addItemListener(this);
        
        lblName = new JLabel(getName());
    }

    public BooleanParameter(String name, boolean defaultValue) {
    	this(name, ButtonSwitch.DEFAULT_SWITCH, defaultValue);
    }

    public BooleanParameter(BooleanParameter other) {
    	this(other.name, other.getButtonSwitch().getSwitchType(), other.value);
	}
    
    public ButtonSwitch getButtonSwitch() {
    	return button;
    }

    private boolean manualSet = false;
    
	/**
     * Sets the value of this parameter.  
     * @param value the new value of this parameter
     */
    public void setValue(boolean value, boolean notifyListeners)
    {
        if (isBeingSet()) return;
        
        valueChangedWithNotificationDisabled = !notifyListeners;
        
        this.value = value;

        manualSet = true;
        
        button.setSelected(value);
//        notifyListeners();
        this.finishSetting();
    }
    
	public void setValue(boolean value) {
		setValue(value, true);
	}

	@Override
	public void itemStateChanged(ItemEvent itemEvent) {
		
		// Reflect button state
		value = button.isSelected();
		lblName.setEnabled(button.isSelected());
		if (!valueChangedWithNotificationDisabled && (manualSet || button.isMousePressed())) notifyListeners();
		
		valueChangedWithNotificationDisabled = false;
		
		manualSet = false;
	}

    /**
     * Gets the value of this parameter.
     * @return the current value of this parameter.
     */
    public boolean getValue() {
        return value;
    }
    
    /**
     * Gets the default value of this parameter.
     * 
     * @return the default value of this parameter.
     */
    public boolean getDefaultValue()
    {
        return defaultValue;
    }

//    private JCheckBox checkBox;
    private ButtonSwitch button;

    /**
     * Enable/disable the checkbox linked to this parameter.
     * @param b
     */
    public void setEnabled(boolean b) {
        if (button != null)
        	button.setEnabled(b);
    }
    
    private JPanel panel = null;
    private JLabel lblName;
    
    public JPanel getControls() {
    	return getControls(Integer.MAX_VALUE);
    }

    public JPanel getControls(boolean leftAligned) {
    	return getControls(Integer.MAX_VALUE, leftAligned);
    }

    public JPanel getControls(int maxlabelwidth) {
    	return getControls(maxlabelwidth, true);
    }
    /**
     * Gets a swing panel with a check box to control this parameter
     * @return the control panel
     */
    public JPanel getControls(int maxlabelwidth, boolean leftAligned) {
        if ( panel != null ) return panel;
        
		final BooleanParameter me = this;
		panel = new JPanel() {
			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				me.setEnabled(enabled);
			}
		};
		        
        lblName = new JLabel(getName());
        lblName.setEnabled(defaultValue);
        lblName.setHorizontalAlignment(SwingConstants.LEFT);
        
        Dimension lbld = lblName.getPreferredSize();
        if (lbld.width > maxlabelwidth) {
        	lbld.width = maxlabelwidth;
        	lblName.setPreferredSize(lbld);
        	lblName.setMaximumSize(lbld);
        	lblName.setToolTipText(getName());
        }

        panel.setLayout(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 0;
        gbc.ipady = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
        
        gbc.weightx = 0;
        gbc.insets = new Insets(4, 4, 2, 4);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(lblName, gbc);
        
        gbc.weightx = 1;
        gbc.gridx++;
        gbc.anchor = GridBagConstraints.EAST;
        gbc.fill = leftAligned ? GridBagConstraints.HORIZONTAL : GridBagConstraints.NONE;
        panel.add(button, gbc);
                
        return panel;    
    }
    
    public String getString() {
    	return getName() + ": " + getValue();
    }

}
