package swing.parameters;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import swing.component.HorizontalFlowPanel;


/**
 * This {@link Parameter}subclass represents a float-valued parameter that can
 * take on values within a specified range.
 * 
 * @author epiuze, adapted from tedmunds and kry.
 */
public class FloatParameter extends Parameter implements PropertyChangeListener, ChangeListener {

    private float defaultValue;
    private boolean defaultChecked = true;
    private float value;
    private float minValue;
    private float maxValue;     
    private BooleanParameter booleanp;
    private ParameterListener booleanListener;
    
    /**
     * Creates a new <code>FloatParameter</code> with the specified name,
     * default value, minimum value, and maximum value.
     * 
     * @param name
     *        the name of this parameter
     * @param defaultValue
     *        the default value of this parameter
     * @param minValue
     *        the minimum value of this parameter
     * @param maxValue
     *        the maximum value of this parameter
     */
    public FloatParameter(String name, float defaultValue, float minValue, float maxValue) {        
        super(name);
        // Make sure default is within range
        this.defaultValue = defaultValue;
        if (defaultValue < minValue) this.defaultValue = minValue;
        if (defaultValue > maxValue) this.defaultValue = maxValue;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.value = defaultValue;
        
        booleanp = new BooleanParameter("", false);
    }
    
    /**
     * Sets the value of this parameter. Registered
     * <code>ParameterListener</code> s are notified of the change. The value
     * is clamped to [minValue, maxValue] where minValue and maxValue are the
     * parameter's minimum and maximum allowable values respectively.
     * @param value
     *        the new value of this parameter
     */
    public void setValue(float value) {
        if (isBeingSet()) return;
        this.value = value;
        if (this.value < minValue) this.value = minValue;
        if (this.value > maxValue) this.value = maxValue;
        updateView();
        notifyListeners();
        this.finishSetting();
    }

    /**
     * Sets the minimum allowed value of this parameter. The default and current
     * value may be adjusted to comply.
     * @param minValue
     */
    public void setMinimum(float minValue) {
        if (isBeingSet()) return;
        this.minValue = minValue;
        if (defaultValue < minValue) this.defaultValue = minValue;
        if (value < minValue) value = minValue;
        updateView();
        notifyListeners();
        this.finishSetting();
    }

    /**
     * Sets the maximum allowed value of this parameter. The default and current
     * value may be adjusted to comply.
     * @param maxValue
     */
    public void setMaximum(float maxValue) {
        if (isBeingSet()) return;        
        this.maxValue = maxValue;
        if (defaultValue > maxValue) this.defaultValue = maxValue;
        if (value > maxValue) value = maxValue;
        updateView();
        notifyListeners();
        this.finishSetting();
    }

    /**
     * Gets the minimum value that this parameter may take on.
     * @return the minimum value of this parameter
     */
    public float getMinimum() {
        return minValue;
    }

    /**
     * Gets the maximum value of this parameter.
     * @return the maximum value that this parameter may take on
     */
    public float getMaximum() {
        return maxValue;
    }

    /**
     * Gets the current value of this parameter
     * @return the current value.
     */
    public float getValue() {
        return value;
    }
    
    /**
     * The slider that displays/controls the parameter.
     */
    private JSlider slider;
    
    /**
     * Whether this slider is a logarithmic scale slider.
     */
    private boolean isLogarithmic;

    /**
     * Default number of ticks for number sliders.
     */
    public static int DEFAULT_SLIDER_TICKS = 2000;
    
    private final static String DEFAULT_EXTENDED_TEXT = ""; 

    /**
     * Default floating point format for a floating point text field.
     */
    public static String DEFAULT_FLOATING_POINT_FORMAT = "0.0000";
    
    /**
     * Default floating point format as a <code>NumberFormat</code>.
     */
    public static NumberFormat DEFAULT_FLOATING_POINT_NUMBER_FORMAT = new DecimalFormat(DEFAULT_FLOATING_POINT_FORMAT);
    
    /**
     * Default width in pixels of the label to the left of a slider in a
     * slideNText panel. If the label doesn't fit in this width a tool tip with
     * the parameter name will be set.
     */
    public static int DEFAULT_SLIDER_LABEL_WIDTH = 140;
    
    /**
     * The text field that displays this parameter's value.
     */
    private JFormattedTextField textField = new JFormattedTextField( DEFAULT_FLOATING_POINT_NUMBER_FORMAT );
    
    /**
     * If the "checked" property was changed before creating the controls, make sure we use that boolean.
     */
    private boolean[] earlyChecked = new boolean[] {false, false};
    
    private JPanel panel = null;
    private JLabel label;
    
    public JPanel getSliderControls() {
        return getSliderControls("", false, false, false);
    }
    
    public void setLabelText(String text) {
    	label.setText(text);
    }
    
    /**
     * Gets a slider and text box control for this float parameter.
     * @return controls
     */
    public Component getSliderControlsExtended() {
        return getSliderControlsExtended(DEFAULT_EXTENDED_TEXT);
    }
    
    public Component getSliderControlsExtended(boolean checked) {
        return getSliderControls(DEFAULT_EXTENDED_TEXT, false, true, checked);
    }

    public Component getSliderControlsExtended(String controlText, boolean checked) {
        return getSliderControls(controlText, false, true, checked);
    }

    /**
     * Gets a slider and text box control for this float parameter.
     * @return controls
     */
    public Component getSliderControlsExtended(String controlText) {
        return getSliderControls(controlText, false, true, defaultChecked);
    }

    public JPanel getSliderControls(boolean isLog) {
    	return getSliderControls("", isLog, false, false);
    }
    

    /**
     * Gets a slider and text box control for this float parameter.
     * @param checkboxText 
     * @return controls
     */
    public JPanel getSliderControls(String checkboxText, boolean isLog, boolean extended, boolean checked) {
        if ( panel != null ) return panel;
        
        booleanp.setName(checkboxText);
        booleanp.setValue(earlyChecked[0] ? earlyChecked[1] : checked, false);
        
        this.isLogarithmic = isLog;
        
        // Create the ui components
        label = new JLabel( getName() );
        slider = new JSlider(SwingConstants.HORIZONTAL, 0, DEFAULT_SLIDER_TICKS, DEFAULT_SLIDER_TICKS / 2);
        NumberFormat format = DEFAULT_FLOATING_POINT_NUMBER_FORMAT;
        format.setMaximumFractionDigits(extended ? 3 : 5);
        format.setMinimumFractionDigits(extended ? 3 : 5);
        
        textField = new JFormattedTextField(format);
        
        textField.setText( "" + value );

        slider.addChangeListener(this);
        textField.addPropertyChangeListener("value", this);

        /*
         * Set component dimensions.
         */
                
        // Check the width of the text label for truncation. Display a tooltip text
        // if the width of the label goes beyond the defined threshold.
        Dimension dlbl = new Dimension(label.getPreferredSize());
        if (dlbl.width > DEFAULT_SLIDER_LABEL_WIDTH) {
            label.setToolTipText( getName() );
        }
        // Use a fixed width for the text label
        dlbl.width = DEFAULT_SLIDER_LABEL_WIDTH;
        label.setPreferredSize(dlbl);
        label.setMinimumSize(dlbl);
        label.setMaximumSize(dlbl);
        
        // Use a fixed minimum width for the slider
        Dimension dslider = new Dimension(slider.getPreferredSize());
        dslider.width = DEFAULT_SLIDER_LABEL_WIDTH / 2;
        slider.setMinimumSize(dslider);
        slider.setPreferredSize(dslider);

        // Set the text field dimensions
        Dimension dtxt = new Dimension(textField.getPreferredSize());
//        dtxt.width = extended ? DEFAULT_SLIDER_LABEL_WIDTH / 2 : DEFAULT_SLIDER_LABEL_WIDTH;
        dtxt.width = DEFAULT_SLIDER_LABEL_WIDTH / 2 - 10;
        textField.setPreferredSize(dtxt);
        textField.setMinimumSize(dtxt);
//        textField.setMaximumSize(dtxt);
        
        // Create the panel that holds the ui component
        panel = new JPanel();

        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 0;
        gbc.ipady = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;

        // Text label (fixed width)
        gbc.weightx = 0;
        gbc.insets = new Insets(8, 4, 2, 4);
        panel.add(label, gbc);

        // Slider
        gbc.gridx++;
        gbc.weightx = 1;
        gbc.insets = new Insets(2, 0, 2, 0); // top, left, bottom, right
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(slider, gbc);

        // Text field
        gbc.gridx++;
        gbc.weightx = extended ? 0 : 1;
        gbc.insets = new Insets(2, 0, 2, 0);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(textField, gbc);

        // If extended, a boolean parameter
        if (extended) {
            gbc.gridx++;
            gbc.weightx = 0;
            gbc.insets = new Insets(0, 0, 0, 0);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.anchor = GridBagConstraints.NORTHEAST;
            gbc.fill = GridBagConstraints.NONE;
            panel.add(booleanp.getControls(2 * DEFAULT_SLIDER_LABEL_WIDTH / 3), gbc);
            
        }
        
        updateView();

        return panel;
    }

    
     /**
     * Gets a  text box control for this float parameter.
     * @return controls
     */
    public JPanel getControls() {
        if ( panel != null ) return panel;
        this.isLogarithmic = false;
        // Create the ui components
        label = new JLabel( getName() );
        
        textField.setText( "" + value );

        textField.addPropertyChangeListener("value", this);
        
        Dimension d;
        
        d = new Dimension(textField.getPreferredSize());
        d.width = DEFAULT_SLIDER_LABEL_WIDTH;
        textField.setPreferredSize(d);
        textField.setMinimumSize(d);

        d = new Dimension(label.getPreferredSize());
        if (d.width > DEFAULT_SLIDER_LABEL_WIDTH) {
            label.setToolTipText( getName() );
        }
        d.width = DEFAULT_SLIDER_LABEL_WIDTH;
        label.setPreferredSize(d);
        label.setMinimumSize(d);
        
        // Create the panel that holds the ui component
        HorizontalFlowPanel hfp = new HorizontalFlowPanel();
        hfp.add( label );
        hfp.add( textField );
        panel = hfp.getPanel();
        
        updateView();

        return panel;
    }

    
    /**
     * Called by the text field when a new value is committed. The updated value
     * of the text field is passed down to the represented parameter.
     * 
     * @param evt
     *        ignored
     * @see java.beans.PropertyChangeListener#propertyChange(PropertyChangeEvent)
     */
    @Override
	public void propertyChange(PropertyChangeEvent evt) {
        Object valueO = textField.getValue();
        if (valueO instanceof Number) {
            float v = ((Number)valueO).floatValue();
            setValue(v);
            return;
        }
    }

    /**
     * Updates this parameter's <code>JSlider</code> and <code>JFormattedTextField</code>
     * to reflect the range and
     * current value of this slider's parameter.
     */
    private void updateView() {
        if ( slider != null ) {
            int sliderRange = slider.getMaximum() - slider.getMinimum();
            int ivalue;
            float min = getMinimum();
            float max = getMaximum();
            float v = getValue();
            if (isLogarithmic)
            {
                min = (float) Math.log(min);
                max = (float) Math.log(max);
                v = (float) Math.log(v);
            }
            ivalue = slider.getMinimum()
                     + (Math.round((v - min) * sliderRange / (max - min)));
            slider.setValue(ivalue);
        }
        // update the text field
        textField.setValue(getValue());        
    }

    /**
     * Prevent excessive changes by using time intervals
     */
    private long t0 = 0, t1 = 0;
    private long dt = (long) (10 * 1e6);

    /**
     * Called whenever the state of this slider's <code>JSlider</code> is
     * changed. The change in state is passed down to this slider's
     * <code>Parameter</code>
     * 
     * @param event
     *        the change event (ignored)
     */
    @Override
	public void stateChanged(ChangeEvent event) {
    	
    	t1 = System.nanoTime();
    	if (t1 - t0 < dt) {
    		return;
    	}
    	t0 = t1;

        int ivalue = slider.getValue();
        int sliderRange = slider.getMaximum() - slider.getMinimum();
        float min = getMinimum();
        float max = getMaximum();
        float v;
        if (isLogarithmic) {
            min = (float) Math.log(min);
            max = (float) Math.log(max);
        }
        v = min + ivalue * (max - min) / sliderRange;
        if (isLogarithmic) {
            v = (float) Math.exp(v);
        }        
        setValue(v);
    }
    
    /**
     * Check or uncheck the checkbox linked to this parameter.
     * @param b
     */
    public void setChecked(boolean b) {
//    	if (booleanp == null) defaultChecked = b;
//    	else booleanp.setValue(b);
//        booleanp.setEnabled(b);
    	booleanp.setValue(b);
    	
    	if (panel == null) {
    		earlyChecked[0] = true;
    		earlyChecked[1] = b;
    	}
    }
    
    /**
     * @return whether this control is enabled.
     */
    public boolean isChecked() {
        return booleanp.getValue();
    }
    
    /**
     * @return the default value of this parameter.
     */
    public float getDefaultValue() {
        return defaultValue;
    }

    /**
     * Set the default value for this parameter.
     * @param v
     */
    public void setDefaultValue(float v) {
        defaultValue = v;
    }

    public void setEnabled(boolean b) {
    	if (panel == null) return;
        label.setEnabled(b);
        textField.setEnabled(b);
        slider.setEnabled(b);
        booleanp.setEnabled(b);
    }

	/**
	 * 
	 */
	public void invalidate() {
		notifyListeners();
	}

    @Override
    public void addParameterListener(ParameterListener listener) {
    	booleanp.addParameterListener(listener);
    	super.addParameterListener(listener);
    }

    @Override
    public void addParameterListeners(List<ParameterListener> listeners) {
    	booleanp.addParameterListeners(listeners);
        super.addParameterListeners(listeners);
    }

}
