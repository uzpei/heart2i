package swing.parameters;

import java.util.LinkedList;
import java.util.List;


public class ParameterManager {
	
	private List<ParameterListener> parameterListeners = new LinkedList<ParameterListener>();

	private List<Parameter> parameters = new LinkedList<Parameter>();

	public void set(ParameterManager other) {
		this.parameterListeners = other.parameterListeners;
		this.parameters = other.parameters;
	}

	public void addParameterListener(ParameterListener l) {
		if (!parameterListeners.contains(l))
			parameterListeners.add(l);
		
		// Add this parameter listener to all parameters
		for (Parameter p : parameters) {
			if (!p.getParameterListeners().contains(l)) {
				p.addParameterListener(l);
			}
		}
	}
	
	public void addParameter(Parameter p) {
		if (!parameters.contains(p))
			parameters.add(p);

		// Add all current listeners to this parameter
		for (ParameterListener pl : parameterListeners) {
			if (!p.getParameterListeners().contains(pl)) {
				p.addParameterListener(pl);
			}
		}
	}

	public void addParameters(List<Parameter> ps) {
		parameters.addAll(ps);

		for (Parameter p : ps) {
			// Add all current listeners to this parameter
			for (ParameterListener pl : parameterListeners) {
				if (!p.getParameterListeners().contains(pl)) {
					p.addParameterListener(pl);
				}
			}
		}
	}

	public void removeParameterListeners() {
		for (ParameterListener pl : parameterListeners) {
			for (Parameter p : parameters) {
				p.removeParameterListener(pl);
			}
		}
		parameterListeners.clear();
	}
	
	public void removeParameters() {
		parameters.clear();
	}
	
	private boolean holding = false;
	
	private boolean notifying = false;
	
	private Object lockingObject = null;
	
	public boolean isHolding() {
		return holding;
	}

	public boolean isNotifying() {
		return notifying;
	}

	private void setNotifying(boolean b) {
		this.notifying = b;
	}
	
	/**
	 * Make all parameters hold 
	 * @param bool
	 */
	public void hold(boolean bool) {
//		if (holding == bool) {
//			// Mismatch in hold calls!
//			// TODO: handle this in some cleaner way
//			return;
//		}
		
		holding = bool;

		for (Parameter p : parameters) {
			p.hold(bool);
		}
		
//		if (bool) {
//			for (ParameterListener pl : parameterListeners) {
//				for (Parameter p : parameters) {
//					p.removeParameterListener(pl);
//				}
//			}
//		}
//		else {
//			for (ParameterListener pl : parameterListeners) {
//				for (Parameter p : parameters) {
//					p.addParameterListener(pl);
//				}
//			}
//		}
	}
	
	public List<Parameter> getParameters() {
		return parameters;
	}
	public List<ParameterListener> getParameterListeners() {
		return parameterListeners;
	}
	
	/**
	 * Manually notify the parameter listeners that a change has occured.
	 */
	public void notifyListeners() {
		setNotifying(true);
		for (ParameterListener pl : parameterListeners) {
			if (isHolding()) return;
			
			pl.parameterChanged(null);
		}
		setNotifying(false);
	}
}
