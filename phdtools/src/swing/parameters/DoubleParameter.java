package swing.parameters;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import swing.component.HorizontalFlowPanel;


/**
 * This {@link Parameter}subclass represents a double-valued parameter that can
 * take on values within a specified range.
 * 
 * @author epiuze, adapted from tedmunds and kry.
 */
public class DoubleParameter extends Parameter implements PropertyChangeListener, ChangeListener {

    private double defaultValue;
    private double value;
    private double minValue;
    private double maxValue;     
    private BooleanParameter booleanp;
    private ParameterListener booleanListener;
    
    /**
     * The slider that displays/controls the parameter.
     */
    private JSlider slider;
    
    /**
     * Whether this slider is a logarithmic scale slider.
     */
    private boolean isLogarithmic;

    /**
     * Default number of ticks for number sliders.
     */
    public static int DEFAULT_SLIDER_TICKS = 20000;
    
    private final static String DEFAULT_EXTENDED_TEXT = ""; 

    /**
     * Default floating point format for a floating point text field.
     */
    public static String DEFAULT_FLOATING_POINT_FORMAT = "0.0000000";
    
    /**
     * Default floating point format as a <code>NumberFormat</code>.
     */
    public static NumberFormat DEFAULT_FLOATING_POINT_NUMBER_FORMAT = new DecimalFormat(DEFAULT_FLOATING_POINT_FORMAT);
    
    /**
     * Default width in pixels of the label to the left of a slider in a
     * slideNText panel. If the label doesn't fit in this width a tool tip with
     * the parameter name will be set.
     */
    public static int DEFAULT_SLIDER_LABEL_SIZE = 140;
    
    /**
     * The text field that displays this parameter's value.
     */
    private JFormattedTextField textField = new JFormattedTextField(DEFAULT_FLOATING_POINT_NUMBER_FORMAT);
    
    /**
     * If the "checked" property was changed before creating the controls, make sure we use that boolean.
     */
    private boolean[] earlyChecked = new boolean[] { false, DefaultChecked };
    
    private JPanel panel = null;
    private JLabel label;
    
    private boolean vertical = false;

    /**
     * Creates a new <code>DoubleParameter</code> with the specified name,
     * default value, minimum value, and maximum value.
     * 
     * @param name
     *        the name of this parameter
     * @param defaultValue
     *        the default value of this parameter
     * @param minValue
     *        the minimum value of this parameter
     * @param maxValue
     *        the maximum value of this parameter
     */
    public DoubleParameter(String name, double defaultValue, double minValue, double maxValue) {        
        super(name);
        // Make sure default is within range
        this.defaultValue = defaultValue;
        if (defaultValue < minValue) this.defaultValue = minValue;
        if (defaultValue > maxValue) this.defaultValue = maxValue;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.value = defaultValue;
        
        booleanp = new BooleanParameter("", DefaultChecked);
    }
    
    public DoubleParameter(DoubleParameter other) {
		super(other.name);
		// Make sure default is within range

		this.defaultValue = other.defaultValue;
		this.minValue = other.minValue;
		this.maxValue = other.maxValue;
		
		this.value = other.value;

		booleanp = new BooleanParameter(other.booleanp);
		
		addParameterListeners(other.getParameterListeners());
	}

	/**
     * Sets the value of this parameter. Registered
     * <code>ParameterListener</code> s are notified of the change. The value
     * is clamped to [minValue, maxValue] where minValue and maxValue are the
     * parameter's minimum and maximum allowable values respectively.
     * @param value
     *        the new value of this parameter
     */
    public void setValue(double value) {
        if (isBeingSet()) return;
        
        this.value = value;
        if (this.value < minValue) this.value = minValue;
        if (this.value > maxValue) this.value = maxValue;
        updateView();
        this.finishSetting();
        notifyListeners();
    }

    /**
     * Sets the minimum allowed value of this parameter. The default and current
     * value may be adjusted to comply.
     * @param minValue
     */
    public void setMinimum(double minValue) {
        if (isBeingSet()) return;
        this.minValue = minValue;
        if (defaultValue < minValue) this.defaultValue = minValue;
        if (value < minValue) value = minValue;
        updateView();
        notifyListeners();
        this.finishSetting();
    }

    /**
     * Sets the maximum allowed value of this parameter. The default and current
     * value may be adjusted to comply.
     * @param maxValue
     */
    public void setMaximum(double maxValue) {
        if (isBeingSet()) return;        
        this.maxValue = maxValue;
        if (defaultValue > maxValue) this.defaultValue = maxValue;
        if (value > maxValue) value = maxValue;
        updateView();
        notifyListeners();
        this.finishSetting();
    }

    /**
     * Gets the minimum value that this parameter may take on.
     * @return the minimum value of this parameter
     */
    public double getMinimum() {
        return minValue;
    }

    /**
     * Gets the maximum value of this parameter.
     * @return the maximum value that this parameter may take on
     */
    public double getMaximum() {
        return maxValue;
    }

    /**
     * Gets the current value of this parameter
     * @return the current value.
     */
    public double getValue() {
        return value;
    }
    
    
    public JPanel getSliderControls() {
        return getSliderControls("", false, false, false, false);
    }
    
    public void setLabelText(String text) {
    	label.setText(text);
    }
    
    /**
     * Gets a slider and text box control for this double parameter.
     * @return controls
     */
    public Component getSliderControlsExtended() {
        return getSliderControlsExtended(DEFAULT_EXTENDED_TEXT);
    }
    
    public Component getSliderControlsExtended(boolean checked) {
        return getSliderControls(DEFAULT_EXTENDED_TEXT, false, true, checked, false);
    }

    public Component getSliderControlsExtended(String controlText, boolean checked) {
        return getSliderControls(controlText, false, true, checked, false);
    }

    /**
     * Gets a slider and text box control for this double parameter.
     * @return controls
     */
    public Component getSliderControlsExtended(String controlText) {
    	return getSliderControlsExtended(controlText, DefaultChecked);
    }

    public JPanel getSliderControls(boolean isLog) {
    	return getSliderControls("", isLog, false, DefaultChecked, false);
    }
    

    /**
     * Gets a slider and text box control for this double parameter.
     * @param checkboxText 
     * @return controls
     */
    public JPanel getSliderControls(String checkboxText, boolean isLog, boolean extended, boolean checked, boolean vertical) {
        if (panel != null) return panel;
        
        // Create the panel that holds the ui component
		final DoubleParameter me = this;
		panel = new JPanel() {
			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				me.setEnabled(enabled);
			}
		};
		
		
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);

        NumberFormat format = DEFAULT_FLOATING_POINT_NUMBER_FORMAT;
        format.setMaximumFractionDigits(extended ? 4 : 5);
        format.setMinimumFractionDigits(extended ? 4 : 5);
        
        this.isLogarithmic = isLog;

        slider = new JSlider(vertical? SwingConstants.VERTICAL : SwingConstants.HORIZONTAL, 0, DEFAULT_SLIDER_TICKS, DEFAULT_SLIDER_TICKS / 2);
        slider.addChangeListener(this);
        
        // Set the text field dimensions
        Dimension dtxt = new Dimension(textField.getPreferredSize());
//        dtxt.width = extended ? DEFAULT_SLIDER_LABEL_WIDTH / 2 : DEFAULT_SLIDER_LABEL_WIDTH;
        dtxt.width = DEFAULT_SLIDER_LABEL_SIZE / 2 - 10;
        textField.setPreferredSize(dtxt);
        textField.setMinimumSize(dtxt);
        textField = new JFormattedTextField(format);
        textField.setText( "" + value );
        textField.addPropertyChangeListener("value", this);
        
        // Check the width of the text label for truncation. Display a tooltip text
        // if the width of the label goes beyond the defined threshold.
        label = new JLabel(getName(), JLabel.CENTER);
        Dimension dlbl = new Dimension(label.getPreferredSize());
        if (dlbl.width > DEFAULT_SLIDER_LABEL_SIZE) {
            label.setToolTipText( getName() );
        }
        // Use a fixed width for the text label
        dlbl.width = Math.min(dlbl.width, DEFAULT_SLIDER_LABEL_SIZE);
        if (getName() == "") {
        	dlbl.width = 0;
        }
        label.setPreferredSize(dlbl);
        label.setMinimumSize(dlbl);
    	label.setMaximumSize(dlbl);

        if (!vertical) {
            if (extended) {
            	booleanp.setName(checkboxText);
                // Set the value but don't notify the listeners
                boolean notifyListeners = false;
                booleanp.setValue(earlyChecked[0] ? earlyChecked[1] : checked, notifyListeners);
        		booleanp.setName(checkboxText);
            }
                        
            // Create the ui components


            /*
             * Set component dimensions.
             */
                    
            // Use a fixed minimum width for the slider
            Dimension dslider = new Dimension(slider.getPreferredSize());
            dslider.width = DEFAULT_SLIDER_LABEL_SIZE / 2;
            slider.setMinimumSize(dslider);
            slider.setPreferredSize(dslider);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.anchor = GridBagConstraints.FIRST_LINE_START;
            gbc.anchor = GridBagConstraints.NORTHWEST;
            
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridheight = 1;
            gbc.gridwidth = 1;
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.ipadx = 0;
            gbc.ipady = 0;
            gbc.weightx = 0;
            gbc.weighty = 0;

            // Text label (fixed width)
            gbc.weightx = 0;
            gbc.insets = new Insets(8, 4, 2, 4);
            panel.add(label, gbc);

            // Slider
            gbc.gridx++;
            gbc.weightx = 1;
            gbc.insets = new Insets(2, 0, 2, 0); // top, left, bottom, right
            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel.add(slider, gbc);

            // Text field
            gbc.gridx++;
//            gbc.weightx = extended ? 0 : 0;
            gbc.weightx = 0;
            gbc.insets = new Insets(2, 0, 2, 0);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel.add(textField, gbc);

            // If extended, a boolean parameter
            if (extended) {
                gbc.gridx++;
                gbc.weightx = 0;
                gbc.insets = new Insets(0, 0, 0, 0);
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.anchor = GridBagConstraints.NORTHEAST;
                gbc.fill = GridBagConstraints.NONE;
                panel.add(booleanp.getControls(2 * DEFAULT_SLIDER_LABEL_SIZE / 3), gbc);
                
            }
        }
        else {
            // Use a fixed minimum width for the slider
            Dimension dslider = new Dimension(slider.getPreferredSize());
//            dslider.width = 10;
            dslider.height = DEFAULT_SLIDER_LABEL_SIZE / 2;
            slider.setMinimumSize(dslider);
            slider.setPreferredSize(dslider);
            slider.setMaximumSize(dslider);
                        
            GridBagConstraints gbc = new GridBagConstraints();

            // Add text label
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 0;
            gbc.weighty = 0;
            gbc.fill = GridBagConstraints.NONE;
            gbc.ipadx = 1;
            gbc.insets = new Insets(8, 4, 2, 4);
            gbc.anchor = GridBagConstraints.FIRST_LINE_START;
            panel.add(label, gbc);
            gbc.ipadx = 0;

            // Add text field
            gbc.gridx = 0;
            gbc.gridy = 1;
//            gbc.insets = new Insets(2, 0, 2, 0);
//            gbc.anchor = GridBagConstraints.LINE_START;
            gbc.anchor = GridBagConstraints.FIRST_LINE_END;
            panel.add(textField, gbc);

            // Add slider
            gbc.gridx = 2;
            gbc.gridy = 0;
            gbc.gridheight = 2;
            gbc.anchor = GridBagConstraints.PAGE_START;
            gbc.fill = GridBagConstraints.NONE;
//            gbc.insets = new Insets(2, 0, 2, 0); // top, left, bottom, right
//            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel.add(slider, gbc);
            label.setHorizontalAlignment(JLabel.RIGHT);
            textField.setHorizontalAlignment(JFormattedTextField.RIGHT);
            
            // Add empty panel for filling
            gbc.gridx = 3;
            gbc.gridy = 0;
            gbc.gridheight = 2;
            gbc.weightx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
//            gbc.insets = new Insets(2, 0, 2, 0);
//            gbc.anchor = GridBagConstraints.LINE_START;
            gbc.anchor = GridBagConstraints.NORTHEAST;
            panel.add(new JPanel(), gbc);
//            panel.add(new JButton("fill"), gbc);
        }
        
        updateView();

        return panel;
    }

    
     /**
     * Gets a  text box control for this double parameter.
     * @return controls
     */
    public JPanel getControls() {
        if ( panel != null ) return panel;
        this.isLogarithmic = false;
        // Create the ui components
        label = new JLabel( getName() );
        
        textField.setText( "" + value );

        textField.addPropertyChangeListener("value", this);
        
        Dimension d;
        
        d = new Dimension(textField.getPreferredSize());
        d.width = DEFAULT_SLIDER_LABEL_SIZE;
        textField.setPreferredSize(d);
        textField.setMinimumSize(d);

        d = new Dimension(label.getPreferredSize());
        if (d.width > DEFAULT_SLIDER_LABEL_SIZE) {
            label.setToolTipText( getName() );
        }
        d.width = DEFAULT_SLIDER_LABEL_SIZE;
        label.setPreferredSize(d);
        label.setMinimumSize(d);
        
        // Create the panel that holds the ui component
        HorizontalFlowPanel hfp = new HorizontalFlowPanel();
        hfp.add( label );
        hfp.add( textField );
        panel = hfp.getPanel();
        
        updateView();

        return panel;
    }

    
    /**
     * Called by the text field when a new value is committed. The updated value
     * of the text field is passed down to the represented parameter.
     * 
     * @param evt
     *        ignored
     * @see java.beans.PropertyChangeListener#propertyChange(PropertyChangeEvent)
     */
    @Override
	public void propertyChange(PropertyChangeEvent evt) {
        Object valueO = textField.getValue();
        if (valueO instanceof Number) {
            double v = ((Number)valueO).doubleValue();
            setValue(v);
            return;
        }
    }
    
    /**
     * Prevent excessive changes by using time intervals
     */
    
    private long t0 = 0, t1 = 0;
    private long dt = (long) (10 * 1e6);
    
    /**
     * Called whenever the state of this slider's <code>JSlider</code> is
     * changed. The change in state is passed down to this slider's
     * <code>Parameter</code>
     * 
     * @param event
     *        the change event (ignored)
     */
    @Override
	public void stateChanged(ChangeEvent event) {
    	
    	t1 = System.nanoTime();
    	if (t1 - t0 < dt) {
    		return;
    	}
    	t0 = t1;
    	
        int ivalue = slider.getValue();
        int sliderRange = slider.getMaximum() - slider.getMinimum();
        double min = getMinimum();
        double max = getMaximum();
        double v;
        if (isLogarithmic) {
            min = Math.log(min);
            max = Math.log(max);
        }
        v = min + ivalue * (max - min) / sliderRange;
        if (isLogarithmic) {
            v = Math.exp(v);
        }        
        setValue(v);
    }
   
    /**
     * Updates this parameter's <code>JSlider</code> and <code>JFormattedTextField</code>
     * to reflect the range and
     * current value of this slider's parameter.
     */
    private void updateView() {
        boolean isHolding = isHolding();
        hold(true);
        
        if ( slider != null ) {
            int sliderRange = slider.getMaximum() - slider.getMinimum();
            int ivalue;
            double min = getMinimum();
            double max = getMaximum();
            double v = getValue();
            if (isLogarithmic)
            {
                min = Math.log(min);
                max = Math.log(max);
                v = Math.log(v);
            }
            ivalue = slider.getMinimum()
                     + (int)(Math.round((v - min) * sliderRange / (max - min)));
            slider.setValue(ivalue);
        }
        // update the text field
        textField.setValue(getValue());        

        hold(isHolding);
    }
 
    /**
     * Check or uncheck the checkbox linked to this parameter.
     * @param b
     */
    public void setChecked(boolean b) {
//    	if (booleanp == null) defaultChecked = b;
//    	else booleanp.setValue(b);
//        booleanp.setEnabled(b);
    	booleanp.setValue(b);
    	
    	if (panel == null) {
    		earlyChecked[0] = true;
    		earlyChecked[1] = b;
    	}
    }
    
    /**
     * @return whether this control is enabled.
     */
    public boolean isChecked() {
        return booleanp.getValue();
    }
    
    /**
     * @return the default value of this parameter.
     */
    public double getDefaultValue() {
        return defaultValue;
    }

    /**
     * Set the default value for this parameter.
     * @param v
     */
    public void setDefaultValue(double v) {
        defaultValue = v;
    }

    public void setEnabled(boolean b) {
    	if (panel == null) return;
    	
        label.setEnabled(b);
        textField.setEnabled(b);
        slider.setEnabled(b);
        booleanp.setEnabled(b);
    }

	/**
	 * 
	 */
	public void invalidate() {
		notifyListeners();
	}

    @Override
    public void addParameterListener(ParameterListener listener) {
    	booleanp.addParameterListener(listener);
    	super.addParameterListener(listener);
    }

    @Override
    public void addParameterListeners(List<ParameterListener> listeners) {
    	booleanp.addParameterListeners(listeners);
        super.addParameterListeners(listeners);
    }

	public float getFloatValue() {
		return (float) getValue();
	}

}
