package swing.parameters;

import java.util.Arrays;

public class IntDiscreteParameter extends IntParameter {

	public static final int DEFAULT_BINS = 10;
	
	private int binCount;
	private int[] bins;
	
	/**
	 * Use default number of bins.
	 * @param name
	 * @param defaultValue
	 * @param minValue
	 * @param maxValue
	 */
	public IntDiscreteParameter(String name, int defaultValue, int minValue,
			int maxValue) {
		
		super(name, 0, 0, DEFAULT_BINS - 1);
		
		binCount = DEFAULT_BINS;
		bins = new int[binCount];
		
		int range = maxValue - minValue;
		int dbin = range / binCount;
		
		for (int i = 0; i < binCount; i++) {
			bins[i] = i * dbin;
		}
		
		// Set initial value
		setValue(defaultValue / dbin);
		updateView();
	}

	/**
	 * Use default number of bins.
	 * @param name
	 * @param defaultValue
	 * @param minValue
	 * @param maxValue
	 */
	public IntDiscreteParameter(String name, int defaultValue, int[] values) {
		super(name, 0, 0, values.length - 1);
		
		binCount = values.length;
		bins = Arrays.copyOf(values, binCount);
		
		// Set initial value
		int closest = findClosestBin(defaultValue);
		setValue(closest);
	}
	
    @Override
	public int getValue() {
		return bins[super.getValue()];
	}

    @Override
	protected void updateView() {
    	int val = getValue();
        textField.setValue(val);
    }

//    @Override
//	public void setValue(int value) {
//    	super.setValue(findClosest(value));
//	}

	private int findClosestBin(int value) {
		
		// Look through the bins and find the closest bound
		if (value <= bins[0]) return getMinimum();
		if (value >= bins[bins.length-1]) return getMaximum();
		
		// Search through all bins
		int binInf, binSup;
		for (int i = 0; i < binCount - 1; i++) {
			binInf = bins[i];
			
			if (i == binCount - 1) {
				if (value >= binInf) return i;
			}

			binSup = bins[i+1];

			int binRange = binSup - binInf;
			int binMid = binInf + binRange / 2;
			
			// If the number is between the inferior bin and mid
			if (value >= binInf && value < binMid) {
				return i;
			}
			// If the number is between the mid and superior bin
			else if (value >= binMid && value <= binSup) {
				return i+1;
			}
		}
		
		return 0;
	}
}
