package swing.parameters;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.JTriangleColorPanel;
import swing.component.VerticalFlowPanel;

public class Parameter3d extends ParameterManager {

	private DoubleParameter px = new DoubleParameter("", 0, 0, 1);
	private DoubleParameter py = new DoubleParameter("", 0, 0, 1);
	private DoubleParameter pz = new DoubleParameter("", 0, 0, 1);
	private JTriangleColorPanel jtcp;
	
	public Parameter3d() {
		addParameter(px);
		addParameter(py);
		addParameter(pz);

		jtcp = new JTriangleColorPanel(100);
		jtcp.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateParametersFromTriangle();
			}
		});
		
		ParameterListener pl = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				apply();
			}
		};
		px.addParameterListener(pl);
		py.addParameterListener(pl);
		pz.addParameterListener(pl);
	}
	
	private void updateParametersFromTriangle() {
		Point3d p = jtcp.getPoint();
		hold(true);
		px.setValue(p.x);
		py.setValue(p.y);
		pz.setValue(p.z);
		hold(false);
		
		notifyListeners();
	}
	
	public Point3d getValue() {
		return new Point3d(px.getValue(), py.getValue(), pz.getValue());
	}
	
	public void setValue(Point3d p) {
		hold(true);
		px.setValue(p.x);
		py.setValue(p.y);
		pz.setValue(p.z);
		apply();
		hold(false);
	}
	
	private Vector3d v = new Vector3d();
	private void apply() {
		// First normalize this point
		hold(true);
		v.set(px.getValue(), py.getValue(), pz.getValue());
		
		if (v.length() > 1e-4)
			v.normalize();
		
		px.setValue(v.x);
		py.setValue(v.y);
		pz.setValue(v.z);
		
		jtcp.setPoint(px.getValue(), py.getValue(), pz.getValue());
		hold(false);
	}
	
	public JPanel getControls() {
//		VerticalFlowPanel vfp = new VerticalFlowPanel();
//		JPanel vfp = new JPanel(new BorderLayout());
		JPanel vfp = new JPanel(new GridBagLayout());
		vfp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Direction", TitledBorder.LEFT, TitledBorder.CENTER));

		// Set minimum size
		vfp.setPreferredSize(new Dimension(350, 140));
		
		// Add color triangle
		GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(0, 20, 0, 0);
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 0;
        gbc.ipady = 0;
        gbc.weightx = 1;
        gbc.weighty = 0;
		vfp.add(jtcp, gbc);
		
		// Add sliders
		VerticalFlowPanel lp = new VerticalFlowPanel();
//		lp.setBorder(BorderFactory.createTitledBorder(
//				BorderFactory.createEtchedBorder(), "", TitledBorder.LEFT, TitledBorder.CENTER));
		lp.add(px.getSliderControls());
		lp.add(py.getSliderControls());
		lp.add(pz.getSliderControls());
		
		gbc.insets = new Insets(0, 0, 0, 0);
        gbc.anchor = GridBagConstraints.EAST;
		gbc.gridx = 1;
		gbc.weightx = 2;
		vfp.add(lp.getPanel(), gbc);

		return vfp;
	}
}
