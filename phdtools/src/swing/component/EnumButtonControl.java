package swing.component;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import swing.parameters.Parameter;


/**
 * @author piuze
 * @param <E>
 */
public final class EnumButtonControl<E extends Enum<E>> extends Parameter {

    /**
     * @param name a title for this combox box
     * @param defaultEnum The initial value of the combox box, as a <code>E</code> enum type.
     */
    public EnumButtonControl(String name, Class<E> enumClass) {
        super(name);

        this.enumClass = enumClass;
        
        values = enumClass.getEnumConstants();
        
        if (values == null) throw new RuntimeException("Invalid enum type");
        
        createControls();
    }
    
    private Class<E> enumClass;

    private E[] values;
    
    private JButton[] buttons;

    public void setActionListener(Enum<E> e, ActionListener al) {
    	buttons[e.ordinal()].addActionListener(al);
    }
    
    private JPanel panel;
    
    private void createControls() {
    	panel = new JPanel();
//    	panel.setLayout(new BorderLayout());
        int n = values.length;
        int m = (int) Math.ceil(n / 3);
        panel.setLayout(new GridLayout(m+1, 3));
        
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), enumClass.getSimpleName(), TitledBorder.LEFT, TitledBorder.CENTER));

        buttons = new JButton[n];
        for (int i = 0; i < n; i++) {
        	buttons[i] = new JButton(values[i].toString());
        	panel.add(buttons[i]);
        }
    }
    
    /**
     * @return A combox box for selecting between enums.
     */
    public JPanel getControls() {
        
        return panel;
    }    
}
