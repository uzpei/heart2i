package swing.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class LabelComboBox<T> extends JPanel {
	
	private static final long serialVersionUID = -1863450505908524437L;
	private List<T> values;
	private JComboBox<String> interList;
	private DefaultComboBoxModel<String> dcm;
	
	public T getSelected() {
        return values.get(Math.max(0, interList.getSelectedIndex()));
	}

	public void updateData(List<T> values) {
		this.values = values;
		
		dcm.removeAllElements();
		
    	for(T t : values) {
    		System.out.println("Adding " + t.toString());
    	    dcm.addElement(t.toString());
    	}
	}
	
	public List<T> getValues() {
		return values;
	}

    public LabelComboBox(String label, List<T> values, ActionListener listener) {
        setName(label);

        dcm = new DefaultComboBoxModel<String>();
        interList = new JComboBox<String>();
        interList.setModel( dcm );
        
    	updateData(values);
    	
        interList.addActionListener(listener);

        setLayout(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 0;
        gbc.ipady = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
        
        JLabel lblName = new JLabel(label);
        lblName.setHorizontalAlignment(SwingConstants.LEFT);

        gbc.weightx = 0;
        gbc.insets = new Insets(4, 4, 2, 4);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(lblName, gbc);
        
        gbc.weightx = 1;
        gbc.gridx++;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(interList, gbc);
    }

	public int getSelectedIndex() {
		return interList.getSelectedIndex();
	}

	public void setSelectedIndex(int index) {
		interList.setSelectedIndex(index);
	}

}
