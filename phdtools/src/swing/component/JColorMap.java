package swing.component;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;

import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import extension.matlab.MatlabScript;

public class JColorMap extends JPanel {

	private static Font labelFont = new Font("Consolas", Font.PLAIN, 12);
	
	private final static Dimension cgdim = new Dimension(250,20);
//	private final static Dimension jcmdim = new Dimension(500,50);
	
	private JColorGradient gradient;
	
	private Point mp1;
	private Point mp2;
	private Point delta = new Point();
	
	private Dimension rangeDim = new Dimension(2, 26);
	
	private double minValue = 0;
	private double maxValue = 0;
	
	/**
	 * Range handle relative positions
	 */
	private int rangePos = 0;
	private int rangeNeg = 0;
	private int rangeZero = 0;
	
	private boolean rangePosSelected = false;
	private boolean rangeNegSelected = false;
	private boolean rangeZeroSelected = false;
	
	private int pickRadius = 10;
	private boolean mouseDown = false;
	
	private List<ActionListener> changeListeners = new LinkedList<ActionListener>();
	
	private BooleanParameter continuous = new BooleanParameter("continuous", false);
	
	private Color ccmin;
	private Color ccmax;
	private Color czero;
	
	private final static Color N_DEFAULT = Color.BLUE;
	private final static Color Z_DEFAULT = Color.BLACK;
	private final static Color P_DEFAULT = Color.GREEN;
	private final static double NMIN_DEFAULT = -1;
	private final static double NMAX_DEFAULT = 1;
	
	private JLabel nlabel = new JLabel();
	private JLabel zlabel = new JLabel();
	private JLabel plabel = new JLabel();
	
	public void addChangeListener(ActionListener l) {
		changeListeners.add(l);
	}
	
	private void notifyListeners() {
		for (ActionListener l : changeListeners) {
			l.actionPerformed(null);
		}
	}

	public JColorMap() {
		this("Color scale", NMIN_DEFAULT, NMAX_DEFAULT, N_DEFAULT, P_DEFAULT, Z_DEFAULT);
	}

	public JColorMap(String title, Color minColor, Color maxColor, Color zeroColor, Dimension size) {
		this(title, NMIN_DEFAULT, NMAX_DEFAULT, minColor, maxColor, zeroColor, size);
	}
	
	public JColorMap(String title, Color minColor, Color maxColor, Color zeroColor) {
		this(title, NMIN_DEFAULT, NMAX_DEFAULT, minColor, maxColor, zeroColor);
	}
	
	private Timer notificationTimer;
	
	public JColorMap(String title, double vmin, double vmax, Color minColor, Color maxColor, Color zeroColor) {
		this(title, vmin, vmax, minColor, maxColor, zeroColor, null);
	}	
	/**
	 * 
	 * @param vmin the value associated with the minimum
	 * @param vmax the value associated with the maximum
	 * @param minColor min value color
	 * @param maxColor max value color
	 * @param zeroColor zero value color
	 */
	public JColorMap(String title, double vmin, double vmax, Color minColor, Color maxColor, Color zeroColor, Dimension size) {
		super();
		
		// Color padding
		int cpad = 15;
		
		super.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent arg0) {
				gradient.createColorScale();
			}
			
			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
//		System.out.println("Min/max = " + vmin + ", " + vmax);

		setMinMax(vmin, vmax);

		notificationTimer = new Timer(10, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				notifyListeners();
			}
		});
		notificationTimer.setCoalesce(true);
		notificationTimer.setRepeats(false);

		addMouseListener(nlabel, "Choose negative color", minColor);
		addMouseListener(zlabel, "Choose zero color", zeroColor);
		addMouseListener(plabel, "Choose positive color", maxColor);
		nlabel.setOpaque(true);
		zlabel.setOpaque(true);
		plabel.setOpaque(true);
		nlabel.setBackground(minColor);
		zlabel.setBackground(zeroColor);
		plabel.setBackground(maxColor);
		
		Dimension cdim = new Dimension(cpad, cpad);
		nlabel.setMinimumSize(cdim);
		nlabel.setPreferredSize(cdim);
		zlabel.setMinimumSize(cdim);
		zlabel.setPreferredSize(cdim);
		plabel.setMinimumSize(cdim);
		plabel.setPreferredSize(cdim);
		
		setColorsFromPickers();
		
		super.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), title, TitledBorder.LEFT, TitledBorder.CENTER));

		// Set grid layout
		super.setLayout(new GridBagLayout());

//		Dimension jcmdim = new Dimension(200,50);
//		setPreferredSize(jcmdim);
//		setMinimumSize(jcmdim);
//		setMaximumSize(jcmdim);

//		Dimension cgdim = new Dimension(250, 20);
		GridBagConstraints gbc = new GridBagConstraints();
		
		// Add gradient
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 0;
        gbc.ipady = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
		gradient = new JColorGradient(cgdim);

		if (size != null) {
			Dimension adaptedSize = new Dimension(size.width, cgdim.height + cpad);
//			super.setSize(adaptedSize);
//			super.setPreferredSize(adaptedSize);
			gradient.setSize(adaptedSize);
//			gradient.setPreferredSize(adaptedSize);
		}

		gradient.createColorScale();
		add(gradient, gbc);

		// Add color labels
        gbc.weightx = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
		gbc.insets = new Insets(0, 5, 0, 0);
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.weightx = 0;

		gbc.gridx = 2;
		add(nlabel, gbc);

		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridx = 3;
		add(zlabel, gbc);

		gbc.insets = new Insets(0, 0, 0, 5);
		gbc.gridx = 4;
		add(plabel, gbc);

		// Add continuous button
        gbc.weightx = 0.5;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.LINE_END;
		gbc.gridx = 5;
		gbc.insets = new Insets(0, 0, 0, 0);
		add(continuous.getControls(), gbc);

		continuous.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				if (gradient != null)
					gradient.createColorScale();
				
//				timer.restart();
				update(false);
			}
		});

		// Set ranges
		rangeNeg = 0;
		rangePos = cgdim.width;
		rangeZero = cgdim.width / 2;

		updateRange(true);

//		Timer timer = new Timer(1000, new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				validate();
//				repaint();
//			}
//		});
//		timer.start();
		
		this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				mouseDown = false;
				rangePosSelected = false;
				rangeNegSelected = false;
				rangeZeroSelected = false;
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					
					if (isInRangePos(mp1, rangePos)) {
						plabel.getMouseListeners()[0].mousePressed(e);
					}
					else if (isInRangePos(mp1, rangeNeg)) {
						nlabel.getMouseListeners()[0].mousePressed(e);
					}
					else if (isInRangePos(mp1, rangeZero)) {
						zlabel.getMouseListeners()[0].mousePressed(e);
					}
				}				
			}
		});
		
		this.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				mp1 = e.getPoint();
				
				if (isInRangePos(mp1, rangePos) || isInRangePos(mp1, rangeNeg) || isInRangePos(mp1, rangeZero)) {
					setCursor(new Cursor(Cursor.HAND_CURSOR));
				}
				else {
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				mp1 = e.getPoint();
				
//				System.out.println(mp1);

				// If this is the first movement, make sure
				// user clicked within the color range
				
				if (!mouseDown) {
					rangePosSelected = isInRangePos(mp1, rangePos);
					rangeNegSelected = isInRangePos(mp1, rangeNeg);
					rangeZeroSelected = isInRangePos(mp1, rangeZero);

					if (rangeZeroSelected) {
						rangeNegSelected = false;
						rangePosSelected = false;
					}
					
					if (rangePosSelected || rangeNegSelected || rangeZeroSelected) {
						if (e.isShiftDown()) {
							rangePosSelected = true;
							rangeNegSelected = true;
							rangeZeroSelected = true;
						}
						
						mouseDown = true;
						mp2 = mp1;
					}
				}
				
				if (mouseDown) {
					if (mp2 != null) delta.x = mp1.x - mp2.x;
					
					update(false);
				}
				mp2 = mp1;
			}
		});
	}
	
	public void set(IntensityVolume volume, IntensityVolumeMask mask) {
		double[] minmax = volume.getMinMax(mask);
//		setMinMax(minmax[0], minmax[1], volume.getMean(mask), volume.getStandardDeviation(mask));
		setMinMax(minmax[0], minmax[1]);
	}
	
	public Color[] sample(int n) {
		double delta = maxValue - minValue;
		double split = delta / (n-1);
		
		Color[] sample = new Color[n];
		for (int i = 0; i < n; i++) {
			sample[i] = getColor(minValue + i * split);
			
			if (sample[i] == null) return null;
		}
		
		return sample;
	}
	
	public String sampleMatlab(int n) {
		Color[] sample = sample(n);
		if (sample == null) return ""; 
				
		String s = "[";
		for (int i = 0; i < sample.length; i++)
			s += sample[i].getRed() / 255d + " " + sample[i].getGreen() / 255d + " " + sample[i].getBlue() / 255d + ";";
		s += "]";
		
		return s;
	}
	
	public void setMinMax(double min, double max) {
		minValue = min;
		maxValue = max;
		
		update(false);
	}

	public void setMinMax(double min, double max, double mean, double std) {
		minValue = min;
		maxValue = max;

		double fullRange = maxValue - minValue;
		
		rangeNeg = rint(Math.abs((mean - 2 * std - minValue) / fullRange) * gradient.getPreferredSize().width);

		rangeZero = rint(Math.abs((mean - minValue) / fullRange) * gradient.getPreferredSize().width);
		
		rangePos = rint(Math.abs((mean + 2 * std - minValue) / fullRange) * gradient.getPreferredSize().width);
	
		update(false);
	}

	public double getMinValue() {
		return minValue;
	}
	
	public double getMaxValue() {
		return maxValue;
	}
	
	public double getZeroValue() {

		double width = gradient.getSize().width;
		
		// Return the value that corresponds to the zero color
		double mid = rangeZero;
		if (continuous.getValue()) {
			mid = (rangePos + rangeNeg) / 2;
		}
		
		return minValue + (mid / width) * (maxValue - minValue);
	}

	private void setColorsFromPickers() {
		ccmin = nlabel.getBackground();
		ccmax = plabel.getBackground();
		czero = zlabel.getBackground();
		
		if (gradient != null)
			gradient.createColorScale();
		
		update(false);
//		timer.restart();
	}
	
	private void addMouseListener(final JLabel label, final String text, final Color defaultColor) {
		label.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				Color newColor = JColorChooser.showDialog(
	                     null,
	                     text,
	                     defaultColor);
				
				if (newColor != null) {
					label.setBackground(newColor);
					setColorsFromPickers();
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});

	}

	protected boolean isInRangePos(Point p, int rangePos) {
		Point g = gradient.getLocation();
		if (p.x < g.x + rangePos - rangeDim.width / 2 - pickRadius)
			return false;
		else if (p.x > g.x + rangePos + rangeDim.width / 2 + pickRadius)
			return false;
		else if (p.y > g.y + gradient.getSize().height)
			return false;
		else if (p.y < g.y)
			return false;
		else
			return true;
	}

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);

		labelFont = new Font("Verdana", Font.PLAIN, 12);
		
		/*
		 * Add text and graph overlays
		 */
		Point p0 = gradient.getLocation();
		Dimension dim = gradient.getPreferredSize();

		g.setColor(Color.BLACK);
//		g.setFont(labelFont);
		
		// Color labels
		int xpad = 5;
		Dimension cdim;
		
		// Min color label and value
		String cminLabel = "Cmin";
//		cdim = getLabelTextSize(cminLabel, labelFont);
//		AttributedString as1 = new AttributedString(cminLabel);
//		as1.addAttribute(TextAttribute.FAMILY, labelFont.getFamily());
//		as1.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER, 1, 4);
//		as1.addAttribute(TextAttribute.SIZE, labelFont.getSize());
//		g.drawString(as1.getIterator(), p0.x - cdim.width + xpad, p0.y + cdim.height);

		// Min value
		String smin = nf.format(minValue);
		g.setFont(labelFont);
		cdim = getLabelTextSize(smin, labelFont);
//		g.drawChars(cmin.toCharArray(), 0, cmin.length(), p0.x - cdim.width / 2, p0.y + dim.height + cdim.height);
//		g.drawChars(cmin.toCharArray(), 0, cmin.length(), p0.x - cdim.width - xpad, p0.y + cdim.height);
		
		// Max color label and value
//		String cmaxLabel = "Cmax";
//		cdim = getLabelTextSize(cmaxLabel, labelFont);
//		AttributedString as2 = new AttributedString(cmaxLabel);
//		as2.addAttribute(TextAttribute.FAMILY, labelFont.getFamily());
//		as2.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER, 1, 4);
//		as2.addAttribute(TextAttribute.SIZE, labelFont.getSize());
//		g.drawString(as2.getIterator(), p0.x + dim.width + xpad, p0.y + cdim.height);

		// Max value
		String smax = nf.format(maxValue);
		cdim = getLabelTextSize(smax, labelFont);
//		g.drawChars(cmax.toCharArray(), 0, cmax.length(), p0.x + dim.width - cdim.width / 2, p0.y + dim.height + cdim.height);
//		g.drawChars(cmax.toCharArray(), 0, cmax.length(), p0.x + dim.width + xpad, p0.y + cdim.height);

		// Draw color scale end lines
		int ls = 5;
//		g.drawLine(p0.x, p0.y - ls, p0.x, p0.y + cgdim.height + ls);
//		g.drawLine(p0.x + cgdim.width, p0.y - ls, p0.x + cgdim.width, p0.y + cgdim.height + ls);

		// Put box around colorscale
		g.setColor(Color.black);
		g.drawRect(p0.x, p0.y, dim.width - 1, dim.height - 1);

		// Draw range handle
		g.setColor(Color.WHITE);
		
		String cneg = nf.format(boundNegative);
		cdim = getLabelTextSize(cneg, labelFont);
//		g.drawChars(cneg.toCharArray(), 0, cneg.length(), p0.x + rangeNeg - cdim.width, p0.y + dim.height + cdim.height);
//		g.drawChars(cneg.toCharArray(), 0, cneg.length(), p0.x - cdim.width - xpad, p0.y + dim.height / 2 + cdim.height / 4);
		g.drawChars(cneg.toCharArray(), 0, cneg.length(), p0.x+ xpad, p0.y + dim.height / 2 + cdim.height / 4);

		String cpos = nf.format(boundPositive);
		cdim = getLabelTextSize(cpos, labelFont);
//		g.drawChars(cpos.toCharArray(), 0, cpos.length(), p0.x + rangePos, p0.y + dim.height + cdim.height);
//		g.drawChars(cpos.toCharArray(), 0, cpos.length(), p0.x + dim.width + xpad, p0.y + dim.height / 2 + cdim.height / 4);
		g.drawChars(cpos.toCharArray(), 0, cpos.length(), p0.x + dim.width - cdim.width - xpad, p0.y + dim.height / 2 + cdim.height / 4);

		double fullRange = maxValue - minValue;
		double zval = minValue + rangeZero * fullRange / gradient.getPreferredSize().width;
		if (Math.abs(zval) < fullRange / gradient.getPreferredSize().width)
			zval = 0;
		
		cpos = nf.format(zval);
		cdim = getLabelTextSize(cpos, labelFont);
//		g.drawChars(cpos.toCharArray(), 0, cpos.length(), p0.x + rangePos, p0.y + dim.height + cdim.height);
//		g.drawChars(cpos.toCharArray(), 0, cpos.length(), p0.x + dim.width + xpad, p0.y + dim.height / 2 + cdim.height / 4);
		g.drawChars(cpos.toCharArray(), 0, cpos.length(), p0.x + rangeZero, p0.y + dim.height / 2 + cdim.height / 4);

		// True zero
		g.setColor(Color.yellow);
		g.fillRect(p0.x + dim.width / 2 - 1, p0.y, 2, dim.height);

		g.setColor(new Color(1f, 1f, 1f, 0.8f));
		g.fillRect(p0.x + rangeNeg - rangeDim.width / 2, p0.y - rangeDim.height / 2 + dim.height / 2, rangeDim.width, rangeDim.height);
		g.fillRect(p0.x + rangePos - rangeDim.width / 2, p0.y - rangeDim.height / 2 + dim.height / 2, rangeDim.width, rangeDim.height);

		// Current zero
		g.setColor(new Color(0f, 0f, 0f, 0.4f));
		g.fillRect(p0.x + rangeZero - rangeDim.width / 2, p0.y - rangeDim.height / 2 + dim.height / 2, rangeDim.width, rangeDim.height);

		gradient.createColorScale();
	}
	
	private void update(boolean quiet) {
		updateRange(quiet);
		
		invalidate();
		repaint();
	}
	
	private void updateRange(boolean quiet) {
		if (gradient == null) return;
		
		Dimension dim = gradient.getPreferredSize();

		int snapStrength = 2;

		if (rangeNegSelected) {
			rangeNeg += delta.x;
			rangeNeg = rangeNeg < snapStrength ? 0 : rangeNeg;
			rangeNeg = rangeNeg > rangeZero ? rangeZero: rangeNeg;
		}
		if (rangePosSelected) {
			rangePos += delta.x;
			rangePos = rangePos < rangeZero ? rangeZero : rangePos;
			rangePos = rangePos > dim.width - snapStrength? dim.width : rangePos;
		}

		if (rangeZeroSelected) {
			rangeZero += delta.x;
			rangeZero = rangeZero < 0 ? 0 : rangeZero;
			rangeZero = rangeZero > dim.width ? dim.width : rangeZero;
			
			rangeZero = Math.abs(rangeZero - dim.width / 2) < snapStrength ? dim.width / 2: rangeZero;
		}
		
		double width = dim.width;
		double fullRange = maxValue - minValue;
		boundNegative = minValue + rangeNeg / width * fullRange;
		boundPositive = minValue + rangePos / width * fullRange;

		delta.x = 0;
		
		if (!quiet) notificationTimer.restart();
	}
	
	public Color getColor(double v) {
		if (gradient == null || gradient.colorScale == null) return null;
		
		// Get color from colorscale image
		double range = maxValue - minValue;
		double pos = (v - minValue) / range;
//		System.out.println(pos);

		int min = 0;
		int max = gradient.colorScale.getWidth() - 1;
		
		// Limit to range [0, 1]
//		pos = Math.max(0, Math.min(1, pos));
		int i = rint((max-min) * pos);
		int j = gradient.getSize().height / 2;

		i = Math.max(min, Math.min(i, max));
		
//		System.out.println(pos);
//		try {
//			int ii = gradient.colorScale.getHeight() / 2;
//			for (int ji = 0; ji < gradient.colorScale.getWidth(); ji += 1) {
//				System.out.println(new Color(gradient.colorScale.getRGB(ji, ii)));
//			}
//		}
//		catch (Exception e) {
//			System.out.println("Not a valid color");
//			return null;
//		}
		
	return new Color(gradient.colorScale.getRGB(i, j));
		
	}

	private double boundNegative, boundPositive;

	private Dimension getLabelTextSize(String text, Font font) {
		FontMetrics metrics = getFontMetrics(font);
		int adv = metrics.stringWidth(text);
		int hgt = metrics.getHeight();
		return new Dimension(adv, hgt);
	}

	private class JColorGradient extends JComponent {
		private Dimension dimension;
		protected BufferedImage colorScale;
		
		public Color getColor(int i, int j) {
			return new Color(colorScale.getRGB(i, j));
		}
		
		JColorGradient(Dimension dimension) {
			this.setPreferredSize(dimension);
//			this.setMinimumSize(dimension);
//			this.setMaximumSize(dimension);
		}
		
		private void createColorScale() {
			Dimension size = getSize();
			if (size.height < 5 || size.width < 5) return;
			
			BufferedImage img = new BufferedImage(size.width, size.height,
					BufferedImage.TYPE_INT_ARGB);

			Graphics2D graphs = img.createGraphics();
			graphs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphs.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			
			int imax = size.width;
			int jmax = size.height;

			int k = size.height / 2;
			
			// Make sure we have saturated colors at the end points
			int coffset = 2;
			
			int xzero = rangeZero;
			Color czeroc = new Color(czero.getRed(), czero.getGreen(), czero.getBlue());
			
			if (continuous.getValue()) {
//				xzero = (rangePos + rangeNeg) / 2; 
				int r = MathToolset.roundInt((ccmin.getRed() + ccmax.getRed()) / 2d);
				int g = MathToolset.roundInt((ccmin.getGreen() + ccmax.getGreen()) / 2d);
				int b = MathToolset.roundInt((ccmin.getBlue() + ccmax.getBlue()) / 2d);
				czeroc = new Color(r, g, b);
			}

			/*
			 *  Negative color to zero
			 */
			GradientPaint gp = new GradientPaint(rangeNeg + coffset, k, ccmin, xzero, k, czeroc);
			graphs.setPaint(gp);
			graphs.fillRect(0, 0, xzero, jmax);

			/*
			 *  Zero color to positive color
			 */
			gp = new GradientPaint(xzero, k, czeroc, rangePos - coffset, k, ccmax);
			graphs.setPaint(gp);
			graphs.fillRect(xzero, 0, getSize().width - xzero, jmax);
			
			// Set color scale
			colorScale = img;
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			if (colorScale == null) {
				createColorScale();
			}
			
//			super.paintComponent(g);

			g.drawImage(colorScale, 0, 0, this);
		}
	}

	private int rint(double d) {
		return (int) Math.round(d);
	}

	/**
	 * Export the current colormap to a pdf file via a Matlab script.
	 */
	public void saveColormap(String outpath, String outName) {
		MatlabScript script = saveColormapScript(outpath, outName, 32);
		script.exit();
		script.run();
	}
	
	public MatlabScript saveColormapScript(String outpath, String outName, int fontSize) {
		MatlabScript script = new MatlabScript();

		script.writeLine("colorbar");
		script.writeLine("colormap(" + sampleMatlab(256) + ")");
		DecimalFormat format = new DecimalFormat("#.###");
		format.setRoundingMode(RoundingMode.HALF_UP);

		script.writeLine("colormap_export('" + new File(outpath).getAbsolutePath() + "/', " + "'" + outName + "'" + ", [" + format.format(getMinValue()) + " " + format.format(getMaxValue()) + "], " +fontSize + ")");
		return script;
		
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new JColorMap());

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(500, 500));
		frame.pack();
		frame.setVisible(true);
		
	}

}
