package swing.component.histogram;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * A region with left/right boundaries
 * @author piuze
 *
 */
public class HistogramExcludedRegion {
	private boolean isSelected = false;
	private boolean isResizingLeft = false;
	private boolean isResizingRight = false;
	private boolean collapsed = false;
	private int collapsedPreWidth = 0;
	
	private Dimension dimension = new Dimension();
	private int xPos = 0;
	
	public HistogramExcludedRegion(Dimension initialDimension, int positionX) {
		this.dimension = new Dimension(initialDimension);
		this.xPos = positionX;
	}
	
	public void release() {
		isSelected = false;
		isResizingLeft = false;
		isResizingRight = false;
	}
	public boolean isSelected() {
		return isSelected;
	}

	public boolean isResizing() {
		return isResizingLeft || isResizingRight;
	}
	
	public boolean isResizingLeft() {
		return isResizingLeft;
	}

	public boolean isResizingRight() {
		return isResizingRight;
	}

	private void setSelected(boolean selected, boolean resizingLeft, boolean resizingRight) {
		isSelected = selected;
		isResizingLeft = resizingLeft;
		isResizingRight = resizingRight;
	}
	
	private boolean isNarrow() {
		return dimension.width < 2;
	}
	
	public boolean pick(Point p, int pickRadius) {
		// Restrict movement if we are narrow
		if (isNarrow()) {
			// Determine which boundary we're on
			// TODO: take care of this more rigorously... maybe pass the boundaries in pick(...) ?
			if (xPos <= 5 && Math.abs(p.x - xPos) < pickRadius) {
				setSelected(false, false, true);
			}
			if (xPos > 5 && Math.abs(p.x - xPos) < pickRadius) {
				setSelected(false, true, false);
			}
			
			return false;
		}
		
		// Detect exact boundaries
		if (Math.abs(p.x - xPos) < pickRadius) {
			setSelected(false, true, false);
		}
		else if (Math.abs(p.x - (xPos + getDimension().width)) < pickRadius) {
			setSelected(false, false, true);
		}
		// Detect outside
		else if (p.x < xPos)
			setSelected(false, false, false);
		else if (p.x > xPos + getDimension().width)
			setSelected(false, false, false);
		else {
			setSelected(true, false, false);
			return true;
		}
		
		return false;
	}
	
	private void advance(int dx, int leftBoundary, int rightBoundary) {
		xPos += dx;
		xPos = xPos > rightBoundary ? rightBoundary : xPos; 
		xPos = xPos < leftBoundary ? leftBoundary : xPos; 
	}
	
	private void resizeLeft(int dx) {
		if (dx > 0 && getDimension().width <= 1) return;
		
		xPos += dx;
		getDimension().width += -dx;
	}

	private void resizeRight(int dx) {
		if (dx < 0 && getDimension().width <= 1) return;
		
		getDimension().width += dx;
	}

	private void clamp(int leftBoundary, int rightBoundary) {
		if (xPos + getDimension().width > rightBoundary) {
			getDimension().width = rightBoundary - xPos;
		}

		if (xPos < leftBoundary) {
			xPos = leftBoundary;
		}
		
		if (xPos > rightBoundary) {
			xPos = rightBoundary;
		}

//		System.out.println("Before: " + getDimension());
		getDimension().width = Math.min(rightBoundary, getDimension().width);
		getDimension().width = Math.max(1, getDimension().width);
		
//		System.out.println("After: " + getDimension());
	}
	
	private boolean isOnLeftBoundary(int boundaryPos, int dx) {
		if (xPos + dx < boundaryPos) return true;
		else return false;
	}

	private boolean isOnRightBoundary(int boundaryPos, int dx) {
		if (xPos + dx + dimension.width > boundaryPos) return true;
		else return false;
	}
	
	public void processAdjustment(int dx, int leftBoundary, int rightBoundary) {
		if (isCollapsed())
			return;
		
		boolean isLeft = isOnLeftBoundary(leftBoundary, dx);
		boolean isRight = isOnRightBoundary(rightBoundary, dx);

		if (isResizingLeft() && !isLeft) {
			resizeLeft(dx);
		}

		if (isResizingRight() && !isRight) {
			resizeRight(dx);
		}
		
		if (dx < 0 && isLeft || dx > 0 && isRight) {
			return;
		}

		if (isSelected()) {
			advance(dx, leftBoundary, rightBoundary);
		}

		clamp(leftBoundary, rightBoundary);
	}
	
	public Dimension getDimension() {
		return dimension;
	}
	
	public int getLeftPosition() {
		return xPos;
	}

	public int getRightPosition() {
		return xPos + getDimension().width;
	}

	public void draw(Graphics2D g, int height, Color cin, Color cbound) {
		int width = Math.max(getDimension().width, 1);
		int x = getLeftPosition();
		
		int cwidth = 5;
		Color gradient = new Color(0, 0, 0, 0);
		GradientPaint gp = new GradientPaint(x, 0, gradient, x + cwidth, cwidth, cin, true);
		
		g.setPaint(gp);
		
//		g.setColor(cin);
		g.fillRect(x, 0, width, height);
		g.drawRect(x, 0, width, height);

		g.setColor(cbound);
		g.drawRect(x, 0, width, height - 1);
		
	}

	public void collapse() {
		collapsedPreWidth = dimension.width;
		dimension.width = 0;
		collapsed = true;
	}

	public void unCollapse() {
		dimension.width = collapsedPreWidth;
		collapsed = false;
	}

	public boolean isCollapsed() {
		return collapsed;
	}
}
