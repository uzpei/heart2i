package swing.component.histogram;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;

import swing.component.JColorMap;
import swing.component.JPanelPainter;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

public class JHistogram extends JPanel implements GLObject {

	private IntParameter histobins = new IntParameter("bins", 0, 0, 0);

	private BooleanParameter showGrids = new BooleanParameter("grid", false);

	private BooleanParameter envelope = new BooleanParameter("envelope", true);

	private BooleanParameter fill = new BooleanParameter("fill", false);

	private final static Dimension DEFAULT_DIMENSION = new Dimension(300,200);

	private Histogram histogram;
	
	private NumberFormat nf = NumberFormat.getNumberInstance();
	
	private JHistogramData hdata;
	
	private String name = "Histogram";
	
	private final static int SNAP_RADIUS = 3;
	
	public static final boolean DefaultDrawText = true;
	
	private boolean drawMore = DefaultDrawText;
	
	public JHistogram(BufferedImage bi) {
		this(new Histogram(bi), new Dimension(DEFAULT_DIMENSION), false);
	}
	
	public JHistogram() {
		this(null, DEFAULT_DIMENSION, false);
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
        setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), name, TitledBorder.LEFT, TitledBorder.CENTER));
	}
	
	public JHistogram(Histogram h, Dimension size, boolean lightWeight) {
		histogram = h;
		
		if (h != null) {
			histobins.setMaximum(h.getBinCount());
			histobins.setValue(h.getBinCount());
		}
		else {
			histobins.setMaximum(100);
			histobins.setValue(50);
		}

		setName(name);
		
		// Set layout
		int controlPadding = 50;
		Dimension dimension = new Dimension(size.width, size.height - controlPadding);
		hdata = new JHistogramData(dimension);
		
		setLayout(new GridBagLayout());

		super.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
//				System.out.println(getSize());
//				hdata.setPreferredSize(getSize());
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});

		assemble(lightWeight);
		
		ParameterListener updateListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				hdata.update(false);
			}
		};
		showGrids.addParameterListener(updateListener);
		envelope.addParameterListener(updateListener);
		fill.addParameterListener(updateListener);			
	}
	
	public void setShowGrids(boolean b) {
		showGrids.setValue(b);
	}
	
	public void setLightWeight(boolean lightWeight) {
		assemble(lightWeight);
		super.invalidate();
		super.updateUI();
	}
	
	private void assemble(boolean lightWeight) {
		super.removeAll();
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 2;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 0.5;
		constraints.weighty = 0.5;
        constraints.fill = GridBagConstraints.BOTH;
		add(hdata, constraints);

		if (lightWeight) {
			drawMore = false;
		}
		else {
			constraints.gridy = 1;
			constraints.gridx = 1;
			constraints.gridwidth = 1;
			constraints.weightx = 1;
			constraints.insets = new Insets(0, 0, 0, 10);
			constraints.anchor = GridBagConstraints.EAST;
	        constraints.fill = GridBagConstraints.NONE;
			
	        histobins.getSliderControls().setBorder(BorderFactory.createLineBorder(Color.black));
			add(histobins.getSliderControls(), constraints);
			histobins.setTextJustification(JTextField.RIGHT);

			JPanel flagPanel = new JPanel(new GridLayout(2, 2));
			
			constraints.anchor = GridBagConstraints.WEST;
			constraints.gridy = 1;
			constraints.gridx = 0;

			flagPanel.add(fill.getControls(false));
			flagPanel.add(envelope.getControls(false));
			flagPanel.add(showGrids.getControls(false));
			add(flagPanel, constraints);
		}
	}
	
	public Histogram getHistogram() {
		return histogram;
	}
	
	public void update(double[] data) {
		histogram.update(data);
		updateHistogram();
	}

	private JColorMap colorMap;
	public void setColorMap(JColorMap colorMap) {
		this.colorMap = colorMap;
		
		colorMap.addChangeListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				updateHistogram();
			}
		});
		updateHistogram();
	}
	
	public void setHistogram(Histogram histogram) {
		this.histogram = histogram;
		
		updateHistogram(); 
	}
	
	public void updateHistogram() {
		if (histogram == null) return;
		
		// Set histogram here
		histogram.update(histobins.getValue());
		
		hdata.update(false);
		
		invalidate();
	}
			
	private List<ActionListener> intervalListeners = new LinkedList<ActionListener>();
	private void notifyIntervalListeners() {
		for (ActionListener l : intervalListeners) {
			l.actionPerformed(null);
		}
	}
	
	public void addThresholdedIntervalListener(ActionListener l) {
		intervalListeners.add(l);
	}
	
	/**
	 * @return an array of thresholded regions or null if none exists
	 */
	public double[][] getThresholdedIntervals() {
		if (histogram == null) return null;
		
		return hdata.getThresholdedIntervals();
	}

	private class JHistogramData extends JPanel {
		
		private Timer paintTimer;
		private JPanelPainter painter = new JPanelPainter(this);
		
		private boolean regionsInitialized = false;
		
		private boolean mouseDown = false;
				
		private double mouseTargetValue = 0;
		private int mouseTargetBinCount = 0;
		private Point mp1 = new Point();
		private Point mp2 = new Point();
		private Point delta = new Point();
				
		/**
		 * TODO: eventually store all excluded regions in a single list
		 */
		private int borderPickRadius = 3;
		
		private List<HistogramExcludedRegion> regions = new LinkedList<HistogramExcludedRegion>();

		private JHistogramData(Dimension dimension) {
			
			setPreferredSize(dimension);
			setMinimumSize(dimension);
			
			createUI();
		}
		
		private void update(boolean notifyListeners) {
			
			if (regions.size() < 2) return;
			
			if (delta.x != 0) {
				HistogramExcludedRegion r1 = regions.get(0);
				HistogramExcludedRegion r2 = regions.get(1);
				r1.processAdjustment(delta.x, 0, r2.getLeftPosition());
				r2.processAdjustment(delta.x, r1.getLeftPosition() +  r1.getDimension().width, getSize().width - 2);

			}

			if (notifyListeners) 
				notifyIntervalListeners();

//			paintTimer.restart();
			painter.paint();
		}
		
		/**
		 * @return an array of thresholded regions or null if none exists
		 */
		private double[][] getThresholdedIntervals() {
			if (regions.size() == 0) return null;

			// Count number of available regions
			int count = 0;
			for (int i = 0; i < regions.size(); i++)
				if (!regions.get(i).isCollapsed())
					count++;
			
			// No region was found (all are collapsed)
			if (count == 0)
				return null;
			
			System.out.println(count + " region was found");
			
			// Create region intervals
			double[][] intervals = new double[count][2];
			
			count = 0;
			for (int i = 0; i < regions.size(); i++) {
				if (!regions.get(i).isCollapsed())
					intervals[count++] = getThresholdedInterval(regions.get(i));
			}
			
			return intervals;
		}
		
		private double[] getThresholdedInterval(HistogramExcludedRegion region) {
			if (regions.size() == 0) return null;
			
			double[] interval = new double[2];

			// Compute thresholded interval for this region
			double v1 = getValue(region.getLeftPosition());
			double v2 = getValue(region.getRightPosition());

			interval[0] = v1;
			interval[1] = v2;
			
			return interval;
		}

		
		/**
		 * @param locationX
		 * @return the bin value associated to this location in the swing histogram. 
		 * This location is relative to the histogram boundary and therefore can range from 0 to dimension.width  
		 */
		private double getValue(int locationX) {
			locationX = clamp(locationX);
			
			double range = histogram.getMaxValue() - histogram.getMinValue();
			return histogram.getMinValue() + ((double) locationX) / getSize().width * range;
		}

		/**
		 * @param value
		 * @return the x-location of this value in the histogram.
		 */
		private int getLocation(double value) {
			
			double range = histogram.getMaxValue() - histogram.getMinValue();
			
			return rint(getSize().width * (value - histogram.getMinValue()) / range);
		}

		private int getBinCount(int locationX) {
			locationX = clamp(locationX);
			
			int bindex = rint(((double) locationX) / getSize().width * (histogram.getBinCount()-1));
			return histogram.getBins()[bindex];
		}
		
		private int clamp(int locationX) {
			// Clamp to limits
			if (locationX > getSize().width || Math.abs(locationX - getSize().width) <= SNAP_RADIUS) {
				locationX = getSize().width;
			}
			else if (locationX < 0 || Math.abs(locationX) <= SNAP_RADIUS) {
				locationX = 0;
			}
			
			return locationX;
		}
		
		@Override
		protected void paintComponent(Graphics gp) {
			super.paintComponent(gp);

//			System.out.println(System.nanoTime() + "Painting histogram...");
			
			if (!regionsInitialized) {
				// Set excluded region params
				
				regions.clear();
				HistogramExcludedRegion r1 = new HistogramExcludedRegion(new Dimension(1,1), 0);
				HistogramExcludedRegion r2 = new HistogramExcludedRegion(new Dimension(1,1), getSize().width - 1);
				regions.add(r1);
				regions.add(r2);
				
				regionsInitialized = true;
				
				update(true);
				
				return;
			}

			/*
			 * Paint
			 */
			// Set some bounds
			int spaceY = 30;
			int pad = 10; 
			
//			dimension.setSize(getSize().width, dimension.height);
			int width = getSize().width;
			int height = getSize().height;
			double widthd = width;
			double heightd = height;
			
			Point pos = new Point(0, height - spaceY);
			Point p1 = new Point();
			Point p2 = new Point();

			// Create image and set rendering params
			BufferedImage img = new BufferedImage(getSize().width, getSize().height,
					BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = img.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
					
			Stroke defaultStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			g.setStroke(defaultStroke);
			
			// Draw bounds
			g.setColor(Color.RED);
//			g.drawRect(0, yoffset, dimension.width - 1, dimension.height - 1);

			if (histogram != null) {
				
				// Draw mean, standard deviation
				double hmin = histogram.getMinValue();
				double hmax = histogram.getMaxValue();
				double mean = histogram.getMean();
				double std = histogram.getStd();
				double mode = histogram.getHighestBinValue();
				double pscale = widthd / histogram.getBinCount();
				
//				int meanPos = pos.x + rint(Math.abs(mean) / (hmax - hmin) * width);
//				int stdPos1 = pos.x + rint(Math.abs(mean - std) / (hmax - hmin) * width);
//				int stdPos2 = pos.x + rint(Math.abs(mean + std) / (hmax - hmin) * width);
//				int modePos = rint(Math.abs(mode) / (hmax - hmin) * width);
				int meanPos = getLocation(mean);
				int modePos = getLocation(mode);
				int stdPos1 = getLocation(mean - std);
				int stdPos2 = getLocation(mean + std);
				int midY = rint(0.5 * (height-spaceY));

				Color meanColor = Color.RED;
				Color modeColor = Color.BLUE;

//				super.setBackground(Color.white);
//				super.setOpaque(true);

				/*
				 *  Draw histogram (filling)
				 *  Use the color map if it's available, otherwise use blue
				 */
				// default histogram color
				g.setColor(Color.BLUE);

				// Get data
				int[] bins = histogram.getBins();
				double max = histogram.getHighestBin() * 1.5;
				int binWidth = 1 + rint(pscale);
				
				if (fill.getValue()) {
					for (int i = 0; i < histogram.getBinCount(); i++) {
						int bheight = rint(bins[i] / max * heightd);
						
						p2.x = rint(pos.x + i * pscale);
						p2.y = pos.y - bheight;

						if (colorMap != null) {
							g.setColor(colorMap.getColor(getValue(p2.x)));
						}
						
						g.fillRect(p2.x, p2.y, binWidth, bheight);
					}
				}
				
				/*
				 * Draw envelope.
				 */
				if (envelope.getValue()) {
					g.setColor(Color.BLACK);
					for (int i = 0; i < histogram.getBinCount(); i++) {
						int bheight = rint(bins[i] / max * heightd);
						
						p2.x = rint(pos.x + i * pscale);
						p2.y = pos.y - bheight;

						if (i > 0)
							g.drawLine(p1.x + binWidth / 2, p1.y, p2.x + binWidth / 2, p2.y);
						
						p1.setLocation(p2);
					}
				}
				
				// Draw histogram (contour)
				g.setColor(Color.BLACK);

				// Draw labels
				Font fontTicks = new Font("Consolas", Font.PLAIN, 8);
				g.setFont(fontTicks);
				g.setColor(Color.BLACK);
				
				nf.setMinimumFractionDigits(0);
				nf.setMaximumFractionDigits(3);
				
				// Set number of ticks
				// TODO: set this in UI
//				int skip = Math.max(10,histogram.getNumBins() / 10);
//				int skip = rint(histogram.getNumBins() / 10d);
				int numticks = 10;
				int tickHeight = 5;
				
				FontMetrics metrics = getFontMetrics(fontTicks);
				int hgt = metrics.getHeight();
				double range = widthd / numticks;
				for (int i = 0; i < numticks; i ++) {
					p1.x = rint(pos.x + (i + 0.5) * range);
					
					double v = getValue(p1.x);
					
					// Draw tick
					p1.y = pos.y;
					g.drawLine(p1.x, p1.y, p1.x, p1.y + tickHeight);

					// Draw bin value
					String txt = nf.format(v);
					int adv = metrics.stringWidth(txt);
					g.drawChars(txt.toCharArray(), 0, txt.length(), p1.x - adv/2, pos.y + hgt + tickHeight);
				}
				
				g.setStroke(defaultStroke);

				/*
				 *  Try to draw the zero (if it exists)
				 */
				// Detect zero crossing
				if (Math.signum(hmin) != Math.signum(hmax)) {
					int zero = pos.x + rint(Math.abs(hmin) / (hmax - hmin) * width);
					float dash = 5f;
					g.setColor(Color.WHITE);
					g.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, dash, new float[] { dash }, 0f));
					g.drawLine(zero, 0, zero, height);

					g.setStroke(defaultStroke);
				}
				
				/*
				 *  Draw stats
				 */
				if (drawMore) {
					Font fontStats = new Font("Consolas", Font.PLAIN, 10);
					g.setFont(fontStats);
					metrics = getFontMetrics(fontStats);
					int dystat = metrics.getHeight();
					int ystat = 0;
					String stat;
					Color textColor = Color.BLACK;
					g.setColor(textColor);
									
					g.setColor(meanColor);
					stat = "Mean = " + nf.format(mean);
					g.drawChars(stat.toCharArray(), 0, stat.length(), pos.x + 10, ystat += dystat);
					
					stat = "Std = " + nf.format(std);
					g.drawChars(stat.toCharArray(), 0, stat.length(), pos.x + 10, ystat += dystat);

					g.setColor(modeColor);
					stat = "Mode = " + nf.format(mode);
					g.drawChars(stat.toCharArray(), 0, stat.length(), pos.x + 10, ystat += dystat);

					g.setColor(textColor);
					stat = "Count = " + nf.format(histogram.getData().length);
					g.drawChars(stat.toCharArray(), 0, stat.length(), pos.x + 10, ystat += dystat);

					stat = "Min/max = " + nf.format(histogram.getMinValue()) + ", " + nf.format(histogram.getMaxValue());
					g.drawChars(stat.toCharArray(), 0, stat.length(), pos.x + 10, ystat += dystat);
					
					// Draw mouse pointer
					g.setFont(new Font("Consolas", Font.PLAIN, 12));
					g.setColor(Color.RED);
					String mvalue = "    v = " + nf.format(mouseTargetValue);
					String mbin   = "count = " + nf.format(mouseTargetBinCount);
					g.drawChars(mvalue.toCharArray(), 0, mvalue.length(), mp1.x + 10, mp1.y - 10);
					g.drawChars(mbin.toCharArray(), 0, mbin.length(), mp1.x + 10, mp1.y);					
				}

				// Draw current mouse
				if (drawMore) {
					int osize = 4;
					int tbiny = rint(getBinCount(mp1.x) / max * heightd);

					double bcount = histogram.getBinCount() - 1;
					double binwidth = widthd / bcount;
					int tbinx = rint((mp1.x / widthd) * (bcount) * binwidth);
							
//					g.drawOval(mp1.x - osize/2 - 1, mp1.y - osize/2 - 2, osize, osize);
					g.setColor(Color.RED);
					g.drawOval(tbinx - osize/2 , pos.y - tbiny - osize / 2, osize, osize);
					
					g.setColor(Color.WHITE);
					g.drawLine(tbinx, pos.y - tbiny + osize / 2, tbinx, pos.y - 1);
				}
				
				/*
				 *  Draw grid
				 */
				if (showGrids.getValue()) {
					// Draw bins
					float dash = 5f;
					g.setColor(new Color(0.5f, 0.5f, 0.5f, 0.5f));
					g.setStroke(new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, dash, new float[] { dash }, 0f));
					for (int i = 0; i < numticks; i ++) {
						p1.x = rint(pos.x + (i + 0.5) * range);
						// Draw tick
						p1.y = pos.y;
						g.drawLine(p1.x, 0, p1.x, pos.y - 1);
					}
					
					// Draw intermediate values
					dash = 3f;
					g.setColor(new Color(0.5f, 0.5f, 0.5f, 0.5f));
					g.setStroke(new BasicStroke(0.5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, dash, new float[] { dash }, 0f));

					for (double i = 0.5; i < numticks; i ++) {
						p1.x = rint(pos.x + (i + 0.5) * range);
						// Draw tick
						p1.y = pos.y;
						g.drawLine(p1.x, 0, p1.x, pos.y - 1);
					}
					g.setStroke(defaultStroke);
				}
				
				// Draw excluded region
				for (HistogramExcludedRegion r : regions) {
					if (r.getDimension().width > 5)
					 r.draw(g, height - spaceY + 1, new Color(0,0,0, 0.2f), Color.BLACK);
				}

				// Draw mean, std, mode
				float mdash = 2f;
				
				g.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				g.setColor(meanColor);
				g.drawLine(meanPos, 0, meanPos, height - spaceY - 1);

				g.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, mdash, new float[] { mdash }, 0f));
				g.setColor(modeColor);
				g.drawLine(modePos, 0, modePos, height - spaceY);

				// Draw image back in graphics
				gp.drawImage(img, 0, 0, this);
				
				// Draw standard deviation as arrow from mean				
				BasicStroke arrowStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, mdash, new float[] { mdash }, 0f);
				drawArrow(gp, meanPos, midY, stdPos1, midY, arrowStroke);
				drawArrow(gp, meanPos, midY, stdPos2, midY, arrowStroke);
			}
		}
		
		private final int ARR_SIZE = 4;

        void drawArrow(Graphics gp, int x1, int y1, int x2, int y2, BasicStroke stroke) {
			Graphics2D g = (Graphics2D) gp.create();

			g.setColor(Color.RED);
			g.setStroke(stroke);
			
            double dx = x2 - x1, dy = y2 - y1;
            double angle = Math.atan2(dy, dx);
            int len = (int) Math.sqrt(dx*dx + dy*dy);
            AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
            at.concatenate(AffineTransform.getRotateInstance(angle));
            g.transform(at);

            // Draw horizontal arrow starting in (0, 0)
            g.drawLine(0, 0, len, 0);
            g.fillPolygon(new int[] {len, len-ARR_SIZE, len-ARR_SIZE, len},
                          new int[] {0, -ARR_SIZE, ARR_SIZE, 0}, 4);
            
        }

		private int rint(double v){
			return (int) Math.round(v);
		}

		private HistogramExcludedRegion popupRegion;
		private JPopupMenu popup;
		private void createPopup() {
			
		    //Create the popup menu.
		    popup = new JPopupMenu();
		    JMenuItem menuItem;
		    ActionListener listener;
		    
		    menuItem = new JMenuItem("Collapse");
		    listener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					collapseSelectedRegion();
				}
			};
		    menuItem.addActionListener(listener);
		    popup.add(menuItem);

		    menuItem = new JMenuItem("Delete all but region");
		    listener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					deleteSelectedExcludedRegion(true, true);
				}
			};
		    menuItem.addActionListener(listener);
		    popup.add(menuItem);

		    menuItem = new JMenuItem("Delete region");
		    listener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					deleteSelectedExcludedRegion(false, false);
				}
			};
		    menuItem.addActionListener(listener);
		    popup.add(menuItem);
		    
		    menuItem = new JMenuItem("Delete all from left");
		    listener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					deleteSelectedExcludedRegion(true, false);
				}
			};
		    menuItem.addActionListener(listener);
		    popup.add(menuItem);
		    
		    menuItem = new JMenuItem("Delete all from right");
		    listener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					deleteSelectedExcludedRegion(false, true);
				}
			};
		    menuItem.addActionListener(listener);
		    popup.add(menuItem);
		}
		
		protected void collapseSelectedRegion() {
			// Mark region as collapsed
			popupRegion.collapse();
			
			// Update the view
			updateHistogram();
			update(true);
			
			// Need to "collapse" again since code above might have triggered a refresh
			popupRegion.collapse();
		}
		
		protected void deleteSelectedExcludedRegion(boolean allFromLeft, boolean allFromRight) {
			// Find values corresponding to left and right boundaries
			double[] interval = getThresholdedInterval(popupRegion);

			// Adjust intervals based on booleans
			if (allFromRight && allFromLeft) {
				double[] newInterval = new double[2];
				newInterval[0] = interval[1];
				newInterval[1] = interval[0];
				interval = newInterval;
			}
			else if (allFromRight)
				interval[0] = getHistogram().getMinValue();

			else if (allFromLeft)
				interval[1] = getHistogram().getMaxValue();
			
			// Delete values
			double[] cropped = new double[histogram.getData().length];
			int k = 0;
			for (double v : histogram.getData()) {
				if (allFromLeft && allFromRight && ((v >= interval[0] || v <= interval[1])))
					continue;
				else if (v >= interval[0] && v <= interval[1])
						continue;

				cropped[k++] = v;
			}

			cropped = Arrays.copyOf(cropped, k);

			System.out.println(histogram.getData().length + " -> "
					+ cropped.length);
			histogram.update(cropped);
			updateHistogram();
		}

		private void createUI() {
			regions.clear();
			HistogramExcludedRegion r1 = new HistogramExcludedRegion(new Dimension(1,1), 0);
			HistogramExcludedRegion r2 = new HistogramExcludedRegion(new Dimension(1,1), getPreferredSize().width - 1);
			regions.add(r1);
			regions.add(r2);

			createPopup();
			
			histobins.addParameterListener(new ParameterListener() {
				
				@Override
				public void parameterChanged(Parameter parameter) {
					if (histogram != null) {
						histogram.update(histobins.getValue());
						update(true);
					}
				}
			});
					
			paintTimer = new Timer(10, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					validate();
					repaint();
				}
			});
			paintTimer.setCoalesce(true);
			paintTimer.setRepeats(false);
//			paintTimer.start();

			this.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					mouseDown = false;
					for (HistogramExcludedRegion r : regions) {
						r.release();
					}
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					mp1 = e.getPoint();
					mouseDown = true;
					boolean popped = false;
					for (HistogramExcludedRegion r : regions) {
						if (r.pick(mp1, borderPickRadius)) {
							if (e.getButton() == MouseEvent.BUTTON3) {
								if (!popped) {
									popped = true;
									popupRegion = r;
						            popup.show(e.getComponent(),
						                       e.getX(), e.getY());
								}
							}
						}
					}
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
			
			this.addMouseMotionListener(new MouseMotionListener() {

				@Override
				public void mouseMoved(MouseEvent e) {
					mp1 = e.getPoint();

					if (histogram == null) return;
					
					// Handle value picking
					mouseTargetValue = getValue(mp1.x);
					mouseTargetBinCount = getBinCount(mp1.x);

					// Return if we are not dragging
					if (!mouseDown) {
						for (HistogramExcludedRegion r : regions) {
							r.pick(mp1, borderPickRadius);
						}

						setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

						for (HistogramExcludedRegion r : regions) {
							if (r.isResizing()) {
								setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
								mp2 = mp1;
								break;
							}
							else if (r.isSelected()) {
								setCursor(new Cursor(Cursor.HAND_CURSOR));
								mp2 = mp1;
								break;
							}
						}
						
						for (HistogramExcludedRegion r : regions) {
							r.release();
						}
					}

					update(false);
				}

				@Override
				public void mouseDragged(MouseEvent e) {
					mp1 = e.getPoint();
					
					// Handle value picking
					mouseTargetValue = getValue(mp1.x);
					mouseTargetBinCount = getBinCount(mp1.x);

					delta.x = mp1.x - mp2.x;

					boolean notify = false;
					for (HistogramExcludedRegion r : regions) {
						notify = notify || r.isSelected() || r.isResizing();
						
						if (notify) break;
					}
					update(notify);
					
					mp2 = mp1;
				}
			});
		}
	}
	
	public static void main(String[] args) throws IOException {
		JFrame frame = new JFrame();
		frame.setSize(500, 500);
		
		BufferedImage bi = ImageIO.read(new File("colors.png"));
		frame.add(new JHistogram(bi));
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void setBinCount(int binCount) {
		histobins.setValue(binCount);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JPanel getControls() {
		return this;
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}

}