package swing.component.histogram;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

public class HistogramGrid extends JPanel {

	private final static boolean DefaultLightweight = false;
	
	public static void main(String[] args) {
		int m = 4;
		int n = 3;
		int[] volSizeBounds = { 10, 1000 };
		Dimension size = new Dimension(200, 250);
		
		final HistogramGrid grid = new HistogramGrid(m, n, size);
		Random rand = new Random();
		int v = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				int count = volSizeBounds[0] + rand.nextInt(volSizeBounds[1] - volSizeBounds[0]);
				double[] data = new double[count];
				double mean = rand.nextGaussian();
				for (int k = 0; k < count; k++) {
					data[k] = mean + rand.nextGaussian();
				}
				grid.set(i, j, data, "Volume " + v++);
			}
		}
		
		JFrame frame = new JFrame();
		JPanel panel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 0;
		constraints.weighty = 0;
        constraints.fill = GridBagConstraints.NONE;
		
        // Add bools
		final BooleanParameter lightWeight = new BooleanParameter("Lightweight", false);
		panel.add(lightWeight.getControls(), constraints);
		
		// Add histogram grid
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 1;
		constraints.weighty = 1;
        constraints.fill = GridBagConstraints.BOTH;
		lightWeight.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				grid.setLightWeight(lightWeight.getValue());
			}
		});
		panel.add(grid, constraints);
		
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void setLightWeight(boolean lightWeight) {
		for (int i = 0; i < m ; i++)
			for (int j = 0; j < n; j++)
				if (exists(i, j)) {
					get(i, j).setLightWeight(lightWeight);
				}
		
	}

	private JHistogram[][] histograms;
	
	private Dimension size;
	
	/**
	 * Number of histograms (columns)
	 */
	private int m;
	
	/**
	 * Number of histograms (rows)
	 */
	private int n;
	
	public HistogramGrid(int m, int n, Dimension size) {
		super(new GridLayout(m, n));
		
		this.m = m;
		this.n = n;
		
		histograms = new JHistogram[m][n];
		this.size = size;
	}
	
	public void setBinCount(int binCount) {
		for (int i = 0; i < m ; i++)
			for (int j = 0; j < n; j++)
				if (exists(i, j)) {
					get(i, j).setBinCount(binCount);
				}
	}

	/**
	 * Assemble the current UI grid.
	 */
	private void assemble() {
		super.removeAll();
		
		for (int i = 0; i < m ; i++)
			for (int j = 0; j < n; j++) {
				if (exists(i, j))
					add(get(i, j));
				else
					add(new JLabel());
			}
	}

	/**
	 * Attempt to fill the histogram grid with this data. The operation stops if all grid coordinates are filled.
	 * @param data an array of vectorized volumes.
	 */
	public void set(double[][] data, String[] labels) {
		
		int k = 0;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++) {
				if (k > data.length)
					return;
				
				set(i, j, data[k], labels[k]);
				k++;
			}
	}
	
	/**
	 * Set the data at the selected grid coordinate.
	 * @param i
	 * @param j
	 * @param data
	 */
	public void set(int i, int j, double[] data, String label) {
		checkCoordinate(i, j);

		if (!exists(i, j)) {
			create(i, j, data, label);
		}
		else {
			update(i, j, data, label);
		}
		
		assemble();
	}
	
	/**
	 * Update the data at the selected grid coordinate.
	 * @param i
	 * @param j
	 * @param data
	 */
	private void update(int i, int j, double[] data, String label) {
		checkCoordinate(i, j);
		
		assert exists(i, j);
		
		get(i, j).update(data);
		get(i, j).setName(label);
		
	}
	
	/**
	 * Create a new histogram at this grid coordinate.
	 * @param i
	 * @param j
	 * @param data
	 */
	private void create(int i, int j, double[] data, String label) {
		Histogram histogram = new Histogram(data);
		histograms[i][j] = new JHistogram(histogram, size, DefaultLightweight);
		histograms[i][j].setName(label);
	}
	
	
	/**
	 * @param i
	 * @param j
	 * @return whether a histogram exists at this grid coordinate.
	 */
	private boolean exists(int i, int j) {
		checkCoordinate(i, j);
		
		return get(i, j) != null;
	}
	
	/**
	 * Throws an exception is this grid coordinate is not valid.
	 * @param i
	 * @param j
	 */
	private void checkCoordinate(int i, int j) {
		if (i < 0 || j < 0 || i > m || j > n)
			throw new IllegalArgumentException("Hisotgram coordinate is out of bounds.");
	}
	
	/**
	 * @param i
	 * @param j
	 * @return the histogram at this grid coordinate.
	 */
	public JHistogram get(int i, int j) {
		checkCoordinate(i, j);
		
		assert histograms != null;
		
		return histograms[i][j];
	}
}
