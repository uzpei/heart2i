package swing.component.histogram;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.vecmath.Vector3d;

import tools.geom.MathToolset;

public class Histogram {

	private int[] bins;
	private double[] binValues;
	private int highestBin;
	private double highestBinValue;
	private double vmin, vmax;
	private double[] data;

	public Histogram(double[] values, int numbins) {
		construct(values, numbins);
	}

	public Histogram(double[] values) {
		this(values, estimateBinCount(values));
	}

	public void update(double[] data) {
		construct(data, estimateBinCount(data));
	}

	public void update(int numbins) {
		construct(data, numbins);
	}


	public double[] getData() {
		return data;
	}
	
	public double getMean() {
		double mean = 0;
		for (double v : data) {
			mean += v;
		}
		return mean / data.length;
	}
	
	public double getStd() {
		double mean = getMean();
		double std = 0;
		for (double v : data) {
			std += (v - mean) * (v - mean);
		}
		
		return Math.sqrt(std / data.length);
	}
	
//	/**
//	 * @return the value corresponding to the histogram bins (i.e. the largest bin)
//	 */
//	public double getMode(double precision) {
////		double range = vmax - vmin;
////		int numbins = rint(range / precision);
////		int[] binc = new int[numbins + 1];
////		
////		int k;
////		int mmax = 0;
////		for (double v : data) {
////			k = rint((v-vmin) / range * numbins);
////			binc[k]++;
////			
////			if (binc[k] > mmax)
////				mmax = k;
////		}
////		return vmin + (range * mmax) / numbins;
//		
//		return getHighestBin();
//	}
	
	private int rint(double v) {
		return (int) Math.round(v);
	}
	
	public int[] getBins() {
		return bins;
	}
	
	public double[] getBinValues() {
		return binValues;
	}
	
	public int getBinCount() {
		return bins.length;
	}
	
	/**
	 * @return the largest bin count
	 */
	public int getHighestBin() {
		return highestBin;
	}

	/**
	 * @return the value corresponding to the largest bin count
	 */
	public double getHighestBinValue() {
		return highestBinValue;
	}

	public Histogram(ByteBuffer bufferRGBA, int numbins) {
		double[] values = new double[bufferRGBA.capacity()];
		
		bufferRGBA.rewind();
		int k = 0;
		while (bufferRGBA.hasRemaining()) {
//			int r = (bufferRGBA.get() & 0xFF);
//			int g = (bufferRGBA.get() & 0xFF);
//			int b = (bufferRGBA.get() & 0xFF);
//			int a = (bufferRGBA.get() & 0xFF);
//			values[k++] = Math.sqrt(r * r + g * g + b * b + a + a);
			values[k++] = bufferRGBA.get() & 0xFF;
		}
		
		construct(values, numbins);
	}
	
	/**
	 * For now use simple sqrt(n) rule.
	 * TODO: move to more advanced heuristics
	 * @return
	 */
	public static int estimateBinCount(double[] data) {
		return estimateBinCount(data.length);
	}

	/**
	 * For now use simple sqrt(n) rule.
	 * TODO: move to more advanced heuristics
	 * @param n
	 * @return
	 */
	public static int estimateBinCount(int n) {
		return MathToolset.roundInt(Math.sqrt(n));
	}

	public void construct(int[] values, int numbins) {
		double[] dvalues = new double[values.length];
		for (int i = 0; i < values.length; i++) {
			dvalues[i] = values[i];
		}
		construct(dvalues, numbins);
	}
	
	private void construct(double[] values, int numbins) {
		data = values;
		
		// First pass to find range
		vmin = Double.MAX_VALUE;
		vmax = Double.MIN_VALUE;
		
		for (double v : values) {
			if (v > vmax)
				vmax = v;
			
			if (v < vmin)
				vmin = v;
		}

		// Set bin values
		double range = vmax - vmin;
		binValues = new double[numbins + 1];
		for (int i = 0; i <= numbins; i++) {
			binValues[i] = vmin + i * range / numbins;
		}
		
		// Then build histogram
		bins = new int[numbins + 1];
		int k = 0;
		highestBin = 0;
		for (double v : values) {
			k = rint((v-vmin) / range * numbins);
			bins[k]++;
			
			if (bins[k] > highestBin) {
				highestBin = bins[k];
				highestBinValue = v;
			}
		}
	}
	
	public Histogram(BufferedImage bi) {
		Vector3d v = new Vector3d();
		double[] values = new double[bi.getWidth() * bi.getHeight()];
		int k = 0;
		for (int x = 0; x < bi.getWidth(); x++) {
			for (int y = 0; y < bi.getHeight(); y++) {
				int pixel = bi.getRGB(x, y);
	            int r = (pixel >> 16) & 255; // red
	            int g = (pixel >> 8) & 255; // green
	            int b = pixel & 255; // blue
				int a = (pixel >> 24) & 255; // alpha
	            
				v.set(r, g, b);
				values[k++] = 255 * v.length() / Math.sqrt(255d * 255d * 255d);
			}
		}
		
		construct(values, estimateBinCount(values));
	}

	public double getMinValue() {
		return vmin;
	}

	public double getMaxValue() {
		return vmax;
	}


	public void addValue(double value) {
		double[] data = getData();
		double[] dataNew = Arrays.copyOf(data, data.length + 1);
		dataNew[data.length] = value;
		
		construct(dataNew, getBinCount());
	}
}
