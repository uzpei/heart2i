/*
 * Created on 4-Jan-2005
 */
package swing.component;

/*
 ** Luxor - XML User Interface Language (XUL) Toolkit
 ** Copyright (c) 2001, 2002 by Gerald Bauer
 **
 ** This program is free software.
 **
 ** You may redistribute it and/or modify it under the terms of the GNU
 ** General Public License as published by the Free Software Foundation.
 ** Version 2 of the license should be included with this distribution in
 ** the file LICENSE, as well as License.html. If the license is not
 ** included with this distribution, you may find a copy at the FSF web
 ** site at 'www.gnu.org' or 'www.fsf.org', or you may write to the
 ** Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139 USA.
 **
 ** THIS SOFTWARE IS PROVIDED AS-IS WITHOUT WARRANTY OF ANY KIND,
 ** NOT EVEN THE IMPLIED WARRANTY OF MERCHANTABILITY. THE AUTHOR
 ** OF THIS SOFTWARE, ASSUMES _NO_ RESPONSIBILITY FOR ANY
 ** CONSEQUENCE RESULTING FROM THE USE, MODIFICATION, OR
 ** REDISTRIBUTION OF THIS SOFTWARE.
 **
 */

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class CollapsiblePanel extends JPanel implements Collapsible {

	private static final long serialVersionUID = 7529478802493432972L;
	private CollapseButton btnCollapse;
	private CollapseButton btnExpand;
	private GridBagConstraints constraints;
	private boolean isCollapsed = false;
	private Border collapsibleBorder;
	
	private JPanel collapsibleContent;
	private JPanel borderPanel;
	private TitledBorder tborder;
	
	public CollapsiblePanel() {
		this(new VerticalFlowPanel().getPanel());
	}
	
	/**
	 * @param content
	 */
	public CollapsiblePanel(JPanel content) {
		this(content, null);
	}
	
	/**
	 * Create a new collapsible panel with the specified name.
	 * 
	 * @param content
	 *            the panel to be collapsiblized
	 * @param name
	 *            the name to be assigned to the panel
	 */
	public CollapsiblePanel(JPanel content, String name) {
		super();
		
		// No name was given
		// Either try to find one or use default
		if (name == null) {
			// Check if a TitleBorder already exists
			// if it's the case we can use its title
			Border border = content.getBorder();
			if (border instanceof TitledBorder) {
				name = ((TitledBorder) border).getTitle();
			}
			
			// If name is still null
			// try to get the content's name (if it was already set)
			if (name == null) {
				name = content.getName();
			}
			
			// If it's still null, then we don't have a choice but to use a default (empty) string
			if (name == null) {
				name = "";
			}
		}

		setName(name);
		
		// Try to remove the content's title
		Border border = content.getBorder();
		if (border instanceof TitledBorder) {
			content.setBorder(BorderFactory.createEmptyBorder());
		}
		
        tborder = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), getName(), TitledBorder.LEFT, TitledBorder.CENTER);
		setBorder(tborder);
		
		collapsibleContent = content;
		
		setLayout(new GridBagLayout());
		constraints = new GridBagConstraints();
		btnCollapse = new CollapseButton(this,
				SwingConstants.HORIZONTAL);
		btnExpand = new CollapseButton(this,
				SwingConstants.VERTICAL);
		borderPanel = new JPanel();
		borderPanel.setLayout(new GridLayout(1, 1));
		collapsibleBorder = BorderFactory.createTitledBorder("");
//		borderPanel.setBorder(collapsibleBorder);

		expand();
	}

	public CollapsiblePanel(VerticalFlowPanel vfp, String title) {
		this(vfp.getPanel(), title);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

	}
	
	/**
	 * Tells you whether this component is currently collapsed. Useful for
	 * checking the component's status.
	 * 
	 * @return true if this component is collapsed, false if it is not.
	 */
	@Override
	public boolean isCollapsed() {
		return isCollapsed;
	}

	/**
	 * Tells you whether this component is collapsible.
	 * 
	 * @return a boolean indicating this component is collapsible.
	 */
	@Override
	public boolean isCollapsible() {
		return collapsible;
	}

	/**
	 * Collapses the panel.
	 */
	@Override
	public CollapsiblePanel collapse() {
		setVisible(false);
		removeAll();
		borderPanel.removeAll();

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 0;
		constraints.weighty = 0;
		
		constraints.insets = getButtonTitleInsets();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.fill = GridBagConstraints.NONE;
		add(btnCollapse, constraints);
		Dimension dim = btnCollapse.getSize();
		btnCollapse.setBounds(0, 0, dim.width, dim.height);

		// Add an empty panel to fill in space
		constraints.insets = new Insets(0, 0, 0, 0);
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.gridheight = GridBagConstraints.REMAINDER;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.BOTH;
		add(borderPanel, constraints);

		isCollapsed = true;
		revalidate();
		setVisible(true);
		
		return this;
	}

	/**
	 * Uncollapses the panel.
	 */
	@Override
	public void expand() {
		setVisible(false);
		removeAll();
		borderPanel.removeAll();

		// Add the the collapse button
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 0;
		constraints.weighty = 0;

		// Place the button next to the titled border
		constraints.insets = getButtonTitleInsets();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.fill = GridBagConstraints.NONE;
		add(btnExpand, constraints);
		Dimension dim = btnExpand.getSize();
		btnExpand.setBounds(0, 0, dim.width, dim.height);

		// Add the content
		constraints.insets = new Insets(0, 0, 0, 0);
//		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridx = 0;
		constraints.gridy = 0;
//		constraints.gridheight = GridBagConstraints.REMAINDER;
//		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.BOTH;
//		add(collapsibleContent, constraints);
		borderPanel.add(collapsibleContent);
		add(borderPanel, constraints);

		isCollapsed = false;
		revalidate();
		setVisible(true);
	}
	
	/**
	 * Leave some room to place a button in the title
	 * @return
	 */
	private Insets getButtonTitleInsets() {
//		FontMetrics metrics = getFontMetrics(tborder.getTitleFont());
		Font font = UIManager.getDefaults().getFont("TitledBorder.font");
		FontMetrics metrics = getFontMetrics(font);
		
		int hgt = metrics.getHeight();
		int adv = metrics.stringWidth(tborder.getTitle().substring(0, tborder.getTitle().length() - trailingSpace.length()));
		Dimension size = new Dimension(adv+2, hgt+2);
		return new Insets(-super.getInsets().top + 1, size.width + 4, 0, 0);
	}

	private static final String trailingSpace = "     ";
	
	@Override
	public void setName(String name) {
//		// Add some trailing space
		name = name + trailingSpace;
		
		super.setName(name);
	}

}
