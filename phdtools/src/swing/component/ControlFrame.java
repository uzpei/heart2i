/*
 * Created on 1-Oct-2003
 */
package swing.component;

import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;


/**
 * A helper class for making nice control panels. <p>
 * Use with {@link VerticalFlowPanel}, or panels with custom layout. <p>
 * <p>
 * The control panel consists of tabs, the contents of which should
 * flow vertically.  Each content panel will be given a vertical scrollbar, and
 * horizontal scrollbars will appear as necessary.<p>
 * <p>
 * Use add( name, yourPanel) to add new controls in a named tab. <p>
 * Use getContentPanel.add( yourPanel , BorderLayout.NORTH ) <p> 
 * <p>
 * The frame is not visible by default, and setVisible(true) must be 
 * called. <p>
 * <p>
 * Note that although this is based off the old JoglGraphics ControlFrame, 
 * it doesn't have the same exception throwing bad behaviour. 
 * 
 * @author kry, epiuze
 */
public class ControlFrame {

    private JFrame frame;
    private JPanel content;
    private JTabbedPane tabs;
    private Dimension dimension;
    private boolean standAlone = false;
    
//    /**
//     * Make a new control frame with default size and placement
//     * 400x600 at 600,100 
//     * @param title 
//     */
//    public ControlFrame( String title ) {
//        this( title, new Dimension( 400,600 ), new Point(600,100) );
//    }
    
    /**
     * Make a new control frame with given size and placement
     * @param title 
     * @param d size
     * @param p location
     */
    public ControlFrame( String title, Dimension d, Point p, boolean standAlone) {    
    	dimension = new Dimension(d);
    	this.standAlone = standAlone;
    	
    	if (standAlone) {
            frame = new JFrame();
            frame.setTitle( title );
            frame.setSize(dimension);
            frame.setPreferredSize(dimension);
            
            if (p != null)
            	frame.setLocation(p);
            
    	}
        
        tabs = new JTabbedPane();
        tabs.setTabPlacement(SwingConstants.BOTTOM);
//        tabs.setTabPlacement(SwingConstants.RIGHT);
//        tabs.setPreferredSize(dimension);
//        tabs.setFocusCycleRoot(true);
        
        content = new JPanel();
        content.setLayout(new BorderLayout());
        content.setPreferredSize(dimension);
        content.add(tabs, BorderLayout.CENTER);
        
        if (standAlone) {
            frame.getContentPane().add(content);        
        }
    }
    
    /**
     * Adds a JPanel with the given name to this control frame.
     * @param panelName
     * @param panel
     */
    public void add( String panelName, JPanel panel ) {
        
        JScrollPane scroller = new JScrollPane(panel,VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
//        JScrollPane scroller = new JScrollPane(panel,VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroller.getVerticalScrollBar().setUnitIncrement(10);
        scroller.getHorizontalScrollBar().setUnitIncrement(10);
        
//        scroller.setPreferredSize(tabs.getSize());
//        scroller.setMaximumSize(dimension);
//        scroller.setSize(dimension);
        
        tabs.add( scroller, panelName );
    }
 
    /**
     * Set the screen location of this control frame
     * @param p
     */
    public void setLocation( Point p ) {
    	setLocation(p.x, p.y);
    }
    
    /**
     * Set the screen location of this control frame
     * @param x
     * @param y
     */
    public void setLocation( int x, int y ) {
    	if (standAlone) {
            frame.setLocation( x, y );
    	}
    	else {
            content.setLocation( x, y );
    	}
    }

    /**
     * Set the visibility of this control frame
     * @param visible
     */
    public void setVisible( boolean visible ) {
    	if (standAlone) {
            frame.setVisible(visible);
    	}
    	else {
            content.setVisible(visible);
    	}
    }
       
    /**
     * Get the content panel. <p>
     * Note that this panel uses a BorderLayout.  Use BorderLayout.NORTH etc.
     * to add universal controls that should be visible at all times.
     * @return content panel with BorderLayout
     */
    public JPanel getContentPanel() {
        return content;
    }
    
    /**
     * Gets the frame.
     * Useful for setting up a shutdown event on closing the window, e.g. <p><p>
     * 
     * getJFrame().addWindowListener( new WindowAdapter() {
     *       public void windowClosing( WindowEvent evt ) {
     *           System.exit(1);
     *       }});
     * 
     * <p><p>
     * @return the JFrame 
     */
    public JFrame getJFrame() {
        return frame;
    }
    
    /**
     * Set the named panel as the visible panel
     * @param panelName
     */
    public void setSelectedTab( String panelName ) {
        //frame.setVisible(true);
        int panelIndex = tabs.indexOfTab( panelName );
        if ( panelIndex != -1 ) {
            tabs.setSelectedIndex( panelIndex );
        }
    }

    public void setSelectedTab(int panelIndex) {
    	if (panelIndex > 0 && panelIndex < tabs.getTabCount())
    		tabs.setSelectedIndex( panelIndex );
    }

	public void dispose() {
		if (standAlone) {
			frame.dispose();
		}
	}

	public boolean isVisible() {
    	if (standAlone) {
            return frame.isVisible();
    	}
    	else {
    		return content.isVisible();
    	}
	}
}
