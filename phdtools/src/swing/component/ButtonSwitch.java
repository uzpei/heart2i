package swing.component;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

public class ButtonSwitch extends JToggleButton implements ActionListener,
		Runnable, MouseMotionListener, MouseListener, HierarchyListener {
	private static final long serialVersionUID = -9060846998092269632L;
	
	public enum Resolution { LOW, MEDIUM, HIGH };
	public enum SwitchType { ONOFF, IO, TRUEFALSE, YESNO };

	private final static String fileroot = "resources";
//	private final static String btnSwitch = "resources/btnOnOff";
//	private final static String btnMask = "resources/btnOnOffMask";
	
	private int buttonX;
	private int deltaX = -1;
	// boolean threadStop;
	private boolean selected;
	private boolean selectionRequest;
	
	private boolean drag;
	private Image switchImage, maskImage;
	private Dimension size;

	public final static SwitchType DEFAULT_SWITCH = SwitchType.IO;
	
	private boolean initialized;
	
	@Override
	public boolean isSelected() {
		if (!isInitialized()) {
			return selectionRequest;
		}
		
		return selected;
	}

	/**
	 * @return whether the button has been initialized (resized and value is set)
	 */
	private boolean isInitialized() {
		return initialized;
	}
	
	/**
	 * Set the control as initialized meaning that its size has been packed and its value (selected or not) is set.
	 */
	private void setInitialized() {
		initialized = true;
	}
	
	
	@Override
	public void setSelected(boolean selected) {
		if (running) return;

		if (!isInitialized()) {
			selectionRequest = selected;
			notifyListeners();
			return;
		}
		
		if (this.selected == selected) return;

		this.selected = selected;
		// threadStop = true;
		
		//System.out.println("-----> SELECTED!");
		startThread();
	}
	
	public ButtonSwitch() {
		this(DEFAULT_SWITCH, Resolution.LOW, true);
	}

	public ButtonSwitch(boolean on) {
		this(DEFAULT_SWITCH, Resolution.LOW, on);
	}

	public ButtonSwitch(SwitchType type, boolean on) {
		this(type, Resolution.LOW, on);
	}
	
	private SwitchType type;

	public ButtonSwitch(SwitchType type, Resolution resolution, boolean on) {
		super();
		
		this.type = type;
		
		// The switch is always false before initialization.
		selected = false;
		buttonX = 0;
		initialized = false;
		selectionRequest = on;
		
		createImages(type, resolution);

		this.addActionListener(this);
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.addHierarchyListener(this);
	
		super.addComponentListener(new ComponentListener() {
			@Override
			public void componentShown(ComponentEvent e) {
				//System.out.println("-----> Component shown...");
				// TODO Auto-generated method stub
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentResized(ComponentEvent e) {
				// This will be called once the component is first resized
				// We can't select/deselect this switch until this event occurs; otherwise, the switch won't behave properly.
				if (!isInitialized()) {
					setInitialized();
					
					//System.out.println("-----> Component resized!");
					setSelected(selectionRequest);
				}
				
			}
		});

	}

	private void createImages(SwitchType type, Resolution res) {
		
		double scale = 1;
		switch (res) {
		case LOW:
			scale = 0.35;
			break;
		case MEDIUM:
			scale = 0.5;
			break;
		case HIGH:
			scale = 1.0;
			break;
		}
		
		double width = 0;
		double height = scale * 60;
		switch (type) {
		case ONOFF:
			width = 212;
			break;
		case IO:
			width = 133;
			break;
		case TRUEFALSE:
			width = 0;
			break;
		case YESNO:
			width = 0;
			break;
		}
		
//		//System.out.println("Type: " + type.toString() + ", " + res +" (" + scale + ")");
		int swidth = (int) (scale * width);
		int sheight = (int) (height);
		size = new Dimension(swidth, sheight);
		
        setPreferredSize(new Dimension(swidth, sheight));
        setMinimumSize(new Dimension(swidth, sheight));
        
        offsetx = (int) (9 * scale);

		switchImage = createButtonImage(type, res);
		maskImage = createMaskImage(type, res);
	}

	private Image createButtonImage(SwitchType type, Resolution res) {
        String filename = fileroot + "/btn" + type.toString() + "@" + (res.ordinal() + 1) + ".png";
		return new ImageIcon(ButtonSwitch.class.getResource(filename)).getImage();
	}

	private Image createMaskImage(SwitchType type, Resolution res) {
        String filename = fileroot + "/btn" + type.toString() + "Mask@" + (res.ordinal() + 1) + ".png";
		return new ImageIcon(ButtonSwitch.class.getResource(filename)).getImage();
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(switchImage, 0, 0, getWidth(), getHeight(), null, null);
		g.drawImage(maskImage, buttonX, 0, getMaskWidth(), this.getHeight(),
				null, null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
//		if (!drag) {
//			selected = !selected;
//			// threadStop = true;
//			startThread();
//		}
	}
	private int offsetx;
	private final static int ANIMATION_SPEED = 2;
	
	@Override
	public int getWidth() {
		return size.width;
//		return super.getWidth() > 0 ? super.getWidth() : size.width;
	}

	@Override
	public int getHeight() {
		return super.getHeight() > 0 ? super.getHeight() : size.height;
	}

	private int getMaskWidth() {
		return this.getWidth() / 2 + offsetx;
	}

	private boolean running = false;
	private final static boolean ANIMATE = true;
	
	private void startThread() {
		if (!enabled) return;

		/*
		 *  TODO: might eventually want to use this animation...
		 *  For now simply move to the desired position
		 */
		
		if (ANIMATE) {
			if (!running && enabled) new Thread(this).start();
		}
		else {

			if (this.isSelected()) {
				buttonX = getMaxX();

			} else {
				buttonX = 0;
			}
			
			notifyListeners();
			
			validate();
			repaint();

			mousePressed = false;
		}
	}

	@Override
	public void run() {
			
		if (!enabled) return;
		
		running = true;
		
//		//System.out.println("running: " + isSelected());
		// while (true) {
		// if (threadStop) {
		if (this.isSelected()) {
			for (; buttonX <= getMaxX(); buttonX += ANIMATION_SPEED) {
				this.repaint();
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			buttonX = getMaxX();

		} else {
			for (; buttonX > 0; buttonX -= ANIMATION_SPEED) {
				this.repaint();
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			buttonX = 0;
		}
		
		running = false;

		//System.out.println("-----> Firing event...");
		
		notifyListeners();
		
		repaint();

		mousePressed = false;
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (!enabled) return;
		
		drag = true;
		// threadStop = false;
		if (deltaX == -1) {
			deltaX = evt.getX() - buttonX;
		}

		buttonX = evt.getX() - deltaX;

		if (buttonX < 0) {
			buttonX = 0;
		}
		if (buttonX > getMaxX() + 1) {
			buttonX = getMaxX() + 1;
		}

		this.repaint();
	}
	
	private int getMaxX() {
		return getMaskWidth() - 2 * offsetx - 1;
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	
	private boolean mousePressed = false;

	public boolean isMousePressed() {
		return mousePressed;
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {
		if (!enabled) return;

		mousePressed = true;
		
		Point p = arg0.getPoint();
		if (!drag && p.x <= getWidth() + 1) {
			selected = !selected;
			// threadStop = true;
			//System.out.println("-----> MOUSE PRESSED!");
			startThread();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if (!enabled) return;

		deltaX = -1;
		if (drag) {
			
			//System.out.println("-----> MOUSE RELEASED!");
			if (buttonX < getMaskWidth() / 2) {
				this.setSelected(false);
			} else {
				this.setSelected(true);
			}
		}
		drag = false;
	}

	@Override
	public void hierarchyChanged(HierarchyEvent arg0) {
//		startThread();
	}
	
	private List<ItemListener> listeners = new LinkedList<ItemListener>();
	
	@Override 
	public void addItemListener(ItemListener listener) {
		listeners.add(listener);
	}
	
	private void notifyListeners() {
		for (ItemListener listener : listeners) {
			listener.itemStateChanged(new ItemEvent(this, ItemEvent.RESERVED_ID_MAX + 2, this, isSelected() ? 1 : 0));
		}
	}
	
	private boolean enabled = true;
	
	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(false);
		enabled = false;
	}
	
	public SwitchType getSwitchType() {
		return type;
	}
}
