package swing.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

public class ColorChooser extends Parameter {

	public final static Color DEFAULT_COLOR = Color.black;
	
	private Color color = DEFAULT_COLOR;
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color newColor) {
		color = newColor;
        lblColor.setBackground(newColor);
	}
	
	public ColorChooser(String name) {
		super(name);
	}

	private final JLabel lblColor = new JLabel("");

	public JPanel getControls() {
		return getControls(this.color);
	}
	
	public JPanel getControls(final Color defaultColor) {

//		final JPanel panel = new JPanel(new GridLayout(1,2));
		final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
//		Dimension d = panel.getSize();
//		d.height -= 5;
//		panel.setSize(d);
        
        JLabel lblName = new JLabel(getName());
        lblName.setHorizontalAlignment(SwingConstants.LEFT);
        lblName.setAlignmentX(Component.LEFT_ALIGNMENT);
//        lblName.setBorder(BorderFactory.createLoweredBevelBorder());

        panel.add(lblName);
        
        Dimension dim = new Dimension(14, 14);
        lblColor.setMinimumSize(dim);
        lblColor.setMaximumSize(dim);
        lblColor.setPreferredSize(dim);
        lblColor.setOpaque(true);
        lblColor.setBackground(defaultColor);
        color = defaultColor;
//        lblColor.setAlignmentX(Component.LEFT_ALIGNMENT);
//        lblColor.setBorder(BorderFactory.createCompoundBorder(
//        		BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
        
        lblColor.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				Color newColor = JColorChooser.showDialog(
		                panel,
		                "Choose " + name + " Color",
		                defaultColor);
				
				if (newColor != null) {
			        lblColor.setBackground(newColor);
			        notifyListeners();
			        color = newColor;
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        panel.add(lblColor);

        return panel;
	}
}
