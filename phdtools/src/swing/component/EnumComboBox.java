package swing.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import swing.parameters.IntParameter;
import swing.parameters.Parameter;


/**
 * @author piuze
 * @param <E>
 */
public final class EnumComboBox<E extends Enum<E>> extends Parameter {

    /**
     * @param defaultEnum The initial value of the combox box, as a <code>E</code> enum type.
     */
    public EnumComboBox(E defaultEnum) {
    	this(defaultEnum.getDeclaringClass().getSimpleName(), defaultEnum);
    }
    /**
     * @param name a title for this combox box
     * @param defaultEnum The initial value of the combox box, as a <code>E</code> enum type.
     */
    public EnumComboBox(String name, E defaultEnum) {
        super(name);
        values = defaultEnum.getDeclaringClass().getEnumConstants();
        selected = defaultEnum;
        createControls();
    }

    private JLabel lblName;

    private E[] values;
    
    private E selected;
    
    private boolean needsRevalidation = true;
    
    public E getSelected() {
        return selected;
    }
    
    @Override
    public void setName(String name) {
    	super.setName(name);
    	lblName.setText(name);
    	
    	needsRevalidation = true;
    }
    
    public void setSelected(Enum e) {
        int i = 0;
        for (Enum m : values) {
            if (m.equals(e)) {
                setSelected(i);
                return;
            }
            i++;
        }
    }
    
    public void setSelected(int n) {
        selected = values[n];
        interList.setSelectedIndex(n);
    }

    
    private JComboBox interList;
    
    private JPanel panel;
    
    private void createControls() {
		final EnumComboBox<E> me = this;
		panel = new JPanel() {
			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				me.setEnabled(enabled);
			}
		};
		        
        int n = values.length;
        final String[] pmethods = new String[n];
        for (int i = 0; i < n; i++) {
            pmethods[i] = values[i].toString();
        }

        interList = new JComboBox(pmethods);
        interList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                JComboBox cb = (JComboBox) ae.getSource();
                String mode = (String) cb.getSelectedItem();

                // Assign the interpolation mode selected
                for (Enum m : values) {
                    if (m.toString().equalsIgnoreCase(mode)) {
                        selected = (E) m;
                        notifyListeners();
                    }
                }
            }
        });

        panel.setLayout(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 0;
        gbc.ipady = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
        
        lblName = new JLabel(getName());
        lblName.setHorizontalAlignment(SwingConstants.LEFT);

        gbc.weightx = 0;
        gbc.insets = new Insets(4, 4, 2, 4);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(lblName, gbc);
        
        gbc.weightx = 1;
        if (getName().length() > 0) gbc.gridx++;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(interList, gbc);
        
        // make background transparent
        panel.setOpaque(false);
        interList.setOpaque(false);
        lblName.setOpaque(false);
        
        setSelected(selected);

    	needsRevalidation = false;
    }
    
    /**
     * @return A combox box for selecting between enums.
     */
    public JPanel getControls() {
        if (needsRevalidation) {
        	createControls();
        }
        return panel;
    }
	public void setEnabled(boolean b) {
		interList.setEnabled(b);
		lblName.setEnabled(b);
	}    
}
