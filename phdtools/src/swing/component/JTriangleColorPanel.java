package swing.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;

import swing.parameters.ParameterListener;

/**
 * @author piuze
 */
public class JTriangleColorPanel extends JPanel implements ComponentListener {

	private static final long serialVersionUID = 32090659352778669L;
	
	private static final Point COMPONENT_OFFSET = new Point(5, 5);
	
	private String fontName = "Lucida Console";
	
	private BufferedImage src = null;
//	private Dimension d;
	private Point mp = null;
	private Point2d pt = new Point2d();
//	private Point3d current = new Point3d();
	private Point tx, ty, tz, tc;
	private Point2d ptd = new Point2d();
	private Vector2d v1 = new Vector2d();
	private Vector2d v2 = new Vector2d();
	private Vector2d v3 = new Vector2d();
	private Vector2d ab = new Vector2d();
	private Vector2d ac = new Vector2d();
	private Vector2d cb = new Vector2d();
	private Vector2d ap = new Vector2d();
	private Vector2d bp = new Vector2d();
	private Vector2d cp = new Vector2d();
	private Point2d a = new Point2d();
	private Point2d b = new Point2d();
	private Point2d c = new Point2d();
	private Point2d d = new Point2d();
	private Point pti = new Point(); 

	private Color[] colors = new Color[] { Color.RED, Color.GREEN, Color.BLUE, Color.BLACK };

	public JTriangleColorPanel(int dim) {
		this(new Dimension(dim, dim));
	}

	private List<ParameterListener> listeners = new LinkedList<ParameterListener>();
	
	public void addParameterListener(ParameterListener l) {
		listeners.add(l);
	}
	
	public JTriangleColorPanel(Dimension dimension) {
				
		// Set preferred size
		super.setSize(dimension);
		super.setPreferredSize(dimension);
		super.setMinimumSize(dimension);

		timer = new Timer(10, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createTriangle();
//				setPoint(current.x, current.y, current.z);
				// Set the current point to lie at the center
				mp = new Point(tc.x, tc.y);
				
				validate();
				repaint();
			}
		});
		timer.setCoalesce(true);
		timer.setRepeats(false);

		this.addComponentListener(this);

		this.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				mp = e.getPoint();

				validate(mp);
				
				validate();
				repaint();
				
				for (ParameterListener l : listeners) {
					l.parameterChanged(null);
				}

			}
		});
	}

	public Point3d getPoint() {
		if (src != null && mp != null) {
			if (mp.x > 0 && mp.x < super.getSize().width && mp.y > 0 && mp.y < super.getSize().height) {
//				mp.x = rint(pt.x);
//				mp.y = rint(pt.y);

				// If we are sitting on a vertex, make sure we use the correct value
				int cindex = snap(mp);
				Color color;
				if (cindex >= 0) {
					color = colors[cindex];
				}
				else {
					color = new Color(src.getRGB(mp.x, mp.y));
				}
				
				Point3d p = new Point3d(color.getRed(), color.getGreen(),
						color.getBlue());

				p.scale(1d / 255d);
				
				return p;
			}
		}

		return null;
	}

	private int rint(double d) {
		return (int) Math.round(d);
	}
	
	/**
	 * Validate (in-triangle) the point in place.
	 * @param p
	 * @return The validated point.
	 */
	private Point validate(Point p) {
//		System.out.println("Current: " + p);
		Point2d ptd = getClosestPoint(p, tx, ty, tz);
		p.x = rint(ptd.x);
		p.y = rint(ptd.y);

		snap(p);
		
//		System.out.println("Final: " + mp);

//		current = getPoint();
		return p;
	}
	/**
	 * Assume range [0, 1]
	 * @param r
	 * @param g
	 * @param b
	 */
	public void setPoint(double x, double y, double z) {
		if (src == null)
			createTriangle();
		
		mp = paramToPoint(x, y, z);
		validate(mp);
		
		validate();
		repaint();
	}
	
	private final static double SNAP_STRENGTH = 5;
	
	/**
	 * Snap a point to the three vertices or the center.
	 * @param p
	 * @return the index corresponding to the snapped point (tx = 0, ty = 1, tz = 2, center = 3) or -1 if none was found.
	 */
	private int snap(Point p) {
		
		// First try to snap to vertices and center
		Point[] targets = new Point[] { tx, ty, tz, tc };
		b.set(tc.x, tc.y);

		for (int i = 0; i < targets.length; i++) {
			Point pt = targets[i];
			
			if (p.distance(pt) < SNAP_STRENGTH) {
				
				p.setLocation(pt);
				
				return i;
			}
		}
		
		// Then snap to outer lines
		c.set(p.x, p.y);
		for (int i = 0; i < 3; i++) {
			Point pt1 = targets[i];
			Point pt2 = targets[(i+1) % 3];

			double dist = distToLine(p, targets[i], targets[(i+1)%3]);
			
			if (dist < SNAP_STRENGTH / 2) {
				// Project onto line
				v1.scaleAdd(1, v1, c);
				p.setLocation(new Point(rint(v1.x), rint(v1.y)));
			}
		}
		
		return -1;
	}
	
	private Point paramToPoint(double r, double g, double b) {
		ptd.x = 0;
		ptd.y = 0;
		
		ptd.x += r * (tx.x - tc.x);
		ptd.y += r * (tx.y - tc.y);

		ptd.x += g * (ty.x - tc.x);
		ptd.y += g * (ty.y - tc.y);

		ptd.x += b * (tz.x - tc.x);
		ptd.y += b * (tz.y - tc.y);
		
		Point pt = new Point(tc.x + rint(ptd.x), tc.y + rint(ptd.y));

		return pt;	
	}
	
	private void createTriangle() {
		Dimension size = super.getSize();
		if (size.height < 5 || size.width < 5) return;
		
		BufferedImage img = new BufferedImage(size.width, size.height,
				BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = img.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		
		// Compute triangle geometry
		double radius = 0.6 * Math.min(super.getSize().width, super.getSize().height);
		double l = radius * Math.sqrt(3) / 2;
		double h = radius / 2d;

		tc = new Point(COMPONENT_OFFSET.x + rint(l), COMPONENT_OFFSET.y + rint(radius));

		tx = new Point(rint(tc.x - l), rint(tc.y + h));
		ty = new Point(rint(tc.x), rint(tc.y - radius));
		tz = new Point(rint(tc.x + l), rint(tc.y + h));
		
		// Finally draw lines to connect corners
		// Compute the three vertex-centers
		a.set(tx.x, tx.y);
		b.set(ty.x, ty.y);
		c.set(tz.x, tz.y);
		ptd.set(tc.x, tc.y);
		
		v1.sub(a, ptd);
		v2.sub(b, ptd);
		v3.sub(c, ptd);
		
		double step = 0.01;
		int n = 1;

		// Don't touch the triangle border
		// otherwise color lookup will be messed up
		a.scaleAdd(step, v1, a);
		b.scaleAdd(step, v2, b);
		c.scaleAdd(step, v3, c);

		BasicStroke stroke = new BasicStroke(7f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g.setStroke(stroke);
		g.setColor(Color.BLACK);

		for (int i = 0; i < n; i++) {
			a.scaleAdd(step, v1, a);
			b.scaleAdd(step, v2, b);
			c.scaleAdd(step, v3, c);
			
			g.drawLine(rint(a.x), rint(a.y), rint(b.x), rint(b.y));
			g.drawLine(rint(a.x), rint(a.y), rint(c.x), rint(c.y));
			g.drawLine(rint(b.x), rint(b.y), rint(c.x), rint(c.y));
		}


		// Fill in pixels
		Point pt = new Point();
		Vector3d I = new Vector3d();

		g.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

		double td = (tx.distance(tc) + ty.distance(tc) + tz.distance(tc)) / 3;

		for (int x = 0; x < size.width; x++) {
			for (int y = 0; y < size.height; y++) {
				pt.setLocation(x, y);

				// Stay within triangle
				if (isInsideTriangle(pt, tx, ty, tz)) {
					double dx = pt.distance(tx);
					double dy = pt.distance(ty);
					double dz = pt.distance(tz);
					double dc = pt.distance(tc) / (td);
					
					double dxy = distToLine(pt, tx, ty);
					double dxz = distToLine(pt, tx, tz);
					double dyz = distToLine(pt, ty, tz);

					double fx = dyz * dy * dz;
					double fy = dxz * dx * dz;
					double fz = dxy * dx * dy;
					
					
//					I.set(fx, fy, fz);
					I.x = fx * colors[0].getRed() + fy * colors[1].getRed() + fz * colors[2].getRed();
					I.y = fx * colors[0].getGreen() + fy * colors[1].getGreen() + fz * colors[2].getGreen();
					I.z = fx * colors[0].getBlue() + fy * colors[1].getBlue() + fz * colors[2].getBlue();
					
					I.normalize();
					I.scale(255);
					I.scale(dc);
					
					I.x = I.x > 255 ? 255 : I.x;
					I.y = I.y > 255 ? 255 : I.y;
					I.z = I.z > 255 ? 255 : I.z;
					try {
//						img.setRGB(x, y, new Color(rint(I.x), rint(I.y), rint(I.z)).getRGB());
						g.setColor(new Color(rint(I.x), rint(I.y), rint(I.z)));
//						g.drawOval(x, y, 1, 1);
						int os = 2;
						g.drawOval(x - os / 2, y - os / 2, os, os);
					}
					catch (Exception e) {
//						System.out.println(I);
					}
				}
			}
		}

		g.setPaint(new TexturePaint(img,new Rectangle2D.Float(0, 0, img.getWidth(), img.getHeight())));
//		AffineTransform xform = new AffineTransform();
//		xform.setToIdentity();
//		xform.rotate(Math.PI/8, img.getWidth()/2, img.getHeight()/2);
//		g.transform(xform);
		g.fillRect(0, 0, img.getWidth(), img.getHeight());
		
		src = img;
	}
	
	private double distToLine(Point p, Point pa, Point pb) {
		// a is the line start
		a.set(pa.x, pa.y);
		
		// b is line end
		b.set(pb.x, pb.y);
		
		c.set(p.x, p.y);
		
		// v1 is n
		v1.sub(b, a);
		v1.normalize();
		
		// a - p
		v2.sub(a, c);
		
		// ((a-p) . n) n
		v3.scale(v2.dot(v1), v1);
		v1.sub(v2, v3);
		double dist = v1.length();
		
		return dist;
	}
	
	/**
	 * Use barycentric coordinates and Lagrange identity to determine if a point
	 * lies within a triangle. Taken from Real-Time Collision Detection
	 * 
	 * @param pt
	 * @param p1
	 * @param p2
	 * @param p3
	 * @return
	 */
	private boolean isInsideTriangle(Point pt, Point p1, Point p2,
			Point p3) {

		// Translate point and triangle so that point lies at origin
		v1.x = p1.x - pt.x;
		v1.y = p1.y - pt.y;
		v2.x = p2.x - pt.x;
		v2.y = p2.y - pt.y;
		v3.x = p3.x - pt.x;
		v3.y = p3.y - pt.y;
		
		double d12 = v1.dot(v2);
		double d13 = v1.dot(v3);
		double d23 = v2.dot(v3);
		double d33 = v3.dot(v3);

		// Make sure plane normals for pab and pbc point in the same direction
		if (d23 * d13 - d33 * d12 < 0) {
			return false;
		}

		// Make sure plane normals for pab and pca point in the same direction
		double d22 = v2.dot(v2);

		if (d12 * d23 - d13 * d22 < 0) {
			return false;
		}

		// Otherwise P must be in (or on) the triangle return 1;
		return true;
	}

	/**
	 * Project a point towards a target until it lies within the triangle
	 * @param p
	 * @param target
	 * @return
	 */
	private Point2d project(Point2d p, Point2d target) {
		double h = 1e-4;

		v1.sub(target, p);
		p.scaleAdd(h, v1, p);
		
		pti.setLocation(rint(p.x), rint(p.y));
		
		while (!isInsideTriangle(pti, tx, ty, tz)) {
			p.scaleAdd(h, v1, p);
			pti.setLocation(rint(p.x), rint(p.y));
		}
		
		return p;
	}
	
	/**
	 * Closest point to a triangle. Taken from Real-Time collision detection by
	 * Ericson
	 * 
	 * @param p
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	private Point2d getClosestPoint(Point pt, Point pa, Point pb, Point pc) {
		Point2d ptc = new Point2d();
		Point2d p = new Point2d(pt.x, pt.y);
		Point2d tc = new Point2d(this.tc.x, this.tc.y);

		if (isInsideTriangle(pt, pa, pb, pc)) {
			ptc.set(pt.x, pt.y);
			return ptc;
		}

		a.set(pa.x, pa.y);
		b.set(pb.x, pb.y);
		c.set(pc.x, pc.y);

		ab.sub(b, a);
		ac.sub(c, a);
		ap.sub(p, a);
		cp.sub(p, c);
		bp.sub(p, b);

		// Check if P in vertex region outside A
		double d1 = ab.dot(ap);
		double d2 = ac.dot(ap);
		// barycentric coordinates (1,0,0)
		if (d1 <= 0 && d2 <= 0) {
			ptc.set(a);

			// Project inside triangle
			return project(ptc, tc);
		}

		// Check if P in vertex region outside B
		double d3 = ab.dot(bp);
		double d4 = ac.dot(bp);

		// barycentric coordinates (0,1,0)
		if (d3 >= 0 && d4 <= d3) {
			ptc.set(b);

			// Project inside triangle
			return project(ptc, tc);
		}

		// Check if P in vertex region outside C
		double d5 = ab.dot(cp);
		double d6 = ac.dot(cp);

		// barycentric coordinates (0,0,1)
		if (d6 >= 0 && d5 <= d6) {
			ptc.set(c);

			// Project inside triangle
			return project(ptc, tc);
		}

		// Check if P in edge region of AB, if so return projection of P onto AB
		double vc = d1 * d4 - d3 * d2;

		if (vc <= 0 && d1 >= 0 && d3 <= 0) {
			double v = d1 / (d1 - d3);
			ab.scaleAdd(v, ab, a);
			// barycentric coordinates (1-v,v,0)
			ptc.set(ab);
			
			// Project inside triangle
			return project(ptc, tc);
		}

		// Check if P in edge region of AC, if so return projection of P onto AC
		double vb = d5 * d2 - d1 * d6;
		if (vb <= 0 && d2 >= 0 && d6 <= 0) {
			double w = d2 / (d2 - d6);
			ac.scaleAdd(w, ac, a);
			// barycentric coordinates (1-w,0,w)
			ptc.set(ac);

			// Project inside triangle
			return project(ptc, tc);
		}

		// Check if P in edge region of BC, if so return projection of P onto BC
		double va = d3 * d6 - d5 * d4;
		if (va <= 0 && (d4 - d3) >= 0 && (d5 - d6) >= 0) {
			double w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
			cb.sub(c, b);
			cb.scaleAdd(w, cb, b);
			// barycentric coordinates (0,1-w,w)
			ptc.set(cb);

			// Project inside triangle
			return project(ptc, tc);
		}

		// P inside face region. Compute Q through its barycentric coordinates
		// (u,v,w)
		double denom = 1.0f / (va + vb + vc);
		double v = vb * denom;
		double w = vc * denom;

		// =u*a+v*b+w*c,u=va*denom=1.0f-v-w
		ab.scale(v);
		ac.scale(w);
		ptc.add(a);
		ptc.add(ab);
		ptc.add(ac);

		// Project inside triangle
		return project(ptc, tc);
	}

	private NumberFormat nf = NumberFormat.getNumberInstance();
			
	@Override
	protected void paintComponent(Graphics g) {
		if (src == null) {
			createTriangle();
		}

		super.paintComponent(g);

		g.drawImage(src, 0, 0, this);

		if (mp != null) {
			
			Point3d pt = getPoint();

			if (pt != null) {
				int osize = 10;
				g.setColor(Color.GRAY);
				g.drawOval(mp.x - osize / 2, mp.y - osize / 2, osize, osize);

				g.setColor(Color.WHITE);
				double radius = 0.5 * Math.min(super.getSize().width, super.getSize().height);
				int dist = rint(0.5 * radius);
				Point2d r = new Point2d(dist * Math.sqrt(3) / 2, dist / 2);
				Font font = new Font(fontName, Font.BOLD, 20);
				g.setFont(font);
				g.drawChars(new char[] { 'X' }, 0, 1, rint(tx.x + r.x) - 5, rint(tx.y - r.y + 7));
				g.drawChars(new char[] { 'Y' }, 0, 1, rint(ty.x - 5), rint(ty.y + dist));
				g.drawChars(new char[] { 'Z' }, 0, 1, rint(tz.x - r.x - 5),
						rint(tz.y - r.y + 7));

				// Draw frame spread
//				g.drawLine(0, 0, super.getSize().width, super.getSize().height);
								
				font = new Font(fontName, Font.PLAIN, 10);
				g.setColor(Color.GRAY);
				g.setFont(font);
				
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				String txt = "(" + nf.format(pt.x) + "," + nf.format(pt.y) + ")";
//				g.drawChars(txt.toCharArray(), 0, txt.length(), mp.x, mp.y);
			}
		}

	}

	public static void main(String[] args) {
		Dimension dim = new Dimension(500, 500);

		final JTriangleColorPanel t = new JTriangleColorPanel(dim);

		JFrame frame = new JFrame();
		frame.getContentPane().add(t);
//		VerticalFlowPanel vfp = new VerticalFlowPanel();
//		vfp.add(t);
//		JButton btndo = new JButton("Do");
//		btndo.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				dosomething(t);
//			}
//		});
//		vfp.add(btndo);
//		frame.getContentPane().add(vfp.getPanel());
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(dim);
		frame.show();
	}

	protected static void dosomething(JTriangleColorPanel t) {
		t.setPoint(0, 0, 0);
	}

	private Timer timer;
	@Override
	public void componentResized(ComponentEvent e) {
		timer.restart();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
	}

	@Override
	public void componentShown(ComponentEvent e) {
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	}

}