package swing.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/*
 * FileChooserDemo.java uses these files:
 *   images/Open16.gif
 *   images/Save16.gif
 */
public class FileChooser extends JPanel
                             implements ActionListener {
    static private final String newline = "\n";
    private JButton openButton, saveButton;
    private JTextArea log;
    private JFileChooser fc;
    private ActionListener saveListener = null;
    private ActionListener openListener = null;
    private File currentFile;
    private JTextPane pathText = new JTextPane();
//    private JTextArea ntext = new JTextArea();
    private JLabel lblText = new JLabel();
    
    private JList list;
    private DefaultListModel listModel;
    
    public FileChooser() {
    	this("");
    }
    
    public FileChooser(String directory) {
        this(directory, "");
    }    

    public FileChooser(String directory, String text) {
    	this(directory, text, new ArrayList<String>(), true, true);
    }

    public FileChooser(String directory, String text, boolean open, boolean save) {
    	this(directory, text, new ArrayList<String>(), open, save);
    }

    public FileChooser(String directory, String text, String filetype) {
    	this(directory, text, filetype, true, true);
    }
    
    public FileChooser(String directory, String text, String filetype, boolean open, boolean save) {
    	this(directory, text, new ArrayList<String>(Arrays.asList(filetype)), open, save);
    }
    
    /**
     * 
     * @param directory
     * @param text
     * @param filetypes a list of file types to parse (without a leading dot e.g. xml, jpg, png)
     */
    public FileChooser(String directory, String text, List<String> filetypes, boolean open, boolean save) {
        super(new BorderLayout());
        
        boolean useHistory = false;

        //Create the log first, because the action listeners
        //need to refer to it.
        log = new JTextArea(5,20);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);

        //Create a file chooser
        fc = new JFileChooser(directory);
        fc.setFileHidingEnabled(true);
        
        if (filetypes == null || filetypes.size() == 0) {
            fc.setAcceptAllFileFilterUsed(true);
        }
        else {
            fc.setAcceptAllFileFilterUsed(false);
	
            for (final String filetype : filetypes) {
            	if (filetype == null || filetype.equals("")) continue;
            	fc.addChoosableFileFilter(new MultipleFileFilter(filetype));
            }
        }
        
        //fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        openButton = null;
        saveButton = null;
        
        if (open) {
            URL urlOpen  = getClass().getResource("resources/Open16.gif");
            ImageIcon openIcon = new ImageIcon(urlOpen);
        	openButton = new JButton("Open...", openIcon);
            openButton.addActionListener(this);
        }
        
        if (save) {
            URL urlSave = getClass().getResource("resources/Save16.gif");
            ImageIcon saveIcon = new ImageIcon(urlSave);
            saveButton = new JButton("Save...", saveIcon);
            saveButton.addActionListener(this);
        }

        //For layout purposes, put the buttons in a separate panel
        JPanel lblPanel = new JPanel(); //use FlowLayout

        pathText.setText("");
        SimpleAttributeSet attribs = new SimpleAttributeSet();  
        StyleConstants.setAlignment(attribs , StyleConstants.ALIGN_LEFT);
        int size = 150;
        pathText.setPreferredSize(new Dimension(size, 20));
        pathText.setParagraphAttributes(attribs, true);  
        pathText.setBorder(BorderFactory.createEtchedBorder());
//        ntext.setPreferredSize(new Dimension(200, 15));
//        ntext.setLineWrap(true);
//        ntext.setWrapStyleWord(true); 
        
        // Add controls
        lblPanel.add(lblText);
        lblPanel.add(pathText);

        int tx = 100;
        int ty = 30;
        lblText.setText(text);
//        lblText.setPreferredSize(new Dimension(tx, ty));
        lblText.setMaximumSize(new Dimension(tx, ty));
        lblText.setSize(tx, ty);

        // Open/Save buttons
        JPanel btnPanel;
        
        if (useHistory) {
        	btnPanel = new JPanel(new GridLayout(2, 1));
        }
        else {
        	btnPanel = lblPanel;
        }
        
        if (open)
        	btnPanel.add(openButton);
        
        if (save)
        	btnPanel.add(saveButton);

        if (useHistory) add(btnPanel, BorderLayout.LINE_START);

        add(lblPanel, useHistory ? BorderLayout.PAGE_START : BorderLayout.LINE_START);

        setPath(directory);
//        
//        if (useHistory) {
//            // Add the history list
//            listModel = new DefaultListModel();
//            listModel.addElement("Line 1");
//            listModel.addElement("Line 2");
//            listModel.addElement("Line 3");
//     
//            // Create the list and put it in a scroll pane.
//            list = new JList(listModel);
//            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//            list.setSelectedIndex(0);
//            list.setVisibleRowCount(3);
//            
//            JScrollPane listScrollPane = new JScrollPane(list);
//            add(listScrollPane, BorderLayout.CENTER);
//            
//            addHistoryListener(new ListSelectionListener() {
//    			
//    			@Override
//    			public void valueChanged(ListSelectionEvent e) {
//    				if (e.getValueIsAdjusting() == false)
//    					System.out.println("Selected " + listModel.getElementAt(list.getSelectedIndex()).toString());
//    			}
//    		});
//        }
        
        // Set bounds
//        Dimension fsize = new Dimension(530, 40);
//        setMaximumSize(fsize);
//        setPreferredSize(fsize);
//        setSize(fsize.width, fsize.height);

    }
    
    public void addHistoryListener(ListSelectionListener listener) {
    	list.addListSelectionListener(listener);
    }

    public void addOpenActionListener(ActionListener l) {
        openListener = l;
    }
    
    public void addSaveActionListener(ActionListener l) {
        saveListener = l;
    }

    public File getCurrentFile() {
        return currentFile;
    }
    
    @Override
	public void actionPerformed(ActionEvent e) {

        //Handle open button action.
        if (e.getSource() == openButton) {
            int returnVal = fc.showOpenDialog(FileChooser.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                currentFile = fc.getSelectedFile();
                //This is where a real application would open the file.
                log.append("Opening: " + currentFile.getName() + "." + newline);

                if (openListener != null) {
                    openListener.actionPerformed(e);
                }
            } else {
                log.append("Open command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());

        //Handle save button action.
        } else if (e.getSource() == saveButton) {
            int returnVal = fc.showSaveDialog(FileChooser.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                currentFile = fc.getSelectedFile();
                //This is where a real application would save the file.
                log.append("Saving: " + currentFile.getName() + "." + newline);
                
                if (saveListener != null) {
                    saveListener.actionPerformed(e);
                }

            } else {
                log.append("Save command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());
        }
        if (currentFile != null) {
            setPath("../" + currentFile.getParentFile().getName() + "/" + currentFile.getName());
        }
    }

    /**
     * @param text
     */
    public void setPath(String text) {
        pathText.setText(text);
    }

    /**
     * @param text
     */
    public void setLabel(String label) {
        lblText.setText(label);
    }

    @Override
    public String toString() {
        return log.getText();
    }

	public String getPath() {
		return pathText.getText().substring(2);
	}
    /*
     * Get the extension of a file.
     */
    private String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }

    public void selectDirectoriesOnly() {
    	fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    public void selectFilesOnly() {
    	fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }

    public void selectFilesAndDirectories() {
    	fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("FileChooserDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Add content to the window.
        frame.add(new FileChooser("", "Some text"));

        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        createAndShowGUI();
        
    }

}
