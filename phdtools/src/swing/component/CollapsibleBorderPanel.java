package swing.component;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class CollapsibleBorderPanel extends CollapsiblePanel {

	private static final long serialVersionUID = -1552366145356841650L;
	
	public CollapsibleBorderPanel(JPanel content, String name) {
		super(content, name);
		
		content.setName(name);
		content.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), name));
	}
	public CollapsibleBorderPanel(VerticalFlowPanel vfp, String name) {
		this(vfp.getPanel(), name);
	}
}
