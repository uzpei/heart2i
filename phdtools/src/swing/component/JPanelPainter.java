package swing.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class JPanelPainter {
	private Timer paintTimer;
	private JPanel panel;

	public JPanelPainter(JPanel p) {
		this(p, 10);
	}
	
	public JPanelPainter(JPanel p, int delay) {
		panel = p;
		
		paintTimer = new Timer(delay, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.validate();
				panel.repaint();
			}
		});
		paintTimer.setCoalesce(true);
		paintTimer.setRepeats(false);
	}
	
	public void paint() {
		paintTimer.start();
	}
}
