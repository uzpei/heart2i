package system.object;

public class Triplet<L, C, R> {

	private final L left;
	private final C center;
	private final R right;

	public Triplet(L left, C center, R right) {
		this.left = left;
		this.center = center;
		this.right = right;
	}

	public L getLeft() {
		return left;
	}

	public C getCenter() {
		return center;
	}
	
	public R getRight() {
		return right;
	}

	@Override
	public int hashCode() {
		return left.hashCode() ^ center.hashCode() ^ right.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Triplet))
			return false;
		Triplet pairo = (Triplet) o;
		return this.left.equals(pairo.getLeft()) && this.center.equals(pairo.getCenter())
				&& this.right.equals(pairo.getRight());
	}

}