package vector;

import javax.vecmath.Point3i;

import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import voxel.VoxelVectorField;

public class DiscreteHelmholtzDecomposition {

	public static void decompose(VoxelVectorField V) {
		// TODO: use mask to speed everything up
		
		Point3i size = V.getDimension();
		int n = size.x * size.y * size.z;

		/*
		 *  Assume that each node is at center of voxel
		 *  (8 cubic neighbors)
		 */

		// Compute neighbor indices
		int[][][] indices = new int[size.x][size.y][size.z]; 
		int l = 0;
		for (int i = 0; i < size.x; i++) {
			for (int j = 0; j < size.y; j++) {
				for (int k = 0; k < size.z; k++) {
					indices[i][j][k] = l++;
				}
			}
		}
		
		// Compute neighbors
		Point3i p = new Point3i();
		int[][] neighborhoods = new int[n][8];
		for (int i = 0; i < size.x; i++) {
			for (int j = 0; j < size.y; j++) {
				for (int k = 0; k < size.z; k++) {
					p.set(i, j, k);
					
					// Find neighbors at current point
					int[][] neighbors = getNeighbors(p);
					
					// Find current point index
					int pindex = indices[i][j][k];
					
					// Fill in neighbor indices for this index
					for (int neighbor = 0; neighbor < 8; neighbor++) {
						// Get neighborhood true index
						int[] pn = neighbors[neighbor];
						
						// TODO: deal with this properly
						// Skip boundary
						if (pn[0] < 0 || pn[0] > size.x) continue;
						if (pn[1] < 0 || pn[1] > size.y) continue;
						if (pn[2] < 0 || pn[2] > size.z) continue;
						
						int nindex = indices[pn[0]][pn[1]][pn[2]];
						
						// Assign to this point's neighborhood
						neighborhoods[pindex][neighbor] = nindex;
					}
				}
			}
		}
		
		// Compute interpolation function phi for each neighbor
		
		
		// Use a flexcomprowmatrix to store basis interpolator derivatives
		FlexCompRowMatrix m = new FlexCompRowMatrix(3 * n, 3 * n);
	}
	
	private static int[][] getNeighbors(Point3i p) {
		// TODO: handle boundary
		
		// array: [neighbor = 1 to 8][component = x, y, z]
		int[][] neighbors = new int[8][3];

		// Find 8 cubic neighbors
		// Go clockwise around cube, back to front
		// starting at max j, k
		// with x pointing towards us and y to the right
		int x = p.x;
		int y = p.y;
		int z = p.z;
		
		int i = 0;
		
		// Back face
		neighbors[i][0] = x + 0;
		neighbors[i][1] = y + 1;
		neighbors[i++][2] = z + 1;

		neighbors[i][0] = x + 0;
		neighbors[i][1] = y + 1;
		neighbors[i++][2] = z + 0;

		neighbors[i][0] = x + 0;
		neighbors[i][1] = y + 0;
		neighbors[i++][2] = z + 0;

		neighbors[i][0] = x + 0;
		neighbors[i][1] = y + 0;
		neighbors[i++][2] = z + 1;

		// Front face
		neighbors[i][0] = x + 1;
		neighbors[i][1] = y + 1;
		neighbors[i++][2] = z + 1;

		neighbors[i][0] = x + 1;
		neighbors[i][1] = y + 1;
		neighbors[i++][2] = z + 0;

		neighbors[i][0] = x + 1;
		neighbors[i][1] = y + 0;
		neighbors[i++][2] = z + 0;

		neighbors[i][0] = x + 1;
		neighbors[i][1] = y + 0;
		neighbors[i++][2] = z + 1;

		return neighbors;
	}
}
