package heart;

import gl.material.GLMaterial;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import com.sun.xml.internal.ws.developer.StreamingAttachment;

import diffgeom.DifferentialOneForm;
import diffgeom.DifferentialOneForm.MovingAxis;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import math.matrix.Matrix3d;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;

public class Tractography {

	private final static GLU glu = new GLU();
	
	private Heart heart;

	private VoxelFrameField frameField;

	private VoxelBox voxelBox;

	private Point3i connectionOrigin = new Point3i();

	private CartanParameter connections = new CartanParameter();

	private LinkedList<LinkedList<Point3d>> tractography;

	private LinkedList<Myofiber> myofibers;
	
	private BooleanParameter enabled = new BooleanParameter("enabled", false);

	private BooleanParameter orthogonalize = new BooleanParameter("orthogonalize", false);

	private DoubleParameter deltaT = new DoubleParameter("delta theta", 180, 0, 180);
	
	// Number of steps
	private IntParameter nsteps = new IntParameter("step count", 20, 0, 1000);

	private IntParameter res = new IntParameter("res", 1, 1, 100);

	private DoubleParameter radius = new DoubleParameter("radius", 0.02, 0.001, 1);
	
	private BooleanParameter fixColors = new BooleanParameter("Fix colors", false);
	
	// Step size
	private DoubleParameter hstep = new DoubleParameter("step size", 0.5, 0, 5);

	private BooleanParameter uvw = new BooleanParameter("uvw", false);

	private BooleanParameter c123 = new BooleanParameter("c123", false);

	private BooleanParameter highDef = new BooleanParameter("highDef", true);

	private BooleanParameter useDT = new BooleanParameter("use dt", false);

	private BooleanParameter dual = new BooleanParameter("dual", true);

	private BooleanParameter helixColoring = new BooleanParameter("helix", true);

	private BooleanParameter useConnections = new BooleanParameter("connection form", false);

	/**
	 * Default constructor.
	 */
	public Tractography() {
		Myofiber.CYLINDER_RADIUS = (float) radius.getDefaultValue();
	}
	
	public void update(Heart heart) {
		this.heart = heart;
		this.frameField = heart.getFrameField();
		this.voxelBox = heart.getVoxelBox();
	}

	public void update(VoxelFrameField frameField, VoxelBox box) {
		this.frameField = frameField;
		this.voxelBox = box;
	}

	private boolean computationRequested = false;
	private boolean displaying = false;
	
	private void applyHelixColor(Vector3d v, float[] color) {
		float ha = (float) Math.asin(v.z);
		double r = 0.6;
		color[1] = (float) (Math.cos(r*(ha + Math.PI / 2)));
		color[0] = (float) (Math.cos(r*(ha - Math.PI / 2)));
		color[3] = 1;
	}
	
	private float[][] colors;

	private boolean colorsNeedRefresh = false;

	public void nextStep(boolean update) {
		nsteps.hold(!update);
		nsteps.setValue(nsteps.getValue() + 1);
		nsteps.hold(false);
	}

	private void updateColors() {
		if ((colors != null && colors.length >= tractography.size() && fixColors.getValue())) {
			colorsNeedRefresh = false;
			return;
		}
		
		colors = new float[tractography.size()][4];

		IntensityVolume dt = heart != null ? heart.getDistanceTransformEpi() : null;
		
		double p = 1;
		if (c123.getValue()) {
//			Matrix3d df = DifferentialOneForm.computeOneForm(heart.getFrameField(), MovingAxis.T, MovingAxis.N, MovingAxis.T);
			Matrix3d df = DifferentialOneForm.computeOneForm(frameField, MovingAxis.F1, MovingAxis.F2, MovingAxis.F3);

			double[] minmax = df.getminMax();
			
			double dv = Math.abs(minmax[1] - minmax[0]);
			double v0 = (minmax[1] - minmax[0]) / 2;
			
			System.out.println(Arrays.toString(minmax));

			int i = 0;
			for (List<Point3d> streamline : tractography) {
				float[] c = new float[4];
				Point3i pi = MathToolset.tuple3dTo3i(streamline.get(0));
				double d = df.get(pi.x, pi.y, pi.z);
				d = Math.abs(d-minmax[0]) / dv;
				
//				System.out.println(d);
				c[0] = (float) (Math.pow(d, p));
				c[1] = (float) (Math.pow(1 - d, p));
				c[2] = (0);
				c[3] = 1;
				colors[i++] = c;
			}			
		}
		else if (useDT.getValue() && dt != null) {
			double[] minmax = dt.getminMax();

			// Saturate
			minmax[1] = minmax[1] / 2;
			
			int i = 0;
			for (List<Point3d> streamline : tractography) {
				float[] c = new float[4];
				Point3i pi = MathToolset.tuple3dTo3i(streamline.get(0));
				double d = (dt.get(pi.x, pi.y, pi.z)-minmax[0]) / (minmax[1] - minmax[0]);
//				System.out.println(d);
				c[1] = (float) (Math.pow(d, p));
				c[2] = (float) (Math.pow(1-d, p));
				c[0] = 0;
				c[3] = 1;
				colors[i++] = c;
			}			
		}
		else if (uvw.getValue()) {
			int i = 0;
//			Point3d center = heart.getVoxelBox().getCenter();
			Point3d o = voxelBox.getOrigin3d();
			double cr = voxelBox.getSpan3d().length();
			for (List<Point3d> streamline : tractography) {
				float[] c = new float[4];
				Point3d p0 = streamline.get(0);
				
				float co = (float) ((p0.y - o.y) / voxelBox.getSpanY());
	
//				c[0] = co;
//				c[1] = 1-co;
//				c[2] = (float) ((p0.x - o.x) / heart.getVoxelBox().getSpanX());
//				c[3] = 1;
				float lambda = (float) ((p0.y - o.y) / voxelBox.getSpanY());
				c[0] = (float) Math.pow(1 - 2 * Math.abs(lambda - 0.5f), 2);
				c[1] = (float) Math.pow(1 - lambda, 2);
				c[2] = (float) Math.pow(lambda, 2);
				c[3] = 1;
				
				colors[i++] = c;
			}
		}
		else if (helixColoring.getValue()) {
			int[] dim = frameField.getMask().getDimension();
			Point3d center = new Point3d(dim[0]/2,dim[1]/2,dim[2]/2);
			
			// FIXME: this was for a single visualization only
			// Pass center as optional parameter
			center.x += 1e-6;
			center.y += 10;
			center.z += 1e-6;
			
			int i = 0;
			Vector3d n = new Vector3d();
			Vector3d z = new Vector3d(0,0,1);
			Vector3d axis = new Vector3d();
			
			Color c1 = new Color(0.1f, 0.2f, 1.0f);
			Color c2 = new Color(1.0f, 0.2f, 0.2f);
			
			float[] c1h = Color.RGBtoHSB(c1.getRed(), c1.getGreen(), c1.getBlue(), null);
			float[] c2h = Color.RGBtoHSB(c2.getRed(), c2.getGreen(), c2.getBlue(), null);
			
			for (List<Point3d> streamline : tractography) {
				Point3d p0 = streamline.get(0);
				Point3i pt = MathToolset.tuple3dTo3i(p0);
				Vector3d v = frameField.getF1().getVector3d(pt);
				
				// remove normal component from f1
				n.sub(p0, center);
				n.normalize();
				v.scaleAdd(-v.dot(n), n, v);
				v.normalize();

				// normalize angle in [0, 1] for [0, 180]
				float ha = (float) (1.2 * Math.acos(v.dot(z)) / Math.PI);
				ha = 1 - (float) MathToolset.clamp(ha, 0, 1);
				
				float[] c = new float[4];
//				c[0] = (float) ha;
//				c[1] = (float) (1-ha);
				
				float h = (1-ha) * c1h[0] + ha * c2h[0];
//				float s = (1-ha) * c1h[1] + ha * c2h[1];
				float s = 1;
				float b = 1f;
				
				Color col = new Color(Color.HSBtoRGB(h, s, b));
				c[0] = col.getRed() / 255f;
				c[1] = col.getGreen() / 255f;
				c[2] = col.getBlue() / 255f;
				c[3] = 1;
				colors[i++] = c;
				
//				double r = 0.6;
//				float ha = (float) Math.asin(v.z);
//				c[1] = (float) (Math.cos(r*(ha + Math.PI / 2)));
//				c[0] = (float) (Math.cos(r*(ha - Math.PI / 2)));
//				c[3] = 1;
			}
		}
		else {
			Random rand = new Random();
			int i = 0;
			for (List<Point3d> streamline : tractography) {
				if (i % 2 == 1)  {
					colors[i] = colors[i-1];
					i++;
					continue;
				}
				
				
				float[] c = new float[4];
				c[0] = rand.nextFloat();
				c[1] = rand.nextFloat();
				c[2] = rand.nextFloat();
				c[3] = 1;
				colors[i++] = c;
				
			}
		}
		
		colorsNeedRefresh = false;
	}
	
	/**
	 * Construct myofibers from traced streamlines.
	 */
	private void updateData() {
		
		System.out.println("Tracing...");
		SimpleTimer timer = new SimpleTimer();

		// First update colors
		updateColors();
		
		// Then load data in
		if (myofibers == null) {
			myofibers = new LinkedList<Myofiber>();
		}
		myofibers.clear();
		
		int i = 0;
		for (List<Point3d> streamline : tractography) {
//			System.out.println("New streamline");
//			System.out.println(streamline.get(0));
//			System.out.println(streamline.get(1));
			myofibers.add(new Myofiber(streamline, res.getValue(), colors[i++]));
		}
		System.out.println(timer.tick_ms());
	}
	
	public void display(GLAutoDrawable drawable) {
		display(drawable, false);
	}

	public void display(GLAutoDrawable drawable, boolean forced) {
		if (tractography == null || computationRequested) return;
		
		if (!forced && !enabled.getValue())
			return;
		
		GL2 gl = drawable.getGL().getGL2();
		
		displaying = true;
		float[] color = new float[4];

		Point3d p0 = new Point3d();
		Vector3d v = new Vector3d();
		
		if (colorsNeedRefresh)
			updateData();

		if (highDef.getValue()) {
			gl.glPushMatrix();
			gl.glTranslated(0.5, 0.5, 0.5);
			gl.glEnable(GL2.GL_LIGHTING);
			GLMaterial.PEARL.apply(gl);
			for (Myofiber myofiber : myofibers) {
				myofiber.display(drawable);
			}
			gl.glPopMatrix();
		}
//		// Random colors
//		else if (highDef.getValue()) {
//			
//			gl.glEnable(GL2.GL_LIGHTING);
//			
//			int i = 0;
//			for (Myofiber myofiber : myofibers) {
//				if (dual.getValue()) {
//					color = colors[(i / 2)];
//				}
//				else {
//					color = colors[i];
//				}
//
//				gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, color, 0);
//				gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, color, 0);
//
//				myofiber.display(drawable);
//				i++;
//			}
//		}
//		else if (helixColoring.getValue()) {
//			for (Myofiber streamline : myofibers) {
//				
//				if (computationRequested) {
//					break;
//				}
//					
//				gl.glBegin(GL.GL_LINE_STRIP);
//				for (Point3d p : streamline.getStreamline()) {
//					v.sub(p, p0);
//					
//					applyHelixColor(v, color);
//					gl.glColor4d(color[0], color[1], color[2], color[3]);
//					gl.glVertex3d(p.x, p.y, p.z);
//					
//					p0 = p;
//				}
//				gl.glEnd();
//			}
//		}
		else {
			gl.glDisable(GL2.GL_LIGHTING);
			gl.glColor4d(1,0, 0, 0.9);

			for (Myofiber streamline : myofibers) {
				
				if (computationRequested) {
					break;
				}
					
				gl.glBegin(GL.GL_LINE_STRIP);
				for (Point3d p : streamline.getStreamline()) {
					gl.glVertex3d(p.x, p.y, p.z);
				}
				gl.glEnd();
			}
		}
		
		
		
		displaying = false;
	}
	
	public void setConnectionParameters(Point3i origin, CartanParameter parameter) {
		connectionOrigin = origin;
		connections = parameter;
		useConnections.setValue(true);
	}
	
	private Point3i exclusionPoint = null;
	
	public void excludeOrigin(Point3i origin) {
		exclusionPoint = origin;
	}
	
	public void trace() {
		if (isHolding || !enabled.getValue() || frameField == null || voxelBox == null) {
			return;
		}
		computationRequested = true;
		
		while (displaying)
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		List<Point3i> seeds = voxelBox.getVolume();
		
		// Construct streamlines starting from seeds
		
		// Maximal angle delta in degrees
		double deltaT = this.deltaT.getValue();
		
		// Number of steps
		int nsteps = this.nsteps.getValue();
		
		// Step size
		double hstep = this.hstep.getValue();
		
		tractography = new LinkedList<LinkedList<Point3d>>();
		
		Vector3d v_prev = new Vector3d();
		Vector3d v = new Vector3d();
		Vector3d cv = new Vector3d();
		
		double rad2deg = Math.PI / 180;
		
		OrientationFrame frame = frameField.getFrame(connectionOrigin); 
		Point3i currentVoxel = new Point3i();
		
		// Start seeds
		for (Point3i seed : seeds) {
			// Continue if this point is excluded
			if (exclusionPoint != null && seed.equals(exclusionPoint))
				continue;
			
			LinkedList<Point3d> streamline = new LinkedList<Point3d>();
			
			Point3d pseed = new Point3d(MathToolset.tuple3iToVector3d(seed));
			streamline.add(pseed);

			// Trace each seed
			Point3d p_prev = pseed;
			for (int it = 0; it < nsteps; it++) {
				
				currentVoxel.set(MathToolset.roundInt(p_prev.x), MathToolset.roundInt(p_prev.y), MathToolset.roundInt(p_prev.z));
				
				if (!useConnections.getValue() && !voxelBox.dimensionContains(currentVoxel))
					break;
				
				// Use the connection form defined at p to generate streamlines
				if (useConnections.getValue()) {
					cv.set(p_prev.x - connectionOrigin.x, p_prev.y - connectionOrigin.y, p_prev.z - connectionOrigin.z);
					OrientationFrame extrapolated = new OrientationFrame(DifferentialOneForm.extrapolate(frame, connections, cv));

					if (orthogonalize.getValue()) {
						MathToolset.orthogonalize(extrapolated.getFrame());
						extrapolated = new OrientationFrame(extrapolated.getFrame());
					}

					v.set(extrapolated.getAxis(FrameAxis.X));
				}
				else
					frameField.getF1().getVector3d(currentVoxel.x, currentVoxel.y, currentVoxel.z, v);

				if (v.angle(v_prev) > deltaT * rad2deg) {
					break;
				}
				
				// Create new point
				Point3d p = new Point3d();
				p.scaleAdd(hstep, v, p_prev);
				streamline.add(p);
				
				p_prev = p;
				v_prev.set(v);
			}
			
			tractography.add(streamline);
			
			// Trace dual (-T) as well
			if (dual.getValue()) {
				streamline = new LinkedList<Point3d>();
				
				pseed = new Point3d(MathToolset.tuple3iToVector3d(seed));
				streamline.add(pseed);

				// Trace each seed
				p_prev = pseed;
				for (int it = 0; it < nsteps; it++) {
					
					currentVoxel.set(MathToolset.roundInt(p_prev.x), MathToolset.roundInt(p_prev.y), MathToolset.roundInt(p_prev.z));
					
					if (!useConnections.getValue() && !voxelBox.dimensionContains(currentVoxel))
						break;

					// Use the connection form defined at p to generate streamlines
					if (useConnections.getValue()) {
						cv.set(p_prev.x - connectionOrigin.x, p_prev.y - connectionOrigin.y, p_prev.z - connectionOrigin.z);
						OrientationFrame extrapolated = new OrientationFrame(DifferentialOneForm.extrapolate(frame, connections, cv));
						v.set(extrapolated.getAxis(FrameAxis.X));
					}
					else
						frameField.getF1().getVector3d(currentVoxel.x, currentVoxel.y, currentVoxel.z, v);

					
					v.scale(-1);
					if (v.angle(v_prev) > deltaT * rad2deg) {
						break;
					}
					
					// Create new point
					Point3d p = new Point3d();
					p.scaleAdd(hstep, v, p_prev);
					streamline.add(p);
					
					p_prev = p;
					v_prev.set(v);
				}
				
				tractography.add(streamline);
			}
		}
		
		computationRequested = false;
		
		colorsNeedRefresh = true;
		
//		System.out.println("Done tracing.");
	}
	
	public void setTractography(LinkedList<LinkedList<Point3d>> tracto) {
		this.tractography = tracto;
		colorsNeedRefresh = true;
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Tractography"));
		
		JPanel boolPanel = new JPanel(new GridLayout(5, 2));
		
		boolPanel.add(enabled.getControls());
		boolPanel.add(orthogonalize.getControls());
		boolPanel.add(fixColors.getControls());
		
		boolPanel.add(highDef.getControls());
		boolPanel.add(useDT.getControls());
		boolPanel.add(c123.getControls());
		boolPanel.add(uvw.getControls());
		boolPanel.add(dual.getControls());
		boolPanel.add(helixColoring.getControls());
		boolPanel.add(useConnections.getControls());
		vfp.add(boolPanel);
		
		vfp.add(nsteps.getSliderControls());
		vfp.add(radius.getSliderControls());
		
		// Radius
		radius.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				Myofiber.CYLINDER_RADIUS = radius.getFloatValue();
				trace();
			}
		});
		
		vfp.add(res.getSliderControls());
		vfp.add(hstep.getSliderControls());
		vfp.add(deltaT.getSliderControls());
		
		JButton btnSMooth = new JButton("smooth");
		btnSMooth.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Myofiber m : myofibers) {
					m.applyLaplacianSmoothing();
				}
			}
		});
		vfp.add(btnSMooth);
		
		ParameterListener tl = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				trace();
			}
		};
		
		c123.addParameterListener(tl);
		res.addParameterListener(tl);
		fixColors.addParameterListener(tl);
		useDT.addParameterListener(tl);
		uvw.addParameterListener(tl);
		dual.addParameterListener(tl);
		enabled.addParameterListener(tl);
		nsteps.addParameterListener(tl);
		hstep.addParameterListener(tl);
		deltaT.addParameterListener(tl);
		useConnections.addParameterListener(tl);
		
		return vfp.getPanel();

	}

	public int getNumSteps() {
		return nsteps.getValue();
	}
	
	public double getStepSize() {
		return hstep.getValue();
	}

	public void setStepSize(double s) {
		hstep.setValue(s);
	}

	public void setNumSteps(int n) {
		nsteps.setValue(n);
	}

	public void setEnabled(boolean b) {
		enabled.setValue(b);
	}

	public void setHighDef(boolean b) {
		highDef.setValue(b);
	}

	private boolean isHolding = false;
	
	public void hold(boolean b) {
		isHolding = b;
	}
	
	public void setOrthogonalization(boolean b) {
		orthogonalize.setValue(b);
	}
}
