package heart.test;

import helicoid.fitting.Fittable;
import helicoid.modeling.HelicoidGenerator;
import helicoid.modeling.tracing.HelicoidTracer;
import helicoid.parameter.HelicoidParameter;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.CoordinateFrameSample;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.MathToolset;

public class FittableHelicoidField implements Fittable {

	private HelicoidGenerator generator;
	
	private HelicoidParameter parameter = new HelicoidParameter();
	
	public FittableHelicoidField(HelicoidGenerator gen) {
		localFrame = new CoordinateFrame();
		
		generator = gen;
		generator.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter = generator.getParameter();
			}
		});
	}
	
	public void setParameter(HelicoidParameter parameter) {
		this.parameter.set(parameter);
	}
	
	private Point3d getTruePosition(Point3i p) {
		Point3d pd = MathToolset.tuple3iToPoint3d(p);
		pd.sub(localFrame.getTranslation());
		
		return pd;
	}
	
	public CoordinateFrameSample[] getCoordinateFrames(Point3i p) {
		Point3d pd = getTruePosition(p);

		OrientationFrame frame = HelicoidTracer.getHelicoidFrame(parameter, pd);

//		frame.postmultiply(localFrame.getRotation(rotation));
		frame.premultiply(localFrame.getRotation(rotation));
				
    	CoordinateFrameSample[] samples = new CoordinateFrameSample[] { new CoordinateFrameSample(frame, p) };
    	samples[0].presampleInteger(axisSamples, samplingStyle, true);

    	return samples;
	}

	private Random rand = new Random();
	
	public Vector3d getOrientation(Point3i p) {
		return new Vector3d();
//		Point3d pd = VectorToolset.tuple3iToPoint3d(p);
//		pd.sub(localFrame.getTranslation());
//		
//    	Vector3d result = new Vector3d();
//		generator.generateSingleDirection(parameter, pd, result);
//
//		result.x += -noiseMag + 2 * noiseMag * rand.nextDouble();
//		result.y += -noiseMag + 2 * noiseMag * rand.nextDouble();
//		result.z += -noiseMag + 2 * noiseMag * rand.nextDouble();
//		result.normalize();
//		
//		return result;
	}

	private double noiseMag = 0;
	public void setNoiseMag(double mag) {
		noiseMag = mag;
	}
	
	private Matrix4d rotation = new Matrix4d();
	private Vector3d vtemp = new Vector3d();
	
	
	@Override
	public void getOrientation(Point3i p, double[] orientation) {

		Point3d pd = getTruePosition(p);
//		Point3d pd = VectorToolset.tuple3iToPoint3d(p);
//		pd.sub(localFrame.getTranslation());
		
//    	Vector3d result = new Vector3d();
		generator.generateSingleDirection(parameter, pd, vtemp);

		if (noiseMag > 1e-2) {
			vtemp.x += Math.cos(noiseMag * rand.nextGaussian()) * Math.sin(noiseMag * rand.nextGaussian());
			vtemp.y += Math.sin(noiseMag * rand.nextGaussian()) * Math.sin(noiseMag * rand.nextGaussian());
			vtemp.z += Math.sin(noiseMag * rand.nextGaussian());
			vtemp.normalize();
		}
		
		localFrame.getRotation(rotation).transform(vtemp);
		
		orientation[0] = vtemp.x;
		orientation[1] = vtemp.y;
		orientation[2] = vtemp.z;
	}

	private int[] axisSamples;
	private SamplingStyle samplingStyle;
	
	@Override
	public void precomputeCoordinateFrames(int numrots,
			int[] axisSamples, SamplingStyle samplingStyle) {
		this.axisSamples = axisSamples;
		this.samplingStyle = samplingStyle;
	}

	@Override
	public List<Point3i> getPointList() {
		
		List<Point3i> pts = new LinkedList<Point3i>();
		pts.add(MathToolset.tuple3dTo3i(localFrame.getTranslation()));
		
		return pts;
	}

	@Override
	public int[] getVolumeDimension() {
		Point3i o = MathToolset.tuple3dTo3i(localFrame.getTranslation());
		o.x += 2;
		o.y += 2;
		o.z += 2;
		
		return new int[] { o.x, o.y, o.z };
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
				
		return vfp.getPanel();
	}

	public void addParameterListener(ParameterListener listener) {
		// TODO Auto-generated method stub

	}

	public void display(final GLAutoDrawable drawable) {
		Point3i pcenter = MathToolset.tuple3dTo3i(localFrame.getTranslation());
		Point3i p = new Point3i();
		
		GL2 gl = drawable.getGL().getGL2();
		
		// Set the origin to the center
		OrientationFrame frame = HelicoidTracer.getHelicoidFrame(parameter, new Point3d());
    	CoordinateFrameSample sample = new CoordinateFrameSample(frame, localFrame.getTranslation());
    	sample.presampleInteger(axisSamples, samplingStyle, true);

    	for (Point3i pt : sample.getIntegerSampling()) {
    		p.add(pt, pcenter);
    		
    		CoordinateFrameSample[] frames = getCoordinateFrames(p);
    		frames[0].display(drawable);
    	}    	
    	
		gl.glColor4d(1, 1, 1, 0.7);
		gl.glPointSize(10f);
		gl.glBegin(GL2.GL_POINTS);
		Point3d t = localFrame.getTranslation();
    	for (Point3i pt : sample.getIntegerSampling()) {
    		gl.glVertex3d(pt.x + t.x, pt.y + t.y, pt.z + t.z);
    	}
		gl.glEnd();
	}

	private CoordinateFrame localFrame;
	public void setLocalFrame(CoordinateFrame frame) {
		this.localFrame = frame;
	}

	@Override
	public double[] getSeedPoint(Point3i p) {
		return new double[] { 0, 0, 0 };
	}

	public HelicoidParameter getParameter() {
		return parameter;
	}

	@Override
	public CoordinateFrame getCoordinateFrame(Point3i p, CoordinateFrame frame) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
