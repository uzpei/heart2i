package heart.test;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import helicoid.fitting.FittableRunner;
import helicoid.modeling.HelicoidGenerator;
import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameter.ParameterType;
import helicoid.parameter.HelicoidParameterNode;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.IntParameter;
import swing.parameters.IntParameterPoint3i;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.HelicoidField;
import tools.SimpleTimer;
import tools.frame.CoordinateFrame;

public class FittingSanity implements GLObject, Interactor {

	private BooleanParameter displayWorld = new BooleanParameter(
			"display world", false);

	private JoglRenderer ev;

	private FittableHelicoidField fittableField;

	private HelicoidGenerator generator;
	
	private BooleanParameter experimentEnabled = new BooleanParameter("experimental mode", false);

	private IntParameter numFitSamples = new IntParameter("num fit samples", 10, 1, 100000);
	private DoubleParameter gmag = new DoubleParameter("random magnitude", 0.5, 0, 2);
	
	private DoubleParameter noiseMag = new DoubleParameter("angular noise (deg)", 0, 0, 180);
	
	private SimpleTimer fitTimer = new SimpleTimer();

	private FittableRunner runner;
	
	private IntParameterPoint3i origin = new IntParameterPoint3i("origin", new Point3i(), new Point3i(), new Point3i(10, 10, 10));
	
	private DoubleParameterPoint3d rotation = new DoubleParameterPoint3d("rotation", new Point3d(), new Point3d(-180, -180, -180), new Point3d(180, 180, 180));
	
	public static void main(String[] args) {
		new FittingSanity();
	}

	public FittingSanity() {

		generator = new HelicoidGenerator();
		
		fittableField = new FittableHelicoidField(generator);
		runner = new FittableRunner(fittableField, true);

		Dimension winsize = new Dimension(1000, 1000);
		ev = new JoglRenderer("Fitting sanity");
		ev.getCamera().zoom(100);

		ev.getControlFrame().add("Generator", generator.getControls());
		
		ev.addInteractor(this);
		ev.start(this);
		ev.getRoom().setVisible(false);

		ev.getCamera().zoom(100f);
	}
	
	private int completed = 0;
	
	private String completionText = "";
	
	private boolean fitting = false;
	
	/**
	 * Fit the selected helicoid parameter (or all of them at the same time).
	 */
	private void fit() {

		System.out.println("Launching fit...");
		
		fitting = true;
		
		System.out.println("Error epsilon = "
				+ runner.getFittingError().getEpsilon());
		System.out.println("Gradient epsilon = "
				+ runner.getFittingError().getGradientEpsilon());

		System.out.println("Angular noise = " + noiseMag.getValue());
		
		completionText = "";
		
		if (experimentEnabled.getValue()) {
			completed = 0;
									
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					double error = 0;
					double time = 0;
					List<Point3i> pts = new LinkedList<Point3i>();
					
					// FIXME: change this to whatever we decide to use as center of fitting
					pts.add(origin.getPoint3i());
					
					List<HelicoidParameterNode> samples = new LinkedList<HelicoidParameterNode>();
					List<HelicoidParameterNode> culprits = new LinkedList<HelicoidParameterNode>();
					List<HelicoidParameterNode> errors = new LinkedList<HelicoidParameterNode>();

//					// Do this once to precompute runnable objects
//					parallelFitter.run(ParameterType.ALL, false);
//
//					while (parallelFitter.isRunning()) {
//						// Wait
//					}
					
					for (int i = 0; i < numFitSamples.getValue(); i++) {
						HelicoidParameter hp = randomize();

						fittableField.setParameter(hp);
						samples.add(new HelicoidParameterNode(hp));
						
						fitTimer.tick();
						runner.reset();
						List<HelicoidParameterNode> nodes = runner.run(ParameterType.ALL, pts);
						time += fitTimer.tick();
						
						// Update error
						double cerror = nodes.get(0).getError();
						errors.add(nodes.get(0));
						error += cerror;

						completed++;
					}

					completionText += "=================================================" + "\n";
					completionText += "Total fit time (including overhead) = " + time + "s" + "\n";
					completionText += "Total error for " + numFitSamples.getValue() + " samples = " + error + "\n";

					// Compute std and thresholds
					double meanError = error / numFitSamples.getValue();

					completionText += "Mean error = " + meanError + "\n";
					
					double std = 0;
					double dmax = 0;
					double dmin = Double.MAX_VALUE;
					for (HelicoidParameterNode node : errors) {
						double v = Math.pow(node.getError() - meanError, 2);
						if (v > dmax)
							dmax = v;
						if (v < dmin)
							dmin = v;
						
						std += v;
						
					}
					std /= numFitSamples.getValue();
										
					completionText += "Standard deviation = " + std + "\n";
					completionText += "Smallest deviation = " + dmin + "\n";
					completionText += "Largest deviation = " + dmax + "\n";
					
					double errorThreshold = meanError + std;

					completionText += "Listing nodes for which error > mean + std = " + errorThreshold + " ..." + "\n";
			        NumberFormat nf = NumberFormat.getNumberInstance();
			        nf.setMaximumFractionDigits(12);
			        nf.setMinimumFractionDigits(12);
			        
			        nf.setMaximumIntegerDigits(9);
			        nf.setMinimumIntegerDigits(1);

					int i = 0;
					for (HelicoidParameterNode node : errors) {
						if (node.getError() > errorThreshold) {
//							System.out.println("E = " + nf.format(node.getError()) + " | HP = " + node.toStringShort() + " | TRUTH = " + samples.get(i).toStringShort());
							completionText += "E = " + nf.format(node.getError()) + " | TRUTH = " + samples.get(i).toStringShort() + " | FIT = " + node.toStringShort() + "\n";
						}

						i++;
					}
					
					fitting = false;
				}
			}).start();
			
		}
		else {
			fitting = false;
		}
	}

	private HelicoidField bfield = new HelicoidField();
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();

		// Create the text renderer if necessary
		if (textRenderer == null) {
			Font font = new Font("Consolas", Font.PLAIN, 18);
			textRenderer = new JoglTextRenderer(font, true, false);
			textRenderer.setColor(0.5f, 0.9f, 0f, 1f);
		}

		if (!experimentEnabled.getValue()) {
			List<Point3i> pts = new LinkedList<Point3i>();
			pts.add(origin.getPoint3i());
			runner.reset();
			List<HelicoidParameterNode> nodes = runner.run(ParameterType.ALL, pts);
			
			if (nodes != null) {
				bfield.set(fittableField.getVolumeDimension(), nodes);
				
				String s = "";
				for (HelicoidParameterNode node : nodes) {
					s += "Target = " + generator.getParameter().toString();
					s += "\nResult = " + (new HelicoidParameter(node)).toString();

					node.add(-1, generator.getParameter());
					s += "\nHelicoid error = " + node.length();
					s += "\nMetric error = " + node.getError();
					s += "\n";
				}
				textRenderer.drawBottomLeft(s, drawable);
			}
		}
		else {
			if (fitting) {
				textRenderer.drawTopLeft("Sample " + completed + " \\ " + numFitSamples.getValue(), drawable);
			}
			else {
				textRenderer.drawTopLeft(completionText, drawable);
			}
			
			textRenderer.drawBottomLeft("Current parameter = " + fittableField.getParameter().toStringShort(), drawable);
		}
		
		Point3i center = origin.getPoint3i();
		Matrix4d full = new Matrix4d();
		full.setIdentity();
		Matrix4d r = new Matrix4d();
		// T
		r.setIdentity();
		r.setColumn(3, center.x, center.y, center.z, 1);
		full.mul(r);

		// X rotation
		r.set(new AxisAngle4d(new Vector3d(1, 0, 0), rotation.getPoint3d().x / 180d * Math.PI));
		full.mul(r);

		// Y rotation
		r.set(new AxisAngle4d(new Vector3d(0, 1, 0), rotation.getPoint3d().y / 180d * Math.PI));
		full.mul(r);

		// Z rotation
		r.set(new AxisAngle4d(new Vector3d(0, 0, 1), rotation.getPoint3d().z / 180d * Math.PI));
		full.mul(r);
		
//		// T-1
//		r.setIdentity();
//		r.setColumn(3, -center.x, -center.y, -center.z, 1);
//		full.mul(r);
		new CoordinateFrame(full).display(drawable);

		if (displayWorld.getValue())
			new CoordinateFrame().display(drawable, 5, true);
		
		fittableField.setLocalFrame(new CoordinateFrame(full));
		
		int ns = runner.getFittingError().getNeighborhoodFitSize();
		fittableField.precomputeCoordinateFrames(1, new int[] { ns, ns, ns }, runner.getFittingError().getSamplingStyle());
		fittableField.display(drawable);
				
	}

	private JoglTextRenderer textRenderer;
	
	private HelicoidParameter randomize() {
		Random rand = new Random();
		double rmag = gmag.getValue();
		HelicoidParameter hp = new HelicoidParameter();

		hp.KT = -rmag + 2 * rmag * rand.nextDouble();
		hp.KN = -rmag + 2 * rmag * rand.nextDouble();
		hp.KB = -rmag + 2 * rmag * rand.nextDouble();
		hp.alpha = 0;

		return hp;
	}

	@Override
	public JPanel getControls() {

		VerticalFlowPanel vfp = new VerticalFlowPanel();

		JButton btnGen = new JButton("randomize");
		btnGen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				generator.setParameter(randomize());
			}
		});
		vfp.add(btnGen);
		
		vfp.add(displayWorld.getControls());
		vfp.add(experimentEnabled.getControls());
		vfp.add(noiseMag.getSliderControls());
		noiseMag.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				fittableField.setNoiseMag(noiseMag.getValue() / 180.0 * Math.PI);
			}
		});
		
		vfp.add(numFitSamples.getSliderControls());
		vfp.add(gmag.getSliderControls());
		
		vfp.add(origin.getControls());
		vfp.add(rotation.getControls());
		
		vfp.add(runner.getControls());
//		vfp.add(parallelFitter.getControls());
		
		vfp.add(bfield.getControls());
		
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub

	}

	private boolean pickNextDown = false;

	private boolean shiftDown = false;

	private boolean altDown = false;

	@Override
	public void attach(Component component) {

		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {

				shiftDown = e.isShiftDown();
				altDown = e.isAltDown();

				if (e.getKeyCode() == KeyEvent.VK_N) {
					pickNextDown = false;
				}				
			}

			@Override
			public void keyPressed(KeyEvent e) {


				shiftDown = e.isShiftDown();
				altDown = e.isAltDown();

				if (e.getKeyCode() == KeyEvent.VK_F) {
					fit();
				}
				else if (e.getKeyCode() == KeyEvent.VK_G) {
					generator.setParameter(randomize());
				}
			}
			
		});
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		new FittingSanity();
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

}
