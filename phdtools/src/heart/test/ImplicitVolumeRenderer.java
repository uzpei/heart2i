
package heart.test;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import implicit.implicit3d.interpolation.ThinPlateSpline;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import volume.IntensityVolume;
import volume.VolumeRenderer;

public class ImplicitVolumeRenderer implements GLObject, Interactor {

	public static void main(String[] args) {		
		ImplicitVolumeRenderer renderer = new ImplicitVolumeRenderer();
	}
	
	private ThinPlateSpline thinPlate;
	private IntensityVolume volume;
	private VolumeRenderer renderer;
	public ImplicitVolumeRenderer() {
		thinPlate = new ThinPlateSpline();
			
		Dimension size = new Dimension(700, 700);

		renderer = new VolumeRenderer(size);

		recomputeVolume();

		JoglRenderer jr = new JoglRenderer("Volume Rendering", this, size, new Dimension(500, size.height));
		jr.start();
		jr.getCamera().zoom(-300f);
		jr.addInteractor(this);
	}
	
	private void injectDoubleMetaballConstraints() {
		addSphere(new Point3d(-1, 0, 0));
		addSphere(new Point3d(1, 0, 0));
	}
	
	private void injectHeartLikeConstraints() {
		double dt = Math.PI / 5;
		
		thinPlate.clear();
		
		Point3d p = new Point3d();
		Point3d ps = new Point3d();
		Vector3d normal = new Vector3d();
		int ind = 0;
		double rho1 = 0.7;
		double rho2 = 0.9;
		double rho3 = (rho1 + rho2) / 2;
		double rhocut = 0.8;
		
//		double coneAngle = 1;
		double coneAngle = 0.1 +  Math.PI - 2 * Math.asin(rhocut / rho2);
		
		// Constraints
		double cd = 0.05;
		List<Point3d> pts = new LinkedList<Point3d>();
		List<Vector3d> normals = new LinkedList<Vector3d>();		
		List<Integer> indices = new LinkedList<Integer>();
		
		for (double theta = 0; theta < 2 * Math.PI; theta += dt) {
			// Annulus
			p.x = rho3 * Math.cos(theta) * Math.sin(coneAngle);
			p.y = rho3 * Math.sin(theta) * Math.sin(coneAngle);
			p.z = rho3 * Math.cos(coneAngle);
			
			normal.set(0, 0, 1);
			
			pts.add(new Point3d(p));
			normals.add(new Vector3d(normal));
			indices.add(new Integer(ind));
			
//			thinPlate.addTriConstraint(p, normal, ind++, cd);
			
			for (double phi = 0; phi < 2 * Math.PI; phi += dt) {
				p.x = Math.cos(theta) * Math.sin(phi);
				p.y = Math.sin(theta) * Math.sin(phi);
				p.z = Math.cos(phi);
				
				if (p.z < rhocut * rho1) {
					normal.set(p);
					normal.negate();
					ps.set(p);
					ps.scale(rho1);
					
					pts.add(new Point3d(ps));
					normals.add(new Vector3d(normal));
					indices.add(new Integer(ind));

//					thinPlate.addTriConstraint(ps, normal, ind++, cd);
				}

				if (p.z < rhocut * rho2) {
					normal.set(p);
					ps.set(p);
					ps.scale(rho2);
//					thinPlate.addTriConstraint(ps, normal, ind++, cd);
					
					pts.add(new Point3d(ps));
					normals.add(new Vector3d(normal));
					indices.add(new Integer(ind));
				}

			}
		}
		
		thinPlate.addTriConstraints(pts, normals, indices, cd);
	}

	private void addSphere(Point3d center) {
		double dt = Math.PI / 3;
		Point3d p = new Point3d();
		Vector3d normal = new Vector3d();
		int ind = 0;
		for (double theta = 0; theta < Math.PI; theta += dt) {
			for (double phi = 0; phi < 2 * Math.PI; phi += dt) {
				p.x = Math.cos(theta) * Math.sin(phi);
				p.y = Math.sin(theta) * Math.sin(phi);
				p.z = Math.cos(phi);
				normal.set(p);
//				p.x -= 0.5;
//				p.y -= 0.5;
//				p.z -= 0.5;
				p.sub(center);
				
				thinPlate.addTriConstraint(p, normal, ind++, 0.05);
			}
		}
	}
	private void recomputeVolume() {
		thinPlate.clear();
		
		// Sample the unit sphere
//		injectDoubleMetaballConstraints();
		injectHeartLikeConstraints();
		
		thinPlate.interpolate();


		createVolume(thinPlate);
	}
	
	private void createVolume(ThinPlateSpline spline) {
		double volScale = 4;

		int volumeRes = 50;
		double ddim = volumeRes;
		volume = new IntensityVolume(volumeRes, volumeRes, volumeRes);
		double v;
		Point3d p = new Point3d();
		Point3d offset = new Point3d(-1, -1, -1);
		for (int i = 0; i < volumeRes; i++) {
			for (int j = 0; j < volumeRes; j++) {
				for (int k = 0; k < volumeRes; k++) {
					p.set(i / ddim, j / ddim, k / ddim);
					p.scale(volScale);
					p.x -= volScale / 2;
					p.y -= volScale / 2;
					p.z -= volScale / 2;
					v = spline.F(p);
//					System.out.println(v);
					
//					if (Math.abs(v) <= 0.5) {
					if (v < 0) {
//						v /= 10;
//						v = 0.5 * Math.signum(v);
						volume.set(i, j, k, v); 
					}
				}
			}
		}
		renderer.setVolume(volume);
	}
	
	@Override
	public String getName() {
		return "Implicit volume renderer";
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		renderer.init(drawable);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		WorldAxis.display(gl);
		
//		if (thinPlate.hasChanged()) {
//			computeSpline();
//		}
//		
		thinPlate.display(drawable);
		
		gl.glScaled(4, 4, 4);
		renderer.display(drawable);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(renderer.getControls());
		vfp.add(thinPlate.getControls());
		return vfp.getPanel();
	}

	@Override
	public void attach(Component component) {
        component.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_C)
                	recomputeVolume();
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });		
	}
}
