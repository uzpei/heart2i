package heart;

import gl.geometry.VertexArrayObject;
import gl.geometry.primitive.Cylinder;

import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import tools.frame.OrientationFrame;
import tools.geom.Polyline;

public class Myofiber {
	
	private VertexArrayObject vbo;
	
//	private List<Point3d> streamline;
	private Polyline streamline;

	private List<Cylinder> geometry;
	
	private static double rtodeg = 180 / Math.PI;
	
	private final static GLU glu = new GLU();

	private float[] color;
	
	private Vector3d v = new Vector3d();

	
	private boolean useVBO = false;

	private int skip = 1;
	
	public Myofiber(List<Point3d> streamline, int skip, float[] color) {
		set(streamline, skip, color);
	}
	
	public void set(List<Point3d> streamline, int skip, float[] color) {
//		this.streamline = streamline;
		this.streamline = new Polyline(streamline, true);
		
		this.color = color;
		
		this.skip = skip;
		
		Polyline.smooth(this.streamline.getPoints(), 10, 0.25);
		createGeometry(skip);
		
		if (useVBO) 
			createVBO(color);
	}
	
	private void createGeometry(int skip) {
//		geometry = Cylinder.extrudeToTriangleStrip(getStreamline(), CYLINDER_RES, CYLINDER_RADIUS);
		geometry = new LinkedList<Cylinder>();

//		CYLINDER_RES = 12;
		
//		System.out.println("Creating geometry from " + streamline.getCount() + " points.");

		if (streamline.getCount() == 0)
			return;
				
		Point3d p0 = streamline.getOrigin();
		int i = -1;
		Point3f pf = new Point3f();
		Vector3d l = new Vector3d();
		Vector3f lf = new Vector3f();
		float h;
		
		Cylinder previous = null;
		for (Point3d p : streamline) {
			i++;
			
			if (i == 0) {
				continue;
			}
			
			if (i % skip != 0) 
				continue;
			
			l.sub(p, p0);
			lf.set(l);
			h = lf.length();
			lf.normalize();
			pf.set(p0);
			
			Cylinder cylinder = new Cylinder(CYLINDER_RES, pf, lf, CYLINDER_RADIUS, h, previous);
			geometry.add(cylinder);
			
			p0 = p;
			previous = cylinder;
		}
	}
	
	private void createVBO(float[] color) {
		List<float[]> data = new LinkedList<float[]>();
		
		for (Cylinder c : geometry) {
			List<Point3f[]> vs = c.getGeometry();
			for (Point3f[] v : vs) {
				float[] vf;
				
				// Top
				vf = new float[6];
				
				// Position
				vf[0] = v[0].x;
				vf[1] = v[0].y;
				vf[2] = v[0].z;
				
				// Color
				vf[3] = color[0];
				vf[4] = color[1];
				vf[5] = color[2];

				data.add(vf);
				
				// Bottom
				vf = new float[6];
				
				// Position
				vf[0] = v[1].x;
				vf[1] = v[1].y;
				vf[2] = v[1].z;
				
				// Color
				vf[3] = color[0];
				vf[4] = color[1];
				vf[5] = color[2];
				
				data.add(vf);
			}
		}
		
		vbo = new VertexArrayObject(data);
	}
	
	public List<Point3d> getStreamline() {
		return streamline.getPoints();
	}
	
	public List<Cylinder> getGeometry() {
		return geometry;
	}

	public static float CYLINDER_RADIUS = 0.2f;
	public static int CYLINDER_RES = 12;
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, color, 0);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, color, 0);

		if (useVBO) {
			vbo.display(drawable);
		}
		else {
			gl.glBegin(GL2.GL_TRIANGLE_STRIP);
			for (Cylinder cylinder : geometry) {
				cylinder.displayList(gl);
			}
			gl.glEnd();
		}
	}
	
	public void setUseVBO(boolean b) {
		if (vbo == null) {
			createVBO(color);
		}
		
		useVBO = b;
	}
	
	public void applyLaplacianSmoothing() {
		// Use Laplacian smoothing (vertices set as average of neighbors)
//		streamline.smooth(s, numcycles, smoothm)
		Polyline.smooth(this.streamline.getPoints(), 10, 0.1);
		set(this.streamline.getPoints(), this.skip, this.color);
	}
}
