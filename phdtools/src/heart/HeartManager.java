package heart;

import heart.Heart.ImageType;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.XMLFilter;

import swing.component.VerticalFlowPanel;
import tools.TextToolset;
 
public class HeartManager extends JPanel { 
	private static final long serialVersionUID = -8010771341968956299L;
	private final static int DATA_ALIGN = JTextField.CENTER;
    private final static int HEADER_ALIGN = JTextField.CENTER;
    private final static int buttonsWidth = 80;
    private final static Dimension viewportSize = new Dimension(100, 100);

    private List<Heart> hearts = new LinkedList<Heart>();
    private Heart selectedHeart = null;
    private int selectedHeartRow = -1;
    private final static String DEFAULT_XML_FILENAME = "hearts.xml";
    private String XMLfilename;
    
    private MyTableModel mtm;
    private JTable table;
//    private HashMap<String, Integer> hmap = new HashMap<String, Integer>();

    public enum HeartSpec {
    	FILE, ID, TYPE, X, Y, Z, DX, DY, DZ, CC, EIG, USE
    };
    
    private JFileChooser fc = null;
    
//    private VerticalFlowPanel heartPanel;
    private JPanel heartPanel;

    public HeartManager() {
    	this(DEFAULT_XML_FILENAME, null, false, true);
    }
    public HeartManager(boolean selectDefaultHeart) {
    	this(DEFAULT_XML_FILENAME, null, selectDefaultHeart, true);
    }

    public HeartManager(boolean selectDefaultHeart, boolean useUI) {
    	this(DEFAULT_XML_FILENAME, null, selectDefaultHeart, useUI);
    }

    public HeartManager(String XMLfilename, boolean selectDefaultHeart) {
    	this(XMLfilename, null, selectDefaultHeart, true);
    }
    
    public HeartManager(String XMLfilename) {
    	this(XMLfilename, null, false, true);
    }

    public HeartManager(ActionListener heartSelectionListener) {
    	this(DEFAULT_XML_FILENAME, heartSelectionListener, false, true);
    }
    
    private boolean usingUI = false;
    
    /**
     * @param heartSelectionListener this selection listener will be triggered when the first heart is found.
     * @param selectDefaultHeart whether the first default heart to be found should be selected (when found). This triggers escalating UI updates so if the caller of this constructor wants to avoid UI operations, a value of <code>false</code> should be used.
     */
    public HeartManager(String XMLfilename, ActionListener heartSelectionListener, boolean selectDefaultHeart, boolean useUI) {
//    	super(new GridBagLayout());
    	super(new GridBagLayout());

    	if (XMLfilename == null)
    		XMLfilename = DEFAULT_XML_FILENAME;
    	
    	this.XMLfilename = XMLfilename;

    	
    	if (heartSelectionListener != null) {
    		addHeartSelectionListener(heartSelectionListener);
    	}   	
    	
    	if (useUI)
    		createUI();
    	
    	setMinimumSize(viewportSize);
//    	setPreferredSize(viewportSize);

		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Heart Manager", TitledBorder.LEFT, TitledBorder.CENTER));
        
        loadXML();

        if (selectDefaultHeart)
        	selectDefaultHeart();
    }
    
    /**
     * Try to select a default heart.
     */
    private void selectDefaultHeart() {
    	if (selectedHeart != null) {
//			System.out.println("Default heart found " + selectedHeart.getID());
			System.out.println("Default heart found:\n " + selectedHeart.toString());
		}
    	else if (selectedHeart == null && hearts.size() > 0) {
			selectedHeart = hearts.get(0);
			selectedHeartRow = 0;
		}

    	// At this point the current heart could still be null...
    	if (selectedHeart != null) {
    		// If the heart was found, select it.
			selectHeart(selectedHeartRow);
		}
    }
    
    private void createUI() {
        // Create objects
        mtm = new MyTableModel();
        mtm.setActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		processTableAction(e);
        	}
        });
        
        // Create UI
        JButton btnLoad = new JButton("Add");
        btnLoad.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				popupLoadHeart();
			}
		});

        JButton btnVerify = new JButton("Verify");
        btnVerify.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				verifySelectedHeart();
			}
		});

        JButton btnReload = new JButton("Reload");
        btnReload.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reloadSelectedHeart();
			}
		});

        JButton btnRemove = new JButton("Remove");
//        btnRemove.setEnabled(false);
        btnRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removeSelectedHeart();
			}
		});


        JButton btnPrint = new JButton("Print");
        btnPrint.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				printSelectedHeart();
			}
		});

        
        Dimension btnSize = new Dimension(buttonsWidth, btnLoad.getPreferredSize().height);
        btnLoad.setPreferredSize(btnSize);
        btnLoad.setMaximumSize(btnSize);
        
//        btnAdd.setPreferredSize(btnSize);
        btnVerify.setPreferredSize(btnSize);
        btnRemove.setPreferredSize(btnSize);
        btnPrint.setPreferredSize(btnSize);

        // Add table
    	GridBagConstraints c = new GridBagConstraints();
    	c.fill = GridBagConstraints.BOTH;
    	c.gridx = 0;
    	c.gridy = 0;
    	c.ipady = 0;
    	c.weightx = 1;
    	c.weighty = 0;
//    	c.gridwidth = 2;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
    	JScrollPane tablePane = createTable();
    	
//    	tablePane.setMinimumSize(viewportSize);
    	
    	tablePane.setPreferredSize(viewportSize);
//    	tablePane.setMaximumSize(viewportSize);
        add(tablePane, c);

        // Add buttons
//        VerticalFlowPanel vfpButtons = new VerticalFlowPanel();
        JPanel vfpButtons = new JPanel(new GridLayout(1,5));
        
    	c.weightx = 0;
    	c.weighty = 0;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
    	c.fill = GridBagConstraints.NONE;
    	c.gridx = 0;
    	c.gridy = 1;
//    	c.gridwidth = 2;
        add(vfpButtons,c);
        
//        heartPanel = new VerticalFlowPanel();
        heartPanel = new JPanel(new BorderLayout());
        
        c.anchor = GridBagConstraints.FIRST_LINE_START;
    	c.fill = GridBagConstraints.BOTH;
    	c.weightx = 1;
    	c.weighty = 1;
    	c.gridx = 0;
    	c.gridy = 2;
//    	c.gridwidth = 2;
        add(heartPanel,c);

        JPanel empty = new JPanel();
    	c.gridy = 3;
        c.weighty = 1;
        add(empty, c);

        vfpButtons.add(btnLoad);
        vfpButtons.add(btnReload);
//        vfpButtons.add(btnSave);
//        vfpButtons.add(btnAdd);
        vfpButtons.add(btnVerify);
        vfpButtons.add(btnRemove);
        vfpButtons.add(btnPrint);
        
        usingUI = true;
    }
    
    public void updateHeartUI() {
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Selected Heart: " + getCurrent().getID(), TitledBorder.LEFT, TitledBorder.CENTER));
		
    	heartPanel.removeAll();
		heartPanel.add(getCurrent().getControls());
//		heartPanel.update();
		heartPanel.invalidate();
		
		
//		validate();
//		invalidate();
		revalidate();
    }
    
    private List<ActionListener> heartSelectionListeners = new LinkedList<ActionListener>();

    private List<ActionListener> dataChangeListeners = new LinkedList<ActionListener>();

    
    public void addHeartSelectionListener(ActionListener listener) {
    	heartSelectionListeners.add(listener);
    }
    
    private void notifyHeartSelectionListeners(final int heartIndex) {
		new Thread(new Runnable() {
			@Override
			public void run() {
		    	for (ActionListener listener : heartSelectionListeners) {
		    		listener.actionPerformed(new ActionEvent(hearts.get(heartIndex), heartIndex, ""));
		    	}
			}
		}).start();
    }

    
	public void addHeartDataChangeListener(ActionListener listener) {
		dataChangeListeners.add(listener);
	}
	
	private void notifyDataChangeListeners() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (ActionListener listener : dataChangeListeners) {
					listener.actionPerformed(null);
				}
			}
		}).start();
	}

    private void reloadSelectedHeart() {
    	if (hearts.size() == 0) return;
    	
    	int row = table.getSelectedRow();
    	
    	if (row < 0) return;
    	
    	Heart heart = hearts.get(row);
    	
    	heart.readHeartDataFromFile(heart.getDirectory(), true);
    	
    	notifyHeartSelectionListeners(row);
    }

    /**
     * Print out stats for this heart. Current stats are:
     * center of mass, number of valid (unmasked) voxels, number of masked voxels
     */
    private void printSelectedHeart() {
    	System.out.println("Unmasked voxel count = " + selectedHeart.getMask().getUnmaskedCount());
       	System.out.println("Masked voxel count = " + selectedHeart.getMask().getMaskedCount());
    }
    
    private void removeSelectedHeart() {
    	if (hearts.size() == 0) return;
    	
    	int row = table.getSelectedRow();

    	if (row < 0) return;
    	
    	Heart heart = hearts.get(row);
    	hearts.remove(row);
    	mtm.removeRow(row);
    	saveXML();
    	
    	// Resume selected
    	if (row > 0 && selectedHeart == heart) {
    		selectedHeart = null;
    		selectedHeartRow = -1;
    		selectHeart(row - 1);
    	}
    }

    private void verifySelectedHeart() {
    	if (hearts.size() == 0) return;
    	
    	int row = table.getSelectedRow();
    	Heart heart = hearts.get(row);
    	
    	int[] dimension = heart.getDimension();
    	double[] voxelSize = heart.getVoxelSize();
    	ImageType type = heart.getImageType();
    	
    	System.out.println("[\t Verifying heart with id = " + heart.getID() + "\t]");

    	System.out.print("Verifying directory: " + heart.getDirectory());
    	File file = new File(heart.getDirectory());
    	if (!file.exists()) {
    		System.out.println("\t Failed! Directory does not exist.");
    		return;
    	}
    	else 
    		System.out.println("\t OK");
    		
    	heart.readHeartDataFromFile(heart.getDirectory(), false);
    	
    	System.out.print("Verifying dimensions: " + Arrays.toString(heart.getDimension()));
    	if (!Arrays.equals(heart.getDimension(), dimension)) {
    		System.out.println("\t Failed! Dimensions do not match.");
    	}
    	else
    		System.out.println("\t OK");

    	System.out.print("Verifying isotropy: " + Arrays.toString(heart.getVoxelSize()));
    	if (!Arrays.equals(heart.getVoxelSize(), voxelSize)) {
    		System.out.println("\t Failed! Isotropy does not match.");
    	}
    	else
    		System.out.println("\t OK");

    	System.out.print("Verifying image type: " + type);
    	if (!(type.ordinal() == heart.getImageType().ordinal())) {
    		System.out.println("\t Failed! Image type do not match.");
    	}
    	else
    		System.out.println("\t OK");

    }
	
	private String[] getDataAtRow(int row) {
		String[] data = new String[mtm.getColumnCount()];
		for (int i = 0; i < mtm.getColumnCount(); i++) {
			data[i] = String.valueOf(mtm.getValueAt(row, i)).toUpperCase();
		}
		return data;
	}
	
	/**
	 * Select the heart located at this row. Triggers a UI update on the heart's end.
	 * @param row
	 */
	private void selectHeart(int row) {
		
//		int currentIndex = hmap.get(selectionString);
		int heartSelectionColumnIndex = HeartSpec.USE.ordinal();
		
		// Previously selected
		int previouslySelectedRow = selectedHeartRow;
		Heart previouslySelectedHeart = selectedHeart;
		
		// New selection
		selectedHeartRow = row;
		selectedHeart = hearts.get(row);

		if (!usingUI) return;

		// Save holding state
		boolean isHolding = mtm.isHolding();
		
		// Uncheck previously selected heart if necessary
		if (previouslySelectedRow >= 0 && previouslySelectedHeart != null && previouslySelectedHeart != selectedHeart) {


			// Hold so that setValueAt does not trigger an update
			mtm.hold(true);
			mtm.setValueAt(false, previouslySelectedRow, heartSelectionColumnIndex);
			
			// Restore holding state
			mtm.hold(isHolding);
		}
		
		// Check new selected heart in selection column
		mtm.hold(true);
		mtm.setValueAt(true, selectedHeartRow, heartSelectionColumnIndex);

		// Restore holding state
		mtm.hold(isHolding);

		// Read data associated to this heart if necessary
		selectedHeart.prepareDataIfNecessary();

		// Update the heart's UI 
		updateHeartUI();

		notifyHeartSelectionListeners(row);
				
	}

    private void popupLoadHeart() {
    	if (fc == null) {
        	try {
				fc = new JFileChooser(new File(".").getCanonicalPath());
		        fc.setFileHidingEnabled(true);
		        fc.setAcceptAllFileFilterUsed(true);
		    	fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		    	if (selectedHeart != null)
		    		fc.setCurrentDirectory(new File(selectedHeart.getDirectory()));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
        int returnVal = fc.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

        	File currentFile = fc.getSelectedFile();
        	loadHeart(currentFile.getAbsolutePath(), true);
        }
    }

    /**
     * Create the table and header
     * @return
     */
	private JScrollPane createTable() {
		for (HeartSpec s : HeartSpec.values()) {
			mtm.addColumn(s.toString());
		}
        
        // Set header alignment
        table = new JTable(mtm);
        ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(HEADER_ALIGN);
		
//        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
 
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        
        return scrollPane;
    }
    
    private void addRowData(Heart heart) {
    	if (!usingUI) return;
    	
    	mtm.addRow(new Object[] { heart.getDirectory(), heart.getID(), heart.getImageType(), heart.getDimension()[0], heart.getDimension()[1], heart.getDimension()[2], heart.getVoxelSize()[0], heart.getVoxelSize()[1], heart.getVoxelSize()[2], heart.isUsingCylindricalConsistency(), heart.isUsingEigenvectorField(), false });

    	// Set data alignment
		int i = table.getModel().getRowCount() - 1;
			for (int j = 0; j < table.getModel().getColumnCount(); j++) {
				TableCellRenderer tcr = table.getCellRenderer(i, j);
				if (tcr instanceof DefaultTableCellRenderer) {
					 ((DefaultTableCellRenderer) tcr).setHorizontalAlignment(DATA_ALIGN);
				}
			}
	}


	private void saveXML() {
		String filename = "./" + XMLfilename;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// Create root element
			Document doc = docBuilder.newDocument();
			Element heartRoot = doc.createElement("hearts");
			doc.appendChild(heartRoot);

			// TODO: loop over all hearts in the table
			for (Heart heart : hearts) {
				
				Element eHeart = doc.createElement("heart");
				eHeart.setAttribute("path", heart.getDirectory());
				eHeart.setAttribute("default", String.valueOf(selectedHeart == heart));
				
				heartRoot.appendChild(eHeart);
			}
			
			// write the content into xml file
			TransformerFactory
			.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(new File(filename)));

//			System.out.println("Heart list saved!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load a stored XML file containing a list of available hearts to load.
	 */
	private void loadXML() {
		String filename = "./" + XMLfilename;

		hearts.clear();

		// First determine if we have a spec file ready
		File hXML = new File(filename);
		if (!hXML.exists()) {
			// Heart list file doesn't exist, create it
			System.out.println("Heart list file not found at " + filename);
			System.out.println("Creating it...");
			saveXML();
		}
		else {

			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(filename);
				doc.getDocumentElement().normalize();

//				System.out.println("Processing: "
//						+ doc.getDocumentElement().getNodeName());
				
				NodeList nList = doc.getElementsByTagName("heart");
//				System.out.println("-----------------------");

				System.out.println(TextToolset.box("HeartManager: reading in hearts from " + new File(filename).getName(), '*'));
				// Fetch heart nodes within xml
				for (int i = 0; i < nList.getLength(); i++) {
					Node node = nList.item(i);
					
					NamedNodeMap nnm = node.getAttributes();
					
					String path = nnm.getNamedItem("path").getNodeValue();
					Heart heart = new Heart(path);

					// Only add if no duplicates were found
					if (!alreadExists(heart)) {
						// Check if this heart should be set as default
						if (Boolean.parseBoolean(nnm.getNamedItem(
								"default").getNodeValue())) {
							selectedHeart = heart;
							selectedHeartRow = hearts.size();
						}
						
						hearts.add(heart);
					}
				}

				
				for (Heart heart :  hearts) {
					addRowData(heart);
				}

			} catch (Exception e) {
				System.err.println("Possibly malformed XML file... Creating a new one.");
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * Look for a duplicate of this heart.
	 * @param heart
	 * @return
	 */
	private boolean alreadExists(Heart heart) {
		return findHeart(heart) != null;
	}

	private Heart findHeart(Heart heart) {
		Heart foundHeart = null;
		for (Heart other : hearts) {
			if (other.getDirectory().equalsIgnoreCase(heart.getDirectory()) && other.getID().equalsIgnoreCase(heart.getID())) {
				return other;
			}
		}

		return null;
	}

  
	private class MyTableModel extends DefaultTableModel {

		private ActionListener actionListener;
		
		private boolean holding = false;
		
		public void hold(boolean h) {
			holding = h ;
		}
		
		public boolean isHolding() {
			return holding;
		}
		
		protected void setActionListener(ActionListener actionListener) {
			this.actionListener = actionListener;
		}
		
		@Override
		public boolean isCellEditable(int row, int col) {
			if (col == HeartSpec.FILE.ordinal()) {
				// It is editable but the changes won't take effect (the filename should never change)
				return true;
			} else if (col == HeartSpec.ID.ordinal() || col == HeartSpec.CC.ordinal() || col == HeartSpec.EIG.ordinal() || col == HeartSpec.USE.ordinal()) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			// System.out.println("Setting value at " + row + "," + col + " to "
			// + value + " (an instance of " + value.getClass() + ")");

//			// Don't do anything if this heart is the selected one and the user
//			// tries to unselect it
//			if (col == HeartSpec.USE.ordinal() && selectedHeartRow == row)
//				return;

			// Always make the change to the UI by calling super
			super.setValueAt(value, row, col);

			// If this action is blocked from notifying listeners, return
			if (isHolding())
				return;
			
			// Notify that this row has changed
			actionListener.actionPerformed(new ActionEvent(this, row,
						String.valueOf(col)));
		}
	}
	
	/**
	 * This will be triggered whenever the table data changes
	 * @param e
	 */
	private void processTableAction(ActionEvent e) {
		if (e.getSource() == mtm) {
			int row = e.getID();
			HeartSpec spec = HeartSpec.values()[Integer.parseInt(e.getActionCommand())];
			
			if (spec == HeartSpec.ID || spec == HeartSpec.CC || spec == HeartSpec.EIG || spec == HeartSpec.USE) {
				
				Heart heart = hearts.get(row);
				
				// Fetch data from table
				String[] data = getDataAtRow(row);
				
				// Update with new data
				heart.updateData(data);
				
				// Save new data
				heart.save();

				// Save the list
				// TODO: do this lazily
				saveXML(); 

				// If data changed in the heart selection column
				if (spec == HeartSpec.USE && (Boolean) mtm.getValueAt(row, HeartSpec.USE.ordinal())) {
					mtm.hold(true);
					selectHeart(row);
					mtm.hold(false);
				}
				
				// If the use cylindrical consistency boolean is false, need to reload the data
				if (spec == HeartSpec.CC) {
		    		// Need to reload
					heart.reloadData();
					
					if (selectedHeart == heart) {
						updateHeartUI();
						notifyHeartSelectionListeners(row);
					}
				}
				else {
					// Notify listeners
					if (spec != HeartSpec.USE) 
						notifyDataChangeListeners();
				}
			}
		}
	}

	public Heart getCurrent() {
		if (selectedHeart == null)
			return null;
		
		selectedHeart.prepareDataIfNecessary();
		return selectedHeart;
	}
	
	

	public List<Heart> getHearts() {
		return hearts;
	}

	public void processHearts(HeartProcessor processor) {
		System.out.println("Initializing processor... " + hearts.size() + " hearts found.");
		for (int i = 0; i < hearts.size(); i++) {
			Heart heart = hearts.get(i);
			
			System.out.println(TextToolset.box("Processing: " + heart.getDirectory(), '%'));

			heart.prepareDataIfNecessary();
			
			processor.process(heart);
		}
	}
	
	/**
	 * Interface for running batch jobs on every hearts.
	 * @author epiuze
	 */
	public interface HeartProcessor {
		public void process(Heart heart);
	}

	public Heart loadHeart(final String heartDirectory) {
		return loadHeart(heartDirectory, false);
	}

	public Heart loadHeart(HeartDefinition definition) {
		Heart heart = loadHeart(definition.filename, false);
		heart.setSpecies(definition.species);
		return heart;
	}

	/**
	 * Load a heart into this manager
	 * @param heartDirectory
	 */
	public Heart loadHeart(final String heartDirectory, boolean asynchronous) {
        System.out.println("Loading heart " + heartDirectory);
        
		if (asynchronous) {
	    	new Thread(new Runnable() {
				
				@Override
				public void run() {
					loadHeartPrivate(heartDirectory);
				}
			}).start();
			return null;
		}
		else {
			return loadHeartPrivate(heartDirectory);
		}
	}
	
	private Heart loadHeartPrivate(String heartPath) {
        Heart heart = new Heart(heartPath + "/");
        
		// Only add if no duplicates were found
        Heart duplicateHeart = findHeart(heart);
		if (heart != null && duplicateHeart != heart) {
            if (usingUI)
            	addRowData(heart);
            
            hearts.add(heart);
            
//            if (selectedHeart == null) {
            	selectedHeart = heart;
            	selectedHeartRow = hearts.size() - 1;
            	selectHeart(selectedHeartRow);
//            }
            
            saveXML();
    		return heart;
		}
		else {
			if (!heart.exists()) {
				System.err.println("No heart was found at this path.");
				return null;
			}
			else
				return duplicateHeart;
		}
	}

	  /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Heart List");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Create and set up the content pane.
        final HeartManager newContentPane = new HeartManager();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
 
        newContentPane.addHeartSelectionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Receiving selected heart: " + newContentPane.getCurrent().getDirectory());
				System.out.println("Frame field hash = " + newContentPane.getCurrent().getFrameField().hashCode());
			}
		});
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
 
    /**
     * Convert all hearts currently loaded to the target file type.
     * @param imageTo
     */
    public void convert(final ImageType imageTo) {
    	processHearts(new HeartProcessor() {
			@Override
			public void process(Heart heart) {
//				File directory = new File("./data/heart/rats/rat07052008/minc/");
				ImageType.convert(new File(heart.getDirectory()), heart.getImageType(), imageTo);
			}
		});
    }
    
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
                createAndShowGUI();
            }
        });
    }
}
