package heart;


import gl.geometry.GLGeometry;
import heart.HeartManager.HeartSpec;
import heart.geometry.HeartGeometry;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import math.matrix.Matrix3d;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import tools.SimpleTimer;
import tools.frame.OrientationFrame.FrameAxis;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.VolumeIO;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import app.pami2013.IterativeFilter;
import extension.matlab.MatlabIO;

public class Heart implements GLGeometry {
	
	public enum Species { Rat, Dog, Human, Unknown }
	
	private Species species = Species.Unknown;
	
	private String maskPath;
	
	public String getMaskPath() {
		return maskPath;
	}
	public Species getSpecies() {
		return species;
	}
	
	public void setSpecies(Species s) {
		species = s;
	}
	
	/**
	 * The order of this enum type is related to its importance when searcing for image types in a directory.
	 * @author epiuze
	 */
	public enum ImageType {
		MAT(".mat"), MINC(".mnc"), TXT(".txt");

		private String fileType;
		public String getFileType() {
			return fileType;
		}
		
		@Override
		public String toString() {
			return getFileType().substring(1, getFileType().length());
		}
		
		private ImageType(String fileType) {
			this.fileType = fileType;
		}
		
		/**
		 * Search this directory for available images of the type <code>typeFrom</code> and converts them to the type <code>typeTo</code>.
		 * @param directory
		 * @return the image type for which there are the greatest number of files.
		 */
		public static void convert(File directory, final ImageType typeFrom, ImageType typeTo) {
			int maxcount = 0;

			// Use an array to circumvent "final"
			final ImageType[] type = new ImageType[1];

			File[] filesFound = directory.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					return filename.endsWith(typeFrom.getFileType());
				}
			});
			
			// Search for a mask
//			String mask = searchForMask(directory.getAbsolutePath(), typeFrom, maskKeywords);
//			IntensityVolumeMask maskVol = null;
//			VoxelBox box = null;
//			if (mask != null) {
//				maskVol = new IntensityVolumeMask(mask);
//				box = new VoxelBox(maskVol);
//			}

			for (File file : filesFound) {
				System.out.println("File found: " + file.getAbsolutePath());
				IntensityVolume volume = null;
				String filename = file.getAbsolutePath();
				
				String dir = file.getParentFile().getParentFile().getAbsolutePath();
				
				// Remove extension from this file and put new one
				String name = file.getName();
				name = name.substring(0, name.length() - typeFrom.getFileType().length());
				name += typeTo.getFileType();
				
				// Converted filename
				String converted = dir + "/" + typeTo.toString() + "/" + name;
//				System.out.println("Loading from " + filename);
				
				// First load into a common placeholder
				volume = new IntensityVolume(filename);

//				if (box != null) {
//					// Apply mask and bounding box
//					maskVol.applyMask(volume);
//					box.cut(CuttingPlane.FIT);
//				
//					volume = volume.extract(box);
//				}
				
				// Then save to desired image type
				System.out.println("Saving to " + converted);
				switch (typeTo) {
				case MINC:
					throw new RuntimeException("Converting to MINC is not available at this time.");
				case TXT:
					volume.exportToAscii(converted);
					break;
				case MAT:
					MatlabIO.save(converted, volume);
					break;
				}
				
			}

		}
	}

    private final static String HEART_SPECS_FILENAME = "specs.xml";

    public final static String fileDistanceTransformEndocardium = "dt_endo";
    public final static String fileDistanceTransformEpicardium = "dt_epi";
    public final static String Tname = "e1";
    public final static String[] fileT = new String[] { Tname + "x", Tname + "y", Tname + "z" };
    public final static String[] fileN = new String[] { "v2x", "v2y", "v2z" };
    
    public final static String[] fileB = new String[] { "projnormx", "projnormy", "projnormz" };
    
//    private final static String[] fileB = new String[] { "e3x", "e3y", "e3z" };
    
    /**
     * List of keywords used when searching for a mask.
     * NOTE: then order is important. Keywords are returned as soon as they are found following
     * the order in this array. 
     */
    public final static String[] maskKeywords = new String[] { "mask", "myocardium", "binary", "b0", "a0"};

	private static final boolean DEFAULT_USE_CYLINDRICAL_CONSISTENCY = false;
	private final static boolean DEFAULT_USE_EIGENVECTOR_FIELD = false;
	
	private static final String DEFAULT_ID = "Unnamed";

	private static final double[] DEFAULT_SPACING = new double[] { 1, 1, 1 };
    
	public final static boolean verbose = false;
	
	private BooleanParameter drawCenterline = new BooleanParameter("draw centerline", false);
	protected HeartGeometry geometry;

	protected ImageType type = ImageType.MAT;
	
	protected int[] dimension;
    
    protected double[] voxelSize;
    
    protected IntensityVolumeMask mask;
    
    protected VoxelFrameField frameField;
    
    protected String ID;
    
    protected String directory;
    
    protected boolean useCylindricalConsistency;

    protected boolean useEigenvectorField;
    
    protected VoxelBox voxelBox;

    protected boolean dataLoaded = false;

    protected boolean dataLoading = false;

	private boolean exists = true;
	public boolean exists() {
		return exists;
	}

	private IntensityVolume dtEndo, dtEpi;
    
	/**
	 * Empty heart.
	 * FIXME: what a sad, sad thing.
	 * Data will be manually loaded.
	 */
    public Heart() {
    }

    public Heart(HeartDefinition def) {
    	this();
    	set(def);
		prepareDataIfNecessary();
    }

    public Heart(String directory, Species species, String ID, ImageType type, int[] dimension, double[] spacing, boolean useCylindricalConsistency, boolean useEigenvectorField) {
    	setSpecs(directory, species, ID, type, dimension, spacing, useCylindricalConsistency, useEigenvectorField);
    }
    
	public Heart(String dir) {
		set(Heart.loadHeartSpecs(dir, false));
	    
		prepareDataIfNecessary();
	}
	
	public Heart(Heart other) {
		this();
		
		// Set parameters
		set(other);
		
		// Deep-copy volumetric data
		this.mask = new IntensityVolumeMask(other.mask);
    	this.dtEndo = new IntensityVolume(other.dtEndo);
    	this.dtEpi = new IntensityVolume(other.dtEpi);
		this.voxelBox = new VoxelBox(other.voxelBox);
    	this.voxelBox.setMask(this.mask);
//    	this.voxelBox.cut(CuttingPlane.ALL);
    	this.voxelBox.cut(other.voxelBox.getCuttingPlane());

    	this.frameField = new VoxelFrameField(other.frameField, this.voxelBox);

    	if (mask != null) {
        	geometry = new HeartGeometry(getMask());
    	}
	}

	public void setSpecs(String directory, Species species, String ID, ImageType type, int[] dimension, double[] spacing, boolean useCylindricalConsistency, boolean useEigenvectorField) {
    	this.directory = directory;
    	this.species = species;
    	this.ID = ID;
    	this.type = type;
    	this.dimension = dimension;
    	this.voxelSize = spacing;
    	this.useCylindricalConsistency = useCylindricalConsistency;
    	this.useEigenvectorField = useEigenvectorField;
	}

	@Override
	public String toString() {
		String s = "";
		s += "Hearth directory = " + directory + "\n"; 
		s += "Species = " + species.toString() + "\n"; 
		s += "ID = " + ID + "\n"; 
		s += "type = " + type + "\n"; 
		s += "dimension = " + Arrays.toString(dimension) + "\n"; 
		s += "voxelSize = " + Arrays.toString(voxelSize) + "\n"; 
		s += "use cylindrical consistency = " + useCylindricalConsistency + "\n";
		s += "use eigenvector field = " + useEigenvectorField + "\n"; 

		return s;
	}
	
	private void set(Heart other) {
		setSpecs(other.directory,other.species, other.ID, other.type, other.dimension, other.voxelSize, other.useCylindricalConsistency, other.useEigenvectorField);
		
    	this.mask = other.mask;
    	this.voxelBox = other.voxelBox;
    	this.frameField = other.frameField;


    	if (mask != null) {
        	geometry = new HeartGeometry(getMask());
    	}
    	
    	if (mask != null && voxelBox != null && frameField != null) {
    		setDataLoaded();
    	}
	}

	public static Heart loadHeartSpecs(String dir, boolean forced) {
		Heart heart = null;
		// Try to load a heart specs XML file from this directory

		File hXML = new File(dir + HEART_SPECS_FILENAME);

		if (!hXML.exists()) {
			System.out.println("No specification file (" + HEART_SPECS_FILENAME + ") found at " + new File(dir).getAbsolutePath());
		}

		// Specs file doesn't exist, create it
		// Need to load all files... this process might be lengthy
		// but it only needs to be loaded once
		if (forced || !hXML.exists()) {
			System.out.println("Force-loading data to determine heart specifications...");
			heart = new Heart();
			heart.readHeartDataFromFile(dir);
//			createXML(dir, heart);
		}
		// If there exists a spec file at this location, preload its specs
		// The data will need to be loaded at a later time
		else {
			heart = preloadHeartFromXML(dir);
		} 

		return heart;
	}
	
	public void save() {
		System.out.println("Saving XML heart data to [" + getDirectory() + "]");
		Heart.createXML(getDirectory(), this);
	}

	/**
	 * Search this directory for available image types.
	 * @param directory
	 * @return the image type for which there are the greatest number of files.
	 */
	private static ImageType searchForImageType(String directory) {
		File dir = new File(directory);
		
		int maxcount = 0;
		
		// Use an array to circumvent final assignment
		final ImageType[] type = new ImageType[1];
		
		for (final ImageType imageType : ImageType.values()) {
			File[] filesFound = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					return filename.endsWith(imageType.getFileType());
				}
			});
			
			if (filesFound == null)
				continue;
			
			if (filesFound.length > maxcount) {
				maxcount = filesFound.length;
				type[0] = imageType;
			}
		}
	
		if (verbose) System.out.println("Found imagetype = " + type[0]);
		return type[0];
	}
	
	/**
	 * Search this directory for a mask using an array of keywords
	 * @param directory
	 * @return the image type for which there are the greatest number of files.
	 */
	private static String searchForMask(String directory, final ImageType type, final String[] keywords) {
		File dir = new File(directory);

		File[] filesFound = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String filename) {
				// Only look for image of the selected type
				if (!filename.endsWith(type.getFileType())) return false;
				
				// Try to match keywords
				for (String keyword : keywords) {
					if (filename.contains(keyword))
						return true;
				}

				return false;
			}
		});

		if (filesFound == null)
			return null;
		
		if (filesFound.length > 0) {
			// Preserve enum order in return matched file
			for (int j = 0; j < maskKeywords.length; j++) {
				for (int i = 0; i < filesFound.length; i++) {
					String mask = filesFound[i].getAbsolutePath();
					if (mask.contains(maskKeywords[j])) {
						if (verbose) System.out.println("Found mask = " + mask);
						return mask;
					}
				}
			}
			
		} else {
			System.err.println("Mask not found in directory " + directory);
		}

		return null;
	}

	public void prepareDataIfNecessary() {
		prepareDataIfNecessary(false);
	}
	
	/**
	 * Prepare data in the backend. 
	 */
	public void prepareDataIfNecessary(boolean asynchronous) {
		
		if (isDataLoaded() || isDataLoading()) 
			return;

		setDataLoading(true);

		if (asynchronous) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					readHeartDataFromFile(directory);
				}
			}).start();
		}
		else
			readHeartDataFromFile(directory);
	}
	
	public boolean isDataLoaded() {
		return dataLoaded;
	}

	public boolean isDataLoading() {
		return dataLoading;
	}

	private IterativeFilter smoothingFilter;

	public IterativeFilter getFilter() {
		return smoothingFilter;
	}
	
	public void setSmoothing(IterativeFilter smoothingFilter) {
		if (smoothingFilter == null || smoothingFilter.getIterations() <= 0 || smoothingFilter.getStandardDeviation() <= 1e-12)
			this.smoothingFilter = null;
		else
			this.smoothingFilter = smoothingFilter;
	}
	
	/**
	 * TODO: make sure that the vector fields are properly merged
	 * @return
	 */
	public VoxelVectorField computeWallNormals() {
		// Set paths
		String fdtEndo = directory + fileDistanceTransformEndocardium + type.getFileType();
		String fdtEpi = directory + fileDistanceTransformEpicardium + type.getFileType();

		// Attempt to load the distance transforms (endo and epicardium)
		this.dtEndo = new IntensityVolume(fdtEndo);
		this.dtEpi = new IntensityVolume(fdtEpi);
		
		// Compute the third frame vector from the gradient of the distance transform
		VoxelVectorField vEndo = new VoxelVectorField("", dtEndo.dxOut().asVolume(), dtEndo.dyOut().asVolume(), dtEndo.dzOut().asVolume());
		VoxelVectorField vEpi = new VoxelVectorField("", dtEpi.dxOut().asVolume(), dtEpi.dyOut().asVolume(), dtEpi.dzOut().asVolume());
		
		// Normalize
		vEndo.normalize(voxelBox);
		vEpi.normalize(voxelBox);
		
		// Average and store the result in the epicardium gradient
		VoxelVectorField normals = vEpi;
		normals.getVolumes()[0].add(vEndo.getVolumes()[0]);
		normals.getVolumes()[1].add(vEndo.getVolumes()[1]);
		normals.getVolumes()[2].add(vEndo.getVolumes()[2]);
		
		normals.normalize();
		
		// fix remaining normals towards centroid
		normals.enforceCenterAlignment(this);
		
		return normals;
	}
	
	public IntensityVolume[] getDistanceTransform() {
		return new IntensityVolume[] { dtEndo, dtEpi } ;
	}
	
	public void setDataLoaded() {
		dataLoaded  = true;
		dataLoading = false;
		
		updateControlPanel();
	}

	private void setDataLoading(boolean b) {
		dataLoading  = b;
	}

	private String extractTopLevelDirectory(String directory) {
		StringTokenizer tokenizer = new StringTokenizer(directory, "/");

		String token = DEFAULT_ID;
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
		}
		return token;
	}
	
	private static Heart preloadHeartFromXML(String directory) {
		String filename = directory + HEART_SPECS_FILENAME;
		System.out.println("Preloading heart from XML spec file: " + filename);
		Heart heart = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(filename);
			doc.getDocumentElement().normalize();

//			System.out.println("Processing: "
//					+ doc.getDocumentElement().getNodeName());
			
			// NodeList nList = doc.getElementsByTagName("heart");
//			System.out.println("-----------------------");

			Element eElement = doc.getDocumentElement();
			NamedNodeMap nnm = eElement.getAttributes();

			// Fetch nodes
			String ID = nnm.getNamedItem("id").getNodeValue();
			
			Species species = Species.valueOf(nnm.getNamedItem("species").getNodeValue());
			
			int dimx = Integer
					.parseInt(nnm.getNamedItem("dimx").getNodeValue());
			int dimy = Integer
					.parseInt(nnm.getNamedItem("dimy").getNodeValue());
			int dimz = Integer
					.parseInt(nnm.getNamedItem("dimz").getNodeValue());
			double dx = Double.parseDouble(nnm.getNamedItem("dx")
					.getNodeValue());
			double dy = Double.parseDouble(nnm.getNamedItem("dy")
					.getNodeValue());
			double dz = Double.parseDouble(nnm.getNamedItem("dz")
					.getNodeValue());
			boolean useCC = Boolean.parseBoolean(nnm.getNamedItem(
					"useCylindricalConsistency").getNodeValue());
			boolean useEigenvectorField = Boolean.parseBoolean(nnm.getNamedItem(
					"useEigenvectorField").getNodeValue());
			ImageType imageType = ImageType.valueOf(nnm.getNamedItem(
					"imageType").getNodeValue().toUpperCase());

			heart = new Heart(directory, species, ID, imageType, new int[] { dimx, dimy,
					dimz }, new double[] { dx, dy, dz }, useCC, useEigenvectorField);
		} catch (Exception e) {
			System.err.println("Malformed XML file... Deleting it and trying again.");
			e.printStackTrace();
			
			return loadHeartSpecs(directory, true);
		}
		
		return heart;
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = nlList.item(0);

		return nValue.getNodeValue();
	}

	private static void createXML(String directory, Heart heart) {
		String filename = directory + HEART_SPECS_FILENAME;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element heartRoot = doc.createElement("heart");
			doc.appendChild(heartRoot);

			heartRoot.setAttribute("id", heart.ID);
			heartRoot.setAttribute("species", String.valueOf(heart.species));
			heartRoot.setAttribute("dimx", String.valueOf(heart.dimension[0]));
			heartRoot.setAttribute("dimy", String.valueOf(heart.dimension[1]));
			heartRoot.setAttribute("dimz", String.valueOf(heart.dimension[2]));
			heartRoot.setAttribute("dx", String.valueOf(heart.voxelSize[0]));
			heartRoot.setAttribute("dy", String.valueOf(heart.voxelSize[1]));
			heartRoot.setAttribute("dz", String.valueOf(heart.voxelSize[2]));
			heartRoot.setAttribute("imageType", String.valueOf(heart.type));
			heartRoot.setAttribute("useCylindricalConsistency", String.valueOf(heart.isUsingCylindricalConsistency()));
			heartRoot.setAttribute("useEigenvectorField", String.valueOf(heart.useEigenvectorField));

			// write the content into xml file
			TransformerFactory
			.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(new File(filename)));

//			System.out.println("File saved!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

    public void setImageType(ImageType type) {
    	this.type = type;
    }
    
    private void setDirectory(String directory) {
    	this.directory = directory;
    }

	/**
	 * This should not be changed during runtime.
	 * @param dimension
	 */
    private void setDimension(int[] dimension) {
    	this.dimension = dimension;
    }
    
    private void setVoxelSize(double[] voxelSize) {
    	this.voxelSize = voxelSize;
    }
    
    public void setMask(IntensityVolumeMask mask) {
    	this.dimension = mask.getDimension();
    	this.mask = mask;
    }
    
    public void setVoxelFrameField(VoxelFrameField frameField) {
    	this.frameField = frameField;
    }
    
    public void setID(String ID) {
    	this.ID = ID;
    }
    
    public void setFilename(String directory) {
    	this.directory = directory;
    }
    
    public void useCylindricalConsistency(boolean b) {
    	useCylindricalConsistency = b;
    	
    	// Update vector field
    	if (b) {
    		if (!isDataLoaded()) {
    			prepareDataIfNecessary();
    		}
    		else {
    			enforceCylindricalConsistency();
    		}
    	}
    }
    
    private void enforceCylindricalConsistency() {
		VoxelFrameField.enforceCylindricalConsistency(getFrameField(), getVoxelBox(), FrameAxis.Z, getMask());
    }

    public void useEigenvectorField(boolean b) {
    	useEigenvectorField = b;
    }

    public VoxelFrameField getFrameField() {
    	if (frameField == null)
    		reloadData();
    	
    	return frameField;
    }
    
    public VoxelBox getVoxelBox() {
    	return voxelBox;
    }
    
    public String getDirectory() {
    	return directory;
    }
    
    public int[] getDimension() {
    	return dimension;
    }
    
    public double[] getVoxelSize() {
    	return voxelSize;
    }
    
    public String getID() {
    	return ID;
    }
    
    public boolean isUsingCylindricalConsistency() {
    	return useCylindricalConsistency;
    }

    public boolean isUsingEigenvectorField() {
    	return useEigenvectorField;
	}

    public ImageType getImageType() {
    	return type;
    }

    /**
     * Update the properties of this heart.
     * The order is assumed to be: filename, id, filetype, dimx, dimy, dimz, scalex, scaley, scalez, useCylindrical
     * @param data
     */
	public void updateData(String[] data) {
		setFilename(data[HeartSpec.FILE.ordinal()]);
		setID(data[HeartSpec.ID.ordinal()]);
		setImageType(ImageType.valueOf(data[HeartSpec.TYPE.ordinal()]));
		int[] dimension = new int[] { Integer.parseInt(data[HeartSpec.X.ordinal()]), Integer.parseInt(data[HeartSpec.Y.ordinal()]), Integer.parseInt(data[HeartSpec.Z.ordinal()]) };
		setDimension(dimension);
		double[] scale = new double[] { Double.parseDouble(data[HeartSpec.DX.ordinal()]), Double.parseDouble(data[HeartSpec.DY.ordinal()]), Double.parseDouble(data[HeartSpec.DZ.ordinal()]) };
		setVoxelSize(scale);
		useEigenvectorField(Boolean.parseBoolean(data[HeartSpec.EIG.ordinal()]));
		useCylindricalConsistency(Boolean.parseBoolean(data[HeartSpec.CC.ordinal()]));
	}

	public IntensityVolumeMask getMask() {
		return mask;
	}

	private final static String DEFAULT_EXTRACTED_DIRECTORY_NAME = "_extracted";
	/**
	 * Create a directory 'extracted' if it doesn't exist and store an extracted volume from this heart.
	 */
	public void saveExtraction() {
		String edirectory = getDirectory() + getID() + DEFAULT_EXTRACTED_DIRECTORY_NAME + "/";

		System.out.println("Extracting heart ID " + getID() + " to " + edirectory);
		
		File efile = new File(edirectory);
		if (efile.getParentFile().exists()) {
			// Save frame field (9 files)
			getFrameField().export(edirectory, getVoxelBox(), ImageType.MAT);

			// Save mask (1 file)
			IntensityVolume maskE = getMask().extract(getVoxelBox());
			MatlabIO.save(edirectory + maskKeywords[0] + ImageType.MAT.getFileType(), maskE, null);

		}
		else {
			System.out.println("Directory does not exist... Creating it and trying again.");
			// Create it and try again
			efile.getParentFile().mkdir();
			saveExtraction();
		}
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		if (!isDataLoaded())
			return;
		
        gl.glPushMatrix();
        getVoxelBox().applyHeartNormalization(gl);

		getFrameField().display(drawable);
		getVoxelBox().display(drawable);
		
//		mask.display(drawable);

		if (drawCenterline.getValue()) {
			if (geometry == null)
				geometry = new HeartGeometry(getMask());

			geometry.display(drawable);
		}

        gl.glPopMatrix();
	}

	private VerticalFlowPanel controlPanel = new VerticalFlowPanel();

	private void updateControlPanel() {
		if (controlPanel == null || getVoxelBox() == null || getFrameField() == null)
			return;
		
		if (isDataLoaded()) {
//			JLabel lblName = new JLabel("Current selected heart: " + getID());
//			lblName.setPreferredSize(new Dimension(lblName.getPreferredSize().width, 25));
//			vfp.add(lblName);
			controlPanel.add(drawCenterline.getControls());
			controlPanel.add(getVoxelBox().getControls());
			controlPanel.add(getFrameField().getControls());
			controlPanel.update();
		}
		else
		{
			controlPanel.removeAll();
		}
	}
	
	public JPanel getControls() {
		return controlPanel.getPanel();
	}

	public void reloadData() {
    	readHeartDataFromFile(getDirectory());
	}

	public void destroy() {
		frameField.destroy();
		voxelBox.destroy();
		dataLoaded = false;
	}

	public void applyMask(IntensityVolumeMask mask) {
		mask.applyMask(this.mask);
		getFrameField().applyMask(mask);
		getVoxelBox().updateVoxelList();
		getVoxelBox().computeVoxelListUpdate();
	}

	public void set(HeartDefinition definition) {
		setFilename(definition.filename);
		setID(definition.id);
		setSpecies(definition.species);
	}

	public IntensityVolumeMask loadMask() {
		// Find image type
		ImageType type = searchForImageType(directory);
		
		if (type == null) {
			String itv = "";
			for (ImageType it : ImageType.values())
				itv += it + "    ";
					
			exists = false;
			
			throw new RuntimeException("Invalid directory: no heart data was found of the following available filenames: " + itv);
		}
		
		// Find mask
		String maskPath = searchForMask(directory, type, maskKeywords);

		mask = new IntensityVolumeMask(new IntensityVolume(maskPath));

		return getMask();
	}

	public HeartDefinition getDefinition() {
		return new HeartDefinition(getID(), getSpecies(), getDirectory());
	}

	public HeartGeometry getGeometry() {
		if (geometry == null) {
			geometry = new HeartGeometry(getMask());
		}
		return geometry;
	}

	public IntensityVolume getDistanceTransformEpi() {
		return dtEpi;
	}
	public IntensityVolume getDistanceTransformEndo() {
		return dtEndo;
	}

	public static Heart randomize(int[] dims) {
		Heart heart = new Heart();
		heart.dimension = dims;
		heart.dataLoaded = true;
		
		IntensityVolumeMask mask = new IntensityVolumeMask(dims);
		mask.fillWith(1);
		heart.setMask(mask);

		heart.frameField = VoxelFrameField.randomize(dims);
		heart.frameField.setMask(mask);
		heart.voxelBox = new VoxelBox(dims);
		heart.voxelBox.setMask(mask);
		return heart;
	}
	public void setFrameField(VoxelFrameField frameField) {
		this.frameField = frameField;
	}
	
	public void setVoxelBox(VoxelBox box) {
		this.voxelBox = box;
	}
	
	public String getSerializationFilename() {
		return getDirectory() + "volumes.ser";
	}
	
	public void serialize() {
		String filename = getSerializationFilename();
		System.out.println("Serializing to " + filename);
		List<Matrix3d> volumes = new LinkedList<Matrix3d>();
		volumes.add(new Matrix3d(mask.getData()));
		volumes.add(new Matrix3d(dtEndo.getData()));
		volumes.add(new Matrix3d(dtEpi.getData()));
		
		// Only need first two axes, compute third by cross-product
		for (int axis = 0; axis < 2; axis++) {
			for (int component = 0; component < 3; component++)
				volumes.add(new Matrix3d(getFrameField().getField(axis).getVolumes()[component].getData()));
		}
		
		VolumeIO.serialize(volumes, filename);
	}
	
	public void deserialize() {
		System.out.println("Deserializing heart from [" + getSerializationFilename() + "]...");

		List<Matrix3d> volumes = VolumeIO.deserialize(getSerializationFilename());
		int vid = 0;
		mask = new IntensityVolumeMask(volumes.get(vid++));
		
		dimension = mask.getDimension();
		
		dtEndo = new IntensityVolume(volumes.get(vid++));
		dtEpi = new IntensityVolume(volumes.get(vid++));
		
//		VoxelFrameField vf = new VoxelFrameField(mask.getDimension());
		VoxelVectorField[] v = new VoxelVectorField[3];
		
		// Only load two axes, third is obtained by cross product
		for (int axis = 0; axis < 2; axis++) {
			IntensityVolume[] vi = new IntensityVolume[3];
			int vic = 0;
			for (int component = 0; component < 3; component++) {
				vi[vic++] = new IntensityVolume(volumes.get(vid++));
			}

			v[axis] = new VoxelVectorField("f" + axis, vi[0], vi[1], vi[2]);
		}
		v[2] = VoxelVectorField.cross(v[0], v[1]);
		
		frameField = new VoxelFrameField(v[0], v[1], v[2]);
		
		voxelBox = new VoxelBox(mask);
		frameField.setVoxelBox(voxelBox);
		frameField.setMask(mask);
		
	}
	
	private boolean needsSmoothing() {
		return smoothingFilter != null && smoothingFilter.getIterations() > 0
				&& smoothingFilter.getStandardDeviation() > 0;
	}
	
	private VoxelFrameField fieldFromFiberDirections(VoxelVectorField f1, VoxelBox voxelBox) {
//		System.out.println("Applying smoothing...");
//		VoxelVectorField f1 = smoothingFilter.apply(directory, maskPath, Tname);
		VoxelVectorField normals = computeWallNormals();
		
		// Orthogonalize with respect to fiber orientation
		normals.orthogonalize(f1, voxelBox);

		// Create second frame axis (as cross product of F1 and F2)
		VoxelVectorField f2 = VoxelVectorField.cross(normals, f1);

		f1.setVoxelBox(voxelBox);
		f2.setVoxelBox(voxelBox);
		normals.setVoxelBox(voxelBox);

		// Create the frame field
		return new VoxelFrameField(f1, f2, normals, voxelBox.getMask());
	}

	/**
	 * @param directory
	 */
	public void readHeartDataFromFile(String directory) {
		setDataLoading(true);

		// Set this heart's directory
		setDirectory(directory);

		// Set voxel sizing and search for mask
		voxelSize = DEFAULT_SPACING;
		type = searchForImageType(directory);
		maskPath = searchForMask(directory, type, maskKeywords);
		useCylindricalConsistency = DEFAULT_USE_CYLINDRICAL_CONSISTENCY;
		useEigenvectorField = DEFAULT_USE_EIGENVECTOR_FIELD;

		SimpleTimer timer = new SimpleTimer();
		
		// First check if this heart has already beeen serialized
		// If it's the case we don't need to load any data
		if (new File(getSerializationFilename()).exists()) {
			deserialize();
			
			if (needsSmoothing()) {
				System.out.println("Applying smoothing...");
				VoxelVectorField f1 = smoothingFilter.apply(directory, maskPath, Tname);
				frameField = fieldFromFiberDirections(f1, voxelBox);
			}
			
			setDataLoaded();
		} 
		else {
			VoxelVectorField f1 = null, f2 = null;
			IntensityVolumeMask maskVol = null;

			System.out.println("---> Reading in data from path = "
					+ getDirectory());
			// if (getID() != null) System.out.println("Heart ID " + getID());

			if (type == null) {
				String itv = "";
				for (ImageType it : ImageType.values())
					itv += it + "    ";

				exists = false;

				throw new RuntimeException(
						"Invalid directory: no heart data was found of the following available filenames: "
								+ itv);
			}

			// Set paths
			String fdtEndo = directory + fileDistanceTransformEndocardium
					+ type.getFileType();
			String fdtEpi = directory + fileDistanceTransformEpicardium
					+ type.getFileType();

			// Load principal direction of diffusion
			if (needsSmoothing()) {
				System.out.println("Applying smoothing...");
				f1 = smoothingFilter.apply(directory, maskPath, Tname);
				System.out.println("Loading images in the back end from "
						+ new File(maskPath).getName() + ", "
						+ new File(fdtEndo).getName() + ", "
						+ new File(fdtEpi).getName() + "...");
			} else {
				String fex = directory + fileT[0] + type.getFileType();
				String fey = directory + fileT[1] + type.getFileType();
				String fez = directory + fileT[2] + type.getFileType();

				// Load fiber orientation volume
				IntensityVolume Tx = new IntensityVolume(fex);
				IntensityVolume Ty = new IntensityVolume(fey);
				IntensityVolume Tz = new IntensityVolume(fez);
				f1 = new VoxelVectorField("", Tx, Ty, Tz);

				System.out.println("Creating images in the back end from "
						+ new File(fex).getName() + ", "
						+ new File(fey).getName() + ", "
						+ new File(fez).getName() + ", "
						+ new File(maskPath).getName() + ", "
						+ new File(fdtEndo).getName() + ", "
						+ new File(fdtEpi).getName() + "...");
			}

			if (verbose)
				System.out
						.println("Input format does not specify a voxel size. Using default isotropic = 1x1x1 mm");

			IntensityVolume.verbose = verbose;

			if (maskPath != null)
				maskVol = new IntensityVolumeMask(new IntensityVolume(maskPath,
						new Float(0)));

			dimension = maskVol.getDimension();

			// Create the voxel box if necessary
			if (this.voxelBox != null) {
				// do nothing, preserve voxel box
			} else {
				voxelBox = new VoxelBox(dimension);
			}

			voxelBox.setMask(maskVol);

//			VoxelVectorField normals = computeWallNormals();
//
//			// Orthogonalize with respect to fiber orientation
//			normals.orthogonalize(f1, voxelBox);
//
//			// Create second frame axis (as cross product of F1 and F2)
//			f2 = VoxelVectorField.cross(normals, f1);
//
//			f1.setVoxelBox(voxelBox);
//			f2.setVoxelBox(voxelBox);
//			normals.setVoxelBox(voxelBox);
//
//			// Create the frame field
//			VoxelFrameField frameField = new VoxelFrameField(f1, f2, normals,
//					maskVol);
			this.mask = maskVol;
			
			VoxelFrameField frameField = fieldFromFiberDirections(f1, voxelBox);

			if (this.frameField != null) {
				this.frameField.set(frameField);
			} else
				this.frameField = frameField;

			setDataLoaded();
			
			// When we are done, compute cylindrical consistency
			// This needs to be done after dataLoaded is set otherwise we'll get
			// stuck in an infinite loop
			if (isUsingCylindricalConsistency()) {
				useCylindricalConsistency(true);
			}
			
			serialize();
		}

		this.mask.setName("mask");
		this.dtEndo.setName("DT endo");
		this.dtEpi.setName("DT epi");

		System.out.println("Total load time = " + timer.tick_ms());


		// Save
		save();
		System.out.println("XML save time = " + timer.tick_ms());
		
		// Notify callbacks
		for (ActionListener callback : loadingCallbacks) {
			callback.actionPerformed(new ActionEvent(this, 0, getID()));
		}
	}

	public static void main(String[] args) {
//		ImageType.convert(new File("./data/heart/rats/rat07052008/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat17122007/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat21012008/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat22012008/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat24012008/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat24022008_17/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat27022008/minc/"), ImageType.MINC, ImageType.MAT);
//		ImageType.convert(new File("./data/heart/rats/rat28012008/minc/"), ImageType.MINC, ImageType.MAT);
//		HeartManager manager = new HeartManager("heartsMinc.xml");
//		manager.convert(ImageType.MAT);
		
		SimpleTimer timer = new SimpleTimer();
		
		timer.tick();
		HeartDefinition def = new HeartDefinition("ratsample", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/");
		Heart heart = new Heart(def);
		heart.prepareDataIfNecessary();
//		System.out.println("Mat loading time = " + timer.tick_s());
//		heart.serialize();
//		System.out.println("Serialization time = " + timer.tick_s());
//		heart.deserialize();
//		System.out.println("Deserialization time = " + timer.tick_s());
	}
	private List<ActionListener> loadingCallbacks = new LinkedList<ActionListener>();
	
	public void addDataLoadedCallback(ActionListener callback) {
		loadingCallbacks.add(callback);
	}
	@Override
	public void init(GLAutoDrawable drawable) {
		// Nothing to do for now
	}
	
	@Override
	public String getName() {
		return getID();
	}


}
