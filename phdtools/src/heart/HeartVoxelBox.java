package heart;

import gl.geometry.WorldAxis;
import gl.renderer.JoglTextRenderer;

import java.awt.Font;

import javax.media.opengl.GLAutoDrawable;

import voxel.VoxelBox;

/**
 * TODO: move stuff from superclass here (e.g. cuttingplane methods)
 * @author piuze
 *
 */
public class HeartVoxelBox extends VoxelBox {
	
	private Font font = new Font("Consolas", Font.PLAIN, 20);
	private JoglTextRenderer renderer = new JoglTextRenderer(font, true, false);

	public HeartVoxelBox(VoxelBox other) {
		super(other);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		
		/*
		 * TODO: draw long-axis, short-axis, left, right
		 */
		renderer.setColor(0.1f, 0.1f, 0.1f, 1f);
		
		// TODO: applying voxel side rotation and translation, then
		// renderer.draw3D("long axis", px, py, pz, size);
	}
}
