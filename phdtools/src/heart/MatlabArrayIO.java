package heart;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.vecmath.Point3i;

import tools.loader.LineReader;
import volume.IntensityVolume;

public class MatlabArrayIO {

	public static IntensityVolume load(String filename) {
		
//		filename = "./data/rat/jon/test.txt";
		
		File file = new File(filename);
		IntensityVolume volume = null;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Loading seed points from " + filename + "...");
		try {
			LineReader reader = new LineReader(file);

			// First read full volume dimension
			String dimline = reader.readLine();
			StringTokenizer tokenizer = new StringTokenizer(dimline, "x");
			Point3i vdim = new Point3i();
			vdim.x = Integer.parseInt(tokenizer.nextToken());
			vdim.y = Integer.parseInt(tokenizer.nextToken());
			vdim.z = Integer.parseInt(tokenizer.nextToken());

			// Then read true span
			dimline = reader.readLine();
			tokenizer = new StringTokenizer(dimline, "x");
			Point3i vspan = new Point3i();
			vspan.x = Integer.parseInt(tokenizer.nextToken());
			vspan.y = Integer.parseInt(tokenizer.nextToken());
			vspan.z = Integer.parseInt(tokenizer.nextToken());
			
			// Then read offset
			dimline = reader.readLine();
			tokenizer = new StringTokenizer(dimline, "x");
			Point3i voffset = new Point3i();
			voffset.x = Integer.parseInt(tokenizer.nextToken());
			voffset.y = Integer.parseInt(tokenizer.nextToken());
			voffset.z = Integer.parseInt(tokenizer.nextToken());
			voffset.scale(0);

			double[][][] data = new double[vdim.x][vdim.y][vdim.z];
			System.out.println("Volume dimension = " + vdim);
			System.out.println("Volume span = " + vspan);
			System.out.println("Volume offset = " + voffset);
			
			String line;
			int linecount = 0;
			int x = 0;
			int y = 0;
			int z = 0;
			double sum = 0;
			int vcount = 0;
			int abscount = 0;
			int xmax = 0;
			int ymax = 0;
			int zmax = 0;
			
			while ((line = reader.readLine()) != null) {
				
				tokenizer = new StringTokenizer(line, ",");
				
				// Read the current line
				int cellIndex = 0;
				while (tokenizer.hasMoreTokens()) {
					
					double currentCell = Double.parseDouble(tokenizer.nextToken());

					// x is just the linecount
					x = linecount + voffset.x;
					
					// y is counted a modulo dy
					y = cellIndex % vspan.y + voffset.y;
					
					// Third dimension (z) are counted as multiples of dy
					z = (int) Math.floor(cellIndex / vspan.y) + voffset.z;
					
//					System.out.println("[" + x + "," + y + "," + z + "] = " + data[x][y][z]);

					// Set to zero if NaN is detected
					if (Double.isNaN(currentCell)) {
//						currentCell = 0;
					}
					else {
						sum += currentCell;
						vcount++;
					}

					data[x][y][z] = currentCell;
					abscount++;
					
//					if (abscount % 10 == 0) scanner.nextLine();
					
					if (x > xmax) xmax = x;
					if (y > ymax) ymax = y;
					if (z > zmax) zmax = z;
					
					// Increase element index in the current line
					cellIndex++;
				}
				
				// Increase x index
				linecount++;
			}

			volume = new IntensityVolume(data);
			
			System.out.println("Max coordinate found = " + xmax + "," + ymax + "," + zmax);
			System.out.println(abscount + " total elements found.");

			System.out.println("Volume (" + vcount + ") average = " + sum / vcount);
			
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return volume;
	}
	
	public static void main(String[] args) {
		IntensityVolume kbv = MatlabArrayIO.load("./data/rat/jon/KB.txt");
		System.out.println(kbv.getMagnitudeAverage());
	}
}
