package heart;

import extension.matlab.MatlabIO;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import heart.HeartManager.HeartProcessor;
import helicoid.fitting.FittableOrientationImage;
import helicoid.fitting.parallel.ParallelFitter;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import tools.TextToolset;
import volume.IntensityVolume;
import volume.PerVoxelMethod;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelVectorField;
import app.pami2013.cartan.CartanFieldVisualizer;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;

public class HeartFitter implements GLObject, Interactor {

	private HeartManager heartManager;

	private final static String DEFAULT_FITPATH = "helicoidfit/";
    private final static int DEFAULT_PROCS = 4;
    private final static int DEFAULT_NEIGHBOR_ERROR_SIZE = 3;
    private final static int DEFAULT_NEIGHBOR_FIT_SIZE = 3;
    private final static int DEFAULT_NUMITS = 100;
    
	private final static boolean DEFAULT_USEVIS = true;
	private final static CuttingPlane DEFAULT_CUTPLANE = CuttingPlane.TRANSVERSE;
	
	private boolean useVisualization = false;
	
    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", false);

    private BooleanParameter displayInfo = new BooleanParameter("Display info", true);

    private BooleanParameter displayCenterline = new BooleanParameter("Display centerline", false);

    private BooleanParameter launchHistograms = new BooleanParameter("launch histograms", false);
    
    private JoglRenderer ev;
    
    private ParallelFitter parallelFitter;

	private List<CartanParameterNode> nodes;

    private FittableOrientationImage image;
    
    private boolean requestFittingTermination = false;
    
    private CartanFieldVisualizer hfield;

	private int neighborFitSize;

	private int neighborErrorSize;

	private int numIts;

	public static void main(String[] args) {
		
		if (args.length > 0) {
			System.out.println(TextToolset.box("Heart helicoid fitting: parsing command-line arguments...", '*'));
			System.out.println("HeartFitter usage: java -Xmx2048m -jar hearfitter.jar HeartXML_OR_HeartFile neighborFitSize neighborErrorSize numProcessors numIterations CuttingPlane UseVisualization");
			System.out.println("HeartDirectory should point to a directory containing 7 files. Three files (the extension can be .MINC, .MAT, or .DAT) named \'e1x\', \'e1y\', \'e1z\' for the tangent direction, three files named \'projnormx\', \'projnormy\', \'projnormz\' for the normal direction to the wall and lastly a mask file named \'mask\'");
			System.out.println();
			System.out.println("Fetching parameters passed (" + args.length + ") :");
			for (String string : args) {
				System.out.println(string);
			}
		}
		
		// Set defaults
		int numprocs = DEFAULT_PROCS;
		int neighborErrorSize = DEFAULT_NEIGHBOR_ERROR_SIZE;
		int neighborFitSize = DEFAULT_NEIGHBOR_FIT_SIZE;
		boolean useVisualization = DEFAULT_USEVIS;
		CuttingPlane cuttingPlane = DEFAULT_CUTPLANE;
		int numIts = DEFAULT_NUMITS;

		// Parse parameters
		int i = 0;

		String filename = null;
		
		// Heart XML of filename
		if (args.length > i)
			filename = args[i++];

		// Neighborhood size
		if (args.length > i)
			neighborFitSize = Integer.parseInt(args[i++]);

		// Neighborhood size
		if (args.length > i)
			neighborErrorSize = Integer.parseInt(args[i++]);

		// Num processors
		if (args.length > i)
			numprocs = Integer.parseInt(args[i++]);

		// Num processors
		if (args.length > i)
			numIts = Integer.parseInt(args[i++]);

		// Cutting plane
		if (args.length > i) {
			try {
				cuttingPlane = Enum.valueOf(CuttingPlane.class, args[i++]);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out
						.println("Invalid cutting plane (" + args[i-1] + "). Please use one of the following: ");
				for (CuttingPlane c : CuttingPlane.values()) {
					System.out.println(c.toString());
				}
			}

		}
		
		// Use visualization
		if (args.length > i)
			useVisualization = Boolean.parseBoolean(args[i++]);
		
//		// Completing filenames if necessary
//		if (!filename.endsWith("/"))
//			filename = filename + '/';

		// Create the fitter
		GLViewerConfiguration config = null;
		
		if (useVisualization) {
			config = new GLViewerConfiguration();
		}
		new HeartFitter(config, filename, neighborFitSize, neighborErrorSize, numprocs, numIts, useVisualization, cuttingPlane);
    }

    public HeartFitter(GLViewerConfiguration config, String heartDirectory, int neighborFitSize, int neighborErrorSize, int numprocs, int numIts, boolean useVisualization, CuttingPlane cuttingPlane) {

    	this.useVisualization = useVisualization;
    	this.neighborErrorSize = neighborErrorSize;
    	this.neighborFitSize = neighborFitSize;
    	
    	this.numIts = numIts;
    	
    	image = new FittableOrientationImage();
    	
    	hfield = new CartanFieldVisualizer();

    	parallelFitter = new ParallelFitter();
    	parallelFitter.setNumthreads(numprocs);
    	parallelFitter.getFittingError().setIterations(numIts);

		// Determine the type of filename (XML or single heart)
		if (heartDirectory == null || heartDirectory.endsWith("xml")) {
        	heartManager = new HeartManager(heartDirectory, useVisualization);
		}
		else {
			heartManager = new HeartManager();
			heartManager.loadHeart(heartDirectory);
		}
		
    	if (useVisualization) {

            ev = new JoglRenderer("Heart Visualization & Fitting Toolbox");
    		
    		heartManager.addHeartSelectionListener(new ActionListener() {
        		@Override
        		public void actionPerformed(ActionEvent e) {
        			load();
        		}
        	});

            hfield.createRenderer(ev);
        	load();

            ev.controlFrame.add("Helicoid Field", getFieldControls());
            ev.controlFrame.add("Display", getSideControls());
            ev.controlFrame.add("Fitting", parallelFitter.getControls());

            ev.addInteractor(this);
            ev.start(this);
            ev.getRoom().setVisible(false);
            ev.getCamera().zoom(-500f);
    	}
    	else {
    		// We are not using visualization, let 
    		// the heart manager do the job and process its loaded hearts.
    		launchAllHearts(heartManager);
    	}
    }
    
    private boolean protectedFitting = false;
    
    private void launchAllHearts(final HeartManager heartManager) {
		new Thread(new Runnable() {
			@Override
			public void run() {

				protectedFitting = true;
				
				// Try to preserve current voxel box shape
				// TODO: maybe this should always be "fit"?
//				final CuttingPlane cplane = image.getVoxelBox().getCuttingPlane();
				final CuttingPlane cplane = CuttingPlane.FIT;
				
				heartManager.processHearts(new HeartProcessor() {
					@Override
					public void process(Heart heart) {
						if (heart == null) return;

						heart.getVoxelBox().cut(cplane);
						image.set(heart);
				    	
						parallelFitter.setFitNeighborhoodSize(neighborFitSize);
						parallelFitter.setErrorNeighborhoodSize(neighborErrorSize);
				    	
						parallelFitter.runExport(image, true);
						
						// If we are using visualization, detach previous voxel box from interactors
						if (useVisualization)
							heart.getVoxelBox().detach(ev.getCanvas());
						
						// Trying to be more memory friendly by destroying previous heart...
						image.getHeart().destroy();
						System.gc();
					}
				});
				
				protectedFitting = false;

			}
		}).start();
    }
    
	private void load() {
		// Try to fetch the current heart from the heart manager
		Heart heart = heartManager.getCurrent();

		if (heart != null) {
			// Remove previous if it exists
			if (image.getVoxelBox() != null)
				ev.removeInteractor(image.getVoxelBox());

			image.setLoading(true);
			image.set(heart);

			VoxelBox box = image.getVoxelBox();
			hfield.setVoxelBox(box);


			if (box != null) {
				ev.addInteractor(box);
			}

			image.setLoading(false);
		}
  }

    private String extractPath(String filename) {
    	String filedir = "";
    	
    	StringTokenizer tk = new StringTokenizer(filename, "/");
    	while (tk.hasMoreTokens()) {
    		String token = tk.nextToken() + "/";
    		if (tk.hasMoreTokens())
    			filedir += token;
    	}
    	
    	return filedir;
    }

    private String extractName(String filename) {
    	String file = "";
    	
    	StringTokenizer tk = new StringTokenizer(filename, "/");
    	while (tk.hasMoreTokens()) {
    		String token = tk.nextToken();
    		if (!tk.hasMoreTokens())
    			file = token;
    	}
    	
    	return file;
    }
    
    /**
     * Fit the selected helicoid parameter (or all of them at the same time).
     */
    private void fit() {
    	    	
    	System.out.println("Launching fit...");
//    	hbox.clear(ecbHelicoid.getSelected());

    	System.out.println("Error epsilon = " + parallelFitter.getFittingError().getEpsilon());
    	System.out.println("Gradient epsilon = " + parallelFitter.getFittingError().getGradientEpsilon());
    	
    	System.out.println("Launching fit...");
    	parallelFitter.run(image, true);
	}
    
    private boolean resumed = false;

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        		
        // Create the text renderer if necessary
        if (textRenderer == null) {
    		Font font = new Font("Consolas", Font.PLAIN, 18);
    		textRenderer = new JoglTextRenderer(font, true,
    				false);
    		textRenderer.setColor(0.5f, 0.9f, 0f, 1f);
        }

        if (!protectedFitting) {
        	probeFittingTermination();
        	// Check if the fitting is complete
        	probeFittingProcess();
        	
            if (!parallelFitter.isRunning()) {
            	image.applyAnisotropicScale(gl);
                drawScene(drawable);
            }
        }
        
        if (parallelFitter.isRunning()) {
        	textRenderer.drawCenter("Fitting in progress... (id: " + image.getHeart().getID() + ")", drawable);
        }
        
		// Draw text info
        if (displayInfo.getValue()) {
        	textRenderer.setText(parallelFitter.getCompletion());
        	textRenderer.drawBufferWithFieldParsing(drawable, 10, 10, false, ":");
        }	

    }

	private void drawScene(GLAutoDrawable drawable) {
    	GL2 gl = drawable.getGL().getGL2();

    	hfield.display(drawable);
    	
        if (displayWorld.getValue())
            WorldAxis.display(gl);
        
        image.display(drawable, altDown && !shiftDown);
	}

    private void probeFittingTermination() {
        if (requestFittingTermination) {
        	parallelFitter.stop();
        	requestFittingTermination = false;
        	
            List<CartanParameterNode> nodes = parallelFitter.forceCollect();
    		
           	this.nodes = nodes;
        }
    }
    
	private void probeFittingProcess() {
		if (parallelFitter.isReadyForCollection()) {
			List<CartanParameterNode> nodes = parallelFitter.collect();

			if (nodes != null) {

				this.nodes = nodes;

				System.out.println("Collecting " + nodes.size() + " fits...");

		    	hfield.set(image.getVoxelBox().getDimension(), nodes);
				
				System.out.println("HeartFitter: Running time = "
						+ parallelFitter.getFormattedRunningTime());

			}
		}
	}

    private JoglTextRenderer textRenderer;

    private void extractHeart() {
    	image.getHeart().saveExtraction();
    }
    
	private void exportFit() {

		String datafile = image.getHeart().getDirectory() + DEFAULT_FITPATH;
		
		System.out.println("Extracting " + nodes.size() + " voxels to "
				+ datafile + "...");

		String extra = "";
		extra += "_n" + parallelFitter.getFittingError().getNeighborhoodFitSize();
		
		IntensityVolume[] vols = hfield.getParameterVolumes();
		for (int i = 0; i < CartanParameter.labels.length; i++) {
			MatlabIO.save(datafile + CartanParameter.getLabel(i) + extra + ".mat", vols[i]);
		}
		MatlabIO.save(datafile + "error" + extra + ".mat", vols[CartanParameter.labels.length + 1]);
	}

	private void export(VoxelVectorField[] vfields) {

		String datafile = "./data/v";

		System.out.println("Extracting to "
				+ datafile + "i.txt...");
		
		final Vector3d v3 = new Vector3d();
		for (int i = 0; i < 3; i++) {
			try {
				final VoxelVectorField v = vfields[i];

				final Point3i o = v.getVoxelBox().getOrigin();

				final PrintWriter out = new PrintWriter(new FileWriter(
						datafile + i + ".txt"));
				
				out.println("------------------------------------");
				out.println("Framed DTI volume extraction - " + (i == 0 ? " x axis (fiber direction)" : i == 1 ? " y axis" : " z axis"));
				out.println("Heart ID: " + heartManager.getCurrent().getID());
				out.println("Created by " + System.getProperty("user.name"));
				out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
				
				out.println("------------------------------------");
				out.println("Volume Dimension =");
				out.println(v.getDimension());

				out.println("Slice origin = ");
				out.println(vfields[0].getVoxelBox().getOrigin());
				out.println("Slice span = ");
				out.println(vfields[0].getVoxelBox().getSpan());
				
				out.println("Vx, Vy, Vz");
				
				v.getVoxelBox().voxelProcess(new PerVoxelMethod() {

					@Override
					public void process(int x, int y, int z) {
						v.getVector3d(x - o.x, y - o.y, z - o.z, v3);
						String s = (x - o.x) + "," + (y - o.y) + "," + (z - o.z) + "," + v3.x + "," + v3.y + "," + v3.z;
						out.println(s);
					}

					@Override
					public boolean isValid(Point3d origin, Vector3d span) {
						// TODO Auto-generated method stub
						return true;
					}
				});

				out.close();
				System.out.println("Done.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
    @Override
    public String getName() {
    	return "Fitter";
    }


    @Override
    public void init(GLAutoDrawable drawable) {
    	hfield.init(drawable);
    }

    @Override
    public JPanel getControls() {
    	VerticalFlowPanel vfp = new VerticalFlowPanel();
    	vfp.add(heartManager);
        return vfp.getPanel();
    }

    private JPanel getFieldControls() {
    	VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add(hfield.getControls());

        return vfp.getPanel();
    }
    private JPanel getSideControls() {
    	VerticalFlowPanel vfp = new VerticalFlowPanel();

        JButton btnLaunch = new JButton("Launch all hearts");
        btnLaunch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				launchAllHearts(heartManager);
			}
		});
        vfp.add(btnLaunch);
        		
		vfp.add(launchHistograms.getControls());

        vfp.add(displayWorld.getControls());
        vfp.add(displayInfo.getControls());
        vfp.add(displayCenterline.getControls());
        
        JButton extract = new JButton("Extract");
        extract.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				VoxelVectorField[] vfields = image.getFrameField().extract(image.getVoxelBox());
				
				export(vfields);
			}
		});
        vfp.add(extract);

        JButton savefit = new JButton("Save fit");
        savefit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				exportFit();
			}
		});
        vfp.add(savefit);

        JButton flip = new JButton("Flip");
        flip.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				image.getFrameField().flip();
			}
		});
        return vfp.getPanel();
    }

    private boolean shiftDown = false;
    private boolean altDown = false;
    
	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {

		 		shiftDown = e.isShiftDown();
				altDown = e.isAltDown();
				
				if (e.getKeyCode() == KeyEvent.VK_F) {
					fit();
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
//					parallelFitter.storeFit(heartManager.getCurrent().getID(), fitresult, nodes);
					exportFit();
				}
				else if (e.getKeyCode() == KeyEvent.VK_X){
					requestFittingTermination = true;
				}
				else if (e.getKeyCode() == KeyEvent.VK_L){
					launchAllHearts(heartManager);
				}
				else if (e.getKeyCode() == KeyEvent.VK_E){
					extractHeart();
				}
				else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					image.getFrameField().setVisible(!image.getFrameField().isVisible());
				}
			}
		});
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void detach(Component component) {
	}

}
