package heart.geometry;

import gl.geometry.FancyArrow;
import gl.material.Colour;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import system.object.Pair;
import system.object.Triplet;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;

public class HeartGeometry {
	private IntensityVolumeMask mask;
	
	/**
	 * List of pairs (centroid, radius)
	 */
	private List<Triplet<Boolean, Point3d, Double>> centerline;
	private List<Vector3d> centerlineTangents;
	private List<Pair<Point3d, Vector3d>> centerlineCoupledTangents;
	
	public HeartGeometry(IntensityVolumeMask mask) {
		this.mask = mask;
		
		update();
	}
	
	public void update() {
		computeCenterline();
		computeCenterlineTangents();
	}
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		// Draw centerline
		gl.glColor3d(1, 1, 1);
		gl.glLineWidth(3f);
		gl.glBegin(GL2.GL_LINE_STRIP);
		for (int z = 0; z < mask.getDimension()[2]; z++) {
			if (centerline.get(z).getLeft()) {
				Point3d c = centerline.get(z).getCenter();
				gl.glVertex3d(c.x, c.y, c.z);
			}
		}		
		gl.glEnd();
		
		// Draw tangents
		gl.glEnable(GL2.GL_LIGHTING);
		for (int z = 0; z < mask.getDimension()[2]; z++) {
			if (centerline.get(z).getLeft()) {
				Point3d c = centerline.get(z).getCenter();
				Vector3d t = new Vector3d(centerlineTangents.get(z));
				t.add(c);
				new FancyArrow(c, t, Colour.blue, 0.5f).drawCoarse(gl);
			}
		}		
	}
	
	private void computeCenterlineTangents() {
		centerlineTangents = new ArrayList<Vector3d>();
		centerlineCoupledTangents = new ArrayList<Pair<Point3d, Vector3d>>();
		
		for (int z = 0; z < mask.getDimension()[2]; z++) {
			Point3d c = centerline.get(z).getCenter();
			if (centerline.get(z).getLeft()) {
				Point3d cprevious = null;
				Point3d cnext = null;
				
				if (z > 0 && centerline.get(z-1).getLeft()) {
					cprevious = centerline.get(z - 1).getCenter();
				}					
				if (z < mask.getDimension()[2] - 1 && centerline.get(z+1).getLeft()) {
					cnext = centerline.get(z + 1).getCenter();
				}					

				// Compute new tangent
				Vector3d tangent = new Vector3d();

				/*
				 *  Use central differences if we have both forward and backwards neighbors
				 */
				if (cprevious != null && cnext != null) {
					tangent.sub(cnext, cprevious);
				}
				/*
				 *  Use forward or backward differences for centroids without neighbors
				 */
				else if (cprevious == null) {
					tangent.sub(cnext, c);
				}
				else if (cnext == null) {
					tangent.sub(c, cprevious);
				}

				tangent.normalize();
				centerlineTangents.add(tangent);
				centerlineCoupledTangents.add(new Pair<Point3d, Vector3d>(c, tangent));
			}
			else {
				// FIXME: no centroid was found, use upwards vector for now
				// this should default to, e.g., the mean tangent
				Vector3d tangent = new Vector3d(0, 0, 1);
				centerlineTangents.add(tangent);
				centerlineCoupledTangents.add(new Pair<Point3d, Vector3d>(c, tangent));
			}
		}
	}
	
	private void computeCenterline() {
		centerline = mask.computeCenterlineAlongWorldZ();
//		int[] dims = mask.getDimension();
//
//		// Create a voxelbox with a transverse (XY) cut
//		VoxelBox box = new VoxelBox(mask);
//		box.cut(CuttingPlane.TRANSVERSE);
//		
//		// Create the list of centroid points
//		centerline = new ArrayList<Triplet<Boolean, Point3d, Double>>();
//		for (int z = 0; z < dims[2]; z++) {
//			// Set the box to the current height value
//			Point3i currentSlice = box.getOrigin();
//			currentSlice.z = z;
//			box.setOrigin(currentSlice);
//			
//			final Point3d centroid = new Point3d();
//			final int[] count = new int[] { 0 };
//			
//			// First find the centroid
//			box.voxelProcess(new PerVoxelMethodUnchecked() {
//				
//				@Override
//				public void process(int x, int y, int z) {
//					centroid.x += x;
//					centroid.y += y;
//					centroid.z += z;
//					count[0]++;
//				}
//			});
//			
//			Boolean flag = false;
//			if (count[0] > 0) {
//				centroid.scale(1d / count[0]);
//				flag = true;
//			}
//			else {
//				centroid.set(Double.NaN, Double.NaN, Double.NaN);
//				flag = false;
//			}
//			
//			// Then find the maximal radius from that centroid
//			final Point3d max = new Point3d(centroid);
//			final Point3d current = new Point3d();
//			box.voxelProcess(new PerVoxelMethodUnchecked() {
//				@Override
//				public void process(int x, int y, int z) {
//					current.set(x, y, z);
//					if (current.distance(centroid) > max.distance(centroid))
//						max.set(current);
//				}
//			});			
//			centerline.add(new Triplet<Boolean, Point3d, Double>(flag, centroid, max.distance(centroid)));
//		}
	}

	public PolarMap<Double> polarMap(final IntensityVolume volume, double dr, int subdivisionTheta) {
		return polarMap(volume, dr, subdivisionTheta, null);
	}
	
	public PolarMap<Double> polarMap(final IntensityVolume volume, double dr, int subdivisionTheta, VoxelBox boxIn) {
		int[] dims = mask.getDimension();

		// Create a voxelbox with a transverse (XY) cut
		VoxelBox box;
		if (boxIn == null)
			box = new VoxelBox(mask);
		else
			box = new VoxelBox(boxIn);
		
//		box.cut(CuttingPlane.TRANSVERSE);
		
		final PolarMap<Double> map = new PolarMap<Double>(dr, subdivisionTheta);
		
		int z0 = boxIn.getOrigin().z;
		int zf = z0 + boxIn.getSpan().z;
		box.cut(CuttingPlane.TRANSVERSE);
		
		// Create the list of centroid points
		for (int z = z0; z < zf; z++) {
			// Set the box to the current height value
			Point3i currentSlice = box.getOrigin();
			currentSlice.z = z;
			box.setOrigin(currentSlice);
			
			box.voxelProcess(new PerVoxelMethodUnchecked() {
				
				@Override
				public void process(int x, int y, int z) {
					if (centerline.get(z).getLeft()) {
//						System.out.println(x + "," + y + "," + z);
						map.map(new Point3d(x, y, z), centerline.get(z).getCenter(), volume.get(x, y, z));
					}
				}
			});
		}
		
		return map;
	}
	
	public List<Triplet<Boolean, Point3d, Double>> getCenterline() {
		return centerline;
	}
	
	public List<Vector3d> getCenterlineTangents() {
		return centerlineTangents;
	}

	public List<Pair<Point3d, Vector3d>> getCenterlineCoupledTangents() {
		computeCenterline();
		computeCenterlineTangents();
		return centerlineCoupledTangents;
	}
}
