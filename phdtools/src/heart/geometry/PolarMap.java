package heart.geometry;

import java.awt.Color;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

import swing.component.JColorMap;
import system.object.Pair;
import tools.geom.MathToolset;
import app.pami2013.cartan.CartanParameter.Parameter;

public class PolarMap<T> {

	private double dr, dtheta;
	private int subdivisionsTheta;
	private int rCoorMax;
	
	private Map<Pair<Integer, Integer>, List<T>> map;
	
	/**
	 * Construct a polar heart map with 2PI wrap-around and an initial 10 * dr radius.
	 * The delta theta parameter is adjusted such that 2PI / dtheta is an integer.
	 * @param dtheta
	 * @param dr
	 */
	public PolarMap(double dr, double dtheta) {
		// Map the delta theta to the nearest number that produces an integer number of divisions of 2PI.
		this(dr, MathToolset.roundInt(2 * Math.PI / dtheta));
	}
	
	/**
	 * Construct a polar heart map with 2PI wrap-around and an initial 10 * dr radius.
	 * @param dtheta
	 * @param subdivisionsTheta
	 */
	public PolarMap(double dr, int subdivisionsTheta) {
		// Set delta subdivisions
		this.subdivisionsTheta = subdivisionsTheta;
		this.dtheta = (2 * Math.PI) / subdivisionsTheta;
		// Set delta radius
		this.dr = dr;
		
		/*
		 *  Create the map
		 */
		map = new HashMap<Pair<Integer,Integer>, List<T>>();
		for (int rCoord = 0; rCoord < 10; rCoord++) {
			for (int thetaCoord = 0; thetaCoord < subdivisionsTheta; thetaCoord++) {
				map.put(new Pair<Integer, Integer>(rCoord, thetaCoord), new LinkedList<T>());
				rCoorMax = Math.max(rCoorMax, rCoord);
			}
		}
	}
	
	/**
	 * Map data to a cell based on the specific discretization of this map. 
	 * @param radius
	 * @param theta
	 */
	public void map(double radius, double theta, T value) {
		// Extract (r, theta) polar coordinates
		Integer rCoord = (int) Math.ceil(radius / dr);
		Integer thetaCoord = (int) Math.ceil(theta / dtheta);

		rCoorMax = Math.max(rCoorMax, rCoord);
		
		Pair<Integer, Integer> pair = new Pair<Integer, Integer>(rCoord, thetaCoord);

		// If this coordinate does not already exist, need to create it
		if (!map.containsKey(pair)) {
			map.put(pair, new LinkedList<T>());
		}
		
		// Add the value
		map.get(pair).add(value);
	}
	
	/**
	 * Map a point with respect to its barycenter, assuming the polar plane is the XY plane.
	 * @param pt
	 * @param barycenter
	 * @param value
	 */
	public void map(Point3d pt, Point3d barycenter, T value) {
		double r = pt.distance(barycenter);
		double theta = Math.atan2(pt.x - barycenter.x, pt.y - barycenter.y);
		map(r, theta, value);
	}
	
	public void displayAsCartanParameter(GLAutoDrawable drawable, Parameter parameter) {
		// TODO: select and display this cartan parameter. Assumes T is instanceof CartanParameter
	}
	
	public void displayAsDouble(GLAutoDrawable drawable, boolean drawGeometry) {
		displayAsDouble(drawable, drawGeometry, null);
	}
	
	/**
	 * Attempt to display the polar map where the interal data type is set to <code>Double</code>.
	 * @param drawable
	 */
	public void displayAsDouble(GLAutoDrawable drawable, boolean drawGeometry, JColorMap colorMap) {
		GL2 gl = drawable.getGL().getGL2();
		
		// Compute min and max
		List<Pair<Double, Double>> minMax = minMax();
		Pair<Double, Double> magMean = minMax.get(0);
		
//		System.out.println(magMean);
		
		Pair<Double, Double> magStd = minMax.get(1);
		
		gl.glBegin(GL2.GL_QUADS);
		for (Pair<Integer, Integer> key : map.keySet()) {
			int rCoord = key.getLeft();
			int thetaCoord = key.getRight();
			double rSub = rCoord * dr;
			double rSup = (rCoord + 1) * dr;
			double thetaSub = thetaCoord * dtheta;
			double thetaSup = (thetaCoord + 1) * dtheta;

			// Construct four rectangle points
			Point2d p1 = new Point2d(rSub * Math.cos(thetaSup), rSub * Math.sin(thetaSup));
			Point2d p2 = new Point2d(rSup * Math.cos(thetaSup), rSup * Math.sin(thetaSup));
			Point2d p3 = new Point2d(rSup * Math.cos(thetaSub), rSup * Math.sin(thetaSub));
			Point2d p4 = new Point2d(rSub * Math.cos(thetaSub), rSub * Math.sin(thetaSub));

			double value = mean(key)[0];
			// Map value to [0, 1]
			if (colorMap != null) {
				Color color = colorMap.getColor(value);
				gl.glColor3d(color.getRed()/255d, color.getGreen()/255d, color.getBlue()/255d	);
			}
			else {
				double valueC = Math.abs(value - magMean.getLeft()) / Math.abs(magMean.getRight() - magMean.getLeft());
				gl.glColor4d(0, 0, valueC, 1);
			}
			
			gl.glVertex3d(p1.x, p1.y, 0);
			gl.glVertex3d(p2.x, p2.y, 0);
			gl.glVertex3d(p3.x, p3.y, 0);
			gl.glVertex3d(p4.x, p4.y, 0);
		}
		gl.glEnd();
		
		if (drawGeometry) {
			// Draw polar map geometry
			gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE );
			gl.glLineWidth(1.0f);
			gl.glColor4d(0, 0, 0, 1);
			gl.glBegin(GL2.GL_QUADS);
			for (int rCoord = 1; rCoord <= rCoorMax; rCoord++) {
				double rSub = rCoord * dr;
				double rSup = (rCoord + 1) * dr;
				
				for (int thetaCoord = 0; thetaCoord < subdivisionsTheta; thetaCoord++) {
					double thetaSub = thetaCoord * dtheta;
					double thetaSup = (thetaCoord + 1) * dtheta;
					
					// Construct four rectangle points
					Point2d p1 = new Point2d(rSub * Math.cos(thetaSup), rSub * Math.sin(thetaSup));
					Point2d p2 = new Point2d(rSup * Math.cos(thetaSup), rSup * Math.sin(thetaSup));
					Point2d p3 = new Point2d(rSup * Math.cos(thetaSub), rSup * Math.sin(thetaSub));
					Point2d p4 = new Point2d(rSub * Math.cos(thetaSub), rSub * Math.sin(thetaSub));
					
					gl.glVertex3d(p1.x, p1.y, 0);
					gl.glVertex3d(p2.x, p2.y, 0);
					gl.glVertex3d(p3.x, p3.y, 0);
					gl.glVertex3d(p4.x, p4.y, 0);
				}
			}
			gl.glEnd();
			gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
		}
	}
	
	private double[] mean(Pair<Integer, Integer> coordinate) {
		List<T> values = map.get(coordinate);
		
		double mean = 0, std = 0;
		if (values.size() > 0) {
			double[] array = new double[values.size()];
			int i = 0;
			for (T value : values) {
				double d = (Double) value;
				if (!Double.isNaN(d))
					array[i++] = d;
			}
			mean = MathToolset.mean(array);
			std = MathToolset.mean(array);
		}

		return new double[] { mean, std };
	}
	
	public List<Pair<Double, Double>> minMax() {
		Pair<Double, Double> magMean = new Pair<Double, Double>(0d, 0d);
		Pair<Double, Double> magStd = new Pair<Double, Double>(0d, 0d);
		
		for (Pair<Integer, Integer> key : map.keySet()) {
			List<T> values = map.get(key);
			
			if (values.size() == 0)
				continue;
			
			double[] stats = mean(key);
			double mean = stats[0];
			double std = stats[1];
			
			if (Double.isNaN(mean) || Double.isNaN(std))
				continue;
				
			// Set min/max
			if (mean > magMean.getRight())
				magMean = new Pair<Double, Double>(magMean.getLeft(), mean);
			
			if (mean < magMean.getLeft())
				magMean = new Pair<Double, Double>(mean, magMean.getRight());

			if (std > magStd.getRight())
				magStd = new Pair<Double, Double>(magStd.getLeft(), std);
			
			if (std < magStd.getLeft())
				magStd = new Pair<Double, Double>(std, magStd.getRight());
		}
		
		return Arrays.asList(magMean, magStd);
	}
	
}
