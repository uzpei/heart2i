package heart;

import heart.Heart.Species;


public class HeartDefinition {

	public String id;
	
	public Species species;
	
	public String filename;
	
	public HeartDefinition(String id, Species s, String filename) {
		this.id = id;
		this.species = s;
		this.filename = filename;
	}
}
