package heart;

import heart.Heart.Species;
import heart.geometry.HeartGeometry;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import projects.inpainting.reconstruction.FrameFieldReconstruction;
import system.object.Pair;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import app.dti.DiffusionSplitter;

public class HeartTools {

	public enum DamageType { Perforate, LongAxis, Noise, Sampled }
	
	public static Heart heartSample() {
		Heart heart = new Heart(new HeartDefinition("rat07052008", Species.Rat,"./data/heart/rat_samples/sampleSubset/"));
		heart.prepareDataIfNecessary();
		return heart;
	}

	public static Heart torusHeart() {
		return torusHeart(true);
	}
	
	public static Heart torusHeart(boolean partial) {

		// minor (inner) radius
		double r0 = 0.15;
//		double r0 = 0.25;
		
		// major radius
		double R = 1;
		
		// Mesh resolution (use odd number)
		int res = 35;

		// Normalization factor such that the torus fits entirely within [-0.5, 0.5] in the axial plane and [-0.5 / ratio, 0.5 / ratio] in the longitudinal 
		double norm = (res - 1) / (2 * (R + r0));
		
		// First populate a volumetric volume
//		IntensityVolumeMask volume = new IntensityVolumeMask(res, res, MathToolset.roundInt(res / ratio));
		IntensityVolumeMask mask = new IntensityVolumeMask(res, res, res);
		mask.fillWith(0);
		int pcenter = (int) Math.floor(mask.getDimension()[0] / 2d);
		Point3i center = new Point3i(pcenter - 1, pcenter - 1, pcenter - 1);
		double dt = 0.01;
		double dp = 0.01;
		double dr = r0 / 10d;
		Point3d p = new Point3d();
		for (double r = r0; r > 0; r -= dr) {
//			for (double theta = 0; theta <= 2 * Math.PI; theta += dt) {
//			for (double theta = 0; theta <= 0; theta += dt) {
//			for (double theta = Math.PI/2; theta <= Math.PI; theta += dt) {
			for (double theta = 0.05 + (partial ? 4.5 * Math.PI/4 : 0); theta <= (partial ? 13 * Math.PI / 8 : 2 * Math.PI); theta += dt) {
				for (double phi = 0; phi <= 2 * Math.PI; phi += dp) {
//				for (double phi = 0; phi <= 1.25 * Math.PI; phi += dp) {

					p.x = R * Math.cos(theta) + r * Math.cos(theta) * Math.cos(phi);
					p.y = R * Math.sin(theta) + r * Math.sin(theta) * Math.cos(phi);
					p.z = 0 + r * Math.sin(phi);

					p.scale(norm);
					Point3i pi = MathToolset.tuple3dTo3i(p);
					pi.add(center);
					if (mask.contains(pi.x, pi.y, pi.z))
						mask.set(pi, 1);
				}
			}
		}
		
		/*
		 * Now compute fiber directions
		 */
//		Point3d centroid = mask.getCenterOfMass();
		Point3d centroid = new Point3d(center.x, center.y, center.z);
		Vector3d r = new Vector3d();
		
		double inner = Double.MAX_VALUE;
		double outer = 0;

		VoxelBox box = new VoxelBox(mask);
		
		for (Point3i v : box.getVolume()) {
			// Attach frame field based on position
			// For now simply use rotational vector
			r.x = v.x - centroid.x;
			r.y = v.y - centroid.y;
			r.z = v.z - centroid.z;
			
			double distance = r.length();
			if (distance > outer) {
				outer = distance;
			}
			
			if (distance < inner) {
				inner = distance;
			}
		}
		
		VoxelFrameField field = new VoxelFrameField(mask.getDimension(), mask);
		IntensityVolume endo = new IntensityVolume(mask.getDimension());
		IntensityVolume epi = new IntensityVolume(mask.getDimension());
		
		Matrix4d M = new Matrix4d();
		Point3i vmin = new Point3i(1,1,1);
		vmin.scale(Integer.MAX_VALUE);
		Point3i vmax = new Point3i(1,1,1);
		vmax.scale(-Integer.MAX_VALUE);
		
		for (Point3i v : box.getVolume()) {
			vmin.x = Math.min(vmin.x, v.x);
			vmin.y = Math.min(vmin.y, v.y);
			vmin.z = Math.min(vmin.z, v.z);
			vmax.x = Math.max(vmax.x, v.x);
			vmax.y = Math.max(vmax.y, v.y);
			vmax.z = Math.max(vmax.z, v.z);
			
			// Attach frame field based on position
			// For now simply use rotational vector
			r.x = v.x - centroid.x;
			r.y = v.y - centroid.y;
			r.z = v.z - centroid.z;
			Vector3d f3 = new Vector3d(r);
			f3.normalize();
			
			double distance = r.length();
			double lambda = MathToolset.clamp(1 - (distance - inner) / (outer - inner), 0, 1);
			epi.set(v, (1-lambda) * (outer - inner));
			endo.set(v, lambda * (outer - inner));

			Vector3d f1 = new Vector3d(-r.y, r.x, 0);
			f1.normalize();
//			Vector3d fz = new Vector3d(0, 0, 1);
//			Vector3d f1 = new Vector3d();
			double phi = lambda * 120d / 180d * Math.PI;
//			f1.scaleAdd(Math.cos(phi), fc, f1);
//			f1.scaleAdd(Math.sin(phi), fz, f1);
//			f1.normalize();
			// Apply rotation
			M.set(new AxisAngle4d(f3, phi));
			M.transform(f1);
			
			Vector3d f2 = new Vector3d(0, 0, 1);
			f2.cross(f3, f1);
			
//			if (partial)
//				v.add(voffset);
			
			field.set(v, new Vector3d[] { f1, f3, f2 });
		}
		
		// Once we are done, center everything with the center of mass
		Point3d cm = mask.getCenterOfMass();
//		mask.getDimensions()
		VoxelBox cbox = new VoxelBox(mask);
		cbox.setOrigin(vmin);
		cbox.setSpan(new Point3i(vmax.x-vmin.x,vmax.y-vmin.y,vmax.z-vmin.z));
		field = field.extractField(cbox);
		mask = field.getMask();
		
		System.out.println(cm);

		Heart heart = new Heart();
		box = new VoxelBox(mask);
		field.setVoxelBox(box);
		heart.setFrameField(field);
		heart.setMask(mask);
		heart.setVoxelBox(box);
		heart.setDataLoaded();

		return heart;
	}

	public static Heart helixHeart() {
		// Produce a sample heart volume
		// with a rectangular fiber bundle that rotates
		// along F3 axis
		int nside = 5;
		int depth = 16;
		final int[] dims = new int[] { nside, depth, nside};
		
		// First create the mask
		IntensityVolumeMask mask = new IntensityVolumeMask(dims);
		mask.add(1);
		
		// Create frame field
		// define center of first XY slice
		final Point3d p0 = new Point3d(nside/2.0, nside/2.0, 0);
		final Point3d p = new Point3d();
		final double alpha0 = Math.PI/2;
		final double alpha1 = -Math.PI/3;
		
		final VoxelFrameField field = new VoxelFrameField(dims);
		final Matrix4d R = new Matrix4d();
		new VoxelBox(mask).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				p.set(x, y, z);
//				double dist = p.distance(p0);
				double dist = y / (double) dims[1];
				
				Vector3d[] f = new Vector3d[3];
				
				f[2] = new Vector3d(0, 1, 0);
				
				double alpha = dist * alpha0 + (1 - dist) * alpha1;
				R.set(new AxisAngle4d(f[2], -alpha));
				
				// Apply rotation
				f[0] = new Vector3d(1,0,0);
				R.transform(f[0]);
				
				// Set final vector
				f[1] = new Vector3d();
				f[1].cross(f[2], f[0]);
				field.set(x, y, z, f);
			}
		});
		field.setMask(mask);

		Heart heart = new Heart();
		VoxelBox box = new VoxelBox(mask);
		heart.setFrameField(field);
		heart.setMask(mask);
		heart.setVoxelBox(box);
		heart.setDataLoaded();
		
		return heart;
	}
	
	/**
	 * Compute a rule-based fiber orientation volume based on 
	 * estimated wall normals from a B0
	 */
	public static Heart computeRuleBased(Heart heart) {
		
//		heart.getFrameField().setVoxelBox(heart.getVoxelBox());
		
		// step) Compute wall normals
		final VoxelVectorField f_n = heart.computeWallNormals();
		
		// step) Estimate long axis and tangents
		HeartGeometry geometry = heart.getGeometry();
		List<Pair<Point3d,Vector3d>> tangents = geometry.getCenterlineCoupledTangents();
		
		// step) Create a lookup table from Z coordinate to long axis tangent
		final Vector3d[] longAxisLookup = new Vector3d[heart.getDimension()[2]];
		for (int z = 0; z < heart.getDimension()[2]; z++) {
			longAxisLookup[z] = tangents.get(z).getRight();
		}
		
		// step) TODO: smooth centerline
		
		// step) assemble long-axis vector field
		final VoxelVectorField f_L = new VoxelVectorField(heart.getVoxelBox());
		new VoxelBox(heart.getMask()).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				f_L.set(x, y, z, longAxisLookup[z]);
			}
		});
		
		// step) Estimate circumferential component as f_circ = f_L x f_normal
		final VoxelVectorField f_circ = VoxelVectorField.cross(f_L, f_n);
		
		// step) set helix angle in outer and inner walls
		// TODO: estimate this from true data, e.g., by averaging out thin shell
		// at outer and inner walls
		final double alpha_outer = 0;
		final double alpha_inner = 120; 
		
		// step) apply axis angle rotation
		final IntensityVolume endo = heart.getDistanceTransformEndo();
		final IntensityVolume epi = heart.getDistanceTransformEpi();
		
		// use 75% of max depth
		final double epi_max = 0.55 * epi.getMagnitudeMaximum();
		final double gamma_mod = -0.5;
		final Matrix4d R = new Matrix4d();

		final VoxelVectorField f_1 = new VoxelVectorField(heart.getVoxelBox());
		new VoxelBox(heart.getMask()).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				// Fetch circumferential and normal components
				Vector3d f = new Vector3d();
				f_circ.getVector3d(x, y, z, f);
				Vector3d f_axis = f_n.getVector3d(x, y, z);
				
				// Interpolate helix angle
				double d_endo = endo.get(x, y, z);
				double d_epi = epi.get(x, y, z);
				
				// Compute linear interpolant
//				double gamma = d_epi / (d_epi + d_endo) % 1;
				double gamma = d_epi / epi_max + gamma_mod;
				
				// Compute helix angle
				double alpha = (1-gamma) * alpha_outer + gamma * alpha_inner; 
						
				// Add angle offset (initial fiber orientation at outer wall) 
				alpha -= 180;
				alpha = alpha / 180 * Math.PI;
				// Set rotation matrix
				R.set(new AxisAngle4d(f_axis, -alpha));
				
				// Apply rotation
				R.transform(f);
				
				// Set final vector
				f.normalize();
				f_1.set(x, y, z, f);
			}
		});
		

		// step) orthogonalize third axis w.r.t f1
		f_n.orthogonalize(f_1, heart.getVoxelBox());
		final VoxelVectorField f_2 = VoxelVectorField.cross(f_n, f_1);
		
		// step) assemble rule-based frame field
		f_1.setColor(new double[] { 1, 0, 0 } );
		f_2.setColor(new double[] { 0, 1, 0 } );
		f_n.setColor(new double[] { 0, 0, 1 } );
		VoxelFrameField frameField = new VoxelFrameField(f_1, f_2, f_n, heart.getMask());

		frameField.setVoxelBox(heart.getVoxelBox());
		frameField.setMask(heart.getMask());
		
		Heart rule = new Heart();
		rule.setVoxelBox(heart.getVoxelBox());
		rule.setMask(heart.getMask());
		rule.set(heart.getDefinition());
		rule.setFrameField(frameField);
		rule.setDataLoaded();
		
		return rule;
	}
	public static Heart heart() {
		Heart heart = new Heart(new HeartDefinition("rat07052008", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/"));
		heart.prepareDataIfNecessary();
		return heart;
	}

	public static Heart heartHuman() {
		Heart heart = new Heart(new HeartDefinition("human", Species.Rat,"./data/heart/human/"));
		heart.prepareDataIfNecessary();
		return heart;
	}
	
	public static Heart heartSynthetic() {

		// minor radius
		final double r0 = 0.35;

		// major radius
		final double R = 0.85;

		// Mesh resolution
		final int res = 32;

		// Normalization factor such that the torus fits entirely within [-0.5,
		// 0.5] in the axial plane and [-0.5 / ratio, 0.5 / ratio] in the
		// longitudinal
		final double norm = 0.5 * (res - 1) / 2 * (R + r0);
//		System.out.println(norm);

		// First populate a volumetric volume
		// IntensityVolumeMask volume = new IntensityVolumeMask(res, res,
		// MathToolset.roundInt(res / ratio));
		IntensityVolumeMask mask = new IntensityVolumeMask(res, res, res);
		mask.fillWith(0);
		final Point3i center = new Point3i(mask.getDimension()[0] / 2,
				mask.getDimension()[1] / 2, mask.getDimension()[2] / 2);
		double dt = 0.05;
		double dp = 0.05;
		double dr = r0 / 10d;
		Point3d p = new Point3d();
		List<Point3i> pts = new LinkedList<Point3i>();
		for (double r = r0; r >= 0; r -= dr) {
			for (double theta = 0; theta <= 2 * Math.PI; theta += dt) {
				// for (double theta = 0; theta <= 0; theta += dt) {
//				 for (double theta = 0; theta <= Math.PI; theta += dt) {
				for (double phi = 0; phi <= 2 * Math.PI; phi += dp) {
					// for (double phi = 0; phi <= 0; phi += dp) {

					p.x = R * Math.cos(theta) + r * Math.cos(theta)
							* Math.cos(phi);
					p.y = R * Math.sin(theta) + r * Math.sin(theta)
							* Math.cos(phi);
					p.z = 0 + r * Math.sin(phi);
					// System.out.println(phi);

					p.scale(norm);
					Point3i pi = MathToolset.tuple3dTo3i(p);
					pi.add(center);
//					if (mask.contains(pi.x, pi.y, pi.z) && pi.x > res / 2 && pi.z == res/2) {
					if (mask.contains(pi.x, pi.y, pi.z)) {
						mask.set(pi, 1);
						pts.add(new Point3i(pi));
					}
				}
			}
		}

		/*
		 * Now compute fiber directions
		 */
		final Vector3d r = new Vector3d();

		final VoxelFrameField field = new VoxelFrameField(mask.getDimension(), mask);
		
		new VoxelBox(mask).voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				// Attach frame field based on position
				// For now simply use rotational vector
				r.x = x - center.x;
				r.y = y - center.y;
				r.z = z - center.z;
				double distance = r.length();
//				double lambda = MathToolset.clamp(1 - (distance - inner)
//						/ (outer - inner), 0, 1);
//				double lambda = Math.abs((R + r0 - distance / norm)/(2*r0));
				double min = norm * (R - r0);
				double max = norm * (R + r0);
				double lambda = 1 - Math.abs((distance - min) / (max - min));
				
				// Vertical component
				Vector3d fz = new Vector3d(0, 0, 1);
				
				// Circumferential component
				Vector3d fc = new Vector3d(-r.y, r.x, 0);
				fc.normalize();
				Vector3d cross = new Vector3d();
				cross.cross(fz, fc);
				fc.scale(Math.signum(cross.dot(r)));

				/*
				 *  Assign fiber direction
				 */
				Vector3d f1 = new Vector3d();
				// Approximately 110deg total turning (-20 to +90)
				double phimin = -Math.PI / 10;
				double phimax = Math.PI / 2;
				
//				phimin = 0;
//				phimax = phimin;
				double phi = phimin + lambda * (phimax - phimin);
				
				f1.scaleAdd(Math.cos(phi), fc, f1);
				f1.scaleAdd(Math.sin(phi), fz, f1);
				f1.normalize();

				Vector3d f3 = new Vector3d();
				f3.set(-r.x, -r.y, 0);
				f3.normalize();

				Vector3d f2 = new Vector3d();
				f2.cross(f3, f1);
				f2.normalize();

				field.set(x, y, z, new Vector3d[] { f1, f2, f3 });			}
		});
		for (Point3i pt : pts) {

		}

		field.setMask(mask);
		field.setVoxelBox(new VoxelBox(mask));
		Heart heart = new Heart();
		heart.set(new HeartDefinition("synthetic", Species.Unknown, "N/A"));
		heart.setMask(mask);
		heart.setFrameField(field);
		heart.setVoxelBox(new VoxelBox(mask));

		return heart;
	}

	public static VoxelFrameField applyDamage(final VoxelFrameField field, DamageType type, double[] parameters) {
		IntensityVolumeMask[] sampling = null;
		
		VoxelFrameField frameFieldDamaged = null;
		
		switch (type) {
		case LongAxis:
			// interleaving
			sampling = DiffusionSplitter.splitMask(field.getMask(), FrameAxis.Z, MathToolset.roundInt(parameters[0]));
			break;
		case Perforate:
			// density, radius
			sampling = HeartTools.perforate(field.getMask(), parameters[0], MathToolset.roundInt(parameters[1]));
			break;
		case Sampled:
			// density
			sampling = HeartTools.subsample(field.getMask(), parameters[0]);
			break;
		case Noise:
			// angular noise
			frameFieldDamaged = field.voxelCopy(); 
			frameFieldDamaged.perturb(parameters[0]);
			break;
		}
		
		if (type == DamageType.LongAxis || type == DamageType.Perforate || type == DamageType.Sampled) {
			// Assign frame field
			IntensityVolumeMask available = sampling[0];
			
			frameFieldDamaged = new VoxelFrameField(available.getDimension());

			// Set only available data
			// (no traces of the old data)
			for (Point3i p : new VoxelBox(available).getVolume()) {
				frameFieldDamaged.set(p, field.get(p));
			}
			frameFieldDamaged.setMask(available);
		}

		return frameFieldDamaged;
	}

	/**
	 * Return a tuple containing: (0) data within the 2D slices and (1) data in-between slices 
	 * @param mask
	 * @param sampling
	 * @return
	 */
	public static IntensityVolumeMask[] sliceLongitudinally(IntensityVolumeMask mask, int sampling) {
		return DiffusionSplitter.splitMask(mask, FrameAxis.Z, sampling);
	}

	public static IntensityVolumeMask[] perforate(IntensityVolumeMask mask, double density) {
		return perforate(mask, density, 3);
	}
	
	/**
	 * Perforate an input volume by inserting randomized ellipsoidal holes with different eccentricities.
	 * @param mask
	 * @param density
	 * @return an array containing the input volume with holes removed, and a volume containing only the holes themselves
	 */
	public static IntensityVolumeMask[] perforate(IntensityVolumeMask mask, double density, int holeRadius) {
		// Minimium radii for anisotropic ellipsoids
		// 0 means we allow planar, linear, and point ellipsoids
		int ellipsoidMinRadius = 0;
		
		// Hole anisotropy in voxels. The three ellipsoidal radii will be max(holeSize +- anisotropy, 0)
		// For now fix at 50% anisotropy
		int anisotropy = MathToolset.roundInt((holeRadius+1)/2);

		// We allow the hole to be anosotropic. This is the largest hole we will possibly get.
		int holeMaxRadius = holeRadius + anisotropy;
		
		double radius = mask.getDimensionLength();
//		HeartTools.perforate(mask, holeSize, anisotropy);
//		MathToolset.poissonSampling(r, plane)
		Point3d[] holeCenters = MathToolset.poissonSampling3D(radius / density, radius);

		// Initialize our mask
		IntensityVolumeMask maskAvailable = new IntensityVolumeMask(mask);
		IntensityVolumeMask maskTarget = new IntensityVolumeMask(mask.getDimension());
		
		// For each sample, generate an anisotropic ellipsoid of radii holeSize
		Random rand = new Random();
//		Point3d com = maskAvailable.getCenterOfMass();
		for (Point3d holeCenter : holeCenters) {
//			holeCenter.add(com);
			Point3i center = MathToolset.tuple3dTo3i(holeCenter);
			
			if (!mask.contains(center.x, center.y, center.z) || mask.isMasked(center))
				continue;
			
			double[] radii = new double[3];
			for (int d = 0; d < 3; d++)
				radii[d] = Math.max(ellipsoidMinRadius, holeRadius + 2 * (-0.5 + rand.nextDouble()) * anisotropy);
			
			// Check all voxels in a holeSize x holeSize x holesize grid
			for (int xi = -holeMaxRadius; xi <= holeMaxRadius; xi++)
				for (int xj = -holeMaxRadius; xj <= holeMaxRadius; xj++)
					for (int xk = -holeMaxRadius; xk <= holeMaxRadius; xk++)
					{
						// Position of sample point within this grid
						Point3i pEllipsoid = new Point3i(xi, xj, xk);
						pEllipsoid.add(center);

						// Determine whether the point lies within the volume and is not already masked
						if (!mask.contains(pEllipsoid.x, pEllipsoid.y, pEllipsoid.z) || mask.isMasked(pEllipsoid))
							continue;

						// Determine whether the point lies within the target ellipsoid
						if ((xi / radii[0]) * (xi / radii[0]) + (xj / radii[1]) * (xj / radii[1]) + (xk / radii[2]) * (xk / radii[2]) <= 1)
						{
							// remove this point in the available mask
							maskAvailable.set(pEllipsoid, 0);
							
							// add this point to the target mask but only if this point is also part of the original volume
							maskTarget.set(pEllipsoid, 1);
						}
					}
				
		}
		
		return new IntensityVolumeMask[] { maskAvailable, maskTarget };
	}
	
	public static IntensityVolumeMask[] subsample(IntensityVolumeMask mask, double density) {
		
		double radius = mask.getDimensionLength() / 2;
		Point3d[] holeCenters = MathToolset.poissonSampling3D(radius / density, radius);

		// Initialize our mask
		IntensityVolumeMask maskAvailable = new IntensityVolumeMask(mask);
		IntensityVolumeMask maskTarget = new IntensityVolumeMask(mask.getDimension());
		
		Point3d com = maskAvailable.getCenterOfMass();
		for (Point3d holeCenter : holeCenters) {
			holeCenter.add(com);
			Point3i center = MathToolset.tuple3dTo3i(holeCenter);
//			System.out.println(center);
			
			if (!mask.contains(center.x, center.y, center.z) || mask.isMasked(center))
				continue;

			// remove this point in the available mask
			maskAvailable.set(center, 0);

			// add this point to the target mask but only if this point is also
			// part of the original volume
			maskTarget.set(center, 1);
		}
		
		return new IntensityVolumeMask[] { maskAvailable, maskTarget };
	}

}
