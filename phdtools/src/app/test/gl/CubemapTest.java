 package app.test.gl;

import gl.geometry.GLObject;
import gl.geometry.primitive.Cube;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.texture.CubeMap;
import gl.texture.GLTexture;

import java.awt.Component;
import java.awt.Dimension;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import tools.SimpleTimer;

import com.sun.opengl.util.texture.Texture;

import config.GraphicsToolsConstants;

public class CubemapTest implements Interactor, GLObject {

	public static void main(String[] args) {

		GLViewerConfiguration glconfig = new GLViewerConfiguration();
		new CubemapTest(glconfig);
	}
	
	private JoglRenderer viewer;
		
	public CubemapTest(GLViewerConfiguration config) {
		
		Dimension dimension = new Dimension(1000, 1000);
		Dimension controlSize = new Dimension(650, (int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Texture Test", this, dimension, controlSize, true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(0f);
		viewer.start();
		
//		viewer.getRoom().setVisible(false);
	}

	private CubeMap cubeMap = null;
	
	@Override
	public void attach(Component component) {
	}
	
	private void createCubemap(GL gl) {
		String cubepath = CubemapTest.class.getResource("/gl/resources/probes/cross/cubemap-diagram.png").getPath();

		cubeMap = new CubeMap(cubepath);
		cubeMap.initializeSeed(gl);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();

		cubeMap.prepareForDrawing(drawable);

		cubeMap.drawCross(drawable);

//		Cube.draw(drawable, 2);

		cubeMap.finishDrawing(drawable);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
				
		SimpleTimer st = new SimpleTimer();
		
		st.tick();
		createCubemap(gl);
		System.out.println("Cube map creation time = " + st.tick_ms());
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
	
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples() + " samples.");
		new CubemapTest(config);
	}
}
