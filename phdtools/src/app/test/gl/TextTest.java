package app.test.gl;

import gl.geometry.GLObject;
import gl.geometry.LightRoom;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.SceneRotator;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;

import com.sun.opengl.util.GLUT;
import com.sun.opengl.util.j2d.TextRenderer;

public class TextTest implements Interactor, GLObject {

	public static void main(String[] args) {
		new TextTest(new GLViewerConfiguration());
	}

	private JoglRenderer viewer;
	
	private LightRoom room = new LightRoom(5);
	
	private SceneRotator rotatortext = new SceneRotator(false, true, false);
	private SceneRotator rotatorgeom = new SceneRotator(false, false, false);

	public TextTest(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650,
				(int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Antialiasing", this, dimension, controlSize,
				true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(250f);
		viewer.start();
		
		rotatortext.setRotationVelocities(0, 0.6, 0);
		rotatorgeom.setRotationVelocities(0, -0.6, 0);
		
		room.setLightPosition(new Point3d(1.67, -0.22, 6.56));
		
		listFonts();
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_F) {
					listFonts();
				}
			}
		});
	}

	TextRenderer renderer, renderer3d;
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
//		gl.glDisable(GL.GL_LIGHTING);
//		gl.glLineWidth(2.0f);
//		gl.glBegin(GL.GL_LINES);
//		gl.glColor3d(0, 1, 0);
//		gl.glVertex3d(0, -5, 0);
//		gl.glVertex3d(0, 5, 0);
//		gl.glEnd();

		gl.glEnable(GL.GL_LIGHTING);
		
		gl.glEnable(GL.GL_CULL_FACE);
		room.display(drawable);
		
//		gl.glRotated(30, 1, 0, 0);
//		gl.glRotated(20, 0, 1, 0);
		
		gl.glPushMatrix();
		rotatorgeom.rotate(drawable);
		float torus_diffuse[] = new float[] { 0.7f, 0.7f, 0.0f, 1.0f };
		GLUT glut = new GLUT();
		int res = 64;

//		gl.glScaled(1.4, 1, 0.4);
		
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, torus_diffuse, 0);
		glut.glutSolidTorus(0.275, 2, res, res);
		gl.glPopMatrix();

		if (renderer == null) 
			renderer = new TextRenderer(new Font("Verdana",
				Font.BOLD, 24), true, false);

		gl.glDisable(GL.GL_CULL_FACE);
		renderer.beginRendering(drawable.getWidth(), drawable.getHeight());
		renderer.setColor(0.1f, 0.2f, 1f, 0.9f);
		renderer.draw("This is a 2D text example in Verdana", drawable.getWidth() / 2 - 250,
				drawable.getHeight() - 30);
		renderer.endRendering();

		
		// Draw 3d text
		
		gl.glPushMatrix();
		rotatortext.rotate(drawable);

		gl.glTranslated(-1.85, 0, 0);
		
		Font font3d = new Font("Monospaced", Font.BOLD, 100);
		if (renderer3d == null) 
			renderer3d = new TextRenderer(font3d, true, false);
		
		gl.glDisable(GL.GL_CULL_FACE);
		renderer3d.begin3DRendering();
		renderer3d.setColor(1.0f, 0.2f, 0.2f, 0.8f);
		
		float fsize = 0.003f; 
		renderer3d.draw3D(".------------------.", 0, 0.4f, 0, fsize);
		renderer3d.draw3D("|This is a 3D test |", 0, 0, 0, fsize);
		renderer3d.draw3D("|(using monospaced)|", 0, -0.4f, 0, fsize);
		renderer3d.draw3D("'------------------'", 0, -0.8f, 0, fsize);
		renderer3d.end3DRendering();

		gl.glPopMatrix();
	}

	private void listFonts() {
		GraphicsEnvironment gEnv = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		String envfonts[] = gEnv.getAvailableFontFamilyNames();
		for (int i = 1; i < envfonts.length; i++) {
			System.out.println(envfonts[i]);
		}
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		vfp.add(room.getControls());
		vfp.add(rotatortext.getControls());
		vfp.add(rotatorgeom.getControls());
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		new TextTest(config);
	}

}
