package app.test.gl;

import gl.geometry.primitive.Plane;
import gl.material.GLMaterial;
import gl.renderer.TrackBallCamera;
import gl.texture.CubeMap;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

import com.sun.opengl.util.FPSAnimator;
import com.sun.opengl.util.GLUT;

public class SimpleCubemap extends JFrame implements
		GLEventListener, KeyListener {
	private static final long serialVersionUID = -4925906362074812304L;

	private GLCapabilities caps;
	private GLCanvas canvas;
	private float rotAngle = 0f;
	private boolean rotate = false;
	private Dimension windowDimension = new Dimension();
	
	private FPSAnimator animator;
	private TrackBallCamera camera;

	public SimpleCubemap() {
		caps = new GLCapabilities();

		windowDimension.setSize(1000, 1000);
//		windowDimension.setSize(500, 500);

		caps.setDoubleBuffered(true);
		caps.setHardwareAccelerated(true);
		// caps.setAccumBlueBits(8);
		// caps.setAccumGreenBits(8);
		// caps.setAccumRedBits(8);
		// caps.setAccumAlphaBits(8);
		// caps.setStencilBits(8);
		// caps.setDepthBits(16);
		//

		caps.setSampleBuffers(true);
		caps.setNumSamples(4);
		
//		caps.setAccumBlueBits(16);
//		caps.setAccumGreenBits(16);
//		caps.setAccumRedBits(16);

		canvas = new GLCanvas(caps);
		canvas.addGLEventListener(this);
		canvas.addKeyListener(this);
		
		camera = new TrackBallCamera();
		camera.zoom(500f);
		camera.attach(canvas);
		
		animator = new FPSAnimator(canvas, 60);
		animator.setRunAsFastAsPossible(false);
		animator.start();

		getContentPane().add(canvas);
	}

	public void run() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(windowDimension);
		setLocationRelativeTo(null);
		setVisible(true);
		canvas.requestFocusInWindow();

		System.out.println("Run completed.");
	}

	public static void main(String[] args) {
		new SimpleCubemap().run();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_DONT_CARE);
		gl.glLineWidth(1.5f);
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
				
	    gl.glEnable(GL.GL_AUTO_NORMAL);
	    gl.glEnable(GL.GL_NORMALIZE);

	    gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glDepthFunc(GL.GL_LEQUAL);
	    
	    createCubemap(gl);
	}
	
	private CubeMap cubeMap;
	
	private boolean recreate = false;

	private static GLUT glut = new GLUT();
	
	private void createCubemap(GL gl) {
//		String cubepath = CubemapTest.class.getResource("/gl/resources/probes/cross/outdoor.png").getPath();
		String cubepath = CubemapTest.class.getResource("/gl/resources/probes/cross/uffizi.png").getPath();
//		String cubepath = CubemapTest.class.getResource("/gl/resources/probes/cubemap-diagram.png").getPath();
//		String cubepath = CubemapTest.class.getResource("/gl/resources/probes/cross/cubemap.png").getPath();
//		String cubepath = CubemapTest.class.getResource("/gl/resources/probes/non-cross/mountain/").getPath();
//		cubepath += "mountain";
		
		cubeMap = new CubeMap(cubepath, true, ".png");
		cubeMap.initializeSeed(gl);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
		if (recreate) {
			createCubemap(gl);
			recreate = false;
		}
		
		gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		camera.prepareForDisplay(drawable);
		
		cubeMap.drawCross(drawable);
//		setLights(gl);
		
//		GLMaterial.BRASS.apply(gl);

		cubeMap.drawSkybox(drawable, camera);
		gl.glDisable(GL.GL_CULL_FACE);

        glut.glutSolidTeapot(1, true);

        Plane p = new Plane(new Point3d(0, -1, 0), new Vector3d(0, 1, 0));
        p.setSize(5, 5);
        p.setMaterial(GLMaterial.TRANSPARENT_LIGHT);
        p.setDrawGrid(true);
        p.display(drawable);
        
//		gl.glScaled(1, 1.5, 1);
//		glut.glutSolidSphere(0.5, 64, 64);
		gl.glPopMatrix();

		camera.cleanupAfterDisplay(drawable);
		gl.glFlush();
	}
	

	private void setLights(GL gl ) {
		gl.glEnable(GL.GL_LIGHTING);
		
		int lightNumber = 1;
		Point3f colorSpecDiff1 = new Point3f(1, 1, 1);
		Point3f colorAmbient1 = new Point3f(1f, 1f, 1f);
		colorAmbient1.scale(0.2f);
		
		gl.glLightfv(GL.GL_LIGHT0 + lightNumber, GL.GL_SPECULAR, new float[] {
				colorSpecDiff1.x, colorSpecDiff1.y, colorSpecDiff1.z, 1 }, 0);
		gl.glLightfv(GL.GL_LIGHT0 + lightNumber, GL.GL_DIFFUSE, new float[] {
				colorSpecDiff1.x, colorSpecDiff1.y, colorSpecDiff1.z, 1 }, 0);
		gl.glLightfv(GL.GL_LIGHT0 + lightNumber, GL.GL_AMBIENT, new float[] {
				colorAmbient1.x, colorAmbient1.y, colorAmbient1.z, 1 }, 0);

		// Set the position of the first main light source
		Point3f p2 = new Point3f(0, 0, 2);
		gl.glLightfv(GL.GL_LIGHT0 + lightNumber, GL.GL_POSITION, new float[] {
				p2.x, p2.y, p2.z, 1 }, 0);
		gl.glEnable(GL.GL_LIGHT0 + lightNumber);
	
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
		GL gl = drawable.getGL();
		GLU glu = new GLU();
		gl.glViewport(0, 0, w, h);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		if (w <= h) //
			glu.gluOrtho2D(-1.0, 1.0, -1.0 * h / w, //
					1.0 * h / w);
		else
			glu.gluOrtho2D(-1.0 * w / h, //
					1.0 * w / h, -1.0, 1.0);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	@Override
	public void keyTyped(KeyEvent key) {
	}

	@Override
	public void keyPressed(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		case KeyEvent.VK_R:
			recreate = true;
			canvas.display();
			break;
		case KeyEvent.VK_I:
			animator.stop();
			dispose();
			new SimpleCubemap().run();
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
	}
}