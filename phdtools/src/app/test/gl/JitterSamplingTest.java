 package app.test.gl;

import gl.antialiasing.SceneJitter3D;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Plane;
import gl.renderer.GLFrustum;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import swing.component.ColorChooser;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;

import com.sun.opengl.util.GLUT;

public class JitterSamplingTest implements Interactor, GLObject {

	public static void main(String[] args) {
		GLViewerConfiguration glconfig = new GLViewerConfiguration();
		glconfig.setClearColor(Color.black);
		
		new JitterSamplingTest(glconfig);
	}
	
	private JoglRenderer viewer;
		
	public JitterSamplingTest(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650, (int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Antialiasing", this, dimension, controlSize, true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(400f);
		viewer.start();
	}
	
	private static GLUT glut = new GLUT();

	private boolean updateRequest = true;
	
	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_U) {
					updateRequest = true;
				}
			}
		});
		
		component.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
//				System.out.println("wheeel");
			}
		});
	}
	
	private Plane p1, p2, p3, p4;
	private Point3d[] j1, j2, j3;
	private Vector4d[] j4;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
//		gl.glClearColor(1, 1, 1, 1);
		
//		WorldAxis.display(gl);

		double y = 0;
		double dist = 0.4;
		
		if (p1 == null || updateRequest) 
			p1 = new Plane(new Point3d(0, y, 0), new Vector3d(-0.2, 1, 0.1));
//			p1 = new Plane(new Point3d(0, 0, 1), new Vector3d(0, 1, 0));
		
		y += dist;
		if (p2 == null || updateRequest) 
			p2 = new Plane(new Point3d(0, y, 0), new Vector3d(-0.2, 1, 0.1));

		y += dist;
		if (p3 == null || updateRequest) 
			p3 = new Plane(new Point3d(0, y, 0), new Vector3d(-0.2, 1, 0.1));
//			p3 = new Plane(new Point3d(0, 0, 0), new Vector3d(0, 1, 0));

		y += dist;
		if (p4 == null || updateRequest) 
			p4 = new Plane(new Point3d(0, y, 0), new Vector3d(-0.2, 1, 0.1));

		int n = 5;
		displayJitter(drawable, p4, n, 0);
		displayJitter(drawable, p3, n, 1);
		displayJitter(drawable, p2, n, 2);
		displayJitter(drawable, p1, n, 3);
		
//		GLFrustum.display(drawable);
		
		updateRequest = false;
	}
	
	private void displayJitter(GLAutoDrawable drawable, Plane p, int n, int method) {
		GL gl = drawable.getGL();
		
		p.display(drawable);
		
		// Test jittering on the plane
		gl.glDisable(GL.GL_LIGHTING);
		gl.glPointSize((45 / n));
		gl.glColor3d(1, 0, 0);
		
		Point3d[] jstrat;
		
		if (method == 0) {
			if (updateRequest || j1 == null)
				j1 = SceneJitter3D.jitter_stratified(n, 1, p);
			jstrat = j1;
		}
		else if (method == 1) {
			if (updateRequest || j2 == null)
				j2 = SceneJitter3D.jitter(n, 1, p);
			jstrat = j2;
		}
		else if (method == 2  || j3 == null) {
			if (updateRequest)
				j3 = SceneJitter3D.jitter_poisson(0.05, p);

			jstrat = j3;
		}
		else {
			if (updateRequest || j4 == null)
			 j4 = SceneJitter3D.jitter_poissonWeighted(0.05, p);
		
			gl.glBegin(GL.GL_POINTS);
			for (int i = 0; i < j4.length; i++) {
				Vector4d pt = j4[i];
				gl.glColor4d(1, 0, 0, pt.w);
				gl.glVertex3d(pt.x, pt.y, pt.z);
			}
			gl.glEnd();
			
			return;
		}
		
		if (method < 3) {
			gl.glBegin(GL.GL_POINTS);
			for (int i = 0; i < jstrat.length; i++) {
				Point3d pt = jstrat[i];
				gl.glVertex3d(pt.x, pt.y, pt.z);
			}
			gl.glEnd();
		}
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
	
        ColorChooser cc = new ColorChooser("clearColor");
		vfp.add(cc.getControls(Color.BLACK));

		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
//		GL gl = drawable.getGL();
		
//		gl.glEnable(GL.GL_LIGHTING);
//		gl.glEnable(GL.GL_LIGHT0);
//		gl.glEnable(GL.GL_DEPTH_TEST);
//		gl.glShadeModel(GL.GL_FLAT);
//		
//		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
//		gl.glClearAccum(0.0f, 0.0f, 0.0f, 0.0f);

	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples() + " samples.");
		new JitterSamplingTest(config);
	}
}
