package app.test.gl;

import gl.geometry.GLGeometry;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Cube;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Dimension;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.parameters.IntParameter;
import tools.FPSTimer;

import com.sun.opengl.util.GLUT;

public class FPSBenchmark implements GLObject {

	public static void main(String[] args) {
		new FPSBenchmark(new GLViewerConfiguration());
	}
	
	private final IntParameter samples = new IntParameter("samples", 3, 0, 100);

	GLGeometry geom = new GLGeometry() {
		@Override
		public void display(GLAutoDrawable drawable) {
			GL gl = drawable.getGL();

			// Draw intersecting lines
			gl.glDisable(GL.GL_LIGHTING);
			gl.glColor3d(0, 0, 0);
			gl.glBegin(GL.GL_LINES);
			
			int n = 500;
			double r = 0.8;
			double x0 = -1.3;
			double y0 = 1.3;
			for (int i = 0; i < n; i++) {
				double x1 = x0 + r * Math.cos(i * Math.PI / n);
				double y1 = y0 + r * Math.sin(i * Math.PI / n);

				double x2 = x0 + r * Math.cos(i * Math.PI / n + Math.PI);
				double y2 = y0 + r * Math.sin(i * Math.PI / n + Math.PI);

				gl.glVertex3d(x1, y1, 0);
				gl.glVertex3d(x2, y2, 0);
			}
			gl.glEnd();

			// Draw a point
			gl.glPushMatrix();
			gl.glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
			gl.glTranslatef(0.75f, 0.60f, 0.0f);

			gl.glColor3d(0, 0, 1);
			gl.glPointSize(50.0f);
			gl.glBegin(GL.GL_POINTS);
			
			// gl.glVertex3d(x0 + 1.5, y0 - 1, 2);
			gl.glVertex3d(-0.25, 0.3, 1);
			gl.glEnd();
			gl.glPopMatrix();

			// Draw some geometry repeatedly translated
			gl.glEnable(GL.GL_LIGHTING);
			
			int numt = samples.getValue();
			
			for (int i = 0; i < numt; i++) {
				float torus_diffuse[] = new float[] { 0.7f, 0.7f, 0.0f, 0.5f };
				float cube_diffuse[] = new float[] { 0.0f, 0.7f, 0.7f, 1.0f };
				float sphere_diffuse[] = new float[] { 0.7f, 0.0f, 0.7f, 1.0f };
				float octa_diffuse[] = new float[] { 0.7f, 0.4f, 0.4f, 0.7f };
				
				gl.glPushMatrix();
				gl.glTranslated((1d * i) / numt, 0, 0);
				
				float s0 = 1f;
				gl.glScalef(s0, s0, s0);
				
				gl.glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
				
				int res = 128;
				gl.glPushMatrix();
				
				gl.glTranslatef(-0.75f, -0.50f, 0.0f);
				gl.glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
				gl.glRotatef(45.0f, 1.0f, 0.0f, 0.0f);
				GLMaterial.PEARL.apply(gl);
				gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, cube_diffuse, 0);
				glut.glutSolidCube(1.5f);
				gl.glPopMatrix();
				
				gl.glPushMatrix();
				gl.glTranslatef(-0.80f, 0.35f, 0.0f);
				gl.glRotatef(100.0f, 1.0f, 0.0f, 0.0f);
				GLMaterial.BRASS.apply(gl);
				gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, torus_diffuse, 0);
				
				glut.glutSolidTorus(0.275, 0.85, res, res);
				
				gl.glPopMatrix();
				
				gl.glPushMatrix();
				gl.glTranslatef(0.75f, 0.60f, 0.0f);
				gl.glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
				GLMaterial.POLISHED.apply(gl);
				gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, sphere_diffuse, 0);
				glut.glutSolidSphere(1.0, res, res);
				gl.glPopMatrix();
				
				gl.glPushMatrix();
				gl.glTranslatef(0.70f, -0.90f, 0.25f);
				GLMaterial.RUBY.apply(gl);
				gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, octa_diffuse, 0);
				glut.glutSolidOctahedron();
				gl.glPopMatrix();
				
				gl.glPopMatrix();
			}
		}

		@Override
		public String getName() {
			return "AA test";
		}

		@Override
		public void init(GLAutoDrawable drawable) {
			// TODO Auto-generated method stub

		}
	};

	private JoglRenderer viewer;
	private FPSTimer fpsTimer = new FPSTimer(true);

	public FPSBenchmark(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650,
				(int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "FPS benchmarking", this, dimension,
				controlSize, true);
		viewer.getCamera().zoom(400f);
		viewer.start();
		viewer.setDrawFPS(false);
	}

	private static GLUT glut = new GLUT();

	@Override
	public void display(GLAutoDrawable drawable) {
		fpsTimer.tick();
		
		GL gl = drawable.getGL();

		gl.glClearColor(1, 1, 1, 1);

		WorldAxis.display(gl);

		geom.display(drawable);
		viewer.getTextRenderer().setColor(0.9f, 0.9f, 0f, 1f);
		viewer.getTextRenderer().drawTopRight(fpsTimer.toString(), drawable);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(samples.getSliderControls());
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
	
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples()
				+ " samples.");
		new FPSBenchmark(config);
	}
}
