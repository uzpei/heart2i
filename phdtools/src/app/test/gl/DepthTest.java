package app.test.gl;

import gl.geometry.Box;
import gl.geometry.GLObject;
import gl.geometry.primitive.Plane;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.ColorChooser;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;

import com.sun.opengl.util.GLUT;

public class DepthTest implements Interactor, GLObject {

	public static void main(String[] args) {
		new DepthTest(new GLViewerConfiguration());
	}

	private JoglRenderer viewer;

	public DepthTest(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650,
				(int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer("Antialiasing", this, dimension, controlSize,
				true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(400f);
		viewer.start();
	}

	/* 8 jitter points */
	public final static Point2d j8[] = { new Point2d(-0.334818, 0.435331),
			new Point2d(0.286438, -0.393495), new Point2d(0.459462, 0.141540),
			new Point2d(-0.414498, -0.192829),
			new Point2d(-0.183790, 0.082102),
			new Point2d(-0.079263, -0.317383), new Point2d(0.102254, 0.299133),
			new Point2d(0.164216, -0.054399) };

	private static GLUT glut = new GLUT();

	private boolean updateRequest = true;

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_U) {
					updateRequest = true;
				}
			}
		});
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();

//		 int jitter;
//		   int[] viewport = new int[4];
//
//		   gl.glGetIntegerv (GL.GL_VIEWPORT, viewport, 0);
//		   gl.glClear(GL.GL_ACCUM_BUFFER_BIT);
//
//		   for (jitter = 0; jitter < 8; jitter++) {
//			   
//			     gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
//			      GLFrustum.accPerspective (drawable, 45.0, 
//			         (double) viewport[2]/(double) viewport[3], 
//			         1.0, 15.0, 0.0, 0.0,
//			         0.33*DepthTest.j8[jitter].x, 0.33*DepthTest.j8[jitter].y, 5.0);
//			      
//			      displayGeom(drawable);
//			      
//			      gl.glAccum (GL.GL_ACCUM, 0.125f);
//             }
//		   gl.glAccum(GL.GL_RETURN, 1.0f);
	      displayGeom(drawable);
		   
	}

	private void displayGeom(GLAutoDrawable drawable) {
		
		Vector3d span = new Vector3d(1, 2, 1);
		double[] color = new double[] { 1, 0, 0, 1 };
		float lineWidth = 5f;

		double dy = -0.2;
		int n = 5;

		for (int i = 0; i < n; i++) {
			 Plane p = new Plane(new Point3d(0, i * dy, 0), new Vector3d(-0.2,
			 1, 0.1));
			 p.display(drawable);
//			Box.display(drawable, new Point3d(i * dy, i * dy, i * dy * 6),
//					span, color, lineWidth, false);
		}
		
	}
	
	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		ColorChooser cc = new ColorChooser("clearColor");
		vfp.add(cc.getControls(Color.BLACK));

		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();

		// gl.glEnable(GL.GL_LIGHTING);
		// gl.glEnable(GL.GL_LIGHT0);
		// gl.glEnable(GL.GL_DEPTH_TEST);
		// gl.glShadeModel(GL.GL_FLAT);
		//		
		// gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		// gl.glClearAccum(0.0f, 0.0f, 0.0f, 0.0f);

	}

	@Override
	public void reload(GLViewerConfiguration config) {
		new DepthTest(config);
	}
}
