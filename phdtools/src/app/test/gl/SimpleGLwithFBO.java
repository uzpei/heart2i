package app.test.gl;

import gl.geometry.LightRoom;
import gl.material.GLMaterial;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

import com.sun.opengl.util.FPSAnimator;
import com.sun.opengl.util.GLUT;

public class SimpleGLwithFBO extends JFrame implements GLEventListener,
		KeyListener {
	private GLCapabilities caps;
	private GLCanvas canvas;
	private float rotAngle = 0f;
	private boolean rotate = false;
	private Dimension fboDimension = new Dimension();
	private Dimension windowDimension = new Dimension();

	private FPSAnimator animator;
	private int frameBufferObject = -1;
	private int msamples = 8;

	public SimpleGLwithFBO() {
		super("aargb");

		caps = new GLCapabilities();

		/*
		 * If either the draw or read framebuffer is framebuffer complete and has
		 * a value of SAMPLE_BUFFERS that is greater than zero, then the error
		 * INVALID_OPERATION is generated if BlitFramebufferEXT is called and
		 * the specified source and destination dimensions are not identical.
		 */
		windowDimension.setSize(1000, 1000);
		fboDimension.setSize(windowDimension);

		// caps.setDoubleBuffered(true);
		// caps.setHardwareAccelerated(true);
		// caps.setAccumBlueBits(8);
		// caps.setAccumGreenBits(8);
		// caps.setAccumRedBits(8);
		// caps.setAccumAlphaBits(8);
		// caps.setStencilBits(8);
		// caps.setDepthBits(16);
		//
		// //try Hardware fsaa
		// caps.setSampleBuffers(true);
		// caps.setNumSamples(8);

		canvas = new GLCanvas(caps);
		canvas.addGLEventListener(this);
		canvas.addKeyListener(this);

		animator = new FPSAnimator(canvas, 60);
		animator.setRunAsFastAsPossible(false);
		animator.start();

		getContentPane().add(canvas);
	}

	public void run() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(windowDimension);
		setLocationRelativeTo(null);
		setVisible(true);
		canvas.requestFocusInWindow();

		System.out.println("Run completed.");
	}

	public static void main(String[] args) {
		new SimpleGLwithFBO().run();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		//
		float values[] = new float[2];
		gl.glGetFloatv(GL.GL_LINE_WIDTH_GRANULARITY, values, 0);
		System.out
				.println("GL.GL_LINE_WIDTH_GRANULARITY value is " + values[0]);
		gl.glGetFloatv(GL.GL_LINE_WIDTH_RANGE, values, 0);
		System.out.println("GL.GL_LINE_WIDTH_RANGE values are " + values[0]
				+ ", " + values[1]);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_DONT_CARE);
		gl.glLineWidth(1.5f);
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gl.glEnable(GL.GL_CULL_FACE);

		initFramebuffering(gl);

		System.out.println("Init completed.");
	}

	private void initFramebuffering(GL gl) {
		gl.glEnable(GL.GL_MULTISAMPLE_EXT);
		gl.glEnable(GL.GL_MULTISAMPLE);

		/*
		 * glGenFramebuffersEXT() requires 2 parameters; the first one is the
		 * number of framebuffers to create, and the second parameter is the
		 * pointer to a GLuint variable or an array to store a single ID or
		 * multiple IDs. It returns the IDs of unused framebuffer objects. ID 0
		 * means the default framebuffer, which is the window-system-provided
		 * framebuffer. And, FBO may be deleted by calling
		 * glDeleteFramebuffersEXT() when it is not used anymore.
		 */
		int[] fboId = new int[1];
		gl.glGenFramebuffersEXT(1, fboId, 0);

		/*
		 * Once a FBO is created, it has to be bound before using it. The first
		 * parameter, target, should be GL_FRAMEBUFFER_EXT, and the second
		 * parameter is the ID of a framebuffer object. Once a FBO is bound, all
		 * OpenGL operations affect onto the current bound framebuffer object.
		 * The object ID 0 is reserved for the default window-system provided
		 * framebuffer. Therefore, in order to unbind the current framebuffer
		 * (FBO), use ID 0 in glBindFramebufferEXT().
		 */
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT, fboId[0]);
		frameBufferObject = fboId[0];

		/*
		 * Once a renderbuffer is created, it returns non-zero positive integer.
		 * ID 0 is reserved for OpenGL.
		 */
		int[] colorId = new int[1];
		gl.glGenRenderbuffersEXT(1, colorId, 0);

		/*
		 * Same as other OpenGL objects, you have to bind the current
		 * renderbuffer object before referencing it. The target parameter
		 * should be GL_RENDERBUFFER_EXT for renderbuffer object.
		 */
		gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, colorId[0]);

		/*
		 * When a renderbuffer object is created, it does not have any data
		 * storage, so we have to allocate a memory space for it. This can be
		 * done by using glRenderbufferStorageEXT(). The first parameter must be
		 * GL_RENDERBUFFER_EXT. The second parameter would be color-renderable
		 * (GL_RGB, GL_RGBA, etc.), depth-renderable (GL_DEPTH_COMPONENT), or
		 * stencil-renderable formats (GL_STENCIL_INDEX). The width and height
		 * are the dimension of the renderbuffer image in pixels.
		 */

		// target, format, width, height
		// gl.glRenderbufferStorageEXT(GL.GL_RENDERBUFFER_EXT, GL.GL_RGBA,
		// dimension.width, dimension.height);
		gl.glRenderbufferStorageMultisampleEXT(GL.GL_RENDERBUFFER_EXT,
				msamples, GL.GL_RGBA, fboDimension.width, fboDimension.height);

		/*
		 * Attaching images to FBO
		 * 
		 * FBO itself does not have any image storage(buffer) in it. Instead, we
		 * must attach framebuffer-attachable images (texture or renderbuffer
		 * objects) to the FBO. This mechanism allows that FBO quickly switch
		 * (detach and attach) the framebuffer-attachable images in a FBO. It is
		 * much faster to switch framebuffer-attachable images than to switch
		 * between FBOs. And, it saves unnecessary data copies and memory
		 * consumption. For example, a texture can be attached to multiple FBOs,
		 * and its image storage can be shared by multiple FBOs.
		 */

		/*
		 * Attaching a 2D texture image to FBO. glFramebufferTexture2DEXT() is
		 * to attach a 2D texture image to a FBO. The first parameter must be
		 * GL_FRAMEBUFFER_EXT, and the second parameter is the attachment point
		 * where to connect the texture image. A FBO has multiple color
		 * attachment points (GL_COLOR_ATTACHMENT0_EXT, ...,
		 * GL_COLOR_ATTACHMENTn_EXT), GL_DEPTH_ATTACHMENT_EXT, and
		 * GL_STENCIL_ATTACHMENT_EXT. The third parameter, "textureTarget" is
		 * GL_TEXTURE_2D in most cases. The fourth parameter is the identifier
		 * of the texture object. The last parameter is the mipmap level of the
		 * texture to be attached.
		 * 
		 * If the textureId parameter is set to 0, then, the texture image will
		 * be detached from the FBO. If a texture object is deleted while it is
		 * still attached to a FBO, then, the texture image will be
		 * automatically detached from the currently bound FBO. However, if it
		 * is attached to multiple FBOs and deleted, then it will be detached
		 * from only the bound FBO, but will not be detached from any other
		 * un-bound FBOs.
		 */
		// gl.glFramebufferTexture2DEXT(GL.GL_FRAMEBUFFER_EXT, attachment,
		// GL.GL_TEXTURE_2D, texture, mipmapLevel);

		/*
		 * Attaching a Renderbuffer image to FBO. A renderbuffer image can be
		 * attached by calling glFramebufferRenderbufferEXT(). The first and
		 * second parameters are same as glFramebufferTexture2DEXT(). The third
		 * parameter must be GL_RENDERBUFFER_EXT, and the last parameter is the
		 * ID of the renderbuffer object.
		 * 
		 * If renderbufferId parameter is set to 0, the renderbuffer image will
		 * be detached from the attachment point in the FBO. If a renderbuffer
		 * object is deleted while it is still attached in a FBO, then it will
		 * be automatically detached from the bound FBO. However, it will not be
		 * detached from any other non-bound FBOs.
		 */
		gl.glFramebufferRenderbufferEXT(GL.GL_FRAMEBUFFER_EXT,
				GL.GL_COLOR_ATTACHMENT0_EXT, GL.GL_RENDERBUFFER_EXT, colorId[0]);

		int[] depthId = new int[1];
		gl.glGenRenderbuffersEXT(1, depthId, 0);
		gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, depthId[0]);
		gl.glRenderbufferStorageMultisampleEXT(GL.GL_RENDERBUFFER_EXT,
				msamples, GL.GL_DEPTH_COMPONENT, fboDimension.width,
				fboDimension.height);

		gl.glFramebufferRenderbufferEXT(GL.GL_FRAMEBUFFER_EXT,
				GL.GL_DEPTH_ATTACHMENT_EXT, GL.GL_RENDERBUFFER_EXT, depthId[0]);

		// Unload
		// gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT, 0);
		// gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, 0);

	}

	private static GLUT glut = new GLUT();
	private LightRoom room = new LightRoom(2.0);

	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
		if (initRequest) {
			initFramebuffering(gl);
			initRequest = false;
		}

		// Clear the main buffer
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT, 0);
		gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// Draw into the FBO
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT, frameBufferObject);

		gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		drawScene(drawable);

		// Display the FBO into which the scene was drawn.
		// (blit content into textures)
		gl.glBindFramebufferEXT(GL.GL_READ_FRAMEBUFFER_EXT, frameBufferObject);

		gl.glBindFramebufferEXT(GL.GL_DRAW_FRAMEBUFFER_EXT, 0);

		gl.glBlitFramebufferEXT(0, 0, fboDimension.width, fboDimension.height,
				0, 0, windowDimension.width, windowDimension.height,
				GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT, GL.GL_NEAREST);
		// GL.GL_LINEAR);

		gl.glBindFramebufferEXT(GL.GL_READ_FRAMEBUFFER_EXT, 0);
		gl.glBindFramebufferEXT(GL.GL_DRAW_FRAMEBUFFER_EXT, 0);

		gl.glFlush();
	}

	private void drawScene(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();

		room.display(drawable);

		gl.glDisable(GL.GL_LIGHTING);

		gl.glLineWidth(15f);

		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glPushMatrix();
		gl.glRotatef(-rotAngle, 0.0f, 0.0f, 0.1f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex2f(-0.5f, 0.5f);
		gl.glVertex2f(0.5f, -0.5f);
		gl.glEnd();
		gl.glPopMatrix();
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glPushMatrix();
		gl.glRotatef(rotAngle, 0.0f, 0.0f, 0.1f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex2f(0.5f, 0.5f);
		gl.glVertex2f(-0.5f, -0.5f);
		gl.glEnd();
		gl.glPopMatrix();

		int sn = 24;
		gl.glEnable(GL.GL_LIGHTING);
		gl.glPushMatrix();
		gl.glTranslated(0, -0.7, 0);
		gl.glRotatef(-rotAngle, 1.0f, 0.0f, 0.1f);
		GLMaterial.RUBY.apply(gl);
		// glut.glutSolidTorus(0.05f, 0.8f, sn, sn);
		glut.glutSolidTorus(0.2f, 1.2f, sn, sn);
		gl.glPopMatrix();

		gl.glPushMatrix();
		gl.glTranslated(0, -0.7, 0);
		gl.glRotatef(rotAngle, 1.0f, 0.0f, 0.1f);
		gl.glTranslated(0.5, 0.7, 0);
		GLMaterial.TURQUOISE.apply(gl);
		glut.glutSolidTorus(0.2f, 1.2f, sn, sn);
		gl.glPopMatrix();

		if (rotate)
			rotAngle += 0.2f;
		if (rotAngle >= 360f)
			rotAngle = 0f;

		String s = "";
		// s += System.nanoTime() + "\n";
		s += "samples = " + msamples;
		JoglRenderer.beginOverlay(drawable);
		JoglTextRenderer.printTextLines(drawable, s, 10, 30, new float[] { 0, 1, 0,
				1 }, GLUT.BITMAP_TIMES_ROMAN_24);
		JoglRenderer.endOverlay(drawable);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
		GL gl = drawable.getGL();
		GLU glu = new GLU();
		gl.glViewport(0, 0, w, h);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		if (w <= h) //
			glu.gluOrtho2D(-1.0, 1.0, -1.0 * h / w, //
					1.0 * h / w);
		else
			glu.gluOrtho2D(-1.0 * w / h, //
					1.0 * w / h, -1.0, 1.0);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	@Override
	public void keyTyped(KeyEvent key) {
	}

	private boolean initRequest = false;

	@Override
	public void keyPressed(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		case KeyEvent.VK_R:
			rotate = !rotate;
			canvas.display();
			break;
		case KeyEvent.VK_I:
			animator.stop();
			dispose();
			new SimpleGLwithFBO().run();
			break;
		case KeyEvent.VK_Q:
			msamples = (msamples + 1) % 9;
			initRequest = true;
			break;

		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
	}
}