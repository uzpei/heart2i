 package app.test.gl;

import gl.geometry.GLObject;
import gl.geometry.primitive.Plane;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import tools.FPSTimer;

import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

public class PlaneRoomTest implements Interactor, GLObject {

	public static void main(String[] args) {

		GLViewerConfiguration glconfig = new GLViewerConfiguration();
		glconfig.setClearColor(Color.white);

		glconfig.setCullFace(false);
		new PlaneRoomTest(glconfig);
	}
	
	private JoglRenderer viewer;
		
	private Texture floorTexture;

	private Texture wallTexture;

	private Texture potTexture;

	public PlaneRoomTest(GLViewerConfiguration config) {
		
		Dimension dimension = new Dimension(1200, 1200);
		Dimension controlSize = new Dimension(650, (int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Antialiasing", this, dimension, controlSize, true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(0f);
		viewer.start();
		
		viewer.getRoom().getBoxRoom().setVisible(false);
		
		viewer.getRotator().enableRotations(true, true, false);
	}
	
	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_U) {
				}
			}
		});
		
	}
	
	private FPSTimer fps = new FPSTimer();
	
	private Plane floor, ceiling, back, left, right, front;
	private void initPlanes() {
		double size = 15;
		
		floor = new Plane(new Point3d(0, -size, 0), new Vector3d(0, 1, 0));
		floor.setSize(2*size, 2*size);
		floor.debug = true;
		floor.setTexture(floorTexture);

		ceiling = new Plane(new Point3d(0,  size, 0), new Vector3d(0, -1, 0));
		ceiling.setSize(2*size, 2*size);
		ceiling.debug = true;
		ceiling.setTexture(wallTexture);

		back = new Plane(new Point3d(0, 0, -size), new Vector3d(0, 0, 1));
		back.setSize(2*size, 2*size);
		back.setTexture(wallTexture);
		back.debug = true;

		left = new Plane(new Point3d(-size, 0, 0), new Vector3d(1, 0, 0));
		left.setSize(2*size, 2*size);
		left.setTexture(wallTexture);
		left.debug = true;

		right = new Plane(new Point3d(size, 0, 0), new Vector3d(-1, 0, 0));
		right.setSize(2*size, 2*size);
		right.setTexture(wallTexture);
		right.debug = true;

		front = new Plane(new Point3d(0, 0, size), new Vector3d(0, 0, -1));
		front.setSize(2*size, 2*size);
		front.setTexture(wallTexture);
		front.debug = true;
  }
	
	@Override
	public void display(GLAutoDrawable drawable) {
		fps.tick();

		GL gl = drawable.getGL();

//		WorldAxis.display(gl);

		GLMaterial fmat = GLMaterial.CHROME;
		GLMaterial wmat = GLMaterial.BRASS;
		
		floor.setMaterial(fmat);
		ceiling.setMaterial(wmat);
		back.setMaterial(wmat);
		left.setMaterial(wmat);
		right.setMaterial(wmat);
		front.setMaterial(wmat);
		
		floor.display(drawable);
		ceiling.display(drawable);
		back.display(drawable);
		left.display(drawable);
		right.display(drawable);
		front.display(drawable);

        JoglRenderer.beginOverlay(drawable);
        JoglTextRenderer.printTextLines(drawable, fps.toString(), 10, 30,  new float[] { 1, 1, 0, 0}, GLUT.BITMAP_HELVETICA_18 );
        JoglRenderer.endOverlay(drawable);
	}
	

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
	
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		initTextures();
		initPlanes();
	}
	
	private void initTextures() {
		try {
			File ffile = new File("./src/gl/resources/diamond_plate_texture.jpg");
//			File ffile = new File("./src/gl/resources/floor_texture.png");
			File wfile = new File("./src/gl/resources/metal_texture.jpg");

			System.out.println("Texture found at " + ffile.getCanonicalPath() +  " ? " + (ffile.exists() ? "YES" : "NO"));
			System.out.println("Texture found at " + wfile.getCanonicalPath() +  " ? " + (wfile.exists() ? "YES" : "NO"));
			
			floorTexture = TextureIO.newTexture(ffile, false);
	        
			wallTexture = TextureIO.newTexture(wfile, false);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	
	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples() + " samples.");
		new PlaneRoomTest(config);
	}
}
