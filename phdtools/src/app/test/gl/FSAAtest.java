package app.test.gl;

import gl.antialiasing.GLMultisampler;
import gl.geometry.GLGeometry;
import gl.geometry.LightRoom;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.renderer.TrackBallCamera;
import gl.texture.GLTexture;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.vecmath.Point3d;

import com.sun.opengl.util.FPSAnimator;
import com.sun.opengl.util.GLUT;

public class FSAAtest extends JFrame implements
		GLEventListener, KeyListener {
	private static final long serialVersionUID = -4925906362074812304L;

	private GLCapabilities caps;
	private GLCanvas canvas;
	private float rotAngle = 0f;
	private boolean rotate = false;
	private Dimension windowDimension = new Dimension();

	private FPSAnimator animator;

	private static int msamples = 0;
	private static int factor = 0;
	
	private GLViewerConfiguration glconfig = new GLViewerConfiguration();

	public FSAAtest(int msamples, int factor) {
		super("Multisampled + Downsampled FBO Test");

		caps = new GLCapabilities();

		windowDimension.setSize(1000, 1000);

		caps.setSampleBuffers(false);
		caps.setNumSamples(0);

		multisampler = null;
		
		// Accumulation bits per channel
		int DEFAULT_ACCUM_BITS = 16;
		caps.setAccumBlueBits(DEFAULT_ACCUM_BITS);
		caps.setAccumGreenBits(DEFAULT_ACCUM_BITS);
		caps.setAccumRedBits(DEFAULT_ACCUM_BITS);
		caps.setDoubleBuffered(true);
		caps.setHardwareAccelerated(true);

		canvas = new GLCanvas(caps);
		canvas.addGLEventListener(this);
		canvas.addKeyListener(this);

		animator = new FPSAnimator(canvas, 60);
		animator.setRunAsFastAsPossible(false);
		animator.start();

		getContentPane().add(canvas);
		
		trackBall.attach(canvas);
		trackBall.zoom(600f);
	}

	public void run() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(windowDimension);
		setLocationRelativeTo(null);
		setVisible(true);
		canvas.requestFocusInWindow();

		System.out.println("Run completed.");
	}

	public static void main(String[] args) {
		new FSAAtest(msamples, factor).run();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		//
		float values[] = new float[2];
		gl.glGetFloatv(GL.GL_LINE_WIDTH_GRANULARITY, values, 0);
		System.out
				.println("GL.GL_LINE_WIDTH_GRANULARITY value is " + values[0]);
		gl.glGetFloatv(GL.GL_LINE_WIDTH_RANGE, values, 0);
		System.out.println("GL.GL_LINE_WIDTH_RANGE values are " + values[0]
				+ ", " + values[1]);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_DONT_CARE);
		gl.glLineWidth(1.5f);
		
//		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		gl.glClearColor(1, 1, 1, 0);

		gl.glEnable(GL.GL_CULL_FACE);
		
		geometry.init(drawable);
	
		glconfig.init(drawable);
	}
	
	private static GLUT glut = new GLUT();
	private LightRoom room;

	private GLGeometry geometry = new GLGeometry() {
		
		private GLTexture floorTexture;

		@Override
		public void init(GLAutoDrawable drawable) {
			floorTexture = new GLTexture("./src/gl/resources/floor_texture.png");
			floorTexture.setMaterial(GLMaterial.BRASS);
			
			room = new LightRoom(10);
			room.init(drawable);
		}
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public void display(GLAutoDrawable drawable) {
			GL gl = drawable.getGL();
			room.setVisible(true);
			room.getBoxRoom().setMaterials(GLMaterial.TRANSPARENT_DARK, GLMaterial.TRANSPARENT_LIGHT);

			room.display(drawable);
			
			floorTexture.apply(gl);
			
			// Draw lines of different widths
			gl.glPushMatrix();

			float minw = 1;
			float maxw = 20f;
			double maxd = 0;
			int numlines = 30;
			gl.glDisable(GL.GL_LIGHTING);
			gl.glColor4d(0, 0.8, 1, 0.8);
			double minx = 0.07 * numlines;
			Point3d pa = new Point3d(-minx/2, -1, 0);
			Point3d pb = new Point3d(-minx/2, 1, 0);
			
	        gl.glTranslated(minx / 2, 0, 0);
        	gl.glRotated(-rotAngle, 0, 0, 1);

			for (int i = 0; i < numlines; i++) {
				gl.glLineWidth(minw + i * (maxw - minw) / numlines);
				gl.glBegin(GL.GL_LINES);
				gl.glVertex3d(pa.x, pa.y, pa.z);
				gl.glVertex3d(pb.x, pb.y, pb.z);
				gl.glEnd();
				
				pa.x += 0.07;
				pb.x = pa.x;
				maxd = pa.x;
			}
			gl.glPopMatrix();
			
			gl.glEnable(GL.GL_LIGHTING);
	        gl.glPushMatrix();

	        gl.glPushMatrix();

	        gl.glRotated(-rotAngle, 0, 1, 0);
	        gl.glTranslated(0, -0.2, 0);
	        gl.glRotated(75, 1, 1, 1);
	        double s = 0.5;
	        gl.glScaled(s, s, s);
	        drawCube(gl);
	        gl.glPopMatrix();

	        gl.glPushMatrix();
        	gl.glRotated(rotAngle, 0, 1, 0);
	        gl.glRotated(75, 1, 1, 1);
	        gl.glScaled(s, s, s);
	        drawCube(gl);
	        gl.glPopMatrix();
	        gl.glPopMatrix();
	        
	        if (rotate) {
	        	rotAngle += 0.4f;
	        }
		}
	};

	private void drawCube(GL gl) {
        gl.glBegin(GL.GL_QUADS);
        // Front Face
        gl.glNormal3d(0, 0, 1);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, 1.0f);
        // Back Face
        gl.glNormal3d(0, 0, -1);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, -1.0f);
        // Top Face
        gl.glNormal3d(0, 1, 0);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, -1.0f);
        // Bottom Face
        gl.glNormal3d(0, -1, 0);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, 1.0f);
        // Right face
        gl.glNormal3d(1, 0, 0);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, 1.0f);
        // Left Face
        gl.glNormal3d(-1, 0, 0);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl.glEnd();
 	}

	private GLMultisampler multisampler = null;

    private TrackBallCamera trackBall = new TrackBallCamera();

	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
        trackBall.prepareForDisplay(drawable);

		if (multisampler == null) {
			multisampler = new GLMultisampler(gl, msamples, factor);
		}
		
		multisampler.apply(drawable, geometry);
		
		String s = "";
		s += System.nanoTime() + "\n";
		s += "samples = " + msamples;
		s += "\nfactor  = " + factor;
		JoglRenderer.beginOverlay(drawable);
		JoglTextRenderer.printTextLines(drawable, s, 10, 20, new float[] { 0, 1, 0,
				1 }, GLUT.BITMAP_9_BY_15);
		JoglRenderer.endOverlay(drawable);

        trackBall.cleanupAfterDisplay(drawable);

		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
		GL gl = drawable.getGL();
		GLU glu = new GLU();
		gl.glViewport(0, 0, w, h);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		if (w <= h) //
			glu.gluOrtho2D(-1.0, 1.0, -1.0 * h / w, //
					1.0 * h / w);
		else
			glu.gluOrtho2D(-1.0 * w / h, //
					1.0 * w / h, -1.0, 1.0);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	@Override
	public void keyTyped(KeyEvent key) {
	}

	@Override
	public void keyPressed(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		case KeyEvent.VK_R:
			rotate = !rotate;
			canvas.display();
			break;
		case KeyEvent.VK_I:
			animator.stop();
			dispose();
			new FSAAtest(msamples, factor).run();
			break;
		case KeyEvent.VK_Q:
			msamples = (msamples + 1) % 9;
			break;
		case KeyEvent.VK_W:
			factor = factor % 4 + 1;
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
	}
}