 package app.test.gl;

import gl.geometry.GLObject;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.texture.GLTexture;

import java.awt.Component;
import java.awt.Dimension;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import config.GraphicsToolsConstants;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import tools.FPSTimer;

public class TextureTest implements Interactor, GLObject {

	public static void main(String[] args) {

		new TextureTest();
	}
	
	private JoglRenderer viewer;
		
	public TextureTest() {
		
		Dimension dimension = new Dimension(1000, 1000);
		viewer = new JoglRenderer("Texture Test");
		viewer.addInteractor(this);
		viewer.getCamera().zoom(0f);
		viewer.start(this);
		
//		viewer.getRoom().getBoxRoom().setVisible(false);
	}

	private GLTexture floorTexture;
	
	@Override
	public void attach(Component component) {
	}
	
	private FPSTimer fps = new FPSTimer();

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		gl.glEnable(GL.GL_TEXTURE_2D);
		
        // Init texture.
		if (floorTexture == null) 
			floorTexture = new GLTexture(GraphicsToolsConstants.METAL_TEXTURE_PATH);

	}

	@Override
	public void display(GLAutoDrawable drawable) {
		fps.tick();

		GL2 gl = drawable.getGL().getGL2();

		floorTexture.setMaterial(GLMaterial.CHROME);
		floorTexture.apply(gl);

//		gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE);  
//		gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);  
//		gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_ADD);  
//		gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_COMBINE);
//		gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE);  
//		gl.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_BLEND);  

        // Display a cube
        gl.glPushMatrix();
        gl.glScaled(2, 2, 2);
        drawCube(gl);
        gl.glPopMatrix();
        
//        JoglTextRenderer.printTextLines(drawable, fps.toString(), 10, 30,  new float[] { 1, 1, 0, 0}, GLUT.BITMAP_HELVETICA_18 );
	}
	

	private void drawCube(GL2 gl) {
        gl.glBegin(GL2.GL_QUADS);
        // Front Face
        gl.glNormal3d(0, 0, 1);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, 1.0f);
        // Back Face
        gl.glNormal3d(0, 0, -1);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, -1.0f);
        // Top Face
        gl.glNormal3d(0, 1, 0);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, -1.0f);
        // Bottom Face
        gl.glNormal3d(0, -1, 0);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, 1.0f);
        // Right face
        gl.glNormal3d(1, 0, 0);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, -1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(1.0f, -1.0f, 1.0f);
        // Left Face
        gl.glNormal3d(-1, 0, 0);

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3f(-1.0f, -1.0f, 1.0f);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, 1.0f);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl.glEnd();
 	}


	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
	
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples() + " samples.");
		new TextureTest();
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
