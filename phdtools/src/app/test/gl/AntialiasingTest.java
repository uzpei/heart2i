package app.test.gl;

import gl.geometry.GLGeometry;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Plane;
import gl.material.GLMaterial;
import gl.renderer.GLFrustum;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector4d;

import swing.component.ColorChooser;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;

import com.jogamp.opengl.util.gl2.GLUT;

public class AntialiasingTest implements Interactor, GLObject {

	public static void main(String[] args) {
		new AntialiasingTest(new GLViewerConfiguration());
	}

	private JoglRenderer viewer;

	// private IntParameter numsamples = new IntParameter("number of samples",
	// 4, 0, 32);
	// private IntDiscreteParameter numjitters = new
	// IntDiscreteParameter("number of jitters", 4,
	// JITTER_NUMBER.getJitterSamples());
	GLGeometry geom = new GLGeometry() {
		@Override
		public void display(GLAutoDrawable drawable) {
			GL2 gl = drawable.getGL().getGL2();

			// Draw intersecting lines
			gl.glDisable(GL2.GL_LIGHTING);
			gl.glColor3d(0, 0, 0);
			gl.glBegin(GL2.GL_LINES);
			int n = 50;
			double r = 0.8;
			double x0 = -1.3;
			double y0 = 1.3;
			for (int i = 0; i < n; i++) {
				double x1 = x0 + r * Math.cos(i * Math.PI / n);
				double y1 = y0 + r * Math.sin(i * Math.PI / n);

				double x2 = x0 + r * Math.cos(i * Math.PI / n + Math.PI);
				double y2 = y0 + r * Math.sin(i * Math.PI / n + Math.PI);

				gl.glVertex3d(x1, y1, 0);
				gl.glVertex3d(x2, y2, 0);
			}
			gl.glEnd();

			// Draw a point
			gl.glPushMatrix();
			gl.glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
			gl.glTranslatef(0.75f, 0.60f, 0.0f);

			gl.glColor3d(0, 0, 1);
			gl.glPointSize(50.0f);
			gl.glBegin(GL2.GL_POINTS);
			// gl.glVertex3d(x0 + 1.5, y0 - 1, 2);
			gl.glVertex3d(-0.25, 0.3, 1);
			gl.glEnd();
			gl.glPopMatrix();

			// Draw some geometry
			gl.glEnable(GL2.GL_LIGHTING);
			
			float torus_diffuse[] = new float[] { 0.7f, 0.7f, 0.0f, 0.5f };
			float cube_diffuse[] = new float[] { 0.0f, 0.7f, 0.7f, 1.0f };
			float sphere_diffuse[] = new float[] { 0.7f, 0.0f, 0.7f, 1.0f };
			float octa_diffuse[] = new float[] { 0.7f, 0.4f, 0.4f, 0.7f };

			gl.glPushMatrix();
			float s0 = 1f;
			gl.glScalef(s0, s0, s0);

			gl.glRotatef(30.0f, 1.0f, 0.0f, 0.0f);

			int res = 128;
			gl.glPushMatrix();
			
			gl.glTranslatef(-0.75f, -0.50f, 0.0f);
			gl.glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
			gl.glRotatef(45.0f, 1.0f, 0.0f, 0.0f);
			GLMaterial.PEARL.apply(gl);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, cube_diffuse, 0);
			glut.glutSolidCube(1.5f);
			gl.glPopMatrix();

			gl.glPushMatrix();
			gl.glTranslatef(-0.80f, 0.35f, 0.0f);
			gl.glRotatef(100.0f, 1.0f, 0.0f, 0.0f);
			GLMaterial.BRASS.apply(gl);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, torus_diffuse, 0);
			
			glut.glutSolidTorus(0.275, 0.85, res, res);
			
			gl.glPopMatrix();

			gl.glPushMatrix();
			gl.glTranslatef(0.75f, 0.60f, 0.0f);
			gl.glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
			GLMaterial.POLISHED.apply(gl);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, sphere_diffuse, 0);
			glut.glutSolidSphere(1.0, res, res);
			gl.glPopMatrix();

			gl.glPushMatrix();
			gl.glTranslatef(0.70f, -0.90f, 0.25f);
			GLMaterial.RUBY.apply(gl);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, octa_diffuse, 0);
			glut.glutSolidOctahedron();
			gl.glPopMatrix();

			gl.glPopMatrix();
		}

		@Override
		public String getName() {
			return "AA test";
		}

		@Override
		public void init(GLAutoDrawable drawable) {
			// TODO Auto-generated method stub

		}
	};

	public AntialiasingTest(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650,
				(int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Antialiasing", this, dimension,
				controlSize, true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(400f);
		viewer.start();
	}

	private static GLUT glut = new GLUT();

	private boolean updateRequest = true;

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_U) {
					updateRequest = true;
				}
			}
		});
	}

	private Plane p1, p2, p3, p4;
	private Point3d[] j1, j2, j3;
	private Vector4d[] j4;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glClearColor(1, 1, 1, 1);

		WorldAxis.display(gl);

		geom.display(drawable);

		GLFrustum.display(drawable);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		ColorChooser cc = new ColorChooser("clearColor");
		vfp.add(cc.getControls(Color.BLACK));

		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// GL gl = drawable.getGL();

		// gl.glEnable(GL2.GL_LIGHTING);
		// gl.glEnable(GL2.GL_LIGHT0);
		// gl.glEnable(GL2.GL_DEPTH_TEST);
		// gl.glShadeModel(GL2.GL_FLAT);
		//
		// gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		// gl.glClearAccum(0.0f, 0.0f, 0.0f, 0.0f);
		
//		JoglContextCapabilities jcc = new JoglContextCapabilities(drawable);
		
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples()
				+ " samples.");
		new AntialiasingTest(config);
	}
}
