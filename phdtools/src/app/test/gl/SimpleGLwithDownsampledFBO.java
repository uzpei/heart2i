package app.test.gl;

import gl.antialiasing.GLMultisampler;
import gl.geometry.GLGeometry;
import gl.geometry.LightRoom;
import gl.material.GLMaterial;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.tracing.Ray;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.vecmath.Point3d;

import com.sun.opengl.util.FPSAnimator;
import com.sun.opengl.util.GLUT;

public class SimpleGLwithDownsampledFBO extends JFrame implements
		GLEventListener, KeyListener {
	private static final long serialVersionUID = -4925906362074812304L;

	private GLCapabilities caps;
	private GLCanvas canvas;
	private float rotAngle = 0f;
	private boolean rotate = false;
	private Dimension fboDimension = new Dimension();
	private Dimension windowDimension = new Dimension();

	private FPSAnimator animator;
	private int frameBufferObjectMultisample = -1;
	private int frameBufferObjectDownsample = -1;
	private boolean initRequest = true;

	private int msamples = 8;
	private int factor = 1;

	public SimpleGLwithDownsampledFBO() {
		super("Multisampled + Downsampled FBO Test");

		caps = new GLCapabilities();

		windowDimension.setSize(1000, 1000);

		caps.setDoubleBuffered(true);
		caps.setHardwareAccelerated(true);
		// caps.setAccumBlueBits(8);
		// caps.setAccumGreenBits(8);
		// caps.setAccumRedBits(8);
		// caps.setAccumAlphaBits(8);
		// caps.setStencilBits(8);
		// caps.setDepthBits(16);
		//

//		caps.setSampleBuffers(true);
//		caps.setNumSamples(4);
//		caps.setAccumBlueBits(16);
//		caps.setAccumGreenBits(16);
//		caps.setAccumRedBits(16);

		canvas = new GLCanvas(caps);
		canvas.addGLEventListener(this);
		canvas.addKeyListener(this);

		animator = new FPSAnimator(canvas, 60);
		animator.setRunAsFastAsPossible(false);
		animator.start();

		getContentPane().add(canvas);
	}

	public void run() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(windowDimension);
		setLocationRelativeTo(null);
		setVisible(true);
		canvas.requestFocusInWindow();

		System.out.println("Run completed.");
	}

	public static void main(String[] args) {
		new SimpleGLwithDownsampledFBO().run();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		//
		float values[] = new float[2];
		gl.glGetFloatv(GL.GL_LINE_WIDTH_GRANULARITY, values, 0);
		System.out
				.println("GL.GL_LINE_WIDTH_GRANULARITY value is " + values[0]);
		gl.glGetFloatv(GL.GL_LINE_WIDTH_RANGE, values, 0);
		System.out.println("GL.GL_LINE_WIDTH_RANGE values are " + values[0]
				+ ", " + values[1]);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_DONT_CARE);
		gl.glLineWidth(1.5f);
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gl.glEnable(GL.GL_CULL_FACE);
		gl.glEnable(GL.GL_MULTISAMPLE);

		System.out.println("Init completed.");
	}

	private void initFramebuffering(GL gl) {
		gl.glEnable(GL.GL_MULTISAMPLE_EXT);
		gl.glEnable(GL.GL_MULTISAMPLE);

		fboDimension.setSize((factor * windowDimension.width),
				(factor * windowDimension.height));
		System.out.println("Window size = " + windowDimension);
		System.out.println("FBO size = " + fboDimension);
		int[] cwind = new int[4];
		gl.glGetIntegerv(GL.GL_SCISSOR_BOX, cwind, 0);
		System.out.println(Arrays.toString(cwind));

		int[] fboIds = new int[2];
		int[] colorId = new int[1];
		int[] depthId = new int[1];

		gl.glGenFramebuffersEXT(2, fboIds, 0);
		frameBufferObjectMultisample = fboIds[0];
		frameBufferObjectDownsample = fboIds[1];

		// Create multisampled FBO
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT,
				frameBufferObjectMultisample);
		gl.glGenRenderbuffersEXT(1, colorId, 0);
		gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, colorId[0]);
		gl.glRenderbufferStorageMultisampleEXT(GL.GL_RENDERBUFFER_EXT,
				msamples, GL.GL_RGBA, fboDimension.width, fboDimension.height);
		gl.glFramebufferRenderbufferEXT(GL.GL_FRAMEBUFFER_EXT,
				GL.GL_COLOR_ATTACHMENT0_EXT, GL.GL_RENDERBUFFER_EXT, colorId[0]);
		gl.glGenRenderbuffersEXT(1, depthId, 0);
		gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, depthId[0]);
		gl.glRenderbufferStorageMultisampleEXT(GL.GL_RENDERBUFFER_EXT,
				msamples, GL.GL_DEPTH_COMPONENT, fboDimension.width,
				fboDimension.height);
		gl.glFramebufferRenderbufferEXT(GL.GL_FRAMEBUFFER_EXT,
				GL.GL_DEPTH_ATTACHMENT_EXT, GL.GL_RENDERBUFFER_EXT, depthId[0]);

		// Create downsampling FBO
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT,
				frameBufferObjectDownsample);

		gl.glGenRenderbuffersEXT(1, colorId, 0);
		gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, colorId[0]);
		gl.glRenderbufferStorageEXT(GL.GL_RENDERBUFFER_EXT, GL.GL_RGBA,
				fboDimension.width, fboDimension.height);

		gl.glGenRenderbuffersEXT(1, depthId, 0);
		gl.glBindRenderbufferEXT(GL.GL_RENDERBUFFER_EXT, depthId[0]);
		gl.glRenderbufferStorageEXT(GL.GL_RENDERBUFFER_EXT,
				GL.GL_DEPTH_COMPONENT, fboDimension.width, fboDimension.height);

		// Attach to framebuffer
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT,
				frameBufferObjectDownsample);
		gl.glFramebufferRenderbufferEXT(GL.GL_FRAMEBUFFER_EXT,
				GL.GL_COLOR_ATTACHMENT0_EXT, GL.GL_RENDERBUFFER_EXT, colorId[0]);
		gl.glFramebufferRenderbufferEXT(GL.GL_FRAMEBUFFER_EXT,
				GL.GL_DEPTH_ATTACHMENT_EXT, GL.GL_RENDERBUFFER_EXT, depthId[0]);

		System.out.println("FBOs initialized.");
	}

	private static GLUT glut = new GLUT();
	private LightRoom room = new LightRoom(2.0);

	private GLMultisampler multisampler = null;
	private GLGeometry geometry = new GLGeometry() {
		
		@Override
		public void init(GLAutoDrawable drawable) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public void display(GLAutoDrawable drawable) {
			GL gl = drawable.getGL();

			room.display(drawable);

			gl.glDisable(GL.GL_LIGHTING);

			gl.glLineWidth(15f);

			gl.glColor3f(0.0f, 1.0f, 0.0f);
			gl.glPushMatrix();
			gl.glRotatef(-rotAngle, 0.0f, 0.0f, 0.1f);
			gl.glBegin(GL.GL_LINES);
			gl.glVertex2f(-0.5f, 0.5f);
			gl.glVertex2f(0.5f, -0.5f);
			gl.glEnd();
			gl.glPopMatrix();
			gl.glColor3f(0.0f, 0.0f, 1.0f);
			gl.glPushMatrix();
			gl.glRotatef(rotAngle, 0.0f, 0.0f, 0.1f);
			gl.glBegin(GL.GL_LINES);
			gl.glVertex2f(0.5f, 0.5f);
			gl.glVertex2f(-0.5f, -0.5f);
			gl.glEnd();
			gl.glPopMatrix();

			int sn = 24;
			gl.glEnable(GL.GL_LIGHTING);
			gl.glPushMatrix();
			gl.glTranslated(0, -0.7, 0);
			gl.glRotatef(-rotAngle, 1.0f, 0.0f, 0.1f);
			GLMaterial.RUBY.apply(gl);
			// glut.glutSolidTorus(0.05f, 0.8f, sn, sn);
			glut.glutSolidTorus(0.2f, 1.2f, sn, sn);
			gl.glPopMatrix();

			gl.glPushMatrix();
			gl.glTranslated(0, -0.7, 0);
			gl.glRotatef(rotAngle, 1.0f, 0.0f, 0.1f);
			gl.glTranslated(0.5, 0.7, 0);
			GLMaterial.TURQUOISE.apply(gl);
			glut.glutSolidTorus(0.2f, 1.2f, sn, sn);
			gl.glPopMatrix();

			if (rotate)
				rotAngle += 0.2f;
			if (rotAngle >= 360f)
				rotAngle = 0f;
		}
	};

	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
		if (initRequest) {
			initFramebuffering(gl);
			initRequest = false;
		}
		// Clear the main buffer
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT, 0);
		gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// Bind and clear the multisampling FBO
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT,
				frameBufferObjectMultisample);
		gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// Draw the scene into the multisampling FBO
		gl.glPushAttrib(GL.GL_VIEWPORT_BIT);
		gl.glViewport(0, 0, fboDimension.width, fboDimension.height);
		geometry.display(drawable);
		gl.glPopAttrib();

		// Bind and clear the downsampling FBO
		gl.glBindFramebufferEXT(GL.GL_FRAMEBUFFER_EXT,
				frameBufferObjectDownsample);
		gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// Blit the multisampling FBO into the downsampling FBO
		gl.glBindFramebufferEXT(GL.GL_READ_FRAMEBUFFER_EXT,
				frameBufferObjectMultisample);
		gl.glBindFramebufferEXT(GL.GL_DRAW_FRAMEBUFFER_EXT,
				frameBufferObjectDownsample);
		gl.glBlitFramebufferEXT(0, 0, fboDimension.width, fboDimension.height,
				0, 0, fboDimension.width, fboDimension.height,
				GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT, GL.GL_NEAREST);

		// Blit the downsampling FBO into the main buffer
		gl.glBindFramebufferEXT(GL.GL_READ_FRAMEBUFFER_EXT,
				frameBufferObjectDownsample);
		gl.glBindFramebufferEXT(GL.GL_DRAW_FRAMEBUFFER_EXT, 0);
		/*
		 * Could blit the depth buffer simultaneously: GL.GL_COLOR_BUFFER_BIT |
		 * GL.GL_DEPTH_BUFFER_BIT
		 */
		gl.glBlitFramebufferEXT(0, 0, fboDimension.width, fboDimension.height,
				0, 0, windowDimension.width, windowDimension.height,
				GL.GL_COLOR_BUFFER_BIT, GL.GL_LINEAR);

		String s = "";
		// s += System.nanoTime() + "\n";
		s += "samples = " + msamples;
		s += "\nfactor  = " + factor;
		JoglRenderer.beginOverlay(drawable);
		JoglTextRenderer.printTextLines(drawable, s, 10, 20, new float[] { 0, 1, 0,
				1 }, GLUT.BITMAP_9_BY_15);
		JoglRenderer.endOverlay(drawable);

		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
		GL gl = drawable.getGL();
		GLU glu = new GLU();
		gl.glViewport(0, 0, w, h);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		if (w <= h) //
			glu.gluOrtho2D(-1.0, 1.0, -1.0 * h / w, //
					1.0 * h / w);
		else
			glu.gluOrtho2D(-1.0 * w / h, //
					1.0 * w / h, -1.0, 1.0);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	@Override
	public void keyTyped(KeyEvent key) {
	}

	@Override
	public void keyPressed(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		case KeyEvent.VK_R:
			rotate = !rotate;
			canvas.display();
			break;
		case KeyEvent.VK_I:
			animator.stop();
			dispose();
			new SimpleGLwithDownsampledFBO().run();
			break;
		case KeyEvent.VK_Q:
			msamples = (msamples + 1) % 9;
			initRequest = true;
			break;
		case KeyEvent.VK_W:
			factor = factor % 4 + 1;
			initRequest = true;
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
	}
}