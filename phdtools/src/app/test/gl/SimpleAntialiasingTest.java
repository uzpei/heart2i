 package app.test.gl;

import gl.geometry.GLObject;
import gl.geometry.primitive.Plane;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import swing.component.ColorChooser;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;

import com.sun.opengl.util.GLUT;

public class SimpleAntialiasingTest implements Interactor, GLObject {

	public static void main(String[] args) {
		GLViewerConfiguration glconfig = new GLViewerConfiguration();
		glconfig.setClearColor(Color.white);

		new SimpleAntialiasingTest(glconfig);
	}
	
	private JoglRenderer viewer;
		
	public SimpleAntialiasingTest(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650, (int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Antialiasing", this, dimension, controlSize, true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(200f);
		viewer.start();
	}
	
	private static GLUT glut = new GLUT();

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_U) {
				}
			}
		});
		
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		
//		gl.glClearColor(1, 1, 1, 1);
		
//		WorldAxis.display(gl);

		gl.glPushMatrix();
		gl.glRotated(30, 1, 0, 0);
		glut.glutSolidCube(3.0f);
		gl.glPopMatrix();
	}
	

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
	
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples() + " samples.");
		new SimpleAntialiasingTest(config);
	}
}
