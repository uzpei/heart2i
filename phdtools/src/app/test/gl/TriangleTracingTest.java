package app.test.gl;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Triangle;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.tracing.Ray;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.StopWatch;

public class TriangleTracingTest implements GLObject, Interactor {

    public static void main(String[] args) {
        new TriangleTracingTest(new GLViewerConfiguration());
    }

    private JoglRenderer renderer;
    private DoubleParameterPoint3d origin = null;
    private DoubleParameterPoint3d target = null;
    
    private List<Ray> rays = new LinkedList<Ray>();
    private List<Triangle> triangles = new LinkedList<Triangle>();

    private IntParameter numrays = new IntParameter("ray count", 100, 0, 10000);
    private IntParameter numtriangles = new IntParameter("triangles count", 10, 0, 1000);
    
    private BooleanParameter drawIntersect = new BooleanParameter("draw intersection", true);
    private BooleanParameter drawrays = new BooleanParameter("draw rays", true);
    private BooleanParameter drawtriangles = new BooleanParameter("draw triangles", true);
    
    public TriangleTracingTest(GLViewerConfiguration config) {
        
    	origin = new DoubleParameterPoint3d("origin", new Point3d(), new Point3d(), new Point3d());
    	target = new DoubleParameterPoint3d("target", new Point3d(), new Point3d(), new Point3d());

        Dimension winsize = new Dimension(800, 800);
        renderer = new JoglRenderer("",
                this, new Dimension(winsize), new Dimension(650,
                        winsize.height + 90), true, config);
        renderer.getCamera().zoom(300);
        renderer.addInteractor(this);
                
        renderer.start();
        
        setTracer();
        origin.setPoint3d(new Point3d(0, 1, 10));
        target.setPoint3d(new Point3d(0, 0, -10));
        
        ParameterListener pl = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				computeRays();
			}
		};
        origin.addParameterListener(pl);
        target.addParameterListener(pl);
        numrays.addParameterListener(pl);
        numtriangles.addParameterListener(pl);
        
        computeRays();
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        gl.glClearColor(1, 1, 1, 1);

        WorldAxis.display(gl);

        gl.glDisable(GL.GL_LIGHTING);

        // Display triangles
        if (drawtriangles.getValue()) {
    		gl.glEnable(GL.GL_LIGHTING);
    		GLMaterial.EMERALD.apply(gl);

            for (Triangle t : triangles) {
//                t.displayOverline(drawable);
            	t.display(drawable);
            }
        }
        
        if (drawrays.getValue()) {
            Point3d rorigin = origin.getPoint3d();
            Point3d rtarget = target.getPoint3d();
            double length = rorigin.distance(rtarget);
            for (Ray ray : rays) {
                ray.display(drawable, length, Color.white, Color.yellow);
            }
        }

        SimpleTimer st = new SimpleTimer();
        st.tick();
        boolean dint = drawIntersect.getValue();
        for (Ray ray : rays) {
            // Intersect with triangles
            for (Triangle t : triangles) {
            	Point3d p = t.intersect(ray);
                if (p != null && dint) drawPoint(gl, p);
            }
        }
        renderer.getTextRenderer().setColor(Color.GREEN);
        renderer.getTextRenderer().drawTopLeft("Total time = " + st.tick_ms() + "\nIntersection tests = " + (numtriangles.getValue() * numrays.getValue()), drawable);
        
    }
        
    private void drawPoint(GL gl, Point3d p) {
    	gl.glDisable(GL.GL_LIGHTING);
    	gl.glColor4d(1, 0, 0, 1);
    	gl.glPointSize(8f);
    	gl.glBegin(GL.GL_POINTS);
    	gl.glVertex3d(p.x, p.y, p.z);
    	gl.glEnd();
    }
    
    private void setTracer() {
    	double size = renderer.getRoom().getSize();
    	Point3d pmin = new Point3d(-size, -size, -size);
    	Point3d pmax = new Point3d(pmin);
    	pmax.scale(-1);

    	origin.setMax(pmax);
    	origin.setMin(pmin);
    	target.setMax(pmax);
    	target.setMin(pmin);
    	
    }
    
    private void computeRays() {
    	System.out.println("Computing rays");
    	
    	rays.clear();
    	
        Point3d rorigin = origin.getPoint3d();
        Point3d rtarget = target.getPoint3d();

        // Create ray
        Ray ray = new Ray(rorigin, rtarget);

        // Create offsets
        rays.add(ray);
        
        int n = numrays.getValue();
        double sx = 10;
        double sy = 10;
        Random rand = new Random();
        
        for (int i = 0; i < n; i++) {
        	// Create xy offset
        	double dx = -sx/2 + sx * rand.nextDouble();
        	double dy = -sy/2 + sy * rand.nextDouble();
        	
        	Point3d rt = new Point3d(rtarget);
        	rt.add(new Point3d(dx, dy, 0));
        	
        	Ray oray = new Ray(rorigin, rt);
        	
        	rays.add(oray);
        }
        
        System.out.println("Computing triangles");
        triangles.clear();
        
        double size = 2;
        sx = 6;
        sy = 6;
        double sz = 6;

        Point3d p0 = new Point3d(-size, 0, 0);
        Point3d p1 = new Point3d(0, size, 0);
        Point3d p2 = new Point3d(size, 0, 0);
        
        n = numtriangles.getValue();

        Point3d op0 = new Point3d();
        Point3d op1 = new Point3d();
        Point3d op2 = new Point3d();
        for (int i = 0; i < n; i++) {
        	// Create xy offset
        	double dx = -sx/2 + sx * rand.nextDouble();
        	double dy = -sy/2 + sy * rand.nextDouble();
        	double dz = -sz * rand.nextDouble();
        	
        	op0.set(p0);
        	op1.set(p1);
        	op2.set(p2);
        	
        	op0.add(new Point3d(dx, dy, dz));
        	op1.add(new Point3d(dx, dy, dz));
        	op2.add(new Point3d(dx, dy, dz));

            Triangle t = new Triangle(op0, op1, op2);
            triangles.add(t);
        }
    }


    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add(origin.getControls());
        vfp.add(target.getControls());
        vfp.add(numrays.getSliderControls());
        vfp.add(numtriangles.getSliderControls());
        vfp.add(drawrays.getControls());
        vfp.add(drawtriangles.getControls());
        vfp.add(drawIntersect.getControls());
        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
    }

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_R) {
					computeRays();
				}
			}
		});
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		new TriangleTracingTest(config);
	}

}
