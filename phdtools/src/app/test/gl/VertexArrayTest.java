 package app.test.gl;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.nio.DoubleBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import tools.FPSTimer;

import com.sun.opengl.util.BufferUtil;
import com.sun.opengl.util.GLUT;

public class VertexArrayTest implements Interactor, GLObject {

	public static void main(String[] args) {
		GLViewerConfiguration glconfig = new GLViewerConfiguration();
		glconfig.setClearColor(Color.white);

		new VertexArrayTest(glconfig);
	}
	
	private JoglRenderer viewer;
		
	public VertexArrayTest(GLViewerConfiguration config) {
		Dimension dimension = new Dimension(800, 800);
		Dimension controlSize = new Dimension(650, (int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer(config, "Antialiasing", this, dimension, controlSize, true);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(200f);
		viewer.start();
	}
	
	private static GLUT glut = new GLUT();

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_U) {
				}
			}
		});
		
	}
	
	private FPSTimer fps = new FPSTimer();
	
	@Override
	public void display(GLAutoDrawable drawable) {
		fps.tick();
		
		GL gl = drawable.getGL();

		WorldAxis.display(gl);

		// Draw a MxN grid
		int N = 40;
		int M = N;
		double scale = 15 / ((double) N);
		
		DoubleBuffer verticesBuffer = BufferUtil.newDoubleBuffer(3 * 2 * M * N) ;
		
		int k = 0;
		int flip = 1;
		for (int j = 0; j < M; j++) {
			for (int i = 0; i < N; i++) {
				double y1 = j * scale + 0;
				double y2 = j * scale + scale;
				
					double x = flip > 0 ? i * scale : ((N-1) - i) * scale;
					verticesBuffer.put(x);
					verticesBuffer.put(y1);
					verticesBuffer.put(0);
					k++;
					
					verticesBuffer.put(x);
					verticesBuffer.put(y2);
					verticesBuffer.put(0);
					k++;
			}
			flip *= -1;
		}
		
		verticesBuffer.rewind();
		
		gl.glEnable(GL.GL_LIGHTING);
		gl.glEnableClientState (GL.GL_VERTEX_ARRAY);        
		gl.glNormal3d(0, 0, 1);

//		GLMaterial.apply(gl, GLMaterial.RUBY);
//		GLMaterial.apply(gl, GLMaterial.CHROME);
		GLMaterial.apply(gl, GLMaterial.CHROME);
		gl.glVertexPointer(3, GL.GL_DOUBLE, 0, verticesBuffer);        
		gl.glDrawArrays(GL.GL_QUAD_STRIP, 0, k);

		GLMaterial.apply(gl, GLMaterial.TURQUOISE);
		gl.glLineWidth(3f);
		
		// TODO: use spanning vectors of the plane in order to generalize the drawing of the grid
		gl.glBegin(GL.GL_LINES);
		for (int i = 0; i < N;i++) {
			double x1 = i * scale;
			double y1 = 0;
			double z1 = 0;
			
			double x2 = i * scale;
			double y2 = M * scale;
			double z2 = 0;
			
			gl.glVertex3d(x1, y1, z1);
			gl.glVertex3d(x2, y2, z2);
		}
		gl.glEnd();
		
		gl.glBegin(GL.GL_LINES);
		for (int j = 0; j <= M;j++) {
			double y1 = j * scale;
			double x1 = 0;
			double z1 = 0;
			
			double y2 = j * scale;
			double x2 = (N-1) * scale;
			double z2 = 0;
			
			gl.glVertex3d(x1, y1, z1);
			gl.glVertex3d(x2, y2, z2);
		}
		gl.glEnd();

        JoglRenderer.beginOverlay(drawable);
        JoglTextRenderer.printTextLines(drawable, fps.toString(), 10, 30,  new float[] { 1, 1, 0, 0}, GLUT.BITMAP_HELVETICA_18 );
        JoglRenderer.endOverlay(drawable);
	}
	

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
	
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		System.out.println("Reloading...");
		System.out.print("Found " + config.getGLCapabilities().getNumSamples() + " samples.");
		new VertexArrayTest(config);
	}
}
