package app.test.geom;

import gl.geometry.FancyArrow;
import gl.geometry.FancyAxis;
import gl.geometry.GLObject;
import gl.material.ColorRandomizer;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.frame.VolumeSampler;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterControl;

import com.jogamp.opengl.util.gl2.GLUT;

public class CartanFieldIntegrator  implements GLObject, Interactor {

	public static void main(String[] args) {
		new CartanFieldIntegrator();
	}
	
	public enum IntegrationSurface { SPHERE };
	
	private JoglRenderer renderer;
	private String name = "Maurer-Cartan Field Integrator";

	/**
	 * List of seed point offsets.
	 */
	private List<Vector3d> seedOffsets = new LinkedList<Vector3d>();
	
	/**
	 * Origin on manifold
	 */
	private Point3d p0 = new Point3d();
	
	/**
	 * Frame (forward Euler) integration
	 */
	private List<List<CoordinateFrame>> frames = new LinkedList<List<CoordinateFrame>>();
	
	private BooleanParameter drawFrames = new BooleanParameter("draw frames", false);
	private BooleanParameter manifoldSeeds = new BooleanParameter("manifold seeds", true);
	
	private DoubleParameter psi = new DoubleParameter(GreekToolset.GreekLetter.PSI.toString(), 0, 0, 2 * Math.PI);
//	private DoubleParameter rho = new DoubleParameter(GreekToolset.GreekLetter.RHO.toString(), 1, 1e-8, 1);
	private DoubleParameter theta = new DoubleParameter(GreekToolset.GreekLetter.THETA.toString(), 0, 0, 2 * Math.PI);
	private DoubleParameter phi = new DoubleParameter(GreekToolset.GreekLetter.PHI.toString(), 0, 0, Math.PI);

	private EnumComboBox<SamplingStyle> samplingStyle = new EnumComboBox<SamplingStyle>(SamplingStyle.NP_NP_Z);

	private IntDiscreteParameter neighborhoodSamples = new IntDiscreteParameter("num samples", 1, new int[] { 1, 3, 5, 7, 9 });
	
	private DoubleParameter neighborhoodScale = new DoubleParameter("scale", 0.01, 0, 1);
	
	private CartanParameterControl cartanParameter = new CartanParameterControl();
	private IntParameter numSteps = new IntParameter("num steps", 500, 1, 10000);
	private DoubleParameter stepSize = new DoubleParameter("step size", 0.01, 0, 1);
	private EnumComboBox<IntegrationSurface> cboxIntegration = new EnumComboBox<IntegrationSurface>(IntegrationSurface.SPHERE);
	
	private ColorRandomizer colorer = new ColorRandomizer();
	
	private GLUT glut = new GLUT();
	
	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		VerticalFlowPanel vfpIntegration = new VerticalFlowPanel();
		vfpIntegration.createBorder("Integration Parameters");
		
		vfpIntegration.add(samplingStyle.getControls());
		vfpIntegration.add(drawFrames.getControls());
		vfpIntegration.add(manifoldSeeds.getControls());
		
		vfpIntegration.add(neighborhoodSamples.getSliderControls());
		vfpIntegration.add(neighborhoodScale.getSliderControls());
//		vfpIntegration.add(rho.getSliderControls());
		vfpIntegration.add(theta.getSliderControls());
		vfpIntegration.add(phi.getSliderControls());
		vfpIntegration.add(psi.getSliderControls());
		vfpIntegration.add(numSteps.getSliderControls());
		vfpIntegration.add(stepSize.getSliderControls());
		vfpIntegration.add(cboxIntegration.getControls());
		
		vfp.add(vfpIntegration.getPanel());
		vfp.add(cartanParameter.getControls());
		
		return vfp.getPanel();
	}

	public CartanFieldIntegrator() {
		renderer = new JoglRenderer(name);
		
		renderer.setBackgroundColor(Color.white);

		createFrames();
		
		ParameterListener frameListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				createFrames();
			}
		};
		
//		rho.addParameterListener(frameListener);
		manifoldSeeds.addParameterListener(frameListener);
		theta.addParameterListener(frameListener);
		phi.addParameterListener(frameListener);
		psi.addParameterListener(frameListener);
		cartanParameter.addParameterListener(frameListener);
		samplingStyle.addParameterListener(frameListener);
		neighborhoodSamples.addParameterListener(frameListener);
		numSteps.addParameterListener(frameListener);
		stepSize.addParameterListener(frameListener);
		neighborhoodScale.addParameterListener(frameListener);
		
		renderer.addInteractor(this);
		renderer.start(this);
		
		// set initial pose
		renderer.getCamera().zoom(-500);
		renderer.getCamera().setRotation(-45, 0);
		
		// set initial parameter
		cartanParameter.set(CartanParameter.Parameter.C121, 0.8);
		createFrames();
	}

	public Point3d getSphereOrigin() {
		// Translate to initial sphere coordinate
		double rho = 1d / Math.abs(cartanParameter.getc121());
		
		Point3d p0 = new Point3d();
		p0.x += rho * Math.sin(phi.getValue()) * Math.cos(theta.getValue());
		p0.y += rho * Math.sin(phi.getValue()) * Math.sin(theta.getValue());
		p0.z += rho * Math.cos(phi.getValue());
		
		return p0;
	}
	
	private Point3d projectPoint(Point3d p) {
		// Project on local sphere
//		Point3d ps = getSphereOrigin();
//
//		// Compute offset
//		Vector3d v = new Vector3d();
//		v.sub(p, ps);
		
		// Compute projected point (for now assume spherical manifold centered at 0)
		Vector3d w = new Vector3d();
		w.set(p);
		w.normalize();
		w.scale(1d / Math.abs(cartanParameter.getc121()));
		Point3d projected = new Point3d(w);
		
		return projected;
	}
	
	private CoordinateFrame constructProjectedFrame(Vector3d offsetFromOrigin) {
		Point3d p = new Point3d();
		p.add(getSphereOrigin(), offsetFromOrigin);
		Point3d projected = projectPoint(p);
		
		// Compute spherical frame field at projected location
		double rho = 1d / Math.abs(cartanParameter.getc121());
		double phi = Math.asin(projected.z / rho);
		double theta = Math.atan2(projected.y, projected.x);
		Vector3d v1 = new Vector3d();
		Vector3d v2 = new Vector3d();
		v1.x = -Math.cos(theta) * Math.sin(phi);
		v1.y = -Math.sin(theta) * Math.sin(phi);
		v1.z = Math.cos(phi);
		v2.x = Math.cos(theta) * Math.cos(phi);
		v2.y = Math.sin(theta) * Math.cos(phi);
		v2.z = Math.sin(phi);
//		v.x = -Math.sin(theta);
//		v.y = Math.cos(theta);
//		v.z = 0;
		
		if (offsetFromOrigin.length() > 0) {
			double s = v1.dot(offsetFromOrigin);
			v1.scale(s);
			v2.scale(s);
		}
		else {
			v1.negate();
			v2.negate();
		}
		Vector3d v3 = new Vector3d();
		v3.cross(v1, v2);
		
//		CoordinateFrame frame = new CoordinateFrame(v, projected);
		CoordinateFrame frame = new CoordinateFrame(v1, v2, v3, projected);

//		OrientationFrame rotation = OrientationFrame.GetRotationMatrix(FrameAxis.Z, psi.getValue());
//		frame.postmultiply(rotation.getFrame());
		
		return frame;
	}
	
	private List<CoordinateFrame> integrate(CoordinateFrame frame) {
				
		// List used to store iterated frames
		List<CoordinateFrame> frames = new LinkedList<CoordinateFrame>();
		
//		CoordinateFrame frame = constructProjectedFrame(offsetFromOrigin);
		
		Vector3d dt = new Vector3d();
		for (int i = 0; i < numSteps.getValue(); i++) {
			// Add current frame
			frames.add(frame);

			// Create frame for the current integration
			// Apply contractions and integrate frame
			dt.scale(stepSize.getValue(), frame.getAxis(FrameAxis.X));
			frame = cartanParameter.contract(frame, dt, true);
		}
		
		return frames;
	}
	
	private void createFrames() {
		// Get central frame
		CoordinateFrame frame0 = constructProjectedFrame(new Vector3d());
		Point3d p0 = frame0.getTranslation();
		
		// Compute seed points
		List<Point3i> offsetsIntegral = VolumeSampler.sampleInteger(new CoordinateFrame(), neighborhoodSamples.getValue() + 2, samplingStyle.getSelected(), true);
		
		// Project and scale offsets onto manifold
//		seedOffsets.clear();
		List<Vector3d> seedOffsets = new LinkedList<Vector3d>();
		Point3d p = new Point3d();
		this.seedOffsets.clear();
		for (Point3i pt : offsetsIntegral) {
			// Get initial offset point
			Vector3d v = new Vector3d(pt.x, pt.y, pt.z);
			v.scale(neighborhoodScale.getValue());
			p.add(p0, v);
			
			// Project onto manifold
			Point3d projection = projectPoint(p);
			
			// Compute new offset and add it
			v.sub(projection, p0);
			seedOffsets.add(v);
		}
		this.seedOffsets = seedOffsets;
		
		List<List<CoordinateFrame>> frames = new LinkedList<List<CoordinateFrame>>();
		this.frames.clear();
		for (Vector3d v : seedOffsets) {
			CoordinateFrame frame;
			if (manifoldSeeds.getValue()) {
				frame = constructProjectedFrame(v);				
			}
			else {
				// Compute contraction at v
				frame = cartanParameter.contract(frame0, v, true);
			}
			frames.add(integrate(frame));
		}
		this.frames = frames;
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		
		GL2 gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);
//		gl.glColor4d(1, 1, 1, 0.2);
		gl.glColor4d(0, 0, 0, 0.7);
		gl.glLineWidth(1f);
		glut.glutWireSphere(0.99 * 1d / Math.abs(cartanParameter.getc121()), 64, 64);

		// Draw initial frame
		for (List<CoordinateFrame> pframes : frames) {
			pframes.get(0).display(drawable, stepSize.getValue(), false);
//			for (CoordinateFrame frame : pframes) {
//				frame.display(drawable, stepSize.getValue(), false);
//			}
		}

		// Draw offsets as fancy arrows
		Color3f facolor = new Color3f(1, 0, 0);
		double fasize = 0.01;
		for (Vector3d v : seedOffsets) {
			Point3d p0 = getSphereOrigin();
			Point3d pf = new Point3d();
			pf.add(p0, v);
			FancyArrow fa = new FancyArrow(p0, pf, facolor, fasize);
			fa.draw(gl);
		}
		
		if (drawFrames.getValue()) {
			// Draw as frames
			double fscale = 0.1;
			FancyAxis fa = new FancyAxis(fscale);
			for (List<CoordinateFrame> pframes : frames) {
				for (CoordinateFrame frame : pframes) {
//					frame.display(drawable, fscale, false);
					gl.glPushMatrix();
					Point3d t = frame.getTranslation();
//					gl.glTranslated(t.x, t.y, t.z);
					frame.applyTransform(gl);
					fa.draw(gl);
					gl.glPopMatrix();
				}
			}
		}
		else {
			// Draw as curves
			gl.glLineWidth(3f);
			int ci = 0;
			double transp = 0.9;
			for (List<CoordinateFrame> pframes : frames) {
				double[] color = colorer.get(ci++);
				gl.glColor4d(color[0], color[1], color[2], transp);
				gl.glBegin(GL2.GL_LINE_STRIP);
				for (CoordinateFrame frame : pframes) {
					Point3d p = frame.getTranslation();
					gl.glVertex3d(p.x, p.y, p.z);
				}
				gl.glEnd();
			}
		}

		Point3d p0 = getSphereOrigin();
		gl.glColor4d(0, 1, 1, 1);
		gl.glPointSize(10f);
		gl.glDisable(GL2.GL_LIGHTING);
		
		// Show zero point
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex3d(p0.x, p0.y, p0.z);
		gl.glEnd();
		
		// Show offsets
		gl.glColor4d(1, 1, 1, 0.5);
		for (List<CoordinateFrame> pframes : frames) {
			Point3d p = pframes.get(0).getTranslation();
			Vector3d e1 = pframes.get(0).getAxis(FrameAxis.X);
			gl.glBegin(GL2.GL_POINTS);
			gl.glVertex3d(p.x, p.y, p.z);
			gl.glEnd();
			
//			Point3d pto = new Point3d();
//			pto.add(p, e1);
//			gl.glEnable(GL2.GL_LIGHTING);
//			new FancyArrow(p, pto, new Color3f(1, 0, 1), 0.01).draw(gl);
//			gl.glBegin(GL2.GL_LINES);
//			gl.glVertex3d(p.x, p.y, p.z);
//			gl.glVertex3d(pto.x, pto.y, pto.z);
//			gl.glEnd();
		}
		
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void attach(Component component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

}
