package app.test.phelicoid;


import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import helicoid.Helicoid;
import helicoid.modeling.PHelicoidGenerator;
import helicoid.parameter.HelicoidParameter;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import math.surface.SurfaceModeler;


import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.Polyline;

public class WispFieldTest implements GLObject {

    public static void main(String[] args) {
        new WispFieldTest();
    }

    /**
     * Generate random strands.
     */
    private PHelicoidGenerator generator;

    private IntParameter smoothing = new IntParameter("Smoothing cycles", 5, 0, 20);
    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", true);


    private JoglRenderer ev;
    
    private SurfaceModeler plane;
    
    private List<Polyline> wisp = new LinkedList<Polyline>();
    
    private DoubleParameter pthreshold = new DoubleParameter("p threshold", 0.1, 0, 1);

    private DoubleParameter oscale = new DoubleParameter("offset scale", 0.1, 0, 1);

    private DoubleParameter closeness = new DoubleParameter("closeness", 0.1, 0, 1);

    private DoubleParameter tspeed = new DoubleParameter("thread speed", 20, 0, 1000);
    
    private BooleanParameter singleorigin = new BooleanParameter("single origin", false);

    private BooleanParameter lockRefresh = new BooleanParameter("Lock refresh", false);
    
    public WispFieldTest() {
        generator = new PHelicoidGenerator();

        Matrix4d wframe = new Matrix4d();
        wframe.setIdentity();
        wframe.setRotation(new AxisAngle4d(0, 0, 1, Math.PI/2));
        generator.setOrientationFrame(wframe);

        plane = new SurfaceModeler();
        
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Wisp test",
                this, new Dimension(winsize), new Dimension(650,
                        winsize.height + 90), true);
        ev.getCamera().zoom(300);

        setGenerator();
        closeness.setValue(1);
        oscale.setValue(0.5);
        generateWisp();

        ev.start();
    }


    private void setGenerator() {
        generator.hold(true);
        generator.setDefaults();
        generator.setTrace(true);
        generator.setVisualize(true);
        generator.getParameterControl().setNoise(0);
        generator.getParameterControl().setWaviness(0);
        generator.hold(false);
    }
    
    private DoubleParameter speed = new DoubleParameter("rotation speed", 0.25, 0, 2);
    
    double rot_anglez = 0;
    
    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        gl.glClearColor(1, 1, 1, 1);

        rot_anglez += speed.isChecked() ? speed.getValue() : -rot_anglez;
        gl.glRotated(-60, 1, 0, 0);
        
        gl.glTranslated(0, 0, -0.5);

        gl.glRotated(rot_anglez, 0, 0, 1);

        if (displayWorld.getValue())
            WorldAxis.display(gl);

        gl.glLineWidth(5f);
//        generator.smooth(generator.generatedStrand.getPoints(), 5);
        generator.display(drawable);
        
//        if (running) {
//            generator.setParameter(hp);
//            generateWisp();
//        }
        
        if (!running) {
            hp = generator.getParameter();
            hp.KN = 0;
            generator.setParameter(hp);
        }
        
//        generator.setParameter(hp);
        if (!lockRefresh.getValue()) generateWisp();

        gl.glLineWidth(1f);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        float c = 0.1f;
        gl.glColor4f(c, c, c, 0.80f);
        for (Polyline h : wisp) {
            h.display(drawable);
        }
    }


    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add(speed.getSliderControlsExtended());
        vfp.add(singleorigin.getControls());
        vfp.add(pthreshold.getSliderControlsExtended());
        vfp.add(closeness.getSliderControls());
        vfp.add(oscale.getSliderControls());
        vfp.add(smoothing.getSliderControls());
        vfp.add(displayWorld.getControls());
        vfp.add(plane.getControls());
        
        ParameterListener l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {
                generateWisp();
            }
        };
        plane.addParameterListener(l);
        singleorigin.addParameterListener(l);
        closeness.addParameterListener(l);
        oscale.addParameterListener(l);
        
        smoothing.addParameterListener(new ParameterListener() {
            
            @Override
            public void parameterChanged(Parameter parameter) {
                smoothWisp();
            }
        });
        
        vfp.add(tspeed.getSliderControls());
        
        JButton btnReset = new JButton("Reset");
        btnReset.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                hp.scale(0);
                generator.setDefaults();
                generator.generate();
                generateWisp();
            }
        });
        vfp.add(btnReset);

        JButton btnKN = new JButton("KN");
        btnKN.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable helic = new Runnable() {
                    @Override
                    public void run() {
                        double ms = 10;
                        double vmin = -ms*HelicoidParameter.getMaxima().KN;
                        double vmax = ms*HelicoidParameter.getMaxima().KN;
                        double zero = 0;
                        boolean done = false;
                        boolean forward = true;
                        boolean backward = false;
                        boolean reset = false; 
                        
                        hp = generator.getParameter();
                        
                        double numd = 100; 
                        double dv = (vmax - vmin) / numd;
                        double v = 0;
                        running = true;
                        while (!done) {
                            if (forward) {
                                v += dv;
                                
                                if (v > vmax) {
                                    forward = false;
                                    backward = true;
                                }
                            }
                            else if (backward) {
                                v -= dv;
                                if (v < vmin) {
                                    forward = false;
                                    backward = false;
                                    reset = true;
                                }
                            }
                            else if (reset) {
                                v += dv;

                                if (Math.abs(v) < zero + dv) {
                                    forward = false;
                                    backward = false;
                                    reset = false;
                                    done = true;
                                }
                            }
                            hp.KN = v;
                            
                            try {
                                Thread.sleep((long) tspeed.getValue());
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                            
                        hp.KN = zero;
                        running = false;
                    }
                };

                Thread t = new Thread(helic);
                t.start();
                
            }
        });
        vfp.add(btnKN);

        JButton btnKT = new JButton("KT");
        btnKT.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable helic = new Runnable() {
                    @Override
                    public void run() {
                        double vmin = -HelicoidParameter.getMaxima().KT;
                        double vmax = HelicoidParameter.getMaxima().KT;
                        double zero = 0.6;
                        boolean done = false;
                        boolean forward = true;
                        boolean backward = false;
                        boolean reset = false; 
                        
                        hp = generator.getParameter();
                        
                        double numd = 100; 
                        double dv = (vmax - vmin) / numd;
                        double v = 0;
                        running = true;
                        while (!done) {
                            if (forward) {
                                v += dv;
                                
                                if (v > vmax) {
                                    forward = false;
                                    backward = true;
                                }
                            }
                            else if (backward) {
                                v -= dv;
                                if (v < vmin) {
                                    forward = false;
                                    backward = false;
                                    reset = true;
                                }
                            }
                            else if (reset) {
                                v += dv;

                                if (v > zero) {
                                    forward = false;
                                    backward = false;
                                    reset = false;
                                    done = true;
                                }
                            }
                            hp.KT = v;
                            
                            try {
                                Thread.sleep((long) tspeed.getValue());
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        
                        hp.KT = zero;
                        running = false;
                    }
                };

                Thread t = new Thread(helic);
                t.start();
                
            }
        });
        vfp.add(btnKT);

        JButton btnKB = new JButton("KB");
        btnKB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable helic = new Runnable() {
                    @Override
                    public void run() {
                        double ms = 5;
                        double vmin = -ms*HelicoidParameter.getMaxima().KB;
                        double vmax = ms*HelicoidParameter.getMaxima().KB;
                        double zero = 0;
                        boolean done = false;
                        boolean forward = true;
                        boolean backward = false;
                        boolean reset = false; 
                        
                        hp = generator.getParameter();
                        
                        double numd = 100; 
                        double dv = (vmax - vmin) / numd;
                        double v = 0;
                        running = true;
                        while (!done) {
                            if (forward) {
                                v += dv;
                                
                                if (v > vmax) {
                                    forward = false;
                                    backward = true;
                                }
                            }
                            else if (backward) {
                                v -= dv;
                                if (v < vmin) {
                                    forward = false;
                                    backward = false;
                                    reset = true;
                                }
                            }
                            else if (reset) {
                                v += dv;

//                                if (Math.abs(v) < zero + dv) {
                                if (v > zero) {
                                    forward = false;
                                    backward = false;
                                    reset = false;
                                    done = true;
                                }
                            }
                            hp.KB = v;
                            
                            try {
                                Thread.sleep((long) tspeed.getValue());
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        
                        hp.KB = zero;
                        running = false;
                    }
                };

                Thread t = new Thread(helic);
                t.start();
                
            }
        });
        vfp.add(btnKB);

        JButton btna = new JButton("alpha");
        btna.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable helic = new Runnable() {
                    @Override
                    public void run() {
                        double vmin = -HelicoidParameter.getMaxima().alpha;
                        double vmax = HelicoidParameter.getMaxima().alpha;
                        double zero = (vmax - vmin) / 2;
                        boolean done = false;
                        boolean forward = true;
                        boolean backward = false;
                        boolean reset = false; 
                        
                        hp = generator.getParameter();
                        
                        double numd = 100; 
                        double dv = (vmax - vmin) / numd;
                        double v = 0;
                        running = true;
                        while (!done) {
                            if (forward) {
                                v += dv;
                                
                                if (v > vmax) {
                                    forward = false;
                                    backward = true;
                                }
                            }
                            else if (backward) {
                                v -= dv;
                                if (v < vmin) {
                                    forward = false;
                                    backward = false;
                                    reset = true;
                                }
                            }
                            else if (reset) {
                                v += dv;

//                                if (Math.abs(v) < zero + dv) {
                                if (v > zero) {
                                    forward = false;
                                    backward = false;
                                    reset = false;
                                    done = true;
                                }
                            }
                            hp.alpha = v;
                            
                            try {
                                Thread.sleep((long) tspeed.getValue());
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        
                        hp.alpha = zero;
                        running = false;
                    }
                };

                Thread t = new Thread(helic);
                t.start();
                
            }
        });
        vfp.add(btna);

        vfp.add(lockRefresh.getControls());
        
        vfp.add(generator.getControls());
        generator.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                if (generator.isHolding() || running) return;

                if (parameter.getName().startsWith("KT")) {
                    generateWisp();
                } else if (parameter.getName().startsWith("KN")) {
                    generateWisp();
                } else if (parameter.getName().startsWith("KB")) {
                    generateWisp();
                } else if (parameter.getName().startsWith("alpha")) {
                    generateWisp();
                } else if (parameter.getName().startsWith("noise")) {
                    generateWisp();
                } else if (parameter.getName().startsWith("wavyness")) {
                    generateWisp();
                }
            }

        });

        return vfp.getPanel();
    }
    
    private boolean running = false;
    private HelicoidParameter hp = new HelicoidParameter();
    
    private void generateWisp() {
//        setGenerator();
        
        generator.hold(true);
        
        List<Point3d> sampling = new LinkedList<Point3d>();
        List<Matrix4d> ms = new LinkedList<Matrix4d>();
        plane.getSampling(sampling, ms);
        
        List<Point3d> points = new LinkedList<Point3d>();

        // Filter the points
        double minx = 0;
        double max = 0;
        for (Point3d p : sampling) {
            
            points.add(p);
            if (p.x < minx) minx = p.x;
            if (p.x > max) max = p.x;
        }
        
        wisp.clear();
        generator.getParameterControl().setOrigin(new Point3d());
        
        int i = 0;
//        Point3d shift = new Point3d(0.1 + -minx, 0, 0);
        Point3d shift = new Point3d(-minx, 0, 0);
//        System.out.println(minx);
        Point3d offset = new  Point3d();
        for (Point3d p : points) {
            
            offset.set(p.x, p.z, p.y);
            offset.add(shift);
            
            if (pthreshold.isChecked()) {
                if (offset.x > pthreshold.getValue() * max) continue;
                else {
                    offset.set(offset.z, offset.y, offset.x - pthreshold.getValue() * max/2.0);
                }
            }
            else {
                if (offset.x > 0) continue;
            }


//            Point3d offset = new  Point3d(p.x, p.z, p.y);
            offset.scale(oscale.getValue());
            generator.getParameterControl().setOffset(offset);
            
            Helicoid hs = generator.generate(hp);
            wisp.add(hs);
            
            if (lockRefresh.getValue()) generator.smooth(hs.getPoints(), smoothing.getValue());

            if (!singleorigin.getValue()) {
                offset.scale(closeness.getValue());
                hs.moveTo(offset);
            }
            i++;
        }
        
        generator.getParameterControl().setOrigin(new Point3d(0, 0, 0));
        generator.hold(false);
        
        generator.getParameterControl().setOffset(new Point3d());

        generator.generate(hp);
        generator.setParameter(hp);
        
        if (lockRefresh.getValue()) smoothWisp();
    }
    
    private void smoothWisp() {
        generator.smooth(generator.getHair().getPoints(), smoothing.getValue());
        for (Polyline h : wisp) {
            generator.smooth(h.getPoints(), smoothing.getValue());            
        }
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glEnable(GL.GL_POINT_SMOOTH);
        
    }


	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
}
