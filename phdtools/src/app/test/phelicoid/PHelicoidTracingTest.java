package app.test.phelicoid;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import helicoid.PiecewiseHelicoid;
import helicoid.modeling.PHelicoidGenerator;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import tools.geom.Polyline;
import tools.geom.PolylineAnalysis;

/**
 * @author piuze
 */
public class PHelicoidTracingTest implements GLObject, Interactor {

	private DoubleParameter hpmag = new DoubleParameter("Helicoid mag", 0.5, 0,
			2);

	private IntParameter pieces = new IntParameter("pieces", 5, 1, 20);
	{
		pieces.setChecked(false);
	}

	private BooleanParameter showFrenet = new BooleanParameter("show Frenet", false); 

	private BooleanParameter blackout = new BooleanParameter("blackout", false); 

	/* Lists */
	private PiecewiseHelicoid guideHair;

	/**
	 * Generates strands
	 */
	public PHelicoidGenerator generator;

	/**
	 * Main method for running this app.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new PHelicoidTracingTest();
	}

	private JoglRenderer ev;

	/**
	 * Start the main program. Set up classes and do the initialization.
	 */
	public PHelicoidTracingTest() {

		setBlocking(true);

		generator = new PHelicoidGenerator();
		generator.setOrientationFrame(mplane);

		setBlocking(false);

		generate();

		Dimension winsize = new Dimension(800, 800);
		ev = new JoglRenderer("Piecewise Wisp App", this);

		ev.addInteractor(this);

		setBlocking(false);

		ev.getCamera().zoom(70f);

		ev.start();
		
		pieces.setValue(2);
		generator.getParameterControl().setStepCount(100);
		generator.getParameterControl().setTraceScale(1);
//		generator.setWaviness(0);
//		generator.setWavynessr(0);
		generate();
	}

	private boolean blocking = false;

	public void setBlocking(boolean val) {
		blocking = val;
	}

	private void generate() {
		Point3d o = new Point3d();
		Random rand = new Random();
		Matrix4d rot = new Matrix4d();
		rot.setIdentity();
		generator.setOrientationFrame(rot);

		guideHair = new PiecewiseHelicoid();

		int pcs = 0;
		if (pieces.isChecked()) {
			pcs = 1 + rand.nextInt(pieces.getValue());
		} else {
			pcs = pieces.getValue();
		}

		rot.setIdentity();
		rot.mul(rot, mplane);

		// rot.setIdentity();
		guideHair = generator.generateRandomPHelicoids(pcs, hpmag.getValue(),
				rot, rot, o);
		
//		System.out.println("Generated a "+guideHair.getCount()+"-helicoid");

	}

	private AxisAngle4d aplane = new AxisAngle4d(0, 0, 1, Math.PI / 2);

	private Matrix4d mplane = new Matrix4d();
	{
		mplane.setIdentity();
		mplane.setRotation(aplane);
	}

	@Override
	public void attach(Component component) {

		component.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		component.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseMoved(MouseEvent e) {
			}
		});

		component.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					generate();
				} 
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	private boolean snapshotRequest = false;

	private int s_count = 0;

	private void setSnapshotRequest() {
		snapshotRequest = true;
	}

	/**
	 * Second level display loop. Directly called by the Scene.
	 * 
	 * @param drawable
	 */
	@Override
	public void display(GLAutoDrawable drawable) {

		GL2 gl = drawable.getGL().getGL2();

		WorldAxis.display(gl);
		
		gl.glColor3d(1, 0, 0);
		gl.glLineWidth(4);
		guideHair.display(drawable, true);
		
		panalysis.displayFrenet(new Polyline(guideHair.getPoints()), drawable);
		
		if (snapshotRequest) {
			processSnapshot(drawable);
		}
	}

	private void processSnapshot(GLAutoDrawable drawable) {
		setBlocking(true);
		File file = new File("./snapshots/snap" + (s_count++) + ".png");
		ev.snapshot(drawable, file);
	}

	private PolylineAnalysis panalysis = new PolylineAnalysis();
	
	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		vfp.add(blackout.getControls());
		vfp.add(panalysis.getControls());
		
		JButton btnSnap = new JButton("Snapshot");
		btnSnap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				setSnapshotRequest();
			}
		});
		vfp.add(btnSnap);

		JButton btnGen = new JButton("Generate");
		btnGen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		vfp.add(btnGen);

		vfp.add(hpmag.getSliderControls());

		vfp.add(pieces.getSliderControlsExtended("random"));

		vfp.add(generator.getControls());
		
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		return "PHelicoid Tracing";
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2.GL_LINE_SMOOTH);
		gl.glEnable(GL2.GL_POINT_SMOOTH);
	}
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

}
