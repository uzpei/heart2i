package app.test.phelicoid;

import gl.geometry.GLObject;
import gl.geometry.LightRoom;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Plane;
import gl.material.ColorRandomizer;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.renderer.SceneRotator;
import helicoid.Helicoid;
import helicoid.PiecewiseHelicoid;
import helicoid.modeling.PHelicoidGenerator;
import helicoid.modeling.WispGenerator;
import helicoid.parameter.HelicoidParameter;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import math.interpolation.HelicoidInterpolator;
import math.surface.SurfaceModeler;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.FPSTimer;
import tools.PolylineExporter;
import tools.PolylineExporter.OBJTYPE;
import tools.SimpleTimer;
import tools.frame.CoordinateFrameSample;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.MathToolset;
import tools.geom.OrientedPoint3d;
import tools.geom.Polyline;
import tools.interpolation.GaussianInterpolator.METRIC;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * @author piuze
 */
public class MultiPlanarInterpolationApp implements GLObject, Interactor {

	private BooleanParameter blackout = new BooleanParameter("blackout", false); 

    /* Boolean parameters */
    private BooleanParameter displayGuide = new BooleanParameter(
            "Display guide hairs", true);

    private BooleanParameter displayInter = new BooleanParameter(
            "Display interpolated hairs", true);

    private BooleanParameter displayWisp = new BooleanParameter(
            "Display wisp hairs", true);

    private BooleanParameter boolgen = new BooleanParameter(
            "enable generation", true);

    private BooleanParameter boolinterpol = new BooleanParameter(
            "enable interpolation", true);

    private BooleanParameter boolwisp = new BooleanParameter("enable wisps",
            false);

    private BooleanParameter dispParam = new BooleanParameter("display params",
            true);

    private BooleanParameter dispParam2 = new BooleanParameter("display i-params",
            true);

    private BooleanParameter dispText = new BooleanParameter("display text",
            true);


    /* Lists */
    private List<PiecewiseHelicoid> guideHairs = new LinkedList<PiecewiseHelicoid>();

    private List<PiecewiseHelicoid> interpolatedHairs = new LinkedList<PiecewiseHelicoid>();

    private List<List<Helicoid>> wisps = new LinkedList<List<Helicoid>>();

    /**
     * Whether or not a random rotation about the normal should be applied when
     * generating a helicoid.
     */
    private BooleanParameter randomFrame = new BooleanParameter("random frame",
            true);

    /**
     * Piecewise count
     */
    private IntParameter pieces = new IntParameter("helicoid pieces",
            3, 1, 10);

    /**
     * Density
     */
    private IntParameter iDensity = new IntParameter("interpolation density",
            1, 0, 500);

//    private DoubleParameter rotx = new DoubleParameter("rot", 0, 0, 2*Math.PI);
//    private DoubleParameter rotx2 = new DoubleParameter("rot2", 0, 0, 2*Math.PI);
    private DoubleParameter rotx = new DoubleParameter("rot", 0, -2*Math.PI, 2*Math.PI);
    private DoubleParameter rotx2 = new DoubleParameter("rot2", 0, -2*Math.PI, 2*Math.PI);

    private DoubleParameter rmag = new DoubleParameter("rmag", 0.3, 0, 1);

    /* Timers */
    private FPSTimer timer = new FPSTimer();

    private SimpleTimer interTimer = new SimpleTimer();

    private SimpleTimer wispTimer = new SimpleTimer();

    private SimpleTimer genTimer = new SimpleTimer();

    private double interTime = 0, genTime = 0, wispTime = 0;

    /* Framework */

    /**
     * Generates strands
     */
    public PHelicoidGenerator generator;

    public CoordinateFrameSample sampler;

    /**
     * Main method for running this app.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new MultiPlanarInterpolationApp();
//    	System.out.println("Yo Phil.");
//    	System.out.println("Je veux une autre ligne!");
//    	
//    	for (int i = 1; i < 11; i = i + 2) {
//    		System.out.println("Le nombre est " + i);
//    	}
    }

    private JoglRenderer ev;

    private HelicoidInterpolator interpolator;

    private WispGenerator wispGenerator;

    private boolean helicoidInterpolation = true;
    
    private ColorRandomizer colorRandomizer;
    
    private SamplingStyle samplingStyle = SamplingStyle.NP_Z_NP;

    /**
     * Start the main program. Set up classes and do the initialization.
     */
    public MultiPlanarInterpolationApp() {

        setBlocking(true);

        generator = new PHelicoidGenerator();
        generator.setOrientationFrame(mplane);

        interpolator = new HelicoidInterpolator(generator);
        
        wispGenerator = new WispGenerator(generator);        

        sampler = new CoordinateFrameSample();
        sampler.presampleDouble(new int[] { 1, 1, 1 }, new double[] { 1, 1, 1 }, samplingStyle);
        
        pieces.setChecked(true);
        
        colorRandomizer = new ColorRandomizer();

        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Planar Helicoid Interpolation", this);

        ev.addInteractor(this);

        ev.controlFrame.add("Generator", generator.getControls());

        iDensity.setValue(15);

        setBlocking(false);

        generate();

//        ev.getCamera().zoom(70f);

        interpolator.setFlippingEnabled(true);
        interpolator.setLengthDropoffEnabled(false);
        
        ev.start();
    }
    
    private boolean blocking = false;

    public void setBlocking(boolean val) {
        blocking = val;
    }

    private void generateTest() {
        if (blocking || !boolgen.getValue())
            return;

        guideHairs.clear();

        List<Point3d> roots = sampler.getDoubleSampling();

        Point3d o = new Point3d();
        Matrix4d frame = new Matrix4d();
        Random rand = new Random();
        Matrix4d rot = new Matrix4d();
        rot.setIdentity();
        frame.setIdentity();
        
//        generator.setStepCount(100);
//        generator.setTraceScale(1);
        
        Point3d p1 = roots.get(0);
        Point3d p2 = roots.get(1);

        frame.set(mplane);
        generator.setOrientationFrame(frame);

    	PiecewiseHelicoid ph1 = new PiecewiseHelicoid();
    	PiecewiseHelicoid ph2 = new PiecewiseHelicoid();

    	double tm = 0.2;
//    	HelicoidParameter hp1 = new HelicoidParameter(tm, 0, 0, 0);
//    	HelicoidParameter hp2 = new HelicoidParameter(-tm, 0, 0, 0);
    	HelicoidParameter hp1 = new HelicoidParameter(1, -0.1, 0.3, 1);
    	HelicoidParameter hp2 = new HelicoidParameter(hp1);
    	hp2.KT = -hp2.KT;

    	ph1.add(generator.generate(hp1, p1));
    	ph2.add(generator.generate(hp2, p2));

    	
        rot.setRotation(new AxisAngle4d(0, 1, 0, rotx.getValue()));
        ph1.moveTo(new Point3d());
    	ph1.transform(rot);
//        rot.mul(mplane);
//    	rot.setIdentity();
        rot.setRotation(new AxisAngle4d(1, 0, 0, rotx.getValue()));
    	ph1.get(0).setRotationFrame(rot);
    	ph1.moveTo(p1);
    	
    	rot.setRotation(new AxisAngle4d(0, 1, 0, rotx2.getValue()));
        ph2.moveTo(new Point3d());
    	ph2.transform(rot);
    	rot.setRotation(new AxisAngle4d(1, 0, 0, rotx2.getValue()));
    	ph2.get(0).setRotationFrame(rot);
    	ph2.moveTo(p2);
    	
    	guideHairs.add(ph1);
    	guideHairs.add(ph2);

        interpolate();
    }

    private void generate() {
        if (blocking || !boolgen.getValue())
            return;

        genTimer.tick_s();

        System.out.println("Generating...");

        double mag = rmag.getValue();
        
        guideHairs.clear();

        List<Point3d> roots = sampler.getDoubleSampling();

        Point3d o = new Point3d();
        Matrix4d frame = new Matrix4d();
        Random rand = new Random();
        Matrix4d rot = new Matrix4d();
        rot.setIdentity();
        
        frame.setIdentity();
        generator.setOrientationFrame(frame);

//        generator.setStepCount(100);
//        generator.setTraceScale(1);

        int hi = 0;
        for (Point3d p : roots) {

        	PiecewiseHelicoid ph = new PiecewiseHelicoid();
        	o.set(p);
        	
        	int pcs = 0;
        	if (pieces.isChecked()) {
        		pcs = 1+rand.nextInt(pieces.getValue());
        	}
        	else {
        		pcs = pieces.getValue();	
        	}
        	
            if (randomFrame.getValue()) {
            	double rotm = rand.nextDouble() * 2 * Math.PI;
                rot.setRotation(new AxisAngle4d(1, 0, 0, rotm));
//                System.out.println("Random rot = " + (rotm/Math.PI*180) + " deg");
            }
            else {
            	rot.setIdentity();
            }
            
//            frame.mul(rot, mplane);
            
//            System.out.println("Gerating a p-helicoid");
            
            ph = generator.generateRandomPHelicoids(pcs, rmag.getValue(), mplane, rot, p);
  
            guideHairs.add(ph);
            hi++;
        }

        genTime = genTimer.tick();

        interpolate();
    }

    private boolean absolute = false;

    private void interpolate() {
        if (blocking || !boolinterpol.getValue() || guideHairs.size() == 0) {
        	System.out.println("BLOCKED!");
            return;
        }

		System.out.println("Interpolating...");

        if (!helicoidInterpolation) {
            interpolateEuclid();
            return;
        }

        Matrix4d ident = new Matrix4d();
        ident.setIdentity();
        
        List<Point3d> roots = sampler.presampleDouble(new int[] { iDensity.getValue(), iDensity.getValue(), iDensity.getValue() }, new double[] { 1, 1, 1 },SamplingStyle.NP_Z_NP);

        List<OrientedPoint3d> oroots = new LinkedList<OrientedPoint3d>();
        for (Point3d p : roots) {
        	oroots.add(new OrientedPoint3d(p, mplane));
//        	oroots.add(new OrientedPoint3d(p, ident));
        }
        
        interTimer.tick_s();
        interpolator.interpolateOrientedPieceByPiece(guideHairs, oroots, interpolatedHairs);
        interTime = interTimer.tick();

        setBlocking(false);
        
        colorRandomizer.update(interpolatedHairs.size());
    }

    private void interpolateEuclid() {
        if (blocking || !boolinterpol.getValue() || guideHairs.size() == 0)
            return;

        interTimer.tick_s();

        List<Point3d> roots = sampler.presampleDouble(new int[] { iDensity.getValue(), iDensity.getValue(), iDensity.getValue() }, new double[] { 1, 1, 1 },SamplingStyle.NP_Z_NP);
        
        interpolator.interpolateEuclidPiecewise(guideHairs, roots, interpolatedHairs);

        generator.setOrientationFrame(mplane);


        setBlocking(false);

        interTime = interTimer.tick();
    }

    private Matrix4d mplane = new Matrix4d();
    {
        mplane.setIdentity();
        mplane.setRotation(new AxisAngle4d(0, 0, 1, Math.PI / 2));
    }

    private boolean shiftDown = false;
    
    @Override
    public void attach(Component component) {

        component.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

        });

        component.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                // TODO Auto-generated method stub
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });

        component.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    guideHairs.clear();
                } else if (e.getKeyCode() == KeyEvent.VK_A) {
                    absolute = !absolute;
                    System.out.println("Absolute = " + absolute);
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_R) {
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_I) {
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_T) {
                    generateTest();
                } else if (e.getKeyCode() == KeyEvent.VK_E) {
                    helicoidInterpolation = !helicoidInterpolation;
//                    setBlocking(true);
//                    interpolator.setPowerH(helicoidInterpolation ? 1 : 2.5);
//                    setBlocking(false);
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_G) {
                    generate();
                } else if (e.isShiftDown())
                    shiftDown = true;
            }

            @Override
            public void keyReleased(KeyEvent e) {
                shiftDown = false;
            }

            @Override
            public void keyTyped(KeyEvent e) {
                // TODO Auto-generated method stub

            }
        });
    }

    /**
     * Second level display loop. Directly called by the Scene.
     * 
     * @param drawable
     */
    @Override
	public void display(GLAutoDrawable drawable) {

        timer.tick();

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(0, 0, 0, 0);
        
//        WorldAxis.display(gl);
        
        if (!blackout.getValue()) WorldAxis.display(gl);

        if (!blackout.getValue()) sampler.display(drawable);

        if (displayWisp.getValue()) {
            gl.glColor3d(0, 0, 1);
            gl.glLineWidth(1);
            for (List<Helicoid> wisp : wisps) {
                for (Helicoid h : wisp) {
                    h.display(drawable);
                }
            }
        }

        if (displayGuide.getValue()) {
            // Display guide strands
            for (PiecewiseHelicoid hair : guideHairs) {
                gl.glColor3d(1, 1, 1);
                gl.glLineWidth(4);
                hair.display(drawable, dispParam.getValue());
//                Polyline pl = new Polyline(hair.toStrand());
//                pl.display(drawable);
                
                // Draw connection points
                for (Helicoid h : hair) {
                    Point3d p = h.getOrigin();
                    gl.glColor3d(0.8, 0.8, 0.8);
                    gl.glPointSize(10);
                    gl.glBegin(GL.GL_POINTS);
                    gl.glVertex3d(p.x, p.y, p.z);
                    gl.glEnd();
                }
            }
        }

        if (displayInter.getValue()) {
//        	interpolate();
            // Display interpolated strands first (to prevent overlay)
            // gl.glColor3d(0, 0, 0);
            gl.glLineWidth(1f);
            int ind = 0;
            for (PiecewiseHelicoid helicoid : interpolatedHairs) {
                gl.glColor3dv(colorRandomizer.get(ind++), 1);
                helicoid.display(drawable, dispParam2.getValue());
//                System.out.println(helicoid.getPoints().size());
            }
        }

        if (dispText.getValue()) {
            displayText(drawable);
        }
    }

    private void displayText(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glColor3fv(new float[] { 0.5f, 1f, 0.5f }, 0);
        JoglTextRenderer.printTextLines(drawable, timer.toString(), drawable
                .getWidth() - 90, 20, 20, GLUT.BITMAP_HELVETICA_18);

        int ghcount = guideHairs.size();
        int ihcount = interpolatedHairs.size();
        int pcount = (ghcount > 0 && ihcount > 0) ? ghcount
                * (guideHairs.get(0).getPoints().size() - 1) : 0;
        pcount += ihcount > 0 ? ihcount
                * (interpolatedHairs.get(0).getPoints().size() - 1) : 0;

        String t = "";
        t += "\nG-hair count = " + ghcount;
        t += "\nI-hair count = " + ihcount;
        t += "\nPrimitive count = " + pcount;
        t += "\nGeneration dt = " + genTime;
        t += "\nInterpolation dt = " + interTime;
        t += "\nWisp dt = " + wispTime;
        gl.glColor3fv(new float[] { 0.5f, 1f, 0.5f }, 0);
        JoglTextRenderer.printTextLines(drawable, t, 10, 20, 10,
                GLUT.BITMAP_HELVETICA_10);

        JoglTextRenderer.printTextLines(drawable,
                !helicoidInterpolation ? "Vertex Interpolation"
                        : "Helicoid Interpolation",
                drawable.getWidth() / 2.0 - 100, 20, new float[] { 0, 0, 1 },
                GLUT.BITMAP_HELVETICA_18);
        JoglTextRenderer.printTextLines(drawable, "(" + interpolator.getNormH()
                + ")", drawable.getWidth() / 2.0 - 100, 35, new float[] { 0.3f,
                0.3f, 0.6f }, GLUT.BITMAP_HELVETICA_12);
    }
    
    private int export_id = 0;
    
    private void export(List<PiecewiseHelicoid> list) {
    	List<Polyline> hairs = new LinkedList<Polyline>();
    	for (PiecewiseHelicoid ph : list) {
    		hairs.add(ph.toStrand());
    	}
    	exportPoly(hairs);
    }
    
    private void exportPoly(List<Polyline> list) {
    	exportPoly(list, 30);
    }
    
    private void exportPoly(List<Polyline> list, int targetcount) {
    	// First subsample the fit
    	for (Polyline p :  list) {
    		p.tesselate(targetcount);
    	}
    	try {
			PolylineExporter.exportOBJ(list, "./data/export" + export_id++ + ".obj", OBJTYPE.BSPLINE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }

    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();

        vfp.add(blackout.getControls());
        
        JButton btneori = new JButton("Export interpolated");
        btneori.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				export(interpolatedHairs);
			}
		});
        vfp.add(btneori);

        JButton btnGen = new JButton("Generate");
        btnGen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generate();
            }
        });
        vfp.add(btnGen);

        JButton btnTest = new JButton("Test");
        btnTest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generateTest();
            }
        });
        vfp.add(btnTest);

        JButton btnInt = new JButton("Interpolate");
        btnInt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                interpolate();
            }
        });
        vfp.add(btnInt);

        JButton btnInte = new JButton("Euclid");
        btnInte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                interpolateEuclid();
            }
        });
        vfp.add(btnInte);

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guideHairs.clear();
                interpolatedHairs.clear();
                wisps.clear();
            }
        });
        vfp.add(btnClear);

        JPanel bpanel = new JPanel(new GridLayout(5, 2));

        bpanel.add(boolgen.getControls());
        bpanel.add(displayGuide.getControls());
        bpanel.add(boolinterpol.getControls());
        bpanel.add(displayInter.getControls());
        bpanel.add(boolwisp.getControls());
        bpanel.add(displayWisp.getControls());
        bpanel.add(randomFrame.getControls());
        bpanel.add(dispParam.getControls());
        bpanel.add(dispText.getControls());
        bpanel.add(dispParam2.getControls());

        vfp.add(bpanel);

        vfp.add(rmag.getSliderControls());
        vfp.add(rotx.getSliderControls());
        rotx.addParameterListener(new ParameterListener() {
        	@Override
        	public void parameterChanged(Parameter parameter) {
        		generateTest();
        	}
        });

        vfp.add(rotx2.getSliderControls());
        rotx2.addParameterListener(new ParameterListener() {
        	@Override
        	public void parameterChanged(Parameter parameter) {
        		generateTest();
        	}
        });

        vfp.add(pieces.getSliderControlsExtended("random"));
        pieces.addParameterListener(new ParameterListener() {
        	@Override
        	public void parameterChanged(Parameter parameter) {
        		generate();
        	}
        	
        });
        vfp.add(iDensity.getSliderControls());
        iDensity.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                interpolate();
            }
        });

        vfp.add(wispGenerator.getControls());
        
//        modeler.addParameterListener(new ParameterListener() {
//
//            @Override
//            public void parameterChanged(Parameter parameter) {
//                String name = parameter.getName();
//                // If we are simply resizing the surface, just udpate the
//                // position
//                // of guide hairs
//                if (name.equalsIgnoreCase("size x")
//                        || name.equalsIgnoreCase("size y")) {
//                    List<Point3d> roots = modeler.getSampling();
//
//                    int i = 0;
//                    for (Point3d p : roots) {
//                        if (i >= guideHairs.size())
//                            break;
//
//                        PiecewiseHelicoid h = guideHairs.get(i);
//
//                        h.moveTo(p);
//                        i++;
//                    }
//
//                    // Remove unused hairs
//                    int n = guideHairs.size();
//                    for (int j = i; j < n; j++) {
//                        int cn = guideHairs.size();
//                        guideHairs.remove(cn - 1);
//                    }
//
//                    interpolate();
//                } else {
//                    generate();
//                }
//            }
//        });

        ParameterListener il = new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
            	System.out.println("Triggering interpolation");
                interpolate();
            }

        };
        interpolator.addParameterListener(il);
        vfp.add(interpolator.getControls());

        generator.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                if (parameter.getName().startsWith("noise")) {
                } else if (parameter.getName().startsWith("wavy")) {
                } else if (parameter.getName().startsWith("g")) {
                }
            }

        });
        
        return vfp.getPanel();
    }

    @Override
    public String getName() {
        return "InterpolationTest";
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
    }
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
