package app.test.projection;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.texture.GLTexture;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLProfile;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import app.test.gl.TextureTest;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.Parameter3d;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;
import tools.loader.PolyVertex;
import tools.loader.obj.ObjModel;

public class ModelCameraProjection  implements GLObject, Interactor {

	public static void main(String[] args) {
		new ModelCameraProjection();
	}
	
	private JoglRenderer renderer;
	private String name = "Model-based Camera Backprojection";
	private List<Point2d> projected = new LinkedList<Point2d>();
	private double[] lambertians;
	
	private DoubleParameter sceneScale = new DoubleParameter("scene scale", 0.1, 0, 1);
	private DoubleParameter aspectRatio = new DoubleParameter("aspect ratio", 1, 0, 1);
	private DoubleParameter fov = new DoubleParameter("fov (deg)", 70, 0, 180);
	private IntParameter imageWidth = new IntParameter("image width", 1, 1, 1920);
	
	private ObjModel obj = new ObjModel();
	private static final float ObjScale = 1.0f;
//	private DoubleParameterPoint3d lightPos = new DoubleParameterPoint3d("light pos", new Point3d(), new Point3d(-1, -1, -1), new Point3d)

	private Parameter3d lightPos = new Parameter3d();
	
	private boolean textureInitializationRequest = false;
	private GLTexture texture;
	
	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(lightPos.getControls());
		vfp.add(aspectRatio.getSliderControls());
		vfp.add(imageWidth.getSliderControls());
		vfp.add(fov.getSliderControls());
		vfp.add(sceneScale.getSliderControls());
		vfp.add(obj.getControls());
		return vfp.getPanel();
	}

	public ModelCameraProjection() {
		renderer = new JoglRenderer(name);

		obj.load("./data/obj/cl_head_LOW.obj", ObjScale);

		obj.setVisible(false);
		
		lightPos.setValue(new Point3d(0, 0, 1));
		applyProjection();
		
		ParameterListener pl = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				applyProjection();
			}
		};
		lightPos.addParameterListener(pl);
		aspectRatio.addParameterListener(pl);
		imageWidth.addParameterListener(pl);
		fov.addParameterListener(pl);
		
		renderer.addInteractor(this);
		renderer.start(this);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glEnable(GL2.GL_LIGHTING);
		obj.display(drawable);

		// Fill texture data
		int i = 0;
		if (textureInitializationRequest) {
			BufferedImage img = new BufferedImage(imageWidth.getValue(),MathToolset.roundInt(imageWidth.getValue() * aspectRatio.getValue()), BufferedImage.TYPE_INT_ARGB);
			
			for (Point2d p : projected) {
				drawPixel(gl, p, lambertians[i++]);
				int pi = MathToolset.roundInt(255 * lambertians[i]++);
				img.setRGB(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), new Color(pi, pi, pi).getRGB());
			}
			
			TextureData textureData = AWTTextureIO.newTextureData(GLProfile.getDefault(), img, false);
			texture = new GLTexture(gl, textureData);
			texture.getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
			texture.getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
			texture.getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
			texture.getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
			textureInitializationRequest = false;
		}
		texture.bind(gl);
		int x = 0;
		int y = 0;
		int dx = imageWidth.getValue();
		int dy = MathToolset.roundInt(imageWidth.getValue() * aspectRatio.getValue());
		gl.glBegin(GL2.GL_QUADS);
		gl.glTexCoord2d(0, 0);
		gl.glVertex2d(x, y);
		gl.glTexCoord2d(0, 1);
		gl.glVertex2d(x, y + dy);
		gl.glTexCoord2d(1, 1);
		gl.glVertex2d(x + dx, y + dy);
		gl.glTexCoord2d(1, 0);
		gl.glVertex2d(x + dx, y);
		gl.glEnd();
		
//		gl.glDisable(GL2.GL_LIGHTING);
//		gl.glDisable(GL2.GL_CULL_FACE);
//		
//		gl.glColor4d(1, 1, 1, 1);
//		
//		gl.glPushMatrix();
//		double scale = sceneScale.getValue();
//		gl.glScaled(scale, scale, scale);
//		gl.glBegin(GL2.GL_POINTS);
//		Point3d lp = lightPos.getValue();
//		Vector3d l = new Vector3d(lp);
//	
//		i = 0;
//		for (Point2d p : projected) {
////			gl.glVertex2d(p.x * scale, p.y * scale);
//			drawPixel(gl, p, lambertians[i++]);
//		}
//		gl.glEnd();
//		gl.glPopMatrix();
	}
	
	private void drawPixel(GL2 gl, Point2d p, double intensity) {
		double scale = 10;
		int x = MathToolset.roundInt(p.x * scale);
		int y = MathToolset.roundInt(p.y * scale);
		int d = 1;

		gl.glColor3d(intensity, intensity, intensity);
		gl.glBegin(GL2.GL_QUADS);
		gl.glVertex2d(x, y);
		gl.glVertex2d(x, y + d);
		gl.glVertex2d(x + d, y + d);
		gl.glVertex2d(x + d, y);
		gl.glEnd();
	}

	private void applyProjection() {
		projected.clear();
		
		if (lambertians == null || lambertians.length != obj.getVertices().length) {
			lambertians = new double[obj.getVertices().length];
		}
		
		renderer.getRoom().setLightPosition(lightPos.getValue());
		
		Matrix4d camIntrinsics = new Matrix4d();
		camIntrinsics.setIdentity();
		
		// Compute the focal lengths
		double width = imageWidth.getValue();
		double height = width * aspectRatio.getValue();
		double fov = this.fov.getValue() / 180 * Math.PI;
		
		double fx = (width/2) / Math.tan(fov / 2);
		double fy = (height/2) / Math.tan(fov / 2);
		double skew = 0;
		double x0 = 0;
		double y0 = 0;
		
		camIntrinsics.m00 = fx;
		camIntrinsics.m01 = skew;
		camIntrinsics.m11 = fy;
		camIntrinsics.m02 = x0;
		camIntrinsics.m12 = y0;
		
		Matrix4d camExtrinsics = new Matrix4d();
		camExtrinsics.setIdentity();
		
		Point4d p = new Point4d();
		Point3d lp = lightPos.getValue();
		Vector3f l = new Vector3f((float) lp.x, (float) lp.y, (float) lp.z);
		l.normalize();
		int i = 0;
		for (PolyVertex v : obj.getVertices()) {
			p.set(v.p.x, v.p.y, v.p.z, 1);

			// Compute lambertian shading
			lambertians[i++] = new Double(v.n.dot(l));
			
			// Apply extrinsics
			camExtrinsics.transform(p);
			
			// Apply intrinsics
			camIntrinsics.transform(p);
			
			Point2d p2 = new Point2d();
			p2.x = p.x / p.z;
			p2.y = p.y / p.z;
			projected.add(p2);
		}

		textureInitializationRequest = true;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_P) {
					applyProjection();
				}
			}
		});
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

}
