package app.test.helicoid;

import gl.geometry.GLObject;
import gl.geometry.LightRoom;
import gl.geometry.WorldAxis;
import gl.material.ColorRandomizer;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.renderer.SceneRotator;
import helicoid.Helicoid;
import helicoid.modeling.PHelicoidGenerator;
import helicoid.modeling.WispGenerator;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import math.surface.SurfaceModeler;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.PolylineExporter;
import tools.PolylineExporter.OBJTYPE;
import tools.geom.Polyline;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * @author piuze
 */
public class WispApp implements GLObject, Interactor {

	/* Boolean parameters */
	private BooleanParameter displayGuide = new BooleanParameter(
			"Display master hair", true);

	private BooleanParameter displayWisp = new BooleanParameter(
			"Display wisp hairs", true);

	private BooleanParameter boolgen = new BooleanParameter(
			"enable generation", true);

	private BooleanParameter dispFrame = new BooleanParameter("display frame",
			true);
	
	private DoubleParameter hpmag = new DoubleParameter("Helicoid mag", 0.8, 0, 2);	
	/* Lists */
	private Helicoid guideHair;
	private List<Helicoid> wisp = new LinkedList<Helicoid>();
	
	private SceneRotator rotator = new SceneRotator();

	/**
	 * Generates strands
	 */
	public PHelicoidGenerator generator;

	/** Models a surface */
	public SurfaceModeler modeler;

	/**
	 * Main method for running this app.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new WispApp();
	}

	private JoglRenderer ev;

	private WispGenerator wispGenerator;

	private ColorRandomizer colorRandomizer;

	/**
	 * Start the main program. Set up classes and do the initialization.
	 */
	public WispApp() {

		setBlocking(true);

		generator = new PHelicoidGenerator();
		generator.setOrientationFrame(mplane);

		wispGenerator = new WispGenerator(generator);

		colorRandomizer = new ColorRandomizer();


		modeler = new SurfaceModeler(SurfaceModeler.SurfaceType.plane);

		setBlocking(false);

		generate();

		Dimension winsize = new Dimension(800, 800);
		ev = new JoglRenderer("Wisp App", this);

		ev.addInteractor(this);

		ev.controlFrame.add("Generator", generator.getControls());

		modeler.setSamplingDivisions(1);

		generate();

		ev.getCamera().zoom(70f);

		ev.start();
		
		generator.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				generate();
			}
		});
	}

	private boolean blocking = false;

	public void setBlocking(boolean val) {
		blocking = val;
	}

	private void generate() {
		if (blocking || !boolgen.getValue())
			return;

		System.out.println("Generating...");

		Point3d p = new Point3d();

		Matrix4d rot = new Matrix4d();
		rot.setIdentity();
		Matrix4d mrot = new Matrix4d();
		mrot.setIdentity();
		Vector3d trans = new Vector3d();

//		HelicoidParameter hp = new HelicoidParameter();
//		hp.randomize(hpmag.getValue());
//
		mrot.setIdentity();

		guideHair = generator.generate();

		mrot.mul(rot);
		trans.set(p);
		mrot.setTranslation(trans);

		// Reorient with plane
		mrot.mul(mplane);

		rot.setTranslation(new Vector3d(p.x, p.y, p.z));

		guideHair.moveTo(p);
		guideHair.setLocalFrame(mrot);

		generateWisps();
	}

	private boolean absolute = false;

	private void generateWisps() {

		wisp.clear();

		generator.hold(true);
		wisp = wispGenerator.generateWisp(guideHair);
		generator.hold(false);
		
	}

	private AxisAngle4d aplane = new AxisAngle4d(0, 0, 1, Math.PI / 2);

	private Matrix4d mplane = new Matrix4d();
	{
		mplane.setIdentity();
		mplane.setRotation(aplane);
	}

	private boolean shiftDown = false;

	@Override
	public void attach(Component component) {

		component.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		component.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseMoved(MouseEvent e) {
			}
		});

		component.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					generate();
				} else if (e.getKeyCode() == KeyEvent.VK_W) {
					generateWisps();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	private boolean snapshotRequest = false;

	private int s_count = 0;

	private void setSnapshotRequest() {
		snapshotRequest = true;
	}
	
	private LightRoom room = new LightRoom(15);

	/**
	 * Second level display loop. Directly called by the Scene.
	 * 
	 * @param drawable
	 */
	@Override
	public void display(GLAutoDrawable drawable) {

		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0, 0, 0, 0);

		rotator.rotate(drawable);

		room.display(drawable);

		modeler.display(drawable);

		if (dispFrame.getValue()) {
			gl.glPushMatrix();
			gl.glTranslated(-5, 0, 0);
			WorldAxis.display(gl, 2, false);
			gl.glPopMatrix();
		}

		if (displayWisp.getValue()) {
			gl.glColor3d(0, 0, 1);
			gl.glLineWidth(0.8f);
			int ind = 0;
			for (Helicoid h : wisp) {
				gl.glColor3dv(colorRandomizer.get(ind++),0);
				h.display(drawable);
			}
		}

		if (displayGuide.getValue()) {
			// Display guide strands
//			gl.glColor3d(1, 1, 1);
//			gl.glLineWidth(8);
			gl.glColor3d(1,0,0);
			gl.glLineWidth(4);
			guideHair.display(drawable);

		}
		
		generator.display(drawable);

        JoglTextRenderer.printTextLines(drawable,
                "Wisp size = " + wisp.size(),
                20, 20, new float[] { 0, 0, 1 },
                GLUT.BITMAP_HELVETICA_18);

		if (snapshotRequest) {
			processSnapshot(drawable);
		}
	}

	private void processSnapshot(GLAutoDrawable drawable) {
		setBlocking(true);
		File file = new File("./snapshots/snap" + (s_count++) + ".png");
		ev.snapshot(drawable, file);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

        JButton btnExport = new JButton("Export");
        btnExport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	export();
            }
        });
        vfp.add(btnExport);

		JButton btnSnap = new JButton("Snapshot");
		btnSnap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				setSnapshotRequest();
			}
		});
		vfp.add(btnSnap);

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				wisp.clear();
			}
		});
		vfp.add(btnClear);
		
		vfp.add(hpmag.getSliderControls());

		JPanel bpanel = new JPanel(new GridLayout(5, 2));

		bpanel.add(boolgen.getControls());
		bpanel.add(displayGuide.getControls());
		bpanel.add(displayWisp.getControls());
		bpanel.add(dispFrame.getControls());

		vfp.add(bpanel);

		wispGenerator.addParameterListener(new ParameterListener() {

			@Override
			public void parameterChanged(Parameter parameter) {
				generateWisps();
			}
		});

		vfp.add(modeler.getControls());

		vfp.add(wispGenerator.getControls());

		generator.addParameterListener(new ParameterListener() {

			@Override
			public void parameterChanged(Parameter parameter) {
				if (parameter.getName().startsWith("noise")) {
				} else if (parameter.getName().startsWith("wavy")) {
				} else if (parameter.getName().startsWith("g")) {
				}
			}

		});
		vfp.add(rotator.getControls());
		vfp.add(room.getControls());

		return vfp.getPanel();
	}

	@Override
	public String getName() {
		return "InterpolationTest";
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2.GL_LINE_SMOOTH);
		gl.glEnable(GL2.GL_POINT_SMOOTH);
	}
	
    private void export() {
    	List<Polyline> hairs = new LinkedList<Polyline>();
    	for (Helicoid h : wisp) {
    		hairs.add(h);
    	}
    	
    	try {
			PolylineExporter.exportOBJ(hairs, "/Users/piuze/Documents/McGill/MSc/testBspline.obj", OBJTYPE.BSPLINE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
