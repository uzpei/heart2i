package app.test.helicoid;


import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.renderer.SceneRotator;
import helicoid.Helicoid;
import helicoid.modeling.HelicoidGenerator;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.StopWatch;

import com.sun.opengl.util.GLUT;

public class IntegrationTest implements GLObject, Interactor {

    public static void main(String[] args) {
        new IntegrationTest();
    }

    /**
     * Generate random strands.
     */
    private HelicoidGenerator generator;

    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", true);
    private BooleanParameter displayO = new BooleanParameter(
            "display original", true);
    private BooleanParameter displayOff = new BooleanParameter(
            "display offset", true);
    
    private JoglRenderer ev;
    
    public IntegrationTest() {
        initializeGenerator();
        hair = generator.generate();
        whair = generator.generate();
        
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Tracing test",
                this, new Dimension(winsize), new Dimension(650,
                        winsize.height + 90), true);
        ev.getCamera().zoom(300);
        ev.addInteractor(this);
        
        ev.start();
    }


    private void initializeGenerator() {
        generator = new HelicoidGenerator();
        generator.setDefaults();
        generator.setTrace(true);
        generator.setVisualize(true);
        
        generator.getParameterControl().setStepCount(1000);
        generator.getParameterControl().setStepSize(0.005);
    }
    
    private Helicoid hair;
    private Helicoid whair;
    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        gl.glClearColor(1, 1, 1, 1);

        rotator.rotate(drawable);
        
        if (displayWorld.getValue())
            WorldAxis.display(gl);

        generator.getParameterControl().setStepCount(100);
        generator.getParameterControl().setStepSize(0.05);

        int sc = generator.getParameterControl().getStepCount();
        double h = generator.getParameterControl().getStepSize();
        
        int n = 10;
        
		double max_ratio = 10;
		
		double inc = max_ratio / n;

        float[][] hc = new float[n][3];
        for (int i = 0; i < n; i++) {
        	hc[i][2] = (1.0f * i) / n;
        }
        
		int hid = 0;
		gl.glDisable(GL.GL_LIGHTING);
		gl.glLineWidth(1);
		StopWatch sw = new StopWatch("dt");
		sw.start();
        for (double d = 1; d <= max_ratio + 1; d += inc) {
			if (hid < hc.length) {
				gl.glColor3f(hc[hid][0], hc[hid][1], hc[hid][2]);
			}
			else {
				hid = 0;
				gl.glColor3f(hc[hid][0], hc[hid][1], hc[hid][2]);
			}
//			gl.glLineWidth(hid/5.0f + 1);
            generator.getParameterControl().setStepCount((int) (sc / d));
            generator.getParameterControl().setStepSize(h * d);
            hair = generator.generate();
            hair.display(drawable);
            
            if (d <= 1) {
              gl.glColor3d(1, 0, 0);
            }
            gl.glPointSize(10.0f);
            gl.glBegin(GL.GL_POINTS);
            Point3d p = hair.getTip();
            gl.glVertex3d(p.x, p.y, p.z);
            gl.glEnd();
            	
            
            hid++;
        }
		sw.stop();

        generator.getParameterControl().setStepCount(sc);
        generator.getParameterControl().setStepSize(h);
        
        
        JoglTextRenderer.printTextLines(drawable, "dt = "
              + sw.toString(), 10, 20, new float[] {0, 0, 0},
              GLUT.BITMAP_8_BY_13);

        if (dosnap) {
        	dosnap = false;
            File file = new File("./snapshots/tsnap" + (s_index++) + ".png");
            ev.snapshot(drawable, file);
        }
        
    }
    
    private boolean dosnap = false;
    private int s_index = 0;

    private SceneRotator rotator = new SceneRotator();
    
    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        JButton snap = new JButton("snap");
        snap.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		dosnap = true;
        	}
        });
        vfp.add(snap);
        
        vfp.add(displayWorld.getControls());
        vfp.add(displayO.getControls());
        vfp.add(displayOff.getControls());
        
        vfp.add(rotator.getControls());
        
        vfp.add(generator.getControls());
        generator.addParameterListener(new ParameterListener() {
        	@Override
        	public void parameterChanged(Parameter parameter) {
        		if (generator.isHolding()) return;
        		if (parameter.getName().equalsIgnoreCase("smoothing cycles")) return;
        		if (parameter.getName().equalsIgnoreCase("smoothing mag")) return;
        		
        		generate();
        	}
        });
        
        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }


	/* (non-Javadoc)
	 * @see tools.viewer.Interactor#attach(java.awt.Component)
	 */
	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			/* (non-Javadoc)
			 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
			 */
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					generate();
				}
			}
		});
	}
	
	private void generate() {
		
        whair = generator.generate();
	}
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
}
