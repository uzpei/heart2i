package app.test.helicoid;


import gl.geometry.FancyAxis;
import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.renderer.SceneRotator;
import helicoid.Helicoid;
import helicoid.modeling.HelicoidGenerator;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

import com.sun.opengl.util.GLUT;

public class TracingTest implements GLObject, Interactor {

    public static void main(String[] args) {
        new TracingTest();
    }

    /**
     * Generate random strands.
     */
    private HelicoidGenerator generator;

    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", true);
    private BooleanParameter displayO = new BooleanParameter(
            "display original", true);
    private BooleanParameter displayOff = new BooleanParameter(
            "display offset", true);
    
    private JoglRenderer ev;
    
    public TracingTest() {
        initializeGenerator();
        whair = generator.generate();
        
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Tracing test",
                this, new Dimension(winsize), new Dimension(650,
                        winsize.height + 90), true);
        ev.getCamera().zoom(300);
        ev.addInteractor(this);
        
        ev.start();
    }


    private void initializeGenerator() {
        generator = new HelicoidGenerator();
        generator.setDefaults();
        generator.setTrace(true);
        generator.setVisualize(true);
    }
    
    private Helicoid whair;
    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        gl.glClearColor(1, 1, 1, 1);

        rotator.rotate(drawable);

        if (displayWorld.getValue()) {
//            WorldAxis.display(gl);
			gl.glEnable(GL.GL_DEPTH_TEST);
        	FancyAxis fa = new FancyAxis(0.35);
        	fa.draw(gl);
        }
        
        double w = generator.getParameterControl().getWavyness();
        double wr = generator.getParameterControl().getWavynessr();
        double no = generator.getParameterControl().getNoise();
        
        generator.hold(true);
        generator.getParameterControl().setWaviness(0);
        generator.getParameterControl().setWavynessr(0);
        generator.getParameterControl().setNoise(0);

        if (displayO.getValue()) {
            generator.display(drawable);
            
//            System.out.println(generator.getHair().getCount());
//            Point3d p1 = generator.getHair().getTip();
//            Point3d p0 = generator.getHair().getOrigin();
//            Vector3d v = new Vector3d();
//            v.sub(p1, p0);
//            System.out.println(v);
            
//            gl.glLineWidth(1);
//            gl.glColor3d(0,0,1);
//            hair.display(drawable);
        }
        
        generator.getParameterControl().setWaviness(w);
        generator.getParameterControl().setWavynessr(wr);
        generator.getParameterControl().setNoise(no);
        generator.hold(false);

        if (displayOff.getValue()) {
            gl.glColor3d(1,0,0);
            Helicoid whair2 = new Helicoid(whair, false);
            generator.smooth(whair2.getPoints());
            
            gl.glLineWidth(7);
            whair2.display(drawable);
        }

        
        JoglTextRenderer.printTextLines(drawable,""
                + generator.getParameter().toStringShort(), 10, 20, new float[] {0, 0, 0},
                GLUT.BITMAP_HELVETICA_18);
     
        if (dosnap) {
        	dosnap = false;
            File file = new File("./snapshots/tsnap" + (s_index++) + ".png");
            ev.snapshot(drawable, file);
        }
        
    }
    
    private boolean dosnap = false;
    private int s_index = 0;

    private SceneRotator rotator = new SceneRotator();
    
    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        JButton snap = new JButton("snap");
        snap.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		dosnap = true;
        	}
        });
        vfp.add(snap);
        
        vfp.add(displayWorld.getControls());
        vfp.add(displayO.getControls());
        vfp.add(displayOff.getControls());
        
        vfp.add(rotator.getControls());
        
        vfp.add(generator.getControls());
        generator.addParameterListener(new ParameterListener() {
        	@Override
        	public void parameterChanged(Parameter parameter) {
        		if (generator.isHolding()) return;
        		if (parameter.getName().equalsIgnoreCase("smoothing cycles")) return;
        		if (parameter.getName().equalsIgnoreCase("smoothing mag")) return;
        		
        		generate();
        	}
        });
                
        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }


	/* (non-Javadoc)
	 * @see tools.viewer.Interactor#attach(java.awt.Component)
	 */
	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			/* (non-Javadoc)
			 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
			 */
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					generate();
				}
			}
		});
	}
	
	private void generate() {
		
        whair = generator.generate();
	}
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
}
