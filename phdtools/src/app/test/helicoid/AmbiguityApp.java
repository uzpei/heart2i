package app.test.helicoid;

import gl.geometry.BoxRoom;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.renderer.SceneRotator;
import gl.renderer.SceneRotator.ViewAngle;
import helicoid.Helicoid;
import helicoid.modeling.HelicoidGenerator;
import helicoid.parameter.HelicoidParameter;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import math.interpolation.HelicoidInterpolator;
import math.surface.SurfaceModeler;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.FPSTimer;
import tools.SimpleTimer;
import tools.interpolation.GaussianInterpolator.METRIC;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * @author piuze
 */
public class AmbiguityApp implements GLObject, Interactor {

    /* Boolean parameters */
    private BooleanParameter displayGuide = new BooleanParameter(
            "Display guide hairs", true);

    private BooleanParameter displayInter = new BooleanParameter(
            "Display interpolated hairs", true);

    private BooleanParameter boolgen = new BooleanParameter(
            "enable generation", true);

    private BooleanParameter boolinterpol = new BooleanParameter(
            "enable interpolation", true);

    private BooleanParameter boolwisp = new BooleanParameter("enable wisps",
            false);

    private BooleanParameter dispParam = new BooleanParameter("display params",
            true);

    private BooleanParameter dispText = new BooleanParameter("display text",
            true);

    private BooleanParameter dispFrame = new BooleanParameter("display frame",
            true);

    /* Lists */
    private List<Helicoid> guideHairs = new LinkedList<Helicoid>();

    private List<Helicoid> interpolatedHairs = new LinkedList<Helicoid>();

    private SceneRotator rotator = new SceneRotator();

    /* Density */
    private IntParameter iDensity = new IntParameter("interpolation density",
            15, 0, 500);

    /* Timers */
    private FPSTimer timer = new FPSTimer();

    private SimpleTimer interTimer = new SimpleTimer();

    private SimpleTimer genTimer = new SimpleTimer();

    private double interTime = 0, genTime = 0, wispTime = 0;

    /* Framework */

    private AxisAngle4d aplane;

    private Matrix4d mplane = new Matrix4d();
    {
        aplane = new AxisAngle4d(0, 0, 1, Math.PI / 2);
        mplane.setIdentity();
        mplane.setRotation(aplane);
    }

    /**
     * Generates strands
     */
    public HelicoidGenerator generator;

    /** Models a surface */
    public SurfaceModeler modeler;

    /**
     * Main method for running this app.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new AmbiguityApp();
    }

    private JoglRenderer ev;

    private HelicoidInterpolator interpolator;

    private boolean helicoidInterpolation = true;

    /**
     * Start the main program. Set up classes and do the initialization.
     */
    public AmbiguityApp() {

        setBlocking(true);

        generator = new HelicoidGenerator();

        interpolator = new HelicoidInterpolator(generator);

        modeler = new SurfaceModeler(SurfaceModeler.SurfaceType.plane);

        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Ambiguity App", this);

        ev.addInteractor(this);

        ev.controlFrame.add("Generator", generator.getControls());

        modeler.setSamplingDivisions(1);
        modeler.setDimension(5, 0.05);
        iDensity.setValue(15);

        setBlocking(false);

        generate(0);

        ev.getCamera().zoom(70f);

        ev.start();
    }

    private boolean blocking = false;

    public void setBlocking(boolean val) {
        blocking = val;
    }

    private int gen_case = 0;

    private void generate() {
        generate(gen_case);
    }

    private void generate(int hcase) {
        if (blocking || !boolgen.getValue())
            return;

        gen_case = hcase;

        genTimer.tick_s();

        System.out.println("Generating...");

        guideHairs.clear();

        List<Point3d> roots = modeler.getSampling();

        // Set the frame and helicoid interpolation length scales right away
        double fscale = 0.3;
        double hscale = 0.3;
        interpolator.setFrameLengthScale(fscale
                * modeler.getSamplingDistance(roots, modeler
                        .getSamplingDivisions()));
        interpolator.setHelicoidLengthScale(hscale
                * modeler.getSamplingDistance(roots, modeler
                        .getSamplingDivisions()));

        AxisAngle4d arot = new AxisAngle4d();
        Matrix4d rot = new Matrix4d();
        Matrix4d mrot = new Matrix4d();
        rot.setIdentity();
        mrot.setIdentity();
        Vector3d trans = new Vector3d();

//        double eps = -1e-12;
        double eps = 1e-12;

        double mag = 0.8;
        HelicoidParameter[] hp = new HelicoidParameter[2];
        hp[0] = new HelicoidParameter();
        hp[1] = new HelicoidParameter();
        hp[0].randomize(mag);
        hp[0].KT = Math.abs(hp[0].KT);
//        hp[0].KN = 0;
//        hp[0].KB = 0;
//        hp[0].alpha = 0;
        hp[1].set(hp[0]);

        if (hcase == 0) {
            hp[0].randomize(mag);
            hp[1].randomize(mag);
            arot.set(0, 1, 0, eps);
        }
        // Opposite k_T and no rotation = opposite
        else if (hcase == 1) {
            hp[1].KT = -hp[0].KT;
            arot.set(0, 1, 0, eps);
        }
        // Same k_T and PI rotation = opposite
        else if (hcase == 2) {
            hp[1].KT = hp[0].KT;
            arot.set(0, 1, 0, Math.PI+eps);
        }
        // Opposite k_T and PI rotation = same
        else if (hcase == 3) {
            hp[1].KT = -hp[0].KT;
            arot.set(0, 1, 0, Math.PI);
        }
        for (int i = 0; i < (roots.size() >= 2 ? 2 : 0); i++) {
            Point3d p = roots.get(i);

            Helicoid h = generator.generate(hp[i]);

            mrot.setIdentity();

            if (i == 1) {
                // Apply rotation
                rot.setIdentity();
                rot.setRotation(arot);
                mrot.mul(rot);
            }

            trans.set(p);
            mrot.setTranslation(trans);

            // Reorient with plane
            mrot.mul(mplane);
            
            // Transform the hair
            h.transform(mrot);
            h.moveTo(p);

            h.setLocalFrame(mrot);

            guideHairs.add(h);
        }

        genTime = genTimer.tick();

        interpolate();
    }

    private boolean i_direction = true;

    private void interpolate() {

        if (blocking || !boolinterpol.getValue() || guideHairs.size() == 0)
            return;

        if (!helicoidInterpolation) {
            interpolateEuclid();
            return;
        }

        interTimer.tick_s();

        List<Point3d> roots = modeler.getSampling(iDensity.getValue());

        interpolator
                .interpolate(guideHairs, roots, interpolatedHairs, i_direction ? 1 : -1);

        setBlocking(false);

        interTime = interTimer.tick();
    }

    private void interpolateEuclid() {
        if (blocking || !boolinterpol.getValue() || guideHairs.size() == 0)
            return;

        interTimer.tick_s();

        List<Point3d> roots = modeler.getSampling(iDensity.getValue());
        interpolator.interpolateEuclid(guideHairs, roots, interpolatedHairs);

        setBlocking(false);

        interTime = interTimer.tick();
    }

    private boolean shiftDown = false;

    @Override
    public void attach(Component component) {

        component.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

        });

        component.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                // TODO Auto-generated method stub
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });

        component.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    guideHairs.clear();
                } else if (e.getKeyCode() == KeyEvent.VK_A) {
                    i_direction = !i_direction;
                    System.out.println("Absolute = " + i_direction);
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_I) {
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_E) {
                    helicoidInterpolation = !helicoidInterpolation;
//                    setBlocking(true);
//                    interpolator.setPowerH(helicoidInterpolation ? 1 : 2.5);
//                    setBlocking(false);
                    interpolate();
                } else if (e.getKeyCode() == KeyEvent.VK_G) {
                    generate();
                } else if (e.isShiftDown())
                    shiftDown = true;
            }

            @Override
            public void keyReleased(KeyEvent e) {
                shiftDown = false;
            }

            @Override
            public void keyTyped(KeyEvent e) {
                // TODO Auto-generated method stub

            }
        });
    }

    private boolean snapshotRequest = false;

    private final int s_count = 9;

    private boolean[] snaplist = new boolean[s_count + 1];

    private void setSnapshotRequest() {
        snapshotRequest = true;
        for (int i = 0; i < s_count; i++) {
            snaplist[i] = false;
        }
    }

    /**
     * Second level display loop. Directly called by the Scene.
     * 
     * @param drawable
     */
    @Override
	public void display(GLAutoDrawable drawable) {

        timer.tick();

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(0, 0, 0, 0);

        rotator.rotate(drawable);

        (new BoxRoom(15)).display(drawable);

        modeler.display(drawable);

        if (dispFrame.getValue()) {
            gl.glPushMatrix();
            gl.glTranslated(-7, 0, 0);
            WorldAxis.display(gl, 2, false);
            gl.glPopMatrix();
            
            // Display frames
            for (Helicoid hair : guideHairs) {
//                DifferentialProfile dp = new DifferentialProfile(hair
//                        .getPoints());
//                Matrix4d m = dp.getFirstFrenetFrame();
                // Matrix4d m =
                // hair.getDifferentialProfile().getFirstFrenetFrame();
                Matrix4d m = hair.getLocalFrame();
                WorldAxis.display(gl, m, 1);
            }
        }

        if (displayInter.getValue()) {
            // Display interpolated strands first (to prevent overlay)
            gl.glColor3d(0, 0, 0);
            gl.glLineWidth(1);
            for (Helicoid hair : interpolatedHairs) {
                hair.display(drawable);
            }
        }

        if (displayGuide.getValue()) {
            // Display guide strands
            gl.glColor3d(1, 0, 0);
            gl.glLineWidth(2);
            for (Helicoid hair : guideHairs) {
                hair.display(drawable);
            }

            // Display helicoid parameters
            if (dispParam.getValue()) {
                for (Helicoid hair : guideHairs) {
                	JoglTextRenderer.print3dTextLines(hair
                            .getParameter().toStringShort(), hair.getOrigin());
                }
            }
        }

        if (dispText.getValue()) {
            displayText(drawable);
        }

        if (snapshotRequest) {
        	processSnapshot(drawable);
        }
    }

    private void processSnapshot(GLAutoDrawable drawable) {
        setBlocking(true);
        // Save 9 snapshots
        // 3 metrics times 3 viewing angles
        for (int i = 0; i <= s_count; i++) {

            // We're done
            if (i >= s_count) {
                File file = new File("./snapshots/snap" + i + ".png");
                ev.snapshot(drawable, file);

                snapshotRequest = false;
                setBlocking(false);
            } else if (snaplist[i] == false) {
                snaplist[i] = true;

                // On the first pass we only interpolate
                if (i > 0) {
                    File file = new File("./snapshots/snap" + i + ".png");
                    ev.snapshot(drawable, file);
                }

                switch (i) {
                case 0:
                    interpolator.setNormH(METRIC.SPATIAL);
                    rotator.applyViewAngle(ViewAngle.X);
                    break;
                case 1:
                    interpolator.setNormH(METRIC.SPATIAL);
                    rotator.applyViewAngle(ViewAngle.Y);
                    break;
                case 2:
                    interpolator.setNormH(METRIC.SPATIAL);
                    rotator.applyViewAngle(ViewAngle.XZ);
                    break;
                case 3:
                    interpolator.setNormH(METRIC.EXP);
                    rotator.applyViewAngle(ViewAngle.X);
                    break;
                case 4:
                    interpolator.setNormH(METRIC.EXP);
                    rotator.applyViewAngle(ViewAngle.Y);
                    break;
                case 5:
                    interpolator.setNormH(METRIC.EXP);
                    rotator.applyViewAngle(ViewAngle.XZ);
                    break;
                case 6:
                    interpolator.setNormH(METRIC.LOG);
                    rotator.applyViewAngle(ViewAngle.X);
                    break;
                case 7:
                    interpolator.setNormH(METRIC.LOG);
                    rotator.applyViewAngle(ViewAngle.Y);
                    break;
                case 8:
                    interpolator.setNormH(METRIC.LOG);
                    rotator.applyViewAngle(ViewAngle.XZ);
                    break;

                }

                setBlocking(false);
                interpolate();
                setBlocking(true);

                break;
            }
        }
        interpolator.setNormH(METRIC.SPATIAL);
    }

    private void displayText(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glColor3fv(new float[] { 0.5f, 1f, 0.5f }, 0);
        JoglTextRenderer.printTextLines(drawable, timer.toString(), drawable
                .getWidth() - 90, 20, 20, GLUT.BITMAP_HELVETICA_18);

        int ghcount = guideHairs.size();
        int ihcount = interpolatedHairs.size();
        int pcount = (ghcount > 0 && ihcount > 0) ? ghcount
                * (guideHairs.get(0).getPoints().size() - 1) : 0;
        pcount += ihcount > 0 ? ihcount
                * (interpolatedHairs.get(0).getPoints().size() - 1) : 0;

        String t = "";
        t += "\nG-hair count = " + ghcount;
        t += "\nI-hair count = " + ihcount;
        t += "\nPrimitive count = " + pcount;
        t += "\nGeneration dt = " + genTime;
        t += "\nInterpolation dt = " + interTime;
        t += "\nWisp dt = " + wispTime;
        gl.glColor3fv(new float[] { 0.5f, 1f, 0.5f }, 0);
        JoglTextRenderer.printTextLines(drawable, t, 10, 20, 10,
                GLUT.BITMAP_HELVETICA_10);

        JoglTextRenderer.printTextLines(drawable,
                !helicoidInterpolation ? "Vertex Interpolation"
                        : "Helicoid Interpolation",
                drawable.getWidth() / 2.0 - 100, 20, new float[] { 0, 0, 1 },
                GLUT.BITMAP_HELVETICA_18);
        JoglTextRenderer.printTextLines(drawable, "(" + interpolator.getNormH()
                + ")" + (i_direction ? " (sign = +1)" : " (sign = -1)"),
                drawable.getWidth() / 2.0 - 100, 35, new float[] { 0.3f, 0.3f,
                        0.6f }, GLUT.BITMAP_HELVETICA_12);
    }

    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();

        JButton btnSnap = new JButton("Snapshot");
        btnSnap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                setSnapshotRequest();
            }
        });
        vfp.add(btnSnap);

        JButton btnGen = new JButton("Gen Rand");
        btnGen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generate(0);
            }
        });

        JButton btnInt = new JButton("Interpolate");
        btnInt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                interpolate();
            }
        });

        JButton btnInte = new JButton("Euclid");
        btnInte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                interpolateEuclid();
            }
        });

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guideHairs.clear();
                interpolatedHairs.clear();
            }
        });

        JButton btnCase1 = new JButton("Case 1");
        btnCase1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generate(1);
            }
        });
        JButton btnCase2 = new JButton("Case 2");
        btnCase2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generate(2);
            }
        });
        JButton btnCase3 = new JButton("Case 3");
        btnCase3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generate(3);
            }
        });

        JPanel bp = new JPanel(new GridLayout(2, 2));
        bp.add(btnGen);
        bp.add(btnClear);
        bp.add(btnInt);
        bp.add(btnInte);

        // bp.add(btnSnap);
        JPanel bpc = new JPanel(new GridLayout(1, 3));
        bpc.add(btnCase1);
        bpc.add(btnCase2);
        bpc.add(btnCase3);

        vfp.add(bp);
        vfp.add(bpc);

        JPanel bpanel = new JPanel(new GridLayout(5, 2));

        bpanel.add(boolgen.getControls());
        bpanel.add(displayGuide.getControls());
        bpanel.add(boolinterpol.getControls());
        bpanel.add(displayInter.getControls());
        bpanel.add(boolwisp.getControls());
        bpanel.add(dispParam.getControls());
        bpanel.add(dispText.getControls());
        bpanel.add(dispFrame.getControls());

        vfp.add(bpanel);

        vfp.add(iDensity.getSliderControls());
        iDensity.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                interpolate();
            }
        });

        vfp.add(modeler.getControls());

        modeler.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                String name = parameter.getName();
                // If we are simply resizing the surface, just udpate the
                // position
                // of guide hairs
                if (name.equalsIgnoreCase("size x")
                        || name.equalsIgnoreCase("size y")) {
                    List<Point3d> roots = modeler.getSampling();

                    int i = 0;
                    for (Point3d p : roots) {
                        if (i >= guideHairs.size())
                            break;

                        Helicoid h = guideHairs.get(i);

                        h.moveTo(p);
                        i++;
                    }

                    // Remove unused hairs
                    int n = guideHairs.size();
                    for (int j = i; j < n; j++) {
                        int cn = guideHairs.size();
                        guideHairs.remove(cn - 1);
                    }

                    interpolate();
                } else {
                    generate(0);
                }
            }
        });

        ParameterListener il = new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                interpolate();
            }

        };
        interpolator.addParameterListener(il);
        vfp.add(interpolator.getControls());

        generator.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                if (parameter.getName().startsWith("noise")) {
                } else if (parameter.getName().startsWith("wavy")) {
                } else if (parameter.getName().startsWith("g")) {
                }
            }

        });
        vfp.add(rotator.getControls());

        return vfp.getPanel();
    }

    @Override
    public String getName() {
        return "InterpolationTest";
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
    }

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
