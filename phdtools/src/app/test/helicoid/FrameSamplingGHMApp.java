package app.test.helicoid;


import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import helicoid.modeling.HelicoidGenerator;
import helicoid.modeling.tracing.HelicoidTracer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import com.sun.opengl.util.GLUT;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.AxisSampling;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.frame.VolumeSampler;
import tools.geom.MathToolset;
import voxel.VoxelBox;

public class FrameSamplingGHMApp implements GLObject, Interactor {

    public static void main(String[] args) {
        new FrameSamplingGHMApp();
    }

    /**
     * Generate random strands.
     */
    private HelicoidGenerator generator;

    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", true);

    private BooleanParameter displayBox = new BooleanParameter(
            "display box", false);

    private JoglRenderer ev;
    
    private OrientationFrame frame;
    
    private SamplingStyle samplingStyle = SamplingStyle.NP_NP_NP;
    {
    	samplingStyle.set(AxisSampling.NEG_POS, AxisSampling.NEG_POS, AxisSampling.NEG_POS);
    }
  
	private IntParameter fsamples = new IntParameter("frame samples", 3, 0, 100);
    
    public FrameSamplingGHMApp() {
        initializeGenerator();

        initializeFrame();
        
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Frame sampling test",
                this, new Dimension(winsize), new Dimension(650,
                        winsize.height + 90), true);
        ev.getCamera().zoom(300);
        ev.addInteractor(this);
        
        ev.start();
    }


    private void initializeGenerator() {
        generator = new HelicoidGenerator();
        generator.setDefaults();
        generator.setTrace(true);
//        generator.setVisualize(true);
    }
    
    private void initializeFrame() {
        frame = new OrientationFrame();
    }
    
    private GLUT glut = new GLUT();
    
    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        int n = fsamples.getValue();
        int nb = n + 1;
        
        gl.glPushMatrix();
        gl.glColor3d(0, 0, 0);

        if (displayBox.getValue()) {
            VoxelBox box = new VoxelBox(new Vector3d(nb, nb, nb), new Point3d());
        	box.display(drawable);
        }
        gl.glPopMatrix();
                
        
        List<Point3i> pts = VolumeSampler.sampleInteger(frame, n, samplingStyle);        
        Vector3d result = new Vector3d();
        Vector3d offset = new Vector3d(nb/2, nb/2, nb/2);
//        offset.scale(1.5);
        
        gl.glPushMatrix();
        
        gl.glTranslated(offset.x, offset.y, offset.z);

        if (displayWorld.getValue()) {
        	frame.display(drawable);
        	gl.glColor4d(1, 1, 1, 0.6);
        	glut.glutSolidSphere(0.1, 32, 32);
        	
        }


        gl.glLineWidth(3);
        
        for (Point3i p : pts) {
         	generator.generateSinglePlanarDirection(generator.getParameter(), p, result);
         	
         	CoordinateFrame E = new CoordinateFrame(HelicoidTracer.getHelicoidFrame(generator.getParameter(), MathToolset.tuple3iToPoint3d(p)), p);
         	E.display(drawable, 1, false);
         	
        	frame.transform(result);
        	
            gl.glColor3d(1, 0, 0);
            gl.glBegin(GL.GL_LINES);
        	gl.glVertex3d(p.x - 0.5 * result.x, p.y - 0.5 * result.y, p.z - 0.5 * result.z);
        	gl.glVertex3d(p.x + 0.5 * result.x, p.y + 0.5 * result.y, p.z + 0.5 * result.z);

            gl.glEnd();
            
            gl.glColor3d(1, 1, 0);
            gl.glPointSize(10);
            gl.glBegin(GL.GL_POINTS);
        	gl.glVertex3d(p.x, p.y, p.z);
        	gl.glEnd();
        }

        gl.glTranslated(0, 0, 2);
        gl.glLineWidth(2);
        generator.display(drawable);

        gl.glPopMatrix();
        
        ev.getTextRenderer().drawTopLeft("# nodes = " + (int) Math.round(Math.pow(pts.size(), 1.0 / 3.0)) + "^3 = " + pts.size(), drawable);
    }
    
    private void randomize() {
		frame.randomize();
    }
    
    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        
        vfp.add(displayWorld.getControls());
        vfp.add(displayBox.getControls());
        
        vfp.add(samplingStyle.getControls());
        vfp.add(fsamples.getSliderControls());
        
        vfp.add(generator.getControls());
        
        JButton extract = new JButton("Extract");
        extract.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				extract();
			}
		});
        vfp.add(extract);

                
        return vfp.getPanel();
    }
    
    private void extract() {
		try {
			String filename = "./data/synthetic.txt";
			
			System.out.println("Extracting to " + new File(filename).getAbsolutePath() + "...");
			final PrintWriter out = new PrintWriter(new FileWriter(filename));
			
			out.println("------------------------------------");
			out.println("Synthetic GHM volume");
			out.println("Curvature parameter (KT, KN, KB, alpha) = " + generator.getParameter().toStringShort());
			out.println("Created by " + System.getProperty("user.name"));
			out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
			
			out.println("------------------------------------");
			out.println("Volume Dimension =");
			out.println(fsamples.getValue() + "x" + fsamples.getValue() + "x" + fsamples.getValue());

			out.println("X, Y, Z, ETx ETy ETz ENx ENy ENz EBx EBy EBz");

			int n = fsamples.getValue();
			int nb = n + 1;
	        Vector3d offset = new Vector3d(nb/2, nb/2, nb/2);

	        List<Point3i> pts = VolumeSampler.sampleInteger(frame, n, samplingStyle);        
	        Vector3d result = new Vector3d();
	        
	        
	        for (Point3i p : pts) {
//	         	generator.generateSingleOrientation(generator.getParameter(), p, result);
//	        	frame.transform(result);
	
	        	OrientationFrame E = HelicoidTracer.getHelicoidFrame(generator.getParameter(), MathToolset.tuple3iToPoint3d(p));
	        	E.premultiply(frame);

	        	Vector3d ET = E.getAxis(FrameAxis.X);
	        	Vector3d EN = E.getAxis(FrameAxis.Y);
	        	Vector3d EB = E.getAxis(FrameAxis.Z);
	        	
	        	p.x += offset.x - 1;
	        	p.y += offset.y - 1;
	        	p.z += offset.z - 1;
	        	
	        	String s = "";
	        	s += p.x + "," + p.y + "," + p.z + ",";
	        	s += ET.x + "," + ET.y + "," + ET.z + ",";
	        	s += EN.x + "," + EN.y + "," + EN.z + ",";
	        	s += EB.x + "," + EB.y + "," + EB.z;
	        	
	        	out.println(s);
	        }

			out.close();
			System.out.println("Done.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }


	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_R) {
					randomize();
				}
				if (e.getKeyCode() == KeyEvent.VK_Z) {
					frame = new OrientationFrame();
				}
			}
		});
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
}
