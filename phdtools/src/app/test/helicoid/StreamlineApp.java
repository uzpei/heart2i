package app.test.helicoid;

import gl.geometry.FancyAxis;
import gl.geometry.GLObject;
import gl.geometry.primitive.Plane;
import gl.material.ColorRandomizer;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import helicoid.Helicoid;
import helicoid.modeling.PHelicoidGenerator;
import helicoid.modeling.WispGenerator;
import helicoid.modeling.tracing.HelicoidTracer.TracingMethod;
import helicoid.parameter.HelicoidParameter.ParameterType;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.HelicoidExplorer;
import tools.PolylineExporter;
import tools.PolylineExporter.OBJTYPE;
import tools.geom.Polyline;

/**
 * @author piuze
 */
public class StreamlineApp implements GLObject, Interactor {

	/* Boolean parameters */
	private BooleanParameter displayGuide = new BooleanParameter(
			"Display master hair", true);

	private BooleanParameter displayWisp = new BooleanParameter(
			"Display wisp hairs", true);

	private BooleanParameter boolgen = new BooleanParameter(
			"enable generation", true);

	private BooleanParameter zoff = new BooleanParameter(
			"z offset", false);

	private BooleanParameter dispFrame = new BooleanParameter("display frame",
			true);
	
	private DoubleParameter hpmag = new DoubleParameter("Helicoid mag", 0.8, 0, 2);	

	private Helicoid guideHair;
	
	private List<Helicoid> wisp = new LinkedList<Helicoid>();
	
	private Plane plane;

	/**
	 * Generates strands
	 */
	public PHelicoidGenerator generator;

	/**
	 * Main method for running this app.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new StreamlineApp();
	}

	private JoglRenderer ev;

	private WispGenerator wispGenerator;

	private ColorRandomizer colorRandomizer;

	private HelicoidExplorer explorer = new HelicoidExplorer();
	
	/**
	 * Start the main program. Set up classes and do the initialization.
	 */
	public StreamlineApp() {

		setBlocking(true);

		generator = new PHelicoidGenerator();
		generator.getVisualizer().enableDrawing();
		
		generator.setTracingMethod(TracingMethod.MIDPOINT);

		wispGenerator = new WispGenerator(generator);
		wispGenerator.setInplane(true);
		wispGenerator.setSpread(1);
		wispGenerator.setOffsetScale(1);		
		
		colorRandomizer = new ColorRandomizer();

		setBlocking(false);

		generate();

		plane = new Plane(new Point3d(), new Vector3d(0, 0, 1));
		plane.setSize(15, 15);
		plane.setDrawGrid(false);
		plane.setMaterial(GLMaterial.TRANSPARENT_LIGHT);
		
		Dimension winsize = new Dimension(800, 800);
		ev = new JoglRenderer("Wisp App");

		ev.addInteractor(this);

		ev.controlFrame.add("Generator", generator.getControls());

		generate();

		ev.getCamera().zoom(130f);

		ev.getRoom().setLightPosition(new Point3d(-5.25, -5.25, 1.5));
		
		ev.start(this);
		
		zoff.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				generate();
			}
		});
		
		generator.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				generate();
			}
		});
		
	}

	private boolean blocking = false;

	public void setBlocking(boolean val) {
		blocking = val;
	}

	private void generate() {
		if (blocking || !boolgen.getValue())
			return;

//		System.out.println("Generating...");

		Point3d p = new Point3d();

		Matrix4d rot = new Matrix4d();
		rot.setIdentity();
		Matrix4d mrot = new Matrix4d();
		mrot.setIdentity();
		Vector3d trans = new Vector3d();

//		HelicoidParameter hp = new HelicoidParameter();
//		hp.randomize(hpmag.getValue());
//
		mrot.setIdentity();

		guideHair = generator.generate();

		mrot.mul(rot);
		trans.set(p);
		mrot.setTranslation(trans);

		rot.setTranslation(new Vector3d(p.x, p.y, p.z));

		guideHair.moveTo(p);
		guideHair.setLocalFrame(mrot);

		generateWisps();
	}

	private void generateWisps() {

		wisp.clear();

		generator.hold(true);
		if (zoff.getValue()) {
			// Find closest hair and use as offset
			Point3d pc = new Point3d();
			double mindist = Double.MAX_VALUE;
			wisp = wispGenerator.generateWisp(guideHair);
			
			for (Helicoid h : wisp) {
				if (h.getOrigin().distance(new Point3d()) < mindist) {
					pc.set(h.getOrigin());
					mindist = h.getOrigin().distance(new Point3d());
				}
			}
			pc.absolute();
			
			Point3d p0 = new Point3d(guideHair.getOrigin());
			Point3d pn = new Point3d(0, pc.y, -1*pc.z);
			guideHair.moveTo(pn);
			wisp = wispGenerator.generateWisp(guideHair);
			guideHair.moveTo(p0);
			
			// Cull the wisp if we are using z-offsetting
			List<Helicoid> toRemove = new LinkedList<Helicoid>();
			if (zoff.getValue()) {
				for (Helicoid h : wisp) {
					if (h.getOrigin().y < 0) {
						toRemove.add(h);
					}
					else if (h.getOrigin().y > 2 * pc.y) {
						toRemove.add(h);
					}
//					else {
//						h.moveTo(new Point3d(0, 0, h.getOrigin().z));
//					}
				}
				
				wisp.removeAll(toRemove);
				
				int i = 0;
				toRemove.clear();
				for (Helicoid h : wisp) {
					if (i++ == wisp.size() / 2 ) {
						// Remove center
						toRemove.add(h);
					}
				}
				wisp.removeAll(toRemove);
			}

			System.out.println(wisp.size());

		}
		else {
			wisp = wispGenerator.generateWisp(guideHair);
		}
		
		generator.hold(false);
		
	}

	@Override
	public void attach(Component component) {

		component.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		component.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseMoved(MouseEvent e) {
			}
		});

		component.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					generate();
				} else if (e.getKeyCode() == KeyEvent.VK_W) {
					generateWisps();
				} else if (e.getKeyCode() == KeyEvent.VK_1) {
					explorer.explore(ParameterType.HKT);
				} else if (e.getKeyCode() == KeyEvent.VK_2) {
					explorer.explore(ParameterType.HKN);
				} else if (e.getKeyCode() == KeyEvent.VK_3) {
					explorer.explore(ParameterType.HKB);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	private boolean snapshotRequest = false;

	private int s_count = 0;

	private void setSnapshotRequest() {
		snapshotRequest = true;
	}
	
	/**
	 * Second level display loop. Directly called by the Scene.
	 * 
	 * @param drawable
	 */
	@Override
	public void display(GLAutoDrawable drawable) {

		generate();

		GL2 gl = drawable.getGL().getGL2();
		
		gl.glClearColor(0, 0, 0, 0);
		gl.glClearColor(1, 1, 1, 0);

		gl.glRotated(90, 0, 0, 1);
		
//		room.display(drawable);
		
//		WorldAxis.display(gl);

//		gl.glTranslated(-1.5, 0, 0);

		gl.glColor3d(1, 1, 1);

		if (explorer.isRunning()) {
			explorer.step();
			generator.setParameter(explorer.getCurrentParameter());
		}
		
		if (guideHair == null)
			guideHair = generator.generate();
		
		generator.display(drawable);

//		generateWisps();

		float ewidth = 4f;
		float gwidth = 6f;
		double alpha = 0.9;
		if (displayWisp.getValue()) {
			gl.glColor3d(0, 0, 1);
			gl.glLineWidth(ewidth);
			int ind = 0;
			for (Helicoid h : wisp) {
//				gl.glColor3dv(colorRandomizer.get(ind++),0);
//				gl.glColor4d(0,0,0, h.getOrigin().z < 0 ? alpha - 0.15 : alpha + 0.1);
				gl.glColor4d(0,0,0, alpha + 0.1);
				
				h.display(drawable);
			}
		}

		if (displayGuide.getValue()) {
			// Display guide strands
//			gl.glColor3d(1, 1, 1);
			gl.glColor4d(1,0,0,alpha);
			gl.glLineWidth(gwidth);
//			guideHair.display(drawable);
//			guideHair.moveTo(new Point3d());
			guideHair.display(drawable);
		}

		gl.glPushMatrix();
		gl.glTranslated(0, 0, -0.1);		

		// Semi transparent material
		float r = 2f;
		float rd = 0f;
		float rs = 0f;
		float a = 0.2f;
		float[] ambient = new float[] { r, r, 2f, a };
		float[] diffuse = new float[] { rd, 0f, rd, a };
		float[] specular = new float[] { 0, 0, 0, 1f };
		float shininess = 0f;

//		plane.setMaterial(new GLMaterial(ambient, diffuse, specular, shininess));
//		plane.display(drawable);
		gl.glPopMatrix();
		
		if (dispFrame.getValue()) {
			gl.glEnable(GL2.GL_LIGHTING);
			r = 0.3f;
			rs = 0.1f;
			ambient = new float[] { r, r, r, 1f };
			diffuse = new float[] { r, r, r, 1f };
			specular = new float[] { rs, rs, rs, 1f };
			shininess = 10f;
			GLMaterial.apply(gl, new GLMaterial(ambient, diffuse, specular, shininess));

			FancyAxis fa = new FancyAxis(1);
			fa.draw(gl);
		}

//		JoglTextRenderer textRenderer = ev.getTextRenderer();
//		textRenderer.setColor(1.0f, 0f, 0f, 1f);
//		textRenderer.addLine(generator.getParameter().toStringShort());
//		textRenderer.addLine("Streamline count = " + wisp.size());
//		textRenderer.drawBuffer(drawable, 10, 10);

//		printTextLines(drawable,""
//                + generator.getParameter().toStringShort(), 10, 20, new float[] {1,1,1},
//                GLUT.BITMAP_HELVETICA_18);

		if (snapshotRequest) {
			processSnapshot(drawable);
		}
		

	}

	private void processSnapshot(GLAutoDrawable drawable) {
		setBlocking(true);
		File file = new File("./snapshots/snap" + (s_count++) + ".png");
		ev.snapshot(drawable, file);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

//		vfp.add(plane.getControls());
		
		vfp.add(explorer.getControls());
		
        JButton btnExport = new JButton("Export");
        btnExport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	export();
            }
        });
        vfp.add(btnExport);

		JButton btnSnap = new JButton("Snapshot");
		btnSnap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				setSnapshotRequest();
			}
		});
		vfp.add(btnSnap);

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				wisp.clear();
			}
		});
		vfp.add(btnClear);
		
		vfp.add(hpmag.getSliderControls());

		JPanel bpanel = new JPanel(new GridLayout(5, 2));

		bpanel.add(boolgen.getControls());
		bpanel.add(displayGuide.getControls());
		bpanel.add(displayWisp.getControls());
		bpanel.add(dispFrame.getControls());
		bpanel.add(zoff.getControls());

		vfp.add(bpanel);

		vfp.add(wispGenerator.getControls());
		wispGenerator.setLinear(true);

		wispGenerator.addParameterListener(new ParameterListener() {

			@Override
			public void parameterChanged(Parameter parameter) {
				generateWisps();
			}
		});

		return vfp.getPanel();
	}

	@Override
	public String getName() {
		return "InterpolationTest";
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL gl = drawable.getGL();
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL2.GL_POINT_SMOOTH);
	}
	
    private void export() {
    	List<Polyline> hairs = new LinkedList<Polyline>();
    	for (Helicoid h : wisp) {
    		hairs.add(h);
    	}
    	
    	try {
			PolylineExporter.exportOBJ(hairs, "/Users/piuze/Documents/McGill/MSc/testBspline.obj", OBJTYPE.BSPLINE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
