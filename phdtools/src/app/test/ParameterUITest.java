package app.test;

import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
import gl.renderer.GLViewerConfiguration.GLBlendingFunctionFactor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.TitledBorder;

import diffgeom.DifferentialOneForm.MovingAxis;
import swing.SwingTools;
import swing.component.ButtonSwitch;
import swing.component.ButtonSwitch.Resolution;
import swing.component.ButtonSwitch.SwitchType;
import swing.component.CollapsiblePanel;
import swing.component.ColorChooser;
import swing.component.EnumComboBox;
import swing.component.FileChooser;
import swing.component.JColorMap;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.JHistogram;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.Parameter3d;
import swing.parameters.ParameterListener;
import volume.IntensityVolume;
import volume.JIntensityVolumeSlicer;
import extension.matlab.MatlabIO;

/**
 * @author epiuze
 * 
 */
public class ParameterUITest extends JFrame {

	private IntParameter refresh = new IntParameter("refresh interval", 10, 1,
			100);
	private Timer timer = new Timer();

	public static void main(String[] args) {
//		applyNimbus();

		new ParameterUITest();
	}

	public static void applyNimbus() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
	}

	public ParameterUITest() {
		JFrame frame = this;
		super.setLocation(GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getCenterPoint().x - 250, GraphicsEnvironment
				.getLocalGraphicsEnvironment().getCenterPoint().y - 250);

		frame.setTitle("Parameter UI test");

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLocation(0, 0);
		Dimension size = new Dimension(500, 800);
		super.setSize(size);
		super.setPreferredSize(size);
//		super.setMaximumSize(size);

		addContent();

		refresh.setChecked(false);
		refresh.addParameterListener(new ParameterListener() {

			@Override
			public void parameterChanged(Parameter parameter) {
				if (timer != null) {
					timer.cancel();
					timer.purge();
				}

				timer = new Timer();

				if (refresh.isChecked())
					timer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							redraw();
						}
					}, 0, refresh.getValue() * 1000);
			}
		});
	}

	protected void redraw() {
		// super.repaint();
		// super.invalidate();
		super.validate();

		addContent();
	}

	private JColorMap jcm;

	private void buttonClicked() {
		if (jcm != null) {
			for (double v = -1.1; v <= 1.1; v += 0.1) {
				Color c = jcm.getColor(v);
				if (c != null) {
					System.out.println(v + ": " + c);
				}
			}
			// System.out.println(jcm.getColor(-1));
		}
	}

	private void addContent() {
		Dimension size = getSize();

		getContentPane().removeAll();

		VerticalFlowPanel vfp = new VerticalFlowPanel();

		// vfp.add(new HeartPanel());

		vfp.add(refresh.getSliderControlsExtended());

		vfp.add(new FileChooser("./data/", "Load"));
		
//		int n = 80;
//		int jiheight = 100;
//		int jwidth = 300;
////		IntensityVolume v = IntensityVolume.createSphericalVolume(n, n, n);
//		IntensityVolume v = new IntensityVolume(MatlabIO.loadMAT("./data/heart/rat/sample1/rat07052008/mat/mask.mat"));
//		JIntensityVolumeSlicer jv = new JIntensityVolumeSlicer(v, new Dimension(jwidth, jiheight));
////		jv.setCoordinates(FrameAxis.Z, v.getDimension()[2]/2);
//		vfp.add(jv);
		
		if (true) {
		jcm = new JColorMap();
		vfp.add(jcm);

		try {
			JHistogram histo = new JHistogram(ImageIO.read(new File(
					"resources/random.png")));
			histo.setColorMap(jcm);
			jcm.setMinMax(0, 30);
			vfp.add(histo);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JPanel ecbPanel = new JPanel(new GridLayout(1, 3));
		ecbPanel.add(new EnumComboBox<MovingAxis>("E1", MovingAxis.F1)
				.getControls());
		ecbPanel.add(new EnumComboBox<MovingAxis>("E2", MovingAxis.F2)
				.getControls());
		ecbPanel.add(new EnumComboBox<MovingAxis>("E3", MovingAxis.F3)
				.getControls());
		vfp.add(ecbPanel);

		JButton btn = new JButton("Action Listener");
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonClicked();
			}
		});
//		btn.setPreferredSize(new Dimension(100, 20));
//		vfp.add(btn);
		vfp.add(SwingTools.encapsulate(btn));
//		vfp.add(SwingTools.encapsulate(new DoubleParameter("test", 0, 0, 1).getSliderControls()));
		vfp.add(new DoubleParameter("test", 0, 0, 1).getSliderControls());
		
		vfp.add(new DoubleParameter("Vertical slider", 0, 0, 1)
				.getSliderControls("", false, true, false, true));
		final Parameter3d p3d = new Parameter3d();
		vfp.add(p3d.getControls());

		final BooleanParameter bpl = new BooleanParameter(
				"boolean with listener", SwitchType.IO, true);
		vfp.add(bpl.getControls());
		bpl.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				System.out
						.println("The parameter has changed and now has value = "
								+ bpl.getValue());
			}
		});

		ColorChooser cc = new ColorChooser("clearColor");
		vfp.add(cc.getControls(Color.BLACK));

		DoubleParameter dp = new DoubleParameter("double", 0, -10, 10);
		dp.setChecked(false);

		// vfp.add(dp.getSliderControlsExtended());
		JPanel pd = new JPanel(new GridLayout(2, 1));
		pd.add(dp.getSliderControlsExtended());
		pd.add(new BooleanParameter("boolean", false).getControls());

		CollapsiblePanel cp = new CollapsiblePanel(pd, "test");

		vfp.add(cp);
		// cp.collapse();

		vfp.add(new BooleanParameter("boolean ON/OFF", SwitchType.ONOFF, true)
				.getControls());

		JPanel bpanel = new JPanel(new GridLayout(2, 3));
		bpanel.setName("Some Parameter");
		// bpanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED)));
		TitledBorder tborder = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Bool party",
				TitledBorder.LEFT, TitledBorder.CENTER);
		bpanel.setBorder(tborder);

		bpanel.add(new BooleanParameter("boolean", SwitchType.IO, true)
				.getControls());
		bpanel.add(new BooleanParameter("boolean", SwitchType.ONOFF, false)
				.getControls());
		bpanel.add(new BooleanParameter("boolean", SwitchType.ONOFF, true)
				.getControls());
		bpanel.add(new BooleanParameter("boolean", SwitchType.ONOFF, true)
				.getControls());
		bpanel.add(new BooleanParameter("boolean", SwitchType.ONOFF, true)
				.getControls());
		bpanel.add(new BooleanParameter("boolean", SwitchType.IO, false)
				.getControls());
		vfp.add(bpanel);

		vfp.add(new IntParameter("integer", 0, -10, 10).getSliderControls());
		vfp.add(new IntParameter("integer extended with a long caption", 0,
				-10, 10)
				.getSliderControlsExtended("this is a very long caption!"));

		vfp.add(new DoubleParameter("double", 0, -10, 10).getSliderControls());

		vfp.add(new DoubleParameter("double extended with a long caption", 0,
				-10, 10).getSliderControlsExtended(false));
		vfp.add(new DoubleParameter("double extended with a long caption", 0,
				-10, 10).getSliderControlsExtended("a boolean"));
		vfp.add(new DoubleParameter("double extended", 0, -10, 10)
				.getSliderControlsExtended("some boolean"));

		JPanel opanel = new JPanel();
		ButtonSwitch oobtn = new ButtonSwitch(SwitchType.ONOFF,
				Resolution.HIGH, true);

		opanel.add(oobtn);
		vfp.add(opanel);

		vfp.add(new EnumComboBox<GLBlendingFunctionFactor>(
				GLBlendingFunctionFactor.ZERO).getControls());
		}
		JButton btnRedraw = new JButton("Redraw");
		btnRedraw.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				redraw();
			}
		});

		vfp.add(btnRedraw);
		
		JScrollPane scroller = new JScrollPane(vfp.getPanel(),
				VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		// JScrollPane scroller = new JScrollPane(vfp,VERTICAL_SCROLLBAR_ALWAYS,
		// ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroller.getVerticalScrollBar().setUnitIncrement(10);
		super.getContentPane().add(scroller);
		// super.getContentPane().add(vfp.getPanel());

		// super.setSize(size);
		// super.setPreferredSize(size);

		super.pack();
		super.repaint();
		super.setVisible(true);

	}

	@Override
	public String getName() {
		return "test";
	}

}
