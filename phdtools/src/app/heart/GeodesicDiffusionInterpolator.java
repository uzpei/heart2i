
package app.heart;

import extension.matlab.MatlabIO;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import tools.interpolation.GaussianInterpolator;
import tools.interpolation.GaussianInterpolator.METRIC;
import tools.interpolation.ParameterVector;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethod;
import volume.VolumeRenderer;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import app.dti.DiffusionSplitter;

public class GeodesicDiffusionInterpolator implements GLObject, Interactor {
    
    private JoglRenderer jr;
    
    private IntensityVolumeMask mask;

    private IntensityVolume geodesicMap;

    private IntensityVolume data;
    
    public IntensityVolumeMask controlMask = null;
    public IntensityVolumeMask interpolatedMask = null;
    public IntensityVolume interpolatedVolume = null;
    
    private VolumeRenderer controlRenderer;
    private VolumeRenderer interpolatedRenderer;
    
    private IntParameter splitIndex = new IntParameter("Z-split fraction", 40, 1, 100);

    private GaussianInterpolator interpolator = new GaussianInterpolator(METRIC.EXP);

    private BooleanParameter singleSliceMode = new BooleanParameter("single slicing", true);
    private IntParameter slabWidth = new IntParameter("slab width", 2, 1, 100);
    private BooleanParameter drawInterpolated = new BooleanParameter("draw interpolated", true);
    private BooleanParameter drawControl = new BooleanParameter("draw control", true);
    
    
	private VoxelBox controlVoxelBox;
	private VoxelBox interpolatedVoxelBox;

	public static void main(String[] args) {
		new DiffusionInterpolator();
	}
	
    public GeodesicDiffusionInterpolator() {

		jr = new JoglRenderer("Heart", this);
    	controlRenderer = new VolumeRenderer(jr);
    	interpolatedRenderer = new VolumeRenderer(jr);
    	
//    	String path = "./data/heart/rats/rat07052008/mat/";
    	String path = "./data/heart/rats/rat07052008/registered/";
    	
    	// Load volumes
    	geodesicMap = new IntensityVolumeMask(MatlabIO.loadMAT(path + "e1x.mat"));
		mask = new IntensityVolumeMask(MatlabIO.loadMAT(path + "e1x.mat"));
		data = new IntensityVolumeMask(MatlabIO.loadMAT(path + "e1x.mat"));
//		data = new IntensityVolumeMask(MatlabIO.loadMAT("./data/heart/rats/rat07052008/mat/processed/helicoidfit/kb_n3_n3.mat"));
		
		controlVoxelBox = new VoxelBox(mask);
		interpolatedVoxelBox = new VoxelBox(mask);
		
		doSplit();

		// Set interpolator parameters
		interpolator.setNorm(METRIC.EXP);
		interpolator.setPower(2.0);
		interpolator.setSigma(10);
		
		// Add custom panels
        VerticalFlowPanel vpanel1 = new VerticalFlowPanel();
        vpanel1.add(controlVoxelBox.getControls());
        vpanel1.add(controlRenderer.getControls());
        VerticalFlowPanel vpanel2 = new VerticalFlowPanel();
        vpanel2.add(interpolatedVoxelBox.getControls());
        vpanel2.add(interpolatedRenderer.getControls());
        jr.addControl("Interpolant", vpanel1.getPanel());
        jr.addControl("Interpolated", vpanel2.getPanel());
        
        ParameterListener splitParam = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				doSplit();
			}
		};
		
        splitIndex.addParameterListener(splitParam);
        singleSliceMode.addParameterListener(splitParam);
        slabWidth.addParameterListener(splitParam);
        
    	jr.addInteractor(this);
        jr.start();

        jr.addInteractor(controlVoxelBox);
        jr.addInteractor(interpolatedVoxelBox);
        
		controlVoxelBox.cut(CuttingPlane.FIT);
//		vboxPolated.cut(CuttingPlane.FIT);
		interpolatedVoxelBox.setSpan(new Vector3d(55, 64, 39));
		interpolatedVoxelBox.setOrigin(new Point3i(3, 0, 41));
		
    	// Use low resolution for debugging purposes
    	controlRenderer.setLowResolution();
    	interpolatedRenderer.setLowResolution();
    	
    	// Use max absorption for debugging purposes
    	controlRenderer.setEmissivity(100);
    	interpolatedRenderer.setEmissivity(100);
    }
    
    private int[] availableControlSlices;
    
    private void doSplit() {
    	if (singleSliceMode.getValue()) {
    		computeSingleSplit();
    	}
    	else {
    		computeMultipleAxialSplit();
    	}
    }
    
    private void computeMultipleAxialSplit() {
    	// Compute split mask
		IntensityVolumeMask[] vols = DiffusionSplitter.splitMask(mask, FrameAxis.Z, splitIndex.getValue());
		setInterpolationVolumes(vols[0], vols[1]);
		
		// Set available slices explicitly (for faster lookup)
		availableControlSlices = DiffusionSplitter.getSplitSlices(mask, FrameAxis.Z, splitIndex.getValue());
    }
    
    private void setInterpolationVolumes(IntensityVolumeMask controlMask, IntensityVolumeMask interpolatedMask) {
		this.controlMask = controlMask;
		
		IntensityVolume interpolantVolumeData = new IntensityVolume(data.hpOut(controlMask));
		
		// Second volume is the region to interpolate
		IntensityVolume interpolatedVolumeData = new IntensityVolume(data.hpOut(interpolatedMask));
		
		controlVoxelBox.setMask(controlMask);
		controlVoxelBox.cut(CuttingPlane.ALL);
		controlRenderer.setVolume(interpolantVolumeData, controlMask, controlVoxelBox);
		
		interpolatedVoxelBox.setMask(interpolatedMask);
		interpolatedVoxelBox.cut(CuttingPlane.ALL);
		interpolatedRenderer.setVolume(interpolatedVolumeData, interpolatedMask, interpolatedVoxelBox);

    }

    private void computeSingleSplit() {
    	
    	int splitIndex = this.splitIndex.getValue();
    	
		controlMask = new IntensityVolumeMask(mask.getDimension());
		interpolatedMask = new IntensityVolumeMask(mask.getDimension());
		
		// Mask for control point is a few slices above and below
		int nslices = slabWidth.getValue();

    	// Make sure we won't be going out of range
		if (splitIndex - nslices < 0) {
			splitIndex = nslices;
		}
		else if (splitIndex + nslices > mask.getDimension()[2]) {
			splitIndex = mask.getDimension()[2] - nslices - 1;
		}

		for (int z = 1; z <= nslices; z++) {
			// Sample above and below
			int zup = splitIndex + z;
			int zdown = splitIndex - z;
			
			controlMask.setZ(mask, zup);
			controlMask.setZ(mask, zdown);
		}
		
		availableControlSlices = MathToolset.linearIntegerSpace(splitIndex - nslices, splitIndex + nslices);

		// Interpolated volume is a single slice
		interpolatedMask.setZ(mask, splitIndex);
		
		setInterpolationVolumes(controlMask, interpolatedMask);
    }

	private void interpolate() {
		SimpleTimer timer = new SimpleTimer();
		timer.tick();
		
		interpolatedVolume = new IntensityVolume(interpolatedMask.getDimension());
		
		final Point3d p = new Point3d();
		
		final List<ParameterVector> controlPoints = new LinkedList<ParameterVector>();
		
		// Number of radial steps
		final int n = 2;

		final double twoPi = 2 * Math.PI;

		// Precompute the exact deltaTheta to cover all neighbors for each r
		// (this is exactly atan(1/r)) but for discrete purposes
		// use the number of voxels on the circumference of a square grid of
		// unit length 1: 8 * i
		final double dtheta[] = new double[n];
		for (int r = 1; r <= n; r++) {
//			dtheta[r-1] = Math.atan(1d / r);
			dtheta[r-1] = twoPi / (8 * r);
		}
		
		// Precompute all neighbor offsets
		final HashSet<int[]> neighbors = new HashSet<int[]>();
		
		for (int nr = 1; nr <= n; nr++) {
//			double r = nr * sqrt2;
			double r = nr;

			// Number of angular steps
			int thetaSteps = MathToolset.roundInt(twoPi / dtheta[nr - 1]);
			
//			System.out.println("Creating new array at r = " + r + "...");
			for (int ntheta = 0; ntheta < thetaSteps; ntheta++) {
				double theta = ntheta * dtheta[nr - 1];
				// Compute neighbor position
				int px = MathToolset.roundInt(r * Math.cos(theta));
				int py = MathToolset.roundInt(r * Math.sin(theta));
				neighbors.add(new int[] { px, py } );
			}
		}
		
		for (int[] values : neighbors) {
			System.out.println("[ " + values[0] + ", " + values[1] + "]");
		}
		
		final Point3d pneighbor = new Point3d();

		/*
		 * Precompute geodesic map for all control slices.
		 * Assumes integer space where�distance is 0 to max(sx, sy)
		 */
//		HashMap<Integer, List<Point3i>> geomap = new HashMap<Integer, List<Point3i>>();
		int[] dim = controlMask.getDimension();
		int maxDist = Math.max(dim[0], dim[1]);
		List<Point3i>[] geoLists = new LinkedList[maxDist];
		
		for (int geo = 0; geo < maxDist; geo++)
			geoLists[geo] = new LinkedList<Point3i>();
		
		for (int zSlice : availableControlSlices) {
			for (int x = 0; x < dim[0]; x++) {
				for (int y = 0; y < dim[1]; y++) {
					if (!controlMask.isMasked(x, y, zSlice)) {
						geoLists[MathToolset.roundInt(geodesicMap.get(x, y, zSlice))].add(new Point3i(x, y, zSlice));
					}
				}				
			}
		}
		
		// For each interpolated voxel
		interpolatedVoxelBox.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				controlPoints.clear();

				// Find the natural coordinate of the current point
				double gmap = geodesicMap.get(x, y, z);
				
				for (int zSlice : availableControlSlices) {
					for (int[] pos : neighbors) {
//						pneighbori.set(x + pos[0], y + pos[1], zSlice);
						if (!controlVoxelBox.isMaskedUnbounded(x + pos[0], y + pos[1], zSlice)) {
							// Add to control points
							pneighbor.set(x + pos[0], y + pos[1], zSlice);
							controlPoints.add(new ParameterVector(pneighbor,
									data.get(x + pos[0], y + pos[1], zSlice)));
						}
					}

				}
				if (controlPoints.size() > 0) {
					p.set(x, y, z);
					ParameterVector result = interpolator.interpolate(p, controlPoints);
					interpolatedVolume.set(x, y, z, result.data[0]);
				}
				else {
					interpolatedVolume.set(x, y, z, -1 + 2 * Math.random());
				}
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});

		interpolatedRenderer.setVolume(interpolatedVolume, interpolatedMask, interpolatedVoxelBox);
		
		System.out.println("Running time = " + timer.tick_s());
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		controlRenderer.init(drawable);
		interpolatedRenderer.init(drawable);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		WorldAxis.display(gl);
		
		gl.glPushMatrix();
		controlVoxelBox.applyViewTransformation(gl);
		
		controlVoxelBox.display(drawable);
		interpolatedVoxelBox.display(drawable);
		gl.glPopMatrix();
		
//		gl.glPushMatrix();
//		vboxPolated.applyViewTransformation(gl);
//		Point3d center = vboxPolated.getCenter();
//		System.out.println(center);
//		int maxdim = vboxPolant.getMaxDimension();
//		gl.glTranslated(center.x / maxdim, center.y / maxdim, center.z / maxdim);
		
		if (drawControl.getValue())
			controlRenderer.display(drawable);
		
		if (drawInterpolated.getValue())
			interpolatedRenderer.display(drawable);
//		gl.glPopMatrix();
	}

	@Override
	public String getName() {
		return "Diffusion Splitter";
	}

	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_I) {
					interpolate();
				}
			}
		});
			
	}
	
	@Override
	public JPanel getControls() {
        // Create UI panels
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(splitIndex.getSliderControls());
		vfp.add(singleSliceMode.getControls());
		vfp.add(slabWidth.getSliderControls());
		vfp.add(drawControl.getControls());
		vfp.add(drawInterpolated.getControls());
		vfp.add(interpolator.getControls());
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
