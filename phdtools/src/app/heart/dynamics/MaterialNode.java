package app.heart.dynamics;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class MaterialNode {

	public Point3d p = new Point3d();
	public Vector3d v = new Vector3d();
	public int index;
	
	public MaterialNode(int index, Point3d p, Vector3d v) {
		this.index = index;
		this.p = p;
		this.v = v;
	}
	
	public String getMaterialXML() {
//		StringBuilder s = new StringBuilder();
//		s.append("<material id=\"" + index + "\" name=\"Material" + index + "\" type=\"muscle material\">\n");
//		s.append("<g1>500</g1>\n");
//		s.append("<g2>500</g2>\n");
//		s.append( "<p1>0.05</p1>\n");
//		s.append( "<p2>6.6</p2>\n");
//		s.append( "<smax>3e5</smax>\n");
//		s.append( "<Lofl>1.07</Lofl>\n");
//		s.append( "<lambda>1.4</lambda>\n");
//		s.append( "<k>1e6</k>\n");
//		s.append( "<fiber type=\"vector\">" + v.x + "," + v.y + "," + v.z + "</fiber>\n");
//		s.append( "</material>\n\n");
//		return s.toString();
		
		return "<element id=\"" + index + "\">" + "<fiber>" + v.x + "," + v.y + "," + v.z + "</fiber>" + "</element>";
		
	}
}
