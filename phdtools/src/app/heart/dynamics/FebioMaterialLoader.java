package app.heart.dynamics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import tools.loader.LineReader;
import tools.loader.LineWriter;

public class FebioMaterialLoader {

	public static void main(String[] args) {
//		File file = new File("./data/stacom/DS_0912_Hexa.mesh");
		File file = new File("./data/stacom/DS_0912_FibreVector.txt");
		
		readFibreVector(file);
	}
	
	public static void readFibreVector(File file) {
		LineReader reader = new LineReader(file);
		String line = "";
		LinkedList<MaterialNode> nodes = new LinkedList<MaterialNode>();
		
		// skip first line
		reader.readLineQuick();
		while ((line = reader.readLineQuick()) != null) {
//			System.out.println(line);
			StringTokenizer tokenizer = new StringTokenizer(line, " ");
//			System.out.println(tokenizer.nextToken());
			int nodeIndex = Integer.parseInt(tokenizer.nextToken());
			Point3d p = new Point3d(Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()));
			Vector3d v = new Vector3d(Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()));
			MaterialNode node = new MaterialNode(nodeIndex, p, v);
			nodes.add(node);
		}		
		
		StringBuilder buffer = new StringBuilder();
		buffer.append("<ElementData>");
		buffer.append("\n");
		for (MaterialNode node : nodes) {
			buffer.append("\t");
			buffer.append(node.getMaterialXML());
			buffer.append("\n");
		}
		buffer.append("</ElementData>");
		
		try {
			LineWriter.write(file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - 4) + "_fiber.txt", buffer.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void readMesh(String mesh) {
		LineReader reader = new LineReader(mesh);
		String line = "";
		ArrayList<Point3d> pts = null;
		while ((line = reader.readLineQuick()) != null) {
			// begin vertices
			if (line.equals("Vertices")) {
				// number of vertices
				int numVertices = Integer.parseInt(reader.readLineQuick());
				pts = new ArrayList<>(numVertices);
				
				// read all vertices
				for (int i = 0; i < numVertices; i++) {
					line = reader.readLineQuick();
					
					// separate coordinates
					StringTokenizer tokenizer = new StringTokenizer(line, "\t");
					Point3d p = new Point3d(Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()));
					pts.add(p);
					System.out.println(p);
				}
			}
			
		}

	}
}
