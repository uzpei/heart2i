package app.heart;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.texture.VolumeTexture;
import heart.Heart;
import heart.HeartManager;
import heart.geometry.PolarMap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import diffgeom.DifferentialOneForm.MovingAxis;
import math.matrix.Matrix3d;
import swing.component.EnumComboBox;
import swing.component.JColorMap;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import system.object.Pair;
import tools.SimpleTimer;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.VolumeRenderer;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import voxel.VoxelBox.CuttingPlane;

public class HeartOneFormStatistics implements GLObject, Interactor {

	private VolumeRenderer oneFormRenderer;

	private VolumeRenderer divergenceRenderer;

    private Heart heart;
    
    private HeartManager manager;
    
    private JoglRenderer jr;
    
    private IntensityVolume[][][] oneForms;

    private JoglTextRenderer textRenderer;
        
    private EnumComboBox<MovingAxis> ecbi = new EnumComboBox<MovingAxis>("Ei", MovingAxis.F1);
	private EnumComboBox<MovingAxis> ecbj = new EnumComboBox<MovingAxis>("Ej", MovingAxis.F2);
	private EnumComboBox<MovingAxis> ecbk = new EnumComboBox<MovingAxis>("Ek", MovingAxis.F3);

    private EnumComboBox<MovingAxis> ecbi2 = new EnumComboBox<MovingAxis>("Ei2", MovingAxis.F1);
	private EnumComboBox<MovingAxis> ecbj2 = new EnumComboBox<MovingAxis>("Ej2", MovingAxis.F2);
	private EnumComboBox<MovingAxis> ecbk2 = new EnumComboBox<MovingAxis>("Ek2", MovingAxis.F3);

	private BooleanParameter enableDivergence = new BooleanParameter("compute divergence", false);
	private BooleanParameter enableCurl = new BooleanParameter("compute curl", false);
	private BooleanParameter enableOneForms = new BooleanParameter("compute one-forms", true);

	private BooleanParameter enableDivergenceAbsolute = new BooleanParameter("absolute value", false);
	private BooleanParameter enableOneFormAbsolute = new BooleanParameter("absolute value", false);

    private IntensityVolume divergence;
    private VoxelVectorField curl;
    private IntensityVolume oneForm;
    
    private double divergenceMean;
    private double oneFormMean;
    private double[] oneFormMinMax = new double[2];
    private double oneFormStd;
    
    private BooleanParameter project = new BooleanParameter("project", true);
    private BooleanParameter trace = new BooleanParameter("trace", false);
    
    private boolean initialized = false;

	public static void main(String[] args) {
//		JoglRenderer.applyNimbusLookAndFeel();
		
		new HeartOneFormStatistics();
    }
 
    public HeartOneFormStatistics() {

    	this.manager = new HeartManager(true);
    	
		// Create the OpenGL renderer
		jr = new JoglRenderer("Heart Math Toolbox");
		
		jr.addInteractor(this);
        divergenceRenderer = new VolumeRenderer(jr);
        oneFormRenderer = new VolumeRenderer(jr);
        
        divergenceRenderer.setVisible(false);
        oneFormRenderer.setVisible(false);

        // Create UI
        createUI();

		// Add control panels
		jr.addControl("Divergence", getDivergencePanel());
//        jr.addControl("Curl", getCurlPanel());
        jr.addControl("One-forms", getOneFormPanel());

		// Start rendering
        jr.getCamera().zoom(-100);
        jr.start(this);
        
        initialized = true;
        
		// Load in the current heart
		load(manager.getCurrent());
    }

    private void exportOneForms() {
    	String filename = "./data/heartStats.txt";
//    	MatlabArrayLoader.save()
    }
    
    private void exportFrameField() {
    	VoxelFrameField vff = heart.getFrameField();
    	vff.export("./data/frameField/" + heart.getID() + "/", heart.getVoxelBox());
//    	heart.saveExtraction();
    }
    
    private void computeStatistics() {
    	if (heart == null || !initialized) return;

    	new Thread(new Runnable() {
			
			@Override
			public void run() {
		    	System.out.println("Computing statistics...");
		    	if (enableDivergence.getValue()) 
		    		computeDivergence();
		    	else
		    		divergence = null;

		    	if (enableCurl.getValue()) 
		    		computeCurl();
		    	else 
		    		curl = null;
		    	
		    	if (enableOneForms.getValue()) 
		    		computeOneForms(heart.getMask());
		    	else
		    		oneForms = null;
		    	
		    	updateVolumes();
		    	
		    	System.out.println("done.");
			}
		}).start();
    }
    
    private void computeDivergence() {
	 	
    	System.out.println("Computing divergence...");
    	divergence = heart.getFrameField().getF1().computeDivergence(1);
    	divergenceRenderer.setVolume(divergence, heart.getMask().extrude(oneForm, Double.NaN), heart.getVoxelBox());
    	
    }
  
    private void computeCurl() {
    	System.out.println("Computing curl...");
    	
    	curl = heart.getFrameField().getF1().computeCurl(1);
		curl.setVoxelBox(heart.getVoxelBox());    	
		curl.setColor(new double[] { 1, 1, 0 });
    }
  
    private void computeOneForms(IntensityVolumeMask mask) {
    	SimpleTimer timer = new SimpleTimer();
    	System.out.println("Computing one forms...");
    	timer.tick();
    	Matrix3d[][][] oneForms = DifferentialOneForm.computeAllOneForms(heart.getFrameField(), mask);
    	this.oneForms = new IntensityVolume[3][3][3];
    	
    	for (int i = 0; i < 3; i++) {
        	for (int j = 0; j < 3; j++) {
            	for (int k = 0; k < 3; k++) {
            		this.oneForms[i][j][k] = oneForms[i][j][k].asVolumeCopy();
            	}
        	}
    	}
    	System.out.println("Total time = " + timer.tick());
    }
    
	protected void setOneFormVolume() {
		if (oneForms == null) return;
		
		MovingAxis ei = ecbi.getSelected();
		MovingAxis ej = ecbj.getSelected();
		MovingAxis ek = ecbk.getSelected();

		MovingAxis ei2 = ecbi2.getSelected();
		MovingAxis ej2 = ecbj2.getSelected();
		MovingAxis ek2 = ecbk2.getSelected();

		IntensityVolume oneForm1 = oneForms[ei.getIndex()][ej.getIndex()][ek.getIndex()];
		IntensityVolume oneForm2 = oneForms[ei2.getIndex()][ej2.getIndex()][ek2.getIndex()];
//		
//		// Take absolute value and add
//		oneForm = new IntensityVolume(oneForm1.getDimension());
//		
//		if (enableOneFormAbsolute.getValue()) {
//			oneForm.add(oneForm1.absOut());
//			oneForm.add(oneForm2.absOut());
//		}
//		else {
//			oneForm.add(oneForm1);
//			oneForm.add(oneForm2);
//		}
//		oneForm.scale(0.5);
		
		oneForm = oneForms[ei.getIndex()][ej.getIndex()][ek.getIndex()];

		if (oneFormRenderer.isVisible()) {
			updateRenderers(1);
		}
		
		oneFormMean = oneForm.getMean(heart.getVoxelBox());
		oneFormStd = oneForm.getStandardDeviation(heart.getVoxelBox());
		oneFormMinMax = oneForm.getminMax();
	}
	
	private void updateRenderers(int rendererIndex) {
		if (rendererIndex == 1)
			oneFormRenderer.setVolume(oneForm, heart.getMask(), heart.getVoxelBox());

		if (rendererIndex == 2) {
			IntensityVolume divergenceProcessed = divergence;
			if (enableDivergenceAbsolute.getValue()) {
				divergenceProcessed = divergence.absOut().asVolume();
			}
			divergenceRenderer.setVolume(divergenceProcessed, heart.getMask().extrude(divergenceProcessed, Double.NaN), heart.getVoxelBox());
		}
	}
	
	private void setDivergenceVolume() {
		if (divergence != null) {
			IntensityVolume divergenceProcessed = divergence;
			if (enableDivergenceAbsolute.getValue()) {
				divergenceProcessed = divergence.absOut().asVolume();
			}
			
			if (divergenceRenderer.isVisible()) {
				updateRenderers(2);
			}
			
			divergenceMean = divergenceProcessed.getMean(heart.getVoxelBox());
		}
	}

	private void updateHeart() {
    	if (heart == null) return;
    	
    	computeStatistics();
	}
	
	private boolean interactorAdded = false;
	
    private void load(Heart heart) {
    	if (heart == null) return;
    	
    	System.out.println("Loading heart from " + heart.getDirectory());
    	
    	// Remove previous interactor
    	if (this.heart != null) {
			jr.removeInteractor(this.heart.getVoxelBox());
    	}

    	this.heart = heart;
    	heart.getVoxelBox().cut(CuttingPlane.TRANSVERSE);
//    	heart.getVoxelBox().addParameterListener(new ParameterListener() {
//			
//			@Override
//			public void parameterChanged(Parameter parameter) {
//				if (project.getValue())
//					constructPolarMap();
//			}
//		});
		
//		b0renderer.setVolume(heart.getMask());
    	computeStatistics();

    	// Add interactors    	
        jr.addInteractor(heart.getVoxelBox());
        
    	heart.getVoxelBox().cut(CuttingPlane.CUT);
    }
    
//    private void load() {
//    	Heart heart = manager.getCurrent();
//    	System.out.println("Loading heart from " + heart.getDirectory());
//    	
//    	// Remove previous interactor
//    	if (this.heart != null) {
//			jr.removeInteractor(heart.getVoxelBox());
//    	}
//    	
//    	this.heart = heart;
//    	
//    	// Add interactors    	
//        jr.addInteractor(heart.getVoxelBox());
//        
//        if (!interactorAdded) {
//        	jr.addInteractor(this);
//        	interactorAdded = true;
//        }
//
//    	// Compute all statistics
//    	computeStatistics();
//    	
//    	manager.updateHeartUI();
//    }
    
    private void updateVolumes() {

    	setDivergenceVolume();
    	
		setOneFormVolume();
    }
    
	@Override
	public void init(GLAutoDrawable drawable) {
		divergenceRenderer.init(drawable);
		oneFormRenderer.init(drawable);
	}
	
	private PolarMap<Double> map = null;
	private JColorMap colorMap = new JColorMap("Polar map", 0, 1, Color.BLUE, Color.RED, Color.BLACK);

	private void constructPolarMap() {
		if (heart == null || heart.getGeometry() == null || heart.getVoxelBox() == null || oneForm == null)
			return;
		
		System.out.println("Constructing polar map...");
		map = heart.getGeometry().polarMap(oneForm, 1, 70, heart.getVoxelBox());
		List<Pair<Double, Double>> minMax = map.minMax();
		Pair<Double, Double> magMean = minMax.get(0);

		colorMap.setMinMax(magMean.getLeft(), magMean.getRight());
		System.out.println("Done.");
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		if (heart == null) return; 
		
		oneFormRenderer.display(drawable);
		divergenceRenderer.display(drawable);

		gl.glDisable(GL2.GL_LIGHTING);
				
		if (heart != null) {
			heart.display(drawable);
			
			gl.glPushMatrix();
			heart.getVoxelBox().applyHeartNormalization(gl);
			Point3d center = heart.getVoxelBox().getCenter();
			
			if (project.getValue()) {
				if (map == null)
					constructPolarMap();
				else{
					gl.glTranslated(center.x, center.y, center.z);
//					map.displayAsDouble(drawable, true);
					map.displayAsDouble(drawable, true, colorMap);
				}
			}
			
			// Expand (trace) the manifold generated from a single point
			if (trace.getValue() && oneForms != null
					&& oneForms[0][1][0] != null && oneForms[0][2][1] != null) {
				IntensityVolume c121 = oneForms[0][1][0];
				IntensityVolume c131 = oneForms[0][2][0];

				// Try to find a valid voxel

				for (Point3i pt0 : heart.getVoxelBox().getVolume()) {
					// pt0 = new Point3i(48,30,35);

					if (pt0 != null
							&& heart.getVoxelBox().dimensionContains(pt0)
							&& !heart.getMask().isMasked(pt0)) {
						Vector3d f1 = heart.getFrameField().getF1()
								.getVector3d(pt0.x, pt0.y, pt0.z);
						f1.normalize();

						int n = 200;

						gl.glDisable(GL2.GL_LIGHTING);
						gl.glColor3d(1d, 1d, 1d);
						gl.glBegin(GL2.GL_LINE_STRIP);
						Point3d p0 = new Point3d(pt0.x, pt0.y, pt0.z);
						Vector3d f = new Vector3d(f1);

						for (int i = 0; i < n; i++) {
							Point3i p0i = MathToolset.tuple3dTo3i(p0);

							if (!heart.getVoxelBox().dimensionContains(p0i)
									|| heart.getMask().isMasked(p0i))
								break;

							Vector3d f2 = heart.getFrameField().getF2()
									.getVector3d(p0i.x, p0i.y, p0i.z);
							Vector3d f3 = heart.getFrameField().getF3()
									.getVector3d(p0i.x, p0i.y, p0i.z);
							double c12h = DifferentialOneForm.computeOneForms(
									heart.getFrameField().getF1(), heart
											.getFrameField().getF2(), f,
									p0i);
							double c13h = DifferentialOneForm.computeOneForms(
									heart.getFrameField().getF1(), heart
											.getFrameField().getF3(), f,
									p0i);
							f2.scale(c12h);
							f3.scale(c13h);
							f.add(f3);
							f.normalize();

							p0.add(f);

							gl.glVertex3d(p0.x, p0.y, p0.z);
						}
						gl.glEnd();
					}
				}
			}
			gl.glPopMatrix();

		}
		
		if (curl != null)
			curl.display(drawable);
		
		if (textRenderer == null) {
			Font font = new Font("Andale Mono", Font.BOLD, 12);
			textRenderer = new JoglTextRenderer(font, true, false);
			textRenderer.setColor(0f, 0.9f, 0f, 1f);
		}

		List<String> s = new LinkedList<String>();
		s.add("Divergence mean = " + divergenceMean);
		s.add("OneForm mean = " + oneFormMean + " +- " + oneFormStd);
		s.add("OneForm min/max = " + oneFormMinMax[0] + ", " + oneFormMinMax[1]);
		
		textRenderer.drawBottomLeft(s, drawable);
		
	}

	@Override
	public String getName() {
		return "Heart";
	}

	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_C) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							computeStatistics();
						}
					}).start();
				}
				else if (e.getKeyCode() == KeyEvent.VK_M) {
					constructPolarMap();
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
					exportOneForms();
				}
				else if (e.getKeyCode() == KeyEvent.VK_E) {
					exportFrameField();
				}
				else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					heart.getFrameField().setVisible(!heart.getFrameField().isVisible());
				}
			}
		});
			
	}
	
	private VerticalFlowPanel vfp;
	private VerticalFlowPanel cvfp;
	private VerticalFlowPanel dvfp;
	private VerticalFlowPanel ovfp;
	
	private JPanel getDivergencePanel() {
		return dvfp.getPanel();
	}

	private JPanel getCurlPanel() {
		return cvfp.getPanel();
	}

	private JPanel getOneFormPanel() {
		return ovfp.getPanel();
	}

	private void createUI() {
        // Create UI panels
		dvfp = new VerticalFlowPanel();
		ovfp = new VerticalFlowPanel();
		cvfp = new VerticalFlowPanel();
		vfp = new VerticalFlowPanel();
//		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Heart Statistics", TitledBorder.LEFT, TitledBorder.CENTER));

		vfp.add(project.getControls());
		vfp.add(colorMap);
		
		project.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				constructPolarMap();
			}
		});
		
		vfp.add(trace.getControls());

		vfp.add(manager);

		if (heart != null) {
			vfp.add(heart.getVoxelBox().getControls());
			vfp.add(heart.getFrameField().getControls());
		}

		// One-form panel
		VerticalFlowPanel po = new VerticalFlowPanel();
//		CollapsiblePanel cpo = new CollapsiblePanel(po, "One-forms");
		po.add(enableOneForms.getControls());
		po.add(enableOneFormAbsolute.getControls());
		po.add(oneFormRenderer.getControls());
		
		JPanel oneformPanel = new JPanel(new GridLayout(2,3));
		oneformPanel.add(ecbi.getControls());
		oneformPanel.add(ecbj.getControls());
		oneformPanel.add(ecbk.getControls());
		oneformPanel.add(ecbi2.getControls());
		oneformPanel.add(ecbj2.getControls());
		oneformPanel.add(ecbk2.getControls());
		po.add(oneformPanel);
		ovfp.add(po.getPanel());
		
		// Divergence panel
		VerticalFlowPanel pd = new VerticalFlowPanel();
//		CollapsiblePanel cpd = new CollapsiblePanel(pd, "Divergence");
		pd.add(enableDivergence.getControls());
		pd.add(enableDivergenceAbsolute.getControls());
		pd.add(divergenceRenderer.getControls());
		dvfp.add(pd.getPanel());
		
		// Curl panel
		VerticalFlowPanel pc = new VerticalFlowPanel();
//		CollapsiblePanel cpc = new CollapsiblePanel(pc, "Curl");
		pc.add(enableCurl.getControls());

		if (curl != null) {
			pc.add(curl.getControls());
		}
		cvfp.add(pc.getPanel());

		// Add listeners
		ParameterListener oneFormListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				setOneFormVolume();
			}
		};
		ecbi.addParameterListener(oneFormListener);
		ecbj.addParameterListener(oneFormListener);
		ecbk.addParameterListener(oneFormListener);
		ecbi2.addParameterListener(oneFormListener);
		ecbj2.addParameterListener(oneFormListener);
		ecbk2.addParameterListener(oneFormListener);

		enableOneFormAbsolute.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateVolumes();
			}
		});

		enableDivergenceAbsolute.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateVolumes();
			}
		});

		// Load up heart manager
		manager.addHeartSelectionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				load(manager.getCurrent());
			}
		});
		
		manager.addHeartDataChangeListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				updateHeart();
			}
		});
	}
	
	@Override
	public JPanel getControls() {
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
	}
}
