
package app.heart;

import extension.matlab.MatlabIO;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.geom.MathToolset;
import tools.interpolation.GaussianInterpolator;
import tools.interpolation.GaussianInterpolator.METRIC;
import tools.interpolation.ParameterVector;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.VolumeRenderer;
import voxel.VoxelFrameField;

public class DiffusionInterpolator implements GLObject, Interactor {
    
    private JoglRenderer jr;
    
    private VoxelFrameField frameField;
    
    private IntensityVolumeMask mask;

    private IntensityVolume data;
    
    public IntensityVolumeMask controlMask = null;
    public IntensityVolumeMask interpolatedMask = null;
    public IntensityVolume interpolatedVolume = null;
    
    private VolumeRenderer rendererControl;
    private VolumeRenderer rendererInterpolation;
    
    private IntParameter controlSlices = new IntParameter("Z-split fraction", 3, 1, 100);

    private IntParameter selectedSlice = new IntParameter("selected slice", 40, 1, 128);
    
    private GaussianInterpolator interpolator = new GaussianInterpolator(METRIC.EXP);

    private IntParameter slabWidth = new IntParameter("slab width", 1, 1, 100);
    private BooleanParameter drawInterpolated = new BooleanParameter("draw interpolated", true);
    private BooleanParameter drawControl = new BooleanParameter("draw control", true);
    
	public static void main(String[] args) {
		new DiffusionInterpolator();
	}
	
    public DiffusionInterpolator() {

		jr = new JoglRenderer("Heart", this);
    	rendererControl = new VolumeRenderer(jr);
    	rendererInterpolation = new VolumeRenderer(jr);
    	
    	String path = "./data/sample/";
//    	String path = "./data/heart/rats/rat07052008/registered/";
		mask = new IntensityVolumeMask(MatlabIO.loadMAT(path + "e1x.mat"));
		data = new IntensityVolumeMask(MatlabIO.loadMAT(path + "e1x.mat"));
//		data = new IntensityVolumeMask(MatlabIO.loadMAT("./data/heart/rats/rat07052008/mat/processed/helicoidfit/kb_n3_n3.mat"));
		controlMask = new IntensityVolumeMask(mask.getDimension());
		interpolatedMask = new IntensityVolumeMask(mask.getDimension());
		interpolatedVolume = new IntensityVolume(mask.getDimension());
		
		// Set interpolator parameters
		interpolator.setNorm(METRIC.EXP);
		interpolator.setPower(2.0);
		interpolator.setSigma(10);
		
		// Add custom panels
        VerticalFlowPanel vpanel1 = new VerticalFlowPanel();
        vpanel1.add(rendererControl.getControls());
        VerticalFlowPanel vpanel2 = new VerticalFlowPanel();
        vpanel2.add(rendererInterpolation.getControls());
        jr.addControl("Interpolant", vpanel1.getPanel());
        jr.addControl("Interpolated", vpanel2.getPanel());
        
        ParameterListener splitParam = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				doSplit();
			}
		};
		
        controlSlices.addParameterListener(splitParam);
        slabWidth.addParameterListener(splitParam);
        selectedSlice.addParameterListener(splitParam);
        interpolator.addListener(splitParam);
        
    	jr.addInteractor(this);
        jr.start();
    	jr.getCamera().zoom(-100f);
		
    	// Use low resolution for debugging purposes
    	rendererControl.setLowResolution();
    	rendererInterpolation.setLowResolution();
    	
    	// Use max absorption for debugging purposes
    	rendererControl.setEmissivity(100);
    	rendererInterpolation.setEmissivity(100);
    	
    	doSplit();
    }
    
    private List<Integer> availableSlices = new LinkedList<Integer>();

    private void doSplit() {
    	computeSingleSplit();
    	interpolate();
    }
    
    private void setInterpolationVolumes() {
		
		IntensityVolume interpolantVolumeData = new IntensityVolume(data.hpOut(controlMask));
		
		// Second volume is the region to interpolate
		IntensityVolume interpolatedVolumeData = new IntensityVolume(data.getDimension());
		interpolatedVolumeData.add(interpolatedVolume.hpOut(interpolatedMask));
		interpolatedVolumeData.add(data.hpOut(controlMask));
		
		rendererControl.setVolume(interpolantVolumeData, controlMask);
		rendererInterpolation.setVolume(interpolatedVolumeData, interpolatedMask);

    }

    private List<ParameterVector> controlPoints = new LinkedList<ParameterVector>();
    
    private void computeSingleSplit() {
    	System.out.println("Computing split volume...");

		controlMask = new IntensityVolumeMask(mask.getDimension());
		interpolatedMask = new IntensityVolumeMask(mask.getDimension());

		// Number of control slices
    	int numSlices = this.controlSlices.getValue();

		// Thickness of each control slice
		int thickness = slabWidth.getValue() - 1;

		/*
		 * Fill-in control mask
		 */
		int[] slices = MathToolset.linearIntegerSpace(2, mask.getDimension()[2]-1, numSlices);
		
		int[] dims = mask.getDimension();
		int z;
		
		availableSlices.clear();
		for (int slice : slices) {
//			controlMask.setZ(mask, slice);
			for (int dz = -thickness; dz <= thickness; dz++) {
				z = slice + dz;
				
				if (z >= 0 && z < dims[2]) {
					controlMask.setZ(mask, z);
					interpolatedMask.setZ(mask,  z);
					availableSlices.add(z);
				}
			}
		}
		
		/*
		 * Fill-in interpolated mask
		 */
		int z0, zf;
		if (selectedSlice.isChecked()) {
			z0 = selectedSlice.getValue();
			zf = selectedSlice.getValue();
		}
		else {
			z0 = (int) MathToolset.clamp(selectedSlice.getValue() - slabWidth.getValue(), 0, dims[2]-1);
			zf = (int) MathToolset.clamp(selectedSlice.getValue() + slabWidth.getValue(), 0, dims[2]-1);
		}

		for (z = z0; z <= zf; z++) 
			interpolatedMask.setZ(mask, z);	
		
		/*
		 *  Compute control points
		 */
		controlPoints.clear();
		for (int slice : availableSlices) {
			for (int x = 0; x < dims[0]; x++) {
				for (int y = 0; y < dims[1]; y++) {
					if (!controlMask.isOutside(x, y, slice)) {
						controlPoints.add(new ParameterVector(new Point3d(x, y, slice), controlMask.get(x, y, slice)));
					}
				}
			}
		}

		System.out.println(availableSlices.toString());
    }

    private int currentSlice = 0;
    private boolean running = false;
    
	private void interpolate() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				interpolateRun();
			}
		}).run();
	}
	
	private void interpolateRun() {
		if (running) return;
		
		running = true;
		
		SimpleTimer timer = new SimpleTimer();
		timer.tick();
		
		final Point3d p = new Point3d();
		int[] dims = interpolatedMask.getDimension();

		/*
		 * Interpolate volume
		 */
//		interpolatedVolume = new IntensityVolume(interpolatedMask.getDimension());
		interpolatedVolume = new IntensityVolume(data.hpOut(controlMask));

//		List<Point3d> interpolationPoints = new LinkedList<Point3d>();
		int totcount = 0;
//		for (int z = 0; z < dims[2]; z++) {

		int z0, zf;
		if (selectedSlice.isChecked()) {
			z0 = selectedSlice.getValue();
			zf = selectedSlice.getValue();
		}
		else {
//			z0 = 0;
//			zf = dims[2]-1;
			z0 = (int) MathToolset.clamp(selectedSlice.getValue() - slabWidth.getValue(), 0, dims[2]-1);
			zf = (int) MathToolset.clamp(selectedSlice.getValue() + slabWidth.getValue(), 0, dims[2]-1);
		}
		
		System.out.println("Running interpolation from " + z0 + " to " + zf);
		
		for (int z = z0; z <= zf; z++) {
			currentSlice = z;
			for (int x = 0; x < dims[0]; x++) {
				for (int y = 0; y < dims[1]; y++) {
//					if (totcount < 1000 && controlMask.isMasked(x, y, z) && !mask.isMasked(x, y, z)) {
//					if (controlMask.isMasked(x, y, z) && !mask.isMasked(x, y, z)) {
					if (!mask.isOutside(x, y, z)) {
						p.set(x, y, z);
						ParameterVector result = interpolator.interpolate(p, controlPoints);
//						System.out.println(result.data[0]);
						interpolatedVolume.set(x, y, z, result.data[0]);
						totcount++;
					}
				}
			}
		}
		
		System.out.println("Total interpolated points = " + totcount);
		
		setInterpolationVolumes();
		
		System.out.println("Running time = " + timer.tick_s());
		
		running = false;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		rendererControl.init(drawable);
		rendererInterpolation.init(drawable);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		WorldAxis.display(gl, true);
		
//		gl.glPushMatrix();
//		vboxPolated.applyViewTransformation(gl);
//		Point3d center = vboxPolated.getCenter();
//		System.out.println(center);
//		int maxdim = vboxPolant.getMaxDimension();
//		gl.glTranslated(center.x / maxdim, center.y / maxdim, center.z / maxdim);

		gl.glPushMatrix();
		int[] dims = mask.getDimension();
		double normalization = 1d / Math.max(Math.max(dims[0], dims[1]), dims[2]);
		double qscale = 50;
		gl.glScaled(qscale*normalization, qscale*normalization, normalization);
		gl.glTranslated(0, 0, -dims[2] / 2);
		
		int z = selectedSlice.getValue() - 1;
		if (z > dims[2]) 
			z = dims[2] - 1;
		
		gl.glColor4d(1, 1, 1, 0.1);
		gl.glBegin(GL2.GL_QUADS);
		gl.glVertex3d(-1, 1, z);
		gl.glVertex3d(1, 1, z);
		gl.glVertex3d(1, -1, z);
		gl.glVertex3d(-1, -1, z);
		gl.glEnd();
		gl.glPopMatrix();
		
		if (drawControl.getValue())
			rendererControl.display(drawable);
		
		if (drawInterpolated.getValue())
			rendererInterpolation.display(drawable);
//		gl.glPopMatrix();k
		
		jr.getTextRenderer().drawTopLeft("Processing slice #" + (1+currentSlice) + "/" + mask.getDimension()[2], drawable);
	}

	@Override
	public String getName() {
		return "Diffusion Splitter";
	}

	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_I) {
					doSplit();
				}
			}
		});
			
	}
	
	@Override
	public JPanel getControls() {
        // Create UI panels
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(selectedSlice.getSliderControlsExtended());
//		selectedSlice.setChecked(false);
		
		vfp.add(controlSlices.getSliderControls());
		vfp.add(slabWidth.getSliderControls());
		vfp.add(drawControl.getControls());
		vfp.add(drawInterpolated.getControls());
		vfp.add(interpolator.getControls());
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
