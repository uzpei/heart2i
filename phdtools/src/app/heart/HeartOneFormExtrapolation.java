
package app.heart;

import diffgeom.DifferentialOneForm;
import diffgeom.DifferentialOneForm.MovingAxis;
import extension.matlab.MatlabIO;
import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import heart.Heart;
import heart.HeartManager;
import heart.HeartManager.HeartProcessor;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import math.matrix.Matrix3d;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.TextToolset;
import tools.frame.CoordinateFrameSample;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.loader.LineWriter;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.VolumeRenderer;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelVectorField;

public class HeartOneFormExtrapolation implements GLObject, Interactor {

	private final static String DEFAULT_FITPATH = "oneFormExtrapolation/";

	private VolumeRenderer oneFormRenderer;

	private VolumeRenderer errorRenderer;

    private Heart heart;
    
    private HeartManager manager;
    
    private JoglRenderer jr;
    
    private JoglTextRenderer textRenderer;
    
    private IntensityVolume[] errorVolume;
    private IntensityVolumeMask errorVolumeMask;

    private final static boolean DEFAULT_USE_CC = false;
    
    private boolean useCylindricalConsistency = DEFAULT_USE_CC;

    private BooleanParameter sphericalShell = new BooleanParameter("spherical shell", false);

    private BooleanParameter repeatedTangents = new BooleanParameter("repeated tangents", false);
    
    private BooleanParameter mimicGHM = new BooleanParameter("Mimic GHM", false);
    
	private IntDiscreteParameter neighborhoodSize = new IntDiscreteParameter("neighborhood size^3", 3, new int[] { 3, 5, 7, 9 });

	private IntDiscreteParameter numprocs = new IntDiscreteParameter("num processors", 4, new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256 });

    private double oneFormMean;
    private double[] oneFormMinMax = new double[2];
    private double oneFormStd;
    
    private boolean initialized = false;

	private boolean exportOneForms = false;

	public static void main(String[] args) {
		
		/*
		 * Compute and save for testing
		 */
		// Save to local dir
		
		String spec = "rat21012008";
		Heart heart = new Heart("./data/heart/rats/" + spec + "/mat/processed/");
		String outpath;
		
		outpath = "./extern/Matlab/cTNB_" + spec + ".mat";
		Matrix3d cTNB = DifferentialOneForm.computeOneForm(heart.getFrameField(), MovingAxis.F1, MovingAxis.F2, MovingAxis.F3);
		MatlabIO.save(outpath, cTNB);

		outpath = "./extern/Matlab/cNBT_" + spec + ".mat";
		Matrix3d cNBT = DifferentialOneForm.computeOneForm(heart.getFrameField(), MovingAxis.F2, MovingAxis.F3, MovingAxis.F1);
		MatlabIO.save(outpath, cNBT);

		outpath = "./extern/Matlab/cTBB_" + spec + ".mat";
		Matrix3d cTBB = DifferentialOneForm.computeOneForm(heart.getFrameField(), MovingAxis.F1, MovingAxis.F3, MovingAxis.F3);
		MatlabIO.save(outpath, cTBB);

		// Test derivative
		Matrix3d[][] jacobian = DifferentialOneForm.jacobian(heart.getFrameField().getF1());
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Matrix3d m = jacobian[i][j];
				String outp = "./extern/Matlab/J" + i + "" + j + "_" + spec + ".mat"; 
				MatlabIO.save(outp, m);
			}
		}
		
		outpath = "./extern/Matlab/dxTx" + spec + ".mat";
		Matrix3d dxTx = heart.getFrameField().getF1().getVolumes()[0].dxOut();
		MatlabIO.save(outpath, dxTx);

		outpath = "./extern/Matlab/dyTx" + spec + ".mat";
		Matrix3d dyTx = heart.getFrameField().getF1().getVolumes()[0].dyOut();
		MatlabIO.save(outpath, dyTx);

		outpath = "./extern/Matlab/dzTx" + spec + ".mat";
		Matrix3d dzTx = heart.getFrameField().getF1().getVolumes()[0].dzOut();
		MatlabIO.save(outpath, dzTx);

		outpath = "./extern/Matlab/Tx_" + spec + ".mat";
		Matrix3d Tx = heart.getFrameField().getF1().getVolumes()[0];
		MatlabIO.save(outpath, Tx);

		
		int i = 0;
		
		String heartXML = "hearts.xml";
		int numNeighbors = 3;
		boolean spherical = false;
		boolean repeatedNeighbors = false;
		boolean mimicGHM = false;
		int numprocs = 4;
		boolean useCC = true;
		boolean useVisualization = true;
		boolean exportOneForms = true;
		
		if (args.length > 0) {
			System.out.println("HeartOneFormExtrapolation usage: java -Xmx2048m -jar heartoneform.jar HeartXML_OR_HeartFile numNeighbors spherical constant numProcessors numIterations CuttingPlane UseVisualization");
			
			System.out.println(TextToolset.box("Heart one-form extrapolation: parsing command-line arguments...", '*'));
			heartXML = args[i++];
			numNeighbors = Integer.parseInt(args[i++]);
			spherical = Boolean.parseBoolean(args[i++]);
			repeatedNeighbors = Boolean.parseBoolean(args[i++]);
			mimicGHM = Boolean.parseBoolean(args[i++]);
			numprocs = Integer.parseInt(args[i++]);
			useCC = Boolean.parseBoolean(args[i++]);
			useVisualization = Boolean.parseBoolean(args[i++]);
			exportOneForms = Boolean.parseBoolean(args[i++]);
		}

		String argsum = "";
		argsum += "Neighborhood size^3 = " + numNeighbors + "\n";
		argsum += "spherical = " + spherical + "\n";
		argsum += "constant = " + repeatedNeighbors + "\n";
		argsum += "repeated neighbors = " + repeatedNeighbors + "\n";
		argsum += "mimic GHM = " + mimicGHM + "\n";
		argsum += "num processors = " + numprocs + "\n";
		argsum += "use cylindrical consistency = " + useCC + "\n";
		argsum += "use visualization = " + useVisualization + "\n";
		argsum += "export one-forms = " + exportOneForms + "\n";
		System.out.println(argsum);
		
		new HeartOneFormExtrapolation(heartXML, numNeighbors, spherical, repeatedNeighbors, mimicGHM, numprocs, useCC, useVisualization, exportOneForms);
    }
 
    public HeartOneFormExtrapolation(String heartXML, int numNeighbors, boolean spherical, boolean repeatedNeighbors, boolean mimicGHM, int numprocs, boolean useCylindricalConsistency, boolean useVisualization, boolean exportOneForms) {

    	// Model selection
    	this.sphericalShell.setValue(spherical);
    	this.repeatedTangents.setValue(repeatedNeighbors);
    	this.mimicGHM.setValue(mimicGHM);
    	
    	this.useCylindricalConsistency = useCylindricalConsistency;
    	neighborhoodSize.setValue(numNeighbors);
    	this.numprocs.setValue(numprocs);
    	this.exportOneForms  = exportOneForms;
    	
    	manager = new HeartManager(heartXML, useVisualization);

    	if (useVisualization) {
    		// Create the OpenGL renderer
    		jr = new JoglRenderer("Heart Math Toolbox");
        	
            oneFormRenderer = jr.createVolumeRendererInstance();
            errorRenderer = jr.createVolumeRendererInstance();

            // Create UI
            createUI();

    		// Add control panels
    		jr.getControlFrame().add("Error", getErrorPanel());

    		// Start rendering
//            jr.getCamera().zoom(-100);
            jr.start(this);

            initialized = true;

    		// Load in the current heart
    		load(manager.getCurrent());
    	}
    	else {
    		launchAllHearts(manager, numNeighbors, spherical, repeatedNeighbors, mimicGHM, numprocs, useCylindricalConsistency);
    	}
    }

    private void launchAllHearts(final HeartManager heartManager, final int neighborhoodSize, final boolean sphericalShell, final boolean repeatTangents, final boolean mimicGHM, final int numprocs, final boolean useCylindricalConsistency) {
		new Thread(new Runnable() {
			@Override
			public void run() {

				heartManager.processHearts(new HeartProcessor() {
					@Override
					public void process(Heart pheart) {
						if (pheart == null) return;
						heart = pheart;

						if (useCylindricalConsistency) {
							heart.useCylindricalConsistency(true);
						}
						
						heart.getVoxelBox().cut(CuttingPlane.FIT);
						errorVolume = computeExtrapolation(heart, neighborhoodSize, sphericalShell, repeatTangents, mimicGHM, numprocs);
						saveExtrapolation();
					}
				});
			}
		}).start();
    }

    private void saveExtrapolation() {
		String datafile = heart.getDirectory() + DEFAULT_FITPATH;
		
		String extra = "";
		if (useCylindricalConsistency) {
			extra += "_CC";
		}
		if (sphericalShell.getValue()) {
			extra += "_spherical";
		}
		if (repeatedTangents.getValue()) {
			extra += "_repeated";
		}
		if (mimicGHM.getValue()) {
			extra += "_ghm";
		}

		IntensityVolume thetaErrorVolume = errorVolume[0];
		IntensityVolume phiVolume = errorVolume[1];
		
		extra += "_n" + neighborhoodSize.getValue();

		// Export theta error
		MatlabIO.save(datafile + "extrapolationErrorTheta" + extra + ".mat", thetaErrorVolume);
		
		// Export phi error
		MatlabIO.save(datafile + "extrapolationPhi" + extra + ".mat", phiVolume);

		// Save one-form volumes
    	if (exportOneForms) {
    		System.out.println("Exporting one-forms...");
        	Matrix3d[][][] oneForms = DifferentialOneForm.computeAllOneForms(heart.getFrameField(), heart.getMask());
        	int T = 0;
        	int N = 1;
        	int B = 2;
        	
        	MatlabIO.save(datafile + "cTNT.mat", new IntensityVolume(oneForms[T][N][T]));
        	MatlabIO.save(datafile + "cTNN.mat", new IntensityVolume(oneForms[T][N][N]));
        	MatlabIO.save(datafile + "cTNB.mat", new IntensityVolume(oneForms[T][N][B]));
        	MatlabIO.save(datafile + "cTBT.mat", new IntensityVolume(oneForms[T][B][T]));
        	MatlabIO.save(datafile + "cTBN.mat", new IntensityVolume(oneForms[T][B][N]));
        	MatlabIO.save(datafile + "cTBB.mat", new IntensityVolume(oneForms[T][B][B]));
        	MatlabIO.save(datafile + "cNBT.mat", new IntensityVolume(oneForms[N][B][T]));
        	MatlabIO.save(datafile + "cNBN.mat", new IntensityVolume(oneForms[N][B][N]));
        	MatlabIO.save(datafile + "cNBB.mat", new IntensityVolume(oneForms[N][B][B]));
    	}
		
		// Save file with results
		try {
			NumberFormat nf = NumberFormat.getNumberInstance();
			nf.setMinimumFractionDigits(0);
			nf.setMaximumFractionDigits(0);
			Calendar currentDate = Calendar.getInstance(); 
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			IntensityVolumeMask mask = heart.getMask();
			
			String buffer = "";
			buffer += TextToolset.box("One-form Extrapolation Stats", '*');
			buffer += "Heart path = " + heart.getDirectory() + "\n";
			buffer += "Local time = " + dateFormat.format(currentDate.getTime()) + "\n";
			buffer += "Total volume voxel count = " + mask.getVolumeCount() + "\n";
			buffer += "Masked voxel count = " + mask.getMaskedCount() + "\n";
			buffer += "Unmasked voxel count = " + (mask.getVolumeCount()-mask.getMaskedCount()) + "\n";
			buffer += "Neighborhood size = " + neighborhoodSize.getValue() + "\n";
			buffer += "Spherical shell = " + sphericalShell.getValue() + "\n";
			buffer += "Repeated tangents = " + repeatedTangents.getValue() + "\n";
			buffer += "Mean theta error = " + thetaErrorVolume.getMean(mask) + " +- " + thetaErrorVolume.getStandardDeviation(mask) + "\n";
			buffer += "Mean phi = " + phiVolume.getMean(mask) + " +- " + phiVolume.getStandardDeviation(mask) + "\n";
			
			System.out.println(buffer);
			
			LineWriter.write(datafile + "info" + extra + ".txt", buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		System.out.println(TextToolset.box("Fit results saved to " + "extrapolationError" + extra + ".mat", '.'));
    }

    private boolean computationInProgress = false;
    
    private void computeStatistics() {
    	if (heart == null || !initialized) return;

    	while (computationInProgress) {
    		try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	new Thread(new Runnable() {
			
			@Override
			public void run() {
		    	System.out.println("Computing statistics...");
		    	computationInProgress = true;
		    	
		   		computeOneForms();
		    	
		    	updateVolumes();
		    	
//		    	updatedUI();
		    	
		    	computationInProgress = false;
		    	System.out.println("done.");
			}
		}).start();
    }
    private void computeOneForms() {
   		errorVolume = computeExtrapolation(heart, neighborhoodSize.getValue(), sphericalShell.getValue(), repeatedTangents.getValue(), mimicGHM.getValue(), numprocs.getValue());
    }
    
	private void updateHeart() {
    	if (heart == null) return;
    	
    	System.out.println("Updating heart...");
    	
    	computeStatistics();
	}
	
	private boolean interactorAdded = false;
	
    private void load(Heart selectedHeart) {
    	if (selectedHeart == null) 
			return;

    	System.out.println("Loading heart from " + selectedHeart.getDirectory());

    	// Remove previous interactor
    	if (heart != null) {
			jr.removeInteractor(heart.getVoxelBox());
    	}
    	
    	heart = selectedHeart;
    	
    	heart.getVoxelBox().cut(CuttingPlane.FIT);
    	// Add interactors    	
        jr.addInteractor(heart.getVoxelBox());
        
        if (!interactorAdded) {
        	jr.addInteractor(this);
        	interactorAdded = true;
        }

    	// Compute all statistics
    	computeStatistics();
    }
    
    private void erodeMask() {
		heart.getMask().dilate(heart.getVoxelBox(), 5);
		heart.getVoxelBox().setMask(heart.getMask());
		computeStatistics();
    }
    
    private void updateVolumes() {
		
    	// For now only look at theta volume
    	IntensityVolumeMask extrudedMask = new IntensityVolumeMask(heart.getMask().copy().getData());
    	IntensityVolume thetaVolume = errorVolume[0];
    	extrudedMask.extrude(thetaVolume, Double.NaN);
		errorRenderer.setVolume(thetaVolume, extrudedMask, heart.getVoxelBox());

//    	IntensityVolume phiVolume = errorVolume[1];
//		extrudedMask.extrude(phiVolume, Double.NaN);
//		errorRenderer.setVolume(phiVolume, extrudedMask, heart.getVoxelBox());
    }
    
	@Override
	public void init(GLAutoDrawable drawable) {
		oneFormRenderer.init(drawable);
		errorRenderer.init(drawable);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		oneFormRenderer.display(drawable);
		errorRenderer.display(drawable);

		gl.glDisable(GL2.GL_LIGHTING);
		
		if (heart != null) {
			heart.display(drawable);
		}
		
//		WorldAxis.display(gl);
		
		if (textRenderer == null) {
			Font font = new Font("Andale Mono", Font.BOLD, 12);
			textRenderer = new JoglTextRenderer(font, true, false);
			textRenderer.setColor(0f, 0.9f, 0f, 1f);
		}

		List<String> s = new LinkedList<String>();
		s.add("OneForm mean = " + oneFormMean + " +- " + oneFormStd);
		s.add("OneForm min/max = " + oneFormMinMax[0] + ", " + oneFormMinMax[1]);
		
		textRenderer.drawBottomLeft(s, drawable);
		
	}

	@Override
	public String getName() {
		return "Heart";
	}

	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_C) {
					System.out.println("key pressed: " + e.getKeyChar());
					computeStatistics();
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
					saveExtrapolation();
				}
				else if (e.getKeyCode() == KeyEvent.VK_E) {
					erodeMask();
				}
				else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					heart.getFrameField().setVisible(!heart.getFrameField().isVisible());
				}
			}
		});
			
	}
	
	private VerticalFlowPanel vfp;
	private VerticalFlowPanel dvfp;
	
	private JPanel getErrorPanel() {
		return dvfp.getPanel();
	}

	private void createUI() {
        // Create UI panels
		dvfp = new VerticalFlowPanel();
		// Error panel

		dvfp.add(sphericalShell.getControls());
		dvfp.add(repeatedTangents.getControls());
		dvfp.add(neighborhoodSize.getSliderControls());
		dvfp.add(numprocs.getSliderControls());
		
		ParameterListener sl = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				computeStatistics();
			}
		};

		neighborhoodSize.addParameterListener(sl);
		sphericalShell.addParameterListener(sl);
		repeatedTangents.addParameterListener(sl);
		numprocs.addParameterListener(sl);
		
		dvfp.add(errorRenderer.getControls());

		vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Heart Statistics", TitledBorder.LEFT, TitledBorder.CENTER));

		vfp.add(manager);
		// Load up heart manager
		manager.addHeartSelectionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				load(manager.getCurrent());
			}
		});
		
		manager.addHeartDataChangeListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				updateHeart();
			}
		});
	}
	
	@Override
	public JPanel getControls() {
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
	
	private IntensityVolume[] computeExtrapolation(final Heart heart, int neighborhoodSize, final boolean sphericalShell, final boolean repeatedTangents, final boolean mimicGHM, int numprocs) {
		
		CoordinateFrameSample cfs = new CoordinateFrameSample();
		
		/*
		 * Initially fill the volume with NaNs. This is done to remove the use of a mask
		 * for further statistics (i.e. the mask is in effect the non-NaN voxels).
		 */
		final IntensityVolume thetaErrorVolume = new IntensityVolume(heart.getMask().getDimension());
		IntensityVolume.fillWith(Double.NaN, thetaErrorVolume.getData());

		final IntensityVolume phiErrorVolume = new IntensityVolume(heart.getMask().getDimension());
		IntensityVolume.fillWith(Double.NaN, thetaErrorVolume.getData());

		boolean useParallelComputing = numprocs > 1;
		
		System.out.println("Using parallel computing = " + useParallelComputing);
		
		SimpleTimer timer = new SimpleTimer();
		// Get volume to process
		List<Point3i> points = heart.getVoxelBox().getVolume();
    	final List<Point3i> neighbors = cfs.presampleInteger(new int[] { neighborhoodSize, neighborhoodSize, neighborhoodSize }, SamplingStyle.NP_NP_NP, true);

		if (useParallelComputing) {
						
			List<Thread> runningThreads = new LinkedList<Thread>();
			int n = points.size();
			int nums = n / numprocs;
			
			for (int i = 0; i < numprocs; i++) {
	            final List<Point3i> subpoints = new LinkedList<Point3i>();
	        	
	            // Upper bound on the voxel indices
	        	int lim1 = i * nums;
	        	int lim2 = (i+1) * nums;
	        	
	        	if (i == numprocs - 1) {
	        		lim2 = points.size();
	        	}
	        	
	        	subpoints.addAll(points.subList(lim1, lim2));
	        	
	        	if (subpoints.size() == 0) continue;
	        	
				runningThreads.add(new Thread(new Runnable() {
					
					@Override
					public void run() {
						extrapolate(heart, thetaErrorVolume, phiErrorVolume, subpoints, neighbors, sphericalShell, repeatedTangents, mimicGHM);
					}
				}));
			}
			
			// Run all threads
			int tnum = 0;
			timer.tick();
			for (Thread thread : runningThreads) {
				thread.setPriority(Thread.MAX_PRIORITY);
				System.out.print("Launching thread #" + (++tnum) + " ");
				thread.start();
			}
			System.out.println();

			System.out.println("Extrapolation in progress...");

			boolean busy = false;
			do {
				busy = false;
				for (Thread thread : runningThreads) {
					if (thread.isAlive()) {
						busy = true;
						break;
					}
				}
				
				try {
					Thread.sleep(100);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
		
			} while (busy);
			
			System.out.println("Computation total elapsed time = " + timer.observeTick());
		}
		else {
			System.out.println("Extrapolation in progress...");
			timer.tick();
			extrapolate(heart, thetaErrorVolume, phiErrorVolume, points, neighbors, sphericalShell, repeatedTangents, mimicGHM);
			System.out.println("Computation total elapsed time = " + timer.observeTick());
		}
		
		return new IntensityVolume[] { thetaErrorVolume, phiErrorVolume };
	}
	
	private void extrapolate(Heart heart, IntensityVolume thetaErrorVolume, IntensityVolume phiErrorVolume, List<Point3i> points, List<Point3i> neighbors, boolean sphericalShell, boolean repeatedTangents, boolean mimicGHM) {
		Vector3d v = new Vector3d();
		Point3i pneighbor = new Point3i();
		Vector3d T = new Vector3d();
		Vector3d Tneighbor  = new Vector3d();
		Vector3d N = new Vector3d();
		Vector3d B = new Vector3d();
		Vector3d Tp = new Vector3d();
		double vdotT, vdotN, vdotB;
		double cTBvT, cTBvN, cTBvB, cTBv;
		
		Vector3d vT = new Vector3d();
		Vector3d vN = new Vector3d();
		Vector3d vB = new Vector3d();
		Vector3d tdiff = new Vector3d();
		double d;
		
		int neighborCount = neighbors.size() - 1;
		
		VoxelVectorField fieldT = heart.getFrameField().getF1();
		VoxelVectorField fieldN = heart.getFrameField().getF2();
		VoxelVectorField fieldB = heart.getFrameField().getF3();

		for (Point3i pvolume : points) {

			// Get local frames axes
			heart.getFrameField().getF1().getVector3d(pvolume.x, pvolume.y, pvolume.z, T);
			heart.getFrameField().getF2().getVector3d(pvolume.x, pvolume.y, pvolume.z, N);
			heart.getFrameField().getF3().getVector3d(pvolume.x, pvolume.y, pvolume.z, B);

			// Compute updates
			double errorTheta = 0;
			double phi = 0;
			boolean discarded = false;
			
			for (Point3i offset : neighbors) {
				
				// Skip voxel itself
				if (offset.x == 0 && offset.y == 0 && offset. z == 0) continue;
			
				// Neighbor position is current voxel + offset
				pneighbor.add(pvolume, offset);

				// First make sure this neighbor lies within the mask
				// If it does not then we're sitting at a boundary point (or near a hole) and the current voxel needs to be discarded
				if (!heart.getVoxelBox().contains(pneighbor) || heart.getMask().isMasked(pneighbor)) {
					discarded = true;
					break;
				}

				/*
				 * From this point on, we can use this neighbor
				 */

				v.set(offset.x, offset.y, offset.z);
				
				// Compute one-forms connecting the neighbor's offset

				// 1) The first is cTN<v>*N. 
				double cTNv = DifferentialOneForm.computeOneForms(fieldT, fieldN, v, pvolume);

				// 2) The second is cTB<v>*B. 
				// Decompose v in terms of the orthonormal basis (T, N, B):
				// v = vT * T + vN * N + vB * B
				vdotT = v.dot(T);
				vdotN = v.dot(N);
				vdotB = v.dot(B);

				/*
				 * Compute decomposed contraction
				 */
				cTBvT = 0;
				cTBvN = 0;
				cTBvB = 0;

				// For spherical shells, cTBN = cTBB = 0
				if (sphericalShell) {
					cTBvT = vdotT * DifferentialOneForm.computeOneForms(fieldT, fieldB, T, pvolume);
					cTBvN = 0;
					cTBvB = 0;
				} 
				// For repeated tangents, cT* = 0;
				else if (repeatedTangents) {
					cTNv = 0;
					cTBvT = 0;
					cTBvN = 0;
					cTBvB = 0;
				}
				else if (mimicGHM) {
					// c_TB is 0
					cTBvT = 0;
					cTBvN = 0;
					cTBvB = 0;
				}
				// This is the full one-form
				else {
					cTBvT = vdotT * DifferentialOneForm.computeOneForms(fieldT, fieldB, T, pvolume);
					cTBvN = vdotN * DifferentialOneForm.computeOneForms(fieldT, fieldB, N, pvolume);
					cTBvB = vdotB * DifferentialOneForm.computeOneForms(fieldT, fieldB, B, pvolume);
				}

				// Recombine contraction
				cTBv = cTBvT + cTBvN + cTBvB;

				// Combine contractions
				Tp.x = T.x + cTNv * N.x + cTBv * B.x;
				Tp.y = T.y + cTNv * N.y + cTBv * B.y;
				Tp.z = T.z + cTNv * N.z + cTBv * B.z;
				
				Tp.normalize();

				// Compute difference with true orientation
				heart.getFrameField().getF1().getVector3d(pneighbor.x, pneighbor.y, pneighbor.z, Tneighbor);
				errorTheta += Math.acos(Math.abs(Tp.dot(Tneighbor)));

				// Compute phi, the angle the error vector makes in the NB plane
				tdiff.sub(Tp, Tneighbor);
				phi += Math.atan2(tdiff.dot(B), tdiff.dot(N));
			}
			
			if (discarded) {
				thetaErrorVolume.set(pvolume.x, pvolume.y, pvolume.z, Double.NaN);
				phiErrorVolume.set(pvolume.x, pvolume.y, pvolume.z, Double.NaN);
			}
			else {
				thetaErrorVolume.set(pvolume.x, pvolume.y, pvolume.z, errorTheta / neighborCount);
				phiErrorVolume.set(pvolume.x, pvolume.y, pvolume.z, phi / neighborCount);
			}
			
//			if (thetaErrorVolume.get(pvolume.x, pvolume.y, pvolume.z) <= 1e-3) {
//				System.out.println(pvolume);
//			}
		}
	}
	
	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
