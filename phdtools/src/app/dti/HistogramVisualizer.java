package app.dti;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;
import heart.HeartManager;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import swing.SwingTools;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.Histogram;
import swing.component.histogram.HistogramGrid;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.IntParameter;
import swing.parameters.ParameterListener;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.core.experiments.PamiExperimentRunner;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;

public class HistogramVisualizer extends JFrame {

	public static void main(String[] args) {
		new HistogramVisualizer();
	}
	
	private IntParameter histobins = new IntParameter("bins", 0, 0, 0);

	private HeartManager manager;
	
	private HistogramGrid histograms;
	
	private FittingExperimentResult results = null;

	private CartanModel model = CartanModel.FULL;
	
	private EnumComboBox<FittingMethod> ecbMethods = new EnumComboBox<FittingMethod>("Method", FittingMethod.Direct);

	private EnumComboBox<Species> ecbSpecies = new EnumComboBox<Species>("Species", Species.Rat);

	private EnumComboBox<OptimizerType> ecbOptimizer = new EnumComboBox<OptimizerType>("Optimizer", OptimizerType.SIMPLEX);
	
	private IntDiscreteParameter neighborhood = new IntDiscreteParameter("Neighborhood", 3, new int[] { 3, 5, 7 });
	
	private BooleanParameter excludeBoundary = new BooleanParameter("exclude bounary", false);
	private BooleanParameter excludeIslands = new BooleanParameter("exclude islands", true);
	
	/*
	 * Optimization
	 */
	private IntParameter numIterations = new IntParameter("Iterations", 100, 1, 500);
	private BooleanParameter useDirectSeeds = new BooleanParameter("direct seeds", true);
	private IntParameter numThreads = new IntParameter("threads", Runtime.getRuntime().availableProcessors(), 1, 24);
	private DoubleParameter errorThreshold = new DoubleParameter("error threshold", 0, 0, 1);
	private DoubleParameter errorDeltaThreshold = new DoubleParameter("delta threshold", 0, 0, 1);
	private DoubleParameter regularization = new DoubleParameter("regularization", 0, 0, 1);
	private EnumComboBox<CartanModel> ecbCartanModels = new EnumComboBox<CartanModel>(CartanModel.FULL);
	private EnumComboBox<NeighborhoodShape> ecbNeighborhoodShapes = new EnumComboBox<NeighborhoodShape>(NeighborhoodShape.Isotropic);
	
	/*
	 * Stats
	 */
	private int fullVolumeCount, interiorVolumeCount, computationVolumeCount, validVolumeCount;

	private BooleanParameter threshold = new BooleanParameter("apply thresholds", true);
	
	private JButton btnRecompute = new JButton("Recompute");
	private JButton btnReload = new JButton("Reload");
	private VerticalFlowPanel controls = new VerticalFlowPanel("Controls");
	private VerticalFlowPanel vfpOptimized = new VerticalFlowPanel("Optimization");

	private JLabel lblCount1 = new JLabel();
	private JLabel lblCount2 = new JLabel();
	private JLabel lblCount3 = new JLabel();
	private JLabel lblCount4 = new JLabel();
	private JLabel lblTimings = new JLabel();
	
	private JLabel lblStatus = new JLabel("Idle");
	
	public HistogramVisualizer() {

		Dimension histogramSize = new Dimension(300, 200);
		
		manager = new HeartManager();
		
		load();
		
		ecbSpecies.setSelected(manager.getCurrent().getSpecies());
//		controls.add(manager);

		// Status label
		lblStatus.setForeground(Color.red);
		controls.add(lblStatus);
		
		JPanel panelBtns = new JPanel(new GridLayout(1,2));
		panelBtns.add(SwingTools.encapsulate(btnRecompute));
		panelBtns.add(SwingTools.encapsulate(btnReload));
		controls.add(panelBtns);
		JButton btnSnap = new JButton("snapshot");
		btnSnap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				takeSnapshot();
			}
		});
		controls.add(btnSnap);
		
		controls.add(ecbSpecies.getControls());
		controls.add(neighborhood.getSliderControls());
		controls.add(ecbMethods.getControls());

		vfpOptimized.add(ecbOptimizer.getControls());
		
//		private EnumComboBox<CartanModel> ecbCartanModels = new EnumComboBox<CartanModel>(CartanModel.FULL);
//		private EnumComboBox<NeighborhoodShape> ecbNeighborhoodShapes = new EnumComboBox<NeighborhoodShape>(NeighborhoodShape.Isotropic);
		vfpOptimized.add(useDirectSeeds.getControls());
		vfpOptimized.add(numIterations.getSliderControls());
		vfpOptimized.add(numThreads.getSliderControls());
		vfpOptimized.add(errorThreshold.getSliderControls());
		vfpOptimized.add(errorDeltaThreshold.getSliderControls());
		vfpOptimized.add(regularization.getSliderControls());
		vfpOptimized.add(ecbCartanModels.getControls());
		vfpOptimized.add(ecbNeighborhoodShapes.getControls());
		controls.add(vfpOptimized.getPanel());
		
		VerticalFlowPanel vfpStats = new VerticalFlowPanel("Results");
		vfpStats.add(threshold.getControls());
		vfpStats.add(excludeBoundary.getControls());
		vfpStats.add(excludeIslands.getControls());
		vfpStats.add(histobins.getSliderControls());
		vfpStats.add(lblTimings);
		vfpStats.add(lblCount1);
		vfpStats.add(lblCount2);
		vfpStats.add(lblCount3);
		vfpStats.add(lblCount4);
		controls.add(vfpStats.getPanel());
		
		// Create parameter listeners
		createListeners();
		
		histograms = new HistogramGrid(4, 3, histogramSize);
		
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
        constraints.weightx = 0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
		panel.add(controls.getPanel(), constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;
        constraints.weighty = 1;
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		panel.add(histograms, constraints);
		add(panel);
		
		updateUI();
		
		super.pack();
		super.setVisible(true);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		computeExperiment(false);
	}
	
	private void takeSnapshot() {
		SwingTools.saveScreenShotPNG(this.getContentPane(), "./histograms" + ecbMethods.getSelected() + ".png");
	}
	
	private void load() {
		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/sampleSubset/");	
//		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/sample2/rat07052008/registered/");	
//		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/rat07052008/registered/");	
		manager.loadHeart(def);
	}

	private void updateUI() {
		boolean toggle;
		if (ecbMethods.getSelected() == FittingMethod.Optimized) {
			toggle = true;
		}
		else {
			toggle = false;
		}
		
//		vfpOptimized.setEnabled(true);
		vfpOptimized.getPanel().setEnabled(toggle);
		for (Component c : vfpOptimized.getPanel().getComponents())
			c.setEnabled(toggle);

	}
	
	private void createListeners() {
		btnRecompute.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				computeExperiment(true);
			}
		});
		
		btnReload.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				manager.getCurrent().reloadData();
				computeExperiment(true);
			}
		});
		
		ecbMethods.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				updateUI();
			}
		});
		
		histobins.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				histograms.setBinCount(histobins.getValue());
			};
		});
		
		ParameterListener processResultListener = new ParameterListener() {
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				updateResults(results);
			}
		};
		excludeBoundary.addParameterListener(processResultListener);
		excludeIslands.addParameterListener(processResultListener);
		ecbSpecies.addParameterListener(processResultListener);
		threshold.addParameterListener(processResultListener);
	}
	
	private void runExperiment() {

		btnRecompute.setEnabled(false);
		String status = "Computing...";
		setTitle(status);
		lblStatus.setText(status);
		Heart heart = manager.getCurrent();

		// TODO: select fitting method, model, and neighborhood size here
		FittingMethod method = ecbMethods.getSelected();
		int neighborhoodSize = neighborhood.getValue();
		HashMap<Parameter, double[]> bounds = null;
		
		if (threshold.getValue()) {
			bounds = PamiExperimentRunner.getCardiacBounds(ecbSpecies.getSelected());
		}

		results = null;
		
		System.out.println("Computing optimized...");
		CartanFittingParameter fittingParameter = new CartanFittingParameter(bounds, ecbMethods.getSelected(), useDirectSeeds.getValue(), numThreads.getValue(), numIterations.getValue(), errorThreshold.getValue(), errorDeltaThreshold.getValue(), regularization.getValue(), model, ecbNeighborhoodShapes.getSelected(), neighborhoodSize);
//		FittingData dataOptimized = new FittingData(new CartanFittingParameter(bounds, model, FittingMethod.Optimized, neighborhoodSize), heart.getFrameField(), heart.getVoxelBox());
		FittingData dataOptimized = new FittingData(fittingParameter, heart.getFrameField(), heart.getVoxelBox());
		results = new FittingExperimentResult(dataOptimized);
		
		OptimizerType type = ecbOptimizer.getSelected();
		if (ecbMethods.getSelected() != FittingMethod.Optimized)
			type = OptimizerType.DIRECT;
		
		CartanFitter fitter = new CartanFitter(type);
		CartanFitter.setVerboseLevel(3);
		fitter.launchFit(results, true);

		if (results != null) {
			fullVolumeCount = heart.getDimension()[0] * heart.getDimension()[1]
					* heart.getDimension()[2];
			interiorVolumeCount = heart.getVoxelBox().countVoxels();
			computationVolumeCount = results.getResultNodes().size();

			// Compute errors
			PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(results.getFittingData());
			for (CartanParameterNode node : results.getResultNodes()) {
				node.setError(fittingError.evaluate(node, results.getFittingData()
						.getNeighborhood(node.getPosition())));
			}

			updateResults(results);
		}

		status = "Idle";
		setTitle(status);
		lblStatus.setText(status);

		btnRecompute.setEnabled(true);
	}

	private void updateResults(FittingExperimentResult results) {
		// Create a local copy of parameter nodes
		List<CartanParameterNode> resultCopy = new LinkedList<CartanParameterNode>();
		resultCopy.addAll(results.getResultNodes());
		
		PamiExperimentRunner.validate(results, excludeBoundary.getValue(),
				excludeIslands.getValue(), threshold.getValue() ? PamiExperimentRunner.getCardiacBounds(manager.getCurrent().getSpecies()) : null);
		
		validVolumeCount = results.getResultNodes().size();

		lblTimings.setText("Time = " + results.getElapsedTime() + "s");
		lblCount1.setText("-Total count = " + fullVolumeCount);
		lblCount2.setText("-Interior = " + interiorVolumeCount);
		lblCount3.setText("-Computation = " + computationVolumeCount);
		lblCount4.setText("-Valid = " + validVolumeCount);
		
		histobins.hold(true);
		histobins.setMaximum(Histogram.estimateBinCount(validVolumeCount));
		histobins.setValue(Histogram.estimateBinCount(validVolumeCount));
		histobins.hold(false);
		histograms.setBinCount(histobins.getValue());
		
		// Update histograms
		double[][] data = new double[12][results.getResultNodes().size()];

		int indexParameter = 0;
		String[] labels = new String[12];

		for (Parameter p : model) {
			int indexNode = 0;
			for (CartanParameterNode node : results.getResultNodes()) {
				data[indexParameter][indexNode] = node.get(p);
				indexNode++;
			}
			labels[indexParameter] = p.toString().toLowerCase();
			indexParameter++;
		}

		histograms.set(data, labels);
		
		// Put back from copy
		results.getResultNodes().clear();
		results.setResults(resultCopy, results.getElapsedTime());
	}

	private void computeExperiment(boolean asynchronous) {

		if (asynchronous) {
			Executors.newSingleThreadExecutor().submit(new Runnable() {

				@Override
				public void run() {
					runExperiment();
				}
			});

		} else {
			runExperiment();
		}
	}
}
