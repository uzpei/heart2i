
package app.dti;

import gl.geometry.FancyAxis;
import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import gl.texture.VolumeTexture;
import heart.Heart;
import heart.HeartManager;
import heart.HeartTools;
import heart.Tractography;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.JColorMap;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.frame.OrientationFrame.FrameAxis;
import volume.IntensityVolume;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelVectorField;
import app.pami2013.core.tools.helical.HelicalVolume;

public class HeartVisualizer implements GLObject, Interactor {

    private Heart heart;
    
    private Tractography tractography;
    
    private HeartManager manager;
    
    private JoglRenderer jr;
    
    private JoglTextRenderer textRenderer;
    
    private VoxelVectorField normals = null;
    
    private JColorMap colormap = new JColorMap("color map", Color.BLACK, Color.RED, Color.YELLOW);
    
    private DoubleParameterPoint3d wpos = new DoubleParameterPoint3d("world pos", new Point3d(), new Point3d(-1,-1,-1), new Point3d(1,1,1));
    
	public static void main(String[] args) {
//		HeartVisualizer.print("Test 1", "Test 2", new Integer(3), new Double(4));
		
		new HeartVisualizer();
	}
	
	public static void print(Object... objects) {
		for (Object o : objects) {
			System.out.println(o);
		}
	}
 
    public HeartVisualizer() {

    	this.manager = new HeartManager(true);
		// Create the OpenGL renderer
//		jr = new JoglRenderer("Heart Math Toolbox", new Dimension(1000, 1000));
		jr = new JoglRenderer("Heart Math Toolbox");
//        b0renderer = new VolumeRenderer(jr);
        
		// Add control panels
    	jr.addInteractor(this);

		tractography = new Tractography();
    	jr.getControlFrame().add("Tracto", tractography.getControls());
//    	jr.getControlFrame().add("b0", b0renderer.getControls());
    	
		// Start rendering
//        jr.getCamera().zoom(-100);
        jr.start(this);
		
		// Load in the current heart
		load(manager.getCurrent());
    }

    private HelicalVolume helicalVolume;
    private void computeNormals() {
    	this.normals = heart.computeWallNormals();
    	normals.normalize(heart.getVoxelBox());
    	// Compute helical volume of dotted orthogonality
    	final IntensityVolume orthovolume = new IntensityVolume(heart.getDimension());
    	final Vector3d v1 = new Vector3d();
    	final Vector3d v2 = new Vector3d();
    	heart.getVoxelBox().voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				normals.getVector3d(x, y, z, v1);
				heart.getFrameField().getF1().getVector3d(x, y, z, v2);
				double v = Math.abs(v1.dot(v2));
				orthovolume.set(x, y, z, v);
			}
		});
    	
    	heart.getGeometry().update();
    	helicalVolume = new HelicalVolume(orthovolume, heart.getMask(), heart.getGeometry().getCenterline(), colormap);
    }

    private void detachInteractor() {
    	System.out.println("Detaching canvas...");
    	heart.getVoxelBox().detach(jr.getCanvas());
    }
    
    private void exportFrameField() {
    	System.out.println("Exporting heart...");
    	heart.saveExtraction();
    }

    private VolumeTexture b0;
    private boolean b0InitializationRequest = false;
    
    private void load(Heart heart) {
    	if (heart == null) return;
    	
    	System.out.println("Loading heart from " + heart.getDirectory());
    	
    	// Remove previous interactor
    	if (this.heart != null) {
			jr.removeInteractor(this.heart.getVoxelBox());
    	}

    	this.heart = heart;
		tractography.update(heart);
		
		b0 = new VolumeTexture(heart.getDistanceTransform()[0]);
		if (b0 != null)
			b0InitializationRequest = true;
		
//		b0renderer.setVolume(heart.getMask());
    	
    	// Add interactors    	
        jr.addInteractor(heart.getVoxelBox());
        
    	heart.getVoxelBox().addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				tractography.trace();
			}
		});
    	
    	heart.getVoxelBox().cut(CuttingPlane.CUT);
    }
    
	@Override
	public void init(GLAutoDrawable drawable) {
//		b0renderer.init(drawable);
	}
	
	private int framecount = 0;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glDisable(GL2.GL_LIGHTING);

		if (b0 != null && b0InitializationRequest) {
			b0.init(gl);
			b0InitializationRequest = false;
		}
		
//		b0renderer.display(drawable);
		
		Point3d center;
		if (heart != null) {
			center = heart.getVoxelBox().getCenter();
		}
		else {
			center = new Point3d();
		}
		
		center.negate();
		gl.glPushMatrix();
//		gl.glRotated(180, 0, 0, 1);
//		gl.glTranslated(0, -0.3, -0.1);
		gl.glTranslated(wpos.getPoint3d().x, wpos.getPoint3d().y, wpos.getPoint3d().z);
		double fs = 5;
		gl.glScaled(fs, fs, fs);
		
//		FancyAxis fa = new FancyAxis(0.1);
//		fa.draw(gl);
		
		gl.glPopMatrix();

		// For PAMI frame snapshot, use following params:
		/*
		 * Origin = 3, 0, 51
		 * Span = 55, 33, 11 (spacing 1)
		 * Helix coloring, high def
		 * Radius = 0.2 for helix, 0.05 for frame + draw tip
		 * Cam Rx = 280 degrees
		 * Hide voxel box
		 * Draw FPS off
		 * 
		 */
		
		if (heart != null) {
			heart.display(drawable);

			if (normals != null) {
		        gl.glPushMatrix();
		        heart.getVoxelBox().applyHeartNormalization(gl);
//		        normals.display(drawable, heart.getVoxelBox().getVolumeUnchecked());
		        helicalVolume.display(drawable, false);
		        gl.glPopMatrix();
			}
		}
		
		String tracto = "";
		if (tractography != null && heart != null) {
			SimpleTimer timer = new SimpleTimer();
			timer.tick();

			gl.glPushMatrix();
			heart.getVoxelBox().applyHeartNormalization(gl);
//			tractography.display(drawable);
			tractography.display(drawable);
//			b0.display(gl, heart.getVoxelBox());

			gl.glPopMatrix();
			
			tracto = timer.tick_ms();
//			System.out.println(tracto);
		}
		
		
//		WorldAxis.display(gl);

		
//		GLMaterial.BRASS.apply(gl);
//		gl.glEnable(GL2.GL_LIGHTING);
////		Cylinder.display(gl, 8, 0, 0, 0, 0.1f, 1);
//		Vector3f l = new Vector3f(1, 1, 1);
//		l.normalize();
//		new Cylinder(32, new Point3f(), l, 0.2f, 5).display(gl);
		
		if (textRenderer == null) {
			Font font = new Font("Verdana", Font.BOLD, 32);
			textRenderer = new JoglTextRenderer(font, true, false);
			textRenderer.setColor(0f, 0.9f, 0f, 1f);
		}
		
//		textRenderer.setColor(0f, 0f, 0f, 1f);
//		textRenderer.draw("t=" + iteration, drawable, drawable.getWidth() / 2 - 30, 32);
//		textRenderer.setColor(0f, 1f, 0f, 1f);
//		textRenderer.drawTopLeft("dt=" + tracto, drawable);

		framecount++;
	}

	@Override
	public String getName() {
		return "Heart Visualizer";
	}
	
	private int iteration = 0;
	
	private void testSlice() {
		if (heart == null)
			return;
		
		heart.getVoxelBox().sliceAxis(FrameAxis.Z, 0.75);
		
	}

	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_C) {
				}
				else if (e.getKeyCode() == KeyEvent.VK_A) {
					testSlice();
				}
				else if (e.getKeyCode() == KeyEvent.VK_E) {
					exportFrameField();
				}
				else if (e.getKeyCode() == KeyEvent.VK_D) {
					detachInteractor();
				}
				else if (e.getKeyCode() == KeyEvent.VK_N) {
					computeNormals();
				}
				else if (e.getKeyCode() == KeyEvent.VK_T) {
					tractography.trace();
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
					final int numits = 170;
					iteration = 0;
					
					new Thread() {
						private void waitForScreen() {
							while (jr.isScreenshotRequested()) {
								try {
									Thread.sleep(100);
								} catch (InterruptedException ev) {
									// TODO Auto-generated catch block
									ev.printStackTrace();
								}
							}
						}
						
						@Override
						public void run() {
							
							iteration = 0;
							jr.setScreenshotRequest();
							waitForScreen();

							for (iteration = 1; iteration <=  numits; iteration++) {
								
								tractography.nextStep(false);
								tractography.trace();

								// Wait for a few screen updates
								int frame0 = framecount;
								while (framecount < frame0 + 5) {
									try {
										Thread.sleep(100);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								waitForScreen();
								jr.setScreenshotRequest();
								waitForScreen();
							}
						};
					}.start();
				}
				else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					heart.getFrameField().setVisible(!heart.getFrameField().isVisible());
				}
			}
		});
			
	}
	
	@Override
	public JPanel getControls() {
        // Create UI panels
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Heart Visualizer", TitledBorder.LEFT, TitledBorder.CENTER));

		vfp.add(wpos.getControls());
		vfp.add(colormap);
		vfp.add(manager);
		// Load up heart manager
		manager.addHeartSelectionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				load(manager.getCurrent());
			}
		});
		
		manager.addHeartDataChangeListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				load(manager.getCurrent());
			}
		});
		
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
