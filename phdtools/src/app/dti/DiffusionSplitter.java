
package app.dti;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import heart.Heart;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.VolumeRenderer;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;

public class DiffusionSplitter implements GLObject, Interactor {
    
    private JoglRenderer jr;
    
    public IntensityVolume processedVolume = null;
    public IntensityVolume sampleVolume = null;
    
    private VolumeRenderer renderer;

    private IntParameter splitFraction = new IntParameter("Z-split fraction", 10, 1, 100);
    
	public static void main(String[] args) {
		new DiffusionSplitter();
	}
	
	public static void print(Object... objects) {
		for (Object o : objects) {
			System.out.println(o);
		}
	}
 
	private Heart originalHeart;
	
    public DiffusionSplitter() {

		jr = new JoglRenderer("Heart", this);
		
//	    HeartManager manager = new HeartManager(true, false);
//	    String heartPath = "./data/heart/rat/smooth/rat1/";
	    String heartPath = "./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/its5std0d2/";
	    
	    originalHeart = new Heart(heartPath);
	    
    	renderer = new VolumeRenderer(jr);
    	renderer.setVisible(false);
		jr.getControlFrame().add("Volume", renderer.getControls());
    	jr.addInteractor(this);
        jr.start();

		jr.addInteractor(originalHeart.getVoxelBox());
		originalHeart.getVoxelBox().cut(CuttingPlane.TRANSSAGITTAL);
		    	
    	applySplitting();

        splitFraction.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				applySplitting();
			}
		});
    }
    
    private void applySplitting() {
    	// This is really slow
//    	heart = new Heart(originalHeart);
//    	heart = originalHeart;

    	IntensityVolumeMask mask = splitMask(originalHeart.getMask(), FrameAxis.Z, splitFraction.getValue())[0];
		sampleVolume = originalHeart.getFrameField().getF1().getVolumes()[0];
    	renderer.setVolume(sampleVolume, splitMask(mask, FrameAxis.Z, splitFraction.getValue())[0], originalHeart.getVoxelBox());
    	originalHeart.applyMask(mask);
    }

	@Override
	public void init(GLAutoDrawable drawable) {
		renderer.init(drawable);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

//		renderer.display(drawable);

		if (originalHeart != null) {
			originalHeart.display(drawable);
		}
		
		originalHeart.getFrameField().display(drawable);
	}

	@Override
	public String getName() {
		return "Diffusion Splitter";
	}

	@Override
	public void attach(Component component) {
		
		component.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_S) {
					applySplitting();
				}
			}
		});
			
	}
	
	@Override
	public JPanel getControls() {
        // Create UI panels
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(splitFraction.getSliderControls());
		vfp.add(originalHeart.getControls());
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
	
	public static List<Integer> generateSlices(VoxelBox box, int interlacing) {
		int bot = 1+MathToolset.roundInt(box.getOrigin().z);
		int center = MathToolset.roundInt(box.getCenter().z) - 1;
		int top = -1+MathToolset.roundInt(box.getOrigin().z + box.getSpanZ()) - 1;

		List<Integer> slices = new LinkedList<Integer>();
		
		int delta = MathToolset.roundInt((top - bot+1) / ((double)interlacing));
		int[] s = MathToolset.linearIntegerSpace(bot, top, delta);
		if (delta == 1)
			slices.add(center);
		else
			for (int slice : s)
				slices.add(slice);
		
		return slices;
	}
	
	/**
	 * Returns an array containing sliced data, with the last volume containing empty voxels
	 *
	 * @param mask
	 * @param axis
	 * @param interlacing: how many slices to skip in-between samples (1 = no sampling, dimz = 1 slice only)
	 * @return an array containing the input volume with slices removed, and a volume containing only the holes themselves
	 */
	public static IntensityVolumeMask[] splitMask(IntensityVolume mask, FrameAxis axis, int interlacing) {
		int[] dim = mask.getDimension();
		IntensityVolumeMask split = new IntensityVolumeMask(dim);
		IntensityVolumeMask empty = new IntensityVolumeMask(dim);
		
		if (interlacing < 1)
			interlacing = 1;
		
		// TODO: make this axis-independent. i.e. could select arbitrary axis direction
		if (axis.equals(FrameAxis.X)) {
			throw new NotImplementedException();
		}
		else if (axis.equals(FrameAxis.Y)) {
			throw new NotImplementedException();
		}
		else if (axis.equals(FrameAxis.Z)) {
			
			VoxelBox box = new VoxelBox(new IntensityVolumeMask(mask));
			box.cut(CuttingPlane.FIT);
			List<Integer> slices = generateSlices(box, interlacing);

			// "empty" mask contains all in-between slices
			for (int z = 0; z < dim[2]; z ++) {
				empty.setZ(mask, z);
			}
			IntensityVolumeMask zero = new IntensityVolumeMask(split.getDimension());
			for (Integer z : slices) {
				split.setZ(mask, z);
				empty.setZ(zero, z);
			}
		}
		
		return new IntensityVolumeMask[] { split, empty };
	}

	public static int[] getSplitSlices(IntensityVolume mask, FrameAxis axis, int interlacing) {
		int[] dim = mask.getDimension();
		int[] slices = null;
		
		// TODO: make this axis-independent. i.e. could select arbitrary axis direction
		int i = 0;
		if (axis.equals(FrameAxis.X)) {
			slices = new int[1+MathToolset.roundInt(dim[0] / interlacing)];
			for (int x = 0; x < dim[0]; x += interlacing) {
				slices[i++] = x;
			}
		}
		else if (axis.equals(FrameAxis.Y)) {
			slices = new int[1+MathToolset.roundInt(dim[1] / interlacing)];
			for (int y = 0; y < dim[1]; y += interlacing) {
				slices[i++] = y;
			}
		}
		else if (axis.equals(FrameAxis.Z)) {
			slices = new int[1+MathToolset.roundInt(dim[2] / interlacing)];

			for (int z = 0; z < dim[2]; z += interlacing) {
				slices[i++] = z;
			}
		}
		
		return slices;
		
	}
}
