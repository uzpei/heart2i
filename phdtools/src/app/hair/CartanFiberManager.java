package app.hair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import tools.frame.CoordinateFrame;
import tools.interpolation.ParameterVector;
import app.pami2013.cartan.CartanParameter.CartanModel;

/**
 * Handles fiber generation and storage.
 * @author piuze
 */
public class CartanFiberManager {

	private ArrayList<LinkedList<CartanFiber>> fibers;

	private ArrayList<LinkedList<CartanFiber>> controlFib;
	
	private BooleanParameter drawControl = new BooleanParameter("draw control", true);

	private BooleanParameter drawFibers = new BooleanParameter("draw fibers", true);

	public CartanFiberManager() {
		fibers = new ArrayList<LinkedList<CartanFiber>>();
		controlFib = new ArrayList<LinkedList<CartanFiber>>();
	}

	public void generate(CartanSurfaceManager managerSurface, CartanVolumeManager managerVolume, CartanModel model, int numLevels) {
		// Get root locations
		managerSurface.generateSampling();
		List<CoordinateFrame> roots = managerSurface.getRoots();

		// Go through each scale level
		controlFib.clear();
		fibers.clear();
		for (int i = 0; i < numLevels; i++) {
			LinkedList<CartanFiber> cfibers = new LinkedList<CartanFiber>();
			LinkedList<CartanFiber> gfibers = new LinkedList<CartanFiber>();
			
			// Interpolate control fibers
			for (ParameterVector parameter : managerVolume.getNodes().get(i)) {
				FiberNode node = (FiberNode) parameter;
				
				CartanFiber fiber = managerVolume.integrateSurfacic(node.getFrame(), model, i);
				cfibers.add(fiber);
			}
			controlFib.add(cfibers);

			// Interpolate generic fibers
			for (CoordinateFrame root : roots) {
				CartanFiber fiber = managerVolume.integrateSurfacic(root, model, i);
				gfibers.add(fiber);
			}
			fibers.add(gfibers);
		}
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Fiber Manager");
		
		return vfp.getPanel();
	}
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glDisable(GL2.GL_LIGHTING);
		
		int numLevels = fibers.size();
		// TODO: construct a display list for all fibers
		
		for (int i = 0; i < 1; i++) {
			
			if (drawFibers.getValue()) {
				gl.glLineWidth(1f);
				LinkedList<CartanFiber> fibers = this.fibers.get(i);
				for (CartanFiber fiber : fibers) {
					double[] c = fiber.getFiberNode().getColor();
					gl.glColor4d(c[0], c[1], c[2], 1);
					fiber.display(drawable);
				}
			}
			
			if (drawControl.getValue()) {
				gl.glLineWidth(2f);
				LinkedList<CartanFiber> controlFib = this.controlFib.get(i);
				for (CartanFiber fiber : controlFib) {
					double[] c = fiber.getFiberNode().getColor();
					gl.glColor4d(c[0], c[1], c[2], 1);
					fiber.display(drawable);
				}
			}
		}
	}

	/**
	 * Embed all generated nodes within a cartanic cylinder.
	 * TODO: move this method somewhere else.
	 * @param embedding
	 */
	public void embed(CartanVolumeManager managerVolume, CartanFiber embedding, int level) {
		for (CartanFiber fiber : fibers.get(level)) {
			fiber.embed(embedding);
		}
	}
}
