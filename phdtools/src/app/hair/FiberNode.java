package app.hair;

import javax.vecmath.Point3d;

import tools.frame.CoordinateFrame;
import tools.interpolation.ParameterVector;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;

public class FiberNode extends ParameterVector {

	private CartanParameter cartanParameter;
	
	private double[] color;
	
	private CoordinateFrame frame;
	
	/**
	 * Parameters are stored in the following order:
	 * length (1) + color (3) + cartan parameters (n)
	 */
	public FiberNode(CartanModel model, ParameterVector other, CoordinateFrame frame) {
		super(other);
		int j = 0;
		
		this.frame = frame;
		
		// length (no need to set anything)
		j++;
		
		// color (set for faster fetching)
		color = new double[] { data[j++], data[j++], data[j++] };
		
		// cartan parameter
		cartanParameter = new CartanParameter(model);
		for (int i : model.getParameterMask()) {
			cartanParameter.set(i, data[j++]);
		}
	}	
	
	public FiberNode(CoordinateFrame frame, CartanParameter cartan, CartanModel model, double length, double[] color) {
		super();
		
		this.frame = frame;
		super.p = new Point3d(frame.getTranslation());
		this.color = color;
		this.cartanParameter = cartan;
		
		// Fill data
		double[] data = new double[1 + 3 + model.getNumberOfParameters()];
		int j = 0;
		data[j++] = length;
		data[j++] = color[0];
		data[j++] = color[1];
		data[j++] = color[2];
		for (int i : model.getParameterMask()) {
			data[j++] = cartan.get(i);
		}
		
		super.data = data;
	}
	
	public double[] getColor() {
		return color;
	}
	
	public double getLength() {
		return super.data[0];
	}
	
	public Point3d getLocation() {
		return super.p;
	}
	
	public CartanParameter getCartanParameter() {
		return cartanParameter;
	}
	
	public CoordinateFrame getFrame() {
		return frame;
	}
}
