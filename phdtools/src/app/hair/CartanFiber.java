package app.hair;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;

public class CartanFiber {
	
	private CartanFiber parent;
	private List<CartanFiber> children;
	private FiberNode node;
	private ArrayList<CoordinateFrame> geometry;
	
	private double length = 0;

	public CartanFiber(ArrayList<CoordinateFrame> geometry, FiberNode node) {
		this(geometry, node, null);
	}
	
	public CartanFiber(ArrayList<CoordinateFrame> geometry, FiberNode node, CartanFiber parent) {
		this.geometry = geometry;
		this.node = node;
		this.parent = parent;
		children = null;
		updateLength();
	}
	
	public double updateLength() {
		// Compute the length of the strand
		length = 0;

		if (geometry.size() == 0)
			return 0;

		Point3d p1 = geometry.get(0).getTranslation();
		for (CoordinateFrame f : geometry) {
			length += f.getTranslation().distance(p1);

			p1 = f.getTranslation();
		}
		
		return length;
	}

	public CartanFiber getParent() {
		return parent;
	}
	
	public List<CartanFiber> getChildren() {
		return children;
	}
	
	public void addChild(CartanFiber child) {
		if (children == null) {
			children = new ArrayList<CartanFiber>();
		}
		children.add(child);
	}
	
	public FiberNode getFiberNode() {
		return node;
	}
	
	public List<CoordinateFrame> getGeometry() {
		return geometry;
	}

	public void setGeometry(ArrayList<CoordinateFrame> geometry) {
		this.geometry = geometry;
	}

	public void setParent(CartanFiber f) {
		parent = f;
	}

	public double getLength() {
		return length;
	}
	
	public int getCount() {
		return geometry.size();
	}
	
	public void embed(CartanFiber embedding) {
		
		if (getGeometry().size() <= 1) return;
		
		Matrix4d R = new Matrix4d();
		Matrix4d Re = new Matrix4d();
		
		double scale = getGeometry().get(1).getTranslation().distance(getGeometry().get(0).getTranslation());
		
		Point3d p0 = getGeometry().get(0).getTranslation();
		for (int i = 0; i < getGeometry().size(); i++) {
			// Current (untransformed) frame
			CoordinateFrame frame = getGeometry().get(i);
			
			// Embedding
			CoordinateFrame frameE = embedding.getGeometry().get(i);

			// Get rotational components
			frame.getRotation(R);
			frameE.getRotation(Re);
			
			// Apply embedding
			R.mul(Re, R);
			
			// Update frame
			frame.setRotation(R);
			frame.setTranslation(p0);
			
			// Update position
			p0.scaleAdd(scale, frame.getAxis(FrameAxis.X), p0);
		}
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		for (CoordinateFrame f : getGeometry()) {
			f.display(drawable, 1 / length, false);
		}
		
	}
}
