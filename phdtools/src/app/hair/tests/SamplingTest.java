package app.hair.tests;

import gl.geometry.primitive.Plane;
import gl.renderer.JoglScene;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;

public class SamplingTest extends JoglScene {

	public static void main(String[] args) {
		new SamplingTest();
	}
	
	private DoubleParameterPoint3d p3d = new DoubleParameterPoint3d("position");
	private DoubleParameter radius = new DoubleParameter("radius", 0.1, 0.01, 0.3);
	private Point3d[] pts;
	private BooleanParameter threeDimensions = new BooleanParameter("3d", false);
	
	public SamplingTest() {
		super("Sampling Test");
		
		ParameterListener pl = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				generateSampling();
			}
		};
		radius.addParameterListener(pl);
		p3d.addParameterListener(pl);
		threeDimensions.addParameterListener(pl);
		
		super.render();
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		if (pts == null) {
			generateSampling();
		}
		
		GL2 gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor4d(0, 1, 0, 0.8);
		gl.glPointSize(5f);
		gl.glBegin(GL2.GL_POINTS);
		for (Point3d p : pts) {
			gl.glVertex3d(p.x, p.y, p.z);
		}
		gl.glEnd();
	
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(radius.getSliderControls());
		vfp.add(p3d.getControls());
		vfp.add(threeDimensions.getControls());
		return vfp.getPanel();		
	}
	
	private void generateSampling() {
		Point3d origin = p3d.getPoint3d();
		if (threeDimensions.getValue()) {
			pts = MathToolset.poissonSampling3D(radius.getValue(), 0.5);
		}
		else {
			pts = MathToolset.poissonSampling2D(radius.getValue(), new Plane(origin, new Vector3d(0,0,1)));
		}
		
		System.out.println(pts.length);
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G) {
					generateSampling();
				}
			}
		});
	}
}
