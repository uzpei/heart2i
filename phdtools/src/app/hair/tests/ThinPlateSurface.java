package app.hair.tests;

import gl.geometry.primitive.Sphere;
import implicit.implicit3d.functions.ImplicitFunction3D;
import implicit.implicit3d.interpolation.Constraint;
import implicit.implicit3d.interpolation.Constraint.Orientation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;

public class ThinPlateSurface implements ImplicitFunction3D {

    private final Vector3d dFv = new Vector3d();

    private List<Constraint> constraints = new LinkedList<Constraint>();
    
    private DenseVector weights;
    
    private double p0, p1, p2, p3;

    private Vector3d tempV = new Vector3d();
    
    private BooleanParameter drawConstraints = new BooleanParameter("Draw constraints", false);
    
     /**
     * Careful!!! Larger values cause instability!
     */
    private DoubleParameter svdT = new DoubleParameter("SVD threshold", 0, Double.MIN_VALUE, 1e1);

    private boolean hasChanged = false;
    
    @Override
	public double F(double x, double y, double z) {
        if (hasChanged) {
            interpolate();
        }
        
        int k = constraints.size();
        
        if (k == 0) return 0;

        double ans = 0;
        
        for (int j = 0; j < k; j++) {
            ans += weights.get(j) * MathToolset.thinPlateBasisFunction(x, y, z, constraints.get(j).p.x, constraints.get(j).p.y, constraints.get(j).p.z);
        }
        
        ans += linearpoly(x, y, z);
        
        return ans;
    }
    
    @Override
	public double F(Point3d point) {
        return this.F(point.x, point.y, point.z);
    }

    @Override
	public Vector3d dF(Point3d point) {
        if (hasChanged) {
            interpolate();
        }
        
        int k = constraints.size();
        
        if (k == 0) {
        	dFv.set(0, 0, 0);
        	return dFv;
        }

        tempV.set(0, 0, 0);

        // dF is computed by taking the derivative
        // of F (see method F)
        dFv.set(0, 0, 0);
        for (int j = 0; j < k; j++) {
        	// Compute x - c_j
            tempV.sub(point, constraints.get(j).p);
            
            // Compute the 2-norm |x - c_j|
            double l = tempV.length();
            
            // Get the thin plate weight at j
            double w = weights.get(j);
            
            // Compute the derivative of F
            dFv.x += w * l * tempV.x;
            dFv.y += w * l * tempV.y;
            dFv.z += w * l * tempV.z;
        }
        
        // For efficiency, scale outside of for loop
        dFv.scale(3);

        // Add grad P
        dFv.x += p1;
        dFv.y += p2;
        dFv.z += p3;
        
        return dFv;
    }
    
    /**
     * Interpolate the control points that have been set. 
     */
    public void interpolate() {
    	
        System.out.print("Interpolating thin-plate function... ");
        int k = constraints.size();
        int n = 4 + k;
        
        if (k <= 0) {
        	System.err.println("No constraints found!");
        }

        DenseMatrix A = new DenseMatrix(n, n);
        
        // Inter-RBF evaluations (top left)
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < k; j++) {
                A.set(i, j, MathToolset.thinPlateBasisFunction(constraints.get(i).p.x, constraints.get(i).p.y, constraints.get(i).p.z, constraints.get(j).p.x, constraints.get(j).p.y, constraints.get(j).p.z));
            }
        }
        
        // 1-column (middle)
        for (int i = 0; i < k; i++) {
            A.set(i, k, 1);
        }

        // 1-column (bottom)
        for (int j = 0; j < k; j++) {
            A.set(k, j, 1);
        }

        // 4x4 lower right 0-matrix
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                A.set(k+i, k+j, 0);
            }
        }
        
        // RBF constraints
        Point3d p = new Point3d();
        for (int j = 0; j < k; j++) {
        	Constraint c = constraints.get(j);
            p.set(c.p);
            
            A.set(j, k+1, p.x);
            A.set(j, k+2, p.y);
            A.set(j, k+3, p.z);

            A.set(k+1, j, p.x);
            A.set(k+2, j, p.y);
            A.set(k+3, j, p.z);
        }

        DenseVector b = new DenseVector(n);
        
        for (int i = 0; i < k; i++) {
            b.set(i, constraints.get(i).val);
        }
        b.set(k, 0);
        b.set(k+1, 0);
        b.set(k+2, 0);
        b.set(k+3, 0);
        
        computeThinPlateWeights(A, b);
        
        hasChanged = false;
    }
    
    private void computeThinPlateWeights(DenseMatrix A, DenseVector b) {
    	int n = A.numColumns();
    	int k = n - 4;
    	
        weights = new DenseVector(n);
        
        // Solve A * weights = b
        if (svdT.isChecked()) {
        	weights = MathToolset.SolveSVD(A, b, svdT.getValue());
        }
        else {
        	weights = MathToolset.SolveLU(A, b);
        }
        
        // Degree one polynomial accounting for the linear and constant
        // portions of f
        p0 = weights.get(k);
        p1 = weights.get(k+1);
        p2 = weights.get(k+2);
        p3 = weights.get(k+3);
    }

    protected double linearpoly(double x, double y, double z) {
        return p0 + p1 * x + p2 * y + p3 * z;
    }

    public void addNormalConstraint(Point3d p, Vector3d normal, double scale) {
 		// Boundary
 		addConstraint(new Constraint(p, Orientation.BOUNDARY));

 		// Interior
 		p.scaleAdd(-scale, normal, p);
 		addConstraint(new Constraint(p,Orientation.INTERIOR));

 		hasChanged = true;
 	}

    public void addConstraint(Constraint c) {
        constraints.add(c);
    }
    
    public void clear() {
    	constraints.clear();
    	hasChanged = true;
    }
    
    @Override
	public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL.GL_BLEND);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        if (drawConstraints.getValue()) {
            gl.glPointSize(10);
            float alpha = 0.8f;
            gl.glBegin(GL.GL_POINTS);
            for (Constraint c : constraints) {
                if (c.orientation == Orientation.BOUNDARY) {
                    gl.glColor4f(0, 0, 1, alpha);
                }
                else if (c.orientation == Orientation.INTERIOR) {
                    gl.glColor4f(1, 1, 0, alpha);
                }
                else if (c.orientation == Orientation.EXTERIOR) {
                    gl.glColor4f(1, 0, 0, alpha);
                }
                
                gl.glVertex3d(c.p.x, c.p.y, c.p.z);
            }
            gl.glEnd();
        }
        
    }

    @Override
	public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.createBorder("Thin plate surface");

        JPanel cp = new JPanel();
        
        JButton btnClear = new JButton("Clear constraints");
        btnClear.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                clear();
            }
        });

       cp.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
//        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.NORTHWEST;

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        cp.add(btnClear, c);

        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 1;
        cp.add(drawConstraints.getControls(), c);

        vfp.add(cp);
        
        svdT.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				hasChanged = true;
			}
		});
        
        vfp.add(svdT.getSliderControlsExtended("enabled"));
        svdT.setChecked(true);

//        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(), getName() + " Function");
//        return panel;
        return vfp.getPanel();
    }
    
	public Point3d[] sampleFrom(Sphere sphere, int n) {
		clear();
		double r = sphere.getRadius();
		
		Point3d[] samples = sphere.sampleHemisphere(n, 0);
		
		// Add a single interior constraint at the center of the sphere
//		addConstraint(new Constraint(sphere.getCenter(), Orientation.INTERIOR));
		
		constraints.clear();
		double constraintScale = 0.01 * r;
		for (int i = 0; i < n; i++) {
//			addTriConstraint(samples[i], sphere.getNormal(samples[i]), i, constraintScale);
//			Vector3d normal = sphere.getNormal(samples[i]);
//			normal.normalize();
//			normal.negate();
//			addTriConstraint(new Point3d(samples[i]), normal, i, constraintScale);
			addNormalConstraint(samples[i], sphere.getNormal(samples[i]), constraintScale);
		}

		return samples;
	}

	@Override
	public String getName() {
		return "Thin plate surface";
	}

	@Override
	public boolean hasChanged() {
		return hasChanged;
	}
}
