package app.hair.tests;

import gl.geometry.primitive.Sphere;
import gl.renderer.JoglScene;
import gl.renderer.JoglTextRenderer;
import implicit.rendering.marching.ImplicitGrid3D;
import implicit.sampling.particles.ImplicitWitkinSampler;
import implicit.sampling.particles.Particle;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.AdapterState;
import swing.parameters.IntParameter;
import swing.text.GreekToolset;
import tools.geom.MathToolset;

public class ParticleTest extends JoglScene {
	
	public static void main(String[] args) {
		new ParticleTest();
	}
	
	private ImplicitWitkinSampler particleSampler;
	
	private IntParameter numSamples = new IntParameter("num samples", 100, 0, 10000);
	private IntParameter greatCircle = new IntParameter(GreekToolset.GreekLetter.PHI.toString(), 30, -90, 90);
	private Sphere sphere = new Sphere();
	private boolean sampleRequest = true;
	private boolean loadMeshRequest = false;
	private ThinPlateSurface tps;
	private ImplicitGrid3D grid;
//	private PolygonSoup polygon = null;
	
	protected ParticleTest() {
		super("Particle test");
		tps = new ThinPlateSurface();
		grid = new ImplicitGrid3D(new Point3d(), 3, 10);
		
		particleSampler = new ImplicitWitkinSampler(tps);
//		particleSampler = new ImplicitWitkinSampler(sphere);

//		polygon = new PolygonSoup("./data/obj/cl_head_LOW.obj");
//		polygon = new PolygonSoup("./data/obj/cl_head.obj");
//		vertexPicker = new ModelPicker(polygon);
		
		VerticalFlowPanel implicitPanel = new VerticalFlowPanel();
		implicitPanel.add(sphere.getControls());
		implicitPanel.add(tps.getControls());
		implicitPanel.add(grid.getControls());
		super.renderer.getControlFrame().add("Implicit", implicitPanel.getPanel());
//		super.renderer.getControlFrame().add("Polygon", polygon.getControls());

		super.render();
		
		// Zoom out
		super.renderer.getCamera().zoom(-300f);
		
		// Move the light source outside of the implicit sphere
		super.renderer.getRoom().setLightPosition(new Point3d(0, 0, 2 * sphere.getRadius()));
	}
	

	private AdapterState state = new AdapterState();

	@Override
	public void display(GLAutoDrawable drawable) {
		if (sampleRequest) {
			
			particleSampler.getParticleSystem().getParticleEngine().clear();	
			Point3d[] samples = tps.sampleFrom(sphere, 30);
			double radius = sphere.getRadius() / samples.length;
			grid.setUpdateRequest(true);
			sampleRequest = false;

			// Get great hemispherical great circle and fix particles
			double phi = (Math.PI * greatCircle.getValue()) / 180d;
			List<Point3d> hsamples = sphere.sampleGreatCircle(MathToolset.roundInt(0.5 * numSamples.getValue() * sphere.getCircumference()), phi);
			for (Point3d sample : hsamples) {
				Particle particle = particleSampler.getParticleSystem().getParticleEngine().createParticle(sample.x, sample.y, sample.z, 0, 0, 0, 2 * radius);
//				particle.fixed = true;
				particle.contour = true;
			}
			
			// Relax fixed particles
			particleSampler.relaxParticles(1e-3, 500, 1e-6);

			// Get random hemisphere samples
			samples = sphere.sampleHemisphere(numSamples.getValue(), Math.min(90, (10+greatCircle.getValue())*1.1) / 180d * Math.PI);
			Point3d pt = new Point3d();
			for (int i = 0; i < samples.length; i++) {
				Vector3d n = sphere.getNormal(samples[i]);
				double s = sphere.getRadius();
				pt.scaleAdd(s, n, samples[i]);
				Vector3d v = MathToolset.randomDirection();
				particleSampler.getParticleSystem().getParticleEngine().createParticle(pt.x, pt.y, pt.z, v.x, v.y, v.z, radius);
			}

		}
		else if (loadMeshRequest) {
//			particleSampler.getParticleSystem().getParticleEngine().clear();	
//			loadMeshRequest = false;
//			
//			// Sample the polygon
//			List<Point3d> samples = PolygonSoup.sampleThinPlate(tps, grid, polygon, numSamples.getValue());
//			double scale = 0.05;
//			double radius = scale / numSamples.getValue();
//			for (Point3d p : samples) {
//				Vector3d v = MathToolset.randomDirection();
//				particleSampler.getParticleSystem().getParticleEngine().createParticle(p.x, p.y, p.z, v.x, v.y, v.z, radius);
//			}
//			grid.setUpdateRequest(true);
		}
		
		particleSampler.display(drawable);
    	JoglTextRenderer.printTextLines(drawable, particleSampler.getDebugString());

//    	polygon.display(drawable);
    	
		GL2 gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor4d(0, 1, 0, 0.8);
		gl.glPointSize(5f);
		
		tps.display(drawable);

		grid.display(drawable, tps);
		
//		if (state.shiftDown()) {
//			gl.glEnable(GL2.GL_CULL_FACE);
//			gl.glEnable(GL2.GL_CULL_VERTEX_EXT);
//			Vertex picked = vertexPicker.pick(drawable, state.getX(), state.getY());
//			
//			if (picked != null) {
//				gl.glPointSize(10f);
//				gl.glColor4d(1, 0, 0, 0.8);
//				gl.glBegin(GL2.GL_POINTS);
//				gl.glVertex3d(picked.p.x, picked.p.y, picked.p.z);
//				gl.glEnd();
//			}
//		}
	}
	
	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(numSamples.getSliderControls());
		vfp.add(greatCircle.getSliderControls());
		vfp.add(particleSampler.getControls());
		return vfp.getPanel();
	}
	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_G)
					sampleRequest = true;
				else if (e.getKeyCode() == KeyEvent.VK_L)
					loadMeshRequest = true;
				
				state.keyPressed(e);
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				state.keyReleased(e);
			}
		});
		component.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				state.mouseMoved(e);
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
