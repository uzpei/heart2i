package app.hair;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.frame.VolumeSampler;

/**
 * Handles surface modeling, sampling, etc.
 * @author piuze
 */
public class CartanSurfaceManager {

	private VolumeSampler sampler;
	
	private List<CoordinateFrame> roots;
	
	public CartanSurfaceManager() {
		roots = new LinkedList<CoordinateFrame>();
		
		sampler = new VolumeSampler(3, SamplingStyle.NP_Z_Z);
		
		generateSampling();
	}
	
	public void generateSampling() {

		// Generate samples in the XY plane
		List<Point3d> samples = sampler.getSamples(new CoordinateFrame(new Vector3d(0, 0, 1)), true);
		
		roots.clear();
		Point3d surfacePoint = new Point3d();
		for (Point3d p : samples) {
			// TODO: move to implicit surface normals and initial frames
			// to fetch frame at p
			CoordinateFrame frame = getFrame(p);
			roots.add(frame);
		}
	}
	
	public List<CoordinateFrame> getRoots() {
		return roots;
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glPointSize(5f);
		gl.glColor4d(1,0,0,0.8);

		for (CoordinateFrame f : roots) {
			f.display(drawable, 0.1, false);
		}

		gl.glBegin(GL2.GL_POINTS);
		for (CoordinateFrame f : roots) {
			Point3d p = f.getTranslation();
			gl.glVertex3d(p.x, p.y, p.z);
		}
		gl.glEnd();
	}

	public Component getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Surface Manager");
		vfp.add(sampler.getControls());
		return vfp.getPanel();
	}

	public void addParameterListener(ParameterListener pl) {
		sampler.addParameterListener(pl);
	}

	/**
	 * TODO: this will be fetched from the implicit thin-plate spline
	 * @param pt
	 * @return
	 */
	public CoordinateFrame getFrame(Point3d pt) {
		return new CoordinateFrame(new Vector3d(1, 0, 0), pt);
	}

}
