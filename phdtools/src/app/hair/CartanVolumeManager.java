package app.hair;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.frame.VolumeSampler;
import tools.interpolation.GaussianInterpolator;
import tools.interpolation.GaussianInterpolator.METRIC;
import tools.interpolation.ParameterVector;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterControl;
import app.pami2013.cartan.CartanParameter.CartanModel;

/**
 * Handles cartan volume and fiber interpolation
 * @author piuze
 */
public class CartanVolumeManager {

	private IntParameter resolution = new IntParameter("resolution", 100, 0, 1000); 
	
	public final static int InteractiveScaleLevel = 0;

	public final static int DefaultMaxLevels = 2;

	/**
	 * List of parameter nodes at each scale.
	 */
	private ArrayList<LinkedList<ParameterVector>> nodes;
	
	private GaussianInterpolator[] interpolator;
	
	private CartanParameterControl cpc;
	
	private VolumeSampler sampler;
	
	private DoubleParameter length = new DoubleParameter("length", 5, 0, 10);

	private DoubleParameter cmag = new DoubleParameter("cartan mag", 1, 0, 100);

	private CartanSurfaceManager managerSurface;
	
	public CartanVolumeManager(CartanSurfaceManager managerSurface) {
		this.managerSurface = managerSurface;
		nodes = new ArrayList<LinkedList<ParameterVector>>();
		interpolator = new GaussianInterpolator[2];
		
		for (int i = 0; i < interpolator.length; i++)
			interpolator[i] = new GaussianInterpolator(METRIC.SPATIAL);
		
		sampler = new VolumeSampler(1, SamplingStyle.NP_Z_Z);
		cpc = new CartanParameterControl(CartanModel.TANGENTIAL);
	
		ParameterListener randomListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				randomize(CartanModel.TANGENTIAL, DefaultMaxLevels);
			}
		};
		resolution.addParameterListener(randomListener);
		sampler.addParameterListener(randomListener);
		length.addParameterListener(randomListener);
		cmag.addParameterListener(randomListener);
		
		ParameterListener interactiveListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateInteractive();
			}
		};
		
		cpc.addParameterListener(interactiveListener);
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Volume Manager");
		vfp.add(cpc.getControls());
		vfp.add(resolution.getSliderControls());
		vfp.add(sampler.getControls());
		vfp.add(length.getSliderControls());
		vfp.add(cmag.getSliderControls());
		
		for (int i = 0; i < interpolator.length; i++)
			vfp.add(interpolator[i].getControls());
		
		return vfp.getPanel();
	}

//	public void addNode(CartanModel model, Point3d location, CartanParameter parameter, double[] color, double length) {
//		FiberNode node = new FiberNode(location, parameter, model, length, color);
//		nodes.add(node);
//	}
	
	public CartanFiber integrateVolumetric(CoordinateFrame frame0, CartanModel model, int maxLevel) {

		CartanFiber root = null;
		CartanFiber pf = null;
		
		for (int i = 0; i < maxLevel; i++) {

			/*
			 * Interpolate parameters for the current level and position
			 */
			// Get root parameters (length, color) 
			ParameterVector pv0 = interpolator[i].interpolate(frame0.getTranslation(), nodes.get(i));
			
			// Create root node
			FiberNode fn0 = new FiberNode(model, pv0, frame0);
			CartanParameter cartan0 = fn0.getCartanParameter();

			/*
			 * Integrate
			 */
			ArrayList<CoordinateFrame> frames = cartan0.integrate(frame0, fn0.getLength(), resolution.getValue());
			
			CartanFiber f = new CartanFiber(frames, fn0, pf);
			
			if (root == null)
				root = f;
			else {
				// Embed the geometry of the new fiber
				f.embed(pf);
				
				// Add the new fiber as a child of the previous one
				pf.addChild(f);
			}
			
			pf = f;
		}
		
		return null;
	}

	public CartanFiber integrateSurfacic(CoordinateFrame frame0, CartanModel model, int level) {
		// Get root parameters (length, color) 
		ParameterVector pv0 = interpolator[level].interpolate(frame0.getTranslation(), nodes.get(level));
		
		// Create root node
		FiberNode fn0 = new FiberNode(model, pv0, frame0);
		
		// Step size
		double step = fn0.getLength() / resolution.getValue();
		
		CartanParameter cartan0 = fn0.getCartanParameter();
		ArrayList<CoordinateFrame> pts = new ArrayList<CoordinateFrame>();

		// Iterative frame
		CoordinateFrame frame = new CoordinateFrame(frame0);
		for (double i = 0; i < fn0.getLength(); i += step) {
			pts.add(frame);
			
			frame = cartan0.contract(frame, step);
		}
		
		return new CartanFiber(pts, fn0);
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glPointSize(10f);

		Color[] colors = new Color[] { Color.WHITE, Color.RED, Color.BLUE };
		for (int i = 0; i < nodes.size(); i++) {
//			gl.glColor4d(1,1,1,0.8);
			Color c = colors[i];
			gl.glColor4d(c.getRed() / 255d, c.getGreen() / 255d, c.getBlue() / 255d, 1.0f - 0.2f * (i+1));
			
			gl.glBegin(GL2.GL_POINTS);
			for (ParameterVector p : nodes.get(i)) {
				FiberNode node = (FiberNode) p;
				gl.glVertex3d(node.p.x, node.p.y, node.p.z);
			}
			gl.glEnd();
		}
	}

	private boolean interactiveUpdateRequested = false;
	
	public void randomize(CartanModel cartanModel, int numLevels) {
		Vector3d normal = new Vector3d(0, 0, 1);
		CoordinateFrame frame = new CoordinateFrame(normal);
		List<Point3d> samples = sampler.getSamples(frame, false);
		Random rand = new Random();
		
		double lstd = 0.1;
		
		nodes.clear();
		for (int i = 0; i < numLevels; i++) {
			LinkedList<ParameterVector> lnodes = new LinkedList<ParameterVector>();
			
			for (Point3d p : samples) {
				Point3d pt = new Point3d(p.x, p.y, p.z);
				double[] data = new double[4 + cartanModel.getNumberOfParameters()];
				
				int j = 0;
				// Randomize length
//				data[j++] = length.getValue() * (1 + lstd * 2 * (-0.5 + rand.nextDouble()));
				data[j++] = length.getValue();
				
				// Randomize color
				data[j++] = Math.min(1, 0.5 + rand.nextDouble());
				data[j++] = Math.min(1, 0.5 + rand.nextDouble());
				data[j++] = Math.min(1, 0.5 + rand.nextDouble());

				// Randomize cartan parameter
				for (int c : cartanModel.getParameterMask()) {
					data[j++] = cmag.getValue() * 2 * (-0.5 + rand.nextDouble());
				}
				
				ParameterVector parameter = new ParameterVector(pt, data);
				FiberNode node = new FiberNode(cartanModel, parameter, managerSurface.getFrame(p));
				
				lnodes.add(node);
			}			
			nodes.add(lnodes);
		}
		
		interactiveUpdateRequested = true;
		
		updateInteractive();
	}

	private void updateInteractive() {
		// Add manual control to the center node
		CoordinateFrame frame = managerSurface.getFrame(new Point3d());
		FiberNode node = new FiberNode(frame, cpc, CartanModel.TANGENTIAL, getLength(), new double[] { 1, 0, 0 });

		if (interactiveUpdateRequested) {
			getNodes().get(InteractiveScaleLevel).add(node);
			interactiveUpdateRequested = false;
		}
		else {
			getNodes().get(InteractiveScaleLevel).set(getNodes().size() - 1, node);
		}
	}
	
	public void addParameterListener(ParameterListener pl) {
		resolution.addParameterListener(pl);
		sampler.addParameterListener(pl);
		length.addParameterListener(pl);
		cmag.addParameterListener(pl);
		
		for (int i = 0; i < interpolator.length; i++)
			interpolator[i].addListener(pl);
		
		cpc.addParameterListener(pl);
	}

	public ArrayList<LinkedList<ParameterVector>> getNodes() {
		return nodes;
	}

	public double getLength() {
		return length.getValue();
	}

	public double getResolution() {
		return resolution.getValue();
	}
}
