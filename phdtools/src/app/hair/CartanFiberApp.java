package app.hair;

import gl.renderer.JoglScene;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.geom.Polyline;
import app.pami2013.cartan.CartanParameterControl;
import app.pami2013.cartan.CartanParameter.CartanModel;


public class CartanFiberApp extends JoglScene {

	public static void main(String[] args) {
		new CartanFiberApp();
	}

	private CartanFiberManager managerFiber;
	private CartanSurfaceManager managerSurface;
	private CartanVolumeManager managerVolume;
	private EnumComboBox<CartanModel> ecbCartanModel;
		
	private CartanParameterControl embedding;
	
	public CartanFiberApp() {
		super("Cartan Fiber Modeler");
		
		managerFiber = new CartanFiberManager();
		managerSurface = new CartanSurfaceManager();
		managerVolume = new CartanVolumeManager(managerSurface);
		ecbCartanModel = new EnumComboBox<CartanModel>(CartanModel.TANGENTIAL);

		embedding = new CartanParameterControl(CartanModel.TANGENTIAL);
		
		initialize();
		
		render();
	}
	
	private void initialize() {
		// Create volume update listener
		ParameterListener volumeListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateAllGeometry();
			}
		};
		managerVolume.addParameterListener(volumeListener);

		// Create surface update listener
		ParameterListener surfaceListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateAllGeometry();
			}
		};
		managerSurface.addParameterListener(surfaceListener);

		embedding.addParameterListener(surfaceListener);
		
		managerVolume.randomize(ecbCartanModel.getSelected(), CartanVolumeManager.DefaultMaxLevels);
		updateAllGeometry();
	}

	private CartanFiber pembedding = null;
	
	/**
	 * Generate new fibers if control parameters have changed.
	 */
	private void updateAllGeometry() {
		// Generate control fibers
		managerFiber.generate(managerSurface, managerVolume, ecbCartanModel.getSelected(), CartanVolumeManager.DefaultMaxLevels);

		// Generate an embedding
		ArrayList<CoordinateFrame> frames = this.embedding.integrate(managerSurface.getFrame(new Point3d()), managerVolume.getLength(), managerVolume.getResolution());
		pembedding = new CartanFiber(frames, new FiberNode(managerSurface.getFrame(new Point3d()), this.embedding, ecbCartanModel.getSelected(), managerVolume.getLength(), new double[] { 0, 0, 1}));
//		Point3d embeddingOrigin = new Point3d();
		
		// Generate current embedding for display
//		CoordinateFrame frame0 = managerSurface.getFrame(embeddingOrigin);
//		CartanFiber fiber = managerVolume.integrateSurfacic(frame0, ecbCartanModel.getSelected(), 0);
//		fiber.embed(embedding);
		
		// Apply a fixed embedding on all fibers at level 1
		managerFiber.embed(managerVolume, pembedding, 0);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glRotated(90, 0, 1, 0);
		gl.glRotated(90, 0, 0, 1);
		managerSurface.display(drawable);
		managerVolume.display(drawable);
		managerFiber.display(drawable);
		
		if (pembedding != null) {
			gl.glColor3d(0, 0, 1);
			gl.glLineWidth(5f);
//			pembedding.display(drawable);
		}
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Cartan-based Fiber Modeling");
		vfp.add(embedding.getControls());
		vfp.add(managerFiber.getControls());
		vfp.add(managerSurface.getControls());
		vfp.add(managerVolume.getControls());
		return vfp.getPanel();
	}

	@Override
	public void attach(Component component) {
	component.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_G) {
				updateAllGeometry();
			}
			else if (e.getKeyCode() == KeyEvent.VK_R) {
				managerVolume.randomize(ecbCartanModel.getSelected(), CartanVolumeManager.DefaultMaxLevels);
				updateAllGeometry();
			}
			
		}
	});
}

}
