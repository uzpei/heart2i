package app.unclassified;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.loader.TractoDatLoader;
import volume.IntensityVolume;
import volume.PerVoxelMethod;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;

public class EigenLoader implements GLObject, Interactor {

	public static void main(String[] args) {
		new EigenLoader(new GLViewerConfiguration());
	}

	/**
	 * Display the world axis.
	 */
	private BooleanParameter displayWorld = new BooleanParameter(
			"display world", true);

	private JoglRenderer ev;

	private IntensityVolume[] data;

	private VoxelVectorField vfield;

	private Point3d pcenter;
	
	private VoxelVectorField vb, vn;

	private DoubleParameterPoint3d x0, x1;
	private DoubleParameter z0, z1;
	
	private String path = "/Users/epiuze/Documents/workspace/PhD/Tractofan/data/mouse/mouseControl12782-01/";
	
	private void exportData(VoxelVectorField[] vfields) {

		String datafile = path + "proje";

		System.out.println("Extracting to "
				+ datafile + "i.txt...");
		
		final Vector3d v3 = new Vector3d();
		for (int i = 0; i < 3; i++) {
			try {
				final VoxelVectorField v = vfields[i];

				final Point3i o = v.getVoxelBox().getOrigin();

				final PrintWriter outx = new PrintWriter(new FileWriter(
						datafile + i + "x.txt"));
				final PrintWriter outy = new PrintWriter(new FileWriter(
						datafile + i + "y.txt"));
				final PrintWriter outz = new PrintWriter(new FileWriter(
						datafile + i + "z.txt"));
				
//				out.println("------------------------------------");
//				out.println("Framed DTI volume extraction - " + (i == 0 ? " x axis (fiber direction)" : i == 1 ? " y axis" : " z axis"));
//				out.println("Heart ID: " + path);
//				out.println("Created by " + System.getProperty("user.name"));
//				out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
//				
//				out.println("------------------------------------");
//				out.println("Volume Dimension =");
//				out.println(v.getDimension());
//
//				out.println("Slice origin = ");
//				out.println(vfields[0].getVoxelBox().getOrigin());
//				out.println("Slice span = ");
//				out.println(vfields[0].getVoxelBox().getSpan());
//				
//				out.println("Vx, Vy, Vz");

				Point3i dim = v.getDimension();
				for (int z = 0; z < dim.z; z++) {
					for (int y = 0; y < dim.y; y++) {
						for (int x = 0; x < dim.x; x++) {
							v.getVector3d(x, y, z, v3);
							outx.println("" + v3.x);
							outy.println("" + v3.y);
							outz.println("" + v3.z);
						}
					}
				}
				
//				v.getVoxelBox().voxelProcess(new PerVoxelMethod() {
//
//					@Override
//					public void process(int x, int y, int z) {
//						v.getVector3d(x - o.x, y - o.y, z - o.z, v3);
//						String s = (x - o.x) + "," + (y - o.y) + "," + (z - o.z) + "," + v3.x + "," + v3.y + "," + v3.z;
//						out.println(s);
//					}
//
//					@Override
//					public boolean isValid(Point3d origin, Vector3d span) {
//						// TODO Auto-generated method stub
//						return true;
//					}
//				});

				outx.close();
				outy.close();
				outz.close();

				System.out.println("Done.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	public EigenLoader(GLViewerConfiguration config) {

		// String imgbase = "./data/rat/pnas/rat07052008/";
		// image = new FramedOrientationImage(imgbase + "e1", imgbase+
		// "projnorm", imgbase + "mask.mnc", CuttingPlane.TRANSVERSE);

		// String path = "./data/rat/pnas/rat07052008/";
		// String path = "./data/rat/pnas/rat21012008/";
		// String path = "./data/rat/pnas/rat24012008/";
		// String path = "./data/rat/pnas/rat27022008/";
		// String path = "./data/rat/28012008/";
		// String path = "./data/rat/unknown/";

		// String path =
		// "/Users/epiuze/Documents/workspace/PhD/Tractofan/data/mouse/mouseTAC12657-01/";
		// String path =
		// "/Users/epiuze/Documents/workspace/PhD/Tractofan/data/mouse/mouseControl12782-02/";

		String mask;

		mask = "mask.txt";
		IntensityVolume imask = null;

		File maskf = new File(path + mask);
		if (maskf.exists()) {
			System.out.println("Mask found at " + maskf.getAbsolutePath());

			int[] dim = new int[] { 128, 128, 64 };
			imask = TractoDatLoader.loadMask(path + mask, dim);
		} else {
			System.out.println("No mask found at " + maskf.getAbsolutePath());
		}

		// DAT file
		// I know dims are 64 x 64 x 128
		// but this is *not* stored in the .dat files
		// int[] dim = new int[] { 64, 64, 128 };
		int[] dim = new int[] { 128, 128, 64 };
		data = TractoDatLoader.load(path, "e1", null, null, dim);
		vfield = new VoxelVectorField("DAT", data[0], data[1], data[2]);
		if (imask != null) {
			vfield.getVoxelBox().setMask(imask);
//			vfield.crop();
//			vb.crop();
//			vn.crop();

			CuttingPlane cuttingPlane = CuttingPlane.TRANSVERSE;
			vfield.getVoxelBox().cut(cuttingPlane);
		}

		// Construct normal variables
		Point3i pd = vfield.getDimension();
		x0 = new DoubleParameterPoint3d("x0", new Point3d(66, 60, 42), new Point3d(0, 0, 0), new Point3d(pd.x, pd.y, pd.z));
		x1 = new DoubleParameterPoint3d("x1", new Point3d(63, 59, 25), new Point3d(0, 0, 0), new Point3d(pd.x, pd.y, pd.z));
		z0 = new DoubleParameter("z0", 42, 0, pd.z);
		z1 = new DoubleParameter("z1", 20, 0, pd.z);
		
		ParameterListener l = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				computeOrthogonalFrame();
			}
		};
		x0.addParameterListener(l);
		x1.addParameterListener(l);
		z0.addParameterListener(l);
		z1.addParameterListener(l);

		computeOrthogonalFrame();

		Dimension winsize = new Dimension(1000, 1000);
		ev = new JoglRenderer("Eigenloader", this, new Dimension(winsize),
				new Dimension(650, winsize.height + 90), true, config);
		// ev.getCamera().zoom(-500);

		ev.addInteractor(this);
		ev.start();
		ev.getRoom().setVisible(false);
	}

	protected void computeOrthogonalFrame() {
		// Compute orthogonal volumes
		CuttingPlane plane = vfield.getVoxelBox().getCuttingPlane();
		vfield.getVoxelBox().cut(CuttingPlane.FIT);
		pcenter = vfield.getVoxelBox().getCenter();

		VoxelVectorField vvn, vvb;
		vvb = VoxelVectorField.orthogonalizeCylindrical(vfield, x0.getPoint3d(), x1.getPoint3d(), z0.getValue(), z1.getValue());
		vvn = VoxelVectorField.cross(vvb, vfield);
		
//		VoxelFrameField.enforceCylindricalConsistency(vfield, vvn, vvb, vfield.getVoxelBox().getMask());
		
		if (vb == null) {
			vn = vvn;
			vb = vvb;
		}
		else {
			vn.set(vvn);
			vb.set(vvb);
		}
		
		vfield.getVoxelBox().cut(plane);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		if (displayWorld.getValue()) {
			WorldAxis.display(gl);
		}

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glLineWidth(1f);

		gl.glPushMatrix();

		Point3i dim = vfield.getDimension();
		double md = Math.max(Math.max(dim.x, dim.y), dim.z);

		gl.glScaled(2.0 / md, 2.0 / md, 2.0 / md);

		Point3d vc = vfield.getVoxelBox().getCenter();
		vc.negate();
		gl.glTranslated(vc.x, vc.y, vc.z);

		// Show midlines
		Point3d x0p = x0.getPoint3d();
		Point3d x1p = x1.getPoint3d();
		Point3d z0p = new Point3d(50, 50, z0.getValue());
		Point3d z1p = new Point3d(50, 50, z1.getValue());

		gl.glLineWidth(5f);
		gl.glBegin(GL2.GL_LINES);
		gl.glColor4d(1, 1, 0, 0.5);
		gl.glVertex3d(x0p.x, x0p.y, x0p.z);
		gl.glColor4d(1, 1, 0, 1);
		gl.glVertex3d(x1p.x, x1p.y, x1p.z);
		
		gl.glColor4d(1, 0, 1, 0.5);
		gl.glVertex3d(z0p.x, z0p.y, z0p.z);
		gl.glColor4d(1, 0, 1, 1);
		gl.glVertex3d(z1p.x, z1p.y, z1p.z);
		gl.glEnd();
		
		gl.glColor3d(0, 0, 1);

		vfield.setColor(new double[] { 1, 0, 0 });
		vfield.display(drawable);

		Point3d p = vfield.getVoxelBox().getCenter();
//		vb = VoxelVectorField.orthogonalizeCylindrical(vfield, p);
//		vn = VoxelVectorField.cross(vfield, vb);

		vn.setColor(new double[] { 0, 1, 0 });
		vn.display(drawable);

		vb.setColor(new double[] { 0, 0, 1 });
		vb.display(drawable);

		// image.display(drawable);

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glPointSize(15f);
		gl.glColor4d(1, 1, 1, 1);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex3d(pcenter.x, pcenter.y, pcenter.z);
		gl.glEnd();

		gl.glPopMatrix();
	}

	@Override
	public JPanel getControls() {

		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(displayWorld.getControls());

		vfp.add(x0.getControls());
		vfp.add(x1.getControls());
		vfp.add(z0.getSliderControls());
		vfp.add(z1.getSliderControls());
		
		vfp.add(vfield.getControls());
		vfp.add(vn.getControls());
		vfp.add(vb.getControls());

		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_R) {
					computeOrthogonalFrame();
				}
				else if (e.getKeyCode() == KeyEvent.VK_E) {
					exportData(new VoxelVectorField[] { vfield, vn, vb });
				}
			}
		});

		component.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
			}
		});
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		new EigenLoader(config);
	}

}
