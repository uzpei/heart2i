package app.unclassified;

import gl.renderer.TrackBallCamera;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import volume.IntensityVolume;

import com.jogamp.opengl.util.FPSAnimator;

public class Texture2DTest implements GLEventListener {

	public static void main(String[] args) {
		new Texture2DTest();
	}

	private FPSAnimator animator;

	Dimension size = new Dimension(512, 512);

	Dimension controlSize = new Dimension(500, 500);

	public Texture2DTest() {
		GLCanvas glCanvas = new GLCanvas(new GLCapabilities(
				GLProfile.getDefault()));
		glCanvas.setSize(size.width, size.height);
		glCanvas.setIgnoreRepaint(true);
		glCanvas.addGLEventListener(this);
		tbc.attach(glCanvas);

		JFrame frame = new JFrame("Volume Rendering");
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
		frame.setLocation(0, 0);

		animator = new FPSAnimator(glCanvas, 60);

		frame.setUndecorated(false);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setVisible(true);

		glCanvas.requestFocus();

		if (animator != null)
			animator.start();
	}

	private TrackBallCamera tbc = new TrackBallCamera();

	private IntensityVolume volume;

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		// Init
		gl.glEnable(GL2.GL_TEXTURE_2D);
		int width = 32;
		int height = width;
		int depth = width;

		// Load data in
		volume = new IntensityVolume(width, height, depth);
		volume.randomize();

		textureId = new int[1];
		gl.glGenTextures(1, textureId, 0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, textureId[0]);

		int[] dims = volume.getDimension();
		gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL.GL_RGBA, dims[0], dims[1], 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, volume.getBuffer(0).rewind());

		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
		
	}

	private int[] textureId;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		tbc.prepareForDisplay(drawable);

		 gl.glDisable(GL2.GL_LIGHTING);

		// Cube.drawFace(drawable, 1);
		gl.glBegin(GL2.GL_QUADS);
		gl.glTexCoord2f(0f, 0f);
		gl.glVertex3f(0, 0, 0f);
		gl.glTexCoord2f(0f, 1f);
		gl.glVertex3f(0, 5f, 0f);
		gl.glTexCoord2f(1f, 1f);
		gl.glVertex3f(5f, 5f, 0f);
		gl.glTexCoord2f(1f, 0f);
		gl.glVertex3f(5f, 0, 0f);
		gl.glEnd();

		tbc.cleanupAfterDisplay(drawable);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		// do nothing
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// do nothing
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		return vfp.getPanel();
	}
}
