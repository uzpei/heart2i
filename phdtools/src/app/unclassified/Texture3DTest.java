package app.unclassified;

import gl.renderer.JoglTextRenderer;
import gl.renderer.TrackBallCamera;
import gl.texture.VolumeTexture;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.ByteBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import volume.IntensityVolume;

import com.jogamp.opengl.util.FPSAnimator;

public class Texture3DTest extends JFrame implements GLEventListener {

	public static void main(String[] args) {
		new Texture3DTest();
	}

	private FPSAnimator animator;

	Dimension size = new Dimension(512, 512);

	Dimension controlSize = new Dimension(500, 500);
	
	private VolumeTexture vtext;

	public Texture3DTest() {
		GLCanvas glCanvas = new GLCanvas(new GLCapabilities(
				GLProfile.getDefault()));
		glCanvas.setSize(size.width, size.height);
		glCanvas.setIgnoreRepaint(true);
		glCanvas.addGLEventListener(this);
		tbc.attach(glCanvas);

		JFrame frame = new JFrame("Volume Rendering");
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
		frame.setLocation(0, 0);

		animator = new FPSAnimator(glCanvas, 60);

		frame.setUndecorated(false);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setVisible(true);

		glCanvas.requestFocus();

		tbc.attach(glCanvas);

		tbc.zoom(-400);

		if (animator != null)
			animator.start();
	}

	private TrackBallCamera tbc = new TrackBallCamera();

	private IntensityVolume volume;

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		// Load the data in
		gl.glEnable(GL2.GL_TEXTURE_3D);
		gl.glEnable(GL2.GL_BLEND);
		
		int vsize = 16;
		IntensityVolume vol = new IntensityVolume(vsize, vsize, vsize);
		vol.randomize(0,1);
		vtext = new VolumeTexture(vol);
		vtext.init(gl);
		
		printInfo(gl);
	}
	

	private void printInfo(GL2 gl) {
		int errorCode = gl.glGetError();
		
		int[] temp = new int[1];
		gl.glGetIntegerv(GL2.GL_MAX_3D_TEXTURE_SIZE, temp, 0);
	
		String errorStr = glu.gluErrorString( errorCode );

		System.out.println( errorStr );
		System.out.println( errorCode );
		System.out.println(gl.glGetString(GL.GL_VERSION));
		System.out.println("Max texture size = " + temp[0]);

		int[] tmp = new int[1];
		gl.glGetIntegerv(GL2.GL_TEXTURE_BINDING_3D, tmp, 0);
		System.out.println("Texture binding = " + tmp[0]);

	}
	
	private int[] textureId;
	
	private float tick = 0f;
	
	private GLU glu = new GLU();

	private JoglTextRenderer textRenderer;
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

//		init(drawable);
		
		tbc.prepareForDisplay(drawable);
		float tickz = tick;
		 		
		gl.glPushMatrix();
		double scale = 3;
		gl.glScaled(scale, scale, scale);
		gl.glColor4d(1,1,1,1);
		vtext.display(gl, tickz);
		gl.glPopMatrix();
		
		tick += 0.005f;
		
		if (tick > 0.5)
			tick = 0f;

		if (textRenderer == null) {
			Font font = new Font("Consolas", Font.PLAIN, 20);
			textRenderer = new JoglTextRenderer(font, true, false);
			textRenderer.setColor(0.9f, 0.9f, 0f, 1f);
		}

		tbc.cleanupAfterDisplay(drawable);
		
		gl.glDisable(GL2.GL_TEXTURE_3D);
		
		textRenderer.drawTopLeft("t=" + tick, drawable);
//		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		// do nothing
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// do nothing
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		return vfp.getPanel();
	}
}
