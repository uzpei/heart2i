package app.unclassified;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.SceneRotator;
import helicoid.fitting.FittableOrientationImage;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLException;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import volume.PerVoxelMethod;
import voxel.VoxelTracer;

import com.jogamp.opengl.util.awt.Screenshot;

public class TractoSliceApp implements GLObject, PerVoxelMethod, Interactor {

    public static void main(String[] args) {
        new TractoSliceApp();
    }

    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", false);
    
    private BooleanParameter onlyTracer = new BooleanParameter("display tracer only", true);
    
    private IntParameter lineCount = new IntParameter("line count", 7, 0, 10);
    private IntParameter fiberX = new IntParameter("fiber x threshold", 12, 0, 100);
    private IntParameter fiberSegments = new IntParameter("fiber segments", 76, 1, 500);
    private IntParameter fiberCutoff = new IntParameter("fiber color cutoff", 5, 1, fiberSegments.getMaximum());
    private DoubleParameter fiberCutoffAlpha= new DoubleParameter("cutoff alpha", 0.2, 0, 1);
    private DoubleParameter fiberResolution = new DoubleParameter("fiber resolution", 0.5, 0, 10);
    private DoubleParameter fiberWidth = new DoubleParameter("fiber width", 3, 0.5, 10);
    
    private boolean snapshot = false;
    
    /**
     * Voxel tracer
     */
    private VoxelTracer voxelTracer;
    
    private JoglRenderer ev;

    private SceneRotator rotator = new SceneRotator();
    
    private FittableOrientationImage framedImage;

    public TractoSliceApp() {
        
    	framedImage = new FittableOrientationImage("./data/rat/unknown/e1", "./data/rat/unknown/norm", "./data/rat/unknown/mask.mnc", false);
    	
    	// Create the ray tracer
    	framedImage.getVoxelBox().sliceTransverse();
    	Point3i origin  = new Point3i(55, 2, 31);
    	Point3i end  = new Point3i(55, 31, 31);
    	voxelTracer = new VoxelTracer(framedImage.getVoxelBox(), origin, end);
    	
    	framedImage.getVoxelBox().addParameterListener(new ParameterListener() {
    		@Override
    		public void parameterChanged(Parameter parameter) {
    			voxelTracer.updateBox();
    		}
    	});
    	initViewer();
    	
    	// Set visibility
    	framedImage.getFrameField().setVisible(false);
    	
    	framedImage.getVoxelBox().setVisible(false);
    }
    
    private void initViewer() {
        Dimension winsize = new Dimension(1000, 1000);
        GLViewerConfiguration glconfig = new GLViewerConfiguration();
        glconfig.setClearColor(Color.white);
        
        ev = new JoglRenderer(glconfig, "",
                this, new Dimension(winsize), new Dimension(680,
                        winsize.height + 90), true);
        
        ev.getRoom().setVisible(false);
        ev.addInteractor(this);
        ev.start();
        
        ev.getCamera().zoom(800f);
        ev.getCamera().zoom(300f);
    }
        
    @Override
    public void display(GLAutoDrawable drawable) {
        
		GL2 gl = drawable.getGL().getGL2();
        
//        gl.glClearColor(0, 0, 0, 1f);
       
        // Make x point upwards
        gl.glRotated(90, 0, 0, 1);

        // Face y
        gl.glRotated(-75, 1, 0, 0);

        int[] dim = framedImage.getFrameField().getDimension();
        Point3d d = new Point3d(dim[0], dim[1], dim[2]);
    	d.scale(0.5 * framedImage.getVoxelBox().getNormalization());

        gl.glPushMatrix();
        rotator.rotate(drawable);

        gl.glTranslated(-d.x, -d.y, -d.z);
        
        gl.glPushMatrix();

        framedImage.getVoxelBox().applyViewNormalization(gl);

		if (!onlyTracer.getValue()) {
			framedImage.getFrameField().display(drawable);
		}
		else {
			framedImage.getFrameField().display(drawable, getIntersectedVoxels());

			List<Point3i> voxels = getIntersectedVoxels();
			
			if (voxels.size() > 0) {
				Point3i pa = voxels.get(0);
				Point3i pb = voxels.get(voxels.size() - 1);
				
				Point3d p0 = new Point3d(pa.x, pa.y, pa.z);
				Point3d p3 = new Point3d(pb.x, pb.y, pb.z);
				
				Vector3d v = new Vector3d();
				v.sub(p3, p0);
				v.x = v.z;
				v.z = v.y;
				v.y = - v.z;
				v.x = 0;
				v.normalize();
				v.scale(3*getSpacing());
				
				Point3d p1 = new Point3d(p0);
				p1.add(v);
				Point3d p2 = new Point3d(p3);
				p2.add(v);
				
				p0.sub(v);
				p3.sub(v);

			}
		}
		framedImage.getVoxelBox().display(drawable);

//        voxelTracer.display(drawable);
        
        // Display orientations
		if (onlyTracer.getValue())
			sampleOrientations(gl);

        gl.glPopMatrix();
        
        if (displayWorld.getValue()) {
            WorldAxis.display(gl, 1.0f, true);
        }

        gl.glPopMatrix();

        Point3i di = framedImage.getFrameField().getF1().getDimension();
        String text = "";
        text += "Dimensions = [ " + di.x + " " + di.y + " " + di.z + " ]\n";
        
        ev.getTextRenderer().setColor(Color.black);
        ev.getTextRenderer().drawTopLeft(text, drawable);
                
        if (snapshot) {
            try {
            	String filename = "snapshot.png";
            	System.out.println("Saving to " + new File(filename).getAbsolutePath() + "...");
    			Screenshot.writeToFile(new File(filename), drawable.getWidth(), drawable.getHeight(), true);
    		} catch (GLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
            snapshot = false;
        }
    }

   
    private void sampleOrientations(GL2 gl) {
    	// Set some defaults
		float lightDiffuse[] = { 1.0f, 1.0f, 1.0f };
		gl.glColor3d(0, 0, 1);
		double length = fiberResolution.getValue();
		int res = 32;
		double radius = 0.1;
		GLU glu = new GLU();
		
		gl.glLineWidth((float) fiberWidth.getValue());

		gl.glDisable(GL2.GL_LIGHTING);

		// Number of samples along perp (use odd number to include main)
		int numsamples = lineCount.getValue();
		int numt = fiberSegments.getValue();

		// Offset size (both neg and pos regions)
		double osize = 0.25;

		// Origin of ray
		Point3d origin = voxelTracer.getOrigin();
		Point3d originbck = new Point3d(origin);
		Point3d target = voxelTracer.getTarget();
		Point3d targetbck = new Point3d(target);
		
		// Parallel dir
		Vector3d dir = voxelTracer.getDirection();
		
		// Perp dir
		Vector3d perp = new Vector3d(dir.x, -dir.z, dir.y);
		
		
//		Vector3d vtest = new Vector3d();
//		double[] angles;
//		
//		vtest.set(1, 0, 0);
//		angles = getAngles(vtest);
//		System.out.println(angles[0]);
//		
//		vtest.set(1, 0, 0.5);
//		angles = getAngles(vtest);
//		System.out.println(angles[0]);
//
//		vtest.set(1, 0, -0.5);
//		angles = getAngles(vtest);
//		System.out.println(angles[0]);

		Point3d pstart = new Point3d();
		Vector3d vprev = new Vector3d();
		Vector3d v = new Vector3d();
		for (int i = 0; i < numsamples; i++) {
//			double pscale = -Math.round(osize / 2) + ((i-Math.round(numsamples/2)) * osize) / numsamples;
//			double pscale = -osize / 2 + i * 0.025;
			double pscale = (i - numsamples/2) * osize / 8;
			
			origin.scaleAdd(pscale, perp, originbck);
			
			target.scaleAdd(1, dir, origin);
			
			voxelTracer.update(origin, target);
			
//			gl.glBegin(GL2.GL_POINTS);
//			gl.glVertex3d(origin.x, origin.y, origin.z);
//			gl.glVertex3d(target.x, target.y, target.z);
//			gl.glEnd();
			
	    	// Get current sample points current line
	    	List<Point3i> pts = voxelTracer.intersect();

	    	// Extract orientations and display
			Point3d tip = new Point3d();
			Point3d p3d = new Point3d();
			Point3d pa = new Point3d();
			Point3d pb = new Point3d();
			
	    	for (Point3i p : pts) {
	    		p3d.set(p.x, p.y, p.z);
	    		
	    		framedImage.getFrameField().getF1().getVector3d(p3d, v);
	    		if (v.x < 0) v.negate();
	    		
//	    		float lightAmbient[] = { 0.75f, i * 0.2f, 0f };
//	    		float lightAmbient[] = { 0.75f, 0.2f + (float) theta, 0f + (float) phi };
//	    		float lightTract[] = { 1f, 0f, 0f };

//	    		drawCylinder(gl, glu, p3d, v, length, radius, res, lightDiffuse, lightAmbient);
	    		
	    		// Reconstruct partial fiber tract
	    		
	    		gl.glBegin(GL2.GL_LINE_STRIP);
	    		pa.set(p3d);
	    		pb.scaleAdd(length, v, pa);

	    		// First point
	    		applyColorFromLocation(gl, pa, v);
	    		gl.glVertex3d(pa.x, pa.y, pa.z);
//	    		gl.glVertex3d(pb.x, pb.y, pb.z);
	    		
	    		tip.set(pb);
	    		for (int j = 0; j < numt; j++) {
	    			// Stop if too high
	    			if (tip.x > p3d.x + fiberX.getValue()) break;
	    			
	    			vprev.set(v);
		    		framedImage.getFrameField().getF1().getVector3d(tip, v);
		    		if (v.x < 0) v.negate();

	    			// Stop if too noisy
		    		if (v.dot(vprev) < 0) break;
		    		
//		    		if (j < fiberCutoff.getValue()) {
		    		if (tip.x < p3d.x + fiberCutoff.getValue()) {
		    			applyColorFromLocation(gl, tip, v);
		    		}
		    		else {
		    			double r = 1;
		    			gl.glColor4d(r, r, r, fiberCutoffAlpha.getValue());
		    		}
		    		gl.glVertex3d(tip.x, tip.y, tip.z);
		    		pb.scaleAdd(length, v, tip);
//		    		gl.glVertex3d(pb.x, pb.y, pb.z);
		    		
		    		tip.scaleAdd(length, v, tip);
		    		
//		    		drawCylinder(gl, glu, tip, v, length, radius, res, lightDiffuse, lightAmbient);
	    		}

	    		gl.glEnd();
	    	}

		}
		
		// Reset main direction
		voxelTracer.update(originbck, targetbck);
    }
    
    private void applyColorFromLocation(GL2 gl, Point3d p, Vector3d v) {
    	
		double[] angles = getAngles(v);
		double theta = angles[0];
		double phi = angles[1];

		// since v.x >= 0, theta is now mapped from 0 to 0.5
		// = 0.5 when fully vertical -> map to 0
		theta /= Math.PI;
		theta = Math.signum(v.z)* (theta - 0.5);
		
		// now this goes from -0.5 to 0.5
		phi /= Math.PI / 2;
		
		double dist = (p.y - 5) / 10;
		gl.glColor4d(0, 0.8 + theta - 1 * dist, 1 - 0.7 * dist, 0.9);
    }
    
    private double[] getAngles(Vector3d v) {
//		double theta = Math.acos(v.x / v.length());
//		double phi = Math.atan(v.z / v.y);
		double theta = Math.asin(v.x / v.length());
		double phi = Math.atan(v.y / v.z);

		return new double[] { theta, phi };
    }
    private void drawCylinder(GL2 gl, GLU glu, Point3d p, Vector3d v, double length, double radius, int res, float[] lightDiffuse, float[] lightAmbient) {
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightAmbient, 0);
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightDiffuse, 0);

		gl.glPushMatrix();
		
		// Determine vectors
		Point3d pa = new Point3d(p.x, p.y, p.z);
		Point3d pb = new Point3d(p.x, p.y, p.z);
		pb.add(v);

		
		// Center cylinder at CM
		Vector3d n = new Vector3d();
		n.sub(pa, pb);
		n.normalize();
		n.scale(length / 2);
		n.add(pa);
		gl.glTranslated(n.x, n.y, n.z);

		// Orient the cylinder
		double theta = -Math.PI / 2 +  Math.acos(v.z / v.length());
//		System.out.println(theta);
		double phi = Math.atan2(v.y, v.x);
		
		gl.glRotated(180 * phi / Math.PI, 0, 0, 1);
		gl.glRotated(180 * theta / Math.PI, 0, 1, 0);

		gl.glRotated(90, 0, 1, 0);

		// Create the GLU quadric with smooth normals and texture coordinates
		GLUquadric quadratic = glu.gluNewQuadric();
		glu.gluQuadricNormals(quadratic, GLU.GLU_SMOOTH);

		// Draw cylinder
		// base, top, height, slices, stacks
		glu.gluCylinder(quadratic, radius, radius, length, res, res);
		
		// Bottom cap
		quadratic = glu.gluNewQuadric();
		glu.gluQuadricNormals(quadratic, GLU.GLU_SMOOTH);
		glu.gluDisk(quadratic, 0, radius, 32, 32);

		gl.glPopMatrix();
		
		// Top cap
		gl.glPushMatrix();
		gl.glTranslated(n.x, n.y, n.z);
		gl.glTranslated(v.x, v.y, v.z);
		
		gl.glRotated(180 * phi / Math.PI, 0, 0, 1);
		gl.glRotated(180 * theta / Math.PI, 0, 1, 0);

		gl.glRotated(90, 0, 1, 0);

		glu.gluDisk(quadratic, 0, radius, 32, 32);

		gl.glPopMatrix();
		
		// Bottom cap
		gl.glPushMatrix();
		gl.glTranslated(n.x, n.y, n.z);
//		v.scale(0);
		gl.glTranslated(v.x, v.y, v.z);
		
		gl.glRotated(180 * phi / Math.PI, 0, 0, 1);
		gl.glRotated(180 * theta / Math.PI, 0, 1, 0);

		gl.glRotated(-90, 0, 1, 0);

		glu.gluDisk(quadratic, 0, radius, 32, 32);

		gl.glPopMatrix();

    }
    
    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        
//        JButton btnSample = new JButton("Sample orientations");
//        btnSample.addActionListener(new ActionListener() {
//        	@Override
//        	public void actionPerformed(ActionEvent arg0) {
//        		sampleOrientations();
//        	}
//        });
//        vfp.add(btnSample);
        
        JButton btnSnapshot = new JButton("snapshot");
        vfp.add(btnSnapshot);
        btnSnapshot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				snapshot = true;
			}
		});
        
        vfp.add(lineCount.getSliderControls());
        vfp.add(fiberSegments.getSliderControls());
        vfp.add(fiberX.getSliderControls());
        vfp.add(fiberCutoff.getSliderControls());
        vfp.add(fiberCutoffAlpha.getSliderControls());
        vfp.add(fiberResolution.getSliderControls());
        vfp.add(fiberWidth.getSliderControls());
        
        vfp.add(displayWorld.getControls());
        vfp.add(onlyTracer.getControls());
        vfp.add(framedImage.getVoxelBox().getControls());
        vfp.add(framedImage.getFrameField().getControls());
        vfp.add(voxelTracer.getControls());
        vfp.add(rotator.getControls());
        
        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }

	@Override
	public void process(int x, int y, int z) {
	}

	public List<Point3i> getIntersectedVoxels() {
		List<Point3i> l = voxelTracer.intersect();
		List<Point3i> valid = new LinkedList<Point3i>();
		
		for (Point3i pt : l) {
			if (!framedImage.getVoxelBox().isOutside(pt.x, pt.y, pt.z)) {
				// Make sure there is a fiber located at this voxel
				valid.add(new Point3i(pt.x, pt.y, pt.z));
			}
		}
		return valid;
	}
	
	public List<Vector3d> getIntersectionTangents() {
		List<Point3i> l = voxelTracer.intersect();
		List<Vector3d> fibers = new LinkedList<Vector3d>();
		for (Point3i pt : l) {
			if (!framedImage.getVoxelBox().isOutside(pt.x, pt.y, pt.z)) {
				fibers.add(framedImage.getFrameField().getF1().getVector3d(pt.x, pt.y, pt.z, new Vector3d()));
			}
		}
		
		return fibers;
	}
	
	public Matrix4d getFrame(int x, int y, int z) {
		return framedImage.getFrameField().get(x, y, z).getFrame().getFrame();
	}

	public double getSpacing() {
		return framedImage.getSpacing();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean isValid(Point3d origin, Vector3d span) {
		return true;
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_S) {
					snapshot = true;
				}
			}
		});		
	}
}
