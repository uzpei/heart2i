package app.unclassified;

import gl.geometry.primitive.Cube;
import gl.renderer.TrackBallCamera;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import volume.IntensityVolume;

import com.jogamp.opengl.util.FPSAnimator;

public class ShaderTest implements GLEventListener {

	public static void main(String[] args) {
		new ShaderTest();
	}

	private FPSAnimator animator;

	Dimension size = new Dimension(512, 512);

	Dimension controlSize = new Dimension(500, 500);

	public ShaderTest() {
		GLCanvas glCanvas = new GLCanvas(new GLCapabilities(GLProfile.getDefault()));
		glCanvas.setSize(size.width, size.height);
		glCanvas.setIgnoreRepaint(true);
		glCanvas.addGLEventListener(this);
		tbc.attach(glCanvas);

		JFrame frame = new JFrame("Volume Rendering");
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(glCanvas, BorderLayout.CENTER); 
		frame.setLocation(0, 0);

		animator = new FPSAnimator(glCanvas, 60);

		frame.setUndecorated(false);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setVisible(true);

		glCanvas.requestFocus();

		if (animator != null)
			animator.start();
	}

	private TrackBallCamera tbc = new TrackBallCamera();

	private IntensityVolume volume;

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Black Background
		gl.glClearDepth(1.0f); // Depth Buffer Setup
		gl.glShadeModel(GL2.GL_FLAT);
		gl.glEnable(GL2.GL_DEPTH_TEST);

		// Init
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glEnable(GL2.GL_TEXTURE_3D);
		
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER,
				GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER,
				GL2.GL_LINEAR);

		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_S,
				GL2.GL_REPEAT);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_T,
				GL2.GL_REPEAT);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_R,
				GL2.GL_REPEAT);

		int width = 32;
		int height = 32;
		int depth = 32;

		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 1);
		
		// Load data in
		volume = new IntensityVolume(width, height, depth);
		volume.randomize();
		volume.getBuffer();

		textureId = new int[1];
		gl.glGenTextures(1, textureId, 0);
		gl.glBindTexture(GL2.GL_TEXTURE_3D, textureId[0]);

		int[] dims = volume.getDimension();
		 gl.glTexImage3D(GL2.GL_TEXTURE_3D, 0, // Mipmap level.
		 GL.GL_RGBA,// GL.GL_RGBA, // Internal Texel Format,
		 dims[0], dims[1], dims[2], 0, // Border
		 GL.GL_RGBA, // External format from image,
		 GL.GL_UNSIGNED_BYTE, volume.getBuffer().rewind());
	}

	private int[] textureId;

	private float tick = 0;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		tbc.prepareForDisplay(drawable);

		gl.glDisable(GL2.GL_LIGHTING);

		// Display texture
		tick += 0.05;

		if (tick > 1f) {
			tick = 0f;
		}

		// Create and bind texture
		gl.glEnable(GL2.GL_TEXTURE_3D);
		gl.glBindTexture(GL2.GL_TEXTURE_3D, textureId[0]);

		Cube.drawFace(drawable, 1);
//		gl.glBegin(GL2.GL_QUADS);
//		gl.glTexCoord3f(0f, 0f, tick);
//		gl.glVertex3f(0, 0, 0f);
//		gl.glTexCoord3f(0f, 1f, tick);
//		gl.glVertex3f(0, 1f, 0f);
//		gl.glTexCoord3f(1f, 1f, tick);
//		gl.glVertex3f(1f, 1f, 0f);
//		gl.glTexCoord3f(1f, 0f, tick);
//		gl.glVertex3f(1f, 0, 0f);
//		gl.glEnd();

		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		// do nothing
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// do nothing
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		return vfp.getPanel();
	}
}
