package app.unclassified;

import extension.minc.MINCImage4d;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.math.FlatMatrix4d;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import app.test.tps.SurfaceVisualizer;
import app.test.tps.fitting.TPSFitter;
import app.test.tps.io.MincToVoxelVolumeExporter;
import app.test.tps.io.VolumeExporter;
import app.test.tps.volume.TPSVoxel;
import app.test.tps.volume.Voxel;
import app.test.tps.volume.VoxelVolume;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import tools.SimpleTimer;
import tools.frame.CoordinateFrame;

public class TPSApp implements GLObject, Interactor {

	public static void main(String[] args) {
		new TPSApp(new GLViewerConfiguration());
	}

    private JoglRenderer ev;	
    
    private VoxelVolume volume;
    
    public TPSApp(GLViewerConfiguration config) {
    	
    	boolean forceLoad = false;
    	
    	String mincFile = "./data/brain/out_full_deconv_Maxima_Cropped";
    	String voxelFile = mincFile + ".txt";
    	
    	SimpleTimer timer = new SimpleTimer();
    	timer.tick();
       	VoxelVolume volume = VolumeExporter.importVolume(voxelFile, false);
       	System.out.println("Import time = " + timer.tick_ms());
       	
    	if (volume != null && !forceLoad) {
    		this.volume = volume;
    	}
    	else {
    		if (forceLoad && volume != null) System.out.println("Manually forcing the MINC file to be read.");
    		if (volume == null) System.out.println("Volume not found... exporting from MINC");
    		
    		// Create the voxel volume
    		timer.tick();
    		this.volume = MincToVoxelVolumeExporter.export(new MINCImage4d(mincFile), voxelFile);
    		System.out.println("Minc load time = " + timer.tick_ms());
    	}
    	
    	// Create the gl viewer
        Dimension winsize = new Dimension(1000, 1000);
        ev = new JoglRenderer("",
                this, new Dimension(winsize), new Dimension(650,
                        500), true, config);
        
        
        ev.addInteractor(this);
        ev.start();
        ev.getRoom().setVisible(false);

    }

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		if (volume != null) {
//			volume.display(drawable, Color.blue);
			
			Voxel centerVoxel = volume.getCenterVoxel();
			if (centerVoxel != null)
				centerVoxel.display(drawable, Color.GREEN);
		}
		
//		System.out.println(Arrays.toString(volume.getDimension()));
		
		WorldAxis.display(gl);
		
		// Display fit
		if (fittedVolume != null) {
			fittedVolume.display(drawable, new Color(100, 0, 0, 255));
			
			List<Voxel> voxels = fittedVolume.getVoxelList();

			Matrix4d rotframe = new Matrix4d();
			rotframe.setIdentity();
			rotframe.setRotation(new AxisAngle4d(0, 1, 0, Math.PI / 2));
			Vector3d voxelV = new Vector3d();
			Point3d voxelP = new Point3d();
			for (Voxel voxel : voxels) {
				TPSVoxel tps = (TPSVoxel) voxel;

				// Apply thresholding on parameter
				double pm = tps.parameter.b * tps.parameter.b;
				
				if (pm < 0.1) continue;
				
				voxelV.set(voxel.v);
				voxelP.set(voxel.p[0], voxel.p[1], voxel.p[2]);

				CoordinateFrame localFrame = new CoordinateFrame(voxelV, voxelP);
				localFrame.postmultiply(rotframe);
				FlatMatrix4d flatm = new FlatMatrix4d(localFrame.getFrame());

//				System.out.println(tps.parameter);
				
				gl.glPushMatrix();
				gl.glMultMatrixd(flatm.asArray(), 0);
				SurfaceVisualizer.drawWithSampling(drawable, tps.parameter.rho, tps.parameter.b, tps.parameter.shape, 1, 3, false);
				gl.glPopMatrix();
			}
		}
		
		// Draw text info
		JoglTextRenderer textRenderer = ev.getTextRenderer();
		textRenderer.setColor(0.3f, 0.9f, 0f, 1f);
		textRenderer.addLine("Fit count = " + (fittedVolume != null ? fittedVolume.getVoxelCount() : 0));
		textRenderer.addLine("Voxel count = " + volume.getVoxelCount());
		textRenderer.drawBuffer(drawable, 10, 10);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private TPSFitter fitter = new TPSFitter();
	private VoxelVolume fittedVolume;
	
	private void fit(boolean whole) {
		if (whole) fittedVolume = fitter.fit(volume);
		else fittedVolume = fitter.fitVoxel(volume, volume.getCenterVoxel());
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_F) {
					fit(false);
				}
				else if (e.getKeyCode() == KeyEvent.VK_V) {
					fit(true);
				}
			}
		});
		
		component.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
//				hbox.doPick(e.getPoint());
			}
		});
		
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		vfp.add(fitter.getControls());
		
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
}
