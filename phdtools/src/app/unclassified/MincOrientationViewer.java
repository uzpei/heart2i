package app.unclassified;

import extension.minc.MINCImage;
import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import helicoid.fitting.FittableOrientationImage;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import volume.IntensityVolume;
import volume.PerVoxelMethod;
import voxel.VoxelVectorField;

public class MincOrientationViewer implements GLObject, PerVoxelMethod, Interactor {

    public static void main(String[] args) {
        new MincOrientationViewer("./data/rat/unknown/e1", "./data/rat/unknown/norm", "./data/rat/unknown/mask.mnc");
//        new MincOrientationViewer("./data/rat/28012008/e1", "./data/rat/28012008/e1", "./data/rat/28012008/mask.mnc");
//        new MincOrientationViewer("./data/human/e1", "./data/human/norm", "./data/human/mask.mnc");
    	
//    	TractoDatLoader.load("./data/rat/28012008/e1");
    }
    
    private JoglRenderer ev;

    private FittableOrientationImage framedImage;
    
    private VoxelVectorField vfield;

    public MincOrientationViewer(String tangentPath, String normPath, String maskPath) {
        
    	framedImage = new FittableOrientationImage(tangentPath, normPath, maskPath, false);
    	framedImage.getVoxelBox().sliceTransverse();
    	
    	// Tangent orientations only
    	String path = "./data/rat/unknown/";
		IntensityVolume ex = new IntensityVolume( new MINCImage(path + "e1x.mnc").getVolume());
		IntensityVolume ey = new IntensityVolume( new MINCImage(path + "e1y.mnc").getVolume());
		IntensityVolume ez = new IntensityVolume( new MINCImage(path + "e1z.mnc").getVolume());
		ex.swapXZ();
		ey.swapXZ();
		ez.swapXZ();
		
		vfield = new VoxelVectorField("MINC", ex, ey, ez);
//		vfieldMINC.getVoxelBox().applyMask(new IntensityVolume(new MINCImage(path + "lvmask.mnc").getVolume()));
//		imask.swapXZ();
    	IntensityVolume imask = new IntensityVolume(new MINCImage(path + "mask.mnc").getVolume());
    	imask.swapXZ();
		vfield.getVoxelBox().setMask(imask);
//		vfield.crop();
    	vfield.getVoxelBox().sliceTransverse();

    	initViewer();
    	
    	// Set visibility
    	framedImage.getFrameField().setVisible(false);
    	framedImage.getVoxelBox().setVisible(true);
    	framedImage.getVoxelBox().setSpacing(3);
    	
    }
    
    private void initViewer() {
        Dimension winsize = new Dimension(1000, 1000);
        GLViewerConfiguration glconfig = new GLViewerConfiguration();
        glconfig.setClearColor(Color.white);
        
        ev = new JoglRenderer(glconfig, "",
                this, new Dimension(winsize), new Dimension(680,
                        winsize.height + 90), true);
        
        ev.getRoom().setVisible(false);
        ev.addInteractor(this);
        ev.start();
        
//        ev.getCamera().zoom(900f);
    }
        
    @Override
    public void display(GLAutoDrawable drawable) {
        
		GL2 gl = drawable.getGL().getGL2();
        
//        // Make x point upwards
//        gl.glRotated(90, 0, 0, 1);
//
//        // Face y
//        gl.glRotated(-70, 1, 0, 0);
//
//        // Angled view
//        gl.glRotated(-10, 0, 0, 1);

//		WorldAxis.display(gl);

		framedImage.getVoxelBox().applyViewNormalizedTranslation(gl);

		framedImage.getVoxelBox().display(drawable);
		framedImage.getFrameField().display(drawable);
		
		gl.glColor3d(1, 0, 1);
		vfield.display(drawable);
		
        Point3i di = framedImage.getFrameField().getF1().getDimension();
        String text = "";
        text += "Volume: [ " + di.x + " x " + di.y + " x " + di.z + " ]\n";
        
        ev.getTextRenderer().setColor(Color.black);
        ev.getTextRenderer().drawTopLeft(text, drawable);
    }

   
 
    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add(framedImage.getVoxelBox().getControls());
        vfp.add(framedImage.getFrameField().getControls());
        vfp.add(vfield.getControls());
        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }

	@Override
	public void process(int x, int y, int z) {
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean isValid(Point3d origin, Vector3d span) {
		return true;
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_S) {
				}
			}
		});		
	}
}
