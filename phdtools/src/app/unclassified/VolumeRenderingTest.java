package app.unclassified;

import gl.renderer.JoglRenderer;
import gl.renderer.SceneRotator;
import gl.renderer.TrackBallCamera;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.ByteBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.ControlFrame;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import volume.IntensityVolume;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;
import com.jogamp.opengl.util.glsl.ShaderState;

public class VolumeRenderingTest extends JFrame implements GLEventListener {

	public static void main(String[] args) {
		new VolumeRenderingTest();
	}

	private FPSAnimator animator;

	private Dimension size = new Dimension(800, 800);

	private Dimension controlSize = new Dimension(500, 500);
	
	private SceneRotator rotator = new SceneRotator(true, true, true);
	
	private TrackBallCamera tbc = new TrackBallCamera();
	
	private IntensityVolume volume;
	
	private ShaderState rayCasterShader;

	private GLU glu = new GLU();
	
	public VolumeRenderingTest() {
		GLCanvas glCanvas = new GLCanvas(new GLCapabilities(
				GLProfile.getDefault()));
		glCanvas.setSize(size.width, size.height);
		glCanvas.setIgnoreRepaint(true);
		glCanvas.addGLEventListener(this);

		JFrame frame = new JFrame("Volume Rendering");
		tbc.attach(glCanvas);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
		frame.setLocation(0, 0);

		animator = new FPSAnimator(glCanvas, 60);

		frame.setUndecorated(false);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setVisible(true);

		glCanvas.requestFocus();

		if (animator != null)
			animator.start();
		
		ControlFrame cframe = new ControlFrame("Controls");
		cframe.add("Volume Rendering", getControls());
		cframe.setVisible(true);
		cframe.add("Camera", tbc.getControls());
		
		tbc.zoom(500f);
	}

	private int frameBuffer;
	private int renderBuffer;
	private int volume_texture;
	private int backfaceTexture, frontfaceTexture;
	private int imageTexture; 
	
	private int[] tmp1 = new int[1];

	private int genbindFrameBuffer(GLAutoDrawable drawable) {
		drawable.getGL().glGenFramebuffers(1, tmp1, 0);
		drawable.getGL().glBindFramebuffer(GL2.GL_FRAMEBUFFER, tmp1[0]);
		return tmp1[0];
	}

	private int genbindRenderBuffer(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		drawable.getGL().glGenRenderbuffers(1, tmp1, 0);

		drawable.getGL().glBindRenderbuffer(GL2.GL_RENDERBUFFER, tmp1[0]);
		gl.glRenderbufferStorage(GL2.GL_RENDERBUFFER, GL2.GL_DEPTH_COMPONENT, size.width, size.height);
		
		gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER, GL2.GL_DEPTH_ATTACHMENT, GL2.GL_RENDERBUFFER, tmp1[0]);
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
		
		return tmp1[0];
	}

	private int genbindTexture2D(GLAutoDrawable drawable) {
		drawable.getGL().glGenTextures(1, tmp1, 0);
		drawable.getGL().glBindTexture(GL2.GL_TEXTURE_2D, tmp1[0]);
		
		GL2 gl = drawable.getGL().getGL2();
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_BORDER);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_BORDER);
		gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0,GL2.GL_RGBA16F, size.width, size.height, 0, GL2.GL_RGBA, GL2.GL_FLOAT, null);

		return tmp1[0];
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glEnable(GL2.GL_CULL_FACE);

		// Create the shader programs
		rayCasterShader = createProgram(gl, "raycast");
//        ShaderCode vsCode = ShaderCode.create( gl, GL2ES2.GL_VERTEX_SHADER, this.getClass(), "shader", "shader/bin", "raycast", false );
//        ShaderCode fsCode = ShaderCode.create( gl, GL2ES2.GL_FRAGMENT_SHADER, this.getClass(), "shader", "shader/bin", "raycast", false );	        
//        ShaderProgram shaderProgram = new ShaderProgram();
//        shaderProgram.add( vsCode );
//        shaderProgram.add( fsCode );
//		if (!shaderProgram.link(gl, System.err)) {
//			throw new GLException("Couldn't link program: " + shaderProgram);
//		}	
//		rayCasterShader = new ShaderState();
//		rayCasterShader.attachShaderProgram( gl, shaderProgram, false );		
//		rayCasterShader.setVerbose(true);

		frameBuffer = genbindFrameBuffer(drawable);

		/*
		 *  Backface texture
		 */
		// Create and init the backface texture
		backfaceTexture = genbindTexture2D(drawable);

		// Attach the texture image to a framebuffer object... in this case to GL_COLOR_ATTACHMENT0
		// 0 is the mipmap level (always)
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, backfaceTexture, 0);

		/*
		 * Front face texture
		 */
		// Create and init the backface texture
		frontfaceTexture = genbindTexture2D(drawable);

		// Attach the texture image to a framebuffer object... in this case to GL_COLOR_ATTACHMENT0
		// 0 is the mipmap level (always)
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, frontfaceTexture, 0);
		
		/*
		 *  Final image buffer
		 */
		imageTexture = genbindTexture2D(drawable);
		renderBuffer = genbindRenderBuffer(drawable);

		/*
		 *  Volumetric data
		 */
//		create_volumetexture(drawable);
		int dim = 128;
		volume = new IntensityVolume(dim, dim, dim);
//		volume.colorcube();
		volume.randomize();
		
		bindBufferData(drawable, volume.getBuffer(), dim);
		
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
//		resize(gl, size.width, size.height);
		tbc.prepareForDisplay(drawable);

		rotator.rotate(drawable);

		gl.glTranslated(-0.5, -0.5, -0.5);

		// Enable the render buffers
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, frameBuffer);
		gl.glBindRenderbuffer(GL2.GL_RENDERBUFFER, renderBuffer);

		renderBackFace(drawable);
//		renderFrontFace(drawable);
		renderRayCast(drawable);
		
		// Disable the render buffer
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
		
		// Render buffer to screen
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glEnable(GL.GL_TEXTURE_2D);
//		gl.glDisable(GL2.GL_TEXTURE_3D);
		gl.glLoadIdentity();

		if (toggle.getValue()) 
			gl.glBindTexture(GL2.GL_TEXTURE_2D, imageTexture);
		else
			gl.glBindTexture(GL2.GL_TEXTURE_2D, backfaceTexture);

		reshape_ortho(gl, size.width, size.height);

		drawFullscreenQuad(gl);

		gl.glDisable(GL2.GL_TEXTURE_2D);
		
//		gl.glFlush();
		tbc.cleanupAfterDisplay(drawable);
	}

	private void renderBackFace(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, backfaceTexture, 0);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );
		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_FRONT);

		drawQuads(gl, 1);

		gl.glDisable(GL.GL_CULL_FACE);
	}
	
	private void renderFrontFace(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, frontfaceTexture, 0);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );
		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_BACK);

		drawQuads(gl, 1);

		gl.glDisable(GL.GL_CULL_FACE);
	}
	
	private void renderRayCast(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, imageTexture, 0);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );

		// Enable shader
		rayCasterShader.useProgram(gl, true);

		gl.glActiveTexture(GL2.GL_TEXTURE0 + 2);
		gl.glBindTexture(GL2.GL_TEXTURE_3D, volume_texture);

		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, backfaceTexture);

		setUniform1f(gl, rayCasterShader, "stepsize", stepSize.getFloatValue());
		setUniform1i(gl, rayCasterShader, "stepcount", stepCount.getValue());
		setUniform1i(gl, rayCasterShader, "tex", 0);
		setUniform1i(gl, rayCasterShader, "volume_tex", 2);

		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_BACK);

		drawQuads(gl, 1f);

		gl.glDisable(GL.GL_CULL_FACE);

		// Disable shader
		rayCasterShader.useProgram(gl, false);
	}
	
	private void setUniform1i(GL2 gl, ShaderState state, String uniform, int value) {
		int id = state.getUniformLocation( gl, uniform);
		gl.glUniform1i(id, value);
//		System.out.println(id);
	}

	private void setUniform1f(GL2 gl, ShaderState state, String uniform, float value) {
		int id = state.getUniformLocation( gl, uniform);
		gl.glUniform1f(id, value);
//		System.out.println(id);
	}

	private ShaderState createProgram(GL2 gl, String shader) {
        ShaderCode vsCode = ShaderCode.create( gl, GL2ES2.GL_VERTEX_SHADER, this.getClass(), "shader", "shader/bin", shader, false );
        ShaderCode fsCode = ShaderCode.create( gl, GL2ES2.GL_FRAGMENT_SHADER, this.getClass(), "shader", "shader/bin", shader, false );
        
        ShaderProgram program = new ShaderProgram();
        program.add( vsCode );
        program.add( fsCode );
		if (!program.link(gl, System.err)) {
			throw new GLException("Couldn't link program: " + program);
		}	
		ShaderState shaderState = new ShaderState();
		shaderState.attachShaderProgram( gl, program, false);		
		
//		shaderState.setVerbose(true);
		
		return shaderState;
	}

	private void drawFullscreenQuad(GL2 gl) {
		gl.glDisable(GL2.GL_DEPTH_TEST);

		gl.glBegin(GL2.GL_QUADS);

		gl.glTexCoord2f(0, 0);
		gl.glVertex2f(0, 0);

		gl.glTexCoord2f(1, 0);
		gl.glVertex2f(1, 0);

		gl.glTexCoord2f(1, 1);

		gl.glVertex2f(1, 1);
		gl.glTexCoord2f(0, 1);
		gl.glVertex2f(0, 1);

		gl.glEnd();
		gl.glEnable(GL2.GL_DEPTH_TEST);

	}

	private void resize(GL2 gl, int w, int h)
	{
		gl.glViewport(0, 0, w, h);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(60f, ((float)w)/h, 0.01, 400.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

	void reshape_ortho(GL2 gl, int w, int h)
	{
		gl.glViewport(0, 0,w,h);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(0, 1, 0, 1);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		// do nothing
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// do nothing
	}

	private BooleanParameter toggle = new BooleanParameter("Toggle", true);
	private DoubleParameter stepSize = new DoubleParameter("step size", 1d/50d, 0, 1);
	private IntParameter stepCount = new IntParameter("step count", 500, 0, 10000);
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		vfp.add(stepSize.getSliderControls());
		vfp.add(stepCount.getSliderControls());
		vfp.add(toggle.getControls());
		vfp.add(rotator.getControls());
		
		return vfp.getPanel();
	}
	
	private void bindBufferData(GLAutoDrawable drawable, ByteBuffer buffer, int dim) {
		GL2 gl = drawable.getGL().getGL2();
		
		drawable.getGL().glPixelStorei(GL2.GL_UNPACK_ALIGNMENT,1);
		drawable.getGL().glGenTextures(1, tmp1, 0);
		volume_texture = tmp1[0];
		
		System.out.println("Volume texture generated with id " + volume_texture);
		
		gl.glEnable(GL2.GL_TEXTURE_3D);

		drawable.getGL().glBindTexture(GL2.GL_TEXTURE_3D, volume_texture);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_R, GL2.GL_REPEAT);
		gl.glTexImage3D(GL2.GL_TEXTURE_3D, 0,GL2.GL_RGBA, dim, dim, dim, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, buffer.rewind());

		gl.glDisable(GL2.GL_TEXTURE_3D);

	}

	private void vertex(GL2 gl, float x, float y, float z) {
		gl.glColor3f(x,y,z);
		gl.glMultiTexCoord3f(GL2.GL_TEXTURE1, x, y, z);
		gl.glVertex3f(x,y,z);

	}
	
	void drawQuads(GL2 gl, float p)
	{
		float x = p;
		float y = p;
		float z = p;
		
		gl.glBegin(GL2.GL_QUADS);
		/* Back side */
		gl.glNormal3f(0.0f, 0.0f, -1.0f);
		vertex(gl,0.0f, 0.0f, 0.0f);
		vertex(gl,0.0f, y, 0.0f);
		vertex(gl,x, y, 0.0f);
		vertex(gl,x, 0.0f, 0.0f);

		/* Front side */
		gl.glNormal3f(0.0f, 0.0f, 1.0f);
		vertex(gl,0.0f, 0.0f, z);
		vertex(gl,x, 0.0f, z);
		vertex(gl,x, y, z);
		vertex(gl,0.0f, y, z);

		/* Top side */
		gl.glNormal3f(0.0f, 1.0f, 0.0f);
		vertex(gl,0.0f, y, 0.0f);
		vertex(gl,0.0f, y, z);
	    vertex(gl,x, y, z);
		vertex(gl,x, y, 0.0f);

		/* Bottom side */
		gl.glNormal3f(0.0f, -1.0f, 0.0f);
		vertex(gl,0.0f, 0.0f, 0.0f);
		vertex(gl,x, 0.0f, 0.0f);
		vertex(gl,x, 0.0f, z);
		vertex(gl,0.0f, 0.0f, z);

		/* Left side */
		gl.glNormal3f(-1.0f, 0.0f, 0.0f);
		vertex(gl,0.0f, 0.0f, 0.0f);
		vertex(gl,0.0f, 0.0f, z);
		vertex(gl,0.0f, y, z);
		vertex(gl,0.0f, y, 0.0f);

		/* Right side */
		gl.glNormal3f(1.0f, 0.0f, 0.0f);
		vertex(gl,x, 0.0f, 0.0f);
		vertex(gl,x, y, 0.0f);
		vertex(gl,x, y, z);
		vertex(gl,x, 0.0f, z);
		gl.glEnd();
		
	}
}
