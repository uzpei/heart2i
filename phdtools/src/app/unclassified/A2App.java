package app.unclassified;

import gl.geometry.FancyAxis;
import gl.math.FlatMatrix4f;
import gl.renderer.JoglRenderer;
import gl.renderer.TrackBallCamera;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.DebugGL2;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;

import swing.component.ControlFrame;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;
import com.jogamp.opengl.util.glsl.ShaderState;

public class A2App implements GLEventListener {
	
	public static void main( String[] args ) {
		new A2App();
	}

    boolean antialiasing = false;
	
    private FPSAnimator animator;
    
    Dimension size = new Dimension(512,512);
    
    Dimension controlSize = new Dimension(500, 500);
    
	public A2App() {
		 ControlFrame controlFrame = new ControlFrame("Controls");
	        // We'll disable the camera tab for now, as nobody should need it until assignment 3
	        // well, then again, perhaps assignment 2??
	        controlFrame.add("Scene", getControls());
	        controlFrame.setSelectedTab("Scene");
	                                
	        controlFrame.setSize(controlSize.width, controlSize.height);
	        controlFrame.setLocation(size.width + 20, 0);
	        controlFrame.setVisible(true);    
	                
	        GLProfile glp = GLProfile.getDefault();        
	        GLCapabilities glcap = new GLCapabilities(glp);
	        
	        // NOTE: you can turn on full screen (window) anti-aliasing with the following
	        // lines, but it may be very slow depending on your hardware!
	        if ( antialiasing ) {
		        glcap.setHardwareAccelerated(true);
		        glcap.setNumSamples(4);
		        glcap.setSampleBuffers(true);
	        }
	        
	        GLCanvas glCanvas = new GLCanvas(glcap);
	        glCanvas.setSize( size.width, size.height );
	        glCanvas.setIgnoreRepaint( true );
	        glCanvas.addGLEventListener( this );
	        tbc.attach(glCanvas);	        
	        
	        JFrame frame = new JFrame( "Momentum Handles" );
	        frame.getContentPane().setLayout( new BorderLayout() );
	        frame.getContentPane().add( glCanvas, BorderLayout.CENTER );  // NOTE: this is where things are glued into the screen.
	        frame.setLocation(0,0);
	        
	        animator = new FPSAnimator( glCanvas, 60 );
	                
	        frame.setUndecorated( false );
	        frame.addWindowListener( new WindowAdapter() {
	            @Override
	            public void windowClosing( WindowEvent e ) {
	            	System.exit(0);
	            }
	        });
	        frame.setSize( frame.getContentPane().getPreferredSize() );
	        frame.setVisible( true );
	 
	        glCanvas.requestFocus();

	        if ( animator != null ) animator.start();
	}
	
    
    private float aspect=-1;

    private DoubleParameter lightViewRadius = new DoubleParameter("light view radius", 3, 1, 10 );
    
    private DoubleParameter camRotate = new DoubleParameter("cam rotate" , 0, -180, 180);
    
    private DoubleParameter fovy = new DoubleParameter("FoVY", 45, 0, 180);
    
    private DoubleParameter sigma = new DoubleParameter( "sigma", 1, 0, 3 );
    
    private DoubleParameter scaleOffset = new DoubleParameter( "scale offset", 0, -3, 3 );
    
    private DoubleParameter scaleMag = new DoubleParameter( "scale magnitude", 1, -3, 3 );

	private GLU glu = new GLU();
	
	private ShaderState pflState = new ShaderState();

	private ShaderProgram pflProgram = new ShaderProgram();
	
	private ShaderState shaderState = new ShaderState();

	private ShaderProgram shaderProgram = new ShaderProgram();

	private int[] renderedTexture = new int[1];

	private int[] depthrenderbuffer = new int[1];

	private int[] savedFrameBufferID = new int[1];
	
	private int[] FramebufferName = new int[1];
	
	private int[] depthTexture = new int[1];

	private int[] depthFBO = new int[1];

	private TrackBallCamera tbc = new TrackBallCamera();

	@Override
	public void init(GLAutoDrawable drawable) {
		drawable.setGL( new DebugGL2( drawable.getGL().getGL2() ) );
		GL2 gl = drawable.getGL().getGL2();
        
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glDisable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL2.GL_LIGHTING);
        
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);    // Black Background
        gl.glClearDepth(1.0f);                      // Depth Buffer Setup
        
        gl.glEnable( GL.GL_BLEND );
        gl.glBlendFunc( GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA );
        gl.glEnable( GL.GL_LINE_SMOOTH );
        gl.glEnable( GL2.GL_POINT_SMOOTH );
        gl.glEnable( GL2.GL_MULTISAMPLE );
	
        // no ambient light by default... ?
        gl.glLightModelfv( GL2.GL_LIGHT_MODEL_AMBIENT, new float[] {0,0,0,1}, 0);

        // some shinyness by default...
        gl.glMaterialfv( GL.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, new float[] {1,1,1,1}, 0 );
        gl.glMaterialfv( GL.GL_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[] {1,1,1,1}, 0 );
        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, new float[] {1,1,1,1}, 0 );
        gl.glMaterialf( GL.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 50 );

        // SET UP FRAGMENT PROGRAM FOR DRAWING DEPTH
        {
	        ShaderCode vsCode = ShaderCode.create( gl, GL2ES2.GL_VERTEX_SHADER, this.getClass(), "shader", "shader/bin", "depthDraw", false );
	        ShaderCode fsCode = ShaderCode.create( gl, GL2ES2.GL_FRAGMENT_SHADER, this.getClass(), "shader", "shader/bin", "depthDraw", false );	        
	        shaderProgram.add( vsCode );
	        shaderProgram.add( fsCode );
			if (!shaderProgram.link(gl, System.err)) {
				throw new GLException("Couldn't link program: " + shaderProgram);
			}	
			shaderState.attachShaderProgram( gl, shaderProgram, false );		
//			shaderState.setVerbose(true);
        }
        
        // SET UP FRAGMENT PROGRAM FOR PER FRAGMENT LIGHTING
        {
        	ShaderCode vsCode = ShaderCode.create( gl, GL2ES2.GL_VERTEX_SHADER, this.getClass(), "shader", "shader/bin", "shadow", false );
	        ShaderCode fsCode = ShaderCode.create( gl, GL2ES2.GL_FRAGMENT_SHADER, this.getClass(), "shader", "shader/bin", "shadow", false );	        
	        pflProgram.add( vsCode );
	        pflProgram.add( fsCode );
			if (!pflProgram.link(gl, System.err)) {
				throw new GLException("Couldn't link program: " + pflProgram);
			}	
			pflState.attachShaderProgram( gl, pflProgram, false );
//			pflState.setVerbose(true);
        }
				
		// SET UP RENDER TO TEXTURE
		
        // Save the current frame buffer binding        
        gl.glGetIntegerv( GL.GL_FRAMEBUFFER_BINDING, savedFrameBufferID, 0 );
		
		// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.		
		gl.glGenFramebuffers(1, FramebufferName, 0);
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, FramebufferName[0]);
		
		// The texture we're going to render to
		gl.glGenTextures(1, renderedTexture, 0);		 
		// "Bind" the newly created texture : all future texture functions will modify this texture
		gl.glBindTexture(GL.GL_TEXTURE_2D, renderedTexture[0]);		 

		// Give an empty image to OpenGL ( the last "0" )
		gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, 512, 512, 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, null);
		
		// Poor filtering. (not actually needed, nor need to set it here)
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);

		// The depth buffer
		gl.glGenRenderbuffers(1, depthrenderbuffer, 0);
		gl.glBindRenderbuffer(GL.GL_RENDERBUFFER, depthrenderbuffer[0]);
		gl.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL2.GL_DEPTH_COMPONENT, 512, 512);
		gl.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_RENDERBUFFER, depthrenderbuffer[0]);

		// Set "renderedTexture" as our colour attachment #0		
		gl.glFramebufferTexture2D( GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0,  GL.GL_TEXTURE_2D, renderedTexture[0], 0);		

		// Set the list of draw buffers.
		int[] DrawBuffers = new int[] { GL.GL_COLOR_ATTACHMENT0 };
		gl.glDrawBuffers(1, DrawBuffers, 0); // "1" is the size of DrawBuffers

		// Always check that our framebuffer is ok
		checkFramebufferStatus( gl );
		
		gl.glGenTextures( 1, depthTexture, 0 );
		gl.glBindTexture( GL.GL_TEXTURE_2D, depthTexture[0] );
		  gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_BORDER);//EDGE);
		  gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_BORDER);//EDGE);
		  gl.glTexParameterfv(GL.GL_TEXTURE_2D, GL2.GL_TEXTURE_BORDER_COLOR, new float[] {1,1,1,1}, 0 );//EDGE);
		
		// These two are essential for a proper lookup!  oof!  Otherwise why?
		  gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);  // default values are bad!  ;)
		  gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		  
	//	gl.glTexParameteri(GL.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, GL2.GL_INTENSITY);
	//	  gl.glTexParameteri(GL.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE, GL2.GL_COMPARE_R_TO_TEXTURE);
//		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE, GL.GL_NONE);
	//	  gl.glTexParameteri(GL.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC, GL.GL_LEQUAL);
		  
//		  //NULL means reserve texture memory, but texels are undefined
//		  //You can also try GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24 for the internal format.
//		  //If GL_DEPTH24_STENCIL8_EXT, go ahead and use it (GL_EXT_packed_depth_stencil)
		  gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_DEPTH_COMPONENT32, 512, 512, 0, GL2.GL_DEPTH_COMPONENT, GL.GL_UNSIGNED_INT, null);
//		  //-------------------------

		  
		  gl.glGenFramebuffers( 1, depthFBO, 0);
		  gl.glBindFramebuffer( GL.GL_FRAMEBUFFER, depthFBO[0] );
//		  //Attach
		  gl.glFramebufferTexture2D( GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_TEXTURE_2D, depthTexture[0], 0);
//		  //-------------------------
//		  //Does the GPU support current FBO configuration?
//		  //Before checking the configuration, you should call these 2 according to the spec.
//		  //At the very least, you need to call glDrawBuffer(GL_NONE)
		  gl.glDrawBuffer(GL.GL_NONE);
		  gl.glReadBuffer(GL.GL_NONE);
		  
		  checkFramebufferStatus( gl );
		
        // Restore the original frame buffer binding
        gl.glBindFramebuffer( GL.GL_FRAMEBUFFER, savedFrameBufferID[0] );
	}
	
    private int checkFramebufferStatus(GL gl) {
    	String statusString = "";
        int framebufferStatus = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);
        switch (framebufferStatus) {
            case GL.GL_FRAMEBUFFER_COMPLETE:
                statusString = "GL_FRAMEBUFFER_COMPLETE";
                break;
            case GL.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                statusString = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENTS";
                break;
            case GL.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                statusString = "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
                break;
            case GL.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
                statusString = "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS";
                break;
            case GL.GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
                statusString = "GL_FRAMEBUFFER_INCOMPLETE_FORMATS";
                break;
            case GL2.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                statusString = "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER";
                break;
            case GL2.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                statusString = "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER";
                break;
            case GL.GL_FRAMEBUFFER_UNSUPPORTED:
                statusString = "GL_FRAMEBUFFER_UNSUPPORTED";
                break;
        }
        if ( framebufferStatus != GL.GL_FRAMEBUFFER_COMPLETE ) {
        	throw new GLException(statusString);
        }
        return framebufferStatus;
    }
    
    DoubleParameter lightPosx = new DoubleParameter( "light pos x", 0, -10, 10 );	
    DoubleParameter lightPosy = new DoubleParameter( "light pos y", 10, -10, 20 );
    DoubleParameter lightPosz = new DoubleParameter( "light pos z", 3, -10, 10 );
    
    float[] lightPos = new float[4];
    
    public void setupLightsInEye( GLAutoDrawable drawable ) {
    	GL2 gl = drawable.getGL().getGL2();
    	gl.glMatrixMode(GL2.GL_MODELVIEW);
    	gl.glPushMatrix();
        gl.glLoadIdentity();
        tbc.applyViewTransformation(drawable);
        FlatMatrix4f M = new FlatMatrix4f();
        gl.glGetFloatv( GL2.GL_MODELVIEW_MATRIX, M.asArray(), 0 );
        M.reconstitute();
        gl.glPopMatrix();        
        gl.glLoadIdentity();

        Point3f pos = new Point3f( lightPosx.getFloatValue(), lightPosy.getFloatValue(), lightPosz.getFloatValue() );
        //M.getBackingMatrix().invert();
        M.getBackingMatrix().transform(pos);
        
        lightPos[0] = pos.x;
        lightPos[1] = pos.y;
        lightPos[2] = pos.z;
        lightPos[3] = 1;
        
        float[] position = { pos.x, pos.y, pos.z, 1 };
        float[] colour = { .8f, .8f, .8f, 1 };
        float[] acolour = {0,0,0,1};//{ .05f, .05f, .05f, 1 };
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_SPECULAR, colour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, acolour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0 );
        gl.glEnable( GL2.GL_LIGHT0 );

    }

    public void setupLightsInWorld( GLAutoDrawable drawable ) {
    	GL2 gl = drawable.getGL().getGL2();
        float[] position = { lightPosx.getFloatValue(), lightPosy.getFloatValue(), lightPosz.getFloatValue(), 1 };
        float[] colour = { .8f, .8f, .8f, 1 };
        float[] acolour = {0,0,0,1};//{ .05f, .05f, .05f, 1 };
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_SPECULAR, colour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, acolour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0 );
        gl.glEnable( GL2.GL_LIGHT0 );
    }
    
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
	
        //////////////////////////////////////////////////////////////////        
		// Render to our off-screen depth frame buffer object (render to texture)
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, depthFBO[0]);
		
		gl.glViewport( 0, 0, 512, 512 ); 
		// Render on the whole framebuffer, complete from the lower left corner to the upper right
		// TODO: this size is probably not right.. need to change how it is constructed above too!
		// BE SAFE!!! STICK TO MULTIPLES OF 2

		gl.glClearColor( 0, 0, 0, 1 );
        gl.glClear( GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );

        final int[] viewportDimensions = new int[4];
        gl.glGetIntegerv(GL.GL_VIEWPORT, viewportDimensions, 0);
        aspect = (viewportDimensions[2] ) / (float)(viewportDimensions[3]);
        
        Point3d lPos = new Point3d( lightPosx.getValue(), lightPosy.getValue(), lightPosz.getValue() );
        Point3d at = new Point3d( 0,0,0 );
        double distance = lPos.distance( at );
        
        gl.glMatrixMode( GL2.GL_PROJECTION );
        gl.glLoadIdentity();        
        // TODO: option... could be changed to an orthographic projection 
        // for a directional light, along with the job of figuring out how big

        gl.glRotatef(camRotate.getFloatValue(),0,0,1);
        glu.gluLookAt( lPos.x, lPos.y, lPos.z, at.x, at.y, at.z, 1, 0, 0 );   
        FlatMatrix4f lightFrame = new FlatMatrix4f();        
        gl.glGetFloatv( GL2.GL_PROJECTION_MATRIX, lightFrame.asArray(), 0 );        
        lightFrame.reconstitute();
        
        gl.glLoadIdentity();
        glu.gluPerspective(fovy.getValue(), aspect, distance - lightViewRadius.getValue(), distance + lightViewRadius.getValue() + farBoost.getValue() );
        
        gl.glRotatef(camRotate.getFloatValue(),0,0,1);
        glu.gluLookAt( lPos.x, lPos.y, lPos.z, at.x, at.y, at.z, 1, 0, 0 );   

        FlatMatrix4f P = new FlatMatrix4f();        
        gl.glGetFloatv( GL2.GL_PROJECTION_MATRIX, P.asArray(), 0 );        
        P.reconstitute();
        FlatMatrix4f Pinv = new FlatMatrix4f();        
        Pinv.getBackingMatrix().invert( P.getBackingMatrix() );
                
        gl.glMatrixMode( GL2.GL_MODELVIEW );	
        gl.glLoadIdentity();
        drawScene( drawable ); 
        
        gl.glMatrixMode( GL2.GL_PROJECTION );	
        tbc.applyInverseViewTransformation(drawable);
        gl.glGetFloatv( GL2.GL_PROJECTION_MATRIX, P.asArray(), 0 );        
        P.reconstitute();
		
        //////////////////////////////////////////////////////////////////        
		// Render to the screen
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0); // alternatively we could use savedFrameBufferID[0]
		
		gl.glViewport(0,0, drawable.getWidth(),drawable.getHeight()); // Render on the whole framebuffer, complete from the lower left corner to the upper right
		gl.glClearColor( .2f, 0, 0, 1 );
        gl.glClear( GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );

		tbc.prepareForDisplay(drawable);

        setupLightsInWorld(drawable);

		if ( enablePFL.getValue() ) {
			pflState.useProgram(gl, true);
			int shadowMapID = pflState.getUniformLocation( gl, "shadowMap");
			int lightProjectionID = pflState.getUniformLocation( gl, "lightProjection");	
			int lightPosID = pflState.getUniformLocation( gl, "lPos");  			
			gl.glUniform1i( shadowMapID, 0 ); // use texture unit zero!
			gl.glUniform4fv( lightPosID, 1, lightPos, 0 );
			gl.glUniformMatrix4fv( lightProjectionID, 1, false, P.asArray(), 0 ); // oof!  // ARGH! don't screw up the numbers here!
		}
		
		drawScene(drawable);
		
		if ( enablePFL.getValue() ) {
			pflState.useProgram(gl, false);
		}
		
		if ( drawFrustum.getValue() ) {

			// draw the light's camera frame
			gl.glLightModelfv( GL2.GL_LIGHT_MODEL_AMBIENT, new float[] {.5f,.5f,.5f,1}, 0);
			lightFrame.getBackingMatrix().invert();
			gl.glPushMatrix();
			gl.glMultMatrixf( lightFrame.asArray(), 0 );
			final FancyAxis fa = new FancyAxis();
			fa.draw(gl);
			gl.glPopMatrix();
	        gl.glLightModelfv( GL2.GL_LIGHT_MODEL_AMBIENT, new float[] {0,0,0,1}, 0);

	        // draw the light frustum
	        gl.glDisable( GL2.GL_LIGHTING );
	        gl.glPushMatrix();
	        gl.glColor4f(1,1,1,0.5f);
	        gl.glMultMatrixf(Pinv.asArray(),0);			
	        gl.glLineWidth(3);       
	        JoglRenderer.glut.glutWireCube(2);
	        gl.glEnable( GL2.GL_LIGHTING );
        
			// draw the depth map on the near plane of the frustum
			shaderState.useProgram(gl, true);
			
			gl.glBindTexture(GL.GL_TEXTURE_2D, depthTexture[0]);		 	
			int texID = shaderState.getUniformLocation( gl, "depthTexture");
			int timeID = shaderState.getUniformLocation( gl, "alpha");  // unused... can remove
			gl.glUniform1i( texID, 0 ); // use texure unit zero!
			gl.glUniform1f( timeID,  0.5f ); //(float) (0.5 + 0.5 * Math.cos(System.nanoTime() / 1e9f) ) );		
			gl.glDisable( GL2.GL_LIGHTING );
	        gl.glEnable( GL.GL_TEXTURE_2D );
	        
	        // Draw a quadrilateral as two triangles, along with texture coordinates. 
	        gl.glBegin( GL2.GL_TRIANGLES );
	        gl.glTexCoord2d(0, 0);
	        gl.glVertex3f(-1,-1, -1.0f);
	        gl.glTexCoord2d(1, 0);
	        gl.glVertex3f(1, -1, -1.0f);
	        gl.glTexCoord2d(0, 1);
	        gl.glVertex3f(-1, 1, -1.0f);
	        gl.glTexCoord2d(0, 1);
	        gl.glVertex3f(-1,  1, -1.0f);
	        gl.glTexCoord2d(1, 0);
	        gl.glVertex3f(1, -1, -1.0f);
	        gl.glTexCoord2d(1, 1);
	        gl.glVertex3f(1,  1, -1.0f);
	        gl.glEnd();
	        gl.glDisable( GL.GL_TEXTURE_2D );
	        gl.glEnable( GL2.GL_LIGHTING );
	        	   
			shaderState.useProgram(gl, false);
			
			gl.glPopMatrix();
		}
        
        tbc.cleanupAfterDisplay(drawable);

	}
	
	public void drawScene( GLAutoDrawable drawable ) {
		GL2 gl = drawable.getGL().getGL2();

        float[] orange = new float[] {1,.5f,0,1};
        float[] red    = new float[] {1,0,0,1};
        float[] green  = new float[] {0,1,0,1};
        float[] blue   = new float[] {0,0,1,1};

        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, red, 0 );

		gl.glDisable(GL.GL_CULL_FACE);       
		gl.glPushMatrix();
		gl.glRotated(45, 0, 1, 0);
		JoglRenderer.glut.glutSolidTeapot(1);
        gl.glPopMatrix();
        
        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, green, 0 );

        gl.glPushMatrix();
        gl.glTranslated(2,0,0);
        JoglRenderer.glut.glutSolidSphere(1,30,20);
        gl.glPopMatrix();
        
        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, blue, 0 );
        gl.glPushMatrix();
        gl.glTranslated(0,0,2);
        JoglRenderer.glut.glutSolidCube(1);
        gl.glPopMatrix();
        
        // don't need this if we're doing it in the per fragment lighting program
        // but we'll include it for when PFL is not enabled
        gl.glEnable( GL2.GL_NORMALIZE );
        
        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, orange, 0 );

        gl.glPushMatrix();
        gl.glTranslated(0,-.75,0);
        gl.glScaled(15,0.1,15);
        JoglRenderer.glut.glutSolidCube(1);
        gl.glPopMatrix();
     
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		// do nothing
	}
	
	@Override
	public void dispose(GLAutoDrawable drawable) {
		// do nothing
	}
	
	private BooleanParameter drawFrustum = new BooleanParameter( "draw frustum", false );
	private BooleanParameter enablePFL = new BooleanParameter( "per fragment lighting", false );
	private DoubleParameter farBoost = new DoubleParameter( "far boost", 5, 0, 10 );

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add( fovy.getSliderControls(false) );
		vfp.add( sigma.getSliderControls(false) );
		vfp.add( scaleOffset.getSliderControls(false) );
		vfp.add( scaleMag.getSliderControls(false) );
		
		vfp.add( lightPosx.getSliderControls(false) );
		vfp.add( lightPosy.getSliderControls(false) );
		vfp.add( lightPosz.getSliderControls(false) );
		
		vfp.add( lightViewRadius.getSliderControls(true) );
		vfp.add( farBoost.getSliderControls(false) );
		vfp.add( camRotate.getSliderControls(false));
		
		vfp.add( drawFrustum.getControls() );
		vfp.add( enablePFL.getControls() );
		
		
		
		return vfp.getPanel();
	}
}
