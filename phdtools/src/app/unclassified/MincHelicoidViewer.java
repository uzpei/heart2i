package app.unclassified;

import extension.minc.MINCImage;
import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.SceneRotator;
import helicoid.Helicoid;
import helicoid.modeling.HelicoidGenerator;
import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameterNode;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.HelicoidField;
import tools.HelicoidVoxelBoxGenerator;
import volume.IntensityVolume;
import volume.PerVoxelMethod;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;

public class MincHelicoidViewer implements GLObject, PerVoxelMethod, Interactor {

    public static void main(String[] args) {
        new MincHelicoidViewer();
    }

    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", true);

    /**
     * Display wisps
     */
    private BooleanParameter displayWisps = new BooleanParameter(
            "display wisps", false);

    private HelicoidGenerator generator;
    private HelicoidVoxelBoxGenerator helicoidVoxelGenerator;
    /**
     * The frame field containing voxels and associated coordinate systems.
     */
    private VoxelFrameField frameField;
   
    /**
     * kt fit
     */
    private IntensityVolume vkt;

    /**
     * kn fit
     */
    private IntensityVolume vkn;

    /**
     * kb fit
     */
    private IntensityVolume vkb;

    /**
     * Voxel box
     */
    private VoxelBox voxelBox;
    
    private JoglRenderer ev;
    
    private HelicoidField helicoidBoxField;
    
    public MincHelicoidViewer() {
        
    	generator = new HelicoidGenerator();
    	helicoidVoxelGenerator = new HelicoidVoxelBoxGenerator(generator);
    	
    	// Create x axis data
    	String imgx = "./data/rat/e1";
		MINCImage ex = new MINCImage(imgx + "x.mnc");
		MINCImage ey = new MINCImage(imgx + "y.mnc");
		MINCImage ez = new MINCImage(imgx + "z.mnc");
    	VoxelVectorField voxelImgx = new VoxelVectorField("X axis", ex, ey, ez);

    	// Create z axis data
    	String imgz = "./data/rat/norm";
		MINCImage nx = new MINCImage(imgz + "x.mnc");
		MINCImage ny = new MINCImage(imgz + "y.mnc");
		MINCImage nz = new MINCImage(imgz + "z.mnc");
    	VoxelVectorField voxelImgz = new VoxelVectorField("Z axis", nx, ny, nz);

    	// Create y axis data (as cross product of Z and X)
    	VoxelVectorField voxelImgy = VoxelVectorField.cross(voxelImgz, voxelImgx);
    	voxelImgy.setTitle("Y axis");

    	// Get the voxel spacing
    	spacing = ex.xspace;
    	System.out.println("Voxel spacing = " + spacing);

    	// Set the voxel box
    	Point3i dim = voxelImgx.getDimension();
    	voxelBox = new VoxelBox(new Vector3d(dim.x,dim.y,dim.z), new Point3d(0, 0, 0));

    	voxelBox.sliceTransSagittal();
    	
    	voxelImgx.setVoxelBox(voxelBox);
    	voxelImgz.setVoxelBox(voxelBox);
    	voxelImgy.setVoxelBox(voxelBox);

    	// Create the frame field
    	frameField = new VoxelFrameField(voxelImgx, voxelImgy, voxelImgz);

    	// Apply the mask
    	System.out.println("Setting mask...");
    	String mask = "./data/rat/mask.mnc";
    	voxelBox.setMask(new IntensityVolume(new MINCImage(mask).getVolume()));
    	    	
    	// Create fitting data
    	String fit_kt = "./data/rat/n5p0_kt.mnc";
    	String fit_kn = "./data/rat/n5p0_kn.mnc";
    	String fit_kb = "./data/rat/n5p0_kb.mnc";
    	
    	vkt = new IntensityVolume(new MINCImage(fit_kt).getVolume());
    	vkn = new IntensityVolume(new MINCImage(fit_kn).getVolume());
    	vkb = new IntensityVolume(new MINCImage(fit_kb).getVolume());
    	
    	System.out.println("Voxel scaling = " + voxelBox.getNormalization());
    	
    	// FIXME: Using vkb for alpha (data is not currently available...)
//    	helicoidBoxField = new HelicoidBoxField(voxelBox, vkt, vkn, vkb, vkb);
//    	if (true) 
//    		throw new RuntimeException("Need to adapt this class to the new HelicoidBoxField (i.e. pass the box field as a list of HelicoidParameterNode along with volume dimensions");

    	helicoidBoxField = new HelicoidField();
    
    	fillVolume();
    	
    	// Start the opengl viewer
    	initViewer();
    }
    
    private void fillVolume() {
    	System.out.println("Filling helicoid volume...");
    	List<HelicoidParameterNode> helicoidNodes = new LinkedList<HelicoidParameterNode>();
    	
    	// Assume all volumes have same dimension
    	int[] dim = vkt.getDimension();
    	
    	double kt, kn, kb;
    	int it = 0;
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (!voxelBox.isOutside(i, j, k)) {
						kt = vkt.getIntensity(i, j, k);
						kn = vkn.getIntensity(i, j, k);
						kb = vkb.getIntensity(i, j, k);
						helicoidNodes.add(new HelicoidParameterNode(new HelicoidParameter(kt, kn, kb, 0), new Point3d(i, j, k)));
					}
				}
			}
		}
    	
    	System.out.println("Done with " + helicoidNodes.size() + " nodes.");
    	helicoidBoxField.set(dim, helicoidNodes);
    }
    
    private void initViewer() {
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("",
                this, new Dimension(winsize), new Dimension(650,
                        winsize.height + 90), true);
        ev.getCamera().zoom(800);
        ev.addInteractor(this);
        
        ev.controlFrame.add("Generator", generator.getControls());
        ev.controlFrame.add("Helicoid Voxels", helicoidVoxelGenerator.getControls());
        
        ev.start();
        
        addParameterListeners();
    }
    
    

    private void addParameterListeners() {
    	// Add a parameter listener such that we recompute the helicoid fits
    	// if the voxel box changes.
    	ParameterListener l = new ParameterListener() {
    		@Override
    		public void parameterChanged(Parameter parameter) {
    			computeHelicoids();
    		}
    	};
    	voxelBox.addParameterListener(l);
    	generator.addParameterListener(l);
    	
    	displayWisps.addParameterListener(l);
//    	helicoidVoxelGenerator.addParameterListener(l);
	}

	@Override
    public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        
        // Make x point upwards
        gl.glRotated(90, 0, 0, 1);

        Point3d d = new Point3d(frameField.getF1().getDimension().x, frameField.getF2().getDimension().y, frameField.getF3().getDimension().z);
    	d.scale(0.5 * voxelBox.getNormalization());

        gl.glClearColor(1, 1, 1, 1);
        
        gl.glPushMatrix();
        rotator.rotate(drawable);

        gl.glTranslated(-d.x, -d.y, -d.z);
        
        gl.glPushMatrix();
        
		double n = voxelBox.getNormalization();
		gl.glScaled(n, n, n);

//        frameField.display(drawable);
//        voxelBox.display(drawable);

//		for (Helicoid h : helicoidFits) {
//			gl.glColor3d(0.3, 0.3, 0.3);
//			h.display(drawable, false);
//		}
//		for (Helicoid h : helicoidWisps) {
//			gl.glColor3d(0, 1, 1);
//			h.display(drawable, false);
//		}

//        helicoidBoxField.display(drawable);

        gl.glPopMatrix();
        
        if (displayWorld.getValue()) {
            WorldAxis.display(gl, 1.0f, true);
        }

        gl.glPopMatrix();
                
        
        Point3i di = frameField.getF1().getDimension();
        String text = "";
        text += "Dimensions = [ " + di.x + " " + di.y + " " + di.z + " ]";
        text += "\nG-Helicoids = " + helicoidFits.size();
        text += "\nG-Helicoids (wisp)= " + helicoidWisps.size();
        
        ev.getTextRenderer().setColor(new Color(0, 0, 0, 0));
        ev.getTextRenderer().drawTopLeft(text, drawable);
    }

	private SceneRotator rotator = new SceneRotator();

    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add(displayWorld.getControls());
        vfp.add(displayWisps.getControls());
        vfp.add(voxelBox.getControls());
        vfp.add(frameField.getControls());
        vfp.add(helicoidBoxField.getControls());
        vfp.add(rotator.getControls());

        return vfp.getPanel();
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }
    
    private boolean isBeingComputed = false;
    private void computeHelicoids() {
    	
    	if (true) return;
    	
    	if (isBeingComputed) return;
    	
    	isBeingComputed = true;
    	
    	System.out.print("Recomputing helicoids...");
    	helicoidFits.clear();
    	voxelBox.voxelProcess(this);
    	
		helicoidWisps.clear();
		if (displayWisps.getValue()) {
			helicoidVoxelGenerator.hold(true);
			helicoidVoxelGenerator.setOffsetScale(spacing);
			helicoidVoxelGenerator.hold(false);
			
			// Use a nxn voxel box
			int n = 9;
			Vector3d span = new Vector3d(n, n, n);
			
			for (Helicoid h : helicoidFits) {
				double center = Math.round((n-1) / 2);
				Point3d origin = new Point3d(h.getOrigin());
				origin.sub(new Point3d(n/2, n/2, n/2));
//				origin.sub(new Point3d(center, center, center));
				VoxelBox vb = new VoxelBox(span, origin);
				
				helicoidWisps.addAll(helicoidVoxelGenerator.generateNeighborhood(vb, h.getParameter()));
			}
		}
		System.out.println("done.");
    	isBeingComputed = false;
    }

    private List<Helicoid> helicoidFits = new LinkedList<Helicoid>();
    private List<Helicoid> helicoidWisps = new LinkedList<Helicoid>();
    private double spacing;
  
	@Override
	public void process(int x, int y, int z) {
		double kt = vkt.getIntensity(x, y, z);
		double kn = vkn.getIntensity(x, y, z);
		double kb = vkb.getIntensity(x, y, z);
		
		// TODO: eventually should get a fit for alpha as well
		double alpha = 0;
		HelicoidParameter hp = new HelicoidParameter(kt, kn, kb, alpha);
//		HelicoidParameter hp = new HelicoidParameter(kb, kn, kt, alpha);
		
		Matrix4d frame = frameField.get(x, y, z).getFrame().getFrame();
		generator.setOrientationFrame(frame);
		
		int n = 20;
		generator.getParameterControl().setStepCount(n);
		generator.getParameterControl().setStepSize(4*spacing / n);

		Helicoid h = generator.generate(hp, new Point3d(x, y, z));	
		
		
		helicoidFits.add(h);
	}
	
    @Override
    public void attach(Component component) {

        component.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_G)
                	computeHelicoids();
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
    }

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean isValid(Point3d origin, Vector3d span) {
		return true;
	}
}
