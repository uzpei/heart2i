package app.pami2013.core.setup.computation.table;

import heart.Heart.Species;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import system.interfaces.TextTransfer;
import tools.TextToolset;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.setup.computation.AllComputationSetup;
import app.pami2013.core.tools.PamiOutputManager;
import extension.latex.LatexTable;
import extension.matlab.MatlabScript;

public class FilteringVsMethodTable extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Direct, FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL);
	protected static List<Integer> neighborhoods = Arrays.asList(3);
//	protected static List<IterativeFilter> filters = AllComputationSetup.filters;
	private static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0), new IterativeFilter(10, 0.2), new IterativeFilter(10, 0.4));

	public FilteringVsMethodTable() {
		super(PamiExperimentSetup.fromList(ExperimentType.FittingMethod, ExperimentType.Smoothing, methods, models, filters, neighborhoods));
		executeExperiments = false;
		combineExperiments = false;
		exportTable = true;
		exportHistograms = false;
		
		super.setTableDefinition(new OutputTableDefinition("Effect of iterative gaussian smoothing applied to $f_1$ on extrapolation errors and connection forms for the direct and optimized parameter computations.", "table:smoothingvsmethods", Arrays.asList(Parameter.values()), true));
	}
	
	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		// TODO: move this to field within FittingExperiment
		Species species = Species.Rat;
		LatexTable table = PamiOutputManager.createLatexTable(experiment, species);
		TextTransfer.appendContent(table.assemble());

		System.out.println(TextToolset.box("LATEX TABLES COPIED TO CLIBPARD", '#'));
		
		return null;
	}

}
