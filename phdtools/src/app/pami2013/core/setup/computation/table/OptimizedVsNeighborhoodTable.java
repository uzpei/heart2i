package app.pami2013.core.setup.computation.table;

import heart.Heart.Species;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import system.interfaces.TextTransfer;
import tools.TextToolset;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.tools.PamiOutputManager;
import extension.latex.LatexTable;
import extension.matlab.MatlabScript;

public class OptimizedVsNeighborhoodTable extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL);
	protected static List<Integer> neighborhoods = Arrays.asList(3, 5, 7);
	protected static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));

	/**
	 * One figure per neighborhood size.
	 * Each figure has all cartan models (legend) for a given connection form or error
	 */
	public OptimizedVsNeighborhoodTable() {
		super(PamiExperimentSetup.fromList(ExperimentType.CartanModel, ExperimentType.NeighborhoodSize, methods, models, filters, neighborhoods));
		executeExperiments = false;
		combineExperiments = false;
		exportTable = true;
		exportHistograms = false;
		
		setTableDefinition(new OutputTableDefinition("Effect of neighborhood size on optimized parameter computations for the rat dataset.", "table:methodcomp", Arrays.asList(Parameter.values()), true));
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		// TODO: move this to field within FittingExperiment
		Species species = Species.Rat;
		LatexTable table = PamiOutputManager.createLatexTable(experiment, species);
		TextTransfer.appendContent(table.assemble());

		System.out.println(TextToolset.box("LATEX TABLES COPIED TO CLIBPARD", '#'));

		return null;
	}

	
}
