package app.pami2013.core.setup.computation.plot;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import extension.matlab.MatlabScript;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.setup.computation.AllComputationSetup;
import app.pami2013.core.tools.PamiOutputManager;

public class FittingMethodVsFilteringPlots extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL);
	protected static List<Integer> neighborhoods = Arrays.asList(3);
	protected static List<IterativeFilter> filters = AllComputationSetup.filters;

	public FittingMethodVsFilteringPlots() {
		super(PamiExperimentSetup.fromList(ExperimentType.FittingMethod, ExperimentType.Smoothing, methods, models, filters, neighborhoods));

		executeExperiments = false;
		combineExperiments = false;
		exportTable = false;
		exportHistograms = true;
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		return Arrays.asList(PamiOutputManager.plotMethodVsFiltering(experiment.getHeartDefinitions(), experiment.getOutputPath()));
	}

}
