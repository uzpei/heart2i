package app.pami2013.core.setup.computation.plot;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import extension.matlab.MatlabScript;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.tools.PamiOutputManager;

public class ModelVsNeighborhoodSizePlots extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL, CartanModel.HOMEOID, CartanModel.HELICOID, CartanModel.C123, CartanModel.CONSTANT);
	protected static List<Integer> neighborhoods = Arrays.asList(3, 5, 7);
	protected static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));

	public ModelVsNeighborhoodSizePlots() {
		super(PamiExperimentSetup.fromList(ExperimentType.CartanModel, ExperimentType.NeighborhoodSize, methods, models, filters, neighborhoods));

		executeExperiments = false;
		combineExperiments = false;
		exportTable = false;
		exportHistograms = true;
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		return Arrays.asList(PamiOutputManager.plotModelsVsNeighborhoodSize(experiment.getHeartDefinitions(), experiment.getOutputPath()));
	}

}
