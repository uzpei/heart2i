package app.pami2013.core.setup.computation.plot;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import extension.matlab.MatlabScript;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.tools.PamiOutputManager;

public class ErrorVersusFormsPlotSetup extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL);
	protected static List<Integer> neighborhoods = Arrays.asList(3);
	protected static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));

	/**
	 * TODO: need to fix this
	 */
	public ErrorVersusFormsPlotSetup() {
		super(PamiExperimentSetup.fromList(ExperimentType.RunOnly, ExperimentType.RunOnly, methods, models, filters, neighborhoods));
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		return Arrays.asList(PamiOutputManager.plotConnectionsVsNeighborhood(experiment.getHeartDefinitions(), experiment.getOutputPath()));
	}

}
