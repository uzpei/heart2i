package app.pami2013.core.setup.computation;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import extension.matlab.MatlabScript;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.core.setup.PamiExperimentSetup;

public class AllComputationN7Setup extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Direct, FittingMethod.Optimized);
//	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL, CartanModel.HOMEOID, CartanModel.HELICOID, CartanModel.C123, CartanModel.CONSTANT);
//	protected static List<CartanModel> models = Arrays.asList(CartanModel.CONSTANT);
	
	protected static List<Integer> neighborhoods = Arrays.asList(7);
	public static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0), new IterativeFilter(5, 0.2), new IterativeFilter(10, 0.2), new IterativeFilter(5, 0.4), new IterativeFilter(10, 0.4));
//	public static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));

	/**
	 * Run all computations but produce no visual output.
	 * <br/>1) All fitting methods (optimized, direct)
	 * <br/>2) All main cartan models (full, homeoid, ghm, c123, constant)
	 * <br/>3) All smoothing magnitudes (0, 0.2sqrt(5),0.2sqrt(10), 0.4sqrt(5), 0.4sqrt(5) 
	 * <br/>4) All neighborhood sizes (3, 5, 7) isotropic
	 */
	public AllComputationN7Setup() {
		super(PamiExperimentSetup.fromList(ExperimentType.RunOnly, ExperimentType.RunOnly, methods, models, filters, neighborhoods));
		executeExperiments = true;
		combineExperiments = true;
		exportTable = false;
		exportHistograms = false;
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		return null;
	}

}
