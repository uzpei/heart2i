package app.pami2013.core.setup.computation;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.tools.PamiOutputManager;
import extension.matlab.MatlabScript;

public class AllConnectionsPlot extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
//	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Direct);
//	protected static List<CartanModel> models = Arrays.asList(CartanModel.C121, CartanModel.C122, CartanModel.C123, CartanModel.C131, CartanModel.C132, CartanModel.C133);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.CONSTANT, CartanModel.C121, CartanModel.C122, CartanModel.C123, CartanModel.C131, CartanModel.C132, CartanModel.C133, CartanModel.C231, CartanModel.C232, CartanModel.C233);
//	protected static List<CartanModel> models = Arrays.asList(CartanModel.C121, CartanModel.C122, CartanModel.C123, CartanModel.C131, CartanModel.C132, CartanModel.C133, CartanModel.C231, CartanModel.C232, CartanModel.C233);
//	protected static List<CartanModel> models = Arrays.asList(CartanModel.C131);
	protected static List<Integer> neighborhoods = Arrays.asList(3);
	public static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));
//	public static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0), new IterativeFilter(5, 0.2), new IterativeFilter(10, 0.2), new IterativeFilter(5, 0.5), new IterativeFilter(10, 0.4));

	public AllConnectionsPlot() {
		super(PamiExperimentSetup.fromList(ExperimentType.CartanModel, ExperimentType.RunOnly, methods, models, filters, neighborhoods));
		validate = false;
		executeExperiments = true;
		combineExperiments = false;
		exportTable = false;
		exportHistograms = true;
		CartanFittingParameter.DEFAULT_USE_DIRECT_SEEDS = false;
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		int bincount = 100;
		int smoothing = 1;
		List<MatlabScript> scripts = new LinkedList<MatlabScript>();
//		scripts.addAll(PamiOutputManager.outputMatlab(experiment, bincount, smoothing));
		scripts.add(PamiOutputManager.plotConnectionsForErrors(experiment.getHeartDefinitions(), experiment.getOutputPath(), experiment.getFittingParameter().getFittingMethod(), experiment.getFittingParameter().getNeighborhoodSize()));
		return scripts;
	}

}
