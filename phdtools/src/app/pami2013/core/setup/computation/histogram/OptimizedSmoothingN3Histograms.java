package app.pami2013.core.setup.computation.histogram;

import java.util.Arrays;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import extension.matlab.MatlabScript;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.setup.computation.AllComputationSetup;
import app.pami2013.core.tools.PamiOutputManager;

public class OptimizedSmoothingN3Histograms extends PamiExperimentSetup {

	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL);
	protected static List<Integer> neighborhoods = Arrays.asList(3);
	protected static List<IterativeFilter> filters = AllComputationSetup.filters;

	/**
	 * Produce a histogram of connection forms for default computation settings.
	 */
	public OptimizedSmoothingN3Histograms() {
		super(PamiExperimentSetup.fromList(ExperimentType.Smoothing, ExperimentType.RunOnly, methods, models, filters, neighborhoods));
		executeExperiments = false;
		combineExperiments = false;
		exportTable = false;
		exportHistograms = true;
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		int bincount = 100;
		int smoothing = 1;
		return PamiOutputManager.outputMatlab(experiment, bincount, smoothing);
	}

}
