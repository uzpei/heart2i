package app.pami2013.core.setup.computation.histogram;

import heart.Heart;
import heart.Heart.Species;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import system.object.Pair;
import volume.IntensityVolume;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.experiments.PamiExperimentRunner;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.tools.PamiOutputManager;
import app.pami2013.measures.DepthMeasurement;
import extension.matlab.MatlabIO;
import extension.matlab.MatlabScript;

public class TransmuralConnectionHistograms extends PamiExperimentSetup {
	protected static List<FittingMethod> methods = Arrays.asList(FittingMethod.Optimized);
	protected static List<CartanModel> models = Arrays.asList(CartanModel.FULL);
	protected static List<Integer> neighborhoods = Arrays.asList(3);
	protected static List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));

	/**
	 * Produce a histogram of connection forms for default computation settings.
	 */
	public TransmuralConnectionHistograms() {
		super(PamiExperimentSetup.fromList(ExperimentType.CartanModel, ExperimentType.RunOnly, methods, models, filters, neighborhoods));
		executeExperiments = false;
		combineExperiments = false;
		exportTable = false;
		exportHistograms = true;
	}
	
	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		// TODO: control these parameters somewhere else
		Species species = Species.Rat;
		double[] wallBounds = new double[] { 0, 20 };
		double boundScale = 0.75;

		List<MatlabScript> scripts = new LinkedList<MatlabScript>();
		HashMap<Parameter, HashMap<Integer, List<Double>>> allParameterDepths = new HashMap<Parameter, HashMap<Integer, List<Double>>>();

		for (Parameter p : CartanParameter.Parameter.values()) {
			allParameterDepths.put(p, new HashMap<Integer, List<Double>>());
		}

		/*
		 * For each rat subject
		 */
		for (Heart heart : experiment.getSpecies(species)) {
			heart.prepareDataIfNecessary();
//			DepthMeasurement measure = new DepthMeasurement(heart);

			/*
			 * For each connection parameter (form and error)
			 */
			for (Parameter parameter : CartanParameter.Parameter.values()) {
				// Load this volume
				IntensityVolume volume = MatlabIO.loadMAT(
						experiment.getOutputPathVolume(heart.getDefinition(),
								parameter)).asVolume();

				// Add contribution from this heart and parameter
//				HashMap<Integer, List<Double>> map = measure.integrateDepth(volume);
				HashMap<Integer, List<Double>> map = DepthMeasurement.integrateShells(volume, heart.getDistanceTransformEpi(), heart.getMask());

				// Merge maps
				allParameterDepths.get(parameter).putAll(map);
			}
		}

		String exportPath = experiment.getImageOutputPath(species) + "transmural/" + experiment.getParameterTag() + "/";

		HashMap<Parameter, double[]> bounds = PamiExperimentRunner.getCardiacBounds(species, boundScale);
		for (Parameter p : CartanParameter.Parameter.values()) {
//		for (Parameter p : Arrays.asList(CartanParameter.Parameter.C121, CartanParameter.Parameter.C122)) {
			scripts.add(PamiOutputManager.plotDepthHist(exportPath, p.toString(), allParameterDepths.get(p), new Pair<double[], double[]>(wallBounds, bounds.get(p)), p.getLatexString()));
		}

		return scripts;
	}

}
