package app.pami2013.core.setup;

import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import extension.matlab.MatlabScript;

public class GenericExperiment extends PamiExperimentSetup {

	public GenericExperiment(OutputTableDefinition tableDefinition,
			List<ParameterDefinition> definitions, ExperimentType variable,
			ExperimentType parameter) {
		super(tableDefinition, definitions, variable, parameter);
	}

	@Override
	public List<MatlabScript> output(FittingExperiment experiment) {
		return null;
	}

}
