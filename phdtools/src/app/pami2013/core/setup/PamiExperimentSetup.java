package app.pami2013.core.setup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import tools.TextToolset;
import tools.geom.MathToolset;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import extension.matlab.MatlabScript;

/**
 * TODO: get number of intra-figure targets (hearts, models, neighborhood sizes, etc.)
 * TODO: get number of inter-figure targets
 * @author piuze
 *
 */
public abstract class PamiExperimentSetup {

	public abstract List<MatlabScript> output(FittingExperiment experiment);
	
	private List<ParameterDefinition> globalDefinitions;
	
	/**
	 * Whether the experiment should be validated for outliers.
	 */
	public boolean validate = true;
	
	private List<CartanModel> models;
	private List<FittingMethod> methods;
	private List<Integer> neighborhoodSizes;
	private List<IterativeFilter> filters;
	
	private ParameterDefinition currentDefinition;
	
	private OutputTableDefinition tableDefinition;
	
	
	public boolean executeExperiments = false; 
	public boolean combineExperiments = false; 
	public boolean exportTable = false; 
	public boolean exportHistograms = false;

	/**
	 * Experiment parameter that we are varying for the comparison.
	 * e.g. the neighborhood size
	 */
	private ExperimentType targetParameter;
	
	/**
	 * Variables that we are comparing together.
	 * e.g. the fitting methods
	 */
	private ExperimentType targetVariables;
	
	/**
	 * Index over all experiment iterations.
	 */
	private int currentGlobalDefinitionIndex;
	
	/**
	 * Index over experiment variables.
	 */
	private int currentIndexVariables;
	
	/**
	 * Index over experiment parameter.
	 */
	private int currentIndexParameter;
	
	public PamiExperimentSetup(OutputTableDefinition tableDefinition, List<ParameterDefinition> definitions, ExperimentType variable, ExperimentType parameter) {
		this.tableDefinition = tableDefinition;
		this.globalDefinitions = definitions;
		this.targetVariables = variable;
		this.targetParameter = parameter;
		currentGlobalDefinitionIndex = 0;
		currentIndexParameter = 0;
		currentIndexVariables = 0;
		
		// Construct sets of parameters
		listParameters();
	}
	

	/**
	 * Remove all direct computations that have the following:
	 * 1) a larger neighborhood size than 3 (we are using central differences)
	 * 2) a cartan model which is not full
	 */
	public void removeDirectPermutations() {
		// Remove appropriate parameter definitions from globalDefinitions
		List<ParameterDefinition> valid = new LinkedList<ParameterDefinition>();
		for (ParameterDefinition def : globalDefinitions) {
			if (def.method == FittingMethod.Direct) {
				if (def.model == CartanModel.FULL && def.neighborhoodSize == 3)
					valid.add(def);
			}
			else
				valid.add(def);
		}
		globalDefinitions = valid;
		
		listParameters();
		System.out.println(TextToolset.box("Experiment list", '#'));
		for (ParameterDefinition definition : globalDefinitions)
			System.out.println(definition.toStringComputation());

	}

	
	public PamiExperimentSetup(PamiExperimentSetup other) {
		this(other.tableDefinition, other.globalDefinitions, other.targetVariables, other.targetParameter);
	}

	public void setTableDefinition(OutputTableDefinition definition) {
		this.tableDefinition = definition;
	}
	
	
	private void listParameters() {
		// Use hashsets to prevent duplicates
		HashSet<CartanModel> hmodels = new HashSet<CartanModel>();
		HashSet<FittingMethod> hmethods = new HashSet<FittingMethod>();
		HashSet<Integer> hneighborhoodSizes = new HashSet<Integer>();
		HashSet<IterativeFilter> hfilters = new HashSet<IterativeFilter>();
		
		for (ParameterDefinition definition : globalDefinitions) {
			hmodels.add(definition.model);
			hmethods.add(definition.method);
			hneighborhoodSizes.add(definition.neighborhoodSize);
			hfilters.add(definition.filter);
			
		}
		
		// Then fill in as lists
		models = new ArrayList<CartanModel>(hmodels);
		methods = new ArrayList<FittingMethod>(hmethods);
		neighborhoodSizes = new ArrayList<Integer>(hneighborhoodSizes);
		filters = new ArrayList<IterativeFilter>(hfilters);
		
		// Sort lists to make sure we follow the enum order 
		Collections.sort(models, new Comparator<CartanModel>() {
			@Override
			public int compare(CartanModel c1, CartanModel c2) {
				return c1.ordinal() - c2.ordinal();
			};
		});
		Collections.sort(methods, new Comparator<FittingMethod>() {
			@Override
			public int compare(FittingMethod f1, FittingMethod f2) {
				return f1.ordinal() - f2.ordinal();
			};
		});
		Collections.sort(neighborhoodSizes);
		Collections.sort(filters, new Comparator<IterativeFilter>() {
			@Override
			public int compare(IterativeFilter f1, IterativeFilter f2) {
				// Compare std * sqrt(numIterations);
				double v1 = f1.getStandardDeviation() * Math.sqrt(f1.getIterations());
				double v2 = f2.getStandardDeviation() * Math.sqrt(f2.getIterations());
				return MathToolset.roundInt(Math.signum(v1 - v2));
			};
		});

	}
	
	public OutputTableDefinition getTableDefinition() {
		return tableDefinition;
	}
	
	public ExperimentType getTargetVariable() {
		return targetVariables;
	}
	
	public ExperimentType getTargetParameter() {
		return targetParameter;
	}

	public List<CartanModel> getModels() {
		return models;
	}
	
	public List<FittingMethod> getFittingMethods() {
		return methods;
	}
	
	public List<Integer> getNeighborhoodSizes() {
		return neighborhoodSizes;
	}
	
	public List<IterativeFilter> getFilters() {
		return filters;
	}
	
	public int countCartanModels() {
		return models.size();
	}

	public int countNeighborhoodSizes() {
		return neighborhoodSizes.size();
	}

	public int countFittingMethods() {
		return methods.size();
	}

	public int countFilters() {
		return filters.size();
	}

	public ParameterDefinition getCurrentDefinition() {
		return new ParameterDefinition(currentDefinition);
	}
	
	/**
	 * 
	 * @return null if all experiments have been cycled through.
	 */
	public ParameterDefinition nextTarget() {
		
		// If we are fully done (gone through all variables and parameters),
		// return null;
		if (finished()) 
			return null;
		
		// If we have processed all the variables, we can increase the parameter by one
		// and rewind the variables.
		if (isFinished(targetVariables, currentIndexVariables)) {
			currentIndexVariables = 0;
			currentIndexParameter++;
		}

		// Fetch the updated definition
		assign(currentDefinition, targetParameter, currentIndexParameter);
		assign(currentDefinition, targetVariables, currentIndexVariables);
		
		// Increment the current variable
		currentIndexVariables++;

		// Return the definition
		return getCurrentDefinition();
	}
	
	/**
	 * Return the current parameter definition and increment the parameter index.
	 * @return the current parameter definition or null if all variables have been processed.
	 */
	public ParameterDefinition nextParameter() {
		// If we have processed all the variables, we are done.
		if (isFinished(targetParameter, currentIndexParameter)) {
			return null;
		}
		
		// Fetch the updated definition
		assign(currentDefinition, targetParameter, currentIndexParameter);

		// Increment the current variable
		currentIndexParameter++;
		
		// Return the definition
		return getCurrentDefinition();
	}
	
	/**
	 * Return the current variable and increment the variable index.
	 * @return the current parameter definition or null if all variables have been processed.
	 */
	public ParameterDefinition nextVariable() {
		// If we have processed all the variables, we are done.
		if (isFinished(targetVariables, currentIndexVariables)) {
			return null;
		}
		
		// Fetch the previous definition
		assign(currentDefinition, targetVariables, currentIndexVariables);

		// Increment the current variable
		currentIndexVariables++;
		
		// Return the definition
		return getCurrentDefinition();
	}
	
	private void assign(ParameterDefinition targetDefinition, ExperimentType type, int index) {
		
		// Update with new parameter
		switch (type) {
		case CartanModel:
			targetDefinition.model = models.get(index);
			break;
		case FittingMethod:
			targetDefinition.method = methods.get(index);
			break;
		case NeighborhoodSize:
			targetDefinition.neighborhoodSize = neighborhoodSizes.get(index);
			break;
		case Smoothing:
			targetDefinition.filter = filters.get(index);
			break;
		}
	}
	
	public boolean isTargetFinished() {
		return isFinished(targetParameter, currentIndexParameter) && isFinished(targetVariables, currentIndexVariables);
	}
	
	private boolean isFinished(ExperimentType type, int currentIndex) {
		
		switch (type) {
		case CartanModel:
			return currentIndex >= models.size();
		case FittingMethod:
			return currentIndex >= methods.size();
		case NeighborhoodSize:
			return currentIndex >= neighborhoodSizes.size();
		case Smoothing:
			return currentIndex >= filters.size();
		case RunOnly:
			return currentIndex >= 1;
		}
		
		return true;
	}
	
	public ParameterDefinition currentGlobalDefinition() {
		return globalDefinitions.get(currentGlobalDefinitionIndex);
	}
	
	public ParameterDefinition nextGobalDefinition() {
		if (!finished()) 
			return globalDefinitions.get(currentGlobalDefinitionIndex++);

		return null;
	}
	
	public void rewind() {
		currentGlobalDefinitionIndex = 0;
		currentIndexParameter = 0;
		currentIndexVariables = 0;
		currentDefinition = currentGlobalDefinition();
	}
	
	public void rewindVariables() {
		currentIndexVariables = 0;
	}

	public void rewindParameters() {
		currentIndexParameter = 0;
	}

	/**
	 * @return whether all definitions have been iterated over.
	 */
	public boolean finished() {
		return currentGlobalDefinitionIndex >= globalDefinitions.size();
	}

	public List<ParameterDefinition> getGlobalDefinitions() {
		return globalDefinitions;
	}
		
	/**
	 * Build a list of experiment definitions based on the selected experiment components.
	 * @param methods
	 * @param models
	 * @param filters
	 * @param neighborhoodSizes
	 * @return
	 */
	public static PamiExperimentSetup fromList(OutputTableDefinition tableDefinition, ExperimentType variable, ExperimentType parameter, List<FittingMethod> methods, List<CartanModel> models, List<IterativeFilter> filters, List<Integer> neighborhoodSizes) {
		List<ParameterDefinition> definitions = new ArrayList<ParameterDefinition>();
		
		for (FittingMethod method : methods)
			for (int neighborhoodSize : neighborhoodSizes)
				for (CartanModel model : models)
					for (IterativeFilter filter : filters)
						definitions.add(new ParameterDefinition(method, model, filter, neighborhoodSize));
		
		return new GenericExperiment(tableDefinition, definitions, variable, parameter);
	}

	public static PamiExperimentSetup fromList(ExperimentType variable, ExperimentType parameter, List<FittingMethod> methods, List<CartanModel> models, List<IterativeFilter> filters, List<Integer> neighborhoodSizes) {
		return fromList(null, variable, parameter, methods, models, filters, neighborhoodSizes);
	}
	public static PamiExperimentSetup fromList(List<FittingMethod> methods, List<CartanModel> models, List<IterativeFilter> filters, List<Integer> neighborhoodSizes) {
		return fromList(ExperimentType.RunOnly, ExperimentType.RunOnly, methods, models, filters, neighborhoodSizes); 
	}	

	/**
	 * Build all combinations of experiments.
	 * @return
	 */
	public static PamiExperimentSetup buildAll() {
		
		List<FittingMethod> methods = Arrays.asList(FittingMethod.Direct, FittingMethod.Optimized);
		List<CartanModel> models = Arrays.asList(CartanModel.FULL, CartanModel.HOMEOID, CartanModel.HELICOID, CartanModel.C123, CartanModel.CONSTANT);
		List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0), new IterativeFilter(5, 0.2), new IterativeFilter(10, 0.2), new IterativeFilter(5, 0.4), new IterativeFilter(10, 0.4));
		List<Integer> sizes = Arrays.asList(3, 5, 7);
		
		return fromList(methods, models, filters, sizes);
	}

	/**
	 * Build all combinations of experiments with zero smoothing.
	 * @return
	 */
	public static PamiExperimentSetup buildAllNoSmoothing() {
		
		List<FittingMethod> methods = Arrays.asList(FittingMethod.Direct, FittingMethod.Optimized);
		List<CartanModel> models = Arrays.asList(CartanModel.FULL, CartanModel.HOMEOID, CartanModel.HELICOID, CartanModel.C123, CartanModel.CONSTANT);
		List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));
		List<Integer> sizes = Arrays.asList(3, 5, 7);
		
		return fromList(methods, models, filters, sizes);
	}

	public static class OutputTableDefinition {
		public boolean transpose;
		public boolean documentWide;
		public String caption;
		public String label;
		public List<Parameter> selectedParameters;
		

		public OutputTableDefinition() {
			// Do nothing.
		}
		
		public OutputTableDefinition(String caption, List<Parameter> parameters) {
			this(caption, null, parameters);
		}
		public OutputTableDefinition(String caption, List<Parameter> parameters, boolean transposed) {
			this(caption, null, parameters, transposed);
		}

		public OutputTableDefinition(String caption, String label, List<Parameter> parameters) {
			this(caption, label, parameters, false);
		}
		
		public OutputTableDefinition(String caption, String label, List<Parameter> parameters, boolean transposed) {
			this.label = label;
			this.caption = caption;
			this.selectedParameters = parameters;
			this.transpose = transposed;
		}
	}

	public static class ParameterDefinition {
		public FittingMethod method;
		public CartanModel model;
		public IterativeFilter filter;
		public int neighborhoodSize;
		
		public ParameterDefinition(FittingMethod method, CartanModel model, IterativeFilter filter, int neighborhoodSize) {
			this.method = method;
			this.model = model;
			this.filter = filter;
			this.neighborhoodSize = neighborhoodSize;
		}

		public ParameterDefinition(ParameterDefinition other) {
			method = other.method;
			model = other.model;
			filter = other.filter;
			neighborhoodSize = other.neighborhoodSize;
		}

		@Override
		public String toString() {
			List<String> strings = new LinkedList<String>();
			for (ExperimentType type : ExperimentType.values())
				strings.add(toString(type));
			return TextToolset.join(strings, ",");
		}

		public String toStringComputation() {
			List<String> strings = new LinkedList<String>();
			strings.add(toString(ExperimentType.FittingMethod));
			strings.add(toString(ExperimentType.CartanModel));
			strings.add(toString(ExperimentType.NeighborhoodSize));
			strings.add(toString(ExperimentType.Smoothing));
			return TextToolset.join(strings, " | ");
		}

		public String toString(ExperimentType type) {
			// Update with new parameter
			switch (type) {
			case CartanModel:
				return model.toString().toLowerCase();
			case FittingMethod:
				return method.toString().toLowerCase();
			case NeighborhoodSize:
				return "n" + neighborhoodSize;
			case Smoothing:
				return filter.toStringShort();
			case RunOnly:
				return "result";
			}
			
			return "";
		}
		
		public String toStringMatlab(ExperimentType type) {
			// Update with new parameter
			switch (type) {
			case CartanModel:
				return model.toString().toLowerCase();
			case FittingMethod:
				return method.toString().toLowerCase();
			case NeighborhoodSize:
				return "$\\Omega_" + neighborhoodSize + "$";
			case Smoothing:
				return filter.toStringMatlab();
			case RunOnly:
				return "result";
			}
			
			return "";
		}

		public String toStringMatlabXLabel(ExperimentType type) {
			// Update with new parameter
			switch (type) {
			case CartanModel:
				return model.toStringLatex();
			case FittingMethod:
				return toStringMatlab(type);
			case NeighborhoodSize:
				return toStringMatlab(type);
			case Smoothing:
				return filter.toStringLatex();
			case RunOnly:
				return toStringMatlab(type);
			}
			
			return "";
		}

	}
	
	public static class NoMoreDefinitionException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	public int countVariables() {
		// Update with new parameter
		switch (targetVariables) {
		case CartanModel:
			return models.size();
		case FittingMethod:
			return methods.size();
		case NeighborhoodSize:
			return neighborhoodSizes.size();
		case Smoothing:
			return filters.size();
		case RunOnly:
			return 1;
		}
		return 0;
	}

//	public String getTargetVariableNameMatlabFormatted() {
//		String name = "";
//		
//		ParameterDefinition definition = getCurrentDefinition();
//		
//		switch (targetVariables) {
//		case CartanModel:
//			name = definition.model.toString().toLowerCase();
//			break;
//		case FittingMethod:
//			name = definition.method.toString().toLowerCase();
//			break;
//		case NeighborhoodSize:
//				name = "\\Omega = " + definition.neighborhoodSize + "^3";
//			break;
//		case Smoothing:
//				name = definition.filter.toStringShort();
//			break;
//		}
//		
//		return name;
//	}
//	
	public List<String> getTargetVariableNamesMatlabFormatted() {
		List<String> names = new ArrayList<String>();

		switch (targetVariables) {
		case CartanModel:
			for (CartanModel model : models)
				names.add(model.toString());
			break;
		case FittingMethod:
			for (FittingMethod method : methods)
				names.add(method.toString());
			break;
		case NeighborhoodSize:
			for (Integer size : neighborhoodSizes)
				names.add("\\Omega = " + size + "^3");
			break;
		case Smoothing:
			int i = 0;
			for (IterativeFilter filter : filters)
//				names.add(filter.toStringMatlab());
				names.add("\\sigma_" + (i++));
			break;
		}
		
		return names;
	}

	public int countParameters() {
		// Update with new parameter
		switch (targetParameter) {
		case CartanModel:
			return models.size();
		case FittingMethod:
			return methods.size();
		case NeighborhoodSize:
			return neighborhoodSizes.size();
		case Smoothing:
			return filters.size();
		}
		
		return 0;
	}

	@Override
	public String toString() {
		ParameterDefinition def = getCurrentDefinition();
		return "Method = " + def.method.toString() + " | " + "Model = " + def.model.toString() + " | " + "Neighborhood = " + def.neighborhoodSize + " | " + "Smoothing = " + def.filter.toStringShort();
	}

}
