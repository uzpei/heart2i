package app.pami2013.core.experiments;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.core.ClosedFormCartan;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import system.object.Pair;
import tools.TextToolset;
import voxel.VoxelBox.CuttingPlane;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.cartan.CartanParameterNode;

public class PamiMethodComparator {

	/**
	 * Test closed form vs direct vs optimized for a selected heart and voxel subset.
	 * @param args
	 */
	public static void main(String[] args) {
		
		boolean computeDirect = false;
		boolean computeOptimized = true;
		boolean computeClosed = false;
		
		boolean excludeBoundary = false;
		boolean excludeIslands = true;
		
		HashMap<Parameter, double[]> bounds = Parameter.getCardiacBounds(Species.Rat); 
		
//		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat07052008/registered/");	
//		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/sample2/rat07052008/registered/");	
		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/sampleSubset/");	
		Heart heart = new Heart(def);

//		HeartManager manager = new HeartManager();
//		Heart heart = manager.getCurrent();
		
		heart.prepareDataIfNecessary();
		heart.getVoxelBox().cut(CuttingPlane.FIT);
		CartanModel model = CartanModel.FULL;
		
		int nsize = 3;
		CartanParameterNode meanOptimized = new CartanParameterNode();
		CartanParameterNode meanDirect = new CartanParameterNode();
		CartanParameterNode meanClosed = new CartanParameterNode();

		int voxelCount = heart.getVoxelBox().countVoxels();
		System.out.println("Number of voxels to process = " + voxelCount);

		FittingExperimentResult resultClosed = null, resultDirect = null, resultOptimized = null;
		
		// Optimized
		if (computeOptimized) {
			System.out.println("Computing optimized...");
			FittingData dataOptimized = new FittingData(new CartanFittingParameter(bounds, model, FittingMethod.Optimized, nsize), heart.getFrameField(), heart.getVoxelBox());

			dataOptimized.getFittingParameter().useDirectSeeds = true;
			
			resultOptimized = new FittingExperimentResult(dataOptimized);
			
			CartanFitter fitter = new CartanFitter(OptimizerType.SIMPLEX);
			fitter.setVerboseLevel(3);
			fitter.launchFit(resultOptimized);
			waitForCompletion(resultOptimized);
			PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(resultOptimized.getFittingData());
			PamiExperimentRunner.validate(resultOptimized, excludeBoundary, excludeIslands, bounds);
			for (CartanParameterNode node : resultOptimized.getResultNodes()) 
				node.setError(fittingError.evaluate(node, resultOptimized.getFittingData().getNeighborhood(node.getPosition())));
			meanOptimized = CartanParameterNode.computeMean(resultOptimized.getResultNodes(), dataOptimized.getFittingParameter().getCartanModel());
			System.out.println("Number of voxels validated = " + resultOptimized.getResultNodes().size() + "/" + voxelCount);
			System.out.println("Running time = " + resultOptimized.getElapsedTime());
		}
		
		// Direct
		if (computeDirect) {
			System.out.println("Computing direct...");
			FittingData dataDirect = new FittingData(new CartanFittingParameter(bounds, model, FittingMethod.Direct, nsize), heart.getFrameField(), heart.getVoxelBox());
			resultDirect = new FittingExperimentResult(dataDirect);
			
			// Compute the connection forms
			DifferentialOneForm.compute(resultDirect, heart.getMask());
			waitForCompletion(resultDirect);
			System.out.println("Number of voxels used = " + resultDirect.getResultNodes().size());

			PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(resultDirect.getFittingData());
			PamiExperimentRunner.validate(resultDirect, excludeBoundary, excludeIslands, bounds);
			
			for (CartanParameterNode node : resultDirect.getResultNodes()) 
				node.setError(fittingError.evaluate(node, resultDirect.getFittingData().getNeighborhood(node.getPosition())));
			meanDirect = CartanParameterNode.computeMean(resultDirect.getResultNodes(), dataDirect.getFittingParameter().getCartanModel());
			System.out.println("Number of voxels validated = " + resultDirect.getResultNodes().size() + "/" + voxelCount);
			System.out.println("Running time = " + resultDirect.getElapsedTime());
		}
		
		// Closed-form
		if (computeClosed) {
			System.out.println("Computing closed-form...");
			FittingData dataClosed = new FittingData(new CartanFittingParameter(bounds, model, FittingMethod.ClosedForm, nsize), heart.getFrameField(), heart.getVoxelBox());
			resultClosed = new FittingExperimentResult(dataClosed);
			ClosedFormCartan.compute(resultClosed);
			waitForCompletion(resultClosed);
			PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(resultClosed.getFittingData());
			PamiExperimentRunner.validate(resultClosed, excludeBoundary, excludeIslands, bounds);
			for (CartanParameterNode node : resultClosed.getResultNodes()) 
				node.setError(fittingError.evaluate(node, resultClosed.getFittingData().getNeighborhood(node.getPosition())));
			meanClosed = CartanParameterNode.computeMean(resultClosed.getResultNodes(), dataClosed.getFittingParameter().getCartanModel());
			System.out.println("Number of voxels validated = " + resultClosed.getResultNodes().size());
			System.out.println("Running time = " + resultClosed.getElapsedTime());
		}

		if (computeClosed)
			System.out.println("   Closed: " + meanClosed.toString());
		
		if (computeDirect)
			System.out.println("   Direct: " + meanDirect.toString());
		
		if (computeOptimized)
			System.out.println("Optimized: " + meanOptimized.toString());
		
		/*
		 *  2) Re-compute errors to make sure all computation methods end up with the same error measure
		 *  i.e. the error should be computed with respect to the original (unsmoothed, unprocessed) data.
		 */

//		System.out.println(resultDirect.toString());
//		System.out.println(resultOptimized.toString());
//		System.out.println(resultClosed.toString());

		// Construct Matlab plot string
		
		if (computeDirect)
			System.out.println("c1=["+ TextToolset.join(meanDirect.getParameterError(), ",") + ", " + resultDirect.getElapsedTime() + "]");
		if (computeOptimized)
			System.out.println("c2=["+ TextToolset.join(meanOptimized.getParameterError(), ",") + ", " + resultOptimized.getElapsedTime() + "]");
		if (computeClosed)
			System.out.println("c3=["+ TextToolset.join(meanClosed.getParameterError(), ",") + ", " + resultClosed.getElapsedTime() + "]");

//		hold on
//		plot(c1, '-r');
//		plot(c2, '-g');
//		plot(c3, '-b');
//		hold off
//		set(gca,'XTickLabel',{'c121','c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233'})
//		legend({'direct', 'optimized', 'closed-form'});
	}
	
	private static void waitForCompletion(FittingExperimentResult experiment) {
		while (experiment.inProgress())
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
