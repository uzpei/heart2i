package app.pami2013.core.experiments;

import java.util.List;
import java.util.Random;

import javax.vecmath.Matrix3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import diffgeom.fitting.experiment.CartanFittingParameter;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import voxel.FramedVoxel;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.Parameter;

public class TestConnectionEstimate {

	public static void main(String[] args) {
		
//		CoordinateFrame omega = new CoordinateFrameSample(coordinateFrame)
		List<Point3i> omega = new CartanFittingParameter(null, 3).getNeighborhood();
		
		// Define dimension and center of expansion
		int n = 5;
		int[] dims = new int[] { n, n, n };
		Point3i x = new Point3i(MathToolset.roundInt(n/2d), MathToolset.roundInt(n/2d), MathToolset.roundInt(n/2d));
		
		// For each connection form
		Random random = new Random();
		
		for (Parameter connection : CartanParameter.CartanModel.FULL.getAvailableConnections()) {
			// Prepare extrapolation
			VoxelFrameField F = new VoxelFrameField(dims);
			CartanParameter parameter = new CartanParameter();
			parameter.set(connection, random.nextDouble());
			Vector3d f1p = new Vector3d();
			Vector3d f2p = new Vector3d();
			Vector3d f3p = new Vector3d();
	
			// Extrapolate neighborhood
			for (Point3i vi : omega) {
				// Direction of expansion
				Vector3d v = new Vector3d(vi.x, vi.y, vi.z);
				
				// Point of expansion
				Point3i p = new Point3i();
				p.add(x, vi);
				
				CoordinateFrame frame = new CoordinateFrame();
				Vector3d f1 = frame.getAxis(FrameAxis.X);
				Vector3d f2 = frame.getAxis(FrameAxis.Y);
				Vector3d f3 = frame.getAxis(FrameAxis.Z);
				Vector3d df1 = dfi(frame, 1, parameter, v);
				Vector3d df2 = dfi(frame, 2, parameter, v);
				Vector3d df3 = dfi(frame, 3, parameter, v);
				
				f1p.add(f1, df1);
				f2p.add(f2, df2);
				f3p.add(f3, df3);
				
//				System.out.println(df1);
				
				F.set(p, new CoordinateFrame(f1p, f2p, f3p, MathToolset.tuple3iToPoint3d(p)));
				Vector3d[] Fp = F.get(p);
				CoordinateFrame f = new CoordinateFrame(Fp[0], Fp[1], Fp[2], MathToolset.tuple3iToPoint3d(p));
				System.out.println(f.toString());
			}
			
			// Estimate back connection forms
			// TODO: estimate both locally (by computing only at p) and globally (by computing whole volume jacobians and contractions)
//			CartanParameter estimated = DifferentialOneForm.computeOneFormParameter(F, x);
			CartanParameter estimated = DifferentialOneForm.computeDirect(F, x);
			
			// Compare
			System.out.println(parameter.toString());
			System.out.println(estimated.toString());
		}
	}
	
	public static Vector3d dfi(CoordinateFrame frame, int i, CartanParameter c, Vector3d v) {
		// Assemble Ci
		Matrix3d Ci = Ci(i, c);
		
		// Assemble F and F^T
		Matrix3d F = new Matrix3d();
		double[] f1 = new double[3];
		double[] f2 = new double[3];
		double[] f3 = new double[3];
		frame.getAxis(FrameAxis.X).get(f1);
		frame.getAxis(FrameAxis.Y).get(f2);
		frame.getAxis(FrameAxis.Z).get(f3);
		for (int k = 0; k < 3; k++) {
			F.setElement(k, 0, f1[k]);
			F.setElement(k, 1, f2[k]);
			F.setElement(k, 2, f3[k]);
		}
		
		Matrix3d FT = new Matrix3d();
		FT.transpose(F);
		
		// Compute dfi = F Ci FT v
		Matrix3d FCi = new Matrix3d();
//		F.mul(Ci, FCi);
		FCi.mul(F, Ci);
		Matrix3d FCiFT = new Matrix3d();
		FCiFT.mul(FCi, FT);
		
		Vector3d dfi = new Vector3d(v);
		FCiFT.transform(dfi);
		
		return dfi;
	}

	public static Matrix3d Ci(int i, CartanParameter c) {
		Matrix3d Ci = new Matrix3d();
		for (int k = 0; k < 3; k++) {
			for (int l = 0; l < 3; l++) {
//				double eps = epsilon(i, k + 1);
				Ci.setElement(k, l, c.get(i, k + 1, l + 1));
			}
		}
		return Ci;
	}
	
	public static double epsilon(int i, int j) {
		return i == j ? 0 : Math.signum(j - i);
	}
	
	
}
