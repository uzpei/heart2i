package app.pami2013.core.experiments;

import heart.Heart;
import heart.HeartDefinition;
import heart.Heart.Species;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import app.pami2013.IterativeFilter;
import app.pami2013.core.setup.computation.AllComputationSetup;

public class PamiSmoothingProcessor {
	public static List<HeartDefinition> hearts = new LinkedList<HeartDefinition>();
	{
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/sample2/rat07052008/registered/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/sample2/rat17122007/mat/"));
//		hearts.add(new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/frame_smooth/"));
		
//		hearts.add(new HeartDefinition("ratsample", Species.Rat, "./data/heart/rat/sampleSubset/"));
		hearts.add(new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/"));
		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat17122007/registered_clean/"));
		hearts.add(new HeartDefinition("rat21012008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat21012008/registered_clean/"));
		hearts.add(new HeartDefinition("rat22012008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat22012008/registered_clean/"));
		hearts.add(new HeartDefinition("rat24012008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat24012008/registered_clean/"));
		hearts.add(new HeartDefinition("rat24022008_17", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat24022008_17/registered_clean/"));
		hearts.add(new HeartDefinition("rat27022008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat27022008/registered_clean/"));
		hearts.add(new HeartDefinition("rat28012008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat28012008/registered_clean/"));
		
		// MBP paths
		// batch
//		hearts.add(new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/rat07052008/registered/"));
//		hearts.add(new HeartDefinition("rat17122007", Species.Rat, "./data/heart/rat/rat17122007/registered/"));
//		hearts.add(new HeartDefinition("rat21012008", Species.Rat, "./data/heart/rat/rat21012008/registered/"));
//		hearts.add(new HeartDefinition("rat22012008", Species.Rat, "./data/heart/rat/rat22012008/registered/"));
//		hearts.add(new HeartDefinition("rat24012008", Species.Rat, "./data/heart/rat/rat24012008/registered/"));
//		hearts.add(new HeartDefinition("rat24022008_17", Species.Rat, "./data/heart/rat/rat24022008_17/registered/"));
//		hearts.add(new HeartDefinition("rat27022008", Species.Rat, "./data/heart/rat/rat27022008/registered/"));
//		hearts.add(new HeartDefinition("rat28012008", Species.Rat, "./data/heart/rat/rat28012008/registered/"));
	}
	
	public static void main(String[] ags) {
		
		new PamiSmoothingProcessor();
	}
	
	public PamiSmoothingProcessor() {
		List<IterativeFilter> filters = AllComputationSetup.filters;
		
		for (IterativeFilter filter : filters) {
			for (HeartDefinition def : hearts) {
				Heart heart = new Heart(def);
				heart.setSmoothing(filter);
				heart.prepareDataIfNecessary();
			}
		}
	}
}
