package app.pami2013.core.experiments;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import system.interfaces.TextTransfer;
import system.object.Pair;
import tools.TextToolset;
import tools.frame.OrientationFrame.FrameAxis;
import tools.loader.LineWriter;
import volume.IntensityVolume;
import voxel.VoxelBox.CuttingPlane;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanFieldVisualizer;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.setup.computation.AllComputationSetup;
import app.pami2013.core.setup.computation.histogram.OptimizedSubjectsHistograms;
import app.pami2013.core.setup.computation.histogram.OptimizedSubjectsHistogramsEnveloped;
import app.pami2013.core.tools.PamiOutputManager;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.core.ClosedFormCartan;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.FittingExperimentResult;
import extension.matlab.MatlabIO;
import extension.matlab.MatlabScript;

public class PamiExperimentRunner {

	public static final double ZcutPercent = 1;
	public static final CuttingPlane cuttingPlane = CuttingPlane.FIT;
//	public static final double ZcutPercent = 1;
	
	public static List<HeartDefinition> hearts = new LinkedList<HeartDefinition>();
	
	public static void main(String[] args) {
		String dir;
		if (args.length > 0) {
			dir = args[0];
		}
		else {
//			dir = "/usr/local/data/shape/heart/pami2013/";
			dir = "./data/heart/rat/atlas_FIMH2013/";
		}		
		
//		hearts.add(new HeartDefinition("ratsample", Species.Rat, "./data/heart/rat/sampleSubset/"));
		hearts.add(new HeartDefinition("rat07052008", Species.Rat, dir + "rat07052008/registered_clean/"));
		hearts.add(new HeartDefinition("rat17122007", Species.Rat, dir + "rat17122007/registered_clean/"));
		hearts.add(new HeartDefinition("rat21012008", Species.Rat, dir + "rat21012008/registered_clean/"));
		hearts.add(new HeartDefinition("rat22012008", Species.Rat, dir + "rat22012008/registered_clean/"));
		hearts.add(new HeartDefinition("rat24012008", Species.Rat, dir + "rat24012008/registered_clean/"));
		hearts.add(new HeartDefinition("rat24022008_17", Species.Rat, dir + "rat24022008_17/registered_clean/"));
		hearts.add(new HeartDefinition("rat27022008", Species.Rat, dir + "rat27022008/registered_clean/"));
		hearts.add(new HeartDefinition("rat28012008", Species.Rat, dir + "rat28012008/registered_clean/"));

//		new PamiExperimentRunner(true);
		new PamiExperimentRunner(false);
	}

	/**
	 * TODO: build list of experiment subclasses for PAMI
	 * TODO: rename and move plot code somewhere else
	 * @param computeAllExperiments
	 */
	public PamiExperimentRunner(boolean computeAllExperiments) {
		/*
		 * Define parameters and experiment output path.
		 */
		String outputPath = "./data/PAMI/";
		MatlabIO.verbose = false;
		MatlabIO.quiet = false;
		
		CartanFittingParameter.DEFAULT_THRESHOLD = 0;
		CartanFittingParameter.DEFAULT_DELTA = 0;

		/*
		 * Construct list of experiments 
		 */
		List<PamiExperimentSetup> setups = new LinkedList<PamiExperimentSetup>();
		
		setups.add(new OptimizedSubjectsHistogramsEnveloped());
//		setups.add(new OptimizedSubjectsHistograms());
//		setups.add(new OptimizedVsDirectHistograms());
//		setups.add(new ModelVsNeighborhoodSizePlots());
//		setups.add(new ErrorsVsModelPerNeighborhoodHistograms());
//		setups.add(new OptimizedSmoothingN3Histograms());
		
		// Replace "Method" by fitingmethod
//		setups.add(new FittingMethodVsFilteringPlots()); 
//		setups.add(new AllConnectionsPlot());
//		setups.add(new TransmuralConnectionHistograms());
		
//		setups.add(new OptimizedVsNeighborhoodTable());
//		setups.add(new ErrorVsModelPerNeighborhoodTable());
//		setups.add(new FilteringVsMethodTable());

		/*
		 * Compute all measurements
		 * Make sure the "execute" and "combine" flags are set to true 
		 * (Comment this if already computed)
		 */
		if (computeAllExperiments) {
			setups.clear();
			setups.add(new AllComputationSetup());
//			setups.add(new OptimizedComputationSetup());
		}
		
		/*
		 * Launch experiments
		 */
		List<FittingExperiment> experiments = new LinkedList<FittingExperiment>();
		for (PamiExperimentSetup setup : setups) {
			experiments.add(launch(hearts, setup, outputPath));
		}
		
		/*
		 * Process experiments output
		 */
		// Helper methods:
//		scripts.addAll(PamiOutputManager.c(experiment));
//		scripts.addAll(PamiOutputManager.matlabPerSpeciesHistograms(experiment));
//		scripts.addAll(PamiOutputManager.matlabHeartHistogramSmoothing(experiment, hearts.get(0), ViewAngle.Z, true));
//		scripts.addAll(PamiOutputManager.multiplot(experiment, Species.Rat, ViewAngle.Z, new Dimension(1,1), true));
		List<MatlabScript> scripts = new LinkedList<MatlabScript>();
		TextTransfer.setClipboardContents("");
		for (FittingExperiment experiment : experiments) {
			PamiExperimentSetup setup = experiment.getExperimentSetup();
			List<MatlabScript> output = setup.output(experiment);
			if (output != null)
				scripts.addAll(output);
		}
		
		/*
		 * Execute output
		 */
		if (scripts.size() > 0)
			PamiOutputManager.runScripts(experiments.get(0).getOutputPath(), scripts);			

	}
	
	public static FittingExperiment launch(List<HeartDefinition> hearts, PamiExperimentSetup setup, String outputPath) {
		/*
		 * Create the experiment
		 */
		FittingExperiment experiment = new FittingExperiment(hearts, setup, cuttingPlane, outputPath);

		if (setup.executeExperiments) {
			// Run the experiment
			// (Comment this once they have all been computed and then select specific ones for output).
			PamiExperimentRunner.execute(experiment);
		}
		
		if (setup.executeExperiments || setup.combineExperiments) {
			// Combine the results
			PamiOutputManager.combine(experiment, false, ZcutPercent);
		}

		return experiment;
	}

	public static void execute(FittingExperiment experiment) {

		// Fetch species/list of hearts pairs.
		List<Pair<Species, List<Heart>>> allSpecies = experiment.getSubjectsPerSpecies();

		experiment.reset();
		
		while (experiment.nextSetup()) {
			String header = "Experiment type: embedding = [" + experiment.getFittingParameter().getCartanModel() + "] and estimation method = [" + experiment.getFittingParameter().getFittingMethod() + "] and smoothing = [" + experiment.getSmoothingFilter().toStringShort() + "]";
			System.out.println(TextToolset.box(header.toUpperCase(), '#'));
			
			process(experiment, allSpecies);
		}
	}
	
	/**
	 * Prepare this heart for the fitting experiment.
	 * 1) Smoothing is applied based on <code>filter</code>.
	 * 2) A VoxelBox cutting plane is applied.
	 * 3) The upper portion of the heart cutoff based on <code>cutPercent</code>
	 * @param experiment
	 * @param heart
	 */
	private static void prepareHeart(FittingExperiment experiment, Heart heart, IterativeFilter filter) {
		// Need to set this right away as smoothing is performed as heart data is loaded
		heart.setSmoothing(filter);
		
		// Load data
		heart.reloadData();

		// Apply cutting plane
		heart.getVoxelBox().cut(experiment.getCuttingPlane());
		
		// Apply Z cutoff
		heart.getVoxelBox().sliceAxis(FrameAxis.Z, ZcutPercent);

		// Shouldn't need to do this
		//		heart.getVoxelBox().computeVoxelListUpdate();
	}
	
	private static void process(FittingExperiment experiment, List<Pair<Species, List<Heart>>> allSpecies) {
		// Process one species at a time
		for (Pair<Species, List<Heart>> heartsPerSpecies : allSpecies) {

			// This is the species we are currently processing
			Species currentSpecies = heartsPerSpecies.getLeft();

			System.out.println("Processing species [" + currentSpecies + "]");
			
			// Fetch list of hearts for this species
			List<Heart> hearts = heartsPerSpecies.getRight();
			
			// Process each separately
			int subjectIndex = 0;
			for (Heart heart : hearts) {
				subjectIndex++;
				
				System.out.println("\n"+ TextToolset.box("Processing " + heart.getID() + " (" + subjectIndex + "/" +hearts.size() + ")" + " | " + experiment.toStringCurrent(), '-'));
				
				// Test whether this experiment was already computed
				if (isAlreadyComputed(heart, experiment)) {
					System.out.println("Experiment already exists. Skipping.");
					continue;
				}

				// Prepare and load heart data
				prepareHeart(experiment, heart, experiment.getSmoothingFilter());
				
				// Run experiment
				runExperiment(experiment, heart);
				FittingExperimentResult result = experiment.getResult();

				// Wait for completion
				System.out.println("Waiting for completion...");
				waitForCompletion(result);

				// Apply post-processing (node cleanup, etc.);
				System.out.println("Applying post-processing...");
				applyPostProcessing(experiment, heart, result);

				// Output results
				System.out.println("Saving results...");
				saveResults(heart, experiment);	
				
				// Free space
				System.out.println("Cleaning up...");
				heart.destroy();
			}
			
			System.out.println("Done.");
		}
	}
	
	/**
	 * Validate results based on theoretical bounds for the target neighborhood size and error:
	 *  |cijk(Omega_n)| <= 3 * |v|_1, v in Omega_n <= 3 * n
	 *  |epsilon_i| < 45 degrees
	 * @param result
	 * @return validated results
	 */
	public static FittingExperimentResult validate(FittingExperimentResult result) {
		boolean excludeBoundary = false;
		boolean excludeIslands = true;
		
		HashMap<Parameter, double[]> bounds = getTheoreticalBounds(result.getFittingData().getFittingParameter());
		
		return validate(result, excludeBoundary, excludeIslands, bounds);
	}
	
	/**
	 * Validate a list of result nodes based on selected node filters.
	 * @param result
	 * @param excludeBoundary whether nodes sitting at the boundary should be excluded
	 * @param excludeIslands whether nodes having no neighbors should be excluded
	 * @return
	 */
	public static FittingExperimentResult validate(FittingExperimentResult result, boolean excludeBoundary, boolean excludeIslands, HashMap<Parameter, double[]> thresholds) {
		List<CartanParameterNode> valid = getValidated(result, excludeBoundary, excludeIslands, thresholds);
				
		// Assign new list of nodes
		result.getResultNodes().clear();
		result.setResults(valid, result.getElapsedTime());

		int finalCount = result.getResultNodes().size();
//		System.out.println("Validation preserved " + finalCount + " out of " + initialCount + " nodes.");
		
		return result;
	}
	
	public static List<CartanParameterNode> getValidated(FittingExperimentResult result, boolean excludeBoundary, boolean excludeIslands, HashMap<Parameter, double[]> thresholds) {
		List<CartanParameterNode> valid = new LinkedList<CartanParameterNode>();
		int fullNeighborhoodSize = result.getFittingData().getFittingParameter().getNeighborhood().size();

		if (result.getResultNodes() == null)
			return null;
		
		for (CartanParameterNode node : result.getResultNodes()) {
			
			int neighborCount = result.getFittingData().getNeighborhood(node.getPosition()).size();
			
			// If this node is garbage
			if (node.isGarbage())
				continue;
			
			// If this node is a boundary
 			if (excludeBoundary && neighborCount != fullNeighborhoodSize)
				continue;
 			
 			// If this node is an island
			if (excludeIslands && neighborCount == 0)
				continue;
			
			// If this node has a NaN number
			if (node.isNan() || Double.isNaN(node.getErrorSumDegrees()))
				continue;
			
			if (thresholds != null) {
				boolean thresholded = false;
				for (Parameter parameter : Parameter.values()) {
					double[] bound = thresholds.get(parameter);
					if (node.get(parameter) <= bound[0] || node.get(parameter) >= bound[1]) {
//					if (node.get(parameter) < bound[0] || node.get(parameter) > bound[1]) {
						thresholded = true;
//						System.out.println("Thresholding bound: " + node.get(parameter) + " vs " + Arrays.toString(bound));
						break;
					}
				}
				
				if (thresholded)
					continue;
			}
			
			valid.add(node);
		}
		
		return valid;
	}
	
	private static void applyPostProcessing(FittingExperiment experiment, Heart heart, FittingExperimentResult result) {
		
		/*
		 *  1) Remove island nodes (voxels with no neighbors).
		 */
		int count = result.getResultNodes().size();
		if (experiment.getExperimentSetup().validate)
			validate(result);
		System.out.println(result.getResultNodes().size() + " validated out of " + count);
		/*
		 *  2) Re-compute errors to make sure all computation methods end up with the same error measure
		 *  i.e. the error should be computed with respect to the original (unsmoothed, unprocessed) data.
		 */

		// Need to reload the original heart data if smoothing was used
		if (experiment.getSmoothingFilter().getIterations() > 0) {
			System.out.println("Reloading heart in order to compute error from intact data...");
			prepareHeart(experiment, heart, new IterativeFilter(0, 0));
		}
		
		FittingData fittingData = new FittingData(result.getFittingParameter(), heart.getFrameField(), heart.getVoxelBox());
    	PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(fittingData);
    	
		/*
		 * 3) Evaluate the fitting error
		 */
		// Recompute error in Omega_n
		result.getFittingData().setNeighborhoodShape(NeighborhoodShape.Isotropic);
		for (CartanParameterNode node : result.getResultNodes()) {
			node.setError(fittingError.evaluate(node, result.getFittingData()
					.getNeighborhood(node.getPosition())));
		}		
	}
	
	private static void saveResults(Heart heart, FittingExperiment experiment) {
		System.out.println(TextToolset.box("Printing experiment results...", '~'));
		System.out.println(experiment.getResult().toString());

		// Save results
		try {
			export(heart, experiment);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private static void waitForCompletion(FittingExperimentResult experiment) {
		while (experiment.inProgress())
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	private static void runExperiment(FittingExperiment experiment, Heart heart) {

		// Construct a heart fitting parameter
		final FittingExperimentResult result = new FittingExperimentResult(new FittingData(experiment.getFittingParameter(), heart.getFrameField(), heart.getVoxelBox()));
		CartanFittingParameter fittingParameter = experiment.getFittingParameter();
		
		if (fittingParameter.getFittingMethod() == FittingMethod.ClosedForm) {
			ClosedFormCartan.compute(result);
		}
		else if (fittingParameter.getFittingMethod() == FittingMethod.Optimized) {
			new CartanFitter().launchFit(result);
		}
		else if (fittingParameter.getFittingMethod() == FittingMethod.Direct) {
			DifferentialOneForm.compute(result, heart.getMask());
		}
		
		experiment.setResult(result);
	}

	public static boolean isAlreadyComputed(Heart heart, FittingExperiment experiment) {
		String outputPath = experiment.getOutputPath(heart.getDefinition());
		
//		System.out.println("---> Verifying is experiment already exists in " + outputPath + "...");
		
		// Build list of files to test agains
		List<File> files = new LinkedList<File>();

		// Output folder
		files.add(new File(outputPath));

		// Header file
		files.add(new File(outputPath + PamiOutputManager.HEADER_NAME));

		// Output volumes (original and compactified)
		for (Parameter parameter : experiment.getFittingParameter().getCartanModel()) {
			files.add(new File(experiment.getOutputPathVolume(heart.getDefinition(), parameter)));
			files.add(new File(experiment.getOutputPathVolumeCompact(heart.getDefinition(), parameter)));
		}
		
		// Test all
		boolean computed = true;
		for (File file : files) {
			if (!file.exists()) {
				System.out.println("---> Experiment will have to be computed since the following path is inexistent:");
				System.out.println("[" + file.getAbsolutePath() + "]\n");
				computed = false;
				break;
			}
		}
		
		return computed;
	}
	
	/**
	 * Export the fitting results (if any are available) to a folder.
	 * @param directory
	 * @throws IOException 
	 */
	public static void export(Heart heart, FittingExperiment experiment) throws IOException {
	
		String outputPath = experiment.getOutputPath(heart.getDefinition());
		
		if (FittingExperimentResult.verboseLevel > 0)
			System.out.println("---> Exporting fitting data to " + outputPath);
		
		// Create directory if necessary
		if (!new File(outputPath).exists())
			new File(outputPath).mkdir();
		
		// Store header file
		String hfile = outputPath + PamiOutputManager.HEADER_NAME;
		if (FittingExperimentResult.verboseLevel > 1)
			System.out.println("Saving " + hfile);

		String out = "";

		// Add experiment information
		out += experiment.toString() + "\n";
		
		// Add result information
		out += experiment.getResult().toString() + "\n";
		
		// Add heart fitting information
		out += experiment.getResult().getFittingParameter().toString() + "\n";
		
		// Print header
		out +=  "\n";
		String ph = "Beginning of user header";
		out += TextToolset.box(ph, '-') + "\n";
		
		out += experiment.getExperimentHeader(heart) + "\n";
		
		LineWriter.write(hfile, out);

		// Construct output volumes
		List<Pair<IntensityVolume, Parameter>> volumes = constructOutputVolumes(heart, experiment);
		
		// Store fitting volumes as MATLAB files
		if (FittingExperimentResult.verboseLevel > 1)
			System.out.println("Exporting volumes...");
		
		boolean verbose = false;
		for (Pair<IntensityVolume, Parameter> volumePair : volumes) {
			IntensityVolume volume = volumePair.getLeft();
			Parameter parameter = volumePair.getRight();
			
			MatlabIO.save(experiment.getOutputPathVolume(heart.getDefinition(), parameter), volume, verbose);
			
			// Also output compactified volume
			double[] volumeFlat = volume.flattenCompact(heart.getMask());
			MatlabIO.save(experiment.getOutputPathVolumeCompact(heart.getDefinition(), parameter), volumeFlat, false);
		}
	}
	
	private static List<Pair<IntensityVolume, Parameter>> constructOutputVolumes(Heart heart, FittingExperiment experiment) {
		List<Pair<IntensityVolume, Parameter>> volumes = new ArrayList<Pair<IntensityVolume, Parameter>>();

		// This is the model we are using
		CartanModel model = experiment.getFittingParameter().getCartanModel();

		// Reconstruct the 3D volumes from lists of ndoes
		if (FittingExperimentResult.verboseLevel > 1)
			System.out.println("Reconstructing volumes...");
		IntensityVolume[] v = CartanFieldVisualizer.nodesToVolume(experiment.getResult().getResultNodes(), model, experiment.getResult().getFittingData().getVoxelBox().getDimension());

		// Add cartan volumes and errors
		int parameterIndex = 0;
		for (Parameter parameter : model) {
			volumes.add(new Pair<IntensityVolume, Parameter>(v[parameterIndex], parameter));
			parameterIndex++;
		}
		
		return volumes;
	}
	
	
	public static double[] getVolumeLimit(Parameter parameter, Species species) {
		HashMap<Parameter, double[]> bounds = getCardiacBounds(species, 1.0);
		return bounds.get(parameter);
	}
	

	public static HashMap<Parameter, double[]> getCardiacBounds(Species s) {
		return getCardiacBounds(s, 1);
	}

	/**
	 * TODO: make sure this is right. Add option to report in degrees as well?
	 * @param s
	 * @param scale
	 * @return
	 */
	public static HashMap<Parameter, double[]> getCardiacBounds(Species s, double scale) {
		HashMap<Parameter, double[]> bounds = new HashMap<CartanParameter.Parameter, double[]>();
		
		if (s == Species.Rat) {
			bounds.put(Parameter.C121, new double[] { -0.2, 0.2 });
			bounds.put(Parameter.C122, new double[] { -0.2, 0.2 });
			bounds.put(Parameter.C123, new double[] { -1.2, 0.5 });

			bounds.put(Parameter.C131, new double[] { -0.2, 0.2 });
			bounds.put(Parameter.C132, new double[] { -0.2, 0.2 });
			bounds.put(Parameter.C133, new double[] { -0.2, 0.2 });

			bounds.put(Parameter.C231, new double[] { -0.2, 0.2 });
			bounds.put(Parameter.C232, new double[] { -0.2, 0.2 });
			bounds.put(Parameter.C233, new double[] { -0.2, 0.2 });

//			bounds.put(Parameter.error1, new double[] { 0, 0.05 });
//			bounds.put(Parameter.error2, new double[] { 0, 0.05 });
//			bounds.put(Parameter.error3, new double[] { 0, 0.02 });
			bounds.put(Parameter.error1, new double[] { 0, 0.02 });
			bounds.put(Parameter.error2, new double[] { 0, 0.02 });
			bounds.put(Parameter.error3, new double[] { 0, 0.02 });
			
			for (double[] bound : bounds.values()) {
				bound[0] *= scale;
				bound[1] *= scale;
			}
		}
		else {
			throw new NotImplementedException();
		}
		
		return bounds;
	}
	
	/**
	 Compute theoretical bounds for the target fitting data
	 *  |cijk(Omega_n)| <= 3 * |v|_1, v in Omega_n <= 3 * n
	 *  |epsilon_i| < 45 degrees
	 * @param fittingData
	 * @return a hashmap from parameter (connection or error) to bounds
	 */
	public static HashMap<Parameter, double[]> getTheoreticalBounds(CartanFittingParameter fittingParameter) {
		/*
		 *  Compute theoretical bounds: 
		 *  |cijk(Omega_n)| <= 3 * |v|_1, v in Omega_n <= 3 * n
		 *  |epsilon_i| < 45 degrees
		 */
		double bound = 3 * (fittingParameter.getNeighborhoodSize() - 1) / 2d;
		double boundError = Math.PI / 4d;
		
		HashMap<Parameter, double[]> bounds = new HashMap<CartanParameter.Parameter, double[]>();
		
		for (Parameter parameter : CartanModel.FULL.getAvailableConnections())
			bounds.put(parameter, new double[] { -bound, bound });

		for (Parameter parameter : Arrays.asList(Parameter.error1, Parameter.error2, Parameter.error3))
			bounds.put(parameter, new double[] { -boundError, boundError });

		return bounds;
	}

}
