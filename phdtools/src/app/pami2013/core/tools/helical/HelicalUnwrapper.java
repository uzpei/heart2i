package app.pami2013.core.tools.helical;

import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import extension.matlab.MatlabScript;
import gl.geometry.FancyArrow;
import gl.geometry.primitive.Quad;
import gl.geometry.primitive.QuadNonPlanar;
import gl.material.GLMaterial;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglScene;
import gl.texture.GLTexture;
import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;
import heart.HeartManager;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Executors;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.JColorMap;
import swing.component.LabelComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import system.object.Pair;
import system.object.Triplet;
import tools.TextToolset;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelVectorField;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.core.experiments.PamiExperimentRunner;
import app.pami2013.core.setup.PamiExperimentSetup.ParameterDefinition;
import app.pami2013.measures.DepthMeasurement;
import config.GraphicsToolsConstants;

public class HelicalUnwrapper extends JoglScene {

	private DoubleParameter phase = new DoubleParameter("phase", 0, 0, 360);
//	private IntParameter zSampling = new IntParameter("z sampling", 200, 0, 300);
//	private IntParameter thetaSampling = new IntParameter(GreekToolset.GreekLetter.THETA.toString() + " sampling", 40, 0, 100);

	private VoxelVectorField endoGradient;
	private VoxelVectorField epiGradient;
	private BooleanParameter drawEndoGradient = new BooleanParameter("draw Endo grad", false);
	private BooleanParameter drawEpiGradient = new BooleanParameter("draw Epi grad", false);

	private DepthMeasurement depthMeasure;
	
	private Boolean volumeLoading = false;
	private IntParameter zSampling = new IntParameter("z sampling", 200, 0, 300);
	private IntParameter thetaSampling = new IntParameter(GreekToolset.GreekLetter.THETA.toString() + " sampling", 40, 0, 100);
	
	private IntParameter N = new IntParameter("subplanar resolution", 16, 1, 32);
	private BooleanParameter drawHelix = new BooleanParameter("Draw helix", false);

	private BooleanParameter drawVectorField = new BooleanParameter("Draw vector field", false);

	private DoubleParameter transparency = new DoubleParameter("transparency", 1, 0, 1);
	
	private BooleanParameter sphericalColors = new BooleanParameter("Spherical colors", false);
	private BooleanParameter interpolated = new BooleanParameter("interpolated", false);
	
	private BooleanParameter optimized = new BooleanParameter("optimized", false);

	private BooleanParameter fixedColorMap = new BooleanParameter("fixedColorMap", false);
	
	private boolean unwrapBlocked = false;
	
	private boolean displayBlocked = false;
	
	private IntensityVolume neighborhoodVolume;
	
	private HeartManager manager;
    private GLTexture texture = null;
    private LabelComboBox<IntensityVolume> volumeBox;
    private JColorMap colorMap;
    
    private List<Triplet<Point3d, Point3d, Point3d>> helix;
    
	protected HelicalUnwrapper() {
		super("Helical Unwrapper");
		
		colorMap = new JColorMap("color map", Color.BLACK, Color.RED, Color.YELLOW);

		load();
		
		manager.getCurrent().getFrameField().setVisible(false);
		super.renderer.getCamera().zoom(-200f);
		
		super.render(290, 270);

		colorMap.addChangeListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				precomputeDisplayList(manager.getCurrent());
			}
		});
		
		applyFiltering(null);
		populateHelicalVolumes(manager.getCurrent());

	}
	
	private void constructDepth() {
		final List<Point3i> startPoints = new LinkedList<Point3i>();
		Heart heart = manager.getCurrent();
		final IntensityVolume outerWall = heart.getDistanceTransformEpi();
		final IntensityVolume innerWall = heart.getDistanceTransformEndo();

		depthMeasure = new DepthMeasurement(heart);

		double epsilon = 5;
		for (Triplet<Point3d, Point3d, Point3d> pt : helix) {
			Point3i pti = MathToolset.tuple3dTo3i(pt.getRight());
			if (Math.abs(outerWall.get(pti.x, pti.y, pti.z)) < epsilon) {
				startPoints.add(pti);
			}
		}

//		double[] depths = depthMeasure.integrate(startPoints, heart.getMask(), outerWall, innerWall);
//		System.out.println("Mean length = " + depths[0] + " +- " + depths[1]);
	}

	private void export() {
		final JoglRenderer renderer = super.renderer;
		Executors.newSingleThreadExecutor().submit(new Runnable() {

			@Override
			public void run() {
				/*
				 * TODO: - Set output path for screenshots - For each filtering
				 * parameter - Apply filter - Populate volumes with optimized
				 * parameters - For each volume - Unwrap volume and display -
				 * Take snapshot and output with volume name
				 */
				
				Heart heart = manager.getCurrent();
				drawVectorField.setValue(false);
				boolean exportColorBars = true;

				/*
				 * First precompute filters
				 */
				List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0), new IterativeFilter(5, 0.2), new IterativeFilter(10, 0.4));
//				List<IterativeFilter> filters = Arrays.asList(new IterativeFilter(0, 0));
				
				// Precompute filters
				System.out.println("Precomputing filters...");
				for (IterativeFilter filter : filters) {
					if (resultMap.containsKey(filter))
						continue;
					
					System.out.println(TextToolset.box(filter.toStringShort(), '#'));
					compute(heart, filter);
				}

				manager.getCurrent().getFrameField().setVisible(false);
				manager.getCurrent().getVoxelBox().setVisible(false);

				// Process all volumes
				List<MatlabScript> scripts = new LinkedList<MatlabScript>();
//		    	List<CartanParameter.Parameter> parameters = Arrays.asList(CartanParameter.Parameter.error1);
		    	List<CartanParameter.Parameter> parameters = Arrays.asList(CartanParameter.Parameter.values());
		    	
				for (CartanParameter.Parameter parameter : parameters) {
					int filterIndex = 0;
					for (IterativeFilter filter : filters) {
						FittingExperimentResult result = resultMap.get(filter);
						
						filterIndex++;
						displayBlocked = true;
						volumeLoading = true;

						/*
						 *  Build color map from thresholded nodes
						 */

						// Compute thresholded nodes
				    	List<CartanParameterNode> valid = PamiExperimentRunner.getValidated(result, false, true, PamiExperimentRunner.getCardiacBounds(Species.Rat));
				    	
						// Asssemble thresholded volume
						IntensityVolume volume = new IntensityVolume(heart.getDimension());
						for (CartanParameterNode node : valid) {
							volume.set(node.getPosition().x, node.getPosition().y, node.getPosition().z, node.get(parameter));
						}

						// Update thresholded color map only for the first filter
						if (filterIndex == 1) {
							colorMap.set(volume, heart.getMask());
						}

						// Reassemble all unthresholded volume
						for (CartanParameterNode node : result.getResultNodes()) {
							volume.set(node.getPosition().x, node.getPosition().y, node.getPosition().z, node.get(parameter));
						}

						/*
						 *  Unwrap volume
						 */

						// Prevent unwrapping from setting its own colormap
						fixedColorMap.setValue(true);
						unwrapHeart(heart, volume);

//						displayBlocked = true;

						/*
						 * Save
						 */
						try {
						String output = "./data/PAMI/images/" + heart.getSpecies()
								+ "/" + heart.getID() + "/" + "helicalVolumes/"
								+ filter.toStringFilename() + "/";
						String volName = parameter.toString().toLowerCase();
						
						Thread.sleep(1000);

						System.out.println("Processing volume " + volName);

						/*
						 * Load volume and wait for it to be fully loaded
						 */
							while (volumeLoading) {
								Thread.sleep(100);
							}
							// Wait 1s for make sure renderer has refreshed
							// FIXME: peek at the framebuffer and wait for a few iterations
							System.out.println("Displaying volume...");
							displayBlocked = false;
							Thread.sleep(2000);

						/*
						 *  Save screenshot
						 */
						System.out.println("Sending screenshot request...");
						renderer.setScreenshotRequest(output + volName + ".png");

						while (renderer.isScreenshotRequested()) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}

						/*
						 *  Save colorbar
						 */
						scripts.add(colorMap.saveColormapScript(output, volName + "_colormap", 48));

						System.out.println("Done with this volume.");
						
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				if (exportColorBars) {
					MatlabScript all = MatlabScript.concatenate(scripts);
					all.exit();
					all.run();
				}
			}
		});
	}
	
    private void load() {
    	if (manager == null) {
//    		manager = new HeartManager(true);
    		manager = new HeartManager(false);
//    		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/sampleSubset/");	
//    		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/rat07052008/registered/");
    		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/");
//    		HeartDefinition def = new HeartDefinition("rat07052008", Species.Rat, "./data/heart/rat_samples/sampleSubset/");
    		manager.loadHeart(def);
    	}

    	Heart heart = manager.getCurrent();
    	if (heart == null) return;

    	System.out.println("Loaded heart from " + heart.getDirectory());

//    	IntensityVolume[] dts = heart.getDistanceTransform();
//    	endoGradient = new VoxelVectorField("", dts[0].dxOut().asVolume(), dts[0].dyOut().asVolume(), dts[0].dzOut().asVolume());
//    	endoGradient.flip();
//    	epiGradient = new VoxelVectorField("", dts[1].dxOut().asVolume(), dts[1].dyOut().asVolume(), dts[1].dzOut().asVolume());
//		endoGradient.normalize(heart.getVoxelBox());
//		epiGradient.normalize(heart.getVoxelBox());

    	// Remove previous interactor
    	super.renderer.removeInteractor(heart.getVoxelBox());

    	// Add interactors    	
        super.renderer.addInteractor(heart.getVoxelBox());
        
    	heart.getVoxelBox().cut(CuttingPlane.FIT);
    	
//    	populateHelicalVolumes(manager.getCurrent());
    }
    
    private IntensityVolume currentVolume = null;
	/**
	 * TODO: either follow local centerline tangents or fit a line through the center or the heart to construct the helical wrapping
	 * @return a list of points along a helical unwrapping of the heart.
	 */
	private void unwrapHeart(Heart heart, IntensityVolume volume) {
//		System.out.println("Unwrapping heart with volume [" + volume.getName() + "]");
		
		currentVolume = volume;
		boolean displayBlockedCached = displayBlocked;
		displayBlocked = true;
		
		if (!fixedColorMap.getValue()) {
			colorMap.set(volume, heart.getMask());
		}
		
		// Spit out colormap for Matlab
//		System.out.println(colorMap.sampleMatlab(256));

		// Make sure we work with udpated geometry
		heart.getGeometry().update();
		List<Triplet<Boolean, Point3d, Double>> centerline = heart.getGeometry().getCenterline();

		// Helical parameters
		helix = new ArrayList<Triplet<Point3d, Point3d, Point3d>>();

		// Compute minimum and maximum z planes
		double z0 = Double.MAX_VALUE, zf = Double.MIN_VALUE;
		
		for (Triplet<Boolean, Point3d, Double> slice : centerline) {
			if (!slice.getLeft())
				continue;
			if (slice.getCenter().z > zf)
				zf = slice.getCenter().z;
			if (slice.getCenter().z < z0)
				z0 = slice.getCenter().z;
		}
		
		// Use min of slice found and voxelbox 
//		zf = Math.min(zf, heart.getVoxelBox().getOrigin().x + heart.getVoxelBox().getSpan().z);
//		zf = heart.getVoxelBox().getOrigin().x + heart.getVoxelBox().getSpan().z;
		zf = 65;
//		System.out.println("z0 = " + z0 + ", zf = " + zf);
		// Assemble helix
		double theta0 = phase.getValue();
		double dtheta = 2 * Math.PI / thetaSampling.getValue();
		double z = 0;
		double theta = theta0;
		double dz = (zf - z0) / zSampling.getValue();
		
		for (Triplet<Boolean, Point3d, Double> slice : centerline) {
			if (!slice.getLeft() || z > zf)
				continue;

			Point3d centroid = slice.getCenter();
			double radius = slice.getRight();
			
			// Set the current slice
//			Point3i boxOrigin = box.getOrigin();
//			boxOrigin.z = MathToolset.roundInt(centroid.z);
//			box.setOrigin(boxOrigin);

			// Continue if we have reached the next slice
			while (z < centroid.z + 1) {
				z += dz;
				theta += dtheta;

				// Current helical point
				Point3d pHelix = new Point3d();
				pHelix.x = centroid.x + radius * Math.cos(theta);
				pHelix.y = centroid.y + radius * Math.sin(theta);
				pHelix.z = z;
				
				// 1) Raytrace along both planar helix normals (in both directions) until we find a hit
				// 2) Once a hit is found, mark this location x0 and completely discard the other normal direction
				// 3) Keep tracing until we are through the heart wall
				// 4) Mark this location x1
				// 5) Find the midpoint (x1 - x0) / 2 and use this as the helical center
				// f(u) = ( acos(u), asin(u), bu )
				
				Pair<Point3d, Point3d> boundary1 = trace(heart, centroid, new Vector3d(Math.cos(theta), Math.sin(theta), 0));
				Pair<Point3d, Point3d> boundary2 = trace(heart, centroid, new Vector3d(-Math.cos(theta), -Math.sin(theta), 0));
				Point3d mid1 = new Point3d();
				Point3d mid2 = new Point3d();

				// Compute midpoints
				// (only if both exist)
				
				double d1 = Double.MAX_VALUE;
				double d2 = Double.MAX_VALUE;
				
				if (boundary1 != null) {
					mid1.add(boundary1.getLeft(), boundary1.getRight());
					mid1.scale(0.5);
					d1 = pHelix.distance(mid1);
				}

				if (boundary2 != null) {
					mid2.add(boundary2.getLeft(), boundary2.getRight());
					mid2.scale(0.5);
					d2 = pHelix.distance(boundary2.getLeft());
				}
				
				// Compute the mid-point boundary
				Pair<Point3d, Point3d> boundary = null;
				Point3d mid = null;
				if (boundary1 != null && boundary2 != null) {
					mid = mid1;
					boundary = d1 < d2 ? boundary1 : boundary2;
				}
				else if (boundary1 != null) {
					mid = mid1;
					boundary = boundary1;
				}
				else if (boundary2 != null) {
					mid = mid2;
					boundary = boundary2;
				}
				
				if (boundary != null && mid != null) {
					helix.add(new Triplet<Point3d, Point3d, Point3d>(boundary.getLeft(), mid, boundary.getRight()));
				}
				else {
					helix.add(new Triplet<Point3d, Point3d, Point3d>(null, null, null));
				}
			}
		}

		int smoothingIterations = 4;
		for (int it = 0; it < smoothingIterations; it++)
			smoothHelix();
		
		precomputeDisplayList(heart);
		
		System.out.println("Done.");
		
		displayBlocked = displayBlockedCached;
		volumeLoading = false;
	}
	
	public boolean validate(Triplet<Point3d, Point3d, Point3d> point) {
		return point.getLeft() != null && point.getCenter() != null && point.getRight() != null;
	}
	
	private void smoothHelix() {
		ListIterator<Triplet<Point3d, Point3d, Point3d>> iterator = helix.listIterator();
		Point3d avg = new Point3d();
		while (iterator.hasNext()) {
			if (iterator.hasPrevious() && iterator.hasNext()) {
				// Fetch previous
				Triplet<Point3d, Point3d, Point3d> previous = iterator.previous();
				// Fetch current
				Triplet<Point3d, Point3d, Point3d> current = iterator.next();
				// Fetch next
				Triplet<Point3d, Point3d, Point3d> next = iterator.next();
				
				if (validate(previous) && validate(current) && validate(next)) {
					// Average previous and next
					avg.set(0,0,0);
					avg.add(previous.getLeft());
					avg.add(current.getLeft());
					avg.add(next.getLeft());
					avg.scale(1d/3d);
					current.getLeft().set(avg);
					
					avg.set(0,0,0);
					avg.add(previous.getCenter());
					avg.add(current.getCenter());
					avg.add(next.getCenter());
					avg.scale(1d/3d);
					current.getCenter().set(avg);
					
					avg.set(0,0,0);
					avg.add(previous.getRight());
					avg.add(current.getRight());
					avg.add(next.getRight());
					avg.scale(1d/3d);
					current.getRight().set(avg);					
				}
				
				// Return to current
				iterator.previous();
			}
			
			// Go to next
			iterator.next();
		}
	}
	
	/**
	 * Return intersection points by starting at the centroid and moving in the input direction.
	 * @param centroid
	 * @param direction
	 * @return
	 */
	private Pair<Point3d, Point3d> trace(Heart heart, Point3d centroid, Vector3d direction) {
		double stepSize = 0.5;
		
		// Need to fine-tune this... could be a major speedup
		int maxSteps = MathToolset.roundInt(0.5 * heart.getVoxelBox().getMaxDimension() / stepSize);
		int step = 0;

		Point3d point = new Point3d();
		boolean[] intersected = new boolean[] { false, false };
		Pair<Point3d, Point3d> boundary = new Pair<Point3d, Point3d>(new Point3d(), new Point3d());
		while(step < maxSteps) {
			point.scaleAdd(step * stepSize, direction, centroid);

			// Check for intersection
			if (checkHit(heart, point)) {
				
				// This is the first hit
				if (!intersected[0]) {
					intersected[0] = true;
					boundary.getLeft().set(point);
				}
				else {
					// Else mark this location and continue
					intersected[1] = true;
					boundary.getRight().set(point);
				}
			}
			// Could terminate abruptly using the distance transform
			// if (step * stepSize > maxDistanceTransform)
			// 	terminate = true;
			
			step++;
		}
		
		if (intersected[0] && intersected[1])
			return boundary;
		else
			return null;
	}
	
	private boolean checkHit(Heart heart, Point3d p) {
		if (!heart.getMask().hasPoint(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z)))
			return false;
		
		return !heart.getMask().isMasked(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z));
	}
    
    private void populateHelicalVolumes(Heart heart) {
    	boolean displayBlockedCached = displayBlocked;
    	displayBlocked = true;
    	/**
    	 * Compute volumes
    	 */
    	List<IntensityVolume> volumes = new ArrayList<IntensityVolume>();
    	
    	/*
    	 * Add neighborhood volume
    	 */
//    	int neighborhoodSize = 3;
//    	HashMap<CartanParameter.Parameter, double[]> bounds = PamiExperimentRunner.getCardiacBounds(Species.Rat);
//    	final FittingData data = new FittingData(new CartanFittingParameter(bounds, neighborhoodSize), heart.getFrameField(), heart.getVoxelBox());
//    	neighborhoodVolume = new IntensityVolume(heart.getDimension());
//    	neighborhoodVolume.setName("neighborhood");
//    	heart.getVoxelBox().voxelProcess(new PerVoxelMethodUnchecked() {
//			@Override
//			public void process(int x, int y, int z) {
//				neighborhoodVolume.set(x, y, z, data.getNeighborhood(new Point3i(x, y, z)).size());
//			}
//		});
//    	volumes.add(neighborhoodVolume);
    	
		/*
		 *  Add heart vector field
		 */
    	for (int i = 0; i < 3; i++) {
//    		volumes.add(heart.getFrameField().getFieldX().getVolumes()[i]);
//    		volumes.add(heart.getFrameField().getFieldY().getVolumes()[i]);
//    		volumes.add(heart.getFrameField().getFieldZ().getVolumes()[i]);
    	}
		
		/*
		 *  Add absolute distance transforms
		 */
//    	heart.getDistanceTransform()[0].abs();
//    	heart.getDistanceTransform()[1].abs();
//    	volumes.add(heart.getDistanceTransform()[0]);
//    	volumes.add(heart.getDistanceTransform()[1]);
		
		/*
		 *  Add selected connection forms and errors
		 */
//    	List<CartanParameter.Parameter> parameters = Arrays.asList(CartanParameter.Parameter.C123, CartanParameter.Parameter.error1);
    	List<CartanParameter.Parameter> parameters = Arrays.asList(CartanParameter.Parameter.values());
//    	List<CartanParameter.Parameter> parameters = Arrays.asList(CartanParameter.Parameter.C121, CartanParameter.Parameter.C123, CartanParameter.Parameter.error1);
    	
    	FittingExperimentResult result = null;
    	if (optimized.getValue()) {
//    		CartanFittingParameter fittingParameter = new CartanFittingParameter(CartanModel.FULL, FittingMethod.Optimized, 3);
    		CartanFittingParameter fittingParameter = new CartanFittingParameter(null, FittingMethod.Optimized, true, Runtime.getRuntime().availableProcessors(), 10, 1e-7, 1e-7, 0, CartanModel.FULL, NeighborhoodShape.Isotropic, 3);
    		result = new FittingExperimentResult(new FittingData(fittingParameter, heart.getFrameField(), heart.getVoxelBox()));
    		System.out.println("Fitting connection forms...");
    		new CartanFitter().launchFit(result, true);
    		System.out.println("Done in " + result.getElapsedTime() + "s");
    	}
    	else {
			// Compute the connection forms
			FittingData dataDirect = new FittingData(new CartanFittingParameter(null, CartanModel.FULL, FittingMethod.Direct, 3), heart.getFrameField(), heart.getVoxelBox());
			result = new FittingExperimentResult(dataDirect);
			DifferentialOneForm.compute(result, heart.getMask());
    	}

    	// Apply thresholds
    	PamiExperimentRunner.validate(result, false, true, PamiExperimentRunner.getCardiacBounds(Species.Rat));
    	
		// Recompute error in Omega_n to the unfiltered data
    	if (heart.getFilter() != null && heart.getFilter().getIterations() > 0) {
    		heart.setSmoothing(null);
    		heart.reloadData();
        	PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(new FittingData(result.getFittingParameter(), heart.getFrameField(), heart.getVoxelBox()));
    		for (CartanParameterNode node : result.getResultNodes()) {
    			node.setError(fittingError.evaluate(node, result.getFittingData()
    					.getNeighborhood(node.getPosition())));
    		}		
    	}
    	
		// Extract volumes
		for (CartanParameter.Parameter parameter : Arrays.asList(CartanParameter.Parameter.C121, CartanParameter.Parameter.C122, CartanParameter.Parameter.C123, CartanParameter.Parameter.C131, CartanParameter.Parameter.C232, CartanParameter.Parameter.error1)) {
			IntensityVolume volume = new IntensityVolume(heart.getDimension());
			volume.setName(parameter.toString().toLowerCase());
			for (CartanParameterNode node : result.getResultNodes()) {
				volume.set(node.getPosition().x, node.getPosition().y, node.getPosition().z, node.get(parameter));
			}
			volumes.add(volume);
		}


    	if (volumeBox == null) {
    		volumeBox = new LabelComboBox<IntensityVolume>("Volume", volumes, new ActionListener() {
    			@Override
    			public void actionPerformed(ActionEvent e) {
    				if (!unwrapBlocked) {
    					unwrapHeart(manager.getCurrent(), volumeBox.getSelected());
    				}
    			}
    		});
    	}
    	else {
    		unwrapBlocked = true;
    		int index = volumeBox.getSelectedIndex();
    		volumeBox.updateData(volumes);
    		
    		if (index > 0)
    			volumeBox.setSelectedIndex(index);
    		unwrapBlocked = false;
    	}

		if (!fixedColorMap.getValue()) {
			colorMap.set(volumeBox.getSelected(), heart.getMask());
		}
		
		// TODO: once color map has been set, get back all voxels that were discarded during the "validate" call
		
		unwrapHeart(manager.getCurrent(), volumeBox.getSelected());
		
		displayBlocked = displayBlockedCached;
    }
    
    private HashMap<IterativeFilter, FittingExperimentResult> resultMap = new HashMap<IterativeFilter, FittingExperimentResult>();
    
    private void compute(Heart heart, IterativeFilter filter) {
    	boolean displayBlockedCached = displayBlocked;
    	displayBlocked = true;

    	// Apply filtering
    	IterativeFilter heartFilter = heart.getFilter();
    	if ((heartFilter != null && !heartFilter.equals(filter)) || (filter != null && filter.getIterations() > 0)) {
    		manager.getCurrent().setSmoothing(filter);
    		manager.getCurrent().reloadData();
    	}

    	FittingExperimentResult result = null;
    	if (optimized.getValue()) {
    		
    		CartanFittingParameter fittingParameter = new CartanFittingParameter(new ParameterDefinition(FittingMethod.Optimized, CartanModel.FULL, filter, 3));
    		result = new FittingExperimentResult(new FittingData(fittingParameter, heart.getFrameField(), heart.getVoxelBox()));
    		new CartanFitter().launchFit(result, true);
    		System.out.println("Done in " + result.getElapsedTime() + "s");
    	}
    	else {
			// Compute the connection forms
			FittingData dataDirect = new FittingData(new CartanFittingParameter(null, CartanModel.FULL, FittingMethod.Direct, 3), heart.getFrameField(), heart.getVoxelBox());
			result = new FittingExperimentResult(dataDirect);
			DifferentialOneForm.compute(result, heart.getMask());
    	}
    	
		// Recompute error in Omega_n to the unfiltered data
    	if (filter != null && filter.getIterations() > 0) {
    		heart.setSmoothing(null);
    		heart.reloadData();
        	PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(new FittingData(result.getFittingParameter(), heart.getFrameField(), heart.getVoxelBox()));
    		for (CartanParameterNode node : result.getResultNodes()) {
    			node.setError(fittingError.evaluate(node, result.getFittingData()
    					.getNeighborhood(node.getPosition())));
    		}		
    	}
    	
    	resultMap.put(filter, result);
    	
		displayBlocked = displayBlockedCached;
    }

	@Override
	public void display(GLAutoDrawable drawable) {
		if (displayBlocked)
			return;
		
		GL2 gl = drawable.getGL().getGL2();
		Heart heart = manager.getCurrent();
		
		if (manager.getCurrent() != null) {
			manager.getCurrent().display(drawable);
			IntensityVolumeMask mask = heart.getMask();
			
			if (helix != null && helix.size() > 0) {
				 if (texture == null)
					 texture = new GLTexture(GraphicsToolsConstants.METAL_TEXTURE_PATH);
				
				gl.glPushMatrix();
				manager.getCurrent().getVoxelBox().applyHeartNormalization(gl);
				
				if (drawHelix.getValue()) {
					// Draw midpoints
					gl.glDisable(GL2.GL_LIGHTING);
					gl.glPointSize(10f);
					gl.glBegin(GL2.GL_POINTS);
					int i = 0;
					for (Triplet<Point3d, Point3d, Point3d> point : helix) {
						Point3d p = point.getCenter();
						if (p == null) continue;
						
						double lambda = i++ / ((double) helix.size());
						gl.glColor4d(1-lambda, 0, lambda, 0.8);
						gl.glVertex3d(p.x, p.y, p.z);
					}
					gl.glEnd();					
				}
				
				gl.glEnable(GL2.GL_LIGHTING);

				VoxelVectorField vectorField = null;
				double scale = 0;
				if (drawVectorField.getValue()) {
					vectorField = heart.getFrameField().getF1();
					scale = 4;
				}
				
				if (drawEndoGradient.getValue()) {
					vectorField = endoGradient;
					scale = 4;
				}
				
				if (drawEpiGradient.getValue()) {
					vectorField = epiGradient;
					scale = 4;
				}
				
				if (vectorField != null) {
					fancyArrow = new FancyArrow(new Point3d(), new Point3d(), new Color3f(1, 1, 1), 0.5);
					Vector3d v = new Vector3d();
					Point3d pScaled = new Point3d();
					Vector3d vWall = new Vector3d();
					double proximity = 0.1;
					
					for (Triplet<Point3d, Point3d, Point3d> point : helix) {
						Point3d p = point.getCenter();
						if (p == null || heart.getMask().isMasked(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z))) continue;
						
						// Draw outer
						vWall.sub(point.getCenter(), point.getLeft());
						pScaled.scaleAdd(proximity, vWall, point.getLeft());
						vectorField.getVector3d(pScaled, v);
						drawArrow(gl, point.getLeft(), v, scale);
						
						// Draw center
						vectorField.getVector3d(point.getCenter(), v);
						drawArrow(gl, point.getCenter(), v, scale);

						// Draw inner
						vWall.sub(point.getCenter(), point.getRight());
						pScaled.scaleAdd(proximity, vWall, point.getRight());
						vectorField.getVector3d(pScaled, v);
						drawArrow(gl, point.getRight(), v, scale);
					}
				}
				
//				else {
					for (Quad quad : helicalQuads) {
						if (quad != null)
							quad.displayMaterials(drawable);
					}
//				}

				if (depthMeasure != null) {
					depthMeasure.display(drawable);
				}
					
				gl.glPopMatrix();
			}
		}
	}

	private Point3d pv = new Point3d();
	private FancyArrow fancyArrow = new FancyArrow(new Point3d(), new Point3d(), new Color3f(0, 0, 1), 0.4);
	private Point3d center = new Point3d();
	private void drawArrow(GL2 gl, Point3d p, Vector3d v, double scale) {
		if (v.length() > 1 + 1e-2) return;
//		v.normalize();
		
		center.set(p);
		v.scale(scale);
		pv.scaleAdd(0.5, v, center);
		center.scaleAdd(-0.5, v, center);
		
		fancyArrow.setFrom(center);
		fancyArrow.setTo(pv);
		fancyArrow.draw(gl);
	}
	
	private List<Quad> helicalQuads;
	
	private void precomputeDisplayList(Heart heart) {

		boolean displayBlockedCached = displayBlocked;
		displayBlocked = true;
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Quad> helicalQuads = new LinkedList<Quad>();
		
		Triplet<Point3d, Point3d, Point3d> previous = null;
		
		int materialCount = interpolated.getValue() ? 4 : 1;
		
		GLMaterial baseMaterial = new GLMaterial(0, 0, 0, 0);
		
		for (Triplet<Point3d, Point3d, Point3d> point : helix) {
			Point3d pLeft = point.getLeft();
			Point3d pMid = point.getCenter();
			Point3d pRight = point.getRight();
			
			if (pLeft == null || pMid == null || pRight == null)
				continue;

			if (previous != null) {
				Point3d prevLeft = previous.getLeft();
				Point3d prevRight = previous.getRight();

				QuadNonPlanar quad = new QuadNonPlanar(prevLeft, pLeft, pRight, prevRight);
				int Ni = N.getValue();
				int Nj = Ni;
				Quad[][] grid = quad.subdivide(Ni, Nj);
				for (int gi = 0; gi < Ni; gi++)
					for (int gj = 0; gj < Nj; gj++) {
						Point3d p = grid[gi][gj].getPoint();
						Point3i pi = MathToolset.tuple3dTo3i(p);
						if (!heart.getMask().hasPoint(pi) || heart.getMask().isMasked(pi)) 
							continue;

						
						GLMaterial[] materials = new GLMaterial[materialCount];
						for (int i = 0; i < materialCount; i++)
							materials[i] = new GLMaterial(baseMaterial);

						if (interpolated.getValue()) {
							
							assign(heart, grid[gi][gj].p1, materials[0]);
							assign(heart, grid[gi][gj].p2, materials[1]);
							assign(heart, grid[gi][gj].p3, materials[2]);
							assign(heart, grid[gi][gj].p4, materials[3]);
							grid[gi][gj].setMaterials(materials);
						}
						else {
							assign(heart, grid[gi][gj].getPoint(), materials[0]);
							grid[gi][gj].setMaterials(materials);
						}
						
						helicalQuads.add(grid[gi][gj]);
					}
				
			}
			
			previous = point;
		}
		
//		List<Quad> prevQuads = this.helicalQuads;
		this.helicalQuads = helicalQuads;
//		if (prevQuads != null) {
//			prevQuads.clear();
//		}
		
		displayBlocked = displayBlockedCached;
	}
	
	private void assign(Heart heart, Point3d p, GLMaterial material) {
		if (sphericalColors.getValue()) {
			// Look up fiber orientation
			Point3i pi = MathToolset.tuple3dTo3i(p);
			double vx = heart.getFrameField().getF1().getVolumes()[0].get(pi.x, pi.y, pi.z);
			double vy = heart.getFrameField().getF1().getVolumes()[1].get(pi.x, pi.y, pi.z);
			double vz = heart.getFrameField().getF1().getVolumes()[2].get(pi.x, pi.y, pi.z);
			float thetaNormalized = (float) (Math.acos(vz) / Math.PI);
			double phi = Math.abs(Math.atan2(vy, vx) / (2*Math.PI));
			float phiNormalized = (float) (phi >= 0 ? phi : phi + 1);
			
//			System.out.println(phi + " -> " + phiNormalized);
			
			/*
			 *  This is the symmetrical Pierpaoli representation
			 */
			Color c = new Color(Color.HSBtoRGB(thetaNormalized, 1f, 1f));
//			Color c = new Color(Color.HSBtoRGB(thetaNormalized, 1f - phiNormalized, 1f));
			material.diffuse[0] = c.getRed() / 255f;
			material.diffuse[1] = c.getGreen() / 255f;
			material.diffuse[2] = c.getBlue() / 255f;
			material.diffuse[3] = transparency.getFloatValue();
		}
		else {
			double data = currentVolume.get(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z));
			Color c = colorMap.getColor(data);
			if (c == null) return;
			material.diffuse[0] = c.getRed() / 255f;
			material.diffuse[1] = c.getGreen() / 255f;
			material.diffuse[2] = c.getBlue() / 255f;
			material.diffuse[3] = transparency.getFloatValue();
		}
	}
	
	private void applyFiltering(IterativeFilter filter) {
		compute(manager.getCurrent(), filter);
		manager.getCurrent().getFrameField().setVisible(false);
		manager.getCurrent().getVoxelBox().setVisible(false);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		VerticalFlowPanel vfpHelix = new VerticalFlowPanel("Helical Projection");
		JPanel pHelixBools = new JPanel(new GridLayout(1, 3));
		pHelixBools.add(drawVectorField.getControls());
		pHelixBools.add(drawHelix.getControls());
		vfpHelix.add(pHelixBools);
		vfpHelix.add(interpolated.getControls());
		vfpHelix.add(sphericalColors.getControls());
		vfpHelix.add(drawEndoGradient.getControls());
		vfpHelix.add(drawEpiGradient.getControls());
		vfpHelix.add(phase.getSliderControls());
		vfpHelix.add(zSampling.getSliderControls());
		vfpHelix.add(thetaSampling.getSliderControls());
		vfpHelix.add(N.getSliderControls());
		vfp.add(vfpHelix);

		VerticalFlowPanel vfpVolume = new VerticalFlowPanel("Volume Visualization");

		JPanel pVisuBools = new JPanel(new GridLayout(1, 2));
		pVisuBools.add(optimized.getControls());
		pVisuBools.add(fixedColorMap.getControls());
		vfpVolume.add(pVisuBools);
		
		vfpVolume.add(transparency.getSliderControls());
		optimized.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				populateHelicalVolumes(manager.getCurrent());
			}
		});
		
		vfpVolume.add(colorMap);
		vfpVolume.add(volumeBox);
		vfp.add(vfpVolume);
		
		vfp.add(manager);
		ParameterListener updateListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				unwrapHeart(manager.getCurrent(), volumeBox.getSelected());
			}
		};
		
		phase.addParameterListener(updateListener);
		zSampling.addParameterListener(updateListener);
		thetaSampling.addParameterListener(updateListener);
		interpolated.addParameterListener(updateListener);
		transparency.addParameterListener(updateListener);
		
		return vfp.getPanel();
	}
	
    @Override
	public void attach(Component component) {
    	super.attach(component);
		this.component = component;
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_C) {
			    	unwrapHeart(manager.getCurrent(), volumeBox.getSelected());
				}
				else if (e.getKeyCode() == KeyEvent.VK_E) {
			    	export();
				}
				else if (e.getKeyCode() == KeyEvent.VK_D) {
			    	constructDepth();
				}
				else if (e.getKeyCode() == KeyEvent.VK_P) {
					applyFiltering(null);
					populateHelicalVolumes(manager.getCurrent());
				}
				else if (e.getKeyCode() == KeyEvent.VK_M) {
					colorMap.saveColormap(new File(".").getAbsolutePath(), "colormap");
				}
				else if (e.getKeyCode() == KeyEvent.VK_S) {
					smoothHelix();
					precomputeDisplayList(manager.getCurrent());
				}
				else if (e.getKeyCode() == KeyEvent.VK_F) {
					applyFiltering(new IterativeFilter(5, 0.2));
				}
			}
		});
	}

	public static void main(String[] args) {
		new HelicalUnwrapper();
	}
}
