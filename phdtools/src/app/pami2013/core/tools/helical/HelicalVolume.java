package app.pami2013.core.tools.helical;

import gl.geometry.primitive.Quad;
import gl.geometry.primitive.QuadNonPlanar;
import gl.material.GLMaterial;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.JColorMap;
import system.object.Pair;
import system.object.Triplet;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelVectorField;

public class HelicalVolume {
	
	private double phase = 0;
	private int zSampling = 200;
	private int thetaSampling = 40;
	private int N = 16;
	private boolean fixedColorMap = false;
	private double transparency = 1;
    private JColorMap colorMap;
    
    private List<Triplet<Point3d, Point3d, Point3d>> helix;
    private List<Triplet<Boolean, Point3d, Double>> centerline;
    
    private IntensityVolume volume;

    private IntensityVolumeMask mask;
    
	private boolean unwrapBlocked = false;
	
	private boolean sphericalColors = false;
	
	private boolean interpolated = false;
	
	private boolean displayBlocked = false;
	
	private Boolean volumeLoading = false;
	
	private VoxelVectorField fieldColor; 

    public HelicalVolume(IntensityVolume volume, IntensityVolumeMask mask, List<Triplet<Boolean, Point3d, Double>> centerline, JColorMap map) {
		this.volume = volume;
		this.mask = mask;
		this.centerline = centerline;

		colorMap = map;
		colorMap.set(volume, mask);

		unwrapHeart();
	}
	
    public void setFieldColor(VoxelVectorField field) {
    	this.fieldColor = field;
    }
    
	/**
	 * TODO: either follow local centerline tangents or fit a line through the center or the heart to construct the helical wrapping
	 * @return a list of points along a helical unwrapping of the heart.
	 */
	private void unwrapHeart() {
//		System.out.println("Unwrapping heart with volume [" + volume.getName() + "]");
		
		boolean displayBlockedCached = displayBlocked;
		displayBlocked = true;
		
		if (!fixedColorMap) {
			colorMap.set(volume, mask);
		}
		
		// Spit out colormap for Matlab
//		System.out.println(colorMap.sampleMatlab(256));

		// Helical parameters
		helix = new ArrayList<Triplet<Point3d, Point3d, Point3d>>();

		// Compute minimum and maximum z planes
		double z0 = Double.MAX_VALUE, zf = Double.MIN_VALUE;
		
		for (Triplet<Boolean, Point3d, Double> slice : centerline) {
			if (!slice.getLeft())
				continue;
			if (slice.getCenter().z > zf)
				zf = slice.getCenter().z;
			if (slice.getCenter().z < z0)
				z0 = slice.getCenter().z;
		}
		
		// Use min of slice found and voxelbox 
//		zf = Math.min(zf, heart.getVoxelBox().getOrigin().x + heart.getVoxelBox().getSpan().z);
//		zf = heart.getVoxelBox().getOrigin().x + heart.getVoxelBox().getSpan().z;
		zf = 65;
//		System.out.println("z0 = " + z0 + ", zf = " + zf);
		// Assemble helix
		double theta0 = phase;
		double dtheta = 2 * Math.PI / thetaSampling;
		double z = 0;
		double theta = theta0;
		double dz = (zf - z0) / zSampling;
		
		for (Triplet<Boolean, Point3d, Double> slice : centerline) {
			if (!slice.getLeft() || z > zf)
				continue;

			Point3d centroid = slice.getCenter();
			double radius = slice.getRight();
			
			// Set the current slice
//			Point3i boxOrigin = box.getOrigin();
//			boxOrigin.z = MathToolset.roundInt(centroid.z);
//			box.setOrigin(boxOrigin);

			// Continue if we have reached the next slice
			while (z < centroid.z + 1) {
				z += dz;
				theta += dtheta;

				// Current helical point
				Point3d pHelix = new Point3d();
				pHelix.x = centroid.x + radius * Math.cos(theta);
				pHelix.y = centroid.y + radius * Math.sin(theta);
				pHelix.z = z;
				
				// 1) Raytrace along both planar helix normals (in both directions) until we find a hit
				// 2) Once a hit is found, mark this location x0 and completely discard the other normal direction
				// 3) Keep tracing until we are through the heart wall
				// 4) Mark this location x1
				// 5) Find the midpoint (x1 - x0) / 2 and use this as the helical center
				// f(u) = ( acos(u), asin(u), bu )
				
				Pair<Point3d, Point3d> boundary1 = trace(centroid, new Vector3d(Math.cos(theta), Math.sin(theta), 0));
				Pair<Point3d, Point3d> boundary2 = trace(centroid, new Vector3d(-Math.cos(theta), -Math.sin(theta), 0));
				Point3d mid1 = new Point3d();
				Point3d mid2 = new Point3d();

				// Compute midpoints
				// (only if both exist)
				
				double d1 = Double.MAX_VALUE;
				double d2 = Double.MAX_VALUE;
				
				if (boundary1 != null) {
					mid1.add(boundary1.getLeft(), boundary1.getRight());
					mid1.scale(0.5);
					d1 = pHelix.distance(mid1);
				}

				if (boundary2 != null) {
					mid2.add(boundary2.getLeft(), boundary2.getRight());
					mid2.scale(0.5);
					d2 = pHelix.distance(boundary2.getLeft());
				}
				
				// Compute the mid-point boundary
				Pair<Point3d, Point3d> boundary = null;
				Point3d mid = null;
				if (boundary1 != null && boundary2 != null) {
					mid = mid1;
					boundary = d1 < d2 ? boundary1 : boundary2;
				}
				else if (boundary1 != null) {
					mid = mid1;
					boundary = boundary1;
				}
				else if (boundary2 != null) {
					mid = mid2;
					boundary = boundary2;
				}
				
				if (boundary != null && mid != null) {
					helix.add(new Triplet<Point3d, Point3d, Point3d>(boundary.getLeft(), mid, boundary.getRight()));
				}
				else {
					helix.add(new Triplet<Point3d, Point3d, Point3d>(null, null, null));
				}
			}
		}

		int smoothingIterations = 4;
		for (int it = 0; it < smoothingIterations; it++)
			smoothHelix();
		
		precomputeDisplayList();
		
		System.out.println("Done.");
		
		displayBlocked = displayBlockedCached;
		volumeLoading = false;
	}
	
	public boolean validate(Triplet<Point3d, Point3d, Point3d> point) {
		return point.getLeft() != null && point.getCenter() != null && point.getRight() != null;
	}
	
	private void smoothHelix() {
		ListIterator<Triplet<Point3d, Point3d, Point3d>> iterator = helix.listIterator();
		Point3d avg = new Point3d();
		while (iterator.hasNext()) {
			if (iterator.hasPrevious() && iterator.hasNext()) {
				// Fetch previous
				Triplet<Point3d, Point3d, Point3d> previous = iterator.previous();
				// Fetch current
				Triplet<Point3d, Point3d, Point3d> current = iterator.next();
				// Fetch next
				Triplet<Point3d, Point3d, Point3d> next = iterator.next();
				
				if (validate(previous) && validate(current) && validate(next)) {
					// Average previous and next
					avg.set(0,0,0);
					avg.add(previous.getLeft());
					avg.add(current.getLeft());
					avg.add(next.getLeft());
					avg.scale(1d/3d);
					current.getLeft().set(avg);
					
					avg.set(0,0,0);
					avg.add(previous.getCenter());
					avg.add(current.getCenter());
					avg.add(next.getCenter());
					avg.scale(1d/3d);
					current.getCenter().set(avg);
					
					avg.set(0,0,0);
					avg.add(previous.getRight());
					avg.add(current.getRight());
					avg.add(next.getRight());
					avg.scale(1d/3d);
					current.getRight().set(avg);					
				}
				
				// Return to current
				iterator.previous();
			}
			
			// Go to next
			iterator.next();
		}
	}
	
	/**
	 * Return intersection points by starting at the centroid and moving in the input direction.
	 * @param centroid
	 * @param direction
	 * @return
	 */
	private Pair<Point3d, Point3d> trace(Point3d centroid, Vector3d direction) {
		double stepSize = 0.5;
		
		// Need to fine-tune this... could be a major speedup
		int maxSteps = MathToolset.roundInt(0.5 * volume.getMaxDimension() / stepSize);
		int step = 0;

		Point3d point = new Point3d();
		boolean[] intersected = new boolean[] { false, false };
		Pair<Point3d, Point3d> boundary = new Pair<Point3d, Point3d>(new Point3d(), new Point3d());
		while(step < maxSteps) {
			point.scaleAdd(step * stepSize, direction, centroid);

			// Check for intersection
			if (checkHit(point)) {
				
				// This is the first hit
				if (!intersected[0]) {
					intersected[0] = true;
					boundary.getLeft().set(point);
				}
				else {
					// Else mark this location and continue
					intersected[1] = true;
					boundary.getRight().set(point);
				}
			}
			// Could terminate abruptly using the distance transform
			// if (step * stepSize > maxDistanceTransform)
			// 	terminate = true;
			
			step++;
		}
		
		if (intersected[0] && intersected[1])
			return boundary;
		else
			return null;
	}
	
	private boolean checkHit(Point3d p) {
		if (!mask.hasPoint(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z)))
			return false;
		
		return !mask.isMasked(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z));
	}
    
	public void display(GLAutoDrawable drawable, boolean drawHelix) {
		if (displayBlocked)
			return;

		GL2 gl = drawable.getGL().getGL2();

		if (helix != null && helix.size() > 0) {

			if (drawHelix) {
				// Draw midpoints
				gl.glDisable(GL2.GL_LIGHTING);
				gl.glPointSize(10f);
				gl.glBegin(GL2.GL_POINTS);
				int i = 0;
				for (Triplet<Point3d, Point3d, Point3d> point : helix) {
					Point3d p = point.getCenter();
					if (p == null)
						continue;

					double lambda = i++ / ((double) helix.size());
					gl.glColor4d(1 - lambda, 0, lambda, 0.8);
					gl.glVertex3d(p.x, p.y, p.z);
				}
				gl.glEnd();
			}

			gl.glEnable(GL2.GL_LIGHTING);

			for (Quad quad : helicalQuads) {
				if (quad != null)
					quad.displayMaterials(drawable);
			}
		}
	}

	private List<Quad> helicalQuads;
	
	private void precomputeDisplayList() {

		boolean displayBlockedCached = displayBlocked;
		displayBlocked = true;
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Quad> helicalQuads = new LinkedList<Quad>();
		
		Triplet<Point3d, Point3d, Point3d> previous = null;
		
		int materialCount = interpolated ? 4 : 1;
		
		GLMaterial baseMaterial = new GLMaterial(0, 0, 0, 0);
		
		for (Triplet<Point3d, Point3d, Point3d> point : helix) {
			Point3d pLeft = point.getLeft();
			Point3d pMid = point.getCenter();
			Point3d pRight = point.getRight();
			
			if (pLeft == null || pMid == null || pRight == null)
				continue;

			if (previous != null) {
				Point3d prevLeft = previous.getLeft();
				Point3d prevRight = previous.getRight();

				QuadNonPlanar quad = new QuadNonPlanar(prevLeft, pLeft, pRight, prevRight);
				int Ni = N;
				int Nj = Ni;
				Quad[][] grid = quad.subdivide(Ni, Nj);
				for (int gi = 0; gi < Ni; gi++)
					for (int gj = 0; gj < Nj; gj++) {
						Point3d p = grid[gi][gj].getPoint();
						Point3i pi = MathToolset.tuple3dTo3i(p);
						if (!mask.hasPoint(pi) || mask.isMasked(pi)) 
							continue;

						
						GLMaterial[] materials = new GLMaterial[materialCount];
						for (int i = 0; i < materialCount; i++)
							materials[i] = new GLMaterial(baseMaterial);

						if (interpolated) {
							
							assign(grid[gi][gj].p1, materials[0]);
							assign(grid[gi][gj].p2, materials[1]);
							assign(grid[gi][gj].p3, materials[2]);
							assign(grid[gi][gj].p4, materials[3]);
							grid[gi][gj].setMaterials(materials);
						}
						else {
							assign(grid[gi][gj].getPoint(), materials[0]);
							grid[gi][gj].setMaterials(materials);
						}
						
						helicalQuads.add(grid[gi][gj]);
					}
				
			}
			
			previous = point;
		}
		
		this.helicalQuads = helicalQuads;
		
		displayBlocked = displayBlockedCached;
	}
	
	private void assign(Point3d p, GLMaterial material) {
		if (sphericalColors && fieldColor != null) {
			// Look up fiber orientation
			Point3i pi = MathToolset.tuple3dTo3i(p);
			double vx = fieldColor.getVolumes()[0].get(pi.x, pi.y, pi.z);
			double vy = fieldColor.getVolumes()[1].get(pi.x, pi.y, pi.z);
			double vz = fieldColor.getVolumes()[2].get(pi.x, pi.y, pi.z);
			float thetaNormalized = (float) (Math.acos(vz) / Math.PI);
			double phi = Math.abs(Math.atan2(vy, vx) / (2*Math.PI));
			float phiNormalized = (float) (phi >= 0 ? phi : phi + 1);
			
//			System.out.println(phi + " -> " + phiNormalized);
			
			/*
			 *  This is the symmetrical Pierpaoli representation
			 */
			Color c = new Color(Color.HSBtoRGB(thetaNormalized, 1f, 1f));
//			Color c = new Color(Color.HSBtoRGB(thetaNormalized, 1f - phiNormalized, 1f));
			material.diffuse[0] = c.getRed() / 255f;
			material.diffuse[1] = c.getGreen() / 255f;
			material.diffuse[2] = c.getBlue() / 255f;
			material.diffuse[3] = (float) transparency;
		}
		else {
			double data = volume.get(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z));
			Color c = colorMap.getColor(data);
			if (c == null) return;
			material.diffuse[0] = c.getRed() / 255f;
			material.diffuse[1] = c.getGreen() / 255f;
			material.diffuse[2] = c.getBlue() / 255f;
			material.diffuse[3] = (float) transparency;
		}
	}
}
