package app.pami2013.core.tools;

import gl.renderer.SceneRotator.ViewAngle;
import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import system.object.Pair;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import tools.loader.LineWriter;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethod;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.experiments.PamiExperimentRunner;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.setup.PamiExperimentSetup.OutputTableDefinition;
import app.pami2013.core.setup.computation.AllComputationSetup;
import extension.latex.LatexTable;
import extension.matlab.AxisStyle;
import extension.matlab.MatlabIO;
import extension.matlab.MatlabScript;

public class PamiOutputManager {
	public static double HistogramLineWidthSubject = 1;
	public static double HistogramLineWidthFusedVolume = 3;
	public static int FontSizeLegend = 24;
	public static int FontSizeAxisLabel = 24;
	public static int FontSizeAxisTicks = 32;
	public static final String HEADER_NAME = "header.txt";
	public static final String ScriptName = "MATLABscript.m";

	public static boolean showLabelX = false;
	public static boolean showAxisY = true;
	
	/**
	 * Run a list of matlab scripts. Make sure they exit by terminating them with "exit();"
	 * @param scripts
	 */
	public static void runScripts(String outputPath, List<MatlabScript> scripts) {
		MatlabScript all = MatlabScript.concatenate(scripts);
//		// Make sure the script terminates with exit() and then execute it.
		all.setFilename(outputPath + ScriptName);
		all.exit();
//		all.save();
		all.run();
//		for (MatlabScript script : scripts) {
//			// Terminate the script
//			script.exit();
//
//			// Run it
//			script.run();
//		}		
	}

	public static MatlabScript plotConnectionsForErrors(List<HeartDefinition> hearts, String outputPath, FittingMethod fittingMethod, int neighborhoodSize) {
		List<MatlabScript> scriptsModels = new LinkedList<MatlabScript>();
//		List<CartanModel> forms = Arrays.asList(CartanModel.C121, CartanModel.C122, CartanModel.C123, CartanModel.C131, CartanModel.C132, CartanModel.C133);
//		List<CartanModel> forms = Arrays.asList(CartanModel.CONSTANT, CartanModel.C121, CartanModel.C122, CartanModel.C123, CartanModel.C131, CartanModel.C132, CartanModel.C133, CartanModel.C231, CartanModel.C232, CartanModel.C233);
		List<CartanModel> forms = Arrays.asList(CartanModel.C121, CartanModel.C122, CartanModel.C123, CartanModel.C131, CartanModel.C132, CartanModel.C133, CartanModel.C231, CartanModel.C232, CartanModel.C233);
		
		PamiExperimentSetup modelsPlotSetup = PamiExperimentSetup.fromList(ExperimentType.RunOnly, ExperimentType.CartanModel, Arrays.asList(fittingMethod), forms, Arrays.asList(new IterativeFilter(0, 0)), Arrays.asList(neighborhoodSize));
		FittingExperiment modelsExperiment = new FittingExperiment(hearts, modelsPlotSetup, CuttingPlane.FIT, outputPath);
		int fontSize = 32;
		int angle = 45;
		scriptsModels.add(PamiPlotter.barplot(modelsExperiment, Species.Rat, Parameter.error1, fontSize, "\\Delta\\epsilon_1 (radians)", angle, true));
		scriptsModels.add(PamiPlotter.barplot(modelsExperiment, Species.Rat, Parameter.error2, fontSize, "\\Delta\\epsilon_2 (radians)", angle, true));
		scriptsModels.add(PamiPlotter.barplot(modelsExperiment, Species.Rat, Parameter.error3, fontSize, "\\Delta\\epsilon_3 (radians)", angle, true));
		MatlabScript allModels = MatlabScript.concatenate(scriptsModels);
		
		return allModels;
	}

	public static MatlabScript plotDepthHist(String exportPath, String volumeName, HashMap<Integer, List<Double>> map, Pair<double[], double[]> thresholds, String ylabel) {
		MatlabScript script = new MatlabScript();

		/*
		 * Count all elements
		 */
		int n = 0;
		for (Integer depth : map.keySet()) {
			List<Double> values = map.get(depth);
			for (Double value : values) {
				n++;
			}
		}
		
		/*
		 * Assemble data
		 */
		double[][] data = new double[2][n];
		
		int element = 0;
		for (Integer depth : map.keySet()) {
			List<Double> values = map.get(depth);
			for (Double value : values) {
				data[0][element] = depth;
				data[1][element] = value;
				element++;
			}
		}	

		/*
		 * Save to temporary matlab files
		 */
		String fileX = null;
		String fileY = null;
		try {
			fileX = MatlabIO.save(data[0], true);
			fileY = MatlabIO.save(data[1], true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String dataDepth = "dataDepth";
		String dataValue = "dataValue";
		script.loadMatlabVolume(dataDepth, fileX);
		script.loadMatlabVolume(dataValue, fileY);

		// Apply thresholds
		if (thresholds != null) {
//			script.thresholdClamp(dataDepth, thresholds.getLeft());
//			script.thresholdClamp(dataValue, thresholds.getRight());
			script.writeLine("garbage = []");
			script.writeLine("garbage = [garbage, find("+dataDepth+"<"+thresholds.getLeft()[0]+" | "+dataDepth+">"+thresholds.getLeft()[1]+")]");
			script.writeLine("garbage = [garbage, find("+dataValue+"<"+thresholds.getRight()[0]+" | "+dataValue+">"+thresholds.getRight()[1]+")]");
			script.writeLine(dataDepth + "(garbage) = []");
			script.writeLine(dataValue + "(garbage) = []");
		}
		
		// Create data
		script.writeLine("data = zeros(numel(" + dataDepth + "), 2)");
		script.writeLine("data(:,1)=" + dataDepth + "(:)");
		script.writeLine("data(:,2)=" + dataValue + "(:)");
		
		/*
		 * Construct histogram
		 */

		script.writeLine("clf");
		
		int nbins = MathToolset.roundInt(Math.sqrt(n));
		script.writeLine("[n, c] = hist3(data, [max(data(:,1))," + nbins +"])");
		
		// Truncate first and last depths
		script.writeLine("n = n(2:end-1,2:end-1)");
		script.writeLine("c{1} = c{1}(2:end-1)");
		script.writeLine("c{2} = c{2}(2:end-1)");

		// Normalize the marginals
		script.write("for depth=1:size(n,1)\n");
//		script.writeLine("n(depth,:) = n(depth,:) ./ trapz(c{2}, n(depth,:))");
		script.writeLine("n(depth,:) = n(depth,:) ./ sum(n(depth,:))");
		script.write("end\n");
		
		// Show image
//		script.writeLine("[xb, yb] = meshgrid(1:size(n,1), 1:size(n,2))");
//		script.writeLine("h = pcolor(xb,yb,n')");
//		script.writeLine("set(h, 'EdgeColor','none','LineStyle','none')");
//		script.writeLine("shading flat");
		script.writeLine("imagesc(n')");
		
		script.writeLine("xticks = get(gca, 'xtick')");
		script.writeLine("yticks = get(gca, 'ytick')");
		
		// Resize with higher resolution (should fix issues with OSX's preview and other PDF viewers that perform PDF smoothing by default)
		// A factor of 10 should be enough
		script.writeLine("sfactor=10");
		script.writeLine("imagesc(imresize(n', sfactor, 'nearest'))");

		// Plot maxima
		script.writeLine("hold on");
		script.writeLine("maxes = zeros(size(n,1),1)");
		script.write("for depth=1:size(n,1)\n");
		script.writeLine("ns = n(depth,:)");
		script.writeLine("[gs, vind] = gaussfit(1:numel(ns),ns)");
//		script.writeLine("[vmax, vind] = max(n(depth,:))");
		script.writeLine("maxes(depth) = vind");
		script.write("end\n");
		script.writeLine("plot(sfactor * ((1:size(n,1))-0.5), sfactor * maxes, '-b', 'LineWidth', 2)");
		script.writeLine("plot(sfactor * ((1:size(n,1))-0.5), sfactor * maxes, '-o', 'LineWidth', 1, 'MarkerSize', 5, 'MarkerEdgeColor', 'k')");
		script.writeLine("hold off");

		// Set axis properties
		script.writeLine("set(gca,'YDir','normal')");
		script.writeLine("colormap(customap('hotinv'))");
		script.writeLine("colorbar");
		script.writeLine("axis on");
		script.writeLine("axis tight");
//		script.writeLine("set(gca,'layer','top')");
		script.writeLine("grid on");
		script.setFigureProperties(null, 24, 0, false, true);
		script.writeLine("set(gca, 'TickDir', 'in' , 'TickLength', [.03 .03])");
		
		// Set axes and round to 3 decimal places
		script.writeLine("set(gca,'XTickLabel',sprintf('%.3f |', 0.5 + c{1}(xticks)))");
		script.writeLine("set(gca,'YTickLabel',sprintf('%.3f |',c{2}(yticks)))");

		script.writeLine("ylabel(" + MatlabScript.toString(ylabel) +")");		
		
		// Could use epi/mid/endo labels instead:
		script.writeLine("lims=xlim;set(gca, 'xtick', [lims(1), (lims(2)-lims(1))/2, lims(2)]);set(gca,'XTickLabel',{'epi', 'mid', 'endo'})");
		
		script.export_fig(new File(exportPath + volumeName + ".pdf").getAbsolutePath(), true, false);

		// Set plot dimensions (max depth, value range)
//		script.writeLine("xlim([0 30])");
//		script.writeLine("ylim([0 30])");
		return script;
	}

	public static MatlabScript plotModelsVsNeighborhoodSize(List<HeartDefinition> hearts, String outputPath) {
		List<MatlabScript> scriptsModels = new LinkedList<MatlabScript>();
		List<CartanModel> selModels = Arrays.asList(CartanModel.FULL, CartanModel.HOMEOID, CartanModel.HELICOID, CartanModel.C123, CartanModel.CONSTANT);
		PamiExperimentSetup modelsPlotSetup = PamiExperimentSetup.fromList(ExperimentType.CartanModel, ExperimentType.NeighborhoodSize, Arrays.asList(FittingMethod.Optimized), selModels, Arrays.asList(new IterativeFilter(0, 0)), Arrays.asList(3, 5, 7));
		FittingExperiment modelsExperiment = new FittingExperiment(hearts, modelsPlotSetup, CuttingPlane.FIT, outputPath);
		int fontSizeAxis = 32;
		double aspectRatio = 1;
		scriptsModels.add(PamiPlotter.plot(modelsExperiment, Species.Rat, Parameter.error1, fontSizeAxis, "\\epsilon_1", aspectRatio));
		scriptsModels.add(PamiPlotter.plot(modelsExperiment, Species.Rat, Parameter.error2, fontSizeAxis, "\\epsilon_2", aspectRatio));
		scriptsModels.add(PamiPlotter.plot(modelsExperiment, Species.Rat, Parameter.error3, fontSizeAxis, "\\epsilon_3", aspectRatio));
		MatlabScript allModels = MatlabScript.concatenate(scriptsModels);
		
		return allModels;
	}

	public static MatlabScript plotMethodVsFiltering(List<HeartDefinition> hearts, String outputPath) {
		List<MatlabScript> scriptsModels = new LinkedList<MatlabScript>();
		PamiExperimentSetup modelsPlotSetup = PamiExperimentSetup.fromList(ExperimentType.FittingMethod, ExperimentType.Smoothing, Arrays.asList(FittingMethod.Direct, FittingMethod.Optimized), Arrays.asList(CartanModel.FULL), AllComputationSetup.filters, Arrays.asList(3));
		FittingExperiment modelsExperiment = new FittingExperiment(hearts, modelsPlotSetup, CuttingPlane.FIT, outputPath);
		int fontSizeAxis = 42;
		double aspectRatio = Double.NaN;
		for (Parameter p : Parameter.values()) {
			scriptsModels.add(PamiPlotter.plot(modelsExperiment, Species.Rat, p, fontSizeAxis, null, aspectRatio));
		}
		MatlabScript allModels = MatlabScript.concatenate(scriptsModels);
		
		return allModels;
	}

	public static MatlabScript plotConnectionsVsNeighborhood(List<HeartDefinition> hearts, String outputPath) {
		List<MatlabScript> scripts = new LinkedList<MatlabScript>();
		
		// Construct list of connection form models
		List<CartanModel> models = Arrays.asList(CartanModel.C121,CartanModel.C122,CartanModel.C123,CartanModel.C131,CartanModel.C132,CartanModel.C133,CartanModel.C231,CartanModel.C232,CartanModel.C233);
		
		PamiExperimentSetup plotSetup = PamiExperimentSetup.fromList(ExperimentType.NeighborhoodSize, ExperimentType.CartanModel, Arrays.asList(FittingMethod.Optimized), models, Arrays.asList(new IterativeFilter(0, 0)), Arrays.asList(3, 5, 7));
		FittingExperiment experiment = new FittingExperiment(hearts, plotSetup, CuttingPlane.FIT, outputPath);
		int fontSizeAxis = 32;
		double aspectRatio = 1;
		scripts.add(PamiPlotter.plot(experiment, Species.Rat, Parameter.error1, fontSizeAxis, "\\epsilon_1",aspectRatio));
		scripts.add(PamiPlotter.plot(experiment, Species.Rat, Parameter.error2, fontSizeAxis, "\\epsilon_2",aspectRatio));
		scripts.add(PamiPlotter.plot(experiment, Species.Rat, Parameter.error3, fontSizeAxis, "\\epsilon_3",aspectRatio));
		MatlabScript all = MatlabScript.concatenate(scripts);

		return all;
	}

	public static MatlabScript output3DSurface(FittingExperiment experiment, Species species, ViewAngle angle, Dimension size, Parameter parameter) {
//		experiment.reset();
//		experiment.nextSetup();
//		
//		String exportPath = new File(experiment.getImageOutputPath(species) + experiment.getParameterTag() + "/surface/" + parameter + "/" + angle + "/").getAbsolutePath() + "/";
//		String volumeVariableName = parameter.toString();
//		
//		MatlabScript script = new MatlabScript(exportPath + ScriptName);
//	
//		script.loadMatlabVolume(volumeVariableName, volumePath)
		
//		slice=image3dslice(C123, 'z');
//		[X,Y] = meshgrid(1:sy, 1:sx);
//		Z = -1.8 * ones(sx,sy);
//		clf
//		hold on
//		surf(slice,'EdgeColor','None','facecolor', 'interp');
//		surf(X, Y, Z,slice);
//		hold off
//
//		colormap(b2r(0.5 * min(slice(:)),0.5*max(slice(:))))
//		camlight(-30, 0); lighting phong
//		view(0, 75)
//		grid off
//		hidden off
//		axis off
//		axis square
//		set(gcf, 'Position', get(0,'Screensize'))
//		zoom(1.3)
//		set(gcf, 'Color', 'white');
//		drawnow
//		export_fig('./test.png', '-transparent', '-q101');
		return null;
	}

	public static List<MatlabScript> matlabHeartHistogramSmoothing(FittingExperiment experiment, HeartDefinition heart, ViewAngle angle, boolean asSurface) {
		List<MatlabScript> scripts = new ArrayList<MatlabScript>();
		experiment.reset();
		experiment.nextSetup();
		scripts.addAll(PamiOutputManager.matlabHeartHistogram(experiment, heart, angle, asSurface));
		experiment.nextSetup();
		scripts.addAll(PamiOutputManager.matlabHeartHistogram(experiment, heart, angle, asSurface));
		experiment.nextSetup();
		scripts.addAll(PamiOutputManager.matlabHeartHistogram(experiment, heart, angle, asSurface));
		return scripts;
	}

	public static List<MatlabScript> matlabHeartHistogram(FittingExperiment experiment, HeartDefinition heart, ViewAngle angle, boolean asSurface) {
		List<MatlabScript> scripts = new ArrayList<MatlabScript>();
		
		for (Parameter parameter : experiment.getFittingParameter().getCartanModel()) {
//		for (Parameter parameter : Parameter.errors()) {
			scripts.add(matlabHeartHistogram(experiment, heart, angle, parameter, asSurface));
		}
		
		return scripts;
	}
	/**
	 * 1. Select the first experiment found.
	 * 2. Plot all results for a species as subplots on the same figure, for a selected connection form.
	 * The selected view angle is used (currently only X, Y, or Z allowed)
	 * @param experiment
	 * @param species
	 * @param angle
	 * @param size the size of the matlab subplot grid.
	 */
	public static MatlabScript matlabHeartHistogram(
			FittingExperiment experiment, HeartDefinition heart,
			ViewAngle angle, Parameter parameter, boolean asSurface) {

		String exportPath = new File(
				experiment.getImageOutputPath(heart.species)
						+ experiment.getParameterTag() + "/" + heart.id + "/"
						+ angle + "/").getAbsolutePath()
				+ "/";
		String volumeVariableName = parameter.toString();

		MatlabScript script = new MatlabScript(exportPath + ScriptName);

		// Prepare subplot
		script.clearFigure();

		// Plot each heart

		// Set parameter path and variable name
		String volumePath = new File(experiment.getOutputPathVolume(heart,
				parameter)).getAbsolutePath();

		// Tell Matlab to load the volume
		if (experiment.isDegreesVolume(parameter))
			script.loadMatlabVolumeAsDegrees(volumeVariableName, volumePath);
		else
			script.loadMatlabVolume(volumeVariableName, volumePath);

		String slice = "slice";

		// Clamp the volume
		script.thresholdClamp(volumeVariableName,
				PamiExperimentRunner.getVolumeLimit(parameter, heart.species));

		if (!asSurface) {
			script.writeLine(volumeVariableName + "(isnan("
					+ volumeVariableName + "))=0");
			// Set view angle and display
			script.imageslice(slice, volumeVariableName, angle);
			script.setAxisStyle(AxisStyle.Image);
			script.setAxisProperties(false, false);
		} else {
			double azimuth = -45;
			double elevation = 30;
			script.imageslice(slice, volumeVariableName, angle);
			script.writeLine("[sx, sy] = size(" + slice + ")");
			script.writeLine("[X,Y] = meshgrid(1:sy, 1:sx)");
			script.writeLine("Z = -1.8 * ones(sx,sy)");
			// script.writeLine("clf");
			script.writeLine("hold on");
			script.writeLine("surf(" + slice
					+ ",'EdgeColor','None','facecolor', 'interp')");
			script.writeLine("surf(X, Y, Z, " + slice + ")");
			script.writeLine("hold off");
			// script.writeLine("colormap(b2r(0.5 * min(slice(:)),0.5*max(slice(:))))");
			script.writeLine("camlight(-30, 0); lighting phong");
			script.writeLine("view(" + azimuth + "," + elevation + ")");
			script.writeLine("grid off");
			script.writeLine("hidden off");
			script.writeLine("axis off");
			script.writeLine("axis square");
			script.writeLine("zoom(1.3)");
			script.writeLine("set(gcf, 'Color', 'white')");
		}

		if (Parameter.errors().contains(parameter)) {
			script.writeLine("colormap jet");
		} else
			script.writeLine("colormap(b2r(0.5 * min(" + slice
					+ "(:)),0.5*max(" + slice + "(:))))");

		script.writeLine("h=colorbar");
		script.writeLine("set(h, 'FontSize', 72)");

		if (asSurface) {
			script.writeLine("hsize = 0.7");
			script.writeLine("set(h, 'Position', [.8 (1-hsize) / 2 .04 hsize])");
		}

		// Fix the colormap to the volume range
		script.caxis(PamiExperimentRunner.getVolumeLimit(parameter, heart.species));

		script.writeLine("set(gcf, 'Position', get(0,'Screensize'))");

		// Export
		script.export_fig(exportPath + volumeVariableName + ".png", true, true);
		script.save();

		return script;
	}

	public static List<MatlabScript> multiplot(FittingExperiment experiment, Species species, ViewAngle angle, Dimension size, boolean asSurface) {
		List<MatlabScript> scripts = new LinkedList<MatlabScript>();
		for (Parameter parameter : Parameter.values())
			scripts.add(multiplot(experiment, species, angle, size, parameter, asSurface));
		
		return scripts;
	}
	
	/**
	 * 1. Select the first experiment found.
	 * 2. Plot all results for a species as subplots on the same figure, for a selected connection form.
	 * The selected view angle is used (currently only X, Y, or Z allowed)
	 * @param experiment
	 * @param species
	 * @param angle
	 * @param size the size of the matlab subplot grid.
	 */
	public static MatlabScript multiplot(FittingExperiment experiment, Species species, ViewAngle angle, Dimension size, Parameter parameter, boolean asSurface) {
		experiment.reset();
		experiment.nextSetup();
		
		String exportPath = new File(experiment.getImageOutputPath(species) + experiment.getParameterTag() + "/multiplot/" + parameter + "/" + angle + "/").getAbsolutePath() + "/";
		String volumeVariableName = parameter.toString();
		
		MatlabScript script = new MatlabScript(exportPath + ScriptName);

		// Prepare subplot
		script.clearFigure();
		
		// Plot each heart
		List<Heart> hearts = experiment.getSpecies(species);
		for (int heartIndex = 0; heartIndex < hearts.size(); heartIndex++) {
			
			// Stop if we are going beyond the selected figure size
			if (heartIndex >= size.height * size.width)
				break;
			
			// Set parameter path and variable name
			String volumePath = new File(experiment.getOutputPathVolume(hearts.get(heartIndex).getDefinition(), parameter)).getAbsolutePath();

			// Set subplot index
			script.subplot(size.height, size.width, (heartIndex+1));
			
			// Tell Matlab to load the volume
			if (experiment.isDegreesVolume(parameter))
				script.loadMatlabVolumeAsDegrees(volumeVariableName, volumePath);
			else
				script.loadMatlabVolume(volumeVariableName, volumePath);

			if (!asSurface) {
				script.writeLine(volumeVariableName + "(isnan(" + volumeVariableName +"))=0");
				// Set view angle and display
				script.imageslice(volumeVariableName, angle);
				script.setAxisStyle(AxisStyle.Image);
				script.setAxisProperties(false, false);
			}
			else {
				String slice = "slice";
				script.imageslice(slice, volumeVariableName, angle);
				script.writeLine("[sx, sy] = size(" + slice + ")");
				script.writeLine("[X,Y] = meshgrid(1:sy, 1:sx)");
				script.writeLine("Z = -1.8 * ones(sx,sy)");
//				script.writeLine("clf");
				script.writeLine("hold on");
				script.writeLine("surf(" + slice + ",'EdgeColor','None','facecolor', 'interp')");
				script.writeLine("surf(X, Y, Z, " + slice + ")");
				script.writeLine("hold off");
				script.writeLine("colormap(b2r(0.5 * min(slice(:)),0.5*max(slice(:))))");
				script.writeLine("camlight(-30, 0); lighting phong");
				script.writeLine("view(0, 75)");
				script.writeLine("grid off");
				script.writeLine("hidden off");
				script.writeLine("axis off");
				script.writeLine("axis square");
				script.writeLine("set(gcf, 'Position', get(0,'Screensize'))");
 				script.writeLine("zoom(1.3)");
				script.writeLine("set(gcf, 'Color', 'white')");
			}
		}

		script.writeLine("colormap(b2r(0.5 * min(" + volumeVariableName + "(:)),0.5*max(" + volumeVariableName + "(:))))");
		script.writeLine("h=subcolorbar(" + size.height + "," + size.width + "," + Math.min(size.width * size.height, hearts.size()) + ")");
		script.writeLine("set(h, 'FontSize', 32)");
		script.writeLine("set(gcf, 'Position', get(0,'Screensize'))");
		
		// Export
		script.export_fig(exportPath + volumeVariableName + ".png", true, true);
		script.save();
		
		return script;
	}
	
	public static List<MatlabScript> matlabPerSpeciesHistograms(FittingExperiment experiment, int bins, int smoothing) {
		return matlabPerSpeciesHistograms(experiment, bins, smoothing, false);
	}
	
	/**
	 * @param experiment
	 * @param bins
	 * @param smoothing
	 * @param enveloped whether an envelope should be used instead of plotting individual volumes
	 * @return
	 */
	public static List<MatlabScript> matlabPerSpeciesHistograms(FittingExperiment experiment, int bins, int smoothing, boolean enveloped) {

		// TODO: could pass this as a parameter and change the second "true" statement
		boolean addCombinedVolume = enveloped ? true : true;
		
		// Fetch species/list of hearts pairs.
		List<Pair<Species, List<Heart>>> allSpecies = experiment.getSubjectsPerSpecies();

		List<MatlabScript> scripts = new LinkedList<MatlabScript>();

		// Go through each experiment and build the scripts
		experiment.reset();
		while (experiment.nextSetup()) {
			String header = "Processing " + experiment.getFittingParameter().toString();
			System.out.println(header);
			
			/*
			 * First combine each species separately
			 */
			// Process one species at a time
			for (Pair<Species, List<Heart>> heartsPerSpecies : allSpecies) {

				// Species we are currently processing
				Species currentSpecies = heartsPerSpecies.getLeft();

				String exportPath = experiment.getImageOutputPath(currentSpecies) + "speciesHistograms/" + experiment.getParameterTag() + "/";

				System.out.println("Processing species [" + currentSpecies + "]");
				
				// Fetch list of hearts for this species
				List<Heart> hearts = heartsPerSpecies.getRight();
				
				// Fetch list of available volumes and volume limits
				List<String> volumeNames = experiment.listVolumeNames(true);
//				List<double[]> volumeLimits = experiment.getVolumeLimits();

				// Create the Matlab script
				MatlabScript script = new MatlabScript(exportPath + "MATLABscript.m");
				scripts.add(script);
				
				// Write species header
				script.header("Processing species [" + currentSpecies + "]");

				script.variable("bins", bins);
				script.variable("smoothing", smoothing);

				
				List<Color> volumeColors = enveloped ? MathToolset.getColors(3) : MathToolset.getColors(hearts.size() + (addCombinedVolume ? 1 : 0));
				
				for (Parameter parameter : experiment.getFittingParameter().getCartanModel()) {

					/*
					 *  Prepare for this figure
					 */
//					String volumeID = TextToolset.removeExtension(volumeNames.get(i));
					String volumeVariableName = parameter.toString();
//					boolean isErrorVolume = volumeID.indexOf("error") > 0;
					
					script.header("Processing [" + volumeVariableName + "]");

					if (enveloped) {
						// load and preprocess the fused volume
						String combinedPath = new File(experiment.getOutputVolumeFilepath(currentSpecies, parameter)).getAbsolutePath();
						if (experiment.isDegreesVolume(parameter))
							script.loadMatlabVolumeAsDegrees(volumeVariableName, combinedPath);
						else
							script.loadMatlabVolume(volumeVariableName, combinedPath);
						script.thresholdRemove(volumeVariableName, PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));

						// extract heights and centers for given number of bins
						script.histogram(volumeVariableName, bins, smoothing, volumeColors.get(0), HistogramLineWidthFusedVolume);
						
						// back up variable
						script.variable("heights_fused", "heights");
						script.variable("centers_fused", "centers");
						
						// set initial min and max vectors
						script.variable("vavg", "zeros(1, length(centers_fused))");
						script.variable("vmin", "1e9 * ones(1, length(centers_fused))");
						script.variable("vmax", "-1e9 * ones(1, length(centers_fused))");

						// now, for each heart
						// load the current volume
						// compute heights given the fused centers
						// update min and max
						
						// Process all hearts for this species
						for (int heartIndex = 0; heartIndex < hearts.size(); heartIndex++) {
							/*
							 * Construct output ID and path
							 */
							String volumePath = new File(experiment.getOutputPathVolumeCompact(hearts.get(heartIndex).getDefinition(), parameter)).getAbsolutePath();

							// Load and preprocess the matlab volume
							if (experiment.isDegreesVolume(parameter))
								script.loadMatlabVolumeAsDegrees(volumeVariableName, volumePath);
							else
								script.loadMatlabVolume(volumeVariableName, volumePath);

							script.thresholdRemove(volumeVariableName, PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));

							// re-use fused centers
							script.histogram(
									volumeVariableName,
									"centers_fused",
									"smoothing",
									Color.black,
									HistogramLineWidthSubject);
							
							// update min/max/average
							script.writeLine("vmin = min(heights, vmin)");
							script.writeLine("vmax = max(heights, vmax)");
							script.writeLine("vavg = vavg + heights ./ " + hearts.size());
						}
						
						// Now we can plot
						script.clearFigure();
						script.hold(true);
						script.writeLine("plot(centers_fused, vmin, '--', 'LineWidth', 2, 'Color', " + MatlabScript.toString(volumeColors.get(0)) +")");
//						script.writeLine("plot(centers_fused, heights_fused, 'LineWidth', 3, 'Color', " + MatlabScript.toString(Color.black) +")");
						script.writeLine("plot(centers_fused, vavg, 'LineWidth', 3, 'Color', " + MatlabScript.toString(Color.black) +")");
						script.writeLine("plot(centers_fused, vmax, '--', 'LineWidth', 2, 'Color', " + MatlabScript.toString(volumeColors.get(2)) +")");
						script.hold(false);
						
					}
					else {
						script.clearFigure();
						
						// Hold on figure drawing
						script.hold(true);
						
						// Process all hearts for this species
						for (int heartIndex = 0; heartIndex < hearts.size(); heartIndex++) {
						/*
						 *  Fetch path for this heart
						 */
						
							/*
							 *  Construct output ID and path
							 */
							String volumePath = new File(experiment.getOutputPathVolumeCompact(hearts.get(heartIndex).getDefinition(), parameter)).getAbsolutePath();
							
							// Load the matlab volume
							if (experiment.isDegreesVolume(parameter))
								script.loadMatlabVolumeAsDegrees(volumeVariableName, volumePath);
							else
								script.loadMatlabVolume(volumeVariableName, volumePath);

							script.thresholdRemove(volumeVariableName, PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));

							// Create histogram
							// Color index 0 is reserved for the species combined volume
							script.histogram(volumeVariableName, "bins", "smoothing", volumeColors.get(heartIndex + (addCombinedVolume ? 1 : 0)), HistogramLineWidthSubject);
						}

						if (addCombinedVolume) {
							// First process the combined (mean) volume
							String combinedPath = new File(experiment.getOutputVolumeFilepath(currentSpecies, parameter)).getAbsolutePath();
							
							if (experiment.isDegreesVolume(parameter))
								script.loadMatlabVolumeAsDegrees(volumeVariableName, combinedPath);
							else
								script.loadMatlabVolume(volumeVariableName, combinedPath);
							
							script.thresholdRemove(volumeVariableName, PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));
							script.histogram(volumeVariableName, bins, smoothing, volumeColors.get(0), HistogramLineWidthFusedVolume);
						}

						// Stop hold off
						script.hold(false);
					}
					
					// Tighten X limits
					if (enveloped) {
						double[] xlims = PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies);
						script.writeLine("xlim([max(" + xlims[0] + ", min(centers_fused(:))), min(" + xlims[1] + ", max(centers_fused(:)))])");
					}
					else
						script.xlim(PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));

					// Add legend
					List<String> legend = new ArrayList<String>();
					if (enveloped) {
						legend.add("min");
						legend.add("fused");
						legend.add("max");
					}
					else {
						legend.addAll(experiment.toString(hearts));
						
						if (addCombinedVolume)
							legend.add("fused");
					}
										
					script.legend(legend, FontSizeLegend);
					String xlabel = volumeVariableName + " " + experiment.getUnit(parameter);
					script.setFigureProperties(xlabel, FontSizeAxisTicks, FontSizeAxisTicks, showLabelX, showAxisY);

					// Export figure
					script.export_fig(new File(exportPath + volumeVariableName + ".pdf").getAbsolutePath(), true, false);
				}
				
				script.save();
			}
		}
		
		return scripts;
	}

	public static void combine(final FittingExperiment experiment, boolean combineSpecies, double ZcutPercent) {

		// Fetch species/list of hearts pairs.
		List<Pair<Species, List<Heart>>> allSpecies = experiment.getSubjectsPerSpecies();

		experiment.reset();
		while (!experiment.getExperimentSetup().finished()) {
			experiment.nextSetup();
			String header = "Combining [" + experiment.getFittingParameter().getCartanModel() + "] using [" + experiment.getFittingParameter().getFittingMethod() + "]";
			System.out.println(header);
			
			/*
			 * First combine each species separately
			 */
			// Process one species at a time
			for (Pair<Species, List<Heart>> heartsPerSpecies : allSpecies) {

				// This is the species we are currently processing
				Species currentSpecies = heartsPerSpecies.getLeft();

				System.out.println("Processing species [" + currentSpecies + "]");
				
				// Fetch list of hearts for this species
				List<Heart> hearts = heartsPerSpecies.getRight();
				
				Map<Parameter, double[]> combinedVolumes = null;

				// Initilize the combined volumes if necessary
				if (combinedVolumes == null) {
					combinedVolumes = new HashMap<Parameter, double[]>();

					for (Parameter parameter : experiment.getFittingParameter().getCartanModel()) {
//						combinedVolumes.add(new double[] { });
						combinedVolumes.put(parameter, new double[] { });
					}
				}

				// Fetch list of available volumes
//				List<String> volumeNames = experiment.listVolumeNames(true);
				
				int volumeCount = experiment.getFittingParameter().getCartanModel().getNumberOfParameters();
				
				// Process each separately
				for (final Heart heart : hearts) {

					// Create a new mask for the volume nodes we are keeping
					// (mask + cutting plane volume + non-boundary nodes)
					// TODO: need to reload the mask using MatlabIO
					final IntensityVolumeMask mask = heart.loadMask();
					final IntensityVolumeMask maskStrict = new IntensityVolumeMask(mask.getDimension());
					final VoxelBox box = new VoxelBox(mask.getDimension());
					box.setMask(mask);
					box.cut(experiment.getCuttingPlane());
					box.sliceAxis(FrameAxis.Z, ZcutPercent);
					box.computeVoxelListUpdate();
					box.voxelProcess(new PerVoxelMethod() {
						
						@Override
						public void process(int x, int y, int z) {
							if (FittingData.isBoundary(box, experiment.getFittingParameter(), x, y, z))
								maskStrict.set(x, y, z, 0);
							else
								maskStrict.set(x, y, z, mask.get(x, y, z));
						}
						
						@Override
						public boolean isValid(Point3d origin, Vector3d span) {
							return true;
						}
					});

					String heartBasePath = experiment.getOutputPath(heart.getDefinition());
					// Add current heart data
//					for (int i = 0; i < combinedVolumes.size(); i++) {
					for (Parameter parameter : experiment.getFittingParameter().getCartanModel()) {
						// Load the volume to combine
						IntensityVolume volume = new IntensityVolume(MatlabIO.loadMAT(experiment.getOutputPathVolume(heart.getDefinition(), parameter)));

						// Flatten the volume
						double[] volumeFlat = volume.flattenCompact(maskStrict);
						int volumeFlatCount = volumeFlat.length;
						
						// Combine with previous data
						int combinedLength = combinedVolumes.get(parameter).length;
						
						double[] combinedVolume = Arrays.copyOf(combinedVolumes.get(parameter), combinedLength + volumeFlatCount);
						System.arraycopy(volumeFlat, 0, combinedVolume, combinedLength, volumeFlatCount);
						combinedVolumes.put(parameter, combinedVolume);
					}
				}
				
				/*
				 * Save combined volumes.
				 */
				for (Parameter parameter : experiment.getFittingParameter().getCartanModel()) {
					
					MatlabIO.save(experiment.getOutputVolumeFilepath(currentSpecies, parameter), combinedVolumes.get(parameter), true);
				}
				
				// Save experiment information
				String info = "";
				info += experiment.toString();
				info += experiment.getFittingParameter().toString() + "\n";
				info += experiment.getStatistics(combinedVolumes);
				
				try {
					LineWriter.write(experiment.getOutputVolumeFilepath(currentSpecies) + HEADER_NAME, info);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			
			if (combineSpecies) {
				// TODO: combine species together here
			}
		}
	}
	
	/**
	 * Iterates over the target parameter type, and for each value, plot multiple experiments 
	 * on the same figure. Repeat this process for each connection form and error, resulting
	 * in 12 (9 cijks + 3 errors) figures in total.
	 * TODO: need to rewrite for loop order: need to go through all experiments in same script for one connection form at a time.
	 * @param experiment
	 * @return
	 */
	public static List<MatlabScript> outputMatlab(FittingExperiment experiment, int binCount, int smoothingMagnitude) {

		// Fetch species/list of hearts pairs.
		List<Pair<Species, List<Heart>>> allSpecies = experiment.getSubjectsPerSpecies();
		
		// Create list of colors for our experiment variables
		List<Color> volumeColors = MathToolset.getColors(experiment.getExperimentSetup().countVariables());

		List<String> legendNames = experiment.getExperimentSetup().getTargetVariableNamesMatlabFormatted();

		// Experiment target tag (e.g. cartanmethodVSsmoothing 
		String targetLabel = experiment.getTargetTag();
		
		/*
		 * Process each species separately
		 * FIXME: eventually write separate method to combine multiple species
		 */
		List<MatlabScript> scripts = new ArrayList<MatlabScript>();
		for (Pair<Species, List<Heart>> heartsPerSpecies : allSpecies) {
			// Fetch current species
			Species currentSpecies = heartsPerSpecies.getLeft();

			// Construct output path for this species and parameter
			String exportPath = new File(experiment.getImageOutputPath(currentSpecies)).getAbsolutePath() + "/" + targetLabel + "/";

			// Create the script that will plot the experiments for this species
			MatlabScript script = new MatlabScript(exportPath + ScriptName);
			scripts.add(script);

			/*
			 * Try all connection volumes
			 */
			for (Parameter parameter : CartanModel.FULL) {

				// Volume name
				String volumeVariableName = parameter.toString();

				// Write volume header
				script.header("Processing volume [" + parameter.toString().toLowerCase() + "]");
				
				// Histogram parameters
				// TODO: set based on number of voxels in volume
				String bins = "bins";
				String smoothing = "smoothing";
				script.variable(bins, binCount);
				script.variable(smoothing, smoothingMagnitude);

				// Iterate over experiment parameters and variables
				experiment.reset();
				List<String> legend = new ArrayList<String>();
				while (experiment.nextParameter()) {
					
					String experimentPath = exportPath + experiment.getTargetParameterTag() + "/";
							
					/*
					 *  Prepare for this figure
					 */
					script.clearFigure();
					script.hold(true);

					int variableIndex = 0;
					while (experiment.nextVariable()) {
						
						// Skip this variable if the connection form does not exist form it.
						if (!experiment.getFittingParameter().getCartanModel().contains(parameter))
							continue;

						// Add to valid legend names
						legend.add(legendNames.get(variableIndex));
						
						System.out.println(parameter.toString() + ": "  + experiment.getExperimentSetup().toString());
						script.header("Loading " + experiment.getExperimentSetup().toString());
						
						// Fetch the volume to plot
						String volumePath = new File(experiment.getOutputVolumeFilepath(currentSpecies, parameter)).getAbsolutePath();
						script.loadMatlabVolume(volumeVariableName, volumePath);
						
						// Apply a threshold on the volume
						script.thresholdRemove(volumeVariableName, PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));
						
						// Plot the histogram
						script.histogram(volumeVariableName, bins, smoothing, volumeColors.get(variableIndex++), HistogramLineWidthFusedVolume);
					}
					experiment.getExperimentSetup().rewindVariables();

					script.hold(false);

					// Tighten X limits
					script.xlim(PamiExperimentRunner.getVolumeLimit(parameter, currentSpecies));
					
					// Add legend
					script.legend(legend, FontSizeLegend);
					String xlabel = volumeVariableName + " " + experiment.getUnit(parameter);
					script.setFigureProperties(xlabel, FontSizeAxisTicks, FontSizeAxisLabel, showLabelX, showAxisY);

					// Export figure
					script.export_fig(new File(experimentPath + volumeVariableName + ".pdf").getAbsolutePath(), true, false);

				}
				
				script.save();				
			}
		}
				
		return scripts;
	}
	
	/**
	 * TODO: use partial horizontal lines, e.g. \cline{2-7}
	 * TODO: pass list of species and iterate over species are create separate table for each
	 * @param setup
	 * @param species
	 * @param parameters
	 */
	public static LatexTable createLatexTable(FittingExperiment experiment, Species species) {

		PamiExperimentSetup setup = experiment.getExperimentSetup();
		OutputTableDefinition tableDefinition = setup.getTableDefinition();
		List<Parameter> parameters = tableDefinition.selectedParameters;
		
		// List all variables
		List<Integer> neighborhoods = setup.getNeighborhoodSizes();
		List<FittingMethod> methods = setup.getFittingMethods();
		List<CartanModel> models = setup.getModels();
		List<IterativeFilter> filters = setup.getFilters();
		
		// Create header names only when more than 1 element is present
		List<String> headerTitles = new ArrayList<String>();
		
		if (filters.size() > 1)
			headerTitles.add(ExperimentType.Smoothing.toStringLatex());
		
		if (neighborhoods.size() > 1)
			headerTitles.add(ExperimentType.NeighborhoodSize.toStringLatex());

		if (methods.size() > 1)
			headerTitles.add(ExperimentType.FittingMethod.toStringLatex());

		if (models.size() > 1)
			headerTitles.add(ExperimentType.CartanModel.toStringLatex());

		for (Parameter parameter : parameters)
			headerTitles.add(parameter.toStringLatex());

		// Create the table with selected headers
		LatexTable table = new LatexTable(headerTitles);

		// Create caption
		table.setCaption(tableDefinition.caption);
		table.setLabel(tableDefinition.label);
		
		DecimalFormat format = new DecimalFormat("#.000");
		format.setRoundingMode(RoundingMode.HALF_UP);

		// Assemble table
		boolean horizontalSeparatorAdded = false;
		int numColumns = headerTitles.size();
		int columnIndex;
		for (int i = 0; i < filters.size(); i++) {
			columnIndex = 0;
			if (filters.size() > 1)
				horizontalSeparatorAdded = false;
			
			IterativeFilter filter = filters.get(i);
			String filterTag = filter.toStringLatex();
			
			for (int j = 0; j < neighborhoods.size(); j++) {
				columnIndex = 1;

				if (!(filters.size() > 1) && neighborhoods.size() > 1)
					horizontalSeparatorAdded = false;
//				if (neighborhoods.size() > 1)
//					horizontalSeparatorAdded = false;

				int nsize = neighborhoods.get(j);
				String nsizeTag = String.valueOf(nsize);
				
				for (int k = 0; k < methods.size(); k++) {
					columnIndex = 2;

					if (!(filters.size() > 1 || neighborhoods.size() > 1) && methods.size() > 1)
						horizontalSeparatorAdded = false;

					FittingMethod method = methods.get(k);
					String methodTag = method.toString().toLowerCase();

					if (k > 0) {
						filterTag = "";
						nsizeTag = "";
					}
					
					for (int l = 0; l < models.size(); l++) {
						columnIndex = 3;
						
						CartanModel model = models.get(l);
						String modelTag = model.toString().toLowerCase();
						
						if (l > 0) {
							methodTag = "";
							filterTag = "";
							nsizeTag = "";
						}

						table.beginRow();
						
						// Add experiment elements (filters, neighborhood size, models)
						if (filters.size() > 1)
								table.addElement(filterTag);
						if (neighborhoods.size() > 1)
								table.addElement(nsizeTag);
						if (methods.size() > 1)
								table.addElement(methodTag);
						if (models.size() > 1)
								table.addElement(modelTag);
						
						// Add connection form elements
						for (Parameter parameter : parameters) {
							// Fetch the corresponding matlab volume
							String volumePath = experiment.getOutputPath(species, parameter, model, method, nsize, filter);
//							System.out.println(volumePath);
							if (new File(volumePath).exists()) {
								IntensityVolume volume = new IntensityVolume(MatlabIO.loadMAT(volumePath));
								double[] volumeArray = volume.flatten();
								
								double mean = MathToolset.mean(volumeArray);
								double std = MathToolset.std(volumeArray);
								
								table.addElement("$" + format.format(mean) + "\\pm" + format.format(std) + "$");
							}
							else {
								table.addElement("N/A");
							}
							
						}
						
						// Add horizontal separator
						if (filters.size() > 1 && !horizontalSeparatorAdded) {
							horizontalSeparatorAdded = true;
							table.hline(true);
						}
						if (neighborhoods.size() > 1 && !horizontalSeparatorAdded) {
							horizontalSeparatorAdded = true;
							table.hline(true);
						}
						if (models.size() > 1 && !horizontalSeparatorAdded) {
							horizontalSeparatorAdded = true;
							table.hline(true);
						}

						table.endRow();
						
						// end of model iteration
						columnIndex = 4;
					} 
					// end of method iteration
					columnIndex = 3;

//					if (j > 0 && k == 0 && neighborhoods.size() > 1)
//						table.cline(columnIndex - 1, numColumns);
				} 
				// end of neighborhood iteration
				columnIndex = 2;

//				if (j == 0 && filters.size() > 1)
//					table.cline(columnIndex - 1, numColumns);
			}
			// end of filter iteration
//			columnIndex = 1;
//			if (i == 0 && filters.size() > 1)
//				table.cline(columnIndex - 1, numColumns);
		}
		
		if (tableDefinition.transpose) {
			table.transpose();
		}
		
		System.out.println(table.assemble());		
		
		return table;
	}
	
}
