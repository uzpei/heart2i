package app.pami2013.core.tools;

import gl.renderer.JoglScene;
import heart.Heart;
import heart.HeartManager;
import heart.Tractography;

import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import math.matrix.Matrix3d;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox.CuttingPlane;

public class ManifoldTracer extends JoglScene {

    private HeartManager manager;
    private BooleanParameter trace = new BooleanParameter("trace manifold", false);
//    private IntensityVolume[][][] oneForms;
    private Tractography tractography;
    
	protected ManifoldTracer() {
		super("Manifold Tracer");

		tractography = new Tractography();
    	
		super.renderer.getControlFrame().add("Tracto", tractography.getControls());

		load();
		
		super.render();
	}

    private void load() {
    	if (manager == null) {
    		manager = new HeartManager(true);
    	}
    	
    	Heart heart = manager.getCurrent();
    	if (heart == null) return;

    	System.out.println("Loading heart from " + heart.getDirectory());

    	tractography.update(heart);
    	
    	// Remove previous interactor
			super.renderer.removeInteractor(heart.getVoxelBox());

    	// Add interactors    	
        super.renderer.addInteractor(heart.getVoxelBox());
        
        heart.getVoxelBox().addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateTrace();
			}
		});
        
    	heart.getVoxelBox().cut(CuttingPlane.CUT);
    }
    
    private void updateTrace() {
		// Expand (trace) the manifold generated from a single point
		Heart heart = manager.getCurrent();
		if (heart != null && trace.getValue()) {

			// Number of steps
			int n = tractography.getNumSteps();
			double df = tractography.getStepSize();
			LinkedList<LinkedList<Point3d>> all = new LinkedList<LinkedList<Point3d>>();
			
			// Trace the manifold starting from all points in the current voxel box
			for (Point3i pt0 : heart.getVoxelBox().getVolume()) {
				
				// Make sure this voxel is valid
				if (heart.getVoxelBox().dimensionContains(pt0)
						&& !heart.getMask().isMasked(pt0)) {
				
					// Trace in both directions
					for (int direction : new int[] { 1, -1 }) {
						LinkedList<Point3d> streamline = new LinkedList<Point3d>();

						// Get f_1 at the voxel, normalize it, and set its direction (heading)
						Vector3d f1 = heart.getFrameField().getF1()
								.getVector3d(pt0.x, pt0.y, pt0.z);
						f1.normalize();
						f1.scale(direction);

						// Trace the manifold
						Point3d p0 = new Point3d(pt0.x, pt0.y, pt0.z);
						Vector3d f = new Vector3d(f1);

						streamline.add(new Point3d(p0));
						
						for (int i = 0; i < n; i++) {
							Point3i p0i = MathToolset.tuple3dTo3i(p0);

							// Stop if we leave the volume
							if (!heart.getVoxelBox().dimensionContains(p0i)
									|| heart.getMask().isMasked(p0i))
								break;

							Vector3d f2 = heart.getFrameField().getF2()
									.getVector3d(p0i.x, p0i.y, p0i.z);
							Vector3d f3 = heart.getFrameField().getF3()
									.getVector3d(p0i.x, p0i.y, p0i.z);
							
							// Always re-use the connection forms at the point of expansion
							double c12h = DifferentialOneForm.computeOneForms(
									heart.getFrameField().getF1(), heart
											.getFrameField().getF2(), f,
									pt0);
							double c13h = DifferentialOneForm.computeOneForms(
									heart.getFrameField().getF1(), heart
											.getFrameField().getF3(), f,
									pt0);
							f2.scale(c12h);
							f3.scale(c13h);
							f.add(f2);
							f.add(f3);
							f.normalize();

//							p0.add(f);
							p0.scaleAdd(df, f, p0);

							streamline.add(new Point3d(p0));
						}
						all.add(streamline);
					}
				}
			}
			
			tractography.setTractography(all);
		}
    }
    
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		if (manager.getCurrent() != null) {
			manager.getCurrent().display(drawable);

			if (trace.getValue()) {
				gl.glPushMatrix();
				manager.getCurrent().getVoxelBox().applyHeartNormalization(gl);
				tractography.display(drawable, true);
				gl.glPopMatrix();

			}
		}
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(trace.getControls());
		vfp.add(manager);
		
		return vfp.getPanel();
	}

	public static void main(String[] args) {
		new ManifoldTracer();
	}
}
