package app.pami2013.core.tools;

import heart.Heart.Species;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import diffgeom.fitting.experiment.FittingExperiment;
import diffgeom.fitting.experiment.FittingExperiment.ExperimentType;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.setup.PamiExperimentSetup.ParameterDefinition;
import extension.matlab.MatlabIO;
import extension.matlab.MatlabScript;

public class PamiPlotter {

	public static MatlabScript plot(FittingExperiment experiment, Species species, Parameter yValues, int fontSizeAxis, String ylabel, double aspectRatio) {
		return plot(experiment, species, yValues, fontSizeAxis, ylabel, 0, false, aspectRatio);
	}
	
	public static MatlabScript plot(FittingExperiment experiment, Species species, Parameter yValues, int fontSizeAxis, String ylabel, int xLabelAngle) {
		return plot(experiment, species, yValues, fontSizeAxis, ylabel, xLabelAngle, false, 1);
	}
	/**
	 * X axis: parameter (e.g. sigma_3, sigma_5, sigma_7) Y axis: mean parameter
	 * value (e.g. error_1) Curves: variable (e.g. cartan models: 1-form,
	 * helicoid, c123)
	 * Default value for xLabelFontSize should be around 32
	 * 
	 * @param experiment
	 * @param p
	 */
	public static MatlabScript plot(FittingExperiment experiment, Species species, Parameter yValues, int fontSizeAxis, String ylabel, int xLabelAngle, boolean barHistogram, double aspectRatio) {

		int fontSizeLegend = 24;
		int lineWidth = 2;
		String legendLocation = "NorthWest";
		boolean applyCrop = false;
		
		// Create script
		String exportPath = new File(experiment.getImageOutputPath(species)).getAbsolutePath() + "/" + experiment.getTargetTag() + "/";
		MatlabScript script = new MatlabScript(exportPath + PamiOutputManager.ScriptName);

		// List x labels
		experiment.reset();
		List<String> xLabels = new ArrayList<String>();
		int mindex = 0;
		while (experiment.nextParameter()) {
			if (experiment.getExperimentSetup().getTargetParameter() == ExperimentType.Smoothing)
				xLabels.add("$\\sigma_" + (mindex++) + "$");
			else
				xLabels.add(experiment.getDefinition().toStringMatlabXLabel(experiment.getExperimentSetup().getTargetParameter()));
		}
		
		// List legends
		experiment.reset();
		List<String> legend = new ArrayList<String>();
		while (experiment.nextVariable()) {
			// Add legend entry for this variable (e.g. cartan model
			legend.add(experiment.getDefinition().toStringMatlab(experiment.getExperimentSetup().getTargetVariable()));
		}
		
		// For each curve (e.g. Cartan model)
		experiment.reset();
		script.clearFigure();
		script.hold(true);

		List<Color> curveColors = MathToolset.getColors(experiment.getExperimentSetup().countVariables());
		
		// If there is a single curve, use black
		if (curveColors.size() == 1)
			curveColors.set(0, Color.black);
		
		int curveIndex = 0;
		while (experiment.nextVariable()) {
//			System.out.println("New variable: " + experiment.getDefinition().toString());
			
			// Assemble values and labels for this curve
			List<Double> parameterMeans = new ArrayList<Double>();
			List<Double> parameterStds = new ArrayList<Double>();
			experiment.getExperimentSetup().rewindParameters();
			
			while (experiment.nextParameter()) {
//				System.out.println("New parameter: " + experiment.getDefinition().toString());
				
				// Load the target volume for the current parameter and target
				// (e.g. error 1)
				String volumePath = experiment.getOutputPath(species, yValues, experiment.getDefinition());
				if (!(new File(volumePath).exists())) {
					parameterMeans.add(0d);
					parameterStds.add(0d);
					continue;
				}
				
				// Compute the mean of that volume
				double[] volumeArray = new IntensityVolume(
						MatlabIO.loadMAT(volumePath)).flatten();
				parameterMeans.add(MathToolset.mean(volumeArray));
				parameterStds.add(MathToolset.std(volumeArray));
			}
			
			// Plot the curve
			// TODO: also plot std as dotted line
			if (barHistogram) {
				script.plotBar(null, parameterMeans, Color.blue, 0.7); 
			}
			else {
				script.plot(parameterMeans, lineWidth, curveColors.get(curveIndex), true);
			}
			curveIndex++;
		}
		
		script.hold(false);

		// Set axis properties

		// Set only if there is more than one legend entry
		if (legend.size() > 1)
			script.legend(legend, fontSizeLegend, legendLocation);
		
		script.setFigureProperties(null, fontSizeAxis, 0, true, true);
		
		if (ylabel != null && ylabel.length() > 0)
			script.setYLabel(ylabel, fontSizeAxis);
		
//		script.fullscreen();
		if (!Double.isInfinite(aspectRatio))
			script.aspect(aspectRatio);
		
		script.setLatexAxisLabels(FrameAxis.X, xLabels, fontSizeAxis, xLabelAngle);
//		script.setAxisProperties(true, false);		
		script.export_fig(exportPath + "plot_" + yValues.toString().toLowerCase() + ".pdf", true, applyCrop);
		
		System.out.println(script.getBuffer());
		
//		script.exit();
//		script.save();
//		script.run();
		
		return script;
	}
	
	/**
	 * X axis: parameter (e.g. sigma_3, sigma_5, sigma_7) Y axis: mean parameter
	 * value (e.g. error_1) Curves: variable (e.g. cartan models: 1-form,
	 * helicoid, c123)
	 * Default value for xLabelFontSize should be around 32
	 * 
	 * @param experiment
	 * @param p
	 */
	public static MatlabScript barplot(FittingExperiment experiment, Species species, Parameter yValues, int fontSizeAxis, String ylabel, int xLabelAngle, boolean toConstantBaseline) {

		boolean applyCrop = true;
		int fontSizeLegend = 24;
		int lineWidth = 2;
		double aspectRatio = 0.7;
		String legendLocation = "NorthWest";
		
		// Create script
		String exportPath = new File(experiment.getImageOutputPath(species)).getAbsolutePath() + "/" + experiment.getTargetTag() + "/";
		MatlabScript script = new MatlabScript(exportPath + PamiOutputManager.ScriptName);

		// List x labels
		experiment.reset();
		List<String> xLabels = new ArrayList<String>();
		int mindex = 0;
		while (experiment.nextParameter()) {
			if (experiment.getExperimentSetup().getTargetParameter() == ExperimentType.Smoothing)
				xLabels.add("$\\sigma_" + (mindex++) + "$");
			else
				xLabels.add(experiment.getDefinition().toStringMatlabXLabel(experiment.getExperimentSetup().getTargetParameter()));
		}
		
		// List legends
		experiment.reset();
		List<String> legend = new ArrayList<String>();
		while (experiment.nextVariable()) {
			// Add legend entry for this variable (e.g. cartan model
			legend.add(experiment.getDefinition().toStringMatlab(experiment.getExperimentSetup().getTargetVariable()));
		}
		
		// For each curve (e.g. Cartan model)
		experiment.reset();
		script.clearFigure();
		script.hold(true);

		List<Color> curveColors = MathToolset.getColors(experiment.getExperimentSetup().countVariables());
		
		// If there is a single curve, use black
		if (curveColors.size() == 1)
			curveColors.set(0, Color.black);
		
		int curveIndex = 0;
		while (experiment.nextVariable()) {
//			System.out.println("New variable: " + experiment.getDefinition().toString());
			
			// Assemble values and labels for this curve
			List<Double> parameterMeans = new ArrayList<Double>();
			List<Double> parameterStds = new ArrayList<Double>();
			experiment.getExperimentSetup().rewindParameters();
			
			// Load the constant baseline
			IntensityVolume baseline = null;
			if (toConstantBaseline) {
				ParameterDefinition def = new ParameterDefinition(experiment.getExperimentSetup().getCurrentDefinition());
				def.model = CartanModel.CONSTANT;
				String volumePath = experiment.getOutputPath(species, yValues, def);
				baseline = new IntensityVolume(MatlabIO.loadMAT(volumePath));
			}
			
			// Next parameter = e.g. cartan model
			while (experiment.nextParameter()) {
//				System.out.println("New parameter: " + experiment.getDefinition().toString());
				
				// Load the target volume for the current parameter and target
				// (e.g. error 1)
				String volumePath = experiment.getOutputPath(species, yValues, experiment.getDefinition());
				if (!(new File(volumePath).exists())) {
					parameterMeans.add(0d);
					parameterStds.add(0d);
					continue;
				}
				
				// Compute the mean of that volume
				IntensityVolume volume = new IntensityVolume(MatlabIO.loadMAT(volumePath));
				
				// Subtract
				if (toConstantBaseline) {
//					volume = baseline.subOut(volume).asVolume();
					volume = volume.subOut(baseline).asVolume();
				}
				
				double[] volumeArray = volume.flatten();
				
				// Remove nans
				int valid = 0;
				for (int i = 0; i < volumeArray.length; i++) {
					if (!Double.isNaN(volumeArray[i]))
						valid++;	
				}
				
				double[] volumeArrayValid = new double[valid];
				int index = 0;
				for (int i = 0; i < volumeArray.length; i++) {
					if (!Double.isNaN(volumeArray[i]))
						volumeArrayValid[index++] = volumeArray[i];
				}

				parameterMeans.add(MathToolset.mean(volumeArrayValid));
				parameterStds.add(MathToolset.std(volumeArrayValid));
			}
			
			// Plot the curve
			// TODO: also plot std as dotted line
			script.plotBarDelta(parameterMeans, parameterStds, 0.5); 
			curveIndex++;
		}
		
		script.hold(false);

		// Set axis properties

		// Set only if there is more than one legend entry
		if (legend.size() > 1)
			script.legend(legend, fontSizeLegend, legendLocation);
		
		script.setFigureProperties(null, fontSizeAxis, 0, true, true);
		
		if (ylabel != null && ylabel.length() > 0)
			script.setYLabel(ylabel, fontSizeAxis);
		
//		script.fullscreen();
		script.aspect(aspectRatio);
		script.setLatexAxisLabels(FrameAxis.X, xLabels, fontSizeAxis, xLabelAngle);
//		script.setAxisProperties(true, false);		
		
		script.export_fig(exportPath + "plot_" + yValues.toString().toLowerCase() + ".pdf", true, applyCrop);

		System.out.println(script.getBuffer());
		
//		script.exit();
//		script.save();
//		script.run();
		
		return script;
	}
}
