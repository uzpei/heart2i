package app.pami2013.test;

import static org.junit.Assert.fail;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import org.junit.Test;

import diffgeom.DifferentialOneForm;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame;
import tools.geom.MathToolset;
import voxel.FramedVoxel;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;

public class ErrorMeasureTest {

	@Test
	public void test() {
		// Generate a synthetic frame field from a point of expansion using a specified connection form
		int[] dims = new int[] { 13, 13, 13 };

		// Idea is to use direct parameter computations to verify different cartan models
		// 1) Generate a random Maurer-Cartan parameter
		Point3i centerPoint = new Point3i(dims[0]/2, dims[1]/2, dims[2]/2);
//		CartanParameter randomParameter = CartanParameter.getRandom(1);
		CartanParameter randomParameter = new CartanParameter(new double[] { 0, 0, 1, 0, 0, 0, 0, 0, 0 });
		
		// 2) Generate a random frame of expansion
		OrientationFrame frame = new OrientationFrame();
		frame.randomize();
		FramedVoxel center = new FramedVoxel(new CoordinateFrame(frame, centerPoint));
		
		// 3) Expand frame field in different neighborhoods
		VoxelFrameField frameField = new VoxelFrameField(dims);
		for (int i = 0; i < dims[0]; i++) {
			for (int j = 0; j < dims[1]; j++) {
				for (int k = 0; k < dims[2]; k++) {
					Vector3d v = new Vector3d(i - centerPoint.x, j - centerPoint.y, k - centerPoint.z);
					
					FramedVoxel extrapolated = PseudolinearCartanFittingError.extrapolate(center, randomParameter, v);
					frameField.set(new Point3i(i, j, k), extrapolated.getFrame());
				}
			}
		}
		
		// 4) Estimate connection forms using direct computations
		CartanParameter estimated = DifferentialOneForm.computeOneFormParameter(frameField, centerPoint);
		
		// 5) Compare estimation with source
		System.out.println(randomParameter.toString());
		System.out.println(estimated.toString());
		
		fail("Not yet implemented");
	}
	

}
