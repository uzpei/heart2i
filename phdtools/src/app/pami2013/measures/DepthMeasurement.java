package app.pami2013.measures;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;
import heart.HeartManager;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelVectorField;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.tools.PamiOutputManager;
import extension.matlab.MatlabScript;

public class DepthMeasurement {

	private Heart heart;

	
	private List<LinkedList<Point3i>> paths = new LinkedList<LinkedList<Point3i>>();
	
	public DepthMeasurement(Heart heart) {
		this.heart = heart;
	}
	
	public List<LinkedList<Point3i>> integrate() {
		return integrate(heart, heart.getVoxelBox());
	}

	public static List<Point3i> shell(final IntensityVolume distanceMap, int distance, VoxelBox box) {
		final List<Point3i> shell = new LinkedList<Point3i>();
		final Integer currentShell = distance;
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
//				if (MathToolset.epsilonEqual(distanceMap.get(x, y, z), currentShell, epsilon)) {
				if (MathToolset.roundInt(distanceMap.get(x, y, z)) == currentShell) {
					shell.add(new Point3i(x, y, z));
				}
			}
		});

		return shell;
	}
	
	public static HashMap<Integer, List<Point3i>> shells(final IntensityVolume distanceMap, IntensityVolumeMask mask) {
		double[] minMax = distanceMap.getminMax();
		
		HashMap<Integer, List<Point3i>> shells = new HashMap<Integer, List<Point3i>>();
		
		VoxelBox box = new VoxelBox(mask);
		for (int distance = MathToolset.roundInt(minMax[0]); distance <= minMax[1]; distance++) {
			final List<Point3i> shell = shell(distanceMap, distance, box);
			shells.put(new Integer(distance), shell);
		}
		
		return shells;
	}
	
	public static HashMap<Integer, List<Double>> integrateShells(IntensityVolume data, IntensityVolume distanceMap, IntensityVolumeMask mask) {
		HashMap<Integer, List<Point3i>> shells = shells(distanceMap, mask);
		HashMap<Integer, List<Double>> map = new HashMap<Integer, List<Double>>();

		// Extract data shells
		for (Integer distance : shells.keySet()) {
			List<Point3i> shell = shells.get(distance);
			List<Double> values = new LinkedList<Double>();
			for (Point3i p : shell)
				values.add(data.get(p));
			
			map.put(distance, values);
		}
		
		return map;
	}
	
	public HashMap<Integer, List<Double>> integrateDepth(IntensityVolume volume) {
		List<LinkedList<Point3i>> paths = integrate();
		
		HashMap<Integer, List<Double>> depthMap = new HashMap<Integer, List<Double>>();
		
		/*
		 * Construct depth bins
		 */
		for (LinkedList<Point3i> path : paths) {
			int depth = 0;
			for (Point3i p : path) {
				if (!depthMap.containsKey(depth)) {
					depthMap.put(depth, new LinkedList<Double>());
				}
				
				depthMap.get(depth).add(volume.get(p));
				depth++;
			}
		}
		
		return depthMap;
	}
	
	/**
	 * Compute the mean depth & std for this heart in the volume defined by the voxel box by integrating along the distance transform from the cardiac outer wall (epicardium).
	 * @param heart
	 * @param box
	 * @return
	 */
	public List<LinkedList<Point3i>> integrate(Heart heart, VoxelBox box) {
		this.heart = heart;
		final List<Point3i> startPoints = new LinkedList<Point3i>();
		final IntensityVolume outerWall = heart.getDistanceTransformEpi();
		final IntensityVolume innerWall = heart.getDistanceTransformEndo();
		final double epsilon = 1;
		// Populate starting points
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				if (Math.abs(outerWall.get(x, y, z)) < epsilon) {
					startPoints.add(new Point3i(x, y ,z));
				}
			}
		});

		return integrate(startPoints, heart.getMask(), outerWall, innerWall);
	}
	
	public static List<LinkedList<Point3i>> integrate(List<Point3i> startPoints, IntensityVolumeMask mask, IntensityVolume outerWall, IntensityVolume innerWall) {
		
		/*
		 *  Integrate
		 */
		
		// TODO: Make these as parameter fields
		double epsilon = 0.5;
		int maxDepthIterations = 100;
		double step = 0.5;
		VoxelVectorField g = outerWall.gradient();
		Vector3d v = new Vector3d();
		
		System.out.println("Found " + startPoints.size() + " starting points.");
		
		List<LinkedList<Point3i>> paths = new LinkedList<LinkedList<Point3i>>();
		// For each starting point
		for (Point3i p : startPoints) {
			
			// Store as we go along
			Point3d pi = new Point3d(p.x, p.y, p.z);
			LinkedList<Point3i> midPoints = new LinkedList<Point3i>();
			g.getVector3d(pi, v);
			for (int i = 0; i < maxDepthIterations; i++) {
				// Current point
				Point3i pii = MathToolset.tuple3dTo3i(pi);
				
				// Stop if we moved out of the volume
				if (!mask.contains(pii.x, pii.y, pii.z)) {
//					System.out.println("Moving outside of volume at iteration " + i);
					break;
				}
				
				// Stop if we have reached the inner wall
				if (Math.abs(innerWall.get(pii.x, pii.y, pii.z)) < epsilon) {
//					System.out.println("Reached endocardium at iteration " + i);
					break;
				}
				
				// Otherwise add this point
				// if it hasn't been added already
				if (midPoints.size() == 0 || !midPoints.get(midPoints.size() - 1).equals(pii)) {
					midPoints.add(pii);
				}
				
				// Move in gradient direction
//				g.getVector3d(pii.x, pii.y, pii.z, v);
				pi.scaleAdd(step, v, pi);
			}
			
			// Add this path
			paths.add(midPoints);
		}
		
		return paths;
	}
	
	public double[] computeMean() {
		// Compute mean of all transmural paths
		double meanLength = 0;
		int count = 0;
		for (LinkedList<Point3i> path : paths) {
			if (path.size() == 0)
				continue;
			
			double length = MathToolset.tuple3iToPoint3d(path.get(0)).distance(MathToolset.tuple3iToPoint3d(path.get(path.size()-1)));
			meanLength += length;
			count++;
		}
		meanLength /= count;
		
		// Compute std
		double stdLength = 0;
		for (LinkedList<Point3i> path : paths) {
			if (path.size() == 0)
				continue;

			double length = MathToolset.tuple3iToPoint3d(path.get(0)).distance(MathToolset.tuple3iToPoint3d(path.get(path.size()-1)));
			stdLength += (meanLength - length) * (meanLength - length);
		}
		stdLength /= count;
		stdLength = Math.sqrt(stdLength);
		
		return new double[] { meanLength, stdLength };
	}
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor3d(0, 0, 1);
		for (LinkedList<Point3i> path : paths) {
			gl.glBegin(GL2.GL_LINE_STRIP);
			for (Point3i p : path) {
				gl.glVertex3d(p.x, p.y, p.z);
			}
			gl.glEnd();
		}
	}
	
	public static void main(String[] args) {
		HeartManager manager = new HeartManager();
		manager.loadHeart(new HeartDefinition("", Species.Rat, "./data/heart/rat/sampleSubset/"));
		Heart heart = manager.getCurrent();
		
		if (heart != null) {
			heart.getVoxelBox().cut(CuttingPlane.FIT);
//			DepthMeasurement measure = new DepthMeasurement(heart);
//			measure.integrate();
//			double depths[] = measure.computeMean();
//			System.out.println("Mean length = " + depths[0] + " +- " + depths[1]);
			
//			IntensityVolume data = heart.getDistanceTransformEpi();
//			data.round();
//			IntensityVolume data = heart.getFrameField().getFieldZ().getVolumes()[0];
			IntensityVolume data = DifferentialOneForm.computeOneForm(heart.getFrameField(), Parameter.C123).asVolume();
			HashMap<Integer, List<Double>> map = DepthMeasurement.integrateShells(data, heart.getDistanceTransformEpi(), heart.getMask());
			
			MatlabScript script = PamiOutputManager.plotDepthHist("./data/", "test", map, null);
			script.save("./data/depthTest.m");
//			script.exit();
//			script.run();
		}
	}
}
