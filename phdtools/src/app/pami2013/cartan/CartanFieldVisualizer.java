package app.pami2013.cartan;

import extension.matlab.MatlabIO;
import gl.renderer.JoglRenderer;

import java.util.List;

import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3i;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;
import volume.IntensityVolume;
import volume.VolumeRenderer;
import voxel.VoxelBox;
import app.pami2013.cartan.CartanParameter.CartanModel;

public class CartanFieldVisualizer {

	private static final CartanModel DEFAULT_CARTAN_MODEL = CartanModel.FULL;

	private static final CartanParameter.Parameter DEFAULT_SELECTED_PARAMETER = CartanParameter.Parameter.C123;

	private VoxelBox voxelBox;

	private List<CartanParameterNode> nodes;

	private EnumComboBox<CartanModel> ecbCartanModel = new EnumComboBox<CartanModel>(
			"Cartan model", DEFAULT_CARTAN_MODEL);
	
	private EnumComboBox<CartanParameter.Parameter> ecbCartanParameter = new EnumComboBox<CartanParameter.Parameter>(
			"Display parameter", DEFAULT_SELECTED_PARAMETER);

	private VolumeRenderer renderer;
	
	private IntensityVolume[] renderingVolumes = new IntensityVolume[CartanParameter.ConnectionFormCount+1];

	public CartanFieldVisualizer(VoxelBox box, List<CartanParameterNode> nodes) {

		this.voxelBox = box;
		this.nodes = nodes;
		
		setParameterListeners();
		
		fillHelicoidVolumes();
	}

	public void createRenderer(JoglRenderer jrenderer) {
		if (jrenderer != null)
			renderer = jrenderer.createVolumeRendererInstance();
		else
			renderer = new VolumeRenderer();
		
		renderer.setVeryLowResolution();
		setRenderingVolume();
	}

	public void setParameterListeners() {
		
		ParameterListener modelListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				setRenderingVolume();
			}
		};
		
		ecbCartanModel.addParameterListener(modelListener);
		ecbCartanParameter.addParameterListener(modelListener);
	}
	private void fillHelicoidVolumes() {
		
		// Parameters
		renderingVolumes = CartanFieldVisualizer.nodesToVolume(nodes, ecbCartanModel.getSelected(), voxelBox.getDimension());
	}
	
	public static IntensityVolume[] nodesToVolume(List<CartanParameterNode> nodes, CartanModel model, int[] dims) {
		// Create array holding intensity volumes
		// Number of volumes = number of cartan parameters in the model + 3 errors
		int numErrorVolumes = 3;
		IntensityVolume[] volumes = new IntensityVolume[model.getNumberOfParameters() + numErrorVolumes];
		
//		SimpleTimer timer = new SimpleTimer();

		for (int i = 0; i < model.getNumberOfParameters(); i++) {
//			System.out.println("Filling parameter " + CartanParameter.getLabel(model.getParameterMaskIndex(i)));
//			timer.tick();
			volumes[i] = new IntensityVolume(dims);
			volumes[i].fillWith(Double.NaN);
			
			for (CartanParameterNode node : nodes) {
				Point3i pt = node.getPosition();
				
				volumes[i].set(pt.x, pt.y, pt.z, node.get(model.getParameterMaskIndex(i)));
			}
//			System.out.println(timer.tick_s());
		}
		
		// Set error volumes
//		System.out.println("Filling error volumes");
//		timer.tick();
		for (int i = 0; i < numErrorVolumes; i++) {
			volumes[model.getNumberOfParameters() + i] = new IntensityVolume(dims);
			volumes[model.getNumberOfParameters() + i].fillWith(Double.NaN);
			for (CartanParameterNode node : nodes) {
				Point3i pt = node.getPosition();
				volumes[model.getNumberOfParameters() + i].set(pt.x, pt.y, pt.z, node.getError()[i]);
			}
//			System.out.println(timer.tick_s());
		}

		
		return volumes;
	}

	public static void exportVolumeNodes(String outdir, List<CartanParameterNode> nodes, CartanModel model, int[] dims) {
		// Create array holding intensity volumes
		// Number of volumes = number of cartan parameters in the model + 3 errors
		int numErrorVolumes = 3;
		IntensityVolume volume = new IntensityVolume(dims);
		volume.fillWith(Double.NaN);
		String prelbl = model.toString().toLowerCase() + "_";
		
		int parameterIndex = 0;
		for (CartanParameter.Parameter parameter : model) {
			for (CartanParameterNode node : nodes) {
				Point3i pt = node.getPosition();
				
				volume.set(pt.x, pt.y, pt.z, node.get(model.getParameterMaskIndex(parameterIndex)));
			}
			String label = prelbl + parameter.toString().toLowerCase();
			MatlabIO.save(outdir + label + ".mat", volume);
			parameterIndex++;
		}
		
		// Set error volumes
//		System.out.println("Filling error volumes");
//		timer.tick();
		for (int i = 0; i < numErrorVolumes; i++) {
			for (CartanParameterNode node : nodes) {
				Point3i pt = node.getPosition();
				volume.set(pt.x, pt.y, pt.z, node.getError()[i]);
			}
			MatlabIO.save(outdir + prelbl + "error" + (i+1) + ".mat", volume);
		}
	}
	
	private void setRenderingVolume() {
		renderer.setVolume(renderingVolumes[ecbCartanParameter.getSelected()
				.ordinal()], voxelBox.getMask(), voxelBox);
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

        vfp.add(ecbCartanModel.getControls());
        vfp.add(ecbCartanParameter.getControls());
		vfp.add(renderer.getControls());

		return vfp.getPanel();
	}

	public void display(GLAutoDrawable drawable) {

		renderer.display(drawable);
	}
	
	public void init(GLAutoDrawable drawable) {
		renderer.init(drawable);
	}

	public void setVoxelBox(VoxelBox box) {
		this.voxelBox = box;
	}
}

