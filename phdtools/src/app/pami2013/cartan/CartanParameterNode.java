package app.pami2013.cartan;

import java.util.Arrays;
import java.util.List;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import app.pami2013.cartan.CartanParameter.Parameter;
import math.matrix.Matrix3d;
import tools.geom.MathToolset;

public class CartanParameterNode extends CartanParameter {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6492901880517943329L;
	private Point3i pt;
	private double[] error = new double[3];
	private boolean isGarbage = false;
	private int index = -1;
	
	public CartanParameterNode(CartanParameter p, Point3i pt) {
		super(p);
		set(new Point3i(pt));
	}
	
	public CartanParameterNode(int[] parameterMask) {
		super(parameterMask);
		set(new Point3i());
	}

	public CartanParameterNode(Point3i pt) {
		super();
		set(new Point3i(pt));
	}

	public CartanParameterNode(int[] parameterMask, Point3i pt) {
		super(parameterMask);
		set(new Point3i(pt));
	}

	public CartanParameterNode(CartanParameterNode other) {
		this(other, other.getPosition());
	}

	public CartanParameterNode(CartanParameter p, Point3d pt) {
		this(p, MathToolset.tuple3dTo3i(pt));
	}

	public CartanParameterNode(CartanParameter p) {
		this(p, new Point3i());
	}

	public CartanParameterNode() {
		this(new CartanParameter(), new Point3i());
	}

	public CartanParameterNode(CartanModel cartanModel) {
		this(cartanModel.getParameterMask());
	}

	public CartanParameterNode(CartanModel cartanModel, Point3i pt) {
		this(cartanModel.getParameterMask(), pt);
	}

	public CartanParameterNode(double[] array, int[] arrayMask) {
		// First set the parameters
		for (int i = 0; i < arrayMask.length; i++) {
			set(arrayMask[i], array[i]);
		}
		
		// Then set the error
		for (int i = 0; i < 3; i++) {
			error[i] = array[array.length - 3 + i];
		}
	}

	private void set(Point3i pt) {
		this.pt = pt;
	}

	public void setError(double[] error) {
		this.error = error;
	}
	
	public void setGarbage(boolean isGarbage) {
		this.isGarbage = isGarbage;
	}
	
	public Point3i getPosition() {
		return pt;
	}

	public double[] getError() {
		return error;
	}
	
	public double[] getErrorAngle() {
		double[] error = getError();
		double[] errorAngle = new double[3];
		for (int i = 0; i < 3; i++)
			errorAngle[i] = Math.acos(1 - error[i]);
		
		return errorAngle;
	}

	public double[] getErrorAngleDegrees() {
		double[] errorAngle = getErrorAngle();
		for (int i = 0; i < 3; i++)
			errorAngle[i] = Math.toDegrees(errorAngle[i]);
		
		return errorAngle;
	}

	public boolean isGarbage() {
		return isGarbage;
	}

	public static CartanParameterNode max(CartanParameterNode p1, CartanParameterNode p2) {
		CartanParameterNode p1n = new CartanParameterNode(p1);
		CartanParameterNode p2n = new CartanParameterNode(p2);
		p1n.negate();
		p2n.negate();
		return min(p1n, p2n);
	}
	
	@Override
	public void negate() {
		super.negate();
		error[0] *= -1;
		error[1] *= -1;
		error[2] *= -1;
	}

	public static CartanParameterNode min(CartanParameterNode p1, CartanParameterNode p2) {
		// First get min for parameters
		CartanParameterNode min = new CartanParameterNode(CartanParameter.min(p1, p2));
		
		// Then find min error
		if (p1.getErrorSumDegrees() <= p2.getErrorSumDegrees())
			min.setError(p1.getError());
		else
			min.setError(p2.getError());
			
		return min;
	}
	
	public double getErrorSumDegrees() {
		double[] errorAngle = getErrorAngleDegrees();
		
		return errorAngle[0] + errorAngle[1] + errorAngle[2];
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public void setLocation(Point3i pt) {
		this.pt.set(pt);
	}
	
	@Override
	public String toString() {
		String s = super.toString();
		for (int i = 0; i < 3; i++) {
			s += "e" + (i+1) + " = " + getError()[i] + "\t";
		}
		
		return s;
	}

	@Override
	public String toStringShort() {
		return super.toStringShort() + " | e1 = " + getError()[0] + " e2 = " + getError()[1] + " e3 = " + getError()[2];
	}
	
	/**
	 * Compute the mean of a list of Cartan parameters.
	 * @param parameters
	 * @return
	 */
	public static CartanParameterNode computeMean(List<CartanParameterNode> parameters, CartanModel model) {
		
		double[][] allParameters = asArray(parameters, model);
		
		// Compute all means
		double[] mean = new double[allParameters.length];
		for (int i = 0; i < allParameters.length; i++) {
			mean[i] = MathToolset.mean(allParameters[i]);
		}
		
		// Construct mean Cartan node
		return new CartanParameterNode(mean, model.getParameterMask());
	}

	/**
	 * Compute the standard deviation of a list of parameters
	 * @param parameters
	 * @return
	 */
	public static CartanParameterNode computeStd(List<CartanParameterNode> parameters, CartanModel model) {
		double[][] allParameters = asArray(parameters, model);

		// Compute all standard deviations
		double[] std = new double[allParameters.length];
		for (int i = 0; i < allParameters.length; i++) {
			std[i] = MathToolset.std(allParameters[i]);
		}
		
		// Construct mean Cartan node
		return new CartanParameterNode(std, model.getParameterMask());
	}
	
	/**
	 * Compute the standard deviation of a list of parameters
	 * @param parameters
	 * @return
	 */
	public static double[][] asArray(List<CartanParameterNode> parameters, CartanModel model) {
		int numParameters = model.getNumberOfParameters();
		
		// Compile all results in one big double array
		// First dimension = number of connection forms + 3 errors
		// second dimension =  number of Cartan nodes
		double[][] allParameters = new double[numParameters + 3][parameters.size()];
		
		// Process each result node
		int nodeIndex = 0;
		for (CartanParameterNode node : parameters) {
			int parameterIndex = 0;
			
			// Add connection forms
			for (int i = 0; i < numParameters; i++)
				allParameters[parameterIndex++][nodeIndex] = node.get(node.getParameterMask()[i]);
			
			// Add error
			for (int i = 0; i < 3; i++) {
				allParameters[parameterIndex++][nodeIndex] = node.getError()[i];
			}
			
			// Increment node index
			nodeIndex++;
		}
		
		return allParameters;
	}
	
	/**
	 * Fetch the connection forms at the current location and use them.
	 * @param connectionVolumes
	 */
	public void set(Matrix3d[] connectionVolumes) {
		for (int i = 0; i < getParameterMask().length; i++) {
			set(parameterMask[i], connectionVolumes[i].get(pt.x, pt.y, pt.z));
		}
	}
	
	@Override
	public double get(Parameter parameter) {
		// If this is a connection form, use the superclass method
		if (!parameter.isError())
			return get(parameter.getIndex());
		else {
			// Otherwise this is an error, so simply subtract the index for the first frame error.
			return error[parameter.getIndex() - Parameter.error1.getIndex()];
		}
			
	}

	public boolean isNan() {
		for (double v : p)
			if (Double.isNaN(v))
				return true;
		
		return false;
	}

	public double[] getParameterError() {
		double[] p = super.getParameter();
		double[] e = getError();
		double[] all = new double[p.length + e.length];
		for (int i = 0; i < p.length; i++)
			all[i] = p[i];
		for (int i = 0; i < e.length; i++)
			all[p.length + i] = e[i];
		
		return all;
	}

	/**
	 * Fill all the available C1jk connections into this array  
	 * @param array
	 */
	public void fillC1(double[] array) {
		// Set seed arrays
		int index = 0;
		for (int i = 0; i < parameterMask.length; i++)
			if (parameterMask[i] < CartanParameter.Parameter.C231.getIndex())
				array[index++] = get(parameterMask[i]);
	}
	
	/**
	 * Fill all the available C1jk connections into this array  
	 * @param array
	 */
	public void fillC2(double[] array) {
		// Set seed arrays
		int index = 0;
		for (int i = 0; i < parameterMask.length; i++)
			if (parameterMask[i] >= CartanParameter.Parameter.C231.getIndex())
				array[index++] = get(parameterMask[i]);
	}

	/**
	 * Set the C1jk and C2jk parts of this cartan parameter.
	 * @param C1
	 * @param C2
	 */
	public void setC1C2(double[] C1, double[] C2) {
		setC1(C1);
		setC2(C2);
	}

	public void setC1(double[] C1) {
		int index1 = 0;
		
		for (int i = 0; i < parameterMask.length; i++) {
			int parameterIndex = parameterMask[i];
			if (parameterIndex < CartanParameter.Parameter.C231.getIndex())
				set(parameterIndex, C1[index1++]);
		}
	}

	public void setC2(double[] C2) {
		int index2 = 0;
		
		for (int i = 0; i < parameterMask.length; i++) {
			int parameterIndex = parameterMask[i];
			if (parameterIndex >= CartanParameter.Parameter.C231.getIndex())
				set(parameterIndex, C2[index2++]);
		}
	}

}
