package app.pami2013.cartan;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JPanel;

import app.hair.CartanFiber;
import swing.SwingTools;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;

/**
 * @author piuze
 *
 */
public class CartanParameterControl extends CartanParameter {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2573118280566392703L;

	private static final CartanModel DefaultCartanModel = CartanModel.F1;
	
	private static final double min = -5;

	private static final double max = 5;

	public DoubleParameter[] parameterSliders;
	
	private EnumComboBox<CartanModel> modelSelection = new EnumComboBox<CartanModel>(DefaultCartanModel);

	public CartanParameterControl() {
		this(DefaultCartanModel);
	}

	public CartanParameterControl(CartanModel cmodel) {
		this(new CartanParameter(cmodel), cmodel);
	}
	
	public CartanParameterControl(CartanParameter hp) {
		this(hp, CartanModel.FULL);
	}
	public CartanParameterControl(CartanParameter hp, CartanModel model) {
		// Call the superclass constructor so we assign the initial parameter values
		super(hp);
		
		// Initialize UI parameters
		initParameters();

		// Create the UI
		createControls();

		// Update 
		updateSliders();
		
		// Update controls when the model type is changed 
		modelSelection.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				updateControls();
			}
		});
		modelSelection.setSelected(model);
	}


	private VerticalFlowPanel controlContainer = null;
	
	private void createControls() {
		controlContainer = new VerticalFlowPanel();
		controlContainer.createBorder("Cartan Model Parameters");
		
		updateControls();
	}
	
	private void updateControls() {
		controlContainer.removeAll();
		
		controlContainer.add(modelSelection.getControls());
		
		// Add only corresponding parameters
		for (int i : modelSelection.getSelected().getParameterMask()) {
			controlContainer.add(parameterSliders[i].getSliderControls());
		}
		
		
		JButton btnReset = new JButton("reset");
		final CartanParameter p = this;
		btnReset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				hold(true);
				for (int i = 0; i < parameterMask.length; i++) 
					parameterSliders[i].setValue(get(i));
				for (int i = 0; i < 9; i++)
					p.set(i, 0);
				
				hold(false);
				
				notifyListeners();
			}
		});
		controlContainer.add(SwingTools.encapsulate(btnReset));
		
		controlContainer.update();
	}
	
	public JPanel getControls() {
		
		if (controlContainer == null) {
			createControls();
		}

		return controlContainer.getPanel();
	}

	/**
	 * TODO: important: override all CartanParameter setters
	 */
	/**
	 * 
	 * @param v
	 */
	@Override
    public void set(CartanParameter v) {
		super.set(v);
		updateSliders();
    }
	
	public void set(CartanParameter.Parameter parameter, double value) {
		super.set(parameter, value);
		updateSliders();
	}
	
    @Override
	public void set(int i, double v) {
    	super.set(i, v);
    	updateSliders();
    }

    @Override
	public void setParameters(double[] p) {
		super.set(Arrays.copyOf(p, p.length), parameterMask);
		updateSliders();
	}

	public void updateSliders() {
		hold(true);
		for (int i = 0; i < parameterMask.length; i++) {
			parameterSliders[i].setValue(get(i));
		}
		hold(false);
//		notifyListeners();
	}
	
	public CartanParameterControl(CartanParameterControl other) {
		super(other);
	}

	protected void initParameters() {
		createParameters();

		addListener();
	}
	
	private void createParameters() {
		parameterSliders = new DoubleParameter[CartanModel.FULL.getNumberOfParameters()];
		for (int i = 0; i < parameterSliders.length; i++) {
			parameterSliders[i] = new DoubleParameter(CartanParameter.Parameter.values()[i].toString(), 0, min, max);
		}
		
		for (DoubleParameter p : parameterSliders) {
			super.addParameter(p);
		}
	}
	
	@Override
	public void zero() {
		for (int i = 0; i < CartanModel.FULL.getNumberOfParameters(); i++) {
			parameterSliders[i].setValue(0);
		}
	}
	
	private void addListener() {
		
		for (int i = 0; i < CartanModel.FULL.getNumberOfParameters(); i++) {
			final int cindex = i;
			final CartanParameter p = this;
			parameterSliders[i].addParameterListener(new ParameterListener() {
				@Override
				public void parameterChanged(swing.parameters.Parameter parameter) {
					if (!isHolding())
						p.set(cindex, parameterSliders[cindex].getValue());
//						set(parameterMask[ifinal], parameterSliders[ifinal].getValue());
				}
			});
		}
	}

	/**
	 * 
	 */
	public void invalidate() {
		for (DoubleParameter p : parameterSliders) {
			p.invalidate();
		}
	}
}
