package app.pami2013.cartan;

import heart.Heart;
import heart.Heart.Species;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm.MovingAxis;
import extension.matlab.MatlabScript;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import swing.parameters.ParameterManager;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;

public class CartanParameter extends ParameterManager implements Serializable {
	
	/**
	 * Automatic serial ID
	 */
	private static final long serialVersionUID = 1790049588303501471L;
	
//	public final static String[] labels = new String[] { "c121","c122","c123","c131","c132","c133","c231","c232","c233" };
	protected final static double[] maxima = new double[] { 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5 };

	public static final int ConnectionFormCount = 9; 
	
	/**
	 * Contains the connection form parameter values.
	 */
	protected double[] p;
	
	/**
	 * Indices corresponding to enabled parameters.
	 * e.g. for the helicoid model parameterMask=[0,1,2] which correspond to c121, c122, c123
	 */
	protected int[] parameterMask;

	public enum Parameter {
		C121(0, "c_{121}", new int[] { 1, 2, 1 }), 
		C122(1, "c_{122}", new int[] { 1, 2, 2 }), 
		C123(2, "c_{123}", new int[] { 1, 2, 3 }), 
		C131(3, "c_{131}", new int[] { 1, 3, 1 }), 
		C132(4, "c_{132}", new int[] { 1, 3, 2 }), 
		C133(5, "c_{133}", new int[] { 1, 3, 3 }), 
		C231(6, "c_{231}", new int[] { 2, 3, 1 }), 
		C232(7, "c_{232}", new int[] { 2, 3, 2 }), 
		C233(8, "c_{233}", new int[] { 2, 3, 3 }), 
		error1(9, "\\epsilon_1", new int[] { 1, 0, 0 }), 
		error2(10, "\\epsilon_2", new int[] { 0, 1, 0 }), 
		error3(11, "\\epsilon_3", new int[] { 0, 0, 1 });
		
		private int index;
		private String latexString;
		private int[] frameIndex;

		private Parameter(int index, String latexString, int[] frameIndex) {
			this.index = index;
			this.latexString = latexString;
			this.frameIndex = new int[] { frameIndex[0] - 1, frameIndex[1] - 1, frameIndex[2] - 1 };
		}
		
		public MovingAxis[] asMovingAxes() {
			return new MovingAxis[] { MovingAxis.fromInt(frameIndex[0]), MovingAxis.fromInt(frameIndex[1]), MovingAxis.fromInt(frameIndex[2]) };
		}
		
		public int getIndex() {
			return index;
		}
		
		public static Parameter FromIndex(int index) {
			// For now c121 is index at 0. Make sure this doesn't change.
			return Parameter.values()[index];
		}
		
		public String toStringLatex() {
			return "$" + latexString + "$";
//			if (index >= 0 && index <=8)
//				return super.toString().toLowerCase();
//			else if (this == error1)
//				return "\\epsilon_1";
//			else if (this == error2)
//				return "\\epsilon_2";
//			else if (this == error3)
//				return "\\epsilon_3";
//			return "";
		}
		
		public static List<Parameter> errors() {
			return Arrays.asList(Parameter.error1, Parameter.error2, Parameter.error3);
		}

		public static List<Parameter> connections() {
			return Arrays.asList(C121, C122, C123, C131, C132, C133, C231, C232, C233);
		}

		public static List<Parameter> connections12k() {
			return Arrays.asList(C121, C122, C123);
		}
		public static List<Parameter> connections13k() {
			return Arrays.asList(C131, C132, C133);
		}
		public static List<Parameter> connections23k() {
			return Arrays.asList(C231, C232, C233);
		}

		public boolean isError() {
			return this == error1 || this == error2 || this == error3;
		}

		public String getLatexString() {
			return latexString;
		}
	}

	public enum CartanModel implements Iterable<Parameter> {
		
		FULL(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 }, "full"), 
		HOMEOID(new int[] { 0, 1, 2, 3, 4, 6, 7 }, "homeoid"), 
		TANGENTIAL(new int[] { 0, 3, 6 }, "tangential"), 
		F1(new int[] { 0, 1, 2, 3, 4, 5}, "f1"), 
		HELICOID(new int[] { 0, 1, 2 }, "helicoid"),
		C121(new int[] { 0 }, "c_{121}"),
		C122(new int[] { 1 }, "c_{122}"),
		C123(new int[] { 2 }, "c_{123}"),
		C131(new int[] { 3 }, "c_{131}"),
		C132(new int[] { 4 }, "c_{132}"),
		C133(new int[] { 5 }, "c_{133}"),
		C231(new int[] { 6 }, "c_{231}"),
		C232(new int[] { 7 }, "c_{232}"),
		C233(new int[] { 8 }, "c_{233}"),
		CONSTANT(new int[] { }, "T");
		
		private CartanModel(int[] parameterMask, String latexLabel) {
			parameters = new ArrayList<Parameter>();
			connections = new ArrayList<Parameter>();
			this.latexLabel = latexLabel;
			set(parameterMask);
		}
		
		public List<Parameter> getParameters() {
			return parameters;
		}
		
		private void set(int[] parameterMask) {
			this.parameterMask = parameterMask;
			
			parameters.clear();
			connections.clear();
			
			// First add connection forms
			for (int i = 0; i < parameterMask.length; i++) {
				parameters.add(Parameter.values()[parameterMask[i]]);
				connections.add(Parameter.values()[parameterMask[i]]);
			}
			
			// Then add errors
			parameters.add(Parameter.error1);
			parameters.add(Parameter.error2);
			parameters.add(Parameter.error3);
		}
		
		public List<Parameter> getAvailableConnections() {
			return connections;
		}
		
		private final ArrayList<Parameter> parameters;

		private final ArrayList<Parameter> connections;
		
		private String latexLabel;

		private int[][] formIndices = new int[][] { new int[]{0,1,0}, new int[]{0,1,1}, new int[]{0,1,2}, new int[]{0,2,0}, new int[]{0,2,1}, new int[]{0,2,2}, new int[]{1,2,0}, new int[]{1,2,1}, new int[]{1,2,2} };
		
		private int[] parameterMask;
		
		public int[] getParameterMask() {
			return parameterMask;
		}
		public int getNumberOfParameters() {
			return parameterMask.length;
		}
		public int getParameterMaskIndex(int maskIndex) {
			return parameterMask[maskIndex];
		}
		public List<CartanParameter> getSeedPoints() {
			return new CartanParameter(getParameterMask()).getSeeds();
		}
		public String toStringLong() {
			String s = "";
			
			if (getNumberOfParameters() > 0) {
				s += Parameter.values()[getParameterMaskIndex(0)];
			}
			
			for (int i = 1; i < getNumberOfParameters(); i++) {
				s += ", " + Parameter.values()[getParameterMaskIndex(i)];
			}
			
			return toString() + " (" + s + ")";
		}
		
		@Override
		public String toString() {
			return super.toString().toLowerCase();
		}
		
		public String toStringLatex() {
			return "$" + latexLabel + "$";
		}
		
		public int[][] getFrameIndices() {
			int numParameters = getParameterMask().length;
			int[][] frameIndices = new int[numParameters][3];
			
			for (int i = 0; i < numParameters; i++) {
				frameIndices[i] = formIndices[getParameterMask()[i]];
			}
			
			return frameIndices;
		}
		
		@Override
		public Iterator<Parameter> iterator() {
			return parameters.iterator();
		}
		
		public boolean contains(Parameter parameter) {
			return parameters.contains(parameter);
		}

		public int countC1() {
			int count = 0;
			for (int i = 0; i < parameterMask.length; i++)
				if (parameterMask[i] < CartanParameter.Parameter.C231.getIndex())
					count++;
			
			return count;
		}
		
		public int countC2() {
			int count = 0;
			for (int i = 0; i < parameterMask.length; i++)
				if (parameterMask[i] >= CartanParameter.Parameter.C231.getIndex())
					count++;
			
			return count;
		}
	}

	public List<CartanParameter> getSeeds() {
		List<CartanParameter> seeds = CartanParameter.getRecursiveSeeds(getParameterMask());
		for (CartanParameter seed : seeds) {
			seed.parameterMask = this.parameterMask;
		}
		return seeds;
	}
	
	private static List<CartanParameter> getRecursiveSeeds(int[] mask) {
		// Create seed array to return
		List<CartanParameter> seeds = new LinkedList<CartanParameter>();

		if (mask.length == 0) {
			seeds.add(new CartanParameter());
			return seeds;
		}

		// Current parameter index
		int currentIndex = mask[0];
		
		// Remove current mask index
		int[] maskShort = Arrays.copyOfRange(mask, 1, mask.length);
		
		// Create double array holding seeds for the current parameter index 
		// Here we could remove/add more if necessary
		double[] currentParameterSeeds = new double[] { -getMaxima(currentIndex), 0, getMaxima(currentIndex) };

		// Go recursively through remaining parameters
//		for (int i : maskShort) {
		List<CartanParameter> recursionSeeds = getRecursiveSeeds(maskShort);

		// Set current parameter seeds
		for (double cseed : currentParameterSeeds) {
			// Create new parameter for each recursion generated
			for (CartanParameter cp : recursionSeeds) {
				CartanParameter cpNew = new CartanParameter(cp);
				cpNew.set(currentIndex, cseed);
				seeds.add(cpNew);
			}
		}
		
		return seeds;
	}
	
	public CartanParameter(int[] parameterMask) {
		set(new double[ConnectionFormCount], Arrays.copyOf(parameterMask, parameterMask.length));
	}

	public CartanParameter(CartanParameter other) {
		set(Arrays.copyOf(other.getParameter(), other.getParameter().length), Arrays.copyOf(other.getParameterMask(), other.getParameterMask().length));
	}

	public CartanParameter(CartanModel model) {
		set(new double[ConnectionFormCount], model.getParameterMask());
	}
	
	public CartanParameter(double[] p) {
		assert p.length == ConnectionFormCount;
		
		set(p, CartanModel.FULL.getParameterMask());
	}
	
	public CartanParameter() {
		set(new double[ConnectionFormCount], CartanModel.FULL.getParameterMask());
	}

	public void set(double[] p) {
		set(p, CartanModel.FULL.parameterMask);
	}
	
	protected void set(double[] p, int[] parameterMask) {
		assert p.length == ConnectionFormCount;

		if (this.p == null)
			this.p = new double[ConnectionFormCount];
		
		for (int index : parameterMask)
			this.p[index] = p[index];
		
		this.parameterMask = parameterMask;
	}
	
	public void set(CartanParameter other) {
		set(other.getParameter(), Arrays.copyOf(other.getParameterMask(), other.getParameterMask().length));
	}

	public void setParameters(double[] p) {
		assert p.length == ConnectionFormCount;

		set(Arrays.copyOf(p, ConnectionFormCount), parameterMask);
	}

	public int[] getParameterMask() {
		return parameterMask;
	}
	
	public double get(int i, int j, int k) {
		if (i == j)
			return 0;
		
		double d = 1;

		if (i > j) {
			d = -1;
			int i2 = i;
			i = j;
			j = i2;
		}
		
		if (i == 1 && j == 2 && k == 1)
			return d*getc121();
		if (i == 1 && j == 2 && k == 2)
			return d*getc122();
		if (i == 1 && j == 2 && k == 3)
			return d*getc123();
		if (i == 1 && j == 3 && k == 1)
			return d*getc131();
		if (i == 1 && j == 3 && k == 2)
			return d*getc132();
		if (i == 1 && j == 3 && k == 3)
			return d*getc133();
		if (i == 2 && j == 3 && k == 1)
			return d*getc231();
		if (i == 2 && j == 3 && k == 2)
			return d*getc232();
		if (i == 2 && j == 3 && k == 3)
			return d*getc233();
		
		return Double.NaN;
	}
	
	public double getc121() {
		return p[0];
	}

	public double getc122() {
		return p[1];
	}

	public double getc123() {
		return p[2];
	}

	public double getc131() {
		return p[3];
	}
	
	public double getc132() {
		return p[4];
	}

	public double getc133() {
		return p[5];
	}

	public double getc231() {
		return p[6];
	}

	public double getc232() {
		return p[7];
	}

	public double getc233() {
		return p[8];
	}

	public void setc121(double v) {
		p[0] = v;
	}

	public void setc122(double v) {
		p[1] = v;
	}

	public void setc123(double v) {
		p[2] = v;
	}

	public void setc131(double v) {
		p[3] = v;
	}
	
	public void setc132(double v) {
		p[4] = v;
	}

	public void setc133(double v) {
		p[5] = v;
	}

	public void setc231(double v) {
		p[6] = v;
	}

	public void setc232(double v) {
		p[7] = v;
	}

	public void setc233(double v) {
		p[8] = v;
	}


	public double[] getParameter() {
		assert p.length == ConnectionFormCount;

		return p;
	}

	public double get(int i) {
		return p[i];
	}
	
	public double get(CartanParameter.Parameter parameter) {
		return get(parameter.getIndex());
	}
	
	public static double getMaxima(int i) {
		return maxima[i];
	}
	
    /**
     * @return the squared 2-norm of this parameter.
     */
    public double lengthSquared() {
    	double sum = 0;
    	for (int i = 0; i < parameterMask.length; i++) {
    		sum += get(parameterMask[i]) * get(parameterMask[i]);
    	}
    	return sum;
    }
    
    /**
     * @return the 2-norm of this parameter.
     */
    public double length() {
        return Math.sqrt(lengthSquared());
    }

    public static Random rand = new Random();

    public CartanParameter getNoisyVersion(double mag) {
        CartanParameter c = new CartanParameter(this);
        c.applyRandomNoise(mag);
        return c;
    }
    
    public CartanParameter applyRandomNoise(double mag) {
    	for (int index : parameterMask) {
    		p[index] += 2 * mag * (rand.nextDouble() - 0.5);
    	}
    	
    	return this;
    }

	public CartanParameter randomize(double mag) {
    	for (int index : parameterMask) {
    		p[index] = 2 * mag * (rand.nextDouble() - 0.5);
    	}
    	
    	return this;
	}

    public void add(CartanParameter other) {
    	for (int index : parameterMask)
    		p[index] += other.p[index];
    }

    public void abs() {
    	for (int index : parameterMask)
    		p[index] = Math.abs(p[index]);
    }

    public void scale(double s) {
    	for (int index : parameterMask) {
    		p[index] *= s;
    	}
    }

    public void zero() {
    	for (int i = 0; i < ConnectionFormCount; i++)
    		p[i] = 0;
    }

    public void divide(double v) {
    	scale(1d / v);
    }

    /**
     * NOTE: This method should only be called for parameters that have the same parameter mask.
     * @param other
     * @return the sum of squared errors between these two parameters.
     */
    public double SSE(CartanParameter other) {
    	double sum = 0;
    	double v = 0;
    	for (int i = 0; i < ConnectionFormCount; i++) {
    		v = other.get(i) - get(i);
    		sum += v * v;
    	}
    	return sum;
    }

	public void set(int i, double d) {
		p[i] = d;
	}

	public void set(Parameter p, double d) {
		if (p.getIndex() > Parameter.error1.getIndex())
			throw new NotImplementedException();
		
		set(p.getIndex(), d);
	}

	public void set(double v) {
		for (int index : parameterMask) {
			p[index] = v;
		}
	}

	/**
     * NOTE: The magnitude cutoff should be ordered following <code>parameterMask</code>.
	 * @param cutoff
	 * @return
	 */
	public CartanParameter cut(double[] cutoffMag) {

		for (int i = 0; i< parameterMask.length; i++) {
			if (Math.abs(p[parameterMask[i]]) > cutoffMag[parameterMask[i]])
				p[parameterMask[i]] = Math.signum(p[parameterMask[i]]) * cutoffMag[parameterMask[i]]; 
			
		}

        return this;
	}
	public CartanParameter cut(double cutoffMag) {
		return cut(new double[] { cutoffMag} );
	}

	public static CartanParameter getRandom(double mag) {
		return (new CartanParameter()).randomize(mag);
	}
	
	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < ConnectionFormCount; i++) {
			s += Parameter.values()[i] + " = " + get(i) + "\t";
		}
		
		return s;
	}
	
	public String toStringShort() {
		String s = "";
		for (int index : getParameterMask()) {
			s += Parameter.values()[index] + "=" + new BigDecimal(get(index)).setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue() + " ";
		}
		
		return s;
	}

	/**
	 * Test seeds listing
	 * @param args
	 */
	public static void main(String[] args) {
		List<CartanParameter> seeds = CartanModel.C123.getSeedPoints();
		System.out.println(seeds.size() + " seeds total.");
		for (CartanParameter seed : seeds) {
			System.out.println(seed);
		}
	}

	public void setParametersMasked(double[] parameters) {
		for (int i = 0; i < getParameterCount(); i++)
			set(parameterMask[i], parameters[i]);
	}

	public int getParameterCount() {
		return parameterMask.length;
	}

	public static CartanParameter min(CartanParameter p1, CartanParameter p2) {
		CartanParameter min = new CartanParameter();
		for (int i = 0; i < ConnectionFormCount; i++) {
			if (p1.get(i) <= p2.get(i))
				min.set(i, p1.get(i));
			else
				min.set(i, p2.get(i));
		}
		
		return min;
	}

	public static CartanParameter max(CartanParameter p1, CartanParameter p2) {
		CartanParameter p1n = new CartanParameter(p1);
		CartanParameter p2n = new CartanParameter(p2);
		p1n.negate();
		p2n.negate();
		
		return min(p1n, p2n);
	}
	
	public void negate() {
		for (int i = 0; i < ConnectionFormCount; i++) {
			set(i, -get(i));
		}
	}

	public void add(double s, CartanParameter other) {
//		for (int i = 0; i < ConnectionFormCount; i++) {
		for (int i : parameterMask) {
			set(i, get(i) + s * other.get(i));
		}
	}
	public CartanParameter exp(double p) {
		CartanParameter exp = new CartanParameter(this);
//		for (int i = 0; i < ConnectionFormCount; i++) {
		for (int i : parameterMask) {
			exp.set(i, Math.pow(get(i), p));
		}
		
		return exp;
	}

	public static CartanParameter computeMean(List<CartanParameter> parameters) {
		CartanParameter mean = new CartanParameter();
		for (CartanParameter p : parameters) {
			mean.add(p);
		}
		
		mean.divide(parameters.size());
		
		return mean;
	}
	
	public double getNorm1() {
		double norm1 = 0;
		for (int index : parameterMask) {
			norm1 += Math.abs(p[index]);
		}
		
		return norm1;
	}

	public double getNorm2() {
		double norm2 = 0;
		for (int index : parameterMask) {
			norm2 += Math.sqrt(p[index] * p[index]);
		}
		
		return norm2;
	}
	
	/**
	 * Subtract a Cartan parameter and return the result as a new parameter.
	 * @param other
	 * @return
	 */
	public CartanParameter subOut(CartanParameter other) {
		CartanParameter sub = new CartanParameter(this);
		
		for (int index : parameterMask) {
			sub.set(index, get(index) - other.get(index));
		}
		
		return sub;
	}

	public CoordinateFrame contract(CoordinateFrame frame, Vector3d dt, boolean applyTranslation) {

		// Get the current frame vectors
		Vector3d e1 = new Vector3d(frame.getAxis(FrameAxis.X));
		Vector3d e2 = new Vector3d(frame.getAxis(FrameAxis.Y));
		Vector3d e3 = new Vector3d(frame.getAxis(FrameAxis.Z));

		// Compute contractions
		double h1 = e1.dot(dt);
		double h2 = e2.dot(dt);
		double h3 = e3.dot(dt);
		
		// de1
		double c12h = getc121() * h1 + getc122() * h2 + getc123() * h3;
		double c13h = getc131() * h1 + getc132() * h2 + getc133() * h3;
		
		// de2
		double c21h = -c12h;
		double c23h = getc231() * h1 + getc232() * h2 + getc233() * h3;

		// Compute e1 = e1 + de1
		e1.scaleAdd(c12h, e2, e1);
		e1.scaleAdd(c13h, e3, e1);
		e1.normalize();
		
		// Compute e2 = e2 + de2
		e2.scaleAdd(c21h, e1, e2);
		e2.scaleAdd(c23h, e3, e2);
		e2.normalize();
		
		// Compute e3 = e1 x e2
		e3.cross(e1, e2);

		Point3d pt = new Point3d(frame.getTranslation());
		if (applyTranslation) {
			// Translate coordinate system
			pt.add(dt);
		}
		return new CoordinateFrame(e1, e2, e3, pt);
	}

	public CoordinateFrame contract(CoordinateFrame frame, double dt, boolean applyTranslation) {
		Vector3d dtv = new Vector3d();
		Vector3d e1 = frame.getAxis(FrameAxis.X);
		dtv.scale(dt, e1);
		return contract(frame, dtv, applyTranslation);
	}
	
	public CoordinateFrame contract(CoordinateFrame frame, double dt) {
		return contract(frame, dt, true);
	}
	
	public ArrayList<CoordinateFrame> integrate(CoordinateFrame frame0, double length, double resolution) {
		
		ArrayList<CoordinateFrame> frames = new ArrayList<CoordinateFrame>();
		CoordinateFrame frame = new CoordinateFrame(frame0);
		double step = length / resolution;

		for (double t = 0; t < length; t += step) {
			frames.add(frame);
			
			frame = contract(frame, step);
		}
		
		return frames;
	}
	
	public double[] getParameterArray() {
		double[] values = new double[getParameterMask().length];
		
		for (int i = 0; i < parameterMask.length; i++)
			values[i] = get(parameterMask[i]);

		return values;
	}
}
