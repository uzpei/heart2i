package app.pami2013;

import heart.Heart;

import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import volume.IntensityVolume;
import voxel.VoxelVectorField;
import extension.matlab.MatlabIO;
import extension.matlab.MatlabScript;

/**
 * Be able to pass it a VoxelVectorField directly
 * @author epiuze
 *
 */
public class IterativeFilter {
	
	private int numIterations;
	
	private double std;
	
	public IterativeFilter(int numIterations, double std) {
		this.numIterations = numIterations;
		this.std = std;
	}
	
	public int getIterations() {
		return numIterations;
	}
	
	public double getStandardDeviation() {
		return std;
	}

	/**
	 * Try to load the filtered volume or compute it when not found.
	 * @param heart
	 */
	public VoxelVectorField apply(String volumePath, String maskPath, String vectorFieldName) {
		String dir = volumePath + toStringFilename() + "/";
		String filex = dir + Heart.fileT[0] + ".mat";
		String filey = dir + Heart.fileT[1] + ".mat";
		String filez = dir + Heart.fileT[2] + ".mat";
		
		if (new File(dir).exists()) {
			IntensityVolume f1x = new IntensityVolume(filex);
			IntensityVolume f1y = new IntensityVolume(filey);
			IntensityVolume f1z = new IntensityVolume(filez);
			
			return new VoxelVectorField(vectorFieldName, f1x, f1y, f1z);
		}
		else {
			VoxelVectorField V = null;
			try {
				V = runGaussianIterativeSmoothing(volumePath, maskPath, vectorFieldName);
				
				// Save it
				MatlabIO.save(filex, V.getX());
				MatlabIO.save(filey, V.getY());
				MatlabIO.save(filez, V.getZ());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return V;
		}
	}

	/**
	 * Example use: runGaussianIterativeSmoothing("/Users/myself/data/", "mask.mat", "V", 5, 0.5, "/Users/myself/data/filtered/"); 
	 * @param volumePath
	 * @param maskNameIncludingExtension
	 * @param vectorFieldName
	 * @param numIterations
	 * @param standardDeviation
	 * @param outputBasePath
	 * @return
	 * @throws IOException
	 */
	public VoxelVectorField runGaussianIterativeSmoothing(String volumePath, String maskPath, String vectorFieldName) throws IOException {
		if (numIterations <= 0 || std <= 0)
			return null;

		MatlabScript script = new MatlabScript();
		String command = "open_principal_direction";
		String arguments = MatlabScript.toString(new File(volumePath).getAbsolutePath() + "/") + ", " + MatlabScript.toString(maskPath) + ", " + MatlabScript.toString(vectorFieldName) + ", " + numIterations +  ", " + std;  
		script.writeLine("F = " + script.asCommand(command, arguments));
		script.writeLine("Fx = squeeze(F(:,:,:,1))");
		script.writeLine("Fy = squeeze(F(:,:,:,2))");
		script.writeLine("Fz = squeeze(F(:,:,:,3))");
		
		String outputBasePath = File.createTempFile("java_heart_field_smooth", "").getAbsolutePath();
		script.writeLine("save(" + MatlabScript.toString(new File(outputBasePath + "x.mat").getAbsolutePath()) + ", 'Fx')");
		script.writeLine("save(" + MatlabScript.toString(new File(outputBasePath + "y.mat").getAbsolutePath()) + ", 'Fy')");
		script.writeLine("save(" + MatlabScript.toString(new File(outputBasePath + "z.mat").getAbsolutePath()) + ", 'Fz')");
		
		script.exit();
		script.save();

		System.out.println("Running " + this.toString());
		script.run();
		
		IntensityVolume v1 = new IntensityVolume(outputBasePath + "x.mat");
		IntensityVolume v2 = new IntensityVolume(outputBasePath + "y.mat");
		IntensityVolume v3 = new IntensityVolume(outputBasePath + "z.mat");
		return new VoxelVectorField("T_smooth", v1, v2, v3);
	}
	
	@Override
	public String toString() {
		return "Iterative Gaussian smoothing with " + numIterations + " iterations at standard deviation " + std;
	}
	
	public String toStringShort() {
		return "std = " + std + " x sqrt(" + numIterations + ")";
	}

	public String toStringMatlab() {
		return std + "\\surd" + numIterations;
	}

	public String toStringLatex() {
		if (numIterations == 0) return "$0$";
		
		DecimalFormat format = new DecimalFormat("#.##");
		return "$" + format.format(std) + "\\sqrt{" + numIterations + "}" + "$";
	}

	public String toStringFilename() {
		return "its" + getIterations() + "std" + String.valueOf(getStandardDeviation()).replace(".", "d");
	}

	public String toStringStd() {
		DecimalFormat format = new DecimalFormat("#.##");
		format.setRoundingMode(RoundingMode.HALF_UP);
		return format.format(getStandardDeviation());
	}

	@Override
	public int hashCode() {
		return toStringShort().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		
		if (o instanceof IterativeFilter)
			return ((IterativeFilter) o).hashCode() == hashCode();
		else
			return false;
	}
}
