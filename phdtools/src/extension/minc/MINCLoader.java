package extension.minc;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import edu.ucla.loni.minc.plugin.MincImageReader;
import edu.ucla.loni.minc.plugin.MincImageReaderSpi;

public class MINCLoader {
	
    public static void main(String[] args) {
        MINCLoader loader = new MINCLoader();
        loader.load("./data/rat/e1x.mnc");
    }

	public MINCLoader() {
		// Empty constructor
	}
	
	public void load(String filename) {
		File file = new File(filename);
		
		try {
			BufferedInputStream input = new BufferedInputStream(new FileInputStream(filename));
			
			// Test the existence of the files
			if (!file.exists() || !file.isFile() || !file.canRead()) {
				System.out.println("Cannot read \"" + file.getName() + "\"");
				System.exit(1);
			}

			// Verify the file is a MINC file
			MincImageReaderSpi spi = new MincImageReaderSpi();
			ImageInputStream inputStream = ImageIO.createImageInputStream(file);
			if (!spi.canDecodeInput(inputStream)) {
				System.out.println("Not a MINC file.");
				System.exit(1);
			}

			// Create the MINC reader
			MincImageReader reader = (MincImageReader) spi
					.createReaderInstance(null);
			inputStream = ImageIO.createImageInputStream(file);
			reader.setInput(inputStream, true, false);

			createData(reader);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	private void createData(MincImageReader reader) throws Exception {
		// Print some data
		System.out.println("------------------");
		int numImages = reader.getNumImages(true);
		System.out.println("Image format = " + reader.getFormatName());
		System.out.println("Number of images detected (third dimension) = "
				+ numImages);

		int minIndex = reader.getMinIndex();
		int height = reader.getHeight(minIndex);
		int width = reader.getWidth(minIndex);
		System.out.println("Image height for first slice = " + height);
		System.out.println("Image width  for first slice = " + width);

//		
//		if (data instanceof DataBufferInt)
//			System.out.println("Data buffer int");
//        else if (data instanceof DataBufferFloat)
//			System.out.println("Data buffer float");
		
		for (int z = 0; z < numImages; z++) {
			BufferedImage img = reader.read(z, null);
			
			Raster ras = img.getData();
//			DataBuffer data = ras.getDataBuffer();
//			data.getFloatElem
			
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++)  {
					double[] v = new double[1];
					v  = ras.getPixel(i, j, v);
					
					if (v[0] != 0) {
						System.out.println(v[0]);
					}
				}
			}
		}
	}


}
