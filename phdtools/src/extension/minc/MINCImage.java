package extension.minc;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.ucla.loni.minc.plugin.MincImageReader;
import edu.ucla.loni.minc.plugin.MincImageReaderSpi;
import edu.ucla.loni.minc.plugin.MincMetadata;

/**
 * @author epiuze
 *
 */
public class MINCImage {

	public enum SpaceUnits {
		mm(1e-3), cm(1e-2), m(1);

		private double scale;

		private SpaceUnits(double scale) {
			this.scale = scale;
		}

		public double getScale() {
			return scale;
		}
	};
	
	private Raster[] slices;
	
	private MincImageReader reader;
	
	private String filename;
	
	public static boolean verbose = true;

	/**
	 * FIXME: this is unoptimal and (very) bad coding practice...
	 */
	public boolean loaded = true;
	
	/**
	 * 
	 * @param filename
	 *            Name of the file to read an image from.
	 */

	public MINCImage(String filename) {
		this.filename = filename;
		if (verbose) System.out.println("Creating MINC image from " + filename + " ...");
		try {
			File file = new File(filename);
			
			if (!file.exists()) return;

			// Get the metadata
			loaded = createReader(file);
			createData(reader);
			
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	private boolean createReader(File file) throws Exception {
		// Test the existence of the files
		if (!file.exists() || !file.isFile() || !file.canRead()) {
			System.err.println("Cannot read \"" + file.getName() + "\"");
			return false;
		}

		// Verify the file is a MINC file
		MincImageReaderSpi spi = new MincImageReaderSpi();
		ImageInputStream inputStream = ImageIO.createImageInputStream(file);
		if (!spi.canDecodeInput(inputStream)) {
			System.err.println("Not a MINC file.");
			return false;
		}

		// Create the MINC reader
		reader = (MincImageReader) spi
				.createReaderInstance(null);
		inputStream = ImageIO.createImageInputStream(file);
		reader.setInput(inputStream, true, false);
		
		return true;
	}


	private void createData(MincImageReader reader) throws Exception {
		// Print some data
		if (verbose) System.out.println("------------------");
		int numImages = reader.getNumImages(true);
		if (verbose) System.out.println("Image format = " + reader.getFormatName());
		if (verbose) System.out.println("Number of images detected (third dimension) = "
				+ numImages);

		int minIndex = reader.getMinIndex();
		int height = reader.getHeight(minIndex);
		int width = reader.getWidth(minIndex);
		if (verbose) System.out.println("Image height for first slice = " + height);
		if (verbose) System.out.println("Image width  for first slice = " + width);
		
		if (verbose) System.out.println("\t Dimensions = [" + height + " x " + numImages + " x " + width + "]");
		
		// Extract scale
		parseMetadata();

		slices = new Raster[numImages];
	
		for (int z = 0; z < numImages; z++) {
//			System.out.println("Processing slice #" + (z + 1) + "/" + numImages);
			BufferedImage img = reader.read(z, null);
			
			slices[z] = img.getData();
		}
	}
	
	public Raster[] getSlices() {
		return slices;
	}
	
	public int getSlicesCount() {
		return slices.length;
	}
	
	private void printData(MincImageReader reader) throws Exception {
		
		int numImages = reader.getNumImages(true);
		int minIndex = reader.getMinIndex();
		int height = reader.getHeight(minIndex);
		int width = reader.getWidth(minIndex);

		// Search for non-zero pixels
		for (int image = 0; image < numImages; image++) {
			System.out.println("Slice #" + (image + 1) + "/" + numImages);
			BufferedImage img = reader.read(image);
			// Raster raster = img.getData();
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					// Use the red channel as intensity
					System.out.println(getImageValue(img.getData(), i, j));
				}
			}
		}
	}
	
	private double[] bufferedDataDouble = new double[1];
	
	/**
	 * @param slice
	 * @param i
	 * @param j
	 * @return
	 */
	private double getImageValue(Raster slice, int i, int j) {
//		return (img.getRGB(x, y) & 0x00FF0000) >> 16;
		bufferedDataDouble = slice.getPixel(i, j, bufferedDataDouble);
		return bufferedDataDouble[0];
	}
	
	/**
	 * @param x coordinate
	 * @param y coordinate
	 * @param z coordinate
	 * @return the value at this voxel location, between 0 and 255;
	 */
	public double getValue(int x, int y, int z) {
//		return (slices[z].getRGB(x, y) & 0x00FF0000) >> 16;
//		return getImageValue(slices[z], x, y);
		return getImageValue(slices[x], z, y);
	}

	/**
	 * Prints the specified data set in tabular form.
	 * 
	 * @param dataSetNode
	 *            Data set Node.
	 */
	public void parseMetadata() {
		try {
			MincMetadata metadata = (MincMetadata) reader.getStreamMetadata();
			String formatName = metadata.getNativeMetadataFormatName();
			Node rootNode = metadata.getAsTree(formatName);

			List<String> queries = new LinkedList<String>();
			queries.add("xspace");
			queries.add("yspace");
			queries.add("zspace");

			parseVariables(findChildNode(rootNode, "variables"), queries);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prints the specified data set in tabular form.
	 * 
	 * @param dataSetNode
	 *            Data set Node.
	 */
	public void printMetadata() {
		try {
			MincMetadata metadata = (MincMetadata) reader.getStreamMetadata();
			String formatName = metadata.getNativeMetadataFormatName();
			Node rootNode = metadata.getAsTree(formatName);

				// Loop through the data elements
			NodeList elementNodes = rootNode.getChildNodes();
			for (int i = 0; i < elementNodes.getLength(); i++) {
				Node elementNode = elementNodes.item(i);
				printnode("", elementNode);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public double xspace = 0;
	public double yspace = 0;
	public double zspace = 0;
	public SpaceUnits units;
	
	private void parseVariables(Node rootNode, List<String> list) {
		for (String name : list) {
			if (verbose) System.out.println("Searching for " + name + "...");
			Node n = findChildNode(rootNode, name);
			if (n == null) continue;
			
			if (n.getNodeName().equalsIgnoreCase("xspace")) {
				xspace = Math.abs(Double.valueOf(findChildNode(findChildNode(findChildNode(n, "attributes"), "step"), "value").getAttributes().item(0).getNodeValue()));
				String sunits = findChildNode(findChildNode(findChildNode(n, "attributes"), "units"), "value").getAttributes().item(0).getNodeValue();
				if (sunits.equalsIgnoreCase("mm")) {
					units = SpaceUnits.mm;
				}
				else if (sunits.equalsIgnoreCase("cm")) {
					units = SpaceUnits.cm;
				}
				else if (sunits.equalsIgnoreCase("m")) {
					units = SpaceUnits.m;
				}
				if (verbose) System.out.println("xspace = " + xspace + units.toString());
			}
			else if (n.getNodeName().equalsIgnoreCase("yspace")) {
				yspace = Math.abs(Double.valueOf(findChildNode(findChildNode(findChildNode(n, "attributes"), "step"), "value").getAttributes().item(0).getNodeValue()));
				if (verbose) System.out.println("yspace = " + yspace + units.toString());
			}
			else if (n.getNodeName().equalsIgnoreCase("zspace")) {
				zspace = Math.abs(Double.valueOf(findChildNode(findChildNode(findChildNode(n, "attributes"), "step"), "value").getAttributes().item(0).getNodeValue()));
				if (verbose) System.out.println("zspace = " + zspace + units.toString());
			}
		}
	}
	
	private Node findChildNode(Node node, String nodeName) {
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			Node elementNode = node.getChildNodes().item(i);
			
			if (elementNode.getNodeName().equalsIgnoreCase(nodeName)) {
//				System.out.println("---> " + elementNode.getNodeName());
				return elementNode;
			}
		}
		
		return null;
	}
	
	private void printnode(String padding, Node e) {
		if (e == null)
			return;

		String pad = "  ";

		System.out.print(padding + e.getNodeName());
		if (e.getNodeValue() != null)
			System.out.println(" = " + e.getNodeValue());
		else
			System.out.println();

		// Print attributes
		NamedNodeMap nm = e.getAttributes();
		for (int j = 0; j < nm.getLength(); j++) {
			Node n = nm.item(j);

			printnode(padding + pad, n);
		}

		// Print child nodes
		NodeList elementNodes = e.getChildNodes();
		for (int i = 0; i < elementNodes.getLength(); i++) {
			Node elementNode = elementNodes.item(i);
			printnode(padding + pad, elementNode);
		}
	}

	public double[][][] getVolume() {

		// Assume all images have same dimension
		int dx = getSlices()[0].getHeight();
		int dy = getSlicesCount();
		int dz = getSlices()[0].getWidth();

		if (verbose) System.out.println("Creating volume from " + filename + " ...");

//		System.out.println("Dims = " + dx + ", " + dy + ", " + dz);
		
		double[][][] data = new double[dx][dy][dz];

		// Store voxels
//		for (int z = 0; z < dz; z++) {
//			BufferedImage img;
//			try {
//				img = reader.read(z);
//				for (int x = 0; x < dx; x++) {
//					for (int y = 0; y < dy; y++) {
//						// Use the red channel as intensity
//						data[x][y][z] = getImageValue(img, x, y);
//					}
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		for (int z = 0; z < dz; z++) {
				for (int x = 0; x < dx; x++) {
					for (int y = 0; y < dy; y++) {
						// Use the red channel as intensity
						data[x][y][z] = getValue(x, y, z);
					}
				}
		}
		
		return data;
	}
	
	public int getSizeX() {
		return getSlices()[0].getWidth();
	}
	public int getSizeY() {
		return getSlices()[0].getHeight();
	}
	public int getSizeZ() {
		return getSlicesCount();
	}

	public double getRealSpacingX() {
		return xspace * units.getScale();
	}
	public double getRealSpacingY() {
		return yspace * units.getScale();
	}
	public double getRealSpacingZ() {
		return zspace * units.getScale();
	}

//	/**
//	 * Recreate the volume and swap X and Z axes.
//	 */
//	public void swapXZ() {
//		int[] dim = getDim();
//		
//		final double[][][] newvol = new double[dim[2]][dim[1]][dim[0]];
//		
//		this.voxelProcess(new PerVoxelMethod() {
//			@Override
//			public void process(int x, int y, int z) {
//				newvol[z][y][x] = volume[x][y][z];
//			}
//			
//			@Override
//			public boolean isValid(Point3d origin, Vector3d span) {
//				return true;
//			}
//		});
//		
//		dim = new int[] { dim[2], dim[1], dim[0] };
//		
//	}

	private int[] getDim() {
		return new int[] {  getSizeX(), getSizeY(), getSizeZ() };
	}
}
