package extension.minc;

import helicoid.fitting.Fittable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.CoordinateFrameSample;
import tools.frame.FrameRotator;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import volume.PerVoxelMethod;
import voxel.VoxelBox;

public class MINCImage4d implements PerVoxelMethod {
	
	private MINCImage in;
	private MINCImage[] ix = new MINCImage[3];
	private MINCImage[] iy = new MINCImage[3];;
	private MINCImage[] iz = new MINCImage[3];;
	private int[] bounds;
	private VoxelBox voxelBox;
	private List<Point3i> volume;
	private CoordinateFrameSample[][][][] frames;
	
	public MINCImage4d(String filename) {
		this(null, filename);
	}
	
	public MINCImage4d(VoxelBox box, String filename) {
		in = new MINCImage(filename + "_n.mnc");
		ix[0] = new MINCImage(filename + "_x1.mnc");
		ix[1] = new MINCImage(filename + "_x2.mnc");
		ix[2] = new MINCImage(filename + "_x3.mnc");
		iy[0] = new MINCImage(filename + "_y1.mnc");
		iy[1] = new MINCImage(filename + "_y2.mnc");
		iy[2] = new MINCImage(filename + "_y3.mnc");
		iz[0] = new MINCImage(filename + "_z1.mnc");
		iz[1] = new MINCImage(filename + "_z2.mnc");
		iz[2] = new MINCImage(filename + "_z3.mnc");
		
		// Set bounds from any image
		bounds = new int[] { in.getSizeX(), in.getSizeY(), in.getSizeZ() };
		
		System.out.println("Image bounds = " + Arrays.toString(bounds));
		
//		voxelBox = new VoxelBox(bounds);
		if (box != null)
			voxelBox = box;
		else 
			voxelBox = new VoxelBox(getVolumeDimension());
		
		voxelBox.addParameterListener(new ParameterListener() {
			
			public void parameterChanged(Parameter parameter) {
				updateVolume();
			}
		});

		createOrientationMaxima();
		
		createFrames();
		
		updateVolume();
	}
	
	private void updateVolume() {
//		System.out.println("Updating volume: " + Arrays.toString(voxelBox.getDimension()));
		volume = voxelBox.getVolume();
	}
	
	private void createFrames() {
		frames = new CoordinateFrameSample[1][bounds[0]][bounds[1]][bounds[2]];

		for (int i = 0; i < bounds[0]; i++) {
			for (int j = 0; j < bounds[1]; j++) {
				for (int k = 0; k < bounds[2]; k++) {
					Point3i pt = new Point3i(i, j, k);
					frames[0][i][j][k] = createFrame(pt);
				}
			}
		}
	}
	
	private Vector3d[][][][] orientationMaxima;
	
	private void createOrientationMaxima() {
		orientationMaxima = new Vector3d[bounds[0]][bounds[1]][bounds[2]][];

		for (int i = 0; i < bounds[0]; i++) {
			for (int j = 0; j < bounds[1]; j++) {
				for (int k = 0; k < bounds[2]; k++) {
					Point3i pt = new Point3i(i, j, k);
					
					List<Vector3d> maxima = computeMaxima(pt);
					if (maxima == null) {
//						System.out.println("Skipping " + pt);
						continue;
					}
					
					orientationMaxima[i][j][k] = new Vector3d[maxima.size()];
					for (int l = 0; l < maxima.size(); l++) {
						orientationMaxima[i][j][k][l] = maxima.get(l);
					}
				}
			}
		}
	}
	
	private CoordinateFrameSample createFrame(Point3i p) {
		Vector3d[] pts = getMaxima(p);
		if (pts == null || pts.length == 0) return new CoordinateFrameSample();
		
		return new CoordinateFrameSample(pts[0], itod(p));
	}

	private boolean isValidPoint(Point3i p) {
		if (p.x >= bounds[0] || p.y >= bounds[1] || p.z >= bounds[2] || p.x < 0 || p.y < 0 || p.z < 0) {
//			System.out.println("\nInvalid: " + p);
//			System.out.println("for bounds = " + Arrays.toString(bounds));
			return false;
		}
		

		return true;
	}
	
	private Vector3d[] getMaxima(Point3i p) {
		if (!isValidPoint(p)) return null;
		
		return orientationMaxima[p.x][p.y][p.z];
	}
	
	private List<Vector3d> computeMaxima(Point3i p) {
		if (!isValidPoint(p)) return null;
		
		// Number of maxima
		int n = (int) in.getValue(p.x, p.y, p.z);
		
		if (n <= 0) return null;
		
		List<Vector3d> maxima = new ArrayList<Vector3d>(n);
		
		for (int i = 0; i < n; i++) {
			Vector3d v = new Vector3d();
			v.x = ix[i].getValue(p.x, p.y, p.z);
			v.y = iy[i].getValue(p.x, p.y, p.z);
			v.z = iz[i].getValue(p.x, p.y, p.z);
			v.normalize();
			
			// FIXME: try to stay consistent in direction
			
			maxima.add(v);
		}
		
		return maxima;
	}
	
	private GL2 gl = null;
	
	public void display(GLAutoDrawable drawable) {
		gl = drawable.getGL().getGL2();
		
		gl.glLineWidth(2.0f);

		voxelBox.voxelProcess(this);
	}
	
	private double[][] maximaColors = new double[][] { { 1, 0, 0 },
			{ 0, 1, 0 }, { 0, 0, 1 } };

	
	public void process(int x, int y, int z) {
		
		int i = 0;
		Vector3d[] maxima = getMaxima(new Point3i(x, y, z));
		if (maxima == null) {
			return; 
		}
		
		gl.glBegin(GL2.GL_LINES);
		for (Vector3d v : maxima) {
			//TODO remove this
			if (i >= 1) break;
			
			gl.glVertex3d(x, y, z);
			gl.glVertex3d(x + v.x, y + v.y, z + v.z);
			gl.glColor4d(maximaColors[i][0], maximaColors[i][1], maximaColors[i][2], 1);
			i++;
		}
		gl.glEnd();
	}

	/**
	 * Get rotated and flipped coordinate frames at that point. The array first
	 * contains n rotated coordinates frames, then n corresponding flipped
	 * frames. The index correspopndance is (i, n + i).
	 */
	
	public CoordinateFrameSample[] getCoordinateFrames(Point3i p) {
		CoordinateFrameSample[] cframes = new CoordinateFrameSample[frames.length];
		
		for (int i = 0; i < frames.length; i++)
			cframes[i] = frames[i][p.x][p.y][p.z];
		return cframes;
	}

	
	public Vector3d getOrientation(Point3i p) {
		Vector3d[] maxima = getMaxima(p);

		// TODO: handle other maxima
		if (maxima != null && maxima.length > 0) return maxima[0];
		else return null;
	}
	
	
	public void getOrientation(Point3i p, double[] orientation) {
		Vector3d[] maxima = getMaxima(p);

		// TODO: handle other maxima
		if (maxima != null && maxima.length > 0) {
			orientation[0] = maxima[0].x;
			orientation[1] = maxima[0].y;
			orientation[2] = maxima[0].z;
		}
		else {
			orientation[0] = Double.NaN;
			orientation[1] = Double.NaN;
			orientation[2] = Double.NaN;
		}
	}
	
	private double[] asArray(Vector3d v) {
		return new double[] { v.x, v.y, v.z };
	}

	
	public List<Point3i> getPointList() {
		return volume;
	}
	
	private Point3i dtoi(Point3d p) {
		return new Point3i((int) p.x, (int) p.y, (int) p.z);
	}
	
	private Point3d itod(Point3i p) {
		return new Point3d(p.x, p.y, p.z);
	}

	public Point3d getCenter() {
//		return new Point3i((int)voxelBox.getSpan().x / 2, (int)voxelBox.getSpan().y / 2, (int)voxelBox.getSpan().z / 2);
		return voxelBox.getCenter();
	}

	
	public int[] getVolumeDimension() {
		return Arrays.copyOf(bounds, bounds.length);
	}
	
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(voxelBox.getControls());
		return vfp.getPanel();
	}

	
	public void precomputeCoordinateFrames(int numrots, int[] axisSamples, SamplingStyle samplingStyle) {
		
		// Each rotation contains two frames for positive and negative x
//		if (frames.length != 2 * numrots) {
			frames = new CoordinateFrameSample[2 * numrots][bounds[0]][bounds[1]][bounds[2]];

			System.out.println("Precomputing local frames...");
			
			for (int i = 0; i < bounds[0]; i++) {
				for (int j = 0; j < bounds[1]; j++) {
					for (int k = 0; k < bounds[2]; k++) {
						// Create a local frame at each voxel
						// Use the first frame to determine rotation
						Point3i pt = new Point3i(i, j, k);
						CoordinateFrame oframe = createFrame(pt);
						
						// Rotate this local frame numrots times
						List<OrientationFrame> samplePlanes = FrameRotator
						.rotateAndFlipE1(oframe, FrameAxis.X, Math.PI, numrots);

						for (int l = 0; l < 2 * numrots; l++) {
							frames[l][i][j][k] = new CoordinateFrameSample(
									samplePlanes.get(l), pt);
							frames[l][i][j][k].presampleInteger(axisSamples, samplingStyle, false);
						}

					}
				}
			}
		}
//	}
	
	public boolean isValid(Point3d origin, Vector3d span) {
		return true;
	}

	
	public void addParameterListener(ParameterListener listener) {
		voxelBox.addParameterListener(listener);
	}

	public VoxelBox getVoxelBox() {
		return voxelBox;
	}

	
	public double[] getSeedPoint(Point3i p) {
		return new double[] { 0, 0, 0 };
	}

}
