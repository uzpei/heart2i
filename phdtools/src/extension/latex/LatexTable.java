package extension.latex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tools.TextToolset;
import tools.loader.LineWriter;
import app.pami2013.cartan.CartanParameter.Parameter;

/**
 * Utility class for generating a nicely formatted LaTeX table.
 * TODO: check for number of columns match with header.
 * TODO: "$" helper for math input
 * TODO: set hline by getting row handle
 * @author piuze
 */
public class LatexTable {
	
	public static void main(String[] args) {
		List<String> header = Arrays.asList("N", "model", "Rat", "Dog");
		LatexTable table = new LatexTable(header);
		table.setCaption("This is a foo table generated from Java.");
		table.setLabel("table:fooTable");
		
		table.beginRow();
		table.addElement("$3^3$");
		table.addElement("1-form");
		table.addElement("0.000");
		table.addElement("1.000");
		table.endRow();
		
		table.beginRow();
		table.addElement("$5^3$");
		table.addElement("ghm-form");
		table.addElement("2.000");
		table.addElement("3.000");
		table.endRow();
		
		table.beginRow();
		table.addElement("$7^3$");
		table.addElement("constant");
		table.addElement("4.000");
		table.addElement("5.000");
		table.endRow();
		
		System.out.println(table.assemble());
	}
	
	public enum ColumnAlignment { 
		LEFT("l"), CENTER("c"), RIGHT("r"); 
		
		private String alignment;
		ColumnAlignment(String alignment) {
			this.alignment = alignment;
		}
		
		@Override
		public String toString() {
			return alignment;
		}
	};
	
	private final static String hline = "\\hline";
	private final static String cline = "\\cline";
	public String newLine = "\\\\";

	/*
	 * Table structure
	 */
	private ArrayList<ArrayList<String>> table;
	private ArrayList<String> header;
	private ArrayList<String> columnAlignments;
	private ArrayList<String> horizontalLines;
	private ArrayList<String> currentRow;
	
	private String caption;
	private String label;
	private String positionOption;
	
	/**
	 * Whether this table should span the whole document (in a two-column style document alike ieee).
	 */
	private boolean documentWide;
	private double arrayStretch;
	
	public LatexTable(List<String> header) {
		
		positionOption = "t";
		documentWide = false;
		arrayStretch = 1.0;
		caption = "";
		label = "";
		
		table = new ArrayList<ArrayList<String>>();
		
		horizontalLines = new ArrayList<String>();
		
		// Construct the header
		this.header = new ArrayList<String>();
		this.header.addAll(header);
		
		// Construct the initial separator
		columnAlignments = new ArrayList<String>();
		for (int i = 0; i < header.size(); i++)
			columnAlignments.add("c");
		
	}
	
	public LatexTable transpose() {

		// Initial table dimensions
		int m = table.size();
		int n = table.get(0).size();

		// Initialize transposed table
		ArrayList<ArrayList<String>> tableTransposed = new ArrayList<ArrayList<String>>();
		horizontalLines.clear();
		for (int j = 0; j < n-1; j++) {
			// Create new row
			ArrayList<String> row = new ArrayList<String>();
			for (int i = 0; i < m + 1; i++) {
				row.add("");
			}
			tableTransposed.add(row);
			horizontalLines.add("");
		}

		// Swap leftmost column towards header
		List<String> headerCopy = header;
		header = new ArrayList<String>();

		// The top-left element should stay untouched
		header.add(headerCopy.get(0));
		
		// Transfer other elements
		for (int i = 0; i < m; i++) {
			header.add(removeTags(table.get(i).get(0)));
		}
		
		// Swap header towards leftmost column
		for (int i = 0; i < n - 1; i++) {
			tableTransposed.get(i).set(0, headerCopy.get(1 + i));
		}

		/*
		 * TODO: will need to properly replace hline by vertical lines
		 * and take care of column alignments.
		 */
		columnAlignments = new ArrayList<String>();
		for (int i = 0; i < header.size(); i++)
			columnAlignments.add("c");
		

		// Create transposed inner table
		for (int i = 0; i < m; i++) {
			for (int j = 1; j < n; j++) {
				String element = removeTags(table.get(i).get(j));
				tableTransposed.get(j - 1).set(i+1, element);
			}
		}
		
		table = tableTransposed;
		
		return this;
	}

	private String removeTags(String string) {
		return string.replace(hline, "");
	}

	public void setColumnAlignment(int colIndex, ColumnAlignment alignment) {
		columnAlignments.set(colIndex, alignment.toString());
	}
	
	public void setRowStyle(int rowIndex, boolean horizontalDivider) {
		horizontalLines.set(rowIndex, horizontalDivider ? hline : "");
	}
	
	public void setProperties(String positionOption, boolean documentWide, double arrayStretch) {
		this.positionOption = positionOption;
		this.documentWide = documentWide;
		this.arrayStretch = arrayStretch;
	}
	
	public void setWide(boolean wide) {
		this.documentWide = wide;
	}
	
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public void beginRow() {
		currentRow = new ArrayList<String>();
		horizontalLines.add("");
	}
	
	public void cline(int i, int j) {
		currentRow.set(0, cline + "{" + i + "-" + j + "} " + currentRow.get(0));
	}

	public void hline(boolean asPrefix) {
		if (!asPrefix)
			currentRow.set(currentRow.size() - 1, currentRow.get(currentRow.size() - 1) + hline);
		else
			currentRow.set(0, hline + " " + currentRow.get(0));
	}
	
	public void endRow() {
		table.add(currentRow);
	}
	
	/**
	 * Add a new element in this row.
	 * @param element
	 */
	public void addElement(String element) {
		currentRow.add(element);
	}
	
	/**
	 * Construct the the tabular and header.
	 * @return
	 */
	private String beginTabular() {
		String tabular = "\\begin{tabular}";
		
		// Write column alignment
		String alignment = "{|";
		
		for (String calign : columnAlignments)
			alignment += calign + "|";
		alignment += "}";
		
		tabular += alignment + TextToolset.newLine;
		
		return tabular;
	}
	
	private String assembleElements() {
		String elements = "";
		int colIndex;

		// First write header
		elements += hline + TextToolset.newLine;
		colIndex = 0;
		for (String headerTitle : header) {
			if (colIndex != 0)
				headerTitle = " & " + headerTitle;
				
			elements += headerTitle;
			colIndex++;
		}
		
		elements += newLine + hline + TextToolset.newLine;
		
		// Then write elements
		int rowIndex = 0;
		for (ArrayList<String> row : table) {
			colIndex = 0;
			for (String element : row) {
				if (colIndex != 0)
					element = " & " + element;
				
				elements += element;
				colIndex++;
			}
			
			elements += newLine + TextToolset.space + horizontalLines.get(rowIndex) + TextToolset.newLine;
			rowIndex++;
		}
		
		return elements;
	}
	
	private String beginTable() {
		String table = "\\begin{table" + (documentWide ? "*" : "") + "}[" + positionOption + "]" + TextToolset.newLine;

		table += header();

		table += beginTabular();
		
		return table;
	}
	
	private String endTable() {
		String end = "";
		end += hline + TextToolset.newLine;
		end += "\\end{tabular}" + TextToolset.newLine;
		end += "\\vspace{5mm}" + TextToolset.newLine;
		end += "\\end{table" + (documentWide ? "*" : "") + "}" + TextToolset.newLine;
		return end;
	}

	private String header() {
		String header = "";
		
		// Array stretch
		header += "\\renewcommand{\\arraystretch}{" + arrayStretch + "}" + TextToolset.newLine;
		
		if (caption != null && caption.length() > 0)
			header += "\\caption{" + caption + "}" + TextToolset.newLine;
		if (label != null && label.length() > 0)
			header += "\\label{" + label + "}" + TextToolset.newLine;
		
		header += "\\centering" + TextToolset.newLine;
		return header;
	}
	
	public String assemble() {
		String table = beginTable();
		
		table += assembleElements();
		
		table += endTable();
		return table;
	}
	
	public String assembleToLatexDocument() {
		String document = "";
		document += "\\documentclass[10pt]{book}\n";
		document += "\\begin{document}\n";
		document += assemble();
		document += "\\end{document}\n";
		return document;
	}
	
	public void save(String filename) {
		String table = assemble();
		
		try {
			LineWriter.write(filename, table);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
