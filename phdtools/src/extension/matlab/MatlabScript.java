package extension.matlab;

import gl.renderer.SceneRotator.ViewAngle;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import system.object.Pair;
import tools.TextToolset;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import tools.loader.LineWriter;

public class MatlabScript {

	private static final String[] MatlabBinaries = new String[] { "/Applications/MATLAB_R2014a.app/bin/matlab", "/Applications/MATLAB_R2013a.app/bin/matlab", "/Applications/MATLAB_R2013b.app/bin/matlab" };
	private static final String MatlabFrameworkPath = new File("./extern/Matlab/").getAbsolutePath();

	private String buffer;

	private String filename = null;

	public MatlabScript(String filename) {
		buffer = "";
		this.filename = filename;
		cd(MatlabFrameworkPath);
		setPath();
	}

	public void setPath() {
		writeLine("setpath");
	}
	
	public MatlabScript() {
		this(createTemporaryFile());
	}
	
	public String getBuffer() {
		return buffer;
	}
	
	private static String createTemporaryFile() {
		try {
			return File.createTempFile("java_matlab_script", ".m").getAbsolutePath();
		}
		catch (IOException e) {
			// Do nothing
		}
		return null;
	}
	
	public void cd(String path) {
		writeLine("cd " + new File(path).getAbsolutePath());
	}

	public void disp(String text) {
		writeLine(asCommand("disp", toString(text)));
	}
	
	public void loadMatlabVolume(String matlabAssignedName, String volumePath) {
		writeLine(matlabAssignedName + "=" + asCommand("loadmat", toString(volumePath)));
	}
	
	public void loadMatlabVolumeAsDegrees(String matlabAssignedName, String volumePath) {
		writeLine(matlabAssignedName + "=" + asCommand("rad2deg",asCommand("loadmat", toString(volumePath))));
	}

	public void header(String text) {
		write(TextToolset.box(text, '%'));
	}
	
	public static String toString(String text) {
		return "'" + text + "'";
	}
	
	public void save() {
		save(filename);
	}
	
	@Override
	public String toString() {
		return getBuffer();
	}
	
	public void save(String filename) {
		if (filename == null)
			return;

		// Add header with machine and timestamp
		String header = "";
		try {
			header = TextToolset.box("MATLAB auto-generated script. Created by user [" + System.getProperty("user.name") + "] on [" + InetAddress.getLocalHost().getHostName() + ", " + java.lang.System.getProperty("os.arch") + "] at " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()), '%');
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			LineWriter.write(filename, header + buffer);
			this.filename = filename;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String asCommand(String commandName, String arguments) {
		return commandName + "(" + arguments + ")";
	}

	public void writeLine() {
		writeLine("");
	}
	public void writeLine(String line) {
		buffer += line + ";" + TextToolset.newLine;
	}
	
	public void write(String text) {
		buffer += text;
	}

	public void hold(boolean b) {
		if (b)
			writeLine("hold on");
		else
			writeLine("hold off");
	}
	
	public void histogram(String variableName, int bins, int smoothing, Color color, double lineWidth) {
		writeLine(asCommand("[heights, centers] = histnorm_thresholded", variableName + "," + bins + "," + smoothing + ", " + toString("Color") + ", " + toString(color) + ", " + asParameter("LineWidth", lineWidth)));
	}
	
	public void histogram(String variableName, String binVariable, String smoothingVariable, Color color, double lineWidth) {
		writeLine(asCommand("[heights, centers] = histnorm", variableName + "," + binVariable + "," + smoothingVariable + ", " + toString("Color") + ", " + toString(color) + ", " + asParameter("LineWidth", lineWidth)));
	}

	public void setFigureProperties(String xlabel, int fontSizeAxisTickLabels, int fontSizeXLabel, boolean xLabelVisible, boolean yAxisVisible) {
		// Hide the y axis if necessary
		if (!yAxisVisible) {
			hideAxis(FrameAxis.Y);
		}

		if (xLabelVisible && xlabel != null && xlabel.length() > 0) {
			// Set x label
			String xlabelHandle = "xlabelHandle";
			variable(xlabelHandle, asCommand("xlabel", toString(xlabel)));
			writeLine(asCommand("set", xlabelHandle + ", " + asParameter("FontSize", fontSizeXLabel)));
		}
		
		// Set axis font size
		setAxisFontSize(fontSizeAxisTickLabels);
	}
	
	public void setAxisFontSize(int fontSize) {
		writeLine(asCommand("set", "gca" + ", " + asParameter("FontSize", fontSize)));
	}
	
	public void setAxisLabels(FrameAxis axis, List<String> latexLabels, int fontSize) {

		String lbls = "{";
		for (String label : latexLabels) {
			lbls += "'" + label + "',";
		}
		lbls = lbls.substring(0, lbls.length() - 1);
		lbls += "}";
//		
//		writeLine("latex_ticks('" + axis.toString() + "', " + lbls + "," + fontSize + ")");
		writeLine("set(gca, 'xticklabel', " + lbls + ")");
		writeLine("set(gca,'XTick', 1:" + latexLabels.size() + ")");
		writeLine("xlim([0 " + (latexLabels.size() + 1) + "])");
//		writeLine("format_ticks(gca, " + lbls + ", [], [], [], xangle, [], [])");

	}

	public void setLatexAxisLabels(FrameAxis axis, List<String> latexLabels, int fontSize, int xLabelAngle) {

		String lbls = "{";
		for (String label : latexLabels) {
			lbls += "'" + label + "',";
		}
		lbls = lbls.substring(0, lbls.length() - 1);
		lbls += "}";
//		
//		writeLine("latex_ticks('" + axis.toString() + "', " + lbls + "," + fontSize + ")");
		writeLine("set(gca,'XTick', 1:" + latexLabels.size() + ")");
		writeLine("xlim([0 " + (latexLabels.size() + 1) + "])");
		writeLine("xangle = " + xLabelAngle);
		writeLine("format_ticks(gca, " + lbls + ", [], [], [], xangle, [], [])");

	}

	private void hideAxis(FrameAxis axis) {
		String ax = axis.toString().toLowerCase(); 
//		writeLine(asCommand("set", "gca, " + asParameter(ax + "TickLabelMode", "Manual")));
		writeLine(asCommand("set", "gca, " + toString(ax + "Tick") + ", []"));
		writeLine(asCommand("set", "gca, " + asParameter("box", "off")));
		writeLine(asCommand("set", "gca, " + asParameter(ax + "Color", "white")));
	}

	private String asParameter(String stringName, String stringValue) {
		return toString(stringName) + ", " + toString(stringValue);
	}

	private String asParameter(String stringName, Color color) {
		return toString(stringName) + ", " + toString(color);
	}

	private String asParameter(String stringName, int intValue) {
		return toString(stringName) + ", " + intValue;
	}

	private String asParameter(String stringName, double intValue) {
		return toString(stringName) + ", " + intValue;
	}
	
	public static String toString(Color color)  {
		DecimalFormat format = new DecimalFormat("#.##");
		format.setRoundingMode(RoundingMode.HALF_UP);
		
		String red = format.format(color.getRed() / 255d); 
		String green = format.format(color.getGreen() / 255d); 
		String blue = format.format(color.getBlue() / 255d); 
		
		return "[" + red + "," + green + "," + blue + "]";
	}
	
	public void clearFigure() {
		writeLine("clf");
	}
	
	public void figure(int index) {
		writeLine("figure(" + index + ")");
	}
	
	public void export_fig(String filename, boolean transparent, boolean crop) {
		writeLine(asCommand("mkdir(fileparts", toString(filename)) + ")");
		writeLine(asCommand("export_fig", toString(filename) + ", '-q101'" + (transparent? ", " + toString("-transparent") : "") + (!crop? ", " + toString("-nocrop") : "")));
	}
	
	public void legend(List<String> names, int fontSize) {
		legend(names, fontSize, "NorthEast");
	}
	public void legend(List<String> names, int fontSize, String location) {
		String legendHandle = "legendHandle";
		
		String text = "legend({";
		for (String name : names)
			text += toString(name) + ",";
		
		// Remove last comma
		text = text.substring(0, text.length() - 1);
		
		text += "}, 'location', " + toString(location) + ")";
		
		variable(legendHandle, text);
		writeLine(asCommand("set", legendHandle + ", " + asParameter("FontSize", fontSize)));
		
		writeLine(asCommand("set", legendHandle + ", " + asParameter("FontWeight", "bold")));

	}

	public void variable(String variableName, String value) {
		writeLine(variableName + " = " + value);
	}

	public void variable(String variableName, int value) {
		writeLine(variableName + " = " + value);
	}

	public void variable(String variableName, double value) {
		writeLine(variableName + " = " + value);
	}
	
	public void variable(String variableName, double[] array) {
		writeLine(variableName + " = [" + MathToolset.asCommaDelimited(array) + "]");
	}

	public void variable(String variableName, int[] array) {
		writeLine(variableName + " = [" + MathToolset.asCommaDelimited(array) + "]");
	}

	/**
	 * Create vector variables from a list of paired elements. Note that the generic types need to be castable to Double.
	 * @param names
	 * @param pairs
	 */
	public <L, R> void variable(Pair<String, String> names, List<Pair<L, R>> pairs) {
		Pair<double[], double[]> vectors = new Pair<double[], double[]>(new double[pairs.size()], new double[pairs.size()]);

		// First construct X and Y arrays
		int i = 0;
		for (Pair<L, R> pair : pairs) {
			vectors.getLeft()[i] = Double.parseDouble(pair.getLeft().toString());
			vectors.getRight()[i] = Double.parseDouble(pair.getRight().toString());
			i++;
		}
		
		// Construct matlab vector from values
		variable(names.getLeft(), vectors.getLeft());
		variable(names.getRight(), vectors.getRight());
	}

	public void thresholdRemove(String variableName, double[] threshold) {
		// e.g. 
		// c123(c123 > 1 | c123 < -1) = NaN;
		writeLine(variableName + "(" + variableName + "<" + threshold[0] + "|" + variableName + ">" + threshold[1] + ") = []");
	}

	public void thresholdClamp(String variableName, double[] clamp) {
		// e.g. 
		// c123(c123 > 1) = 1;
		// c123(c123 < -1) = -1;
		writeLine(variableName + "(" + variableName + "<" + clamp[0] + ") = " + clamp[0]);
		writeLine(variableName + "(" + variableName + ">" + clamp[1] + ") = " + clamp[1]);
	}
	
	public void caxis(double[] caxis) {
		// e.g.
		// caxis([-0.5, 0.5])
		writeLine("caxis([" + caxis[0] + ", " + caxis[1] + "])");
	}
	

	public String getFilename() {
		return filename;
	}

	public void exit() {
		writeLine("exit");
	}

	public void run() {
		if (getFilename() == null) return;
		
		save();
		
		// Search for a matlab binary
		String binary = null;
		for (String availableBinaries : MatlabBinaries) {
			if (new File(availableBinaries).exists()) {
				binary = availableBinaries;
				break;
			}
		}
		
		String command = binary + " -nodesktop -r run('" + new File(getFilename()).getAbsolutePath() + "')";

		System.out.println(command);
		Runtime rt = Runtime.getRuntime();
	     Process proc;
		try {
			proc = rt.exec(command);
			System.out.println("Matlab terminated with code " + proc.waitFor());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void run(String script) {
		try {
			File temp = File.createTempFile("script" + System.nanoTime(), null);
			LineWriter.write(temp, script);
			
			MatlabScript mscript = new MatlabScript(temp.getAbsolutePath());
			
			// Make sure the script will exit upon completion.
			mscript.exit();
			mscript.run();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void subplot(int row, int column, int figureIndex) {
//		writeLine(asCommand("subplot_tight", row + "," + column + "," + figureIndex));
		writeLine(asCommand("subplot", row + "," + column + "," + figureIndex));
	}
	
	public void imageslice(String volumeVariableName, ViewAngle angle) {
		imageslice(null, volumeVariableName, angle);
	}
	
	public void imageslice(String storeVariable, String volumeVariableName, ViewAngle angle) {
		if (storeVariable != null && storeVariable.length() > 0)
			variable(storeVariable, asCommand("image3dslice", volumeVariableName + ", " + toString(angle.toString().toLowerCase())));
		else
			writeLine(asCommand("image3dslice", volumeVariableName + ", " + toString(angle.toString().toLowerCase())));
	}
	
	public void setAxisStyle(AxisStyle style) {
		writeLine("axis " + style.toString().toLowerCase());
	}

	public void setAxisProperties(boolean xAxisVisible, boolean yAxisVisible) {
		if (!xAxisVisible && !yAxisVisible) {
			writeLine(asCommand("set", "gca," + asParameter("visible", "off")));
		}
		else if (!xAxisVisible) {
			hideAxis(FrameAxis.X);
		}
		else if (!yAxisVisible) {
			hideAxis(FrameAxis.Y);
		}
	}

	/**
	 * Concatenate a list of scripts.
	 * @param scripts
	 */
	public static MatlabScript concatenate(List<MatlabScript> scripts) {
		MatlabScript scriptConc = new MatlabScript();
		for (MatlabScript script : scripts) {
			scriptConc.header("Concatenating from " + script.filename);
			scriptConc.write(script.buffer + "\n\n");
		}

		return scriptConc;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public <T> void plot(List<T> values, int lineWidth, Color color, boolean marker) {
		plot(null, values, lineWidth, color, marker);
	}
	
	public <X, Y> void plot(List<X> x, List<Y> y, int lineWidth, Color color, boolean marker) {
		String lineStyle = toString(".-");
		if (x != null)	
			writeLine("plotHandle = plot(" + "[" + TextToolset.join(x, ",") + "]" + "," + "[" + TextToolset.join(y, ",") + "]" + ", " + lineStyle + "," + asParameter("LineWidth", lineWidth) + "," + asParameter("color", color) + ")");
		else
			writeLine("plotHandle = plot(" + "[" + TextToolset.join(y, ",") + "]" + ", " + lineStyle + "," + asParameter("LineWidth", lineWidth) + "," + asParameter("color", color) + ")");
			
//		writeLine("set(plotHandle, " + "'MarkerSize', 20, 'MarkerEdgeColor', 'black')");
		writeLine("set(plotHandle, " + "'MarkerSize', 20, 'MarkerEdgeColor', " + toString(color) + ")");
	}

	public <X, Y> void plotBar(List<X> x, List<Y> y, Color color, double spacing) {
		if (x != null)	
			writeLine("plotHandle = bar(" + "[" + TextToolset.join(x, ",") + "]" + "," + "[" + TextToolset.join(y, ",") + "]" + "," + spacing + ")");
		else
			writeLine("plotHandle = bar(" + "[" + TextToolset.join(y, ",") + "]" + "," + spacing + ")");
			
		writeLine("set(plotHandle, " + " 'FaceColor', " + toString(color) + ")");
	}

	public <X, Y> void plotBarDelta(List<X> x, List<Y> dx, double spacing) {
		writeLine("bardelta(" + "[" + TextToolset.join(x, ",") + "]'" + "," + "[" + TextToolset.join(dx, ",") + "]'" + "," + spacing + ")");
			
	}

	public void fullscreen() {
		writeLine("set(gcf, 'Position', get(0,'Screensize'))");		
	}

	public void setYLabel(String ylabel, int fontSize) {
		String labelHandle = "ylabelHandle";
		variable(labelHandle, asCommand("ylabel", toString(ylabel)));
		writeLine(asCommand("set", labelHandle + ", " + asParameter("FontSize", fontSize)));
	}

	public void setXLabel(String xlabel, int fontSize) {
		String xlabelHandle = "xlabelHandle";
		variable(xlabelHandle, asCommand("xlabel", toString(xlabel)));
		writeLine(asCommand("set", xlabelHandle + ", " + asParameter("FontSize", fontSize)));
	}

	public void xlim(double[] lims) {
		writeLine("xlim([" + lims[0] + "," + lims[1] + "])");
	}

	public void aspect(double ratio) {
		writeLine(asCommand("aspect", String.valueOf(ratio)));
	}
}
