package extension.matlab;

public enum AxisStyle {
	Tight, Image, Square;
}
