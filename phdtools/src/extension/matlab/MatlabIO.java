package extension.matlab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import math.matrix.Matrix3d;
import volume.IntensityVolume;
import voxel.VoxelBox;

import com.jmatio.io.MatFileHeader;
import com.jmatio.io.MatFileReader;
import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;
import com.jmatio.types.MLNumericArray;

public class MatlabIO {

	public static boolean verbose = false;

	public static boolean quiet = false;

	public static void save(String filename, Matrix3d volume, boolean verbose) {
		save(filename, volume, null, verbose);
	}

	public static void save(String filename, Matrix3d volume) {
		save(filename, volume, null, verbose);
	}

	public static void save(String filename, Matrix3d volume, VoxelBox box) {
		save(filename, volume, box, verbose);
	}
	
	public static String save(double[] array, boolean verbose) throws IOException {
		File temp = File.createTempFile("mat" + System.nanoTime(), null);
		String filename = temp.getAbsolutePath() + ".mat";
		save(filename, array, verbose);
		
		return filename;
	}
	
	public static void save(String filename, double[] array, boolean verbose) {
		MatlabIO.verbose = verbose;
		
		// Create matlab data
		ArrayList<MLArray> list = new ArrayList<MLArray>();
		list.add(new MLDouble("data", array, 1));

		save(filename, list);
	}
	public static void save(String filename, Matrix3d volume, VoxelBox box, boolean verbose) {

		MatlabIO.verbose = verbose;
		
		// Create matlab data
		ArrayList<MLArray> list = new ArrayList<MLArray>();
		list.add(flatten(volume, box));
		
		save(filename, list);
	}
	
	private static void save(String filename, ArrayList<MLArray> data) {
		try {
			// Create the file if it doesn't exist
			File file = new File(filename).getParentFile();
			if(file != null && !file.exists()) {
				if (verbose)
					System.out.println("Creating directory: " + file.getAbsolutePath());
				file.mkdirs();
//			    file.createNewFile();
			} 
			
			if (verbose) System.out.println("Saving " + filename);
			else if (!quiet) System.out.println("Saving " + new File(filename).getName());
			new MatFileWriter(filename, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Flatten all unmasked voxels in this volume.
	 * The order is for k, j, i
	 * @param box
	 * @return
	 */
	public static MLDouble flatten(Matrix3d volume, VoxelBox box) {
		int[] dims = volume.getDimension();
		double[][][] data = volume.getData();
		double outValue = 0;
		
//		double[] result = new double[dims[0] * dims[1] * dims[2]];
		MLDouble flat = new MLDouble("data", volume.getDimension());

		int l = 0;
		if (box == null)
			for (int k = 0; k < dims[2]; k++)
				for (int j = 0; j < dims[1]; j++)
					for (int i = 0; i < dims[0]; i++)
							flat.set(data[i][j][k], l++); 
		else {
			for (int k = 0; k < dims[2]; k++) {
				for (int j = 0; j < dims[1]; j++) {
					for (int i = 0; i < dims[0]; i++) {
						if (box.isOutside(i, j, k)) {
							flat.set(data[i][j][k], l++); 
						}
						else {
							flat.set(outValue, l++); 
						}
					}
				}
			}
		}
		
		return flat;
	}

	/**
	 * Assume double
	 * @param filename
	 * @return
	 */
	public static Matrix3d loadMAT(String filename) {
		return loadMAT(filename, new Double(0));
	}
	
	/**
	 * Load a MAT file.
	 * TODO: support struct
	 * @param filename
	 */
	public static <T extends Number> Matrix3d loadMAT(String filename, T type) {
		Matrix3d m  = null;
		
		try {
			MatFileReader reader = new MatFileReader(filename);
			MatFileHeader header = reader.getMatFileHeader();

			if (verbose) System.out.println("Loading MAT from :" + filename);
			
			if (verbose) System.out.println(header.getDescription());
			
//			List<MLArray> data = reader.getMLArray(name)
//			Map<String, MLArray> map = reader.getContent();
			List<MLArray> data = new LinkedList<MLArray>(reader.getContent().values());
			
			if (data.size() > 1) 
				throw new RuntimeException("Not supported at the moment.");

			MLNumericArray<T> mn = (MLNumericArray<T>) data.get(0);
//			MLNumericArray<Double> mn = (MLNumericArray<Double>) data.get(0);
			int[] volDim = mn.getDimensions();
			int[] dim = new int[] { 1, 1, 1 };
			
			if (volDim.length > 0)
				dim[0] = volDim[0];
			if (volDim.length > 1)
				dim[1] = volDim[1];
			if (volDim.length > 2)
				dim[2] = volDim[2];

			if (verbose) System.out.println("MAT matrix found: " + mn.name);
			if (verbose) System.out.println("Matrix dimension = " + Arrays.toString(dim));
			if (verbose) System.out.println("Flat dimensions = " + mn.getM() + "," + mn.getN());

			if (!verbose && !quiet) System.out.println("Loading MAT from: " + new File(filename).getName() + ", matrix dimension = " + Arrays.toString(dim));

			// Need to convert to XY (not IJ)
			
			
			
			m = new Matrix3d(new int[] { dim[0], dim[1], dim[2]});
			
			int ti, tj, tk;
			for (int i = 0; i < dim[0]; i++) {
				for (int j = 0; j < dim[1] * dim[2]; j++) {
					// Read columns in chunks of dims[2]
					int k = j / dim[1];

					ti = j % dim[1];
					tj = i;
					tk = k;

//					System.out.println("i = " + ti + " j = " + tj + " k = " + tk);

					// Fill in matrix
//					m.set(tj, ti, tk, mn.get(i, j).doubleValue());
					m.set(tj, ti, tk, mn.get(i, j).floatValue());
				}
			}
			
			mn.dispose();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		System.out.println(m.toString());
		
		return m;
	}
	
	public static void main(String[] args) {
//		String filename = "./data/testmatrix.mat";
//		MatlabIO.loadMAT(filename);
		
		int size = 3;
		
		IntensityVolume vtest = new IntensityVolume(size, size, size);
//		vtest.randomize();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				for (int k = 0; k < size; k++) {
					double v = Double.parseDouble("9." + ("" + i) + ("" + j) + ("" + k) + "9");
					vtest.set(i, j, k, v);
				}
			}
		}
		
		System.out.println(vtest.toString());
		
		Matrix3d m = testVolumeIO(vtest);

		System.out.println(m.toString());
		
		// Create a large randomized volume, save it, load it and compare element-wise
		size = 64;
		vtest = IntensityVolume.createRandomizedVolume(size, size / 2, size / 4);
		testVolumeIO(vtest);
	}
	
	public static Matrix3d testVolumeIO(IntensityVolume volume) {
		
		System.out.println("Old mat, dim = " + Arrays.toString(volume.getDimension()));
		String ftest = "./test.mat";
		MatlabIO.save(ftest, volume, null);
		Matrix3d m = MatlabIO.loadMAT(ftest);
		System.out.println("New mat, dim = " + Arrays.toString(m.getDimension()));
		System.out.println("Difference between two matrices: norm = " + m.subOut(volume).norm());
		
		return m;
	}

}
