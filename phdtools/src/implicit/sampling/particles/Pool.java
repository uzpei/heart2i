package implicit.sampling.particles;

import java.util.List;
import java.util.Stack;

abstract public class Pool<T> {
	/** 
	 * The maximum number of objects that can be pooled. 
	 **/
	private int max;
	
	private int peak;
	
	private final Stack<T> freeObjects;

	/** Creates a pool with an initial capacity of 16 and no maximum. */
	public Pool () {
		this(16, Integer.MAX_VALUE);
	}

	/** Creates a pool with the specified initial capacity and no maximum. */
	public Pool (int initialCapacity) {
		this(initialCapacity, Integer.MAX_VALUE);
	}

	/** @param max The maximum number of free objects to store in this pool. */
	public Pool (int initialCapacity, int max) {
		freeObjects = new Stack<T>();

		for (int i = 0; i < initialCapacity; i++)
			newObject();
		
		this.max = max;
		this.peak = freeObjects.size();
	}

	abstract protected T newObject();

	/** Returns an object from this pool. The object may be new (from {@link #newObject()}) or reused (previously
	 * {@link #free(Object) freed}). */
	public T obtain () {
		return freeObjects.size() == 0 ? newObject() : freeObjects.pop();
	}

	/** Puts the specified object in the pool, making it eligible to be returned by {@link #obtain()}. If the pool already contains
	 * {@link #max} free objects, the specified object is reset but not added to the pool. */
	public void free(T object) {
		if (object == null) throw new IllegalArgumentException("object cannot be null.");
		if (freeObjects.size() < max) {
			freeObjects.push(object);
			peak = Math.max(peak, freeObjects.size());
		}
		if (object instanceof Poolable) ((Poolable)object).reset();
	}

	/** Puts the specified objects in the pool. Null objects within the array are silently ignored.
	 * @see #free(Object) */
	public void freeAll (List<T> objects) {
		for (T object : objects) {
			free(object);
		}
	}

	/** Removes all free objects from this pool. */
	public void clear () {
		freeObjects.clear();
	}

	/** The number of objects available to be obtained. */
	public int getFreeCount() {
		return freeObjects.size();
	}

	/** Objects implementing this interface will have {@link #reset()} called when passed to {@link #free(Object)}. */
	static public interface Poolable {
		/** Resets the object for reuse. Object references should be nulled and fields may be set to default values. */
		public void reset ();
	}
}