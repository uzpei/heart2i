package implicit.sampling.particles;

import implicit.sampling.methods.PPImprovedMehod;
import implicit.sampling.methods.ParticleMeshMethod;
import implicit.sampling.methods.ParticleMethod;
import implicit.sampling.methods.ParticleMethod.ParticleMethodType;
import implicit.sampling.methods.ParticleParticleMethod;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import picking.ModelPicker;
import swing.component.CollapsiblePanel;
import swing.component.EnumComboBox;
import swing.component.FileChooser;
import swing.component.VerticalFlowPanel;
import swing.event.AdapterState;
import swing.event.ExtendedAdapter;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.StopWatch;
import tools.loader.Vertex;



/**
 * Implementation of a simple particle system
 * Tasks:
 * managing particles 
 * assigning particle neighbors
 * integrating forces
 * TODO: move mouse picking outside of this class
 * TODO: parallel processing for neighborhood lookup
 * TODO: implement contour picking in ImplicitWitkinSampler
 * @author epiuze
 */
public class ParticleSystem extends ExtendedAdapter {
    
    private ParticleMethod particleMethod = new PPImprovedMehod();

	private static final ParticleMethodType DefaultParticleMethod = ParticleMethodType.PP_IMPROVED;  
    
    private ParticleEngine particleEngine;
    
    /** A general-purpose random number generator */

    private GL2 gl;
    
    private double time = 0;

    /**
     * Trigger a system step;
     */
    public boolean stepRequest = false;

    private FileChooser fc = new FileChooser("./data/", "Load");

    private int avgNeighbor = 0;
    
    private double maxNeighborDistance = Double.MAX_VALUE;
    
    private BooleanParameter run = new BooleanParameter("run", false);
    
    private DoubleParameter stepsize = new DoubleParameter("step size", 0.005, 1e-5, 1);
    public double getStepSize() {
    	return stepsize.getValue();
    }
    
    private List<ParticleMethod> methods = new ArrayList<ParticleMethod>();

    private ModelPicker picker;

    private int activeParticleCount = 0;
    private int fixedParticleCount = 0;
    private int contourParticleCount = 0;

    /**
     * Active or disable gravity.
     */
    public BooleanParameter useg = new BooleanParameter( "use gravity", false );

    /**
     * Gravity constant.
     */
    public DoubleParameter g = new DoubleParameter( "gravity", 9.8, -50, 50 );

    /**
     * Draw bounding box.
     */
    public BooleanParameter drawbb = new BooleanParameter( "bounding box", false);

    /**
     * Draw timers.
     */
    public BooleanParameter drawTimers = new BooleanParameter( "timers", false);

    /**
     * Draw particles
     */
    public BooleanParameter drawparticles = new BooleanParameter( "particles", true);

    /**
     * Draw particle radius
     */
    public BooleanParameter drawradius = new BooleanParameter( "radius", false);

    /**
     * The attraction strength to fixed particles;
     */
    public DoubleParameter attraction = new DoubleParameter("guide attraction", 0.7, 0,
            2);

    /**
     * Velocity feedback term to remove energy.
     */
    public DoubleParameter viscosity = new DoubleParameter("viscosity", 0.01, 0,
            1);

//    public DoubleParameter sigmaSampling = new DoubleParameter(GreekLetter.SIGMA2.toString() + "(sampling)", 0.1, 0.001, 10);

    /**
     * Draw interactions.
     */
    public BooleanParameter drawNeighbors = new BooleanParameter("interactions", true);

    /**
     * Draw a line to the cell containing this particle.
     */
    public BooleanParameter drawContainer = new BooleanParameter("container", false);

    private StopWatch swNeighbor = new StopWatch("Spatial Connectivity");
    private StopWatch swExternal = new StopWatch("External forces");
    private StopWatch swUpdate = new StopWatch("Euler");

    private Point3d avgPosition = new Point3d();
    private Vector3d avgVelocity = new Vector3d();
    
    /**
     * Creates an empty particle system
     * @param ps 
     * @param functions 
     * @param mpicker 
     * @param c 
     */
    public ParticleSystem(ModelPicker mpicker) {
        picker = mpicker;
        
        particleEngine = new ParticleEngine();
        
        selectParticleMethod(DefaultParticleMethod);
    }
    
    protected void selectParticleMethod(ParticleMethodType selected) {

    	// Construct particle methods
    	if (methods.size() == 0) {
            methods.add(ParticleMethodType.PP.ordinal(), new ParticleParticleMethod());
            methods.add(ParticleMethodType.PP_IMPROVED.ordinal(), new PPImprovedMehod());
            methods.add(ParticleMethodType.PP_MESH.ordinal(), new ParticleMeshMethod(getParticles()));
    	}
    	
    	// Select
    	particleMethod = methods.get(selected.ordinal());
    	updateParticleMethodUI();
	}

    public ParticleSystem() {
    	this(null);
    }
    
    private boolean computing = false;
    
    /**
     * Updates the position of all particles based on the current particle states.
     * @param h 
     */
    public void step(boolean assignNeighbors) {
        
    	if (computing) return;
        
        // Advance the system (if it is running or wants to be stepped)
        if (!run.getValue() && !stepRequest) {
        	return;
        }
        
        double h = stepsize.getValue();

    	computing = true;
        
        // Count particle types
        activeParticleCount = 0;
        contourParticleCount = 0;
        fixedParticleCount = 0;
        for (Particle p : particleEngine.getParticles()) {
            if (p.contour) contourParticleCount++;
            else if (p.fixed) fixedParticleCount++;
            else activeParticleCount++;
        }

        // Assign particle neighbors.
        if (assignNeighbors)
        	assignNeighbors();

        // Add external forces (gravity, viscosity)
        addExternalForces();

        // Apply constraints (fixed and contour particles)
        applyConstraint(particleEngine.getParticles(), h);

        // Integrate particle system
        update(h);
        
        // Increment the current simulation time by the time step.
        time = time + h;        
        
        computing = false;
    }
    
    private void update(double h) {
        swUpdate.start();
        updateVelocity(h);
        updatePosition(h);
        swUpdate.stop();
	}

	/**
     * @param h step size
     */
    private void applyConstraint(List<Particle> particles, double h) {
        
		for (Particle pi : particles) {

            // Immobilize contour and fixed particles
            // Simple (but unstable) pinning constraint
            if (pi.contour || pi.fixed) {
                pi.v.scale(0);
                pi.f.scale(0);
                continue;
            }

//			// Prevents the system from blowing up
//			if (pi.v.length() > 2)
//				pi.v.normalize();
		}
    }

    /**
     * Initializes the forces to the the gravity force (mg).
     */
    private void addExternalForces() {
        swExternal.start();

        // Viscosity to stabilize the system
        // from the last time step.
        Vector3d v = new Vector3d();
        double drag = viscosity.getValue();
        for (Particle p : getParticles()) {
            if (p.contour) continue;
            
            v.scaleAdd(-drag, p.v, p.v);
            p.addForce(v);
        }

        // Gravity
        if (useg.getValue()) {
            Vector3d fg = new Vector3d(0, g.getValue(), 0);
            for ( Particle p : getParticles() ) {
                p.f.x += fg.x * p.mass;
                p.f.y += fg.y * p.mass;
                p.f.z += fg.z * p.mass;
            }        
        }
        
        swExternal.stop();
    }

    /**
     * Fix all particles.
     */
    public void fixParticles() {
        for (Particle p : getParticles()) {
            if (!p.contour) p.fixed = true;
        }
    }

    /**
     * Integrate forces using forward Euler
     * @param h
     */
    private void updateVelocity(double h) {
        Vector3d tmp = new Vector3d();
        
        avgVelocity.set(0, 0, 0);
        for (Particle p : getParticles()) {
            if (p.contour || p.fixed) continue;
            
            // Integrate forces using forward Euler
            tmp.scale(h / p.mass, p.f);
            p.v.scaleAdd(h / p.mass, p.f, p.v);
            avgVelocity.add(p.v);
        }
        
        avgVelocity.scale(1d / getParticles().size());
    }

    /**
     * Integrate velocities using forward Euler.
     * @param h
     */
    private void updatePosition(double h) {
        Vector3d tmp = new Vector3d();
        avgPosition.set(0, 0, 0);
        
        for (Particle p : getParticles()) {
            if (p.contour || p.fixed) continue;
            
            p.p.scaleAdd(h, p.v, p.p);
            avgPosition.add(p.p);
        }
        
        avgPosition.scale(1d / getParticles().size());
    }

    /**
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {

        gl = drawable.getGL().getGL2();
        
        if (particleMethod instanceof ParticleMeshMethod) {
            ((ParticleMeshMethod) particleMethod).display(drawable);
        }
        
        gl.glDisable(GL2.GL_LIGHTING);
                
        if (drawparticles.getValue()) {
            gl.glPointSize(5);
            gl.glBegin( GL.GL_POINTS );
            for (Particle p : getParticles()) {
                p.glVertex3d(gl);
            }
            gl.glEnd();
            
            // Display particle radius (repulsion and neighborhood)
            boolean drawRadius = drawradius.getValue();
            if (drawRadius) {
                gl.glColor4f(1, 1, 1, 0.1f);
                gl.glLineWidth(2);
                for (Particle p : getParticles()) {
                    double dt = 2 * Math.PI / 10;
                    gl.glBegin(GL.GL_LINE_STRIP);
                    for (double angle = 0; angle <= 2 * Math.PI; angle += dt) {
                        gl.glVertex3d(p.p.x + p.getRepRadius() * Math.cos(angle),
                                p.p.y + p.getRepRadius() * Math.sin(angle), p.p.z);
                    }
                    gl.glEnd();
                }
            }

            gl.glColor4f(0, 0, 1, 0.3f);
            gl.glLineWidth(2);
            boolean drawNRadius = true;
            if (drawNRadius) {
                for (Particle p : getParticles()) {
                    if (!p.fixed) continue;
                    
                    double dt = 2 * Math.PI / 10;
                    gl.glBegin(GL.GL_LINE_STRIP);
                    for (double angle = 0; angle <= 2 * Math.PI; angle += dt) {
                        gl.glVertex3d(p.p.x + p.getNeighborhoodRadius()
                                * Math.cos(angle), p.p.y
                                + p.getNeighborhoodRadius() * Math.sin(angle), p.p.z);
                    }
                    gl.glEnd();
                }
            }
            
        }

        // Display the bounding box
        if (drawbb.getValue()) {
            // TODO
        }
        
        if (getParticles().size() > 0) {
            if (drawNeighbors.getValue()) {
                // Draw a line connecting neighboring particles
                gl.glLineWidth(5.0f);
                gl.glBegin(GL.GL_LINES);
                for (Particle p : getParticles()) {
                    if (p.contour) continue;
                    
                    // Draw neighbors
                    gl.glColor4f(0, 0, 1, 0.3f);
                    for (Particle neighbor : p.getNeighbors()) {
                        gl.glVertex3d(p.p.x, p.p.y, p.p.z);
                        gl.glVertex3d(neighbor.p.x, neighbor.p.y, neighbor.p.z);
                    }
                }
                gl.glEnd();
            }
            
            if (drawContainer.getValue()) {
                // Draw the cell container
                gl.glLineWidth(5.0f);
                gl.glBegin(GL.GL_LINES);
                for (Particle p : getParticles()) {
                    if (p.contour) continue;

                    // Draw a line to the cell container
                    gl.glColor4f(1, 1, 1, 0.1f);
                    if (p.getCell() != null) {
                        gl.glVertex3d(p.p.x, p.p.y, p.p.z);
                        gl.glVertex3d(p.getCell().getCenter().x, p.getCell().getCenter().y, p.getCell().getCenter().z);
                    }
                }
                gl.glEnd();
            }
        }
        
        if (requestPicking()) {
            gl.glColor3d(0, 0, 1);
            doPicking(drawable);
        }
    }
    

//    /**
//     * Create a test system.
//     * @param which
//     */
//    public void createSystem(int which) {
//        double x, y, z;
//        
//        // Use the centroid of all boundary particles.
//        // Create 10 random particles
//        if (which == -1) {
//        	if (getParticles().size() == 0) return;
//        	
//        	Point3d centroid = new Point3d();
//        	for (Particle p : getParticles()) {
//        		if (p.contour) {
//        			centroid.add(p.p);
//        		}
//        	}
//        	centroid.scale(1d / getParticles().size());
//        	
//        	double r = 0;
//        	Vector3d dif = new Vector3d();
//        	for (Particle p : getParticles()) {
//        		if (p.contour) {
//        			dif.sub(p.p, centroid);
//        			r += dif.length();
//        		}
//        	}
//        	r /= getParticles().size();
//
//        	int n = 5;
//        	// Focus on center
//        	r /= 5.0;
//        	for (int i = 0; i < n; i++) {
//                x = centroid.x + r * rand.nextDouble() - r/2;
//                y = centroid.y + r * rand.nextDouble() - r/2;
//                z = centroid.z + r * rand.nextDouble() - r/2;
//                particleEngine.createParticle(x, y, z, false, sigmatargetp.getValue());
//        	}
//        	
//        }
//        
//        // Place n random particles
//        if (which == 1) {
//            int n = 1;
//            double r = 10;
//            for (int i = 0; i < n; i++) {
//                x = -r / 2 + r * rand.nextDouble();
//                y = -r / 2 + r * rand.nextDouble();
//                z = -r / 2 + r * rand.nextDouble();
//                particleEngine.createParticle(x, y, z, false, sigmatargetp.getValue());
//            }
//        }
//        // Test simple 2 particles repulsion
//        else if (which == 2) {
//            particleEngine.createParticle(-1, 0, 0, false, sigmatargetp.getValue());
//            particleEngine.createParticle(1, 0, 0, false, sigmatargetp.getValue());
//            
//        }
//        // Particle circle on top
//        else if (which == 3) {
//            // number of particles to create
//            int n = 50;
//            
//            double r = 1;
//            double dt = 2*Math.PI / n;
//            for (double angle = 0; angle <= 2*Math.PI; angle += dt) {
//                x = r * Math.cos(angle);
//                z = r * Math.sin(angle);
//                particleEngine.createParticle(x, 3, z, false, sigmatargetp.getValue());
//            }
//        }
////        updateIndices();
//        assignNeighbors();
//        
////        relaxParticles(1e-2, 400);
//    }

    /**
     * Gets the particles in the system
     * @return the particle set
     */
    public List<Particle> getParticles() {
    	return particleEngine.getParticles();
    }

    private void addLoadControls(VerticalFlowPanel vfp) {
        final ParticleSystem system = this;
        fc.addOpenActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                getParticles().clear();
                ParticleIO.loadParticles(system, fc.getCurrentFile().getAbsolutePath(), true);
            }

        });

        fc.addSaveActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ParticleIO.saveParticles(system, fc.getCurrentFile().getAbsolutePath(), true);
            }

        });
        vfp.add(fc);
    }

    private VerticalFlowPanel particleMethodPanel = new VerticalFlowPanel();
    
    private EnumComboBox<ParticleMethodType> ecbPmethods = new EnumComboBox<ParticleMethodType>(
			DefaultParticleMethod);

    private void addSimulationControls(VerticalFlowPanel vfp) {
        VerticalFlowPanel cpanel = new VerticalFlowPanel();
        cpanel.createBorder("Particle System");
        
        JPanel buttonPanel = new JPanel(new GridLayout(1, 4));
        
        JButton clearBtn = new JButton("Clear");
        clearBtn.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	// Clear only non fixed particles
                particleEngine.clear(false, false);
            }
        });
        buttonPanel.add(clearBtn);

        JButton btnFix = new JButton("Fix");
        btnFix.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                fixParticles();
            }
        });
        buttonPanel.add(btnFix);
        cpanel.add(buttonPanel);

        cpanel.add(run.getControls());
        cpanel.add(stepsize.getSliderControls());
        cpanel.add(viscosity.getSliderControls());

//        cpanel.add(sigmaSampling.getSliderControls());
        cpanel.add(attraction.getSliderControls());

		ecbPmethods.addParameterListener(new ParameterListener() {

			@Override
			public void parameterChanged(Parameter p) {
				selectParticleMethod(ecbPmethods.getSelected());
			}
		});
		cpanel.add(ecbPmethods.getControls());

		cpanel.add(particleMethodPanel);
		
        vfp.add(cpanel);
    }
    
    private void updateParticleMethodUI() {
    	ParticleMethod method = methods.get(ecbPmethods.getSelected().ordinal());
        particleMethodPanel.removeAll();
        
        if (method.getControls() != null) {
        	JPanel mpanel = method.getControls();
        	mpanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), ecbPmethods.getSelected().toString()));
        	CollapsiblePanel cpanel = new CollapsiblePanel(mpanel);
        	cpanel.collapse();
        	particleMethodPanel.add(cpanel);
        }
        particleMethodPanel.update();
    }

    private void addDisplayControls(VerticalFlowPanel vfp) {
        JPanel jpdraw = new JPanel();
        jpdraw.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Display settings"));
        jpdraw.setLayout(new GridLayout(3, 2));
        CollapsiblePanel pdraw = new CollapsiblePanel(jpdraw, "Display settings");
        jpdraw.add(drawparticles.getControls());
        jpdraw.add(drawTimers.getControls());
        jpdraw.add(drawradius.getControls());
        jpdraw.add(drawbb.getControls());
        jpdraw.add(drawNeighbors.getControls());
        jpdraw.add(drawContainer.getControls());
        vfp.add(pdraw);
	}

    /**
     * @return UI controls for this class.
     */
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.createBorder("Particle System");

        // Add load system
        addLoadControls(vfp);

        // Add simulation controls
        addSimulationControls(vfp);

        addDisplayControls(vfp);

//        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(),
//        "Particle System");
        return vfp.getPanel();
    }

    public String getDebugString() {
        String s = "";
        s += "\n" + "time = " + time;
        s += " (" + "h = " + stepsize.getValue() + ")";
        s += "\nparticles = " + getParticles().size();
        s += " (";
        s += "act = " + activeParticleCount;
        s += ", cont = " + contourParticleCount;
        s += ", fixed = " + fixedParticleCount;
        s += ")";        
        s += "\naverage position = " + avgPosition;
        s += "\naverage velocity = " + avgVelocity;
        s += "\naverage neighbor count = " + avgNeighbor;
        s += "\n" + "method = " + particleMethod.toString(); 
//        s += "\n" + "integrator = " + "Euler Explicit"; 
        s += (isActive() && state.altDown() ? "\n \n[CONTOUR MODE] Left click to add, right click to generate" : "");
        s += (isActive() && state.shiftDown() ? "\n \n[PICKING MODE] Left click = particle, Right click = pinned particle" : "");
   
        if (drawTimers.getValue()) {
            s += "\n" + swNeighbor.toString();
            s += "\n" + swUpdate.toString(); 
        }

        return s;
    }
        
    /**
     * Assign particle neighbors depending on the simulation method used.
     * Particle-particle (PP) method sets all particles as neighbors to all particles.
     * Particle-particle particle-mesh (P3M) method sets only nearby particles
     * as neighbors. 
     */
    public void assignNeighbors() {
        swNeighbor.start();

        // Cells within this distance
        // will be considered to assign
        // new neighbors.
        // TOOD: Control active particles and fixed
        // particles independently.
        // For now only consider 3*sigma
//        double r = sigmaSampling.getValue();
    	double rc = 0;
        for (Particle p : getParticles()) {
        	rc += p.getRepRadius();
        	
            if (p.fixed) {
                p.setNeighborhoodRadius(maxNeighborDistance * p.getRepRadius());
            }
            else {
                p.setNeighborhoodRadius(Math.max(maxNeighborDistance, 3 * p.getRepRadius()));
            }
        }
        // Adjust to thrice the average particle radius
        maxNeighborDistance = 3 * rc / getParticles().size();

        particleMethod.assignNeighbors(getParticles());
        
        // Compute average neighbor count
        if (getParticles().size() > 0) {
            avgNeighbor = 0;
            for (Particle p : getParticles()) {
                avgNeighbor += p.getNeighbors().size();
            }
            avgNeighbor /= getParticles().size();
        }
        
        swNeighbor.stop();
    }

    private Vertex pickedVertex = null;

    @Override
    public void doPicking(GLAutoDrawable drawable) {
    	if (picker == null) return;
        float[] color = new float[]{ 0, 0.7f, 0, 0.7f };
        pickedVertex = picker.pick(drawable, state.getX(), state.getY(), color);
//        System.out.println(pickedVertex.p);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
        // Handles picking
        if (pickedVertex != null) {
            // Create a new particle at this location
            if (state.mouseLeftPressed() && state.altDown()) {
                // Add the particle slightly offseted from the surface
                Point3d p = new Point3d();
                double scale = 0.01;
                p.scaleAdd(scale, new Point3d(pickedVertex.n), new Vector3d(pickedVertex.p));
                particleEngine.createParticle(p.x, p.y, p.z);
            }
        }
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_C) {
            if (state.altDown()) particleEngine.clear(false, true);
        }
    }

    @Override
    public void addComponent(Component c) {
        c.addMouseListener(state);
        c.addMouseMotionListener(state);
        c.addKeyListener(state);
    }
    
    @Override
    public boolean requestPicking() {
        if (state.altDown()) return true;
        else return false;
    }
    
    @Override
    public String toString() {
        return "Particle system";
    }
    
    @Override
    public void setAdapterState(AdapterState state) {
        this.state = state;
    }

	@Override
	public String getHandleString() {
        String s = "";
//		s += (state.altDown() ? "[alt] = particle system <--" : "[alt] = particle system") + "\n";
        s += (requestPicking() ? "-> [Left click] to add a particle \n" : "");
        s += (requestPicking() ? "-> [Right click] to add a contour point\n" : "");
        s += (requestPicking() ? "-> [Press G] to generate the contour\n" : "");
        return s;
	}

    public ParticleMethod getParticleMethod() {
        return particleMethod;
    }

    public void setParticleMethod(String me) {
        for (ParticleMethod m : methods) {
            if (me.equals(m.toString())) {
                particleMethod = m;
            }
        }
   }
      
    public void run(boolean b) {
    	stepsize.setChecked(b);
    }
    
    public ParticleEngine getParticleEngine() {
    	return particleEngine;
    }
    
	public void kill(Particle p) {
		p.dead = true;
	}

	public boolean isRunning() {
		return run.getValue();
	}

	public double getDrag() {
		return viscosity.getValue();
	}
}