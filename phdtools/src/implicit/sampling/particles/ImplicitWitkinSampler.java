package implicit.sampling.particles;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import implicit.implicit3d.functions.ImplicitFunction3D;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import math.spline.Contour;
import swing.SwingTools;
import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset.GreekLetter;
import tools.SimpleTimer;
import tools.StopWatch;
import tools.loader.Vertex;

public class ImplicitWitkinSampler implements GLObject, Interactor {
    /** Greek letters definitions */
    private static final char GAMMA = '\u03B1' + 2;  
    private static final char SIGMA = '\u03B1' + 18;  
    private static final char NU = '\u03B1' + 12;  
    private static final char RHO = '\u03B1' + 16;  
    private static final char BETA = '\u03B1' + 1;

    
	private Vector3d v1 = new Vector3d();
	private Vector3d v2 = new Vector3d();
	private Point3d tp1 = new Point3d();
	private Point3d tp2 = new Point3d();
	private double vscale = 0.1;
	private double pscale = 0.5;

    /**
     * Contains the bounding box of the particle
     * system, as delimited by contour particles.
     * min x, max x, min y, max y
     */
//    private double[] bb = new double[4];

    /**
     * A contour determining the boundary of the particle system.
     */
    private Contour contour;

    /**
     * Implicit surface function.
     */
    private ImplicitFunction3D implicitSurface;

    private DoubleParameter relaxScale = new DoubleParameter("relaxation scale", 1e-3, 0, 1e0);

    /**
     * Monitor stages of the execution.
     */
    private StopWatch swRepulsion = new StopWatch("Repulsion forces");
    private StopWatch swFission = new StopWatch("Particle Fission");
    private StopWatch swKill = new StopWatch("Particle Death");

    private IntParameter substeps = new IntParameter("substeps", 1, 1, 100);

    /**
     * Freeze fission and death.
     */
    public BooleanParameter systemFreeze = new BooleanParameter( "freeze", false);

    /**
     * Follow implicit constraint.
     */
    public BooleanParameter enableConstraint= new BooleanParameter( "constraints", true);

    /**
     * The target particle radius to use as a
     * fission criteria.
     */
    public DoubleParameter sigmatargetp = new DoubleParameter("" + SIGMA + "^ (desired radius)", 0.05, 0.001, 100);

	private IntParameter maxParticles = new IntParameter("max particles", 4000, 0, 100000);

    /**
     * The target particle count.
     */
//    public DoubleParameter autoTarget = new DoubleParameter("automatic count", 500, 0,8000);
    
    /**
     * Active or disable particle fission.
     */
    public BooleanParameter doFission = new BooleanParameter( "fission", true );

    /**
     * Active or disable particle kill.
     */
    public BooleanParameter doKill = new BooleanParameter("death", true);

    /**
     * Active or disable particle radius adaptation.
     */
    public BooleanParameter adjustr = new BooleanParameter("expansion", true);

    /**
     * The repulsion strength.
     */
    public DoubleParameter repulsion = new DoubleParameter("repulsion", 6, 0,
            100);

    /**
     * Repulsion sigma for contour particles.
     */
    public DoubleParameter contourRepulsion = new DoubleParameter("" + GreekLetter.SIGMA2.toString() + "_c contour rep.", 0.05, 0, 1);


    /**
     * Epsilon radius
     */
    public DoubleParameter deltap = new DoubleParameter("" + GreekLetter.DELTA.code + " (" + GreekLetter.SIGMA2.code + " death fraction)", 0.7, 0,
            100);

    /**
     * Fission fraction.
     */
    public DoubleParameter nup = new DoubleParameter("" + NU + " (fission fraction)", 0.2, 0,
            100);
    
    /**
     * Equilibrium speed
     */
    public DoubleParameter gammap = new DoubleParameter("" + GAMMA + " (equilibrium speed)", 4, 0,
            100);

    /**
     * Feedback coefficient.
     */
    public DoubleParameter rhop = new DoubleParameter("" + RHO + " (feedback coef)", 15, 0,
            100);

    /**
     * Prevent division by zero
     */
    public DoubleParameter betap = new DoubleParameter("" + BETA + " (prevent /0)", 10, 0,
            100);

    
    private ParticleSystem particleSystem;
    
    public ImplicitWitkinSampler(ImplicitFunction3D function) {
    	implicitSurface = function;
    	
        // By default don't automatically adjust radius
//        autoTarget.setChecked(false);
    	
        contour = new Contour();
        
        particleSystem = new ParticleSystem();
        
//        bb[0] = 0;
//        bb[1] = 2;
//        bb[2] = 0;
//        bb[3] = 2;
    }
    
    public ParticleSystem getParticleSystem() {
    	return particleSystem;
    }
    
    public void computeForces(double h) {

    	// Reset energy, forces and desired velocities
        for (Particle p : particleSystem.getParticles()) {
            p.f.set(0, 0, 0);
        }

        // Compute particle neighbors;
    	particleSystem.assignNeighbors();
    	
        // Compute repulsion, energy, velocity and radius updates.
        computeEnergyEvolution(h);
        
        // Project particles on implicit surface
        // If we use implicit constraints on desired particle velocity
        if (enableConstraint.getValue()) {
        	setImplicitVelocities(h);
        }   
        
        setDesiredVelocities();

        // Integrate velocity and position
    	particleSystem.step(false);

        // Fission particles that have too much energy
        // (see method description).
        swFission.start();
        if (doFission.getValue() && 2 * particleSystem.getParticles().size() <= maxParticles.getValue()) 
//        if (doFission.getValue()) 
        	fissionParticles(false, h);
        swFission.stop();
        
        // Kill particles that are not needed anymore
        // (see method description).
        swKill.start();
        if (doKill.getValue()) 
        	killParticles();
        swKill.stop();
        
        // Purge particles
        particleSystem.getParticleEngine().purgeParticles();
    }
    
    private void setDesiredVelocities() {
        for (Particle pi : particleSystem.getParticles()) {
        	pi.v.set(pi.dP);
        }
    }
    
    /**
     * @param h step size
     */
    private void setImplicitVelocities(double h) {
        Vector3d dF = new Vector3d();
        double phi = rhop.getValue();
        double F;
        double epsilon = 1e-6;
        
		for (Particle pi : particleSystem.getParticles()) {
			if (pi.contour || pi.fixed) {
				continue;
			}

			// Evaluate the implicit function at the particle position
			F = implicitSurface.F(pi.p);

			// Evaluate the gradient of the implicit surface at the particle
			// position
			dF = implicitSurface.dF(pi.p);

			// TODO: why negate? We need to be consistent
			// about whether normals are pointing inwards or outwards
			// from the implicit surface.
//			dF.normalize();
//			dF.negate();

			// Project the particle onto the implicit surface using
			if (dF.length() > epsilon) {
//			if (Math.abs(F) > epsilon) {
				pi.dP.scale(1 - particleSystem.getDrag());
				pi.dP.scaleAdd(-(dF.dot(pi.dP) + phi * F) / dF.dot(dF), dF, pi.dP);
//				pi.dP.scaleAdd(-dF.dot(pi.dP), arg1)
			} 
			else {
				pi.dP.set(0, 0, 0);
			}
		}
	}

    private void computeEnergyEvolution(double h) {
        swRepulsion.start();

        // Compute the repulsion energy and force for each particle
        double Eij, Eji, r, r2, si, sj, sj2, si2, si3;
        Vector3d dr = new Vector3d();
        double Dsigma_i = 0;
        double Di = 0;
        double Etarget = 0.8 * repulsion.getValue();
        double rho = rhop.getValue(); // Drifting coef
        double beta = betap.getValue(); // Prevent division by 0
        double c_rep = contourRepulsion.getValue();
        for (Particle particle : particleSystem.getParticles()) {

            if (particle.contour) 
            	particle.setRepRadius(c_rep);
            
            si = particle.getRepRadius();
            si2 = si * si;
            si3 = si * si * si;
            
            // Zero the potential energy accumulator
            Eij = 0;
            Eji = 0;
            Dsigma_i = 0;
            Di = 0;
            
            particle.energy = 0;
            particle.dP.set(0, 0, 0);
            particle.sigmadot = 0;

            HashSet<Particle> neighbors = particle.getNeighbors();
            
            for (Particle neighbor : neighbors) {
//                if (neighbor == particle || (particle.contour && neighbor.contour))
                if (neighbor == particle)
                    continue;

                // Vector from the neighbor to this particle
                dr.sub(particle.p, neighbor.p);
                r = dr.length();
                r2 = r * r;
                dr.normalize();

                // Compute repulsion energy
                sj = neighbor.getRepRadius();
                sj2 = sj * sj;
                Eij = repulsion.getValue() * Math.exp(-r2 / (2 * si2));
//                Eji = repulsion.getValue() * Math.exp(-r2 / (2 * sj2));

                // Increment the portion of this particle's repulsion energy
                // that is directly affected by a change in its own repulsion radius
                Di += Eij;

                // Increment the total energy of this particle
                particle.energy += (Eij + Eji);
//                neighbor.energy += Eij;

                // Adjust desired velocity
//                particle.dP.scaleAdd(Eij / (si * si) - Eji / (sj * sj), dr, particle.dP);
                particle.dP.scaleAdd(Eij, dr, particle.dP);

                // Increment the change in energy with respect to a change in radius
                // This will then need to be divided by the cube of the repulsion radius.
                // This is the change in energy with respect to a change in radius
                Dsigma_i += (r2 * Eij) / si3;
            }
            
			// linear feedback equation
			// where rho is the feedback constant.
			double Ddot_i = -rho * (Di - Etarget);

			// This is the change in the repulsion radius for this particle.
			double sigmadot = Ddot_i / (Dsigma_i + beta);

			particle.sigmadot = sigmadot;
		}
        
        if (adjustr.getValue()) {
        	double pr, pdr;
            for (Particle particle : particleSystem.getParticles()) {
                if (!particle.fixed) {
                	pr = particle.getRepRadius();
                	pdr = particle.sigmadot;
                	
                    particle.setRepRadius(pr + h * pdr);
                }
            }
        }

        swRepulsion.stop();
    }

    /** 
     * Fission particles based on the following:
     * 1) The particle is near equilibrium
     * AND
     * 2) either the particle radius is huge OR
     * (the particle is sufficiently energetic
     * AND has a sufficient radius).
     * @param forced if we disregard the preconditions and just do the fission.
     */
    public void fissionParticles(boolean forced, double stepSize) {
        if (!forced) {
            double Etarget = 0.8 * repulsion.getValue();
            double gamma = gammap.getValue();
            double sigmatarget = sigmatargetp.getValue();
            double sigmalarge = 1.5 * sigmatarget;
            
            double nu = nup.getValue();
//            double sigmamax = Math.max(Math.max(bb[1]-bb[0], bb[3] - bb[2]) / 2, 1.5 * sigmatarget);
            double radius;
            for (Particle p : particleSystem.getParticles()) {
                if (p.contour || p.fixed) continue;
                
                radius = p.getRepRadius();

                // If the particle is near equilibrium
                if (p.v.length() < gamma * radius) {
                	// The particle's repulsion radius is very large
                	// or it is adequately energized and its radius is above the desired radius
                    if (radius > sigmalarge || (p.energy > nu * Etarget && radius > sigmatarget)) {
                        fission(p, stepSize);
                    }
                }
            }
        }
        else {
            for (Particle p : particleSystem.getParticles()) {
                if (p.contour) continue;
                fission(p, stepSize);
            }
        }
    }


    private final double dsqrt2 = 1d / Math.sqrt(2d);
    
	/**
	 * Fission this particle into two subparticles.
	 * @param p1
	 * @param newIndex
	 * @param surface
	 * @return
	 */
    public void fission(Particle p, double stepSize) {
    	double repRadius = p.getRepRadius();
        
        // New particle radius is scaled down by 1 / sqrt(2)
        double d = repRadius * dsqrt2;

        vscale = 1;
        
        // Split the particle in two opposite direction.
        // This direction is chosen randomly
        v1.set(-1 + 2 * rand.nextDouble(), -1 + 2 * rand.nextDouble(), -1 + 2 * rand.nextDouble());
        v1.normalize();
        v1.scale(repRadius * vscale);
        
        v2.set(v1);
        v2.negate();
        
        // Project on the surface if necessary
//        if (surface != null) {
//        	// Find the projection of the point onto the tangent plane
//            Vector3d dF = surface.dF(p.p);
//            v1.cross(v1, dF);
//            v2.cross(v2, dF);
//        }
        
        // Assign new positions and velocities
        // Particles are moved slightly in the direction of their velocity
        // to prevent initial overlap and energy explosion.
        tp1.set(p.p);
        tp1.scaleAdd(stepSize, v1, p.p);
        Particle p1 = particleSystem.getParticleEngine().obtain(tp1.x, tp1.y, tp1.z, v1.x, v1.y, v1.z, d, false, 0);
        
        tp1.set(p.p);
        tp1.scaleAdd(stepSize, v2, p.p);
        Particle p2 = particleSystem.getParticleEngine().obtain(tp1.x, tp1.y, tp1.z, v2.x, v2.y, v2.z, d, false, 0);
        
        if (p.contour) {
            p1.contour = true;
            p2.contour = true;
        }
     
        // Mark the fissioned particle as dead
        p.dead = true;
    }

    private Random rand = new Random();
    
    /**
     * Kill particles based on the following:
     * 1) The particle is near equilibrium
     * AND
     * 2) The particle's repulsion radius is too small
     * AND
     * 3) A biased randomized test succeeds.
     */
    public void killParticles() {
        double delta = deltap.getValue();
        double gamma = gammap.getValue();
        double sigmatarget = sigmatargetp.getValue();
        double r;

        // We need at least one particle
        if (particleSystem.getParticles().size() == 1)
        	return;
        
        for (Particle p : particleSystem.getParticles()) {
            if (p.contour || p.fixed) continue;
            
            r = p.getRepRadius();
            
            // Kill the particle conditional to:
            // The particle is near equilibrium and the particle's repulsion radius is too small
            if (p.v.length() < gamma * r && r < delta * sigmatarget) {
            	// Only kill if the randomized test succeeds (stochastic to prevent mass killing)
                if (rand.nextDouble() > r / (delta * sigmatarget)) {
                	// Kill this particle
                	particleSystem.kill(p);
                }
            }
        }
    }

    /**
     * Relax this particle by projecting it onto the implicit surface.
     * @param p
     * @param rate
     * @param numit
     */
    private void relaxParticle(Particle p, double rate, int numit, double epsilon) {
        double F;
        
        tp1.set(p.p);
        for (int i = 0; i < numit; i++) {
            v2 = implicitSurface.dF(p.p);
            F = implicitSurface.F(p.p);                    
                                
            // Otherwise do another gradient descent iteration.
            // Lower the rate to prevent oscillation.
            v2.normalize();
            v2.scale(-Math.signum(F) * rate);
//            v2.scale(-Math.abs(F) * rate);
            p.p.add(v2);
            
            double delta = p.p.distance(tp1);
            if (delta < epsilon) {
            	System.out.println("Stopping at " + i);
            	break;
            }
        }
    }
    /**
     * Do a gradient descent to relax particles on the implicit surface.
     * TODO: add epsilon threshold if we are close enough to the boundary 
     * (i.e. F ~ +- e)
     * @param rate 
     * @param numit 
     */
    public void relaxParticles(double rate, int numit, double epsilon) {
        for (Particle p : particleSystem.getParticles()) {
            relaxParticle(p, rate, numit, epsilon);
        }
    }

    private void relaxParticles() {
        relaxParticles(relaxScale.getValue(), 100, Double.MAX_VALUE);
    }
    
    public void addContourPoint(Vertex v) {
        contour.addPoint(new Vertex(v));
    }
    /**
     * Generate contour particles from this contour.
     * Relax their position by projecting them on the implicit surface.
     */
    public void generateContour() {
        if (contour.getSpline().size() > 0) {
            // First clear all other particles
           particleSystem.getParticleEngine().clear();
            
            // Generate contour particles
            // skip the last particle to avoid overlap
            for (Point3d p : contour.getSpline()) {
               particleSystem.getParticleEngine().createParticle(p.x, p.y, p.z, false, sigmatargetp.getValue());
            }

            // Connect the end points
            Point3d pend = contour.getLastPoint().p;
           particleSystem.getParticleEngine().createParticle(pend.x, pend.y, pend.z, false, sigmatargetp.getValue());

            // Important: project the contour onto the implicit surface.
            relaxParticles(relaxScale.getValue(), 500, Double.MAX_VALUE);
            
            // Now these particles are part of the contour.
            for (Particle p : particleSystem.getParticles()) {
                p.contour = true;
            }            
        }
    }
    
    /**
     * Refine the cloud of particles by creating particles at mid points.
     */
    public void refineParticles() {
    	HashSet<Particle> done = new HashSet<Particle>();
//    	List<Particle> nps = new LinkedList<Particle>();
    	SimpleTimer st = new SimpleTimer();
		List<Point3d> closest = new LinkedList<Point3d>();
		Point3d npt = new Point3d();
    	System.out.print("Creating particles...");
    	st.tick_s();
    	int created = 0;
    	for (Particle p : particleSystem.getParticles()) {
    		// Skip contour
    		if (p.contour) continue;
    		
    		HashSet<Particle> ns = p.getNeighbors();
    		closest.clear();
    		// Find mid points for each neighbor
    		// Only consider 5 neighbors
    		for (Particle n : ns) {
    			if (done.contains(n) || n.contour) {
    				continue;
    			}
    			
    			if (closest.size() <= 5) {
    				closest.add(n.p);
    			}
    			// Only keep 5 closest neighbors
    			else {
        			double d = p.distance(n.p);
        			double maxd = 0;
        			int maxi = -1;
    				for (int i = 0; i < closest.size(); i++) {
    					double dd = closest.get(i).distance(p.p);
    					if (dd < d) continue;
    					else {
    						if (dd > maxd) {
    							maxd = dd;
    							maxi = i;
    						}
    					}
    				}
    				if (maxi > -1) closest.set(maxi, n.p);
    			}
    		}

    		// Compute average dist
    		// could precompute this
    		double avgd = 0;
    		for (Point3d pt : closest) {
    			avgd += pt.distance(p.p);
    		}
    		avgd /= closest.size();
//			double rr = p.getRepRadius() / 2;
			double rr = avgd / 2;;
			p.setRepRadius(rr);
			
    		double eps = 0.8 * avgd;
    		for (Point3d pt : closest) {
    			
    			// Remove neighbors that are too far, are too close
//    			if (pt.distance(p.p) > avgd + eps) continue;
//    			if (pt.distance(p.p) < 0.5 * eps) continue;
    			
    			npt.sub(pt, p.p);
    			npt.scale(0.5);
    			npt.add(p.p);

//    			Particle np = new Particle(npt.x,npt.y,npt.z);
//    			np.setRepRadius(rr);
//    			nps.add(np);
    			particleSystem.getParticleEngine().createParticle(npt.x, npt.y, npt.z, false, rr);
    			created++;
    		}
    		
    		
    		done.add(p);
    	}
    	
    	// Add new particles
//    	particles.addAll(nps);

    	List<Particle> particles = particleSystem.getParticleEngine().getParticles();
    	System.out.println(created + " particles created (total = " + particles.size() + "). Time = " + st.tick_s());    	

    	// Relax their position
    	System.out.print("Relaxing...");
    	st.tick_s();
    	relaxParticles(relaxScale.getValue(), 1, Double.MAX_VALUE);
    	System.out.println(st.tick_s());

    	System.out.print("Computing repulsion...");
    	st.tick_s();
    	computeEnergyEvolution(particleSystem.getStepSize());
    	System.out.println(st.tick_s());

    	// Assign neighbors (for next refinement step)
    	System.out.print("Assigning neighbors...");
    	st.tick_s();
    	particleSystem.assignNeighbors();
    	System.out.println(st.tick_s());

    	
    	// Remove particles that are too close
    	// ... TODO
    }
    public Contour getContour() {
    	return contour;
    }

	private void addEvolutionControls(VerticalFlowPanel vfp) {
		
		VerticalFlowPanel evoPanel = new VerticalFlowPanel();
		evoPanel.createBorder("Particle evolution");
        evoPanel.add(substeps.getSliderControls());
        evoPanel.add(maxParticles.getSliderControls());

        JPanel energyPanel = new JPanel(new GridLayout(3, 2));
        energyPanel.add(systemFreeze.getControls());
        energyPanel.add(doFission.getControls() );
        energyPanel.add(doKill.getControls() );
        systemFreeze.addParameterListener(new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {

                boolean b = !systemFreeze.getValue();
                
//                sigmaSampling.setChecked(!b);
//                if (b) sigmaSampling.setDefaultValue(sigmaSampling.getValue());
//                sigmaSampling.setValue( !b ? sigmaSampling.getDefaultValue() : 0);
//                
//                attraction.setChecked(!b);
//                if (b) attraction.setDefaultValue(attraction.getValue());
//                attraction.setValue( !b ? attraction.getDefaultValue() : 0);

                doKill.setEnabled(b);
                doKill.setValue(b);
                
                doFission.setValue(b);
                doFission.setEnabled(b);

                adjustr.setValue(true);
//                adjustr.setEnabled(true);
            }
            
        });

        energyPanel.add(adjustr.getControls() );
        energyPanel.add(enableConstraint.getControls() );
        
        JButton refBtn = new JButton("Refine");
        refBtn.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                refineParticles();
            }
        });
        energyPanel.add(SwingTools.encapsulate(refBtn));
        evoPanel.add(energyPanel);
        vfp.add(evoPanel);
        
        VerticalFlowPanel vfps = new VerticalFlowPanel();
        JPanel spanel = vfps.getPanel();
        spanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Sampling Parameters"));

//        vfps.add(autoTarget.getSliderControlsExtended());
//        autoTarget.setChecked(false);
        
        vfps.add(contourRepulsion.getSliderControls());
        vfps.add(sigmatargetp.getSliderControls());
        vfp.add(spanel);
    }
    
    private void addEvolutionParametersControls(VerticalFlowPanel vfp) {
        VerticalFlowPanel vfp2 = new VerticalFlowPanel();
        JPanel ppanel = vfp2.getPanel();
        ppanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Sampling parameters"));
        CollapsiblePanel cparam = new CollapsiblePanel(ppanel, "Sampling parameters");
        vfp2.add(deltap.getSliderControls(false));
        vfp2.add(nup.getSliderControls(false));
        vfp2.add(gammap.getSliderControls(false));
        vfp2.add(rhop.getSliderControls(false));
        vfp2.add(betap.getSliderControls(false));
        vfp2.add(repulsion.getSliderControls(false));
        vfp.add(cparam);
        cparam.collapse();
    }
    
    @Override
	public JPanel getControls() {
    	VerticalFlowPanel vfp = new VerticalFlowPanel();
    	vfp.createBorder("Particle Sampling");
    	
    	vfp.add(particleSystem.getControls());
    	
        // Add contour controls
        vfp.add(contour.getControls());

    	addEvolutionControls(vfp);
    	addEvolutionParametersControls(vfp);
    	
    	return vfp.getPanel();
    }
    
    public void mousePressed(MouseEvent e) {
        
//        // Handles picking
//        if (pickedVertex != null) {
//            // If the left mouse button is down
//            // this point is added.
//            if (state.mouseRightPressed() && state.altDown()) {
//            	addContourPoint(pickedVertex);
//            }
//            // Create a new particle at this location
//            else if (state.mouseLeftPressed() && state.altDown()) {
//                // Add the particle slightly offseted from the surface
//                Point3d p = new Point3d();
//                double scale = 0.01;
//                p.scaleAdd(scale, new Point3d(pickedVertex.n), new Vector3d(pickedVertex.p));
//                particleEngine.createParticle(p.x, p.y, p.z);
//            }
//        }
    }
    
    public String getDebugString() {
        String s = particleSystem.getDebugString();
        
        s += "\ncontour count =" + contour.getPointCount();
        s += "\n" + swRepulsion.toString();
        s += "\n" + swFission.toString();
        s += "\n" + swKill.toString();
        return s;
    }

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		if (particleSystem.isRunning()) {
	        for (int i = 0; i < substeps.getValue(); i++) {
	    		computeForces(particleSystem.getStepSize());
	        }
		}
        
		particleSystem.display(drawable);
        contour.display(drawable);
	}

	@Override
	public String getName() {
		return "Particle Sampling";
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
		    @Override
		    public void keyPressed(KeyEvent e) {
		        if (e.getKeyCode() == KeyEvent.VK_F) {
		            fissionParticles(true, 1e-3);
		        }
		        else if (e.getKeyCode() == KeyEvent.VK_S) {
		            relaxParticles();
		        }
		        else if (e.getKeyCode() == KeyEvent.VK_G && e.isAltDown()) {
		            generateContour();
		        }
		    }

		});
	}

	@Override
	public void detach(Component component) {
	}

//    private void adjustParticleCount() {
//        double timestep = 0.1;
//        double minrate = 4; // particle death/creation per unit of time
//        double rate = (lastCount - activeParticleCount) / timestep;
//        double absrate = Math.abs(rate);
//        lastCount = activeParticleCount;
//
////        System.out.println(rate);
//
//        double csigma = sigmatargetp.getValue();
//        double tol = 0.05;
//        double speed = 1;
//        double ptarget = autoTarget.getValue();
//        double error = (ptarget - activeParticleCount) / ptarget;
//        double absError = Math.abs(error);
//        
//        // If the death/fission rate is too high, adjust the radius.
////        if (absrate > 0.1 * tol * diff && Math.signum(rate) == Math.signum(error)) {
//////            sigmatargetp.setValue(csigma - Math.signum(rate) * speed * tol);
////            //wait
////            return;
////        }
//        
//        // If there is at least one particle in the system
//        // AND enough time has passed (to enforce stabilization)
//        // AND the current rate of particle fission is too low
//        // lower the target repulsion radius (to enforce fission).
//        if (activeParticleCount > 0 && time - particleCountTime > timestep && absrate < minrate) {
//            
//            if (absError > tol) {
//                // If the error is too great, prevent
//                // excessive death/fission by incrementing a smaller value.
//                csigma *= 1 - Math.signum(error) * speed * tol;
//                
//                sigmatargetp.setValue(csigma);
//
//            }
//            
//            particleCountTime = time;
//            lastCount = activeParticleCount;
//        }
//        // If we don't have any particles, take direct action
//        // (force the creation of new particles).
//        else if (activeParticleCount == 0 && !(autoTarget.getValue() <= 0.5)) {
//            // TODO
//        }
//    }
}
