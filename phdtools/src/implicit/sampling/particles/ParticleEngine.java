package implicit.sampling.particles;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class ParticleEngine {

	private Random r = new Random();
	private int allocated = 0;
	
	private boolean hasChanged = false;
	private List<Particle> particles;
	private List<Particle> freshParticles;

	public ParticleEngine() {
		// Create a new pool with 1000 particles and unlimited size.
//		super(0, Integer.MAX_VALUE);
		
		particles = new LinkedList<Particle>();
		freshParticles = new LinkedList<Particle>();
	}

	/**
	 * Update the inner state by freeing dead particles.
	 */
	public void purgeParticles() {
		List<Particle> activeParticles = new LinkedList<Particle>();
		
		activeParticles.addAll(freshParticles);
		freshParticles.clear();
		
		// Purge particles
		for (Particle p : particles) {
			if (p.dead) {
//				free(p);
			}
			else {
				activeParticles.add(p);
			}
		}
		
		// Free dead particles
		particles.clear();
		
		particles = activeParticles;
	}
	
	public List<Particle> getParticles() {
		if (hasChanged) {
			purgeParticles();
			hasChanged = false;
		}
		
		return particles;
//        if (particles == null) return new LinkedList<Particle>();
//        else return particles;
	}
	
	public void kill(Particle p) {
		p.dead = true;
		hasChanged = true;
	}

//	@Override
	protected Particle newObject() {
		Particle p = new Particle();
		p.index = ++allocated;
		return p;
	}
	
//	@Override
	public void clear() {
		for (Particle p : particles) {
			p.dead = true;
		}

		hasChanged = true;
	}

//	@Override
	public Particle obtain() {
		return obtain(0, 0, 0, 0, 0, 0, 0, false, 0);
	}
	
	public Particle obtain(double x, double y, double z, double vx, double vy, double vz, double repulsionRadius, boolean contour, int specialIndex) {
//		Particle p = super.obtain();
		Particle p = newObject();
		
		p.dead = false;
		
        p.setInitial(x, y, z, vx, vy, vz);
        p.setRepRadius(repulsionRadius);
        p.contour = contour;
        p.special_index = specialIndex;

		freshParticles.add(p);
		
		hasChanged = true;

		return p;
	}

	public void clear(boolean fixBit, boolean contourBit) {
		purgeParticles();
		for (Particle p : particles) {
			if (p.fixed == fixBit && p.contour == contourBit) {
				p.dead = true;
			}
		}
		hasChanged = true;
	}

	/**
	 * Only free fixed particles.
	 */
	public void clearFixed() {
		clear(true, false);
	}
	
	/**
	 * Only clear not-fixed and not-contour particles.
	 */
	public void clearActive() {
		for (Particle p : particles) {
			if (!p.fixed && !p.contour) {
				p.clearNeighbors();
				p.dead = true;
			}
		}
	}

    public Particle createParticle(double x, double y, double z) {
    	return createParticle(x, y, z, 0, 0, 0, Particle.DEFAULT_RADIUS, false, 0);
    }

    public Particle createParticle(double x, double y, double z, boolean contour, int specialIndex, double repulsionRadius) {
    	return createParticle(x, y, z, 0, 0, 0, repulsionRadius, contour, specialIndex);
    }

    public Particle createParticle(double x, double y, double z, boolean contour, double repulsionRadius) {
    	return createParticle(x, y, z, 0, 0, 0, repulsionRadius, contour, 0);
    }
    
    public Particle createParticle(double x, double y, double z, double vx, double vy, double vz, double repulsionRadius) {
    	return obtain(x, y, z, vx, vy, vz, repulsionRadius, false, 0);
    }
    
    public Particle createParticle(double x, double y, double z, double vx, double vy, double vz, double repulsionRadius, boolean contour, int specialIndex) {
    	return obtain(x, y, z, vx, vy, vz, repulsionRadius, contour, specialIndex);
    }
}
