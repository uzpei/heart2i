package implicit.sampling.particles;

import implicit.sampling.grid.ParticleCell;
import implicit.sampling.particles.Pool.Poolable;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;



/**
 * @author piuze
 */
public class Particle implements Poolable {
    
    public static double DEFAULT_RADIUS = 0.05;
    
    public boolean dead = false;
    
    /**
     * If this particle is part of the surface contour.
     */
    public boolean contour = false;

    /**
     * If this particle is fixed.
     */
    public boolean fixed = false;
    
    public double sigmadot = 0;
    
    /**
     * Desired velocity of this particle.
     */
    public Vector3d dP = new Vector3d();

    /**
     * @return The desired velocity for this particle.
     */
    public Vector3d getDesiredVelocity() {
        return dP;
    }
    
    /**
     * Set the desired velocity for this particle.
     * @param v
     */
    public void setDesiredVelocity(Vector3d v) {
        dP.set(v);
    }
    
    /**
     * The cell containing this particle.
     */
    private ParticleCell cell;
    
    /**
     * The cells that are in reach of this particle.
     */
    private List<ParticleCell> neighborCells = new LinkedList<ParticleCell>();
    
    /**
     * Neighboring cells.
     */
    private HashSet<Particle> neighbors = new HashSet<Particle>();

    /**
     * The total energy of this particle.
     */
    public double energy = 0;
    
    /**
     * The repulsion radius of this particle.
     */
    private double rep_radius = 0;

    /**
     * The neighborhood radius of this particle.
     */
    private double n_radius = 0;
        
    /**
     * This particle's position.
     */
    public Point3d p = new Point3d();

    /**
     * This particle's velocity.
     */
    public Vector3d v = new Vector3d();

    /**
     * This particle's initial position.
     */
    public Point3d p0 = new Point3d();
    
    /**
     * This particle's initial velocity.
     */
    Vector3d v0 = new Vector3d();

    /**
     * The accumulated force on this particle.
     */
    public Vector3d f = new Vector3d();
    
    /**
     * The mass of this particle.
     */
    public double mass = 1.0;
    
    /**
     * An index representing this particle. Set to zero initially.
     * There is no guarantee on the uniqueness of this index.
     */
    public int index = 0;
    
    public int special_index = -1;

    /**
     * Creates a particle.
     * @param x 
     * @param y 
     * @param z 
     * @param vx 
     * @param vy 
     * @param vz 
     * @param radius 
     */
    public Particle(double x, double y, double z, double vx, double vy, double vz, double radius) {
        p0.set(x, y, z);
        v0.set(vx, vy, vz);
        reset();
        this.rep_radius = radius;
    }

    /**
     * Creates a particle.
     * @param x 
     * @param y 
     * @param z 
     * @param vx 
     * @param vy 
     * @param vz 
     * @param radius 
     */
    public Particle(double x, double y, double z, double vx, double vy, double vz, double radius, boolean contour) {
        p0.set(x, y, z);
        v0.set(vx, vy, vz);
        reset();
        this.rep_radius = radius;
        this.contour = contour;
    }

    /**
     * Creates a particle with the given position and velocity
     * @param x
     * @param y
     * @param vx
     * @param vy
     */
    public Particle(double x, double y, double z, double vx, double vy, double vz) {
        this(x, y, z, vx, vy, vz, DEFAULT_RADIUS);
    }

    /**
     * Creates a particle with the given position and velocity
     * @param x
     * @param y
     * @param vx
     * @param vy
     */
    public Particle(double x, double y, double z) {
        this(x, y, z, 0, 0, 0, DEFAULT_RADIUS);
    }

    /**
     * Create a default particle at the origin.
     */
    public Particle() {
        p0.set(0, 0, 0);
        v0.set(0, 0, 0);
        rep_radius = DEFAULT_RADIUS;
        reset();
    }

    /**
     * Create a particle and makes a copy of this one.
     * @param other
     */
    public Particle(Particle other) {
        this(other.p.x, other.p.y, other.p.z, other.v.x, other.v.y, other.v.z, other.rep_radius);
    }

    /**
	 * @param pr
	 */
	public Particle(Point3d p) {
		this(p.x, p.y, p.z);
	}

	/**
     * Resets the position of this particle
     */
	@Override
	public void reset() {
		p0.scale(0);
		v0.scale(0);
        p.set(p0);
        v.set(v0);
        f.set(0, 0, 0);
		dP.scale(0);
        clearNeighbors();
        dead = true;
    }
    
    /**
     * Add the given force to this particle
     * @param force
     */
    public void addForce(Vector3d force ) {
        f.add(force);
    }

    /**
     * Add the given force to this particle
     * @param force
     */
    public void addForce(Vector3d force, double s) {
        f.x += s * force.x;
        f.y += s * force.y;
        f.z += s * force.z;
    }

    /**
     * Computes the distance of a point to this particle
     * @param pt A point.
     * @return the distance to this point
     */
    public double distance(Point3d pt) {
        return pt.distance(p);
    }
   
    private double alpha = 0.5;
    
    public void glVertex3d(GL2 gl) {

        if ( contour ) {
            gl.glColor4d(1, 0, 0, alpha);
        }
        else if (fixed) {
            gl.glColor4d(0, 0, 1, alpha);
        }
        else {
            gl.glColor4d(0, 0.95,0, alpha);
        }

        gl.glVertex3d(p.x, p.y, p.z);
    }

    public ParticleCell getCell() {
        return cell;
    }
    
    public List<ParticleCell> getNeighborCells() {
        return neighborCells;
    }
    
    public void addNeighborCell(ParticleCell fluidcell) {
        neighborCells.add(fluidcell);
    }
    
    public void setCell(ParticleCell fluidCell) {
        cell = fluidCell; 
    }

    public void clearNeighbors() {
        cell = null;
        neighbors.clear();
        neighborCells.clear();
        
    }
    
    public double getEnergy() {
        return energy;
    }
    
    public HashSet<Particle> getNeighbors() {
        return neighbors;
    }
    
    public void addNeighbor(Particle p) {
        if (p == this) return;
        
        neighbors.add(p);
    }

    /**
     * Set this particle's repulsion radius. Enforce this value to be larger than 0.
     * @param value
     */
    public void setRepRadius(double value) {
        rep_radius = Math.max(0, value);
    }

    /**
     * Set this particle's neighborhood radius.
     * @param value
     */
    public void setNeighborhoodRadius(double value) {
        n_radius = value;
    }

    /**
     * @return This particle's repulsion radius.
     */
    public double getRepRadius() {
        return rep_radius;
    }

    /**
     * @return This particle's neighborhood radius.
     */
    public double getNeighborhoodRadius() {
        return n_radius;
    }

    @Override
	public String toString() {
        String s = "";
        s +=  "(" + index + ")\n";
        s += "State = ";
        if (contour) s += "contour ";
        if (fixed) s += "fixed ";
        s += "\n" + p;
        s += "\nEnergy = " + energy;
        s += "\nradius = " + rep_radius;
        s += "\n" + neighbors.size() + " neighbors";
        s += "\n" + neighborCells.size() + " cells";
        
        return s;
    }

	public void setInitial(double x, double y, double z, double vx, double vy, double vz) {
		p.set(x, y, z);
		v.set(vx, vy, vz);
		dP.set(v);
		p0.set(p);
		v0.set(v);
		f.scale(0);
		clearNeighbors();
	}

}
