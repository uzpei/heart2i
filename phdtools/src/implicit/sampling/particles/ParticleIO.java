package implicit.sampling.particles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3d;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import tools.xml.XMLException;
import tools.xml.XMLHelper;

public class ParticleIO {
    
    private static void writeSystem(ParticleSystem system, BufferedWriter out) throws IOException {
        out.write("\n   <System");
        out.write(" sigma=\"" + system.sigmatargetp.getValue() + "\"");
        out.write(" step=\"" + system.stepsize.getValue() + "\"");
        out.write(" method=\"" + system.getParticleMethod() + "\"");
        out.write("/>");

    }
    
    private static List<Particle> writeParticles(List<Particle> particles, boolean includeContour, BufferedWriter out) throws IOException {
        List<Particle> plist = new LinkedList<Particle>();
        for (Particle p : particles) {
            
            if (p.contour && !includeContour) continue;
            
            // Info we want to keep:
            // position, contour
            // energy, repulsion radius, neighborhood radius
            out.write("\n   <Particle");
            out.write(" xpos=\"" + p.p.x + "\"");
            out.write(" ypos=\"" + p.p.y + "\"");
            out.write(" zpos=\"" + p.p.z + "\"");
            out.write(" contour=\"" + p.contour + "\"");
            out.write(" radius=\"" + p.getRepRadius() + "\"");
            out.write("/>");
            
            plist.add(new Particle(p));
        }
        
        return plist;
    }
    
    private static BufferedWriter writeHeader(String filename) throws IOException {
        // Create file
        FileWriter fstream = new FileWriter(filename);
        BufferedWriter out = new BufferedWriter(fstream);

        System.out.println("Saving particles to "
                + (new File(filename)).getAbsolutePath());

        // XML header
        out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        out.write("\n<ParticleSystem>");

        return out;
    }
    
    private static void writeFooter(String filename, BufferedWriter out) throws IOException {
        out.write("\n</ParticleSystem>");
        out.close();
    }

    /**
     * Saves information about this particle system to an xml file.
     * 
     * @param system
     * @param filename
     * @param includeContour 
     */
    public static void saveParticles(ParticleSystem system, String filename, boolean includeContour) {

        List<Particle> particles = system.getParticles();

        BufferedWriter out;
        try {
            out = writeHeader(filename);
            writeSystem(system, out);
            writeParticles(particles, includeContour, out);
            writeFooter(filename, out);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Saves information about this particle system to an xml file.
     * 
     * @param system
     * @param filename
     */
    public static void saveParticles(ParticleSystem system, String filename) {
        saveParticles(system, filename, true);
    }

    /**
     * Save particles to an xml file.
     * @param particles 
     * @param filename
     * @param includeContour 
     * @return 
     */
    public static List<Particle> saveParticles(List<Particle> particles, String filename, boolean includeContour) {

        BufferedWriter out;
        List<Particle> plist = null;
        try {
            out = writeHeader(filename);
            plist = writeParticles(particles, includeContour, out);
            writeFooter(filename, out);
        } catch (Exception e) {// Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        
        return plist;
    }
    
    /**
     * Saves information about this particle system to an xml file.
     * @param particles 
     * @param filename
     */
    public static List<Particle> saveParticles(List<Particle> particles, String filename) {
        return saveParticles(particles, filename, true);
    }

    public static void loadParticles(ParticleSystem system, String filename) {
        loadParticles(system, filename, false);
    }
    
    public static void loadSystem(ParticleSystem system, Element header) throws XMLException {
        // First load system info
        Element systemels = XMLHelper.getFirstChildElementByTagName(header,
                "System");

        if (systemels != null && systemels.hasAttributes()) {
            double sigma = XMLHelper.getFloatAttribute(systemels, "sigma", 0);
            double step = XMLHelper.getFloatAttribute(systemels, "step", 0);
            String meth = XMLHelper.getStringAttribute(systemels, "method", "");
            system.sigmatargetp.setValue(sigma);
            system.stepsize.setValue(step);
            system.setParticleMethod(meth);
        }
    }
    
    public static void loadParticles(List<Particle> particles, Element header, boolean includeContour) throws XMLException {
        boolean contour;
        double radius;
        List<Element> elements = XMLHelper.getChildElementListByTagName(
                header, "Particle");
        for (Element e : elements) {

            Point3d p = new Point3d();
            p.x = XMLHelper.getFloatAttribute(e, "xpos", 0);
            p.y = XMLHelper.getFloatAttribute(e, "ypos", 0);
            p.z = XMLHelper.getFloatAttribute(e, "zpos", 0);
            
            contour = XMLHelper.getBooleanAttribute(e, "contour", false);

            radius = XMLHelper.getDoubleAttribute(e, "radius", 0);

            if (contour && !includeContour) continue;

            particles.add(new Particle(p.x,p.y,p.z,0,0,0,radius,contour));
        }
        
    }
    public static void loadParticles(ParticleSystem system, String filename, boolean includeContour) {
        File f = new File(filename);

        System.out.println("Loading particle system from "
                + f.getAbsolutePath());

        Document document = null;

        try {
            document = XMLHelper.openAsDOM(f.getAbsolutePath());

            Element header = document.getDocumentElement();

            // First load system info
            loadSystem(system, header);

            // Then load particles
            List<Particle> particles = new LinkedList<Particle>();
            loadParticles(particles, header, includeContour);
            system.getParticles().clear();
            system.getParticles().addAll(particles);

            System.out.println("Loaded " + particles.size() + " particles.");

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }
    public static void loadParticles(List<Particle> particles, String filename) {
       loadParticles(particles, filename, false);
    }

    public static void loadParticles(List<Particle> particles, String filename, boolean includeContour) {
        File f = new File(filename);

        System.out.println("Loading particle system from "
                + f.getAbsolutePath());

        Document document = null;

        try {
            document = XMLHelper.openAsDOM(f.getAbsolutePath());

            Element header = document.getDocumentElement();

            // Load particles
            boolean contour;
            List<Element> elements = XMLHelper.getChildElementListByTagName(
                    header, "Particle");
            for (Element e : elements) {

                Point3d p = new Point3d();
                p.x = XMLHelper.getFloatAttribute(e, "xpos", 0);
                p.y = XMLHelper.getFloatAttribute(e, "ypos", 0);
                p.z = XMLHelper.getFloatAttribute(e, "zpos", 0);

                contour = XMLHelper.getBooleanAttribute(e, "contour", false);

                
                if (contour && !includeContour) continue;
                
                Particle par = new Particle(p.x, p.y, p.z);
                par.contour = contour;
                particles.add(par);
            }

            System.out.println("Loaded " + particles.size() + " particles.");

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

}
