package implicit.sampling.grid;

import implicit.rendering.marching.Cube;
import implicit.sampling.particles.Particle;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.vecmath.Point3d;

/**
 * @author piuze
 */
public class ParticleCell {
    
    private double radius;
    private Point3d center = new Point3d();
    
    private Cube cube;
    /** 
     * Particles contained in that cell.
     */
    private List<Particle> particles = new LinkedList<Particle>();
    
    /**
     * Neighboring cells
     */
    private HashSet<ParticleCell> neighbors = new HashSet<ParticleCell>();

    /**
     * Creates a new cell at this location with
     * this radius, collapse level and parent cell.0
     * @param x0
     * @param y0
     * @param cradius
     */
    public ParticleCell(double x0, double y0, double z0, double cradius) {
        center.set(x0, y0, z0);
        radius = cradius;
    }
    
    /**
     * Empty this cell from
     * particles.
     * Should not get rid of neighbors
     * (the root needs to remember its neighbors)
     */
    public void empty() {
        particles.clear();
    }

    public Point3d getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    /**
     * The assignment is recursive such that
     * we go down the cell tree to the leaf.
     */
    public void addParticle(Particle particle) {
        // The particle is added to this cell
        particles.add(particle);
    }
    
    public List<Particle> getParticles() {
        return particles;
    }
    
    public void addNeighbor(ParticleCell c) {
        neighbors.add(c);
    }
    
    /**
     * @param pt A point.
     * @return Whether or not this cell contains this point.
     */
    public boolean contains(Point3d pt) {
        return true 
        && pt.x >= center.x - getRadius() 
        && pt.x <= center.x + getRadius()
        && pt.y >= center.y - getRadius()
        && pt.y <= center.y + getRadius()
        && pt.z >= center.z - getRadius()
        && pt.z <= center.z + getRadius();
    }
    
    /**
     * Display this cell.
     * @param gl
     */
    public void display(GL2 gl) {
        // Draw grid center points
        gl.glPointSize(10);
        gl.glColor4d(0, 1, 1, 0.9);
        gl.glBegin(GL.GL_POINTS);
        gl.glVertex3d(getCenter().x, getCenter().y, getCenter().z);
        gl.glEnd();

        if (particles.size() == 0) {
            gl.glColor4f(1, 1, 1, 0.5f);
        }
        else {
            gl.glColor4f(0, 0, 1, 0.5f);
        }
    }
    
    /**
     * Computes the distance of a point to this cell
     * @param x
     * @param y
     * @return the distance
     */
    public double distance(double x, double y, double z) {
        Point3d tmp = new Point3d(x, y, z);
        return tmp.distance(center);
    }

    @Override
	public String toString() {
        String s = "";
        s += "\n" + center;
        s += "\nradius = " + radius;
        s += "\n" + particles.size() + " particles";
        s += "\n" + neighbors.size() + " cell neighbors";
        
        return s;
    }

    public HashSet<ParticleCell> getNeighbors() {
        return neighbors;
    }

    public void clearNeighbors() {
        neighbors.clear();
    }

}
