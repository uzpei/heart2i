package implicit.sampling.grid;

import gl.renderer.JoglTextRenderer;
import implicit.sampling.particles.Particle;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.StopWatch;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * 
 * @author piuze
 */
public class ParticleGrid {

    private List<ParticleCell> cells = new LinkedList<ParticleCell>();

    private Point3d position = new Point3d();

    private List<Particle> particles;

    private Random rand = new Random();

    private DoubleParameter energyMin = new DoubleParameter("energy threshold", 0, 0, 10000);

    private BooleanParameter autoGrid = new BooleanParameter("adaptive grid resolution", true);

    private DoubleParameter gratio = new DoubleParameter("cell to grid ratio", 0.21, 0, 2);

    /** 
     * The size of the default grid.
     * Used only in particle-particle mode. 
     */
    private DoubleParameter gridSize = new DoubleParameter("size", 3, 0, 20);

    /**
     * The resolution of the default grid.
     * Used only in particle-particle mode.
     */
    private DoubleParameter gridResolution = new DoubleParameter("resolution",
            7, 1, 15);


    private BooleanParameter draw = new BooleanParameter("draw", true);

    private BooleanParameter drawNeighbors = new BooleanParameter("draw neighbors", false);

    /**
     * If we are currently preventing parameter listeners to execute their code.
     * (i.e. to change many of them at the same without having them all
     * trigger the same event one after the other).
     */
    private boolean holding = false;
    
    public ParticleGrid(List<Particle> ps) {
        particles = ps;
        
        createMesh();
    }

    /**
     * 
     * @param bb The mesh bounding box.
     */
    public void updateMesh(double[] bb) {
        position.x = bb[0];
        position.y = bb[2];
        
        double dx = bb[1] - bb[0];
        double dy = bb[3] - bb[2];
        double size = Math.max(dx, dy);
        
        gridSize.setValue(size);
    }

    private void updateResolution(double ratio) {
        
        // Adjust grid size and resolution
        double tratio = gratio.getValue();
        double error = Math.abs(tratio - ratio) / tratio;
        // Percentage error allowed
        double delta = 0.15;
        
        boolean verbose = false;
        
        if (error > delta) {
            if (ratio > tratio && !(gridResolution.getValue() == 0)) {
                hold(true);
                if (verbose) System.out.println("Adaptive grid: " + ratio + " to " + tratio + " has error " + (error*100) + "%. Lowering the resolution.");
                gridResolution.setValue(gridResolution.getValue() - 1);
                hold(false);
                createMesh();
            } else if (ratio < tratio ) {
                hold(true);
                if (verbose) System.out.println("Adaptive grid: " + ratio + " to " + tratio + " has error " + (error*100) + "%. Augmenting the resolution.");
                gridResolution.setValue(gridResolution.getValue() + 1);
                hold(false);
                createMesh();
            }
        }

    }
    
    private void hold(boolean b) {
        holding = b;
    }
  
    private StopWatch swCreate = new StopWatch("Create mesh");

    private void createMesh() {
        
        if (holding) return;

        swCreate.start();
        
//        System.out.print("Recreating particle mesh...");
        
        cells.clear();
        
        double gsize = gridSize.getValue();
        double x0 = position.x - gsize/2;
        double y0 = position.y - gsize/2;
        double z0 = position.z - gsize/2;
        double gres = Math.round(gridResolution.getValue());

        double radius = gsize / gres;
        
        double x, y, z;
        for (int i = 0; i < gres; i++) {
            for (int j = 0; j < gres; j++) {
                for (int k = 0; k < gres; k++) {
                    x = x0 + i * radius + radius / 2;
                    y = y0 + j * radius + radius / 2;
                    z = z0 + k * radius + radius / 2;
                    cells.add(new ParticleCell(x, y, z, radius));
                }
            }
        }
        
        
        // Expensive way of testing for neighborhood
        // FIXME: Delete this and using tree recursion instead
        ParticleCell c1, c2;
        // Use the first cell as a reference since (for now) all cells
        // have the same radius.
        double maxDistance = Math.sqrt(2) * cells.get(0).getRadius() + 1e-4;
        for (ParticleCell c : cells) {
            c.clearNeighbors();
        }
        for (int i = 0; i < cells.size() - 1; i++) {
            c1 = cells.get(i);
            
            for (int j = i + 1; j < cells.size(); j++) {
                c2 = cells.get(j);
                
                if (c1.getCenter().distance(c2.getCenter()) <= maxDistance) {
                    c1.addNeighbor(c2);
                    c2.addNeighbor(c1);
                }
            }
        }

//        System.out.println("Done with " + cells.size() + " cells.");
        
        swCreate.stop();
    }

    private StopWatch swEmpty = new StopWatch("Empty");
    private StopWatch swMatch = new StopWatch("Match");
    
    /**
     * Update the grid with the current system state.
     */
    public void update() {

        // Update grid size, cell connectivity, etc.
        updateGrid();

        // Match particles and cells.
        swMatch.start();
        matchParticles();
        swMatch.stop();
    }

    private void updateGrid() {
        if (autoGrid.getValue()) {
            // Compute the average radius
            double avgr = 0;
            double count = 0;
            for (Particle p : particles) {
                if (!p.contour) {
                    avgr += p.getRepRadius();
                    count ++;
                }
            }
            if (count > 0) {
                avgr /= count;
                
                // Adjust the grid
                double csize = 0.5 * gridSize.getValue() / gridResolution.getValue();
                double targetRatio = 1.5*avgr / csize;
                updateResolution(targetRatio);
            }
        }
        
        // Remove particles from all cells.
        swEmpty.start();
        emptyCells();
        swEmpty.stop();
    }

    private void matchParticles() {
        
        ParticleCell closest = null;
        double distance, closestDistance;
        
        // Match each particle with the closest cell
        for (Particle p : particles) {
            p.clearNeighbors();
            
            // Find closest cell
            closestDistance = Double.MAX_VALUE;
            for (ParticleCell c : cells) {
                // If this root cell contains the particle,
                // assign it.
                // The assignment is recursive such that
                // we go down the cell hierarchy until we
                // reach a leaf.
                distance = c.distance(p.p.x, p.p.y, p.p.z);
                if (c.contains(p.p) && distance < closestDistance) {
                    closestDistance = distance;
                    closest = c;
                    
                    // If we are close enough, no need to search for another cell
//                    if (distance <= p.getNeighborhoodRadius()) break;
                }
            }
            
            if (closest != null) {
                closest.addParticle(p);
                p.setCell(closest);
            }

        }

        // Particles from neighboring cells
        // are set as neighbors
        double r;
        for (Particle p : particles) {
            
            // USE PP METHOD ON FIXED PARTICLES
            // SUCH THAT THEY INFLUENCE ALL OTHER PARTICLES
            if (p.fixed) {
                double dist;

                for (Particle neighbor : particles) {
                        if (neighbor == p) continue;
                        
                        dist = p.p.distance(neighbor.p);
                        if (dist <= p.getNeighborhoodRadius()) {
                            p.addNeighbor(neighbor);
                        }
                }

            }
            else {
            	// This particle was not matched
                if (p.getCell() == null) {
                	p.dead = true;
                    continue;
                }
                
                // First add particles inside the same cell
                for (Particle neighbor : p.getCell().getParticles()) {
                    if (p == neighbor) continue;
                    
                    r = p.p.distance(neighbor.p);
                    
                    if (r <= p.getNeighborhoodRadius()) {
                        p.addNeighbor(neighbor);
                    }
                }                
                
                // Then add neighbors
                for (ParticleCell c : p.getCell().getNeighbors()) {
                    for (Particle neighbor : c.getParticles()) {
                        if (p == neighbor) continue;
                        
                        r = p.p.distance(neighbor.p);
                        
                        if (r <= p.getNeighborhoodRadius()) {
                            p.addNeighbor(neighbor);
                        }
                    }                
                }
            }
        }
    }

    private void emptyCells() {
        for (ParticleCell cell : cells) {
            cell.empty();
        }
    }

    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();

        vfp.add(gratio.getSliderControls(false));
        vfp.add(autoGrid.getControls());
        vfp.add(energyMin.getSliderControls(false));
        vfp.add(gridSize.getSliderControls(false));
        vfp.add(gridResolution.getSliderControls(false));
        vfp.add(draw.getControls());
        vfp.add(drawNeighbors.getControls());
        
        ParameterListener l = new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                createMesh();
            }
        };
        gridSize.addParameterListener(l);
        gridResolution.addParameterListener(l);
        
        return vfp.getPanel();

    }

    public List<ParticleCell> getCells() {
        return cells;
    }

    public int getParticleCellCount() {
        int count = 0;
        for (ParticleCell c : cells) {
            count += c.getParticles().size();
        }
        
        return count;
    }

    /**
     * 
     * @param gl2
     */
    public void display(GLAutoDrawable drawable) {
        
        GL2 gl = drawable.getGL().getGL2();

        String t = "";
        t += swCreate.toShortString();
        t += "\n" + swMatch.toShortString();
        JoglTextRenderer.printTextLines(drawable, t, 10, drawable.getHeight() - 20, new float[] { 1, 1, 0 }, GLUT.BITMAP_HELVETICA_10);

        gl.glDisable(GL2.GL_LIGHTING);
        
        if (draw.getValue()) {
            for (ParticleCell c : cells) {
                c.display(gl);
            }
        }
        
//        System.out.println(cells.size());
        
        if (drawNeighbors.getValue()) {
            gl.glColor4f(1, 1, 0, 0.5f);
            gl.glBegin(GL.GL_LINES);
            for (ParticleCell c1 : cells) {
                for (ParticleCell c2 : c1.getNeighbors()) {
                    gl.glVertex3d(c1.getCenter().x, c1.getCenter().y, c1.getCenter().z);
                    gl.glVertex3d(c2.getCenter().x, c2.getCenter().y, c2.getCenter().z);
                }
            }
            gl.glEnd();
        }
    }

}
