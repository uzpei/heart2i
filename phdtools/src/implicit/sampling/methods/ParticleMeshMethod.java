package implicit.sampling.methods;

import implicit.sampling.grid.ParticleGrid;
import implicit.sampling.particles.Particle;

import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;

/**
 * @author piuze
 */
public class ParticleMeshMethod implements ParticleMethod {

    private ParticleGrid grid;
        
    public ParticleMeshMethod(List<Particle> particles) {
        grid = new ParticleGrid(particles);

    }
    
    @Override
    public void assignNeighbors(List<Particle> particles) {
        // Update the adaptive mesh
        // and assign particle neighbors.
        grid.update();
        
        // Remove particles that are outside the grid
        for (Particle p : particles) {
            if (p.getCell() == null)
            	p.dead = true;
        }
    }

    @Override
    public JPanel getControls() {
        return grid.getControls();
    }

    @Override
    public String toString() {
        return "Particle-Particle/Particle-Mesh";
    }

    public void display(GLAutoDrawable drawable) {
        grid.display(drawable);
    }


}
