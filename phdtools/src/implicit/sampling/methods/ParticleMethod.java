package implicit.sampling.methods;

import implicit.sampling.particles.Particle;

import java.util.List;

import javax.swing.JPanel;

public interface ParticleMethod {
    
	public enum ParticleMethodType { PP, PP_IMPROVED, PP_MESH };
	
    /**
     * Assign neighbors to a list of particles.
     * @param particles
     */
    public void assignNeighbors(List<Particle> particles);
    
    /**
     * @return A descriptive name for this particle method.
     */
    @Override
	public String toString();
    
    /**
     * @return A JPanel controlling this particle method.
     */
    public JPanel getControls();
    
}
