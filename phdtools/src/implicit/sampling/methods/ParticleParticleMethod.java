package implicit.sampling.methods;

import implicit.sampling.particles.Particle;

import java.util.List;

import javax.swing.JPanel;

/**
 * @author piuze
 */
public class ParticleParticleMethod implements ParticleMethod {

    @Override
    public void assignNeighbors(List<Particle> particles) {
        for (Particle p : particles) {
            p.clearNeighbors();
        }
        
        for (Particle p1 : particles) {
        	for (Particle p2 : particles) {
        		if ((p1.contour && p2.contour) || (p1 == p2))
        			continue;
        		
                // Contour particles don't know about
                // each other.
        		p1.addNeighbor(p2);
            }
        }

//        Particle pp1, pp2;
//        for (int i = 0; i < particles.size() - 1; i++) {
//            pp1 = particles.get(i);
//            
//            for (int j = i + 1; j < particles.size(); j++) {
//                pp2 = particles.get(j);
//
//                // Contour particles don't know about
//                // each other.
//                if (!(pp1.contour && pp2.contour)) {
//                    pp1.addNeighbor(pp2);
//                    pp2.addNeighbor(pp1);
//                }
//            }
//        }
    }

    @Override
    public JPanel getControls() {
        // Nothing to control really...
        return null;
    }

    @Override
    public String toString() {
        return "Particle-Particle";
    }

}
