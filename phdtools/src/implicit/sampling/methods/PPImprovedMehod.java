package implicit.sampling.methods;

import implicit.sampling.particles.Particle;

import java.util.List;

import javax.swing.JPanel;

public class PPImprovedMehod implements ParticleMethod {

    @Override
    public void assignNeighbors(List<Particle> particles) {
        // Small improvement by considering only particles that
        // are less than 3*sigma radius away.
        double dist;

        for (Particle p1 : particles) {
            p1.clearNeighbors();
            for (Particle p2 : particles) {
                if (p1 == p2)
                    continue;

                dist = p1.p.distance(p2.p);
                if (dist <= p1.getNeighborhoodRadius()) {
                    p1.addNeighbor(p2);
                }
            }
        }
    }

    @Override
    public JPanel getControls() {
        // TODO Control the neighborhood radius explicitly and remove
        // it from the Particle class.
        return null;
    }

    @Override
    public String toString() {
        return "Particle-Particle Improved";
    }

}
