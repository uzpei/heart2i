package implicit.implicit3d.interpolation;

import javax.vecmath.Point3d;

/**
 * @author piuze
 */
public class Constraint {
    public enum Orientation { INTERIOR, EXTERIOR, BOUNDARY };
    
    public Point3d p;
    public double val;
    public Orientation orientation;
    public int index;
    
    /**
     * @param position
     * @param value
     */
    public Constraint(Point3d position, int index, Orientation orientation) {
        this.orientation = orientation;
        
        switch (orientation) {
        case INTERIOR:
            val = 1;
            break;
        case EXTERIOR:
            val = -1;
            break;
        case BOUNDARY:
            val = 0;
            break;
        }
        p = new Point3d(position);
        this.index = index;
    }

    public Constraint(Point3d p, Orientation o) {
        this(p, 0, o);
    }

	public double getValue() {
		return val;
	}
}
