package implicit.implicit3d.interpolation;

import implicit.implicit3d.functions.ImplicitFunction3D;
import implicit.implicit3d.interpolation.Constraint.Orientation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import no.uib.cipr.matrix.DenseLU;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.MatrixEntry;
import no.uib.cipr.matrix.MatrixSingularException;
import no.uib.cipr.matrix.NotConvergedException;
import no.uib.cipr.matrix.SVD;
import no.uib.cipr.matrix.TridiagMatrix;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import swing.component.FileChooser;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.loader.PolygonSoup;
import tools.loader.Vertex;
import tools.xml.XMLHelper;

public abstract class ImplicitSurface extends ImplicitFunction3D {

    protected final List<Constraint> constraints = new LinkedList<Constraint>();
    
    protected DenseVector weights;
    
    protected double p0, p1, p2, p3;

    protected final Vector3d tempV = new Vector3d();
    
    /**
     * Whether the thin-plate spline has already been interpolated.
     */
//    protected boolean interpolated = false;
    
    protected final Point3d offset = new Point3d();
    
    protected final DoubleParameter xo = new DoubleParameter("RBF offset x", 0, -10, 10);
    protected final DoubleParameter yo = new DoubleParameter("RBF offset y", 0, -10, 10);
    protected final DoubleParameter zo = new DoubleParameter("RBF offset z", 0, -10, 10);

    /**
     * Careful!!! Larger values cause instability!
     */
    protected final DoubleParameter svdT = new DoubleParameter("SVD threshold", 0, Double.MIN_VALUE, 1e1);

    public BooleanParameter drawConstraints = new BooleanParameter("draw contraints", true);

    protected final HashSet<Integer> pickedIndices = new HashSet<Integer>();

    protected String constraintFile = "src/implicit/interpolation/constraints.xml";

    public void setInterpolationRequest() {
    	setHasChanged(true);
    }
    
    protected abstract double basisFunction(double x, double y, double z, Point3d c);
	protected abstract double basisFunction(Constraint c1, Constraint c2);

    /**
     * Interpolate the control points that have been set. 
     */
    public void interpolate() {
    	
        System.out.print("Interpolating thin-plate function... ");
        int k = constraints.size();
        int n = 4 + k;
        
        if (k <= 0) {
        	System.err.println("No constraints found!");
        }

        DenseMatrix A = new DenseMatrix(n, n);
        
        // Inter-RBF evaluations (top left)
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < k; j++) {
                A.set(i, j, basisFunction(constraints.get(i), constraints.get(j)));
            }
        }
        
        // 1-column (middle)
        for (int i = 0; i < k; i++) {
            A.set(i, k, 1);
        }

        // 1-column (bottom)
        for (int j = 0; j < k; j++) {
            A.set(k, j, 1);
        }

        // 4x4 lower right 0-matrix
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                A.set(k+i, k+j, 0);
            }
        }
        
        // RBF constraints
        Point3d p = new Point3d();
        for (int j = 0; j < k; j++) {
        	Constraint c = constraints.get(j);
            p.set(c.p);
            
            A.set(j, k+1, p.x);
            A.set(j, k+2, p.y);
            A.set(j, k+3, p.z);

            A.set(k+1, j, p.x);
            A.set(k+2, j, p.y);
            A.set(k+3, j, p.z);
        }

        DenseVector b = new DenseVector(n);
        
        for (int i = 0; i < k; i++) {
            b.set(i, constraints.get(i).val);
        }
        b.set(k, 0);
        b.set(k+1, 0);
        b.set(k+2, 0);
        b.set(k+3, 0);
        
        weights = new DenseVector(n);

        
        boolean useSVD = svdT.isChecked();

        // Solve A * weights = b
        if (useSVD) {
        	svdSolve(A, b, weights);
        }
        else {
        	// These matrix methods will fail if A is singular
        	// TODO: compute determinant != 0
        	
        	boolean LUfact = true;
        	
        	if (LUfact) {
        		try {
            		System.out.println("Computing LU factorization...");
                    DenseLU lu = DenseLU.factorize(A);
                    DenseMatrix B = new DenseMatrix(b);
                    System.out.println("Solving...");
                    lu.solve(B);
                    weights.set(new DenseVector(B.getData()));
        		}
        		catch (MatrixSingularException e) {
        			// If the matrix is singular, use SVD
        			System.err.println("Matrix is singular. Using SVD.");
        			svdSolve(A, b, weights);
        		}
        	}
        	else {	
        		// other method?
        	}
        }
        
        // Degree one polynomial accounting for the linear and constant
        // portions of f
        p0 = weights.get(k);
        p1 = weights.get(k+1);
        p2 = weights.get(k+2);
        p3 = weights.get(k+3);
        
        setHasChanged(false);
        System.out.println("done.");
    }


    protected void svdSolve(DenseMatrix A, DenseVector b, DenseVector weights) {
        Matrix s1 = svdInvert(A, svdT.getValue());           
        s1.mult(b, weights);            
    }

    protected Matrix invert(Matrix A) {
        Matrix Ainv = new DenseMatrix(A.numColumns(), A.numRows()); 
        A.solve(Ainv, Matrices.identity(A.numRows()));
        
        return Ainv;
    }
    
    protected Matrix svdInvert(Matrix A, double threshold) {
        SVD svd = new SVD(A.numRows(), A.numColumns());
        try {
            svd = SVD.factorize( A);
        } catch (NotConvergedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        TridiagMatrix S = new TridiagMatrix(svd.getS().length);
        System.arraycopy(svd.getS(), 0, S.getDiagonal(), 0, svd.getS().length);

        // Invert U
        DenseMatrix U = svd.getU();
        DenseMatrix Ut = new DenseMatrix(U.numColumns(), U.numRows());
        U.transpose(Ut);
        
        // Invert Vt
        DenseMatrix Vt = svd.getVt();
        DenseMatrix V = new DenseMatrix(Vt.numColumns(), Vt.numRows());
        Vt.transpose(V);
        
        // Invert S
        double v;
        for (MatrixEntry e : S) {
            v = e.get();
            e.set(v > threshold ? 1 / v : 0);
        }

        // Compute V*S-1*Ut
        Matrix s1 = V.mult(
                S.mult(Ut, new DenseMatrix(S.numRows(), U
                .numColumns())), new DenseMatrix(A.numColumns(), A.numRows()));            

        return s1;
    }
    
    protected double linearpoly(double x, double y, double z) {
        return p0 + p1 * x + p2 * y + p3 * z;
    }

    public void saveConstraints(String filename) {
        constraintFile = filename;

        fc.setPath(filename);
        
        try {
            // Create file
            FileWriter fstream = new FileWriter(constraintFile);
            BufferedWriter out = new BufferedWriter(fstream);

            System.out.println("Saving constraints to " + (new File(filename)).getAbsolutePath());

            // XML header
            out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.write("\n<ConstraintList>");

            for (Constraint c : constraints) {
                
                // Info we want to keep:
                // 1) id
                // 2) linkedID (vertex id for virtual marker and virtual marker id for mocap markers)
                // 3) the rest should be saved in the javabin. with 1) + 2), we should have
                // enough to get started (and this way we won't need to hand pick all
                // the vertices again...)
                out.write("\n   <Constraint");
                out.write(" orientation=\"" + c.orientation + "\"");
                out.write(" value=\"" + c.val + "\"");
                out.write(" xpos=\"" + c.p.x + "\"");
                out.write(" ypos=\"" + c.p.y + "\"");
                out.write(" zpos=\"" + c.p.z + "\"");
                out.write(" index=\"" + c.index + "\"");
                out.write("/>");
            }
            
            // Footer
            out.write("\n</ConstraintList>");
            
            out.close();
        } catch (Exception e) {// Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
    
    public void loadConstraints(String filename) {
        constraintFile = filename;
        
        fc.setPath(filename);
        
        clear();
        
        File f = new File(constraintFile);
        
        System.out.println("Loading constraints from " + f.getAbsolutePath());

        Document document = null;
        
        try {
            document = XMLHelper.openAsDOM(f.getAbsolutePath());

            Element header = document.getDocumentElement();
            int index;
            List<Element> elements = XMLHelper.getChildElementListByTagName( header, "Constraint" );
            for( Element e : elements ) {

                String type = XMLHelper.getStringAttribute(e, "orientation");
                Orientation o = Orientation.valueOf(type);
                double val = XMLHelper.getFloatAttribute(e, "value", 0);

                Point3d p = new Point3d();
                p.x = XMLHelper.getFloatAttribute(e, "xpos", 0);
                p.y = XMLHelper.getFloatAttribute(e, "ypos", 0);
                p.z = XMLHelper.getFloatAttribute(e, "zpos", 0);

                index = XMLHelper.getIntAttribute(e, "index", 0);
                
                if (o != null) {
//                    System.out.println("Adding constraint: " + o);
                    Constraint c = new Constraint(p, index, o);
                    addConstraint(c);
                }

//                if (type.equals("boundary")) {
//                }
//                else if (type.equals("exterior")) {
//                }
//                else if (type.equals("interior")) {
//                }
                
            }
            
            System.out.println("Loaded " + constraints.size() + " constraints.");
            
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }                
        
    }
    
    public void addConstraint(Vertex vertex) {
        if (!pickedIndices.contains(vertex.index)) {
            addConstraint(new Point3d(vertex.p), new Vector3d(vertex.n), vertex.index);
            pickedIndices.add(vertex.index);
        }
    }
    
    protected final Point3d tempP = new Point3d();

    public void addBiConstraint(Point3d p, Vector3d normal, int index, double scale) {

    	// Boundary
//        addConstraint(new Constraint(p, index,  Orientation.BOUNDARY));

        // Add a slightly offseted interior constraint
        // following the negative normal at this point.
        tempP.scaleAdd(-scale, normal, p);
        addConstraint(new Constraint(tempP, index,  Orientation.INTERIOR));

        // Add a slightly offseted exterior constraint
        // following the normal at this point.
        tempP.set(p);
        tempP.scaleAdd(scale, normal, p);
        addConstraint(new Constraint(tempP, index,  Orientation.EXTERIOR));

        setInterpolationRequest();
    }

    public void addBiInteriorConstraint(Point3d p, Vector3d normal, int index, double scale) {
		// Boundary
		addConstraint(new Constraint(p, index, Orientation.BOUNDARY));

		// Interior
		p.scaleAdd(scale, normal, p);
		addConstraint(new Constraint(p, index, Orientation.INTERIOR));

		setInterpolationRequest();
	}

    public void addTriConstraints(List<Point3d> pts, List<Vector3d> normals, List<Integer> indices, double scale) {
    	for (Integer i : indices) {
    		addTriConstraint(pts.get(i), normals.get(i), i, scale);
    	}
    	
    }
    
    public void addTriConstraint(Point3d p, Vector3d normal, int index, double scale) {
		// Boundary
		addConstraint(new Constraint(p, index, Orientation.BOUNDARY));

		// Interior
		p.scaleAdd(scale, normal, p);
		addConstraint(new Constraint(p, index, Orientation.INTERIOR));

		// Exterior
		p.scaleAdd(-2*scale, normal, p);
		addConstraint(new Constraint(p, index, Orientation.EXTERIOR));

		setInterpolationRequest();
	}

    public void addConstraint(Point3d p, Vector3d normal, int index, double scale) {

        // First add this point as a boundary constraint 
        // (i.e. the surface should pass through this point)
        addConstraint(new Constraint(p, index, Orientation.BOUNDARY));
        
        // Add a slightly offseted interior constraint
        // following the negative normal at this point.
        tempP.scaleAdd(-scale, normal, p);
        addConstraint(new Constraint(tempP, index,  Orientation.INTERIOR));

        // Add a slightly offseted exterior constraint
        // following the normal at this point.
//        tempP.set(p);
//        tempP.scaleAdd(scale, normal, p);
//        addConstraint(new Constraint(tempP, index,  Orientation.EXTERIOR));

        setInterpolationRequest();
    }
  
    public void addConstraint(Point3d p, Vector3d normal, int index) {
    	addConstraint(p, normal, index, 0.5);
    }
    
    public void addConstraint(Constraint c) {
        constraints.add(c);
    }

    /**
     * Use information about this polygon to add
     * RBF constraints.
     * @param model
     */
    public void addPolygon(PolygonSoup model) {
        clear();
        
        Point3d p = new Point3d();
        Vector3d n = new Vector3d();

        HashSet<Integer> vs = new HashSet<Integer>();
        Random rand = new Random();
        int count = 50;
        int size = model.getVertices().size();
        int dc = size / count;
        
//        // Random way
        for (int i = 0; i < count; i++) {
            vs.add(rand.nextInt(size));
        }
        
//        // Deterministic
//        for (int i = 0; i < size; i += dc) {
//            vs.add(rand.nextInt(size));
//        }
        
        for (Integer i : vs) {
            p.set(model.getVertices().get(i).p);
            n.set(model.getVertices().get(i).n);
            
            addConstraint(p, n, i);
        }

        System.out.println("Sampled " + vs.size() + " vertices.");
        
        setInterpolationRequest();
    }

    protected final FileChooser fc = new FileChooser("./data/", "Load constraints");
    
    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), getName() + " Function"));

        JPanel cp = new JPanel();
        
        fc.addOpenActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                loadConstraints(fc.getCurrentFile().getAbsolutePath());
            }
            
        });

        fc.addSaveActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                saveConstraints(fc.getCurrentFile().getAbsolutePath());
            }
            
        });

        
        JButton btnClear = new JButton("Clear constraints");
        btnClear.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                clear();
            }
        });

        vfp.add(fc);
        
        cp.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
//        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.NORTHWEST;

//        c.gridx = 0;
//        c.gridy = 0;
//        cp.add(fc, c);
        
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        cp.add(btnClear, c);

        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 1;
        cp.add(drawConstraints.getControls(), c);

        vfp.add(cp);
        
        ParameterListener l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {
               offset.set(xo.getValue(), yo.getValue(), zo.getValue());
            }
        };
        xo.addParameterListener(l);
        yo.addParameterListener(l);
        zo.addParameterListener(l);
        
        l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {
               setInterpolationRequest();
            }
        };

        svdT.addParameterListener(l);
        
        vfp.add(xo.getSliderControls(false));
        vfp.add(yo.getSliderControls(false));
        vfp.add(zo.getSliderControls(false));
        
        vfp.add(svdT.getSliderControlsExtended("enabled"));
        svdT.setChecked(true);

//        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(), getName() + " Function");
//        return panel;
        return vfp.getPanel();
    }
    
    public List<Constraint> getConstraints() {
        return constraints;
    }

	public void scale(double s) {
		for (Constraint c : constraints) {
			c.p.scale(s);
		}
	}

	public void setSVDthreshold(double d) {
		svdT.setValue(d);
	}

	/**
	 * Project these points onto the implicit surface using a default epsilon (1e-4).
	 * @param p
	 */
	public void project(Point3d[] pts) {
		for (Point3d p : pts) {
			project(p, 1e-4);
		}
	}

	/**
	 * Project this point onto the implicit surface using a default epsilon (1e-4).
	 * @param p
	 */
	public void project(Point3d p) {
		project(p, 1e-4);
	}
	
	/**
	 * Project these points onto the implicit surface using the given epsilon.
	 * @param p
	 * @param eps
	 */
	public void project(Point3d[] pts, double eps) {
		for (Point3d p : pts) {
			project(p, eps);
		}
	}

	/**
	 * Project this point onto the implicit surface using the given epsilon.
	 * @param p
	 * @param eps
	 */
	public void project(Point3d p, double eps) {
		double eval = Double.MAX_VALUE;
		int maxn = 50;
		int its = 0;
		
		while (its < maxn) {
			if (eval < eps) break;
			
    		Vector3d dF = dF(p);
    		
    		double dFm = dF.length();
    		dF.scale(F(p) / (dFm * dFm));
    		
    		p.scaleAdd(-1, dF, p);
    		
    		eval = Math.abs(F(p)) / dFm;
    		its++;
    	}
//		System.out.println(its + ": " + eval);
	}
	
	public Vector3d[] computeTangentPlane(Point3d p) {
		
		double ds = 1e-4;
		
		// Compute n
		Vector3d n = dF(p);
		n.normalize();
		
		// Compute u
		Vector3d u = new Vector3d();
		Vector3d uavg = new Vector3d();
		Point3d pds = new Point3d();
		double i = 0;

		pds.set(p.x + ds, p.y + ds, p.z);
		project(pds);
		u.sub(pds, p);
		u.normalize();
		uavg.add(u);
		i++;

//		pds.set(p.x - ds, p.y, p.z);
//		project(pds);
//		u.sub(pds, p);
//		u.normalize();
//		uavg.add(u);
//		i++;
//
//		pds.set(p.x, p.y + ds, p.z);
//		project(pds);
//		u.sub(pds, p);
//		u.normalize();
//		uavg.add(u);
//		i++;
//
//		pds.set(p.x + ds, p.y - ds, p.z);
//		project(pds);
//		u.sub(pds, p);
//		u.normalize();
//		uavg.add(u);
//		i++;

		uavg.scale(1 / i);

		// Compute v
//		Point3d pdt = new Point3d(p);
//		pds.y += dt;
//		project(pdt);
		Vector3d v = new Vector3d();
//		v.sub(pdt, p);
		v.cross(u, n);
		v.normalize();
		
		u.cross(n, v);
		u.normalize();
		
		Vector3d[] T = new Vector3d[] { u, v };

		return T;
	}
	
	public double computeCurvature(Point3d p) {
		Vector3d[] T = computeTangentPlane(p);
		Vector3d u = T[0];
		Vector3d v = T[1];
		
		double e = 1e-2;

		// Get local normal
		Vector3d n = new Vector3d(dF(p));

		// Compute offsets and normals
		Vector3d nprime;
		Vector3d cdF = new Vector3d();
		double dmin = Double.MAX_VALUE;
		double d;
		Point3d pd = new Point3d();

		// New point (e, 0)
		pd.scaleAdd(e, u, p);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}
		
		// New point (e, e)
		pd.scaleAdd(e, u, p);
		pd.scaleAdd(e, v, pd);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}
		
		// New point (0, e)
		pd.scaleAdd(e, v, p);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}

		// New point (-e, e)
		pd.scaleAdd(-e, u, p);
		pd.scaleAdd(e, v, pd);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}

		// New point (-e, 0)
		pd.scaleAdd(-e, u, p);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}
		
		// New point (-e, -e)
		pd.scaleAdd(-e, u, p);
		pd.scaleAdd(-e, v, pd);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}
		
		// New point (0, -e)
		pd.scaleAdd(-e, v, p);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}

		// New point (e, -e)
		pd.scaleAdd(e, u, p);
		pd.scaleAdd(-e, v, pd);
		nprime = dF(pd);
		nprime.normalize();
		
		d = nprime.dot(n);		
		if (d < dmin) {
			dmin = d;
			cdF.set(nprime);
		}
		
//		System.out.println(dmin);

		// Estimate curvature using small dot
		double theta = Math.acos(dmin);
		double k = e / Math.tan(theta);
		
		return k;
	}
    
    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL.GL_BLEND);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        if (drawConstraints.getValue()) {
            gl.glPointSize(10);
            float alpha = 0.8f;
            gl.glBegin(GL.GL_POINTS);
            for (Constraint c : constraints) {
                if (c.orientation == Orientation.BOUNDARY) {
                    gl.glColor4f(0, 0, 1, alpha);
                }
                else if (c.orientation == Orientation.INTERIOR) {
                    gl.glColor4f(1, 1, 0, alpha);
                }
                else if (c.orientation == Orientation.EXTERIOR) {
                    gl.glColor4f(1, 0, 0, alpha);
                }
                
                gl.glVertex3d(c.p.x, c.p.y, c.p.z);
            }
            gl.glEnd();
        }
        
    }

    /**
     * Clears all constraints from this RBF.
     */
    public void clear() {
        constraints.clear();
        pickedIndices.clear();
        
//        setInterpolationRequest();
    }
}
