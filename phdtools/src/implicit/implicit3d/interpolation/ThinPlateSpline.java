package implicit.implicit3d.interpolation;

import gl.geometry.primitive.Sphere;

import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import tools.geom.MathToolset;

/**
 * Class for interpolating surfaces using thin-plate splines.
 * @author piuze
 */
public class ThinPlateSpline extends ImplicitSurface {
    
    private final Vector3d dFv = new Vector3d();
    
    public ThinPlateSpline() {
    	// Do nothing
    }
    
    @Override
    public String getName() {
        return "Thin-plate Spline";
    }

    private final static double RADIAL_EPSILON = 1e-6;
    private final static double RADIAL_COEF = -0.0398;
    /**
     * Thin-plate spline RBF.
     * phi = | c1 - c2 | ^ 3
     * @param p1
     * @param c
     * @return
     */
    @Override
	protected double basisFunction(double x, double y, double z, Point3d c) {
        
        // Compute L2 norm
        double l = Math.sqrt((x - c.x) * (x - c.x) + (y - c.y) * (y - c.y) + (z - c.z) * (z - c.z));

        // Check for threshold
        if (l <= RADIAL_EPSILON) return 0;
        
//        // Thin-plate radial basis function |x-c| ^3
//        // Coefficient = gamma(n / 2 - 2) / (16 pi^(n/2)) where n = dimensionality
//        // for n != 2 or 4
//        // here n = 3
//        // such that coef = gamma(3/2 - 2) / (16 pi^(3/2)) = -0.0398
//        return RADIAL_COEF * (l * l * l);
//    	return l*l*Math.log(l);
        return l * l * l;
    }

    @Override
	protected double basisFunction(Constraint c1, Constraint c2) {
		return basisFunction(c1.p.x, c1.p.y, c1.p.z, c2.p);
	}
	
    @Override
    public double F(double x, double y, double z) {
        if (hasChanged()) {
            interpolate();
            setHasChanged(false);
        }

        x -= offset.x;
        y -= offset.y;
        z -= offset.z;
        
        int k = constraints.size();
        
        if (k == 0) return 0;

        double ans = 0;
        
        for (int j = 0; j < k; j++) {
            ans += weights.get(j) * basisFunction(x, y, z, constraints.get(j).p);
        }
        
        ans += linearpoly(x, y, z);
        
        return ans;
    }
    
    @Override
    public double F(Point3d point) {
        return this.F(point.x, point.y, point.z);
    }

    @Override
    public Vector3d dF(Point3d point) {
        if (hasChanged()) {
            interpolate();
        }
        
        Vector3d x = new Vector3d();
        
        int k = constraints.size();

        if (k == 0) return x;

        // dF is computed by taking the derivative
        // of F (see method F)
        dFv.set(0, 0, 0);
        for (int j = 0; j < k; j++) {
            x.sub(point, constraints.get(j).p);
            x.sub(offset);
            double l = x.length();
            
            dFv.x += weights.get(j) * l * x.x;
            dFv.y += weights.get(j) * l * x.y;
            dFv.z += weights.get(j) * l * x.z;
        }
        
        dFv.scale(3);

//        dFv.negate();
        
        // Add grad P
        dFv.x += p1;
        dFv.y += p2;
        dFv.z += p3;
        
        return dFv;
    }
    
	@Override
	public Vector3d[] computeTangentPlane(Point3d p) {
    	double eps = 1e-5;
    	int accuracy = 4;
    	int pn = accuracy + 1;
    	double[] cs = getCentralDifferencesCoef(accuracy);
    	
    	// FIXME: currently need to decouple x and y
    	// since the same array is returned for both.
    	Vector3d u = new Vector3d();
    	
    	// X derivative
    	Vector3d dx = new Vector3d(1, 0, 0);
    	Point3d[] ptsx = getProjectedLinearSampling(p, dx, eps, accuracy);
		Vector3d dv = new Vector3d();
    	for (int i = 0; i < pn; i++) {
    		dv.sub(ptsx[i], p);
    		u.x += cs[i] * dv.x;
    		u.y += cs[i] * dv.y;
    		u.z += cs[i] * dv.z;
    	}
    	
    	// Y derivative
    	Vector3d dy = new Vector3d(0, 1, 0);
    	Point3d[] ptsy = getProjectedLinearSampling(p, dy, eps, accuracy);
    	for (int i = 0; i < pn; i++) {
    		dv.sub(ptsy[i], p);
    		u.x += cs[i] * dv.x;
    		u.y += cs[i] * dv.y;
    		u.z += cs[i] * dv.z;
    	}

    	u.normalize();
    	
		Vector3d v = new Vector3d();
		Vector3d n = dF(p);
		n.normalize();
		
		v.cross(u, n);
		v.normalize();
		
		u.cross(n, v);
		u.normalize();
		
		Vector3d[] T = new Vector3d[] { u, v, n };
		
		return T;
	}

    /**
     * @param point
     * @return the Hessian matrix H at this location.
     */
    public Matrix3d H(Point3d point) {
    	Matrix3d H = new Matrix3d();

    	double[] dF2 = dF2(point);
    	double[] dFc = dFcross(point);
    	
    	// dF / d.2
    	H.m00 = dF2[0];
    	H.m11 = dF2[1];
    	H.m22 = dF2[2];
    	
    	// dF / dxdy, dydx
    	H.m01 = dFc[0];
    	H.m10 = dFc[0];
    	
    	// dF / dxdz, dzdx
    	H.m02 = dFc[1];
    	H.m20 = dFc[1];
    	
    	// dF / dydz, dzdy
    	H.m21 = dFc[2];
    	H.m12 = dFc[2];
 	
    	return H;
    }
    
    /**
     * @param point
     * @return the adjoint of the hessian H* at this location.
     */
    public Matrix3d Hstar(Point3d point) {
    	Matrix3d Hs = new Matrix3d();
    	Matrix3d H = H(point);
    	
    	for (int i = 0; i < 3; i++) {
    		for (int j = 0; j < 3; j++) {
    			Hs.setElement(i, j, cofactor(H, i ,j));
    		}
    	}
    	
    	return Hs;
    }
    
    /**
     * @param A
     * @param i
     * @param j
     * @return the cofactor of A at (i,j)
     */
    private double cofactor(Matrix3d A, int i, int j) {
    	
    	// zero corresponding row and column
    	A.setRow(i, 0, 0, 0);
    	A.setColumn(j, 0, 0, 0);
    	
    	double Mij = A.determinant();

    	return ((i + i) % 2 == 0) ? Mij : -Mij;
    }
    
    /**
     * @param point
     * @return [dF2/dx2, dF2/dy2, dF2/dz2]
     */
    public double[] dF2(Point3d point) {
        int k = constraints.size();

        Vector3d dp = new Vector3d();
        double dx2 = 0;
        double dy2 = 0;
        double dz2 = 0;
        
        for (int j = 0; j < k; j++) {
            dp.sub(point, constraints.get(j).p);
            dp.sub(offset);
            double l = dp.length();
            
            // Compute dF2 / dx2
            dx2 += weights.get(j) * (dp.x * dp.x / l + l);
            
            // Compute dF2 / dy2
            dy2 += weights.get(j) * (dp.y * dp.y / l + l);

            // Compute dF2 / dydz
            dz2 += weights.get(j) * (dp.z * dp.z / l + l);
        }
        
        dx2 *= 3;
        dy2 *= 3;
        dz2 *= 3;
        
        return new double[] { dx2, dy2, dz2 } ;
    }
    
    /**
     * @param point
     * @return [dF2/dxdy, dF2/dxdz, dF2/dydz]
     */
    public double[] dFcross(Point3d point) {
        int k = constraints.size();

        Vector3d dp = new Vector3d();
        double dxdy = 0;
        double dxdz = 0;
        double dydz = 0;
        
        for (int j = 0; j < k; j++) {
            dp.sub(point, constraints.get(j).p);
            dp.sub(offset);
            double l = dp.length();
            
            // Compute dF2 / dxdy
            dxdy += weights.get(j) * dp.x * dp.y / l;
            
            // Compute dF2 / dxdz
            dxdz += weights.get(j) * dp.x * dp.z / l;

            // Compute dF2 / dydz
            dydz += weights.get(j) * dp.y * dp.y / l;
        }
        
        dxdy *= 3;
        dxdz *= 3;
        dydz *= 3;
        
        return new double[] { dxdy, dxdz, dydz } ;
    }

    public double getGaussianCurvature(Point3d point) {
    	Vector3d dF = dF(point);
    	
    	Matrix3d Hs = Hstar(point);
    	
    	// gradF * H * gradF^T / |gradF|^4
    	Vector3d HdF = new Vector3d(dF);
    	Hs.transform(HdF);
    	double K = dF.dot(HdF);
    	return K / Math.pow(dF.length(), 4);
    }
    
    public double getMeanCurvature(Point3d point) {
    	Vector3d dF = dF(point);
    	Matrix3d H = H(point);
    	Matrix3d Hs = Hstar(point);

    	// gradF * H * gradF^T
    	Vector3d HdF = new Vector3d(dF);
    	Hs.transform(dF, HdF);
    	double K = dF.dot(HdF);
    	
    	// -|gradF|^2 * trace(H)
    	K -= Math.pow(dF.length(), 2) * trace(H);
    	
    	// / |gradF|^3
    	return  K / Math.pow(dF.length(), 3);
    }

    public Vector3d getCurvatureGradient(Point3d point) {
    	
    	double eps = 1e-5;
    	int accuracy = 4;
    	int n = accuracy + 1;
    	double[] cs = getCentralDifferencesCoef(accuracy);
//    	System.out.println(Arrays.toString(cs));
    	
    	// FIXME: currently need to decouple x and y
    	// since the same array is returned for both.
    	Vector3d dk = new Vector3d();
    	
    	// X derivative
    	Vector3d dx = new Vector3d(1, 0, 0);
    	Point3d[] ptsx = getProjectedLinearSampling(point, dx, eps, accuracy);
    	for (int i = 0; i < n; i++) {
    		dk.x += cs[i] * Math.abs(getMeanCurvature(ptsx[i]));
//    		dk.x += cs[i] * getMeanCurvature(ptsx[i]);
    	}
    	
    	// Y derivative
    	Vector3d dy = new Vector3d(0, 1, 0);
    	Point3d[] ptsy = getProjectedLinearSampling(point, dy, eps, accuracy);
    	for (int i = 0; i < n; i++) {
    		dk.y += cs[i] * Math.abs(getMeanCurvature(ptsy[i]));
//    		dk.y += cs[i] * getMeanCurvature(ptsy[i]);
    	}

    	dk.scale(1 / eps);

    	return dk;
    }   
    
    private double[][] cfdc = 
    	new double[][] { 
    		{ -1/2f, 0, 1/2f },
    		{ 1/12f, -2/3f, 0, 2/3f, -1/12f },
    		{ -1/60f, 3/20f, -3/4f, 0, 3/4f, -3/20f, 1/60f },
    		{ 1/280f, -4/105f, 1/5f, -4/5f, 0, 4/5f, -1/5f, 4 / 105f, -1/280f }
    		};
    
    public double[] getCentralDifferencesCoef(int accuracy) {
    	int index = accuracy / 2 - 1;
    	int n = cfdc[index].length;

    	double cs[] = new double[n];
    	for (int i = 0; i < n; i++) {
    		cs[i] = cfdc[index][i];
    	}
    	return cs;
    }
    
    private Point3d[] lsampling2 = null;
    private Point3d[] lsampling4 = null;
    private Point3d[] lsampling6 = null;
    private Point3d[] lsampling8 = null;
    private Point3d[] sampling7 = null;
    private Point3d[] sampling9 = null;

    private Point3d[] getProjectedLinearSampling(Point3d p, Vector3d dx, double eps, int accuracy) {
    	Point3d[] pts;
    	int n = accuracy + 1;
    	dx.normalize();
    	
    	switch (accuracy) {
    	case 2:
    		if (lsampling2 == null) {
        		lsampling2 = new Point3d[n];
        		for (int i = 0; i < n; i++)
        			lsampling2[i] = new Point3d();
    		}
    		pts = lsampling2;
    		break;
    	case 4:
    		if (lsampling4 == null) {
        		lsampling4 = new Point3d[n];
        		for (int i = 0; i < n; i++)
        			lsampling4[i] = new Point3d();
    		}
    		pts = lsampling4;
    		break;
    	case 6:
    		if (lsampling6 == null) {
        		lsampling6 = new Point3d[n];
        		for (int i = 0; i < n; i++)
        			lsampling6[i] = new Point3d();
    		}
    		pts = lsampling6;
    		break;
    	case 8:
    		if (lsampling8 == null) {
        		lsampling8 = new Point3d[n];
        		for (int i = 0; i < n; i++)
        			lsampling8[i] = new Point3d();
    		}
    		pts = lsampling8;
    		break;
    	default:
    		return getProjectedLinearSampling(p, dx, eps, 4);
    	}    	
    	
//    	System.out.println("===");
    	for (int i = -accuracy/2; i <= accuracy/2; i++) {
//    		System.out.println(i + accuracy / 2);
//    		System.out.println(dx);
    		pts[i + accuracy/2].scaleAdd(-i * eps, dx, p);
//    		System.out.println(pts[i+accuracy/2]);
    	}
		project(pts);
		
		return pts;
    }
    
    /**
     * Sample an n-point region around p with distance eps.
     * TODO: speed up by preconstructing arrays 
     * @param p
     * @param eps
     * @param n
     * @return
     */
    public Point3d[] getProjectedCircularSampling(Point3d p, double eps, int n) {
    	switch (n) {
    	case 7:
    		if (sampling7 == null) {
    			sampling7 = new Point3d[n];
    			for (int i = 0; i < n; i++) 
    				sampling7[i] = new Point3d();
    		}
    		//  . . 
    		// . p .
    		//  . .
    		// Clockwise from left side
    		sampling7[0].set(p);
    		sampling7[0].x -= eps;
    		
    		sampling7[1].set(p);
    		sampling7[1].x -= eps / 2;
    		sampling7[1].y -= eps;
    		
    		sampling7[2].set(p);
    		sampling7[2].x += eps / 2;
    		sampling7[2].y -= eps;
    		
    		sampling7[3].set(p);
    		sampling7[3].x += eps;

    		sampling7[4].set(p);
    		sampling7[4].x += eps / 2;
    		sampling7[4].y += eps;
    		
    		sampling7[5].set(p);
    		sampling7[5].x -= eps / 2;
    		sampling7[5].y += eps;
    		
    		sampling7[6].set(p);
    		
    		project(sampling7);
    		
    		return sampling7;
    	case 9:
    		if (sampling9 == null) {
    			sampling9 = new Point3d[n];
    			for (int i = 0; i < n; i++) 
    				sampling9[i] = new Point3d();
    		}
    		
    		// . . . 
    		// . p .
    		// . . .
    		// Clockwise top left
    		sampling9[0].set(p);
    		sampling9[0].x -= eps;
    		sampling9[0].y -= eps;
    		
    		sampling9[1].set(p);
    		sampling9[1].y -= eps;
    		
    		sampling9[2].set(p);
    		sampling9[2].x += eps;
    		sampling9[2].y -= eps;
    		
    		sampling9[3].set(p);
    		sampling9[3].x += eps;

    		sampling9[4].set(p);
    		sampling9[4].x += eps;
    		sampling9[4].y += eps;
    		
    		sampling9[5].set(p);
    		sampling9[5].y += eps;

    		sampling9[6].set(p);
    		sampling9[6].x -= eps;
    		sampling9[6].y += eps;

    		sampling9[7].set(p);
    		sampling9[7].x -= eps;

    		sampling9[8].set(p);

    		project(sampling9);

    		return sampling9;
    	default:
    		return getProjectedCircularSampling(p, eps, 7);
    	}
    }
    
	private double trace(Matrix3d h) {
		return h.m00 + h.m11 + h.m22;
	}

	private Point3d inp1 = new Point3d();
	private Point3d inp2 = new Point3d();
	
	public boolean isInflection(Point3d p, Vector3d direction) {
		double eps = 1e-1;
		int n = 7;
		
		Point3d[] pts = getProjectedCircularSampling(p, eps, n);
		
		double kmax = -Double.MAX_VALUE;
		double kmin = Double.MAX_VALUE;
		
//		System.out.println("===");
		for (Point3d pt : pts) {
			double k = getMeanCurvature(pt);
			
//			System.out.println(k);
			if (k > kmax) {
				inp1.set(pt);
				kmax = k;
			}
			if (k < kmin) {
				inp2.set(pt);
				kmin = k;
			}
		}
		
		boolean isInflection = Math.signum(kmax) != Math.signum(kmin);
		
		if (isInflection) {
			direction.add(inp1, inp2);
			direction.scale(0.5);
			direction.sub(p);
		}
		return isInflection; 
	}

	public Point3d[] sampleFrom(Sphere sphere) {
		clear();
		double r = sphere.getRadius();
		double normalization = 1;
		
//		int n = MathToolset.roundInt(sphere.computeSurface() / normalization);
//		int n = MathToolset.roundInt(r * Math.PI * 2);
//		int n = MathToolset.roundInt(r * 30);
		int n = MathToolset.roundInt(30);
		
		Point3d[] samples = sphere.sampleHemisphere(n);
		
		double constraintScale = 0.1 * r;
		for (int i = 0; i < n; i++) {
//			addTriConstraint(samples[i], sphere.getNormal(samples[i]), i, constraintScale);
			Vector3d normal = sphere.getNormal(samples[i]);
			normal.normalize();
			normal.negate();
			
			addConstraint(new Point3d(samples[i]), normal, i, constraintScale);
		}

		return samples;
	}
}
