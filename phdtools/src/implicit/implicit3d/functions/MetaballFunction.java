package implicit.implicit3d.functions;



import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset.GreekLetter;

import com.jogamp.opengl.util.gl2.GLUT;

public class MetaballFunction extends ImplicitFunction3D {
    private Point3d p0 = new Point3d();
    private DoubleParameter x0p = new DoubleParameter("origin x", 0, -10, 10);
    private DoubleParameter y0p = new DoubleParameter("origin y", 0, -10, 10);
    private DoubleParameter z0p = new DoubleParameter("origin z", 0, -10, 0);
    private DoubleParameter sigmap = new DoubleParameter(GreekLetter.SIGMA2.code, 2, 1e-12, 100);
    private DoubleParameter bp = new DoubleParameter("b", 0.5f, 1e-12, 10);
    private BooleanParameter draw = new BooleanParameter("draw", false);
    
    private Vector3d tmpv = new Vector3d();
    private Point3d tmp = new Point3d();
    
    public MetaballFunction(Point3d origin, double radius, double bias) {
        p0.set(origin);
        x0p.setValue(p0.x);
        y0p.setValue(p0.y);
        z0p.setValue(p0.z);
        sigmap.setValue(radius);
        bp.setValue(bias);
        
        setHasChanged(true);
    }
    public MetaballFunction() {
        p0.set(x0p.getValue(), y0p.getValue(), z0p.getValue());
        setHasChanged(true);
    }

    @Override
    public double F(Point3d p ) {
        return this.F(p.x, p.y, p.z);
    }
    
    @Override
    public double F(double x, double y, double z) {
        tmp.set(x, y, z);
        
        double r = tmp.distance(p0);
        double b = bp.getValue();
        double s = sigmap.getValue();
        return (b - Math.exp(-r*r/(s*s)));
    }
    
    @Override
    public Vector3d dF(Point3d p) {
        tmpv.sub(p, p0);
        double r = tmpv.length();
        double s2 = sigmap.getValue() * sigmap.getValue();
        tmpv.scale((2 * Math.exp(-r*r/s2) / s2));
        
        return tmpv;
    }
    
    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), getName() + " Function"));

        ParameterListener l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {
                p0.set(x0p.getValue(), y0p.getValue(), z0p.getValue());
            }
        };
        bp.addParameterListener(l);
        sigmap.addParameterListener(l);
        x0p.addParameterListener(l);
        y0p.addParameterListener(l);
        z0p.addParameterListener(l);
        
        vfp.add(bp.getSliderControls(false));
        vfp.add(sigmap.getSliderControls(false));
        vfp.add(x0p.getSliderControls(false));
        vfp.add(y0p.getSliderControls(false));
        vfp.add(z0p.getSliderControls(false));
        vfp.add(draw.getControls());

        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(), getName() + " Function");
        return panel;
    }
    
    @Override
    public String getName() {
        return "Metaball";
    }
    
    private GLUT glut = new GLUT();
    /**
     * Draws a sphere instead of the metaball, which is
     * clearly wrong but useful as an approximation.
     */
    @Override
    public void display(GLAutoDrawable drawable) {
        if (!draw.getValue()) return;
        
        GL2 gl = drawable.getGL().getGL2();
        gl.glColor4f(0, 0, 1, 0.5f);
        gl.glPushMatrix();
        gl.glTranslated(p0.x, p0.y, p0.z);
        glut.glutSolidSphere(sigmap.getValue(), 20, 20);
        gl.glPopMatrix();
    }

}
