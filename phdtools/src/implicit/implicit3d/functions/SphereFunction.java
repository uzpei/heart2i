package implicit.implicit3d.functions;



import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

import com.sun.opengl.util.GLUT;

public class SphereFunction extends ImplicitFunction3D {
    private final Point3d p0 = new Point3d();
    private final DoubleParameter radiusp = new DoubleParameter("radius", 10, 1e-12, 100);
    private final DoubleParameter x0p = new DoubleParameter("origin x", 0, -5, 5);
    private final DoubleParameter y0p = new DoubleParameter("origin y", 0, -5, 5);
    private final DoubleParameter z0p = new DoubleParameter("origin z", 0, -5, 5);
    private final BooleanParameter draw = new BooleanParameter("draw", false);
    
    private double radius;
    
    private final Vector3d tmp = new Vector3d();
    
    public SphereFunction(Point3d origin, double radius) {
        p0.set(origin.x, origin.y, origin.z);
        x0p.setValue(p0.x);
        y0p.setValue(p0.y);
        z0p.setValue(p0.z);
        this.radiusp.setValue(radius);
        this.radius = radius;
    }
    public SphereFunction() {
        p0.set(x0p.getValue(), y0p.getValue(), z0p.getValue());
        radius = radiusp.getValue();
    }
    
    private final Vector3d dr = new Vector3d();
    
//    @Override
//    public Vector3d dF(Point3d pt) {
//        dr.sub(pt, p0);
////        dr.normalize();
//        
//        return dr;
//    }
    
    @Override
    public double F(Point3d p) {
        return this.F(p.x, p.y, p.z);
    }
    
    @Override
    public double F(double x, double y, double z) {
        tmp.set(x, y, z);
        tmp.sub(p0);
        
        double r = tmp.lengthSquared();
        
        return r - radius*radius;
    }
    
    
    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), getName() + " Function"));

        ParameterListener l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {
                p0.set(x0p.getValue(), y0p.getValue(), z0p.getValue());
                radius = radiusp.getValue();
            }
        };
        radiusp.addParameterListener(l);
        x0p.addParameterListener(l);
        y0p.addParameterListener(l);
        z0p.addParameterListener(l);
        
        vfp.add(draw.getControls());
        vfp.add(radiusp.getSliderControls(false));
        vfp.add(x0p.getSliderControls(false));
        vfp.add(y0p.getSliderControls(false));
        vfp.add(z0p.getSliderControls(false));

        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(), getName() + " Function");
        return panel;
    }
    
    @Override
    public String getName() {
        return "Sphere";
    }
    
    private final GLUT glut = new GLUT();
    @Override
    public void display(GLAutoDrawable drawable) {
        if (!draw.getValue()) return;
        
        GL gl = drawable.getGL();
        gl.glColor4f(0, 0, 1, 0.5f);
        gl.glPushMatrix();
        gl.glTranslated(p0.x, p0.y, p0.z);
        glut.glutSolidSphere(radius, 20, 20);
        gl.glPopMatrix();
    }
    
    public double getRadius() {
        return radius;
    }
	@Override
	public Vector3d dF(Point3d pt) {
		// TODO Auto-generated method stub
		return null;
	}
}
