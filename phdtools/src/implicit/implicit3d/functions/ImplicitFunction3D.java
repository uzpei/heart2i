package implicit.implicit3d.functions;

import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public interface ImplicitFunction3D {
    
    /**
     * Evaluate the implicit function at this point.
     * @param x A point in R3.
     * @return The result of the evaluation.
     */
    public abstract double F(double x, double y, double z);

    public abstract double F(Point3d pt);

    public abstract Vector3d dF(Point3d pt);

    public abstract JPanel getControls();
    
    public abstract String getName();
    
    public abstract void display(GLAutoDrawable drawable);

    public abstract boolean hasChanged();
    
//    private boolean hasChanged = false;
    
//    /**
//     * @return whether this implicit function has changed (i.e. internal data was modified)
//     */
//	public boolean hasChanged() {
//		return hasChanged;
//	}
//	
//	public void setHasChanged(boolean b) {
//		hasChanged = b;
//	}
}
