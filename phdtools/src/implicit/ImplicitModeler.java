package implicit;

import implicit.implicit3d.interpolation.ThinPlateSpline;
import implicit.rendering.ImplicitRenderer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.HashSet;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import math.spline.Contour;

import picking.ModelPicker;
import swing.component.VerticalFlowPanel;
import swing.event.AdapterState;
import swing.event.ExtendedAdapter;
import tools.loader.PolygonSoup;
import tools.loader.Vertex;

/**
 * Fits a 3d model to an implicit surface.
 * @author piuze
 */
public class ImplicitModeler extends ExtendedAdapter {
    private PolygonSoup model;

    private ThinPlateSpline spline = new ThinPlateSpline();

    private ModelPicker picker;
    
    /**
     * Renders the implicit surface.
     */
    public ImplicitRenderer renderer;

    /**
     * @param pmodel a polygon mesh on which we fit an implicit surface.
     * @param mpicker pick vertices on the model and adding constraints.
     */
    public ImplicitModeler(PolygonSoup pmodel) {
        model = pmodel;
        picker = new ModelPicker(pmodel);
        renderer = new ImplicitRenderer(spline, false);
    }
    
    /**
     * @return The implicit function fitting the polygon model.
     */
    public ThinPlateSpline getImplicitFunction() {
        return spline;
    }
    
    /**
     * @return UI controls for this class.
     */
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Implicit Modeler"));
        
        vfp.add(spline.getControls());
        
        JButton btnPoly = new JButton("Sample vertices");
        btnPoly.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                spline.clear();
                spline.addPolygon(model);
            }
        });
        vfp.add(btnPoly);
        
        vfp.add(picker.getControls());
        
        vfp.add(renderer.getControls());

        return vfp.getPanel();
    }

    /**
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        
        renderer.display(drawable);
        
        if (requestPicking()) {
            gl.glColor3d(0, 0, 1);
            doPicking(drawable);
        }
        
        if (pickedVertex != null) {
            Point3d p = pickedVertex.p;
            gl.glPointSize(15);
            gl.glBegin(GL.GL_POINTS);
            gl.glVertex3d(p.x, p.y, p.z);
            gl.glEnd();
        }
    }
    
    public void clear() {
        spline.clear();
    }

    @Override
    public String toString() {
        return "Implicit fitting";
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (pickedVertex != null) {
            if (state.shiftDown() && e.getButton() == MouseEvent.BUTTON1) {
                spline.addConstraint(pickedVertex);
            }
        }
    }
    
    private Vertex pickedVertex = null;
    
    /**
     * @return the last picked vertex or null if there was none.
     */
    public Vertex getPickedVertex() {
    	return pickedVertex;
    }
    
    @Override
    public void doPicking(GLAutoDrawable drawable) {
        if (state.shiftDown() || state.altDown()) {
            float[] color = new float[] { 0, 0, 0.7f, 0.7f };
            Vertex v = picker.pick(drawable, model, state.getX(), state.getY(), color);
            
            if (v != null) {
                pickedVertex = v;
            }
        }
    }

    @Override
    public boolean requestPicking() {
        if (state.shiftDown()) return true;
        else return false;
    }

    @Override
    public String getHandleString() {
        String s = "";
//		s += (state.shiftDown() ? "[shift] = implicit fitting <--" : "[shift] = implicit fitting") + "\n";
        if (pickedVertex != null) 
        	s += (requestPicking() ? "[Vertex " + pickedVertex.index + "]\n" : "");
        s += (requestPicking() ? "-> [Left click] to add a constraint\n" : "");
        s += (requestPicking() ? "-> [Right click] to select a vertex\n" : "");
        return s;
    }

    /**
     * Add all vertices from the contour to the list of constraints. Use
     * a hashset to prevent duplicates.
     * @param c
     */
    public void addConstraints(Contour c) {
        HashSet<Integer> indices = new HashSet<Integer>();
        
        for (Vertex v : c.getControlPoints()) {
            indices.add(v.index);
        }
        
        for (Integer i : indices) {
            spline.addConstraint(model.getVertices().get(i));
        }
    }

    public ImplicitRenderer getRenderer() {
        return renderer;
    }

    @Override
    public void setAdapterState(AdapterState state) {
        this.state = state;
    }
    
    

}
