package implicit;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import implicit.implicit3d.interpolation.ThinPlateSpline;
import implicit.sampling.particles.Particle;
import implicit.sampling.particles.ParticleSystem;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import math.spline.Contour;
import picking.ModelPicker;
import swing.event.AdapterState;
import swing.event.ExtendedAdapter;
import swing.event.Interactor;
import tools.FPSTimer;
import tools.computations.MotionTools;
import tools.loader.PolygonSoup;

/**
 * Renders implicit surfaces in 2D
 * 
 * @author piuze
 */
public class ImplicitSampler extends ExtendedAdapter implements GLObject,
        Interactor {

    private JoglRenderer ev;

    /**
     * Determines the dimension of the OpenGL canvas (if created).
     */
    private Dimension winsize = new Dimension(800, 800);

    /**
     * The particle system in charge of the sampling.
     */
    private ParticleSystem particleSystem;

    /**
     * In charge of fitting the 3d model to a thin plate spline.
     */
    public ImplicitModeler modeler;

    /**
     * The 3d model we are sampling.
     */
    private PolygonSoup model;

    /** 
     * The implicit fit to the 3d model.
     */
    public ThinPlateSpline implicitFunction;

    /**
     * If this class should create the canvas.
     */
    private boolean createCanvas = false;

    /**
     * Entry point for application
     * 
     * @param args
     */
    public static void main(String[] args) {
        PolygonSoup m = null;
//        m = new PolygonSoup("./data/obj/cl_head_LOW.obj");
          m = new PolygonSoup("./data/obj/cl_head.obj");
//        m = new PolygonSoup("./resources/frot.obj");
//        m = new PolygonSoup("./resources/fish.obj");

        new ImplicitSampler(m);
    }

    /**
     * @param m
     * @param ownCanvas
     */
    public ImplicitSampler(PolygonSoup m) {
        this(m, null, true);
    }

    /**
     * Creates the application / scene instance
     * @param model 
     * @param e
     *            If set to null, a new GL canvas will be created.
     * @param createCanvas If this class should own (create) the OpenGL canvas
     */
    public ImplicitSampler(PolygonSoup model, JoglRenderer e, boolean createCanvas) {

        this.model = model;
        ModelPicker mpicker = new ModelPicker(model);
        
        if (createCanvas) {
            this.createCanvas = true;
        } else {
            this.createCanvas = false;
            ev = e;
        }

        modeler = new ImplicitModeler(model);

        implicitFunction = modeler.getImplicitFunction();

        particleSystem = new ParticleSystem(implicitFunction, mpicker);

        if (createCanvas) {

            ev = new JoglRenderer("Implicit Particle Sampling", this);

            // add interactors to set up mouse and keyboard controls
            ev.controlFrame.add("Particles", particleSystem.getControls());
            ev.controlFrame.add("Implicit", modeler.getControls());
            ev.controlFrame.add("3D Model", model.getControls());

            ev.addInteractor(this);

        }

        state = ev.getAdapterState();

        modeler.setAdapterState(state);
        particleSystem.setAdapterState(state);

        if (createCanvas) {
            ev.getCamera().zoom(600f);
            ev.start();
        }
        
        loadData();
    }
    
    private void loadData() {
        // Then preload data (contour, constraints)
        String name = model.getName();
        String path = "./data/";
        File file;
        
        file = new File(path + "contour_" + name + ".xml");
        if (file.exists()) {
            particleSystem.getContour().loadContour(file.getAbsolutePath());
        }
        else {
        	System.err.println(file.getAbsolutePath() + " not found.");
        }

        file = new File(path + "implicit_" + name + ".xml");
        if (file.exists()) {
            modeler.getImplicitFunction().loadConstraints(file.getAbsolutePath());
        }
        else {
        	System.err.println(file.getAbsolutePath() + " not found.");
        }
        
        // Add the contour, in case it hasn't been added already.
//        modeler.addConstraints(particleSystem.getContour());
        
        if (createCanvas) particleSystem.generateContour();

    }

    @Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
    }

    private FPSTimer fps = new FPSTimer();

    @Override
	public void display(GLAutoDrawable drawable) {
    	fps.tick();
		GL2 gl = drawable.getGL().getGL2();

        gl.glDisable(GL2.GL_LIGHTING);

        gl.glPushMatrix();
        
//        if (createCanvas)
//            WorldAxis.display(gl);

        gl.glPushMatrix();
 
        model.display(drawable);

        gl.glPopMatrix();

        if (createCanvas) particleSystem.display(drawable);

        if (createCanvas)
            ev.getCamera().enable(!state.shiftDown() && !state.altDown());

        modeler.display(drawable);
        
        gl.glPopMatrix();

        if (createCanvas)
            displayText(drawable);

        // Refresh the window size
        winsize.height = drawable.getHeight();
        winsize.width = drawable.getWidth();
    }

    private void displayText(GLAutoDrawable drawable) {
        // Display text info
    	JoglTextRenderer.printTextLines(drawable, getHandleString());
    }

    @Override
	public JPanel getControls() {
        // Load obj
        JLabel txt1 = new JLabel("1. Load a Wavefront .obj model.");
        JButton btn1 = new JButton("Go");
        btn1.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
                ev.controlFrame.setSelectedTab("3D Model");
        	}
        });

        // Particle contour
        JLabel txt2 = new JLabel("2. Define a particle boundary (hold alt).");
        JButton btn2 = new JButton("Go");
        btn2.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
                ev.controlFrame.setSelectedTab("Particles");
        	}
        });

        JButton transfer = new JButton("Contour->constraints");
        transfer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modeler.addConstraints(particleSystem.getContour());
			}
		});

        // Implicit constraints
        JLabel txt3 = new JLabel("3. Surface implicit fit (hold shift).");
        JButton btn3 = new JButton("Go");
        btn3.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
                ev.controlFrame.setSelectedTab("Implicit");
        	}
        });

        // Sample the surface
        JLabel txt4 = new JLabel("4. Run the particle system.");
        JButton btn4 = new JButton("Go");
        btn4.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
                ev.controlFrame.setSelectedTab("Particles");
        	}
        });


        // Save!
        JLabel txt5 = new JLabel("5. Save!");

        JPanel cpanel = new JPanel();
        cpanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.ipadx = 20;
        c.ipady = 10;
        c.weightx = 0;
        c.weighty = 0;
        
        int i = 0;
        
        c.gridx = 0;
        c.gridy = i;
        cpanel.add(txt1, c);

        c.gridx = 1;
        c.gridy = i++;
        cpanel.add(btn1, c);

        c.gridx = 0;
        c.gridy = i;
        cpanel.add(txt2, c);

        c.gridx = 1;
        c.gridy = i;
        cpanel.add(btn2, c);

        c.gridx = 2;
        c.gridy = i++;
        cpanel.add(transfer, c);

        c.gridx = 0;
        c.gridy = i;
        cpanel.add(txt3, c);

        c.gridx = 1;
        c.gridy = i++;
        cpanel.add(btn3, c);

        c.gridx = 0;
        c.gridy = i;
        cpanel.add(txt4, c);

        c.gridx = 1;
        c.gridy = i++;
        cpanel.add(btn4, c);

        c.gridx = 0;
        c.gridy = i;
        cpanel.add(txt5, c);

        return cpanel;
    }

    @Override
	public void attach(Component component) {

        if (createCanvas) {
            addComponent(component);
            particleSystem.addComponent(component);
            modeler.addComponent(component);

            component.addMouseListener(this);
            component.addMouseMotionListener(this);
            component.addKeyListener(this);
        }
    }

    @Override
    public String getHandleString() {
        String text = ""
                + (createCanvas ? "[ " + fps.toString() + " ]" : "") + "\n"
                + particleSystem.getDebugString() + "\n" 
        		+ (state.altDown() ? "[alt] = particle system <--" : "[alt] = particle system") + "\n"
        		+ (state.shiftDown() ? "[shift] = implicit fitting <--" : "[shift] = implicit fitting") + "\n"
        		+ "----------------" + "\n"
                + modeler.getHandleString()
                + particleSystem.getHandleString()
        // + renderer.toString()
        ;

        return text;
    }

    /**
     * @return The current list of particles sampling the surface.
     */
    public List<Particle> getParticles() {
        return particleSystem.getParticles();
    }

    public Vector3d getNormal(Point3d p) {
        Vector3d n = implicitFunction.dF(p);
        n.scale(-1);
        n.normalize();
        return n;
    }

    public ImplicitModeler getModeler() {
        return modeler;
    }

    @Override
    public String toString() {
        return "Particle sampler";
    }

    @Override
    public void mousePressed(MouseEvent e) {
            particleSystem.mousePressed(e);
            modeler.mousePressed(e);
    }

    @Override
    public void keyPressed(KeyEvent e) {

        particleSystem.keyPressed(e);
        
        modeler.keyPressed(e);

        if (e.getKeyCode() == KeyEvent.VK_N) {
        	model.computeNormals();
        } else if (e.getKeyCode() == KeyEvent.VK_R) {
            particleSystem.getContour().generatePoints();
        }

    }

    public ParticleSystem getParticleSystem() {
        return particleSystem;
    }

    @Override
    public String getName() {
        return "Main";
    }

    public Contour getContour() {
        return particleSystem.getContour();
    }

    @Override
    public void setAdapterState(AdapterState state) {
        this.state = state;
    }

    public ThinPlateSpline getFunction() {
        return modeler.getImplicitFunction();
    }

    public Matrix4d getLocalFrame(Point3d p) {
        Vector3d N = getNormal(p);
        Vector3d T = new Vector3d();
        Vector3d U = new Vector3d(0, 1, 0);
        
        T.cross(U, N);
        U.cross(T, N);

        N.normalize();
        U.normalize();
        T.normalize();

        return buildFrame(N, U, T, p);
    }
    
    private final Point3d zero = new Point3d(); 
    public Matrix4d getLocalOrientationFrame(Point3d p) {
        Vector3d N = getNormal(p);
        Vector3d T = new Vector3d();
        Vector3d U = new Vector3d(0, 1, 0);
        
        T.cross(U, N);
        U.cross(T, N);

        N.normalize();
        U.normalize();
        T.normalize();

        return buildFrame(N, U, T, zero);
    }
    
    	/**
	 * @param p
	 * @param n
	 * @param u
	 * @param t
	 */
	public void getLocalOrientationFrame(Point3d p, Vector3d N, Vector3d U,
			Vector3d T) {
        N.set(getNormal(p));
        U.set(0, 1, 0);
        
        T.cross(U, N);
        U.cross(T, N);

        N.normalize();
        U.normalize();
        T.normalize();
	}

    public Matrix4d getLocalFrameInv(Point3d p) {
        Matrix4d m = getLocalFrame(p);
        MotionTools.invertRigidTransformation(m, m);
        
        return m;
    }

    /**
     * Build a frame from an orthogonal basis T, B, N corresponding to x=T, y=B,
     * z=N with origin located at o
     * 
     * @param T
     * @param N
     * @param B
     * @param o
     * @return the frame
     */
    private Matrix4d buildFrame(Vector3d T, Vector3d N, Vector3d B, Point3d o) {
        Matrix4d frame = new Matrix4d();

        frame.setColumn(0, T.x, T.y, T.z, 0);
        frame.setColumn(1, N.x, N.y, N.z, 0);
        frame.setColumn(2, B.x, B.y, B.z, 0);
        frame.setColumn(3, o.x, o.y, o.z, 1);

        return frame;
    }

    /**
     * For debugging only. Scales the position of all particles, contour, and constraints.
     * @param s
     */
    public void scale(double s) {
        modeler.getImplicitFunction().scale(s);
        particleSystem.getContour().scale(s);
        for (Particle p : particleSystem.getParticles()) {
        	p.p.scale(s);
        }
    }

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
