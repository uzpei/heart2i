package implicit.implicit2D;

import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point2d;

public class Cell2D {
    public int index;
    private final double radius;
    private final Point2d pos = new Point2d();

    // The value of the implicit function at the vertices
    public double[] potential = new double[4];

    // The location of vertices (in/out of the implicit f)
    public boolean[] in = new boolean[4];
    
    // Vertices
    public Point2d[] p = new Point2d[4];
    
    // Side length
    public double d;
    
    
    public Cell2D(int index, double x0, double y0, double radius) {
        this.index = index;
        
        pos.set(x0, y0);
        this.radius = radius;
        
        p[0] = new Point2d(x0 - radius, y0 + radius);
        // Top
        p[1] = new Point2d(x0 - radius, y0 - radius);
        // Right
        p[2] = new Point2d(x0 + radius, y0 - radius);
        // Bottom
        p[3] = new Point2d(x0 + radius, y0 + radius);
        
        d = p[1].distance(p[2]);
    }
    
    /**
     * Evaluate the function at the given vertex.
     * @param f
     * @param vertex
     * @return
     */
    public double evaluate(ImplicitFunction f, int vertex) {
        
        return f.evaluate(p[vertex]);
    }
    
    public Point2d getPosition() {
        return pos;
    }

    public double getRadius() {
        return radius;
    }

    public void evaluate(List<ImplicitFunction> functions, boolean accumulate) {
        
        // Initialize outside the surface
        for (int i = 0; i < 4; i++) {
            in[i] = false;
        }
        
        
        if (accumulate) {
            // Zero the potential accumulator
            for (int i = 0; i < 4; i++) {
                potential[i] = 0;
            }
            
            for (ImplicitFunction f : functions) {
                // Evaluate the function at the four vertices
                for (int i = 0; i < 4; i++) {
                    potential[i] += evaluate(f, i);
                }
            }
            for (int i = 0; i < 4; i++) {
                in[i] = potential[i] <= 0 ? true : in[i];
            }

        }
        else {
            // Initialize as far as possible
            for (int i = 0; i < 4; i++) {
                potential[i] = Double.MAX_VALUE;
            }

            for (ImplicitFunction f : functions) {
                // Evaluate the function at the four vertices
                for (int i = 0; i < 4; i++) {
                    potential[i] = Math.min(potential[i], evaluate(f, i));
                }
            }
            
            for (int i = 0; i < 4; i++) {
                in[i] = potential[i] <= 0;
            }
        }
    }

    public void displayMarchingSquare(GLAutoDrawable drawable) {
        boolean ambiguous = false;
        boolean inside = false;
        boolean outside = false;
        
        Point2d l1 = new Point2d();
        Point2d l2 = new Point2d();
        Point2d l3 = new Point2d();
        Point2d l4 = new Point2d();
        double a = d / 2;
        
        /*
         * Display lines by checking which of the 15 cases apply
         */
        
        // 0
        if (in[0] & in[1] & in[2] & in[3]) {
            // Completely in!
            inside = true;
        } // 1
        else if (!in[0] & in[1] & in[2] & in[3]) {
            l1.set(p[0].x, p[0].y - a);
            l2.set(p[0].x + a, p[0].y);
        } // 2
        else if (in[0] & in[1] & in[2] & !in[3]) {
            l1.set(p[3].x - a, p[3].y);
            l2.set(p[3].x, p[3].y - a);
        } // 3
        else if (!in[0] & in[1] & in[2] & !in[3]) {
            l1.set(p[0].x, p[0].y - a);
            l2.set(p[3].x, p[3].y - a);
        } // 4
        else if (in[0] & in[1] & !in[2] & in[3]) {
            l1.set(p[2].x - a, p[2].y);
            l2.set(p[2].x, p[2].y + a);
        } // 5
        else if (!in[0] & in[1] & !in[2] & in[3]) {
            l1.set(p[0].x, p[0].y - a);
            l2.set(p[2].x - a, p[2].y);
            
            l3.set(p[0].x + a, p[0].y);
            l4.set(p[2].x, p[2].y + a);
            ambiguous = true;
        } // 6
        else if (in[0] & in[1] & !in[2] & !in[3]) {
            l1.set(p[2].x - a, p[2].y);
            l2.set(p[3].x - a, p[3].y);
        } // 7
        else if (!in[0] & in[1] & !in[2] & !in[3]) {
            l1.set(p[0].x, p[0].y - a);
            l2.set(p[2].x - a, p[2].y);
        } // 8
        else if (in[0] & !in[1] & in[2] & in[3]) {
            l1.set(p[1].x, p[1].y + a);
            l2.set(p[1].x + a, p[1].y);
        } // 9
        else if (!in[0] & !in[1] & in[2] & in[3]) {
            l1.set(p[0].x + a, p[0].y);
            l2.set(p[1].x + a, p[1].y);
        } // 10
        else if (in[0] & !in[1] & in[2] & !in[3]) {
            l1.set(p[1].x, p[1].y + a);
            l2.set(p[3].x - a, p[3].y);
            
            l3.set(p[1].x + a, p[1].y);
            l4.set(p[3].x, p[3].y - a);
            ambiguous = true;
        } // 11
        else if (!in[0] & !in[1] & in[2] & !in[3]) {
            l1.set(p[2].x - a, p[2].y);
            l2.set(p[2].x, p[2].y + a);
        } // 12
        else if (in[0] & !in[1] & !in[2] & in[3]) {
            l1.set(p[0].x, p[0].y - a);
            l2.set(p[3].x, p[3].y - a);
        } // 13
        else if (!in[0] & !in[1] & !in[2] & in[3]) {
            l1.set(p[3].x - a, p[3].y);
            l2.set(p[3].x, p[3].y - a);
        } // 14
        else if (in[0] & !in[1] & !in[2] & !in[3]) {
            l1.set(p[0].x, p[0].y - a);
            l2.set(p[0].x + a, p[0].y);
        } // 15
        else if (!in[0] & !in[1] & !in[2] & !in[3]) {
            // Completely out!
            outside = true;
        }

        if (inside || outside) return;
		GL2 gl = drawable.getGL().getGL2();
        gl.glBegin(GL.GL_LINES);
        gl.glColor4f(1, 0, 0, 0.9f);
        gl.glVertex2d(l1.x, l1.y);
        gl.glVertex2d(l2.x, l2.y);
        
        if (ambiguous) {
            gl.glVertex2d(l3.x, l3.y);
            gl.glVertex2d(l4.x, l4.y);
        }
        gl.glEnd();
        
    }

    private final double epsilon = 1e-5;
    
}
