package implicit.implicit2D;


import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;


/**
 * Renders implicit surfaces in 2D
 * @author piuze
 */
public class ImplicitRenderer2D implements GLObject, Interactor {

    private final JoglRenderer ev;

    private final Grid2D grid = new Grid2D(new Point2d(320, 240), 350);
    
    private final List<ImplicitFunction> functions = new LinkedList<ImplicitFunction>();

    /**
     * Entry point for application
     * @param args
     */
    public static void main(String[] args) {
        new ImplicitRenderer2D();
    }

    private final Dimension winsize = new Dimension(640, 480);

    /**
     * Creates the application / scene instance
     */
    public ImplicitRenderer2D() {
        functions.add(new MetaballFunction(new Point2d(260, 240), 100));
        functions.add(new MetaballFunction(new Point2d(380, 240), 100));
        
        ev = new JoglRenderer("Implicit Surface Visualization", this, new Dimension(winsize),
                new Dimension(winsize.width, winsize.height + 90), true);
        // we add ourselves as an interactor to set up mouse and keyboard controls
        ev.addInteractor(this);
        
        ev.start();
    }

    @Override
	public void init(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glEnable(GL.GL_POINT_SMOOTH);
    }

    @Override
	public void display(GLAutoDrawable drawable) {

        GL gl = drawable.getGL();

        JoglRenderer.beginOverlay(drawable);
        gl.glDisable(GL.GL_LIGHTING);
        
        grid.evaluate(functions);
        grid.display(drawable);

        for (ImplicitFunction f : functions) {
            f.display(drawable);
        }
        
        JoglRenderer.endOverlay(drawable);
        
        // Refresh the window size
        winsize.height = drawable.getHeight();
        winsize.width = drawable.getWidth();
        
        ArrayList<Point3d> list;
        LinkedList<Point3d> l2;
        int [] array = new int[] { 1, 2, 3, 4};
        Matrix4d m = new Matrix4d();
        List<Point3d> l;
        
    }

    @Override
	public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        for (ImplicitFunction f : functions) {
            vfp.add(f.getControls());
        }
        vfp.add(grid.getControls());
        
        return vfp.getPanel();
    }


    private int xcurrent, ycurrent;
    private boolean controlDown, shiftDown;
    @Override
	public void attach(Component component) {
        component.addMouseMotionListener(new MouseMotionListener() {

            @Override
			public void mouseDragged(MouseEvent e) {
                xcurrent = e.getPoint().x;
                ycurrent = e.getPoint().y;
            }

            @Override
			public void mouseMoved(MouseEvent e) {
                xcurrent = e.getPoint().x;
                ycurrent = e.getPoint().y;

            }
        });
        component.addMouseListener(new MouseListener() {

            @Override
			public void mouseClicked(MouseEvent e) {
                // do nothing
            }

            @Override
			public void mouseEntered(MouseEvent e) {
            }

            @Override
			public void mouseExited(MouseEvent e) {
            }

            @Override
			public void mousePressed(MouseEvent e) {
            }

            @Override
			public void mouseReleased(MouseEvent e) {
            }
        });
        component.addKeyListener(new KeyAdapter() {

            @Override
			public void keyPressed(KeyEvent e) {
                controlDown = e.isControlDown();
                shiftDown = e.isShiftDown();

                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    // TODO
                } else if (e.getKeyCode() == KeyEvent.VK_S) {
                    // TODO
                }
            }
        });
    }

    @Override
    public String getName() {
        return "Renderer";
    }

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

}
