package implicit.implicit2D;


import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;


import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;


/**
 * 
 * @author piuze
 */
public class Grid2D {

    private final List<Cell2D> cells = new LinkedList<Cell2D>();
    private final Point2d position = new Point2d();
    
    public DoubleParameter gridSize = new DoubleParameter("size", 200, 0, 1000);
    public DoubleParameter gridResolution = new DoubleParameter("resolution", 15, 0, 300);
    public DoubleParameter ratio = new DoubleParameter("radius ratio", 1.0, 0.0, 10);
    public DoubleParameter gridDrawSize = new DoubleParameter("draw size", 1, 1, 10); 
    public BooleanParameter drawEvaluation = new BooleanParameter("draw evaluation", false);
    public BooleanParameter draw = new BooleanParameter("draw", true);
    public BooleanParameter accumulate = new BooleanParameter("Accumulate", true);
    
    private double cellSize;
    
    /**
     * @param origin 
     * @param size
     * @param resolution
     */
    public Grid2D(Point2d origin, int size) {
        gridSize.setValue(size);
        position.set(origin);
        
        createMesh();
    }
    
    public Grid2D() {
        position.set(0, 0);

        createMesh();
    }

    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Grid (Marching Squares)"));

        ParameterListener l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {

                createMesh();
            }
        };
        gridSize.addParameterListener(l);
        gridResolution.addParameterListener(l);
        ratio.addParameterListener(l);
        
        
        vfp.add(accumulate.getControls());
        vfp.add(gridSize.getSliderControls(false));
        vfp.add(gridResolution.getSliderControls(false));
        vfp.add(ratio.getSliderControls(false));
        vfp.add(draw.getControls());
        vfp.add(drawEvaluation.getControls());
        vfp.add(gridDrawSize.getSliderControls(false));

        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(), "Grid (Marching Squares)");
        return panel;
        
    }
    
    private void createMesh() {
        cells.clear();
        
        double gsize = gridSize.getValue();
        double x0 = position.x - gsize / 2;
        double y0 = position.y - gsize / 2;
        int gres = (int) gridResolution.getValue();

        cellSize = gsize / gres;

        double radius = ratio.getValue()  * cellSize / 2;
        
        double x, y;
        int it = 0;
        for (int i = 0; i <= gres; i++) {
            for (int j = 0; j <= gres; j++) {
                x = x0 + i * cellSize;
                y = y0 + j * cellSize;
                cells.add(new Cell2D(it++, x, y, radius));
            }
        }
    }
    
    /**
     * 
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        double x0 = position.x - gridSize.getValue() / 2 - cellSize / 2;
        double y0 = position.y + gridSize.getValue() / 2 + cellSize / 2;
        double dd = gridSize.getValue() + cellSize;
        
        gl.glColor4f(1, 1, 1, 0.7f);
        gl.glBegin(GL.GL_LINE_STRIP);
        // Bottom
        gl.glVertex2d(x0, y0);
        gl.glVertex2d(x0 + dd, y0);
        // Left
        gl.glVertex2d(x0, y0);
        gl.glVertex2d(x0, y0 - dd);
        // Top
        gl.glVertex2d(x0, y0 - dd);
        gl.glVertex2d(x0 + dd, y0 - dd);
        // Right
        gl.glVertex2d(x0 + dd, y0 - dd);
        gl.glVertex2d(x0 + dd, y0);
        
        gl.glEnd();
        
        if (draw.getValue()) {
            // Draw the grid center points
            Point2d p;
            gl.glPointSize((int) gridDrawSize.getValue());
            gl.glBegin(GL.GL_POINTS);
            for (Cell2D cell : cells) {
                p = cell.getPosition();

                gl.glColor4d(0, 1, 0, 0.9);
                gl.glVertex2d(p.x, p.y);
            }
            gl.glEnd();

            // Draw the grid
            gl.glColor4f(1, 1, 1, 0.1f);
            for (Cell2D cell : cells) {
                gl.glBegin(GL.GL_LINE_STRIP);
                
                for (int i = 0; i < 3; i++) {
                    gl.glVertex2d(cell.p[i].x, cell.p[i]. y);
                    gl.glVertex2d(cell.p[i+1].x, cell.p[i+1]. y);
                }
                gl.glVertex2d(cell.p[3].x, cell.p[3]. y);
                gl.glVertex2d(cell.p[0].x, cell.p[0]. y);
                gl.glEnd();
            }
            
        }

        // Draw the marching cube contour
        for (Cell2D cell : cells) {
            cell.displayMarchingSquare(drawable);
        }
        
        // Draw the function evaluation
        if (drawEvaluation.getValue()) displayEvaluation(drawable);

    }
    
    /**
     * Displays the corners of each grid cell
     * with a color code representing whether or not
     * the point lies inside or outside of the implicit
     * surface.
     * @param drawable
     */
    public void displayEvaluation(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        
        float[] gray = new float[] { 1f, 0.2f, 0.2f, 0.3f};
        float[] in = new float[] { 1, 1, 1, 1};
        
        gl.glPointSize(5);
        gl.glBegin(GL.GL_POINTS);
        for (Cell2D cell : cells) {
            for (int i = 0; i < 4; i++) {
                if (cell.in[i]) gl.glColor3fv(in, 1);
                else gl.glColor3fv(gray, 1);
                
                gl.glVertex2d(cell.p[i].x, cell.p[i].y);
            }
        }
        gl.glEnd();

    }

    public void evaluate(List<ImplicitFunction> functions) {
        for (Cell2D cell : cells) {
            cell.evaluate(functions, accumulate.getValue());
        }
    }
}
