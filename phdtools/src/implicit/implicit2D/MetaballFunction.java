package implicit.implicit2D;


import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point2d;


import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

public class MetaballFunction extends ImplicitFunction {
    private final Point2d p0 = new Point2d();
    private final DoubleParameter radiusp = new DoubleParameter("radius", 100, 1e-12, 500);
    private final DoubleParameter x0p = new DoubleParameter("origin x", 320, 0, 1000);
    private final DoubleParameter y0p = new DoubleParameter("origin y", 240, 0, 1000);
    private final BooleanParameter draw = new BooleanParameter("draw", false);
    
    private double radius;
    
    public MetaballFunction(Point2d origin, double radius) {
        p0.set(origin);
        x0p.setValue(p0.x);
        y0p.setValue(p0.y);
        this.radiusp.setValue(radius);
        this.radius = radius;
    }
    public MetaballFunction() {
        p0.set(x0p.getValue(), y0p.getValue());
        radius = radiusp.getValue();
    }
    
    @Override
    public double evaluate(Point2d p) {
        double r = p.distance(p0);
        double b = r/100;
        double s = radiusp.getValue();
        return b - Math.exp(-r*r/(s*s));
//        double r = p.distance(p0);
//        double f;
//        
//        if (r < radius/3.0) {
//            f = 1 - ((3 * r * r) / (radius*radius));
//          } else if (r >= radius/3.0 && r < radius) {
//            f = 1.5 * (1-(r/radius)) * (1-(r/radius));
//          } else {
//            f = 0;
//          }
//        
//        return f;
    }
    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), getName() + " Function"));

        ParameterListener l = new ParameterListener() {
            @Override
            public void parameterChanged(Parameter parameter) {
                p0.set(x0p.getValue(), y0p.getValue());
                radius = radiusp.getValue();
            }
        };
        radiusp.addParameterListener(l);
        x0p.addParameterListener(l);
        y0p.addParameterListener(l);
        
        vfp.add(radiusp.getSliderControls(false));
        vfp.add(x0p.getSliderControls(false));
        vfp.add(y0p.getSliderControls(false));
        vfp.add(draw.getControls());

        CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(), getName() + " Function");
        return panel;
    }
    
    @Override
    public String getName() {
        return "Metaball";
    }
    
    @Override
    public void display(GLAutoDrawable drawable) {
        if (!draw.getValue()) return;
        
        int n = 30;
        double da = 2*Math.PI / n;
        
        GL gl = drawable.getGL();
        
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glColor4f(1, 1, 0, 0.4f);
        double x, y;
        for (int i = 0; i <= n; i++) {
            x = p0.x + radius * Math.cos(i * da);
            y = p0.y + radius * Math.sin(i * da);
            gl.glVertex2d(x, y);
        }
        gl.glEnd();
    }
    
}
