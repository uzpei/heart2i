package implicit.implicit2D;

import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point2d;

public abstract class ImplicitFunction {
    public abstract double evaluate(Point2d p);
    
    public abstract JPanel getControls();
    
    public abstract String getName();
    
    public abstract void display(GLAutoDrawable drawable);
}
