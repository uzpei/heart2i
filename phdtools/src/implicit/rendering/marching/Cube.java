package implicit.rendering.marching;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;

/**
 * Class representing a cube.
 * @author piuze
 */
public class Cube {

    /**
     * The vertices making up this cube.
     */
    public Vertex[] v = new Vertex[8];
    
    /**
     * The edges making up this cube.
     */
    public Edge[] e = new Edge[12];
    
    /**
     * The side length of this cube.
     */
    public double length;
    
    /**
     * The center of this cube.
     */
    public Point3d center = new Point3d();
    
    /**
     * The 6-tetrahedra decomposition of this cube.
     */
    public Tetrahedron[] tetras = new Tetrahedron[6];
    
// 4 .--------. 5
//   |\       |\          floor   :  0,1,2 0,2,3
//   | \      | \         ceiling :  4,5,6 4,6,7 
//   |7 .--------. 6      back    :  0,1,5 0,5,4 
//   |  |     |  |        front   :  3,7,6 3,6,2 
// 0 .--|-----. 1|        left    :  0,4,7 0,7,3 
//    \ |      \ |        right   :  1,5,6 1,6,2 
//     \|       \|        
//    3 .--------. 2
// Edges are indexed by going in a counter clockwise fashion,
// from bottom to up, starting at vertex 0 (e.g. edge
// joining vertices 0 and 3 is ID4).
// with side length m, origin (x,y) vertices are:
// x + 0 , y + 0, z + 0    // 0    
// x + m , y + 0, z + 0    // 1    
// x + m , y + 0, z + m    // 2    
// x + 0 , y + 0, z + m    // 3    
// x + 0 , y - m, z + 0    // 4    
// x + m , y - m, z + 0    // 5    
// x + m , y - m, z + m    // 6    
// x + 0 , y - m, z + m    // 7    
    
    /**
     * Creates a new cube.
     * @param p The position of the bottom and back left vertex (v0, see header)
     * @param m The side length.
     */
    public Cube(Point3d p, double m) {
        center.set(p.x + m/2, p.y - m/2, p.z + m/2);
        
        length = m;
        
        // First create the vertices.
        for (int i = 0; i < 8; i++) {
            v[i] = new Vertex();
        }
        
        // Set the vertices based on the origin and length;
//        v[0].set(p.x + 0, p.y + 0, p.z + 0);
//        v[1].set(p.x + m, p.y + 0, p.z + 0);
//        v[2].set(p.x + m, p.y + 0, p.z + m);
//        v[3].set(p.x + 0, p.y + 0, p.z + m);
//        v[4].set(p.x + 0, p.y - m, p.z + 0);
//        v[5].set(p.x + m, p.y - m, p.z + 0);
//        v[6].set(p.x + m, p.y - m, p.z + m);
//        v[7].set(p.x + 0, p.y - m, p.z + m);
        v[0].set(p.x - m/2, p.y + m/2, p.z - m/2);
        v[1].set(p.x + m/2, p.y + m/2, p.z - m/2);
        v[2].set(p.x + m/2, p.y + m/2, p.z + m/2);
        v[3].set(p.x - m/2, p.y + m/2, p.z + m/2);
        v[4].set(p.x - m/2, p.y - m/2, p.z - m/2);
        v[5].set(p.x + m/2, p.y - m/2, p.z - m/2);
        v[6].set(p.x + m/2, p.y - m/2, p.z + m/2);
        v[7].set(p.x - m/2, p.y - m/2, p.z + m/2);
        
        // Set the edges
        e[0] = new Edge(v[0], v[1]);
        e[1] = new Edge(v[1], v[2]);
        e[2] = new Edge(v[2], v[3]);
        e[3] = new Edge(v[3], v[0]);
        e[4] = new Edge(v[0], v[4]);
        e[5] = new Edge(v[1], v[5]);
        e[6] = new Edge(v[2], v[6]);
        e[7] = new Edge(v[3], v[7]);
        e[8] = new Edge(v[4], v[5]);
        e[9] = new Edge(v[5], v[6]);
        e[10] = new Edge(v[6], v[7]);
        e[11] = new Edge(v[7], v[4]);
        
        // Tetrahedrize the cube
        decompose();
    }
    
    /**
     * Decompose the cube into 6 tetrahedra.
     * @param c
     */
    private void decompose() {
        int v0 = 0;
        int v1 = 1;
        int v2 = 2;
        int v3 = 3;
        int v4 = 4;
        int v5 = 5;
        int v6 = 6;
        int v7 = 7;
        
        tetras[0] = new Tetrahedron(v[v0], v[v2], v[v3], v[v7]);
        tetras[1] = new Tetrahedron(v[v0], v[v2], v[v6], v[v7]);
        tetras[2] = new Tetrahedron(v[v0], v[v4], v[v6], v[v7]);
        tetras[3] = new Tetrahedron(v[v0], v[v6], v[v1], v[v2]);
        tetras[4] = new Tetrahedron(v[v0], v[v6], v[v1], v[v4]);
        tetras[5] = new Tetrahedron(v[v5], v[v6], v[v1], v[v4]);
    }

    public void display(GLAutoDrawable drawable, boolean drawTetra) {
        display(drawable);
        
        if (drawTetra) {
            // Draw the tetra decomposition
          int i = 0;
          GL2 gl = drawable.getGL().getGL2();
          for (Tetrahedron t : tetras) {
              gl.glColor3f(i / 6.0f, (6-i) / 6.0f, 0);
              t.display(drawable);
              i++;
          }
        }
    }
    
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glBegin(GL.GL_LINES);
        for (Edge edge : e) {
            gl.glVertex3d(edge.v1.x, edge.v1.y, edge.v1.z);
            gl.glVertex3d(edge.v2.x, edge.v2.y, edge.v2.z);
        }
        gl.glEnd();

    }
}
