package implicit.rendering.marching;


import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * Extends a triangle by adding information about the 
 * normal and center.
 * @author piuze
 */
public class ExtendedTriangle extends Triangle {

    public Vector3d normal = new Vector3d();
    public Point3d center = new Point3d();
    
    public ExtendedTriangle(Vertex v0, Vertex v1, Vertex v2) {
        super(v0, v1, v2);
        createNormal(v0, v1, v2, 1);
    }

    public ExtendedTriangle(Vertex v0, Vertex v1, Vertex v2, boolean b, int flip) {
        super(v0, v1, v2, b);
        createNormal(v0, v1, v2, flip);
    }

    private void createNormal(Vertex v0, Vertex v1, Vertex v2, int flip) {
        
        center.set(v0);
        center.add(v1);
        center.add(v2);
        center.scale(1d/3d);

        Vector3d u = new Vector3d();
        Vector3d v = new Vector3d();
        u.sub(v1, v0);
        v.sub(v2, v0);
        normal.cross(u, v);
        normal.normalize();
        Vector3d z = new Vector3d(0,0,-1);
        if (z.dot(normal) > 0) normal.scale(-1);
//        normal.scale(flip);
//        System.out.println("FLIP = " + flip + " NORMAL = " + normal);
    }

    private GL2 gl;
    private GLUT glut;
    @Override
	public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL().getGL2();

        // Draw the triangle
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor4d(1, 1, 1, 0.5f);
        gl.glNormal3d(normal.x, normal.y, normal.z);
        gl.glVertex3d(v[0].x, v[0].y, v[0].z);
        gl.glVertex3d(v[1].x, v[1].y, v[1].z);
        gl.glVertex3d(v[2].x, v[2].y, v[2].z);
        gl.glEnd();
    }

    public void displayDebug(GL2 gl2) {
        gl = gl2;
        
        // Draw the center
        gl.glPointSize(10);
        gl.glBegin(GL.GL_POINTS);

        gl.glColor4d(0, 1, 0, 0.9);
        gl.glVertex3d(center.x, center.y, center.z);
        gl.glEnd();

        // Draw the normal
        gl.glLineWidth(2f);
        gl.glBegin(GL.GL_LINES);
        gl.glColor3f(0, 0, 1);
        gl.glVertex3d(center.x, center.y, center.z);
        gl.glVertex3d(center.x + 0.1 * normal.x, center.y + 0.1 * normal.y, center.z + 0.1 * normal.z);
        gl.glEnd();

        // Draw the triangle
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor4d(1, 1, 1, 0.5f);
        gl.glNormal3d(normal.x, normal.y, normal.z);
        gl.glVertex3d(v[0].x, v[0].y, v[0].z);
        gl.glVertex3d(v[1].x, v[1].y, v[1].z);
        gl.glVertex3d(v[2].x, v[2].y, v[2].z);
        gl.glEnd();
        
        // Draw the vertex indices
        glut = new GLUT();
        gl.glColor4f(1, 1, 1, 1);
        for (int i = 0; i < 3; i++) {
            gl.glRasterPos3d(v[i].x, v[i].y, v[i].z); glut.glutBitmapString(GLUT.BITMAP_9_BY_15, "v" + i);
        }

    }


}
