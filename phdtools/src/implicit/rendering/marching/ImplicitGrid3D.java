package implicit.rendering.marching;

import implicit.implicit3d.functions.ImplicitFunction3D;
import implicit.implicit3d.interpolation.ThinPlateSpline;

import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.StopWatch;

/**
 * 
 * @author piuze
 */
public class ImplicitGrid3D {

	private final List<Cell3D> cells = new LinkedList<Cell3D>();
	private final Point3d position = new Point3d();

	private final DoubleParameter gridSize = new DoubleParameter("size", 4, 0, 100);
	private final DoubleParameter gridResolution = new DoubleParameter("resolution",
			10, 1, 25);
	private final DoubleParameter ratio = new DoubleParameter("radius ratio", 1,
			0.0, 10);
	private final BooleanParameter drawImplicit = new BooleanParameter(
			"implicit", true);
	private final BooleanParameter drawEvaluation = new BooleanParameter(
			"evaluation", false);
	private final BooleanParameter draw = new BooleanParameter("draw", false);
	private final BooleanParameter accumulate = new BooleanParameter("Accumulate", false);
	private final BooleanParameter drawDebug = new BooleanParameter("debug mode", false);

	private final BooleanParameter useTransparency = new BooleanParameter("transparent", false);
	private final BooleanParameter displayCurvature = new BooleanParameter("curvature", false);
	private final BooleanParameter displayInflection = new BooleanParameter("inflection", false);
	
	private double cellSize;

	/**
	 * @param origin
	 * @param gsize
	 * @param resolution
	 */
	public ImplicitGrid3D(Point3d origin, double gsize, double resolution) {
		position.set(origin);
		gridSize.setValue(gsize);
		gridResolution.setValue(resolution);
		
		recreateMesh();
	}

	public ImplicitGrid3D() {
		recreateMesh();
	}
	
	private void addParameterListener() {
		ParameterListener l = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				recreateMesh();
			}
		};
	}

	public void resize(Point3d pos, int size, int res) {
		position.set(pos);
		gridSize.setValue(size);
		gridResolution.setValue(res);
	}

	private boolean updateRequest = false;

	public void setUpdateRequest(boolean b) {
		updateRequest = b;
	}

	private final DoubleParameter xo = new DoubleParameter("x offset", 0, 0, 100);
	private final DoubleParameter yo = new DoubleParameter("y offset", 0, 0, 100);
	private final DoubleParameter zo = new DoubleParameter("z offset", 0, 0, 100);

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Grid (Marching Tetrahedra)"));

		ParameterListener l = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				setUpdateRequest(true);
			}
		};
		gridSize.addParameterListener(l);
		gridResolution.addParameterListener(l);
		ratio.addParameterListener(l);

		vfp.add(gridSize.getSliderControls(false));
		vfp.add(gridResolution.getSliderControls(false));
		vfp.add(ratio.getSliderControls(false));

		JPanel pbs = new JPanel(new GridLayout(2, 3));
		pbs.setBorder(new TitledBorder("Display"));
		
		pbs.add(draw.getControls());
		pbs.add(drawImplicit.getControls());
		pbs.add(drawEvaluation.getControls());
//		pbs.add(accumulate.getControls());
		pbs.add(drawDebug.getControls());
		pbs.add(displayCurvature.getControls());
//		pbs.add(displayInflection.getControls());
		pbs.add(useTransparency.getControls());
		
		vfp.add(pbs);
		
		VerticalFlowPanel vfpp = new VerticalFlowPanel();
		vfpp.setBorder(new TitledBorder("position"));

		vfpp.add(xo.getSliderControls());
		vfpp.add(yo.getSliderControls());
		vfpp.add(zo.getSliderControls());

		ParameterListener pl = new ParameterListener() {

			@Override
			public void parameterChanged(Parameter parameter) {
				position.set(xo.getValue(), yo.getValue(), zo.getValue());
			}
		};
		xo.addParameterListener(pl);
		yo.addParameterListener(pl);
		zo.addParameterListener(pl);

		xo.addParameterListener(l);
		yo.addParameterListener(l);
		zo.addParameterListener(l);

		CollapsiblePanel cp = new CollapsiblePanel(vfpp.getPanel());
		cp.collapse();
		vfp.add(cp);

		CollapsiblePanel panel = new CollapsiblePanel(vfp.getPanel(),
				"Grid (Marching Tetrahedra)");
		return panel;

	}

	public StopWatch swCreate = new StopWatch("Create");

	private void recreateMesh() {
		setUpdateRequest(false);

		swCreate.start();

		cells.clear();

		double gsize = gridSize.getValue();
		double x0 = position.x - gsize / 2;
		double y0 = position.y - gsize / 2;
		double z0 = position.z - gsize / 2;
		double gres = Math.round(gridResolution.getValue());

		cellSize = gsize / gres;

		double radius = ratio.getValue() * cellSize;

		double x, y, z;
		int it = 0;
		for (int i = 0; i < gres; i++) {
			for (int j = 0; j < gres; j++) {
				for (int k = 0; k < gres; k++) {
					x = x0 + i * cellSize + cellSize / 2;
					y = y0 + j * cellSize + cellSize / 2;
					z = z0 + k * cellSize + cellSize / 2;
					cells.add(new Cell3D(it++, x, y, z, radius));
				}
			}
		}

		swCreate.stop();
	}

	/**
	 * 
	 * @param drawable
	 */
	public void display(GLAutoDrawable drawable, ImplicitFunction3D function) {
        GL2 gl = drawable.getGL().getGL2();
		
		if (updateRequest || function.hasChanged()) {
			System.out.println("Grid update request");
			recreateMesh();
//			function.setHasChanged(true);
//		}

//		if (function.hasChanged()) {
			for (Cell3D cell : cells) {
				cell.evaluate(function, accumulate.getValue());
			}
		}

		if (draw.getValue()) {
			// Draw the grid center points
			Point3d p;
			gl.glPointSize(2);
			gl.glColor4d(0, 1, 0, 0.9);
			gl.glBegin(GL2.GL_POINTS);
			for (Cell3D cell : cells) {
				p = cell.cube.center;
				gl.glVertex3d(p.x, p.y, p.z);
			}
			gl.glEnd();

			// Draw the grid
			gl.glColor4f(1, 1, 1, 0.1f);
			gl.glLineWidth(1.5f);
			for (Cell3D cell : cells) {
				cell.cube.display(drawable);
			}

		}

		// Draw the generated polygonization
		if (drawImplicit.getValue()) {
	        gl.glDisable(GL.GL_CULL_FACE );

			if (drawDebug.getValue()) {
				for (Cell3D cell : cells) {
					cell.display(gl, true);
				}
			} else {
				boolean btransparency = useTransparency.getValue();
				boolean bcurvature = displayCurvature.getValue();
				boolean binflection = displayInflection.getValue();
				
				Vector3d dummyv = new Vector3d();
				
				if (!btransparency) {
					gl.glEnable(GL2.GL_LIGHTING);
					
//			        gl.glEnable(GL2.GL_DEPTH_TEST);
////			        gl.glDisable(GL2.GL_DEPTH_TEST);
//			        gl.glDepthFunc(GL2.GL_LEQUAL);
//			        gl.glClearDepth(1);
//					gl.glDepthMask(true);
//			        gl.glDisable(GL2.GL_CULL_FACE);

//			        gl.glShadeModel(GL2.GL_SMOOTH);
//			        
			        float[] color = { 229f / 255f, 208f / 255f, 207f / 255f, 1f };
			        float r = 0.2f;
			        // final float[] shinycolour = new float[] {r,r,r,1};
			        final float[] shinycolour = new float[] { r * color[0],
			                r * color[1], r * color[2], 1 };

			        // Leave this enabled
			        gl.glEnable(GL2.GL_LIGHTING);
			        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE,
			                color, 0);
			        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, shinycolour, 0);
			        gl.glMateriali(GL2.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 0);
				}
				else {
					gl.glDisable(GL2.GL_LIGHTING);
				}
				
				double[] tcol = new double[] { 0, 0, 1, 0.25 };
//				gl.glColor4d(0, 0, 0, 0.25f);
				
				if (function instanceof ThinPlateSpline) {
					ThinPlateSpline tps = (ThinPlateSpline) function;

//		            gl.glColor4d(1, 1, 1, 0.4f);
//		            gl.glBegin(GL.GL_TRIANGLES);
//		            for (ExtendedTriangle t : isotriangles) {
//		                gl.glNormal3d(t.normal.x, t.normal.y, t.normal.z);
//		                gl.glVertex3d(t.v[0].x, t.v[0].y, t.v[0].z);
//		                gl.glVertex3d(t.v[1].x, t.v[1].y, t.v[1].z);
//		                gl.glVertex3d(t.v[2].x, t.v[2].y, t.v[2].z);
//		            }
//		            gl.glEnd();

					gl.glBegin(GL2.GL_TRIANGLES);
					for (Cell3D cell : cells) {
						for (ExtendedTriangle t : cell.isotriangles) {
			                gl.glNormal3d(t.normal.x, t.normal.y, t.normal.z);
							for (Vertex v : t.v) {
								if (bcurvature) {
									double k = Math
											.abs(tps.getMeanCurvature(v));
									double rfact = 1.5 * Math.abs(k);
									tcol[0] = rfact;
									tcol[3] = 0.25 + 0.1 * rfact;
								}
								
								gl.glColor4d(tcol[0], tcol[1], tcol[2], tcol[3]);
								gl.glVertex3d(v.x, v.y, v.z);
							}
						}
					}
					gl.glEnd();
					// Draw inflection points
					if (binflection) {
						double eps = 1e-3;
						int n = 7;
						gl.glPointSize(10f);
						gl.glBegin(GL2.GL_POINTS);
						for (Cell3D cell : cells) {
							for (ExtendedTriangle t : cell.isotriangles) {

								for (Vertex v : t.v) {
									if (tps.isInflection(v, dummyv)) {
										gl.glColor4d(0, 1, 1, 0.8);
										gl.glVertex3d(v.x, v.y, v.z);
									}
								}
							}
						}
						gl.glEnd();
					}
				}
				else {
					if (useTransparency.getValue()) {
						gl.glDisable(GL2.GL_LIGHTING);
						for (Cell3D cell : cells) {
							cell.display(gl, false);
						}
					}
					else {
			            gl.glColor4d(1, 1, 1, 0.4f);
						gl.glBegin(GL2.GL_TRIANGLES);
						for (Cell3D cell : cells) {
							for (ExtendedTriangle t : cell.isotriangles) {
//				                gl.glNormal3d(t.normal.x, t.normal.y, t.normal.z);
								for (Vertex v : t.v) {
									Vector3d n1 = function.dF(v);
									n1.normalize();
//									n1.scale(-1);
									gl.glNormal3d(n1.x, n1.y, n1.z);
									gl.glVertex3d(v.x, v.y, v.z);
								}
							}
						}
						gl.glEnd();
					}
				}
			}

	        gl.glEnable(GL.GL_CULL_FACE );
		}

		// Draw the function evaluation
		if (drawEvaluation.getValue())
			displayEvaluation(drawable);
	}

	/**
	 * Displays the corners of each grid cell with a color code representing
	 * whether or not the point lies inside or outside of the implicit surface.
	 * 
	 * @param drawable
	 */
	public void displayEvaluation(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

		float[] gray = new float[] { 1f, 0.2f, 0.2f, 0.3f };
		float[] in = new float[] { 1, 1, 1, 1 };

		gl.glPointSize(5);
		gl.glBegin(GL2.GL_POINTS);
		for (Cell3D cell : cells) {
			for (int i = 0; i < 4; i++) {
				if (cell.in[i])
					gl.glColor3fv(in, 1);
				else
					gl.glColor3fv(gray, 1);

				gl.glVertex3d(cell.p[i].x, cell.p[i].y, cell.p[i].z);
			}
		}
		gl.glEnd();

	}

	/**
	 * @return The number of cells in the discretization of space.
	 */
	public int getCellCount() {
		return cells.size();
	}

	public void setResolution(double v) {
		gridResolution.setValue(v);
	}

	public void setSize(double v) {
		gridSize.setValue(v);
	}

	public void setDisplayGrid(boolean b) {
		draw.setValue(b);
	}

	public void setRadiusRatio(double d) {
		ratio.setValue(d);
	}

	public void setUseTransparency(boolean b) {
		useTransparency.setValue(b);
	}
	
	public void setDisplayCurvature(boolean b) {
		displayCurvature.setValue(b);
	}

	public void setDisplayInflection(boolean b) {
		displayInflection.setValue(b);
	}

}
