package implicit.rendering.marching;

import implicit.implicit3d.functions.ImplicitFunction3D;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Cell3D {
    
    public Cube cube;
    
    public int index;
    private double radius;
    private Point3d pos = new Point3d();

    // The value of the implicit function at the 8 vertices
    public double[] potential = new double[8];

    // The location of vertices (in/out of the implicit f)
    public boolean[] in = new boolean[8];
    
    // Vertices
    public Vertex[] p = new Vertex[8];
    
    // Side length
    public double d;
    
    public MarchingTetrahedron mt;
    
    public List<ExtendedTriangle> isotriangles = new LinkedList<ExtendedTriangle>();
    
    public Cell3D(int index, double x0, double y0, double z0, double length) {
        this.index = index;
        
        pos.set(x0, y0, z0);
        radius = length / 2;
        
        cube = new Cube(pos, length);
        
        mt = new MarchingTetrahedron();
        
        for (int i = 0; i < 8; i++) {
            p[i] = cube.v[i];
        }

        d = p[1].distance(p[2]);
        
    }
    
    /**
     * Evaluate the function at the given vertex.
     * @param f
     * @param vertex
     * @return
     */
    public double evaluate(ImplicitFunction3D f, int vertex) {
        
        return f.F(p[vertex]);
    }
    
    public Point3d getPosition() {
        return pos;
    }

    public double getRadius() {
        return radius;
    }

    public void evaluate(ImplicitFunction3D function, boolean accumulate) {
        
        if (accumulate) {
            double  v;
            // Assign the potential to the tetrahedra
            for (int i = 0; i < 6; i++) {
                // Evaluate each function at the four vertices
                for (int k = 0; k < 4; k++) {
                    cube.tetras[i].v[k].val = 0;
                    cube.tetras[i].v[k].val += function.F(cube.tetras[i].v[k]);
                }
            }
        }
        else {
            // Assign the potential to the tetrahedra
            for (int i = 0; i < 6; i++) {
                // Evaluate each function at the four vertices
                for (int k = 0; k < 4; k++) {
                        cube.tetras[i].v[k].val = function.F(cube.tetras[i].v[k]);
                }
            }
        }

        for (int i = 0; i < 8; i++) {
            in[i] = p[i].val <= 0 ? true : in[i];
        }

        isotriangles.clear();
        for (int i = 0; i < 6; i++) {
            ExtendedTriangle[] ts = mt.triangulate(cube.tetras[i], 0);
            if (ts == null) continue;
            for (ExtendedTriangle t : ts) {
            	t.normal = new Vector3d(function.dF(t.center));
            	t.normal.normalize();
            	t.normal.scale(-1);
                isotriangles.add(t);
            }
        }
    }
    
    /**
     * Children cells.
     */
    private List<Cell3D> subcells = new LinkedList<Cell3D>();

    /**
     * Neighboring cells.
     */
    private HashSet<Cell3D> neighbors = new HashSet<Cell3D>();
    
    /**
     * Add a cell neighbor to this cell.
     * @param c
     */
    public void addNeighbor(Cell3D c) {
        neighbors.add(c);
    }
    
    /**
     * @return The list of neighboring cells.
     */
    public HashSet<Cell3D> getNeighbors() {
        return neighbors;
    }
    
    /**
     * Remove all neighbors from this cell.
     */
    public void clearNeighbors() {
        neighbors.clear();
    }
    
    /**
     * @return The center of this cell.
     */
    public Point3d getCenter() {
        return pos;
    }
    
    /**
     * @param p A point.
     * @return Whether or not this cell contains this point.
     */
    public boolean contains(Point3d p) {
        return true 
        && p.x >= pos.x - getRadius() 
        && p.x <= pos.x + getRadius()
        && p.y >= pos.y - getRadius()
        && p.y <= pos.y + getRadius()
        && p.z >= pos.z - getRadius()
        && p.z <= pos.z + getRadius();
    }

    /**
     * Avoid calling this if you are handling numerous cells. It is better
     * to minimize the number of glBegin/glEnd calls by placing them
     * outside an iteration over all cells.
     * @param gl
     * @param debug 
     */
    public void display(GL2 gl, boolean debug) {
        if (debug) {
            gl.glColor4d(1, 1, 1, 0.4f);
            for (ExtendedTriangle t : isotriangles) {
                t.displayDebug(gl);
            }
        }
        else {
            gl.glColor4d(1, 1, 1, 0.4f);
            gl.glBegin(GL.GL_TRIANGLES);
            for (ExtendedTriangle t : isotriangles) {
                gl.glNormal3d(t.normal.x, t.normal.y, t.normal.z);
                gl.glVertex3d(t.v[0].x, t.v[0].y, t.v[0].z);
                gl.glVertex3d(t.v[1].x, t.v[1].y, t.v[1].z);
                gl.glVertex3d(t.v[2].x, t.v[2].y, t.v[2].z);
            }
            gl.glEnd();
        }
    }

}
