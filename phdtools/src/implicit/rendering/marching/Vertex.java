package implicit.rendering.marching;

import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Class extending a Point3d by allowing
 * to store a scalar.
 * @author piuze
 */
public class Vertex extends Point3d {

    /**
     * Automatically generated.
     */
    private static final long serialVersionUID = 4432020205646731848L;

    /**
     * A list of normal vectors at this vertex
     */
    private List<Vector3d> normals = null;
    
    /**
     * A scalar assigned to this vertex.
     */
    public double val = 0;
    
    public Vertex() {
        super();
    }
    
    public Vertex(Vertex another) {
        super(another);
        val = another.val;
    }
    
    public Vertex(Point3d p) {
        super(p);
    }

    public Vertex(double x, double y, double z) {
        super(x, y ,z);
    }
    
    public void addNormal(Vector3d v) {
    	if (normals == null) {
    		normals = new LinkedList<Vector3d>();
    	}
    	
    	normals.add(v);
    }
}
