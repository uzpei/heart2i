package implicit.rendering.marching;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Class representing an edge between two vertices.
 * @author piuze
 */
public class Edge {
    /**
     * First vertex.
     */
    public Vertex v1 = new Vertex();
    
    /**
     * Second vertex.
     */
    public Vertex v2 = new Vertex();
    
    /**
     * Difference vector between the
     * two vertices.
     */
    public Vector3d dv = new Vector3d();
    
    /**
     * Creates a new edge between two vertices.
     * @param p1
     * @param p2
     */
    public Edge(Point3d p1, Point3d p2) {
        v1.set(p1);
        v2.set(p2);
        dv.sub(v2, v1);
    }
}
