package implicit.rendering.marching;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.gl2.GLUT;


/**
 * Class representing a triangle, composed of
 * three Point3d and three Edge.
 * @author piuze
 */
public class Triangle {
    
    /**
     * The three vertices of this triangle.
     */
    public Vertex[] v = new Vertex[3];
    
    /**
     * The three edges of this triangle.
     */
    public Edge[] e = new Edge[3];
    
    /**
     * Build a triangle out of three vertices.
     * Keep the vertices by reference.
     * @param v1
     * @param v2
     * @param v3
     */
    public Triangle(Vertex v1, Vertex v2, Vertex v3) {

        v[0] = v1;
        v[1] = v2;
        v[2] = v3;

        e[0] = new Edge(v1, v2);
        e[1] = new Edge(v2, v3);
        e[2] = new Edge(v3, v1);
    }
    
    /**
     * Build a triangle out of three vertices.
     * Keep the vertices by reference, or by value,
     * as specified by the boolean.
     * @param v1
     * @param v2
     * @param v3
     * @param byRef if vertices should be kept by reference or by value.
     */
    public Triangle(Vertex v1, Vertex v2, Vertex v3, boolean byRef) {
        this(byRef ? v1 : new Vertex(v1), byRef ? v2 : new Vertex(v2), byRef ? v3 : new Vertex(v3));
    }
    
    private GL2 gl;
    private GLUT glut;
    public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL().getGL2();
        
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor4d(1, 1, 1, 0.5f);
        gl.glVertex3d(v[0].x, v[0].y, v[0].z);
        gl.glVertex3d(v[1].x, v[1].y, v[1].z);
        gl.glVertex3d(v[2].x, v[2].y, v[2].z);
        gl.glEnd();

    }

    public void displayDebug(GLAutoDrawable drawable) {
        gl = drawable.getGL().getGL2();
        
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor4d(1, 1, 1, 0.5f);
        gl.glVertex3d(v[0].x, v[0].y, v[0].z);
        gl.glVertex3d(v[1].x, v[1].y, v[1].z);
        gl.glVertex3d(v[2].x, v[2].y, v[2].z);
        gl.glEnd();

        // Draw the vertex indices
        glut = new GLUT();
        gl.glColor4f(1, 1, 1, 1);
        for (int i = 0; i < 3; i++) {
            gl.glRasterPos3d(v[i].x, v[i].y, v[i].z); glut.glutBitmapString(GLUT.BITMAP_9_BY_15, "v" + i);
        }
        
    }

}
