package implicit.rendering.marching;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * Decomposes a cube into 6 tetrahedra.
 * @author piuze
 */
public class MarchingTetrahedron {
    private final Vector3d tmpV = new Vector3d();
    private final Triangle[] tmpTs = new Triangle[2];
    
    public void displayTest(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        Vertex v0 = new Vertex();
        Vertex v1 = new Vertex();
        Vertex v2 = new Vertex();
        Vertex v3 = new Vertex();

        // Test the 7 polygon cases
        GLUT glut = new GLUT();
        Tetrahedron t;
        ExtendedTriangle[] triangles;
        double offset;
        double dx = 1.5;
        double iso = 0;

        // Case 0001
        offset = 0;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = -1;v2.val = -1;v1.val = -1;v0.val = 1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(1, 0, 0); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);
        
        // Case 0010
        offset += dx;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = -1;v2.val = -1;v1.val = 1;v0.val = -1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(1, 1, 0); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);

        // Case 0100
        offset += dx;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = -1;v2.val = 1;v1.val = -1;v0.val = -1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(1, 1, 1); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);

        // Case 1000
        offset += dx;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = 1;v2.val = -1;v1.val = -1;v0.val = -1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(0, 1, 0); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);

        // Case 0011
        offset += dx;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = -1;v2.val = -1;v1.val = 1;v0.val = 1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(0, 1, 1); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);

        // Case 0101
        offset += dx;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = -1;v2.val = 1;v1.val = -1;v0.val = 1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(0, 0, 1); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);

        // Case 0110
        offset += dx;
        v0.set(0 + offset, 0, 0); v1.set(1 + offset, 0, 0);
        v2.set(0.5 + offset, 1, 0); v3.set(0.5 + offset, 0.5, 1);
        v3.val = -1;v2.val = 1;v1.val = 1;v0.val = -1;
        t = new Tetrahedron(v0, v1, v2, v3, false);
        triangles = triangulate(t, iso);
        gl.glColor3f(1, 0, 1); t.displayDebug(drawable);
        if (triangles != null) for (ExtendedTriangle tri : triangles) tri.displayDebug(drawable);

    }

    /**
     * Returns a simple polygonization of an implicit value by using
     * its evaluation at the vertices of a tetrahedra.
     * @param t
     * @param iso
     * @return
     */
    public ExtendedTriangle[] triangulate(Tetrahedron t, double iso) {
        
        int v0 = 0;
        int v1 = 1;
        int v2 = 2;
        int v3 = 3;

        // Case index uses 2^i for each vertex i
        // such that each vertex represents a bit 0 or 1
        // in the number 0000 -> v3v2v1v0
        int index = 0;
        if (t.v[v0].val < iso) index = index | 1;
        if (t.v[v1].val < iso) index = index | 2;
        if (t.v[v2].val < iso) index = index | 4;
        if (t.v[v3].val < iso) index = index | 8;

        /* Form the vertices of the triangles for each case */
        ExtendedTriangle[] tri = null;
        Vertex p0 = new Vertex();
        Vertex p1 = new Vertex();
        Vertex p2 = new Vertex();
        
        // 4-Bit indexing
//        System.out.println(Integer.toBinaryString(index));
        switch (index) {
        case 0x00:
        case 0x0F:
           break;
        case 0x0E:
        case 0x01:
           isoInterpolate(iso, t.v[v0], t.v[v1], p0);
           isoInterpolate(iso, t.v[v0], t.v[v2], p1);
           isoInterpolate(iso, t.v[v0], t.v[v3], p2);
           tri = new ExtendedTriangle[] { new ExtendedTriangle(p0, p1, p2, false, 1) };
           break;
        case 0x0D:
        case 0x02:
            isoInterpolate(iso, t.v[v1], t.v[v0], p0);
            isoInterpolate(iso, t.v[v1], t.v[v3], p1);
            isoInterpolate(iso, t.v[v1], t.v[v2], p2);
            tri = new ExtendedTriangle[] { new ExtendedTriangle(p0, p1, p2, false, 1) };
           break;
        case 0x0C:
        case 0x03:
            tri = new ExtendedTriangle[2];

            isoInterpolate(iso, t.v[v0], t.v[v3], p0);
            isoInterpolate(iso, t.v[v0], t.v[v2], p1);
            isoInterpolate(iso, t.v[v1], t.v[v3], p2);
            tri[0] = new ExtendedTriangle(p0, p1, p2, false, 1);

            isoInterpolate(iso, t.v[v1], t.v[v2], p0);
            tri[1] = new ExtendedTriangle(p2, p0, p1, false, -1);
           break;
        case 0x0B:
        case 0x04:
            isoInterpolate(iso, t.v[v2], t.v[v0], p0);
            isoInterpolate(iso, t.v[v2], t.v[v1], p1);
            isoInterpolate(iso, t.v[v2], t.v[v3], p2);
            tri = new ExtendedTriangle[] { new ExtendedTriangle(p0, p1, p2, false, 1) };
           break;
        case 0x0A:
        case 0x05:
            tri = new ExtendedTriangle[2];

            isoInterpolate(iso, t.v[v0], t.v[v1], p0);
            isoInterpolate(iso, t.v[v2], t.v[v3], p1);
            isoInterpolate(iso, t.v[v0], t.v[v3], p2);
            tri[0] = new ExtendedTriangle(p0, p1, p2, false, 1);

            isoInterpolate(iso, t.v[v1], t.v[v2], p2);
            tri[1] = new ExtendedTriangle(p0, p2, p1, false, 1);
           break;
        case 0x09:
        case 0x06:
            tri = new ExtendedTriangle[2];

            isoInterpolate(iso, t.v[v0], t.v[v1], p0);
            isoInterpolate(iso, t.v[v1], t.v[v3], p1);
            isoInterpolate(iso, t.v[v2], t.v[v3], p2);
            tri[0] = new ExtendedTriangle(p0, p1, p2, false, 1);

            isoInterpolate(iso, t.v[v0], t.v[v2], p1);
            tri[1] = new ExtendedTriangle(p0, p1, p2, false, -1);

           break;
        case 0x07:
        case 0x08:
            isoInterpolate(iso, t.v[v3], t.v[v0], p0);
            isoInterpolate(iso, t.v[v3], t.v[v2], p1);
            isoInterpolate(iso, t.v[v3], t.v[v1], p2);
            tri = new ExtendedTriangle[] { new ExtendedTriangle(p0, p1, p2, false, 1) };
           break;
        }

        return tri;
    }
    
    private final double epsilon = 1e-5;
    /**
     * Interpolates the location where the surface
     * crosses this edge with end points evaluated
     * to values f1 and f2;
     * @param iso 
     * @param v1 
     * @param v2 
     * @param target 
     * @return The position of the interpolant. 
     */
    public Point3d isoInterpolate(double iso, Vertex v1, Vertex v2, Vertex target) {
        target.val = iso;
        
        if (Math.abs(iso - v1.val) < epsilon) {
            target.set(v1);
        }
        if (Math.abs(iso - v2.val) < epsilon) {
            target.set(v2);
        }
        if (Math.abs(v2.val - v1.val) < epsilon) {
            target.set(v1);
        }
        
        // For now, simply do linear interpolation
        double f = (iso - v1.val)/(v2.val-v1.val);
        tmpV.sub(v2, v1);
        tmpV.scale(f);
        target.set(v1);
        target.add(tmpV);
        return target;
    }
}
