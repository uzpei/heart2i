package implicit.rendering.marching;

import implicit.implicit3d.functions.ImplicitFunction3D;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * Class representing a triangle, composed of
 * four Point3d and five Edge.
 * @author piuze
 */
public class Tetrahedron {
    
//        + 0
//        /|\
//       / | \
//      /  |  \
//     /   |   \
//    /    |    \
//   /     |     \
//  +-------------+ 1
// 3 \     |     /
//    \    |    /
//     \   |   /
//      \  |  /
//       \ | /
//        \|/
//         + 2

	
    /**
     * The four vertices of this tetrahedron.
     */
    public Vertex[] v = new Vertex[4];
    
    /**
     * The six edges of this tetrahedron
     */
    public Edge[] e = new Edge[6];


    /**
     * The three triangles making up this tetrahedron.
     */
    public Triangle[] triangles = new Triangle[3];
    
    /**
     * Builds a tetrahedron from four vertices. See header
     * for the vertex indices.
     * Vertices are passed and kepts by reference.
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
    public Tetrahedron(Vertex v0, Vertex v1, Vertex v2, Vertex v3) {

        v[0] = v0;
        v[1] = v1;
        v[2] = v2;
        v[3] = v3;

        e[0] = new Edge(v0, v1);
        e[1] = new Edge(v0, v2);
        e[2] = new Edge(v0, v3);
        e[3] = new Edge(v1, v2);
        e[4] = new Edge(v1, v3);
        e[5] = new Edge(v2, v3);
        
        triangles[0] = new Triangle(v[0], v[1], v[2]);
        triangles[1] = new Triangle(v[0], v[1], v[3]);
        triangles[2] = new Triangle(v[1], v[2], v[3]);
    }
    /**
     * Builds a tetrahedron from four vertices. See header
     * for the vertex indices.
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     * @param byRef Whether or not vertices should be kept by reference or by value.
     */
    public Tetrahedron(Vertex v0, Vertex v1, Vertex v2, Vertex v3, boolean byRef) {
        this(byRef ? v0 : new Vertex(v0), byRef ? v1 : new Vertex(v1), byRef ? v2 : new Vertex(v2), byRef ? v3 : new Vertex(v3));
    }

    
    /**
     * Evaluates a function at the vertices
     * of this tetrahedron.
     * @param function
     */
    public void evaluate(ImplicitFunction3D function) {
        for (int i = 0; i < 4; i++) {
            v[i].val = function.F(v[i]);
        }
    }
    
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glBegin(GL.GL_LINES);
        for (Edge edge : e) {
            gl.glVertex3d(edge.v1.x, edge.v1.y, edge.v1.z);
            gl.glVertex3d(edge.v2.x, edge.v2.y, edge.v2.z);
        }
        gl.glEnd();
    }

    private GLUT glut = new GLUT();
    private GL2 gl;
    public void displayDebug(GLAutoDrawable drawable) {
        display(drawable);
        
        // Draw the vertex indices
        gl = drawable.getGL().getGL2();
        gl.glColor3f(1, 1, 1);
        for (int i = 0; i < 4; i++) {
            gl.glRasterPos3d(v[i].x, v[i].y, v[i].z); glut.glutBitmapString(GLUT.BITMAP_9_BY_15, "v" + i + "(" + v[i].val + ")");
        }

    }
}
