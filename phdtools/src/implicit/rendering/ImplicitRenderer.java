package implicit.rendering;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;
import implicit.implicit3d.functions.ImplicitFunction3D;
import implicit.implicit3d.functions.MetaballFunction;
import implicit.rendering.marching.Cube;
import implicit.rendering.marching.ExtendedTriangle;
import implicit.rendering.marching.ImplicitGrid3D;
import implicit.rendering.marching.MarchingTetrahedron;
import implicit.rendering.marching.Vertex;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import tools.StopWatch;

/**
 * @author piuze
 */
public class ImplicitRenderer implements GLObject, Interactor {
    private JoglRenderer ev;

    private final ImplicitGrid3D grid;
    
    private ImplicitFunction3D function;

    /**
     * Entry point for application
     * @param args
     */
    public static void main(String[] args) {
        new ImplicitRenderer(null, true);
    }

    private final Dimension winsize = new Dimension(800, 800);

    private final BooleanParameter drawTestCases = new BooleanParameter("test mode", false);

    private final boolean ownCanvas;
    
    /**
     * Creates the application / scene instance
     * @param functions2 
     * @param ownCanvas If it should display in its own canvas.
     */
    public ImplicitRenderer(ImplicitFunction3D functions2, boolean ownCanvas) {
        this.ownCanvas = ownCanvas;
        
        if (functions2 == null) {
            function = new MetaballFunction();
        }
        else {
            function = functions2;
        }
        
        grid = new ImplicitGrid3D();
        
        if (ownCanvas) {
            ev = new JoglRenderer("Implicit Surface Visualization");
            ev.addInteractor(this);
            ev.start(this);
        }
    }

    public ImplicitGrid3D getImplicitGrid() {
    	return grid;
    }
    
    @Override
	public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
    }

    private final StopWatch swFPS = new StopWatch("FPS");
    
    private double xrot = 0, yrot = 0;

	private final double zrot = 0;

	private double drot = 0.01;
    private double xrotc = 0, yrotc = 0;

	private final double zrotc = 0;
    
    @Override
	public void display(GLAutoDrawable drawable) {
        swFPS.start();

        GL2 gl = drawable.getGL().getGL2();

        if (ownCanvas) {
            // Display text info        
            JoglTextRenderer.printTextLines(drawable, toString());
        }

        gl.glDisable(GL2.GL_LIGHTING);
        gl.glDisable(GL.GL_CULL_FACE );

        gl.glPushMatrix();
        xrotc = xrotc + xrot % 360;
        yrotc = yrotc + yrot % 360;
        drot = 0.25;
        gl.glRotated(-xrotc, 1, 0, 0);
        gl.glRotated(-yrotc, 0, 1, 0);
        
        grid.display(drawable, function);

        gl.glPopMatrix();

        function.display(drawable);
        
        if (drawTestCases.getValue()) {
            Cube c = new Cube(new Point3d(0, -1, 0), 2);
            c.display(drawable, true);
            ExtendedTriangle et = new ExtendedTriangle(new Vertex(3, -2, 0),
                    new Vertex(4, -1, 0), new Vertex(5, -2, 0));
            et.displayDebug(drawable);
            MarchingTetrahedron mt = new MarchingTetrahedron();
            mt.displayTest(drawable);
        }

        // Refresh the window size
        winsize.height = drawable.getHeight();
        winsize.width = drawable.getWidth();

        swFPS.stop();
    }

    @Override
	public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Implicit Renderer"));

        if (ownCanvas) {
            if (function.getControls() != null)
                    vfp.add(function.getControls());
        }
        vfp.add(grid.getControls());
        vfp.add(drawTestCases.getControls());
        
        return vfp.getPanel();
    }


    private int xcurrent, ycurrent;
    private boolean controlDown, shiftDown;
    @Override
	public void attach(Component component) {
        component.addMouseMotionListener(new MouseMotionListener() {

            @Override
			public void mouseDragged(MouseEvent e) {
                xcurrent = e.getPoint().x;
                ycurrent = e.getPoint().y;
            }

            @Override
			public void mouseMoved(MouseEvent e) {
                xcurrent = e.getPoint().x;
                ycurrent = e.getPoint().y;

            }
        });
        component.addMouseListener(new MouseListener() {

            @Override
			public void mouseClicked(MouseEvent e) {
                // do nothing
            }

            @Override
			public void mouseEntered(MouseEvent e) {
            }

            @Override
			public void mouseExited(MouseEvent e) {
            }

            @Override
			public void mousePressed(MouseEvent e) {
            }

            @Override
			public void mouseReleased(MouseEvent e) {
            }
        });
        component.addKeyListener(new KeyAdapter() {

            @Override
			public void keyPressed(KeyEvent e) {
                controlDown = e.isControlDown();
                shiftDown = e.isShiftDown();

                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    // TODO
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    xrot += drot;
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    xrot -= drot;
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    yrot += drot;
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    yrot -= drot;
                } else if (e.getKeyCode() == KeyEvent.VK_R) {
                    xrotc = 0;
                    yrotc = 0;
                    xrot = 0;
                    yrot = 0;
                } else if (e.getKeyCode() == KeyEvent.VK_S) {
                    // TODO
                }
            }
        });
    }

    public JoglRenderer getViewer() {
        return ev;
    }
    
    @Override
    public String toString() {
        String text = ""
            + "\nRendering " + swFPS.toString()
            + "\nmarching cells = " + grid.getCellCount()
            + "\n" + grid.swCreate 
            ;

        return text;
    }

    @Override
    public String getName() {
        return "Implicit Surface";
    }

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
}
