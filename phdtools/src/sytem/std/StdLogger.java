package sytem.std;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;

import tools.SimpleTimer;
import tools.TextToolset;

public class StdLogger {
	public PrintStream stdout;
	public PrintStream stderr;
	private StdHandler uiHandler;
	
	public StdLogger() {
		boolean toFile = false;
		boolean toStream = true;
		
		// initialize logging to go to rolling log file
        LogManager logManager = LogManager.getLogManager();
        logManager.reset();

        if (toFile) {
            // Create the log file
            // log file max size 10K, 3 rolling files, append-on-open
            Handler fileHandler = null;
			try {
				fileHandler = new FileHandler("log.txt", 10000, 3, true);
	            fileHandler.setFormatter(new SimpleFormatter());
	            Logger.getLogger("").addHandler(fileHandler);
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        if (toStream) {
        	uiHandler = new StdHandler(StdOutErrLevel.STDOUT, StdOutErrLevel.STDERR);
            Logger.getLogger("").addHandler(uiHandler);
        }
        
        // preserve old stdout/stderr streams in case they might be useful      
        stdout = System.out;                                        
        stderr = System.err;                                        

        // now rebind stdout/stderr to logger                                   
        LoggingOutputStream los;                                                

        final Logger loggerStd = Logger.getLogger("stdout");                                    
        los = new LoggingOutputStream(loggerStd, StdOutErrLevel.STDOUT);           
        System.setOut(new PrintStream(los, true));                              
        
        final Logger loggerErr = Logger.getLogger("stderr");                                    
        los= new LoggingOutputStream(loggerErr, StdOutErrLevel.STDERR);            
        System.setErr(new PrintStream(los, true));                              
	}
	
	public JPanel getControls() {
		return uiHandler.getControls();
	}
	
	public class StdHandler extends Handler {
		private List<String> log = new LinkedList<String>();
//		private JFrame frame;
		private JTextPane textPane;
		private Level lvlOut;
		private Level lvlErr;
		private StyledDocument doc;
		private SimpleAttributeSet styleError = new SimpleAttributeSet();
		private SimpleAttributeSet styleOut = new SimpleAttributeSet();
		private final static int DEFAULT_TAB_SIZE = 20;
		private boolean mousePressed = false;
		private JPanel contentPanel = new JPanel();
		private Dimension dim = new Dimension(300, 200);
		private final Color TOGGLECOLOR1 = Color.WHITE;
		private final Color TOGGLECOLOR2 = Color.GRAY;
		private boolean toggle = false;
		
		public StdHandler(Level out, Level err) {
			super();
		
			this.lvlOut = out;
			this.lvlErr = err;
            super.setFormatter(new SimpleFormatter());

            // Set logger styles
    		StyleConstants.setForeground(styleError, Color.RED);
    		StyleConstants.setBackground(styleError, Color.BLACK);
    		StyleConstants.setBold(styleError, true);
			StyleConstants.setFontFamily(styleError, "Monospaced");
			StyleConstants.setFontSize(styleError, 10);

    		StyleConstants.setForeground(styleOut, TOGGLECOLOR1);
    		StyleConstants.setBackground(styleOut, Color.BLACK);
    		StyleConstants.setBold(styleOut, false);
			StyleConstants.setFontFamily(styleOut, "Monospaced");
			StyleConstants.setFontSize(styleOut, 10);

    		// Set frame dimension
    		// Create text pane
    		textPane = new JTextPane();
//			textPane.setMaximumSize(dim);
			textPane.setPreferredSize(dim);
			textPane.setEditable(false);
			textPane.setBackground(Color.BLACK);
			
			// Set style
			doc = textPane.getStyledDocument();

			// This doesn't work for styled documents
//   		    doc.putProperty(PlainDocument.tabSizeAttribute, 2);
			StyleContext sc = StyleContext.getDefaultStyleContext();
			TabSet tabs = new TabSet(new TabStop[] { new TabStop(DEFAULT_TAB_SIZE) });
			AttributeSet paraSet = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.TabSet, tabs);
			textPane.setParagraphAttributes(paraSet, false);
			
			// Set init text
			SimpleAttributeSet initMessage = new SimpleAttributeSet();
			StyleConstants.setForeground(initMessage, Color.WHITE);
			StyleConstants.setBold(initMessage, true);
			StyleConstants.setItalic(initMessage, true);
			StyleConstants.setFontFamily(initMessage, "Monospaced");
			try {
				String filling = "*";
				String imsg = filling + "\t" + "Logger initialized." + "\t" + filling;
				String lmsg = "";
				lmsg += TextToolset.fill(filling, imsg.length()) + "\n";
				lmsg += imsg + "\n";
				lmsg += TextToolset.fill(filling, imsg.length()) + "\n";
				
				doc.insertString(doc.getLength(), lmsg, initMessage);
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			contentPanel.setLayout(new BorderLayout());
	        contentPanel.setPreferredSize(dim);
//			contentPanel.add(textPane, BorderLayout.PAGE_START);
			
			final JScrollPane jsp = new JScrollPane(textPane);
			jsp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "System I/O Logger", TitledBorder.LEFT, TitledBorder.CENTER));
			
	        jsp.getVerticalScrollBar().setUnitIncrement(10);
	        
	        MouseAdapter mousePressedAdapter = new MouseAdapter() {
	        	@Override
	        	public void mousePressed(MouseEvent e) {
	        		mousePressed = true;
	        	}
	        	@Override
	        	public void mouseReleased(MouseEvent e) {
	        		mousePressed = false;
	        	}
			};
			
	        // Prevent adjustment of the caret when scrolling with mouse
	        jsp.getVerticalScrollBar().addMouseListener(mousePressedAdapter);
	        contentPanel.addMouseListener(mousePressedAdapter);
	        textPane.addMouseListener(mousePressedAdapter);
	        

//			frame.add(contentPanel);
//	        frame.add(jsp);
	        contentPanel.add(jsp, BorderLayout.CENTER);
//	        contentPanel.setMaximumSize(dim);
		}
		
		public JPanel getControls() {
			return contentPanel;
		}
		
		@Override
		public void close() throws SecurityException {
		}

		@Override
		public void flush() {
		}

		private SimpleTimer timer = new SimpleTimer();
		
		@Override
		public void publish(LogRecord record) {
			log.add(record.getMessage());

			// Format message
			String logTxt = record.getMessage() + "\n";
			
			try {
				if (record.getLevel().getName() == lvlOut.getName()) {
		    		
					StyleConstants.setForeground(styleOut, toggle ? TOGGLECOLOR1 : TOGGLECOLOR2);
					toggle = !toggle;
		    		
					doc.insertString(doc.getLength(), timer.tick_s() + " | " + logTxt, styleOut);
					scrollDown();
					stdout.println(record.getMessage());
				}
				else if (record.getLevel().getName() == lvlErr.getName()) {
					logTxt = "\t" + "PRIORITY: " + logTxt;
					doc.insertString(doc.getLength(), logTxt, styleError);
					scrollDown();
					stderr.println(record.getMessage());
				}
				else {
					stdout.print(logTxt);
				}
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// Parse the level
		}
		
		private void scrollDown() {
			if (!mousePressed) {
//				SwingUtilities.invokeLater(new Runnable() {
//					
//					@Override
//					public void run() {
//						textPane.setCaretPosition(doc.getLength());
//					}
//				});
				textPane.setCaretPosition(doc.getLength());
			}
		}
	}

	public static void main(String[] args) throws SecurityException, IOException {
		StdLogger stdOut = new StdLogger();
		
		    // Create frame
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dim = new Dimension(500,500);
		frame.setPreferredSize(dim);
		frame.setSize(dim);
        frame.add(stdOut.getControls());

		// Show UI
		frame.setUndecorated(false);
		frame.setVisible(true);

		
        // now log a message using a normal logger
        Logger logger = Logger.getLogger("test");
        logger.info("This is a test log message");

        // now show stderr stack trace going to logger
        try {
            throw new RuntimeException("Test exception");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // and output on the original stdout
//        stdout.println("Hello on old stdout");

        // Launch thread to print info
        new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 1000; i++) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					System.out.println(i + ": " + System.nanoTime());
					System.err.println(i + ": " + i + System.nanoTime());
				}
				
				// Then try to read the stream directly
			}
		}).start();
	}
	
}
