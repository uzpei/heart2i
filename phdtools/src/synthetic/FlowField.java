package synthetic;

import javax.vecmath.Vector3d;

public interface FlowField {
	public Vector3d getOrientation(double x, double y, double z);
}
