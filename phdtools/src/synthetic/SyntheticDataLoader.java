package synthetic;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Plane;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.ParameterListener;

public class SyntheticDataLoader implements GLObject, Interactor {
	public static void main(String[] args) {
		new SyntheticDataLoader(new GLViewerConfiguration());
	}

	private BooleanParameter displayWorld = new BooleanParameter(
			"display world", false);

	private DoubleParameter thetal = new DoubleParameter("theta left", Math.PI / 4, 0, Math.PI);
	private DoubleParameter thetar = new DoubleParameter("theta right", Math.PI / 4, 0, Math.PI);
	private DoubleParameter maxdist = new DoubleParameter("max dist", 10, 0, 100);
	private DoubleParameter splaymag = new DoubleParameter("splay magnitude", 0, -2, 10);
	private DoubleParameter noise = new DoubleParameter("directional noise magnitude", 1e-1, 0, 1);
	
	private DoubleParameter direction = new DoubleParameter("direction (rads)", 0, -Math.PI / 2, Math.PI / 2);
	
	private DoubleParameterPoint3d F = new DoubleParameterPoint3d("fiber location", new Point3d(0, 0.5, 0), new Point3d(0, 0, 0), new Point3d(1, 1, 0));
	
	private BooleanParameter trace = new BooleanParameter("trace", false);
	private BooleanParameter drawfield = new BooleanParameter("draw field", true);
	
	private Plane plane;

	private JoglRenderer ev;

	public SyntheticDataLoader(GLViewerConfiguration config) {

		// Offset the plane by half its size
		double size = 25;
		Point3d x0 = new Point3d(0, 0, 0);
		Vector3d n = new Vector3d(0, 0, 1);
		plane = new Plane(x0, n);
		plane.setSize(size, size);
		plane.setDrawGrid(true);

		Dimension winsize = new Dimension(800, 800);
		ev = new JoglRenderer("", this, new Dimension(winsize), new Dimension(
				650, winsize.height + 90), true, config);
		ev.addInteractor(this);
		ev.start();
		ev.getRoom().setVisible(false);

	}

	private Vector3d q = new Vector3d();
	private Random rand = new Random();
	
	private Vector3d getOrientation(SyntheticVoxel p, Point3d F, Vector3d v, boolean cut) {
		
		q.sub(p.p3d, F);

		if (cut && q.length() > maxdist.getValue())
			return null;

		q.normalize();

		// Z component of q x v
		double crossz = q.x * v.y - v.x * q.y;
		double alpha = Math.acos(q.dot(v)) * Math.signum(crossz);

		if (!cut || (alpha > -thetar.getValue() && alpha < thetal.getValue())) {
//			q.scale(2);
//			q.scaleAdd(splaymag.getValue(), v, q);
//			q.normalize();

			q.add(p.noise);
			q.normalize();

//			q.y += Math.signum(q.y) * q.length() * splaymag.getValue();
			
			
			q.normalize();
			
			return q;
		}
		else return null;
	}
	
	private List<SyntheticVoxel> voxels;
	
	private Vector3d getFiberDirection() {
		Vector3d v = new Vector3d();
		v.x = Math.cos(direction.getValue());
		v.y = Math.sin(direction.getValue());
		v.z = 0;
		
		return v;
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		if (displayWorld.getValue())
			WorldAxis.display(gl);

		gl.glPushMatrix();
		gl.glTranslated(0, 0, -0.1);
		plane.display(drawable);
		gl.glPopMatrix();
		
		if (plane.hasChanged()) {
			voxels = createVoxels(plane.getGrid());
		}
		
		gl.glDisable(GL2.GL_LIGHTING);

		// TODO: change this through the UI
		// Location of focal plane
		Point3d F = new Point3d(this.F.getPoint3d());
		F.x = (-0.5 + F.x) * plane.getSize()[0];
		F.y = (-0.5 + F.y) * plane.getSize()[1];

		// Fiber direction
		Vector3d v = getFiberDirection();
		
		// Draw fiber info
		gl.glColor4d(1, 1, 0, 1);
		gl.glPointSize(10f);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex3d(F.x, F.y, F.z);
		gl.glEnd();
		
		v.normalize();
		v.scale(plane.getSmallestSide());
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex3d(F.x, F.y, F.z);
		gl.glVertex3d(F.x + v.x, F.y + v.y, F.z + v.z);
		gl.glEnd();
		v.normalize();
				
		gl.glColor4d(1, 0, 0, 1);
		gl.glPointSize(4f);
		
		/*
		 * p is the current voxel
		 * q is the vector from the voxel to the focal point
		 * F is the focal point
		 * v is the direction at the focal point
		 */
		if (drawfield.getValue()) {
			double zo = 0.1;
			gl.glLineWidth(1.0f);
			gl.glBegin(GL2.GL_LINES);
			for (SyntheticVoxel p : voxels) {
				Vector3d q = getOrientation(p, F, v, true);
				
				if (q != null) {
					
//				gl.glBegin(GL2.GL_POINTS);
//				gl.glVertex3d(p.x, p.y, p.z);
//				gl.glEnd();
					gl.glVertex3d(p.p3d.x, p.p3d.y, p.p3d.z + zo);
					gl.glVertex3d(p.p3d.x + q.x, p.p3d.y + q.y, p.p3d.z + q.z + zo);
				}
			}
			gl.glEnd();
		}

		// This is slow... the list-search is suboptimal
		if (trace.getValue()) {
			// Trace some field lines by sampling perpendicularly to fiber direction
			int N = plane.getSampleCount();
			double dv = plane.getSmallestSide() / (N);

			Vector3d n = new Vector3d(-v.y, v.x, 0);
			n.normalize();
			gl.glColor3d(0, 0, 1);
			gl.glLineWidth(3.0f);
			double tight = 3;
			int numstreams = (int) (N / 2.0);
			for (int i = 0; i <= numstreams; i++) {
				// Origin of streamline
				Point3d p = new Point3d();
//				p.scaleAdd((i - N/2) * dv * tightness, n, F);
//				p.scaleAdd(tightness, v, p);
				double th = i / ((double) numstreams) * Math.PI - Math.PI / 2 + direction.getValue();
				p.x = dv * tight * Math.cos(th);
				p.y = dv * tight * Math.sin(th);
				p.add(F);
				p.scaleAdd(dv, v, p);

				double color = 1 - Math.abs(i - (numstreams / 2.0)) / (numstreams / 2.0);
				gl.glColor4d(0, 0, 0, color);
				
				gl.glBegin(GL2.GL_POINTS);
				gl.glVertex3d(p.x, p.y, p.z);
				gl.glEnd();
				
				// Trace a new field line at p
				gl.glBegin(GL2.GL_LINE_STRIP);
				gl.glVertex3d(p.x, p.y, p.z);
				for (int j = 0; j < N; j++) {
					SyntheticVoxel voxel = searchVoxels(voxels, p);
					
					if (voxel == null) break;
					
					Vector3d q = getOrientation(voxel, F, v, false);
					
					p.scaleAdd(dv, q, p);
					gl.glVertex3d(p.x, p.y, p.z);
				}
				gl.glEnd();
			}
		}

		gl.glLineWidth(1.0f);

		gl.glEnable(GL2.GL_LIGHTING);

		// Draw text info
		JoglTextRenderer textRenderer = ev.getTextRenderer();
		textRenderer.setColor(0.3f, 0.9f, 0f, 1f);
		textRenderer.addLine("Voxel count = " + voxels.size() + "");
		textRenderer.drawBuffer(drawable, 10, 10);
	}

	private List<SyntheticVoxel> createVoxels(List<Point3d> pts) {
		List<SyntheticVoxel>voxels = new LinkedList<SyntheticVoxel>();

		Point3d F = new Point3d(this.F.getPoint3d());
		F.x = (-0.5 + F.x) * plane.getSize()[0];
		F.y = (-0.5 + F.y) * plane.getSize()[1];

		// Fiber direction
		Vector3d v = getFiberDirection();

		for (Point3d p : pts) {
			
			Vector3d noiseq = new Vector3d();
			
			if (noise.isChecked()) {
				// Add noise
				double dnoise = noise.getValue()*rand.nextGaussian();
				noiseq.x = Math.cos(dnoise);
				noiseq.y = Math.sin(dnoise);
			}
			
			SyntheticVoxel voxel = new SyntheticVoxel(new Point3i(round(p.x), round(p.y), round(p.z)), noiseq, 1);
			voxel.p3d.x = p.x;
			voxel.p3d.y = p.y;
			
			Vector3d q = getOrientation(voxel, F, v, true);

			if (q != null) voxels.add(voxel);
		}
		
		return voxels;
	}
	
	private int round(double d) {
		return (int)Math.round(d);
	}

	private SyntheticVoxel searchVoxels(List<SyntheticVoxel> voxels, Point3d p) {
		SyntheticVoxel closest = null;
		double cdist = Double.MAX_VALUE;
		
		for (SyntheticVoxel voxel : voxels) {
			double dist = voxel.p3d.distance(p);
			
			if (dist < cdist) {
				cdist = dist;
				closest = voxel;
			}
		}
		
		return closest;
	}

	@Override
	public JPanel getControls() {

		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(displayWorld.getControls());
		vfp.add(plane.getControls());
		vfp.add(direction.getSliderControls());
		vfp.add(thetal.getSliderControls());
		vfp.add(thetar.getSliderControls());
		vfp.add(maxdist.getSliderControls());
		vfp.add(splaymag.getSliderControls());
		vfp.add(noise.getSliderControlsExtended());
		
		ParameterListener cl = new ParameterListener() {
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				voxels = createVoxels(plane.getGrid());
			}
		};

		direction.addParameterListener(cl);
		thetal.addParameterListener(cl);
		thetar.addParameterListener(cl);
		maxdist.addParameterListener(cl);
		splaymag.addParameterListener(cl);
		thetal.addParameterListener(cl);
		noise.addParameterListener(cl);
		
		vfp.add(trace.getControls());
		vfp.add(drawfield.getControls());

		vfp.add(F.getControls());
		
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_F) {
				}
			}
		});

		component.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
			}
		});
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		new SyntheticDataLoader(config);
	}

}
