package synthetic;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Tuple3i;
import javax.vecmath.Vector3d;

public class SyntheticVoxel extends Tuple3i {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4317919979747735860L;

	public Vector3d noise;
	
	public double anisotropy = 1;
	
	/**
	 * Floating point coordinate.
	 */
	public Point3d p3d = new Point3d();
	
	public SyntheticVoxel(Point3i p, Vector3d noise, double anisotropy) {
		super(p);
		
		p3d.x = p.x;
		p3d.y = p.y;
		p3d.z = p.z;
		
		this.noise = noise;
		this.anisotropy = anisotropy;
	}
}
