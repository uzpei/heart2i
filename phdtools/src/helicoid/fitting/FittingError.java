package helicoid.fitting;

import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.MultivariateRealFunction;
import org.apache.commons.math.optimization.RealConvergenceChecker;
import org.apache.commons.math.optimization.RealPointValuePair;

import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.CoordinateFrameSample;
import tools.frame.OrientationFrame.SamplingStyle;
import app.heart.FirstOrderExtrapolationFittingErrorMeasure;
import app.pami2013.FittingErrorMeasure;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;

public class FittingError implements MultivariateRealFunction,
		RealConvergenceChecker {
	
	private FittingErrorMeasure errorMeasure = null;

	private IntDiscreteParameter neighborhoodFitSamples = new IntDiscreteParameter("fit neighborhood size", 3, new int[] { 1, 3, 5, 7, 9 });
	
	private IntDiscreteParameter neighborhoodErrorSamples = new IntDiscreteParameter("error neighborhood size", 3, new int[] { 1, 3, 5, 7, 9 });
	
	private IntParameter rots = new IntParameter("frame rotations", 1, 1, 20);

	private IntParameter iterations = new IntParameter("iterations", 100, 0,
			1000);

	private DoubleParameter epsilon = new DoubleParameter("error epsilon",
			1e-5, 0, 1);

	private DoubleParameter egrad = new DoubleParameter(
			"error gradient epsilon", 1e-6, 0, 0.1);
	private DoubleParameter lambda = new DoubleParameter("regularization",
			0.001, 0, 5);

	private double scale = 1;

	
	private DoubleParameter scalep = new DoubleParameter(
			"volumetric scale (mm)", 1, 0, 1);

	private int cachedNeighborhoodFitSamples;

	private int cachedNeighborhoodErrorSamples;

	private int cachedRots;

	private int cachedIterations;

	private double cachedEpsilon;

	private double cachedGrad;

	private boolean verbose = false;

	private List<Point3i> voxelNeighborhood;
	
	private CartanParameter currentParameter;
	
	private void updateCache() {
		cachedNeighborhoodFitSamples = neighborhoodFitSamples.getValue();
		cachedNeighborhoodErrorSamples = neighborhoodErrorSamples.getValue();
		
		cachedRots = rots.getValue();
		cachedIterations = iterations.getValue();
		cachedEpsilon = epsilon.getValue();
		cachedGrad = egrad.getValue();
		errorMeasure.setRegularizationLambda(lambda.getValue());
		scale = scalep.getValue();

		errorMeasure.setNeighborhoodCount(SamplingStyle.getCount(samplingStyle, cachedNeighborhoodFitSamples));
	}

	private void addListeners() {
		ParameterListener l = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateCache();
			}
		};

		neighborhoodFitSamples.addParameterListener(l);
		neighborhoodErrorSamples.addParameterListener(l);
		rots.addParameterListener(l);
		iterations.addParameterListener(l);
		epsilon.addParameterListener(l);
		egrad.addParameterListener(l);
		lambda.addParameterListener(l);
		samplingStyle.addParameterListener(l);
		scalep.addParameterListener(l);
		updateCache();
		
		// Sampling
		ParameterListener sl = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateFrameSampling();
			}
		};
		
		neighborhoodFitSamples.addParameterListener(sl);
		samplingStyle.addParameterListener(sl);
	}

	private Fittable fittable;
	private Point3i currentSpatialPoint;

	private CoordinateFrame frame = new CoordinateFrame();
	private CoordinateFrame frameInv = new CoordinateFrame();

	/**
	 * First compute forward neighbors This is also the sampling style used for
	 * computing local frames in the fittable object.
	 */

	private SamplingStyle samplingStyle = SamplingStyle.NP_NP_NP;

	private Vector3d vGHM = new Vector3d();

	private Vector3d vData = new Vector3d();

	private Point3d pti = new Point3d();
	
	private Matrix3d m3d = new Matrix3d();

	private Vector3d v3d = new Vector3d();
	
	public FittingError(Fittable fittable) {
		this.fittable = fittable;
		
		// TODO: allow other error measures
		errorMeasure = new FirstOrderExtrapolationFittingErrorMeasure();

		addListeners();
	}

	public FittingError() {
		this(null);
	}
	public void setFittable(Fittable fittable) {
		this.fittable = fittable;
	}

	public void set(FittingError other) {
		this.neighborhoodFitSamples = other.neighborhoodFitSamples;
		this.neighborhoodErrorSamples = other.neighborhoodErrorSamples;
		this.rots = other.rots;
		this.iterations = other.iterations;
		this.epsilon = other.epsilon;
		this.egrad = other.egrad;
		this.lambda = other.lambda;
		this.fittable = other.fittable;
		this.verbose = other.verbose;
		this.scalep = other.scalep;
		this.scale = other.scale;
		
		this.fittable = other.fittable;
		
		// Don't assign by reference. We will be using this extensively and need fast lookup time.
//		this.voxelNeighborhood = other.voxelNeighborhood;
		this.voxelNeighborhood = new LinkedList<Point3i>();
		this.voxelNeighborhood.addAll(other.voxelNeighborhood);

		addListeners();
	}

	private Matrix4d rotationInv = new Matrix4d();
	
	public void init(Point3i p, CartanModel model) {
		currentSpatialPoint = p;
		
		currentParameter = new CartanParameter(model);
		
		// Get the coordinate frames at this voxel and extract rotation
		fittable.getCoordinateFrame(currentSpatialPoint, frameInv);
		CoordinateFrame.invert(frameInv.getFrame(), m3d, v3d);
		frameInv.getRotation(rotationInv);
		errorMeasure.setRotation(rotationInv);
		
		errorMeasure.init(fittable, voxelNeighborhood);
	}

	@Override
	public String toString() {
		String s = "";
		s += "Fit neighborhood size = " + cachedNeighborhoodFitSamples + "\n";
		s += "Error neighborhood size = " + cachedNeighborhoodErrorSamples + "\n";
		s += "Frame rotations = " + cachedRots + "\n";
		s += "Max iterations = " + cachedIterations + "\n";
		s += "Error epsilon = " + cachedEpsilon + "\n";
		s += "Error delta epsilon = " + cachedGrad + "\n";
		s += "Regularization constant = " + errorMeasure.getRegularizationLambda() + "\n";
		s += "Scale = " + scale + "\n";

		return s;
	}
	
	public void updateFrameSampling() {
		if (fittable == null) return;
		
		// System.out.println("Updating sampling... [" + getNumSamples() +
		// "] sample for [" + samplingStyle.toString() + "]");

		int numSamples = getNeighborhoodFitSize();

		int[] axisSamples = new int[] { numSamples, numSamples, numSamples };

		fittable.precomputeCoordinateFrames(getNumRots(), axisSamples,
				samplingStyle);
		
		CoordinateFrameSample sample = new CoordinateFrameSample();
		sample.presampleInteger(axisSamples, samplingStyle, true);
		voxelNeighborhood = sample.getIntegerSampling();
	}

	/**
	 * Evaluate the (unregularized) error in the neighborhood previously specified by setNeighborhoodErrorSize().
	 * @param parameter
	 * @param p
	 * @return
	 */
	public double evaluateUnregularizedError(CartanParameter parameter, Point3i p) {
		double[] point = parameter.getParameter();
		
		// Back up previous state
		List<Point3i> ntemp = voxelNeighborhood;		
		
		// Compute new error neighborhood
		int numSamples = getNeighborhoodErrorSize();
		CoordinateFrameSample sample = new CoordinateFrameSample();
		sample.presampleInteger(new int[] { numSamples, numSamples, numSamples }, samplingStyle, true);
		voxelNeighborhood = sample.getIntegerSampling();
		
		try {
			// Save enforce
			boolean tempEnforced = errorMeasure.isNeighborhoodSizeEnforced();
			errorMeasure.setNeighborhoodSizeEnforced(false);

			// Compute the (unregularized) error
			double error = value(point) - errorMeasure.getRegularizationLambda() * parameter.length();
			
			// Restore enforce
			errorMeasure.setNeighborhoodSizeEnforced(tempEnforced);
			
			return error ;
		}
		catch (Exception e) {
			e.printStackTrace();

			// If we get to this point then an error has occured, return NaN.
			return Double.NaN;
		}
		finally {
			// Restore changes before returning
			voxelNeighborhood = ntemp;
		}
	}
	
	@Override
	public double value(double[] optimizationPoint) throws FunctionEvaluationException,
			IllegalArgumentException {

		currentParameter.setParametersMasked(optimizationPoint);
		return errorMeasure.getError(currentSpatialPoint, currentParameter);
	}

	@Override
	public boolean converged(int iteration, RealPointValuePair previous,
			RealPointValuePair current) {

		if (previous == null)
			return false;

		double cv = current.getValue();
		double pv = previous.getValue();

		// Stop if one of the following occurs:
		// 1) The error falls below a threshold
		// 2) The error gradient between two successive iterations falls below a
		// threshold (i.e. a local minima is reached)
		// 3) The maximum number of iterations was reached

		// System.out.println(cv + " for HP = " +
		// Arrays.toString(current.getPoint()));

		if (cv <= cachedEpsilon || Math.abs(pv - cv) <= cachedGrad
				|| iteration > cachedIterations) {
			return true;
		}

		return false;
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Error Setting"));

		vfp.add(scalep.getSliderControls());

		vfp.add(neighborhoodFitSamples.getSliderControls());
		vfp.add(neighborhoodErrorSamples.getSliderControls());
		vfp.add(rots.getSliderControls());

		vfp.add(samplingStyle.getControls());
		vfp.add(iterations.getSliderControls());
		vfp.add(epsilon.getSliderControls());
		vfp.add(egrad.getSliderControls());
		vfp.add(lambda.getSliderControls());

		return vfp.getPanel();
	}

	public int getIterations() {
		return cachedIterations;
	}

	public int getNeighborhoodFitSize() {
		return cachedNeighborhoodFitSamples;
	}

	public int getNeighborhoodErrorSize() {
		return cachedNeighborhoodErrorSamples;
	}
	
	public int getNumRots() {
		return cachedRots;
	}

	public SamplingStyle getSamplingStyle() {
		return samplingStyle;
	}

	public double getGradientEpsilon() {
		return cachedGrad;
	}

	public double getLambda() {
		return errorMeasure.getRegularizationLambda();
	}

	public double getEpsilon() {
		return cachedEpsilon;
	}

	public void setVerbose(boolean b) {
		verbose = b;
	}

	/**
	 * A voxel neighborhood of this size will be used when optimizing for curvature parameters.
	 * Set the neighborhood (cubic) size^3.
	 * Allowed values are: 1, 3, 5, 7, 9
	 * @param size
	 */
	public void setFitNeighborhoodSize(int size) {
		neighborhoodFitSamples.setValue(size);
	}

	/**
	 * A voxel neighborhood of this size will be used for reporting the error of fit.
	 * @param size
	 */
	public void setErrorNeighborhoodSize(int size) {
		neighborhoodErrorSamples.setValue(size);
	}

	/**
	 * Set the number of optimization iterations.
	 * @param iterations
	 */
	public void setIterations(int iterations) {
		this.iterations.setValue(iterations);
	}
}
