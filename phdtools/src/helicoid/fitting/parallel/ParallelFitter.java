package helicoid.fitting.parallel;

import helicoid.fitting.FittableOrientationImage;
import helicoid.fitting.FittableRunner;
import helicoid.fitting.FittingError;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3i;

import swing.component.VerticalFlowPanel;
import swing.parameters.IntParameter;
import tools.SimpleTimer;
import tools.TextToolset;
import tools.loader.LineWriter;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.cartan.CartanParameter.CartanModel;
import extension.matlab.MatlabIO;

/**
 * @author piuze
 */
public class ParallelFitter {

    private List<FittingRunnable> runnables;
    private List<Thread> fittingThreads;
    
    private double runningTime = 0;

    public boolean initializing = false;
    private List<CartanParameterNode> allFits = new LinkedList<CartanParameterNode>();
    private boolean collected = true;
    
    private IntParameter threadCount = new IntParameter("multithreading x", 2, 1, 128);
    
    private final static int DEFAULT_THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    
    private SimpleTimer timer = new SimpleTimer();

    private FittableRunner defaultRunner;
    
    /**
     * Create a parallel fitter with the specified number of threads.
     * @param numcores
     */
    public ParallelFitter() {
        threadCount.setValue(DEFAULT_THREAD_COUNT);
        
        runnables = new LinkedList<FittingRunnable>();
        fittingThreads = new LinkedList<Thread>();

        defaultRunner = new FittableRunner(CartanModel.FULL);
    }
    
    public void setNumthreads(int count) {
    	threadCount.setValue(count);
    }
    
    private void initRunnable(FittableOrientationImage volume) {
    	currentVolume = volume;
       	defaultRunner.setFittable(volume);
    }

    public void runExport(FittableOrientationImage volume) {
    	runExport(volume, false);
    }
    public void runExport(final FittableOrientationImage volume, boolean verbose) {

    	run(volume, verbose);

//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				waitAndStore(volume);
//			}
//		}).start();
		waitAndStore();
    }
    
    private void waitAndStore() {
    	// Loop waiting time in ms
    	final int msTimeout = 500;
    	timer.tick();
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumFractionDigits(0);
		nf.setMaximumFractionDigits(0);
    	
		while (!isReadyForCollection()) {
			
			if (verbose) System.out.print(currentVolume.getHeart().getID() + " ---> Fitting progress: " + ((int)getAverageCompletion()) + "% | " + nf.format(timer.observeTick_ms()) + "ms" + "     \r");
	        
	        // Let the thread wait to reduce overhead
	        try {
	        	 
	        	Thread.sleep(msTimeout);
			} catch (InterruptedException e) {
				// That's ok, do nothing.
			}
		}

		if (verbose) System.out.println();

    	storeFit();
    }

    /**
     * @param the header for the output file
     * @param filename the output filename
     * @param nodes the nodes to export
     * @param hbf the box field used to filter invalid and thresholded nodes
     * @param automated if nodes should be thresholded automatically based on the mean error
     */
    public void storeFit() {
    	List<CartanParameterNode> nodes = collect();
    	
		IntensityVolume[] parameterVolumes = currentVolume.getResultAsIntensityVolumes(nodes);

		// The last volume is the error volume
		IntensityVolume errorVolume = parameterVolumes[parameterVolumes.length - 2];
		
		String datafile = currentVolume.getHeart().getDirectory() + "helicoidfit/";
		
		String extra = "";
		extra += "_fn" + getFittingError().getNeighborhoodFitSize();
		extra += "_en" + getFittingError().getNeighborhoodErrorSize();
//		extra += "_its" + getFittingError().getIterations();
//		extra += "_procs" + threadCount.getValue();
		
		CartanModel cartanModel = defaultRunner.getCartanModel();
		
		for (int i = 0; i < cartanModel.getNumberOfParameters(); i++) {
			MatlabIO.save(datafile + CartanParameter.getLabel(cartanModel.getParameterMask()[i]) + extra + ".mat", parameterVolumes[i]);
		}
		
		MatlabIO.save(datafile + "error" + extra + ".mat", errorVolume);
		
		// Save file with results
		try {
			NumberFormat nf = NumberFormat.getNumberInstance();
			nf.setMinimumFractionDigits(0);
			nf.setMaximumFractionDigits(0);
			Calendar currentDate = Calendar.getInstance(); 
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			IntensityVolumeMask mask = currentVolume.getMask();
			
			String buffer = "";
			buffer += TextToolset.box("Generalized Helicoid Fitting Parameters", '*');
			buffer += "Heart path = " + currentVolume.getHeart().getDirectory() + "\n";
			buffer += getFittingError().toString() + "\n";
			
			buffer += TextToolset.box("Stats", '*') + "\n";
			buffer += "Local time = " + dateFormat.format(currentDate.getTime()) + "\n";
			buffer += "Total computation time = " + nf.format(timer.observeTick_ms()) + "ms"  + "\n";
			buffer += "Number of threads = " + threadCount.getValue() + "\n";
			buffer += "Total volume voxel count = " + mask.getVolumeCount() + "\n";
			buffer += "Masked voxel count = " + mask.getMaskedCount() + "\n";
			buffer += "Unmasked voxel count = " + (mask.getVolumeCount()-mask.getMaskedCount()) + "\n";
			buffer += "Number of voxels used = " + nodes.size() + "\n";
			
			for (int i = 0; i < cartanModel.getNumberOfParameters(); i++) {
				buffer += "Mean " + CartanParameter.getLabel(cartanModel.getParameterMask()[i]) + " = " + parameterVolumes[i].getMean(mask) + " +- " + parameterVolumes[i].getStandardDeviation(mask) + "\n";
			}
			buffer += "Mean error = " + parameterVolumes[cartanModel.getNumberOfParameters()].getMean(mask) + " +- " + parameterVolumes[cartanModel.getNumberOfParameters()].getStandardDeviation(mask) + "\n";
			
			LineWriter.write(datafile + "info" + extra + ".txt", buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    
    public void run(FittableOrientationImage volume) {
    	run(volume, false);
    }

    private boolean verbose = true;
    
    private FittableOrientationImage currentVolume;
    
    public void run(FittableOrientationImage volume, boolean verbose) {
    	currentVolume = volume;
    	
    	this.verbose = verbose;
    	
        if (isRunning()) return;

        initRunnable(currentVolume);
        
        initializing = true;

        timer.stop();

        timer.tick();

        List<Point3i> points = currentVolume.getPointList();

        int n = points.size();
        int numcores = threadCount.isChecked() ? threadCount.getValue() : 1;
        int nums = n / numcores;
        
        runnables.clear();

        System.out.println(points.size() + " points in total.");

        System.out.println("Updating frame sampling...");
        getFittingError().updateFrameSampling();
        
        System.out.println("Creating threads...");
        for (int i = 0; i < numcores; i++) {
//        	System.out.println("Thread " + i);
        	
        	// Create list of points for this thread
            List<Point3i> subpoints = new LinkedList<Point3i>();
        	
            // Upper bound on the voxel indices
        	int lim1 = i * nums;
        	int lim2 = (i+1) * nums;
        	
        	if (i == numcores - 1) {
        		lim2 = points.size();
        	}
        	
//        	for (int j = lim1; j < lim2; j++) {
//        		subpoints.add(points.get(j));
//        	}
        	subpoints.addAll(points.subList(lim1, lim2));
        	
        	if (subpoints.size() == 0) continue;
        	
            FittingRunnable t = new FittingRunnable(i+1, new FittableRunner(defaultRunner), subpoints);
            t.setVerbose(verbose);
            runnables.add(t);
        }
        
        fittingThreads.clear();
        System.out.println("Launching threads...");
        for (FittingRunnable runnable : runnables) {
        	Thread t = new Thread(runnable);
        	t.setPriority(Thread.MAX_PRIORITY);
            t.start();
            fittingThreads.add(t);
        }
        System.out.println("Waiting for termination...");
        
        collected = false;
        initializing = false;
    }
    
	public boolean isRunning() {
		
		if (initializing) return true;
		
        for (FittingRunnable t : runnables) {
            if (t.probe()) return true;
        }
        
        return false;
    }

	public boolean isReadyForCollection() {
//		System.out.println(isRunning());
		return runnables.size() > 0 && !isCollected() && !isRunning();
	}

    private boolean isCollected() {
    	return collected;
    }

    public List<CartanParameterNode> collect() {

    	if (isCollected()) return allFits;

    	if (isRunning()) return null;
    	
        if (runnables.size() == 0) return null;
        
        forceCollect();
        
        if (verbose) System.out.println("Total running time = " + timer.tick());
        
        timer.stop();
        
        collected = true;
        
        // Filter garbage voxels
        List<CartanParameterNode> validNodes = new LinkedList<CartanParameterNode>();
        
        for (CartanParameterNode node : allFits) {
        	if (!node.isGarbage()) {
        		validNodes.add(node);
        	}
        }
        
        allFits = validNodes;

        return allFits;
    }

    public List<CartanParameterNode> forceCollect() {
        allFits = new LinkedList<CartanParameterNode>();

        runningTime = 0;
        
        for (FittingRunnable thread : runnables) {
        	List<CartanParameterNode> threadNodes = thread.getFit();
        	
        	if (threadNodes == null) return null;
        	
        	// Remove invalid nodes
        	for (CartanParameterNode node : threadNodes) {
//        		if (!node.isGarbage()) {
//        			System.out.println(node.toStringShort() + ", " + node.getError());
        			allFits.add(node);
//        		}
        	}
        	
//            allFits.addAll(threadNodes);
            runningTime += thread.getRunningTime();
        }
        
        runningTime /= threadCount.isChecked() ? threadCount.getValue() : 1;

        return allFits;
    }

    private VerticalFlowPanel vfp;

    private void createControls() {
    	if (vfp == null) {
            vfp = new VerticalFlowPanel();
            vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Parallel fitting"));
    	}
    	
    	vfp.removeAll();
    	
        vfp.add(threadCount.getSliderControlsExtended());
        vfp.add(defaultRunner.getControls());
        
        vfp.update();
    }
    
    /**
     * @return UI controls for this class.
     */
    public JPanel getControls() {
    	createControls();
        
        return vfp.getPanel();
    }
    
    /**
     * @return the running time for fitting all hairs.
     */
    public String getFormattedRunningTime() {
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        
        nf.setMaximumIntegerDigits(9);
        nf.setMinimumIntegerDigits(1);

        return nf.format(runningTime * 1e-9) + "s" + " (" + threadCount.getValue() + " threads)";
    }

    public double getRunningTime() {
    	return runningTime * 1e-9;	
    }
    
    public String getFormattedMeanRunningTime() {
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(3);
        nf.setMinimumFractionDigits(3);
        
        nf.setMaximumIntegerDigits(9);
        nf.setMinimumIntegerDigits(1);

        return allFits.size() > 0 ? nf.format(1e-9 * runningTime / allFits.size()) + "s" : "0";
    }

	private static NumberFormat formatter = NumberFormat.getNumberInstance();
	
	private double getAverageCompletion() {
		int completedAll = 0;
		int completedTotalAll = 0;
		
		for (FittingRunnable thread :  runnables) {
			double c = thread.getRunner().getCompletionPercentage();
			int completed = thread.getRunner().getCompleted();
			int completedTotal = thread.getRunner().getCompletedTotal(); 
			
			completedAll += completed;
			completedTotalAll += completedTotal;
		}
		if (completedTotalAll == 0) 
			return 0;
		else
			return (100.0 * completedAll) / completedTotalAll;

	}
	
	private List<String> completionText = new LinkedList<String>();
	
	public List<String> getCompletion() {
		formatter.setMinimumIntegerDigits(0);
		formatter.setMinimumFractionDigits(0);
		formatter.setMaximumFractionDigits(0);

		completionText.clear();
		
		double cAll = 0;
		int completedAll = 0;
		int completedTotalAll = 0;
		int ti = 1;
		
		String[] cs = new String[runnables.size()];
		for (FittingRunnable thread :  runnables) {
			double c = thread.getRunner().getCompletionPercentage();
			int completed = thread.getRunner().getCompleted();
			int completedTotal = thread.getRunner().getCompletedTotal(); 
			
			cs[ti - 1] = threadName + " " + (ti++) + ": " + formatter.format(c) + "%" + " (" + formatter.format(completed) + " / " + formatter.format(completedTotal) + ")";
			
			completedAll += completed;
			completedTotalAll += completedTotal;
		}
		if (completedTotalAll == 0) 
			cAll = 0;
		else
			cAll = (100.0 * completedAll) / completedTotalAll;
		
//		s += TextToolset.fillSpace("Total (" + formatter.format(timer.observeTick()), threadName.length()) + "s): " + formatter.format(cAll) + "%" + " (" + formatter.format(completedAll) + " / " + formatter.format(completedTotalAll) + ")\n";
//		s += TextToolset.fill("Total", ".", threadName.length() + 2) + ": " + formatter.format(cAll) + "%" + " (" + formatter.format(completedAll) + " / " + formatter.format(completedTotalAll) + ")" + " (" + formatter.format(timer.observeTick_ms()) + "ms)" + "\n";
//		if (terminated) {
//			s += "Total" + ":" + formatter.format(cAll) + "%" + " (" + formatter.format(completedAll) + " / " + formatter.format(completedTotalAll) + ")" + " (" + formatter.format(timer.observe()) + "ms)" + "\n";
//		}
//		else {
		completionText.add("Total" + ":" + formatter.format(cAll) + "%" + " (" + formatter.format(completedAll) + " / " + formatter.format(completedTotalAll) + ")" + " (" + formatter.format(timer.observeTick_ms()) + "ms)");
//		}
		
		// Add processors
		for (int i = 0; i < cs.length; i++) {
			completionText.add(cs[cs.length - i - 1]);
		}
		
		if (isRunning())
			completionText.add("Fitting in progress... " + currentVolume.getHeart().getID());
		
		if (!isRunning()) 
			completionText.add("Awaiting initialization...");
		
		return completionText;
	}
	
	private final String threadName = "Thread";

	public List<CartanParameterNode> stop() {
		
		timer.stop();
		
		// Halt all threads
		for (FittingRunnable thread : runnables) {
			thread.stop();
		}
		
		collected = true;

		return forceCollect();
	}

	public FittingError getFittingError() {
		return defaultRunner.getFittingError();
	}

	public void setFitNeighborhoodSize(int size) {
		defaultRunner.getFittingError().setFitNeighborhoodSize(size);
	}

	public void setErrorNeighborhoodSize(int size) {
		defaultRunner.getFittingError().setErrorNeighborhoodSize(size);
	}
}
    
