package helicoid.fitting.parallel;

import helicoid.fitting.FittableRunner;
import helicoid.parameter.HelicoidParameterNode;

import java.util.List;

import javax.vecmath.Point3i;

import app.pami2013.cartan.CartanParameterNode;



public class FittingRunnable implements Runnable {

	private boolean running = false;
	private List<CartanParameterNode> fit;
	private long runningTime;
	private FittableRunner runner;
	private List<Point3i> points;
	private int index = 0;
	private boolean verbose = true;
	
	public FittingRunnable(int index, FittableRunner runner, List<Point3i> pts) {
		this.runner = runner;
		this.points = pts;
		this.index = index;
	}
	
	public void setVerbose(boolean v) {
		verbose = v;
	}
	
	public FittableRunner getRunner() {
		return runner;
	}
	
    /**
     * @return whether this thread is still running.
     */
    public boolean probe() {
        return running || fit == null;
    }

    public List<CartanParameterNode> getFit() {
        if (running) return null;
        else return fit;
    }

    @Override
    public void run() {
        running = true;
        runningTime = System.nanoTime();
        
        int errorSize = runner.getFittingError().getNeighborhoodErrorSize();
        int fitSize = runner.getFittingError().getNeighborhoodFitSize();
        
    	if (verbose) System.out.println("---> Thread " + index + " launching: " + "iterations = " + runner.getFittingError().getIterations() + " | fit size = " + fitSize + "x" + fitSize + "x" + fitSize + " | error size = " + errorSize + "x" + errorSize + "x" + errorSize);

//		System.out.println(points);
		fit = runner.run(points);

        runningTime = System.nanoTime() - runningTime;
        running = false;
        
    	if (verbose) System.out.println("---> Thread " + index + " completed.");
    }
    
    public long getRunningTime() {
    	return runningTime;
    }

	public void stop() {
		runner.stop();
	}
    
	public FittableRunner getFittableRunner() {
		return runner;
	}

	public void reset() {
		fit = null;
	}
}
