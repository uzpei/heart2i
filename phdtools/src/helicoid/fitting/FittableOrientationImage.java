package helicoid.fitting;

import gl.geometry.WorldAxis;
import gl.math.FlatMatrix4d;
import gl.renderer.JoglTextRenderer;
import heart.Heart;
import helicoid.parameter.HelicoidParameterNode;

import java.awt.Font;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethod;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameterNode;

public class FittableOrientationImage implements Fittable {

	private BooleanParameter drawSeed[] = new BooleanParameter[3];
	
	private List<HelicoidParameterNode> nodes;

    private IntensityVolume[] seedVolumes = new IntensityVolume[3];
    
    private Heart heart;

    public FittableOrientationImage() {
    	// Do nothing, wait for <code>setHeart(Heart heart)</code>
    }
    
    public FittableOrientationImage(Heart heart) {
    	set(heart);
    }

    public IntensityVolume[] getResultAsIntensityVolumes(List<CartanParameterNode> nodes) {
    	   	
    	int[] dims = getHeart().getDimension();
    	
    	// Extract information from first parameter
    	// (assume all parameter have the same structure)
    	int n = nodes.get(0).getParameterMask().length;
    	
    	IntensityVolume[] curvatureVolumes = new IntensityVolume[n];
    	
		for (int i = 0; i < n+1; i++) {
			curvatureVolumes[i] = new IntensityVolume(dims);
			
			/*
			 * Initially fill the volume with NaNs. This is done to remove the use of a mask
			 * for further statistics (i.e. the mask is in effect the non-NaN voxels).
			 */
			IntensityVolume.fillWith(Double.NaN, curvatureVolumes[i].getData());
		}

		// Fill volumes with data
		for (CartanParameterNode node : nodes) {
			
			Point3i pt = node.getPosition();
			
			for (int i = 0; i < n; i++) {
				curvatureVolumes[i].set(pt.x, pt.y, pt.z, node.get(i));
			}
			
			// Last node is the error
			curvatureVolumes[n+1].set(pt.x, pt.y, pt.z, node.getError()[0]);
		}

		return curvatureVolumes;
    }
    
	public void set(Heart heart) {
    	this.heart = heart;
    	heart.prepareDataIfNecessary();
    }

	/**
	 * Set seed KT, KN, KB volumes.
	 * @param volumes
	 */
	public void setSeedVolumes(IntensityVolume[] volumes) {
		seedVolumes = volumes;
	}
	
	public IntensityVolume[] getSeedVolumes() {
		return seedVolumes;
	}

	public void setHeart(Heart heart) {
		this.heart = heart;
		heart.prepareDataIfNecessary();
	}
	
	private CoordinateFrame[][][] frames; 
	
	private boolean precomputeFrames = false;
	
	@Override
	public void precomputeCoordinateFrames(int numrots,
			int[] axisSamples, SamplingStyle samplingStyle) {

		int[] dim = getVolumeDimension();
		precomputeFrames = false;
		
		if (!precomputeFrames) return;

		frames = new CoordinateFrame[dim[0]][dim[1]][dim[2]];

		// Construct all frames
		heart.getVoxelBox().voxelProcess(new PerVoxelMethod() {
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}

			@Override
			public void process(int x, int y, int z) {
				frames[x][y][z] = new CoordinateFrame(heart.getFrameField().get(x, y, z).getFrame());
//				CoordinateFrame.invert(frames[x][y][z].getFrame(), m3d, v3d);
				
//				// Ortho check
//				Matrix4d m = new Matrix4d();
//				CoordinateFrame frame = frameField.get(x, y, z).getFrame();
//				m.mul(frame.getFrame(), frames[x][y][z].getFrame());
//				System.out.println(m);
			}
		});
		
	}
	
	private Matrix3d m3d = new Matrix3d();
	private Vector3d v3d = new Vector3d();

	@Override
	public CoordinateFrame getCoordinateFrame(Point3i p, CoordinateFrame frame) {
		if (precomputeFrames)
			frame.set(frames[p.x][p.y][p.z].getFrame());
		else {
			frame.set(heart.getFrameField().get(p.x, p.y, p.z).getFrame().getFrame());
//			CoordinateFrame.invert(frame.getFrame(), m3d, v3d);
		}

		return frame;
	}

	@Override
	public int[] getVolumeDimension() {
		return heart.getDimension();
	}

    private VerticalFlowPanel vfp = new VerticalFlowPanel();
    
	@Override
	public JPanel getControls() {
		return null;
	}
	
	@Override
	public void getOrientation(Point3i p, double[] orientation) {
		if (heart.getVoxelBox().isMaskedUnbounded(p.x, p.y, p.z)) {
			orientation[0] = Double.NaN;
			orientation[1] = Double.NaN;
			orientation[2] = Double.NaN;
		}
		else {
			heart.getFrameField().getF1().getVectorArray(p.x, p.y, p.z, orientation);
		}
	}

	@Override
	public List<Point3i> getPointList() {
		return getVoxelBox().getVolume();
	}

	public void display(GLAutoDrawable drawable) {
		display(drawable, false);
	}
	
	public void display(GLAutoDrawable drawable, boolean xOnly) {
		if (heart == null) return;

		final GL2 gl = drawable.getGL().getGL2();
		
		if (textRenderer == null) {
			Font font = new Font("Andale Mono", Font.BOLD, 54);
			textRenderer = new JoglTextRenderer(font, true, false);
			textRenderer.setColor(0.9f, 0.9f, 0.9f, 1f);
		}

		if (loading || textRenderer.isFadingOut()) {

			textRenderer.drawCenter("Loading...", drawable);
		}		

		if (loading) return;
		
		boolean drawdebug = false;

		if (!drawdebug)
			heart.display(drawable);

		final Matrix4d rot = new Matrix4d();
		final FlatMatrix4d fm = new FlatMatrix4d();

		if (drawdebug && frames != null) {
			// Draw the frame field directly as transformed canonical bases
			// for debugging purposes
			heart.getVoxelBox().voxelProcess(new PerVoxelMethod() {

				@Override
				public void process(int x, int y, int z) {
					if (frames[x][y][z] != null) {
						CoordinateFrame s = frames[x][y][z];

						gl.glPushMatrix();
						s.getRotation(rot);
						fm.setBackingMatrix(rot);

						gl.glTranslated(x + 0.5, y + 0.5, z + 0.5);
						gl.glMultMatrixd(fm.asArray(), 0);

						WorldAxis.display(gl);
						gl.glPopMatrix();
					}
				}

				@Override
				public boolean isValid(Point3d origin, Vector3d span) {
					return true;
				}
			});
		}
	}

	private JoglTextRenderer textRenderer;
	private boolean loading = false;
	private boolean fitting = false;
	
	public void setLoading(boolean b) {
		loading = b;
		
		if (!loading && textRenderer != null) {
			textRenderer.setFadeout(0, 2000);
		}
	}

	private double[] defaultSeedPoint = new double[] { 0, 0, 0 };
	@Override
	public double[] getSeedPoint(Point3i p) {
		if (seedVolumes[0] != null) {
			return new double[] { seedVolumes[0].getIntensity(p), seedVolumes[1].getIntensity(p), seedVolumes[2].getIntensity(p) };
		}
		else
			return defaultSeedPoint;
	}

	public int getCount() {
		if (heart == null) return 0;
		
		final int[] c = new int[] { 0 };
		heart.getVoxelBox().voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				c[0]++;
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});
		
		return c[0];
	}

	public void applyAnisotropicScale(GL2 gl) {
		if (heart == null) return;
		
		gl.glScaled(heart.getVoxelSize()[0], heart.getVoxelSize()[1], heart.getVoxelSize()[2]);
	}

	@Override
	public VoxelBox getVoxelBox() {
		if (heart == null) return null;
		
		return heart.getVoxelBox();
	}

	public IntensityVolumeMask getMask() {
		if (heart == null) return null;
		
		return heart.getMask();
	}

	@Override
	public VoxelFrameField getFrameField() {
		if (heart == null) return null;
		
		return heart.getFrameField();
	}

	public Heart getHeart() {
		return heart;
	}
	
	public String getName() {
		return heart.getID();
	}

	@Override
	public Fittable deepCopy() {
		return new FittableOrientationImage(new Heart(this.getHeart()));
	}
}
