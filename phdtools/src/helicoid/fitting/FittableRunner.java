package helicoid.fitting;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Point3i;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.optimization.GoalType;
import org.apache.commons.math.optimization.OptimizationException;
import org.apache.commons.math.optimization.RealPointValuePair;
import org.apache.commons.math.optimization.direct.DirectSearchOptimizer;
import org.apache.commons.math.optimization.direct.NelderMead;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset.GreekLetter;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.cartan.CartanParameter.CartanModel;

public class FittableRunner {

	private Fittable fittable;
	
    private boolean verbose = false;
    
    private BooleanParameter verbosep = new BooleanParameter("verbose", false);
    
    private FittingError fittingError;

	/**
	 * Nelder-Mead parameters
	 */
	private DoubleParameter nmrho;

	private DoubleParameter nmchi;

	private DoubleParameter nmgamma;

	private DoubleParameter nmsigma;
	
	private BooleanParameter automaticSeeding = new BooleanParameter("automatic seeding", true);

	private CartanModel cartanModel;
	
	private DirectSearchOptimizer dos;

	public FittableRunner() {
		this(null, false, CartanModel.FULL);
	}

	public FittableRunner(CartanModel model) {
		this(null, false, model);
	}

	public FittableRunner(Fittable fittable) {
		this(fittable, false, CartanModel.FULL);
	}

	public FittableRunner(Fittable fittable, boolean updateSampling, CartanModel model) {
		cartanModel = model;
		setFittable(fittable, updateSampling);
	}
	public FittableRunner(FittableRunner defaultRunner) {
		set(defaultRunner, false);
	}

	public void setFittable(Fittable fittable) {
		setFittable(fittable, false);
	}	
	
	public void setFittable(Fittable fittable, boolean updateSampling) {
		this.fittable = fittable;

		initOptimizer();
		
		init(updateSampling);
	}
	
	public void set(FittableRunner other, boolean udpateSampling) {
		// Create a new fittable such that data can be accessed asynchronously between different threads.
//		this.fittable = other.fittable;
		this.fittable = fittable.deepCopy();
		
		// Set parameters
		this.nmrho = other.nmrho;
		this.nmchi = other.nmchi;
		this.nmgamma = other.nmgamma;
		this.nmsigma = other.nmsigma;
		
		this.automaticSeeding = other.automaticSeeding;
		this.verbosep = other.verbosep;
		this.verbose = other.verbose;
		this.cartanModel = other.cartanModel;
		
		init(udpateSampling);
		
		fittingError.set(other.getFittingError());
	}
	
	private void initOptimizer() {
		nmrho = new DoubleParameter("Reflection "
				+ GreekLetter.RHO.code, 1, 0, 10);

		nmchi = new DoubleParameter("Expansion "
				+ GreekLetter.CHI.code, 2, 1, 10);

		nmgamma = new DoubleParameter("Contraction "
				+ GreekLetter.GAMMA.code, 0.5, 0, 1);

		nmsigma = new DoubleParameter("Shrink "
				+ GreekLetter.SIGMA2.code, 0.5, 0, 1);

	}
	
	private void init(boolean udpateSampling) {
		
		if (fittingError == null) {
			fittingError = new FittingError();
		}
		
		fittingError.setFittable(fittable);

		if (udpateSampling) fittingError.updateFrameSampling();
				
		ParameterListener nmListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateOptimizer();
			}
		};

		reset();
		
		nmrho.addParameterListener(nmListener);
		nmchi.addParameterListener(nmListener);
		nmgamma.addParameterListener(nmListener);
		nmsigma.addParameterListener(nmListener);
		
		buildSeedPoints();
	}
	
	public CartanModel getCartanModel() {
		return cartanModel;
	}
	
	public void reset() {
		resetCompletion();
		updateOptimizer();
	}
	
	private void updateOptimizer() {
		// DirectSearchOptimizer dos = new MultiDirectional();
		dos = new NelderMead(nmrho.getValue(), nmchi
				.getValue(), nmgamma.getValue(), nmsigma.getValue());
		dos.setConvergenceChecker(fittingError);
	}
	
	
	private static NumberFormat formatter = NumberFormat.getNumberInstance();
	private int completed;
	private int completed_total;
	
	public String getCompletion() {
		formatter.setMinimumIntegerDigits(0);
//		formatter.setMaximumIntegerDigits(3);
		formatter.setMinimumFractionDigits(0);
		formatter.setMaximumFractionDigits(0);
		return formatter.format(getCompletionPercentage()) + "%" + " (" + formatter.format(getCompleted()) + " / " + formatter.format(getCompletedTotal()) + ")";
	}

	public double getCompletionPercentage() {
		return (100.0 * getCompleted()) / getCompletedTotal();
	}
	
	public int getCompleted() {
		return completed;
	}
	
	public int getCompletedTotal() {
		return completed_total;
	}
	
	private void resetCompletion() {
		completed = 0;
		completed_total = 1;
		
//		updateSampling();
	}
	
	private void updateCompletion(int completed, int total) {
		this.completed = completed;
		this.completed_total = total;
	}
	
	public double[][][][] runall() {
		
		resetCompletion();
		
		int numParams = cartanModel.getNumberOfParameters();
		
		double[][][][] volume = new double[numParams][fittable.getVolumeDimension()[0]][fittable.getVolumeDimension()[1]][fittable.getVolumeDimension()[2]];
		
		List<Point3i> pts = fittable.getPointList();
		
		int i = 0;
		int n = pts.size();
		
		for (Point3i p : pts) {
			CartanParameterNode node = run(p);
			
			for (int j = 0; j < numParams; j++) {
				volume[j][p.x][p.y][p.z] = node.get(cartanModel.getParameterMaskIndex(j));
			}
			
			// Update completion
			updateCompletion(++i, n);
		}
				
		return volume;
	}
	
	/**
	 * Fit a portion of the whole volume, defined by dimensions <code>dim</code> and containing a list of points <code>pts</code> 
	 * @param type The helicoid parameter type to fit (can be all)
	 * @param pts a list of points contained in the subvolume
	 * @return a tridimensional array containing the fit.
	 */
	public List<CartanParameterNode> run(List<Point3i> pts) {

		terminationRequest = false;
		
		resetCompletion();
		
		int i = 0;
		int n = pts.size();
		List<CartanParameterNode> nodes = new ArrayList<CartanParameterNode>(n);
		
		for (Point3i p : pts) {
			if (terminationRequest) {
				System.out.println("Termination request. Stopping thread " + Thread.currentThread().getName());
				break;
			}

//			System.out.println("---\n" + p);
			CartanParameterNode node = run(p);
			
			if (Double.isNaN(node.getError())) 
				node.setGarbage(true);
			
			nodes.add(node);

			// Update completion
			updateCompletion(++i, n);
		}
		
		return nodes;
	}
	
	private boolean terminationRequest = false;
	
	public void stop() {
		terminationRequest = true;
	}

	private List<double[]> seedPoints;
	
	private void buildSeedPoints() {
		if (seedPoints == null) {
			seedPoints = new LinkedList<double[]>();
		}
		
		// Construct seed points
		// Important... we only lose a few iterations
		seedPoints.clear();
		List<CartanParameter> seeds = cartanModel.getSeedPoints();

		// Construct double list from CartanParameter list
		for (CartanParameter cp : seeds) {
			seedPoints.add(cp.getParameter());
		}
	}
	
	public CartanParameterNode run(Point3i point) {

		if (verbose) {
			System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~"
								+ "Fitting at location: " + point
								+ "~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("Evaluating seed points...");
		}
		
		CartanParameterNode resultNode = new CartanParameterNode(cartanModel.getParameterMask(), point);

		try {
			fittingError.init(point, cartanModel);

			// Fit seed point
			double[] seedPoint = null;

//			// Use an array of one element when fitting a single parameter
//			if (!allParameters)
//				seedPoint = new double[] { 0 };
//			// otherwise use a 3-tuple seed point for kt, kn, kb
//			else
//				// kt, kn, kb
//				seedPoint = new double[] { 0, 0, 0 };
			double bestDistance = Double.MAX_VALUE;
			if (automaticSeeding.getValue()) {
				// Pick best seedpoint
				for (double[] seed : seedPoints) {
					double distance = fittingError.value(seed);
					if (distance < bestDistance) {
						seedPoint = seed;
						bestDistance = distance;
					}
				}
				
			}
			else {
				seedPoint = fittable.getSeedPoint(point);
				bestDistance = fittingError.value(seedPoint);
				
			}
			
			if (seedPoint == null) {
				resultNode.setGarbage(true);
				resultNode.setError(Double.NaN);
				return resultNode;
			}

			if (verbose) {	
				System.out.println("Best seed point = " + Arrays.toString(seedPoint));
				System.out.println("Initial error = " + bestDistance);
				System.out.println("\n === Launching Optimization ===");
			}
			
			if (fittingError.getIterations() == 1) {
				if (verbose)
					System.out.println("Minimum number of iterations reached. Returning seed point.");
				
				resultNode.setParameters(seedPoint);
				resultNode.setError(bestDistance);
				return resultNode;
			}
			
			// Begin the optimization
			RealPointValuePair pcp = dos.optimize(fittingError, GoalType.MINIMIZE,
					seedPoint);

			// When this point is reached, the optimization is finished
//			double distance = pcp.getValue();
			for (int i = 0; i < cartanModel.getNumberOfParameters(); i++) {
				resultNode.set(cartanModel.getParameterMaskIndex(i), pcp.getPoint()[i]);
			}
			
			double error = fittingError.evaluateUnregularizedError(resultNode, point);
//			double error = fittingError.value(pcp.getPoint());

			if (verbose) {
				System.out.println("Final error after " + dos.getIterations() + " iterations = " + Math.acos(1 - error));
				System.out.println("final value = " + Arrays.toString(pcp.getPoint()));
			}

			// Use error in radians
			resultNode.setError(Math.acos(1 - error));
			
		} catch (OptimizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FunctionEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultNode;
	}


	public Fittable getFittable() {
		return fittable;
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Optimization"));

		vfp.add(automaticSeeding.getControls());
		vfp.add(verbosep.getControls());
		verbosep.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				setVerbose(verbosep.getValue());
			}
		});
		
//        vfp.add(fittable.getControls());

        vfp.add(fittingError.getControls());
        
		VerticalFlowPanel popt = new VerticalFlowPanel();
		popt.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Nelder-Mead"));
		popt.add(nmrho.getSliderControls());
		popt.add(nmchi.getSliderControls());
		popt.add(nmgamma.getSliderControls());
		popt.add(nmsigma.getSliderControls());
		CollapsiblePanel cp = new CollapsiblePanel(popt.getPanel(),
				"Nelder-Mead");
		cp.collapse();
		vfp.add(cp);
		
		JButton btnRun = new JButton("run");
		vfp.add(btnRun);
		
		btnRun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runall();
			}
		});

		return vfp.getPanel();
	}

	public FittingError getFittingError() {
		return fittingError;
	}
	
	public void setVerbose(boolean b) {
		verbose = b;
		
		fittingError.setVerbose(b);
	}
	
}
