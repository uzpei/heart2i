package helicoid.fitting;


import java.util.List;

import javax.swing.JPanel;
import javax.vecmath.Point3i;

import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.SamplingStyle;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;
import voxel.VoxelFrameField;


public interface Fittable {

	/**
	 * @param p
	 * @return the coordinate frame at p
	 */
	public CoordinateFrame getCoordinateFrame(Point3i p, CoordinateFrame frame);

	/**
	 * @return a voxel box for this volume.
	 */
	public VoxelBox getVoxelBox();

	/**
	 * @return a frame field for this fittable volume.
	 */
	public VoxelFrameField getFrameField();
	
//	heart.getFrameField().getFieldX().getVector3d(pvolume.x, pvolume.y, pvolume.z, T);
//	heart.getFrameField().getFieldY().getVector3d(pvolume.x, pvolume.y, pvolume.z, N);
//	heart.getFrameField().getFieldZ().getVector3d(pvolume.x, pvolume.y, pvolume.z, B);

	/**
	 * @param p
	 * @param orientation a 3-dim array into which the vector will be stored
	 */
	public void getOrientation(Point3i p, double[] orientation);


	public void precomputeCoordinateFrames(int numrots, int[] axisSamples, SamplingStyle samplingStyle);
	
	/**
	 * @return the list of points from which a fitting will be performed
	 */
	public List<Point3i> getPointList();
	
	/**
	 * @return the dimension of the volume, in voxel*voxel*voxel
	 */
	public int[] getVolumeDimension();
	
	public JPanel getControls();
	
	public double[] getSeedPoint(Point3i p);

	public Fittable deepCopy();
	
}
