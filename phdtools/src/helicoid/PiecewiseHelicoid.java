package helicoid;

import gl.renderer.JoglTextRenderer;
import helicoid.parameter.HelicoidParameter;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import tools.geom.Polyline;
import tools.geom.PolylineAnalysis;

/**
 * Class representing multiple helicoid hairs stiched together.
 * 
 * @author piuze
 */
public class PiecewiseHelicoid implements Iterable<Helicoid>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8045907932834794310L;

	private List<Helicoid> helicoids = new LinkedList<Helicoid>();

	private List<Point3d> points = new LinkedList<Point3d>();

	private Polyline strand = new Polyline();

	private static PolylineAnalysis hairAnalysis = new PolylineAnalysis();

	private double length = 0;

	/**
	 * Create a new multi hair using this helicoid strand as a root.
	 */
	public PiecewiseHelicoid(Helicoid root) {

		helicoids.add(root);
		points.addAll(root.getPoints());

		length = root.getLength();
	}

	@Override
	public PiecewiseHelicoid clone() {
		return new PiecewiseHelicoid(this);
	}
	
	/**
	 * Create a new and empty multi hair.
	 */
	public PiecewiseHelicoid() {
	}
	
	public PiecewiseHelicoid(PiecewiseHelicoid other) {
		set(other);
	}

	private void set(PiecewiseHelicoid other) {
		if (other == null) return;
		
		helicoids.clear();
		points.clear();
		for (Helicoid h : other.getHelicoids()) {
			add(h.clone());
		}
		
		// Keep frame information
		get(0).setRotationFrame(other.get(0).getRotationFrame());
		get(0).setLocalFrame(other.get(0).getLocalFrame());
	}


	public Helicoid get(int i) {
		return helicoids.get(i);
	}

	public List<Point3d> getPoints() {
		return points;
	}

	public double getLength() {
		return length;
	}

	/**
	 * Add this hair to the tip of the multi-hair.
	 * 
	 * @param h
	 */
	public void add(Helicoid h) {
		add(h, false);
	}

	/**
	 * @param h
	 * @param align if the added piece should be aligned with the previous ones.
	 * @return
	 */
	public Helicoid add(Helicoid h, boolean align) {

		if (helicoids.size() > 0) {
			// glue this new hair to the tip of the last one,
			// using its Frenet frame or simply its origin
			Helicoid last = helicoids.get(helicoids.size() - 1);

			if (align) {
				h.transform(last.getEndFrame());
				h.moveTo(last.getTip());
			}
		}
		helicoids.add(h);
		points.addAll(h.getPoints());

		strand.concatenate(new Polyline(h.getPoints(), true));

		length += h.getLength();

		return h;
	}

	@Override
	public Iterator<Helicoid> iterator() {
		return helicoids.iterator();
	}

	public void display(GLAutoDrawable drawable) {
		display(drawable, false);
	}

	// 710 (32)
	public void display(GLAutoDrawable drawable, boolean useColorCoding) {
		if (getCount() == 0 || get(0).getPoints().size() == 0) return;

		GL2 gl = drawable.getGL().getGL2();

		float[] colorOptiMulti = new float[] { 0, 1, 0 };

		boolean drawpieces = false;
		boolean drawFrenet = false;
		boolean debug = false;
		
		int psize1 = 15;
		int psize2 = 1;

		gl.glDisable(GL2.GL_LIGHTING);

		if (useColorCoding) {
			float[][] hc = new float[][] { 
					{ 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 },
					{ 1, 0, 1 }, { 1, 1, 0 }, { 0, 1, 1 }, 
					{ 1, 0.5f, 0}, { 0.5f, 1, 0 }, { 0.5f, 0, 1 },
					{ 1, 0.5f, 1}, { 1, 1, 0.5f }, { 0.5f, 1, 1 } 
					};
			int hid = 0;
			
			for (Helicoid hs : helicoids) {
				if (useColorCoding) {
					if (hid < helicoids.size() && hid < hc.length) {
						gl.glColor3f(hc[hid][0], hc[hid][1], hc[hid][2]);
					}
				}
				hs.display(drawable);
				hid++;
			}
		}
		else {
			for (Helicoid hs : helicoids) {
				hs.display(drawable);
			}
		}

		if (!debug)
			return;
		// displayLengthTest(drawable, fit);

		/*
		 * First draw the points of the candidate strand
		 */
		gl.glColor3f(1, 0, 0);
		gl.glPointSize(psize1);
		gl.glBegin(GL2.GL_POINTS);
		for (Helicoid hs : helicoids) {
			Point3d p = hs.getPoint(0);
			gl.glVertex3d(p.x, p.y, p.z);
		}
		gl.glEnd();

		int hi = 0;
		gl.glColor3f(0, 0, 0);
		for (Helicoid hs : helicoids) {
			hi++;
			JoglTextRenderer.print3dTextLines("" + hi, hs.getOrigin());
		}

		gl.glColor3f(colorOptiMulti[0], colorOptiMulti[1], colorOptiMulti[2]);

	}

	public Polyline toStrand() {
		return strand;
	}

	public List<Helicoid> getHelicoids() {
		return helicoids;
	}

	public void clear() {
		helicoids.clear();
	}

	public Helicoid toHelicoid() {
		List<Point3d> pts = new LinkedList<Point3d>();
		for (Polyline h : helicoids) {
			pts.addAll(h.getPoints());
		}
		Helicoid h = new Helicoid(pts, new HelicoidParameter(helicoids.get(0)
				.getParameter()));
		h.setLocalFrame(this.get(0).getLocalFrame());
		h.setEndFrame(this.get(getHelicoids().size()-1).getEndFrame());
		
		return h;
	}

	public int size() {
		return helicoids.size();
	}

	public void scale(double d) {
		for (Point3d p : points) {
			p.scale(d);
		}
	}

	public PiecewiseHelicoid transform(Matrix4d m) {
		for (Helicoid h : helicoids) {
			h.transform(m);
//			h.getLocalFrame().mul(m, h.getLocalFrame());
		}

		return this;
	}

	public void computeLength() {
		length = 0;
		for (Helicoid h : helicoids) {
			length += h.getLength();
		}
	}
	
	public Point3d getOrigin() {
		return helicoids.get(0).getPoint(0);
	}

	public Point3d getTip() {
		return helicoids.get(helicoids.size() - 1).getTip();
	}

	public void moveTo(Tuple3d o) {
		if (size() == 0) return;
		
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		Vector3d v = new Vector3d();
		v.scale(-1, helicoids.get(0).getOrigin());
		v.add(o);

		m.setTranslation(v);

//		for (Polyline h : helicoids) {
//			h.transform(m);
//		}
		for (Helicoid h : helicoids) {
			Point3d p = new Point3d(h.getOrigin());
			p.add(v);
			h.moveTo(p);
			
		}
	}
	
	/**
	 * @return the number of helicoid pieces in this p-helicoid.
	 */
	public int getCount() {
		return getHelicoids().size();
	}

	/**
	 * Reduces the number of vertices in that piecewise helicoid.
	 * The first and last vertex of each helicoid is added automatically.
	 * @param skip
	 * @return
	 */
	public PiecewiseHelicoid tesselate(int targetCount) {
		if (targetCount > points.size()) return this;
		
		double perc = targetCount / ((double)points.size());
		
		// Make sure the point count is matched
		int hcount = 0;
		for (Helicoid h : this) {
			hcount += h.getCount();
		}
		
		System.out.println(hcount);
		
//		if (perc > 1) return this;
		
		for (Helicoid h : this) {
			h.tesselate(perc);
		}
		
		System.nanoTime();
		
		return this;
	}

	public void setFirst(Helicoid h) {
		
        moveTo(new Point3d());
        transform(h.getEndFrame());
        moveTo(h.getTip());

		points.addAll(0, h.getPoints());
		helicoids.add(0, h);
 	}
}
