package helicoid.modeling.tracing;

import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameterControl;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.IntParameter;
import swing.parameters.ParameterManager;
import swing.text.GreekToolset.GreekLetter;

public class TraceParameterControl extends ParameterManager {
    
	public final static double DEFAULT_TRACESCALE = 1.0;

    protected DoubleParameterPoint3d origin;
    protected DoubleParameterPoint3d offset;
    
    protected HelicoidParameterControl hpc = new HelicoidParameterControl();
    protected DoubleParameter noiseMag = new DoubleParameter("noise "
            + GreekLetter.SIGMA2.code, 0.018, 0, 0.1);

    protected DoubleParameter lengthrand = new DoubleParameter("length randomness", 0, 0, 1);

    /**
     * Wave pitch
     */
    protected DoubleParameter wavypitch = new DoubleParameter("wave pitch ", 0.02, 0,
            1);
    
    /**
     * Wave radius
     */
    protected DoubleParameter wavyradius = new DoubleParameter("wave radius ", 0.1, 0,
            1);

    protected DoubleParameter pstepSize = new DoubleParameter("Step Size",
            0.05, 0, 10);

    protected IntParameter pstepCount = new IntParameter("Step Count", 100, 1,
            1000);

    protected DoubleParameter pstepSkip = new DoubleParameter("Step Skip %", 0, 0,
            100);

    protected DoubleParameter traceScale = new DoubleParameter("trace scale",
    		DEFAULT_TRACESCALE, 0, 100);

    public double getKT() {
    	return hpc.getKT();
    }
    public double getKN() {
    	return hpc.getKN();
    }
    public double getKB() {
    	return hpc.getKB();
    }
//    public double getAlpha() {
//    	return hpc.alpha;
//    }
    
    public TraceParameterControl() {
    	
    	double max = 1000;
    	
        origin = new DoubleParameterPoint3d("Origin", new Point3d(), new Point3d(-max, -max, -max), new Point3d(max, max, max));
        offset = new DoubleParameterPoint3d("Offset", new Point3d(), new Point3d(-max, -max, -max), new Point3d(max, max, max));

    	noiseMag.setChecked(false);
    	wavypitch.setChecked(false);
    	wavyradius.setChecked(false);
    	lengthrand.setChecked(false);

    	super.addParameters(origin.getParameters());
    	super.addParameters(offset.getParameters());
    	super.addParameters(hpc.getParameters());
    	super.addParameter(noiseMag);
    	super.addParameter(lengthrand);
    	super.addParameter(wavypitch);
    	super.addParameter(wavyradius);
    	super.addParameter(pstepSize);
    	super.addParameter(pstepCount);
    	super.addParameter(pstepSkip);
    	super.addParameter(traceScale);
    }
    
    public void setParameter(HelicoidParameter p) {
        hold(true);
        hpc.set(p);
        hold(false);
    }

    public void setOrigin(Point3d p) {
        hold(true);
        origin.setPoint3d(p);
        hold(false);
    }

    public Point3d getOrigin() {
    	return origin.getPoint3d();
    }
    
	public void setOriginToZero() {
		hold(true);
		origin.setPoint3d(new Point3d());
		hold(false);
	}

    public void setOffset(Point3d p) {
        hold(true);
        offset.setPoint3d(p);
        hold(false);
    }
    
	public void setOffset(double ox, double oy, double oz) {
        hold(true);
        offset.setPoint3d(ox ,oy, oz);
        hold(false);
	}
    
    public Point3d getOffset() {
    	return offset.getPoint3d();
    }

    public void setStepCount(int count) {
        hold(true);
        pstepCount.setValue(count);
        hold(false);
    }

    public void setStepSkip(double percent) {
        hold(true);
        pstepSkip.setValue(percent);
        hold(false);
    }

    public void setStepSize(double size) {
        hold(true);
        pstepSize.setValue(size);
        hold(false);
    }
    
    public int getStepCount() {
    	return pstepCount.getValue();
    }

    public double getStepSkip() {
    	return pstepSkip.getValue();
    }

    public double getStepSize() {
    	return pstepSize.getValue();
    }

	/**
	 * Adjust the step skipping such that generated hairs have the desired number of points.
	 * @param count
	 */
	public void setTargetCount(int count) {
		setStepSkip(1.0 / count);
	}

    public void setTargetLength(double target) {
        hold(true);

        pstepSize.setValue(target / pstepCount.getValue());
        traceScale.setValue(1);
        // traceScale.setValue(target / (pstepCount.getValue() *
        // pstepSize.getValue()));
        hold(false);
    }

    public void setTraceScale(double v) {
    	hold(true);
        traceScale.setValue(v);
    	hold(false);
    }
    
    public double getTraceScale() {
    	return traceScale.getValue();
    }
    
    /**
     * @return the current parameter vector.
     */
    public HelicoidParameter getParameter() {
        return hpc;
    }

	public double getNoise() {
		return noiseMag.getValue();
	} 

    public void setNoise(double v) {
        noiseMag.setValue(v);
        noiseMag.setChecked(v > 1e-4 ? true : noiseMag.isChecked());
    }

    public void setWavynessr(double v) {
        wavyradius.setValue(v);
    }

    public void setWaviness(double v) {
        wavypitch.setValue(v);
    }

    public double getWavyness() {
    	return wavypitch.getValue();
    }
    public double getWavynessr() {
    	return wavyradius.getValue();
    }

	public void set(TraceParameterControl other) {
        setOrigin(other.origin.getPoint3d());
        setOffset(other.offset.getPoint3d());
        setParameter(other.getParameter());
        setStepCount(other.getStepCount());
        setStepSkip(other.getStepSkip());
        setWaviness(other.getWavyness());
        setNoise(other.getNoise());
	}
	
	public JPanel getHelicoidControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(hpc.getControls());
        vfp.add(pstepSize.getSliderControls(false));
        vfp.add(pstepCount.getSliderControls());
        vfp.add(pstepSkip.getSliderControls(false));
        
		return vfp.getPanel();
	}
	
	public JPanel getOtherControls() {
		VerticalFlowPanel hpanel = new VerticalFlowPanel();

		hpanel.add(traceScale.getSliderControls(false));
		
		VerticalFlowPanel lpanel = new VerticalFlowPanel();
		String title = "Positional Parameters";
		lpanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), title));

		lpanel.add(origin.getControls());
		lpanel.add(offset.getControls());
		
		CollapsiblePanel cp = new CollapsiblePanel(lpanel, title);
		cp.collapse();
		hpanel.add(cp);

        hpanel.add(noiseMag.getSliderControlsExtended());
        
        hpanel.add(wavyradius.getSliderControlsExtended("enabled"));
        hpanel.add(wavypitch.getSliderControlsExtended("p rand"));
        
        wavyradius.setChecked(false);
        wavypitch.setChecked(false);

        hpanel.add(lengthrand.getSliderControls());

		return hpanel.getPanel();
	}
	
    public void setDefaults() {
        hold(true);
        offset.setPoint3d(new Point3d());
        hpc.set(new HelicoidParameter(0, 0, 0, 0));

        noiseMag.setValue(0);
        noiseMag.setChecked(false);
        wavypitch.setValue(0);
        
        // groundTruth.setValue(true);
        
        // It was determined that 100 steps of size 0.05 are enough to explore the helicoid space
        // i.e. keep this length (5.0) to get similar results (adjust worldpos scaling if necessary)
        pstepCount.setValue(100);
        pstepSize.setValue(0.05);
        hold(false);
    }

    public boolean isWavinessEnabled() {
    	return wavyradius.isChecked() && wavypitch.getValue() > 1e-8;
    }
    
    public boolean isWavinessRandom() {
    	return wavypitch.isChecked();
    }
    
    public boolean isNoiseEnabled() {
    	return noiseMag.isChecked();
    }
    
    public boolean isLengthRandomnessEnabled() {
    	return lengthrand.isChecked();
    }
	public void setWavinessRandom(boolean b) {
		wavypitch.setChecked(b);
	}
}

