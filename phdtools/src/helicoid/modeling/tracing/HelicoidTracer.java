/**
 * 
 */
package helicoid.modeling.tracing;

import helicoid.Helicoid;
import helicoid.parameter.HelicoidParameter;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import tools.frame.OrientationFrame;

/**
 * Class for tracing helicoids, using different integration methods.
 * 
 * @author piuze
 * 
 */
public abstract class HelicoidTracer {

	public enum TracingMethod {
		FORWARD_EULER, MIDPOINT, RK_4
	};

	public static final TracingMethod DEFAULT_TRACING_METHOD = TracingMethod.FORWARD_EULER;

	private static final Random rand = new Random();

	private Vector3d ET = new Vector3d();

	private Vector3d EN = new Vector3d();

	private Vector3d EB = new Vector3d();

	private int turn_count = 0;
	private double theta = 0;
	private double theta_p = 0;
	private double phi = 0;
	private Vector3d tangent = new Vector3d();
	private Point3d worldPos = new Point3d();
	protected Point3d helicPos = new Point3d();
	private List<Point3d> points = null;
	private Matrix4d endFrame = new Matrix4d();

	/**
	 * These all need to be set explicitly.
	 */
//	private HelicoidParameter hp = new HelicoidParameter();
//	private Point3d offset = new Point3d();
//	private int stepCount = 100;
//	private int stepSkip = 1;
//	private double h = 0.05;
//	private double traceScale = 1;
	private Matrix4d orientationFrame = new Matrix4d();
//	private Point3d origin = new Point3d();
	private TraceParameterControl traceParameterControl;
	
//	/**
//	 * Waviness parameters.
//	 */
//	private boolean useWaviness = false;
//	private boolean useRandomPitch = false;
//	private double w_radius;
//	private double w_pitch;
	private Vector3d wave = new Vector3d();
	private Vector3d wave_0 = new Vector3d();
	private double t = 0;
	private double t_0 = 0;
	private int skipmod;

//	private double lengthRandomness = 0;

	public HelicoidTracer(TraceParameterControl p) {
		traceParameterControl = p;
	}
	
//	public double getLengthRandomness() {
//		return lengthRandomness;
//	}
//	
//	public void setLengthRandomness(double percentage) {
//		lengthRandomness = percentage;
//	}
	
	public void setPhase0(double t0) {
		t_0 = t0;
	}	

//	public void setParameters(HelicoidParameter p, Point3d offset,
//			int stepCount, double stepSkipPercent, double stepSize, double traceScale,
//			Matrix4d orientationFrame, Point3d origin) {
//		
//		if (isBlocking()) {
//			System.err.println("Blocking");
//			return;
//		}
//		setParameter(p);
//		setOffset(offset);
//		setStepCount(stepCount);
//		setStepSkip(1+(int)(stepSkipPercent/100 * (stepCount-1)));
//		setStepSize(stepSize);
//		setTraceScale(traceScale);
//		setOrientationFrame(orientationFrame);
//		setOrigin(origin);
//	}
	
//	public void setTraceScale(double scale) {
//		traceScale = scale;
//		
//		// Incorporate length randomness
//		traceScale *= (1 + 2* lengthRandomness * (-0.5 + rand.nextDouble()));
//	}
	
	public void setOrientationFrame(Matrix4d frame) {
		orientationFrame.set(frame);
	}
	
	public Helicoid trace() {
		initializeTracing();
		
		doInit();
		
		for (int i = 0; i <= traceParameterControl.getStepCount(); i++) {
			beginStep();
			
			doTrace(traceParameterControl.getStepSize(), theta, phi, tangent);
			
			computeWaviness(i);
			endStep(i);
		}
		
		return terminateTracing();
	}
	
	protected abstract void doTrace(double h, double theta, double phi, Vector3d tangent);
	protected abstract void doInit();

	/**
	 * @param hp
	 * @param x
	 * @param y
	 * @param z
	 * @param v store the result in this vector
	 */
	public void doTraceSingle(HelicoidParameter hp, double x, double y, double z, Vector3d v) {
//		Vector3d v = new Vector3d();
		helicPos.set(x, y, z);
		turn_count = 0;
		
        double theta = rightHelicoid(hp, x, y, z);
        double phi = getPhi(theta, hp);

		v.x = (Math.cos(phi) * Math.cos(theta));
		v.y = (Math.cos(phi) * Math.sin(theta));
		v.z = Math.sin(phi);

		
		v.normalize();
		
//		return v;
	}
	
	/**
	 * @param hp
	 * @param offset
	 * @param v
	 * @return
	 */
	public void doTraceSingle(HelicoidParameter hp, Point3d offset, Vector3d v) {
		doTraceSingle(hp, offset.x, offset.y, offset.z, v);
	}

	private void initializeTracing() {
		
		skipmod = 1+(int)(traceParameterControl.getStepSkip()/100 * (traceParameterControl.getStepCount()-1));

		// Initial wave offset
//		wave_0.set(0, 0, 0);
		if (traceParameterControl.isWavinessRandom()) {
            t_0 = 0.7 * rand.nextDouble() * 2 * Math.PI; // start at random phase
		}
		t = t_0;
		
		// A list that contains the points forming a helicoid.
		points = new LinkedList<Point3d>();

		// First step tangent direction.
		tangent.set(1, 0, 0);
		
		// Initial position in the world space.
//		worldPos.set(p0);
		worldPos.set(0,0,0);

		// Position in the helicoid space.
		helicPos = new Point3d(traceParameterControl.getOffset());

		// Keep track of the number of 2Pi turns that theta makes
		turn_count = 0;

		// Initial orientation angles.
		// Get theta from the offset
		// (is zero if offset = origin)
		getTheta();
		getPhi();
		theta_p = theta;

	}

	private Helicoid terminateTracing() {
		Helicoid h = new Helicoid(points, traceParameterControl.getParameter(), true);

		// Set the last local frame
		endFrame.setColumn(0, ET.x, ET.y, ET.z, 0);
		endFrame.setColumn(1, EN.x, EN.y, EN.z, 0);
		endFrame.setColumn(2, EB.x, EB.y, EB.z, 0);
		endFrame.setColumn(3, 0, 0, 0, 1);

		h.setEndFrame(endFrame);

		h.transform(orientationFrame);

		h.moveTo(traceParameterControl.getOrigin());
	
		h.setTraceScale(traceParameterControl.getTraceScale());
		
		h.setWavyPhase(t_0, t);
		
		return h;
	}

	/**
	 * Extract theta from the current tangent. Used when updating theta from
	 * one time step to another where the tangent was obtained by Runge-Kutta or another
	 * non-FE approach.
	 */
	public void extractImplicitTheta() {
//		theta = Math.acos(tangent.z / tangent.length());
		theta = Math.atan2(tangent.y, tangent.x);
		
		// Use turning information
		theta += turn_count * 2 * Math.PI;

		updateTurning(true);
	}

	private void getAngles(Point3d p, boolean b) {
		getTheta(p);
		updateTurning(b);
		getPhi();
	}

	protected void getAngles() {
		getAngles(helicPos, true);
	}

	protected Vector3d f(Point3d p, Vector3d y) {
		getAngles(p, false);
		
		y.x = (Math.cos(phi) * Math.cos(theta));
		y.y = (Math.cos(phi) * Math.sin(theta));
		y.z = Math.sin(phi);

		return y;
	}

	private void beginStep() {
		setHelicoidFrame(tangent, theta, phi);
	}

	private void endStep(int i) {

		// Update position
		helicPos.add(tangent);

		// Transform and add the new vector in the world coordinate frame.
		worldPos.scaleAdd(traceParameterControl.getTraceScale(), tangent, worldPos);
		
//		if (i == 0) {
//			System.out.println(tangent);
//			System.out.println(worldPos);
//		}
		
		// Subsampling restriction
		if (i == 0 || i > 0 && i % skipmod == 0 || i == traceParameterControl.getStepCount()) {
			Point3d p = new Point3d(worldPos);

			// Add waviness offset
			if (traceParameterControl.isWavinessEnabled()) {
				p.add(wave);
			}

			points.add(p);
		}

		theta_p = theta;
	}
		
	private void computeWaviness(int i) {
		if (!traceParameterControl.isWavinessEnabled()) return;
		
        wave.scale(traceParameterControl.getWavynessr() * Math.cos(t), EN);
        wave.scaleAdd(traceParameterControl.getWavynessr() * Math.sin(t), EB, wave);
        
        if (i == 0) {
        	wave_0.set(wave);
        }
        
        wave.sub(wave_0);
        
        t += traceParameterControl.getWavyness();
	}

	/**
	 * Evaluates the helicoid orientation angle at this location.
	 * 
	 * @param p
	 *            the location in helicoid space.
	 * @return
	 */
	public double rightHelicoid(Point3d p) {
		return rightHelicoid(traceParameterControl.getParameter(), p.x, p.y, p.z);
	}

	public double rightHelicoid(HelicoidParameter hp, Point3d p) {
		return rightHelicoid(hp, p.x, p.y, p.z);
	}

	/**
	 * Evaluates the helicoid orientation angle at this location.
	 * 
	 * @param p
	 *            the location in helicoid space.
	 * @return
	 */
	public static double rightHelicoid(HelicoidParameter hp, double x, double y, double z) {
		double theta = Math.atan2(hp.getKT() * x + hp.getKN() * y, 1
				+ hp.getKN() * x - hp.getKT() * y) + hp.getKB() * z;
		
		// Remap the angle from [-pi, pi] to [0, 2pi]
		if (theta < 0)
			theta += 2 * Math.PI;

		return theta;
	}

	/**
	 * Update the current theta and increment the turn count if necessary.
	 * 
	 * @return The theta angle at this location.
	 */
	private void getTheta(Point3d p) {
		theta = rightHelicoid(p);

		// Use turn count
		theta += turn_count * 2 * Math.PI;
	}
	
	private void getTheta() {
		getTheta(helicPos);
	}

	private void updateTurning(boolean increment) {
		// Keep track of the number of 2Pi turns
		// important otherwise inconsistencies appear
		// between two successive angles.
		if (Math.abs(theta_p - theta) > Math.PI) {
			// Increment the turn count in the correct direction
			double s = Math.signum(theta_p - theta);
			theta += s * 2 * Math.PI;
			
			if (increment) 
				turn_count += s;
		}
	}

	/**
	 * @return The phi angle corresponding to the current theta.
	 */
	private double getPhi() {

//		phi = hp.alpha * (theta % Math.PI);
//		phi = traceParameterControl.getAlpha() * theta;
		phi = theta;
		
		return phi;
	}
	
	public double getPhi(double theta, HelicoidParameter hp) {
//		return theta * hp.alpha;
		return theta;
	}
	
	private void setHelicoidFrame(Vector3d tangent, double theta, double phi) {
		// The three orthogonal vector that define the current local frame
		// along the curve
		ET.set(tangent);
		EN.set(-Math.sin(theta), Math.cos(theta), 0);
		EB.set(-Math.sin(phi) * Math.cos(theta),-Math.sin(phi) * Math.sin(theta), Math.cos(phi));
		
		ET.normalize();
		EN.normalize();
		EB.normalize();
	}	

	public static OrientationFrame getHelicoidFrame(HelicoidParameter hp, Point3d p) {
		
		double theta = rightHelicoid(hp, p.x, p.y, p.z);
		
		Vector3d ET = new Vector3d();
		Vector3d EN = new Vector3d();
		Vector3d EB = new Vector3d();
		
//		double phi = hp.alpha * theta;
		double phi = theta;

		// The three orthogonal vector that define the current local frame
		// along the curve
		ET.set(Math.cos(phi) * Math.cos(theta), Math.cos(phi) * Math.sin(theta), Math.sin(phi));
		EN.set(-Math.sin(theta), Math.cos(theta), 0);
		EB.set(-Math.sin(phi) * Math.cos(theta),-Math.sin(phi) * Math.sin(theta), Math.cos(phi));
		
		ET.normalize();
		EN.normalize();
		EB.normalize();

		OrientationFrame E = new OrientationFrame(ET, EN, EB);
		
		return E;
	}	

}
