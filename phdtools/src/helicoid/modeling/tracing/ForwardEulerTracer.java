package helicoid.modeling.tracing;

import javax.vecmath.Vector3d;

public class ForwardEulerTracer extends HelicoidTracer {
	
	public ForwardEulerTracer(TraceParameterControl p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doInit() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doTrace(double h, double theta, double phi, Vector3d tangent) {
		
		super.getAngles();
		
		tangent.x = h * (Math.cos(phi) * Math.cos(theta));
		tangent.y = h * (Math.cos(phi) * Math.sin(theta));
		tangent.z = h * Math.sin(phi);
		
	}
}
