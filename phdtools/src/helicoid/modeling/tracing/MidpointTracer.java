package helicoid.modeling.tracing;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class MidpointTracer extends HelicoidTracer {

	public MidpointTracer(TraceParameterControl p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	private Vector3d k1;
	private Vector3d k2;
	private Point3d y;

	@Override
	public void doInit() {
		k1 = new Vector3d();
		k2 = new Vector3d();
		y = new Point3d();
	}

	@Override
	public void doTrace(double h, double theta, double phi, Vector3d tangent) {
		
		// k1 = f(t_n, y_n)
		y.set(super.helicPos);
		super.f(y, k1);

		// k2 = f(t_n + 1/2 * h, y_n + 1/2 * h * k_1)
		tangent.set(k1);
		tangent.scale(h / 2);
		y.add(helicPos, tangent);
		super.f(y, k2);

		// y_n+1 = y_n + h * f(h/2 * f(t_n, y_n))
		tangent.set(k2);
		tangent.scale(h);

		// Update theta for next step
		super.extractImplicitTheta();
	}
	
}
