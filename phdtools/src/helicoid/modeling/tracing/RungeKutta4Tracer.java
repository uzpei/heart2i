package helicoid.modeling.tracing;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class RungeKutta4Tracer extends HelicoidTracer {
	
	public RungeKutta4Tracer(TraceParameterControl p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	private Vector3d k1, k2, k3, k4;
	private Point3d y;

	@Override
	public void doInit() {
		k1 = new Vector3d();
		k2 = new Vector3d();
		k3 = new Vector3d();
		k4 = new Vector3d();
		y = new Point3d();
	}

	@Override
	public void doTrace(double h, double theta, double phi, Vector3d tangent) {
		
		// k1 = f(t_n, y_n)
		y.set(helicPos);
		f(y, k1);

		// k2 = f(t_n + 1/2 * h, y_n + 1/2 * h * k_1)
		tangent.set(k1);
		tangent.scale(h / 2);
		y.add(helicPos, tangent);
		f(y, k2);

		// k3 = f(t_n + 1/2 * h, y_n + 1/2 * h * k_2)
		tangent.set(k2);
		tangent.scale(h / 2);
		y.add(helicPos, tangent);
		f(y, k3);

		// k4 = f(t_n + h, y_n + h * k_3
		tangent.set(k3);
		tangent.scale(h);
		y.add(helicPos, tangent);
		f(y, k4);

		// y_n+1 = y_n + h/6 * (k_1 + 2*k_2 + 2*k_3 + k_4)
		tangent.set(k1);
		tangent.scaleAdd(2, k2, tangent);
		tangent.scaleAdd(2, k3, tangent);
		tangent.scaleAdd(1, k4, tangent);
		tangent.scale(h / 6);

		// Update theta for next step
		super.extractImplicitTheta();
	}
}
