package helicoid.modeling;


import helicoid.Helicoid;
import helicoid.PiecewiseHelicoid;
import helicoid.parameter.HelicoidParameter;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrameSample;
import tools.frame.OrientationFrame.AxisSampling;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.BoundingBox;

public class WispGenerator {
	private DoubleParameter spread = new DoubleParameter("spread", 0.06,
			0, 10);

	private DoubleParameter oscale = new DoubleParameter("offset scale", 50,
			0, 10);
	
	private IntParameter wdensity = new IntParameter("wisp divisions", 5, 0, 50);

	private DoubleParameter randomize = new DoubleParameter("Randomize", 0.6, 0, Math.PI);
	{
		randomize.setChecked(false);
	}

	private boolean holding = false;
	
	private PHelicoidGenerator generator;

	public boolean isHolding() {
		return holding;
	}
	
	public void setHold(boolean b) {
		holding  = b;
	}
	
	public void setOffsetScale(double v) {
		oscale.setValue(v);
	}

	public void setSpread(double v) {
		spread.setValue(v);
	}

	public WispGenerator(HelicoidGenerator generator) {
		this.generator = new PHelicoidGenerator(generator);
	}

	
	public WispGenerator(PHelicoidGenerator generator) {
		this.generator = generator;
	}
	
	public List<PiecewiseHelicoid> generatePiecewiseWisp(PiecewiseHelicoid ph) {

		// List of p-helicoids forming a wisp
		List<PiecewiseHelicoid> wisp = new LinkedList<PiecewiseHelicoid>();

		if (ph.size() == 0) return wisp;
		
		Point3d o = ph.getOrigin();
		double dist = spread.getValue();
		List<Point3d> sampling = new LinkedList<Point3d>();

		if (!inplane) {
			// First determine a grid sampling (patch) around the master hair
			List<Point3d> cloud = new LinkedList<Point3d>();

			// Two points are enough to specify the extent of the patch
			Point3d p0 = new Point3d(o.x - dist, 0, o.z - dist);
			Point3d p1 = new Point3d(o.x + dist, 0, o.z + dist);
			cloud.add(p0);
			cloud.add(p1);

			List<Point2d> bbox = BoundingBox.getGridSampling(wdensity.getValue(),
					cloud, false);

			// map sampling from 2D to 3D
			for (Point2d p : bbox) {
				sampling.add(new Point3d(p.x, 0, p.y));
			}
		}
		else {
			// First determine a grid sampling (patch) around the master hair
			List<Point3d> cloud = new LinkedList<Point3d>();

			// Two points are enough to specify the extent of the patch
			Point3d p0 = new Point3d(o.x, 0, o.z);
			Point3d p1 = new Point3d(o.x +  2* dist, 0, o.z + 2*dist);
			cloud.add(p0);
			cloud.add(p1);

			List<Point2d> bbox = BoundingBox.getGridSampling(wdensity.getValue(),
					cloud, false);

			// map sampling from 2D to 3D
			for (Point2d p : bbox) {
				sampling.add(new Point3d(p.x, 0, p.y));
			}
		}

		int i = 0;
		Point3d offset = new Point3d();
		Vector3d odir = new Vector3d();
        Matrix4d frame = new Matrix4d();
        AxisAngle4d arot = new AxisAngle4d();
        Matrix4d rot = new Matrix4d();
        
        // Determine a random rotation
        rot.setIdentity();
        Random rand = new Random();
		double rmag = randomize.getValue();

		// Center the wisp at the origin
		generator.getParameterControl().setOrigin(new Point3d());

		// Determine the list of parameters
		List<HelicoidParameter> parameters = new LinkedList<HelicoidParameter>();
		for (Helicoid h : ph) {
			parameters.add(h.getParameter());
		}

		// Compute a p-helicoid for each offset
		for (Point3d p : sampling) {
	        frame.set(ph.get(0).getRotationFrame());

			// Compute the offset from the 
			odir.sub(o, p);

			// offset.set(Math.signum(odir.x), 0, 0);
			offset.set(1 * Math.signum(ph.get(0).getParameter().KT), 0, 0);
			offset.scale(oscale.getValue() * odir.lengthSquared());

			generator.getParameterControl().setOffset(offset);
			PiecewiseHelicoid wph = generator.generate(parameters);
			
			if (randomize.isChecked()) {
                arot.set(0, 1, 0, -rmag / 2  + rand.nextDouble() * rmag);
                rot.setRotation(arot);
                
                frame.mul(frame, rot);
			}

            wph.transform(frame);

			wph.moveTo(p);

			wisp.add(wph);
			
			i++;
		}

		// Reset
		generator.getParameterControl().setOrigin(new Point3d(0, 0, 0));
		generator.getParameterControl().setOffset(new Point3d());
		
		return wisp;
	}
	
	public List<Helicoid> generateWisp(Helicoid h) {
		
		Point3d h0 = h.getOrigin();
		HelicoidParameter hparameter = h.getParameter();
		List<Helicoid> wisp = new LinkedList<Helicoid>();
		
		// First determine a grid sampling (patch) around the master hair
		// in helicoid space coordinates (master lives at 0,0,0)
		List<Point2d> cloud = new LinkedList<Point2d>();

		// Two points are enough to specify the extent of the patch
//		double spread = this.spread.getValue();
//		Point2d p0 = new Point2d(-spread, -spread);
//		Point2d p1 = new Point2d(+spread, +spread);
//		
//		if (wdensity.isChecked()) {
//			p0.y = 0;
//			p1.y = 0;
//		}
//		
//		cloud.add(p0);
//		cloud.add(p1);
//		List<Point2d> helicoidsampling = BoundingBox.get2DGridSampling(wdensity.getValue(),
//				cloud, wdensity.isChecked());
		
//		System.out.println("Sampling count = " + helicoidsampling.size());
		
		Point3d offset = new Point3d();
		Vector3d odir = new Vector3d();
        AxisAngle4d arot = new AxisAngle4d();
        Matrix4d rot = new Matrix4d();
        rot.setIdentity();
        Random rand = new Random();
		double rmag = randomize.getValue();

		// Put the wisp helicoid in the same frame as the master's
        Matrix4d localFrame = new Matrix4d(h.getLocalFrame());
        localFrame.setColumn(3, h0.x, h0.y, h0.z, 1);
        
        // TODO: preserve parameter listeners
		generator.getParameterControl().setOffset(new Point3d());
		generator.setOrientationFrameToIdentity();
//        generator.setStepCount(20);

		Matrix4d m = new Matrix4d();
		m.setIdentity();
		CoordinateFrameSample cfs = new CoordinateFrameSample(m);
	
//		int[] samples = new int[] {wdensity.getValue(), wdensity.getValue(), wdensity.getValue()};
		int[] samples = new int[] { wdensity.getValue(), wdensity.getValue(), wdensity.getValue() };
		double[] scales = new double[] { 1, 1, 1 };
		SamplingStyle samplingStyle = SamplingStyle.make(AxisSampling.ZERO, AxisSampling.ZERO, AxisSampling.NEG_POS);
		List<Point3d> helicoidSampling = cfs.presampleDouble(samples, scales, samplingStyle);
		
//        Point3d p = new Point3d();
//		for (Point2d p2d : helicoidsampling) {
		for (Point3d p : helicoidSampling) {
//			p.set(0, p2d.x, p2d.y);
			
//			if (wdensity.isChecked()) p.z = 0;

			// Must map the x-z plane offset to the x-y plane
//			offset.scale(oscale.getValue() / spread, p);
			offset.scale(oscale.getValue(), p);
			
			generator.getParameterControl().setOffset(offset);
			generator.getParameterControl().setOrigin(p);
			
			// Generate a wisp helicoid
			Helicoid hs = generator.generate(hparameter);
			
			// Transform to local frame
			hs.transform(localFrame);
			
			if (randomize.isChecked()) {
                arot.set(0, 1, 0, -rmag / 2  + rand.nextDouble() * rmag);
                rot.setRotation(arot);
                hs.transform(rot);
                localFrame.mul(rot);
    			localFrame.setTranslation(new Vector3d(p.x,p.y,p.z));
			}
			
			wisp.add(hs);
		}
		
//		System.out.println("Wisp count = " + wisp.size());
		
		generator.getParameterControl().setOffset(new Point3d());
		generator.getParameterControl().setOrigin(new Point3d());

		return wisp;
	}

	public List<Helicoid> generateWispOld(Helicoid h) {

		Point3d o = h.getOrigin();
		double dist = spread.getValue();
		List<Helicoid> wisp = new LinkedList<Helicoid>();
		
		List<Point3d> sampling = new LinkedList<Point3d>();

		if (!inplane) {
			// First determine a grid sampling (patch) around the master hair
			List<Point3d> cloud = new LinkedList<Point3d>();

			// Two points are enough to specify the extent of the patch
			Point3d p0 = new Point3d(o.x - dist, 0, o.z - dist);
			Point3d p1 = new Point3d(o.x + dist, 0, o.z + dist);
			cloud.add(p0);
			cloud.add(p1);

			List<Point2d> bbox = BoundingBox.getGridSampling(wdensity.getValue(),
					cloud, false);

			// map sampling from 2D to 3D
			for (Point2d p : bbox) {
				sampling.add(new Point3d(p.x, 0, p.y));
			}
		}
		else {
			// First determine a grid sampling (patch) around the master hair
			List<Point3d> cloud = new LinkedList<Point3d>();

			// Two points are enough to specify the extent of the patch
//			Point3d p0 = new Point3d(o.x, 0, o.z - dist);
//			Point3d p1 = new Point3d(o.x, 0, o.z + dist);
			double sign = -Math.signum(generator.getParameter().KN);
			sign = sign == 0? 1 : sign;
			Point3d p0 = new Point3d(o.x, 0, o.z);
			Point3d p1 = new Point3d(o.x, 0, o.z + 2*dist*sign);
			cloud.add(p0);
			cloud.add(p1);

			List<Point2d> bbox = BoundingBox.getGridSampling(wdensity.getValue(),
					cloud, false);

			// map sampling from 2D to 3D
			for (Point2d p : bbox) {
				sampling.add(new Point3d(p.x, p.y, 0));
			}
		}

		generator.getParameterControl().setOrigin(new Point3d());

		int i = 0;
		Point3d offset = new Point3d();
		Vector3d odir = new Vector3d();
        Matrix4d frame = new Matrix4d();
        AxisAngle4d arot = new AxisAngle4d();
        Matrix4d rot = new Matrix4d();
        rot.setIdentity();
        Random rand = new Random();
		double rmag = randomize.getValue();

		for (Point3d p : sampling) {
	        frame.set(generator.getOrientationFrame());

			// Must map the x-z plane offset to the x-y plane
			odir.sub(p, o);

			if (!inplane) {
				// offset.set(Math.signum(odir.x), 0, 0);
				offset.set(1 * Math.signum(h.getParameter().KT), 0, 0);
				offset.scale(oscale.getValue() * odir.lengthSquared());
			}
			else {
				offset.set(odir);
//				odir.scale(oscale.getValue());
//				odir.add(o);
				odir.set(p);
			}
			
			generator.getParameterControl().setOffset(offset);
			Helicoid hs = generator.generate(h.getParameter());
			hs.moveTo(new Point3d(p));
			hs.setLocalFrame(frame);
			
			if (randomize.isChecked()) {
//                arot.set(0, 1, 0, -2*rmag + rand.nextDouble() * 2*rmag);
//                arot.set(0, 1, 0, -rmag + rand.nextDouble() * rmag);
                arot.set(0, 1, 0, -rmag / 2  + rand.nextDouble() * rmag);
                rot.setRotation(arot);
                hs.transform(rot);
                frame.mul(rot);
    			frame.setTranslation(new Vector3d(p.x,p.y,p.z));
			}

			wisp.add(hs);
			
			i++;
		}

		// Reset
		generator.getParameterControl().setOrigin(new Point3d(0, 0, 0));
		generator.getParameterControl().setOffset(new Point3d());
		
		return wisp;
	}
	
	public JPanel getControls() {
		VerticalFlowPanel wp = new VerticalFlowPanel();
		wp.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Wisps"));

		wp.add(wdensity.getSliderControlsExtended("linear"));
		wdensity.setChecked(false);
		wp.add(spread.getSliderControls());
		wp.add(oscale.getSliderControls());
		wp.add(randomize.getSliderControlsExtended());

		return wp.getPanel();
	}
	
	public void addParameterListener(ParameterListener wl) {
		wdensity.addParameterListener(wl);
		spread.addParameterListener(wl);
		oscale.addParameterListener(wl);
		randomize.addParameterListener(wl);
	}

	private boolean inplane = false;
	public void setInplane(boolean b) {
		inplane = b;
	}

	public void setLinear(boolean b) {
		wdensity.setChecked(b);
	}

	public double getSpread() {
		return spread.getValue();
	}

}
