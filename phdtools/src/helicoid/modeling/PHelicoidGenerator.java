/**
 * 
 */
package helicoid.modeling;

import helicoid.Helicoid;
import helicoid.PiecewiseHelicoid;
import helicoid.parameter.HelicoidParameter;

import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector4d;

/**
 * @author piuze
 *
 */
public class PHelicoidGenerator extends HelicoidGenerator {

	public PHelicoidGenerator() {
		// Empty constructor
	}
	
    public PHelicoidGenerator(HelicoidGenerator generator) {
    	set(generator);
	}

	/**
     * @param n the number of segments (helicoids) attached back to back
     * @return the multihair generated.
     */
    public PiecewiseHelicoid generateRandomPHelicoids(int n) {
    	Matrix4d m = new Matrix4d();
    	m.setIdentity();
    	return generateRandomPHelicoids(n, m, m, new Point3d());
    }
    
    /**
     * @param n the number of pieces of the p-helicoid.
     * @param mag the magnitude of the helicoid parameters.
     * @param frame the full local frame (including world AND rotation)
     * @param rotation the rotation frame alone (extracted from the frame)
     * @param origin the origin of the p-helicoid.
     * @return
     */
    public PiecewiseHelicoid generateRandomPHelicoids(int n, Matrix4d frame, Matrix4d rotation, Point3d origin) {
    	return generateRandomPHelicoids(n, 1, frame, rotation, origin);
    }

    /**
     * @param n the number of pieces of the p-helicoid.
     * @param mag the magnitude of the helicoid parameters.
     * @param frame the full local frame (including world AND rotation)
     * @param rotation the rotation frame alone (extracted from the frame)
     * @param origin the origin of the p-helicoid.
     * @return
     */
    public PiecewiseHelicoid generateRandomPHelicoids(int n, double mag, Matrix4d frame, Matrix4d rotation, Point3d origin) {

		List<HelicoidParameter> list = new LinkedList<HelicoidParameter>();
    	for (int i = 0; i < n ; i++) {
    		HelicoidParameter hp = new HelicoidParameter();
    		hp.randomize();
    		hp.scale(mag);
    		
//    		hp.set(0.5, 0, 0, 0);
    		list.add(hp);
    	}
    	return generate(list, frame, rotation, origin);
    }
    
    private void tesselate(PiecewiseHelicoid ph) {
    	// FIXME: this is currently broken. The subtle point is that for
    	// a tesselation to make sense (in terms of interpolation) a new trace
    	// scale has to be found
//    	ph.tesselate(getStepCount());
    }

    public PiecewiseHelicoid generate(List<HelicoidParameter> parameters) {
    	Matrix4d identity = new Matrix4d();
    	identity.setIdentity();
    	return generate(parameters, identity, identity);
    }
    public PiecewiseHelicoid generate(List<HelicoidParameter> parameters, Matrix4d frame, Matrix4d rotation) {
    	return generate(parameters, frame, rotation, new Point3d());
    } 
    
    public PiecewiseHelicoid generate(List<HelicoidParameter> parameters, Matrix4d frame, Matrix4d rotation, Point3d origin) {
    	PiecewiseHelicoid ph = new PiecewiseHelicoid();

    	// Number of parameters
    	int n = parameters.size();
    	
    	if (n == 0) return null;

    	// Initial trace scale
       	double tscale = getParameterControl().getTraceScale();
       	getParameterControl().setTraceScale(tscale / n);

    	// Set frame as identity since we take care of this manually
    	super.setOrientationFrameToIdentity();
    	
    	// Set origin
    	getParameterControl().setOriginToZero();
    	
    	// Maintain phase information for waviness offset
    	boolean b = getParameterControl().isWavinessRandom();
    	double t_0 = b ? 0.7 * rand.nextDouble() * 2 * Math.PI : 0;
    	setInitialPhase(t_0);
    	for (int i = 0; i < n ; i++) {
//    		System.out.println("-Generating piece # " + i);
    		// Update after first
    		if (i > 0) {
    			hold (true);
    			getParameterControl().setWavinessRandom(false);
    			
    			setInitialPhase(t_0 - 2*getParameterControl().getWavyness());
//    			setInitialPhase(t_0);
    			hold(false);
    		}
    		Helicoid h = generate(parameters.get(i));
    		t_0 = h.getWavyPhase()[1];
//    		ph.add(h, false);
    		ph.add(h, true);
    	}
//    	System.out.println("T final = " + t_0);
    	
    	hold(true);
    	getParameterControl().setWavinessRandom(b);
    	hold(false);

    	// First add rotation
    	// considering that the hair was grown along the X-axis. 
    	ph.transform(rotation);
		ph.get(0).setRotationFrame(rotation);
    	
    	// Then place the hair in the appropriate world frame
    	ph.transform(frame);
       	ph.get(0).setLocalFrame(frame);
    	
    	// Give a warning if the frame is not centered at zero
    	Vector4d c3 = new Vector4d();
    	frame.getColumn(3, c3);
    	if (c3.x * c3.x + c3.y * c3.y + c3.z * c3.z != 0) {
    		System.err.println("Warning in PHelicoidGenerator: specified world frame has a non-zero translational component.");
    	}
    	
    	// Move the hair to the origin of the frame
    	ph.moveTo(origin);

    	getParameterControl().setTraceScale(tscale);

    	tesselate(ph);
    	
    	return ph;
    }
}
