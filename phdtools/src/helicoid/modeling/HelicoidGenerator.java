package helicoid.modeling;

import helicoid.Helicoid;
import helicoid.PiecewiseHelicoid;
import helicoid.modeling.tracing.ForwardEulerTracer;
import helicoid.modeling.tracing.HelicoidTracer;
import helicoid.modeling.tracing.HelicoidTracer.TracingMethod;
import helicoid.modeling.tracing.MidpointTracer;
import helicoid.modeling.tracing.RungeKutta4Tracer;
import helicoid.modeling.tracing.TraceParameterControl;
import helicoid.parameter.HelicoidParameter;
import helicoid.parameter.HelicoidParameterNode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.parameters.ParameterManager;
import tools.SimpleTimer;
import tools.frame.OrientationFrame;
import tools.geom.PlottingTools;
import tools.geom.Polyline;
import tools.geom.PolylineAnalysis;

public class HelicoidGenerator extends ParameterManager implements Serializable {

    private static final long serialVersionUID = -5137060840024297967L;

    private BooleanParameter showTrace = new BooleanParameter("show trace",
            false);

    private BooleanParameter normalize = new BooleanParameter("normalize", false);
    
    
    private BooleanParameter showFrenet = new BooleanParameter("show Frenet", false);
    
    private double cutoff_mag;
    private DoubleParameter useCutoff = new DoubleParameter("use cutoff", 1.5,
    		0, 3);

    private Helicoid generatedStrand = null;

    private HelicoidFieldVisualizer visualizer;

    private HelicoidTracer tracer;
    /**
     * A world frame transformation to be applied on any generated strand.
     */
    private Matrix4d orientationFrame = new Matrix4d();
    {
        orientationFrame.setIdentity();
    }

	public HelicoidTracer getTracer() {
		return tracer;
	}
	
	protected TraceParameterControl traceParameterControl = new TraceParameterControl();

    private double[] angles = null;

    private double[] angles2 = null;
    
    protected ParameterListener generateListener = new ParameterListener() {

        @Override
        public void parameterChanged(Parameter parameter) {
            if (isHolding() || traceParameterControl.isHolding()) {
                return;
            } else {
                generate();
            }
        }
    };


    public HelicoidGenerator() {
        visualizer = new HelicoidFieldVisualizer(this);

        setDefaults();
        
        setTracingMethod(HelicoidTracer.DEFAULT_TRACING_METHOD);

        smooth.setChecked(false);
        
        useCutoff.setChecked(false);
        cutoff_mag = useCutoff.getDefaultValue();
        
    	super.addParameters(traceParameterControl.getParameters());
    	super.addParameter(smooth);
    	super.addParameter(smoothmag);
    	super.addParameter(normalize);
    	super.addParameter(ecb);
    	
        super.addParameterListener(generateListener);
    }
    
    public HelicoidGenerator(HelicoidGenerator other) {
        this();
        
        set(other);
    }
    
    public TraceParameterControl getParameterControl() {
    	return traceParameterControl;
    }

    public void set(HelicoidGenerator other) {
        hold(true);
        this.traceParameterControl.set(other.getParameterControl());
        
        this.orientationFrame.set(other.orientationFrame);
        this.cutoff_mag = other.cutoff_mag;
        this.useCutoff.setChecked(other.useCutoff.isChecked());
        this.useCutoff.setValue(other.useCutoff.getValue());
        
        // TODO: use other's method
        setTracingMethod(HelicoidTracer.DEFAULT_TRACING_METHOD);

        super.removeParameterListeners();
        for (ParameterListener pl : other.getParameterListeners()) {
        	super.addParameterListener(pl);
        }
        hold(false);
    }
    
	final EnumComboBox<TracingMethod> ecb = new EnumComboBox<TracingMethod>(HelicoidTracer.DEFAULT_TRACING_METHOD);

    public void setOrientationFrame(Matrix4d m) {
        orientationFrame.set(m);
    }
    public Matrix4d getOrientationFrame() {
        return orientationFrame;
    }

    public Helicoid generate(HelicoidParameter hp, Point3d origin) {
        hold(true);
        traceParameterControl.setOrigin(origin);
        hold(false);
        return forceGenerate(hp);
    }    
    
    private Point3d zero = new Point3d();
    
	public void generateSinglePlanarDirection(HelicoidParameter hp, Point3i p, Vector3d result) {
		
		double theta = Math.atan2(hp.getKT() * p.x + hp.getKN() * p.y, 1
				+ hp.getKN()* p.x - hp.getKT()* p.y) + hp.getKB()* p.z;

		result.x = Math.cos(theta);
		result.y = Math.sin(theta);
		result.z = 0;
		
//		result.normalize();
	}

	public void generateSingleDirection(HelicoidParameter hp, Point3d p, Vector3d result) {
		
		double theta = Math.atan2(hp.getKT() * p.x + hp.getKN() * p.y, 1
				+ hp.getKN() * p.x - hp.getKT() * p.y) + hp.getKB() * p.z;
//		double phi = hp.alpha * theta;
		double phi = theta;
		
		result.x = Math.cos(theta) * Math.cos(phi);
		result.y = Math.sin(theta) * Math.cos(phi);
		result.z = Math.sin(phi);
		
//		result.normalize();
	}

    public void generateSingleOrientation(HelicoidParameter hp, Point3d offset, Vector3d result) {
		generateSingleOrientation(hp, offset.x, offset.y, offset.z, result);
    }
    
    private SimpleTimer timer1 = new SimpleTimer();
    private SimpleTimer timer2 = new SimpleTimer();
    /**
     * Generate a single orientation and returns it
     * @param hp
     * @param offset
     * @return
     */
    public void generateSingleOrientation(HelicoidParameter hp, double ox, double oy, double oz, Vector3d result) {
//    	System.out.println();
//    	String t; 
//    	
//    	timer1.tick();
//    	timer2.tick();
    	
    	hold(true);
//        t = timer1.tick_ms();
//        System.out.println("Hold true: " + t);
//        timer1.tick();
        
        tracer.doTraceSingle(hp, ox, oy, oz, result);
        
//        t = timer1.tick_ms();
//        System.out.println("Generate: " + t);
//        timer1.tick();
        
        hold(false);
//        t = timer1.tick_ms();
//        System.out.println("Hold false: " + t);
//        timer1.tick();
        
//        t = timer2.tick_ms();
//        System.out.println("Total time = " + t);
    }

    public Helicoid generate() {
        return generate(traceParameterControl.getParameter(), false);
    }

    public Helicoid generate(HelicoidParameter v) {
        return generate(v, false);
    }
    
    private Helicoid generate(HelicoidParameter v, boolean set) {
        if (set) 
        	traceParameterControl.setParameter(v);
        return forceGenerate(v);
    }
    
	public List<Helicoid> generate(HelicoidParameterNode node, List<Point3d> pts, OrientationFrame frame) {
		HelicoidParameterNode hp = node;

		Point3d pto = new Point3d();
		
		setOrientationFrame(frame.getFrame());
		
        List<Helicoid> helicoids = new LinkedList<Helicoid>();

		for (Point3d p : pts) {
			pto.add(node.getPosition(), p);
	        traceParameterControl.setOrigin(pto);
			traceParameterControl.setOffset(p);

//	        helicoids.add(forceGenerate(hp));
			Vector3d v = new Vector3d();
	        tracer.doTraceSingle(hp, p.x, p.y, p.z, v);
	        frame.transform(v);
	        
	        List<Point3d> pth = new LinkedList<Point3d>();
	        pth.add(new Point3d(pto));
	        v.add(pto);
	        pth.add(new Point3d(v.x, v.y, v.z));
	        		
	        Helicoid h = new Helicoid(pth, hp);
	        
	        helicoids.add(h);
		}
		
		return helicoids;
	}

    protected double t_0 = 0;
    
    public void setInitialPhase(double t) {
    	t_0 = t % (2*Math.PI);
    }
    
	protected Helicoid forceGenerate(HelicoidParameter v) {
//		hold(true);
		
		// FIXME: Should only really set when necessary, to speed things up
//		System.out.print("-> Force generation: ");
//		tracer.useWaviness(wavyradius.isChecked());
//		tracer.setWaviness(wavyradius.getValue(), wavypitch.getValue(),
//				wavypitch.isChecked(), t_0);
//		tracer.setParameters(v, offset, getStepCount(), getStepSkip(),
//				getStepSize(), getTraceScale(), orientationFrame, origin);
//		tracer.setLengthRandomness(lengthrand.getValue());

		traceParameterControl.setParameter(v);
		tracer.setOrientationFrame(orientationFrame);
		
//		System.out.println("Calling tracer...");
		Helicoid h = tracer.trace();

		// Add noise
		if (traceParameterControl.isNoiseEnabled()) {
			Vector3d nv = new Vector3d();
			for (Point3d p : h) {
				nv.x += traceParameterControl.getNoise() * rand.nextGaussian();
				nv.y += traceParameterControl.getNoise() * rand.nextGaussian();
				nv.z += traceParameterControl.getNoise() * rand.nextGaussian();
				p.add(nv);
			}
		}

		generatedStrand = h;

		if (smooth.isChecked()) {
			smooth(h.getPoints());
		}

//		hold (false);
		return h;
	}

	/**
	 * @param drawable
	 */
    public void display(GLAutoDrawable drawable) {

		GL2 gl = drawable.getGL().getGL2();

        boolean displayPlot = false;

        boolean drawCurvature = false;

        if (displayPlot) {
            double maxval = 2 * Math.PI;
            float[] thetaColor = new float[] { 1, 0, 0, 1 };
            PlottingTools.display(drawable, angles, 0, maxval, 0.2,
                    thetaColor);
            float[] phiColor = new float[] { 1, 0, 1, 1 };
            PlottingTools.display(drawable, angles2, 0, maxval, 0.2,
                    phiColor);
        }

        // Display the current template hair.
        if (generatedStrand == null)
            generate();

        if (generatedStrand != null && drawCurvature)
        	PolylineAnalysis.displayCurvature(generatedStrand, drawable);


//        SurfaceModeler s = new SurfaceModeler();
//        s.setDimension(10, 10);
//        gl.glPushMatrix();
//        gl.glRotated(90, 1, 0, 0);
//        gl.glScaled(1, 0, 1);
//        s.display(drawable);
//        gl.glPopMatrix();

        gl.glDisable(GL2.GL_LIGHTING);
        
        if (showTrace.getValue()) {
            // Projection on the x-y plane
            Helicoid h = new Helicoid(generatedStrand);
            for (Point3d p : h) {
            	p.z = 0;
            }
            // Display the generated strand
//            gl.glLineWidth(7);

            double mag = 0.2;
            gl.glColor4d(mag, mag, mag, 0.5);
            h.display(drawable);

            gl.glColor3d(0, 0, 1);
            generatedStrand.display(drawable);
        }

        if (showFrenet.getValue() && generatedStrand != null)
        	HelicoidGenerator.displayFrenet(drawable, generatedStrand);

        // Display the helicoid vector field
        visualizer.display(drawable);
    }
    
    public static void displayFrenet(GLAutoDrawable drawable, Helicoid h) {
        (new PolylineAnalysis()).displayFrenet(h, drawable);
    }

    public static void displayFrenet(GLAutoDrawable drawable, PiecewiseHelicoid h) {
        (new PolylineAnalysis()).displayFrenet(new Polyline(h.getPoints()), drawable);
    }

    public Helicoid getRandomHair(double mag) {
        return getRandomHairs(1, mag).get(0);
    }

    public Helicoid getRandomHair() {
        return getRandomHairs(1, HelicoidParameter.DEFAULT_RANDOM_PARAMETER_MAG).get(0);
    }

    public List<Helicoid> getRandomHairs(int n) {
        return getRandomHairs(n, HelicoidParameter.DEFAULT_RANDOM_PARAMETER_MAG);
    }

    public List<Helicoid> getRandomHairs(int n, double mag) {
    	double s = traceParameterControl.getTraceScale();
        List<Helicoid> hairs = new LinkedList<Helicoid>();
        for (int i = 0; i < n; i++) {
        	traceParameterControl.setParameter((HelicoidParameter) HelicoidParameter.getRandom(mag));
        	traceParameterControl.setTraceScale(s * 0.5 * ( 1 + rand.nextDouble()));
            Helicoid hair = generate();
            hairs.add(hair);
        }
        
        traceParameterControl.setTraceScale(s);

        return hairs;
    }

    /**
     * @return UI controls for this class.
     */
    public JPanel getControls() {
        VerticalFlowPanel p = new VerticalFlowPanel();
        
        VerticalFlowPanel hpanel = new VerticalFlowPanel();
        hpanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Helicoid parameters"));

        JButton btnReset = new JButton("Reset");
        btnReset.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setDefaults();
                generate();
            }
        });
        hpanel.add(btnReset);

        hpanel.add(traceParameterControl.getHelicoidControls());
        hpanel.add(useCutoff.getSliderControlsExtended());
        
		ecb.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				setTracingMethod(ecb.getSelected());
			}
		});
		hpanel.add(ecb.getControls());

        useCutoff.addParameterListener(new ParameterListener() {

            @Override
            public void parameterChanged(Parameter parameter) {
                cutoff_mag = useCutoff.getValue();
            }
        });

        hpanel.add(traceParameterControl.getOtherControls());
        
        hpanel.add(smooth.getSliderControlsExtended());
        hpanel.add(smoothmag.getSliderControlsExtended());

        VerticalFlowPanel opanel = new VerticalFlowPanel();
        opanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Others"));
        opanel.add(showFrenet.getControls());
        opanel.add(showTrace.getControls());
        opanel.add(normalize.getControls());

        p.add(hpanel.getPanel());
        p.add(visualizer.getControls());
        
//        p.add(opanel.getPanel());
        CollapsiblePanel cpo = new CollapsiblePanel(opanel.getPanel(), "Others");
        cpo.collapse();
        p.add(cpo);
        return p.getPanel();
    }

    public void smoothHelicoids(List<Helicoid> hairs, int numcycles) {
    	if (numcycles <= 0) return;
    	
    	for (Helicoid h : hairs) {
    		smooth(h.getPoints(), numcycles,smoothmag.getValue());
    	}
    }

    public void smooth(List<Point3d> pts) {
    	smooth(pts, smooth.getValue(), smoothmag.getValue());
    }
    
    public void smooth(List<Point3d> pts, double smoothm) {
    	Polyline.smooth(pts, smooth.getValue(), smoothm);
    }

    public void smooth(List<Point3d> pts, int numcycles, double smoothm) {
    	Polyline.smooth(pts, numcycles, smoothm);
    }
    
    public Polyline getHair() {
        return generatedStrand;
    }
    
	public void cut(HelicoidParameter q) {
		q.cut(cutoff_mag);
	}

    public void setDefaults() {
        hold(true);
        traceParameterControl.setDefaults();
        orientationFrame.setIdentity();
        hold(false);
    }

    public void setTrace(boolean enabled) {
        showTrace.setValue(enabled);
    }

    public void setVisualize(boolean enabled) {
        visualizer.setVisible(enabled);
    }

    public void normalize(boolean b) {
        normalize.setValue(b);
    }

    public void setSmoothingCycles(int n) {
    	smooth.setValue(n);
    	smooth.setChecked(true);
    }

    public int getSmoothingCycles() {
    	return smooth.getValue();
    }


	public boolean isUsingCutoff() {
		return useCutoff.isChecked();
	}
	
	/**
	 * @return
	 */
	public boolean isNormalizing() {
		return normalize.getValue();
	}

	protected IntParameter smooth = new IntParameter("smoothing cycles", 0, 0, 10);

	protected DoubleParameter smoothmag = new DoubleParameter("smoothing mag", 0.5, 0, 1);
    
	protected static Random rand = new Random();


    public HelicoidFieldVisualizer getVisualizer() {
		return visualizer;
	}

	public void setOrientationFrameToIdentity() {
		orientationFrame.setIdentity();
	}

	public void setTracingMethod(TracingMethod method) {
		switch (method) {
		case FORWARD_EULER:
			tracer = new ForwardEulerTracer(traceParameterControl);
			break;
		case MIDPOINT:
			tracer = new MidpointTracer(traceParameterControl);
			break;
		case RK_4:
			tracer = new RungeKutta4Tracer(traceParameterControl);
			break;
		default:
			tracer = new ForwardEulerTracer(traceParameterControl);
		}
	}
	
	public void setParameter(HelicoidParameter parameter) {
		traceParameterControl.setParameter(parameter);
	}
	
	public HelicoidParameter getParameter() {
		return traceParameterControl.getParameter();
	}
}
