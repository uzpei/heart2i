package helicoid.modeling;

import helicoid.parameter.HelicoidParameter;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.text.GreekToolset.GreekLetter;

public class HelicoidFieldVisualizer {
	private HelicoidGenerator sg;

	private DoubleParameter oX = new DoubleParameter("x", 0, -10, 10);

	private DoubleParameter oY = new DoubleParameter("y", 0, -10, 10);

	private DoubleParameter oZ = new DoubleParameter("z", 0, -10, 10);

	private DoubleParameter dX = new DoubleParameter(GreekLetter.DELTA.code
			+ "x", 0, 0, 100);

	private DoubleParameter dY = new DoubleParameter(GreekLetter.DELTA.code
			+ "y", 3, 0, 100);

	private DoubleParameter dZ = new DoubleParameter(GreekLetter.DELTA.code
			+ "z", 3, 0, 100);

	private DoubleParameter density = new DoubleParameter("density", 15, 0,
			1000);

	private DoubleParameter scale = new DoubleParameter("scale", 0.18, 0, 1);

	private BooleanParameter draw = new BooleanParameter("draw", false);

	private BooleanParameter draws = new BooleanParameter("draw singularity",
			false);

	public HelicoidFieldVisualizer(HelicoidGenerator generator) {
		sg = generator;
	}

	public void enableDrawing() {
		draw.setValue(true);
	}

	public void disableDrawing() {
		draw.setValue(false);
	}
	
	public void setSize(double dx, double dy, double dz) {
		dX.setValue(dx);
		dY.setValue(dy);
		dZ.setValue(dz);
	}
	
	public void setScale(double s) {
		scale.setValue(s);
	}
	
	public void setDensity(double d) {
		density.setValue(d);
	}

	public JPanel getControls() {
		VerticalFlowPanel p = new VerticalFlowPanel();
		p.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Visualization"));
		p.add(draw.getControls());
		p.add(draws.getControls());

		p.add(oX.getSliderControls(false));
		p.add(oY.getSliderControls(false));
		p.add(oZ.getSliderControls(false));
		p.add(dX.getSliderControls(false));
		p.add(dY.getSliderControls(false));
		p.add(dZ.getSliderControls(false));
		p.add(density.getSliderControls(false));
		p.add(scale.getSliderControls(false));

		CollapsiblePanel cpanel = new CollapsiblePanel(p.getPanel(),
				"Visualization");
		cpanel.collapse();
		return cpanel;
	}

	public void displaySingularity(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glDisable(GL2.GL_LIGHTING);

		gl.glLineWidth(1);

		HelicoidParameter p = sg.getParameterControl().getParameter();

		double x0, xf, y0, yf;
		
		// First line
		x0 = oX.getValue() - dX.getValue() / 2;
		xf = oX.getValue() + dX.getValue() / 2;
		y0 = (1 + p.getKN() * x0) / p.getKT();
		yf = (1 + p.getKN() * xf) / p.getKT();

		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(0, 1, 1);
		gl.glVertex3d(x0, y0, 0);
		gl.glVertex3d(xf, yf, 0);
		gl.glEnd();

		// Second line
		x0 = oY.getValue() - dY.getValue() / 2;
		xf = oY.getValue() + dY.getValue() / 2;
		y0 = -p.getKT() * x0 / p.getKN();
		yf = -p.getKT() * xf / p.getKN();

		if (Double.isInfinite(y0)) {
			y0 = x0;
			yf = xf;
			x0 = 0;
			xf = 0;
		}

		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(1, 0, 1);
		gl.glVertex3d(x0, y0, 0);
		gl.glVertex3d(xf, yf, 0);
		gl.glEnd();

		// Intersection
		x0 = - p.getKN() / (p.getKN()*p.getKN()+ p.getKT()* p.getKT());
		y0 = -p.getKT() * x0 / p.getKN();
		y0 = (1 + p.getKN() * x0) / p.getKT();
		gl.glPointSize(10);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex3d(x0, y0, 0);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(1, 0, 0);
		gl.glVertex3d(x0, y0, -dZ.getValue()/2);
		gl.glVertex3d(x0, y0, dZ.getValue()/2);
		gl.glEnd();

	}
	
	public void display(GLAutoDrawable drawable) {
		if (!draw.getValue())
			return;

		GL2 gl = drawable.getGL().getGL2();

		if (draws.getValue())
			displaySingularity(drawable);

		gl.glDisable(GL2.GL_LIGHTING);

		gl.glPushMatrix();

		double dscale = sg.isNormalizing() ? 1 / (sg.getParameterControl().getStepCount() * sg.getParameterControl()
				.getStepSize()) : 1;
		gl.glScaled(dscale, dscale, dscale);

		// Recompute the vector field on every frame
		HelicoidParameter hp = sg.getParameter();
		double hscale = scale.getValue();

		double x0 = oX.getValue() - dX.getValue() / 2;
		double y0 = oY.getValue() - dY.getValue() / 2;
		double z0 = oZ.getValue() - dZ.getValue() / 2;

		double xf = oX.getValue() + dX.getValue() / 2;
		double yf = oY.getValue() + dY.getValue() / 2;
		double zf = oZ.getValue() + dZ.getValue() / 2;

		double sx = dX.getValue() / density.getValue() + 1e-5;
		double sy = dY.getValue() / density.getValue() + 1e-5;
		double sz = dZ.getValue() / density.getValue() + 1e-5;

		gl.glColor4f(0, 0, 0, 0.8f);
		gl.glLineWidth(2);

//		gl.glEnable(GL2.GL_DEPTH_TEST);

		Vector3d v = new Vector3d();
		double dp = hscale/2;
		for (double x = x0; x <= xf; x += sx) {
			for (double y = y0; y <= yf; y += sy) {
				for (double z = z0; z <= zf; z += sz) {
					sg.getTracer().doTraceSingle(hp, x, y, z, v);
					v.normalize();
					v.scale(hscale);

					gl.glBegin(GL2.GL_LINES);
					gl.glVertex3d(x - v.x/2, y-v.y/2, z-v.z/2);
					gl.glVertex3d(x + v.x/2, y + v.y/2, z + v.z/2);
					gl.glEnd();

				}
			}
		}

		gl.glPopMatrix();
	}

	public void setVisible(boolean enabled) {
		draw.setValue(enabled);
	}

}
