package helicoid.parameter;


import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

/**
 * Extends a helicoid parameter by adding
 * a spatial location.
 * @author piuze
 */
public class HelicoidParameterNode extends HelicoidParameter {
    
	private static final long serialVersionUID = 7230635154287978378L;
	
	public double sigma;
    private Point3d p = new Point3d();
    
    public HelicoidParameterNode() {
        // TODO
    }
    
    public HelicoidParameterNode(HelicoidParameter hp) {
    	this(hp, new Point3d());
    }
    
    public HelicoidParameterNode(Point3d position, HelicoidParameter param, double radius) {
        sigma = radius;
        super.set(param);
        p.set(position);
    }
    
    public HelicoidParameterNode(HelicoidParameter hp,
			Point3d p) {
    	this(p, hp, 0);
	}
    
    public HelicoidParameterNode(HelicoidParameter hp, Point3i p) {
    	this(new Point3d(p.x, p.y, p.z), hp, 0);
	}

	public HelicoidParameterNode(HelicoidParameterNode other) {
		this(other, other.p);
		setError(other.getError());
		setGarbage(other.isGarbage());
	}

	public void setValue(ParameterType t, double value, double radius) {
		set(t, value);
        sigma = radius;
    }
    
    public Point3d getPosition() {
        return p;
    }
    
    public double getSigma() {
        return sigma;
    }
    
    public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        
        int i = 0;
        int n = 10;
        double dt = 2*Math.PI / n;
        Point3d dp = new Point3d();
        
        gl.glLineWidth(2);
        gl.glBegin(GL2.GL_LINE_STRIP);
        for (double angle = 0; angle <= 2*Math.PI; angle += dt) {
            // Will need to transform this using normal
            // s.t. the circle is locally parallel to the surface.
            dp.x = p.x + sigma * Math.cos(angle);
            dp.z = p.z + sigma * Math.sin(angle);
            dp.y = p.y;
            
            gl.glVertex3d(dp.x, dp.y, dp.z);
            i++;
        }
        gl.glEnd();
        
//        System.out.println(i);
//        System.out.println("" + acolor[0] + "," + acolor[1] + "," + acolor[2] + "," + acolor[3]);
    }

    public void setPosition(Point3f pos) {
        p.set(pos);
    }

	public void setPosition(Point3d pos) {
		p.set(pos);
	}
	
	public void setPosition(Point3i pos) {
		p.set(pos.x, pos.y, pos.z);
	}

	public void setPosition(int x, int y, int z) {
		p.set(x, y, z);
	}

	@Override
	public String toString() {
		return getPosition() + ": " + super.toString();
	}

	public int getIndex() {
		return index;
	}
	
	private int index = 0;
	public void setIndex(int index) {
		this.index = index;
	}

	private double error;

	private HelicoidParameter helicoidError = new HelicoidParameter();

	public void setHelicoidError(ParameterType type, HelicoidParameter error) {
		this.helicoidError.set(type, error);
	}

	public void setError(ParameterType type, HelicoidParameter error) {
		this.helicoidError.set(type, error);
	}

	public void setError(ParameterType type, double error) {
		this.helicoidError.set(type, error);
	}

	public void setError(double error) {
		this.error = error;
	}

	public double getError() {
		return error;
	}

	private boolean garbage = false;
	public void setGarbage(boolean b) {
		garbage = b;
	}
	
	public boolean isGarbage() {
		return garbage || Double.isNaN(getKT()) || Double.isNaN(getKN()) || Double.isNaN(getKB());
	}
}
