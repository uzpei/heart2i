/**
 * 
 */
package helicoid.parameter;

import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.ParameterListener;

/**
 * @author piuze
 *
 */
public class HelicoidParameterControl extends HelicoidParameter {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2573118280566392703L;

	public DoubleParameter skt = new DoubleParameter("kT", 0, -ParameterType.HKT.getMaximumMagnitude(), ParameterType.HKT.getMaximumMagnitude());
	public DoubleParameter skn = new DoubleParameter("kN", 0, -ParameterType.HKN.getMaximumMagnitude(), ParameterType.HKN.getMaximumMagnitude());
	public DoubleParameter skb = new DoubleParameter("kB", 0, -ParameterType.HKB.getMaximumMagnitude(), ParameterType.HKB.getMaximumMagnitude());
//	public DoubleParameter salpha = new DoubleParameter("alpha", 0, -ParameterType.HALPHA.getMaximumMagnitude(), ParameterType.HALPHA.getMaximumMagnitude());
	
	public HelicoidParameterControl() {
		this(new HelicoidParameter());
		
		super.addParameter(skt);
		super.addParameter(skn);
		super.addParameter(skb);
//		super.addParameter(salpha);
	}
	
	public HelicoidParameterControl(HelicoidParameter hp) {
		cinit();
		
		set(hp);
		
		updateSliders();
	}

	@Override
    public void set(HelicoidParameter v) {
		super.set(v);
		updateSliders();
    }

	@Override
	public void set(ParameterType type, double value) {
		super.set(type, value);
		updateSliders();
	}

    @Override
	public void set(int i, double v) {
    	super.set(i, v);
    	updateSliders();
    }

    @Override
	public void set(double t, double n, double b, double a) {
    	super.set(t, n, b, a);
    	updateSliders();
    }

	@Override
	public void set(double[] h) {
		super.set(h);
		updateSliders();
	}

	public void updateSliders() {
		hold(true);
		skt.setValue(getKT());
		skn.setValue(getKN());
		skb.setValue(getKB());
		hold(false);
	}
	
	public HelicoidParameterControl(HelicoidParameterControl other) {
		super(other);
	}

	protected void cinit() {
		addListener();
	}
	
	public void zero() {
		skn.setValue(0);
		skb.setValue(0);
		skt.setValue(0);
	}
	
//	@Override
//	public void addParameterListener(ParameterListener pl) {
//		skn.addParameterListener(pl);
//		skb.addParameterListener(pl);
//		salpha.addParameterListener(pl);		
//		skt.addParameterListener(pl);
//	}
	
	private void addListener() {
		ParameterListener ktl = new ParameterListener() {
			
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				if (!isHolding())
					setKT(skt.getValue());
			}
		};
		skt.addParameterListener(ktl);
		
		ParameterListener knl = new ParameterListener() {
			
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				if (!isHolding())
					setKN(skn.getValue());
			}
		};
		skn.addParameterListener(knl);

		ParameterListener kbl = new ParameterListener() {
			
			@Override
			public void parameterChanged(swing.parameters.Parameter parameter) {
				if (!isHolding())
					setKB(skb.getValue());
			}
		};
		skb.addParameterListener(kbl);
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		vfp.add(skt.getSliderControls());
		vfp.add(skn.getSliderControls());
		vfp.add(skb.getSliderControls());
		
		return vfp.getPanel();
	}

	/**
	 * 
	 */
	public void invalidate() {
		skn.invalidate();
		skb.invalidate();
		skt.invalidate();
	}
}
