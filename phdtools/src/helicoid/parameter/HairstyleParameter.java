package helicoid.parameter;

import helicoid.modeling.tracing.TraceParameterControl;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

public class HairstyleParameter {
	private double waviness = 0;
	private double noise = 0;
	private double rotx = 0;
	private double roty = 0;
	private double rotz = 0;
	private double lengthScale = TraceParameterControl.DEFAULT_TRACESCALE;
	private Point3d origin = new Point3d();

	
	private Matrix4d frame = new Matrix4d();
	{
		frame.setIdentity();
	}
	
	public HairstyleParameter(HairstyleParameter other) {
		set(other);
	}
	
	public void reset() {
		this.waviness = 0;
		this.noise = 0;
		this.rotx = 0;
		this.roty = 0;
		this.rotz = 0;
		this.lengthScale = 0;
		this.frame.setIdentity();
		this.origin.set(0, 0, 0);
	}

	/**
	 * @param parameter
	 */
	public void set(HairstyleParameter other) {
		this.waviness = other.waviness;
		this.noise = other.noise;
		this.rotx = other.rotx;
		this.roty = other.roty;
		this.rotz = other.rotz;
		this.lengthScale = other.lengthScale;
		this.frame.set(other.frame);
		this.origin.set(other.origin);
	}

	public HairstyleParameter() {
		frame.setIdentity();
	}
	
	public void setWaviness(double waviness) {
		this.waviness = waviness;
	}
	public double getWaviness() {
		return waviness;
	}
	public void setNoise(double noise) {
		this.noise = noise;
	}
	public double getNoise() {
		return noise;
	}
	public void setRotx(double rotx) {
		this.rotx = rotx;
	}
	public double getRotx() {
		return rotx;
	}
	public void setRoty(double roty) {
		this.roty = roty;
	}
	public double getRoty() {
		return roty;
	}
	public void setRotz(double rotz) {
		this.rotz = rotz;
	}
	public double getRotz() {
		return rotz;
	}
	public void setLengthScale(double lengthScale) {
		this.lengthScale = lengthScale;
	}
	public double getLengthScale() {
		return lengthScale;
	}
	public void setRot(double x, double y, double z) {
		setRotx(x);
		setRoty(y);
		setRotz(z);
	}
	public void setFrame(Matrix4d frame) {
		this.frame.set(frame);
	}
	public Matrix4d getFrame() {
		return frame;
	}

	public void setOrigin(Point3d origin) {
		this.origin.set(origin);
	}

	public Point3d getOrigin() {
		return origin;
	}

}
