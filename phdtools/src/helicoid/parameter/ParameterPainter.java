package helicoid.parameter;

import helicoid.parameter.HelicoidParameter.ParameterType;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.vecmath.Point3f;

import picking.ModelPicker;
import swing.component.VerticalFlowPanel;
import swing.event.AdapterState;
import swing.event.ExtendedAdapter;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import tools.loader.Vertex;

/**
 * Place a helicoid parameter overlay over the 2D conformal mapping of a surface.
 * Discretize the 2D mappipng into a grid an assign values to the grid points.
 * @author piuze
 */
public class ParameterPainter extends ExtendedAdapter {
    private ParameterType currentType = ParameterType.HALPHA;
    
    private DoubleParameter psigma = new DoubleParameter("radius", 0.2, 0, 5); 

    private DoubleParameter pvalue = new DoubleParameter("intensity", 0.1, 0, 1); 

    private ModelPicker picker;
    
    private HelicoidParameterNode currentNode = new HelicoidParameterNode();

    private Vertex pickedVertex = null;

    private BooleanParameter painting = new BooleanParameter("Painting mode", true);

    public ParameterPainter(ModelPicker mpicker) {
        picker = mpicker;
    }
    
    public void setNodePosition(Vertex p) {
        currentNode.setPosition(new Point3f(p.p));
    }


    public void display(GLAutoDrawable drawable) {
        float alpha = 0.2f + 0.8f * (float) Math.abs((currentNode.get(currentType) / HelicoidParameter.getMaxima().get(currentType)));
        
        float[] color = new float[] {1, 0, 0};
        
        switch (currentType) {
        case HKT:
            color = new float[] {1, 0, 0 };
            break;
        case HKB:
            color = new float[] {0, 1, 0};
            break;
        case HKN:
            color = new float[] {0, 0, 1};
            break;
        case HALPHA:
            color = new float[] {1, 1, 0};
        }
        
        float[] acolor = new float[] { color[0], color[1], color[2], alpha };

		GL2 gl = drawable.getGL().getGL2();

        gl.glDisable(GL2.GL_LIGHTING);
        gl.glColor4f(acolor[0], acolor[1], acolor[2], acolor[3]);

        currentNode.setValue(currentType, pvalue.getValue(), psigma.getValue());
        currentNode.display(drawable);
    }
    
    @Override
    public void doPicking(GLAutoDrawable drawable) {
        
        if (state.shiftDown() || state.altDown()) {
            float[] color = new float[] { 0, 0, 0.7f, 0.7f };
            Vertex v = picker.pick(drawable, state.getX(), state.getY(), color);
            
            if (v != null) {
                pickedVertex = v;
                setNodePosition(v);
            }
        }
    }
    
    @Override
    public String toString() {
        return "Parameter painting";
    }

    @Override
    public boolean requestPicking() {
        if (painting.getValue()) return true;
        else return false;
    }

    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Parameter Painting"));
        
        vfp.add(painting.getControls());
        
        List<JRadioButton> ptns = new LinkedList<JRadioButton>();
        for (ParameterType t : ParameterType.values()) {
            ptns.add(new JRadioButton(t.name(), false));
        }

        ItemListener al = new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                JRadioButton cb = (JRadioButton) e.getSource();
                currentType = ParameterType.valueOf(cb.getText());
                
                System.out.println(cb.getText());
                System.out.println(currentType);
            }
        };
        for (JRadioButton b : ptns) b.addItemListener(al);
        
        
        ButtonGroup bgroup = new ButtonGroup();
        for (JRadioButton b : ptns) bgroup.add(b);

        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new GridLayout(ptns.size() / 3, 3));
        for (JRadioButton b : ptns) radioPanel.add(b);

        radioPanel.setBorder(BorderFactory.createTitledBorder(
                   BorderFactory.createEtchedBorder(), "Parameter Selection"));
        
        vfp.add(radioPanel);
        
        vfp.add(pvalue.getSliderControls(false));
        vfp.add(psigma.getSliderControls(false));

        return vfp.getPanel();
       
    }

	@Override
	public void setAdapterState(AdapterState state) {
		this.state = state;
	}

	@Override
	public String getHandleString() {
		return "ParameterPainter";
	}

}
