/**
 * 
 */
package helicoid.parameter;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;


import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

/**
 * @author piuze
 *
 */
public class CollapsibleHelicoidParameterControl extends HelicoidParameterControl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3195389210496939493L;

	private final CollapsiblePanel collapsiblePanel;
	private int index = 0;
	private String label;
	
	/**
	 * @param content
	 * @param name
	 */
	public CollapsibleHelicoidParameterControl(int index) {
		this(index, new HelicoidParameter());
	}
	
	public CollapsibleHelicoidParameterControl(int index, HelicoidParameter hp) {
		super();
		this.index = index;
		set(hp);
		updateSliders();

		label = "[Piece " + index + "] " + toString();
		collapsiblePanel = new CollapsiblePanel(buildPanel(), label);
		collapse();
		
		ParameterListener l = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updatePanel();
			}
		};
		addParameterListener(l);
	}

	private final VerticalFlowPanel vfp = new VerticalFlowPanel();

	private JPanel buildPanel() {
		
		String text = label;
		vfp.setBorder(new TitledBorder(text));

		vfp.add(skt.getSliderControls());
		vfp.add(skn.getSliderControls());
		vfp.add(skb.getSliderControls());
		vfp.add(salpha.getSliderControls());

		return vfp.getPanel();
	}
	
	public void expand() {
		collapsiblePanel.expand();
	}
	public void collapse() {
		collapsiblePanel.collapse();
	}
	
	private void updatePanel() {
		label = "[Piece " + index + "] " + toString();
		collapsiblePanel.setName(label);
		vfp.setBorder(new TitledBorder(label));
	}
	
	public JPanel getPanel() {
		
		return collapsiblePanel;
	}
	
}
