package helicoid.parameter;

import helicoid.parameter.HelicoidParameter.ParameterType;

import java.text.NumberFormat;
import java.util.Random;

import app.pami2013.cartan.CartanParameter;

/**
 * Class representing a vector of helicoid parameters k_T, k_N, k_B, alpha
 * 
 * @author piuze
 */
//public class HelicoidParameter extends ParameterManager implements Serializable {
public class HelicoidParameter extends CartanParameter {
        
//    /**
//	 * 
//	 */
//	private static final long serialVersionUID = -5756725466642300373L;
	
	public final static double DEFAULT_RANDOM_PARAMETER_MAG = 0.8;

	public enum ParameterType {
        HKT(2*Math.PI), HKN(2 * Math.PI), HKB(2 * Math.PI), ALL(0);

        private int index;
        private double maxmag;
        private ParameterType(double maxmag) {
        	this.maxmag = maxmag;
        }
        
        private final static ParameterType[] parameterValues = new ParameterType[] { HKT, HKN, HKB }; 
        
        public final static ParameterType[] parameterValues() {
        	return parameterValues;
        }
        
        public double getMaximumMagnitude() {
        	return maxmag;
        }
        
        public double extractValue(HelicoidParameter p) {
            double val = 0;
            switch (this) {
            case HKT:
                val = p.getKT();
                break;
            case HKB:
                val = p.getKN();
                break;
            case HKN:
                val = p.getKB();
                break;
            }

            return val;
        }

		public static ParameterType fromIndex(int i) {
            switch (i) {
            case 0:
            	return HKT;
            case 1:
            	return HKN;
            case 2:
            	return HKB;
            default:
            	return ParameterType.ALL;
            }
		}
    }

	public double getKT() {
		return super.getc121();
	}
	public double getKN() {
		return super.getc122();
	}
	public double getKB() {
		return super.getc123();
	}
	public void setKT(double v) {
		super.setc121(v);
	}
	public void setKN(double v) {
		super.setc122(v);
	}
	public void setKB(double v) {
		super.setc123(v);
	}
	
    /**
     * Create a new parameter vector with default values.
     */
    public HelicoidParameter() {
    	super(new int[] { 0, 1, 2 });
        set(0);
    }


    /**
     * Create a new parameter vector with the values from another.
     * 
     * @param v
     */
    public HelicoidParameter(HelicoidParameter v) {
        set(v);
    }

    /**
     * Create a new parameter vector with the given parameter values.
     */
    public HelicoidParameter(double KT, double KN, double KB, double alpha) {
    	set(KT, KN, KB, alpha);
    }

    public HelicoidParameter(double[] parameter) {
    	set(parameter);
    }

    private static NumberFormat formatter = NumberFormat.getNumberInstance();
    {
        formatter.setMinimumIntegerDigits(0);
        formatter.setMaximumIntegerDigits(4);
        formatter.setMinimumFractionDigits(4);
        formatter.setMaximumFractionDigits(4);
    }

    @Override
    public String toString() {
        // return "KT=" + formatter.format(KT) + " KN=" + formatter.format(KN) +
        // " KB=" + formatter.format(KB) + " alpha=" + formatter.format(alpha) +
        // " length=" + length + " curliness="+curliness;
        return "KT=" + formatter.format(getKT()) + " KN=" + formatter.format(getKN())
                + " KB=" + formatter.format(getKB()) + " alpha=0";
    }

    @Override
	public String toStringShort() {
        // return "KT=" + formatter.format(KT) + " KN=" + formatter.format(KN) +
        // " KB=" + formatter.format(KB) + " alpha=" + formatter.format(alpha) +
        // " length=" + length + " curliness="+curliness;
        return "(" + formatter.format(getKT()) + ", " + formatter.format(getKN())
                + ", " + formatter.format(getKB()) + ", "
                + 0 + ")";
    }


    @Override
	public void set(int i, double v) {
        if (i == 0) setKT(v);
        else if (i == 1) setKN(v);
        else if (i == 2) setKB(v);
    }

    public void set(double t, double n, double b, double a) {
		setKT(t);
		setKN(n);
		setKB(b);
    }

	public void set(ParameterType type, double value) {
		switch (type) {
		case HKT:
			setKT(value);
			return;
		case HKN:
			setKN(value);
			return;
		case HKB:
			setKB(value);
			return;
		case ALL:
			setKT(value);
			setKN(value);
			setKB(value);
			return;
		}

	}

	public double get(ParameterType type) {
		switch (type) {
		case HKT:
			return getKT();
		case HKN:
			return getKN();
		case HKB:
			return getKB();
		}

		return 0;
	}

    public void set(HelicoidParameter v) {
    	for (ParameterType type : ParameterType.parameterValues())
    		set(type, v);
    }
    
	/**
	 * @param h
	 */
	public void set(double[] h) {
		set(h[0], h[1], h[2], h[3]);
		
	}

	public void set(ParameterType type, HelicoidParameter other) {
		if (type == ParameterType.ALL) {
			set(ParameterType.HKT, other);
			set(ParameterType.HKN, other);
			set(ParameterType.HKB, other);
		}
		else
			set(type, type.extractValue(other));
	}

	public void add(ParameterType type, double d) {
		double v = get(type);
		set(type, d + v);
	}

}
