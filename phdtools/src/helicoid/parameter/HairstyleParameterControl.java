/**
 * 
 */
package helicoid.parameter;

import javax.swing.BoxLayout;
import javax.swing.JPanel;


import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import swing.text.GreekToolset.GreekLetter;

/**
 * @author piuze
 *
 */
public class HairstyleParameterControl {
	/**
	 * Limits for hairstyle parameters. Order: waviness, noise, theta_x, theta_y, scale.
	 */
	private static double[] lim = new double[] { 0.4, 0.1, Math.PI, 0.5*Math.PI, 0.5*Math.PI, 5 };
	
	private DoubleParameter scale = new DoubleParameter("Scale", 0, 0, lim[5]);
	
	private DoubleParameter thetax = new DoubleParameter(GreekToolset
			.toString(GreekLetter.THETA)+"x", 0, -lim[2], lim[2]);

	private DoubleParameter thetay = new DoubleParameter(GreekToolset
			.toString(GreekLetter.THETA)+"y", 0, -lim[3], lim[3]);

	private DoubleParameter thetaz = new DoubleParameter(GreekToolset
			.toString(GreekLetter.THETA)+"z", 0, -lim[4], lim[4]);

	private DoubleParameter wav = new DoubleParameter("Waviness", 0, 0, lim[0]);
	private DoubleParameter noise = new DoubleParameter("Noise", 0, 0, lim[1]);

	private HairstyleParameter parameter = null;
	
	private final JPanel panel = new JPanel();
	
	/**
	 * @param parameter
	 */
	public HairstyleParameterControl(HairstyleParameter parameter) {
		this.parameter = parameter;
		updateSliders();
		buildPanel();
	}

	public JPanel getPanel() {
		return panel;
	}
	
	public void updateSliders() {
		scale.setValue(parameter.getLengthScale());
		thetax.setValue(parameter.getRotx());
		thetay.setValue(parameter.getRoty());
		thetaz.setValue(parameter.getRotz());
		wav.setValue(parameter.getWaviness());
		noise.setValue(parameter.getNoise());
	}
	
	private void buildPanel() {
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		panel.add(scale.getSliderControls());
		panel.add(thetax.getSliderControls());
		panel.add(thetay.getSliderControls());
		panel.add(thetaz.getSliderControls());
		panel.add(wav.getSliderControls());
		panel.add(noise.getSliderControls());

		scale.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter.setLengthScale(scale.getValue());
			}
		});

		wav.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter.setWaviness(wav.getValue());
			}
		});

		noise.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter.setNoise(noise.getValue());
			}
		});
		
		thetax.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter.setRotx(thetax.getValue());
			}
		});
		thetay.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter.setRoty(thetay.getValue());
			}
		});
		thetaz.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter p) {
				parameter.setRotz(thetaz.getValue());
			}
		});
	}
}
