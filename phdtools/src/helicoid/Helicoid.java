package helicoid;

import gl.renderer.JoglTextRenderer;
import helicoid.parameter.HelicoidParameter;

import java.io.NotActiveException;
import java.io.Serializable;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import tools.geom.Polyline;

/**
 * Class representing a strand as a set of points.
 * 
 * @author piuze
 */
public class Helicoid extends Polyline implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 566426117303503616L;
	private HelicoidParameter p = new HelicoidParameter();
    private double t_0 = 0, t_f = 0;
    
    public double[] getWavyPhase() {
    	return new double[] { t_0, t_f};
    }
    
    public void setWavyPhase(double t_0, double t_f) {
    	this.t_0 = t_0;
    	this.t_f = t_f;
    }
    
    /**
     * The local frame defined by the surface on which this helicoid lives.
     */
    private Matrix4d endFrame = new Matrix4d();
    {
    	endFrame.setIdentity();
    }
    
    @Override
	public Helicoid clone() {
    	return new Helicoid(this, false);
    }

    /**
     * 
     * @param other
     * @param byref whether the reference to the point list should be kept (otherwise a new list and new points are created)
     */
    public Helicoid(Helicoid other, boolean byref) {
//        super(other.getPoints(), byref);
        set(other, byref);
    }

	public void set(Helicoid other) {
		set(other, false);
	}
	/**
	 * @param helicoid
	 */
	public void set(Helicoid other, boolean byref) {
        set(other.getPoints(), byref);
        setTraceScale(other.traceScale);
        p.set(other.getParameter());
        setLocalFrame(other.getLocalFrame());
        setEndFrame(other.getEndFrame());
        setRotationFrame(other.getRotationFrame());
	}

    public Helicoid(Polyline other) {
        this(other.getPoints(), new HelicoidParameter());
    }
    
    public Helicoid(List<Point3d> pts, HelicoidParameter v) {
        super(pts);
        p.set(v);
    }
    
    public Helicoid(Helicoid other) {
    	this(other, false);
    }

    /**
     * @param pts
     * @param v
     * @param byref if we should keep the reference to the list of points or create a new one.
     */
    public Helicoid(List<Point3d> pts, HelicoidParameter v, boolean byref) {
        super(pts, byref);
        p.set(v);
    }

    public void display(GLAutoDrawable drawable, boolean drawparams) {
        GL gl = drawable.getGL();

        super.display(drawable);

        // Could draw additional things here
        // like parameters...
        if (drawparams) {
            JoglTextRenderer.print3dTextLines( 
                    getParameter().toStringShort(), getOrigin());
        }
    }

    /**
     * @return The parameters defining this hair.
     */
    public HelicoidParameter getParameter() {
        return p;
    }

    public void setEndFrame(Matrix4d endFrame) {
        this.endFrame.set(endFrame);
    }

    public Matrix4d getEndFrame() {
        return endFrame;
    }
    
    @Override
    public void transform(Matrix4d m) {
    	super.transform(m);
        getEndFrame().mul(m, getEndFrame());
    }

    private double traceScale = 1;
    
	/**
	 * @param traceScale
	 */
	public void setTraceScale(double traceScale) {
		this.traceScale = traceScale;
	}
	
	public double getTraceScale() {
		return traceScale;
	}

	@Override
	public void scale(double s) {
		super.scale(s);
		setTraceScale(s*traceScale);
	}
	
	@Override
	public Polyline tesselate(double perc) {
		try {
			throw new NotActiveException("Not implemented for helicoids yet. Use on a polyline instead.");
		} catch (NotActiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

}
