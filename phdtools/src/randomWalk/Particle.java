package randomWalk;
//
import gl.geometry.FancyArrow;
import gl.material.Colour;
import gl.renderer.GLViewerConfiguration;

import java.awt.Color;
import java.io.Serializable;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import com.jogamp.opengl.util.gl2.GLUT;

/**
 * @author epiuze
 */
public class Particle implements Serializable {
    
    /**
     * This particle's position.
     */
    public Point3d p = new Point3d();

    /**
     * This particle's velocity.
     */
    public Vector3d v = new Vector3d();

    /**
     * This particle's initial position.
     */
    public Point3d p0 = new Point3d();
    
    /**
     * This particle's initial velocity.
     */
    public Vector3d v0 = new Vector3d();
 
    public boolean ghosted = false;
    /**
     * 
     * @param p0
     * @param v0
     */
    public Particle(Point3d p0, Vector3d v0) {
//    	System.out.println("Creating new particle at " + p0 + " and with velocity = " + v0);
    	this.p0.set(p0);
    	this.v0.set(v0);
    	this.p.set(p0);
    	this.v.set(v0);
    }

    public void move(double stepSize) {
		p.scaleAdd(stepSize, v, p);
    }
    
    private static GLUT glut = new GLUT();
    
    public float size = 0.07f;
    
    public int res = 16;
    
    public Color3f color = new Color3f(Colour.red);
    
    public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glEnable(GL2.GL_LIGHTING);
		float sphere_diffuse[] = new float[] { 0.0f, 1.0f, 0.0f, 1.0f };
		float sphere_diffuse_ghost[] = new float[] { sphere_diffuse[0], sphere_diffuse[1], sphere_diffuse[2], 0.4f };

    	gl.glPushMatrix();
    	gl.glTranslated(p.x, p.y, p.z);
    	
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, !ghosted ? sphere_diffuse : sphere_diffuse_ghost, 0);

    	glut.glutSolidSphere(size, res, res);
    	
    	gl.glPopMatrix();
    	
    	Point3d pv = new Point3d();
    	Vector3d vp = new Vector3d(v);
    	vp.normalize();
    	pv.scaleAdd(0.3, v, p);
    	FancyArrow fa = new FancyArrow(p, pv, color, size / 5f);
    	fa.draw(gl);
    }

	public double distance(Point3d target) {
		return p.distance(target);
	}

}
