package randomWalk;

import gl.geometry.FancyArrow;
import gl.geometry.GLObject;
import gl.geometry.LightRoom;
import gl.material.Colour;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLException;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;

import com.jogamp.opengl.util.awt.Screenshot;
import com.jogamp.opengl.util.gl2.GLUT;

public class RandomDirectionalWalkSimulation implements Interactor, GLObject {


	private JoglRenderer viewer;
	
	private LightRoom room = new LightRoom(5.0f);

	public RandomDirectionalWalkSimulation(
			GLViewerConfiguration viewerConfiguration) {
		
		initialize(true);

		room.setLightPosition(new Point3d(-5.9, 3, 0));
		
//		Dimension dimension = new Dimension(1000, 1000);
		Dimension dimension = new Dimension(800, 600);
		Dimension controlSize = new Dimension(650, (int) (dimension.getHeight() + 90));
		viewer = new JoglRenderer("Antialiasing", this);
		viewer.addInteractor(this);
		viewer.getCamera().zoom(400f);
		viewer.start();
	}

	private List<Particle> particles = new LinkedList<Particle>();
	private List<FancyArrow> arrows = new LinkedList<FancyArrow>();
	
	private Particle currentParticle = null;
	private Point3d target = new Point3d();
	
	private boolean running = false;
	private boolean orientingTheta = false;
	private boolean orientingPhi = false;
	
	private void initialize(boolean start) {
		particles.clear();
		arrows.clear();
		
		sid = 0;
		frameCount = 0;
		
		t = timeStep.getValue();
		orientingTheta = true;
		orientingPhi = true;

		currentParticle = new Particle(new Point3d(), new Vector3d());

		evaluate(timeStep.getValue(), target);
		
		// Get target angles
		currentParticle.v.sub(target, currentParticle.p);
		currentParticle.v.normalize();
		Particle p0 = new Particle(currentParticle.p, currentParticle.v);
		p0.ghosted = true;
		particles.add(p0);

		targetTheta = getCurrentAngles()[0];
		targetPhi = getCurrentAngles()[1];
		dTheta = getCurrentAngles()[0];
		dPhi = getCurrentAngles()[1];

		System.out.println("Current target angles = " + targetTheta / Math.PI * 180+ ", " + targetPhi / Math.PI * 180);

		currentParticle.v.set(new Vector3d(1, 0, 0));
		
//		step();

//
//		t = timeStep.getValue();
//
//		evaluate(t, target);
		
		running = start;
	}
	
	private void step() {
		t += timeStep.getValue();
		
		if (t >= maxt.getValue()) {
			running = false;
			System.out.println("Ending simulation.");
			return;
		}
		
		Point3d currentTarget = new Point3d(target);

		// Move this before defining the direction for changes to take effect right away
		evaluate(t, target);

		// Save the direction of the current particle
		Vector3d pv = new Vector3d(currentParticle.v);
		
		// Get the current angles of the particle's direction
		double thetaCurrent = getCurrentAngles()[0];
		double phiCurrent = getCurrentAngles()[1];

		Vector3d v = new Vector3d();
		v.sub(target, currentTarget);
		v.normalize();
		currentParticle.v.set(v);
		
		orientingTheta = true;
		orientingPhi = true;
		
		targetTheta = getCurrentAngles()[0];
		targetPhi = getCurrentAngles()[1];
		
		dTheta = targetTheta - thetaCurrent;
		dPhi = targetPhi - phiCurrent;
		
		System.out.println("Current target angles = " + targetTheta / Math.PI * 180+ ", " + targetPhi / Math.PI * 180);

		createNewParticle(currentTarget, pv);
		
		// Wait until we're finished rotating to set the previous orientation
		particles.get(particles.size()-1).v.scale(0);

		// Create arrow
		createArrow(currentTarget, pv, Colour.grey3);
	}
	
	private void createNewParticle(Point3d p, Vector3d v) {
		currentParticle.ghosted = true;
		particles.add(currentParticle);

		currentParticle = new Particle(p, v);
	}

	private double targetTheta, targetPhi;
	private double dTheta, dPhi;
	
	private double[] getCurrentAngles() {
		double theta = Math.acos(currentParticle.v.z / currentParticle.v.length()); 
		double phi = Math.atan2(currentParticle.v.y, currentParticle.v.x); 
		
		return new double[] { theta, phi };
	}
	
	private void updateDirection(double theta, double phi) {
		double r = currentParticle.v.length();
		currentParticle.v.x = r * Math.sin(theta) * Math.cos(phi);
		currentParticle.v.y = r * Math.sin(theta) * Math.sin(phi);
		currentParticle.v.z = r * Math.cos(theta);
	}
	
	private void createArrow(Point3d from, Vector3d direction, Color3f color) {
		Vector3d v = new Vector3d(direction);
		v.normalize();
		Point3d p = new Point3d();
		p.scaleAdd(0.3, v, from);
		arrows.add(new FancyArrow(from, p, color, 0.01));
	}
	
	/**
	 * @param target
	 * @return if the target has been reached.
	 */
	private boolean move() {
		
		if (orientingTheta) {
			double[] angles = getCurrentAngles();
			double theta = angles[0]; 
			double phi = angles[1]; 

//			System.out.println("Current angles (+theta) = " + theta / Math.PI * 180+ ", " + phi / Math.PI * 180);

			if (Math.abs(targetTheta - theta) < Math.abs(dTheta / (dangle.getValue())) || Math.signum(targetTheta - theta) != Math.signum(dTheta)) {
				orientingTheta = false;
				theta = targetTheta;
				
				// Create arrow
				createArrow(currentParticle.p, currentParticle.v, Colour.green);
			}
			else {
//				theta += dangle.getValue() * Math.signum(targetTheta - theta);
//				System.out.println("Increment = " + (targetTheta - theta) / ((double)dangle.getValue()));
				theta += (dTheta) / (dangle.getValue());
			}
			
			updateDirection(theta, phi);
			
			return false;
		}
		
		if (orientingPhi) {
			double[] angles = getCurrentAngles();
			double theta = angles[0]; 
			double phi = angles[1]; 
			
//			System.out.println("Current angles (+phi) = " + theta / Math.PI * 180+ ", " + phi / Math.PI * 180);

//			System.out.println(Math.abs(phi - targetPhi) + " vs " + dPhi / ((double) dangle.getValue()));
			if (Math.abs(phi - targetPhi) < Math.abs(dPhi / (dangle.getValue())) || Math.signum(targetPhi - phi) != Math.signum(dPhi)) {
				orientingPhi = false;
				phi = targetPhi;
				
			}
			else {
//				phi += dangle.getValue() * Math.signum(targetPhi - phi);
				phi += (dPhi) / (dangle.getValue());
			}

			updateDirection(theta, phi);

			// When we are done, update previous orientation
			if (orientingPhi == false) 
				particles.get(particles.size()-1).v.set(currentParticle.v);
			
			return false;
		}
		
//		System.out.println("Moving particle...");
		if (currentParticle.distance(target) >= speed.getValue() * currentParticle.v.length()) {
			currentParticle.move(speed.getValue());
			return false;
		}
		else {
//			System.out.println("Target reached!");
			return true;
		}
	}
	
	private void evaluate(double t, Point3d target) {
//		System.out.println("Creating new target at t = " + t + ", p(t) = " + target);
		
//		target.x = t;
//		target.y = amplitude.getValue() * Math.sin(frequency.getValue() * t);
//		target.z = scale.getValue() * t * t;

		target.x = t;
		target.y = amplitude.getValue() * Math.sin(frequencyy.getValue() * t);
//		target.z = 0;
		target.z = amplitude.getValue() * (-1 + Math.cos(frequencyz.getValue() * t));
	
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_R) {
					initialize(true);
				}
				else if (e.getKeyCode() == KeyEvent.VK_C) {
					initialize(false);
				}
			}
		});
	}
	
	private DoubleParameter speed = new DoubleParameter("speed", 0.02, 0, 5);
	private DoubleParameter timeStep = new DoubleParameter("time step", 0.3, 1e-3, 5);
	private DoubleParameter maxt = new DoubleParameter("max t", 5.29, 0, 10);
	private DoubleParameter amplitude = new DoubleParameter("sinusoidal amplitude", 0.64, 0, 10);
	private DoubleParameter frequencyy = new DoubleParameter("sin frequency y", 2.98, 0, 10);
	private DoubleParameter frequencyz = new DoubleParameter("cos frequency z", 3.845, 0, 10);
	private DoubleParameter scale = new DoubleParameter("parabola scalar", 0.5, 0, 10);
	private IntParameter dangle = new IntParameter("angle steps", 50, 1, 500);
	private BooleanParameter plotTrajectory = new BooleanParameter("plot trajectory", false);
	private IntParameter screenshotPeriod = new IntParameter("screenshot period", 2, 1, 500);

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(speed.getSliderControlsExtended());
		vfp.add(timeStep.getSliderControls());
		vfp.add(maxt.getSliderControls());
		vfp.add(amplitude.getSliderControls());
		vfp.add(frequencyy.getSliderControls());
		vfp.add(frequencyz.getSliderControls());
		vfp.add(scale.getSliderControls());
		vfp.add(dangle.getSliderControls());
		vfp.add(plotTrajectory.getControls());
		vfp.add(screenshotPeriod.getSliderControlsExtended());
		screenshotPeriod.setChecked(false);
		
		vfp.add(room.getControls());
		
		return vfp.getPanel();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Parameterisation of trajectory.
	 */
	private double t = 0;
	private int frameCount = 0;
	private int sid = 0;
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		GLUT glut = new GLUT();

		room.display(drawable);
		
//		WorldAxis.display(gl);
//		new FancyAxis(0.5).draw(gl);
		
		if (running) {
			if (move()) step();
		}
		
		// Display the current particle
		currentParticle.display(drawable);
		
		
		for (Particle p : particles) {
			p.display(drawable);
		}

		gl.glLineStipple(8, (short) 0xAAAA);
		gl.glLineWidth(1f);
		gl.glEnable(GL2.GL_LINE_STIPPLE);

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor4d(1, 1, 1, 1);
		gl.glBegin(GL2.GL_LINE_STRIP);
		for (Particle p : particles) {
			gl.glVertex3d(p.p.x, p.p.y, p.p.z);
		}
		gl.glVertex3d(currentParticle.p.x, currentParticle.p.y, currentParticle.p.z);
		
		gl.glEnd();
		
		gl.glDisable(GL2.GL_LINE_STIPPLE); 
		
		for (FancyArrow arrow : arrows) {
			arrow.draw(gl);
		}
		
//		if (running) {
//			// Display the target
//			gl.glPushMatrix();
//			gl.glTranslated(target.x, target.y, target.z);
//			gl.glScaled(-1, 1, 1);
//			float color[] = new float[] { 1.0f, 0.0f, 0.0f, 1.0f };
//	    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, color, 0);
//			glut.glutSolidSphere(0.1f, 16, 16);
////			glut.glutSolidTeapot(0.1f, true);
//			gl.glPopMatrix();
//		}
		
		if (plotTrajectory.getValue()) 
			plot(gl);
		
		if (running && screenshotPeriod.isChecked() && frameCount % screenshotPeriod.getValue() == 0) {
			try {
				Screenshot.writeToFile(new File("screenshots/screen_" + sid++ + ".png"),
						drawable.getWidth(), drawable.getHeight());
			} catch (GLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		frameCount++;
	}
	
	private void plot(GL2 gl) {
		
		gl.glEnable(GL2.GL_LIGHTING);
//		gl.glColor4d0000000(1, 0, 0, 1);

//		float color[] = new float[] { 1.0f, 0.0f, 0.0f, 1.0f };
		float color[] = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, color, 0);

		Point3d p = new Point3d();
		gl.glLineStipple(8, (short) 0xAAAA);
		gl.glLineWidth(1f);
		gl.glEnable(GL2.GL_LINE_STIPPLE);

		gl.glBegin(GL2.GL_LINE_STRIP);

		double dt = 0.01;
		for (double t = 0; t < maxt.getValue(); t += dt) {
			evaluate(t, p);
			gl.glVertex3d(p.x, p.y, p.z);
		}
		gl.glEnd();

		GLUT glut = new GLUT();
		for (double t = 0; t < maxt.getValue(); t += timeStep.getValue()) {
			evaluate(t, p);
			gl.glPushMatrix();
			gl.glTranslated(p.x, p.y, p.z);
//			glut.glutSolidTeapot(0.1f, false);
			glut.glutSolidSphere(0.03f, 16, 16);
			gl.glPopMatrix();
		}
		
		gl.glDisable(GL2.GL_LINE_STIPPLE);

	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		GLViewerConfiguration glconfig = new GLViewerConfiguration();
		glconfig.setColor(Color.black);
		new RandomDirectionalWalkSimulation(glconfig);
	}
}
