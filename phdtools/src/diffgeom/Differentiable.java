package diffgeom;

public interface Differentiable {

	public abstract double F(double x);
}
