package diffgeom;

public class Differentiator {

	public static double d(Differentiable d, double x, double h) {
		// Use fourth-order central differences
		return 1d/12d * d.F(x - 2 * h) - 2d/3d * d.F(x - h) + 2d/3d * d.F(x + h) - 1d/12d * d.F(x + 2 * h);
	}
}
