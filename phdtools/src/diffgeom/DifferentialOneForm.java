package diffgeom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import math.matrix.Matrix3d;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.Matrix.Norm;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import tools.SimpleTimer;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.VolumeSampler;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.NeighborhoodMap;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.core.experiments.PamiExperimentRunner;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;

public class DifferentialOneForm {
	
	public enum MovingAxis { 
			F1(0, new int[] { 1, 0, 0}), F2(1, new int[] { 0, 1, 0}), F3(2, new int[] { 0, 0, 1});
			
			private int index;
			private int[] basis;
			public int getIndex() {
				return index;
			}
			private MovingAxis(int index, int[] basis) {
				this.index = index;
				this.basis = basis;
			}
			
			public int[] getBasis() {
				return basis;
			}
			
			public static MovingAxis fromInt(int axis) {
				if (axis == 0)
					return F1;
				else if (axis == 1)
					return F2;
				else if (axis == 2)
					return F3;
				
				return null;
			}
		};
	
	private VoxelFrameField frameField;
	
//	private VolumeRenderer volumeRenderer;
	
	public void setFrameField(VoxelFrameField frameField) {
		this.frameField = frameField;
		
//		volumeRenderer = new VolumeRenderer(frameField.getDimension());
	}

	private EnumComboBox<MovingAxis> ecbi = new EnumComboBox<MovingAxis>("Ei", MovingAxis.F1);
	private EnumComboBox<MovingAxis> ecbj = new EnumComboBox<MovingAxis>("Ej", MovingAxis.F2);
	private EnumComboBox<MovingAxis> ecbk = new EnumComboBox<MovingAxis>("Ek", MovingAxis.F3);

	private Matrix3d currentOneForm;
	
	public Matrix3d computeOneForm() {
		currentOneForm = DifferentialOneForm.computeOneForm(frameField, ecbi.getSelected(), ecbj.getSelected(), ecbk.getSelected());
		return currentOneForm;
	}
	
	public static Matrix3d[][][] computeAllOneForms(VoxelFrameField frameField, IntensityVolumeMask mask) {
		Matrix3d[][][] m = new Matrix3d[3][3][3];

		// Erode the mask such since voxels that lie on the boundary do not have a valid central difference.
//		VoxelBox box = new VoxelBox(mask.getDimension());
//		IntensityVolumeMask maskEroded = new IntensityVolumeMask(mask.copy().getData());
//		box.setMask(maskEroded);
//		maskEroded.erode(box, 3);
		IntensityVolumeMask maskEroded = mask;

		// Skew symmetric: w_ij<k> = -w_ij<k>
		System.out.println("Computing Maurer-Cartan connection forms...");
		
		MovingAxis[] axis = new MovingAxis[] { MovingAxis.F1, MovingAxis.F2, MovingAxis.F3 };
		
		for (int i = 0; i < 2; i++) {
			for (int j = i + 1; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					System.out.println(i + ", " + j + ", " + k);
					m[i][j][k] = DifferentialOneForm.computeOneForm(frameField, axis[i], axis[j], axis[k]);
					m[i][j][k].setName("c" + String.valueOf(i+1) + String.valueOf(j+1) + String.valueOf(k+1));
					maskEroded.applyMask(m[i][j][k]);
					
					// Negate
					m[j][i][k] = m[i][j][k].scaleOut(-1);
					m[j][i][k].setName("c" + String.valueOf(j+1) + String.valueOf(i+1) + String.valueOf(k+1));
				}
			}
		}

		/*
		 *  Fill in unwanted matrices
		 */
		// Diagonal
		for (int i = 0; i < 3; i++) {
			m[i][i][i] = null;
		}
		// Identity variation
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (j == i) continue;
				
				m[i][i][j] = null;
			}
		}

//		// Diagonal
//		for (int i = 0; i < 3; i++) {
//			m[i][i][i] = new Matrix3d(frameField.getDimension());
//		}
//		// Identity variation
//		for (int i = 0; i < 3; i++) {
//			for (int j = 0; j < 3; j++) {
//				if (j == i) continue;
//				
//				m[i][i][j] = new Matrix3d(frameField.getDimension());
//			}
//		}
		
		return m;
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(ecbi.getControls());
		vfp.add(ecbj.getControls());
		vfp.add(ecbk.getControls());
		return vfp.getPanel();
	}
	
//	/**
//	 * Compute the connection forms of a frame field and store them 
//	 * @param frameField
//	 * @return
//	 */
//	public static Matrix3d[] compute(final VoxelFrameField frameField) {
//		int[] dim = frameField.getDimension();
//		
//		System.out.println("Computing curvatures of a frame field with dimension = " + Arrays.toString(dim) + " (" + dim[0]*dim[1]*dim[2] + " nodes)");
//
//		SimpleTimer timer = new SimpleTimer();
//		timer.tick();
//
//		final Matrix3d[] e = new Matrix3d[3];
//		final Matrix3d[] f = new Matrix3d[3];
//		final Matrix3d[] g = new Matrix3d[3];
//		final double[] ev = new double[3];
//		final double[] fv = new double[3];
//		final double[] gv = new double[3];
//		
//		for (int i = 0; i < 3; i++) {
//			e[i] = new Matrix3d(dim);
//			f[i] = new Matrix3d(dim);
//			g[i] = new Matrix3d(dim);
//		}
//		
//		final int[] ai = new int[] { 0 };
//
//		System.out.println("Rebuilding frame matrices...");
//
//		frameField.voxelProcess(new PerVoxelMethod() {
//			
//			@Override
//			public void process(int x, int y, int z) {
//				
//				frameField.getFieldX().getVectorArray(x, y, z, ev);
//				frameField.getFieldY().getVectorArray(x, y, z, fv);
//				frameField.getFieldZ().getVectorArray(x, y, z, gv);
//				
//				for (int i = 0; i < 3; i++) {
//					e[i].set(x, y, z, ev[i]);
//					f[i].set(x, y, z, fv[i]);
//					g[i].set(x, y, z, gv[i]);
//				}
//				
//				ai[0] += 1;
//			}
//			
//			@Override
//			public boolean isValid(Point3d origin, Vector3d span) {
//				return true;
//			}
//		});
//		
//		System.out.println("Computing curvatures...");
//		Matrix3d[] K = compute(e[0], e[1], e[2], f[0], f[1], f[2], g[0], g[1], g[2]);
//		System.out.println("Total time = " + timer.tick() + "s");
//		System.out.println("Total nodes = " + ai[0]);
//		return K;
//	}
	
//	/**
//	 * TODO: why did we implement this method?!!
//	 * @return
//	 */
//	public static Matrix3d[] compute(Matrix3d e1, Matrix3d e2, Matrix3d e3, Matrix3d f1, Matrix3d f2, Matrix3d f3, Matrix3d g1, Matrix3d g2, Matrix3d g3 ) {
//		Matrix3d[] K = new Matrix3d[3];
//		
//		Matrix3d[] result = IIlight(e1, e2, e3, f1, f2, f3, g1, g2, g3);
//		Matrix3d w121 = result[0];
//		Matrix3d w122 = result[1];
//		Matrix3d w123 = result[2];
//		Matrix3d w131 = result[3];
//		Matrix3d w132 = result[4];
//		Matrix3d w133 = result[5];
//
//		K[0] = w121.signOut().hpOut((w121.squareOut().addOut(w131.squareOut()) ).sqrtOut());
//		K[1] = w122.signOut().hpOut((w122.squareOut().addOut(w132.squareOut()) ).sqrtOut());
//		K[2] = w123.signOut().hpOut((w123.squareOut().addOut(w133.squareOut()) ).sqrtOut());
//
//		return K;
//	}
//	
//	public static Matrix3d[] IIlight(Matrix3d u1, Matrix3d u2, Matrix3d u3, Matrix3d v1, Matrix3d v2, Matrix3d v3, Matrix3d w1, Matrix3d w2, Matrix3d w3) {
//		// Calculate the 6 out of 9 possible fields of connecting forms w_ij<v_k> where i,j,k in {1,2,3}.
//		
//		Matrix3d[] uframe = jacobianFrame(u1, u2, u3);
////		Matrix3d[] vframe = jacobianFrame(v1, v2, v3);
////		Matrix3d[] wframe = jacobianFrame(w1, w2, w3);
//
//		// u frame extraction
//		Matrix3d u1100 = uframe[0];
//		Matrix3d u1010 = uframe[1];
//		Matrix3d u1001 = uframe[2];
//		Matrix3d u2100 = uframe[3];
//		Matrix3d u2010 = uframe[4];
//		Matrix3d u2001 = uframe[5];
//		Matrix3d u3100 = uframe[6];
//		Matrix3d u3010 = uframe[7];
//		Matrix3d u3001 = uframe[8];
//		
////		// v frame extraction
////		Matrix3d v1100 = vframe[0];
////		Matrix3d v1010 = vframe[1];
////		Matrix3d v1001 = vframe[2];
////		Matrix3d v2100 = vframe[3];
////		Matrix3d v2010 = vframe[4];
////		Matrix3d v2001 = vframe[5];
////		Matrix3d v3100 = vframe[6];
////		Matrix3d v3010 = vframe[7];
////		Matrix3d v3001 = vframe[8];
////
////		// w frame extraction
////		Matrix3d w1100 = wframe[0];
////		Matrix3d w1010 = wframe[1];
////		Matrix3d w1001 = wframe[2];
////		Matrix3d w2100 = wframe[3];
////		Matrix3d w2010 = wframe[4];
////		Matrix3d w2001 = wframe[5];
////		Matrix3d w3100 = wframe[6];
////		Matrix3d w3010 = wframe[7];
////		Matrix3d w3001 = wframe[8];
//		
//		SimpleTimer timer = new SimpleTimer();
//
//		System.out.print("Computing connection forms...");
//		timer.tick();
//		
//		Matrix3d w121 = u3.hpOut(v1.hpOut(u1001).addOut(v2.hpOut(u2001)).addOut(v3.hpOut(u3001))).addOut(u2.hpOut(v1.hpOut(u1010).addOut(v2.hpOut(u2010)).addOut(v3.hpOut(u3010)))).addOut(u1.hpOut(v1.hpOut(u1100).addOut(v2.hpOut(u2100)).addOut(v3.hpOut(u3100))));
//		Matrix3d w122 = v3.squareOut().hpOut(u3001).addOut(v2.squareOut().hpOut(u2010)).addOut(v2.hpOut(v3).hpOut(u2001.addOut(u3010)).addOut(v1.squareOut().hpOut(u1100)).addOut(v1.hpOut(v2.hpOut(u1010.addOut(u2100)).addOut(v3.hpOut(u1001.addOut(u3100))))));
//		Matrix3d w123 = v1.hpOut(w3.hpOut(u1001).addOut(w2.hpOut(u1010)).addOut(w1.hpOut(u1100))).addOut(v2.hpOut(w3.hpOut(u2001).addOut(w2.hpOut(u2010)).addOut(w1.hpOut(u2100)))).addOut(v3.hpOut(w3.hpOut(u3001).addOut(w2.hpOut(u3010)).addOut(w1.hpOut(u3100))));
//		Matrix3d w131 = u3.hpOut(w1.hpOut(u1001).addOut(w2.hpOut(u2001)).addOut(w3.hpOut(u3001))).addOut(u2.hpOut(w1.hpOut(u1010).addOut(w2.hpOut(u2010)).addOut(w3.hpOut(u3010)))).addOut(u1.hpOut(w1.hpOut(u1100).addOut(w2.hpOut(u2100)).addOut(w3.hpOut(u3100))));
//		Matrix3d w132 = v3.hpOut(w1.hpOut(u1001).addOut(w2.hpOut(u2001)).addOut(w3.hpOut(u3001))).addOut(v2.hpOut(w1.hpOut(u1010).addOut(w2.hpOut(u2010)).addOut(w3.hpOut(u3010)))).addOut(v1.hpOut(w1.hpOut(u1100).addOut(w2.hpOut(u2100)).addOut(w3.hpOut(u3100))));
//		Matrix3d w133 = w3.squareOut().hpOut(u3001).addOut(w2.squareOut().hpOut(u2010)).addOut(w2.hpOut(w3).hpOut(u2001.addOut(u3010)).addOut(w1.squareOut().hpOut(u1100)).addOut(w1.hpOut(w2.hpOut(u1010.addOut(u2100)).addOut(w3.hpOut(u1001.addOut(u3100))))));
//		
//		System.out.println(" (took " + timer.tick() + "s)");
//		
//		Matrix3d[] result = new Matrix3d[6];
//		result[0] = w121;
//		result[1] = w122;
//		result[2] = w123;
//		result[3] = w131;
//		result[4] = w132;
//		result[5] = w133;
//		
//		return result;
//	}
//	
	public static Matrix3d[] jacobianFrame(Matrix3d v1, Matrix3d v2, Matrix3d v3) {
		// Calculate the Jacobian matrix field of a vector field.
		Matrix3d[] dv1 = Matrix3d.gradient(v1, (IntensityVolumeMask) null);
		Matrix3d[] dv2 = Matrix3d.gradient(v2, (IntensityVolumeMask) null);
		Matrix3d[] dv3 = Matrix3d.gradient(v3, (IntensityVolumeMask) null);

		Matrix3d[] frame = new Matrix3d[9];
		for (int i = 0; i < 3; i++) {
			frame[i] = dv1[i];
			frame[3 + i] = dv2[i];
			frame[6 + i] = dv3[i];
		}

		return new Matrix3d[] { dv1[0], dv1[1], dv1[2], dv2[0], dv2[1], dv2[2], dv3[0], dv3[1], dv3[2] };
	}
	
	public static Matrix3d[][] jacobian(VoxelVectorField V) {
		return Matrix3d.jacobian(new Matrix3d[] { V.getX(), V.getY(), V.getZ() });
	}
	
	/**
	 * Compute all connection forms related to this model.
	 * @param hear
	 */
	public static Matrix3d[] computeOneForm(VoxelFrameField frameField, CartanModel model) {
    	final Matrix3d[] oneForms = new Matrix3d[model.getNumberOfParameters()];

    	int k = 0;
    	for (int[] connectionForm : model.getFrameIndices()) {
	    	oneForms[k++] = DifferentialOneForm.computeOneForm(frameField, MovingAxis.values()[connectionForm[0]], MovingAxis.values()[connectionForm[1]], MovingAxis.values()[connectionForm[2]]);
    	}

    	return oneForms;
	}

	public static Matrix3d computeOneForm(VoxelFrameField frameField, CartanParameter.Parameter parameter) {
		MovingAxis[] axes = parameter.asMovingAxes();
		return computeOneForm(frameField, axes[0], axes[1], axes[2]);
	}

	public static Matrix3d computeOneForm(VoxelFrameField frameField, CartanParameter.Parameter parameter, IntensityVolumeMask mask) {
		MovingAxis[] axes = parameter.asMovingAxes();
		return computeOneForm(frameField, axes[0], axes[1], axes[2], mask);
	}
	
	public static Matrix3d computeOneForm(VoxelFrameField frameField, MovingAxis i, MovingAxis j, MovingAxis k) {
		return computeOneForm(frameField, i, j, k, null);
	}
	
	public static Matrix3d computeOneForm(VoxelFrameField frameField, MovingAxis i, MovingAxis j, MovingAxis k, IntensityVolumeMask mask) {
		VoxelVectorField Ei = frameField.getField(i);
		VoxelVectorField Ej = frameField.getField(j);
		VoxelVectorField Ek = frameField.getField(k);
		
		Matrix3d onef =  computeOneForm(Ei, Ej, Ek, mask);
		onef.setName("c" + String.valueOf(i) + String.valueOf(j) + String.valueOf(k));
		return onef;
	}
	
	public static Matrix3d computeOneForm(VoxelVectorField Ei, VoxelVectorField Ej, VoxelVectorField Ek) {
		return computeOneForm(Ei, Ej, Ek, null);
	}
	
	public static Matrix3d computeOneForm(VoxelVectorField Ei, VoxelVectorField Ej, VoxelVectorField Ek, IntensityVolumeMask mask) {
		IntensityVolume[] vEi = Ei.getVolumes();
		IntensityVolume[] vEj = Ej.getVolumes();
		IntensityVolume[] vEk = Ek.getVolumes();
		
		Matrix3d[] mEi = new Matrix3d[] { vEi[0], vEi[1], vEi[2] };
		Matrix3d[] mEj = new Matrix3d[] { vEj[0], vEj[1], vEj[2] };
		Matrix3d[] mEk = new Matrix3d[] { vEk[0], vEk[1], vEk[2] };
		
		return computeOneForm(mEi, mEj, mEk, mask);
	}
	
	public static Matrix3d computeOneForm(Matrix3d[] E1, Matrix3d[] E2, Matrix3d[] E3) {
		return computeOneForm(E1, E2, E3, null);

	}
	
	/**
	 * Compute the one-form w_ij(k) where i,j,k is in (T, N, B)
	 * @param E1 
	 * @param E2
	 * @param E3 
	 * @return when taking a step in the direction Ek, describes how much Ei turns towards Ej
	 */
	public static Matrix3d computeOneForm(Matrix3d[] E1, Matrix3d[] E2, Matrix3d[] E3, IntensityVolumeMask mask) {

		// Get dimensions from first volume
		// (assume same for all volumes)
		int[] dimension = E1[0].getDimension();

		// Compute the Jacobian of E1
		// d_ij = dE1i / dxj
		Matrix3d[][] dEi = Matrix3d.jacobian(E1, mask);
		
		// Compute three rows of A = gradE1 x E3
		Matrix3d r0 = new Matrix3d(dimension);
		Matrix3d r1 = new Matrix3d(dimension);
		Matrix3d r2 = new Matrix3d(dimension);
		
		// A = product of gradE1 and E3
		for (int j = 0; j < 3; j++) {
			r0.add(dEi[0][j].hp(E3[j]));
			r1.add(dEi[1][j].hp(E3[j]));
			r2.add(dEi[2][j].hp(E3[j]));
		}
		
		// Dot product of A^T and E2
		Matrix3d result = r0.hp(E2[0]).add(r1.hp(E2[1])).add(r2.hp(E2[2]));
		return result;
	}

	public static CartanParameter computeDirect(VoxelFrameField frameField, Point3i p) {
		CartanParameter result = new CartanParameter();
		DenseMatrix Fc = new DenseMatrix(3, 6);
		DenseMatrix J = new DenseMatrix(6, 3);
		DenseMatrix F = new DenseMatrix(3, 3); 
		
		// Compute Jacobians at p
		VoxelVectorField F1 = frameField.getF1();
		VoxelVectorField F2 = frameField.getF2();
		VoxelVectorField F3 = frameField.getF3();
		
		Vector3d f1 = F1.getVector3d(p.x, p.y, p.z);
		Vector3d f2 = F2.getVector3d(p.x, p.y, p.z);
		Vector3d f3 = F3.getVector3d(p.x, p.y, p.z);
		
		double[][] J1 = Matrix3d.jacobian(F1, p);
		double[][] J2 = Matrix3d.jacobian(F2, p);
		
		// Assemble J = [ J1; J2 };
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				J.set(i, j, J1[i][j]);
				J.set(3 + i, j, J2[i][j]);
			}
		}
		
		// Assemble F
		F.set(0, 0, f1.x);
		F.set(1, 0, f1.y);
		F.set(2, 0, f1.z);
		F.set(0, 1, f2.x);
		F.set(1, 1, f2.y);
		F.set(2, 1, f2.z);
		F.set(0, 2, f3.x);
		F.set(1, 2, f3.y);
		F.set(2, 2, f3.z);
		
		// Assemble F~
		Fc.set(0, 0, f2.x);
		Fc.set(0, 1, f2.y);
		Fc.set(0, 2, f2.z);
		Fc.set(1, 0, f3.x);
		Fc.set(1, 1, f3.y);
		Fc.set(1, 2, f3.z);
		Fc.set(2, 3, f3.x);
		Fc.set(2, 4, f3.y);
		Fc.set(2, 5, f3.z);
		
		// Compute C = F~ * J * F
		DenseMatrix JF = new DenseMatrix(6, 3);
		J.mult(F, JF);
		DenseMatrix C = new DenseMatrix(3, 3);
		Fc.mult(JF, C);
		
		result.setc121(C.get(0, 0));
		result.setc122(C.get(0, 1));
		result.setc123(C.get(0, 2));

		result.setc131(C.get(1, 0));
		result.setc132(C.get(1, 1));
		result.setc133(C.get(1, 2));

		result.setc231(C.get(2, 0));
		result.setc232(C.get(2, 1));
		result.setc233(C.get(2, 2));

		return result;
	}

	public static CartanParameterNode[][][] connectionFormParametersDirect(final VoxelFrameField frameField) {
		int[] dims = frameField.getDimension();
		final CartanParameterNode[][][] result = new CartanParameterNode[dims[0]][dims[1]][dims[2]];
		final Point3i p = new Point3i();
		frameField.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				p.set(x, y, z);
				result[x][y][z] = new CartanParameterNode(DifferentialOneForm.computeDirect(frameField, p), p);
			}
		});
		
		return result;
	}

	public static List<IntensityVolume> connectionFormsDirect(final VoxelFrameField frameField) {
		final List<IntensityVolume> result = new ArrayList<IntensityVolume>(9);
		for (int i = 0; i < 9; i++)
			result.add(new IntensityVolume(frameField.getDimension()));
		
		final CartanParameterNode[][][] nodes = connectionFormParametersDirect(frameField);
		frameField.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				for (int i = 0; i < 9; i++) {
					result.get(i).set(x, y, z, nodes[x][y][z].get(i));
//					System.out.println(result.get(i).get(x, y, z));
				}
			}
		});
		
		return result;
	}

	public static List<IntensityVolume> connectionFormsIpmi(VoxelFrameField frameField) {
		return connectionFormsIpmi(frameField, frameField.getMask());
	}
	
	public static List<IntensityVolume> connectionFormsIpmi(final VoxelFrameField frameField, IntensityVolumeMask target) {
		
		/*
		 * Computation parameters
		 */
		// Nodes below this will be marked as well-conditioned
		// TODO: pass this as parameter
//		double conditionThreshold = 10;
		double conditionThreshold = 10; // use around 10 for C
//		double conditionThreshold = 3; // use 3 for V
		
		// neighborhood minimum completeness ratio for closed form computation
		double nratio = 3d / 5d;
		Norm norm = Norm.One;
		// Fiber errors above this threshold will be replaced with zero differentials
		double thresh = Math.PI / 4;

		
		/*
		 * Optimization parameters
		 */
		boolean useSeeds = true;
		int numThreads = Runtime.getRuntime().availableProcessors();
		int iterations = 500;
		double errorThreshold = 1e-8;
		double regularization = 0.0001;
//		double regularization = 100;
		CartanModel model = CartanModel.FULL;
		CartanFitter.setVerboseLevel(0);
		NeighborhoodShape neighborhoodShape = NeighborhoodShape.Isotropic;
		int nsize = 3;

		/*
		 *  IPMI2015: recompute connection forms in current available data.
		 *  Multi-step pass, identify the following:
		 *  1) island nodes -> dF = 0
		 *  2) nonsingular -> dF = closed-form
		 *  3) singular -> dF = optimized with direct seeds
		 */
				
		CartanFittingParameter fittingParameter = new CartanFittingParameter(neighborhoodShape, nsize);
//		CartanFittingParameter fittingParameter = new CartanFittingParameter(null, null, useSeeds, numThreads, iterations, errorThreshold, 0, regularization, model, neighborhoodShape, nsize);
		FittingData fittingData = new FittingData(fittingParameter, frameField, new VoxelBox(frameField.getMask()));
		List<Point3i> fullNeighborhood = VolumeSampler.sample(neighborhoodShape, nsize);
		IntensityVolumeMask maskLowRank = new IntensityVolumeMask(frameField.getDimension());
		IntensityVolumeMask maskFullRank = new IntensityVolumeMask(frameField.getDimension());
		IntensityVolumeMask maskPartialNeighborhood = new IntensityVolumeMask(frameField.getDimension());
		IntensityVolumeMask maskFullNeighborhood = new IntensityVolumeMask(frameField.getDimension());

		List<Point3i> fullrank = new LinkedList<Point3i>(); // closed-form
		List<Point3i> lowrank = new LinkedList<Point3i>(); // optimizer

		for (Point3i p : new VoxelBox(target).getVolume()) {
			if (frameField.getMask().isMasked(p))
				continue;
			
			List<Point3i> neighbors = fittingData.getNeighborhood(p);
			
			// Debug: to try only closed-form
//			maskFullRank.set(p, 1);
//			fullrank.add(p);
//			if (true)
//				continue;
			
			// 1) Full neighborhood
			if (neighbors.size() == fullNeighborhood.size()) {
				
				// compute condition number k(A) = ||A^-1|| * ||A||
				double k = MathToolset.conditionNumber(computeClosedFormMatrixC(frameField, p, neighbors), norm);
				
				maskFullNeighborhood.set(p, 1);
				if (k > conditionThreshold || Double.isNaN(k)) {
					// use optimizer
					lowrank.add(p);
					maskLowRank.set(p, 1);
//					System.out.println("BAD: " + k);
				}
				else {
					// use closed form
					fullrank.add(p);
					maskFullRank.set(p, 1);
//					System.out.println("GOOD: " + k);
				}
			}
			else {
				// Check V
				double k = MathToolset.conditionNumber(computeClosedFormMatrixC(frameField, p, neighbors), norm);

				if (k > conditionThreshold || Double.isNaN(k) || neighbors.size() < nratio * fullNeighborhood.size()) {
					// use optimizer
					lowrank.add(p);
					maskLowRank.set(p, 1);
//					System.out.println("BAD: " + k);
				}
				else {
					// use closed form
					fullrank.add(p);
					maskFullRank.set(p, 1);
//					System.out.println("GOOD: " + k);
				}
			}
		}

		/*
		 * Optimize
		 */
		VoxelFrameField frameFieldOptimized = frameField.extractField(new VoxelBox(maskLowRank));
		
		// 3) create data placeholder for the optimization: fit parameter, frame field, and computation window
		fittingParameter = new CartanFittingParameter(null, FittingMethod.Optimized, useSeeds, numThreads, iterations, errorThreshold, 0, regularization, model, neighborhoodShape, nsize);
		fittingData = new FittingData(fittingParameter, frameFieldOptimized, new VoxelBox(frameFieldOptimized.getMask()));

		// 4) this will contain the result of our optimization
		FittingExperimentResult fittingResult = new FittingExperimentResult(fittingData);

		// 5) Launch fit
		CartanFitter fitter = new CartanFitter(FittingMethod.Optimized);
		fitter.launchFit(fittingResult, true);

		// 6) validate
//		PamiExperimentRunner.validate(fittingResult);
		final List<CartanParameterNode> nodesOpti = fittingResult.getResultNodes();

		/*
		 * Closed form
		 */
		VoxelFrameField frameFieldClosed = frameField.extractField(new VoxelBox(maskFullRank));
		
		// 3) create data placeholder for the optimization: fit parameter, frame field, and computation window
		fittingParameter = new CartanFittingParameter(null, FittingMethod.ClosedForm, useSeeds, numThreads, iterations, errorThreshold, 0, regularization, model, neighborhoodShape, nsize);
		fittingData = new FittingData(fittingParameter, frameFieldClosed, new VoxelBox(frameFieldClosed.getMask()));

		// 4) this will contain the result of our optimization
		fittingResult = new FittingExperimentResult(fittingData);

		// 5) Launch fit
		fitter = new CartanFitter(FittingMethod.ClosedForm);
		fitter.launchFit(fittingResult, true);

		// 6) validate
//		PamiExperimentRunner.validate(fittingResult);
		final List<CartanParameterNode> nodesClosed = fittingResult.getResultNodes();
//		final List<CartanParameterNode> nodesClosed = null;
//		final List<CartanParameterNode> nodesOpti = null;
		
		/*
		 * Print info
		 */
		System.out.println("Optimized count = " + lowrank.size() + " | " + "Closed count = " + fullrank.size());
		
		/*
		 * Fill volumes
		 */
		final List<IntensityVolume> result = new ArrayList<IntensityVolume>(9);
		for (int i = 0; i < 12; i++)
			result.add(new IntensityVolume(frameField.getDimension()));
		boolean fillWithSignature = false;
		if (fillWithSignature) {
			for (int i = 0; i < 4; i++)
				result.get(i).fillWith(-1);
			List<Point3i> pts;
			
			// Set first volume with complete neighborhoods
			pts = new VoxelBox(maskFullNeighborhood).getVolume();
			System.out.println("Full neighborhoods = " + pts.size());
			for (Point3i p : pts) {
				result.get(0).set(p, 1);
			}

			// Second with partial
			pts = new VoxelBox(maskPartialNeighborhood).getVolume();
			System.out.println("Partial neighborhoods = " + pts.size());
			for (Point3i p : pts) {
				result.get(1).set(p, 1);
			}

			// Third with full rank
			pts = new VoxelBox(maskFullRank).getVolume();
			System.out.println("Full rank= " + pts.size());
			for (Point3i p : pts) {
				result.get(2).set(p, 1);
			}
			// Fourth with low rank
			pts = new VoxelBox(maskLowRank).getVolume();
			System.out.println("Low rank = " + pts.size());
			for (Point3i p : pts) {
				result.get(3).set(p, 1);
			}
		}
		else {
			// Merge results
			List<CartanParameterNode> nodes = new LinkedList<CartanParameterNode>();
			if (nodesClosed != null)
				nodes.addAll(nodesClosed);
			
			if (nodesOpti != null)
				nodes.addAll(nodesOpti);
			
//			System.out.println(nodes.size() + " have been found");
			
			for (CartanParameterNode node : nodes) {
				for (int i = 0; i < 12; i++)
					// first 9 volumes are cijk
					if (i < 9) {
						result.get(i).set(node.getPosition(), node.getError()[0] < thresh ? node.get(i) : 0);
//						result.get(i).set(node.getPosition(), node.get(i));
					}
					else
						// 3 last are errors
						result.get(i).set(node.getPosition(), node.getError()[i-9]);
			}
		}
		
		return result;
	}

	public static List<IntensityVolume> connectionFormsDefault(final VoxelFrameField frameField, FittingMethod method, OptimizerType type) {
		final List<IntensityVolume> result = new ArrayList<IntensityVolume>(9);
		for (int i = 0; i < 12; i++)
			result.add(new IntensityVolume(frameField.getDimension()));
		
//		final CartanParameterNode[][][] nodes = connectionFormParametersDirect(frameField);

		// 1) default parameters
		CartanFitter.setVerboseLevel(0);
		int iterations = 500;
		boolean useSeeds = true; 
		int nsize = 3;
		int numThreads = Runtime.getRuntime().availableProcessors();
//		int numThreads = 4;
		double regularization = 1e-4;
		double errorThreshold = 1e-8;
		CartanModel model = CartanModel.FULL;
		NeighborhoodShape neighborhoodShape = NeighborhoodShape.Isotropic;
		FittingMethod fittingMethod = method;
		
		// 2) set fitting parameter:  contains information for our optimizer
		CartanFittingParameter fittingParameter = new CartanFittingParameter(null, fittingMethod, useSeeds, numThreads, iterations, errorThreshold, 0, regularization, model, neighborhoodShape, nsize);
		fittingParameter.setBounds(null);
		
		// 3) create data placeholder for the optimization: fit parameter, frame field, and computation window
		FittingData fittingData = new FittingData(fittingParameter, frameField, new VoxelBox(frameField.getMask()));

		// 4) this will contain the result of our optimization
		FittingExperimentResult fittingResult = new FittingExperimentResult(fittingData);
		
		// 5) Launch fit
		CartanFitter fitter = new CartanFitter(fittingMethod);
		fitter.setOptimizerType(type);
		fitter.launchFit(fittingResult, true);

		// 6) validate
		PamiExperimentRunner.validate(fittingResult);
		final List<CartanParameterNode> nodes = fittingResult.getResultNodes();
		
		for (CartanParameterNode node : nodes) {
			for (int i = 0; i < 12; i++)
				// first 9 volumes are cijk
				if (i < 9)
					result.get(i).set(node.getPosition(), node.get(i));
				else
					// 3 last are errors
					result.get(i).set(node.getPosition(), node.getError()[i-9]);
		}
		
		return result;
	}

	
	public static List<CartanParameterNode> computeClosedFormParameterAsList(final VoxelFrameField frameField) {
		final List<CartanParameterNode> nodes = new LinkedList<CartanParameterNode>();
		final Point3i p = new Point3i();

		// Compute voxel neighborhood
		VoxelBox box = frameField.getF1().getVoxelBox();
		final NeighborhoodMap neighborMap = new NeighborhoodMap(box, NeighborhoodShape.Isotropic, 3);
		
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				p.set(x, y, z);
				List<Point3i> neighbors = neighborMap.getNeighborhood(p);
				
				// We need a minimum of three neighbors... although this might still yield an undetermined system
				// Let's use min = 4
				if (neighbors.size() >= 4)
					nodes.add(computeClosedFormParameter(frameField, p, neighbors));
			}
		});
		
		return nodes;
	}
	
	public static CartanParameterNode[][][] computeClosedFormParameter(final VoxelFrameField frameField) {
		final List<CartanParameterNode> nodes = computeClosedFormParameterAsList(frameField);
		
		int[] dims = frameField.getDimension();
		
		final CartanParameterNode[][][] result = new CartanParameterNode[dims[0]][dims[1]][dims[2]];
		for (CartanParameterNode node : nodes) {
			result[node.getPosition().x][node.getPosition().y][node.getPosition().z] = node;
		}
		
		return result;
	}
	
	/**
	 * Use 6-neighbors by default
	 * @param frameField
	 * @param x
	 * @return
	 */
	public static CartanParameterNode computeClosedFormParameter(VoxelFrameField frameField, Point3i x) {
		return computeClosedFormParameter(frameField, x, NeighborhoodShape.Isotropic, 3);
	}
	
	public static CartanParameterNode computeClosedFormParameter(VoxelFrameField frameField, Point3i x, NeighborhoodShape shape, int neighborhoodSize) {
		List<Point3i> neighbors = VolumeSampler.sample(shape, neighborhoodSize);
		
		VoxelBox box = frameField.getF1().getVoxelBox();
		List<Point3i> validated;
		if (box != null)
			validated = NeighborhoodMap.validate(box, neighbors, x);
		else
			validated = neighbors;
		
		return computeClosedFormParameter(frameField, x, validated);
	}
	
	/**
	 * Compute "V" neighbor matrix
	 * @param frameField
	 * @param x
	 * @param availableNeighbors
	 * @return
	 */
	public static DenseMatrix computeClosedFormMatrixV(VoxelFrameField frameField, Point3i x, List<Point3i> availableNeighbors) {
		/*
		 *  Precompute frame expansion variables
		 */
		int n = availableNeighbors.size();
//		System.out.println("Reconstructing from " + n + " available neighbors.");
		Vector3d[] f = frameField.get(x);
		Vector3d f1 = f[0];
		Vector3d f2 = f[1];
		Vector3d f3 = f[2];
		
		Vector3d v = new Vector3d();
		
		/*
		 *  Assemble V
		 */
		DenseMatrix V = new DenseMatrix(n, 3);
		int i = 0;
//		System.out.println("f1 = " + f[0]);
//		System.out.println("f2 = " + f[1]);
//		System.out.println("f3 = " + f[2]);
		for (Point3i p : availableNeighbors) {
			v.set(p.x, p.y, p.z);
			
			// project onto f_i at x0
			V.set(i, 0, f1.dot(v));
			V.set(i, 1, f2.dot(v));
			V.set(i, 2, f3.dot(v));
			
//			System.out.println("v = " + v);
//			System.out.println("v.F = " + V.get(i, 0) + "," + + V.get(i, 1) + ","+ + V.get(i, 2));
			
			// Next row
			i++;
		}
	
		return V;
	}
	
	public static DenseMatrix computeClosedFormMatrixC(VoxelFrameField frameField, Point3i x, List<Point3i> availableNeighbors) {
		int n = availableNeighbors.size();
//		System.out.println("Reconstructing from " + n + " available neighbors.");
		Vector3d[] f = frameField.get(x);
		Vector3d f1 = f[0];
		Vector3d f2 = f[1];
		Vector3d f3 = f[2];
		
		Point3i xv = new Point3i();
		Vector3d fv12 = new Vector3d();
		Vector3d fv13 = new Vector3d();
		Vector3d fv23 = new Vector3d();

		DenseMatrix Ctilde = new DenseMatrix(n, 3);
		int i = 0;
//		System.out.println(x);
		for (Point3i p : availableNeighbors) {
			xv.add(x, p);
			Vector3d[] fv = frameField.get(xv);
			Vector3d fv1 = fv[0];
			Vector3d fv2 = fv[1];
			Vector3d fv3 = fv[2];

//			System.out.println(xv + ": " + fv[0] + " | " + fv[1] + " | " + fv[2]);

			/*
			 *  Compute tangent projections
			 */
			// f1(v) projected onto the f1-f2 plane (f3 subtracted)
			fv12.scaleAdd(-fv1.dot(f3), f3, fv1);
			
			// f1(v) projected onto the f1-f3 plane (f2 subtracted)
			fv13.scaleAdd(-fv1.dot(f2), f2, fv1);
			
			// f2(v) projected onto the f2-f3 plane (f1 subtracted)
			fv23.scaleAdd(-fv2.dot(f1), f1, fv2);
			
			// TODO: print theta_12, theta_13, theta_23
			// print f_i^j(v)
			
			// Compute the signed angle between the projected frame vector extrapolation and the corresponding frame vector
			double dot12 = Math.abs(f1.dot(fv12) / fv12.length());
			double dot13 = Math.abs(f1.dot(fv13) / fv13.length());
			double dot23 = Math.abs(f2.dot(fv23) / fv23.length());
			
			// due to numerical round-off we need to clamp in [0, 1] before computing acos
			dot12 = MathToolset.clamp(dot12, 0, 1);
			dot13 = MathToolset.clamp(dot13, 0, 1);
			dot23 = MathToolset.clamp(dot23, 0, 1);
			
			double theta12 = Math.acos(dot12);
			double theta13 = Math.acos(dot13);
			double theta23 = Math.acos(dot23);
			theta12 *= Math.signum(f2.dot(fv12));
			theta13 *= Math.signum(f3.dot(fv13));
			theta23 *= Math.signum(f3.dot(fv23));
			
			// Compute the full connection forms c_ij<v>
			double c12vRoot = Math.tan(theta12);
			double c13vRoot = Math.tan(theta13);
			double c23vRoot = Math.tan(theta23);
			
			// Debug
//			System.out.println("\n" + p.toString());
//			System.out.println("c12 = " + c12vRoot);
//			System.out.println("c13 = " + c13vRoot);
//			System.out.println("c23 = " + c23vRoot);
			
			// Assemble C~
			Ctilde.set(i, 0, c12vRoot);
			Ctilde.set(i, 1, c13vRoot);
			Ctilde.set(i, 2, c23vRoot);
			
			// Next row
			i++;
		}
		
		return Ctilde;
	}
	
	public static CartanParameterNode computeClosedFormParameter(final VoxelFrameField frameField, Point3i x, List<Point3i> availableNeighbors) {
		
		/*
		 *  Precompute frame expansion variables
		 */
//		System.out.println("Reconstructing from " + n + " available neighbors.");
		/*
		 *  Assemble V
		 */
		DenseMatrix V = computeClosedFormMatrixV(frameField, x, availableNeighbors);
		
		/*
		 *  Assemble C~
		 */
		DenseMatrix Ctilde = computeClosedFormMatrixC(frameField, x, availableNeighbors);

		/*
		 *  Solve linear system: V * C = C~
		 */
		// All connections, written as 
		// 121 131 231
		// 122 132 232
		// 123 133 233
		
		// Print some debugging info
//		System.out.println("\nAssembling reconstruction system...");
//		for (i = 0; i < V.numRows(); i++) {
//			System.out.println("[i=" + i + "] | v=" + V.get(i, 0) + "," + V.get(i, 1) + "," + V.get(i, 2) + " | C=" + Ctilde.get(i, 0) + "," + Ctilde.get(i, 1) + "," + Ctilde.get(i, 2));
//		}

		// Solve for x = (A^T * A)-1 * A^T * B
		// QR solve
//		DenseMatrix C2 = new DenseMatrix(3, 3);
//		V.solve(Ctilde, C2);
//		CartanParameterNode parameter2 = new CartanParameterNode();
//		parameter2.setLocation(x);
//		parameter2.set(C2.getData());

		// Solve with Tikhonov regularization
		// x' = (A^T * A + M^T * M)^-1 * A^T * B
//		double gamma = 5;
//		DenseMatrix M2 = new DenseMatrix(3,3);
//		for (int d = 0; d < 3;d++)
//			M2.set(d, d, gamma * gamma);
//				
//		// A = V, b = Ctilde
//		DenseMatrix Vt = new DenseMatrix(V.numColumns(), V.numRows());
//		V.transpose(Vt);
//		
//		// Compute V^T * V
//		DenseMatrix VTV = new DenseMatrix(V.numColumns(), V.numColumns());
//		Vt.mult(V, VTV);
//		
//		// Compute V^T * V + M^T * M
//		VTV.add(M2);
//		
//		// Compute M = (V^T * V + M^T * M)^-1
//		DenseMatrix M = new DenseMatrix(MathToolset.pseudoInverse(VTV, 1e-12));
//		
//		// Compute (V^T * V + M^T * M)^-1 * V^T
//		DenseMatrix Vinv = new DenseMatrix(V.numColumns(),V.numRows());
//		M.mult(Vt, Vinv);
//		
//		// Compute result
//		DenseMatrix C3 = new DenseMatrix(3, 3);
//		Vinv.mult(Ctilde, C3);
//		CartanParameterNode parameter3 = new CartanParameterNode();
//		parameter3.setLocation(x);
//		parameter3.set(C3.getData());
//		System.out.println(parameter3);

		// SVD solve
		CartanParameterNode parameter1 = new CartanParameterNode();
		int n = availableNeighbors.size();
		try {
			DenseMatrix C1 = MathToolset.SolveSVD(V, Ctilde, 1e-12);
			parameter1.setLocation(x);
			parameter1.set(C1.getData());
		
			// Test solution (see if v * C = C~)
			DenseMatrix CtildeEstimated = new DenseMatrix(n, 3);
			V.mult(C1, CtildeEstimated);
			// System.out.println("\nTest reconstruction...");
			// for (i = 0; i < V.numRows(); i++) {
			// System.out.println("[i=" + i + "] | C=" + Ctilde.get(i, 0) + ","
			// + Ctilde.get(i, 1) + "," + Ctilde.get(i, 2) + " | C~=" +
			// CtildeEstimated.get(i, 0) + "," + CtildeEstimated.get(i, 1) + ","
			// + CtildeEstimated.get(i, 2));
			// }
		}
		catch (Exception e) {
			// usually not converged
			parameter1.set(0);
		}

		// Test first vector explicitly
//		Vector3d v1 = new Vector3d(V.get(0, 0), V.get(0, 1), V.get(0, 2));
//		Vector3d c1 = new Vector3d(C1.get(0, 0), C1.get(1, 0), C1.get(2, 0));
//		System.out.println(v1 + " x " + c1 + " = " + (v1.dot(c1)));
//		System.out.println(parameter1);
		return parameter1;
	}
	
	/**
	 * Compute the one-form w_ij(v) at point p
	 * @param E1 
	 * @param E2
	 * @param v 
	 * @return when taking a step in the direction v at p, describes how Ei turns towards Ej
	 */
	public static double computeOneForms(Matrix3d[] E1, Matrix3d[] E2, Vector3d v, Point3i p) {

		// Compute the Jacobian of E1
		// and evaluate it at p
		// d_ij = dE1i / dxj
		double[][] dEi = Matrix3d.jacobian(E1, p);
		
		// Compute three rows of A = gradE1 x E3
		double[] va = new double[] { v.x, v.y, v.z };

		double[] vdot = new double[] { 0, 0, 0 };
		// A = product of gradE1 and E3
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				vdot[i] += dEi[i][j] * va[j];
			}
		}
		
		double E2x = E2[0].get(p.x, p.y, p.z);
		double E2y = E2[1].get(p.x, p.y, p.z);
		double E2z = E2[2].get(p.x, p.y, p.z);
		
		// Dot product of A^T and E2
		return vdot[0] * E2x + vdot[1] * E2y + vdot[2] * E2z;
	}
	
	/**
	 * Compute all one-forms w_ijk at point p
	 * @param E1 
	 * @param E2
	 * @return when contracted on a vector v at p, describes how Ei turns towards Ej
	 */
	public static double[] computeOneForms(Matrix3d[] E1, Matrix3d[] E2, Point3i p) {
		// Compute the Jacobian of E1
		// and evaluate it at p
		// d_ij = dE1i / dxj
		double[][] dEi = Matrix3d.jacobian(E1, p);
		
		double[] E2v = new double[] { E2[0].get(p.x, p.y, p.z), E2[1].get(p.x, p.y, p.z), E2[2].get(p.x, p.y, p.z) };

		double[] EjdEi = new double[] { 0, 0, 0 };
		// A = product of gradE1 and E3
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 3; i++) {
				EjdEi[i] += dEi[i][j] * E2v[i];
			}
		}
		return EjdEi;
	}

	public static double computeOneForms(VoxelVectorField Ei, VoxelVectorField Ej, Vector3d v, Point3i p) {
		IntensityVolume[] vEi = Ei.getVolumes();
		IntensityVolume[] vEj = Ej.getVolumes();
		
		Matrix3d[] mEi = new Matrix3d[] { vEi[0], vEi[1], vEi[2] };
		Matrix3d[] mEj = new Matrix3d[] { vEj[0], vEj[1], vEj[2] };
		return computeOneForms(mEi, mEj, v, p);
	}

	public static double[] computeOneForms(VoxelVectorField Ei, VoxelVectorField Ej, Point3i p) {
		IntensityVolume[] vEi = Ei.getVolumes();
		IntensityVolume[] vEj = Ej.getVolumes();
		
		Matrix3d[] mEi = new Matrix3d[] { vEi[0], vEi[1], vEi[2] };
		Matrix3d[] mEj = new Matrix3d[] { vEj[0], vEj[1], vEj[2] };
		return computeOneForms(mEi, mEj, p);
	}
	
	public static FittingExperimentResult compute(FittingExperimentResult result, IntensityVolumeMask mask) {
    	SimpleTimer timer = new SimpleTimer();
    	final CartanModel model = result.getFittingData().getFittingParameter().getCartanModel();
    	final VoxelFrameField frameField = result.getFittingData().getFrameField();

    	final HashMap<Parameter, Matrix3d> formMap = new HashMap<Parameter, Matrix3d>();
    	for (Parameter p : model) {
    		if (p == Parameter.error1 || p == Parameter.error2 || p == Parameter.error3)
    			continue;
    		
    		formMap.put(p, DifferentialOneForm.computeOneForm(frameField, p, mask));
    	}

    	// Assemble list of Cartan parameters
    	final List<CartanParameterNode> cartanList = new LinkedList<CartanParameterNode>();
    	
    	result.getFittingData().getVoxelBox().voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				CartanParameterNode cp = new CartanParameterNode(model, new Point3i(x, y, z));
//				CartanParameterNode cp = new CartanParameterNode(new CartanParameter(), new Point3i(x, y, z));
				
		    	for (Parameter p : model.getAvailableConnections()) {
		    		cp.set(p, formMap.get(p).get(x, y, z));
		    	}
		    	
//		    	if (cp.isNan()) {
//		    		System.out.println("Nan detected!");
//		    		return;
//		    	}

				cartanList.add(cp);
			}
		});
    	
    	/*
    	 * Compute error
    	 */
		PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(result.getFittingData());
		
		for (CartanParameterNode node : cartanList) 
			node.setError(fittingError.evaluate(node, result.getFittingData().getNeighborhood(node.getPosition())));

    	/*
    	 * Set results
    	 */
    	result.setResults(cartanList, timer.tick());
    	
    	return result;
	}

	public static Vector3d[] extrapolate(CartanParameterNode cp, Vector3d v, Vector3d[] f, Vector3d[] fe) {
		return extrapolate(cp, v, f, fe, true);
	}

	public static Vector3d[] extrapolate(OrientationFrame frame, CartanParameter parameter, Vector3d v) {
		return extrapolate(frame, parameter, v, true);
	}
	
	public static Vector3d[] extrapolate(OrientationFrame frame, CartanParameter parameter, Vector3d v, boolean normalize) {
		Vector3d[] f = new Vector3d[] { frame.getAxis(FrameAxis.X), frame.getAxis(FrameAxis.Y), frame.getAxis(FrameAxis.Z)} ;
		Vector3d[] fe = new Vector3d[] { new Vector3d(), new Vector3d(), new Vector3d() };
		
		extrapolate(parameter, v, f, fe, normalize);
		
		return new Vector3d[] { fe[0], fe[1], fe[2] };
	}

	/**
	 * 
	 * @param cp
	 * @param v
	 * @param f
	 * @param fe
	 * @param normalize whether the resulting frame vectors should be normalized
	 * @return
	 */
	public static Vector3d[] extrapolate(CartanParameter cp, Vector3d v, Vector3d[] f, Vector3d[] fe, boolean normalize) {
		/*
		 *  Decompose v on the orthonormal basis (f1, f2, f3):
		 *  v = <v,f1> * f1 + <v,f2> * f2 + <v,f3> * f3
		 */
		double vdotf1 = v.dot(f[0]);
		double vdotf2 = v.dot(f[1]);
		double vdotf3 = v.dot(f[2]);
		
		/*
		 * Compute decomposed contraction
		 */
		// Compute c12<v>, c13<v>, c23<v>
		double c12v = vdotf1 * cp.getc121() + vdotf2 * cp.getc122() + vdotf3 * cp.getc123();
		double c13v = vdotf1 * cp.getc131() + vdotf2 * cp.getc132() + vdotf3 * cp.getc133();
		double c23v = vdotf1 * cp.getc231() + vdotf2 * cp.getc232() + vdotf3 * cp.getc233();
		
		/*
		 * Extrapolate by combining contractions for all frame axes
		 * i.e. df_i = sum_j c_ij * f_j
		 */
		
		// f1 = f1 + c12<v> * f2 + c13<v> * f3
		fe[0].x = f[0].x + c12v * f[1].x + c13v * f[2].x;
		fe[0].y = f[0].y + c12v * f[1].y + c13v * f[2].y;
		fe[0].z = f[0].z + c12v * f[1].z + c13v * f[2].z;

		// f2 = f2 - c21<v> * f1 + c23<v> * f3
		fe[1].x = f[1].x - c12v * f[0].x + c23v * f[2].x;
		fe[1].y = f[1].y - c12v * f[0].y + c23v * f[2].y;
		fe[1].z = f[1].z - c12v * f[0].z + c23v * f[2].z;

		// f3 = f3 - c13<v> * f1 - c23<v> * f2
		fe[2].x = f[2].x - c13v * f[0].x - c23v * f[1].x;
		fe[2].y = f[2].y - c13v * f[0].y - c23v * f[1].y;
		fe[2].z = f[2].z - c13v * f[0].z - c23v * f[1].z;

		// Normalize extrapolation
		if (normalize) {
			fe[0].normalize();
			fe[1].normalize();
			fe[2].normalize();
		}
		
		return fe;
	}
	
	public static CartanParameterNode[][][] asArray(final List<IntensityVolume> cijks, IntensityVolumeMask mask) {
		int[] dims = cijks.get(0).getDimension();
		final CartanParameterNode[][][] nodes = new CartanParameterNode[dims[0]][dims[1]][dims[2]];
		new VoxelBox(mask).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				nodes[x][y][z] = new CartanParameterNode();
				for (int i = 0; i < 9; i++)
					nodes[x][y][z].set(i, cijks.get(i).get(x, y, z));
			}
		});
		return nodes;
	}
	
}