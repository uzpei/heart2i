package diffgeom.fitting.error.pseudolinear;

import java.util.List;

import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.FrameFittingError;
import diffgeom.fitting.experiment.FittingData;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;

public class PseudolinearCartanFittingError extends FrameFittingError {

	private Vector3d direction;
	private Vector3d[] fData;
	private Vector3d[] fExtrapolated;
	private Vector3d[] fLocal;
	private int[] signs = new int[] { -1, 1 };
	
	public PseudolinearCartanFittingError(FittingData fittingData) {
		super(fittingData);

		// Create object storage
		fLocal  = new Vector3d[] { new Vector3d(), new Vector3d(), new Vector3d() };
		fData  = new Vector3d[] { new Vector3d(), new Vector3d(), new Vector3d() };
		fExtrapolated  = new Vector3d[] { new Vector3d(), new Vector3d(), new Vector3d() };
		direction = new Vector3d();
	}
	
	@Override
	public double[] evaluateImmersed(CartanParameterNode cartanParameter) {

		VoxelFrameField frameField = fittingData.getFrameField();
		int n = (fittingData.getFittingParameter().getNeighborhoodSize() - 1) / 2;
		
		// Initialize variables
		Point3i pvolume = cartanParameter.getPosition();
		frameField.getF1().getVector3d(pvolume.x, pvolume.y, pvolume.z, fLocal[0]);
		frameField.getF2().getVector3d(pvolume.x, pvolume.y, pvolume.z, fLocal[1]);
		frameField.getF3().getVector3d(pvolume.x, pvolume.y, pvolume.z, fLocal[2]);
		
		// Construct
		int neighborCount = 0;
		double[] error = new double[3];
		for (int i = 0; i < 3; i++) {
			for (int sign : signs) {
				for (int alpha = 1; alpha <= n; alpha++) {
					// Set computation direction
					direction.scale(sign * alpha, fLocal[i]);
					if (addError(frameField, cartanParameter, pvolume, fLocal, MathToolset.tuple3dTo3i(direction), direction, error))
						neighborCount++;
					else {
						System.nanoTime();
					}
				
				}
			}
		}
		
		error[0] /= neighborCount;
		error[1] /= neighborCount;
		error[2] /= neighborCount;
		
		return error;
	}
	
	@Override
	public double[] evaluate(CartanParameterNode cp, List<Point3i> offsets) {

		VoxelFrameField frameField = fittingData.getFrameField();
		
		// Initialize variables
		Point3i pvolume = cp.getPosition();
		frameField.getF1().getVector3d(pvolume.x, pvolume.y, pvolume.z, fLocal[0]);
		frameField.getF2().getVector3d(pvolume.x, pvolume.y, pvolume.z, fLocal[1]);
		frameField.getF3().getVector3d(pvolume.x, pvolume.y, pvolume.z, fLocal[2]);

		int neighborCount = 0;
		double[] error = new double[3];
		for (Point3i offset : offsets) {
			direction.set(offset.x, offset.y, offset.z);

			if (addError(frameField, cp, pvolume, fLocal, offset, direction, error))
				neighborCount++;
		}
		
//		if (neighborCount == 0) {
//			System.out.println("0 neighbors detected at " + pvolume);
//		}
		
//		if (Double.isNaN(error[1])) {
//			System.out.println("NaN detected at " + pvolume);
//		}
		
		error[0] /= neighborCount;
		error[1] /= neighborCount;
		error[2] /= neighborCount;
		return error;
	}

	/**
	 * Compute and add the error at an offseted location if it is located within the volume (and mask)
	 * @param frameField
	 * @param cp
	 * @param x
	 * @param direction
	 * @param error
	 * @return whether the point round(x + v) is within the volume limits and mask 
	 */
	private boolean addError(VoxelFrameField frameField, CartanParameterNode cp, Point3i x, Vector3d[] fLocal, Point3i offset, Vector3d direction, double[] error) {
		
		/*
		 *  Continue if the direction falls outside of the volume
		 */
		int xv1 = x.x + offset.x;
		int xv2 = x.y + offset.y;
		int xv3 = x.z + offset.z;
		
		if (fittingData.getVoxelBox().isMaskedUnbounded(xv1, xv2, xv3))
			return false;
		
		
		// Extrapolate in the direction v
		DifferentialOneForm.extrapolate(cp, direction, fLocal, fExtrapolated);
		
		/*
		 * 	 Fetch true neighboring frame vectors
		 */
		frameField.getF1().getVector3d(xv1, xv2, xv3, fData[0]);
		frameField.getF2().getVector3d(xv1, xv2, xv3, fData[1]);
		frameField.getF3().getVector3d(xv1, xv2, xv3, fData[2]);
		
		// Compute error
		error[0] += 1 - Math.abs(fExtrapolated[0].dot(fData[0]));
		error[1] += 1 - Math.abs(fExtrapolated[1].dot(fData[1]));
		error[2] += 1 - Math.abs(fExtrapolated[2].dot(fData[2]));
		
		// Compute distance between near-orthonormal matrices: tr(X'Y)
		
		return true;
	}
}
