package diffgeom.fitting.experiment;


public abstract class FittingCompletionListener {

	public abstract void fittingCompleted(FittingExperimentResult result);

}
