package diffgeom.fitting.experiment;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3i;

import diffgeom.fitting.core.CartanOptimizer;
import diffgeom.fitting.core.FrameFittingError;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;
import voxel.VoxelFrameField;
import app.pami2013.cartan.CartanParameter.Parameter;

public class FittingData {

	private CartanFittingParameter parameter;
	
	private VoxelFrameField frameField;
	
	private VoxelBox voxelBox;
	
	private HashMap<Point3i, List<Point3i>> neighborhoodMap;
	
	public FittingData(CartanFittingParameter parameter, VoxelFrameField frameField, VoxelBox box) {
		this.parameter = parameter;
		this.frameField = frameField;
		this.voxelBox = box;
		
		buildNeighborhoods();
	}
	
	public VoxelFrameField getFrameField() {
		return frameField;
	}
	
	public VoxelBox getVoxelBox() {
		return voxelBox;
	}
	
	/**
	 * Create a deep copy (including backing data) of this fitting parameter.
	 * @return
	 */
	public FittingData deepCopy() {
		// Create a deep copy of the voxel box.
		VoxelBox boxCopy = new VoxelBox(getVoxelBox());
		
		// Create a deep copy of the fiber orientation.
		VoxelFrameField fieldCopy = new VoxelFrameField(getFrameField(), boxCopy);
		
		// Create the fitting parameter.
		FittingData copy = new FittingData(parameter.copy(), fieldCopy, boxCopy);
		
		buildNeighborhoods();
		
		return copy;
	}

	
	/**
	 * Create an optimizer. TODO: add choice of optimizer as private field (yet another parameter to control...)
	 * @return
	 */
	public CartanOptimizer createOptimizer(OptimizerType optimizer) {
		return CartanOptimizer.createInstance(optimizer, this);
	}

	/**
	 * TODO: add choice of fitting error
	 * @return
	 */
	public FrameFittingError createFittingErrorMeasure() {
		return new PseudolinearCartanFittingError(this);
	}

	/**
	 * Remove all boundary points from a list of points.
	 * @param allpoints
	 * @return
	 */
	public List<Point3i> removeBoundary(List<Point3i> allpoints) {
        List<Point3i> validPoints = new LinkedList<Point3i>();
        Point3i pn = new Point3i();
        List<Point3i> neighbors = parameter.getNeighborhood();
        VoxelBox voxelBox = getVoxelBox();
        
        for (Point3i pt : allpoints) {
        	boolean discard = false;
        	for (Point3i n : neighbors) {
        		pn.add(pt, n);
        		
        		if (!voxelBox.dimensionContains(pn) || voxelBox.isOutside(pn.x, pn.y, pn.z)) {
        			discard = true;
        			break;
        		}
        	}
        	
        	if (!discard) {
        		validPoints.add(pt);
        	}
        }
        
        return validPoints;
	}

	public boolean isBoundary(int x, int y, int z) {
		return isBoundary(getVoxelBox(), getFittingParameter(), x, y, z);
	}
	
	/**
	 * @param pt
	 * @return whether a given point is located on or outside of the boundary.
	 */
	public static boolean isBoundary(VoxelBox voxelBox, CartanFittingParameter parameter, int x, int y, int z) {
		Point3i pn = new Point3i();
		List<Point3i> neighbors = parameter.getNeighborhood();
		boolean isBoundary = false;
		// Make sure no neighbor sits outside of the volume
		for (Point3i n : neighbors) {
			pn.x = x + n.x;
			pn.y = y + n.y;
			pn.z = z + n.z;

			if (!voxelBox.dimensionContains(pn)
					|| voxelBox.isOutside(pn.x, pn.y, pn.z)) {
				isBoundary = true;
				break;
			}
		}

		return isBoundary;
	}

	public CartanFittingParameter getFittingParameter() {
		return parameter;
	}
	
	private void buildNeighborhoods() {

		neighborhoodMap = new HashMap<Point3i, List<Point3i>>();
		final List<Point3i> neighborhood = parameter.getNeighborhood();
		final Point3i pn = new Point3i();
		getVoxelBox().voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				Point3i p = new Point3i(x, y, z);
				
				if (getVoxelBox().getMask().isMasked(x, y, z)) {
					neighborhoodMap.put(p, null);
					return;
				}
				pn.set(x, y, z);
				
				// Create the list of accessible voxel neighbors for this location
				List<Point3i> accessibleNeighborhood = new LinkedList<Point3i>();
				
				// Make sure no neighbor sits outside of the volume
				for (Point3i n : neighborhood) {
					pn.x = x + n.x;
					pn.y = y + n.y;
					pn.z = z + n.z;

					// If this neighbor exists and is accessible, add it
					if (getVoxelBox().dimensionContains(pn)
							&& !getVoxelBox().isOutside(pn.x, pn.y, pn.z)) {
						accessibleNeighborhood.add(n);
					}
				}

				// No need to create a new list if we are using all voxels
				if (accessibleNeighborhood.size() == neighborhood.size()) {
					accessibleNeighborhood.clear();
					accessibleNeighborhood = neighborhood;
				}
				
				// Insert the list into the hash map
				neighborhoodMap.put(p, accessibleNeighborhood);
			}
		});
	}
	
	/**
	 * @param location
	 * @return a neighborhood of existing voxels about that point. 
	 */
	public List<Point3i> getNeighborhood(Point3i location) {
		return neighborhoodMap.get(location);
	}

	public void setNeighborhoodShape(NeighborhoodShape shape) {
		parameter.setNeighborhood(shape);
		buildNeighborhoods();
	}

	public void setFrameField(VoxelFrameField frameField) {
		this.frameField = frameField;
	}
}
