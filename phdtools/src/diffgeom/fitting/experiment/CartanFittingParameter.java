package diffgeom.fitting.experiment;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3i;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.frame.VolumeSampler;
import tools.io.XMLDocument;
import tools.xml.XMLHelper;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.setup.PamiExperimentSetup.ParameterDefinition;

/**
 * Wrapper class for storing fitting parameters
 * @author piuze
 */
public class CartanFittingParameter {

	public enum FittingMethod { 
		ClosedForm, Direct, Optimized; 
		
		@Override
		public String toString() {
			return super.toString().toLowerCase();
		}
	}
	
	public enum NeighborhoodShape { Isotropic, SixNeighbors, IsotropicXY, IsotropicXZ, IsotropicYZ, HemisphericalForward, HemisphericalBackwards }

	public final static String NodeExperimentParameters = "experimentParameters";

	public static int DEFAULT_ITERATIONS = 300;
	public static double DEFAULT_THRESHOLD = 1e-7;
	public static double DEFAULT_DELTA = 1e-8;
	public static double DEFAULT_REGULARIZATION = 0;
	public static boolean DEFAULT_USE_DIRECT_SEEDS = true;

//	private static final int DEFAULT_THREADS = 4;
	public static int DEFAULT_THREADS = Runtime.getRuntime().availableProcessors();

	private static final CartanModel DEFAULT_MODEL = CartanModel.FULL;
	
	private static final NeighborhoodShape DEFAULT_NEIGHBORHOOD_SHAPE = NeighborhoodShape.Isotropic;

	private static final int DEFAULT_NEIGHBORHOOD_SIZE = 3;

	private static final FittingMethod DEFAULT_FITTING_METHOD = FittingMethod.Direct;
	
	protected int numIterations = DEFAULT_ITERATIONS;
	
	protected double errorThreshold = DEFAULT_THRESHOLD;
	
	protected double errorDeltaThreshold = DEFAULT_DELTA;
	
	protected CartanModel cartanModel = DEFAULT_MODEL;
	
	protected double regularization = DEFAULT_REGULARIZATION;

	protected int numThreads = DEFAULT_THREADS;

	protected NeighborhoodShape neighborhoodShape = DEFAULT_NEIGHBORHOOD_SHAPE;
	
	public boolean useDirectSeeds = DEFAULT_USE_DIRECT_SEEDS;

	protected int neighborhoodSize =  DEFAULT_NEIGHBORHOOD_SIZE;
	
	protected FittingMethod fittingMethod =  DEFAULT_FITTING_METHOD;
	
	protected List<Point3i> neighborhood;
	
	protected HashMap<Parameter, double[]> bounds = null;

	@Override
	public String toString() {
		String s = "";
		s += "Fitting method = " + fittingMethod.toString() + "\n";
		s += "Cartan model = " + cartanModel.toStringLong() + "\n";
		s += Runtime.getRuntime().totalMemory() / 1048576d + "MB" + " used out of " + Runtime.getRuntime().maxMemory() / 1048576d + "MB available" + "\n";
		s += numThreads + " thread(s) created from " + Runtime.getRuntime().availableProcessors() + " available processors \n";
		
		if (fittingMethod == FittingMethod.Optimized) {
			s += "Using direct seeds = " + useDirectSeeds + "\n";
			s += "Iterations = " + numIterations + "\n";
			s += "Error threshold = " + errorThreshold + "\n";
			s += "Error delta threshold = " + errorDeltaThreshold + "\n";
			s += "Regularization = " + regularization + "\n";
		}
		
		s += "Neighborhood shape = " + neighborhoodShape.toString() + "\n";
		s += "Neighborhood size = " + neighborhood.size() + "\n";
		s += "Neighborhood: " + neighborhood + "\n";
		
		return s;
	}
	
	public CartanFittingParameter() {
		this(null, DEFAULT_FITTING_METHOD, DEFAULT_USE_DIRECT_SEEDS, DEFAULT_THREADS, DEFAULT_ITERATIONS, DEFAULT_THRESHOLD, DEFAULT_DELTA, DEFAULT_REGULARIZATION, DEFAULT_MODEL, DEFAULT_NEIGHBORHOOD_SHAPE, DEFAULT_NEIGHBORHOOD_SIZE); 
	}
	
	/**
	 * @param targetFrameField the target frame field to optimize
	 * @param voxelBox a {@code VoxelBox} containing information about the volume and fitting region
	 * @param numIterations the number of optimization iterations
	 * @param errorThreshold the optimization error threshold for accepting a fit
	 * @param errorDeltaThreshold the delta optimization error threshold for accepting a fit
	 * @param cartanModel the {@code CartanModel} to use for optimizing (e.g. helicoid, homeoid, etc.)
	 * @param neighborhoodSize the isotropic neighborhood size
	 */
	public CartanFittingParameter(HashMap<Parameter, double[]> bounds, FittingMethod method, boolean useDirectSeeds, int numThreads, int numIterations, double errorThreshold, double errorDeltaThreshold, double regularization, CartanModel cartanModel, NeighborhoodShape neighborhoodShape, int neighborhoodSize) {

		this.bounds = bounds;
		this.fittingMethod = method;
		this.numThreads = numThreads;
		this.useDirectSeeds = useDirectSeeds;
		this.numIterations = numIterations;
		this.errorThreshold = errorThreshold;
		this.errorDeltaThreshold = errorDeltaThreshold;
		this.regularization = regularization;
		this.cartanModel = cartanModel;
		this.neighborhoodShape = neighborhoodShape;
		this.neighborhoodSize = neighborhoodSize;
		
		buildNeighborhood();
	}
	
	public void setBounds(HashMap<CartanParameter.Parameter, double[]> bounds) {
		this.bounds = bounds;
	}
	
	public CartanFittingParameter(CartanFittingParameter other) {
		this(other.bounds, other.fittingMethod, other.useDirectSeeds, other.numThreads, other.numIterations, other.errorThreshold, other.errorDeltaThreshold, other.regularization, other.cartanModel, other.neighborhoodShape, other.neighborhoodSize);
	}
	
	public CartanFittingParameter(HashMap<Parameter, double[]> bounds, CartanModel model, FittingMethod method, int neighborhoodSize) {
		this(bounds, method, DEFAULT_USE_DIRECT_SEEDS, DEFAULT_THREADS, DEFAULT_ITERATIONS, DEFAULT_THRESHOLD, DEFAULT_DELTA, DEFAULT_REGULARIZATION, model, DEFAULT_NEIGHBORHOOD_SHAPE, neighborhoodSize); 
	}

	public CartanFittingParameter(HashMap<Parameter, double[]> bounds, int neighborhoodSize) {
		this(bounds, DEFAULT_FITTING_METHOD, DEFAULT_USE_DIRECT_SEEDS, DEFAULT_THREADS, DEFAULT_ITERATIONS, DEFAULT_THRESHOLD, DEFAULT_DELTA, DEFAULT_REGULARIZATION, DEFAULT_MODEL, DEFAULT_NEIGHBORHOOD_SHAPE, neighborhoodSize); 
	}

	public CartanFittingParameter(ParameterDefinition definition) {
		this(null, definition.model, definition.method, definition.neighborhoodSize);
	}

	public CartanFittingParameter(NeighborhoodShape neighborhoodShape,
			int neighborhoodSize) {
		this();
		this.neighborhoodShape = neighborhoodShape;
		this.neighborhoodSize = neighborhoodSize;
		buildNeighborhood();
	}

	public CartanFittingParameter copy() {
		return new CartanFittingParameter(this);
	}

	private void buildNeighborhood() {
		neighborhood = VolumeSampler.sample(neighborhoodShape, neighborhoodSize);
	}
	
	public int getNumIterations() {
		return numIterations;
	}
	
	public double getErrorThreshold() {
		return errorThreshold;
	}
	
	public double getErrorDeltaThreshold() {
		return errorDeltaThreshold;
	}
	
	public CartanModel getCartanModel() {
		return cartanModel;
	}
	
	public List<Point3i> getNeighborhood() {
		return neighborhood;
	}

	public int getNumThreads() {
		return numThreads;
	}
	
	public double getRegularization() {
		return regularization;
	}
	
	public NeighborhoodShape getNeighborhoodShape() {
		return neighborhoodShape;
	}
	
	public int getNeighborhoodSize() {
		return neighborhoodSize;
	}
	
	public FittingMethod getFittingMethod() {
		return fittingMethod;
	}
	
	public boolean isUsingDirectSeeds() {
		return useDirectSeeds;
	}
	
	public final static String NodeUseDirectSeeds = "useDirectSeeds";
	public final static String NodeFittingMethod = "fittingMethod";
	public final static String NodeNeighborhoodSize = "neighborhoodSize";
	public final static String NodeNeighborhoodShape = "neighborhoodShape";
	public final static String NodeCartanModel = "cartanModel";
	public final static String NodeThreads = "numThreads";
	public final static String NodeIterations = "numIterations";
	public final static String NodeThreshold = "errorThreshold";
	public final static String NodeDeltaThreshold = "errorDeltaThreshold";
	public final static String NodeRegularization = "regularization";

	
	public void printXML(XMLDocument doc) {
		doc.addAttribute(NodeFittingMethod, fittingMethod.toString());
		doc.addAttribute(NodeUseDirectSeeds, String.valueOf(useDirectSeeds));
		doc.addAttribute(NodeNeighborhoodShape, neighborhoodShape.toString());
		doc.addAttribute(NodeNeighborhoodSize, String.valueOf(neighborhoodSize));
		doc.addAttribute(NodeThreads, String.valueOf(numThreads));
		doc.addAttribute(NodeIterations, String.valueOf(numIterations));
		doc.addAttribute(NodeThreshold, String.valueOf(errorThreshold));
		doc.addAttribute(NodeDeltaThreshold, String.valueOf(errorDeltaThreshold));
		doc.addAttribute(NodeRegularization, String.valueOf(regularization));
		doc.addAttribute(NodeCartanModel, cartanModel.toString());
	}
	
	public CartanFittingParameter loadXML(String filename) {
		Document document = null;

		try {
			document = XMLHelper.openAsDOM(new File(filename).getAbsolutePath());
			
			Element trueHeader = document.getDocumentElement();
			
			Element header = XMLHelper.getFirstChildElementByTagName(trueHeader, NodeExperimentParameters);

			// Fitting method
			fittingMethod = FittingMethod.valueOf(XMLHelper.getStringAttribute(header, NodeFittingMethod));
			System.out.println("Method = " + fittingMethod);

			// Direct seeds
			useDirectSeeds = XMLHelper.getBooleanAttribute(header, NodeUseDirectSeeds);
			System.out.println("Use direct seeds = " + useDirectSeeds);

			// Neighborhood shape
			neighborhoodShape = NeighborhoodShape.valueOf(XMLHelper.getStringAttribute(header, NodeNeighborhoodShape));
			System.out.println("Shape = " + neighborhoodShape);

			// Neighborhood size
			neighborhoodSize = XMLHelper.getIntAttribute(header, NodeNeighborhoodSize);
			System.out.println("Size = " + neighborhoodSize);

			// Num threads
			numThreads = XMLHelper.getIntAttribute(header, NodeThreads);
			System.out.println("Threads = " + numThreads);

			// Num iterations
			numIterations = XMLHelper.getIntAttribute(header, NodeIterations);
			System.out.println("Iterations = " + numIterations);

			// Error threshold
			errorThreshold = XMLHelper.getFloatAttribute(header, NodeThreshold);
			System.out.println("Error threshold = " + errorThreshold);

			// Error delta threshold
			errorDeltaThreshold = XMLHelper.getFloatAttribute(header, NodeDeltaThreshold);
			System.out.println("Error delta threshold = " + errorDeltaThreshold);

			// Regularization
			regularization = XMLHelper.getFloatAttribute(header, NodeRegularization);
			System.out.println("Regularization = " + regularization);

			// Cartan model
			cartanModel = CartanModel.valueOf(XMLHelper.getStringAttribute(header, NodeCartanModel));
			System.out.println("Cartan model = " + cartanModel);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return this;
	}
	
	public HashMap<Parameter, double[]> getBounds() {
		return bounds ;
	}

	public void setNumIterations(int iterations) {
		numIterations = iterations;
	}

	public void setNeighborhood(NeighborhoodShape shape) {
		this.neighborhoodShape = shape;
		buildNeighborhood();
	}
}
