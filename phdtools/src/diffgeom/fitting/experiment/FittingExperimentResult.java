package diffgeom.fitting.experiment;

import java.util.List;

import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import app.pami2013.cartan.CartanParameterNode;

public class FittingExperimentResult {

	private FittingData fittingData;
	
//	private Heart heart;
	
	public static int verboseLevel = 2; 
	
	private List<CartanParameterNode> result;
	
	private double elapsedTime;
	
	private boolean inProgress = false;
	
	public FittingExperimentResult(FittingData fittingData) {
		this.fittingData = fittingData;
		markInProgress();
	}
	
	public List<CartanParameterNode> getResultNodes() {
		return result;
	}
	
	public double getElapsedTime() {
		return elapsedTime;
	}
	
	public FittingData getFittingData() {
		return fittingData;
	}
	
	public void setResults(List<CartanParameterNode> result, double elapsedTime) {
		this.result = result;
		this.elapsedTime = elapsedTime;
		inProgress = false;
		markAsFinished();
	}
	
	public void markInProgress() {
		inProgress = true;
	}
	
	public void markAsFinished() {
		inProgress = false;
	}
	
	public boolean inProgress() {
		return inProgress;
	}

	public FittingMethod getFittingMethod() {
		return fittingData.getFittingParameter().getFittingMethod();
	}

	@Override
	public String toString() {
		String out = "";
		CartanParameterNode mean = CartanParameterNode.computeMean(result, getFittingParameter().getCartanModel());
		CartanParameterNode std = CartanParameterNode.computeStd(result, getFittingParameter().getCartanModel());
		
		out += result.size() + " valid voxels used out of " + fittingData.getVoxelBox().countVoxels() + " available." + "\n";
		out += "Mean parameter \n" + mean.toStringShort() + "\n";
		
		for (int i = 0; i < 3; i++)
			out += "Mean error e" + (i+1) + " (deg) = " + mean.getErrorAngleDegrees()[i] + " +- " + std.getErrorAngleDegrees()[i] + "\n";
		
		out += "Mean error avg (deg) = " + mean.getErrorSumDegrees() / 3 + "\n";
        out += "Elapsed time = " + elapsedTime + "s" + "\n";
        
        return out;
	}
	
	public CartanFittingParameter getFittingParameter() {
		return getFittingData().getFittingParameter();
	}
}
