package diffgeom.fitting.experiment;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;

import java.awt.Color;
import java.io.File;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import system.object.Pair;
import tools.TextToolset;
import tools.geom.MathToolset;
import tools.io.XMLDocument;
import tools.xml.XMLHelper;
import voxel.VoxelBox.CuttingPlane;
import app.pami2013.IterativeFilter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.core.setup.PamiExperimentSetup;
import app.pami2013.core.setup.PamiExperimentSetup.ParameterDefinition;

public class FittingExperiment {

	public enum ExperimentType {
		/**
		 * Use this type indicating the experiment will run once, without processing the results further.
		 */
		RunOnly(""),
		/**
		 * This experiment targets the smoothing strentgh.
		 */
		Smoothing("$\\sigma$"), 
		/**
		 * This experiment targets the neighborhood size in the fitting and error methods.
		 */
		NeighborhoodSize("$\\Omega$"), 
		/**
		 * This experiment targets the type of connection form model used (e.g. 1-form, homeoid, GHM, etc.)
		 */
		CartanModel("model"), 
		/**
		 * This experiment targets the type of fitting method used (e.g. direct, optimized, closed-form)
		 */
		FittingMethod("method"); 
		
		/**
		 * TODO: this should actually be set in the LatexTable class... move it there.
		 */
		private String latexString;
		
		private ExperimentType(String latexString) {
			this.latexString = latexString;
		}
		
		public String toStringLatex() {
			return latexString;
		}
	}
	
	/**
	 * List of XML parameters to save/load.
	 */
	private final static String NodeHearts = "hearts";
	public final static String NodeOutputPath = "outputPath";
	public final static String NodeSmoothingSigmas = "smoothingSigmas";
	
	private Map<Species, List<Heart>> mapSpecies;
	private String outputPath;
//	private List<IterativeFilter> smoothingFilters;
	
	private IterativeFilter currentFilter;
	
	private CartanFittingParameter fittingParameter;
	
	private FittingExperimentResult currentResult;


	private CuttingPlane plane;
	
	private PamiExperimentSetup setup;
	private List<HeartDefinition> definitions;

	/**
	 * Construct a new experiment parameter with default values.
	 */
	public FittingExperiment() {
		// Create an empty list of hearts.
		this(new LinkedList<Heart>(), null);
	}

	/**
	 * Load an experiment parameter from an XML file.
	 * @param filename
	 */
	public FittingExperiment(String filename) {
		load(filename);
	}

	/**
	 * Create a default simulation for this list of hearts.
	 * @param hearts
	 * @param outputpath
	 */
	public FittingExperiment(List<Heart> hearts, String outputPath) {
		this.outputPath = outputPath;
		
		fittingParameter = new CartanFittingParameter();
		
		updateHearts(hearts);
	}

	public FittingExperiment(List<HeartDefinition> hearts, PamiExperimentSetup setup, CuttingPlane plane, String outputPath) {
		fittingParameter = new CartanFittingParameter(setup.currentGlobalDefinition());
		set(hearts);
		this.setup = setup;
		this.outputPath = outputPath;
		this.plane = plane;
		
	}

	public CuttingPlane getCuttingPlane() {
		return plane;
	}
			
	public PamiExperimentSetup getExperimentSetup() {
		return setup;
	}

	private void setDefinition(ParameterDefinition definition) {
		fittingParameter = new CartanFittingParameter(definition);
		currentFilter = definition.filter;	
	}
	
	public boolean nextParameter() {
		ParameterDefinition definition = setup.nextParameter();
		
		if (definition == null) 
			return false;
		
		setDefinition(definition);
		
		return true;
	}

	public boolean nextVariable() {
		ParameterDefinition definition = setup.nextVariable();
		
		if (definition == null) 
			return false;
		
		setDefinition(definition);
		
		return true;
	}

	public boolean nextSetup() {
		ParameterDefinition definition = setup.nextGobalDefinition();
		
		if (definition == null) 
			return false;
		
		setDefinition(definition);
		
		return true;
	}
	
	public CartanFittingParameter getFittingParameter() {
		return fittingParameter;
	}
	
	/**
	 * Set to default settings and loads a list of hearts stored as directories.
	 * @param heartDirectories
	 * @param outputPath
	 */
	public void set(List<HeartDefinition> heartDefinitions) {
		this.definitions = heartDefinitions;
		List<Heart> hearts = new LinkedList<Heart>();
		for (HeartDefinition definition: heartDefinitions) {
			Heart heart = new Heart();
			heart.set(definition);
			hearts.add(heart);
		}
		
		updateHearts(hearts);
	}
	
	public List<HeartDefinition> getHeartDefinitions() {
		return definitions;
	}
	
	private void updateHearts(List<Heart> hearts) {
		constructSpeciesMap(hearts);
	}

	public List<Pair<Species, List<Heart>>> getSubjectsPerSpecies() {
		List<Pair<Species, List<Heart>>> species = new LinkedList<Pair<Species, List<Heart>>>();
		
		for (Species s : mapSpecies.keySet()) {
			species.add(new Pair<Species, List<Heart>>(s, mapSpecies.get(s)));
		}
		
		return species;
	}
	
	public List<Heart> getSpecies(Species s) {
		return mapSpecies.get(s);
	}

	/**
	 * Construct a map from species to list of hearts.
	 * @param hearts
	 */
	private void constructSpeciesMap(List<Heart> hearts) {
		
		// Construct a new map
		mapSpecies = new HashMap<Heart.Species, List<Heart>>();

		// Compile the map
		for (Heart heart : hearts) {
			// Get the species of this heart
			Species s = heart.getSpecies();
			
			// If the list (map value) for this species does not already exist, create it
			if (!mapSpecies.containsKey(s))
				mapSpecies.put(s, new ArrayList<Heart>());

			// Add this heart to the appropriate list (map value)
			getSpecies(s).add(heart);
		}
	}


	public void save(String filename) {
		XMLDocument doc = new XMLDocument(filename);

		// Header
		doc.pushNodeList("Experiment");
		
		// Experiment parameters
		doc.pushNodeList(CartanFittingParameter.NodeExperimentParameters, true);
		
		// Add experiment parameters
		doc.addAttribute(NodeOutputPath, outputPath);
//		doc.addAttribute(NodeSmoothingSigmas, smoothingFilters.toString());
		
		// Add fitting parameters
		fittingParameter.printXML(doc);
		
		doc.popNodeList();
		
		// Add hearts clustered by species
		doc.pushNodeList(NodeHearts);
		for (Species s : Species.values()) {
			doc.pushNodeList(s.toString());
			
			if (mapSpecies.containsKey(s)) {
				// Write heart + directory
				for (Heart heart : mapSpecies.get(s)) {
					doc.addNode("heart", heart.getDirectory(), false);
				}
			}
			
			doc.popNodeList();
		}
		doc.popNodeList();

		doc.popNodeList();

		doc.save();
	}
	
	public void load(String filename) {

		// Extract the cartan fitting parameter from this file
		fittingParameter = new CartanFittingParameter().loadXML(filename);
		
		File f = new File(filename);

		mapSpecies = new HashMap<Heart.Species, List<Heart>>();

		Document document = null;

		try {
			// Open the XML document
			document = XMLHelper.openAsDOM(f.getAbsolutePath());

			// Get the header
			Element trueHeader = document.getDocumentElement();
			Element headerParameters = XMLHelper.getFirstChildElementByTagName(trueHeader, CartanFittingParameter.NodeExperimentParameters);

			// Output path
			outputPath = XMLHelper.getStringAttribute(headerParameters, NodeOutputPath);
			System.out.println("Output = " + outputPath);

			// Smoothing sigmas
//			smoothingSigmas = XMLHelper.getDoubleArrayAttribute(headerParameters, NodeSmoothingSigmas);
//			System.out.println("Smoothing sigmas = " + Arrays.toString(smoothingSigmas));

			// Get list of species / hearts
			Element headerHearts = XMLHelper.getFirstChildElementByTagName(trueHeader, NodeHearts);
			List<Element> elements = XMLHelper.getChildElementList(headerHearts);
			List<Heart> hearts = new LinkedList<Heart>();
			for (Element e : elements) {
				
				// Parse species
				Species s = Species.valueOf(e.getNodeName());

				// Get list of hearts for that species
				List<Element> heartElements = XMLHelper.getChildElementList(e);

				System.out.println("Listing " + s);
				for (Element heart : heartElements) {
					String path = XMLHelper.getStringAttribute(heart, "value");
					System.out.println("Loading from " + path);
//					hearts.add(new Heart(path));
				}
			}
			
			constructSpeciesMap(hearts);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// Test a dummy xml file
		String filename = "./data/";
		String testFile = filename + "test.xml";
		
		List<Heart> hearts = new LinkedList<Heart>();
		Random rand = new Random();
		for (int i = 0; i < 10; i++) {
			Heart heart = new Heart();
			heart.setFilename("./data/dummy" + i + "/");
			heart.setSpecies(Species.values()[rand.nextInt(Species.values().length)]);
			hearts.add(heart);
		}
		
		FittingExperiment ep = new FittingExperiment(hearts, filename);
		ep.save(testFile);
		ep.load(testFile);
	}

	public String getOutputPathVolumeCompact(HeartDefinition heart, Parameter parameter) {
		return getOutputPath(heart) + parameter.toString().toLowerCase() + "_compact" + ".mat";
	}

	public String getImageOutputPath(Species s) {
		return getOutputPath() + "images/" + s.toString() + "/";
	}

	public String getOutputPath(HeartDefinition heart) {
		return getOutputPath(heart.species) + heart.id + "/"; 
	}
	
	public String getOutputPathVolume(HeartDefinition heart, Parameter parameter) {
		return getOutputPath(heart) + parameter.toString().toLowerCase() + ".mat";
	}

	public String getOutputVolumeFilepath(Species s) {
		return getOutputPath(s) + "species/";
	}
	
	public String getOutputVolumeFilepath(Species s, Parameter parameter) {
		return getOutputVolumeFilepath(s) + parameter.toString().toLowerCase() + ".mat";
	}

	public String getOutputPath(Species s) {
		return getExperimentParameterPath() + s + "/"; 
	}

	public String getTaggedImageOutputPath(Species s) {
		return getImageOutputPath(s) + getParameterTag();
	}
	
	public String getExperimentParameterPath() {
		return outputPath + getParameterTag() + "/"; 
	}

	public String getParameterTag() {
		return getParameterTag(fittingParameter.getCartanModel(), fittingParameter.getFittingMethod(), fittingParameter.getNeighborhoodSize(), getSmoothingFilter());
	}

	public String getParameterTag(CartanModel model, FittingMethod method, int neighborhoodSize, IterativeFilter filter) {
		if (filter == null)
			filter = new IterativeFilter(0, 0);
		
		String smoothingTag = "_" + filter.toStringFilename();
		return "result_" + model.toString() + "_" + method + "_n" + neighborhoodSize + smoothingTag; 
	}
	
	public String getOutputPath(Species s, Parameter parameter, CartanModel model, FittingMethod method, int neighborhoodSize, IterativeFilter filter) {
		return outputPath + getParameterTag(model, method, neighborhoodSize, filter) + "/" + s + "/"  + "species/" + parameter.toString().toLowerCase() + ".mat";
	}

	public String getOutputPath(Species s, Parameter parameter, ParameterDefinition definition) {
		return getOutputPath(s, parameter, definition.model, definition.method, definition.neighborhoodSize, definition.filter);
	}

	/**
	 * TODO: add more experiment information
	 * @return
	 */
	public String getExperimentHeader(Heart heart) {
		// For now only print heart info
		String header = "";
		header += heart.toString();
		return header;
	}

	public String toStringCurrent() {
		return "Method = " + fittingParameter.getFittingMethod() + " | Model = " + fittingParameter.getCartanModel() + " | Neighborhood = " + fittingParameter.getNeighborhoodSize() + " | Smoothing = " + getSmoothingFilter().toStringShort();
	}
	@Override
	public String toString() {
		String out = "";
		
		String headerTop = "";
		try {
			headerTop = "Fitting header file created by user [" + System.getProperty("user.name") + "] on [" + InetAddress.getLocalHost().getHostName() + ", " + java.lang.System.getProperty("os.arch") + "] at " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
			out += TextToolset.box(headerTop, '-') + "\n";
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out += "List of all hearts used in study = ";
		
		for (List<Heart> hearts : mapSpecies.values())
			for (Heart heart : hearts)
				out += heart.getID() + "(" + heart.getSpecies() + ") | ";
		out += "\n";
		
		out += "Cutting plane = " + plane + "\n";
		out += "Output path = " + outputPath + "\n";
		out += "Smoothing sigmas = " + currentFilter.toString() + "\n";
		
		return out;
	}
	public FittingExperiment copy() {
		FittingExperiment copy = new FittingExperiment();
		copy.mapSpecies = this.mapSpecies;
		copy.outputPath = outputPath;
		copy.fittingParameter = new CartanFittingParameter(fittingParameter);
	
		return copy;
	}
	
	public FittingExperimentResult getResult() {
		return currentResult;
	}
	
	public void setResult(FittingExperimentResult result) {
		currentResult = result;
	}

	public void reset() {
		setup.rewind();
	}

	public List<String> toString(List<Heart> hearts) {
		List<String> names = new ArrayList<String>();
//		for (Heart heart : hearts) {
//			names.add(heart.getID());
//		}
		for (int i = 0; i < hearts.size(); i++) {
			names.add(hearts.get(i).getSpecies().toString().toLowerCase() + " " + String.valueOf(TextToolset.asLetter(i)).toUpperCase());
		}
		
		return names;
	}
	
	public List<String> listVolumeNames(boolean addExtension) {
		return listVolumeNames(getFittingParameter().getCartanModel(), addExtension);
	}
	
	public List<String> listVolumeNames(CartanModel model, boolean addExtension) {

		List<String> names = new ArrayList<String>();
		
		// Add connection forms
		for (Parameter parameter : model) {
			names.add(parameter.toString() + (addExtension ? ".mat" : ""));
		}
		
		// Store error
		int numErrors = 3;
		for (int i = 0; i < numErrors; i++) {
			String label = "error" + (i+1) +  (addExtension ? ".mat" : "");
			names.add(label);
		}
		
		return names;
	}
	
	public String getOutputPath() {
		return outputPath;
	}

	public String getStatistics(Map<Parameter, double[]> volumes) {
		CartanModel model = getFittingParameter().getCartanModel();
		
		// Set number formatting to 8 decimal places
		DecimalFormat format = new DecimalFormat("#.########");
		format.setRoundingMode(RoundingMode.HALF_UP);

		String statistics = "";
		
		// Assume all volumes have the same number of voxels
		// FIXME: is this always the case?
		int numVoxels = -1;
		
		for (Parameter parameter : model) {
			if (numVoxels < 0) {
				numVoxels = volumes.get(parameter).length;
				statistics += "Number of voxels per volume = " + numVoxels + "\n";
			}
			
			double[] array = volumes.get(parameter);
			double mean = MathToolset.mean(array);
			double std = MathToolset.std(array);

			String label = parameter.toString();
			
			String units = "(rads/voxel)";
			if (parameter.toString().indexOf("error") > 0)
				units = "(rads)";
			
			statistics += "mean " + label + " = " + format.format(mean) + " +- " + format.format(std) + " " + units + "\n";
		}
		
		return statistics;
	}
	
	public IterativeFilter getSmoothingFilter() {
		return currentFilter;
	}

	/**
	 * TODO: Control each volume separately
	 * @param volumeID
	 * @return
	 */
	public boolean isDegreesVolume(Parameter parameter) {
//		return parameter == Parameter.error1 || parameter == Parameter.error2 || parameter == Parameter.error3;
		return false;
	}
	
	public String getUnit(Parameter parameter) {
		if (parameter == Parameter.error1 || parameter == Parameter.error2 || parameter == Parameter.error3) {
			return "(rad)";
		}
		else {
			return "(rad/voxel)";
		}
	}

	public String getTargetTag() {
		return setup.getTargetVariable().toString().toLowerCase() + (setup.getTargetParameter() != ExperimentType.RunOnly ? "VS" + setup.getTargetParameter().toString().toLowerCase() : "");
	}
	
	public String getTargetParameterTag() {
		return setup.getCurrentDefinition().toString(setup.getTargetParameter());
	}

	public String getTargetVariableTag() {
		return setup.getCurrentDefinition().toString(setup.getTargetVariable());
	}

	public ParameterDefinition getDefinition() {
		return setup.getCurrentDefinition();
	}
}
