package diffgeom.fitting;

import heart.Heart;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import voxel.VoxelBox.CuttingPlane;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.fitting.core.CartanFitter;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;

public class CartanFitterApp {

//	private static final String DEFAULT_HEART_FILENAME = "/usr/local/data/heart_sample/";
	private static final String DEFAULT_HEART_FILENAME = "/private/tmp/";
	
    public static void main(String[] args) throws InterruptedException {
    	// List of arguments:
		// - Target directory
		// - Output directory
    	// - CuttingPlane
		// - Neighborhood size
		// - Number of iterations
    	// - error threshold
    	// - error delta threshold
    	// - # of cores (<= 0 means auto)
		// - verbose level (0 = off, 1 = minimal, 2 = full);
		// - List of Cartan models

    	LinkedList<CartanModel> models = new LinkedList<CartanModel>();
    	
    	if (args.length == 0) {
    		CuttingPlane cp = CuttingPlane.CUT;
        	int numIts = 150;
        	double errorThreshold = 1e-5;
        	double errorDeltaThreshold = 1e-6;
        	int nsize = 3;
        	int numThreads = Runtime.getRuntime().availableProcessors();
        	String dir = DEFAULT_HEART_FILENAME;
        	String outdir = dir + "fitting/";
    		double regularization = 1e-6;

//        	for (CartanModel m : CartanModel.values())
//        		models.add(m);
        	models.add(CartanModel.C123);
        	
        	int verboseLevel = 2;
    		launchMockupSimulation(dir, outdir, cp, numIts, errorThreshold, errorDeltaThreshold, regularization, nsize, numThreads, models, verboseLevel);
    	}
    	else {
    		int i = 0;
    		
    		String dir = args[i++];
    		String outdir = args[i++];
    		
    		// Select Cutting Plane
    		CuttingPlane cp = null;
			try {
				cp = Enum.valueOf(CuttingPlane.class, args[i++].toUpperCase());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.println("Invalid Cartan Model (" + args[i - 1]
						+ "). Use one of the following: ");
				for (CartanModel c : CartanModel.values()) {
					System.out.println(c.toString());
				}
			}
    		
    		int nsize = Integer.parseInt(args[i++]);
    		int numits = Integer.parseInt(args[i++]);
    		double threshold = Double.parseDouble(args[i++]);
    		double errorDeltaThreshold = Double.parseDouble(args[i++]);
    		double regularization = Double.parseDouble(args[i++]);
    		int numcores = Integer.parseInt(args[i++]);
    		numcores = numcores > 0 ? numcores : Runtime.getRuntime().availableProcessors();
    		int verboseLevel = Integer.parseInt(args[i++]);

    		// List Cartan models
			try {
				for (; i < args.length; i++) {
					CartanModel model = Enum.valueOf(CartanModel.class, args[i].toUpperCase());
					models.add(model);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.println("Invalid Cartan Model (" + args[i - 1]
						+ "). Use one of the following: ");
				for (CartanModel c : CartanModel.values()) {
					System.out.println(c.toString());
				}
			}
			
    		launchMockupSimulation(dir, outdir, cp, numits, threshold, errorDeltaThreshold, regularization, nsize, numcores, models, verboseLevel);

    	}
    }

    /**
     * 
     * @param dir
     * @param outdir
     * @param c
     * @param numIts
     * @param errorThreshold
     * @param errorDeltaThreshold
     * @param regularization
     * @param nsize
     * @param numThreads
     * @param models possibility to run multiple models
     * @param verboseLevel
     */
    public static void launchMockupSimulation(String dir, final String outdir, CuttingPlane c, int numIts, double errorThreshold, double errorDeltaThreshold, double regularization, int nsize, int numThreads, List<CartanModel> models, final int verboseLevel) {
    	
		if (!new File(outdir).exists())
			new File(outdir).mkdir();

    	final Heart heart = new Heart(dir);
    	
    	if (verboseLevel > 0)
    		System.out.println("Cutting plane = " + c.toString());

    	heart.getVoxelBox().cut(c);
    	
    	for (CartanModel m : models) {
    		if (verboseLevel > 0)
    			System.out.println("\n\nLaunching fit with model " + m);

	    	final HeartFittingParameter fittingParameter = new HeartFittingParameter(heart, FittingMethod.Optimized, numThreads, numIts, errorThreshold, errorDeltaThreshold, regularization, m, NeighborhoodShape.Isotropic, nsize);

        	final CartanFitter fitter = new CartanFitter();
        	fitter.setVerboseLevel(verboseLevel);
        	
	    	fitter.launchFit(fittingParameter, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					List<CartanParameterNode> result = fitter.getFittingResult();

					if (verboseLevel > 1) {
						CartanParameterNode mean = CartanParameterNode.computeMean(result);
						System.out.println("Mean parameter = \n" + mean.toStringShort());
						System.out.println("Mean error sum (deg) = " + Math.toDegrees(Math.acos(1 - mean.getErrorSum()/3)));
						System.out.println("Mean error e1 (deg) = " + Math.toDegrees(Math.acos(1 - mean.getError()[0])));
						System.out.println("Mean error e2 (deg) = " + Math.toDegrees(Math.acos(1 - mean.getError()[1])));
						System.out.println("Mean error e3 (deg) = " + Math.toDegrees(Math.acos(1 - mean.getError()[2])));
					}

					String header = "";
					header += heart.toString();

					try {
						fitter.export(outdir + fittingParameter.getCartanModel().toString().toLowerCase() + "/", header);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
	    	
	    	while (!fitter.isFinishedRunning() || !fitter.hasExported()) {
	    		try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    	}
    	}
    }
}
