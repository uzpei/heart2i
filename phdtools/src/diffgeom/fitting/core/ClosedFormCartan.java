package diffgeom.fitting.core;

import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;

import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.DifferentialOneForm;
import diffgeom.DifferentialOneForm.MovingAxis;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter;
import diffgeom.fitting.experiment.FittingCompletionListener;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.Matrix;
import tools.SimpleTimer;
import tools.TextToolset;
import tools.geom.MathToolset;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameterNode;

public class ClosedFormCartan {

    public static FittingExperimentResult compute(FittingExperimentResult result) {
    	return compute(result, null);
    }
    
    public static FittingExperimentResult compute(FittingExperimentResult result, FittingCompletionListener completionListener) {

    	SimpleTimer timer = new SimpleTimer();
    	
    	// Fill result here
    	FittingData fittingData = result.getFittingData();
        VoxelBox voxelBox = fittingData.getVoxelBox();
        List<Point3i> allpoints = voxelBox.getVolume();
        List<Point3i> points = fittingData.removeBoundary(allpoints);

    	List<Point3i> neighbors = result.getFittingData().getFittingParameter().getNeighborhood();
    	VoxelFrameField f = result.getFittingData().getFrameField();
    	VoxelVectorField f1 = f.getF1();
    	VoxelVectorField f2 = f.getF2();
    	VoxelVectorField f3 = f.getF3();
    	
    	// Prepare storage
    	DenseMatrix GammaT = new DenseMatrix(3, 6);
		DenseMatrix GammaTGamma = new DenseMatrix(3, 3);
		DenseMatrix vgammaTGamma = new DenseMatrix(3, 3);
		DenseVector vecCt = new DenseVector(3 * 3);
		double singularThreshold = 1e-12;
    	List<CartanParameterNode> fitResults = new LinkedList<CartanParameterNode>();

		// Process all voxels
    	Vector3d v = new Vector3d();
    	Vector3d f1x = new Vector3d();
    	Vector3d f2x = new Vector3d();
    	Vector3d f3x = new Vector3d();
        for (Point3i p : points) {

        	// Evaluate frame vectors at x_0
        	f1.getVector3d(p.x, p.y, p.z, f1x);
        	f2.getVector3d(p.x, p.y, p.z, f2x);
        	f3.getVector3d(p.x, p.y, p.z, f3x);
        	
        	// Start processing this voxel
        	DenseVector sumNumerator = new DenseVector(3*3);
        	DenseMatrix sumDenominator = new DenseMatrix(9,9);
        	
        	for (Point3i vn : neighbors) {
        		// Construct (vn . f1, vn . f2, vn . f3)
        		v.x = f1x.x * vn.x + f1x.y * vn.y + f1x.z * vn.z;
        		v.y = f2x.x * vn.x + f2x.y * vn.y + f2x.z * vn.z;
        		v.z = f3x.x * vn.x + f3x.y * vn.y + f3x.z * vn.z;
        		
        		// Assemble Gamma
        		DenseMatrix Gamma = Gamma(p, vn, f);
        		
        		// Assemble gamma
        		DenseVector gamma = gamma(p, vn, f);
        		
        		// Add up vec(outer(v, gamma) * Gamma)
        		// v is 3x1, gamma is 6x1
        		// outer(v, gamma) is 3x6
        		// Gamma is 6x3
        		DenseMatrix vgammaT = MathToolset.outerProduct(vn, gamma);
        		vgammaT.mult(Gamma, vgammaTGamma);
        		sumNumerator.add(MathToolset.vectorize(vgammaTGamma));
        		
        		// Compute GammaT * Gamma => (3 x 3)
        		// and v * v^T
        		Gamma.transpose(GammaT);
        		GammaT.mult(Gamma, GammaTGamma);
        		DenseMatrix vvT = MathToolset.outerProduct(v, v);
        		
        		// Compute Kronecker product (GammaT * Gamma) (x) (v * vT)
        		DenseMatrix outerGammaV = MathToolset.kronecker(GammaTGamma, vvT);
        		sumDenominator.add(outerGammaV);
        	}
        	
        	// Compute -(denominator)^-1 * numerator
        	sumDenominator.scale(-1);
        	Matrix pseudoInv = MathToolset.pseudoInverse(sumDenominator, singularThreshold);
        	pseudoInv.mult(sumNumerator, vecCt);
        	
        	fitResults.add(new CartanParameterNode(new CartanParameter(vecCt.getData()), p));
        }
        
        result.setResults(fitResults, timer.tick());
    	
    	// Trigger listener
		if (completionListener != null)
			completionListener.fittingCompleted(result);
		
		return result;
    }	
    
    /**
     * Direct product between f_i at an offset p + v and f_j at p
     * TODO: make sure i and j are properly index.
     * @param i
     * @param j
     * @return
     */
    private static double gamma(Point3i p, Point3i v, VoxelFrameField f, int i, int j) {
    	VoxelVectorField fi = f.getField(i);
    	VoxelVectorField fj = f.getField(j);
    	
    	Vector3d fiv = fi.getVector3d(p.x + v.x, p.y + v.y, p.z + v.z);
    	Vector3d fj0 = fj.getVector3d(p.x, p.y, p.z);
    	
    	return fiv.dot(fj0);
    }
    
    
    private static DenseVector gamma(Point3i p, Point3i v, VoxelFrameField f) {
    	DenseVector gammav = new DenseVector(6);
    	gammav.set(0, gamma(p, v, f, 0, 1));
    	gammav.set(1, gamma(p, v, f, 0, 2));
    	gammav.set(2, gamma(p, v, f, 1, 0));
    	gammav.set(3, gamma(p, v, f, 1, 2));
    	gammav.set(4, gamma(p, v, f, 2, 0));
    	gammav.set(5, gamma(p, v, f, 2, 1));
    	
    	return gammav;
    }
    
    private static DenseMatrix Gamma(Point3i p, Point3i v, VoxelFrameField f) {
    	DenseMatrix m = new DenseMatrix(6, 3);

    	// Row 1
    	m.set(0, 0, -gamma(p, v, f, 0, 0));
    	m.set(0, 1, 0);
    	m.set(0, 2, gamma(p, v, f, 0, 2));

    	// Row 2
    	m.set(1, 0, 0);
    	m.set(1, 1, -gamma(p, v, f, 0, 0));
    	m.set(1, 2, -gamma(p, v, f, 0, 1));

    	// Row 3
    	m.set(2, 0, gamma(p, v, f, 1, 1));
    	m.set(2, 1, gamma(p, v, f, 1, 2));
    	m.set(2, 2, 0);

    	// Row 4
    	m.set(3, 0, 0);
    	m.set(3, 1, -gamma(p, v, f, 1, 0));
    	m.set(3, 2, -gamma(p, v, f, 1, 1));

    	// Row 5
    	m.set(4, 0, gamma(p, v, f, 2, 1));
    	m.set(4, 1, gamma(p, v, f, 2, 2));
    	m.set(4, 2, 0);

    	// Row 6
    	m.set(5, 0, -gamma(p, v, f, 2, 0));
    	m.set(5, 1, 0);
    	m.set(5, 2, gamma(p, v, f, 2, 2));

    	return m;
    }
    
}
