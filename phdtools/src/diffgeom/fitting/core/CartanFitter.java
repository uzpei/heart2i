package diffgeom.fitting.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3i;

import math.matrix.Matrix3d;
import swing.component.VerticalFlowPanel;
import swing.parameters.IntParameter;
import tools.SimpleTimer;
import tools.geom.MathToolset;
import voxel.VoxelBox;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameter.CartanModel;
import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer.OptimizerType;
import diffgeom.fitting.error.pseudolinear.PseudolinearCartanFittingError;
import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingCompletionListener;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.experiment.FittingExperimentResult;

public class CartanFitter implements ActionListener {

	private OptimizerType optimizerType = OptimizerType.SIMPLEX;
	
	/**
	 * Verbose level. 0 = quiet, 1 = minimal, 2 = full
	 */
	public static int verboseLevel = 1;
	
	/**
	 * A list of available fitting workers.
	 */
	private List<CartanWorker> workers = new LinkedList<CartanWorker>();
	
	/**
	 * Fitting results for the current volume.
	 */
	private List<CartanParameterNode> fitResults = new LinkedList<CartanParameterNode>();
	
	/**
	 * Number of threads (workers) to use for fitting.
	 */
    private IntParameter threadCount = new IntParameter("multithreading x", 0, 1, 128);

    /**
     * A timer for monitoring computational timings.
     */
    private SimpleTimer timer = new SimpleTimer();
    
    /**
     * Total fitting time in seconds
     */
    private double fittingTime = 0;
    
    private boolean postRunActionPerformed = false;
    
    private FittingCompletionListener completionListener;
    
	/**
	 * Fitting used for storing optimization parameters, includes a {@code CartanFittingParameter}.
	 */
    private FittingData fittingData;
	
    private FittingExperimentResult result;
    
    /**
     * Create a default {@code CartanFitter}
     */
    public CartanFitter() {
    	this(OptimizerType.SIMPLEX);
    }

    public CartanFitter(FittingMethod method) {
    	this(method == FittingMethod.Optimized ? OptimizerType.SIMPLEX : OptimizerType.DIRECT);
    }

    public void setOptimizerType(OptimizerType type) {
    	this.optimizerType = type;
    }
    
    public CartanFitter(OptimizerType optimizer) {
    	setOptimizerType(optimizer);
    }
    
    public void launchFit(FittingExperimentResult result, FittingCompletionListener completionListener, boolean waitForCompletion) {
    	result.markInProgress();
    	this.result = result;
    	this.fittingData = result.getFittingData();
    	
//    	hasExported = false;
    	postRunActionPerformed = false;
    	
    	this.completionListener = completionListener;
    	
    	threadCount.setValue(fittingData.getFittingParameter().getNumThreads());

    	// Target fitting voxels
        VoxelBox voxelBox = fittingData.getVoxelBox();
        List<Point3i> allpoints = voxelBox.getVolume();
        List<Point3i> points = allpoints;
        
        if (allpoints.size() == 0) {
        	// nothing to do, return
        	return;
        }
        
    	/*
    	 * Create workers and Distribute fitting volume among them
    	 */    	
        workers.clear();

        // Number of fitting voxels
        int n = points.size();
        
        // Number of fitting threads
        int numcores = threadCount.getValue();
        
        // Number of points per thread
        int nums = (int) Math.floor(n / (double) numcores);
        
        // If nums is zero then we have more than one core per pt
        // dump everything into first worker
        if (nums == 0) {
        	nums = n;
        	numcores = 1;
        }
        
        // Spit out some information before we begin
        if (verboseLevel > 0) {
            System.out.println("Using " + points.size() + " voxels out of " + allpoints.size());
        }
        
        if (verboseLevel > 1) {
            System.out.println(points.size() + " voxels in total.");
            System.out.println("Creating threads...");
        }
        
    	CartanModel model = fittingData.getFittingParameter().getCartanModel();
    	
    	// If the model is constant, we don't have to do anything.
    	// Simply create a long list of empty voxels.
    	if (model == CartanModel.CONSTANT) {
    		computeConstant(points);
        	return;
    	}
    	
        // Compute seed from direct computations
        Matrix3d[] directSeeds = null;
        if (fittingData.getFittingParameter().useDirectSeeds) {
        	directSeeds = DifferentialOneForm.computeOneForm(fittingData.getFrameField(), model);
        }
//        System.out.println("Num nodes = " + n);
//    	System.out.println(numcores);
//    	System.out.println(nums);
        for (int core = 0; core < numcores; core++) {
        	// List of seed points for this worker
            List<CartanParameterNode> seedpoints = new LinkedList<CartanParameterNode>();
        	
            // Upper bound on the voxel indices
            // The last worker gets the odd cut
        	int lim1 = core * nums;
        	int lim2 = (core != numcores - 1) ? (core+1) * nums : points.size();

        	// Subsample points for this worker
        	List<Point3i> samples = points.subList(lim1, lim2);
        	for (Point3i pt : samples) {
        		CartanParameterNode seed = new CartanParameterNode(model, pt);
        		
        		if (fittingData.getFittingParameter().useDirectSeeds) {
        			// Assign values
        			seed.set(directSeeds);
        		}
        		
        		seedpoints.add(seed);
        	}
        	
        	
        	// If no seedpoints were added then nothing to do and this worker will not be assigned any voxels.
        	if (seedpoints.size() == 0) break;
        	
        	// Debug
//        	for (CartanParameterNode node : seedpoints)
//        		System.out.println("Core #" + core + " gets " + node.getPosition());
        	
        	// Create an optimizer of the given type
        	CartanOptimizer optimizer = fittingData.createOptimizer(optimizerType);
        	
        	// Create the worker with the sampled points
        	// and a copy of the experiment parameters
        	CartanWorker worker = new CartanWorker(core + 1, optimizer, seedpoints); 
        	workers.add(worker);
        }
    	
    	// Launch workers
        timer.tick();
        
        if (verboseLevel > 1) {
            System.out.println("Launching workers...");
        }
        
        for (CartanWorker worker : workers) {
        	worker.start(this);
        }
        
        if (waitForCompletion) {
			while (result.inProgress())
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
    }
    
    private void computeConstant(List<Point3i> points) {
    	timer.tick();
    	
    	fitResults = new LinkedList<CartanParameterNode>();
		PseudolinearCartanFittingError fittingError = new PseudolinearCartanFittingError(result.getFittingData());

    	for (Point3i voxel : points) {
    		CartanParameterNode node = new CartanParameterNode(new CartanParameter(), voxel);
			node.setError(fittingError.evaluate(node, result.getFittingData().getNeighborhood(node.getPosition())));
    		fitResults.add(node);
    	}
    	
		fittingTime = timer.tick();
		if (verboseLevel > 0)
			System.out.println("Elapsed time = " + fittingTime + "s");

		result.setResults(fitResults, fittingTime);

		
		if (completionListener != null)
			completionListener.fittingCompleted(result);
		
		postRunActionPerformed = true;
    }

    public void launchFit(FittingExperimentResult result, boolean waitForCompletion) {
    	launchFit(result, null, waitForCompletion);
    }
    
    public void launchFit(FittingExperimentResult result) {
    	launchFit(result, null, false);
    }
    
    /**
     * Careful with this method. Two work threads might finish at the same time.
     */
	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		CartanWorker worker = ((CartanWorker) e.getSource());
		
		if (verboseLevel > 0)
			System.out.println("Worker thread " + worker.getIndex() + " finished.");
		
		fitResults.addAll(worker.getFittingResult());
		
		worker.terminate();
		
		// Verify if all workers have finished
		if (workersStarted() && workersFinishedWorking()) {
			fittingTime = timer.tick();

			if (verboseLevel > 0)
				System.out.println("Elapsed time = " + fittingTime + "s");

			result.setResults(fitResults, fittingTime);
			
			if (completionListener != null)
				completionListener.fittingCompleted(result);
			
			postRunActionPerformed = true;
		}
	}
	
    public JPanel getControls() {
    	VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Cartan fitting"));

        return vfp.getPanel();
    }

    private boolean workersFinishedWorking() {
		for (CartanWorker worker : workers) {
			if (worker.isRunning())
				return false;
		}
		
		return true;
    }

    private boolean workersStarted() {
		for (CartanWorker worker : workers) {
			if (!worker.hasStarted())
				return false;
		}
		
		return true;
    }

    /**
     * @return whether we are still optimizing
     */
	public boolean isFinishedRunning() {
		return postRunActionPerformed && workersFinishedWorking();
	}

	public List<CartanParameterNode> getFittingResult() {
		return fitResults;
	}
	
//	public void export(String directory) {
//		hasExported = false;
//		
////		ExperimentResult result = new ExperimentResult(fittingParameter);
//		experiment.getExperimentParameter().outputPath = directory;
//		experiment.setResults(getFittingResult(), fittingTime);
//		try {
//			PamiExperiment.export(experiment);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		hasExported = true;
//	}
	
//	public boolean hasExported() {
//		return hasExported;
//	}

	public static void setVerboseLevel(int verboseLevel) {
		CartanFitter.verboseLevel = verboseLevel;
	}

	public double getElapsedTime() {
		return fittingTime;
	}

}
