package diffgeom.fitting.core;

import java.util.List;

import javax.vecmath.Point3i;

import org.apache.commons.math3.analysis.MultivariateFunction;

import diffgeom.fitting.experiment.FittingData;
import app.pami2013.cartan.CartanParameterNode;

public abstract class FrameFittingError implements MultivariateFunction {
	public enum EnergyTarget { F1, ALL }
	
	private EnergyTarget energyTarget = EnergyTarget.ALL;
	
	private boolean sinusoidReparameterization = false;
	
	public void setEnergyTarget(EnergyTarget target) {
		this.energyTarget = target;
	}
	
	protected FittingData fittingData;
	
	/**
	 * Pre-created in the constructor to speed up computations. 
	 */
	protected CartanParameterNode computationNode;
	
	public abstract double[] evaluate(CartanParameterNode cp, List<Point3i> neighborOffsets);

	public abstract double[] evaluateImmersed(CartanParameterNode cp);

	public FrameFittingError(FittingData fittingData) {
		this.fittingData = fittingData;
		computationNode = new CartanParameterNode(fittingData.getFittingParameter().getCartanModel());
	}

	public void initializeSeed(CartanParameterNode seed) {
		computationNode.setLocation(seed.getPosition());
		
		computationNode.zero();

		if (sinusoidReparameterization) {
			// Transform seeds into inverse sinusoids
			for (int i : seed.getParameterMask()) {
				seed.set(i, Math.asin(seed.get(i)));
			}
		}
	}

	@Override
	public double value(double[] point) {
		
		if (sinusoidReparameterization) {
			// Constrained optimization trick for connection forms
			// within the range [-1, 1]
			for (int i = 0; i < point.length; i++)
				point[i] = Math.sin(point[i]);
		}
		
		// Set the computation node
		switch (energyTarget) {
		case ALL:
			computationNode.setParametersMasked(point);
			break;
		case F1:
			computationNode.setC1(point);
			break;
		}
		
//		System.out.println(computationNode.toString());
		double[] e = evaluate(computationNode, fittingData.getNeighborhood(computationNode.getPosition()));
//		double[] e = evaluateImmersed(computationNode);
		
		// Return sum of errors and add regularization term
		double errorRegularization = fittingData.getFittingParameter().getRegularization() * computationNode.getNorm1();
		switch (energyTarget) {
		case ALL:
			return errorRegularization + (e[0] + e[1] + e[2]) / 3;
		case F1:
			return errorRegularization + e[0];
		}
		
//		return (e[0] + e[1] + e[2]) / 3 + fittingData.getFittingParameter().getRegularization() * computationNode.getNorm1(); 
		return Double.NaN;
	}

}
