package diffgeom.fitting.core;

import java.util.Arrays;
import java.util.Random;

import javax.swing.JPanel;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.AbstractSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;

import diffgeom.fitting.experiment.CartanFittingParameter.FittingMethod;
import diffgeom.fitting.experiment.FittingData;
import diffgeom.fitting.method.CartanBOBYQAOptimizer;
import diffgeom.fitting.method.CartanClosedEstimator;
import diffgeom.fitting.method.CartanDirectEstimator;
import diffgeom.fitting.method.CartanSimplexOptimizer;
import tools.SimpleTimer;
import tools.TextToolset;
import tools.geom.MathToolset;
import app.pami2013.cartan.CartanParameterNode;
import app.pami2013.cartan.CartanParameter.CartanModel;

public abstract class CartanOptimizer {
	
	protected FittingData fittingData;
	
	protected FrameFittingError fittingError;
	public abstract CartanParameterNode doOptimize(CartanParameterNode seed);
	private static MultivariateOptimizer optimizer_test;

	public enum OptimizerType { 
		// Simplex-based optimizer
		SIMPLEX, 
		// nonlinear descent optimizer
		BOBYQA, 
		// direct optimizer (e.g. closed form or finite cartan)
		DIRECT
		
	};
	
	public static class OptimizerSetting {
		public FittingMethod method;
		public OptimizerType type;
		public OptimizerSetting(FittingMethod method, OptimizerType type) {
			this.method = method;
			this.type = type;
		}
	}
	
	public CartanOptimizer(FittingData fittingData) {
		
		// Create a local copy of the fitting parameters and error
		this.fittingData = fittingData.deepCopy();
		this.fittingError = fittingData.createFittingErrorMeasure();
	}
	
	public FittingData getFittingData() {
		return fittingData;
	}
	
	public abstract JPanel getControls();
	
	public CartanParameterNode optimize(CartanParameterNode seed) {
//		System.out.println("Running optimization at " + seed.getPosition());
		
		CartanParameterNode result;
		
		// If we are using the constant model 
		// or if the number of target iterations is 0 *and* the optimizer is a simplex optimizer,
		// there is nothing to optimize: simply return error
		if (getFittingData().getFittingParameter().getCartanModel() == CartanModel.CONSTANT 
				|| (fittingData.getFittingParameter().getNumIterations() <= 0 && this instanceof CartanSimplexOptimizer)) {
			result = new CartanParameterNode(seed);
			result.setError(fittingError.evaluate(result, getFittingData().getNeighborhood(seed.getPosition())));
		}
		else {
			// Initialize the seed point
			CartanParameterNode initializedSeed = new CartanParameterNode(seed);
			fittingError.initializeSeed(initializedSeed);
			result = doOptimize(initializedSeed);
		}
		
		return result;
	}

	public static void main(String[] args) {
		/*
		 * Test simple cubic polynomial regression.
		 */
		Random rand = new Random();
		int numRealizations = 1;
		final int degree = 3;
		double mag = 1;
		final MaxIter maxIter = new MaxIter(500);
		final MaxEval maxEval = MaxEval.unlimited();
		// f = ax^3 + bx^2 + cx^1 + dx^0
		SimpleTimer timer = new SimpleTimer();
		timer.tick();

		boolean constrained = false;
		
		if (constrained) {
			int interpoints = Math.max(4, Math.min(MathToolset.roundInt((degree+2) * (degree + 1) / 2d), Math.max(degree + 2, 2 * degree + 1)));
			System.out.println("Using " + interpoints + " interpolation points in BOBYQA optimizer.");
			optimizer_test = new BOBYQAOptimizer(interpoints);
		} else {
			ConvergenceChecker<PointValuePair> convergence = new ConvergenceChecker<PointValuePair>() {
				@Override
				public boolean converged(int iteration,
						PointValuePair previous, PointValuePair current) {
					// FIXME: bug in commons api: currently need to bypass the
					// <code>iteration</code> parameter since the
					// SimplexOptimizer always sets it to "0".
					return optimizer_test.getIterations() >= maxIter
							.getMaxIter();
				}
			};
			optimizer_test = new SimplexOptimizer(convergence);
		}
		
		double[] seed = new double[degree + 1];
		double[] lB = new double[degree + 1];
		double[] uB = new double[degree + 1];
		for (int coef = 0; coef <= degree; coef++) {
			lB[coef] = -mag;
			uB[coef] = mag;
			seed[coef] = 0;
		}
		SimpleBounds parameterBounds = new SimpleBounds(lB, uB);

		double error = 0;
		for (int i = 0; i < numRealizations; i++) {
			System.out.println("Realization " + i);

			// Generate random polynomial coefficients and function evaluation point
			final double[] evaluationPoint = new double[1];
			for (int coef = 0; coef < evaluationPoint.length; coef++) {
				evaluationPoint[coef] = -mag + 2 * mag * rand.nextDouble();
			}
			
			final double[] coefs = new double[degree + 1];
			for (int coef = 0; coef <= degree; coef++) {
				coefs[coef] = -mag + 2 * mag * rand.nextDouble();
			}

			PointValuePair result = null;

			ObjectiveFunction function = new ObjectiveFunction(
					new MultivariateFunction() {

						@Override
						public double value(double[] point) {
							double error = 0;
							for (double x : evaluationPoint)
								for (int coef = 0; coef <= degree; coef++) {
									double pow = Math.pow(x, degree - coef);
									error += (coefs[coef] - point[coef]) * pow;
								}
							error = Math.abs(error);
							return error;
						}
					});

			// try {
				if (constrained) {
					result = optimizer_test.optimize(GoalType.MINIMIZE,
							new InitialGuess(seed), function, parameterBounds,
							maxIter, maxEval);
				} else {
					AbstractSimplex simplex = new NelderMeadSimplex(degree + 1);
//					AbstractSimplex simplex = new MultiDirectionalSimplex(degree + 1);
					result = optimizer_test.optimize(GoalType.MINIMIZE,
							new InitialGuess(seed), function, simplex, maxIter, maxEval);
				}
//			} catch (TooManyEvaluationsException e) {
//				System.out.println("Did not converge with " + maxIterations
//						+ " iterations for " + Arrays.toString(coefs));
//			}

			if (result != null) {
				
				for (double x : evaluationPoint)
				error += Math.abs(MathToolset.polyevaluate(x, coefs)
						- MathToolset.polyevaluate(x,
								result.getPoint()));
				
				System.out.println("true = " + Arrays.toString(coefs));
				System.out.println("opti = " + Arrays.toString(result.getPoint()));
				System.out.println(MathToolset.polyevaluate(evaluationPoint[0], coefs));
				System.out.println(MathToolset.polyevaluate(evaluationPoint[0], result.getPoint()));
			}
		}

		error /= numRealizations;
		double elapsed = timer.tick();
		System.out.println(TextToolset.box("   Finished   ", '-'));
		System.out.println("Elapsed time for " + numRealizations + " realizations = "
				+ elapsed + "s -> " + (elapsed / numRealizations) + "s per node.");
		System.out.println("E    \t= " + error);
	}

	public static CartanOptimizer createInstance(OptimizerType optimizer, FittingData data) {
		switch (optimizer) {
		case BOBYQA:
			return new CartanBOBYQAOptimizer(data);
		case SIMPLEX:
			return new CartanSimplexOptimizer(data);
		case DIRECT:
			if (data.getFittingParameter().getFittingMethod() == FittingMethod.Direct)
				return new CartanDirectEstimator(data);
			else if (data.getFittingParameter().getFittingMethod() == FittingMethod.ClosedForm)
				return new CartanClosedEstimator(data);
			else
				return null;
			
		default:
			return CartanOptimizer.createInstance(OptimizerType.DIRECT, data);
		}
	}
}
