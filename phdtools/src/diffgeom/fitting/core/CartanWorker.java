package diffgeom.fitting.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import app.pami2013.cartan.CartanParameterNode;

public class CartanWorker implements Runnable {
	
	/**
	 * Working thread.
	 */
	private Thread workThread;

	private ActionListener completionListener;
	
	private int workerIndex;
	private List<CartanParameterNode> seeds;
	private CartanOptimizer optimizer;
	
	private List<CartanParameterNode> fittingResult  = new LinkedList<CartanParameterNode>();
	private boolean running = false;
	private boolean started = false;
	
	/**
	 * @param workerIndex a unique index for this worker
	 * @param frameField the frame field used for fitting
	 * @param seeds target locations to fit
	 * @param optimizer the optimizer to use
	 */
	public CartanWorker(int workerIndex, CartanOptimizer optimizer, List<CartanParameterNode> seeds) {
		this.workerIndex = workerIndex;
		this.seeds = seeds;
		this.optimizer = optimizer;

		running = false;
		started = false;
	}
	
	public boolean hasStarted() {
		return started;
	}
	
	public void start(ActionListener completionListener) {
		running = true;
		started = true;
		
		this.completionListener = completionListener;

		// Create a new working thread
		workThread = new Thread(this);
		workThread.start();
	}

	@Override
	public void run() {
		running = true;
		started = true;
		
		fittingResult.clear();
		
		for (CartanParameterNode seed : seeds) {
			CartanParameterNode cartanFit = optimizer.optimize(seed);
			fittingResult.add(cartanFit);
		}
		
		completionListener.actionPerformed(new ActionEvent(this, workerIndex, null));
	}

	public boolean isRunning() {
		return running;
	}

	public List<CartanParameterNode> getFittingResult() {
		
		return fittingResult;
	}

	public int getIndex() {
		return workerIndex;
	}
	
	public void terminate() {
		running = false;
	}
}
