package diffgeom.fitting.method;

import javax.swing.JPanel;

import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.AbstractSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;

import diffgeom.fitting.core.CartanOptimizer;
import diffgeom.fitting.core.FrameFittingError.EnergyTarget;
import diffgeom.fitting.experiment.FittingData;
import app.pami2013.cartan.CartanParameterNode;

public class CartanSimplexOptimizer extends CartanOptimizer implements
		ConvergenceChecker<PointValuePair> {
	private MultivariateOptimizer optimizer;
	private AbstractSimplex simplex;
	private AbstractSimplex simplexC1;

	private boolean splitOptimization = false;
	
	private MaxIter maxIter;
	private MaxEval maxEval;
	
	/**
	 * Number of connection forms associated with each frame axis
	 */
	private int countC1jk = 0;

	/**
	 * Arrays that hold optimization seed points.
	 */
	private double[] seedArrayC1;


	public CartanSimplexOptimizer(final FittingData fittingData) {
		super(fittingData);

		splitOptimization = false;
				
		maxEval = MaxEval.unlimited();
		maxIter = new MaxIter(Math.max(1, fittingData.getFittingParameter()
				.getNumIterations()));

//		int n = fittingData.getFittingParameter().getCartanModel()
//				.getNumberOfParameters();

		optimizer = new SimplexOptimizer(this);
//		simplex = new NelderMeadSimplex(n);
		// simplex = new MultiDirectionalSimplex(n);
		
		/*
		 *  Count number of available connections
		 */
		countC1jk = fittingData.getFittingParameter().getCartanModel().countC1();

		/*
		 *  Create arrays that will hold seed points
		 */
		seedArrayC1 = new double[countC1jk];
		
		/*
		 * Create simplex optimizers
		 */
		if (splitOptimization)  {
			simplexC1 = new NelderMeadSimplex(countC1jk);
		}
		simplex = new NelderMeadSimplex(fittingData.getFittingParameter().getCartanModel().getNumberOfParameters());
	}

	@Override
	public CartanParameterNode doOptimize(CartanParameterNode seed) {

		// Create the node that will contain the result of the optimization.
		CartanParameterNode resultNode = new CartanParameterNode(seed);
		resultNode.zero();

		try {
			fittingError.setEnergyTarget(EnergyTarget.ALL);
			PointValuePair result = optimizer.optimize(GoalType.MINIMIZE, new InitialGuess(seed.getParameterArray()), new ObjectiveFunction(fittingError), simplex, maxIter, maxEval);
			resultNode.setParametersMasked(result.getFirst());				
			
			if (splitOptimization) {
				// Optimize separately for connections that are in the set { c121, c122, c123, c131, c132, c133 }
				resultNode.fillC1(seedArrayC1);
				fittingError.setEnergyTarget(EnergyTarget.F1);
				PointValuePair resultC1pvp = optimizer.optimize(GoalType.MINIMIZE, new InitialGuess(seedArrayC1), new ObjectiveFunction(fittingError), simplexC1, maxIter, maxEval);
				double[] resultC1 = resultC1pvp.getFirst();

				// Fill in result
				resultNode.setC1(resultC1);
			}
			
			double[] error = fittingError.evaluate(resultNode, getFittingData()
					.getNeighborhood(seed.getPosition()));
			resultNode.setError(error);
		} catch (TooManyEvaluationsException e) {
			resultNode.setError(null);
			resultNode.setGarbage(true);
		} catch (NumberIsTooLargeException e) {
			resultNode.setError(null);
			resultNode.setGarbage(true);
		} catch (NumberIsTooSmallException e) {
			resultNode.setError(null);
			resultNode.setGarbage(true);
		}

		return resultNode;
	}

	@Override
	public boolean converged(int iteration, PointValuePair previous,
			PointValuePair current) {

//		if (previous == null)
//			return false;

		double cv = current.getValue();
		double pv = previous.getValue();

		// Stop if one of the following occurs:
		// 1) The error falls below a threshold
		// 2) The error gradient between two successive iterations falls below a
		// threshold (i.e. a local minima is reached)
		// 3) The maximum number of iterations was reached

		// System.out.println(cv + " for HP = " +
		// Arrays.toString(current.getPoint()));

		// FIXME: bug in commons api: currently need to bypass the
		// <code>iteration</code> parameter since the
		// SimplexOptimizer always sets it to "0".
		if (cv <= fittingData.getFittingParameter().getErrorThreshold()
				|| Math.abs(pv - cv) <= fittingData.getFittingParameter()
						.getErrorDeltaThreshold()
				|| optimizer.getIterations() >= fittingData
						.getFittingParameter().getNumIterations())
			return true;

		return false;
	}

	@Override
	public JPanel getControls() {
		// TODO Auto-generated method stub
		return null;
	}
}
