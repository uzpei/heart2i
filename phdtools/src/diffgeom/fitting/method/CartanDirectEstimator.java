package diffgeom.fitting.method;

import javax.swing.JPanel;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.fitting.core.CartanOptimizer;
import diffgeom.fitting.experiment.FittingData;
import math.matrix.Matrix3d;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame;
import tools.frame.OrientationFrame.FrameAxis;
import voxel.VoxelFrameField;
import voxel.VoxelVectorField;
import app.pami2013.cartan.CartanParameter;
import app.pami2013.cartan.CartanParameterNode;

public class CartanDirectEstimator extends CartanOptimizer {
	
	public CartanDirectEstimator(final FittingData fittingData) {
		super(fittingData);

	}
	
	@Override
	public CartanParameterNode doOptimize(CartanParameterNode seed) {
		CartanParameterNode resultNode = estimate(fittingData.getFrameField(), seed.getPosition());
		double[] error = fittingError.evaluate(resultNode, getFittingData().getNeighborhood(seed.getPosition()));
		resultNode.setError(error);
		
		return resultNode;
	}
	
	private Vector3d f1 = new Vector3d();
	private Vector3d f2 = new Vector3d();
	private Vector3d f3 = new Vector3d();
	
	public CartanParameterNode estimate(VoxelFrameField frameField, Point3i p) {
		CartanParameterNode parameter = new CartanParameterNode(p);
		
		frameField.getF1(p, f1);
		frameField.getF2(p, f2);
		frameField.getF3(p, f3);
		
		/*
		 * Compute all cijk = F_j^T * J_i * F_k
		 */

		// Precompute jacobians
		Vector3d[] j1 = jacobian(frameField.getF1(), p);
		Vector3d[] j2 = jacobian(frameField.getF2(), p);

		// Compute c_12k
		parameter.setc121(f2.dot(multiply(j1, f1)));
		parameter.setc122(f2.dot(multiply(j1, f2)));
		parameter.setc123(f2.dot(multiply(j1, f3)));

		// Compute c_13k
		parameter.setc131(f3.dot(multiply(j1, f1)));
		parameter.setc132(f3.dot(multiply(j1, f2)));
		parameter.setc133(f3.dot(multiply(j1, f3)));

		// Compute c_23k
		parameter.setc231(f3.dot(multiply(j2, f1)));
		parameter.setc232(f3.dot(multiply(j2, f2)));
		parameter.setc233(f3.dot(multiply(j2, f3)));

		return parameter;
	}
	
	/**
	 * Compute the jacobian of this vector field at a given position.
	 * Array indices represent rows
	 * @param p
	 * @param frameField
	 * @return
	 */
	public Vector3d[] jacobian(VoxelVectorField v, Point3i p) {

		Vector3d dvdx = gradient(v, p, OrientationFrame.FrameAxis.X.getAxisArray());
		Vector3d dvdy  = gradient(v, p, OrientationFrame.FrameAxis.Y.getAxisArray());
		Vector3d dvdz = gradient(v, p, OrientationFrame.FrameAxis.Z.getAxisArray());
		
		Vector3d j0 = new Vector3d(dvdx.x, dvdy.x, dvdz.x);
		Vector3d j1 = new Vector3d(dvdx.y, dvdy.y, dvdz.y);
		Vector3d j2 = new Vector3d(dvdx.z, dvdy.z, dvdz.z);
		
		return new Vector3d[] { j0, j1, j2 };
	}
	
	public Vector3d multiply(Vector3d[] jacobian, Vector3d v) {
		return new Vector3d(jacobian[0].dot(v), jacobian[1].dot(v), jacobian[2].dot(v));
	}

	/**
	 * Compute gradient using central differences
	 * OR forward/backward when not available
	 * @param v
	 * @param p
	 * @param direction
	 * @return
	 */
	public static Vector3d gradient(VoxelVectorField v, Point3i p, int[] direction) {
		// Compute forward and backward coordinates
		int fi = p.x + direction[0];
		int fj = p.y + direction[1];
		int fk = p.z + direction[2];
		
		int bi = p.x - direction[0];
		int bj = p.y - direction[1];
		int bk = p.z - direction[2];

		/*
		 *  Validate forward and backward coordinates.
		 *  Central values are used for invalid coordinates.
		 */
		boolean foundForward = true;
		boolean foundBackward = true;
		
		// Use any volume for testing bounds
		Matrix3d bounds = v.getX();
		
		double h = 0.5; // h = 1/2

		// Check whether we have access to forward neighbor
		if (!bounds.contains(fi, fj, fk) || v.getVoxelBox().getMask().isMasked(fi, fj, fk)) {
//		if (!bounds.contains(fi, fj, fk)) {
			fi = p.x;
			fj = p.y;
			fk = p.z;
			foundForward = false;
			h = 1;
		}
		
		// Check whether we have access to backward neighbor
		if (!bounds.contains(bi, bj, bk) || v.getVoxelBox().getMask().isMasked(bi, bj, bk)) {
//		if (!bounds.contains(bi, bj, bk)) {
			bi = p.x;
			bj = p.y;
			bk = p.z;
			foundBackward = false;
			h = 1;
		}
		
		// Neither forward nor backward were found, gradient will be zero
		if (!foundBackward && !foundForward) {
			return new Vector3d();
		}
		else {
			double vx = h * (v.getX().get(fi, fj, fk) - v.getX().get(bi, bj, bk));
			double vy = h * (v.getY().get(fi, fj, fk) - v.getY().get(bi, bj, bk));
			double vz = h * (v.getZ().get(fi, fj, fk) - v.getZ().get(bi, bj, bk));
			return new Vector3d(vx, vy, vz);
		}
		
	}
		
	
	@Override
	public JPanel getControls() {
		// TODO Auto-generated method stub
		return null;
	}
}
