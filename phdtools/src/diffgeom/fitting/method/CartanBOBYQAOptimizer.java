package diffgeom.fitting.method;

import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;

import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer;

import diffgeom.fitting.core.CartanOptimizer;
import diffgeom.fitting.experiment.FittingData;
import tools.geom.MathToolset;
import app.pami2013.cartan.CartanParameter.Parameter;
import app.pami2013.cartan.CartanParameterNode;

/**
 * FIXME: currently the optimizer will run for an unlimited number of evaluations. It is not possible to reduce the number of evaluations
 * at this point since the optimizer throws a TooManyEvaluationsException if it goes beyond any specified limit and it is not possible
 * to recover the best solution found so far. One fix would be to bypass the jar and modify the source code.
 * @author piuze
 *
 */
public class CartanBOBYQAOptimizer extends CartanOptimizer {
	
	private MultivariateOptimizer optimizer;
	private SimpleBounds parameterBounds;
	
	/**
	 * Constrained optimization will BOBYQA (Bound Optimization BY Quadratic Approximation) with generic cardiac bounds on connection forms.
	 */
	private MaxIter maxIter;
	private MaxEval maxEval;

	public CartanBOBYQAOptimizer(final FittingData fittingData) {
		super(fittingData);
		maxEval = MaxEval.unlimited();
//		maxEval = new MaxEval(fittingData.getFittingParameter().getNumIterations());
//		maxIter = new MaxIter(fittingData.getFittingParameter().getNumIterations());
		maxIter = MaxIter.unlimited();
		
		int n = fittingData.getFittingParameter().getCartanModel()
				.getNumberOfParameters();

		int min = n + 2;
		int max = MathToolset.roundInt((n + 2) * (n + 1) / 2d);
		int estimate = MathToolset.roundInt(2 * n + 1);
		int interpoints = Math.max(min,Math.min(estimate,max));
		
		System.out.println("Using " + interpoints
				+ " interpolation points in BOBYQA optimizer.");
		optimizer = new BOBYQAOptimizer(interpoints);

		// Construct lower and upper bounds on connection parameters
		// For unit-length frame fields, the value should be less than or equal
		// to one.
		HashMap<Parameter, double[]> bounds = fittingData.getFittingParameter().getBounds();
		
		List<Parameter> parameters = fittingData.getFittingParameter()
				.getCartanModel().getAvailableConnections();
		double[] lowerBounds = new double[parameters.size()];
		double[] upperBounds = new double[parameters.size()];
		for (int i = 0; i < parameters.size(); i++) {
			Parameter parameter = parameters.get(i);
			
			if (bounds != null && bounds.containsKey(parameter)) {
				lowerBounds[i] = bounds.get(parameter)[0];
				upperBounds[i] = bounds.get(parameter)[1];
			}
			else {
				lowerBounds[i] = -Double.MAX_VALUE;
				upperBounds[i] = Double.MAX_VALUE;
			}
		}
		
		parameterBounds = new SimpleBounds(lowerBounds, upperBounds);
	}
	
	@Override
	public CartanParameterNode doOptimize(CartanParameterNode seed) {

		// Create the node that will contain the result of the optimization.
		CartanParameterNode resultNode = new CartanParameterNode(seed);

		// Begin the optimization
		// Size of the problem to optimize
		try {
			PointValuePair result = optimizer.optimize(
			            GoalType.MINIMIZE,
			            new InitialGuess(seed.getParameterArray()),
			            new ObjectiveFunction(fittingError),
			            parameterBounds,
			            maxEval, maxIter);

//					RealPointValuePair pcp = dos.optimize(fittingError, GoalType.MINIMIZE, seed.getParameterArray());
//					// When this point is reached, the optimization is finished
//					resultNode.setParametersMasked(pcp.getPoint());
					resultNode.setParametersMasked(result.getFirst());
					double[] error = fittingError.evaluate(resultNode, getFittingData().getNeighborhood(seed.getPosition()));
//					// Use error in radians
//					resultNode.setError(Math.acos(1 - error));
					resultNode.setError(error);
		}
		catch (TooManyEvaluationsException e) {
			resultNode.setError(null);
			resultNode.setGarbage(true);
		}
		catch (NumberIsTooLargeException e) {
			resultNode.setError(null);
			resultNode.setGarbage(true);
		}
		catch (NumberIsTooSmallException e) {
			resultNode.setError(null);
			resultNode.setGarbage(true);
		}
			
		return resultNode;
	}

	@Override
	public JPanel getControls() {
		// TODO Auto-generated method stub
		return null;
	}

}
