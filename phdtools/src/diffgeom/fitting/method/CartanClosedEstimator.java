package diffgeom.fitting.method;

import javax.swing.JPanel;

import app.pami2013.cartan.CartanParameterNode;
import diffgeom.DifferentialOneForm;
import diffgeom.fitting.core.CartanOptimizer;
import diffgeom.fitting.experiment.FittingData;

public class CartanClosedEstimator extends CartanOptimizer {
	
	public CartanClosedEstimator(final FittingData fittingData) {
		super(fittingData);

	}
	
	@Override
	public CartanParameterNode doOptimize(CartanParameterNode seed) {
		
		CartanParameterNode resultNode = DifferentialOneForm.computeClosedFormParameter(fittingData.getFrameField(), seed.getPosition(), fittingData.getNeighborhood(seed.getPosition()));
		double[] error = fittingError.evaluate(resultNode, getFittingData().getNeighborhood(seed.getPosition()));
		resultNode.setError(error);
		
		return resultNode;
	}
	
	@Override
	public JPanel getControls() {
		// TODO Auto-generated method stub
		return null;
	}
}
