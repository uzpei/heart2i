package gl.texture;

import gl.geometry.primitive.Cube;
import gl.geometry.primitive.Plane;

import java.nio.ByteBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import tools.geom.MathToolset;
import volume.IntensityVolume;
import voxel.VoxelBox;

public class VolumeTexture {
	private IntensityVolume volume;
	private ByteBuffer buffer;
	private short[] data;
	private int[] textureId;

	public VolumeTexture(IntensityVolume volume) {
		this.volume = volume;
	}
	
	public void init(GL2 gl) {
		initTexture(gl);
		
		int[] dims = volume.getDimension();
		volume.normalize();
				
		buffer = IntensityVolume.getBuffer(volume);
		gl.glTexImage3D(GL2.GL_TEXTURE_3D, 0, GL.GL_RGBA, dims[0], dims[1], dims[2], 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, buffer.rewind());
	}
	
	private void initTexture(GL2 gl) {
		// Create the texture handle
//		gl.glActiveTexture(GL2.GL_TEXTURE0);
		textureId = new int[1];

		// FIXME: Why can' we use the GL2 object? This might be a bug in JOGL 2.0...
		gl.glGenTextures(1, textureId, 0);
//		drawable.getGL().glGenTextures(1, textureId, 0);
		
		gl.glBindTexture(GL2.GL_TEXTURE_3D, textureId[0]);
		System.out.println("Using generated texture ID " + textureId[0]);

		// Init parameters
//		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
//		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);

		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_R, GL2.GL_CLAMP);
	}

	public void display(GL2 gl) {
		gl.glEnable(GL2.GL_TEXTURE_3D);
		
		gl.glBegin(GL2.GL_QUADS);
		float dz = 1f;
		for (float z = 0f; z < 0.5f; z += 0.005f) {
			gl.glTexCoord3f(0f, 0f, z);
			gl.glVertex3f(0, 0, z * dz);
			gl.glTexCoord3f(0f, 1f, z);
			gl.glVertex3f(0, z, z * dz);
			gl.glTexCoord3f(1f, 1f, z);
			gl.glVertex3f(z, z, z * dz);
			gl.glTexCoord3f(1f, 0f, z);
			gl.glVertex3f(z, 0, z * dz);
		}
		gl.glEnd();
	}
	
	public void display(GL2 gl, float z) {
		gl.glEnable(GL2.GL_TEXTURE_3D);
		gl.glColor4d(1,1,1,1);

		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(0, 0, 1);
		gl.glTexCoord3f(0f, 0f, z);
		gl.glVertex3f(-1.0f, -1.0f, z);
		gl.glTexCoord3f(1.0f, 0.0f, z);
		gl.glVertex3f(1.0f, -1.0f, z);
		gl.glTexCoord3f(1.0f, 1.0f, z);
		gl.glVertex3f(1.0f, 1.0f, z);
		gl.glTexCoord3f(0.0f, 1.0f, z);
		gl.glVertex3f(-1.0f, 1.0f, z);
		gl.glEnd();
	}

	public void display(GL2 gl, Plane plane) {
	}

	public void display(GL2 gl, VoxelBox box) {
		// Get box limits and extract a 3D volume
		Vector3d[] bounds = box.getNormalizedBounds();
		Vector3d bo = bounds[0];
		Vector3d bs = bounds[1];
		int[] dims = box.getDimension();
		
//		gl.glColor4d(1,1,1,1);
//		WorldAxis.display(gl);
		
		gl.glEnable(GL2.GL_TEXTURE_3D);
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor4d(1,1,1,0.9);

		gl.glBindTexture(GL2.GL_TEXTURE_3D, textureId[0]);
		
		Point3i p0 = box.getOrigin();
		Point3i v = box.getSpan();
		double z0;
		double zv;
		Vector3d wtot = new Vector3d(1.0 / dims[0], 1.0 / dims[1], 1.0 / dims[2]);
		Cube.draw(gl, MathToolset.tuple3iToPoint3d(p0), MathToolset.tuple3iToVector3d(v), wtot);
		
//		Vector3d o = new Vector3d(1.0 / dims[0], 1.0 / dims[1], 1.0 / dims[2]);
//		o.scale(0);
//		
////		double s = 0.99;
////		gl.glScaled(s, s, s);
//		
//		double dof = -1;
//		for (int z = p0.z; z < p0.z + v.z; z++) {
//			z0 = (z+1) / ((double) dims[2]);
//			zv = z + 0.5;
//			
//			gl.glBegin(GL2.GL_QUADS);
//			gl.glNormal3d(0, 0, 1);
//			gl.glTexCoord3d(bo.x + o.x, bo.y + o.y, z0);
//			gl.glVertex3d(0f, 0f, zv);
//			
//			gl.glTexCoord3d(bo.x + bs.x + o.x, bo.y + o.y, z0);
//			gl.glVertex3d(dims[0]+dof, 0.0f, zv);
//			
//			gl.glTexCoord3d(bo.x + bs.x + o.x, bo.y + bs.y + o.y, z0);
//			gl.glVertex3d(dims[0]+dof, dims[1]+dof, zv);
//			
//			gl.glTexCoord3d(bo.x + o.x, bo.y + bs.y + o.y, z0);
//			gl.glVertex3d(0.0f, dims[1]+dof, zv);
//			
//			gl.glEnd();
//		}

		gl.glDisable(GL2.GL_TEXTURE_3D);

	}

}
