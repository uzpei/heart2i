package gl.texture;

import gl.material.GLMaterial;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

public class GLTexture {
	private String filename;
	private Texture texture;
	private TextureData data;
//	private int id;
	private GLMaterial material = null;
	
	public void setMaterial(GLMaterial material) {
		this.material = material;
	}

	public GLTexture(String filename) {
		this.filename = filename;
		
		loadFromFile(filename);
	}

	public GLTexture(GL2 gl, TextureData textureData) {
		this.data = textureData;
		this.texture = new Texture(gl, data);
	}

//	public GLTexture(String filename, int textureId) {
//		this.filename = filename;
//		this.id = textureId;
//		
//		loadFromFile(filename);
//	}
//	
//	public GLTexture(Texture texture, TextureData textureData, int textureId) {
//		this.texture = texture;
//		this.data = textureData;
//		this.id = textureId;
//	}
//
//	public GLTexture(TextureData data, int textureId) {
//		this.data = data;
//		this.texture = TextureIO.newTexture(data);
//		this.id = textureId;
//	}

//	public GLTexture(GL gl, String filename) {
//		int[] textId = new int[1];
//		gl.glGenTextures(1, textId, 0);
//		id = textId[0];
//		loadFromFile(filename);
//	}

	private void loadFromFile(String filename) {
		try {
			File tfile = new File(filename);
			System.out.println("Texture exists at " + tfile.getCanonicalPath() +  " ? " + (tfile.exists() ? "YES" : "NO"));
			
			texture = TextureIO.newTexture(tfile, false);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void apply(GL2 gl) {
		if (material != null) {
			material.apply(gl);
		}
		
		bind(gl);
	}
	
	public Texture getTexture() {
		return texture;
	}
	
//	public int getTextureID() {
//		return id;
//	}

	public Dimension getDimension() {
		return new Dimension(data.getWidth(), data.getHeight());
	}
	
	public TextureData getData() {
		return data;
	}
	
	public int getPixelFormat() {
		return data.getPixelFormat();
	}
	
	public int getPixelType() {
		return data.getPixelType();
	}

	public void enable(GL2 gl) {
		texture.enable(gl);
	}

	public void bind(GL2 gl) {
		// Equivalent of:
		// gl.glBindTexture(GL.GL_TEXTURE_2D, id);
		texture.bind(gl);
	}
}

