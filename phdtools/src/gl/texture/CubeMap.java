package gl.texture;

import gl.geometry.primitive.Cube;
import gl.geometry.primitive.Quad;
import gl.renderer.TrackBallCamera;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLProfile;
import javax.vecmath.Point2d;

import com.jogamp.opengl.util.awt.ImageUtil;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

public class CubeMap {
	
	private String filename;
	private GLTexture[] cubeMap;
	private BufferedImage[] sides; 
	private int[] cubemapId;
	private Dimension dimension;
	
	private final static int[] GL_CUBEMAP_TARGETS = new int[] { 
			GL2.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 
			GL2.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 
			GL2.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 
			GL2.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 
			GL2.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 
			GL2.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 
			}; 


//	private BufferedImage image;
//	private TextureData data;
//	private Texture texture;

	/**
	 * Create a cubemap from a single (cross) image.
	 * @param filename
	 */
	public CubeMap(String filename) {
		this(filename, true, "");
	}
	/**
	 * @param filename
	 * @param cross whether this filename points to a single image (cross) or 6 cube faces with the format "filename_px, filename_nx, ..., filename_nz" 
	 * @param format the file format (e.g. ".png") only in the case of a non-cross cube map. Leave null for a cross image. 
	 */
	public CubeMap(String filename, boolean cross, String format) {
		this.filename = filename;
		
		sides = new BufferedImage[6];
		cubeMap = new GLTexture[6];

		try {
	        if (cross) {
				File tfile = new File(filename);
				System.out.println("Cross cubemap found at " + tfile.getCanonicalPath() +  " ? " + (tfile.exists() ? "YES" : "NO"));
				
				BufferedImage image = ImageIO.read(tfile);
				dimension = new Dimension(image.getWidth(), image.getHeight());
	        	extractCubeMap(image);
	        }
	        else {
	        	
				File tfile = new File(filename);
				System.out.println("Non cross cubemap images found at " + tfile.getCanonicalPath() +  " ? " + (tfile.exists() ? "YES" : "NO"));

	        	int i = 0;
//	        	System.out.println(filename + "px" + format);
	        	sides[i++] = ImageIO.read(new File(filename + "px" + format));
	        	sides[i++] = ImageIO.read(new File(filename + "nx" + format));
	        	sides[i++] = ImageIO.read(new File(filename + "py" + format));
	        	sides[i++] = ImageIO.read(new File(filename + "ny" + format));
	        	sides[i++] = ImageIO.read(new File(filename + "pz" + format));
	        	sides[i++] = ImageIO.read(new File(filename + "nz" + format));
	        	
	        	// Flip all images
	        	for (BufferedImage bi : sides) {
	    			ImageUtil.flipImageVertically(bi);
	        	}
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Extract the six faces from the cube map
	 */
	private void extractCubeMap(BufferedImage image) {
		
		// TODO: determine horizontal or vertical-cross format
		// for now, assume horizontal
		// i.e. image has height < width
		// rows = y, columns = x
		//  0 +Y  0  0
		// -X +Z +X -Z
		//  0 -Y  0  0
		
		// Cubemap is arranged as a 3 x 4 tiled image
		System.out.println(getDimension());
		int size = getDimension().height / 3;		
		Rectangle[] tiles = new Rectangle[6];

		// Positive x
		tiles[0] = new Rectangle(2*size, 1*size, size, size);
		
		// Negative x
		tiles[1] = new Rectangle(0*size, 1*size, size, size);

		// Positive y
		tiles[2] = new Rectangle(1*size, 2*size, size, size);

		// Negative y
		tiles[3] = new Rectangle(1*size, 0*size, size, size);

		// Positive z
		tiles[4] = new Rectangle(1*size, 1*size, size, size);
						
		// Negative z
		tiles[5] = new Rectangle(3*size, 1*size, size, size);

		// TODO: speed this up
		for (int i = 0; i < 6; i++) {
//			System.out.println("Loading side " + i);
			// Extract each side
			sides[i] = image.getSubimage(tiles[i].x, tiles[i].y, tiles[i].width, tiles[i].height);
//			ImageUtil.flipImageVertically(sides[i]);
		}
		
	}
	
	private Dimension getDimension() {
		return dimension;
	}
	
	/**
	 *  Can also use GL_REPEAT, GL_CLAMP_TO_EDGE
	 */
	private int cubemapRepeatMode = GL2.GL_CLAMP;
	
	/**
	 * Can also use GL_NEAREST, GL_LINEAR
	 */
	private int cubemapMappingMode = GL2.GL_LINEAR;
	
	/**
	 * Can also use, GL_NORMAL_MAP, GL_REFLECTION_MAP, GL_SPHERE_MAP
	 */
	private int cubemapGenmode = GL2.GL_NORMAL_MAP;

	public void setEnabled(GL2 gl, boolean cubemapEnabled) {

		int mappingMode;
		int repeatMode;
		int genmode;

		if (cubemapEnabled) {
			mappingMode = cubemapMappingMode;
			repeatMode = cubemapRepeatMode;
			genmode = cubemapGenmode;
		} else {
			// mappingMode = GL2.GL_LINEAR;
			mappingMode = GL2.GL_NEAREST;
			repeatMode = GL2.GL_CLAMP;
			/*
			 * GL_OBJECT_LINEAR, GL_EYE_LINEAR, GL_SPHERE_MAP, GL_NORMAL_MAP, or
			 * GL_REFLECTION_MAP
			 */
			genmode = GL2.GL_SPHERE_MAP;
		}

		for (int i = 0; i < 6; i++) {
			cubeMap[i].getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_WRAP_S, repeatMode);
			cubeMap[i].getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_WRAP_T, repeatMode);
			cubeMap[i].getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_MAG_FILTER, mappingMode);
			cubeMap[i].getTexture().setTexParameterf(gl, GL2.GL_TEXTURE_MIN_FILTER, mappingMode);
		}

		// Set texture repeat mode
		gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_WRAP_S, repeatMode);
		gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_WRAP_T, repeatMode);
		gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_WRAP_R, repeatMode);

		// Set mapping mode
		gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_MAG_FILTER, mappingMode);
		gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_MIN_FILTER, mappingMode);

		// Texture generation mode
		gl.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, genmode);
	    gl.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, genmode);
	    gl.glTexGeni(GL2.GL_R, GL2.GL_TEXTURE_GEN_MODE, genmode);

		if (cubemapEnabled) {
			gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
			gl.glBindTexture(GL2.GL_TEXTURE_CUBE_MAP, cubemapId[0]);
			gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
		    
		    gl.glDisable(GL2.GL_TEXTURE_GEN_S);
		    gl.glDisable(GL2.GL_TEXTURE_GEN_T);
		    gl.glDisable(GL2.GL_TEXTURE_GEN_R);
		    
		}
		else {
			gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);
//			gl.glBindTexture(GL2.GL_TEXTURE_CUBE_MAP, 0);

			//gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
//		    gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);

		    // Leave this off otherwise 2D textures won't work
		    gl.glEnable(GL2.GL_TEXTURE_GEN_S);
		    gl.glEnable(GL2.GL_TEXTURE_GEN_T);
		    gl.glEnable(GL2.GL_TEXTURE_GEN_R);
		}

	}
	
	public void init(GL2 gl) {
		
		cubemapId = new int[1];
		gl.glGenTextures(1, cubemapId, 0);
		gl.glBindTexture(GL2.GL_TEXTURE_CUBE_MAP, cubemapId[0]);
//		gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 1);
		
		// 0 or 1
		int border = 1;

		// Define 6 faces. +X,Y,Z-X,Y,Z
        
		// The internal cubemap texture OpenGL order is: +X, -X, +Y, -Y, +Z, -Z
		for (int i = 0; i < 6; i++) {
			cubeMap[i] = new GLTexture(gl, AWTTextureIO.newTextureData(GLProfile.getDefault(), sides[i], false));
	        
//			cubeMap[i].getTexture().setTexParameterf(GL2.GL_TEXTURE_WRAP_S, repeatMode);
//			cubeMap[i].getTexture().setTexParameterf(GL2.GL_TEXTURE_WRAP_T, repeatMode);
//			cubeMap[i].getTexture().setTexParameterf(GL2.GL_TEXTURE_MAG_FILTER, mappingMode);
//			cubeMap[i].getTexture().setTexParameterf(GL2.GL_TEXTURE_MIN_FILTER, mappingMode);
			cubeMap[i].enable(gl);

			// Connect data
			cubeMap[i].bind(gl);
			int pixelFormat = cubeMap[i].getData().getPixelFormat();
			int pixelType = cubeMap[i].getData().getPixelType();
			int internalFormat = GL2.GL_RGBA;
			int width = cubeMap[i].getDimension().width;
			int height = cubeMap[i].getDimension().height;

			Buffer buffer = cubeMap[i].getData().getBuffer();
			buffer.rewind();
			
			gl.glTexImage2D(GL_CUBEMAP_TARGETS[i], 0, internalFormat, width, height, border, pixelFormat, pixelType, buffer);
		}
	}
		
	public GLTexture[] getTextures() {
		return cubeMap;
	}

	public void drawSkybox(GLAutoDrawable drawable) {
		drawSkybox(drawable, null);
	}
	
	/*
	 * For best performance, it is recommended to take advantage of early-z. So,
	 * you should render everything in your scene first, then render the
	 * cubemap. You should leave z-testing on and the depth test should be
	 * GL_LEQUAL (less or equal). Never change the depth test while rendering a
	 * scene. In order for the skybox to appear far away, you need to scale it
	 * sufficiently but not too much. If you scale by too much, the zfar clip
	 * plane will cut it. If not scaled enough, some elements of your scene will
	 * be out of the skybox.
	 * 
	 * What about transparent objects in your scene? You should render
	 * non-transparent stuff first, then render the skybox, then render
	 * transparent stuff. So you will miss out on the advantage of early-z
	 * because of transparent objects but this shouldn't be a problem
	 */
	public void drawSkybox(GLAutoDrawable drawable, TrackBallCamera camera) {
		GL2 gl = drawable.getGL().getGL2();

		setEnabled(gl, true);

		gl.glMatrixMode(GL2.GL_TEXTURE);
		gl.glPushMatrix();
//		gl.glRotated(180, 0, 0, 1);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
//        gl.glOrtho(-1, 1, -1, 1, -1, 1);
//        gl.glOrtho(-1, 1, -1, 1, -200, 200);
        
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
//		gl.glEnable(GL2.GL_CULL_FACE);
//		gl.glEnable(GL2.GL_DEPTH_TEST);

		if (camera != null) {
//			camera.applyRotationTransformation(drawable);
		}
		
		gl.glRotated(-90, 0, 0, 1);

		// We loaded an identity projection matrix so
		// the far is 2
		new Cube(2).draw(drawable);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glPopMatrix();
		
		gl.glMatrixMode(GL2.GL_TEXTURE);
		gl.glPopMatrix();

		setEnabled(gl, false);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

	public void drawCross(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
//		gl.glDisable(GL2.GL_CULL_FACE);
		
		setEnabled(gl, false);
		
		Quad[] squares = new Quad[6];
		double s = 0.5;
		int j = 0;
		
		// +X, -X
		squares[j++] = new Quad(new Point2d(2*s, 0), s);
		squares[j++] = new Quad(new Point2d(0, 0), s);

		// +Y, -Y
		squares[j++] = new Quad(new Point2d(s, -s), s);
		squares[j++] = new Quad(new Point2d(s, s), s);
		
		// +Z, -Z
		squares[j++] = new Quad(new Point2d(s, 0), s);
		squares[j++] = new Quad(new Point2d(3*s, 0), s);

	    gl.glDisable(GL2.GL_TEXTURE_GEN_S);
	    gl.glDisable(GL2.GL_TEXTURE_GEN_T);
	    gl.glDisable(GL2.GL_TEXTURE_GEN_R);

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glColor4d(1, 1, 1, 1);
		for (int i = 0; i < 6; i++) {
			cubeMap[i].bind(gl);
			squares[i].display(drawable);
		}

//		gl.glEnable(GL2.GL_CULL_FACE);
	}
	public void prepareForDrawing(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		setEnabled(gl, true);
	}
	public void finishDrawing(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		setEnabled(gl, false);
		gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glDisable(GL2.GL_LIGHTING);
		
	    gl.glDisable(GL2.GL_TEXTURE_GEN_S);
	    gl.glDisable(GL2.GL_TEXTURE_GEN_T);
	    gl.glDisable(GL2.GL_TEXTURE_GEN_R);

	}
}
