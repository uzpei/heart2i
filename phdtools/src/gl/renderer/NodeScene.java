package gl.renderer;

import gl.geometry.GLGeometry;
import gl.geometry.GLObject;

import java.awt.event.KeyAdapter;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JComponent;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;

public class NodeScene extends JoglScene {
//	private final static double elevation = 290;
//	private final static double azimuth = 270;
	private final static double elevation = 0;
	private final static double azimuth = 0;
	private List<GLGeometry> nodes = new LinkedList<GLGeometry>();
	private List<GLObject> nodeObjects = new LinkedList<GLObject>();
	private boolean running = false;
	private boolean orthoRequest = false;
	private boolean uiUpdateRequested = true;
    
	private List<GLGeometry> nodesToInitialize = new LinkedList<GLGeometry>();
	
    public NodeScene() {
    	this(true);
    }

    public NodeScene(boolean start) {
    	this(start, 0);
    }

    public NodeScene(boolean start, boolean maximized) {
    	this(start, 0, maximized, true);
    }

    public NodeScene(boolean start, boolean maximized, boolean showGLConfigDisplay) {
    	this(start, 0, maximized, showGLConfigDisplay);
    }

    public NodeScene(float zoom) {
    	this(true, zoom);
    }

    public NodeScene(boolean start, float zoom) {
    	this(start, zoom, false, true);
    }
    
    public NodeScene(boolean start, float zoom, boolean maximized, boolean showGLConfigDisplay) {
		super("OpenGL Scene");
		
		super.renderer.getCamera().zoom(zoom);
		super.renderer.getConfig().setShowDisplay(showGLConfigDisplay);
		if (start) {
			running = true;
			super.render(elevation, azimuth);
		}
		
//		if (maximized)
//			super.renderer.updateSize(JoglRenderer.SizePreset.Large);
//		else
//			super.renderer.updateSize(JoglRenderer.SizePreset.Medium);
	}
    
    public void start() {
		super.render(elevation, azimuth);
		running = true;
    }

	public void addNode(GLGeometry geometry) {
		displayLock.lock();
		try {
			nodes.add(geometry);
			if (!running)
				nodesToInitialize.add(geometry);
		} finally {
			displayLock.unlock();
		}
	}

	public void addControlNode(GLObject object) {
		nodeObjects.add(object);
		
		addControl(object.getControls());
	}
	
	public void clearNodes() {
		nodes.clear();
	}

	private Lock displayLock = new ReentrantLock();
	
	@Override
	public void display(GLAutoDrawable drawable) {
		displayLock.lock();
		
		if (uiUpdateRequested) {
			vfp.update();
			uiUpdateRequested = false;
		}
		
//		if (orthoRequest) {
//			JoglRenderer.setOrtho(drawable);
//			orthoRequest = false;
//		}
		// initialize geometry if necessary
		if (nodesToInitialize.size() > 0) {
			for (GLGeometry geometry : nodesToInitialize)
				geometry.init(drawable);
			nodesToInitialize.clear();
		}
		
		try {
			GL2 gl = drawable.getGL().getGL2();
//			gl.glScaled(0.1, 0.1, 0.1);
			for (GLGeometry node : nodes) {
				node.display(drawable);
			}
		} finally {
			
			displayLock.unlock();
		}
	}

	private VerticalFlowPanel vfp = null;
	
	public JoglRenderer getRenderer() {
		return super.renderer;
	}
	
	@Override
	public JPanel getControls() {
		if (vfp == null)
			vfp = new VerticalFlowPanel();
		
		for (GLObject object : nodeObjects) {
			vfp.add(object.getControls());
		}
		
		return vfp.getPanel();
	}
	
	public static void main(String[] args) {
		new NodeScene();
	}

	public void addControl(JComponent control) {
		vfp.add(control);
		vfp.update();
		
		uiUpdateRequested = true;
//		vfp.getPanel().invalidate();
//		vfp.getPanel().revalidate();
	}

	public void addKeyListener(KeyAdapter adapter) {
		component.addKeyListener(adapter);
	}

	public void setOrtho() {
		orthoRequest = true;
	}
}
