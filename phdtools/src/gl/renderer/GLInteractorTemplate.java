package gl.renderer;

import gl.geometry.GLObject;
import gl.geometry.WorldAxis;

import java.awt.Component;
import java.awt.Dimension;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;

import com.jogamp.opengl.util.gl2.GLUT;

public class GLInteractorTemplate implements GLObject, Interactor {

    public static void main(String[] args) {
        new GLInteractorTemplate(new GLViewerConfiguration());
    }

    /**
     * Display the world axis.
     */
    private BooleanParameter displayWorld = new BooleanParameter(
            "display world", true);


    private JoglRenderer ev;
    
    public GLInteractorTemplate(GLViewerConfiguration config) {
        
        Dimension winsize = new Dimension(800, 800);
        ev = new JoglRenderer("Template");
        ev.getCamera().zoom(300);
        ev.addInteractor(this);
        ev.start(this);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor(1, 1, 1, 1);

        if (displayWorld.getValue())
            WorldAxis.display(gl);
        
        
        JoglRenderer.beginOverlay(drawable);
        JoglTextRenderer.printTextLines(drawable, "Sample Text", 10, 20, new float[] {0, 0, 0},
                GLUT.BITMAP_8_BY_13);
        JoglRenderer.endOverlay(drawable);
    }


    @Override
    public JPanel getControls() {

        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add(displayWorld.getControls());

        return vfp.getPanel();
    }


    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        
    }

	@Override
	public void attach(Component component) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
		new GLInteractorTemplate(config);
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

}
