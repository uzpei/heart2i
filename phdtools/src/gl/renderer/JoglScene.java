package gl.renderer;

import gl.geometry.FancyAxis;
import gl.geometry.GLObject;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLProfile;
import javax.swing.JPanel;

import swing.event.Interactor;
import tools.SimpleTimer;


public abstract class JoglScene implements GLObject, Interactor {

    protected JoglRenderer renderer;
    
    protected String name = "JOGL scene";

    private final static SimpleTimer timer = new SimpleTimer();
    
    public static void main(String[] args) {
    	timer.tick();
    	
    	JoglScene scene = new JoglScene("Test Run") {
			
			@Override
			public JPanel getControls() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void display(GLAutoDrawable drawable) {
				if (timer.isRunning()) {
					timer.stop();
					System.out.println("Total time to first frame = " + timer.observeTick_ms() + "ms");
				}
				
				new FancyAxis().draw(drawable.getGL().getGL2());
			}
		};
		scene.renderer.getRoom().setVisible(false);
//		scene.renderSnapshot();
		scene.render();
    }
    
    protected JoglScene(String name) {
        this.name = name;
        renderer = new JoglRenderer(name);
        renderer.addInteractor(this);
        
    }
    
    public void render() { 	
        renderer.start(this);
    }

    /**
     * Render with this initial orientation
     * @param elevation
     * @param azimuth
     */
    public void render(double elevation, double azimuth) {
        renderer.getCamera().setRotation(elevation, azimuth);
        renderer.start(this);
    }
    
    public void renderSnapshot() {
        renderer.startSnapshotAndExit(this);
    }

	@Override
	public abstract void display(GLAutoDrawable drawable);

    @Override
	public void init(GLAutoDrawable drawable) {
    	GL2 gl = drawable.getGL().getGL2();
    	
    	// Set some default properties to have everything look nice.
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2.GL_LINE_SMOOTH);
		gl.glEnable(GL2.GL_POINT_SMOOTH);
    }

    @Override
    public abstract JPanel getControls();

    @Override
    public String getName() {
    	return name;
    }
    
    protected Component component;

	@Override
	public void attach(Component component) {
		this.component = component;
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// Handle events
			}
		});
	}
	
	@Override
	public void reload(GLViewerConfiguration config) {
	}

	@Override
	public void detach(Component component) {
		
	}

	public void addInteractor(Interactor interactor) {
		renderer.addInteractor(interactor);
	}
}
