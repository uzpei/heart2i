package gl.renderer;


import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import swing.component.ColorChooser;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.FloatParameter;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.SimpleTimer;

public class GLViewerConfiguration {

	private static final String RELOAD_REQUEST_MESSAGE = "[!] reload required [!]";

	private static final boolean DEFAULT_USE_HIGH_RESOLUTION = true;
	
	private static final int SAMPLES_HIGHRES = 8;
	
	private static final int SAMPLES_LOWRES = 0;
	
	private JoglTextRenderer textRenderer = new JoglTextRenderer();

	private boolean showDisplay = true;
	
	
	/**
	 * Default multisampling = 0x
	 */
	private final static int DEFAULT_FSAA = 0;
	
	private IntDiscreteParameter numsamples;

	/**
	 * Default number of accumulation bits per channel.
	 */
	private final static int DEFAULT_ACCUM_BITS = 16;

		private ColorChooser backgroundColorChooser = new ColorChooser("clearColor");

	/**
	 * glBlendFunc
	 * 
	 * @author epiuze
	 */
	public enum GLBlendingFunctionFactor {
		ZERO(GL.GL_ZERO), ONE(GL.GL_ONE), DST_COLOR(GL.GL_DST_COLOR), ONE_MINUS_DST_COLOR(
				GL.GL_ONE_MINUS_DST_COLOR), SRC_ALPHA(GL.GL_SRC_ALPHA), ONE_MINUS_SRC_ALPHA(
				GL.GL_ONE_MINUS_SRC_ALPHA), DST_ALPHA(GL.GL_ONE_MINUS_SRC_ALPHA), ONE_MINUS_DST_ALPHA(
				GL.GL_ONE_MINUS_DST_ALPHA), SRC_ALPHA_SATURATE(
				GL.GL_SRC_ALPHA_SATURATE);

		private int code;

		private GLBlendingFunctionFactor(int c) {
			code = c;
		}

		/**
		 * @return the GL constant associated with this enum.
		 */
		public int GL() {
			return code;
		}
	};

	/**
	 * glHint
	 * 
	 * @author epiuze
	 */
	public enum GLHint {
		NICEST(GL.GL_NICEST), FASTEST(GL.GL_FASTEST), DONT_CARE(GL.GL_DONT_CARE);

		private int code;

		private GLHint(int c) {
			code = c;
		}

		/**
		 * @return the GL constant associated with this enum.
		 */
		public int GL() {
			return code;
		}
	};

	/**
	 * glShadeModel
	 * 
	 * @author epiuze
	 */
	public enum GLShade {
		FLAT(GL2.GL_FLAT), SMOOTH(GL2.GL_SMOOTH);

		private int code;

		private GLShade(int c) {
			code = c;
		}

		/**
		 * @return the GL constant associated with this enum.
		 */
		public int GL() {
			return code;
		}
	};

	private final static GLBlendingFunctionFactor DEFAULT_SFACTOR = GLBlendingFunctionFactor.SRC_ALPHA;

	private final static GLBlendingFunctionFactor DEFAULT_DFACTOR = GLBlendingFunctionFactor.ONE_MINUS_SRC_ALPHA;

	private final static GLShade DEFAULT_SHADE = GLShade.SMOOTH;

	private static final float clearDepth = 1.0f;
	
	private boolean glNormalize = true;
	
	private final float lineValues[] = new float[3];

	
	private BooleanParameter glBlend = new BooleanParameter("GL blend", true);

	private EnumComboBox<GLBlendingFunctionFactor> ecbBlendSrc = new EnumComboBox<GLBlendingFunctionFactor>(
			"source", DEFAULT_SFACTOR);

	private EnumComboBox<GLBlendingFunctionFactor> ecbdBlendDest = new EnumComboBox<GLBlendingFunctionFactor>(
			"destination", DEFAULT_DFACTOR);

	private EnumComboBox<GLShade> ecbShade = new EnumComboBox<GLShade>(
			"smoothing", DEFAULT_SHADE);

	private static final Color DEFAULT_CLEAR_COLOR = Color.black;

	private BooleanParameter glDepthTest = new BooleanParameter(
			"GL depth test", true);

	private BooleanParameter glCullFace = new BooleanParameter("GL cull face",
			true);

	private BooleanParameter glLineSmooth = new BooleanParameter(
			"line", true);

	private BooleanParameter glPointSmooth = new BooleanParameter(
			"point", true);

	private BooleanParameter glPolygonSmooth = new BooleanParameter(
			"polygon", true);

	private FloatParameter textureAnisotropy = new FloatParameter("Anisotropic filtering", 1, 1, 16);
	
	private GLCapabilities capabilities;

	private boolean hasChanged = false;

	private boolean needsReload = false;

	public boolean hasChanged() {
		return hasChanged;
	}

	public boolean needsReload() {
		return needsReload;
	}

	public GLViewerConfiguration() {
		createDefaultGLCapabilities(DEFAULT_USE_HIGH_RESOLUTION);
	}

	public GLViewerConfiguration(boolean highResolution) {
		createDefaultGLCapabilities(highResolution);
	}

	public void setGLCapabilities(GLCapabilities capabilities) {
		this.capabilities = capabilities;
	}
	
	public static boolean verbose = true;

	private void createDefaultGLCapabilities(boolean highResolution) {

		SimpleTimer timer = new SimpleTimer();
		
		// Multisampling
        GLProfile glp = GLProfile.getDefault();
//		GLProfile.initSingleton();
//		GLProfile glp = GLProfile.get(GLProfile.GL2);

		capabilities = new GLCapabilities(glp);

		if (verbose)
			System.out.println("GLProfile creation time = " + timer.tick_ms());

//		capabilities.setSampleBuffers(DEFAULT_MULTISAMPLES > 0 ? true : false);
//		capabilities.setNumSamples(DEFAULT_MULTISAMPLES);
		
		capabilities.setSampleBuffers(highResolution);
		if (highResolution)
			capabilities.setNumSamples(SAMPLES_HIGHRES);
		else
			capabilities.setNumSamples(SAMPLES_LOWRES);

		// Accumulation bits per channel
//		capabilities.setAccumBlueBits(DEFAULT_ACCUM_BITS);
//		capabilities.setAccumGreenBits(DEFAULT_ACCUM_BITS);
//		capabilities.setAccumRedBits(DEFAULT_ACCUM_BITS);
		
		capabilities.setHardwareAccelerated(true);
		capabilities.setDoubleBuffered(true);
		
		// capabilities.setStereo(false);

		// capabilities.setAlphaBits(alphaBits());
		// capabilities.setDepthBits(depthBits());
		// capabilities.setStencilBits(stencilBits());

		numsamples = new IntDiscreteParameter("FSAA x", DEFAULT_FSAA, new int[] { 0, 2, 4, 8,
				16, 32 });
		
//		if (verbose)
//			System.out.println("Viewer configuration creation time = " + timer.tick_ms());
	}

	public GLCapabilities getGLCapabilities() {
		return capabilities;
	}

	public void displayDetails(GLAutoDrawable drawable) {
		if (displayTimeout) {
			displayTimeout = false;
			textRenderer.setFadeout(3000, 4000);
		}
		
		if (textRenderer.isFadingOut() && showDisplay) {
			textRenderer.clearBuffer();
			textRenderer.addLine("ClearColor = " + "(" + backgroundColorChooser.getColor().getRed() + ", " + backgroundColorChooser.getColor().getGreen() + ", " + backgroundColorChooser.getColor().getBlue() + ", " + backgroundColorChooser.getColor().getAlpha() + ")");
			textRenderer.addLine("Depth test = " + glDepthTest.getValue());
			textRenderer.addLine("Depth function = " + "GL_LEQUAL");
			textRenderer.addLine("Clear depth = " + clearDepth);
			textRenderer.addLine("GL_NORMALIZE = " + glNormalize);
			textRenderer.addLine("Cull face = " + glCullFace.getValue());
			textRenderer.addLine("Shade model = " + ecbShade.getSelected().toString());
			textRenderer.addLine("Blending = " + glBlend.getValue());
			textRenderer.addLine("Blend source = " + ecbBlendSrc.getSelected().toString());
			textRenderer.addLine("Blend destination = " + ecbdBlendDest.getSelected().toString());
			textRenderer.addLine("Point smoothing = " + glPointSmooth.getValue());
			textRenderer.addLine("Line smoothing = " + glLineSmooth.getValue());
			textRenderer.addLine("Line granularity = " + lineValues[0]);
			textRenderer.addLine("Line width range = " + "[" + lineValues[1] + ", " + lineValues[2] + "]");
			textRenderer.addLine("Polygon smoothing = " + glPolygonSmooth.getValue());
			textRenderer.addLine("FSAA multisampling = x" + numsamples.getValue());
			textRenderer.addLine("FBO super sampling = x0");
			textRenderer.addLine("Anisotropic filtering = x" + textureAnisotropy.getValue());
			textRenderer.formatFieldParsing("=");
			textRenderer.drawBufferWithFieldParsing(drawable, 10, 10, true, "=");
			
		}
		
	}

	private boolean displayTimeout = false;
	
	public void init(GLAutoDrawable drawable) {
		// drawable.setGL(new DebugGL(drawable.getGL()));
		GL2 gl = drawable.getGL().getGL2();
		
		// List of monospaced fonts:
		/*
		 * Andale Mono, Consolas, Courier, DejaVu Sans Mono, Droid Sans Mono,
		 * Fixedsys, HyperFont, Inconsolata, Letter Gothic, Lucida Console,
		 * Monaco, monofur, Prestige, Profont.
		 */
		
		// Available:
		/*
		 * Andale Mono, Courier, Letter Gothic Std, Lucida Console, Monaco, Monospaced, Prestige Elite Std 
		 */
		Font font = new Font("Andale Mono", Font.BOLD, 12);
		// Font font = new Font("Consolas", Font.PLAIN, 12);
		textRenderer = new JoglTextRenderer(font, true, false);
		textRenderer.setColor(0.9f, 0.9f, 0f, 1f);
		displayTimeout = true;

		// TODO: List of settings that need to be incorporated eventually
		if (glNormalize == true) {
			gl.glEnable(GL2.GL_NORMALIZE);
		}
		else {
			gl.glDisable(GL2.GL_NORMALIZE);
		}
		
		// FIXME: anisotropy does not work
		// TODO: need to restart in order for this to apply
		if (textureAnisotropy.isChecked()) {
			float[] maxaniso = new float[1];
			gl.glGetFloatv(GL.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, maxaniso, 0);
			textureAnisotropy.setMaximum(maxaniso[0]);

//			gl.glTexParameterf(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, textureAnisotropy.getValue());
//			gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, textureAnisotropy.getValue());
		}
		else {
			// Do not use anisotropy
//			gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, 1.0f);
		}


		// Enable multisampling
		if (capabilities.getSampleBuffers() && capabilities.getNumSamples() > 0) {
			gl.glEnable(GL.GL_MULTISAMPLE);
		} else {
			gl.glDisable(GL.GL_MULTISAMPLE);
		}

		// Enable smooth shading
		gl.glShadeModel(ecbShade.getSelected().GL());

		// Clear color
		Color color = backgroundColorChooser.getColor();
		gl.glClearColor(color.getRed() / 255f, color.getGreen() / 255f,
				color.getBlue() / 255f, 0f);
		
		// Clear depth
		gl.glClearDepth(clearDepth);

		if (glDepthTest.getValue())
			gl.glEnable(GL.GL_DEPTH_TEST);
		else
			gl.glDisable(GL.GL_DEPTH_TEST);

		if (glCullFace.getValue()) {
			gl.glEnable(GL.GL_CULL_FACE);
		}
		else
			gl.glDisable(GL.GL_CULL_FACE);

		// TODO: add combox box for this
		gl.glDepthFunc(GL.GL_LEQUAL);

		// Default blending options
		// FIXME: seems like we cannot put it back on if disabled during runtime
		if (glBlend.getValue())
			gl.glEnable(GL.GL_BLEND);
		else
			gl.glDisable(GL.GL_BLEND);

		gl.glBlendFunc(ecbBlendSrc.getSelected().GL(), ecbdBlendDest
				.getSelected().GL());

		if (glLineSmooth.getValue()) {
			gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
			gl.glEnable(GL.GL_LINE_SMOOTH);
		} else
			gl.glDisable(GL.GL_LINE_SMOOTH);

		// Line settings
		gl.glGetFloatv(GL2.GL_LINE_WIDTH_GRANULARITY, lineValues, 0);
		gl.glGetFloatv(GL2.GL_LINE_WIDTH_RANGE, lineValues, 1);

		// Point smoothing
		if (glPointSmooth.getValue()) {
			gl.glHint(GL2.GL_POINT_SMOOTH_HINT, GL.GL_NICEST);
			gl.glEnable(GL2.GL_POINT_SMOOTH);
		} else
			gl.glDisable(GL2.GL_POINT_SMOOTH);

		// Polygon smoothing
		if (glPolygonSmooth.getValue()) {
			gl.glHint(GL2.GL_POLYGON_SMOOTH_HINT, GL.GL_NICEST);
			gl.glEnable(GL2.GL_POLYGON_SMOOTH);
		} else
			gl.glDisable(GL2.GL_POLYGON_SMOOTH);

		hasChanged = false;
	}

	private VerticalFlowPanel panel = null;

	public JPanel getControls() {
		if (panel != null)
			return panel.getPanel();

		panel = new VerticalFlowPanel();

		ParameterListener changeListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				hasChanged = true;
			}
		};

		// Reload label
		final JLabel lblr = new JLabel();
		lblr.setForeground(Color.red);

		// Multisampling
		numsamples.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {

				System.out.println(RELOAD_REQUEST_MESSAGE);
				GLCapabilities glcaps = getGLCapabilities();
				glcaps.setNumSamples(numsamples.getValue());
				glcaps.setSampleBuffers(numsamples.getValue() > 1);

				System.out.println("Setting with " + glcaps.getNumSamples()
						+ " samples.");

				// caps.setSampleBuffers(true);
				// caps.setNumSamples(samples);
				// caps.setHardwareAccelerated(true);
				// caps.setDoubleBuffered(true);

				lblr.setText(RELOAD_REQUEST_MESSAGE);
			}
		});

		panel.add(numsamples.getSliderControlsExtended());
		panel.add(textureAnisotropy.getSliderControlsExtended());
		textureAnisotropy.setChecked(false);

		VerticalFlowPanel blendPanel = new VerticalFlowPanel();
		blendPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Blending"));
		blendPanel.add(glBlend.getControls());

		blendPanel.add(ecbBlendSrc.getControls());
		blendPanel.add(ecbdBlendDest.getControls());
		panel.add(blendPanel.getPanel());

		// Smoothing
		JPanel smoothPanel = new JPanel(new GridLayout(1, 3));
		smoothPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Smoothing"));

		smoothPanel.add(glLineSmooth.getControls());
		smoothPanel.add(glPointSmooth.getControls());
		smoothPanel.add(glPolygonSmooth.getControls());
		panel.add(smoothPanel);

		// Options
		VerticalFlowPanel optionsPanel = new VerticalFlowPanel();
		optionsPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Options"));

		optionsPanel.add(ecbShade.getControls());
		optionsPanel.add(glDepthTest.getControls());
		optionsPanel.add(glCullFace.getControls());
		optionsPanel.add(backgroundColorChooser.getControls());

		panel.add(optionsPanel.getPanel());

		textureAnisotropy.addParameterListener(changeListener);
		glBlend.addParameterListener(changeListener);
		ecbBlendSrc.addParameterListener(changeListener);
		ecbdBlendDest.addParameterListener(changeListener);
		glLineSmooth.addParameterListener(changeListener);
		glPointSmooth.addParameterListener(changeListener);
		glPolygonSmooth.addParameterListener(changeListener);
		ecbShade.addParameterListener(changeListener);
		glDepthTest.addParameterListener(changeListener);
		glCullFace.addParameterListener(changeListener);
		backgroundColorChooser.addParameterListener(changeListener);

		/*
		 * Reload panel
		 */
		JPanel reloadpanel = new JPanel(new FlowLayout());
		JButton btnReload = new JButton("reload");
		btnReload.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				needsReload = true;
			}
		});

		reloadpanel.add(lblr);
		reloadpanel.add(btnReload);
		panel.add(reloadpanel);

		return panel.getPanel();
	}

	public void setColor(Color color) {
		backgroundColorChooser.setColor(color);
	}
	
	public void setClearColor(Color clearColor) {
		backgroundColorChooser.setColor(clearColor);
	}

	public void setCullFace(boolean b) {
		glCullFace.setValue(b);
	}

	public void setReloaded() {
		needsReload = false;
	}

	public int getFSAA() {
		return numsamples.getValue();
	}
	
	public void setShowDisplay(boolean b) {
		showDisplay = b;
	}

}
