package gl.renderer;

import gl.math.FlatMatrix4d;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.Parameter3d;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import swing.text.GreekToolset.GreekLetter;

public class SceneRotator {
	public enum ViewAngle { DEFAULT, X, Y, Z, XY, XZ, YZ, XYZ };

    private DoubleParameter rspeed = new DoubleParameter("rotation speed", 0.001, -0.05, 0.05);
    private final static Random rand = new Random();
    private final static Vector3d randvec = new Vector3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
    private Parameter3d dpicker = new Parameter3d();
//    private DoubleParameter theta = new DoubleParameter("theta", 0, 0, 2 * Math.PI);
    private BooleanParameter piStop = new BooleanParameter(GreekToolset.toString(GreekLetter.PI) + "-stop", false);
    
    private double thetav = 0;
    private Vector3d direction = new Vector3d();

    public SceneRotator() {
    	r = new Matrix4d();
    	r.setIdentity();
    	r0 = new Matrix4d();
    	rtheta = new Matrix4d();
    	rv = new Matrix4d();
    	direction = new Vector3d();
    	
    	applyView(1, 0, 0);
    	
    	// Launch rotation thread
        Timer timer = new Timer();
        
        // No need to go too fast... refresh rate is at most 60 fps
        double fps = 60;

        TimerTask tt = new TimerTask() {
			@Override
			public void run() {
		    	
		    	updateComposedMatrix();
		    	
		    	if (rspeed.isChecked() && init) {
			    	double tp = thetav + rspeed.getValue();
			    	
			    	if (tp > 2 * Math.PI) {
			    		tp = tp - 2 * Math.PI;
			    		
			    		if (piStop.getValue()) {
			    			rspeed.setChecked(false);
			    			thetav = 0;
			    			return;
			    		}
			    	}
			    	thetav = tp;
		    	}
			}
        };        
        timer.scheduleAtFixedRate(tt, 0, (long) (1000 / fps));

    }
    
    private EnumComboBox<ViewAngle> ecb = new EnumComboBox<ViewAngle>("View Angle", ViewAngle.DEFAULT);
    
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Scene Rotator"));

        vfp.add(dpicker.getControls());
        dpicker.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				direction.set(dpicker.getValue());
				direction.normalize();
			}
		});
        
        vfp.add(rspeed.getSliderControlsExtended());
        rspeed.setChecked(false);
        
        vfp.add(piStop.getControls());
        
//        rspeed.addParameterListener(new ParameterListener() {
//			
//			@Override
//			public void parameterChanged(Parameter parameter) {
//				setInitialRotation();
//			}
//		});
        
        ecb.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				applyViewAngle(ecb.getSelected());
			}
		});
      
        vfp.add(ecb.getControls());
        
        CollapsiblePanel cp = new CollapsiblePanel(vfp.getPanel(), "Scene rotator");
//        cp.collapse();
		return cp;
    }

    private boolean init = false;
    
    public void rotate(GLAutoDrawable drawable) {
    	init = true;
    	
    	FlatMatrix4d fm = new FlatMatrix4d(r);
    	
    	GL2 gl = drawable.getGL().getGL2();
    	gl.glMultMatrixd(fm.asArray(), 0);
    }


    private Matrix4d r0;
    private Matrix4d r;
    private Matrix4d rtheta;
    private Matrix4d rv;
    
    private Matrix4d updateComposedMatrix() {
    	// R = RT_0 * R_theta * Rv
//    	r.mul(computeRotationMatrix(theta), computeDirectionalMatrix());
//    	r.mul(r0, r);
//    	r.mul(rv, computeRotationMatrix(theta));
//    	r.setIdentity();
    	
    	// 0-length vector means no rotation
    	if (direction.length() > 1e-4) {
    		r.set(new AxisAngle4d(direction, thetav));
    	}
    	else {
    		r.setIdentity();
    	}
    	
    	return r;
    }
    
    /**
     * The (inverse) initial rotation will pre-transform all successive rotations
     */
    private void computeInitialRotation() {
    	r0 = new Matrix4d(r);
    	r0.transpose();
    }
    
    @Deprecated
    private Matrix4d computeRotationMatrix(double theta) {
    	rtheta.setColumn(0, 1, 0, 0, 0);
    	rtheta.setColumn(1, 0, Math.cos(theta), -Math.sin(theta), 0);
    	rtheta.setColumn(2, 0, Math.sin(theta), Math.cos(theta), 0);
    	rtheta.setColumn(3, 0, 0, 0, 1);
    	return rtheta;
    }
    
    @Deprecated
    private Matrix4d computeDirectionalMatrix() {
    	Vector3d v2 = new Vector3d();
    	Vector3d v3 = new Vector3d();
    	
    	v2.cross(direction, randvec);
    	v3.cross(v2, direction);
    	
    	direction.normalize();
    	v2.normalize();
    	v3.normalize();

    	// Fill in matrix
    	rv.setColumn(0, direction.x, direction.y, direction.z, 0);
    	rv.setColumn(1, v2.x, v2.y, v2.z, 0);
    	rv.setColumn(2, v3.x, v3.y, v3.z, 0);
    	rv.setColumn(3, 0, 0, 0, 1);
    	return rv;
    }
    
    private void applyView(int x, int y, int z) {
    	direction.set(x, y, z);
    	direction.normalize();
    	
    	dpicker.setValue(new Point3d(direction));
    	
    	thetav = 0;
    }
    
    public void applyViewAngle(ViewAngle view) {
    	switch (view) {
    	case DEFAULT:
    		reset();
    		break;
    	case X:
    		applyView(1, 0, 0);
    		break;
    	case Y:
    		applyView(0, 1, 0);
    		break;
    	case Z:
    		applyView(0, 0, 1);
    		break;
    	case XY:
    		applyView(1, 1, 0);
    		break;
    	case XZ:
    		applyView(1, 0, 1);
    		break;
    	case YZ:
    		applyView(0, 1, 1);
    		break;
    	case XYZ:
    		applyView(1, 1, 1);
    		break;
    	}
	}
    
    public void reset() {
    	thetav = 0;
    	direction.set(1, 0, 0);
    	computeInitialRotation();
    }

}
