package gl.renderer;

import gl.antialiasing.MotionBlur;
import gl.antialiasing.SceneJitter2D;
import gl.antialiasing.SceneJitter2D.JITTER_NUMBER;
import gl.geometry.GLGeometry;
import gl.geometry.GLObject;
import gl.geometry.LightRoom;
import gl.geometry.WorldAxis;
import gl.geometry.primitive.Cube;
import gl.math.FlatMatrix4d;
import gl.texture.CubeMap;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import math.interpolation.TrilinearInterpolation;
import swing.SwingTools;
import swing.component.CollapsiblePanel;
import swing.component.ControlFrame;
import swing.component.VerticalFlowPanel;
import swing.event.AdapterState;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.IntDiscreteParameter;
import swing.parameters.IntParameter;
import sytem.std.StdLogger;
import tools.FPSTimer;
import tools.SimpleTimer;
import tools.geom.MathToolset;
import tools.loader.ByteUtility;
import volume.VolumeRenderer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.awt.ImageUtil;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;
import com.jogamp.opengl.util.glsl.ShaderState;

import config.GraphicsToolsConstants;

/**
 * @author piuze
 */
public class JoglRenderer implements GLEventListener, Interactor {

	public enum LayoutStyle {
		FOURQUAD, CANVASDOMINANT;
		
		public static LayoutStyle DEFAULT = CANVASDOMINANT;
	}

	private boolean terminationRequest = false;
	
	private boolean running = false;

	private LightRoom room;

	private AdapterState adapterState;

	private FPSTimer fps = new FPSTimer();

	private JoglTextRenderer textRenderer;

	/**
	 * The trackball and camera
	 */
	private TrackBallCamera trackBall;

	/**
	 * The scene, needed in the loaded architecture when loading cameras
	 */
	private GLObject scene;

	/**
	 * The frame containing all controls for this viewing application
	 */
	public ControlFrame controlFrame;

	private SceneRotator rotator = new SceneRotator();

	/**
	 * The dimension of the display screen
	 */
	public final Dimension size;
	public final Dimension controlSize;
	public final Dimension initSize;

	public final static boolean DefaultShowLogger = true;
	private boolean showLogger = DefaultShowLogger;
	
	public JFrame frame;

	private GLCanvas glCanvas;

	public GLCanvas getCanvas() {
		return glCanvas;
	}

//	private GLMultisampler multisampler = null;

	private List<Interactor> interactors = new LinkedList<Interactor>();

	/**
	 * Note: fullscreen mode has issues with respect to getting the size of the
	 * drawable. This should most certainly not be used!
	 */
	private BooleanParameter fullscreen = new BooleanParameter("Fullscreen", false);

	/**
	 * Device used for full screen mode
	 */
	private GraphicsDevice usedDevice;

	private FPSAnimator animator;

	private GLViewerConfiguration glconfig;

	private IntDiscreteParameter numjitters;

	private DoubleParameterPoint3d blur = new DoubleParameterPoint3d("offset",
			new Point3d(0.1, 0.1, 0.1), new Point3d(-1, -1, -1), new Point3d(1,
					1, 1));

	private DoubleParameter blurMag = new DoubleParameter("magnitude", 2.2, 0,
			10);

	private IntParameter blurIts = new IntParameter("substeps", 12, 0, 100);

	private BooleanParameter blurEnabled = new BooleanParameter("enabled",
			false);

	private BooleanParameter drawFps = new BooleanParameter("draw fps",
			true);

	private BooleanParameter drawWorld = new BooleanParameter("draw world",
			false);

	private CubeMap coordinateMap = null;

	private FlatMatrix4d worldTransformation;
	
	public static StdLogger stdLogger = new StdLogger();

	public final static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	public final static int DEFAULT_CONTROLS_WIDTH = 550;
	public final static int SCREEN_PADDING = 10;
	
	public static Dimension getDefaultCanvasDimension() {
		return new Dimension(screenSize.width - DEFAULT_CONTROLS_WIDTH - SCREEN_PADDING, screenSize.height-100);
	}
	
	public static Dimension getDefaultControlsDimension(Dimension canvasDimension) {
		
		return new Dimension(DEFAULT_CONTROLS_WIDTH, canvasDimension.height);
	}

	public JoglRenderer(String name) {
		this(name, getDefaultCanvasDimension(), getDefaultControlsDimension(getDefaultCanvasDimension()), true);
	}

	public JoglRenderer(String name, GLObject scene) {
		this(scene, name, getDefaultCanvasDimension(), getDefaultControlsDimension(getDefaultCanvasDimension()), true);
	}	
	
	public JoglRenderer(String name, GLObject scene, boolean highResolution) {
		this(scene, name, getDefaultCanvasDimension(), getDefaultControlsDimension(getDefaultCanvasDimension()), true, new GLViewerConfiguration(highResolution), LayoutStyle.DEFAULT);
	}

	public JoglRenderer(String name, Dimension dimension) {
		this(name, dimension, getDefaultControlsDimension(dimension), true);
	}

	/**
	 * Creates a new easy viewer with given sizes for display and controls
	 * 
	 * @param name
	 * @param scene
	 * @param canvasSize
	 * @param controlSize
	 */
	public JoglRenderer(String name, Dimension canvasSize,
			Dimension controlSize, boolean attachTrackBall) {
		this(null, name, canvasSize, controlSize,
				attachTrackBall, new GLViewerConfiguration(), LayoutStyle.DEFAULT);
	}

	public JoglRenderer(GLObject scene, String name, Dimension canvasSize,
			Dimension controlSize, boolean attachTrackBall) {
		this(scene, name, canvasSize, controlSize,
				attachTrackBall, new GLViewerConfiguration(), LayoutStyle.DEFAULT);
	}

	public static boolean verbose = true;
	/**
	 * Creates a new easy viewer with given sizes for display and controls
	 * 
	 * @param name
	 * @param scene
	 * @param size
	 * @param controlSize
	 * @param attachTrackBall
	 *            If the trackball should be receiving event notifications.
	 */
	public JoglRenderer(GLObject scene, String name, Dimension canvasSize, Dimension controlSize,
			boolean attachTrackBall, GLViewerConfiguration config, LayoutStyle style) {
		
		this.scene = scene;
		
		// Use canva's height
		controlSize.height = canvasSize.height;
		this.controlSize = controlSize;

		this.size = canvasSize;
		this.initSize = new Dimension(size.width + controlSize.width, size.height);
		
		this.glconfig = config;

		SimpleTimer timer = new SimpleTimer();
		adapterState = new AdapterState();

		room = new LightRoom();
		room.setVisible(false);

		trackBall = new TrackBallCamera();
		this.attachTrackBall = attachTrackBall;

		worldTransformation = new FlatMatrix4d();
		worldTransformation.getBackingMatrix().setIdentity();

		// Create the UI
		frame = new JFrame(name);

		// Create the canvas
		createCanvas(glconfig.getGLCapabilities(), size);
		
		createRendererControls(style);
		
		if (verbose) 
			System.out.println("Viewer creation time = " + timer.tick_ms());
	}
	
	private JSplitPane splitPane;
	
	private static final double SplitPaneRatio = 0.25;
	
	public enum SizePreset { Small, Medium, Large }
	
	public void updateSize(SizePreset preset) {

		double ratio = 1;
		switch (preset) {
		case Large:
			ratio = 1;
			break;
		case Medium:
			ratio = 0.75;
			break;
		case Small:
			ratio = 0.5;
			break;
		}
		Dimension dimFrame = new Dimension(MathToolset.roundInt(ratio * screenSize.width), MathToolset.roundInt(ratio * screenSize.height));
//		Dimension dimCanvas = new Dimension(dimFrame.width, dimFrame.height);
//		glCanvas.setPreferredSize(dimCanvas);
//		glCanvas.setSize(dimCanvas);
		frame.setPreferredSize(dimFrame);
		frame.setSize(dimFrame);
	}
	
	public void setShowLogger(boolean b) {
		if (splitPane.getTopComponent() instanceof JSplitPane) {
			JSplitPane pane = (JSplitPane) splitPane.getLeftComponent();
			if (b) {
				pane.setDividerLocation(1d-SplitPaneRatio);
			}
			else {
				pane.setDividerLocation(1d);
			}
		}
	}
	
	public void applyStyle(LayoutStyle style) {
		frame.getContentPane().removeAll();
		
		// Dummy non-zero dimension
		Dimension mindim = new Dimension(100, 100);
		
		switch (style) {
		case CANVASDOMINANT:
			frame.getContentPane().setLayout(new BorderLayout());
			JPanel splitCanvas = new JPanel(new BorderLayout());
			
			// Set canvas
			splitCanvas.setMinimumSize(new Dimension());
			splitCanvas.setSize(mindim);

			// Set logger + controls
			JSplitPane controlSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, controlFrame.getContentPanel(), stdLogger.getControls());
			controlSplit.setResizeWeight(1-SplitPaneRatio);

			// Assemble split pane
			splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, controlSplit, splitCanvas);
			
			frame.getContentPane().add(splitPane);
			splitPane.setContinuousLayout(true);
			splitCanvas.add(glCanvas);
			splitPane.setResizeWeight(SplitPaneRatio);
			break;
			
		case FOURQUAD:
			frame.getContentPane().setLayout(new GridLayout(2,1));
						
			// Set canvas
			glCanvas.setMinimumSize(new Dimension());
			glCanvas.setPreferredSize(mindim);
			JPanel split2 = new JPanel(new BorderLayout());

			split2.setMinimumSize(new Dimension());
			split2.setSize(mindim);
			split2.add(glCanvas);

			// Set logger
			JPanel split1 = stdLogger.getControls();
			split1.setMinimumSize(new Dimension());
			split1.setPreferredSize(mindim);
			split1.setSize(mindim);
			
			splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, split1, split2);
			splitPane.setResizeWeight(SplitPaneRatio);
			
			frame.getContentPane().add(splitPane);
			frame.getContentPane().add(controlFrame.getContentPanel());
			break;
		default:
			break;
		}
	}
	
	private void createCanvas(GLCapabilities caps, Dimension size) {
		glCanvas = new GLCanvas(caps);
		glCanvas.setPreferredSize(size);
//		glCanvas.setSize(size);
		glCanvas.setIgnoreRepaint(true);
		glCanvas.addGLEventListener(this);

		glCanvas.addMouseListener(adapterState);
		glCanvas.addMouseMotionListener(adapterState);
		glCanvas.addKeyListener(adapterState);
		
	}

	private void setScene(GLObject scene) {
		
		this.scene = scene;
		buildGeometry();
		addSceneControls();
	}
	
	private void createRendererControls(LayoutStyle style) {
		controlFrame = new ControlFrame("Controls", controlSize, null, false);

		controlFrame.add("Camera", trackBall.getControls());

		// Viewer panel
		VerticalFlowPanel vpanel = new VerticalFlowPanel();

		vpanel.add(drawFps.getControls());
		vpanel.add(drawWorld.getControls());
		
		JButton btnScreenshot = new JButton("Take Screenshot");
		btnScreenshot.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setScreenshotRequest();
			}
		});
		vpanel.add(btnScreenshot);
//		vpanel.add(fullscreen.getControls());
		vpanel.add(rotator.getControls());

		// Config panel
//		controlFrame.add("OpenGL", glconfig.getControls());
		JPanel glconfigPanel = glconfig.getControls();
		vpanel.add(glconfigPanel);

		/*
		 * Visual effects
		 */
		VerticalFlowPanel effectPanel = new VerticalFlowPanel();
		effectPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Effectors"));

		/*
		 * Antialiasing
		 */

		VerticalFlowPanel aliasingPanel = new VerticalFlowPanel();
		aliasingPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Antialiasing"));
		effectPanel.add(aliasingPanel.getPanel());

		// Jittering offsets
		numjitters = new IntDiscreteParameter("number of jitters", 0,
				JITTER_NUMBER.getJitterSamples());
		aliasingPanel.add(numjitters.getSliderControlsExtended());

		/*
		 * Motion blur
		 */
		VerticalFlowPanel blurPanel = new VerticalFlowPanel();
		blurPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Motion blur"));
		effectPanel.add(blurPanel.getPanel());

		blurPanel.add(blurEnabled.getControls());
		blurPanel.add(blur.getControls());
		blurPanel.add(blurMag.getSliderControls());
		blurPanel.add(blurIts.getSliderControls());

		CollapsiblePanel cpEffects = new CollapsiblePanel(effectPanel,  "Effectors");
		vpanel.add(cpEffects.collapse());

//		vpanel.add(effectPanel.getPanel());

		// vpanel.add(glconfig.getControls());

		// Room panel
//		controlFrame.add("Room", room.getControls());
		JPanel roomPanel = room.getControls();
		CollapsiblePanel cpRoom = new CollapsiblePanel(roomPanel,  "Room");
		vpanel.add(cpRoom.collapse());

		controlFrame.add("Renderer", vpanel.getPanel());

//		fullscreen.addParameterListener(new ParameterListener() {
//			@Override
//			public void parameterChanged(Parameter parameter) {
//				updateFullscreen();
//			}
//		});
		
		frame.setUndecorated(false);
		frame.setSize(frame.getContentPane().getPreferredSize());

		/*
		 * Apply layout style
		 */
		applyStyle(style);
		
		frame.setLocation(SCREEN_PADDING / 2, SCREEN_PADDING / 2);
	}
	
	private void addSceneControls() {
		if (scene != null) {
			String sname = scene.getName() == null || scene.getName().equals("") ? "Scene": scene.getName();

			// Scene panel
			controlFrame.add(scene.getName(), scene.getControls());
			controlFrame.setSelectedTab(sname);
		}
	}

	public static void applyNimbusLookAndFeel() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
	}

	public void setWorldTransformation(Matrix4d t) {
		worldTransformation.setBackingMatrix(t);
	}

	private boolean attachTrackBall = false;
	private boolean useCubeMap = false;
	
	private void createCubemap(GL2 gl) {
		String cubepath = GraphicsToolsConstants.CUBEMAP_TEXTURE_PATH;

		coordinateMap = new CubeMap(cubepath);
		coordinateMap.init(gl);
	}

	protected void reload() {
		stop(false);
		glconfig.setReloaded();
		scene.reload(glconfig);
	}

	public TrackBallCamera getCamera() {
		return trackBall;
	}

	public void start() {
		if (scene != null){
			start(scene);
		}
	}
	
	public void startSnapshotAndExit(GLObject scene) {
		start(scene);
		
		// Wait asynchronously for a few seconds and then ask for a termination request
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					int frameBufferSize = 15;
					
					// Wait for a few frames
					waitForFrames(frameBufferSize);
					
					// Set a snapshot request
					setScreenshotRequest();
					
					// Wait for a few more frames
					waitForFrames(frameBufferSize);

					terminationRequest = true;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).run();
	}
	
	public void waitForFrames(int numFrames) throws InterruptedException {
		int numTicks = fps.numTicks();
		// Wait until the selected number of frames has been buffered
		while (fps.numTicks() < numTicks + numFrames)
			Thread.sleep(100);
	}
	
	/**
	 * Starts the viewer
	 */
	public void start(GLObject scene) {
		if (running) return;
		
		running = true;

		// No need to go higher than 60Hz with most monitors...
		animator = new FPSAnimator(glCanvas, 60, true);

		if (attachTrackBall)
			addInteractor(trackBall);
		
		addInteractor(this);
		
		setScene(scene);
		
		try {
			frame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					stop(true);
				}
			});

			glCanvas.requestFocus();

			frame.setSize(initSize);
			frame.setPreferredSize(initSize);
			frame.setVisible(true);

			if (animator != null)
				animator.start();

			// FIXME: this is a hack to make sure controls are properly arranged
			toggleControlVisibility();
			waitForFrames(5);
			toggleControlVisibility();
			setShowLogger(false);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	private void updateFullscreen() {
//
//		if (fullscreen.getValue()) {
//			frame.setUndecorated(true);
//			usedDevice = GraphicsEnvironment.getLocalGraphicsEnvironment()
//					.getDefaultScreenDevice();
//			usedDevice.setFullScreenWindow(frame);
//			usedDevice.setDisplayMode(findDisplayMode(usedDevice
//					.getDisplayModes(), screenSize.width,
//					screenSize.height, // size.width, size.height,
//					usedDevice.getDisplayMode().getBitDepth(), usedDevice
//							.getDisplayMode().getRefreshRate()));
//		} else {
//			if (usedDevice != null) {
//				usedDevice.setFullScreenWindow(null);
//				usedDevice = null;
//			}
//			frame.setUndecorated(false);
//			frame.setSize(frame.getContentPane().getPreferredSize());
//			// frame.setLocation(
//			// ( screenSize.width - frame.getWidth() ) / 2,
//			// ( screenSize.height - frame.getHeight() ) / 2
//			// );
//		}
//	}

	/**
	 * Stops the viewer
	 */
	public void stop(boolean quit) {
		try {
			if (animator != null && animator.isAnimating())
				animator.stop();
			if (fullscreen.getValue()) {
				usedDevice.setFullScreenWindow(null);
				usedDevice = null;
			}
			pullThePlug(frame);
			pullThePlug(controlFrame.getJFrame());
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (quit)
				System.exit(0);
		}
	}
	
	/**
	 * UI proper shutdown
	 * Taken from StackOverflow (usr Gaki Alankus)
	 */
	private void pullThePlug(JFrame frame) {
		if (frame == null)
			return;
		
		// Prevent frame from triggering exit
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

//	    // this will make sure WindowListener.windowClosing() et al. will be called.
//	    WindowEvent wev = new WindowEvent(frame, WindowEvent.WINDOW_CLOSING);
//	    Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);

	    // this will hide and dispose the frame, so that the application quits by
	    // itself if there is nothing else around. 
	    frame.setVisible(false);
	    frame.dispose();
	}

	public void stop() {
		stop(true);
	}

	private static final int DONT_CARE = -1;

	public DisplayMode findDisplayMode(DisplayMode[] displayModes,
			int requestedWidth, int requestedHeight, int requestedDepth,
			int requestedRefreshRate) {
		// Try to find an exact match
		DisplayMode displayMode = findDisplayModeInternal(displayModes,
				requestedWidth, requestedHeight, requestedDepth,
				requestedRefreshRate);

		// Try again, ignoring the requested bit depth
		if (displayMode == null)
			displayMode = findDisplayModeInternal(displayModes, requestedWidth,
					requestedHeight, DONT_CARE, DONT_CARE);

		// Try again, and again ignoring the requested bit depth and height
		if (displayMode == null)
			displayMode = findDisplayModeInternal(displayModes, requestedWidth,
					DONT_CARE, DONT_CARE, DONT_CARE);

		// If all else fails try to get any display mode
		if (displayMode == null)
			displayMode = findDisplayModeInternal(displayModes, DONT_CARE,
					DONT_CARE, DONT_CARE, DONT_CARE);

		return displayMode;
	}

	private DisplayMode findDisplayModeInternal(DisplayMode[] displayModes,
			int requestedWidth, int requestedHeight, int requestedDepth,
			int requestedRefreshRate) {
		DisplayMode displayModeToUse = null;
		for (int i = 0; i < displayModes.length; i++) {
			DisplayMode displayMode = displayModes[i];
			if ((requestedWidth == DONT_CARE || displayMode.getWidth() == requestedWidth)
					&& (requestedHeight == DONT_CARE || displayMode.getHeight() == requestedHeight)
					&& (requestedHeight == DONT_CARE || displayMode
							.getRefreshRate() == requestedRefreshRate)
					&& (requestedDepth == DONT_CARE || displayMode
							.getBitDepth() == requestedDepth))
				displayModeToUse = displayMode;
		}

		return displayModeToUse;
	}

	/**
	 * Causes the 3D viewer to be repainted. Currently, this is ignored as we're
	 * using an FPSanimator
	 */
	public void redisplay() {
		// frame.repaint();
		// glCanvas.display();
	}

	/**
	 * Attaches the provided interactor to this viewer's 3D canvas
	 * 
	 * @param interactor
	 */
	public void addInteractor(Interactor interactor) {
		if (interactors.contains(interactor) || interactor == null) return;
		
		interactors.add(interactor);
		interactor.attach(glCanvas);
		
	}

	public void removeInteractor(Interactor interactor) {
		interactors.remove(interactor);
		interactor.detach(glCanvas);
	}

	public void removeInteractors() {
		interactors.clear();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		glconfig.init(drawable);

		sceneGeometry.init(drawable);

		if (useCubeMap)
			createCubemap(gl);
		
		// Font font = new Font("Andale Mono", Font.BOLD, 20);
		Font font = new Font("Consolas", Font.PLAIN, 20);
		textRenderer = new JoglTextRenderer(font, true, false);
		textRenderer.setColor(0.9f, 0.9f, 0f, 1f);

//		System.out.println("Creating multisampler with " + glconfig.getFSAA() + " FSAA samples and " + NUM_SUPER_DEFAULT + " x supersampling.");
//		multisampler = new GLMultisampler(gl, glconfig.getFSAA(), NUM_SUPER_DEFAULT);
//		multisampler = new GLMultisampler(gl, 0, 0);
//
//		if (!multisampler.isMultisamplingEnabled()) {
//			System.err
//					.println("ERROR: multisampling was not enabled. Turn on JOGL's multisampling in GLCapabilities and restart application.");
//		}

	}

	/**
	 * The geometry for the room and scene.
	 */
	private GLGeometry sceneGeometry;

	private void buildGeometry() {
		sceneGeometry = new GLGeometry() {

			@Override
			public void init(GLAutoDrawable drawable) {
				scene.init(drawable);
				room.init(drawable);
			}

			@Override
			public String getName() {
				return "";
			}

			@Override
			public void display(GLAutoDrawable drawable) {
				GL2 gl = drawable.getGL().getGL2();
				
				// Update canvas dimension
				updateCanvasSize(drawable.getWidth(), drawable.getHeight());

				// Set light position based on camera position
				setCamLight();
				
				if (!blurEnabled.getValue())
					room.display(drawable);

				// Motion blur has priority over Jittering

				if (blurEnabled.getValue() && blurIts.getValue() > 0) {
					Vector3d blurv = new Vector3d(blur.getPoint3d());
					blurv.scale(blurMag.getValue());
					MotionBlur.blur(drawable, scene, blurv, blurIts.getValue());
				}
				// Do jittering
				else if (numjitters.isChecked() && numjitters.getValue() > 0) {
					SceneJitter2D.display(drawable, scene,
							JITTER_NUMBER.valueOf(numjitters.getValue()));
				} else {
					gl.glPushMatrix();
					scene.display(drawable);
					gl.glPopMatrix();
				}

				if (blurEnabled.getValue())
					room.display(drawable);
				
			}
		};
	}

	/**
	 * Set light position based on camera position.
	 */
	private void setCamLight() {
//		Point3d p_cam = new Point3d(0,0.4,0.8);
//		Point3d p_cam = new Point3d(-0.5,-0.5,0.5);
		Point3d p_cam = new Point3d(0, 0, 0);
		Matrix4d cam = trackBall.getModelviewTransformationPipeline();
//		System.out.println(cam);
		cam.invert();
		cam.transform(p_cam);
//		System.out.println(p_cam);
		room.setLightPosition(p_cam);
	}
	
	protected void updateCanvasSize(int width, int height) {
		size.setSize(width, height);
	}
	
	private boolean doubleBufferWait = false;

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		fps.tick();
		
		if (takingScreenshot)
			return;
		
		if (glconfig.hasChanged()) {
			glconfig.init(drawable);
		}
		
//		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
//		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		trackBall.prepareForDisplay(drawable);

		rotator.rotate(drawable);

//		gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE_MINUS_SRC_ALPHA);

		if (drawWorld.getValue()) {
			WorldAxis.display(gl);
		}
		sceneGeometry.display(drawable);
		
		if (useCubeMap && coordinateMap != null && !(screenshotRequested || screenshotWithControlsRequested )&& !takingScreenshot) {
			// Draw a cube in the lower left corner
			gl.glMatrixMode(GL2.GL_PROJECTION);
			gl.glPushMatrix();
			gl.glLoadIdentity();

			gl.glMatrixMode(GL2.GL_MODELVIEW);
			gl.glPushMatrix();
			gl.glLoadIdentity();
			trackBall.applyRotationTransformation(drawable);
			rotator.rotate(drawable);

			gl.glDisable(GL2.GL_LIGHTING);
			double vsize = Math.min(size.getWidth(), size.getHeight());

			// Draw the coordinate map in the bottom left corner
			gl.glViewport(drawable.getWidth() - (int) (vsize / 7), 0,
					(int) (vsize / 7), (int) (vsize / 7));
			coordinateMap.prepareForDrawing(drawable);
			Cube.draw(drawable, 1);
			coordinateMap.finishDrawing(drawable);
			// Restore viewport
			gl.glViewport(0, 0, (int) size.getWidth(), (int) size.getHeight());

			gl.glPopMatrix();
			gl.glPopMatrix();

		}

		if (!(screenshotRequested  || screenshotWithControlsRequested) && !takingScreenshot && !doubleBufferWait)
			glconfig.displayDetails(drawable);

		if (drawFps.getValue() && !(screenshotRequested || screenshotWithControlsRequested) && !takingScreenshot) {
			textRenderer.setColor(0.9f, 0.9f, 0f, 1f);
			textRenderer.drawTopRight(fps.toString(), drawable);
		}

		if (screenshotRequested && !takingScreenshot && !doubleBufferWait) {
			takeScreenshot(drawable);
			screenshotRequested = false;
		}
		else if (screenshotWithControlsRequested && !takingScreenshot && !doubleBufferWait) {
			takeSceenshotWithControls(drawable, screenshotFilename);
			screenshotWithControlsRequested = false;
		}
		else if ((screenshotRequested || screenshotWithControlsRequested) && doubleBufferWait) {
			doubleBufferWait = false;
		}

		// TODO: no longer need to call this
		trackBall.cleanupAfterDisplay(drawable);

		if (glconfig.needsReload()) {
			reload();
		}

		gl.glFlush();
		
		if (terminationRequest) {
			stop(true);
		}
	}

	/**
	 * Saves a snapshot of the current canvas to a file. The image is saved in
	 * png format and will be of the same size as the canvas. Note that if you
	 * are assembling frames saved in this way into a video, for instance, using
	 * virtualdub, then you'll need to take care that the canvas size is nice
	 * (i.e., a multiple of 16 in each dimension), or add a filter in virtualdub
	 * to resize the image to be a codec friendly size.
	 * 
	 * @param drawable
	 * @param file
	 * @return true on success
	 */
	public boolean snapshot(GLAutoDrawable drawable, File file) {
		GL2 gl = drawable.getGL().getGL2();
		
		int width = drawable.getWidth();
		int height = drawable.getHeight();
		gl.glReadPixels(0, 0, width, height, GL2.GL_ABGR_EXT,
				GL.GL_UNSIGNED_BYTE, imageBuffer);
		ImageUtil.flipImageVertically(image);

		try {
			if (!ImageIO.write(image, "png", file)) {
				System.err
						.println("Error writing file using ImageIO (unsupported file format?)");
				return false;
			}
		} catch (IOException e) {
			System.err.println("trouble writing " + file);
			e.printStackTrace();
			return false;
		}

		// print a message in the display window
		beginOverlay(drawable);
		String text = "RECORDED: " + file.toString();
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor4f(1, 0, 0, 1);
		JoglTextRenderer.printTextLines(drawable, text, 10,
				drawable.getHeight() - 20, 10, GLUT.BITMAP_HELVETICA_10);
		gl.glEnable(GL2.GL_LIGHTING);
		endOverlay(drawable);
		return true;
	}

	/** Image for sending to the image processor */
	private BufferedImage image;

	/** Image Buffer for reading pixels */
	private Buffer imageBuffer;

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		imageBuffer = ByteBuffer.wrap(((DataBufferByte) image.getRaster()
				.getDataBuffer()).getData());
	}

	public SceneRotator getRotator() {
		return rotator;
	}

	/**
	 * GLUT object to be shared
	 */
	public static GLUT glut = new GLUT();

	/**
	 * A <code>GLU</code> object (like a <code>GLUT</code> object) should only
	 * be used by one thread at a time. So create your own if you need one.
	 */
	public static GLU glu = new GLU();

	static public void setOrtho(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		int width = drawable.getWidth();
		int height = drawable.getHeight();
		
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(0, width, 0, height);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glViewport(0, 0, width, height);
		gl.glLoadIdentity();
	}
	
	/**
	 * Begin drawing overlay (e.g., text, screen pixel coordinate points and
	 * lines)
	 * 
	 * @param drawable
	 */
	static public void beginOverlay(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glPushAttrib(GL.GL_DEPTH_BUFFER_BIT | GL2.GL_ENABLE_BIT
				| GL2.GL_FOG_BIT | GL2.GL_LIGHTING_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		int width = drawable.getWidth();
		int height = drawable.getHeight();
		glu.gluOrtho2D(0, width, height, 0);
		gl.glDisable(GL.GL_DEPTH_TEST);
		gl.glDisable(GL.GL_TEXTURE_2D);
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glMatrixMode(GL2.GL_MODELVIEW);

	}

	/**
	 * End drawing overlay.
	 * 
	 * @param drawable
	 */
	static public void endOverlay(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glPopMatrix();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glPopMatrix();
		gl.glPopAttrib();
	}

	/**
	 * The program stacks used by <code>pushProgram()</code>/
	 * <code>popProgram()</code>. Since a different program can be active in
	 * each GL context, there is a different stack for each context.
	 */
	private static Map<GL, Stack<Integer>> programStacks = new HashMap<GL, Stack<Integer>>();

	/**
	 * Pushes a record of the current Slang Program for the specified GL
	 * context. In the same manner as <code>GL.glPushAttrib()</code>, this
	 * allows the rendering state to be restored (by calling
	 * {@link #popProgram(GL)}).
	 * 
	 * @param gl
	 *            the OpenGL context for which to push the active Slang Program
	 *            record
	 */
	public static void pushProgram(GL2 gl) {
		Stack<Integer> programStack = programStacks.get(gl);
		if (programStack == null) {
			programStack = new Stack<Integer>();
			programStacks.put(gl, programStack);
		}
		int currentProgram = gl.glGetHandleARB(GL2.GL_PROGRAM_OBJECT_ARB);
		programStack.push(currentProgram);
	}

	/**
	 * Pops the Slang Program state. The specified GL context is set to use the
	 * program (or fixed functionality) that was in use before the most recent
	 * call to {@link #pushProgram(GL)}(with the same <code>GL</code>).
	 * 
	 * @param gl
	 *            the OpenGL context for which to pop the active Slang Program
	 */
	public static void popProgram(GL2 gl) {
		Stack<Integer> programStack = programStacks.get(gl);
		if (programStack == null) {
			throw new RuntimeException("Slang Program stack underflow:"
					+ "popProgram(GL) was called for a"
					+ " GL context in which the Slang"
					+ " Program state had not been" + " pushed.",
					new EmptyStackException());
		}
		gl.glUseProgramObjectARB(programStack.pop());
		if (programStack.isEmpty()) {
			// Delete an empty stack, to avoid memory leaks in situations where
			// the GL for a given window changes
			programStacks.remove(gl);
		}
	}

	/**
	 * Uninstalls any currently installed program from the graphics processors.
	 * Fixed functionality is restored to the pipeline.
	 * 
	 * @param gl
	 *            the OpenGL context
	 */
	public static void useNoProgramObject(GL2 gl) {
		gl.glUseProgramObjectARB(0);
	}

	public static ShaderState createProgram(GL2 gl, String shader) {
		System.out.println("Creating shader program " + shader);
        ShaderCode vsCode = ShaderCode.create( gl, GL2ES2.GL_VERTEX_SHADER, JoglRenderer.class, "shader", "shader/bin", shader, false );
        ShaderCode fsCode = ShaderCode.create( gl, GL2ES2.GL_FRAGMENT_SHADER, JoglRenderer.class, "shader", "shader/bin", shader, false );
        
        ShaderProgram program = new ShaderProgram();
        program.add( vsCode );
        program.add( fsCode );
		if (!program.link(gl, System.err)) {
			throw new GLException("Couldn't link program: " + program);
		}	
		ShaderState shaderState = new ShaderState();
		shaderState.attachShaderProgram( gl, program, false);		
		
		shaderState.setVerbose(true);
		
		return shaderState;
	}

	public static ShaderState createProgram(GL2 gl, String vertex, String fragment) {
	    ShaderCode vsCode = new ShaderCode(GL2ES2.GL_VERTEX_SHADER, 1, new CharSequence[][] { { vertex.subSequence(0, vertex.length()) } });
	    ShaderCode fsCode = new ShaderCode(GL2ES2.GL_FRAGMENT_SHADER, 1, new CharSequence[][] { { fragment.subSequence(0, fragment.length()) } });

        ShaderProgram program = new ShaderProgram();
        program.add(vsCode);
        program.add(fsCode);
		if (!program.link(gl, System.err)) {
			throw new GLException("Couldn't link program: " + program);
		}	
		ShaderState shaderState = new ShaderState();
		shaderState.attachShaderProgram( gl, program, false);		
		
		shaderState.setVerbose(true);
		
		return shaderState;
	}

	/**
	 * Beging drawing with the regular opengl pipeline.
	 * <p>
	 * NOTE: this is hacked to assume that you're using shadows and these
	 * shadows are bound to units 3 and 4. This could perhaps be fixed to work
	 * properly with arbitrary shadow texture settings, but was necessary
	 * because push and pop attrib calls do not seem to save and restore this
	 * state properly.
	 * 
	 * @param gl
	 */
	static public void beginFixedPipelineMode(GL2 gl) {
		JoglRenderer.pushProgram(gl);
		JoglRenderer.useNoProgramObject(gl);
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glActiveTexture(GL.GL_TEXTURE0 + 3);
		gl.glDisable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE0 + 4);
		gl.glDisable(GL.GL_TEXTURE_2D);
	}

	/**
	 * Restore the previous vertex program (i.e., shadowed lighting mode) NOTE:
	 * this is hacked to assume that you're using shadows and these shadows are
	 * bound to units 3 and 4. This could perhaps be fixed to work properly with
	 * arbitrary shadow texture settings, but was necessary because push and pop
	 * attrib calls do not seem to save and restore this state properly.
	 * 
	 * @param gl
	 */
	static public void endFixedPipelineMode(GL2 gl) {
		gl.glActiveTexture(GL.GL_TEXTURE0 + 3);
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE0 + 4);
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE0);
		JoglRenderer.popProgram(gl);
	}

	public AdapterState getAdapterState() {
		return adapterState;
	}

	public LightRoom getRoom() {
		return room;
	}

	public JoglTextRenderer getTextRenderer() {
		return textRenderer;
	}
	
	private static List<String> OpenGLEXTs;
	private static List<String> GLUEXTs;

	public static List<String> listExtensions(GL gl) {
		if (OpenGLEXTs == null) {
			OpenGLEXTs = new LinkedList<String>();
			String exts = gl.glGetString(GL.GL_EXTENSIONS);
			StringTokenizer st = new StringTokenizer(exts, "\n");
			while (st.hasMoreTokens()) {
				// Space-delimited
				String ext = st.nextToken("\u0020");
				OpenGLEXTs.add(ext);
			}
		}
		
		return OpenGLEXTs;
	}

	public static List<String> listExtensions(GLU glu) {
		if (GLUEXTs == null) {
			GLUEXTs = new LinkedList<String>();
			String exts = glu.gluGetString(GL.GL_EXTENSIONS);
			
			if (exts == null) return null;
			
			StringTokenizer st = new StringTokenizer(exts, "\n");
			while (st.hasMoreTokens()) {
				// Space-delimited
				String ext = st.nextToken("\u0020");
				GLUEXTs.add(ext);
			}
		}
		
		return GLUEXTs;
	}

	public static boolean isExtensionSupported(GL gl, String extension) {
		return listExtensions(gl).contains(extension);
	}
	
	public ControlFrame getControlFrame() {
		return controlFrame;
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public Point getCanvasPosition() {
		Point position = new Point(splitPane.getDividerLocation() + splitPane.getDividerSize(), splitPane.getHeight());
		return position;
	}
	
	public Rectangle getCanvasRectangle() {
		Point pos = getCanvasPosition();
		Rectangle rect = new Rectangle(pos.x, 0, splitPane.getWidth(), splitPane.getHeight());
		return rect;
	}
	
	public void setDrawFPS(boolean b) {
		drawFps.setValue(b);
	}

	public void setDrawWorld(boolean b) {
		drawWorld.setValue(b);
	}

	private static int screenshotNumber = 0;
	
	private boolean screenshotWithControlsRequested = false;
	
	private boolean screenshotRequested = false;
	private boolean takingScreenshot = false;
	private String screenshotFilename = null;

	public boolean isScreenshotRequested() {
		return screenshotRequested;
	}

	public void setScreenshotRequest() {
		setScreenshotRequest(null);
	}
	
	public void setScreenshotRequest(String filename) {
		doubleBufferWait = true;
		screenshotWithControlsRequested = false;
		screenshotRequested = true;
		screenshotFilename = filename;
	}

	public void setScreenshotControlRequest(String filename) {
		doubleBufferWait = true;
		screenshotRequested = false;
		screenshotWithControlsRequested = true;
		screenshotFilename = filename;
	}

	/**
	 * Render
	 * @param drawable
	 * @param format either GL.GL_RGBA (for transparent background) or GL_RGB
	 * @return
	 */
	public BufferedImage renderToImage(GLAutoDrawable drawable, int format) {
		int w = drawable.getWidth();
		int h = drawable.getHeight();
		GL2 gl = drawable.getGL().getGL2();

		int bytesPerPixel = format == GL2.GL_RGBA ? 4 : 3;

		ByteBuffer pixelsRGBA = Buffers.newDirectByteBuffer(w * h * bytesPerPixel);
		pixelsRGBA.rewind();
		gl.glReadBuffer(GL.GL_BACK);
//		gl.glReadBuffer(GL.GL_FRONT);
		gl.glPixelStorei( GL.GL_PACK_ALIGNMENT, 1 );
		gl.glReadPixels(0, 0, w, h, format, GL2.GL_UNSIGNED_BYTE, pixelsRGBA);

		BufferedImage img = byteBufferToBufferedImage(drawable, pixelsRGBA, format);

		return img;
	}

	private void takeSceenshotWithControls(GLAutoDrawable drawable, String filepath) {
		
		takingScreenshot = true;
		
		// Full controls
		BufferedImage controls = SwingTools.getScreenShot(getFrame().getContentPane());

		// Render
		BufferedImage render = renderToImage(drawable, GL.GL_RGB);

		// Assemble composite (controls + render)
		Graphics2D g = (Graphics2D) controls.getGraphics();
		g.drawImage(render, getCanvasPosition().x, 0, null);

		try {
			File outputfile = new File(filepath);
			ImageIO.write(controls, "png", outputfile);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		takingScreenshot = false;
	}
	
	private void takeScreenshot(GLAutoDrawable drawable) {
		if (takingScreenshot)
			return;
		
		takingScreenshot = true;
		
		screenshotNumber++;
		
		if (screenshotFilename == null) {
			screenshotFilename = "screenshotAlpha" + screenshotNumber + ".png";
		}
		File sfile = new File(screenshotFilename);
		
		// Need to do this to prevent overwriting during next screenshot
		screenshotFilename = null;
		
		BufferedImage img = renderToImage(drawable, GL.GL_RGBA);
		
		try {
			sfile.mkdirs();
			ImageIO.write(img, "png", sfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Screenshot written to "
				+ sfile.getAbsolutePath());
		
		takingScreenshot = false;
	}
	
	private BufferedImage byteBufferToBufferedImage(GLAutoDrawable drawable, ByteBuffer pixels, int format) {

		int frameWidth = drawable.getWidth();
		int frameHeight = drawable.getHeight();

		int[] flatPixels = new int[frameWidth * frameHeight];
		int pixel = 0;
		pixels.rewind();
		
		BufferedImage bufferedImage = null;
		
		if (format == GL.GL_RGBA) {
			while (pixels.hasRemaining()) {
				// Do manual RGBA <> ARGB conversion
				byte R = pixels.get();
				byte G = pixels.get();
				byte B = pixels.get();
				byte A = pixels.get();
//				int bint = ByteUtility.bytesToInt(A, R, G, B, false);
				int bint = ((A & 0xff) << 24) + ((R & 0xff) << 16) + ((G & 0xff) << 8) + ((B & 0xff));
				flatPixels[pixel++] = bint;
			}
			
			
			// Create a new BufferedImage from the pixeldata.
			bufferedImage = new BufferedImage(frameWidth,frameHeight, BufferedImage.TYPE_INT_ARGB);
		}
		else if (format == GL.GL_RGB) {
			while (pixels.hasRemaining()) {
				// Do manual RGBA <> ARGB conversion
				byte R = pixels.get();
				byte G = pixels.get();
				byte B = pixels.get();
//				int bint = ByteUtility.bytesToInt(A, R, G, B, false);
				int bint = ByteUtility.bytesToInt(R, G, B, false);
//				int bint = ((A & 0xff) << 24) + ((R & 0xff) << 16) + ((G & 0xff) << 8) + ((B & 0xff));
				flatPixels[pixel++] = bint;
			}
			
			// Create a new BufferedImage from the pixeldata.
			bufferedImage = new BufferedImage(frameWidth,frameHeight, BufferedImage.TYPE_INT_RGB);
		}
		
		bufferedImage.setRGB(0, 0, frameWidth, frameHeight, flatPixels, 0, frameWidth);

		// Flip the image vertically
		AffineTransform transform = AffineTransform.getScaleInstance(1,- 1);
		transform.translate(0, -bufferedImage.getHeight(null));
		AffineTransformOp op = new AffineTransformOp(transform,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		bufferedImage = op.filter(bufferedImage, null);
		
		return bufferedImage;
	}

	@Override
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * This object is updated in place each frame.
	 * @return the dimension of the current rendering context
	 */
	public Dimension getDimension() {
		return size;
	}

	@Override
	public void attach(Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					stop(true);
				}
				else if (e.getKeyCode() == KeyEvent.VK_H) {
					toggleControlVisibility();
				}
				else if (e.getKeyCode() == KeyEvent.VK_0) {
					setScreenshotRequest();
				}
				
			}
		});
	}
	
	private int previousDividerLocation = 0; 

	public void setControlVisibility(boolean b) {
		if (!b && splitPane.getComponent(0).isVisible())
			toggleControlVisibility();
		else if (b && !splitPane.getComponent(0).isVisible())
			toggleControlVisibility();
	}
	
	private void toggleControlVisibility() {
		Component c = splitPane.getComponent(0);
		splitPane.setContinuousLayout(false);
		
		if (c.isVisible()) {
			previousDividerLocation = splitPane.getDividerLocation();
			c.setVisible(false);
			splitPane.setDividerLocation(0);
		}
		else {
			c.setVisible(true);
			splitPane.setDividerLocation(previousDividerLocation);
		}
	}
	
	public VolumeRenderer createVolumeRendererInstance() {
		return new VolumeRenderer(this);
	}
	
	public static void main(String[] args) {
		JoglRenderer jr = new JoglRenderer("Renderer test");
		
		GLObject object = new GLObject() {
			
			@Override
			public void init(GLAutoDrawable drawable) {
			}
			
			@Override
			public String getName() {
				return "Test Scene";
			}
			
			@Override
			public void display(GLAutoDrawable drawable) {
				TrilinearInterpolation.display(drawable);
			}
			
			@Override
			public void reload(GLViewerConfiguration config) {
			}
			
			@Override
			public JPanel getControls() {
				return null;
			}
		};
		
//		jr.applyStyle(LayoutStyle.CANVASDOMINANT);

		jr.setDrawFPS(true);
		jr.getRoom().setVisible(true);
		jr.getRoom().getBoxRoom().setDrawWireframe(true);
		jr.start(object);
	}

	public void addControl(String name, JPanel panel) {
		controlFrame.add(name, panel);
	}
	@Override
	public void detach(Component component) {
	}

	public void setBackgroundColor(Color white) {
		glconfig.setClearColor(white);
	}
	
	public GLViewerConfiguration getConfig() {
		return glconfig;
	}
}
