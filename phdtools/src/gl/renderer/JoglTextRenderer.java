package gl.renderer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;

import tools.TextToolset;

import com.jogamp.opengl.util.awt.TextRenderer;
import com.jogamp.opengl.util.gl2.GLUT;

public class JoglTextRenderer extends TextRenderer {

	private static Font font = null;
	private static JoglTextRenderer textRenderer = null;
	private Color renderColor = new Color(0, 0, 0, 0);
	private int lineSpacing = 0; // Inter line spacing in pixels
	private int lineHeight = 14; // Will be set by getBounds() or by application
	private final static int DRAWABLE_BORDER_OFFSET = 5;
//	private String buffer = "";
	private List<String> buffer = new LinkedList<String>();
	private List<String> buffer2 = new LinkedList<String>();
	private List<String> singleLineBuffer = new LinkedList<String>();
	
	private int alpha_timer = 255;
	private double alpha_timer_delta;
	private boolean fadingOut = false;
	private boolean fadedOut = false;


	public enum TextEffect {
		NONE, SHADOW, OUTLINE
	};

	public JoglTextRenderer() {
		this(new Font("Consolas", Font.PLAIN, 20), true, false);
	}

	public JoglTextRenderer(Font font) {
		super(font);
	}

	public JoglTextRenderer(Font font, boolean mipmap) {
		super(font, mipmap);
	}

	public JoglTextRenderer(Font font, boolean antialiased,
			boolean useFractionalMetrics) {
		super(font, antialiased, useFractionalMetrics);
	}

	public JoglTextRenderer(Font font, boolean antialiased,
			boolean useFractionalMetrics, RenderDelegate renderDelegate) {
		super(font, antialiased, useFractionalMetrics, renderDelegate);
	}

	public JoglTextRenderer(Font font, boolean antialiased,
			boolean useFractionalMetrics, RenderDelegate renderDelegate,
			boolean mipmap) {
		super(font, antialiased, useFractionalMetrics, renderDelegate, mipmap);
	}

	/**
	 * Print 3D text in the current opengl context. FIXME: a new text renderer
	 * is created at each call, making this method unscalable and resource
	 * hungry. Instead consider creating static instances in the classes calling this method.
	 * 
	 */
	static public void print3dTextLines(String text, Point3d p) {
		print3dTextLines(text, Color.WHITE, 0.005f, p.x, p.y, p.z);
	}

	/**
	 * Print 3D text in the current opengl context. FIXME: a new text renderer
	 * is created at each call, making this method unscalable and resource
	 * hungry. Instead consider creating static instances in the classes calling this method.
	 * 
	 */
	static public void print3dTextLines(String text, Point3d p, Color color) {
		print3dTextLines(text, color, 0.005f, p.x, p.y, p.z);
	}

	/**
	 * Print 3D text in the current opengl context. FIXME: a new text renderer
	 * is created at each call, making this method unscalable and resource
	 * hungry. Instead consider creating static instances in the classes calling this method.
	 * 
	 * @param text
	 * @param color
	 * @param size
	 * @param x
	 * @param y
	 * @param z
	 */
	static public void print3dTextLines(String text, Color color, float size,
			double x, double y, double z) {

//		if (textRenderer == null) {
			font = new Font("Monospaced", Font.BOLD, 100);
			textRenderer = new JoglTextRenderer(font, true, false);
//		}
		
		textRenderer.setColor(color);

		textRenderer.begin3DRendering();
		textRenderer.draw3D(text, (float) x, (float) y, (float) z, size);
		textRenderer.end3DRendering();

		textRenderer.dispose();
	}

	/**
	 * Draws multi-line text.
	 * 
	 * @param drawable
	 * @param text
	 *            Text lines to draw, delimited by '\n'.
	 */
	static public void printTextLines(GLAutoDrawable drawable, String text) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glColor3f(1, 1, 1);
		printTextLines(drawable, text, 10, 10, 12, GLUT.BITMAP_HELVETICA_10);
	}

	/**
	 * Draws multi-line text.
	 * 
	 * @param drawable
	 * @param text
	 *            Text lines to draw, delimited by '\n'.
	 * @param x
	 *            The starting x raster position.
	 * @param y
	 *            The starting y raster position.
	 * @param color
	 * @param font
	 *            The font to use (e.g. GLUT.BITMAP_HELVETICA_10).
	 */
	static public void printTextLines(GLAutoDrawable drawable, String text,
			double x, double y, float[] color, int font) {
		GL2 gl = drawable.getGL().getGL2();

		JoglRenderer.beginOverlay(drawable);

		gl.glColor3f(color[0], color[1], color[2]);
		printTextLines(drawable, text, x, y, 12, font);

		JoglRenderer.endOverlay(drawable);

	}

	/**
	 * Draws text in 2D, as an overlay.
	 * 
	 * @param drawable
	 * @param text
	 *            The String to draw.
	 * @param x
	 *            The starting x raster position.
	 * @param y
	 *            The starting y raster position.
	 * @param h
	 *            The height of each line of text.
	 * @param font
	 *            The font to use (e.g. GLUT.BITMAP_HELVETICA_10).
	 */
	static public void printTextLines(GLAutoDrawable drawable, String text,
			double x, double y, double h, int font) {
		GL2 gl = drawable.getGL().getGL2();
		JoglRenderer.beginOverlay(drawable);

		StringTokenizer st = new StringTokenizer(text, "\n");
		int line = 0;
		while (st.hasMoreTokens()) {
			String tok = st.nextToken();
			gl.glRasterPos2d(x, y + line * h);
			JoglRenderer.glut.glutBitmapString(font, tok);
			line++;
		}

		JoglRenderer.endOverlay(drawable);
	}

	/**
	 * Returns the bounding rectangle for a NON multi-line string. 
	 * Note that the X
	 * component of the rectangle is the number of lines (1) and
	 * the Y component of the rectangle is the max line height encountered. Note
	 * too that this method will automatically set the current line height to
	 * the max height found.
	 * 
	 * @param text
	 *            the multi-line text to evaluate.
	 * @return the bounding rectangle for the string.
	 */
	@Override
	public Rectangle getBounds(String text) {
		
		// FIXME: Hack around a bug in the TextRenderer getBounds() method
		// that trims white spaces
		text = text.replace(' ', '_');
		
		int width = 0;
		int maxLineHeight = 0;
			
		Rectangle2D lineBounds = super.getBounds(text);
		width = (int) Math.max(lineBounds.getWidth(), width);
		maxLineHeight = (int) Math.max(lineBounds.getHeight(), lineHeight);
			
		// Make sure we have the highest line height
		maxLineHeight = (int) Math.max(getMaxLineHeight(), maxLineHeight);
		// Set current line height for future draw
		this.lineHeight = maxLineHeight;
		// Compute final height using maxLineHeight and number of lines
		return new Rectangle(1, lineHeight, width, 1);
	}

	public Rectangle getBounds(List<String> text) {
		int width = 0;
		int maxLineHeight = 0;
		for (String line : text) {
			// FIXME: Hack around a bug in the TextRenderer getBounds() method
			// that trims white spaces
			line.replace(' ', '_');
			Rectangle2D lineBounds = super.getBounds(line);
			width = (int) Math.max(lineBounds.getWidth(), width);
			maxLineHeight = (int) Math.max(lineBounds.getHeight(), lineHeight);
		}
		
		// Make sure we have the highest line height
		maxLineHeight = (int) Math.max(getMaxLineHeight(), maxLineHeight);
		// Set current line height for future draw
		this.lineHeight = maxLineHeight;
		// Compute final height using maxLineHeight and number of lines
		return new Rectangle(text.size(), lineHeight, width, text.size()
				* maxLineHeight + (text.size() - 1) * this.lineSpacing);
	}
	/**
	 * Get the maximum line height for the given text renderer.
	 * 
	 * @param tr
	 *            the TextRenderer.
	 * @return the maximum line height.
	 */
	public double getMaxLineHeight() {
		// Check underscore + capital E with acute accent
		return super.getBounds("_\u00c9").getHeight();
	}

	// @Override
	// public void draw(String s, int x, int y) {
	// Rectangle bounds = getBounds(s);
	// }

	@Override
	public void setColor(float r, float g, float b, float a) {
		renderColor = new Color(r, g, b, a);
		super.setColor(renderColor);
	}

	@Override
	public void setColor(Color color) {
		renderColor = new Color(color.getRed(), color.getGreen(), color
				.getBlue(), color.getAlpha());
		super.setColor(renderColor);
	}


	public void draw(String text, GLAutoDrawable drawable, int x, int y) {
		singleLineBuffer.clear();
		singleLineBuffer.add(text);
		draw(singleLineBuffer, drawable, x, y, false, TextEffect.NONE);
	}


	/**
	 * Draw this text into the GLAutoDrawable.
	 * 
	 * @param text
	 * @param drawable
	 * @param x
	 * @param y
	 */
	public void draw(List<String> text, GLAutoDrawable drawable, int x, int y) {
		draw(text, drawable, x, y, false, TextEffect.NONE);
	}

	private void drawEffect(String text, int x, int y, TextEffect effect) {
		if (effect == TextEffect.OUTLINE) {
			super.draw(text, x, y + 1);
			super.draw(text, x + 1, y);
			super.draw(text, x, y - 1);
			super.draw(text, x - 1, y);
		} else if (effect == TextEffect.SHADOW) {
			super.setColor(Color.DARK_GRAY);
			super.draw(text, x + 1, y - 1);
			super.setColor(renderColor);
		}
	}

	public void drawTopRight(List<String> string, GLAutoDrawable drawable) {
		Rectangle sbounds = getBounds(string);

		int tx = drawable.getWidth() - sbounds.width - DRAWABLE_BORDER_OFFSET;
		int ty = drawable.getHeight() - sbounds.height - 15;
		draw(string, drawable, tx, ty);
	}
	
	public void drawTopRight(String string, GLAutoDrawable drawable) {
		singleLineBuffer.clear();
		singleLineBuffer.add(string);
		drawTopRight(singleLineBuffer, drawable);
	}


	public void drawTopLeft(List<String> string, GLAutoDrawable drawable) {
		Rectangle sbounds = getBounds(string);

		int tx = DRAWABLE_BORDER_OFFSET;
		int ty = drawable.getHeight() - sbounds.height - 15;
		draw(string, drawable, tx, ty);
	}

	public void drawTopLeft(String string, GLAutoDrawable drawable) {
		singleLineBuffer.clear();
		singleLineBuffer.add(string);
		drawTopLeft(singleLineBuffer, drawable);
	}

	public void drawBottomRight(List<String> string, GLAutoDrawable drawable) {
		Rectangle sbounds = getBounds(string);

		int tx = drawable.getWidth() - sbounds.width - DRAWABLE_BORDER_OFFSET;
		int ty = DRAWABLE_BORDER_OFFSET;
		draw(string, drawable, tx, ty);
	}

	public void drawBottomRight(String string, GLAutoDrawable drawable) {
		singleLineBuffer.clear();
		singleLineBuffer.add(string);
		drawBottomRight(singleLineBuffer, drawable);
	}

	public void drawBottomLeft(List<String> string, GLAutoDrawable drawable) {
		int tx = DRAWABLE_BORDER_OFFSET;
		int ty = DRAWABLE_BORDER_OFFSET;
		draw(string, drawable, tx, ty);
	}

	public void drawBottomLeft(String string, GLAutoDrawable drawable) {
		singleLineBuffer.clear();
		singleLineBuffer.add(string);
		drawBottomLeft(singleLineBuffer, drawable);
	}

	public void drawCenter(String string, GLAutoDrawable drawable) {
		singleLineBuffer.clear();
		singleLineBuffer.add(string);
		drawCenter(singleLineBuffer, drawable);
	}
	
	public void drawCenter(List<String> string, GLAutoDrawable drawable) {
		Rectangle sbounds = getBounds(string);

		int tx = (drawable.getWidth() - sbounds.width) / 2 + DRAWABLE_BORDER_OFFSET;
		int ty = (drawable.getHeight() - sbounds.height) / 2 + DRAWABLE_BORDER_OFFSET;
		draw(string, drawable, tx, ty);
	}

	/**
	 * Draw the text into the GLAutoDrawable.
	 * 
	 * @param x
	 * @param y
	 * @param fadeout
	 *            whether the text should be faded out, as specified by a
	 *            previous call to <code>setFadeout</code>.
	 * @param effect
	 *            a texture effect to apply, either
	 *            <code>TextEffect.OUTLINE</code> or
	 *            <code>TextEffect.SHADOW</code>.
	 */
	public void draw(List<String> text, GLAutoDrawable drawable, int x, int y,
			boolean fadeout, TextEffect effect) {
		clearBuffer();
		setText(text);
		drawBuffer(drawable, x, y, fadeout, effect);
	}

	/**
	 * Draw the text accumulated in the buffer into the GLAutoDrawable.
	 * 
	 * @param x
	 * @param y
	 */
	public void drawBuffer(GLAutoDrawable drawable, int x, int y) {
		drawBuffer(drawable, x, y, false, TextEffect.NONE);
	}

	/**
	 * Draw the text accumulated in the buffer into the GLAutoDrawable.
	 * 
	 * @param x
	 * @param y
	 * @param effect
	 *            a texture effect to apply, either
	 *            <code>TextEffect.OUTLINE</code> or
	 *            <code>TextEffect.SHADOW</code>.
	 */
	public void drawBuffer(GLAutoDrawable drawable, int x, int y,
			TextEffect effect) {
		drawBuffer(drawable, x, y, false, effect);
	}

	/**
	 * Draw the text accumulated in the buffer into the GLAutoDrawable.
	 * 
	 * @param x
	 * @param y
	 * @param fadeout
	 *            whether the text should be faded out, as specified by a
	 *            previous call to <code>setFadeout</code>.
	 * @param effect
	 *            a texture effect to apply, either
	 *            <code>TextEffect.OUTLINE</code> or
	 *            <code>TextEffect.SHADOW</code>.
	 */
	public void drawBuffer(GLAutoDrawable drawable, int x, int y,
			boolean fadeout, TextEffect effect) {
		Rectangle bounds = getBounds(buffer);

		// Count the number of lines
		int n = buffer.size();
		double h = ((double) bounds.height) / (n);

		int a = renderColor.getAlpha();
		if (fadingOut)
			setColor(new Color(renderColor.getRed(), renderColor.getGreen(),
					renderColor.getBlue(), alpha_timer));

		super.beginRendering(drawable.getWidth(), drawable.getHeight());

		int line = 0;
		for (String text : buffer) {
			super.draw(text, x, (int) (y + (n - line - 1) * h));

			if (effect != TextEffect.NONE)
				drawEffect(text, x, (int) (y + line * h), effect);

			line++;
		}
		
		super.endRendering();

		super.flush();

		clearBuffer();

		// Reset transparency for future draw calls
		if (fadingOut)
			setColor(new Color(renderColor.getRed(), renderColor.getGreen(),
					renderColor.getBlue(), a));

	}

	public void clearBuffer() {
		buffer.clear();
	}

	public void addLine(String s) {
		buffer.add(s);
	}

	/**
	 * Draw the text accumulated in the buffer into the GLAutoDrawable with a
	 * special parsing schema: lines contain a field, a separator for
	 * tokenization, and a value. Booleans are parsed using a different color
	 * (true = blue, false = red).
	 * 
	 * @param x
	 * @param y
	 * @param fadeout
	 *            whether the text should be faded out, as specified by a
	 *            previous call to <code>setFadeout</code>.
	 * @param effect
	 *            a texture effect to apply, either
	 *            <code>TextEffect.OUTLINE</code> or
	 *            <code>TextEffect.SHADOW</code>.
	 */
	public void drawBufferWithFieldParsing(GLAutoDrawable drawable, int x,
			int y, boolean fadeout, String separator) {
		Rectangle bounds = getBounds(buffer);

		// Count the number of lines
		int n = buffer.size();
		double h = ((double) bounds.height) / (n + 1);

		if (fadingOut)
			setColor(new Color(renderColor.getRed(), renderColor.getGreen(),
					renderColor.getBlue(), Math.min(alpha_timer, 255)));

		if (fadedOut)
			setColor(new Color(renderColor.getRed(), renderColor.getGreen(),
					renderColor.getBlue(), 0));

		super.beginRendering(drawable.getWidth(), drawable.getHeight());

		int lineIndex = 0;
		Color fieldColor = new Color(renderColor.getRed(), renderColor
				.getGreen(), renderColor.getBlue(), renderColor.getAlpha());
		int mag = 200;
		Color valueColor = new Color(mag, mag, mag, renderColor.getAlpha());
		Color sepColor = new Color(255, 255, 255, renderColor.getAlpha());

		Color falseColor = new Color(255, 0, 100, renderColor.getAlpha());
		Color trueColor = new Color(0, 100, 255, renderColor.getAlpha());

		formatFieldParsing(separator);
		
		for (String line : buffer) {
			int separatorIndex = line.indexOf(separator);
			
			if (separatorIndex == -1) {
				setColor(sepColor);
				super.draw(line, x, (int) (y + lineIndex * h));
			}
			else {
				String field = line.substring(0, separatorIndex);
				String value = line.substring(separatorIndex + 1);

				// Draw field
					setColor(fieldColor);
					super.draw(field, x, (int) (y + lineIndex * h));

					// Draw separator
					setColor(sepColor);
					super.draw(TextToolset.fillSpace("", field.length()) + separator, x + 1, (int) (y + lineIndex * h));
					
					setColor(sepColor);

					if (value.contains("false"))
						setColor(falseColor);
					else if (value.contains("true"))
						setColor(trueColor);
					else
						setColor(valueColor);
					
					super.draw(" " + value, x + getBounds(field + separator).width, (int) (y + lineIndex * h));
				}
			
			lineIndex++;
		}

		setColor(fieldColor);

		super.endRendering();

		super.flush();

		clearBuffer();

	}

	/**
	 * Properly format the current buffer using separator as delimiter for field
	 * & value.
	 * 
	 * @param separator
	 */
	public void formatFieldParsing(String separator) {
		// First count length of longest field
		int maxFieldLength = TextToolset.getMaxFieldLengthFromMultilineString(buffer, separator);
//		 System.out.println("max length = " + maxFieldLength);

		// Adjust each field by adding empty spaces if necessary
		for (String line : buffer) { 

			// Parse the line using the separator
			StringTokenizer stsep = new StringTokenizer(line, separator);

			// Only parse lines that have the form: FIELD SEPARATOR VALUE
			if (stsep.countTokens() < 2) {
				buffer2.add(line);
				continue;
			}

			int sepindex = 0;
			String newline = "";
			while (stsep.hasMoreTokens()) {
				String septoken = stsep.nextToken();

				// Trim white spaces
				septoken = septoken.trim();
				
				// Field
				if (sepindex == 0) {
					// Adjust field and add
					newline += TextToolset.fillSpace(septoken, maxFieldLength)
							+ separator;
				} else {
					// Add rest
					newline += septoken;
				}

				sepindex++;
			}
			buffer2.add(newline);
		}

		// Copy current buffer
		buffer2.clear();
		buffer.addAll(buffer2);
	}

	public void setFadeout(int startDelay_ms, final int fadeDuration_ms) {

		fadingOut = true;
		fadedOut = false;

		TimerTask delayedFadeoutTask = new TimerTask() {
			@Override
			public void run() {
				alpha_timer = 255;
				int chunks = 100;
//				fadingOut = true;

				alpha_timer_delta = 255.0 / chunks;

				TimerTask fadeoutTask = new TimerTask() {
					@Override
					public void run() {
						alpha_timer -= alpha_timer_delta;

						if (alpha_timer < 0 || fadingOut == false) {
							alpha_timer = 0;
							fadingOut = false;
							fadedOut = true;
							super.cancel();
						}
					}
				};

				// Launch the fadeout timer
				// fadeoutTimer.scheduleAtFixedRate(fadeoutTask, 0, duration_ms
				// / chunks);
				new Timer(true).scheduleAtFixedRate(fadeoutTask, 0, fadeDuration_ms
						/ chunks);
			}
		};

		// delayedFadeoutTimer.schedule(delayedFadeoutTask, delay_ms);
		new Timer(true).schedule(delayedFadeoutTask, startDelay_ms);
	}

	public void setText(List<String> lines) {
		buffer.clear();
		buffer.addAll(lines);
	}
	
	public boolean hasFadedOut() {
		return fadedOut;
	}

	public boolean isFadingOut() {
		return fadingOut;
	}

}
