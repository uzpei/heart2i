package gl.renderer;


import gl.math.FlatMatrix4d;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.DoubleParameter;


/**
 * Implementation of a simple trackball and camera
 * 
 * @author kry (modified by epiuze)
 */
public class TrackBallCamera implements Interactor, MouseListener, MouseMotionListener, MouseWheelListener, KeyListener {

	private final static float KEYBOARD_PAN_FACTOR = 16f;
	
    private static boolean enabled = true;
    
    private boolean verbose = false;
    
    private Component trackingSource;
     
    private DoubleParameter dollyRate;
    
    /**
     * This is actually a horizontal or vertical dolly, rather than a pan, which would
     * be a rotation at the camera.  
     */
    public DoubleParameter panRate;
    
    /**
     * Advancing is the same as dollying in and out
     */
    private DoubleParameter advanceRate;
    
    /**
     * The computed angle of rotation will be multipled by this value before
     * applying the rotation.  Values larger than one are useful for getting
     * useful amounts of rotation without requiring lots of mouse movement.
     */
    private DoubleParameter trackballGain;
    
    /**
     * The fit parameter describes how big the ball is relative to the smallest 
     * screen dimension.  With a square window and fit of 2, the ball will fit
     * just touch the edges of the screen.  Values less than 2 will give a ball
     * larger than the window while smaller values will give a ball contained 
     * entirely inside. 
     */
    private DoubleParameter trackballFit;
        
    /** 
     * previous trackball vector 
     */
    private Vector3f trackball_v0 = new Vector3f();

    /** 
     * current trackball vector 
     */
    private Vector3f trackball_v1 = new Vector3f();

    /**
     * previous trackball vector, Not Projected onto the sphere
     */
    private Vector3f trackball_v0np = new Vector3f();

    /**
     * current trackball vector, Not Projected onto the sphere
     */
    private Vector3f trackball_v1np = new Vector3f();
        
    /**
     * Flags of what actions are currently happening.
     */
    private int currentAction;
    
    /**
     * Our current transformation (always baked, it's smackball after all)
     */
    private Matrix4d bakedTransformation = new Matrix4d();
    
    /**
     * A flat matrix for passing to opengl, backed by our transformation matrix
     */
    private FlatMatrix4d transformation = new FlatMatrix4d(bakedTransformation);
            
    /**
     * Create a new trackball with the default settings
     */
    public TrackBallCamera() {
        bakedTransformation.setIdentity();
        createTrackBallParameters();
        createCameraParameters();
    }
    
    public void saveCurrentProjection(GL2 gl) {
    	
    }
    
    /**
     * Attach this trackball to the given component.<p>
     * This also removes us from any previous component to which we were 
     * attached.
     * @param component 
     */
    @Override
	public void attach(Component component) {
        if ( trackingSource != null ) {
            trackingSource.removeMouseListener(this);
            trackingSource.removeMouseMotionListener(this);
        }
        trackingSource = component;
        trackingSource.addMouseListener(this);
        trackingSource.addMouseMotionListener(this);
        trackingSource.addMouseWheelListener(this);
        
        trackingSource.addKeyListener(this);
    }

    private static final int ROTATE_BIT = 1;
    
    private static final int TWIST_BIT = 2;
    
    private static final int DOLLY_BIT = 4;
    
    private static final int PAN_BIT = 8;
    
    private static final int ADVANCE_BIT = 16;

    private Vector3f tmpVector3f = new Vector3f();

    private Vector3d tmpVector3d = new Vector3d();
    
    private Matrix4d tmpMatrix4d = new Matrix4d();
        
    private static final Vector3d Z_AXIS = new Vector3d(0, 0, 1);
      
    private AxisAngle4d tumble = new AxisAngle4d();
    
    /**
     * Compute the current_rotation matrix given two trackball vectors and 
     * a scale factor for the angle.
     * @param v0
     * @param v1
     * @param factor
     */
    private void applyRotation( Vector3f v0, Vector3f v1, float factor ) {
        tmpVector3f.cross( v0, v1 );        
        float alpha = factor * v0.angle( v1 );
        if ( v0.dot( v1 ) < 0 ) {
            alpha += Math.PI / 2.0f;
        }
        alpha *= trackballGain.getValue();
        tumble.set( tmpVector3f.x, tmpVector3f.y, tmpVector3f.z, alpha );
        tmpMatrix4d.set( tumble );     
        tmpMatrix4d.mul( bakedTransformation );
        bakedTransformation.set( tmpMatrix4d );
    }
    
    /** 
     * Compute a twist about the center of the screen given the previous
     * and current np trackball vectors. 
     * @param v0
     * @param v1
     * @param factor
     */
    private void applyTwist( Vector3f v0, Vector3f v1, float factor ) {        
        double delta = Math.atan2( v1.y, v1.x ) - Math.atan2( v0.y, v0.x );
        tumble.set( Z_AXIS, delta * factor );
        tmpMatrix4d.set(tumble);
        // accumulate
        tmpMatrix4d.mul( bakedTransformation );
        bakedTransformation.set( tmpMatrix4d );
    }
    
    private void applyFocalPointTranslation( Vector3d trans ) {
        double focalX = focalPointX.getValue() + trans.x;
        double focalY = focalPointY.getValue() + trans.y;
        double focalZ = focalPointZ.getValue() + trans.z;            
        focalPointX.setValue(focalX);
        focalPointY.setValue(focalY);
        focalPointZ.setValue(focalZ);
        
        if (verbose) System.out.println("Focal point: " + focalX + ", " + focalY + ", " + focalZ);
    }

    /**
     * Set the focal point of this camera manually.
     * @param point
     */
    public void setFocalPoint(Point3d point) {
        focalPointX.setValue(point.x);
        focalPointY.setValue(point.y);
        focalPointZ.setValue(point.z);
        
        if (verbose) System.out.println("Focal point: " + focalPointX.getValue() + ", " + focalPointY.getValue() + ", " + focalPointZ.getValue());
    }

    private void applyDolly( Vector3f v0, Vector3f v1, float factor ) {
        float deltaY = (v1.y - v0.y) * factor;        
        float fac = (float)Math.pow(2, deltaY * dollyRate.getValue());
        focalDistance.setValue(focalDistance.getValue() * fac);
        near.setValue(near.getValue() * fac );
        far.setValue(far.getValue() * fac );
        panRate.setValue( panRate.getValue() * fac );
        
        if (verbose) System.out.println("Focal distance = " + focalDistance.getValue());
    }

    private void applyPan( Vector3f v0, Vector3f v1, float factor ) {
        float deltaX = v0.x - v1.x;
        float deltaY = v0.y - v1.y;                 
        tmpVector3d.set(deltaX, deltaY, 0);
        tmpVector3d.scale( factor );
        transformWithTranspose( bakedTransformation, tmpVector3d );
        tmpVector3d.scale(panRate.getValue());            
        applyFocalPointTranslation( tmpVector3d );
    }
    
    private void applyAdvance( Vector3f v0, Vector3f v1, float factor ) {
        float deltaY = v0.y - v1.y;
        tmpVector3d.set(0, 0, deltaY * factor);
        transformWithTranspose( bakedTransformation, tmpVector3d );            
        tmpVector3d.scale(advanceRate.getValue());
        applyFocalPointTranslation( tmpVector3d );
    }
    
    private void doInteraction() {
        if ((currentAction & ROTATE_BIT) != 0) {
            applyRotation( trackball_v0, trackball_v1, 1 );            
        }
        if ((currentAction & TWIST_BIT) != 0) {
            applyTwist( trackball_v0np, trackball_v1np, 1 );
        }
        if ((currentAction & DOLLY_BIT) != 0) {                                    
            applyDolly( trackball_v0np, trackball_v1np, 1 );
        }
        if ((currentAction & PAN_BIT) != 0) {
            applyPan( trackball_v0np, trackball_v1np, 1 );
        }
        if ((currentAction & ADVANCE_BIT) != 0) {            
            applyAdvance( trackball_v0np, trackball_v1np, 1 );
        }        
    }
    
    /**
     * Transform a vector using the transpose of a matrix. <p>
     * t becomes m.transpose() * t <p>
     * Might this eventually want to live in vec math helpers?
     * @param m
     * @param t
     */
    static private void transformWithTranspose( Matrix4d m, Vector3d t ) {
        double x, y, z;
        x = m.m00 * t.x + m.m10 * t.y + m.m20 * t.z;
        y = m.m01 * t.x + m.m11 * t.y + m.m21 * t.z;
        z = m.m02 * t.x + m.m12 * t.y + m.m22 * t.z;
        t.set(x, y, z);
    }
    
    /**
     * Set trackball vector v, given the mouse position and the window size.
     * @param point
     * @param v
     * @param vnp
     */
    private void setTrackballVector( Point point, Vector3f v, Vector3f vnp ) {
        int width = trackingSource.getWidth();
        int height = trackingSource.getHeight();
        int mouseX = point.x;
        int mouseY = point.y;
        double trackball_scale = trackballFit.getValue() / Math.min(width, height);
        float xcen = width / 2.0f;
        float ycen = height / 2.0f;
        v.set( (mouseX - xcen), (ycen - mouseY), 0 );
        vnp.set( v );
        v.scale( (float) trackball_scale );
        float x2y2 = v.x * v.x + v.y * v.y;
        if ( x2y2 < 1.0 ) {
            v.z = (float) Math.sqrt(1.0f - x2y2);
        } else {
            v.z = 0;
            v.normalize();
        }
    }    
    
    /**
     * Decide all the actions that should be taking place based on 
     * the mouse buttons and modifiers
     * @param e
     */
    private void setAction( MouseEvent e ) {
        int modifiers = e.getModifiersEx();
        boolean ctl = (modifiers & InputEvent.CTRL_DOWN_MASK) != 0;
        boolean b1 = (modifiers & InputEvent.BUTTON1_DOWN_MASK) != 0;
        boolean b2 = (modifiers & InputEvent.BUTTON2_DOWN_MASK) != 0;
        boolean b3 = (modifiers & InputEvent.BUTTON3_DOWN_MASK) != 0;
        currentAction = 0;
        if ( ctl & b1 ) currentAction |= TWIST_BIT;
        if ( ctl & b3 ) currentAction |= ADVANCE_BIT;
        if ( !ctl & b1 ) currentAction |= ROTATE_BIT;
        if ( !ctl & b2 ) currentAction |= PAN_BIT;
        if ( !ctl & b3 ) currentAction |= DOLLY_BIT;
    }
    
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
        if (!enabled) return;
        
        currentAction = 0;
        currentAction |= DOLLY_BIT;
        
        // Start from center of screen
        Point pt = new Point(trackingSource.getSize().width / 2, trackingSource.getSize().height / 2);

//        setTrackballVector( new Point(trackingSource.getSize().width / 2, trackingSource.getSize().height / 2), trackball_v1, trackball_v1np );
        setTrackballVector( pt, trackball_v1, trackball_v1np );
//        setTrackballVector( new Point(452, 353), trackball_v1, trackball_v1np );
        trackball_v0.set( trackball_v1 );
        trackball_v0np.set( trackball_v1np );
        
        pt.y += -e.getWheelRotation() * 10;
        setTrackballVector( pt, trackball_v1, trackball_v1np );
        
        doInteraction();        
        // copy current vectors to previous vectors
      
        trackball_v0.set( trackball_v1 );
        trackball_v0np.set( trackball_v1np );
        
        // TODO: move towards current pointer location
        // FIXME: not working properly
        
        // Mimic mouse pressed at center
//        setTrackballVector( pt, trackball_v1, trackball_v1np );
//        trackball_v0.set( trackball_v1 );
//        trackball_v0np.set( trackball_v1np );
//
//        // Mimic mouse dragged towards target
//        currentAction = 0;
//        currentAction |= PAN_BIT;
//        
//        Vector2d v = new Vector2d();
//        v.x = e.getPoint().x - pt.x;
//        v.y = e.getPoint().y - pt.y;
//        v.normalize();
//        // At least 10 pixels
//        v.scale(2); 
//        pt.x = pt.x - (int) v.x;
//        pt.y = pt.y - (int) v.y;
//        
//        setTrackballVector( pt, trackball_v1, trackball_v1np );
//        doInteraction();
//        
//        System.out.println("2:" + trackball_v0np + ", " + trackball_v1np);
//
//        
//        // copy current vectors to previous vectors
//      
//        trackball_v0.set( trackball_v1 );
//        trackball_v0np.set( trackball_v1np );
	}

    @Override
	public void mousePressed(MouseEvent e) {
        if (!enabled) return;
        
        setTrackballVector( e.getPoint(), trackball_v1, trackball_v1np );
        trackball_v0.set( trackball_v1 );
        trackball_v0np.set( trackball_v1np );
        setAction(e);
    }
    
    @Override
	public void mouseDragged(MouseEvent e) {
        if (!enabled) return;

        setAction(e);        
        setTrackballVector( e.getPoint(), trackball_v1, trackball_v1np );
        doInteraction();        
        // copy current vectors to previous vectors
      
        trackball_v0.set( trackball_v1 );
        trackball_v0np.set( trackball_v1np );
    }

    @Override
	public void mouseReleased(MouseEvent e) {
        if (!enabled) return;

        // probably not necessary... but will avoid shearing if someone
        // plays with their trackball just a little too much.
        Matrix3d m = new Matrix3d();        
        bakedTransformation.getRotationScale( m );
        m.normalizeCP();
        bakedTransformation.setRotationScale( m );
    }
    
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
        if (!enabled) return;

        if (e.getKeyCode() == KeyEvent.VK_UP)
        	applyPan(new Vector3f(0, -1, 0), new Vector3f(), KEYBOARD_PAN_FACTOR);
        else if (e.getKeyCode() == KeyEvent.VK_DOWN)
        	applyPan(new Vector3f(0, 1, 0), new Vector3f(), KEYBOARD_PAN_FACTOR);
        else if (e.getKeyCode() == KeyEvent.VK_LEFT)
        	applyPan(new Vector3f(1, 0, 0), new Vector3f(), KEYBOARD_PAN_FACTOR);
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
        	applyPan(new Vector3f(-1, 0, 0), new Vector3f(), KEYBOARD_PAN_FACTOR);
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

    protected JPanel controlPanel;

    /**
     * Gets the controls for the trackball and the camera
     * @return controls
     */
    public JPanel getControls() {
        if ( controlPanel != null ) return controlPanel;
        
        VerticalFlowPanel panel = new VerticalFlowPanel();
        
        VerticalFlowPanel vfpcam = new VerticalFlowPanel();
        vfpcam.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Camera Settings"));

        vfpcam.add(near.getSliderControls());
        vfpcam.add(far.getSliderControls());
        vfpcam.add(ortho.getSliderControlsExtended(false));
        vfpcam.add(fovy.getSliderControls(false));
        vfpcam.add(magnification.getSliderControls());

        vfpcam.add(focalPointX.getSliderControls());
        vfpcam.add(focalPointY.getSliderControls());
        vfpcam.add(focalPointZ.getSliderControls());
        vfpcam.add(focalDistance.getSliderControls());
        
//        panel.add( new JLabel("TrackBall Settings") );
        VerticalFlowPanel vfptrack = new VerticalFlowPanel();
        vfptrack.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Trackball Settings"));
        
        vfptrack.add(dollyRate.getSliderControls(true));
        vfptrack.add(panRate.getSliderControls(true));
        vfptrack.add(advanceRate.getSliderControls(true));
        vfptrack.add(trackballFit.getSliderControls(false));
        vfptrack.add(trackballGain.getSliderControls(true));
        
        vfptrack.add(rx.getSliderControls());
        vfptrack.add(ry.getSliderControls());
        vfptrack.add(rz.getSliderControls());
        
        panel.add(vfpcam);
        panel.add(vfptrack);

        controlPanel = panel.getPanel();
        return controlPanel;        
    }
    
    private DoubleParameter rx = new DoubleParameter("Rx", 0, 0, 360);
    private DoubleParameter ry = new DoubleParameter("Ry", 0, 0, 360);
    private DoubleParameter rz = new DoubleParameter("Rz", 0, 0, 360);

    @Override
	public void mouseClicked(MouseEvent e) { /* do nothing */ }
    @Override
	public void mouseEntered(MouseEvent e) { /* do nothing */ }
    @Override
	public void mouseExited(MouseEvent e) { /* do nothing */ }
    @Override
	public void mouseMoved(MouseEvent e) { /* do nothing */ }
    
    // Intrinsic parameters
    protected DoubleParameter near;
    protected DoubleParameter far;
    protected DoubleParameter ortho;
    protected DoubleParameter fovy;
    protected DoubleParameter magnification;
        
    // Extrinsic parameters
    protected DoubleParameter focalPointX;
    protected DoubleParameter focalPointY;
    protected DoubleParameter focalPointZ;

    protected DoubleParameter focalDistance;
    
    protected void createCameraParameters() {
        near = new DoubleParameter("Near distance", 0.05, 0, 100);
        far = new DoubleParameter("Far distance", 10, Double.MIN_VALUE, 1000);

        ortho = new DoubleParameter("Ortho", 15, 0, 1000);

        fovy = new DoubleParameter("FoVY", 35, 0, 180);
        magnification = new DoubleParameter("Zoom", 1, Double.MIN_VALUE, 1000);

        focalPointX = new DoubleParameter("Focal point.x", 0f, -1000, 1000);
        focalPointY = new DoubleParameter("Focal point.y", 0f, -1000, 1000);
        focalPointZ = new DoubleParameter("Focal point.z", 0f, -1000, 1000);
        focalDistance = new DoubleParameter("Focal distance", 1, 0, 1000);
    }
    
    protected void createTrackBallParameters() {
        dollyRate = new DoubleParameter("Dolly rate", 0.005, 0.0001, 0.01);
        panRate = new DoubleParameter("Pan rate", 0.001, 0.0000001, 1);
        advanceRate = new DoubleParameter("Advance rate", 0.05, 0.001, 1);
        trackballGain = new DoubleParameter("trackball gain", 1.2, 0.1, 5);
        trackballFit = new DoubleParameter("trackball fit", 2.0, 0.1, 5);
    }

    private int[] viewportDimensions = new int[4];
    
    private float aspect=-1;
    
    private GLU glu = new GLU();

    private void applyProjectionTransformation(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        if (ortho.isChecked()) {
            double halfWidth = ortho.getValue() / 2;
            double halfHeight = ortho.getValue() / 2;
            gl.glOrtho(-halfWidth, halfWidth, -halfHeight, halfHeight, near.getValue(), far.getValue());
        } else {
            gl.glGetIntegerv(GL2.GL_VIEWPORT, viewportDimensions, 0);
            aspect = (viewportDimensions[2] ) / (float)(viewportDimensions[3]);
            glu.gluPerspective(fovy.getValue(), aspect, near.getValue(), far.getValue());
        }
    }

    private Matrix4d rotation = new Matrix4d();
    private Matrix4d m4d = new Matrix4d();
    private FlatMatrix4d f4d = new FlatMatrix4d();
    
    public void applyRotationTransformation(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        // Apply orientation
        transformation.getBackingMatrix().transpose();
        gl.glMultMatrixd(transformation.asArray(), 0);  
        transformation.getBackingMatrix().transpose();

        applyLocalRotation(gl);
    }

    /**
     * Set the rotation angles in degrees.
     * @param rx
     * @param ry
     * @param rz
     */
    public void setRotation(double rx, double ry, double rz) {
    	// convert/clamp to [0, 2pi]
    	rx = angleInRange(rx);
    	ry = angleInRange(ry);
    	rz = angleInRange(rz);
    	this.rx.setValue(rx);
    	this.ry.setValue(ry);
    	this.rz.setValue(rz);
    }
    
    /**
     * @param angle
     * @return the equivalent angle in the range [0, 360]
     */
    private double angleInRange(double angle) {
    	// first correct sign
    	if (angle < 0)
    		angle = 360 + angle;
    	
    	// then remove modulus
    	if (angle > 360) {
    		double decimal = angle - ((int) angle);
    		angle = (((int) angle) % 360) + decimal;
    	}
    		
    	return angle;
    }

    /**
     * Set the rotation elevation and azimuth, in degrees.
     * @param elevation
     * @param azimuth
     */
    public void setRotation(double elevation, double azimuth) {
    	setRotation(elevation, 0 ,azimuth);
    }
    
    private Matrix4d assembleRotation() {
        // Local rotation is applied in the order RxRyRz
        rotation.setIdentity();
        m4d.set(new AxisAngle4d(1, 0, 0, rx.getValue() / 180 * Math.PI));
        rotation.mul(m4d);
        m4d.set(new AxisAngle4d(0, 1, 0, ry.getValue() / 180 * Math.PI));
        rotation.mul(m4d);
        m4d.set(new AxisAngle4d(0, 0, 1, rz.getValue() / 180 * Math.PI));
        rotation.mul(m4d);

        return rotation;
    }
    
    private void applyLocalRotation(GL2 gl) {
        f4d.setBackingMatrix(assembleRotation());
        gl.glMultMatrixd(f4d.asArray(), 0);        
    }
    
    public Matrix4d getModelviewTransformationPipeline() {
    	Matrix4d transf = new Matrix4d();
    	transf.setIdentity();
    	transf.m23 += -focalDistance.getValue();
    	transf.m00 *= magnification.getValue();
    	transf.m11 *= magnification.getValue();
    	transf.m22 *= magnification.getValue();
//    	transf.mul(transformation.getBackingMatrix(), transf);
    	transf.mul(transformation.getBackingMatrix());

    	transf.m03 += -focalPointX.getValue();
    	transf.m13 += -focalPointY.getValue();
    	transf.m23 += -focalPointZ.getValue();

//    	transf.mul(assembleRotation(), transf);
    	transf.mul(assembleRotation());
    	
    	return transf;
    }
    
    public void applyViewTransformation(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        // Dolly back
        gl.glTranslated(0, 0, -focalDistance.getValue());
        // Zoom
        double zoom = magnification.getValue(); 
        gl.glScaled( zoom, zoom, zoom );
        // Apply orientation
        gl.glMultMatrixd( transformation.asArray(), 0 );        
        // Move to focal point
        gl.glTranslated(-focalPointX.getValue(), -focalPointY.getValue(), -focalPointZ.getValue());
        
        applyLocalRotation(gl);
    }

    public void applyInverseViewTransformation(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        // Move to focal point
        gl.glTranslated(focalPointX.getValue(), focalPointY.getValue(), focalPointZ.getValue());

        // Apply orientation
        transformation.getBackingMatrix().transpose();
        gl.glMultMatrixd( transformation.asArray(), 0 );        
        transformation.getBackingMatrix().transpose();

        // Zoom        
        double zoom = 1.0/magnification.getValue(); 
        gl.glScaled( zoom, zoom, zoom );
        
        // Dolly back
        gl.glTranslated(0, 0, focalDistance.getValue());
    } 

    /**
     * Sets up projection and modelview parameters.
     * @param drawable
     */
    public void prepareForDisplay(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        
        gl.glMatrixMode(GL2.GL_PROJECTION);
//        gl.glPushMatrix();
        gl.glLoadIdentity();
        applyProjectionTransformation(drawable);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
//        gl.glPushMatrix();
        gl.glLoadIdentity();
        applyViewTransformation(drawable);

    }

    /**
     * Pops projection and modelview matrices pushed on the stack by
     * the prepareForDisplay call.
     * @param drawable
     */
    public void cleanupAfterDisplay(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glMatrixMode(GL2.GL_PROJECTION);
//        gl.glPopMatrix();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
//        gl.glPopMatrix();
    }

    public static void enable(boolean b) {
        enabled = b;
    }
    
    /**
     * Manually set the dolly (zoom).
     * @param factor
     */
    public void zoom(float factor) {
        float fac = (float)Math.pow(2, -factor * dollyRate.getValue());
        focalDistance.setValue(focalDistance.getValue() * fac);
        near.setValue(near.getValue() * fac );
        far.setValue(far.getValue() * fac );
        panRate.setValue( panRate.getValue() * fac );
    }
    
    /**
     * Manually set the pan.
     * @param v
     */
    public void pan(Vector3f v) {
    	// Pan from origin
        applyPan( new Vector3f(), v, 1 );
    }

    public double getFar() {
    	return far.getValue();
    }
    
    public void setVerbose(boolean verbose) {
    	this.verbose = verbose;
    }

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

	public Vector3f getPan() {
		return new Vector3f(focalPointX.getFloatValue(), focalPointY.getFloatValue(), focalPointZ.getFloatValue());
	}

	public void setPan(Vector3f pan) {
		focalPointX.setValue(pan.x);
		focalPointY.setValue(pan.y);
		focalPointZ.setValue(pan.z);
	}
}