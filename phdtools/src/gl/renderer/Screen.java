package gl.renderer;

import java.awt.Dimension;
import java.awt.Toolkit;

public class Screen {

	private Dimension screenSize;
	
	public static Dimension getMaxSupportedSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}
	
	public Screen(Dimension size) {
		screenSize = new Dimension(size);
	}
	
	public void setSize(Dimension size) {
		screenSize.setSize(size);
	}

}
