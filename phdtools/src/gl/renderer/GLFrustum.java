package gl.renderer;

import gl.geometry.primitive.Plane;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class GLFrustum {

	public static void accFrustum(GLAutoDrawable drawable, double left, double right,
			double bottom, double top, double near, double far, double pixdx,
			double pixdy, double eyedx, double eyedy, double focus) {
		GL2 gl = drawable.getGL().getGL2();
		double xwsize, ywsize;
		double dx, dy;
		int[] viewport = new int[4];

		gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);

		xwsize = right - left;
		ywsize = top - bottom;
		dx = -(pixdx * xwsize / viewport[2] + eyedx * near / focus);
		dy = -(pixdy * ywsize / viewport[3] + eyedy * near / focus);

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glFrustum(left + dx, right + dx, bottom + dy, top + dy, near, far);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		gl.glTranslatef((float) -eyedx, (float) -eyedy, 0.0f);
	}

	public static void accPerspective(GLAutoDrawable drawable, double fovy, double aspect,
			double near, double far, double pixdx, double pixdy, double eyedx,
			double eyedy, double focus) {
		double fov2, left, right, bottom, top;
		fov2 = ((fovy * Math.PI) / 180.0) / 2.0;

		top = near / (Math.cos(fov2) / Math.sin(fov2));
		bottom = -top;
		right = top * aspect;
		left = -right;

		accFrustum(drawable, left, right, bottom, top, near, far, pixdx, pixdy,
				eyedx, eyedy, focus);
	}

	/**
	 * Extract view frustum using Gribb & Hartmann's (2001)
	 * 
	 * @return
	 */
	public static Plane getNearPlane(GL2 gl) {

		double near = getNear(getProjectionMatrix(gl));

		double[] vertices = getFrustumVertices(gl);
		double nLeft = vertices[0];
		double nRight = vertices[1];
		double nTop = vertices[2];
		double nBottom = vertices[3];

		Point3d near1 = new Point3d(nLeft, nBottom, -near);
		Point3d near2 = new Point3d(nLeft, nTop, -near);
		Point3d near3 = new Point3d(nRight, nTop, -near);
		Point3d near4 = new Point3d(nRight, nBottom, -near);

		return new Plane(near1, near2, near3, near4);
	}

	public static Plane getFarPlane(GL2 gl) {

		double far = getFar(getProjectionMatrix(gl));

		double[] vertices = getFrustumVertices(gl);
		double fLeft = vertices[4];
		double fRight = vertices[5];
		double fTop = vertices[6];
		double fBottom = vertices[7];

		Point3d far1 = new Point3d(fLeft, fBottom, -far);
		Point3d far2 = new Point3d(fLeft, fTop, -far);
		Point3d far3 = new Point3d(fRight, fTop, -far);
		Point3d far4 = new Point3d(fRight, fBottom, -far);

		return new Plane(far1, far2, far3, far4);
	}

	public static double getNear(double[] projectionMatrix) {
		return projectionMatrix[11] / (projectionMatrix[10] - 1);
	}

	public static double getFar(double[] projectionMatrix) {
		return projectionMatrix[11] / (projectionMatrix[10] + 1);
	}

	public static double getNearLeft(double[] projectionMatrix) {
		return getNear(projectionMatrix) * (projectionMatrix[2] - 1)
				/ (projectionMatrix[0]);
	}

	public static double getNearRight(double[] projectionMatrix) {
		return getNear(projectionMatrix) * (projectionMatrix[2] + 1)
				/ (projectionMatrix[0]);
	}

	public static double getNearTop(double[] projectionMatrix) {
		return getNear(projectionMatrix) * (projectionMatrix[6] + 1)
				/ (projectionMatrix[5]);
	}

	public static double getNearBottom(double[] projectionMatrix) {
		return getNear(projectionMatrix) * (projectionMatrix[6] - 1)
				/ (projectionMatrix[5]);
	}

	public static double getFarLeft(double[] projectionMatrix) {
		return getFar(projectionMatrix) * (projectionMatrix[2] - 1)
				/ (projectionMatrix[0]);
	}

	public static double getFarRight(double[] projectionMatrix) {
		return getFar(projectionMatrix) * (projectionMatrix[2] + 1)
				/ (projectionMatrix[0]);
	}

	public static double getFarTop(double[] projectionMatrix) {
		return getFar(projectionMatrix) * (projectionMatrix[6] + 1)
				/ (projectionMatrix[5]);
	}

	public static double getFarBottom(double[] projectionMatrix) {
		return getFar(projectionMatrix) * (projectionMatrix[6] - 1)
				/ (projectionMatrix[5]);
	}

	/**
	 * @return the view frustum vertices, near left, right, top bottom, far
	 *         left, right, top bottom
	 */
	public static double[] getFrustumVertices(GL2 gl) {
		double[] proj = getProjectionMatrix(gl);
		double near = getNear(proj);
		double far = getFar(proj);

		double[] verts = new double[8];

		// Get the sides of the near plane.
		verts[0] = near * (proj[2] - 1.0) / proj[0];
		verts[1] = near * (1.0 + proj[2]) / proj[0];
		verts[2] = near * (1.0 + proj[6]) / proj[5];
		verts[3] = near * (proj[6] - 1.0) / proj[5];

		// Get the sides of the far plane.
		verts[4] = far * (proj[2] - 1.0) / proj[0];
		verts[5] = far * (1.0 + proj[2]) / proj[0];
		verts[6] = far * (1.0 + proj[6]) / proj[5];
		verts[7] = far * (proj[6] - 1.0) / proj[5];

		return verts;
	}

	public static Matrix4d getProjectionMatrix4d(GL2 gl) {
		double[] projectionMatrix = new double[16];
		gl.glGetDoublev(GL2.GL_PROJECTION_MATRIX, projectionMatrix, 0);
		return new Matrix4d(projectionMatrix);
	}

	public static double[] getProjectionMatrix(GL2 gl) {
		double[] projectionMatrix = new double[16];
		gl.glGetDoublev(GL2.GL_PROJECTION_MATRIX, projectionMatrix, 0);
		return projectionMatrix;
	}

	public static Point3d getCameraPos() {
		return new Point3d();
	}

	public static Vector3d getCameraViewRay() {
		return new Vector3d();
	}

	public static void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		double near = getNear(getProjectionMatrix(gl));
		double far = getFar(getProjectionMatrix(gl));

		double[] vertices = getFrustumVertices(gl);
		double nLeft = vertices[0];
		double nRight = vertices[1];
		double nTop = vertices[2];
		double nBottom = vertices[3];

		double fLeft = vertices[4];
		double fRight = vertices[5];
		double fTop = vertices[6];
		double fBottom = vertices[7];

		Point3d near1 = new Point3d(nLeft, nBottom, -near);
		Point3d near2 = new Point3d(nLeft, nTop, -near);
		Point3d near3 = new Point3d(nRight, nTop, -near);
		Point3d near4 = new Point3d(nRight, nBottom, -near);

		new Plane(near1, near2, near3, near4).display(drawable);

		Point3d far1 = new Point3d(fLeft, fBottom, -far);
		Point3d far2 = new Point3d(fLeft, fTop, -far);
		Point3d far3 = new Point3d(fRight, fTop, -far);
		Point3d far4 = new Point3d(fRight, fBottom, -far);

		new Plane(far1, far2, far3, far4).display(drawable);

		gl.glColor3d(0, 0, 0);
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glLineWidth(1);
		gl.glBegin(GL2.GL_LINES);

		gl.glVertex3d(0.0f, 0.0f, 0.0f);
		gl.glVertex3d(fLeft, fBottom, -far);

		gl.glVertex3d(0.0f, 0.0f, 0.0f);
		gl.glVertex3d(fRight, fBottom, -far);

		gl.glVertex3d(0.0f, 0.0f, 0.0f);
		gl.glVertex3d(fRight, fTop, -far);

		gl.glVertex3d(0.0f, 0.0f, 0.0f);
		gl.glVertex3d(fLeft, fTop, -far);

		// far
		gl.glVertex3d(fLeft, fBottom, -far);
		gl.glVertex3d(fRight, fBottom, -far);

		gl.glVertex3d(fRight, fTop, -far);
		gl.glVertex3d(fLeft, fTop, -far);

		gl.glVertex3d(fRight, fTop, -far);
		gl.glVertex3d(fRight, fBottom, -far);

		gl.glVertex3d(fLeft, fTop, -far);
		gl.glVertex3d(fLeft, fBottom, -far);

		// near
		gl.glVertex3d(nLeft, nBottom, -near);
		gl.glVertex3d(nRight, nBottom, -near);

		gl.glVertex3d(nRight, nTop, -near);
		gl.glVertex3d(nLeft, nTop, -near);

		gl.glVertex3d(nLeft, nTop, -near);
		gl.glVertex3d(nLeft, nBottom, -near);

		gl.glVertex3d(nRight, nTop, -near);
		gl.glVertex3d(nRight, nBottom, -near);

		gl.glEnd();
		gl.glLineWidth(1);
	}

}
