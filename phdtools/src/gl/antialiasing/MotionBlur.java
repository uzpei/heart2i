package gl.antialiasing;

import gl.geometry.GLGeometry;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Vector3d;

public class MotionBlur {

	public static void blur(GLAutoDrawable drawable, GLGeometry object, Vector3d blurOffset, int blurSteps) {
		GL2 gl = drawable.getGL().getGL2();

		Vector3d dv = new Vector3d(blurOffset);
		dv.scale(1.0 / blurSteps);
		Vector3d v = new Vector3d();

		gl.glClear(GL2.GL_ACCUM_BUFFER_BIT);

		for (int i = 0; i < blurSteps; i++) {
			gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
			gl.glPushMatrix();
			
			v.add(dv);
			gl.glTranslated(v.x, v.y, v.z);
			object.display(drawable);
			gl.glPopMatrix();
//			gl.glAccum(GL2.GL_ACCUM, 1.0f / blurSteps);
			
			if (i == blurSteps - 1) 
				gl.glAccum(GL2.GL_ACCUM, 0.5f);
			else
				gl.glAccum(GL2.GL_ACCUM, 0.5f / (blurSteps - 1));

		}
		gl.glAccum(GL2.GL_RETURN, 1.0f);

		gl.glFlush();

	}
}
