package gl.antialiasing;

import gl.geometry.GLGeometry;

import java.awt.Dimension;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

public class GLMultisampler {

	/**
	 * Also GL_RGBA, GL_RGBA8, GL2.GL_RGBA16F_ARB, GL2.GL_RGBA32F_ARB
	 */
	private static int colorFormat = GL2.GL_RGBA16F;

	/**
	 * Also GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT32
	 */
	private static int depthFormat = GL2.GL_DEPTH_COMPONENT32;

	private boolean FSAAing = false;
	private int samples = 8;

	private int frameBufferObjectMultisample = -1;
	private int frameBufferObjectSupersample = -1;

	private boolean supersampling = false;
	private int ssFactor = 1;

	/**
	 * The dimension of the FSAA and supersampling FBOs.
	 */
	private Dimension dimensionFBO;
	
	/**
	 * If the settings for the multisampling and supersampling FBOs have changed
	 * and new FBOs should be generated.
	 */
	private boolean initRequest = false;
	
	private void setInitRequest() {
		initRequest = true;
	}
	
	/**
	 * Create a new OpenGL multisampler and supersampler. <br>
	 * <br>
	 * A frame buffer object (FBO) is created and multisampled by x
	 * <code>samplesFSAA</code>. The multisampling FBO is then blit into the
	 * main OpenGL frame buffer. <br>
	 * <br>
	 * The FSAA is limited by the specifications of the GPU so you must make
	 * sure the values selected are valid.
	 * 
	 * @param gl
	 *            the GL context into which the mutlisampling will be performed
	 * @param samplesFSAA
	 *            the number of FSAA samples
	 * @param ssFactor
	 *            the supersampling applied to the multisampled FBO
	 */
	public GLMultisampler(GL2 gl, int samplesFSAA) {
		this(gl, samplesFSAA, 1);
	}

	/**
	 * Create a new OpenGL multisampler and supersampler. <br>
	 * <br>
	 * A frame buffer object (FBO) is created and multisampled by x
	 * <code>samplesFSAA</code>. In general, it is not possible to directly
	 * downsample a multisampled FBO. Consequently, if the
	 * <code>supersamplingFactor</code> is larger than 1, a second FBO is
	 * created into which the multisampled FBO is copied (blit without
	 * downsampling) during rendering. The downsampling is performed by blitting
	 * the non-multisampled FBO into the main OpenGL frame buffer. <br>
	 * <br>
	 * The combined multisampling and supersampling is limited by the
	 * specifications of the GPU so you must make sure the values selected are
	 * valid (total multisampling ~ <code>samplesFSAA</code>x
	 * <code>supersamplingFactor</code>). In general, it is best to use a
	 * largest FSAA value and a smaller <code>supersamplingFactor</code>. If
	 * hardware FSAA is not supported by the GPU, supersampling is to be used
	 * alone, in which case factors of 2 yield best results.
	 * 
	 * @param gl
	 *            the GL context into which the mutlisampling will be performed
	 * @param samplesFSAA
	 *            the number of FSAA samples
	 * @param supersamplingFactor
	 *            the supersampling applied to the multisampled FBO
	 *            
	 * @return whether the initialization was successful
	 */
	public GLMultisampler(GL2 gl, int samplesFSAA, int supersamplingFactor) {
		initBuffering(gl, samplesFSAA, supersamplingFactor);
	}
	
	private boolean multisamplingInitialized = false;
	
	public boolean isMultisamplingEnabled() {
		return multisamplingInitialized;
	}
	
	public void initBuffering(GL2 gl, int samplesFSAA, int supersamplingFactor) {
		// Determine max FSAA samples
		int maxSamples = getGLMaxSamples(gl);
		
		/*
		 * Apply settings for FSAA and supersampling.
		 */
		FSAAing = samplesFSAA > 1 && maxSamples > 0;
		this.samples = samplesFSAA;

		supersampling = supersamplingFactor > 1;
		
		if (!supersampling) 
			ssFactor = 1;
		else {
			ssFactor = supersamplingFactor;
		}
		/*
		 * Verify capabilities
		 */
		// Make sure the frame buffer has a valid status
		if (!glExtCheckFramebufferStatus(gl)) {
			System.err.println("FBO status error before initialization.");
			initRequest = false;
			multisamplingInitialized = false;
		}

		if (maxSamples == 0) {
			System.err
					.println("FBO FSAA does not seem to be compatible with this hardware.");
			
			// TODO: If the requested FSAA was larger than one, try to use supersampling
			// TODO: need to find a way to determine if the SS is also compatible... (e.g. on Mac in the lab it doesn't work)
			initRequest = false;
			multisamplingInitialized = false;
			
			if (!supersampling) return;
		} 
		else if (!FSAAing) {
			System.out.print("FSAA disabled +");
		}
		else {
			System.out.print("FSAA with FBO multisampling: " + samplesFSAA
					+ " samples used out of " + maxSamples
					+ " samples supported +");
		}

		// TODO: determine max supersampling
		if (supersampling) {
			System.out
					.println(" supersampling (x"
							+ ssFactor
							+ ") the multisampled FBO and downsampling to standard frame buffer.");
		}
		else {
			System.out.println(" NOT using supersampling.");
		}

		// Size of the AA
		int[] cwind = new int[4];
		gl.glGetIntegerv(GL2.GL_SCISSOR_BOX, cwind, 0);
		dimensionFBO = new Dimension(ssFactor * cwind[2],
				ssFactor * cwind[3]);

		/*
		 * Enable multisampling
		 */
		if (FSAAing)
			gl.glEnable(GL2.GL_MULTISAMPLE);

		
		/*
		 * FSAA FBO
		 */
		if (FSAAing) {
			
			// Generate a frame buffer object and bind it
			frameBufferObjectMultisample = generateBindFBO(gl);
			System.out.println("Created multisampling FBO with id = " + frameBufferObjectMultisample);

			// Generate multisample color and depth render buffers
			int colorId = genRB(gl, dimensionFBO, samplesFSAA, colorFormat);
			int depthId = genRB(gl, dimensionFBO, samplesFSAA, depthFormat);

			// Attach the render buffers to the frame buffer
			gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER,
					GL2.GL_COLOR_ATTACHMENT0, GL2.GL_RENDERBUFFER, colorId);
			gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER,
					GL2.GL_DEPTH_ATTACHMENT, GL2.GL_RENDERBUFFER, depthId);
			
			System.out.println("FSAA FBO initialized.");
		}

		/*
		 * Supersampling FBO
		 */
		if (supersampling) {
			// Generate a frame buffer object
			frameBufferObjectSupersample = generateBindFBO(gl);
			System.out.println("Created supersampling FBO with id = " + frameBufferObjectSupersample);

			// Generate multisample color and depth render buffers
			int colorId = genRB(gl, dimensionFBO, 0, colorFormat);
			int depthId = genRB(gl, dimensionFBO, 0, depthFormat);

			// Attach the render buffers to the frame buffer
			gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER,
					GL2.GL_COLOR_ATTACHMENT0, GL2.GL_RENDERBUFFER, colorId);
			gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER,
					GL2.GL_DEPTH_ATTACHMENT, GL2.GL_RENDERBUFFER, depthId);

			System.out.println("Supersampling FBO initialized.");
		}

		glExtCheckFramebufferStatus(gl);
		
		initRequest = false;
		
		System.out.println("GL multisamper initialized.");
		
		multisamplingInitialized = true;
	}

	/**
	 * Create a frame buffer object (FBO) and bind it.
	 * 
	 * @param gl
	 * @return the id corresponding to the frame buffer created
	 */
	private static int generateBindFBO(GL gl) {
		int[] fboId = new int[1];
		gl.glGenFramebuffers(1, fboId, 0);
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, fboId[0]);

		return fboId[0];
	}

	/**
	 * Generate a multisample render buffer (RB)
	 * 
	 * @param gl
	 * @param samples
	 * @param dimension
	 * @param format
	 *            the format of the render buffer (e.g. GL_DEPTH_COMPONENT,
	 *            GL_RGB8, ...)
	 * @return the id corresponding to the render buffer created
	 */
	private static int genRB(GL2 gl, Dimension dimension,
			int samples, int format) {
		int[] bufferId = new int[1];

		gl.glGenRenderbuffers(1, bufferId, 0);
		gl.glBindRenderbuffer(GL2.GL_RENDERBUFFER, bufferId[0]);
		
		if (samples > 0) {
			gl.glRenderbufferStorageMultisample(GL2.GL_RENDERBUFFER, samples,
					format, dimension.width, dimension.height);
		}
		else {
			gl.glRenderbufferStorage(GL2.GL_RENDERBUFFER,
					format, dimension.width, dimension.height);
		}

		return bufferId[0];
	}

	/**
	 * @param gl
	 * @return the maximum number of supported FSAA samples on the graphics card
	 *         in use
	 */
	public static int getGLMaxSamples(GL gl) {
		int[] samples = new int[1];
		gl.glGetIntegerv(GL2.GL_MAX_SAMPLES, samples, 0);

		return samples[0];
	}

	private static boolean glExtCheckFramebufferStatus(GL gl) {
		int status = gl.glCheckFramebufferStatus(GL2.GL_FRAMEBUFFER);
		int code = 0;
		String msg = "";

		switch (status) {
		case GL2.GL_FRAMEBUFFER_COMPLETE:
			break;
		case GL2.GL_FRAMEBUFFER_UNSUPPORTED:
			// Choose different formats
			msg = "Framebuffer object format is unsupported by the video hardware. (GL2.GL_FRAMEBUFFER_UNSUPPORTED)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			msg = "Incomplete attachment. (GL2.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			msg = "Incomplete missing attachment. (GL2.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			msg = "Incomplete dimensions. (GL2.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			msg = "Incomplete formats. (GL2.GL_FRAMEBUFFER_INCOMPLETE_FORMATS)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			msg = "Incomplete draw buffer. (GL2.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			msg = "Incomplete read buffer. (GL2.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER)(FBO - 820)";
			code = -1;
			break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			msg = "Incomplete multisample buffer. (GL2.GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE)(FBO - 820)";
			code = -1;
			break;
		default:
			// Programming error; will fail on all hardware
			msg = "Some video driver error or programming error occured. Framebuffer object status is invalid. (FBO - 823)";
			code = -2;
		}

		if (code != 0)
			System.err.println(msg);

		return code == 0;
	}

	/**
	 * Draw this geometry into the multisample frame buffer object passed as a
	 * argument. *Note that the multisample frame buffer must have been
	 * previously generated using glGenFramebuffers and the proper ID must be
	 * used.
	 * 
	 * @param drawable
	 * @param frameBufferID
	 * @param geometry
	 */
	public void apply(GLAutoDrawable drawable, GLGeometry geometry) {
		GL2 gl = drawable.getGL().getGL2();

		if (initRequest) {
			initBuffering(gl, samples, ssFactor);
		}

		if (!FSAAing && !supersampling) {
			geometry.display(drawable);
			return;
		}
		
		// Reuse the same clear color
		float[] bkColor = new float[4];
		gl.glGetFloatv(GL2.GL_COLOR_CLEAR_VALUE, bkColor, 0);
		
		// Get the main buffer dimension
		// FIXME: getting from the OpenGL server has a considerable overhead... 
		int[] cwind = new int[4];
		gl.glGetIntegerv(GL2.GL_SCISSOR_BOX, cwind, 0);
//		System.out.println(Arrays.toString(cwind));
		Dimension wdim = new Dimension(cwind[2], cwind[3]);

		// TODO: verify if the size of the canvas has changed, in which case we need to 
		// reinitialize the FBOs

		/*
		 * Determine into which buffer we draw the geometry
		 */
		if (FSAAing) {
			// Bind and clear the multisampling FBO
			// then draw the scene into the multisampling FBO
			gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER,
					frameBufferObjectMultisample);
		}
		else if (supersampling) {
			// Bind and clear the multisampling FBO
			// then draw the scene into the supersampling FBO
			gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER,
					frameBufferObjectSupersample);
		}
		else {
			gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER,
					0);
		}

		gl.glClearColor(bkColor[0], bkColor[1], bkColor[2], bkColor[3]);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		/*
		 * Draw the geometry. Scale the viewport if we are supersampling.
		 */
		if (supersampling) {
			gl.glPushAttrib(GL2.GL_VIEWPORT_BIT);
			gl.glViewport(0, 0, dimensionFBO.width, dimensionFBO.height);
			geometry.display(drawable);
			gl.glPopAttrib();
		}
		else {
			geometry.display(drawable);
		}
		
		if (FSAAing && supersampling) {
			// Blit the multisampling FBO into the supersampling FBO
			gl.glBindFramebuffer(GL2.GL_READ_FRAMEBUFFER,
					frameBufferObjectMultisample);
			gl.glBindFramebuffer(GL2.GL_DRAW_FRAMEBUFFER,
					frameBufferObjectSupersample);
			gl.glBlitFramebuffer(0, 0, dimensionFBO.width, dimensionFBO.height,
					0, 0, dimensionFBO.width, dimensionFBO.height,
					GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT, GL2.GL_NEAREST);
			
			// Blit the supersampling FBO into the main buffer
			gl.glBindFramebuffer(GL2.GL_READ_FRAMEBUFFER,
					frameBufferObjectSupersample);
			gl.glBindFramebuffer(GL2.GL_DRAW_FRAMEBUFFER, 0);
			gl.glBlitFramebuffer(0, 0, dimensionFBO.width, dimensionFBO.height,
					0, 0, wdim.width, wdim.height,
					GL2.GL_COLOR_BUFFER_BIT, GL2.GL_LINEAR);
		}
		else if (FSAAing && !supersampling) {
			// Blit the multisampling FBO directly into the main buffer
			// NOTE: the multisampling FBO *should* have the same same as the main buffer.
			gl.glBindFramebuffer(GL2.GL_READ_FRAMEBUFFER,
					frameBufferObjectMultisample);
			gl.glBindFramebuffer(GL2.GL_DRAW_FRAMEBUFFER, 0);
			gl.glBlitFramebuffer(0, 0, dimensionFBO.width, dimensionFBO.height,
					0, 0, wdim.width, wdim.height,
//					GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT, GL2.GL_LINEAR);
					GL2.GL_COLOR_BUFFER_BIT, GL2.GL_LINEAR);
		}
		else if (!FSAAing && supersampling) {
			// Blit the supersampling FBO directly into the main buffer
			gl.glBindFramebuffer(GL2.GL_READ_FRAMEBUFFER,
					frameBufferObjectSupersample);
			gl.glBindFramebuffer(GL2.GL_DRAW_FRAMEBUFFER, 0);
			gl.glBlitFramebuffer(0, 0, dimensionFBO.width, dimensionFBO.height,
					0, 0, wdim.width, wdim.height,
					GL2.GL_COLOR_BUFFER_BIT, GL2.GL_LINEAR);
		}
		else if (!FSAAing && !supersampling){
			// Do nothing, we are drawing already in the main buffer
		}

		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);

	}
	
	public void setFSAA(int samples) {
		this.samples = samples;
		
		setInitRequest();
	}
}
