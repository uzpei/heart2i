package gl.antialiasing;

import gl.geometry.primitive.Plane;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector4d;

public class SceneJitter3D {
	/**
	 * @param n
	 *            the number of jittered points.
	 * @param size
	 *            the size of one side of the sampling grid.
	 * @return a stratified jittered sampling consisting of n = (m * m) points
	 *         in a grid of size <code>size</code>.
	 */
	public static Point3d[] jitter_stratified(int n, double size, Plane plane) {
		Point3d[] jitter = new Point3d[n * n];

		// Determine grid dimensions
		double step = size / (n - 1);

		for (int p = 0; p < n * n; p++) {
			int i = p / n;
			int j = p % n;

			Point3d pt = new Point3d();
			pt.add(plane.getCorner());
//			pt.add(p1);
			pt.scaleAdd(i * step, plane.getS(), pt);
			pt.scaleAdd(j * step, plane.getT(), pt);
			jitter[p] = pt;
		}

		return jitter;
	}

	/**
	 * @param n
	 *            the number of jittered points.
	 * @param size
	 *            the size of one side of the sampling grid.
	 * @return a randomized stratified jittered sampling consisting of n = (m *
	 *         m) points in a grid of size <code>size</code>.
	 */
	public static Point3d[] jitter(int n, double size, Plane plane) {
		Point3d[] jitter = new Point3d[n * n];

		// Determine grid dimensions
		double step = size / (n - 1);
		Random rand = new Random();
		double rmag = 0.5 * step;
		double rmag2 = rmag / 2;
		double ri, rj;
		int i, j;

		for (int p = 0; p < n * n; p++) {
			i = p / n;
			j = p % n;

			ri = -rmag2 + rmag * rand.nextDouble();
			rj = -rmag2 + rmag * rand.nextDouble();

			Point3d pt = new Point3d();
			pt.add(plane.getCorner());
			pt.scaleAdd(i * step + ri, plane.getS(), pt);
			pt.scaleAdd(j * step + rj, plane.getT(), pt);
			jitter[p] = pt;
		}

		return jitter;
	}

	/**
	 * Poisson disc sampling in 2D (n=2) using Bridson's Fast Poisson Disk
	 * Sampling in Arbitrary Dimensions.
	 */
	public static Point3d[] jitter_poisson(double r, Plane plane) {

		int k = 30;
		int n = 2;

		// Cell size (for accelerating search... to implement later)
		double csize = r / Math.sqrt(n);

		// Domain size
		double xs = plane.getS().length();
		double ys = plane.getT().length();
		double size = Math.min(xs, ys) / 2;

		// Initialize active and background lists
		List<Integer> activeList = new ArrayList<Integer>();
		List<Point2d> backgroundList = new ArrayList<Point2d>();

		// Center of region
		Point2d c = new Point2d(0, 0);
		
		// Initial sample x0
		Random rand = new Random();
//		Point2d x0 = new Point2d(rand.nextDouble() * xs, rand.nextDouble() * ys);
		activeList.add(0);
		backgroundList.add(c);

		int maxits = 100000;
		int it = 0;
		while (activeList.size() > 0 && it < maxits) {
			boolean found = false;

			// Select random index in active list
			int i = rand.nextInt(activeList.size());
			Point2d xi = backgroundList.get(activeList.get(i));

			double theta = 0;
			for (int j = 0; j < k; j++) {
				// Generate point in spherical annulus between radius r and 2r
				// around xi
				Point2d p = new Point2d();
				double ar = r * (1 + rand.nextDouble());

				p.x = xi.x + ar * Math.cos(theta);
				p.y = xi.y + ar * Math.sin(theta);

				// Check if new point lies within distance r of existing
				// samples.
				// If it is sufficiently far, emit it as the next sample and add
				// it to the active list.
				if (verifyMinimumDistance(r, backgroundList, p) && p.distance(c) < size) {
					activeList.add(backgroundList.size());
					backgroundList.add(p);
					found = true;
				}

				theta += 2 * Math.PI / k;
			}

			 if (found == false)
				 activeList.remove(i);

			it++;
		}

		// Map to 3d coordinates inside the plane
		Point3d[] jitter = new Point3d[backgroundList.size()];
		int ji = 0;
		for (Point2d pt : backgroundList) {
			Point3d pt3 = new Point3d();
			pt3.scaleAdd(pt.x, plane.getS(), pt3);
			pt3.scaleAdd(pt.y, plane.getT(), pt3);
			pt3.add(plane.getOrigin());
			jitter[ji++] = pt3;
		}

		return jitter;
	}
	
	/**
	 * Poisson disc sampling in 2D (n=2) using Bridson's Fast Poisson Disk
	 * Sampling in Arbitrary Dimensions.
	 */
	public static Vector4d[] jitter_poissonWeighted(double r, Plane plane) {
		Point3d[] jitter = jitter_poisson(r, plane);
		
		double l = plane.getSmallestSide() / 2;
		
		Vector4d[] weighted = new Vector4d[jitter.length];
		int i = 0;
		for (Point3d p : jitter) {
			weighted[i++] = new Vector4d(p.x, p.y, p.z, 1 - p.distance(plane.getOrigin()) / l);
		}

		return weighted;
	}


	private static boolean verifyMinimumDistance(double r, List<Point2d> points,
			Point2d p) {
		for (Point2d pt : points) {
			if (pt.distance(p) < r)
				return false;
		}

		return true;
	}

}
