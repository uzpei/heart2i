package gl.geometry;


import gl.geometry.primitive.Plane;
import gl.material.GLMaterial;
import gl.renderer.GLViewerConfiguration;
import gl.texture.GLTexture;
import gl.tracing.Ray;
import gl.tracing.Traceable;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import config.GraphicsToolsConstants;

/**
 * A simple box room without lighting.
 * 
 * @author epiuze
 */
public class BoxRoom implements GLObject, Traceable {

	private final BooleanParameter draw = new BooleanParameter("draw", true);
	
	private final BooleanParameter highres = new BooleanParameter("High resolution", false);

	private final DoubleParameter floorSize = new DoubleParameter("Floor size", 15, 0, 100);
	
	private final BooleanParameter wireframe = new BooleanParameter("wireframe", false);
	
	private static String DEFAULT_TEXTURE_FILENAME = GraphicsToolsConstants.METAL_TEXTURE_PATH;
		
	/**
	 * Creates a room in which we can display something.
	 */
	public BoxRoom() {
		this(2);
	}

	/**
	 * Creates a room in which we can display something.
	 */
	public BoxRoom(double size) {
		setMaterials(GLMaterial.PURPLE, GLMaterial.TURQUOISE);
		
		floorSize.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateSize();
			}
		});
		
		floorSize.setValue(size);
	}
	
	public void setMaterials(GLMaterial wallMaterial, GLMaterial floorMaterial) {
		this.wallMaterial = wallMaterial;
		this.floorMaterial = floorMaterial;
		
		if (floor != null && ceiling != null && back != null && left != null && right != null && front!= null) {
			floor.setMaterial(floorMaterial);
			ceiling.setMaterial(wallMaterial);
			back.setMaterial(wallMaterial);
			left.setMaterial(wallMaterial);
			right.setMaterial(wallMaterial);
			front.setMaterial(wallMaterial);
		}
	}
	
	public void setSize(double size) {
		floorSize.setValue(size);
	}
	
	private void updateSize() {
		double size = floorSize.getValue();

		computePlanes();
		
		updateGrid();
	}
	
	private Plane floor, ceiling, back, left, right, front;
	private List<Plane> walls;
	private int N;

	private void computePlanes() {
		N = highres.getValue() ? 100 : 50;
		
		double size = floorSize.getValue();
		
		floor = new Plane(new Point3d(0, -size, 0), new Vector3d(0, 1, 0));
		floor.setSize(2*size, 2*size, N);
		
		ceiling = new Plane(new Point3d(0,  size, 0), new Vector3d(0, -1, 0));
		ceiling.setSize(2*size, 2*size, N);

		back = new Plane(new Point3d(0, 0, -size), new Vector3d(0, 0, 1));
		back.setSize(2*size, 2*size, N);

		left = new Plane(new Point3d(-size, 0, 0), new Vector3d(1, 0, 0));
		left.setSize(2*size, 2*size, N);

		right = new Plane(new Point3d(size, 0, 0), new Vector3d(-1, 0, 0));
		right.setSize(2*size, 2*size, N);

		front = new Plane(new Point3d(0, 0, size), new Vector3d(0, 0, -1));
		front.setSize(2*size, 2*size, N);
		
		walls = new LinkedList<Plane>();
		walls.add(floor);
		walls.add(ceiling);
		walls.add(back);
		walls.add(left);
		walls.add(right);
		walls.add(front);
		
		setTextures();
		
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
//		WorldAxis.display(gl);
		
		if (draw.getValue()) {
			gl.glEnable(GL2.GL_LIGHTING);
			drawBuffered(drawable);
		}
	}

	private GLMaterial wallMaterial, floorMaterial;
	
	private void drawBuffered(GLAutoDrawable drawable) {
		floor.display(drawable);
		ceiling.display(drawable);
		back.display(drawable);
		left.display(drawable);
		right.display(drawable);
		front.display(drawable);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(new TitledBorder("Room controls"));
		vfp.add(highres.getControls());
		highres.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				computePlanes();
				updateGrid();
			}
		});
		vfp.add(wireframe.getControls());
		wireframe.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateGrid();
			}
		});

		vfp.add(floorSize.getSliderControls());
		vfp.add(draw.getControls());
//		CollapsiblePanel cp = new CollapsiblePanel(vfp.getPanel());
//		cp.collapse();
//		return cp;
		return vfp.getPanel();
	}

	private void updateGrid() {
		floor.setDrawGrid(wireframe.getValue());
		ceiling.setDrawGrid(wireframe.getValue());
		back.setDrawGrid(wireframe.getValue());
		front.setDrawGrid(wireframe.getValue());
		left.setDrawGrid(wireframe.getValue());
		right.setDrawGrid(wireframe.getValue());
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		initDefaultTexture();
	}
	

	private void initDefaultTexture() {
		try {
			if (DEFAULT_TEXTURE_FILENAME == null) {
				System.err.println("ERROR in BoxRoom.initDefaultTexture: texture not found!");
			}
			
			File tfile = new File(DEFAULT_TEXTURE_FILENAME);
			
			System.out.println("Texture exists at " + tfile.getCanonicalPath() +  " ? " + (tfile.exists() ? "YES" : "NO"));

			floorTexture = new GLTexture(DEFAULT_TEXTURE_FILENAME);
//			floorTexture = TextureIO.newTexture(tfile, false);
			
	        setTextures();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setTextures() {
		floor.setTexture(floorTexture);
		ceiling.setTexture(floorTexture);
		back.setTexture(floorTexture);
		front.setTexture(floorTexture);
		left.setTexture(floorTexture);
		right.setTexture(floorTexture);
	}
	
	private GLTexture floorTexture;


	@Override
	public String getName() {
		return "Box room";
	}

	public void setVisible(boolean b) {
		draw.setValue(b);
	}
	
	public void setDrawWireframe(boolean b) {
		wireframe.setValue(b);
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
	}

	@Override
	public Point3d intersect(Ray ray) {
		for (Plane plane : walls) {
			Point3d p = plane.intersect(ray);
			
			if (p != null) return p;
		}
		
		return null;
	}
}
