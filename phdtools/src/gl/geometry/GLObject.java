package gl.geometry;

import gl.renderer.GLViewerConfiguration;

import javax.swing.JPanel;

/**
 * @author epiuze
 */
public interface GLObject extends GLGeometry {
    
    /**
     * Gets a control panel associated with this object.
     * @return a control panel
     */
    public JPanel getControls();
    
    /**
     * Reload and recreate this object (e.g. can be used to recreate the scene if parameters that can only be set during initialization have been changed).
     * @param parameters the parameters to pass to the object.
     */
    public void reload(GLViewerConfiguration config);

}