package gl.geometry;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Abstract class for polygonal objects
 * @author piuze
 *
 */
public interface Polygon {
	/**
	 * @return the normal to this object.
	 */
	public Vector3d getNormal();
	
	/**
	 * @return a point on the surface of this object.
	 */
	public Point3d getPoint();
	
//	public Point3d[] getConvexHull();
	
	/**
	 * @param p a point in 3D space
	 * @return whether this point lies inside the polygon
	 */
	public boolean isInside(Point3d p);

}
