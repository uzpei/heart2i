package gl.geometry;

import gl.material.GLMaterial.MaterialTemplate;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.FloatParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

import com.jogamp.opengl.util.gl2.GLUT;

public class LightRoom {
	private BoxRoom room = null;

	private final FloatParameter sint = new FloatParameter(
			"Spot light intensity", 1.0f, 0.0f, 1.0f);
	private final FloatParameter aint = new FloatParameter(
			"Ambient light intensity", 0f, 0.0f, 1.0f);
	private final FloatParameter dimming = new FloatParameter(
			"Dim light intensity", 0.3f, 0.0f, 1.0f);
	private final FloatParameter dist = new FloatParameter("Distance", 1f,
			0.0f, 5.0f);

	private final BooleanParameter drawbulb = new BooleanParameter("draw bulb",
			false);

	private final static float DEFAULT_SIZE = 1f;
	
	private float size = 1.0f;

	private GLUT glut = new GLUT();

	private final static double l = 10;
	private final DoubleParameterPoint3d lightpos = new DoubleParameterPoint3d(
			"Light position", new Point3d(0.5,0.5,1), new Point3d(-l,-l,-l), new Point3d(l,l,l));

	public LightRoom(double size) {
		setSize(size);
	}

	public LightRoom() {
		this(DEFAULT_SIZE);
	}

	public void setSize(double roomSize) {
		size = ((float) roomSize);

		if (room == null) {
			room = new BoxRoom(roomSize);
		}

		// Use a max light distance equal to twice the room size
		double ps = 5 * roomSize;
		lightpos.setMax(new Point3d(ps, ps, ps));
		lightpos.setMin(new Point3d(-ps, -ps, -ps));
		
		// Place the light on the z axis
		lightpos.setPoint3d(new Point3d(0, -0.25 * lightpos.getMax().y, 0.5 *
				lightpos.getMax().z));
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		room.display(drawable);
		
		// Get the current modelview matrix
//		double[] curmat = new double[16];
//		gl.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, curmat, 0);
//		Matrix4d cMat = new Matrix4d(curmat);
//		// (row-major order, so need to transpose)
//		cMat.transpose();
//		cMat.invert();
//		Point3d p0 = new Point3d();
//		cMat.transform(p0);
//		lightpos.setPoint3d(p0);
		   
		   // Enable lighting
		gl.glEnable(GL2.GL_LIGHTING);

		// Get light position
		Point3f lightPosition = lightpos.getPoint3f();
		lightPosition.scale(dist.getValue());
		Point3f lightPositionDim = new Point3f(lightPosition);
		lightPositionDim.negate();

		// Get specular, diffuse, ambient colors
		Point3f colorSpecDiff1 = new Point3f(sint.getValue(), sint.getValue(), sint.getValue());
		Point3f colorAmbient1 = new Point3f(aint.getValue(), aint.getValue(), aint.getValue());

		/*
		 * Set global quantities
		 */
		gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, new float[] { 0.2f, 0.2f, 0.2f, 1.0f }, 0);
//		gl.glLightModeli(GL2.GL_LIGHT_MODEL_LOCAL_VIEWER, GL.GL_TRUE);
//		gl.glLightModeli(GL2.GL_LIGHT_MODEL_TWO_SIDE, GL.GL_TRUE);
		
		/*
		 * Main light is at the top front of the room.
		 */
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, new float[] {
				colorAmbient1.x, colorAmbient1.y, colorAmbient1.z, 1 }, 0);
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float[] {
				colorSpecDiff1.x, colorSpecDiff1.y, colorSpecDiff1.z, 1 }, 0);
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, new float[] {
				colorSpecDiff1.x, colorSpecDiff1.y, colorSpecDiff1.z, 1 }, 0);

		// Set the type of light
//		gl.glLightfv(GL2.GL_LIGHT0 + lightNumber, GL2.GL_SPOT_DIRECTION, new float[] { 0, 0, -1, 1 }, 0);
//		gl.glLightfv(GL2.GL_LIGHT0 + lightNumber, GL2.GL_SPOT_CUTOFF, new float[] { 60 }, 0 );
		
		// Set the position of the first main light source
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[] {
				lightPosition.x, lightPosition.y, lightPosition.z, 1 }, 0);
		gl.glEnable(GL2.GL_LIGHT0);

		/*
		 *  Set the position of the second main light source
		 */
//		Point3f p1 = lightpos.getPoint3f();
//		p1.scale(15);
//		gl.glLightfv(GL2.GL_LIGHT0 + lightNumber, GL2.GL_POSITION, new float[] {
//				p1.x, p1.y, p1.z, 1 }, 0);
//		gl.glEnable(GL2.GL_LIGHT0 + lightNumber);

		/*
		 * Set dim light opposing the main light
		 */

		if (dimming.isChecked()) {

			// Specular and diffuse color
			Point3f colorSpecDiff2 = new Point3f(1, 1, 1);
			colorSpecDiff2.scale(dimming.getValue() * (dimming.isChecked() ? 1 : 0)
					* sint.getValue());

			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, new float[] {
					colorSpecDiff2.x, colorSpecDiff2.y, colorSpecDiff2.z, 1 }, 0);

			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, new float[] {
					colorSpecDiff2.x, colorSpecDiff2.y, colorSpecDiff2.z, 1 }, 0);

			// Ambient
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, new float[] {
					0, 0, 0, 1 }, 0);

			// Set light position
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, new float[] {
					lightPositionDim.x, lightPositionDim.y, lightPositionDim.z, 1 }, 0);
			gl.glEnable(GL2.GL_LIGHT1);
		}
		else {
			gl.glDisable(GL2.GL_LIGHT1);
		}

		/*
		 * Main light bulb
		 */
		if (drawbulb.getValue()) {
			gl.glDisable(GL2.GL_LIGHTING);

			// Main light
			gl.glColor4d(1, 1, 0, 0.8);
			gl.glPushMatrix();
			gl.glTranslated(lightPosition.x, lightPosition.y, lightPosition.z);
			glut.glutSolidSphere(size / 30, 16, 16);
			gl.glPopMatrix();

			// Dim light
			gl.glColor4d(0, 1, 1, 0.5);
			gl.glPushMatrix();
			gl.glTranslated(lightPositionDim.x, lightPositionDim.y, lightPositionDim.z);
			glut.glutSolidSphere(size / 30, 16, 16);
			gl.glPopMatrix();

//			// Main light 1
//			lightPositionDim.scale(-1);
//			gl.glColor4d(1, 1, 0, 0.8);
//			gl.glPushMatrix();
//			gl.glTranslated(lightPositionDim.x, lightPositionDim.y, lightPositionDim.z);
//			glut.glutSolidSphere(size / 30, 16, 16);
//			gl.glPopMatrix();
			
			gl.glEnable(GL2.GL_LIGHTING);

		}
		
	}

	private EnumComboBox<MaterialTemplate> ecbWallMaterial;
	private EnumComboBox<MaterialTemplate> ecbFloorMaterial;

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(new TitledBorder("Light room"));
		vfp.add(lightpos.getControls());
		vfp.add(drawbulb.getControls());
		vfp.add(sint.getSliderControls());
		vfp.add(aint.getSliderControls());
		vfp.add(dimming.getSliderControlsExtended(true));
		vfp.add(dist.getSliderControls(false));
		
		VerticalFlowPanel vmat = new VerticalFlowPanel();
		vmat.setBorder(new TitledBorder("Materials and textures"));

		ecbWallMaterial = new EnumComboBox<MaterialTemplate>("wall material", MaterialTemplate.CHROME);
		ecbFloorMaterial = new EnumComboBox<MaterialTemplate>("floor material", MaterialTemplate.BRASS);
		vmat.add(ecbWallMaterial.getControls());
		vmat.add(ecbFloorMaterial.getControls());
		
		vfp.add(vmat);
		
		ParameterListener mlist = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				room.setMaterials(ecbWallMaterial.getSelected().getMaterial(), ecbFloorMaterial.getSelected().getMaterial());
			}
		};
		
		room.setMaterials(ecbWallMaterial.getSelected().getMaterial(), ecbFloorMaterial.getSelected().getMaterial());
		ecbWallMaterial.addParameterListener(mlist);
		ecbFloorMaterial.addParameterListener(mlist);
		
		vfp.add(room.getControls());

//		CollapsiblePanel cp = new CollapsiblePanel(vfp.getPanel());
//		cp.collapse();
//		return cp;
		return vfp.getPanel();
	}

	/**
	 * @param f
	 */
	public void setAmbient(float f) {
		aint.setValue(f);
	}

	public void setSpot(float f) {
		sint.setValue(f);
	}

	public void setDistance(float f) {
		dist.setValue(f);
	}

	public void setLightPosition(Point3d p) {
		lightpos.setPoint3d(p);
	}

	public BoxRoom getBoxRoom() {
		return room;
	}

	public double getSize() {
		return size;
	}

	public void init(GLAutoDrawable drawable) {
		room.init(drawable);
	}

	public void setVisible(boolean b) {
		room.setVisible(b);
	}

}
