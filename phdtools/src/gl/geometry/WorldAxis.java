package gl.geometry;

import gl.math.FlatMatrix4d;
import gl.renderer.JoglRenderer;
import gl.renderer.JoglTextRenderer;

import java.awt.Color;
import java.awt.Font;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.vecmath.Matrix4d;

import com.jogamp.opengl.util.gl2.GLUT;

public class WorldAxis {

	private static JoglTextRenderer textRenderer = null;
	
    public static void display(GL2 gl, double scale, boolean drawScale) {
    	display(gl, scale, drawScale, true);
    }
    
    public static void display(GL2 gl, double scale, boolean drawScale, boolean drawLabels) {
    	if (textRenderer == null) {
    		Font font = new Font("Monospaced", Font.BOLD, 100);
    		textRenderer = new JoglTextRenderer(font, true, false);
    	}
    	
        gl.glDisable(GL2.GL_LIGHTING);

        double[] aoffset = { 0f, 0f, 0f};
        double[] offset = { 0.03f, 0.06f, 0f};
        gl.glPushMatrix();
        gl.glScaled(scale, scale, scale);
        gl.glTranslated(aoffset[0], aoffset[1], aoffset[2]);
        
//        gl.glLineWidth(1f);
        gl.glBegin(GL2.GL_LINES);
        
        // x
        gl.glColor4f(1, 0, 0, 1);
        gl.glVertex3d(0, 0, 0);
        gl.glColor4f(1, 0, 0, 0.2f);
        gl.glVertex3d(1, 0, 0);
        
        // y
        gl.glColor4f(0, 1, 0, 1);
        gl.glVertex3d(0, 0, 0);
        gl.glColor4f(0, 1, 0, 0.2f);
        gl.glVertex3d(0, 1, 0);

        // z
        gl.glColor4f(0, 0, 1, 1);
        gl.glVertex3d(0, 0, 0);
        gl.glColor4f(0, 0, 1, 0.2f);
        gl.glVertex3d(0, 0, 1);

        gl.glEnd();
        
        gl.glPopMatrix();

        float shift = (float) scale;
        float size = (float) (0.0015 * scale);
        
//        gl.glDisable(GL2.GL_CULL_FACE);
        if (drawLabels) {
    		textRenderer.begin3DRendering();
    		textRenderer.setColor(Color.red);
    		textRenderer.draw3D(drawScale ? "x = " + scale : "x", shift, 0, 0, size);
    		textRenderer.setColor(Color.green);
    		textRenderer.draw3D(drawScale ? "y = " + scale : "y", 0, shift, 0, size);
    		textRenderer.setColor(Color.blue);
    		textRenderer.draw3D(drawScale ? "z = " + scale : "z", 0, 0, shift, size);
    		textRenderer.end3DRendering();
        }
//        gl.glEnable(GL2.GL_CULL_FACE);
    }

    public static void display(GL2 gl) {
        display(gl, 1.0f, false);
    }

    public static void display(GL2 gl, boolean drawScale) {
        display(gl, 1.0f, drawScale);
    }

    public static void display(GL2 gl, Matrix4d localFrame, float scale) {
        gl.glPushMatrix();
        FlatMatrix4d fm = new FlatMatrix4d(localFrame);
        gl.glMultMatrixd(fm.asArray(), 0);
        display(gl, scale, false);
        gl.glPopMatrix();
    }

	public static void display2D(GL2 gl) {
		display(gl, 1, true);
	}
	
	public static void display2D(GL2 gl, double scale, boolean drawScale) {
      double[] offset = { 0.03, 0.03};
      gl.glPushMatrix();
      gl.glScaled(scale, scale, scale);
      
      gl.glTranslatef(100, 100, 0);

      gl.glLineWidth(1f);
      gl.glBegin(GL2.GL_LINES);
      
      // x
      gl.glColor3d(1, 0, 0);
      gl.glVertex2d(0, 0);
      gl.glVertex2d(1, 0);
      
      // y
      gl.glColor3d(0, 1, 0);
      gl.glVertex2d(0, 0);
      gl.glVertex2d(0, 1);

      gl.glEnd();
      
      gl.glPopMatrix();

      gl.glColor3f(1, 0, 0);
      double shift = 0.8 * scale;
      gl.glRasterPos3d(offset[0] + shift, offset[1] + 0, offset[2] + 0);
      JoglRenderer.glut.glutBitmapString(GLUT.BITMAP_9_BY_15, drawScale ? "x = " + scale : "x");

      gl.glColor3f(0, 1, 0);
      gl.glRasterPos3d(offset[0] + 0, offset[1] + shift, offset[2] + 0);
      JoglRenderer.glut.glutBitmapString(GLUT.BITMAP_9_BY_15, drawScale ? "y = " + scale : "y");

	}

}
