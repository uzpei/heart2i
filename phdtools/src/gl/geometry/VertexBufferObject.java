package gl.geometry;

import gl.geometry.primitive.FastQuad2d;
import gl.renderer.NodeScene;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import math.coloring.ColorMap;
import visualization.helical.HelicalSlicer;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;

import com.jogamp.common.nio.Buffers;

public class VertexBufferObject implements GLGeometry {

	protected int[] aiVertexBufferIndices = new int[] { -1 };
	protected Vertex[] vertices;
	
	protected boolean bufferNeedsUpdate;
	
	private boolean hasNormals = false;
	private boolean hasAlpha = false;
	private boolean hasVertices = true;
	
	private final static int countVertex = 3;
	private final static int countRGB = 3;
	private final static int countRGBA = 4;
	private final static int countNormal = 3;
	private boolean wireframe = false;
	private Color lineColor;
	
	private int drawMode = GL2.GL_QUADS;
	
	public VertexBufferObject(Vertex[] vertices) {
		this.vertices = vertices;
		bufferNeedsUpdate = true;
	}

	public VertexBufferObject(IntensityVolume volume) {
		this(volume, new IntensityVolumeMask(volume.getDimension(), true));
	}

	public VertexBufferObject(IntensityVolume volume, ColorMap.Map cmap) {
		this(volume, new IntensityVolumeMask(volume.getDimension(), true), cmap);
	}

	public VertexBufferObject(IntensityVolume volume, IntensityVolumeMask mask) {
		this(volume, mask, ColorMap.Map.JET);
	}

	public VertexBufferObject(IntensityVolumeMask mask) {
		this(mask, mask, ColorMap.Map.JET);
	}
	
	public VertexBufferObject(final IntensityVolume volume, final IntensityVolumeMask mask, ColorMap.Map cmap) {
		final double[] minMax = volume.getMinMax(mask);
		final ColorMap map = new ColorMap(cmap);
		final List<Vertex> vertices = new LinkedList<Vertex>();
		new VoxelBox(mask).voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				double v = volume.get(x, y, z);
				float[] color = new float[4];
				map.colorize(v, minMax[0], minMax[1]).getRGBComponents(color);
				cube(x, y, z, 1, 1, 1, color, vertices);
			}
		});
		this.vertices = vertices.toArray(new Vertex[vertices.size()]);
		bufferNeedsUpdate = true;
	}
	
	public void setWireframe(boolean enabled, Color lineColor) {
		wireframe = enabled;
		this.lineColor = lineColor;
	}
	
	public void setDrawMode(int glDrawMode) {
		drawMode = glDrawMode;
	}
	
	private int elementsPerVertex() {
		int strideElements = 0;
		strideElements += hasVertices ? countVertex : 0;
		strideElements += hasAlpha ? countRGBA : countRGB;
		strideElements += hasNormals ? countNormal : 0;

		return strideElements;
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		if (wireframe) {
			displayWireframe(drawable);
			return;
		}
		
		gl.glPushMatrix();

		// TODO: remove this
//		gl.glScaled(0.1, 0.1, 0.1);
		
		gl.glDisable(GL2.GL_LIGHTING);
//		gl.glDisable(GL2.GL_CULL_FACE);
//		gl.glDisable(GL2.GL_DEPTH_TEST);
		
		// create vertex buffers if needed, then copy data in
		createAndFillVertexBuffer(gl, vertices);

		// needed so material for quads will be set from color map
		gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);
		gl.glEnable(GL2.GL_COLOR_MATERIAL);

		// draw all quads in vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, aiVertexBufferIndices[0]);
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		
		gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
		
		int stride = Buffers.SIZEOF_FLOAT * elementsPerVertex();
		int position = 0;
		
		// Vertex
		gl.glVertexPointer(countVertex, GL.GL_FLOAT, stride, position);
		position += (hasVertices ? countVertex : 0) * Buffers.SIZEOF_FLOAT;
		
		// Color
		gl.glColorPointer(hasAlpha ? countRGBA : countRGB, GL.GL_FLOAT, stride, position);
		position += (hasAlpha ? countRGBA : countRGB) * Buffers.SIZEOF_FLOAT;
		
		// TODO: normals
		// TODO: textures
		
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);

		gl.glDrawArrays(drawMode, 0, vertices.length);

		// disable arrays once we're done
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
		
		gl.glDisableClientState(GL2.GL_COLOR_ARRAY);
		gl.glDisable(GL2.GL_COLOR_MATERIAL);
		
		gl.glPopMatrix();
	}

	private void displayWireframe(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glPushMatrix();

		gl.glDisable(GL2.GL_LIGHTING);
		
		// create vertex buffers if needed, then copy data in
		if (bufferNeedsUpdate)
			createAndFillVertexBuffer(gl, vertices);

		// draw all quads in vertex buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, aiVertexBufferIndices[0]);
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		
		int stride = Buffers.SIZEOF_FLOAT * elementsPerVertex();
		
		// Vertex
		gl.glVertexPointer(countVertex, GL.GL_FLOAT, stride, 0);
		
		gl.glColor3d(lineColor.getRed(), lineColor.getGreen(), lineColor.getBlue());
		gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
		gl.glDrawArrays(drawMode, 0, vertices.length);

		// disable arrays once we're done
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
		gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
		
		gl.glPopMatrix();

		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
	}
	
	/**
	 * Creates vertex buffer object used to store vertices and colors (if it
	 * doesn't exist). Fills the object with the latest vertices and colors from
	 * the data store.
	 * 
	 * @param gl2
	 *            GL object used to access all GL functions.
	 * @return the number of vertices in each of the buffers.
	 */
	protected void createAndFillVertexBuffer(GL2 gl2, Vertex[] vertices) {

		if (!bufferNeedsUpdate)
			return;
		
		// create vertex buffer object if needed
		if (aiVertexBufferIndices[0] == -1) {
			// check for VBO support
			if (!gl2.isFunctionAvailable("glGenBuffers")
					|| !gl2.isFunctionAvailable("glBindBuffer")
					|| !gl2.isFunctionAvailable("glBufferData")
					|| !gl2.isFunctionAvailable("glDeleteBuffers")) {
				// Activator.openError( "Error",
				// "Vertex buffer objects not supported." );
			}

			gl2.glGenBuffers(1, aiVertexBufferIndices, 0);

			// create vertex buffer data store without initial copy
			gl2.glBindBuffer(GL.GL_ARRAY_BUFFER, aiVertexBufferIndices[0]);
			gl2.glBufferData(GL.GL_ARRAY_BUFFER, vertices.length * Buffers.SIZEOF_FLOAT * elementsPerVertex(), null, GL2.GL_DYNAMIC_DRAW);
		}

		// map the buffer and write vertex and color data directly into it
		gl2.glBindBuffer(GL.GL_ARRAY_BUFFER, aiVertexBufferIndices[0]);
		ByteBuffer bytebuffer = gl2.glMapBuffer(GL.GL_ARRAY_BUFFER, GL2.GL_WRITE_ONLY);
		FloatBuffer floatbuffer = bytebuffer.order(ByteOrder.nativeOrder()).asFloatBuffer();

		storeVerticesAndColors(floatbuffer, vertices);

		gl2.glUnmapBuffer(GL.GL_ARRAY_BUFFER);

		bufferNeedsUpdate = false;
	}

	protected void disposeVertexBuffers(GLAutoDrawable drawable) {
		GL2 gl2 = drawable.getGL().getGL2();
		gl2.glDeleteBuffers(1, aiVertexBufferIndices, 0);
		aiVertexBufferIndices[0] = -1;
	}

	public static class Vertex {
		public float x, y, z;
		public float nx, ny, nz;
		public float[] color;
		public float[] normal;

		public Vertex(float x, float y, float z, float[] color) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.color = color;
		}

		public Vertex(float x, float y, float z, float nx, float ny, float nz,
				float[] color) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.color = color;
			this.nx = nx;
			this.ny = ny;
			this.nz = nz;
		}
	}

	protected void storeVerticesAndColors(FloatBuffer floatbuffer,
			Vertex[] vertices) {
		for (Vertex v : vertices) {
			floatbuffer.put(v.x);
			floatbuffer.put(v.y);
			floatbuffer.put(v.z);

			floatbuffer.put(v.color[0]);
			floatbuffer.put(v.color[1]);
			floatbuffer.put(v.color[2]);
			if (hasAlpha)
				floatbuffer.put(v.color[3]);
		}
	}

	public static Vertex[] asVertices(FastQuad2d q) {

		Vertex v1 = new Vertex(q.x, q.y, 0, q.getColor());
		Vertex v2 = new Vertex(q.x + q.width, q.y, 0, q.getColor());
		Vertex v3 = new Vertex(q.x + q.width, q.y + q.height, 0, q.getColor());
		Vertex v4 = new Vertex(q.x, q.y + q.height, 0, q.getColor());

		return new Vertex[] { v1, v2, v3, v4 };
	}

	@Override
	public void init(GLAutoDrawable drawable) {

	}

	@Override
	public String getName() {
		return null;
	}
	
	public static Vertex[] createGeometry3d(int n) {
		float size = 1f;
		Random rand = new Random();
		List<Vertex> list = new LinkedList<Vertex>();
		float w = size / n;
		float h = size / n;
		float d = size / n;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				for (int k = 0; k < n; k++) {
					// Cut some holes
					if (i > n / 2 && j > n / 2 && k > n / 2)
						continue;
					
				float[] color = { rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat() };
//				float[] color = { i / (float) n, j / (float) n, k / (float) n, 1f };

				// Lower corner
				float x = i * size / n;
				float y = j * size / n;
				float z = k * size / n;
				
				cube(x, y, z, w, h, d, color, list);

				}
			}
		}

		return list.toArray(new Vertex[list.size()]);
	}
	
	public static void cube(float x, float y, float z, float w, float h, float d, float[] color, List<Vertex> list) {
		// XY - 0 face (alpha OK)
		list.add(new Vertex(x + 0, y + 0, z + d, color));
		list.add(new Vertex(x + w, y + 0, z + d, color));
		list.add(new Vertex(x + w, y + h, z + d, color));
		list.add(new Vertex(x + 0, y + h, z + d, color));

		// XY - Z face (alpha ?)
		list.add(new Vertex(x + 0, y + 0, z + 0, color));
		list.add(new Vertex(x + 0, y + h, z + 0, color));
		list.add(new Vertex(x + w, y + h, z + 0, color));
		list.add(new Vertex(x + w, y + 0, z + 0, color));

		// XZ - 0 face
		list.add(new Vertex(x + 0, y + h, z + 0, color));
		list.add(new Vertex(x + 0, y + h, z + d, color));
		list.add(new Vertex(x + w, y + h, z + d, color));
		list.add(new Vertex(x + w, y + h, z + 0, color));

		// XZ - Y face
		list.add(new Vertex(x + 0, y + 0, z + 0, color));
		list.add(new Vertex(x + w, y + 0, z + 0, color));
		list.add(new Vertex(x + w, y + 0, z + d, color));
		list.add(new Vertex(x + 0, y + 0, z + d, color));

		// YZ - 0 face
		list.add(new Vertex(x + 0, y + 0, z + 0, color));
		list.add(new Vertex(x + 0, y + 0, z + d, color));
		list.add(new Vertex(x + 0, y + h, z + d, color));
		list.add(new Vertex(x + 0, y + h, z + 0, color));

		// YZ - X face
		list.add(new Vertex(x + w, y + 0, z + 0, color));
		list.add(new Vertex(x + w, y + h, z + 0, color));
		list.add(new Vertex(x + w, y + h, z + d, color));
		list.add(new Vertex(x + w, y + 0, z + d, color));
	}

	public static Vertex[] createGeometry2d(int n) {
		float size = 1f;
		Random rand = new Random();
		List<Vertex> list = new LinkedList<Vertex>();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				float[] color = { rand.nextFloat(), rand.nextFloat(),
						rand.nextFloat(), 1f };
				Vertex[] qv = asVertices(new FastQuad2d(i * size / n, j * size
						/ n, size / n, size / n, color));
				list.add(qv[0]);
				list.add(qv[1]);
				list.add(qv[2]);
				list.add(qv[3]);
			}
		}

		return list.toArray(new Vertex[list.size()]);
	}
	static NodeScene scene;
	
	public static void main(String[] args) {
		scene = new NodeScene(false, -800);

//		VertexBufferObject vbo = new VertexBufferObject(createGeometry3d(40));
		int[] dims = new int[] { 40, 40, 40 };
		IntensityVolume volume = IntensityVolume.createSphericalVolume(dims[0], dims[1], dims[2]);
		IntensityVolume mask = IntensityVolume.createSphericalVolume(dims[0], dims[1], dims[2]);
//		VertexBufferObject vbo = new VertexBufferObject(volume, new IntensityVolumeMask(mask.getData()));
//		mask.negate();
//		mask.threshold(-0.5, 0);
//		mask.abs();
//		mask.toBinary();
//		IntensityVolumeMask mask2 = new IntensityVolumeMask(mask.getData());
//		VertexBufferObject vbo = new VertexBufferObject(volume, mask2);
		

		// Create a helical volume 
		HelicalSlicer hv = new HelicalSlicer(volume);
		
		VertexBufferObject vbo = new VertexBufferObject(volume, hv.asVoxelMask());
		
		scene.addNode(vbo);
		scene.start();
	}

	public Vertex[] getVertices() {
		return vertices;
	}

}
