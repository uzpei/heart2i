package gl.geometry;

import gl.material.GLMaterial;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.common.nio.Buffers;


public class VertexArrayObject {
	
	private List<float[]> data;
	
	private FloatBuffer vertexBuffer;

	private FloatBuffer normalBuffer;

	private int[] colorBufferId = new int[] { -1 };

	private int[] vertexBufferId = new int[] { -1 };

	private boolean useNormals = false;
	
	/**
	 * Data is a list of float[6] containing vertex position (3 channels) and color (3 channels).
	 * @param data
	 */
	public VertexArrayObject(List<float[]> data) {
		this.data = data;
	}
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		if (vertexBufferId[0] == -1) {
			generateBuffers(gl);
		}
		
		// needed so material for quads will be set from color map
		GLMaterial.CHROME.apply(gl);
		gl.glEnable( GL2.GL_COLOR_MATERIAL );
		gl.glColorMaterial( GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE );
		 
		// draw all triangle strips in vertex buffer
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
		if (useNormals) 
			gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);

		gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, vertexBufferId[0] );
//		gl.glVertexPointer(3, GL2.GL_FLOAT, 0, 0);
//		gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, colorBufferId[0] );
//		gl.glColorPointer(3, GL2.GL_FLOAT, 0, 0);

		if (useNormals) {
			gl.glVertexPointer( 3, GL.GL_FLOAT, 9 * Buffers.SIZEOF_FLOAT, 0 );
			gl.glNormalPointer(GL2.GL_FLOAT, 0, normalBuffer);
			gl.glColorPointer( 3, GL.GL_FLOAT, 9 * Buffers.SIZEOF_FLOAT, 6 * Buffers.SIZEOF_FLOAT );
		}
		else {
			gl.glVertexPointer( 3, GL.GL_FLOAT, 6 * Buffers.SIZEOF_FLOAT, 0 );
			gl.glColorPointer( 3, GL.GL_FLOAT, 6 * Buffers.SIZEOF_FLOAT, 3 * Buffers.SIZEOF_FLOAT );
		}
		
		gl.glPolygonMode( GL2.GL_FRONT, GL2.GL_FILL );
		gl.glDrawArrays( GL2.GL_TRIANGLE_STRIP, 0, data.size());
		 
		// disable arrays once we're done
		gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, 0 );
		gl.glDisableClientState( GL2.GL_VERTEX_ARRAY );
		gl.glDisableClientState( GL2.GL_COLOR_ARRAY );
		gl.glDisableClientState( GL2.GL_NORMAL_ARRAY );
		gl.glDisable( GL2.GL_COLOR_MATERIAL );
	}
	
	/**
	 * Creates vertex buffer object used to store vertices and colors
	 * (if it doesn't exist). Fills the object with the latest
	 * vertices and colors from the data store.
	 *
	 * @param gl2 GL object used to access all GL functions.
	 * @return the number of vertices in each of the buffers.
	 */
	protected int generateBuffers(GL2 gl2) {
	 
	    // create vertex buffer object if needed
	        // check for VBO support
	        if(    !gl2.isFunctionAvailable( "glGenBuffers" )
	            || !gl2.isFunctionAvailable( "glBindBuffer" )
	            || !gl2.isFunctionAvailable( "glBufferData" )
	            || !gl2.isFunctionAvailable( "glDeleteBuffers" ) ) {
	            System.err.println( "Error: vertex buffer objects not supported." );
	        }
	 
	        gl2.glGenBuffers( 1, vertexBufferId, 0 );
	        gl2.glBindBuffer( GL2.GL_ARRAY_BUFFER, vertexBufferId[0] );
	        gl2.glBufferData( GL2.GL_ARRAY_BUFFER, data.size() * 3 * Buffers.SIZEOF_FLOAT * 2, null, GL2.GL_STATIC_DRAW );

//	        gl2.glGenBuffers( 1, colorBufferId, 0 );
//	        gl2.glBindBuffer( GL2.GL_ARRAY_BUFFER, colorBufferId[0] );
//	        gl2.glBufferData( GL2.GL_ARRAY_BUFFER, data.size() * 3 * Buffers.SIZEOF_FLOAT, null, GL2.GL_STATIC_DRAW );

	    // map the buffer and write vertex and color data directly into it
	        ByteBuffer bytebuffer = gl2.glMapBuffer( GL.GL_ARRAY_BUFFER, GL2.GL_WRITE_ONLY );
	        vertexBuffer = bytebuffer.order( ByteOrder.nativeOrder()).asFloatBuffer();

	        		//	    vertexBuffer = FloatBuffer.allocate(data.size() * 3);
//	    vertexBuffer.rewind();
//	    colorBuffer = FloatBuffer.allocate(data.size() * 3);
//	    colorBuffer.rewind();
	    
	    System.out.println("Filling VBO buffer from " + data.size() + " points.");
//		for (float[] v : data) {
//			for (int vi = 0; vi < 3; vi++) 
//				vertexBuffer.put(v[vi]);
//			for (int vi = 3; vi < 6; vi++) 
//				colorBuffer.put(v[vi]);
//		}
		for (float[] v : data) {
			for (int vi = 0; vi < 6; vi++) 
				vertexBuffer.put(v[vi]);
		}
	 
		gl2.glUnmapBuffer( GL2.GL_ARRAY_BUFFER );
		
	    return vertexBufferId[0];
	}
}
