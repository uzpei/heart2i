package gl.geometry.primitive;

import java.awt.Color;

import gl.geometry.GLGeometry;
import gl.material.GLMaterial;
import gl.tracing.TraceablePolygon;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Quad extends TraceablePolygon implements GLGeometry {

	public Point3d p1, p2, p3, p4;
	public GLMaterial[] materials;
	public float[] color;
	protected Point3d center;
	
	private Vector3d normal;
	
	public Quad(Point3d[] pts) {
		this(pts[0], pts[1], pts[2], pts[3]);
	}
	
	public Quad (Point2d origin, double size) {
		this(new Point3d(origin.x, origin.y, 0), new Point3d(origin.x, origin.y + size, 0), new Point3d(origin.x + size, origin.y + size, 0), new Point3d(origin.x + size, origin.y, 0));
	}

	public Quad (Point3d p1, Point3d p2, Point3d p3, Point3d p4) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.p4 = p4;
		
		Vector3d u = new Vector3d();
		u.sub(p4, p1);
		u.normalize();
		Vector3d v = new Vector3d();
		v.sub(p3, p4);
		v.normalize();
		
		normal = new Vector3d();
		normal.cross(u, v);
		
		center = new Point3d();
		center.add(p1);
		center.add(p2);
		center.add(p3);
		center.add(p4);
		center.scale(1d/4d);
	}
	
	public void setMaterials(GLMaterial[] materials) {
		this.materials = materials;
	}
	
	public void setColor(float[] color) {
		this.color = color;
	}
	
	@Override
	public Vector3d getNormal() {
		return normal;
	}

	@Override
	public Point3d getPoint() {
		return center;
	}

	@Override
	public boolean isInside(Point3d p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		display(drawable, false);
	}
	
	public void display(GLAutoDrawable drawable, boolean outline) {
		GL2 gl = drawable.getGL().getGL2();
		
		if (color != null)
			gl.glColor3f(color[0], color[1], color[2]);
		
		gl.glBegin(GL2.GL_QUADS);
		gl.glVertex3d(p1.x, p1.y, p1.z);
		gl.glVertex3d(p2.x, p2.y, p2.z);
		gl.glVertex3d(p3.x, p3.y, p3.z);
		gl.glVertex3d(p4.x, p4.y, p4.z);
		
//		gl.glNormal3d(normal.x, normal.y, normal.z);
//		gl.glTexCoord2d(0, 0);
//		gl.glVertex3d(p1.x, p1.y, p1.z);
//		gl.glNormal3d(normal.x, normal.y, normal.z);
//		gl.glTexCoord2d(0, 1);
//		gl.glVertex3d(p2.x, p2.y, p2.z);
//		gl.glNormal3d(normal.x, normal.y, normal.z);
//		gl.glTexCoord2d(1, 1);
//		gl.glVertex3d(p3.x, p3.y, p3.z);
//		gl.glNormal3d(normal.x, normal.y, normal.z);
//		gl.glTexCoord2d(1, 0);
//		gl.glVertex3d(p4.x, p4.y, p4.z);
		gl.glEnd();
		
		if (outline) {
			gl.glColor3d(0, 0, 0);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex3d(p1.x, p1.y, p1.z);
			gl.glVertex3d(p2.x, p2.y, p2.z);
			gl.glVertex3d(p3.x, p3.y, p3.z);
			gl.glVertex3d(p4.x, p4.y, p4.z);
			gl.glVertex3d(p1.x, p1.y, p1.z);
			gl.glEnd();
		}
	}

	public void displayMaterial(GLAutoDrawable drawable, GLMaterial[] materials) {
		setMaterials(materials);
	}
	
	public void displayMaterials(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		if (materials == null)
			return;
		
		gl.glBegin(GL2.GL_QUADS);
		
		if (materials.length > 0) materials[0].apply(gl);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(0, 0);
		gl.glVertex3d(p1.x, p1.y, p1.z);

		if (materials.length > 1) materials[1].apply(gl);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(0, 1);
		gl.glVertex3d(p2.x, p2.y, p2.z);

		if (materials.length > 2) materials[2].apply(gl);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(1, 1);
		gl.glVertex3d(p3.x, p3.y, p3.z);
		
		if (materials.length > 3) materials[3].apply(gl);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(1, 0);
		gl.glVertex3d(p4.x, p4.y, p4.z);
		
		gl.glEnd();
	}
	
	/**
	 * TODO: use previous and next to set per-vertex normals
	 * @param drawable
	 * @param previous
	 * @param next
	 */
	public void display(GLAutoDrawable drawable, Quad previous, Quad next) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(0, 0);
		gl.glVertex3d(p1.x, p1.y, p1.z);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(0, 1);
		gl.glVertex3d(p2.x, p2.y, p2.z);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(1, 1);
		gl.glVertex3d(p3.x, p3.y, p3.z);
		gl.glNormal3d(normal.x, normal.y, normal.z);
		gl.glTexCoord2d(1, 0);
		gl.glVertex3d(p4.x, p4.y, p4.z);
		gl.glEnd();
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
