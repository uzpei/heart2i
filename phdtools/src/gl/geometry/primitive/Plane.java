package gl.geometry.primitive;

import gl.geometry.FancyArrow;
import gl.geometry.GLGeometry;
import gl.material.Colour;
import gl.material.GLMaterial;
import gl.material.GLMaterial.MaterialTemplate;
import gl.math.FlatMatrix4d;
import gl.texture.GLTexture;
import gl.tracing.Ray;
import gl.tracing.Traceable;
import gl.tracing.TraceablePolygon;

import java.io.File;
import java.io.IOException;
import java.nio.DoubleBuffer;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import config.GraphicsToolsConstants;

public class Plane extends TraceablePolygon implements GLGeometry, Traceable {

	private static final double eps = 1e-8;

	private Point3d x0;
	private Vector3d s;
	private Vector3d t;
	private Vector3d n = new Vector3d();
	private Point3d p1, p2, p3, p4;
	private DoubleBuffer vertexBuffer;
	private DoubleBuffer textureBuffer;

	private DoubleParameter sizex = new DoubleParameter("size x", 5, 1e-6, 1e3);
	private DoubleParameter sizey = new DoubleParameter("size y", 5, 1e-6, 1e3);
	private IntParameter samples = new IntParameter("samples", 40, 1, 1000);
	private BooleanParameter drawgrid = new BooleanParameter("draw grid", false);
	
	private EnumComboBox<MaterialTemplate> ecbMaterial = new EnumComboBox<MaterialTemplate>("material", MaterialTemplate.SOFTWHITE);

	private int N = 40;
	private int k;

	public boolean debug = false;

	public Plane(Point3d x0, Vector3d n, Vector3d s, Vector3d t) {
		set(x0, n, s, t);
	}

	/**
	 * Create a plane using the given normal <code>n</code>
	 * 
	 * @param x0
	 *            the corner of the plane
	 * @param n
	 *            the normal to the plane
	 */
	public Plane(Point3d x0, Vector3d n) {
		// Need to find two arbitrary vectors parallel to plane
		this(x0, n, Plane.linearSpan(x0, n)[0], Plane.linearSpan(x0, n)[1]);
	}

	/**
	 * Create a plane from orthogonal <code>s</code> and <code>t</code>
	 * 
	 * @param x0
	 *            the corner of the plane
	 * @param s
	 *            the first spanning vector of the plane
	 * @param t
	 *            the second spanning vector of the plane
	 */
	public Plane(Point3d x0, Vector3d s, Vector3d t) {
		n.cross(t, s);
		set(x0, n, s, t);
	}

	/**
	 * Define a plane using parameteric form ax + by + cz + d = 0
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 */
	public Plane(double a, double b, double c, double d) {
		n = new Vector3d(a, b, c);
		double l = n.length();
		d /= l;
		n.scale(1 / l);

		// Find a point in the plane
		x0 = new Point3d(1, 1, 1);

		if (Math.abs(n.x) > eps)
			x0.x = (-n.y * x0.y - n.z * x0.z - d) / n.x;
		else if (Math.abs(n.y) > eps)
			x0.y = (-n.x * x0.x - n.z * x0.z - d) / n.y;
		else if (Math.abs(n.z) > eps)
			x0.z = (-n.x * x0.x - n.y * x0.y - d) / n.z;

		// Need to find two arbitrary vectors parallel to plane
		Vector3d[] span = Plane.linearSpan(x0, n);
		set(x0, n, span[0], span[1]);
	}

	/**
	 * Define a rectangular plane from four of its points, clockwise. The line
	 * segment p1-p2 must be orthogonal to p2-p3.
	 * 
	 * @param p1
	 * @param p2
	 * @param p3
	 * @param p4
	 */
	public Plane(Point3d p1, Point3d p2, Point3d p3, Point3d p4) {

		// Find the normal
		s = new Vector3d();
		t = new Vector3d();

		s.sub(p2, p1);
		// s.normalize();
		t.sub(p3, p2);
		// t.normalize();

		n = new Vector3d();
		n.cross(t, s);

		// Use the first point as the origin
		set(p1, n, s, t);
	}

	private void set(Point3d x0, Vector3d n, Vector3d s, Vector3d t) {
		this.n = new Vector3d(n);
		this.n.normalize();
		this.x0 = new Point3d(x0);
		this.s = new Vector3d(s);
		// this.s.normalize();
		this.t = new Vector3d(t);
		// this.t.normalize();

		computeCorners();

		createVertexArray();
	}

	/**
	 * Precompute the four corners of this plane.
	 */
	private void computeCorners() {
		p1 = new Point3d(x0);
		p1.scaleAdd(-0.5, s, p1);
		p1.scaleAdd(-0.5, t, p1);

		p2 = new Point3d(x0);
		p2.scaleAdd(-0.5, s, p2);
		p2.scaleAdd(0.5, t, p2);

		p3 = new Point3d(x0);
		p3.scaleAdd(0.5, s, p3);
		p3.scaleAdd(0.5, t, p3);

		p4 = new Point3d(x0);
		p4.scaleAdd(0.5, s, p4);
		p4.scaleAdd(-0.5, t, p4);
	}

	/**
	 * @param x0
	 * @param n
	 * @return two normalized vectors spanning the plane defined by x0 and n.
	 */
	public static Vector3d[] linearSpan(Point3d x0, Vector3d n) {
		Vector3d s = new Vector3d();
		Vector3d t = new Vector3d();
		Vector3d N = new Vector3d(n);

		// 0 = ax + by + cz + d
		// TODO: could store that
		// double d = -(n.x * x0.x + n.y * x0.y + n.z * x0.z);

		if (n.length() < eps)
			throw new RuntimeException(
					"Error while computing the linear span of a plane: the normal vector must be non-zero");

		// Find smallest component of n (in magnitude) and set to zero
		N.absolute();
		s.set(n);

		// x, y | z
		if (N.x == N.y && N.y == N.z)
			s.x = 0;
		else if (N.x <= N.y && N.x <= N.z)
			s.x = 1;
		// y, x | z
		else if (N.y <= N.x && N.y <= N.z)
			s.y = 1;
		// z, x | y
		else if (N.z <= N.x && N.z <= N.y)
			s.z = 1;

		s.normalize();
		s.cross(s, n);
		s.normalize();

		t.cross(n, s);
		t.normalize();

		return new Vector3d[] { s, t };
	}

	private GLMaterial material = GLMaterial.SOFTWHITE;
	
	public void setMaterial(GLMaterial material) {
		ecbMaterial.setSelected(MaterialTemplate.getMaterialTemplate(material));
		this.material = material;
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		boolean drawquad = false;
		if (drawquad) {
			// Draw the plane using a QUAD loop
			gl.glBegin(GL2.GL_QUADS);
			gl.glNormal3d(n.x, n.y, n.z);
			gl.glVertex3d(p1.x, p1.y, p1.z);
			gl.glVertex3d(p2.x, p2.y, p2.z);
			gl.glVertex3d(p3.x, p3.y, p3.z);
			gl.glVertex3d(p4.x, p4.y, p4.z);
			gl.glEnd();
		} else {
			gl.glPushMatrix();
			
			// Draw plane as a vertex array
			
			// Enable parameters
			gl.glEnable(GL2.GL_LIGHTING);
			GLMaterial.apply(gl, material);
			gl.glNormal3d(n.x, n.y, n.z);

			// Setup client
			gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);

			vertexBuffer.rewind();
			textureBuffer.rewind();
			
			if (texture != null) {
				gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
				gl.glEnable(GL.GL_TEXTURE_2D);
//				gl.glEnable(GL.GL_BLEND);
//				texture.enable();
//				texture.bind();
											
				texture.bind(gl);
				gl.glTexCoordPointer(2, GL2.GL_DOUBLE, 0, textureBuffer);
			}
			
			gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, vertexBuffer);
			gl.glDrawArrays(GL2.GL_QUAD_STRIP, 0, k);

			// Restore texture
			gl.glDisableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
			gl.glDisable(GL.GL_TEXTURE_2D);

			gl.glPopMatrix();
		}

		if (drawgrid.getValue())
			drawGrid(gl);

		if (debug) {
			double asize = getSmallestSide() / 100;
			
			// Draw n, s and t
			Point3d pt = new Point3d();
			pt.scaleAdd(0.5, s, x0);
			FancyArrow fas = new FancyArrow(x0, pt, Colour.red, asize);
			fas.draw(gl);

			pt.scaleAdd(0.5, t, x0);
			// FancyArrow fat = new FancyArrow(p1, p2, Colour.green, 0.02);
			FancyArrow fat = new FancyArrow(x0, pt, Colour.green, asize);
			fat.draw(gl);

			Point3d pn = new Point3d();
			pn.scaleAdd(getSmallestSide() / 2, n, x0);
			FancyArrow fan = new FancyArrow(x0, pn, Colour.blue, asize);
			fan.draw(gl);

//			GLViewerHelper.print3dTextLines(drawable, "p1", p1);
//			GLViewerHelper.print3dTextLines(drawable, "p2", p2);
//			GLViewerHelper.print3dTextLines(drawable, "p3", p3);
//			GLViewerHelper.print3dTextLines(drawable, "p4", p4);

			drawTeapot(gl);
		}
	}

	private boolean teapotInitialized = false;
	private Texture potTexture;
	
	private void initTeapot() {
		try {
			File pfile = new File(GraphicsToolsConstants.METAL_TEXTURE_PATH);

			System.out.println("Texture found at " + pfile.getCanonicalPath() +  " ? " + (pfile.exists() ? "YES" : "NO"));
			
			potTexture = TextureIO.newTexture(pfile, false);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
		teapotInitialized = true;
	}
	
	private void drawTeapot(GL2 gl) {
		if (!teapotInitialized) {
			initTeapot();
		}
		
//		double size = getSmallestSide() / 15;
		double size = getSmallestSide() / 10;
		
		gl.glEnable(GL2.GL_LIGHTING);

		float[] tableColor = { 0.3f, 0.5f, 0.75f, 1 };
		float[] whiteColor = new float[] { 1f, 1f, 1f, 1 };

		gl.glDisable(GL.GL_CULL_FACE);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE,
				tableColor, 0);
		gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, whiteColor, 0);
		gl.glMaterialf(GL.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 20);

		GLMaterial.BRASS.apply(gl);
		
		potTexture.bind(gl);
		
		gl.glPushMatrix();
		/*
		 * Premultiply local frame
		 */
		Vector3d sn = new Vector3d(s);
		sn.normalize();
		Vector3d tn = new Vector3d(t);
		tn.normalize();
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		m.setColumn(0, n.x, n.y, n.z, 0);
		m.setColumn(1, sn.x, sn.y, sn.z, 0);
		m.setColumn(2, tn.x, tn.y, tn.z, 0);
		// m.setColumn(3, x0.x + s.x / 2 + t.x / 2, x0.y + s.y / 2 + t.y / 2,
		// x0.z
		// + s.z / 2 + t.z / 2, 1);
		m.setColumn(3, x0.x, x0.y, x0.z, 1);

		FlatMatrix4d fm = new FlatMatrix4d();
		fm.setBackingMatrix(m);
		gl.glMultMatrixd(fm.asArray(), 0);

		// Draw a teapot & axis lying on the plane
		gl.glRotated(90, 0, 1, 0);
		GLUT glut = new GLUT();
		glut.glutSolidTeapot(size, false);
		gl.glPopMatrix();
	}

	private List<Point3d> grid = new LinkedList<Point3d>();
	
	private List<Point3d> computeGrid() {
		
		grid.clear();
		
		// Chop s and t into N pieces
		Vector3d sch = new Vector3d(s);
		sch.scale(1.0 / N);
		Vector3d tch = new Vector3d(t);
		tch.scale(1.0 / N);

		Point3d cx0 = new Point3d(x0);
		cx0.scaleAdd(-0.5, s, cx0);
		cx0.scaleAdd(-0.5, t, cx0);

		Point3d p;
		for (int i = 0; i <= N; i ++) {
			for (int j = 0; j <= N; j++) {
				p = new Point3d();
				
				p.scaleAdd(i, sch, cx0);
				p.scaleAdd(j, tch, p);
				grid.add(p);
			}
		}
		
		return grid;
	}
	
	public List<Point3d> getGrid() {
		return grid;
	}
	
	private void drawGrid(GL2 gl) {
		// Chop s and t into N pieces
		Vector3d sch = new Vector3d(s);
		sch.scale(1.0 / N);
		Vector3d tch = new Vector3d(t);
		tch.scale(1.0 / N);

		Point3d cx0 = new Point3d(x0);
		cx0.scaleAdd(-0.5, s, cx0);
		cx0.scaleAdd(-0.5, t, cx0);

		// Skip some vertices
		// TODO: find all factors of N and select the middle one from those
		// e.g. for 30: 1, 2, 3, 5, 6, 10, 15, 30 --> select 5
		int gskip = 1;

		gl.glEnable(GL2.GL_LIGHTING);
		GLMaterial.apply(gl, GLMaterial.TURQUOISE);
		gl.glBegin(GL.GL_LINES);
		for (int i = 0; i <= N; i += gskip) {
			p1.scaleAdd(i, sch, cx0);

			p2.set(p1);
			p2.add(t);

			gl.glVertex3d(p1.x, p1.y, p1.z);
			gl.glVertex3d(p2.x, p2.y, p2.z);
		}
		gl.glEnd();

		gl.glBegin(GL.GL_LINES);
		for (int j = 0; j <= N; j += gskip) {
			p1.scaleAdd(j, tch, cx0);

			p2.set(p1);
			p2.add(s);

			gl.glVertex3d(p1.x, p1.y, p1.z);
			gl.glVertex3d(p2.x, p2.y, p2.z);
		}
		gl.glEnd();
	}
	
	private GLTexture texture;
	
	public void setTexture(GLTexture t) {
		texture = t;
		createVertexArray();
	}
	
	private void createVertexArray() {
		// Draw a NxN grid
		vertexBuffer = Buffers.newDirectDoubleBuffer(3 * 2 * N * N + 2 * 3 * N);
		textureBuffer = Buffers.newDirectDoubleBuffer(2 * 2 * N * N + 2 * 3 * N);

		// Chop s and t into N pieces
		Vector3d sch = new Vector3d(s);
		sch.scale(1.0 / N);
		Vector3d tch = new Vector3d(t);
		tch.scale(1.0 / N);

		Point3d cx0 = new Point3d(x0);
		cx0.scaleAdd(-0.5, s, cx0);
		cx0.scaleAdd(-0.5, t, cx0);

		k = 0;

		// Used to "rewind" the quad strip such that we get overlap at the end
		// otherwise, the plane is traversed sequentially from one end to the
		// other
		// and the vertex array intersects itself.
		boolean rewind = true;

		Point3d p1 = new Point3d();
		Point3d p2 = new Point3d();
		
		Point3d pc = new Point3d(cx0);
		pc.add(s);
		pc.add(t);
		int ri = 0;
		for (int j = 0; j < N; j++) {
			for (int i = 0; i <= N; i++) {
				ri = rewind ? i : N - i;
				
				p1.scaleAdd(ri, sch, cx0);
				p2.set(p1);

				p1.scaleAdd(j + (rewind ? 1 : 0), tch, p1);
				p2.scaleAdd(j + (rewind ? 0 : 1), tch, p2);

				vertexBuffer.put(p1.x);
				vertexBuffer.put(p1.y);
				vertexBuffer.put(p1.z);
				
				textureBuffer.put(((double)ri) / ((double)N));
				textureBuffer.put(((double)(!rewind ? j: j + 1)) / (N));
				
				k++;

				vertexBuffer.put(p2.x);
				vertexBuffer.put(p2.y);
				vertexBuffer.put(p2.z);
				
				textureBuffer.put(((double)ri) / ((double)N));
				textureBuffer.put(((double)(!rewind ? j + 1 : j)) / (N));

				k++;
			}
			rewind = !rewind;
		}

		vertexBuffer.rewind();
		textureBuffer.rewind();
	}

	public double getSmallestSide() {
		return Math.min(s.length(), t.length());
	}

	public double getLargestSide() {
		return Math.max(s.length(), t.length());
	}

	public boolean isParameterTsmallestSide() {
		return t.length() <= s.length();
	}

	public boolean isParameterSsmallestSide() {
		return s.length() <= t.length();
	}

	public Point3d getCorner() {
		return p1;
	}

	public Vector3d getS() {
		return s;
	}

	public Vector3d getT() {
		return t;
	}

	public Point3d getOrigin() {
		return x0;
	}

	public void setSize(double height, double width) {
		setSize(height, width, N);
	}
	
	/**
	 * Set the size of this plane
	 * @param height
	 * @param width
	 * @param N the number of subdivisions to use when drawing as a grid
	 */
	public void setSize(double height, double width, int N) {
		sizex.hold(true);
		sizey.hold(true);
		samples.hold(true);
		sizex.setValue(width);
		sizey.setValue(height);
		samples.setValue(N);
		sizex.hold(false);
		sizey.hold(false);
		samples.hold(false);
		
		s.normalize();
		s.scale(width);
		t.normalize();
		t.scale(height);
		
		// Adjust p1, p2, p3, p4
		computeCorners();

		this.N = N;
		createVertexArray();
		
		computeGrid();
		
		hasChanged = true;
	}
	
	public void setDrawGrid(boolean b) {
		drawgrid.setValue(b);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
	}

	@Override
	public String getName() {
		return "plane";
	}

	@Override
	public Point3d intersect(Ray ray) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector3d getNormal() {
		return n;
	}

	@Override
	public Point3d getPoint() {
		return x0;
	}

	@Override
	public boolean isInside(Point3d p) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public JPanel getControls() {
		VerticalFlowPanel panel = new VerticalFlowPanel();
		panel.setBorder(new TitledBorder("Plane controls"));
		
		panel.add(sizex.getSliderControls());
		panel.add(sizey.getSliderControls());
		panel.add(samples.getSliderControls());
		panel.add(drawgrid.getControls());
		panel.add(ecbMaterial.getControls());
		
		ParameterListener sizel = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				setSize(sizey.getValue(), sizex.getValue(), samples.getValue());
			}
		};
		
		sizex.addParameterListener(sizel);
		sizey.addParameterListener(sizel);
		samples.addParameterListener(sizel);
		
		ParameterListener mlist = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				material = ecbMaterial.getSelected().getMaterial();
			}
		};
		ecbMaterial.setSelected(MaterialTemplate.getMaterialTemplate(material));
		
		ecbMaterial.addParameterListener(mlist);

		return panel.getPanel();
	}

	public double[] getSize() {
		return new double[] { sizex.getValue(), sizey.getValue() };
	}

	public int getSampleCount() {
		return N;
	}

	/**
	 * Not threadsafe. Only use for debugging / prototyping
	 */
	private boolean hasChanged;
	
	public boolean hasChanged() {
		if (hasChanged) {
			hasChanged = false;
			return true;
		}
		
		return false;
	}
}
