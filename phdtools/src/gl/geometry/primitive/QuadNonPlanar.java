package gl.geometry.primitive;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class QuadNonPlanar extends Quad {

	public QuadNonPlanar(Point3d p1, Point3d p2, Point3d p3, Point3d p4) {
		super(p1, p2, p3, p4);
		
		// Construct side vectors
		gamma12.sub(p2, p1);
		gamma23.sub(p3, p2);
		gamma43.sub(p3, p4);
		gamma14.sub(p4, p1);
	}
	
	public Quad[][] subdivide(int Ni, int Nj) {
		Quad[][] grid = new Quad[Ni][Nj];

		// Assemble grid
		for (int i = 0; i < Ni; i++) {
			for (int j = 0; j < Nj; j++) {
				// Construct four corners
				Point3d c1 = coordinate(i, j, Ni, Nj);
				Point3d c2 = coordinate(i, j+1, Ni, Nj);
				Point3d c3 = coordinate(i+1, j+1, Ni, Nj);
				Point3d c4 = coordinate(i+1, j, Ni, Nj);
				grid[i][j] = new Quad(c1, c2, c3, c4);
			}
		}
		
		return grid;
	}
	
	private Vector3d gamma12 = new Vector3d();
	private Vector3d gamma23 = new Vector3d();
	private Vector3d gamma43 = new Vector3d();
	private Vector3d gamma14 = new Vector3d();

	private Point3d pt1 = new Point3d();
	private Point3d pt2 = new Point3d();
	private Point3d pt3 = new Point3d();
	private Point3d pt4 = new Point3d();

	private Point3d coordinate(int i, int j, int Ni, int Nj) {
		// Compute side coordinates
		double lambdai = i / ((double) Ni);
		double lambdaj = j / ((double) Nj);
		
		Point3d c1 = new Point3d();
		c1.scaleAdd((1-lambdai) * (1-lambdaj), p1, c1);
		c1.scaleAdd((1-lambdai) * (lambdaj), p2, c1);
		c1.scaleAdd((lambdai) * (lambdaj), p3, c1);
		c1.scaleAdd((lambdai) * (1-lambdaj), p4, c1);
		
		return c1;
	}
}
