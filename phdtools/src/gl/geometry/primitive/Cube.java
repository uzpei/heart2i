package gl.geometry.primitive;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Simple class representing a cube.
 * @author piuze
 *
 */
public class Cube {

	private double size = 1;
	
	public Cube() {
		// Do nothing
	}
	
	public Cube(double size) {
		this.size = size;
	}

	public static void draw(GLAutoDrawable drawable, double size) {
        GL2 gl = drawable.getGL().getGL2();
		
		gl.glPushMatrix();
		gl.glScaled(size / 2, size /2 , size /2 );
		gl.glBegin(GL2.GL_QUADS);
				
		// Right face (+X)
		gl.glNormal3d(1, 0, 0);
		gl.glTexCoord3d(1.0f, -1.0f, -1.0f);
		gl.glVertex3d(1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, -1.0f);
		gl.glVertex3d(1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, 1.0f);
		gl.glVertex3d(1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, 1.0f);
		gl.glVertex3d(1.0f, -1.0f, 1.0f);

		// Left Face (-X)
		gl.glNormal3d(-1, 0, 0);
		gl.glTexCoord3d(-1.0f, -1.0f, -1.0f);
		gl.glVertex3d(-1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, -1.0f, 1.0f);
		gl.glVertex3d(-1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, 1.0f);
		gl.glVertex3d(-1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, -1.0f);
		gl.glVertex3d(-1.0f, 1.0f, -1.0f);

		// Front Face (+Y)
		gl.glNormal3d(0, 0, 1);
		gl.glTexCoord3d(1.0f, -1.0f, 1.0f);
		gl.glVertex3d(-1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, -1.0f, 1.0f);
		gl.glVertex3d(1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, 1.0f);
		gl.glVertex3d(1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, 1.0f);
		gl.glVertex3d(-1.0f, 1.0f, 1.0f);

		//
		// Back Face (-Y)
		gl.glNormal3d(0, 0, -1);
		gl.glTexCoord3d(1.0f, -1.0f, -1.0f);
		gl.glVertex3d(-1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, -1.0f);
		gl.glVertex3d(-1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, -1.0f);
		gl.glVertex3d(1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, -1.0f, -1.0f);
		gl.glVertex3d(1.0f, -1.0f, -1.0f);

		// Top Face (+Z)
		gl.glNormal3d(0, +1, 0);
		gl.glTexCoord3d(-1.0f, 1.0f, -1.0f);
		gl.glVertex3d(-1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, 1.0f);
		gl.glVertex3d(-1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, 1.0f);
		gl.glVertex3d(1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, -1.0f);
		gl.glVertex3d(1.0f, 1.0f, -1.0f);

		// Bottom Face (-Z)
		gl.glNormal3d(0, -1, 0);
		gl.glTexCoord3d(-1.0f, -1.0f, -1.0f);
		gl.glVertex3d(-1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, -1.0f);
		gl.glVertex3d(1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, 1.0f);
		gl.glVertex3d(1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, -1.0f, 1.0f);
		gl.glVertex3d(-1.0f, -1.0f, 1.0f);

		gl.glEnd();
		gl.glPopMatrix();
	}
	
	public static void draw(GL2 gl, Point3d p0, Vector3d span, Vector3d worldToTexture) {
//        GL2 gl = drawable.getGL().getGL2();
		
		gl.glColor4d(1, 0, 0, 1);
		gl.glDisable(GL2.GL_LIGHTING);
		
        /*
         *  Draw the box from p0 to p0 + span.
         *  Draw the texture in the range
         */
        
        // Construct the texture coordinates
        Point3d t0 = new Point3d(p0);
        t0.x *= worldToTexture.x;
        t0.y *= worldToTexture.y;
        t0.z *= worldToTexture.z;
        
        Vector3d s = new Vector3d(span);
        s.x *= worldToTexture.x;
        s.y *= worldToTexture.y;
        s.z *= worldToTexture.z;
        
//        System.out.println(s);
        
		gl.glPushMatrix();
//		gl.glScaled(size / 2, size /2 , size /2 );
		gl.glBegin(GL2.GL_QUADS);
				
		// Right face (+X)
		gl.glNormal3d(1, 0, 0);
		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z);
		gl.glVertex3d(p0.x + span.x, p0.y, p0.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y + s.y, t0.z);
		gl.glVertex3d(p0.x + span.x, p0.y + span.y, p0.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y + s.y, t0.z + s.z);
		gl.glVertex3d(p0.x + span.x, p0.y + span.y, p0.z + span.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z + s.z);		
		gl.glVertex3d(p0.x + span.x, p0.y, p0.z + span.z);

		// Left Face (-X)
		gl.glNormal3d(-1, 0, 0);
		gl.glTexCoord3d(t0.x, t0.y, t0.z);
		gl.glVertex3d(p0.x, p0.y, p0.z);
		gl.glTexCoord3d(t0.x, t0.y, t0.z + s.z);
		gl.glVertex3d(p0.x, p0.y, p0.z + span.z);
		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z + s.z);
		gl.glVertex3d(p0.x, p0.y + span.y, p0.z + span.z);
		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z);
		gl.glVertex3d(p0.x, p0.y + span.y, p0.z);
		
		// Front Face (+Y)
//		gl.glNormal3d(0, 0, 1);
//		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z + s.z);
//		gl.glVertex3d(p0.x, p0.y, p0.z + span.z);
//		gl.glTexCoord3d(t0.x, t0.y, t0.z + s.z);
//		gl.glVertex3d(p0.x + span.x, p0.y, p0.z + span.z);
//		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z + s.z);
//		gl.glVertex3d(p0.x + span.x, p0.y + span.y, p0.z + span.z);
//		gl.glTexCoord3d(t0.x + s.x, t0.y + s.y, t0.z + s.z);
//		gl.glVertex3d(p0.x, p0.y + span.y, p0.z + span.z);
		
		// Back Face (-Y)
		gl.glNormal3d(0, 0, -1);
//		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z);
		gl.glTexCoord3d(t0.x, t0.y, t0.z);
		gl.glVertex3d(p0.x, p0.y, p0.z);
//		gl.glTexCoord3d(t0.x + s.x, t0.y + s.y, t0.z);
		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z);
		gl.glVertex3d(p0.x, p0.y + span.y, p0.z);
//		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y + s.y, t0.z);
		gl.glVertex3d(p0.x + span.x, p0.y + span.y, p0.z);
//		gl.glTexCoord3d(t0.x, t0.y, t0.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z);
		gl.glVertex3d(p0.x + span.x, p0.y, p0.z);

		// Top Face (+Z)
		gl.glNormal3d(0, +1, 0);
		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z);
		gl.glVertex3d(p0.x, p0.y + span.y, p0.z);
		gl.glTexCoord3d(t0.x, t0.y + s.y, t0.z + s.z);
		gl.glVertex3d(p0.x, p0.y + span.y, p0.z + span.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y + s.y, t0.z + s.z);
		gl.glVertex3d(p0.x + span.x, p0.y + span.y, p0.z + span.z);
		gl.glTexCoord3d(1.0f, t0.y + s.y, t0.z);
		gl.glVertex3d(p0.x + span.x, p0.y + span.y, p0.z);

		// Bottom Face (-Z)
		gl.glNormal3d(0, -1, 0);
		gl.glTexCoord3d(t0.x, t0.y, t0.z);
		gl.glVertex3d(p0.x, p0.y, p0.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z);
		gl.glVertex3d(p0.x + span.x, p0.y, p0.z);
		gl.glTexCoord3d(t0.x + s.x, t0.y, t0.z + s.z);
		gl.glVertex3d(p0.x + span.x, p0.y, p0.z + span.z);
		gl.glTexCoord3d(t0.x, t0.y, t0.z + s.z);
		gl.glVertex3d(p0.x, p0.y, p0.z + span.z);

		gl.glEnd();
		gl.glPopMatrix();
	}

	public static void drawFace(GLAutoDrawable drawable, int index) {
        GL2 gl = drawable.getGL().getGL2();
		
		switch (index) {
		case (1):
			drawxp(gl);
			break;
		case (2):
			drawxn(gl);
			break;
		case (3):
			drawyp(gl);
			break;
		case (4):
			drawyn(gl);
			break;
		case (5):
			drawzp(gl);
			break;
		case (6):
			drawzn(gl);
			break;
		}
	}
	
	private static void drawxp(GL2 gl) {
		// Right face (+X)
		gl.glPushName(1);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(1, 0, 0);
		gl.glTexCoord3d(1.0f, -1.0f, -1.0f);
		gl.glVertex3d(1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, -1.0f);
		gl.glVertex3d(1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, 1.0f);
		gl.glVertex3d(1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, 1.0f);
		gl.glVertex3d(1.0f, -1.0f, 1.0f);
		gl.glEnd();
		gl.glPopName();
	}
	private static void drawxn(GL2 gl) {
		// Left Face (-X)
		gl.glPushName(2);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(-1, 0, 0);
		gl.glTexCoord3d(-1.0f, -1.0f, -1.0f);
		gl.glVertex3d(-1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, -1.0f, 1.0f);
		gl.glVertex3d(-1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, 1.0f);
		gl.glVertex3d(-1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, -1.0f);
		gl.glVertex3d(-1.0f, 1.0f, -1.0f);
		gl.glEnd();
		gl.glPopName();
	}
	private static void drawyp(GL2 gl) {
		// Front Face (+Y)
		gl.glPushName(3);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(0, 0, 1);
		gl.glTexCoord3d(-1.0f, -1.0f, 1.0f);
		gl.glVertex3d(-1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, 1.0f);
		gl.glVertex3d(1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, 1.0f);
		gl.glVertex3d(1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, 1.0f);
		gl.glVertex3d(-1.0f, 1.0f, 1.0f);
		gl.glEnd();
		gl.glPopName();
	}
	private static void drawyn(GL2 gl) {
		// Back Face (-Y)
		gl.glPushName(4);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(0, 0, -1);
		gl.glTexCoord3d(-1.0f, -1.0f, -1.0f);
		gl.glVertex3d(-1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, -1.0f);
		gl.glVertex3d(-1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, -1.0f);
		gl.glVertex3d(1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, -1.0f);
		gl.glVertex3d(1.0f, -1.0f, -1.0f);
		gl.glEnd();
		gl.glPopName();
	}
	private static void drawzp(GL2 gl) {
		// Top Face (+Z)
		gl.glPushName(5);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(0, 1, 0);
		gl.glTexCoord3d(-1.0f, 1.0f, -1.0f);
		gl.glVertex3d(-1.0f, 1.0f, -1.0f);
		gl.glTexCoord3d(-1.0f, 1.0f, 1.0f);
		gl.glVertex3d(-1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, 1.0f);
		gl.glVertex3d(1.0f, 1.0f, 1.0f);
		gl.glTexCoord3d(1.0f, 1.0f, -1.0f);
		gl.glVertex3d(1.0f, 1.0f, -1.0f);
		gl.glEnd();
		gl.glPopName();
	}

	private static void drawzn(GL2 gl) {
		// Bottom Face (-Z)
		gl.glPushName(6);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(0, -1, 0);
		gl.glTexCoord3d(-1.0f, -1.0f, -1.0f);
		gl.glVertex3d(-1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, -1.0f);
		gl.glVertex3d(1.0f, -1.0f, -1.0f);
		gl.glTexCoord3d(1.0f, -1.0f, 1.0f);
		gl.glVertex3d(1.0f, -1.0f, 1.0f);
		gl.glTexCoord3d(-1.0f, -1.0f, 1.0f);
		gl.glVertex3d(-1.0f, -1.0f, 1.0f);
		gl.glEnd();
		gl.glPopName();
	}

	public void drawSide(GLAutoDrawable drawable, int side) {
		
	}
	
	public void draw(GLAutoDrawable drawable) {
		Cube.draw(drawable, size);
	}

	public static void draw(GLAutoDrawable drawable, Point3d translation, double size) {
        GL2 gl = drawable.getGL().getGL2();
		gl.glPushMatrix();
		gl.glTranslated(translation.x, translation.y, translation.z);
		draw(drawable, size);
		gl.glPopMatrix();
	}
	
	public static void drawWireframe(GLAutoDrawable drawable, double size) {
        GL2 gl = drawable.getGL().getGL2();
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
		Cube.draw(drawable, size);
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
	}
	
	public static void drawWireframe(GLAutoDrawable drawable, Point3d origin, Vector3d span) {
        GL2 gl = drawable.getGL().getGL2();
		
		gl.glPushMatrix();
		gl.glTranslated(origin.x + span.x / 2, origin.y + span.y / 2, origin.z + span.z / 2);
		gl.glScaled(span.x, span.y, span.z);
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
		Cube.draw(drawable, 1);
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
		gl.glPopMatrix();
	}
	
	public static void drawPickableWireframe(GLAutoDrawable drawable, Point3d origin, Vector3d span) {
        GL2 gl = drawable.getGL().getGL2();
		
		gl.glPushMatrix();
		gl.glTranslated(origin.x + span.x / 2, origin.y + span.y / 2, origin.z + span.z / 2);
		gl.glScaled(span.x / 2, span.y / 2, span.z / 2);
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
		Cube.drawFace(drawable, 1);
		Cube.drawFace(drawable, 2);
		Cube.drawFace(drawable, 3);
		Cube.drawFace(drawable, 4);
		Cube.drawFace(drawable, 5);
		Cube.drawFace(drawable, 6);
		gl.glPopMatrix();
	}
}
