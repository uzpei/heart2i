package gl.geometry.primitive;

public class FastQuad3d {

	public FastQuad3d(float x, float y, float height, float width, float[] color) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
	}
	
	public float x;
	public float y;
	public float height;
	public float width;
	public float[] color;
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float getHeight() {
		return height;
	}
	
	public float[] getColor() {
		return color;
	}

}
