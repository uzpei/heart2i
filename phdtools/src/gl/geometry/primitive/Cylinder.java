package gl.geometry.primitive;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import tools.geom.MathToolset;

public class Cylinder {

	private final static float PI = (float) Math.PI;

	private final static float PI2 = (float) Math.PI / 2;

	private static float rtodeg = (float) (180f / Math.PI);
	
	private boolean drawArrow = false;

	/**
	 * Assume unit directional vector <code>v</code>
	 * @param gl
	 * @param res
	 * @param x
	 * @param y
	 * @param z
	 * @param r
	 * @param height
	 * @param v
	 */
	public static void display(GL2 gl, int res, float x, float y, float z, float r, float height, Vector3d v) {
		gl.glPushMatrix();
		gl.glTranslated(x, y, z);

		// Extract spherical angles
		double theta = -PI2 +  Math.acos(v.z);
		double phi = Math.atan2(v.y, v.x);
		
		// Orient the cylinder
		gl.glRotated(rtodeg * phi, 0, 0, 1);
		gl.glRotated(rtodeg * theta, 0, 1, 0);
		gl.glRotated(90, 0, 1, 0);
		display(gl, res, x, y, z, r, height);
		gl.glPopMatrix();
	}	
	
	public static void display(GL2 gl, int res, float x, float y, float z, float r, float height) {
		
		gl.glBegin(GL2.GL_TRIANGLE_FAN);

		// bottom circle
		gl.glNormal3f(0, -1, 0);
		gl.glVertex3f(x,y,z);
		for(int i=0; i<=res; i++) {
			gl.glVertex3f(x + ((float) Math.cos((float)i/res * 2 *PI))*r,y, z+ ((float) Math.sin((float)i/res * 2 * PI))*r);
		}
		gl.glEnd();

		gl.glBegin(GL2.GL_TRIANGLE_FAN);
		//top circle
		gl.glNormal3f(0, 1, 0);
		gl.glVertex3f(x,y + height,z);
		for(int i=0; i<=res; i++) 
			gl.glVertex3f(x + ((float) Math.cos((float)i/res * 2 *PI))*r,y+ height, z+ ((float) Math.sin((float)i/res * 2 * PI))*r);
		gl.glEnd();

		/*
		 * Cylinder body
		 */
		
		// Precompute the vertices
		float[][] vs = new float[res+1][3];
		for(int i=0; i<=res; i++) 
		{
			vs[i][0] = ((float)Math.cos((float)i/res * 2 *PI))*r;
			vs[i][1] = 0;
			vs[i][2] = ((float)Math.sin((float)i/res * 2 * PI))*r;
		}

		gl.glBegin(GL2.GL_TRIANGLE_STRIP);
		for(int i=0; i<=res; i++) {
			gl.glNormal3f(vs[i][0] / r, 0, vs[i][2] / r);
			gl.glVertex3f(x+vs[i][0], y+vs[i][1], z+vs[i][2]);
			gl.glVertex3f(x+vs[i][0], y+vs[i][1] + height, z+vs[i][2]);
		}
		gl.glEnd();
	}
	
	private List<Point3f[]> pts = null;
	private List<Point3f> bottomCap = null;
	private List<Point3f> topCap = null;
	private List<Point3f> normals = null;
	private Vector3f lh;
	private Point3f arrowOffset;
	private Point3f centerBottom;
	private Point3f centerTop;
	private float length;
	private Vector3f r00;
	
	/**
	 * Create a cylinder a position <code>p</code> and going in the direction <code>l</code>
	 * @param res
	 * @param p
	 * @param l
	 * @param radius
	 * @param height
	 */
	public Cylinder(int res, Point3d p, Vector3d l, double radius, double height) {
		this(res, MathToolset.tuple3dTo3f(p), MathToolset.tuple3dToVector3f(l), (float) radius, (float) height);
	}
	/**
	 * Create a cylinder a position <code>p</code> and going in the direction <code>l</code>
	 * @param res
	 * @param p
	 * @param l
	 * @param radius
	 * @param height
	 */
	public Cylinder(int res, Point3f p, Vector3f l, float radius, float height) {
		
		this.length = height;
		
		// Construct axis-aligned height vector
		lh = new Vector3f(l);
		lh.normalize();
		
		// Find r0, an arbitrary vector perpendicular to l
		Vector3f r00 = MathToolset.getOrthogonal(lh);
		
		Point3f r0i;
		Vector3f r;
		
		float dpi = (float) (2 * Math.PI / (res));
		Matrix4f m = new Matrix4f();
		Point3f[] pn;
		bottomCap = new ArrayList<Point3f>(res+1);
		topCap = new ArrayList<Point3f>(res+1);
		normals = new ArrayList<Point3f>(res+1);
		pts = new ArrayList<Point3f[]>(res+1);
		for (int i = 0; i <= res; i++) {
			// Create vertex structure
			pn = new Point3f[3];
			
			// Compute radial offset vector
			r = new Vector3f(r00);
			m.set(new AxisAngle4f(l.x, l.y, l.z, -i * dpi));
			m.transform(r);
			r.scale(radius);
			
			// Create bottom part
			r0i = new Point3f();
			r0i.add(p);
			r0i.add(r);
			r0i.scaleAdd(0f * height, lh, r0i);
			pn[0] = r0i;
			bottomCap.add(pn[0]);
			
			// Create top part 
			r0i = new Point3f();
			r0i.add(p);
			r0i.add(r);
			r0i.scaleAdd(1f * height, lh, r0i);
			pn[1] = r0i;
			topCap.add(pn[1]);
			
			// Create normal
			r.normalize();
			pn[2] = new Point3f(r);
			normals.add(pn[2]);

			// Create vertex point
			pts.add(pn);
		}

		centerTop = new Point3f();
		centerTop.scaleAdd(1f * height, lh, p);
		
		centerBottom = new Point3f();
		centerBottom.scaleAdd(0f * height, lh, p);

		// Create arrow
		arrowOffset = new Point3f();
		arrowOffset.scaleAdd(1.05f * height, lh, p);
	}
	
	/**
	 * Create a cylinder a position <code>p</code> and going in the direction <code>l</code>
	 * @param res
	 * @param p
	 * @param l
	 * @param radius
	 * @param height
	 */
	public Cylinder(int res, Point3f p, Vector3f l, float radius, float height, Cylinder previous) {
		
		this.length = height;
		
		// Construct axis-aligned height vector
		lh = new Vector3f(l);
		lh.normalize();
		
		// Find r0, an arbitrary vector perpendicular to l
		if (previous != null) {
			r00 = new Vector3f(previous.r00);
			r00.scaleAdd(-r00.dot(l), l, r00);
		}
		else
			r00 = MathToolset.getOrthogonal(lh);
		
		Point3f r0i;
		Vector3f r;
		
		float dpi = (float) (2 * Math.PI / (res));
		Matrix4f m = new Matrix4f();
		Point3f[] pn;
		bottomCap = new ArrayList<Point3f>(res+1);
		topCap = new ArrayList<Point3f>(res+1);
		normals = new ArrayList<Point3f>(res+1);
		pts = new ArrayList<Point3f[]>(res+1);
		for (int i = 0; i <= res; i++) {
			// Create vertex structure
			pn = new Point3f[3];
			
			// Compute radial offset vector
			r = new Vector3f(r00);
			m.set(new AxisAngle4f(l.x, l.y, l.z, -i * dpi));
			m.transform(r);
			r.scale(radius);
			
			// Create bottom part
			if (previous != null) {
				pn[0] = new Point3f(previous.topCap.get(i));
			}
			else {
				r0i = new Point3f();
				r0i.add(p);
				r0i.add(r);
				r0i.scaleAdd(0f * height, lh, r0i);
				pn[0] = r0i;
			}
			
			bottomCap.add(pn[0]);
			
			// Create top part 
			r0i = new Point3f();
			r0i.add(p);
			r0i.add(r);
			r0i.scaleAdd(1f * height, lh, r0i);
			pn[1] = r0i;
			topCap.add(pn[1]);
			
			// Create normal
			r.normalize();
			pn[2] = new Point3f(r);
			normals.add(pn[2]);

			// Create vertex point
			pts.add(pn);
		}

		centerTop = new Point3f();
		centerTop.scaleAdd(1f * height, lh, p);
		
		centerBottom = new Point3f();
		centerBottom.scaleAdd(0f * height, lh, p);

		// Create arrow
		arrowOffset = new Point3f();
		arrowOffset.scaleAdd(1.05f * height, lh, p);
	}
	
	public Cylinder(List<Point3f[]> geometry) {
		pts = geometry;
	}

	public void displayList(GL2 gl) {
		for (Point3f[] p : pts) {
			gl.glNormal3f(p[2].x, p[2].y, p[2].z);
			gl.glVertex3f(p[0].x, p[0].y, p[0].z);
			gl.glVertex3f(p[1].x, p[1].y, p[1].z);
		}
	}

	public void display(GL2 gl) {
		
		// Draw cylinder body
		gl.glBegin(GL2.GL_TRIANGLE_STRIP);
		for (Point3f[] p : pts) {
			gl.glNormal3f(p[2].x, p[2].y, p[2].z);
			gl.glVertex3f(p[0].x, p[0].y, p[0].z);
			gl.glVertex3f(p[1].x, p[1].y, p[1].z);
		}
		gl.glEnd();

		// Draw top
//		gl.glFrontFace(GL2.GL_CW);
		gl.glBegin(GL2.GL_TRIANGLE_FAN);
		gl.glNormal3f(lh.x, lh.y, lh.z);
		gl.glVertex3d(centerTop.x, centerTop.y, centerTop.z);
		// Use clockwise ordering for top part
		// (could also call gl.glFrontFace(GL2.GL_CW))
		for (int i = 0; i < pts.size(); i++) {
			Point3f[] p = pts.get(pts.size() - i - 1);
			gl.glVertex3f(p[1].x, p[1].y, p[1].z);
		}
		gl.glEnd();

		// Draw bottom
		// Use counter-clockwise ordering for top part
		// (could also call gl.glFrontFace(GL2.GL_CCW))
//		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
		gl.glBegin(GL2.GL_TRIANGLE_FAN);
		gl.glNormal3f(-lh.x, -lh.y, -lh.z);
		gl.glVertex3d(centerBottom.x, centerBottom.y, centerBottom.z);
		for (int i = 0; i < pts.size(); i++) {
			Point3f[] p = pts.get(i);
			gl.glVertex3f(p[0].x, p[0].y, p[0].z);
		}
		gl.glEnd();
//		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
		
		if (drawArrow) {
			// Draw arrow
			float ar = 0.015f;
			gl.glBegin(GL2.GL_TRIANGLE_FAN);
			gl.glNormal3f(lh.x, lh.y, lh.z);
			gl.glVertex3f(arrowOffset.x, arrowOffset.y, arrowOffset.z);
			for (int i = 0; i < pts.size(); i++) {
				Point3f[] p = pts.get(pts.size() - i - 1);
				gl.glVertex3f(p[1].x + ar * length * p[2].x, p[1].y + ar * length * p[2].y, p[1].z + ar * length * p[2].z);
			}
			gl.glEnd();
			
			// Draw arrow cap
			gl.glBegin(GL2.GL_TRIANGLE_FAN);
			gl.glVertex3d(centerTop.x, centerTop.y, centerTop.z);
			for (int i = 0; i < pts.size(); i++) {
				Point3f[] p = pts.get(i);
				gl.glNormal3f(p[2].x, p[2].y, p[2].z);
				gl.glVertex3f(p[1].x + ar * length * p[2].x, p[1].y + ar * length * p[2].y, p[1].z + ar * length * p[2].z);
			}
			gl.glEnd();
		}
	}
	
	public List<Point3f[]> getGeometry() {
		return pts;
	}

	public List<Point3f> getBottomCap() {
		return bottomCap;
	}

	public List<Point3f> getTopCap() {
		return topCap;
	}

	public List<Point3f> getNormals() {
		return normals;
	}

	private void drawCylinder(GL2 gl, GLU glu, Vector3d v, Point3d p0, Point3d p1, double radius, int res, GLUquadric quadric) {
		v.sub(p1,p0);
		double length = v.length();
		
		gl.glPushMatrix();
		gl.glTranslated(p0.x, p0.y, p0.z);

		// Extract spherical angles
		double theta = -Math.PI / 2 +  Math.acos(v.z / v.length());
		double phi = Math.atan2(v.y, v.x);
		
		// Orient the cylinder
		gl.glRotated(rtodeg * phi, 0, 0, 1);
		gl.glRotated(rtodeg * theta, 0, 1, 0);
		gl.glRotated(90, 0, 1, 0);
		glu.gluCylinder(quadric, radius, radius, length, res, res);
		gl.glPopMatrix();
	}
	
	/**
	 * Extrude a list of points and return a list of points to be drawn as a triangle strip
	 * @param points
	 * @return
	 */
	public static List<Cylinder> extrudeToTriangleStrip(List<Point3d> points, int resolution, double extrusion) {
		List<Cylinder> strip = new LinkedList<Cylinder>();
		
		Point3d p0 = points.get(0);
		int i = -1;
		Point3f pf = new Point3f();
		Vector3d l = new Vector3d();
		Vector3f lf = new Vector3f();
		float h;
		
		List<Point3f> previousTop = null;
		
		for (Point3d p : points) {
			i++;
			
			if (i == 0) {
				continue;
			}

			l.sub(p, p0);
			lf.set(l);
			h = lf.length();
			lf.normalize();
			pf.set(p0);
			
			Cylinder cyl = new Cylinder(resolution, pf, lf, (float) extrusion, h);

			// Assemble a cylinder with the "top" of the previous cylinder
			List<Point3f> bottom = previousTop;
			
			if (i == 1)
				bottom = cyl.getBottomCap();
			
			List<Point3f> top = cyl.getTopCap();
			List<Point3f> normals = cyl.getNormals();
			
			// Assemble connected geometry
			int n = cyl.getGeometry().size();
			List<Point3f[]> geometry = new ArrayList<Point3f[]>();
			
			int index;

			index = 0;
			for (index = 0; index < n; index++)
				geometry.add(new Point3f[3]);
			index = 0;
			for (Point3f pt : bottom)
				geometry.get(index++)[0] = pt;
			index = 0;
			for (Point3f pt : top)
				geometry.get(index++)[1] = pt;
			index = 0;
			for (Point3f normal : normals)
				geometry.get(index++)[2] = normal;
			
			
			strip.add(new Cylinder(geometry));
			
			p0 = p;
			
			previousTop = top;
		}
		
		return strip;
	}

	public void setDrawArrow(boolean b) {
		drawArrow = b;
	}
}
