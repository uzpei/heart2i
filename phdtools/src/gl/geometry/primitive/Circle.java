package gl.geometry.primitive;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

public class Circle {

	public static void draw(GLAutoDrawable drawable, double radius, int resolution) {
		GL2 gl = drawable.getGL().getGL2();
		
		double dt = 2 * Math.PI / resolution;
		
		gl.glBegin(GL.GL_LINE_STRIP);
		for (double t = 0; t <= 2 * Math.PI + dt; t += dt) {
			gl.glVertex3d(radius * Math.cos(t), radius * Math.sin(t), 0);
		}
		gl.glEnd();
	}
}
