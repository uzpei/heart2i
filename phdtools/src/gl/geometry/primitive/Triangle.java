package gl.geometry.primitive;

import gl.geometry.GLGeometry;
import gl.renderer.JoglTextRenderer;
import gl.tracing.TraceablePolygon;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Triangle extends TraceablePolygon implements GLGeometry {

	private Point3d p0 = new Point3d();
	private Point3d p1 = new Point3d();
	private Point3d p2 = new Point3d();
	
	public Triangle(Point3d p0, Point3d p1, Point3d p2) {
		this.p0.set(p0);
		this.p1.set(p1);
		this.p2.set(p2);
	}

	private Vector3d v1;
	private Vector3d v0;
	private Vector3d dv;
	private Vector3d n;
	private double d00;
	private double d11;
	private double d01;
	double invdenom;
	
	private void lazyCompute() {
		v1 = new Vector3d();
		v0 = new Vector3d();
		dv = new Vector3d();

		v0.sub(p1, p0);
		v1.sub(p2, p0);
		
		n = new Vector3d();
		n.cross(v1, v0);
		n.normalize();
		
		d00 = v0.dot(v0);
		d11 = v1.dot(v1);
		d01 = v0.dot(v1);

		invdenom = 1 / (d00 * d11 - d01 * d01);
	}

	/**
	 * @param p
	 *            a point in 3D space
	 * @return the [u, v] barycentric coordinates of this point
	 */
	public double[] getBarycentric(Point3d p) {
		if (v0 == null || v1 == null || dv == null) {
			lazyCompute();
		}
		// equation of point p in barycentric coordinates
		// centered at p0
		// P = p0 + v * (p1 - p0) + w * (p2 - p0)
		// P = p0 + u * l1 + v * l2;
		// Adapted from Realtime collision detection by Ericson (2005)
		dv.sub(p, p0);
		double d20 = dv.dot(v0);
		double d21 = dv.dot(v1);

		double v = (d11 * d20 - d01 * d21) * invdenom;
		double w = (d00 * d21 - d01 * d20) * invdenom;

		return new double[] { 1 - v - w, v, w };
	}

	@Override
	public boolean isInside(Point3d p) {
		double[] barycentricCoordinates = getBarycentric(p);
		return     (barycentricCoordinates[0] >= 0)
				&& (barycentricCoordinates[1] >= 0)
				&& (barycentricCoordinates[2] >= 0)
				&& (barycentricCoordinates[0] <= 1)
				&& (barycentricCoordinates[1] <= 1)
				&& (barycentricCoordinates[2] <= 1)
				&& (barycentricCoordinates[0] + barycentricCoordinates[1] + barycentricCoordinates[2] <= 1);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		
		if (v1 == null || v0 == null || dv == null || n == null) {
			lazyCompute();
		}

		GL2 gl = drawable.getGL().getGL2();
		
		gl.glBegin(GL2.GL_TRIANGLES);
		gl.glNormal3d(n.x, n.y, n.z);
		gl.glVertex3d(p2.x, p2.y, p2.z);
		gl.glVertex3d(p1.x, p1.y, p1.z);
		gl.glVertex3d(p0.x, p0.y, p0.z);
		gl.glEnd();
	}
	
	public void displayOverline(GLAutoDrawable drawable) {
		display(drawable);
		
		GL2 gl = drawable.getGL().getGL2();
        gl.glDisable(GL2.GL_LIGHTING);
		gl.glLineWidth(5.0f);

		gl.glBegin(GL2.GL_LINE_STRIP);
		gl.glColor4d(1, 0, 0, 0.8);
		gl.glVertex3d(p0.x, p0.y, p0.z);
		gl.glColor4d(0, 1, 0, 0.8);
		gl.glVertex3d(p1.x, p1.y, p1.z);
		gl.glColor4d(0, 0, 1, 0.8);
		gl.glVertex3d(p2.x, p2.y, p2.z);
		gl.glColor4d(1, 0, 0, 0.8);
		gl.glVertex3d(p0.x, p0.y, p0.z);
		gl.glEnd();
		
		JoglTextRenderer.print3dTextLines("p0", p0);
		JoglTextRenderer.print3dTextLines("p1", p1);
		JoglTextRenderer.print3dTextLines("p2", p2);
		
		Point3d p = new Point3d();
		p.add(p0);
		p.add(p1);
		p.add(p2);
		p.scale(1.0/3.0);
		gl.glColor4d(1, 1, 0, 0.8);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex3d(p.x,p.y,p.z);
		p.add(n);
		gl.glVertex3d(p.x,p.y,p.z);
		gl.glEnd();
	}

	@Override
	public String getName() {
		return "triangle";
	}

	@Override
	public Vector3d getNormal() {
		if (v1 == null || v0 == null || dv == null || n == null) {
			lazyCompute();
		}

		return n;
	}

	@Override
	public Point3d getPoint() {
		return p0;
	}

}
