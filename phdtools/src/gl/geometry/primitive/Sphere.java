package gl.geometry.primitive;

import implicit.implicit3d.functions.ImplicitFunction3D;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.IntParameter;

import com.jogamp.opengl.util.gl2.GLUT;

public class Sphere implements ImplicitFunction3D {
	private static final double maxRadius = 100;
	private IntParameter res = new IntParameter("res", 32, 1, 128);
	private DoubleParameter radius = new DoubleParameter("radius", 1, 0,
			maxRadius);
	private DoubleParameterPoint3d p = new DoubleParameterPoint3d("position",
			new Point3d(), new Point3d(-maxRadius, -maxRadius, -maxRadius),
			new Point3d(maxRadius, maxRadius, maxRadius));
	private static GLUT glut = new GLUT();

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		Point3d pt = p.getPoint3d();
		gl.glPushMatrix();
		gl.glTranslated(pt.x, pt.y, pt.z);
		glut.glutSolidSphere(radius.getValue(), res.getValue(), res.getValue());
		gl.glPopMatrix();
	}

	public Point3d getCenter() {
		return p.getPoint3d();
	}

	/**
	 * TODO: this is not exactly uniform but will have to suffice for now
	 * 
	 * @param n
	 * @return
	 */
	public Point3d[] sampleHemisphere(int n, double minPhi) {
		Point3d[] samples = new Point3d[n];
		double r = radius.getValue();
		Point3d p0 = getCenter();

		Random rand = new Random();
		int i = 0;
		while (i < n) {
			// Vector3d vr = new Vector3d(-1 + 2 * rand.nextDouble(), -1 + 2 *
			// rand.nextDouble(), -1 + 2 * rand.nextDouble());
			Vector3d vr = new Vector3d(-1 + 2 * rand.nextDouble(), -1 + 2
					* rand.nextDouble(), rand.nextDouble());
			vr.normalize();
			
			if (Math.asin(vr.z / 1) < minPhi)
				continue;

			// Nudge by 5% of radius
			// vr.scale(r * ((1 - 2 * 0.05) + 0.05 * 2 * rand.nextDouble()));
			vr.scale(r);
			samples[i] = new Point3d(vr);
			samples[i].add(p0);
			i++;
		}
		
		return samples;
	}

	/***
	 * @param n
	 * @param phi angular deviation from the horizontal plane along the Z-axis
	 * @return
	 */
	public List<Point3d> sampleGreatCircle(int n, double phi) {
		Point3d p0 = getCenter();
		double r = radius.getValue() * Math.cos(phi);

		// Sample n points within a circumference arc
		List<Point3d> samples = new LinkedList<Point3d>();
		double dtheta = 2 * Math.PI / (n);
		for (int i = 0; i < n; i++) {
			Point3d p = new Point3d();
			p.x = p0.x + r * Math.cos(i * dtheta);
			p.y = p0.y + r * Math.sin(i * dtheta);
			p.z = r * Math.sin(phi);
			samples.add(p);
		}

		return samples;
	}

	public Vector3d getNormal(Point3d pt) {
		Vector3d n = new Vector3d(pt);
		n.sub(p.getPoint3d());
		n.normalize();
		// n.negate();

		// n.scale(Math.signum(F(pt)));
		// n.scale(F(pt));
		return n;
	}

	public double computeSurface() {
		double r = getRadius();
		return 4 * Math.PI * r * r;
	}

	public double computeVolume() {
		double r = getRadius();
		return 4 * Math.PI * r * r * r / 3d;
	}

	public double getRadius() {
		return radius.getValue();
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Sphere");
		vfp.add(radius.getSliderControls());
		vfp.add(res.getSliderControls());
		// vfp.add(p.getControls());
		return vfp.getPanel();
	}

	@Override
	public double F(double x, double y, double z) {
		// Return distance from the point to the centroid
		Point3d pt = p.getPoint3d();
		double rx = x - pt.x;
		double ry = y - pt.y;
		double rz = z - pt.z;
		double r = Math.sqrt(rx * rx + ry * ry + rz * rz);

		return r - radius.getValue();
	}

	@Override
	public double F(Point3d pt) {
		return F(pt.x, pt.y, pt.z);
	}

	@Override
	public Vector3d dF(Point3d pt) {
		return getNormal(pt);
	}

	@Override
	public String getName() {
		return "Metaball";
	}

	@Override
	public boolean hasChanged() {
		return false;
	}

	public double getCircumference() {
		return 2 * Math.PI * getRadius();
	}
}
