package gl.geometry;

import javax.media.opengl.GLAutoDrawable;

public interface GLGeometry {
    
    /**
     * Initialize the geometry.  
     * @param drawable
     */
    public void init(GLAutoDrawable drawable);

    /**
     * Displays the contents of this geometry.
     * @param drawable
     */
    public void display(GLAutoDrawable drawable);

    public String getName();
}
