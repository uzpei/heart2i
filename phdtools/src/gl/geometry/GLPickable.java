package gl.geometry;

import javax.media.opengl.GLAutoDrawable;

public interface GLPickable {

	/**
	 * Draw the scene in a simplified way and ready for OpenGL name picking. All scene objects
	 * should be hierarchically named (glPushName and glPopName).
	 * @param drawable
	 */
	public void renderInSelectionMode(GLAutoDrawable drawable);
	
	/**
	 * Process a name hit in the scene. <code>name</code> can be null if no objects were selected. 
	 * @param name
	 */
	public void processHit(Integer name);
}
