package gl.tracing;

import java.awt.Color;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Ray {
	private Point3d origin = new Point3d(0, 0, 0);
	
	private Vector3d direction = new Vector3d(0,0,-1);
		
	public Ray(Point3d origin, Vector3d direction) {
		this.origin.set(origin);
		this.direction.set(direction);
		this.direction.normalize();
	}
	
	public Ray(Point3d origin, Point3d target) {
		this.origin.set(origin);
		this.direction.sub(target,origin);
		this.direction.normalize();
	}
	
	public Point3d getOrigin() {
		return origin;
	}
	
	public Vector3d getDirection() {
		return direction;
	}

	public void display(GLAutoDrawable drawable, double length, Color originColor, Color endColor) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glLineWidth(1f);
		
		gl.glBegin(GL2.GL_LINES);
		gl.glColor4d(originColor.getRed() / 255.0, originColor.getGreen() / 255.0, originColor.getBlue() / 255.0, originColor.getAlpha() / 255.0);
		gl.glVertex3d(origin.x, origin.y, origin.z);
		Point3d p = new Point3d();
		p.scaleAdd(length, direction, origin);
		gl.glColor4d(endColor.getRed() / 255.0, endColor.getGreen() / 255.0, endColor.getBlue() / 255.0, endColor.getAlpha() / 255.0);
		gl.glVertex3d(p.x, p.y, p.z);
		gl.glEnd();
	}
}
