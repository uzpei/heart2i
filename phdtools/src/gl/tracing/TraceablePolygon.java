package gl.tracing;

import gl.geometry.Polygon;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Abstract class for convex polygonal, traceable objects
 * @author piuze
 */
public abstract class TraceablePolygon implements Polygon, Traceable {
	
	@Override
	public Point3d intersect(Ray ray) {
		Vector3d n = getNormal();
		Point3d p = new Point3d(getPoint());
		Vector3d dray = new Vector3d();
		dray.sub(p, ray.getOrigin());
		
		// Rays facing the polygon will dot to a negative number
		double denom = -ray.getDirection().dot(n);
		double d;
		
		// Cull face
		if (denom <= 1e-8) 
			d = Double.POSITIVE_INFINITY;
		else
			d = -dray.dot(n) / denom;
		
//		System.out.println(denom);
//		System.out.println(d);
		
		// Equation for line: p = origin + d * v
		p.scaleAdd(d, ray.getDirection(), ray.getOrigin());
		
		return isInside(p) ? p : null;
	}
}
