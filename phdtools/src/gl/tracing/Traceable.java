package gl.tracing;

import gl.geometry.GLGeometry;

import javax.vecmath.Point3d;

/**
 * Interface or traceable objects
 * @author piuze
 *
 */
public interface Traceable {
	/**
	 * Intersect the given ray with this object.
	 * @param ray
	 * @return the position of the intersection or null if the ray does not intersect this object
	 */
	public Point3d intersect(Ray ray);
}
