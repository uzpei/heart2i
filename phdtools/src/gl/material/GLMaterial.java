package gl.material;

import java.util.Arrays;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

/**
 * Parameters for common gl materials, defining  ambient, diffuse, specular, shininess
 * @author piuze
 *
 */
public class GLMaterial {
	public float[] ambient = new float[4];
	public float[] diffuse = new float[4];
	public float[] specular = new float[4];
	public float shininess = 0f;

	/**
	 * enum wrapper around available GLMaterial templates
	 * @author piuze
	 */
	public enum MaterialTemplate { 
		PURPLE(GLMaterial.PURPLE), 
		LIGHT(GLMaterial.LIGHT), 
		BRASS(GLMaterial.BRASS),
		PEARL(GLMaterial.PEARL),
		POLISHED(GLMaterial.POLISHED),
		CHROME(GLMaterial.CHROME),
		EMERALD(GLMaterial.EMERALD),
		RUBY(GLMaterial.RUBY),
		TURQUOISE(GLMaterial.TURQUOISE),
		SOFTWHITE(GLMaterial.SOFTWHITE),
		TRANSPARENT_DARK(GLMaterial.TRANSPARENT_DARK),
		TRANSPARENT_LIGHT(GLMaterial.TRANSPARENT_LIGHT);
	
		private GLMaterial material;
		
		private MaterialTemplate(GLMaterial m) {
			material = m;
		}
		
		public GLMaterial getMaterial() {
			return material;
		}
		
		public static MaterialTemplate getMaterialTemplate(GLMaterial material) {
			if (material == GLMaterial.PURPLE)
				return MaterialTemplate.PURPLE;
			else if (material == GLMaterial.LIGHT)
				return MaterialTemplate.LIGHT;
			else if (material == GLMaterial.BRASS)
				return MaterialTemplate.BRASS;
			else if (material == GLMaterial.POLISHED)
				return MaterialTemplate.POLISHED;
			else if (material == GLMaterial.CHROME)
				return MaterialTemplate.CHROME;
			else if (material == GLMaterial.EMERALD)
				return MaterialTemplate.EMERALD;
			else if (material == GLMaterial.RUBY)
				return MaterialTemplate.RUBY;
			else if (material == GLMaterial.TURQUOISE)
				return MaterialTemplate.TURQUOISE;
			else if (material == GLMaterial.SOFTWHITE)
				return MaterialTemplate.SOFTWHITE;
			else if (material == GLMaterial.TRANSPARENT_DARK)
				return MaterialTemplate.TRANSPARENT_DARK;
			else if (material == GLMaterial.TRANSPARENT_LIGHT)
				return MaterialTemplate.TRANSPARENT_LIGHT;
			
			return null;
		}
	};
	
	/*
	 * Custom
	 */
	public final static GLMaterial PURPLE = new GLMaterial(
			new float[] { 0.5f, 0.5f, 0.65f, 1f },
			new float[] { 0.5f, 0.5f, 0.65f, 1f },
			new float[] { 0f, 0f, 0f, 1f },
			0f);
	
	public final static GLMaterial LIGHT = new GLMaterial(
			new float[] { 0.75f, 0.75f, 0.05f, 0.25f },
			new float[] { 0.6f, 0.6f, 0.076f, 0.25f },
			new float[] { 0.728f, 0.728f, 0.2f, 0.25f },
			76.8f);

	/*
	 * From Modeling Material Smoothness at opengl.org
	 * Ambient, diffuse, specular.
	 */
	public final static GLMaterial BRASS = new GLMaterial(
			new float[] { 0.329f, 0.224f, 0.028f, 1f },
			new float[] { 0.780f, 0.569f, 0.114f, 1f },
			new float[] { 0.992f, 0.941f, 0.808f, 1f },
			27.89f);

	public final static GLMaterial PEARL = new GLMaterial(
			new float[] { 0.250f, 0.207f, 0.207f, 0.922f },
			new float[] { 1.000f, 0.829f, 0.829f, 0.922f },
			new float[] { 0.297f, 0.297f, 0.297f, 0.922f },
			11.264f);

	public final static GLMaterial POLISHED = new GLMaterial(
			new float[] { 0.250f, 0.250f, 0.250f, 1f },
			new float[] { 0.400f, 0.400f, 0.400f, 1f },
			new float[] { 0.775f, 0.775f, 0.775f, 1f },
			76.8f);

	public final static GLMaterial CHROME = new GLMaterial(
			new float[] { 0.250f, 0.250f, 0.250f, 1f },
			new float[] { 0.400f, 0.400f, 0.400f, 1f },
			new float[] { 0.775f, 0.775f, 0.775f, 1f },
			76.8f);

	public final static GLMaterial EMERALD = new GLMaterial(
			new float[] { 0.022f, 0.175f, 0.022f, 0.55f },
			new float[] { 0.076f, 0.614f, 0.076f, 0.55f },
			new float[] { 0.633f, 0.728f, 0.633f, 0.55f },
			76.8f);

	public final static GLMaterial RUBY = new GLMaterial(
			new float[] { 0.175f, 0.012f, 0.012f, 0.55f },
			new float[] { 0.614f, 0.041f, 0.041f, 0.55f },
			new float[] { 0.728f, 0.627f, 0.627f, 0.55f },
			76.8f);

	public final static GLMaterial TURQUOISE = new GLMaterial(
			new float[] { 0.100f, 0.187f, 0.175f, 0.8f },
			new float[] { 0.396f, 0.742f, 0.691f, 0.8f },
			new float[] { 0.297f, 0.308f, 0.307f, 0.8f },
			76.8f);

	public final static GLMaterial SOFTWHITE = new GLMaterial(
			new float[] { 1, 1, 1, 1 },
			new float[] { 1, 1, 1, 1 },
			new float[] { 1, 1, 1, 1 },
			76.8f);

	// a, d, s, shinyness
	public final static GLMaterial TRANSPARENT_DARK = new GLMaterial(
			new float[] { 0, 0, 0, 0.6f },
			new float[] { 0, 0, 0, 0.6f },
			new float[] { 0, 0, 0, 0.6f },
			0f);

	public final static GLMaterial TRANSPARENT_LIGHT = new GLMaterial(
			new float[] { 1, 1, 1, 0.6f },
			new float[] { 1, 1, 1, 0.6f },
			new float[] { 1, 1, 1, 0.6f },
			0f);

	public GLMaterial(float[] ambient, float[] diffuse, float[] specular, float shininess) {
		this.ambient = Arrays.copyOf(ambient, ambient.length);
		this.diffuse = Arrays.copyOf(diffuse, diffuse.length);
		this.specular = Arrays.copyOf(specular, specular.length);
		this.shininess = shininess;
	}

	public GLMaterial(float ambient, float diffuse, float specular, float shininess) {
		this.ambient = new float[] { ambient, ambient, ambient, 1 };
		this.diffuse = new float[] { diffuse, diffuse, diffuse, 1 };
		this.specular = new float[] { specular, specular, specular, 1 };
		this.shininess = shininess;
	}
	
	public GLMaterial(GLMaterial other) {
		this.ambient = Arrays.copyOf(other.ambient, other.ambient.length);
		this.diffuse = Arrays.copyOf(other.diffuse, other.diffuse.length);
		this.specular = Arrays.copyOf(other.specular, other.specular.length);
		this.shininess = other.shininess;
	}
	
	public void apply(GL2 gl) {
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT, ambient, 0);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, diffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, specular, 0);
		gl.glMaterialf(GL2.GL_FRONT_AND_BACK, GL2.GL_SHININESS, shininess);
	}
	
	public static void apply(GL2 gl, GLMaterial material) {
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT, material.ambient, 0);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, material.diffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, material.specular, 0);
		gl.glMaterialf(GL2.GL_FRONT_AND_BACK, GL2.GL_SHININESS, material.shininess);
	}
}
