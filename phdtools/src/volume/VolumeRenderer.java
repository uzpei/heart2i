
package volume;

import extension.matlab.MatlabIO;
import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.nio.ByteBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import swing.component.JColorMap;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.IntParameterPoint3i;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;
import voxel.VoxelBox;

import com.jogamp.opengl.util.glsl.ShaderState;

public class VolumeRenderer implements GLObject, Interactor {
	static boolean DEVELOPPER_MODE = false; 
	
	private int frameBuffer = -1;
	private int renderBuffer = -1;
	private int volumeTexture = -1;
	private int maskTexture = -1;
	private int backfaceTexture = -1;
	private int imageTexture = -1; 
	
	private boolean requestTextureUpdate = false;

	private int CLAMP_MODE = GL2.GL_CLAMP_TO_EDGE;
//	private int TEX_FILTER = GL2.GL_LINEAR;
	private int TEX_ENV = GL2.GL_REPLACE;
//	private int TEX_ENV = GL2.GL_BLEND;

//	private int activeTexture = GL2.GL_TEXTURE0;
//	private int activeSampler2D = 0;
//	private int volumeActiveSampler3D = 1;
//	private int maskActiveSampler3D = 2;
	private int activeSampler2D = GL2.GL_TEXTURE0;
	private int volumeActiveSampler3D = GL2.GL_TEXTURE1;
	private int maskActiveSampler3D = GL2.GL_TEXTURE2;

	private int[] tmp1 = new int[1];
	
	private ShaderState rayCasterShader;

	private JHistogram histogram;
	
//	private Dimension size;
	
	private VoxelBox voxelBox = null;
	private IntensityVolume volume = null;
	private IntensityVolume partialVolume = null;

	private IntensityVolumeMask oneMask = null;
	private IntensityVolumeMask mask = null;
	private IntensityVolumeMask partialMask = null;

	private boolean volumeInitializationRequested = false;

	private boolean volumeInitializationRequestTriggeredByHistogram = false;

	private boolean volumeInitializationRequestTriggeredByColorMap = false;

	private BooleanParameter draw = new BooleanParameter("draw", true);

	private BooleanParameter linearFilter3D = new BooleanParameter("3D filter", false);

	private BooleanParameter linearFilter2D = new BooleanParameter("2D linear filter", true);

	private BooleanParameter useMask = new BooleanParameter("use mask", true);

	private BooleanParameter statcolor = new BooleanParameter("stat-colors", false);

	private BooleanParameter drawBack = new BooleanParameter("backface", false);

	private BooleanParameter drawFront = new BooleanParameter("frontface", false);

	private BooleanParameter drawRays = new BooleanParameter("rays", false);

	private BooleanParameter highResolution = new BooleanParameter("high", false);

	private BooleanParameter lowResolution = new BooleanParameter("low", false);

	private ParameterListener voxelBoxListener = new ParameterListener() {
		
		@Override
		public void parameterChanged(Parameter parameter) {
			updateVoxelBox();
		}
	};
	
	private final static double DEFAULT_STEP_SIZE = 0.005;
	private final static double DEFAULT_SCALE = 3;
	private final static double DEFAULT_EMISSIVITY = 0.15;
	private final static int DEFAULT_DEPTH = 300;

	private final static double HR_STEP_SIZE = 0.001;
	private final static double HR_SCALE = 3;
	private final static double HR_EMISSIVITY = 0.002;
	private final static int HR_DEPTH = 1000;

	private DoubleParameter stepSize = new DoubleParameter("step size", DEFAULT_STEP_SIZE, HR_STEP_SIZE, 1);

	private DoubleParameter scale = new DoubleParameter("intensity scale", DEFAULT_SCALE, 0, 10);
	
	private DoubleParameter emissivity = new DoubleParameter("particle absorption", DEFAULT_EMISSIVITY, 0, 1);

	private DoubleParameter epsilon = new DoubleParameter("intensity epsilon", 0, 0, 1);
	
	private JColorMap colorMap;

//	private IntParameterPoint3i colorPos = new IntParameterPoint3i("color (+)", new Point3i(90, 54, 14), new Point3i(), new Point3i(255,255,255)); 
//	private IntParameterPoint3i colorNeg = new IntParameterPoint3i("color (-)", new Point3i(57, 87, 167), new Point3i(), new Point3i(255,255,255)); 

	private Color colorPos = new Color(90, 54, 14);
	private Color colorNeg = new Color(57, 87, 167);
	
	private final static IntParameterPoint3i vsize = new IntParameterPoint3i("sample volume size", new Point3i(100, 50, 10), new Point3i(1,1,1), new Point3i(128,128,128)); 

	private final static IntParameter parameter1 = new IntParameter("parameter 1", 0, 1, 100); 

	private IntParameter depth = new IntParameter("ray depth", DEFAULT_DEPTH, 1, HR_DEPTH);

	private Timer volumeInitializationTimer;
	
	/**
	 * Requests for refreshing the volume will wait for this amount of time (in ms)
	 * The timer coalesces so no timer stackign will occur.
	 */
	private final static int VOLUME_REFRESH_INTERVAL = 1;
	
	private JoglRenderer renderer;
	private boolean rendererSet = false;
	
	public VolumeRenderer() {
		this(null);
		// Wait for set(JoglRenderer renderer)
	}
	
	public VolumeRenderer(JoglRenderer renderer) {
		if (renderer != null)
			setRenderer(renderer);
		
		initialize();
		
		setLowResolution();
	}

	private void initialize() {
		createTimer();
		createHistogram();
		createColorMap();
		
		histogram.setColorMap(colorMap);
	}
	
	public void setRenderer(JoglRenderer renderer) {
		if (rendererSet) {
			throw new RuntimeException("Renderer is already set for this volume renderer.");
		}
		
		rendererSet = true;
		
		this.renderer = renderer;
	}

	private void createColorMap() {
		colorMap = new JColorMap("Color map", colorNeg, colorPos, Color.BLACK);
		ActionListener cl = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVolumeInitializationRequest(false, true);
			}
		};
		
		colorMap.addChangeListener(cl);
		
	}
	
	private void createTimer() {
		volumeInitializationTimer = new Timer(VOLUME_REFRESH_INTERVAL, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				volumeInitializationRequested = true;
			}
		});
		volumeInitializationTimer.setCoalesce(true);
		volumeInitializationTimer.setRepeats(false);
	}
			
	private void createHistogram() {
		histogram = new JHistogram();
		histogram.addThresholdedIntervalListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVolumeInitializationRequest(true, false);
			}
		});
	}
	
	private void setVolumeInitializationRequest(boolean triggeredByHistogram, boolean triggeredByColorMap) {
		if (volumeInitializationRequested || volumeInitializationRequestTriggeredByColorMap || volumeInitializationRequestTriggeredByHistogram) return;
		
		// Launch a coalescing timer 
		volumeInitializationRequestTriggeredByColorMap = triggeredByColorMap;
		volumeInitializationRequestTriggeredByHistogram = triggeredByHistogram;
		volumeInitializationTimer.restart();
	}
	
	private void updateColorMap() {
		if (mask != null && statcolor.getValue()) {
			double[] minmax = volume.getMinMax(mask);
			double mean = volume.getMean(mask);
			double std = volume.getStandardDeviation(mask);
			colorMap.setMinMax(minmax[0], minmax[1], mean, std);
		}
		else {
			double[] minmax = volume.getminMax();
			colorMap.setMinMax(minmax[0], minmax[1]);
		}

		// Update bound histogram
//		histogram.updateHistogram();
	}
	
	private Dimension initSize;
	
	@Override
	public void init(GLAutoDrawable drawable) {
		if (isInitialized()) return;

		if (renderer != null)
			initSize = new Dimension(renderer.getDimension());
		else
			initSize = new Dimension();
		
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glEnable(GL2.GL_CULL_FACE);

		// Create the shader program
		// No need to create it more than once
		if (rayCasterShader == null) 
			rayCasterShader = JoglRenderer.createProgram(gl, "maskedraycast");

		// Create the frame buffer
		frameBuffer = genbindFrameBuffer(drawable);

		// Create and init the backface texture
		// UPdate size this
		backfaceTexture = genbindTexture2D(drawable);
			
		// Attach the texture image to a framebuffer object... in this case to
		// GL_COLOR_ATTACHMENT0
		// 0 is the mipmap level (always)
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0,
				GL2.GL_TEXTURE_2D, backfaceTexture, 0);

		/*
		 * Final image buffer
		 */
		imageTexture = genbindTexture2D(drawable);

		// Update size
		renderBuffer = genbindRenderBuffer(drawable);

		setInitialized(true);
	}

	private void updateVoxelBox() {
		// Assume rest of data hasn't changed
		setVolume(volume, mask, voxelBox);
	}

	public void setVolume(IntensityVolume volume, IntensityVolumeMask mask) {
		setVolume(volume, mask, null);
	}
	
	public void setVolume(IntensityVolume volume, IntensityVolumeMask mask, VoxelBox box) {

		if (volume == null) return;
		
		boolean volumeChanged = false;
		
		// Only update historam and color scale if necessary
		if (this.volume != volume) {
			volumeChanged = true;
			this.volume = volume;
			
			voxelBox = box;
			
			if (voxelBox != null)
				voxelBox.addParameterListener(voxelBoxListener);
			
			this.mask = mask;
//			this.oneMask = new IntensityVolumeMask(mask.getDimension());
//			this.oneMask.fillWith(1d);
		}
		
		partialVolume = volume;
		partialMask = mask;

		if (voxelBox != null) {
			partialVolume = volume.extract(voxelBox);
			
			if (mask != null) {
				partialMask = mask.extract(voxelBox);
			}
		}

		if (volumeChanged) {
			// Update color map first since it will be passed to the histogram
			setHistogram();
			updateColorMap();
			histogram.updateHistogram();
		}
		
		// Force a refresh
		setVolumeInitializationRequest(false, false);
	}
	
	public void setVolume(IntensityVolume volume) {
		setVolume(volume, null, null);
	}
	
	public static float[] clear = new float[] { 0f, 0f, 0f, 0f };
	
	private boolean reloadShaderRequested = false;
	
	private boolean displayBlocked = false;
	
	@Override
	public void display(GLAutoDrawable drawable) {
		if (displayBlocked)
			return;
		
		if (reloadShaderRequested) {
			// Prevent rendering until we are done loading the shader
			reloadShaderRequested = false;
//			displayBlocked = true;
			rayCasterShader = JoglRenderer.createProgram(drawable.getGL().getGL2(), "improvedraycast");
			
//			try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
			
//			displayBlocked = false;
			return;
		}
		
		if (initSize.width != renderer.getDimension().width || initSize.height != renderer.getDimension().height) {
			System.err.println("WARNING: Canvas dimension mismatch in volume rendering: " + initSize + " | " + renderer.getDimension() + "\nEntering unknown territory. Will at least try to reinitialize the shader.");
			setInitialized(false);
		}
		
		if (!isInitialized()) {
			init(drawable);
		}
		
		// Create a new partial volume if needed
		if (volumeInitializationRequested) {
			volumeTexture = updateVolumeData(drawable, partialVolume, volumeActiveSampler3D, volumeTexture);
			maskTexture = updateVolumeData(drawable, partialMask, maskActiveSampler3D, maskTexture);

			volumeInitializationRequested = false;
		}
		
		volumeInitializationRequestTriggeredByHistogram = false;
		volumeInitializationRequestTriggeredByColorMap = false;
		
		// Texture update: change in 3D interpolation scheme
		if (requestTextureUpdate) {
			setTexture3DParam(drawable, volumeActiveSampler3D);
			setTexture3DParam(drawable, maskActiveSampler3D);
		}

		if (!draw.getValue() || partialVolume == null) {
			return;
		}

		// Begin drawing geometry
		GL2 gl = drawable.getGL().getGL2();		
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glPushMatrix();
//		gl.glScaled(2,2,2);
		
		// Enable the render buffers
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, frameBuffer);
		gl.glBindRenderbuffer(GL2.GL_RENDERBUFFER, renderBuffer);

		renderBackFace(drawable);
		renderRayCast(drawable);

		// Disable the render buffer
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
		
		// Render buffer to screen
//		gl.glClearColor(clear[0], clear[1], clear[2], clear[3]);
//		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glEnable(GL.GL_TEXTURE_2D);
		
		gl.glActiveTexture(activeSampler2D);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, imageTexture);

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glPushMatrix();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glPushMatrix();

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glViewport(0, 0, renderer.getDimension().width, renderer.getDimension().height);
		JoglRenderer.glu.gluOrtho2D(0, 1, 0, 1);
//		JoglRenderer.glu.gluOrtho2D(-0.5, 0.5, -0.5, 0.5);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glDisable(GL2.GL_DEPTH_TEST);
		drawFullscreenQuad(gl);
		gl.glEnable(GL2.GL_DEPTH_TEST);

		gl.glDisable(GL2.GL_TEXTURE_2D);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glPopMatrix();
        
        gl.glPopMatrix();
	}

	void drawQuads(GL2 gl, float p)
	{
		gl.glPushMatrix();
		
		Point3d dims = MathToolset.tuple3iToPoint3d(new Point3i(volume.getDimension()));
		Point3d pdims = MathToolset.tuple3iToPoint3d(new Point3i(partialVolume.getDimension()));
		
//		System.out.println(dims);
//		System.out.println(pdims);
		
		double dm = Math.max(Math.max(dims.x, dims.y), dims.z);
	
		// Preserve aspect ratio
		gl.glScaled(dims.x / dm, dims.y / dm, dims.z / dm);
		
		// Preserve true size
		gl.glScaled(pdims.x / dims.x, pdims.y / dims.y, pdims.z / dims.z);
//		gl.glScaled(pdims.x, pdims.y, pdims.z);
		
//		voxelBox.get
		
		float x = p;
		float y = p;
		float z = p;
		
		gl.glBegin(GL2.GL_QUADS);
		/* Back side */
		gl.glNormal3f(0.0f, 0.0f, -1.0f);
		vertex(gl,0.0f, 0.0f, 0.0f);
		vertex(gl,0.0f, y, 0.0f);
		vertex(gl,x, y, 0.0f);
		vertex(gl,x, 0.0f, 0.0f);

		/* Front side */
		gl.glNormal3f(0.0f, 0.0f, 1.0f);
		vertex(gl,0.0f, 0.0f, z);
		vertex(gl,x, 0.0f, z);
		vertex(gl,x, y, z);
		vertex(gl,0.0f, y, z);

		/* Top side */
		gl.glNormal3f(0.0f, 1.0f, 0.0f);
		vertex(gl,0.0f, y, 0.0f);
		vertex(gl,0.0f, y, z);
	    vertex(gl,x, y, z);
		vertex(gl,x, y, 0.0f);

		/* Bottom side */
		gl.glNormal3f(0.0f, -1.0f, 0.0f);
		vertex(gl,0.0f, 0.0f, 0.0f);
		vertex(gl,x, 0.0f, 0.0f);
		vertex(gl,x, 0.0f, z);
		vertex(gl,0.0f, 0.0f, z);

		/* Left side */
		gl.glNormal3f(-1.0f, 0.0f, 0.0f);
		vertex(gl,0.0f, 0.0f, 0.0f);
		vertex(gl,0.0f, 0.0f, z);
		vertex(gl,0.0f, y, z);
		vertex(gl,0.0f, y, 0.0f);

		/* Right side */
		gl.glNormal3f(1.0f, 0.0f, 0.0f);
		vertex(gl,x, 0.0f, 0.0f);
		vertex(gl,x, y, 0.0f);
		vertex(gl,x, y, z);
		vertex(gl,x, 0.0f, z);
		gl.glEnd();
		
		gl.glPopMatrix();
	}

	private void renderBackFace(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, backfaceTexture, 0);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );
		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_FRONT);

		drawQuads(gl, 1);

		gl.glDisable(GL.GL_CULL_FACE);
	}
	
	private void renderRayCast(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		// Enable shader
		rayCasterShader.useProgram(gl, true);

		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, imageTexture, 0);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );

		// Assign sampler IDs
		// note: those are NOT the same as texture IDs
		// they are 0, 1, ..., n corresponding to the GLSL texture IDs
		// they are the glActiveTexture(CurrentTextureUnit + SamplerID)
		// Set the texture units
		setUniform1i(gl, rayCasterShader, "tex", 0);
		setUniform1i(gl, rayCasterShader, "volume_tex", 1);
		setUniform1i(gl, rayCasterShader, "mask_tex", 2);
		
		// Bind the samplers
		gl.glActiveTexture(volumeActiveSampler3D);
		gl.glBindTexture(GL2.GL_TEXTURE_3D, volumeTexture);
		gl.glBindSampler(0, volumeTexture);
		
		gl.glActiveTexture(maskActiveSampler3D);
		gl.glBindTexture(GL2.GL_TEXTURE_3D, maskTexture);
		gl.glBindSampler(1, maskTexture);

		gl.glActiveTexture(activeSampler2D);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, backfaceTexture);
		gl.glBindSampler(2, backfaceTexture);
		
		setUniform1f(gl, rayCasterShader, "stepSize", stepSize.getFloatValue());
		setUniform1f(gl, rayCasterShader, "scale", scale.getFloatValue());
		setUniform1f(gl, rayCasterShader, "epsilon", epsilon.getFloatValue());
		
//		int rdepth = VectorToolset.roundInt(depth.getValue() / stepSize.getValue());
		setUniform1i(gl, rayCasterShader, "rayDepth", depth.getValue());
		
		setUniform1f(gl, rayCasterShader, "emissivity", emissivity.getFloatValue());
		setUniform1i(gl, rayCasterShader, "drawBack", drawBack.getValue() ? 1 : 0);
		setUniform1i(gl, rayCasterShader, "drawFront", drawFront.getValue() ? 1 : 0);
		setUniform1i(gl, rayCasterShader, "drawRays", drawRays.getValue() ? 1 : 0);
		setUniform1i(gl, rayCasterShader, "useMask", useMask.getValue() ? 1 : 0);

		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_BACK);

//		setTextureParam(drawable);
		
		drawQuads(gl, 1f);

		gl.glDisable(GL.GL_CULL_FACE);

		// Disable shader
		rayCasterShader.useProgram(gl, false);
	}
	
	private void setUniform1i(GL2 gl, ShaderState state, String uniform, int value) {
		int id = state.getUniformLocation( gl, uniform);
		gl.glUniform1i(id, value);
//		System.out.println(id);
	}

	private void setUniform1f(GL2 gl, ShaderState state, String uniform, float value) {
		int id = state.getUniformLocation( gl, uniform);
		gl.glUniform1f(id, value);
//		System.out.println(id);
	}

	private void drawFullscreenQuad(GL2 gl) {
		gl.glBegin(GL2.GL_QUADS);

		gl.glTexCoord2f(0, 0);
		gl.glVertex2f(0, 0);
		gl.glTexCoord2f(1, 0);
		gl.glVertex2f(1, 0);
		gl.glTexCoord2f(1, 1);
		gl.glVertex2f(1, 1);
		gl.glTexCoord2f(0, 1);
		gl.glVertex2f(0, 1);

//		gl.glTexCoord2f(0, 0);
//		gl.glVertex2f(-0.5f, -0.5f);
//		gl.glTexCoord2f(1, 0);
//		gl.glVertex2f(0.5f, -0.5f);
//		gl.glTexCoord2f(1, 1);
//		gl.glVertex2f(0.5f, 0.5f);
//		gl.glTexCoord2f(0, 1);
//		gl.glVertex2f(-0.5f, 0.5f);

		gl.glEnd();

	}

	private void vertex(GL2 gl, float x, float y, float z) {
		gl.glColor3f(x,y,z);
		gl.glMultiTexCoord3f(GL2.GL_TEXTURE1, x, y, z);
		gl.glVertex3f(x - 0.5f, y - 0.5f, z - 0.5f);

	}
	

	private int genbindFrameBuffer(GLAutoDrawable drawable) {
		drawable.getGL().getGL2().glGenFramebuffers(1, tmp1, 0);
		drawable.getGL().getGL2().glBindFramebuffer(GL2.GL_FRAMEBUFFER, tmp1[0]);
		return tmp1[0];
	}

	private int genbindRenderBuffer(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		drawable.getGL().getGL2().glGenRenderbuffers(1, tmp1, 0);

		drawable.getGL().getGL2().glBindRenderbuffer(GL2.GL_RENDERBUFFER, tmp1[0]);
		gl.glRenderbufferStorage(GL2.GL_RENDERBUFFER, GL2.GL_DEPTH_COMPONENT, renderer.getDimension().width, renderer.getDimension().height);
		
		gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER, GL2.GL_DEPTH_ATTACHMENT, GL2.GL_RENDERBUFFER, tmp1[0]);
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
		
		return tmp1[0];
	}
	
	private int updateVolumeData(GLAutoDrawable drawable, IntensityVolume volume, int activeTexture, int volumeTexture) {		
		
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glActiveTexture(activeTexture);

		/*
		 * Leave this since we might have just bound a null volume (but that's ok!)
		 */
		if (volume == null) {
			gl.glGenTextures(1, tmp1, 0);
			gl.glBindTexture(GL2.GL_TEXTURE_3D, tmp1[0]);
			return tmp1[0];
		}

		if (volumeTexture < 0) {
			gl.glGenTextures(1, tmp1, 0);
		}
		else {
			tmp1[0] = volumeTexture;
		}
		
		gl.glBindTexture(GL2.GL_TEXTURE_3D, tmp1[0]);

		int[] dims = volume.getDimension();

		ByteBuffer buffer = null;
		if (volume instanceof IntensityVolumeMask) {
			buffer = IntensityVolume.getBuffer((IntensityVolumeMask) volume);
		}
		else {
			buffer = IntensityVolume.getBuffer(volume, colorMap, histogram);
		}
		buffer.rewind();
		gl.glTexImage3D(GL2.GL_TEXTURE_3D, 0, GL2.GL_RGBA, dims[0], dims[1], dims[2], 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, buffer);
		
		setTexture3DParam(drawable, activeTexture);

		return tmp1[0];
	}
	
	private void setHistogram() {
		// Update histogram with fresh data
		// Apply histogram threshold and epsilon selection
		if (mask != null)
			histogram.setHistogram(new Histogram(volume.flattenAndDiscard(mask, epsilon.getValue())));
		else 
			histogram.setHistogram(new Histogram(volume.flattenAndDiscard(epsilon.getValue())));
	}

	private int genbindTexture2D(GLAutoDrawable drawable) {

		GL2 gl = drawable.getGL().getGL2();
		
		gl.glActiveTexture(activeSampler2D);
		
		gl.glGenTextures(1, tmp1, 0);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, tmp1[0]);

		gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0,GL2.GL_RGBA16F, renderer.getDimension().width, renderer.getDimension().height, 0, GL2.GL_RGBA, GL2.GL_FLOAT, null);
		
		setTexture2DParam(drawable);

		return tmp1[0];
	}

	
	private void setTexture3DParam(GLAutoDrawable drawable, int activeSamplerTexture) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glActiveTexture(activeSamplerTexture);
		
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, TEX_ENV);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER, linearFilter3D.getValue() ? GL2.GL_LINEAR : GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER, linearFilter3D.getValue() ? GL2.GL_LINEAR : GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_S, CLAMP_MODE);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_T, CLAMP_MODE);
		gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_R, CLAMP_MODE);

		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT,1);
	}

	private void setTexture2DParam(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, TEX_ENV);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, linearFilter2D.getValue() ? GL2.GL_LINEAR : GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, linearFilter2D.getValue() ? GL2.GL_LINEAR : GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, CLAMP_MODE);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, CLAMP_MODE);

//		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT,1);
	}
	
	@Override
	public String getName() {
		return "Volume renderer";
	}

	@Override
	public void reload(GLViewerConfiguration config) {
		// TODO Auto-generated method stub
		
	}

	private boolean isInitialized = false;
	private void setInitialized(boolean b) {
		isInitialized = b;
	}
	public boolean isInitialized() {
		return isInitialized;
	}
	
	public void setVisible(boolean b) {
		draw.setValue(b);
	}
	
	public boolean isVisible() {
		return draw.getValue();
	}
	
	public void setLowResolution() {
		highResolution.hold(true);
		highResolution.setValue(false);
		highResolution.hold(false);
		
		scale.setValue(DEFAULT_SCALE);
		emissivity.setValue(0.25);
		stepSize.setValue(0.01);
		depth.setValue(150);
	}
	
	public void setVeryLowResolution() {
		lowResolution.hold(true);
		lowResolution.setValue(false);
		lowResolution.hold(false);
		
		highResolution.hold(true);
		highResolution.setValue(false);
		highResolution.hold(false);

		scale.setValue(1);
		emissivity.setValue(1);
		stepSize.setValue(0.1);
		depth.setValue(10);
	}

	public void setHighResolution() {
		lowResolution.hold(true);
		lowResolution.setValue(false);
		lowResolution.hold(false);
		
		scale.setValue(HR_SCALE);
		emissivity.setValue(HR_EMISSIVITY);
		stepSize.setValue(HR_STEP_SIZE);
		depth.setValue(HR_DEPTH);
	}
	
	public void setDefaultResolution() {
		// Set defaults
		scale.setValue(DEFAULT_SCALE);
		emissivity.setValue(DEFAULT_EMISSIVITY);
		stepSize.setValue(DEFAULT_STEP_SIZE);
		depth.setValue(DEFAULT_DEPTH);
	}
	
	private VerticalFlowPanel vfp;

	@Override
	public JPanel getControls() {
		if (vfp != null) {
			return vfp.getPanel();
		}
		
		 vfp = new VerticalFlowPanel();
		
		vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Volume Renderer", TitledBorder.LEFT, TitledBorder.CENTER));

		vfp.add(draw.getControls());
		vfp.add(stepSize.getSliderControls());
		vfp.add(depth.getSliderControls());
		vfp.add(scale.getSliderControls());
		vfp.add(epsilon.getSliderControls());
		epsilon.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				setHistogram();				
			}
		});
		
		vfp.add(emissivity.getSliderControls());
//		vfp.add(linearFilter2D.getControls());
		vfp.add(histogram);
		vfp.add(colorMap);

		JPanel resPanel = new JPanel(new GridLayout(1, 2));
		resPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Resolution", TitledBorder.LEFT, TitledBorder.CENTER));
		resPanel.add(lowResolution.getControls());
		resPanel.add(highResolution.getControls());
		vfp.add(resPanel);
		
		JPanel optionPanel = new JPanel(new GridLayout(1, 3));
		optionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Options", TitledBorder.LEFT, TitledBorder.CENTER));
		optionPanel.add(useMask.getControls());
		optionPanel.add(linearFilter3D.getControls());
		optionPanel.add(statcolor.getControls());
		vfp.add(optionPanel);
		
		highResolution.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
					if (highResolution.getValue())
						setHighResolution();
					else 
						setDefaultResolution();
				}
		});
		
		lowResolution.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				if (lowResolution.getValue())
					setLowResolution();
				else 
					setDefaultResolution();
			}
		});
		
		JPanel dpanel = new JPanel(new GridLayout(1,3));
		dpanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Debug", TitledBorder.LEFT, TitledBorder.CENTER));
		dpanel.add(drawBack.getControls());
		dpanel.add(drawFront.getControls());
		dpanel.add(drawRays.getControls());
		vfp.add(dpanel);

		/*
		 * DEVELOPER MODE
		 */
		final VolumeRenderer vthis = this;

		if (DEVELOPPER_MODE) {
			ParameterListener pl = new ParameterListener() {
				@Override
				public void parameterChanged(Parameter parameter) {
					VolumeRenderer.setSampleVolume(vthis);
				}
			};
			
			vfp.add(vsize.getControls());
			vsize.addParameterListener(pl);
			
			vfp.add(parameter1.getSliderControls());
			parameter1.addParameterListener(pl);
		}
		
		ParameterListener p = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				requestTextureUpdate = true;
			}
		};
		
		linearFilter2D.addParameterListener(p);
		linearFilter3D.addParameterListener(p);
		
		return vfp.getPanel();
	}

	public static int DEVELOPER_TEST_VOLUME = 1;
	
	private static IntensityVolume[] getSampleVolume(int volumeID, Point3i dim) {
		IntensityVolume volume = null;
		IntensityVolumeMask mask = null;
		if (volumeID == 1) {
			// Create an unmasked spherical volume
			volume = IntensityVolume.createSphericalVolume(dim.x, dim.y, dim.z);
		}
		else if (volumeID == 2) {
			// Create an unmasked randomized volume
			volume = IntensityVolume.createRandomizedVolume(dim.x, dim.y, dim.z);			
		}
		else if (volumeID == 3) {
			// Create a randomized volume masked by an exterior sphere
			volume = IntensityVolume.createRandomizedVolume(dim.x, dim.y, dim.z);			

			// Clamp above some thin shell located at half-width
			IntensityVolume volumeMask = IntensityVolume.createSphericalVolume(dim.x, dim.y, dim.z);
			volumeMask.negate();
			volumeMask.threshold(-0.5, 0);
			volumeMask.abs();
			volumeMask.toBinary();
			
			mask = new IntensityVolumeMask(volumeMask.getData());
		}
		else if (volumeID == 4) {
			
			int dx = parameter1.getValue();
			
			// Clamp above some thin shell located at half-width
			IntensityVolume ball1 = IntensityVolume.createSphericalVolume(dim.x, dim.y, dim.z);
			ball1.translate(new Point3i(-dx, 0, 0));

			IntensityVolume ball2 = IntensityVolume.createSphericalVolume(dim.x, dim.y, dim.z);
			ball2.translate(new Point3i(dx, 0, 0));

			volume = new IntensityVolume(dim.x, dim.y, dim.z);
			volume.add(ball1);
			volume.add(ball2);
			
			mask = new IntensityVolumeMask(dim.x, dim.y, dim.z);
			mask.setVolume(volume.copy().getData());
			mask.toBinary();
			
		}		
		return new IntensityVolume[] { volume, mask };
	}
	
	private static void setSampleVolume(VolumeRenderer renderer) {
		IntensityVolume[] vols = getSampleVolume(DEVELOPER_TEST_VOLUME, vsize.getPoint3i());
		if (vols[1] != null) {
			renderer.setVolume(vols[0], (IntensityVolumeMask) vols[1], null);
		}
		else {
			renderer.setVolume(vols[0]);
		}
	}
	
	@Override
	public void attach(Component component) {
		final VolumeRenderer vthis = this;
		
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				
				if (e.getKeyCode() == KeyEvent.VK_I) {
					setInitialized(false);
				}
				else if (e.getKeyCode() == KeyEvent.VK_1) {
					DEVELOPER_TEST_VOLUME = 1;
					VolumeRenderer.setSampleVolume(vthis);
				}
				else if (e.getKeyCode() == KeyEvent.VK_2) {
					DEVELOPER_TEST_VOLUME = 2;
					VolumeRenderer.setSampleVolume(vthis);
				}
				else if (e.getKeyCode() == KeyEvent.VK_3) {
					DEVELOPER_TEST_VOLUME = 3;
					VolumeRenderer.setSampleVolume(vthis);
				}
				else if (e.getKeyCode() == KeyEvent.VK_4) {
					DEVELOPER_TEST_VOLUME = 4;
					VolumeRenderer.setSampleVolume(vthis);
				}
				else if (e.getKeyCode() == KeyEvent.VK_R) {
					reloadShaderRequested = true;
				}
			}
		});
	}

	public static void main(String[] args) {
		VolumeRenderer.DEVELOPPER_MODE = true;
		VolumeRenderer.DEVELOPER_TEST_VOLUME = 4;
		
		JoglRenderer jr = new JoglRenderer("Volume Rendering");
		VolumeRenderer renderer = jr.createVolumeRendererInstance();
		jr.addInteractor(renderer);
		
		// Start
		VolumeRenderer.setSampleVolume(renderer);
		jr.start(renderer);
		jr.getCamera().zoom(-100f);
	}

	@Override
	public void detach(Component component) {
		// TODO Auto-generated method stub
		
	}

	public JHistogram getHistogram() {
		return histogram;
	}
	
	public void setEmissivity(double v) {
		emissivity.setValue(v);
	}
}
