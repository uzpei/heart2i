package volume;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import math.matrix.Matrix3d;

public class VolumeIO {
	
	public static void main(String[] args) {
		String filename = "./data/test.ser";
		
		int n = 10;
		int m = 100;
		Random rand = new Random();
		List<Matrix3d> matrices = new LinkedList<Matrix3d>();
		for (int i = 0; i < n; i++) 
			matrices.add(new Matrix3d(new int[] { 1 + rand.nextInt(m), 1 + rand.nextInt(m), 1 + rand.nextInt(m) }).randomize());

		serialize(matrices, filename);
		
		List<Matrix3d> reserialized = deserialize(filename);
		
		for (int i = 0; i < matrices.size(); i++) {
			System.out.println(Arrays.toString(matrices.get(i).getDimension()) + " = " + Arrays.toString(reserialized.get(i).getDimension()));
		}
	}

	public static void serialize(List<Matrix3d> matrices, String filename) {
		
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(filename);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
//	         out.writeObject(volumes);
	         for (Matrix3d m : matrices) {
	        	 out.writeObject(m);
	         }
	         out.close();
	         fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static List<Matrix3d> deserialize(String filename) {
		List<Matrix3d> matrices = new LinkedList<Matrix3d>();
	      try
	      {
	         FileInputStream fileIn = new FileInputStream(filename);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         
	         while (true) {
	        	 try{  
	        		 matrices.add((Matrix3d) in.readObject());  
	        	    } catch (EOFException e){
	       	         	in.close();
	       	         	fileIn.close();
	        	    	return matrices;  
	        	    }  
	         }
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return matrices;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("Employee class not found");
	         c.printStackTrace();
	         return matrices;
	      }
	}
}
