package volume;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.vecmath.Point3i;

import swing.parameters.ParameterListener;
import swing.parameters.ParameterManager;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;

public class JScopedVolume extends JPanel {
	private final static int IMG_TYPE = BufferedImage.TYPE_INT_RGB;
	private ParameterManager parameterManager = new ParameterManager();
	private BufferedImage imgRaw = null, imgScaled = null;
	private FrameAxis axis = FrameAxis.Z;
	private boolean crossSectional;
	private boolean dataChanged;
	private IntensityVolume volume;
	private Point mp = new Point();
//	private JPanelPainter painter = new JPanelPainter(this, 1);
	private Point3i coordinate;
	private Point3i span;

	public JScopedVolume(IntensityVolume volume, FrameAxis axis, boolean crossSectional, Point3i coordinate, Point3i span) {
		this.volume = volume;
		this.axis = axis;
		this.crossSectional = crossSectional;
		dataChanged = true;
		
		setCoordinate(coordinate, span);
		
		createListeners();
	}
	
	public void addParameterListener(ParameterListener l) {
		parameterManager.addParameterListener(l);
	}
	
	public void setCoordinate(Point3i coordinate, Point3i span) {
		this.coordinate = coordinate;
		this.span = span;
		
		mp = worldToPoint(coordinate);
		
		dataChanged = true;
		paint();
	}
	
	private void paint() {
//		validate();
		repaint();
//		painter.paint();
	}
	
	private void createbuffer() {
		int[] dims = volume.getDimension();
		int[] inds = getImageAxes();
//		int nind = getNotImageAxis();

		imgRaw = new BufferedImage(dims[inds[0]], dims[inds[1]], IMG_TYPE);
		
	}
	
	private void applyScale(Dimension targetDimension) {
		int w = imgRaw.getWidth();
		int h = imgRaw.getHeight();
		AffineTransform at = new AffineTransform();
		
		// Apply scale to fully fill rectangle
		double dx = targetDimension.getWidth() / w;
		double dy = targetDimension.getHeight() / h;
		at.scale(dx, dy);
		
//		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BICUBIC);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		AffineTransform.getScaleInstance(1, -1);
		imgScaled = scaleOp.filter(imgRaw, null);
		
		// Flip the image vertically
//		AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
//		tx.translate(0, -imgScaled.getHeight(null));
//		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
//		imgScaled = op.filter(imgScaled, null);

	}
	
	private void fillBuffer(Dimension targetDimension) {
		
		int[] dims = volume.getDimension();
		
		int[] inds = getImageAxes();
		int nind = getNotImageAxis();
		int[] ijk = new int[3];
		int[] cijk = new int[3];
		
		if (crossSectional) {
			coordinate.get(cijk);
			ijk[nind] = cijk[nind];
			
			for (int u = 0; u < dims[inds[0]]; u++)
				for (int v = 0; v < dims[inds[1]]; v++) {
					ijk[inds[0]] = u;
//					ijk[inds[1]] = dims[inds[1]] - v -1;
					ijk[inds[1]] = v;
					
					int c = 255 * MathToolset.roundInt(volume.get(ijk));
					imgRaw.setRGB(u, v, itorgb(c));
				}
		}
		else {
			// Make y coordinate to the center
			Point3i center = new Point3i();
			center.set(coordinate);
			center.y = MathToolset.roundInt(volume.getDimension()[axis.getIndex()] / 2);
			center.get(cijk);
			
			ijk[nind] = cijk[nind];
			
			for (int u = 0; u < dims[inds[0]]; u++)
				for (int v = 0; v < dims[inds[1]]; v++) {
					ijk[inds[0]] = dims[inds[0]] - u - 1;
					ijk[inds[1]] = dims[inds[1]] - v - 1;
					
					int c = 255 * MathToolset.roundInt(volume.get(ijk));
					imgRaw.setRGB(u, v, itorgb(c));
				}
		}
		
		applyScale(targetDimension);
		
		dataChanged = false;
	}
	
	private int itorgb(int c) {
		return ((255 & 0xFF) << 24) |
        ((c & 0xFF) << 16) |
        ((c & 0xFF) << 8)  |
        ((c & 0xFF) << 0);
	}

	@Override
	protected void paintComponent(Graphics gp) {
		if (parameterManager.isNotifying()) return;
		
		super.paintComponent(gp);
		
		if (dataChanged) {
			createbuffer();
			fillBuffer(getSize());
		}

		// dimension.setSize(getSize().width, dimension.height);
		int width = getSize().width;
		int height = getSize().height;

		// Create image and set rendering params
		BufferedImage backbuffer = new BufferedImage(getSize().width,
				getSize().height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = backbuffer.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);

		Stroke defaultStroke = new BasicStroke(1, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND);
		float dash = 5f;
		Stroke dashStroke = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, dash, new float[] { dash }, 0f);
		
		g.setStroke(defaultStroke);
		
		// Draw the buffer
		g.drawImage(backbuffer, 0, 0, null);
		
		Color longColor = Color.CYAN;

		// Draw mouse coords
		if (crossSectional) {
			g.setStroke(dashStroke);
			g.setColor(longColor);
			g.drawLine(0, mp.y, width, mp.y);
			g.setColor(Color.RED);
			g.drawLine(mp.x, 0, mp.x, height);
		}
		else {
			g.setStroke(dashStroke);
			g.setColor(Color.RED);
			g.drawLine(0, mp.y, width, mp.y);
			
//			g.setStroke(defaultStroke);
//			g.setColor(Color.RED);
//			g.drawLine(width / 2, 0, width/2, height);
		}
				
		// Draw contour
		if (crossSectional) {
			g.setStroke(defaultStroke);
			g.setColor(Color.GRAY);
			g.drawRect(0, 0, width-1, height-1);
		}
		else {
			g.setStroke(dashStroke);
			g.setColor(longColor);
			g.drawRect(0, 0, width-1, height-1);
		}
		
		// Draw label
		Font fontTicks = new Font("Consolas", Font.BOLD, 18);
		g.setFont(fontTicks);
		g.setColor(Color.BLUE);
		FontMetrics metrics = getFontMetrics(fontTicks);
		int txtPad = metrics.getHeight() / 2 + 1;
		String txt;
		int[] ninds = axis.getOrthogonalIndices();
			
		if (crossSectional) {
			txt = FrameAxis.values()[ninds[0]].toString();
			int adv = metrics.stringWidth(txt);
			g.drawChars(txt.toCharArray(), 0, txt.length(), MathToolset.roundInt(width/2f - adv/2f), txtPad);
			
			txt = FrameAxis.values()[ninds[1]].toString();
			g.drawChars(txt.toCharArray(), 0, txt.length(), 1, MathToolset.roundInt(height/2f));
		}
		else {
			txt = axis.toString();
			g.drawChars(txt.toCharArray(), 0, txt.length(), 1, MathToolset.roundInt(height/2f));
		}

		// Draw span
		int[] is = new int[3];
		span.get(is);
		int[] dims = volume.getDimension();
		int[] inds = getImageAxes();
		int sx = MathToolset.roundInt(((double) is[inds[0]]) / dims[inds[0]] * width);
		int sy = MathToolset.roundInt(((double) is[inds[1]]) / dims[inds[1]] * height);
		g.setColor(new Color(255, 0, 0, 100));
		
		if (crossSectional)
			g.fillRect(mp.x, mp.y, sx, sy);
		else
			g.fillRect(0, mp.y, width, sy);
			
		
		// Draw image back in graphics
		gp.drawImage(imgScaled, 0, 0, null);
		gp.drawImage(backbuffer, 0, 0, this);
		
	}
	
	private void notifyListeners() {
		parameterManager.notifyListeners();
	}
	
	private Point worldToPoint(Point3i coordinate) {
		int[] ninds = axis.getOrthogonalIndices();
		int[] coords = new int[3];
		coordinate.get(coords);
		
		Point pt = new Point(0, 0);
		if (crossSectional) {
			pt.x = MathToolset.roundInt(getSize().getWidth() * coords[0] / volume.getDimension()[ninds[0]]);
			pt.y = MathToolset.roundInt(getSize().getHeight() * coords[1] / volume.getDimension()[ninds[1]]);
		}
		else {
			
			pt.y = MathToolset.roundInt(getSize().getHeight() * (volume.getDimension()[axis.getIndex()] - coords[axis.getIndex()]) / volume.getDimension()[axis.getIndex()]);
		}
		
		return pt;
	}
	
	private void applyMouseCoordinate(Point p) {
		
		double dx = p.x / getSize().getWidth();
		double dy = p.y / getSize().getHeight();
//		System.out.println(leftMouse);
		
		if (leftMouse) {
			mp.setLocation(p);
			int[] ninds = axis.getOrthogonalIndices();
			int[] coords = new int[3];
			if (crossSectional) {
				int vCross0 = clamp(MathToolset.roundInt(dx * volume.getDimension()[ninds[0]]), ninds[0]);
				int vCross1 = clamp(MathToolset.roundInt(dy * volume.getDimension()[ninds[1]]), ninds[1]);
				coordinate.get(coords);
				coords[ninds[0]] = vCross0;
				coords[ninds[1]] = vCross1;
			}
			else {
				int vLong = clamp(MathToolset.roundInt(dy * volume.getDimension()[axis.getIndex()]), axis.getIndex());
				coordinate.get(coords);
				coords[axis.getIndex()] = volume.getDimension()[axis.getIndex()] - vLong - 1;
			}
			
			coordinate.set(coords);
			
			fillBuffer(getSize());
			
			// No need to repaint since callback will take care of it
//			painter.paint();
		}
		else {
			Point dp = getLocationFromMouse(p);
			int[] coords = new int[3];
			coordinate.get(coords);
			int[] inds = getImageAxes();
			Point pc = new Point(coords[inds[0]], coords[inds[1]]);
			pc.x = dp.x - pc.x;
			pc.y = dp.y - pc.y;
			
			int[] span = new int[3];
			this.span.get(span);
			span[inds[0]] = Math.max(pc.x, 0);
			span[inds[1]] = Math.max(pc.y, 0);
			this.span.set(span);
//			System.out.println(dp);
		}
		
		notifyListeners();
	}
	
	private Point getLocationFromMouse(Point mp) {
		double dx = mp.x / getSize().getWidth();
		double dy = mp.y / getSize().getHeight();

		int[] inds = getImageAxes();
		int p0, p1;
		p0 = clamp(MathToolset.roundInt(dx * volume.getDimension()[inds[0]]),
				inds[0]);
		p1 = clamp(MathToolset.roundInt(dy * volume.getDimension()[inds[1]]),
				inds[1]);
		
		return new Point(p0, p1);
	}
	
	public void setDirty() {
		dataChanged = true;
		paint();
	}
	
	public Point3i getCoordinate() {
		return coordinate;
	}
	
	private int clamp(int v, int index) {
		int v2 = Math.max(0,Math.min(volume.getDimension()[index]-1, v));
		return v2;
	}

	private boolean leftMouse = true;
	
	private void createListeners() {
		
		this.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
//				leftMouse = e.getButton() == 1;
				applyMouseCoordinate(e.getPoint());
			}
		});
		
		this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {
				leftMouse = e.getButton() == 1;
				applyMouseCoordinate(e.getPoint());
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		
		addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				createbuffer();
				fillBuffer(getSize());
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});

	}

	public void setDimensions(int width, int height) {
		Dimension d = new Dimension();
		d.height = height;
		
		// See if we can get the correct image ratio
		double ratio = getVolumeRatio();
		
		d.width = MathToolset.roundInt(ratio * d.height);
		
		setPreferredSize(d);
		dataChanged = true;
		paint();
	}
	
	private double getVolumeRatio() {
		int[] dims = volume.getDimension();
		int[] inds = getImageAxes();
		return ((double) dims[inds[0]]) / dims[inds[1]];
	}
	
	private int[] getImageAxes() {
		int[] ninds = axis.getOrthogonalIndices();
		
		if (!crossSectional) {
			ninds[1] = axis.getIndex();
		}
		
		return ninds;
	}

	private int getNotImageAxis() {
		int[] ninds = axis.getOrthogonalIndices();
		
		if (crossSectional) {
			return axis.getIndex();
		}
		else {
			return ninds[1];
		}
	}
}
