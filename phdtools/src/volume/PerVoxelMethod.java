package volume;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public interface PerVoxelMethod {
	/**
	 * Process a voxel locataed at position (x,y,z).
	 * @param x
	 * @param y
	 * @param z
	 */
	public void process(int x, int y, int z);
	
	public boolean isValid(Point3d origin, Vector3d span);
}
