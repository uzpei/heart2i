package volume;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3i;

import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import tools.frame.VolumeSampler;
import voxel.VoxelBox;

public class NeighborhoodMap {

	private HashMap<Point3i, List<Point3i>> neighborhoodMap;
	private List<Point3i> neighborhood;
	private NeighborhoodShape shape;
	private int size;
	private VoxelBox box;
	private Point3i pn = new Point3i();
	
	public NeighborhoodMap(VoxelBox box, NeighborhoodShape shape, int size) {
		this.size = size;
		this.shape = shape;
		this.box = box;
		neighborhood = VolumeSampler.sample(shape, size);
		
		construct();
	}
	
	/**
	 * @param location
	 * @return a neighborhood of existing voxels about that point. 
	 */
	public List<Point3i> getNeighborhood(Point3i location) {
		return neighborhoodMap.get(location);
	}
	
	public static List<Point3i> validate(VoxelBox box, List<Point3i> neighborhood, Point3i p) {
		if (box.getMask() == null)
			return neighborhood;
		
		if (box.getMask().isMasked(p)) {
			return null;
		}
		
		// Create the list of accessible voxel neighbors for this
		// location
		List<Point3i> accessibleNeighborhood = new LinkedList<Point3i>();

		// Make sure no neighbor sits outside of the volume
		for (Point3i n : neighborhood) {

			// If this neighbor exists and is accessible, add it
			if (box.dimensionContains(p.x + n.x, p.y + n.y, p.z + n.z)
					&& !box.isOutside(p.x + n.x, p.y + n.y, p.z + n.z)) {
				accessibleNeighborhood.add(n);
			}
		}

		// No need to create a new list if we are using all voxels
		if (accessibleNeighborhood.size() == neighborhood.size()) {
			accessibleNeighborhood.clear();
			accessibleNeighborhood = neighborhood;
		}

		return accessibleNeighborhood;
	}

	private void construct() {
		neighborhoodMap = new HashMap<Point3i, List<Point3i>>();
		box.voxelProcess(new PerVoxelMethodUnchecked() {

			@Override
			public void process(int x, int y, int z) {
				Point3i p = new Point3i(x, y, z);

				// Compute accessible neighborhood at this point
				List<Point3i> accessibleNeighborhood = validate(box, neighborhood, p);

				// Insert the list into the hash map
				neighborhoodMap.put(p, accessibleNeighborhood);
			}
		});
	}

}
