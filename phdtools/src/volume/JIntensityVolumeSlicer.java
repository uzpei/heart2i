package volume;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3i;

import swing.component.JPanelPainter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.parameters.ParameterManager;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;

public class JIntensityVolumeSlicer extends JPanel {

	/**
	 * Ratio images / controls
	 */
	private double SPLIT_RATIO = 0.6;
	
	private ParameterManager manager = new ParameterManager();

	private JFormattedTextField[] lblPos = new JFormattedTextField[3];
	private JFormattedTextField[] lblSpan = new JFormattedTextField[3];
	
	private JPanelPainter painter = new JPanelPainter(this, 10);
	
	private JScopedVolume volCross, volLong;
	
	private Point3i coordinate = new Point3i();
	private Point3i span = new Point3i();
	
	/**
	 * Use minimum 300 width and 100 height
	 * @param volume
	 * @param dimension
	 */
	public JIntensityVolumeSlicer(IntensityVolume volume, Dimension dimension) {
		setPreferredSize(dimension);
		setMinimumSize(dimension);

		addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				applyComponentResize();
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});

		span.set(15,10,5);
		volCross = new JScopedVolume(volume, FrameAxis.Z, true, coordinate, span);
		volLong = new JScopedVolume(volume, FrameAxis.Z, false, coordinate, span);
		
		ParameterListener labelListener = new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				updateCoordinateLabels(true);
			}
		};
		volCross.addParameterListener(labelListener);
		volLong.addParameterListener(labelListener);

		int swidth = MathToolset.roundInt(dimension.width * SPLIT_RATIO);

		volCross.setDimensions(swidth, dimension.height);
		volLong.setDimensions(swidth, dimension.height);
		
		createControls();
		
		painter.paint();
	}
	
	public Point3i[] getCoordinates() {
		return new Point3i[] { coordinate, span };
	}
	
	public ParameterManager getParameterManager() {
		return manager;
	}
		
	private void updateCoordinateLabels(boolean notifyListeners) {
		
		labelTriggerBlocked = true;
		
		int[] coords = new int[3];
		coordinate.get(coords);
		for (int i = 0; i < 3; i++) {
			lblPos[i].setValue(coords[i]);
		}

		span.get(coords);
		for (int i = 0; i < 3; i++) {
			lblSpan[i].setValue(coords[i]);
		}

		labelTriggerBlocked = false;
		
		// Set all images dirty
		volCross.setDirty();
		volLong.setDirty();
		
		if (notifyListeners)
			manager.notifyListeners();
	}
	
	private boolean labelTriggerBlocked = false;
	
	protected void updateCoordinateFromLabels() {
		if (labelTriggerBlocked)
			return;
		
		int[] coordinate = new int[3];
		int[] span = new int[3];
		for (int i = 0; i < 3; i++) {
			Object valueO = lblPos[i].getValue();
			if (valueO instanceof Number) {
				coordinate[i] = ((Number) valueO).intValue();
			}
			valueO = lblSpan[i].getValue();
			if (valueO instanceof Number) {
				span[i] = ((Number) valueO).intValue();
			}
		}
		
//		this.coordinate.set(coordinate);
//		this.span.set(span);
//		volCross.setCoordinate(this.coordinate, this.span);
//		volLong.setCoordinate(this.coordinate, this.span);
		setCoordinates(new Point3i(coordinate), new Point3i(span));
		
		manager.notifyListeners();
	}

	private void setCoordinates(Point3i coordinate, Point3i span) {
		this.coordinate.set(coordinate);
		this.span.set(span);
		
		volCross.setCoordinate(this.coordinate, this.span);
		volLong.setCoordinate(this.coordinate, this.span);

	}

	public void set(Point3i coordinate, Point3i span) {
		set(coordinate, span, true);
	}
	
	public void set(Point3i coordinate, Point3i span, boolean notifyListeners) {
		setCoordinates(coordinate, span);
		updateCoordinateLabels(notifyListeners);
	}


	private void applyComponentResize() {
		int swidth = MathToolset.roundInt(getSize().width * SPLIT_RATIO / 2);

		volCross.setDimensions(swidth, getSize().height);
		volLong.setDimensions(swidth, getSize().height);
	}
	
	private void createControls() {
		removeAll();
		
//		setLayout(new GridLayout(1,3));
		setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.weightx = 0;
		c.gridx = 0;		
		c.insets = new Insets(0, 0, 0, 5);
		add(volCross,c);
		
		c.gridx = 1;
		c.insets = new Insets(0, 0, 0, 5);
		add(volLong,c);
		
		JPanel lblPanel = new JPanel(new GridBagLayout());
		
		int swidth = MathToolset.roundInt(getPreferredSize().width * (1-SPLIT_RATIO));
		Dimension psize = new Dimension(swidth, getPreferredSize().height);
		lblPanel.setPreferredSize(psize);
		GridBagConstraints gbc = new GridBagConstraints();
//		
		Dimension ldim = new Dimension(40, 25);
		int[] coords = new int[3];
		volCross.getCoordinate().get(coords);
		String[] lbls = new String[] { "x", "y", "z" };
		PropertyChangeListener pcl = new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
// 				System.out.println(evt.getPropertyName());
				
				// FIXME: this is not robust and will almost certainly eventually fail
				if (evt.getPropertyName() == "value" || evt.getPropertyName() == "editValid")
					updateCoordinateFromLabels();
			}
		};
		
		for (int i = 0; i < 3; i++) {
			gbc.gridx = 0;
			gbc.gridy = i;
			gbc.weighty = 0.2;
			gbc.gridheight = 1;
			gbc.gridy = i;
			gbc.weightx = 1;
			gbc.gridwidth = 1;
			gbc.fill = GridBagConstraints.NONE;
			
			gbc.anchor = GridBagConstraints.EAST;
			gbc.ipadx = 10;
			lblPanel.add(new JLabel(lbls[i]), gbc);
			gbc.anchor = GridBagConstraints.WEST;

			gbc.ipadx = 0;
			gbc.gridx = 1;
			lblPos[i] = new JFormattedTextField(new DecimalFormat("0"));
			lblPos[i].setValue(coords[i]);
			lblPos[i].setPreferredSize(ldim);
			lblPos[i].setMinimumSize(ldim);
			lblPos[i].addPropertyChangeListener(pcl);
			
			lblPanel.add(lblPos[i], gbc);
			
			gbc.gridx = 2;
			lblPanel.add(new JLabel("+"), gbc);

			gbc.gridx = 3;
			lblSpan[i] = new JFormattedTextField(new DecimalFormat("0"));
			lblSpan[i].setValue(coords[i]);
			lblSpan[i].setPreferredSize(ldim);
			lblSpan[i].setMinimumSize(ldim);
			lblSpan[i].addPropertyChangeListener(pcl);
			lblPanel.add(lblSpan[i], gbc);

		}

		lblPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Coordinates", TitledBorder.CENTER, TitledBorder.CENTER));
		c.gridx = 2;
		c.weightx = 1;
		c.ipadx = 10;
//		c.fill = GridBagConstraints.HORIZONTAL;
		add(lblPanel,c);
	}
	
}
