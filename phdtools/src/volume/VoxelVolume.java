package volume;

public interface VoxelVolume {

	public void voxelProcess(PerVoxelMethod pvm);

	public int[] getDimension();
}
