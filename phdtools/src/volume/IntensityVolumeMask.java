package volume;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import math.matrix.Matrix3d;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import system.object.Triplet;
import tools.frame.CoordinateFrameSample;
import tools.frame.VolumeSampler;
import tools.frame.OrientationFrame.SamplingStyle;
import voxel.VoxelBox;
import voxel.VoxelBox.CuttingPlane;

/**
 * Assume a binary image 0/1.
 * Valid voxels = 1
 * Outside = 0;
 * @param x
 * @param y
 * @param z
 * @return
 */
public class IntensityVolumeMask extends IntensityVolume {

	private static final long serialVersionUID = 4572319187739682893L;

	private BooleanParameter enabled = new BooleanParameter("enabled", true);

	private BooleanParameter drawCom = new BooleanParameter("draw CoM", false);

	public IntensityVolumeMask(IntensityVolumeMask other) {
		super(other.copy().getData());
		this.enabled = other.enabled;
		toBinary();
	}

	public IntensityVolumeMask(String filename) {
		super(filename);
		toBinary();
	}

	public IntensityVolumeMask(double[][][] volume) {
		super(volume);
		toBinary();
	}


	public IntensityVolumeMask(IntensityVolume other) {
		this(other.copy().getData());
	}

	/**
	 * Create a new intensity volume mask with zeros everywhere such that all voxels are considered dirty.
	 * @param dim
	 */
	public IntensityVolumeMask(int[] dim) {
		this(dim, false);
	}
	
	/**
	 * Create a new intensity volume mask with ones.
	 * @param dim
	 */
	public IntensityVolumeMask(int[] dim, boolean filled) {
		super(dim);

		if (filled)
			fillWith(1);
		
		toBinary();
	}


	public IntensityVolumeMask(Matrix3d m) {
		super(m);
	}

	public IntensityVolumeMask(int x, int y, int z) {
		super(x, y, z);
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Volume Mask", TitledBorder.LEFT, TitledBorder.CENTER));
		JPanel bpanel = new JPanel(new GridLayout(1,2));
		bpanel.add(enabled.getControls());
		bpanel.add(drawCom.getControls());
		
		vfp.add(bpanel);
		return vfp.getPanel();
	}
	

	/**
	 * Assume a binary image 0/1.
	 * Valid voxels (not masked) = 1
	 * Outside (masked) = 0;
	 * @param x
	 * @param y
	 * @param z
	 * @return whether this voxel is masked (dirty) or not
	 */
	public boolean isMasked(int x, int y, int z) {
//		System.out.println("isMasked? : " + Thread.currentThread().getName());
//		return !(getIntensity(x, y, z) > 0);	
		return super.getIntensity(x, y, z) <= EPSILON;	
	}

	/**
	 * @param p
	 * @return whether this voxel is masked (dirty) or not
	 */
	public boolean isMasked(Point3i p) {
//		System.out.println("isMasked? : " + Thread.currentThread().getName());
		return super.getIntensity(p.x, p.y, p.z) <= EPSILON;	
	}

	public boolean isEnabled() {
		return enabled.getValue();
	}
	
	private Point3d cm = null; 

	/**
	 * @return the center of mass of all non-masked voxels (assuming a volume unit 1) 
	 */
	public Point3d getCenterOfMass() {
		if (cm != null)
			return new Point3d(cm);

		int[] dim = getDimension();
		int vcount = 0;
		cm = new Point3d();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (!isMasked(i, j, k)) {
						vcount++;
						cm.x += i;
						cm.y += j;
						cm.z += k;
					}
				}
			}
		}
		
		cm.scale(1d / vcount);
		return new Point3d(cm);
	}

	/**
	 * @return the center of mass of non-masked voxels within this box 
	 */
	public Point3d getCenterOfMass(VoxelBox box) {
		final int[] vcount = new int[] { 0 };
		final Point3d cm = new Point3d();

		box.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				if (!isMasked(x, y, z)) {
					vcount[0]++;
					cm.x += x;
					cm.y += y;
					cm.z += z;
				}
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});

		cm.scale(1d / vcount[0]);
		return cm;
	}

	/**
	 * Apply this mask to the volume. 
	 * Warning: This will discards values outside of the mask. All values outside of the mask are set to NaN.
	 * @param m
	 */
	public void applyMask(Matrix3d m) {
		int[] dim = m.getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (isMasked(i, j, k)) {
						m.set(i, j, k, Double.NaN);
					}
				}
			}
		}
	}
	
	public void applyMask(IntensityVolume v) {
		int[] dim = v.getDimension();
//		System.out.println("Applying mask from " + Thread.currentThread().getName());
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
//					System.out.println("Voxel " + i + "," + j + "," + k + ", v = " + (super.getIntensity(i,j,k)==0));
					// FIXME: for some unknown reason the app crashes here with an invalid memory location!!!
					// if I call isMasked(i,j,k)... what is going on?
					if (isMasked(i, j, k)) {
//					if (super.getIntensity(i,j,k) == 0 || Double.isNaN(super.getIntensity(i, j, k))) {
//						v.set(i, j, k, Double.NaN);
						v.set(i, j, k, 0);
					}
				}
			}
		}
	}

	@Override
	public IntensityVolumeMask extractPreserveBounds(VoxelBox box) {
		return new IntensityVolumeMask(super.extractPreserveBounds(box).getData());
	}

	@Override
	public IntensityVolumeMask extract(VoxelBox box) {
		return new IntensityVolumeMask(super.extract(box).getData());
	}

	public IntensityVolumeMask apply(VoxelBox box) {
		IntensityVolumeMask extraction = new IntensityVolumeMask(super.extract(box).getData());
		
		Point3i o = box.getOrigin();
		int[] dimExtract = extraction.getDimension();

		// Start from empty and fill
		scale(0);
		
		for (int i = 0; i < dimExtract[0]; i++) {
			for (int j = 0; j < dimExtract[1]; j++) {
				for (int k = 0; k < dimExtract[2]; k++) {
					set(o.x + i, o.y + j, o.z + k, extraction.get(i, j, k));
				}
			}
		}
		
		return this;
	}

	public void display(GLAutoDrawable drawable) {
		if (drawCom.getValue()) {
			GL2 gl = drawable.getGL().getGL2();
			Point3d pcc = getCenterOfMass();
			gl.glColor4d(0.8, 0.8, 0.8, 0.8);
			gl.glPointSize(10f);
			gl.glBegin(GL2.GL_POINTS);
			gl.glVertex3d(pcc.x, pcc.y, pcc.z);
			gl.glEnd();
		}
		
		int[] dim = getDimension();
		GL2 gl = drawable.getGL().getGL2();
		gl.glPushMatrix();
		gl.glPointSize(10f);
		gl.glTranslated(0.5, 0.5, 0.5);
		gl.glBegin(GL2.GL_POINTS);
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (!isMasked(i, j, k)) {
						gl.glVertex3d(i, j, k);
					}
				}
			}
		}
		gl.glEnd();
		gl.glPopMatrix();
	}

	/**
	 * Extrude this mask to all voxels that have value v. This method was created to patch up NaN boundaries.
	 * @param nan
	 * @return
	 */
	public IntensityVolumeMask extrude(IntensityVolume extruder, double v) {
		int[] dim = getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					double ev = extruder.get(i, j, k);
					if (!isMasked(i, j, k) && (Double.isNaN(ev) || Math.abs(ev - v) < EPSILON)) {
						set(i, j, k, 0);
					}
				}
			}
		}
		
		return this;
	}
	
	/**
	 * Invert this volume in place i.e. 0 -> 1, 1 -> 0
	 */
	public IntensityVolumeMask invert() {
		int[] dim = getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
						set(i, j, k, 1 - get(i, j, k));
				}
			}
		}
		
		toBinary();
		
		return this;
	}

	public IntensityVolumeMask erode(int dilation) {
		return dilate(new VoxelBox(this), dilation);
	}
	
	/**
	 * Erode this mask using a spherical structural element of size <code>dilation</code>.
	 * @param box
	 * @param dilation the size of the structural element (e.g. 3 for a 3-voxel isotropic element)
	 * @return
	 */
	public IntensityVolumeMask dilate(final VoxelBox box, int dilation) {
		final List<Point3i> neighbors = VolumeSampler.sample(NeighborhoodShape.Isotropic, dilation);
    	final Point3i pn = new Point3i();
    	
    	for (Point3i pt : box.getVolume()) {
			for (Point3i neighbor : neighbors) {
				// Compute neighbor position in world coordinates
				pn.add(pt, neighbor);

				// Only add if this voxel is within the box and not already an existing voxel
				if (contains(pn.x, pn.y, pn.z)) {
					set(pn, 1);
				}
			}
    	}
    	
		return this;
	}

	public int getUnmaskedCount() {
		int[] dim = getDimension();
		int v = 0;
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (!isMasked(i, j, k)) {
						v++;
					}
				}
			}
		}
		
		return v;
	}
	
	public int getMaskedCount() {
		int[] dim = getDimension();
		int v = 0;
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (isMasked(i, j, k)) {
						v++;
					}
				}
			}
		}
		
		return v;
	}
	
	public List<int[]> getVolumeIndexing() {
		int[] dim = getDimension();
		List<int[]> pts = new LinkedList<int[]>();
		
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (isMasked(i, j, k)) {
						pts.add(new int[] { i, j, k });
					}
				}
			}
		}
		
		return pts;
	}

	public List<Triplet<Boolean, Point3d, Double>> computeCenterlineAlongWorldZ() {
		int[] dims = getDimension();

		// Create a voxelbox with a transverse (XY) cut
		VoxelBox box = new VoxelBox(this);
		box.cut(CuttingPlane.TRANSVERSE);
		
		// Create the list of centroid points
		List<Triplet<Boolean, Point3d, Double>> centerline = new ArrayList<Triplet<Boolean, Point3d, Double>>();
		for (int z = 0; z < dims[2]; z++) {
			// Set the box to the current height value
			Point3i currentSlice = box.getOrigin();
			currentSlice.z = z;
			box.setOrigin(currentSlice);
			
			final Point3d centroid = new Point3d();
			final int[] count = new int[] { 0 };
			
			// First find the centroid
			box.voxelProcess(new PerVoxelMethodUnchecked() {
				
				@Override
				public void process(int x, int y, int z) {
					centroid.x += x;
					centroid.y += y;
					centroid.z += z;
					count[0]++;
				}
			});
			
			Boolean flag = false;
			if (count[0] > 0) {
				centroid.scale(1d / count[0]);
				flag = true;
			}
			else {
				centroid.set(Double.NaN, Double.NaN, Double.NaN);
				flag = false;
			}
			
			// Then find the maximal radius from that centroid
			final Point3d max = new Point3d(centroid);
			final Point3d current = new Point3d();
			box.voxelProcess(new PerVoxelMethodUnchecked() {
				@Override
				public void process(int x, int y, int z) {
					current.set(x, y, z);
					if (current.distance(centroid) > max.distance(centroid))
						max.set(current);
				}
			});			
			centerline.add(new Triplet<Boolean, Point3d, Double>(flag, centroid, max.distance(centroid)));
		}
		
		return centerline;
	}

	public double getDimensionLength() {
		int[] dims = getDimension();
		return Math.sqrt(dims[0]*dims[0] + dims[1] * dims[1] + dims[2] * dims[2]);
	}

	public void removeTopFront() {
		int sx = getDimension()[0];
		int sy = getDimension()[1];
		int sz = getDimension()[2];

		// Remove x > sx/2 && y > sy/2 && z > sz / 2
		for (int x = sx / 2; x < sx; x++)
			for (int y = 0; y < sy; y++)
				for (int z = sz / 2; z < sz; z++)
					set(x, y, z, 0);
	}

}
