package volume;

import gl.geometry.GLGeometry;
import gl.geometry.GLSimpleGeometry;
import gl.geometry.VertexBufferObject;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.HeartTools;
import heart.Heart.ImageType;

import java.awt.Color;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.StringTokenizer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import math.coloring.ColorMap;
import math.coloring.JColorizer;
import math.matrix.Matrix3d;
import swing.component.JColorMap;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.JHistogram;
import tools.geom.MathToolset;
import tools.loader.LineReader;
import voxel.VoxelBox;
import voxel.VoxelVectorField;

import com.jogamp.common.nio.Buffers;

import extension.matlab.MatlabIO;
import extension.minc.MINCImage;

public class IntensityVolume extends Matrix3d {

	public static boolean verbose = false;

	/**
	 * Create an empty volume.
	 */
	public IntensityVolume() {
		super();
	}

	public IntensityVolume(IntensityVolume other) {
		this(other.elements);
	}

	/**
	 * @param volumeData
	 * @param normalizedColorMap if the color map should be normalized such that all intensities fall within the range [0, 1]
	 */
	public IntensityVolume(double[][][] volumeData) {
		setVolume(volumeData);
	}

	public IntensityVolume(int[] dimension) {
		this(new double[dimension[0]][dimension[1]][dimension[2]]);
	}

	public IntensityVolume(int sx, int sy, int sz) {
		this(new double[sx][sy][sz]);
	}

	/**
	 * Assume double type.
	 * @param filename
	 */
	public <T extends Number> IntensityVolume(String filename) {
		this(filename, new Float(0));
	}
	
	public <T extends Number> IntensityVolume(String filename, T numberType) {
		// Determine filetype
		ImageType type = null;
		
		for (ImageType searchType : ImageType.values()) {
			if (filename.toLowerCase().endsWith(searchType.getFileType().toLowerCase())) {
				type = searchType;
				break;
			}
		}
		
		switch (type) {
		case MINC:
			setByReference(new MINCImage(filename).getVolume());
			break;
		case TXT:
			loadFromTxt(filename);
			break;
			
		case MAT:
			setByReference(MatlabIO.loadMAT(filename, numberType).asVolume());
			break;
		default:
			loadFromTxt(filename);
		}

	}
	
	private void loadFromTxt(String filename) {
		LineReader reader = new LineReader(filename);

		try {
			// Read dimension
			// Assume header has one single line with dimensions
			String dims = reader.readLine();
//			int[] dimension = new int[] { Integer.parseInt(dims.substring(0, 0)), Integer.parseInt(dims.substring(2, 2)), Integer.parseInt(dims.substring(4, 4)) };
			StringTokenizer tokenizer = new StringTokenizer(dims, ",");
			int dx = Integer.parseInt(tokenizer.nextToken());
			int dy = Integer.parseInt(tokenizer.nextToken());
			int dz = Integer.parseInt(tokenizer.nextToken());
			int[] dimension = new int[] { dx, dy, dz };
			
			double[][][] data = new double[dimension[0]][dimension[1]][dimension[2]];
			double[] datav = new double[dimension[0] * dimension[1] * dimension [2]];
			
			int l = 0;
			String s;
			while ((s = reader.readLine()) != null) {
				datav[l++] = Double.parseDouble(s);
			}

			// Then fill in 3D array
			l = 0;
			for (int k = 0; k < dimension[2]; k++) {
				for (int j = 0; j < dimension[1]; j++) {
					for (int i = 0; i < dimension[0]; i++) {
						data[i][j][k] = datav[l++];
					}
				}
			}
			
			setVolume(data);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public IntensityVolume(Matrix3d m) {
		this(m.getData());
	}

	public void setVolume(double[][][] volumeData) {
		super.setByReference(volumeData);
	}
	
	/**
	 * @return a copy of this volume
	 */
	@Override
	public IntensityVolume copy() {
		return new IntensityVolume(super.copy().getData());
	}

	public double getIntensity(int i, int j, int k) {
		return elements[i][j][k];
	}

	public double getIntensity(Point3i pt) {

		return getIntensity(pt.x, pt.y, pt.z);
	}

	public Point3i getDimensions() {
		return new Point3i(super.getDimension());
	}

	public double getMean(VoxelBox box) {
		final double[] total = new double[] { 0 };
		
		final int[] vcount = new int[] { 0 };
		box.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				double v = elements[x][y][z];
				
				if (!Double.isNaN(v)) {
					vcount[0]++;
					total[0] += v; 
				}
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});
		return total[0] / vcount[0];
	}
	
	public double[] getMinMax(IntensityVolumeMask mask) {
		final double[] minmax = new double[] { 0, 0 };
		
//		System.out.println(elements.length + "," + elements[0].length + "," + elements[0][0].length);
//		System.out.println(mask.getElements().length + "," + mask.getElements()[0].length + "," + mask.getElements()[0][0].length);
		
		int[] dimension = getDimension();	
		for (int k = 0; k < dimension[2]; k++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int i = 0; i < dimension[0]; i++) {
//		for (int k = 0; k < elements[0][0].length; k++) {
//			for (int j = 0; j < elements[0].length; j++) {
//				for (int i = 0; i < elements.length; i++) {
					double v = elements[i][j][k];
					if (!mask.isMasked(i, j, k) && !Double.isNaN(v)) {
						if (v < minmax[0]) minmax[0] = v; 
						if (v > minmax[1]) minmax[1] = v; 
					}
				}
			}
		}
		return minmax;
	}

	public double getMean(IntensityVolumeMask mask) {
		double sum = 0;
		int n = 0;
		int[] dimension = getDimension();
		for (int k = 0; k < dimension[2]; k++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int i = 0; i < dimension[0]; i++) {
					double v = elements[i][j][k];
					if (!mask.isMasked(i, j, k) && !Double.isNaN(v)) {
						n++;
						sum += v; 
					}
				}
			}
		}
		
//		System.out.println("sum = " + sum + ", n = " + n);

		if (n == 0)
			return 0;
		
		return sum / n;
	}

	/**
	 * There are faster ways to compute the STD in a single pass... See Chan,
	 * Golub & Leveque
	 */
	public double getStandardDeviation(IntensityVolumeMask mask) {
		double sum = 0;
		double mean = getMean(mask);
		int n = 0;
		int[] dimension = getDimension();
		for (int k = 0; k < dimension[2]; k++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int i = 0; i < dimension[0]; i++) {
					double v = elements[i][j][k];
					if (!mask.isMasked(i, j, k) && !Double.isNaN(v)) {
						n++;
//						System.out.println((v - mean));
						sum += (v - mean) * (v - mean); 
					}
				}
			}
		}
				
		return Math.sqrt(sum / n);
	}

	public double getStandardDeviation(VoxelBox box) {
		final double[] total = new double[] { 0 };
		
		final double mean = getMean(box);
		
		final int[] vcount = new int[] { 0 };
		box.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				double v = elements[x][y][z];
				
				if (!Double.isNaN(v)) {
					vcount[0]++;
					total[0] += (v - mean) * (v - mean); 
				}
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});
		return Math.sqrt(total[0] / vcount[0]);
	}


	/**
	 * Extract a portion of this volume based on the given voxelbox but preserve the size of the original volume.
	 * i.e. the rest of the volume is padded with zeros
	 * @param box
	 * @return
	 */
	public IntensityVolume extractPreserveBounds(VoxelBox box) {
		
		final IntensityVolume extracted = new IntensityVolume(getDimension());
		
		// Fill in the volume
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				extracted.set(x, y, z, get(x, y, z));
			}
		});
		
		return extracted;
	}
	
	/**
	 * Extract a portion of this volume based on the given voxelbox.
	 * @param box
	 * @return
	 */
	public IntensityVolume extract(VoxelBox box) {
		
		// Get the new volume dimension
		Point3i span = box.getSpan();
		final Point3i o = box.getOrigin();

		// Create an empty volume
//		final double[][][] data = new double[span.x + 1][span.y + 1][span.z + 1];
		final double[][][] data = new double[span.x][span.y][span.z];
		
		// Fill with NaNs
//		fillWith(Double.NaN, data);
		fillWith(0, data);
		final int[] dims = getDimension();
		
		// Fill in the volume
		box.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				if (x >= dims[0] || y >= dims[1] || z >= dims[2]) 
					return;
				
				if (x - o.x >= data.length || y - o.y >= data[0].length || z - o.z >= data[0][0].length)
					return;
				
				if (x - o.x < 0 || y - o.y < 0|| z - o.z < 0)
					return;
				
				double v = getIntensity(x, y, z);
				data[x - o.x][y - o.y][z - o.z] = v;
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});
		
		return new IntensityVolume(data);
	}
	
	/**
	 * Copy an intensity volume into this one. The volume dimensions must be the same.
	 * @param div
	 */
	public void setByReference(IntensityVolume other) {
		super.setByReference(other.elements);
	}

	public void createSphere() {
		int[] dim = getDimension();
		int sx  = dim[0] / 2;
		int sy  = dim[1] / 2;
		int sz  = dim[2] / 2;

		// Use 1/3 of volume for the radius
		Point3i center = new Point3i(sx, sy, sz);
		Point3i pt = new Point3i();
		double radius = Math.max(Math.max(sx, sy), sz) / 1.5;
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					pt.set(i, j, k);
					
					// Don't touch voxels outside of the sphere
					double dist = MathToolset.dist(pt, center);
					
//					volume[i][j][k] = dist;
					if (dist > radius) {
						elements[i][j][k] = 0;
					}
					else {
						elements[i][j][k] = dist / radius;
					}
 				}
			}
		}
	}
	
	public void translate(Point3i v) {
		
		Point3i pt = new Point3i();
		IntensityVolume icopy = copy();
		
		int[] dim = getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					pt.set(i, j, k);
					pt.add(v);
					
					if (pt.x >= 0 && pt.x < dim[0] && pt.y >= 0 && pt.y < dim[1] && pt.z >= 0 && pt.z < dim[2]) {
						elements[i][j][k] = icopy.get(pt.x, pt.y, pt.z);
					}
					else {
						elements[i][j][k] = 0;
					}
						
				}
			}
		}
	}

	public static ByteBuffer getBuffer(IntensityVolume volume) {
		return getBuffer(volume, null, null);
	}
	
	/**
	 * 
	 * @param volume
	 * @return
	 */
	public static ByteBuffer getBuffer(IntensityVolume volume, JColorMap colorMap, JHistogram histogram) {
		
		int width = volume.getDimension()[0];
		int height = volume.getDimension()[1];
		int depth = volume.getDimension()[2];

		double[] minmax = volume.getminMax();
		double vrange = minmax[1] - minmax[0];
		
		double vmin, vmax, v0;
		double rangePos, rangeNeg;
		
		if (colorMap != null) {
			vmin = colorMap.getMinValue();
			vmax = colorMap.getMaxValue();
			v0 = colorMap.getZeroValue();

			rangePos = Math.abs(vmax - v0);
			rangeNeg = Math.abs(vmin - v0);
		}
		else {
			
			v0 = 0;
			rangePos = 1;
			rangeNeg = 1;
		}
		
//		System.out.println("Zero = " + v0);
//		System.out.println("Range neg = " + rangeNeg);
//		System.out.println("Range pos = " + rangePos);
		
		int bytesPerPixel = 4;
		ByteBuffer data = Buffers.newDirectByteBuffer(width * height * depth * bytesPerPixel);
		
		double[][] intervals = null;
		if (histogram != null) {
			intervals = histogram.getThresholdedIntervals();			
		}
		
		if (intervals == null) {
			intervals = new double[][] { };
		}
		
		boolean thresholded;
		Color color = Color.BLACK;
		double backgroundFilling = 0;
		for (int z = 0; z < depth; z++) {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
						double v = volume.getIntensity(x, y, z);
//						System.out.println(v);
						thresholded = false;
						
						if (Double.isNaN(v)) {
							thresholded = true;
						}
						else if (colorMap != null) {
							color = colorMap.getColor(v);
							
							if (color == null) {
								thresholded = true;
							}
							else {
								// Apply interval thresholding
								for (double[] vi : intervals) {
									if (v >= vi[0] && v <= vi[1]) {
										thresholded = true;
										break;
									}
								}
							}
						}
						else {
//							System.out.println((float) (v / vrange));
//							color = new Color((float) (v / vrange), (float) (v / vrange), (float) (v / vrange));
							color = new Color((float) v, (float) v, (float) v);
						}
						
						if (thresholded) {
							data.put((byte) backgroundFilling);
							data.put((byte) backgroundFilling);
							data.put((byte) backgroundFilling);
							data.put((byte) backgroundFilling);
						}
						else {
							data.put((byte) (color.getRed()));
							data.put((byte) (color.getGreen()));
							data.put((byte) (color.getBlue()));
							data.put((byte) (255 * Math.abs(v - v0) / (v < v0 ? rangeNeg : rangePos)));
						}
				}
			}
		}

		return data;
	}

	public static ByteBuffer getBuffer(IntensityVolumeMask volume) {
		int width = volume.getDimension()[0];
		int height = volume.getDimension()[1];
		int depth = volume.getDimension()[2];
		
		
		int bytesPerPixel = 4;
		ByteBuffer data = Buffers.newDirectByteBuffer(width * height * depth * bytesPerPixel);
		
		byte backgroundFilling = 0;
		byte maskFilling = 1;
		for (int d = 0; d < depth; d++) {
			for (int h = 0; h < height; h++) {
				for (int w = 0; w < width; w++) {
						double v = volume.getIntensity(w, h, d);
						
						if (volume.isMasked(w, h, d) || Double.isNaN(v)) {
							data.put(backgroundFilling);
							data.put(backgroundFilling);
							data.put(backgroundFilling);
							data.put(backgroundFilling);
						}
						else {
							data.put(maskFilling);
							data.put(maskFilling);
							data.put(maskFilling);
							data.put(maskFilling);
						}
				}
			}
		}

		return data;
	}

	public void cutQuarter() {
		int[] dim = getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (k > dim[2] / 2 && j > dim[1] / 2) 
						elements[i][j][k] = 1;
//					else volume[i][j][k] = 1;
 				}
			}
		}
	}

	public void cutThreeQuarter() {
		int[] dim = getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (k > dim[2] / 2 && j > dim[1] / 2) 
						elements[i][j][k] = 0;
 				}
			}
		}
	}

	/**
	 * Set a single Z-slice from this volume.
	 * @param volume
	 * @param z
	 */
	public void setZ(IntensityVolume volume, int z) {
		int[] dim = getDimension();
		int k = z;
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				elements[i][j][k] = volume.elements[i][j][k];
			}
		}
	}

	/**
	 * Set a single X-slice from this volume.
	 * @param volume
	 * @param x
	 */
	public void setX(IntensityVolume volume, int x) {
		int[] dim = getDimension();
		int i = x;
		for (int j = 0; j < dim[1]; j++) {
			for (int k = 0; k < dim[2]; k++) {
				if (k > dim[2] / 2 && j > dim[1] / 2)
					elements[i][j][k] = volume.elements[i][j][k];
			}
		}
	}

	/**
	 * Set a single Y-slice from this volume.
	 * @param volume
	 * @param y
	 */
	public void setY(IntensityVolume volume, int y) {
		int[] dim = getDimension();
		int j = y;
		for (int i = 0; i < dim[0]; i++) {
			for (int k = 0; k < dim[2]; k++) {
				elements[i][j][k] = volume.elements[i][j][k];
			}
		}
	}

	public static void normalizeScale(IntensityVolume volume, GL2 gl) {
        int[] dim = volume.getDimension();
        double md = Math.max(Math.max(dim[0], dim[1]), dim[2]);

        gl.glScaled(1.0 / md, 1.0 / md, 1.0 / md);
	}

	public double[] flattenAndDiscard(final IntensityVolumeMask mask, final double epsilon) {
		int[] d = getDimension();
		
		// Create enough space
		double[] values = new double[d[0] * d[1] * d[2]];
		int l = 0;
		for (int i = 0; i < d[0]; i++) {
			for (int j = 0; j < d[1]; j++) {
				for (int k = 0; k < d[2]; k++) {
					double v = elements[i][j][k];
					if (Double.isNaN(v) || Math.abs(v) < epsilon) {
						continue;
					}
					else if (mask.isMasked(i, j, k)) {
						continue;
					}
					else
						values[l++] = elements[i][j][k];
				}
			}
		}

		// Do a second pass to trim spurious array elements
		double[] valuesTrimmed = Arrays.copyOf(values, l);
		return valuesTrimmed;
	}

	public double[] flattenAndDiscard(double epsilon) {
		int[] d = getDimension();
		
		// Create enough space
		double[] values = new double[d[0] * d[1] * d[2]];
		int l = 0;
		for (int i = 0; i < d[0]; i++) {
			for (int j = 0; j < d[1]; j++) {
				for (int k = 0; k < d[2]; k++) {
					double v = elements[i][j][k];
					
					if (Double.isNaN(v) || Math.abs(v) < epsilon) {
						continue;
					}
					else {
						values[l++] = elements[i][j][k];
					}
				}
			}
		}

		// Do a second pass to trim spurious array elements
		double[] valuesTrimmed = Arrays.copyOf(values, l);
		return valuesTrimmed;
	}
	
	public double[] flattenAndDiscard(IntensityVolumeMask mask) {
		int[] d = getDimension();
		double[] values = new double[d[0] * d[1] * d[2]];
		
		int l = 0;
		for (int i = 0; i < d[0]; i++) {
			for (int j = 0; j < d[1]; j++) {
				for (int k = 0; k < d[2]; k++) {
					if (mask.isMasked(i, j, k) || Double.isNaN(elements[i][j][k])) continue;
					
					values[l++] = elements[i][j][k];
				}
			}
		}
		return values;
	}

	public double[] flattenCompact(IntensityVolumeMask mask) {
		int[] d = getDimension();
		System.out.println("Flattening volume of dimension " + Arrays.toString(d) + "...");
		double[] values = new double[d[0] * d[1] * d[2]];
		
		int length = 0;
		for (int k = 0; k < d[2]; k++) {
			for (int j = 0; j < d[1]; j++) {
				for (int i = 0; i < d[0]; i++) {
					if (!mask.isMasked(i, j, k) && !Double.isNaN(elements[i][j][k])) {
						values[length++] = elements[i][j][k];
					}
				}
			}
		}
		
		// Compactify the array
		return Arrays.copyOf(values, length);
	}

	public double[] flatten() {
		int[] d = getDimension();
//		System.out.println("Flattening volume of dimension " + Arrays.toString(d) + "...");
		double[] values = new double[d[0] * d[1] * d[2]];
		
		int l = 0;
		for (int k = 0; k < d[2]; k++) {
			for (int j = 0; j < d[1]; j++) {
				for (int i = 0; i < d[0]; i++) {
					if (Double.isNaN(elements[i][j][k])) {
						values[l] = 0;
					}
					else {
						values[l] = elements[i][j][k];
					}
					l++;
				}
			}
		}
		return values;
	}

	public void exportToAscii(String filename) {
		System.out.println("Exporting to " + filename + "...");
		try {
			final PrintWriter out = new PrintWriter(new FileWriter(filename));
			
			double[] values = flatten();
			
			System.out.println(values.length + " values found.");
			
			int[] dims = getDimension();
			out.println(dims[0] + "," + dims[1] + "," + dims[2]);
			for (double v : values) {
				out.println(v);
			}

			out.close();
			System.out.println("Done.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	}
	

	/**
	 * Create a volume in which intensities run from -1 to 1
	 * @param sx
	 * @param sy
	 * @param sz
	 * @return
	 */
	public static IntensityVolume createRandomizedVolume(int sx, int sy, int sz) {
		
		IntensityVolume volume =  new IntensityVolume(sx, sy, sz);
		volume.randomize();

		return volume;
	}
	

	/**
	 * Create a volume in which intensities run from -1 to 1
	 * @param sx
	 * @param sy
	 * @param sz
	 * @return
	 */
	public static IntensityVolume createSphericalVolume(int sx, int sy, int sz) {
		
		IntensityVolume volume =  new IntensityVolume(sx, sy, sz);
		volume.createSphere();

		return volume;
	}

	public int getVolumeCount() {
		int[] dim = getDimension();;
		return dim[0] * dim[1] * dim[2];
	}

	public boolean hasPoint(Point3i pt) {
		return hasPoint(pt.x, pt.y, pt.z);
	}
	
	public boolean hasPoint(double x, double y, double z) {
		int[] dims = getDimension();
		
		if (x < 0) return false;
		else if (y < 0) return false;
		else if (z < 0) return false;
		else if (x >= dims[0]) return false;
		else if (y >= dims[1]) return false;
		else if (z >= dims[2]) return false;
		else return true;
	}

	public void display(final GL2 gl, VoxelBox voxelBox,
			final double normalization) {
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glPointSize(15f);
		gl.glBegin(GL2.GL_POINTS);
		voxelBox.voxelProcess(new PerVoxelMethod() {

			@Override
			public void process(int x, int y, int z) {
				double d = get(x, y, z) / normalization;
				gl.glColor4d(d, d, d, 1);
				gl.glVertex3d(x, y, z);
			}

			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});
		gl.glEnd();
	}

	/**
	 * Apply a nan mask to this volume.
	 * @param mask
	 */
	public void nanify(IntensityVolumeMask mask) {
		final IntensityVolumeMask copy = new IntensityVolumeMask(this);
		copy.fillWith(Double.NaN);
		
		VoxelBox box = new VoxelBox(getDimension());
		box.setMask(mask);
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				copy.set(x, y, z, get(x, y, z));
			}
		});
		
		this.elements = copy.elements;
	}

	public VoxelVectorField gradient() {
		return new VoxelVectorField("", dxOut().asVolume(), dyOut().asVolume(), dzOut().asVolume());
	}

	private VertexBufferObject vbo = null;
	
	private IntensityVolumeMask displayMask;
	private JColorizer colorMap;

	public void createDisplayMask(IntensityVolumeMask mask) {
		if (mask == null)
			return;
		
		this.displayMask = mask;
		createColorMapIfNecessary();
		vbo = new VertexBufferObject(this, mask, colorMap.getMap());

		double[] minMax = getminMax();
		colorMap.setMinMax(minMax[0], minMax[1]);
	}

	public void displayAsVoxels(GLAutoDrawable drawable, IntensityVolumeMask mask) {
		if (displayMask == null)
			createDisplayMask(mask);
		displayAsVoxels(drawable);
	}
	
	public void displayAsVoxels(GLAutoDrawable drawable) {
		if (vbo == null)
			return;
		else {
//			GL2 gl = drawable.getGL().getGL2();
//			gl.glPushMatrix();
//			double s = getMaxDimension();
//			gl.glScaled(1/s, 1/s, 1/s);
//			Point3d cm = displayMask.getCenterOfMass();
//			cm.negate();
//			gl.glTranslated(cm.x, cm.y, cm.z);
			vbo.display(drawable);
//			gl.glPopMatrix();
		}
	}
	
	public void setColorMap(JColorizer colorizer) {
		if (this.colorMap == null)
			this.colorMap = colorizer;
		else
			this.colorMap.setMap(colorizer.getMap());
		
	}
	
	private void createColorMapIfNecessary() {
		if (colorMap == null) {
			colorMap = new JColorizer(ColorMap.Map.HOT);
			double[] minMax = getminMax();
			colorMap.setMinMax(minMax[0], minMax[1]);
			colorMap.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					createDisplayMask(displayMask);
				}
			});
		}
	}
	
	public JPanel getDisplayPanel() {
		VerticalFlowPanel vfp = new VerticalFlowPanel("Volume Rendering Test");

		createColorMapIfNecessary();
		vfp.add(colorMap);
		
		return vfp.getPanel();
	}
	
	public static void main(String[] args) {
		NodeScene scene = new NodeScene();
		Heart heart = HeartTools.heartSample();
		final IntensityVolume volume = heart.getDistanceTransformEpi();
		volume.createDisplayMask(heart.getMask());
		scene.addNode(new GLSimpleGeometry() {
			@Override
			public void display(GLAutoDrawable drawable) {
				volume.displayAsVoxels(drawable);
			}
		});
		
		scene.addControl(volume.getDisplayPanel());
	}

}