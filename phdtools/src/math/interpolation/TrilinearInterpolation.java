package math.interpolation;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.JoglRenderer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

public class TrilinearInterpolation {

	private static int[][] coefficients = new int[][]
		{
		{ 1, 0, 0, 0, 0, 0, 0, 0 },
		{ -1, 0, 0, 0, 1, 0, 0, 0 },
		{ -1, 0, 1, 0, 0, 0, 0, 0 },
		{ -1, 1, 0, 0, 0, 0, 0, 0 },
		{ 1, 0, -1, 0, -1, 0, 1, 0 },
		{ 1, -1, -1, 1, 0, 0, 0, 0 },
		{ 1, -1, 0, 0, -1, 1, 0, 0 },
		{ -1, 1, 1, -1, 1, -1, -1, 1 }
		};
	
	public static double interpolate(Point3d p, Point3d q0, Point3d q1, double[] values) {
		/*
		 * Represent the cube going from (qx0,qy0,qz0) to (qx1,qy1,qz1)
		 * Values are assumed to be listed in the following order:
		 * p000, p001, p010, p011, p100, p101, p110, p111
		 */
		double dx = (p.x - q0.x) / (q1.x - q0.x);
		double dy = (p.y - q0.y) / (q1.y - q0.y);
		double dz = (p.z - q0.z) / (q1.z - q0.z);

		// Compute coefficients
		double[] coefs = new double[8];
		for (int i = 0; i < 8; i++) {
			coefs[i] = 0;
			for (int j = 0; j < 8; j++) {
				coefs[i] += coefficients[i][j] * values[j];
			}
		}
		
		// Compute distance vector
		double[] Q = new double[] { 1, dx, dy, dz, dx * dy, dy * dz, dx * dz, dx * dy * dz };
		
		// Compute interpolation at at p
		double v = 0;
		for (int i = 0; i < 8; i++) {
			v += coefs[i] * Q[i];
		}
		
		return v;
	}
	
	public static void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glDisable(GL2.GL_LIGHTING);
		
		gl.glPushMatrix();
		gl.glScaled(2,2,2);
		gl.glPointSize(15f);
		
		double dp = 0.05;
		Point3d p = new Point3d();
		Point3d q0 = new Point3d();
		Point3d q1 = new Point3d(1,1,1);
		double[] rvalues = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.5 }; 
		double[] gvalues = new double[] { 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.2 }; 
		double[] bvalues = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.8 }; 
		for (double i = 0; i < 1; i+=dp) {
			for (double j = 0; j < 1; j+=dp) {
				for (double k = 0; k < 1; k+=dp) {
					p.set(i,j,k);
					double r = interpolate(p, q0, q1, rvalues);
					double g = interpolate(p, q0, q1, gvalues);
					double b = interpolate(p, q0, q1, bvalues);
					
					gl.glColor4d(r, g, b, 0.5);
					gl.glBegin(GL2.GL_POINTS);
					gl.glVertex3d(i,j,k);
					gl.glEnd();
				}
			}
		}
		gl.glPopMatrix();
	}

	public static void main(String[] args) {
		JoglRenderer jr = new JoglRenderer("Renderer test");
		
		jr.start(new GLObject() {
			
			@Override
			public void init(GLAutoDrawable drawable) {
			}
			
			@Override
			public String getName() {
				return "Trilinear Interpolator";
			}
			
			@Override
			public void display(GLAutoDrawable drawable) {
				TrilinearInterpolation.display(drawable);
			}
			
			@Override
			public void reload(GLViewerConfiguration config) {
			}
			
			@Override
			public JPanel getControls() {
				return null;
			}
		});
		jr.setDrawFPS(true);
		jr.getCamera().zoom(-200f);
	}

}
