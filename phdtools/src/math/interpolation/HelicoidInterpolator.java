package math.interpolation;

import helicoid.Helicoid;
import helicoid.PiecewiseHelicoid;
import helicoid.modeling.HelicoidGenerator;
import helicoid.parameter.HelicoidParameter;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Quat4d;
import javax.vecmath.Vector3d;


import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.ParameterListener;
import tools.geom.OrientedPoint3d;
import tools.geom.Polyline;
import tools.interpolation.FrameInterpolator;
import tools.interpolation.GaussianInterpolator;
import tools.interpolation.ParameterVector;
import tools.interpolation.GaussianInterpolator.METRIC;

public class HelicoidInterpolator {
	/**
	 * Frame interpolation
	 */
	private FrameInterpolator finterpolator = new FrameInterpolator();

	/**
	 * Helicoid interpolation
	 */
	private GaussianInterpolator hinterpolator = new GaussianInterpolator();

	private HelicoidGenerator generator;

	private DoubleParameter lengthDropoff = new DoubleParameter(
			"length dropoff", 0, 0, 5);
	{
		lengthDropoff.setChecked(true);
	}

	private DoubleParameter lengthScale = new DoubleParameter("length scale",
			15, 0, 100);
	{
		lengthScale.setChecked(true);
	}

	/**
	 * Sets a threshold on the magnitude that negative k_T can have before it is
	 * flipped. Can use something like 0.2...
	 */
	private DoubleParameter kTthresh = new DoubleParameter(
			"k_T flip threshold", 0, 0, 2);
	{
		kTthresh.setChecked(false);
	}

	public HelicoidInterpolator(HelicoidGenerator generator) {
		this.generator = generator;

		finterpolator.setPower(3);

		hinterpolator.setPower(3);
	}

	public void setHelicoidLengthScale(double v) {
		hinterpolator.setSigma(v);
	}

	public void setFrameLengthScale(double v) {
		finterpolator.setSigma(v);
	}

	public void setNormH(METRIC m) {
		hinterpolator.setNorm(m);
	}

	public void setNormF(METRIC m) {
		finterpolator.setNorm(m);
	}

	public void setPowerH(double v) {
		hinterpolator.setPower(v);
	}

	public void setPowerF(double v) {
		finterpolator.setPower(v);
	}

	public METRIC getNormH() {
		return hinterpolator.getNorm();
	}

	public void interpolatePieceByPiece(
			List<PiecewiseHelicoid> controlHelicoids, List<Point3d> nodes,
			List<PiecewiseHelicoid> interpolatedHelicoids) {

		interpolateOrientedPieceByPiece(controlHelicoids,
				getOrientedNodes(nodes), interpolatedHelicoids);
	}

	/*
	 * This will not work if the component is zero. Averaging (0,q) and (0,-q)
	 * which are the same rotation gives 0, which is meaningless.
	 * 
	 * Thus the average and scale procedure makes only sense if all quaternions
	 * are oriented the same way. One way to achieve this for any set of unit
	 * quaternions that do not stray too much is the following:
	 * 
	 * 1. apply to all quaternions a rotation that moves one of them to 1 (for
	 * example one that is closest to the trivial average), 2. orient all
	 * results to positive real part, 3. average the results, 4. rotate back the
	 * result, 5. normalize.
	 * 
	 * More costly but completely reliable.
	 * 
	 * 
	 * Arnold Neumaier
	 */

	public void interpolateOrientedPieceByPiece(
			List<PiecewiseHelicoid> controlHelicoids,
			List<OrientedPoint3d> nodes,
			List<PiecewiseHelicoid> interpolatedHelicoids) {
		interpolatedHelicoids.clear();

		// Set this using a parameter
		double kT_threshold = kTthresh.isChecked() ? kTthresh.getValue()
				: Double.MAX_VALUE;

		// List containing local frames
		List<ParameterVector> frameNodes = new LinkedList<ParameterVector>();
		List<ParameterVector> flipNodes = new LinkedList<ParameterVector>();
		Matrix4d frame = new Matrix4d();
		for (PiecewiseHelicoid ph : controlHelicoids) {

			// Properly enforce k_T positivity
			HelicoidParameter hpf = ph.get(0).getParameter();

			frame.set(ph.get(0).getRotationFrame());

			if (hpf.KT < -kT_threshold) {
				Matrix4d kflip = new Matrix4d();
				kflip.setIdentity();
				kflip.setRotation(new AxisAngle4d(1, 0, 0, Math.PI));

				// frame.mul(kflip, frame);
				frame.mul(frame, kflip);

				flipNodes.add(new ParameterVector(ph.getOrigin(),
						new double[] { -1 }));
			} else {
				flipNodes.add(new ParameterVector(ph.getOrigin(),
						new double[] { +1 }));
			}

			// Add the node corresponding to the local frame
			Quat4d q = new Quat4d();
			frame.get(q);
			q.normalize();

			// System.out.println(q);
			frameNodes.add(new ParameterVector(ph.getOrigin(), new double[] {
					q.x, q.y, q.z, q.w }));
		}

		// List containing length parameters
		List<ParameterVector> pieceNodes = new LinkedList<ParameterVector>();

		for (PiecewiseHelicoid ph : controlHelicoids) {
			// Build # of pieces node
			pieceNodes.add(new ParameterVector(ph.getOrigin(), ph.getCount()));

		}

		// FIXME: Set the lengthscale more carefully... in some very sparse
		// datasets that might not be enough.
		// on the other hand, in very dense datasets it might be too much...
		// double lengthScale = 5*hinterpolator.getSigma();
		double lengthScale = this.lengthScale.isChecked() ? this.lengthScale
				.getValue() : Double.MAX_VALUE;

		HelicoidParameter hp = new HelicoidParameter();

		// Interpolate # of desired pieces at each new location
		double tscale = generator.getParameterControl().getTraceScale();
		int scount = generator.getParameterControl().getStepCount();
		double ssize = generator.getParameterControl().getStepSize();
		double eps = 1e-8;
		Random rand = new Random();
//		Matrix4d kflip = new Matrix4d();
		for (OrientedPoint3d node : nodes) {
			Point3d p = new Point3d(node);

			// Determine the number of pieces
			ParameterVector pparam = hinterpolator
					.interpolate(node, pieceNodes);
			double closest = Math.rint(pparam.data[0]);
			double incompletePercentage = Math.abs(closest - pparam.data[0]);
			int completePieces;
			double res;

			// Consider as complete if the residual is small
			if (incompletePercentage < eps) {
				pparam.data[0] = closest;
				incompletePercentage = 0;
				completePieces = (int) closest;
				res = 1;
			} else {
				completePieces = (int) Math.ceil(pparam.data[0]);
				/*
				 * Compute length residual. Since we round up, the smaller the
				 * residual, the shorter the last piece should be.
				 */
				res = 1 - Math.abs(completePieces - pparam.data[0]);
			}

			/*
			 * If the residual is 0, then we hit a length exactly: res = res <=
			 * eps ? 1 : res;
			 */
			double rscale = tscale / pparam.data[0];

			// The first orientation is that of the node
			// FIXME: don't use negative kt, rotate instead
			Quat4d q = new Quat4d(
					finterpolator.interpolate(node, frameNodes).data);
			frame.set(q);
			// frame.setIdentity();
			// frame.mul(frame, node.getFrame());
			frame.mul(node.getFrame(), frame);
			generator.setOrientationFrame(frame);

			// Initialize the p-helicoid
			PiecewiseHelicoid phelicoid = new PiecewiseHelicoid();

			// Prepare parameter nodes: helicoid, step count, trace scale
			List<ParameterVector> helicoidNodes = new LinkedList<ParameterVector>();
			List<ParameterVector> scaleNodes = new LinkedList<ParameterVector>();
			List<ParameterVector> countNodes = new LinkedList<ParameterVector>();

			for (int i = 0; i < completePieces; i++) {

				// List containing helicoid parameters
				helicoidNodes.clear();
				scaleNodes.clear();
				countNodes.clear();

				// Process the control helicoids
				double cdist = Double.MAX_VALUE;
				double avgdist = 0;
				for (PiecewiseHelicoid ph : controlHelicoids) {
					// If this control helicoid is located beyond
					// the length scale, skip it.
					double dist = ph.getOrigin().distance(node);
					avgdist += dist;
					if (dist < cdist)
						cdist = dist;
					if (dist > lengthScale) {
						continue;
					}

					Helicoid current;

					// Repeat the last piece from that control helicoid
					// if it has not enough pieces.
					if (i >= ph.getCount()) {
						// current = ph.get(ph.getCount() - 1);
						// hp.set(current.getParameter());
						// Use straight line to smooth the end
						hp.set(0, 0, 0, 0);
						// continue;
					} else {
						current = ph.get(i);
						hp.set(ph.get(i).getParameter());
						scaleNodes.add(new ParameterVector(ph.getOrigin(),
								new double[] { current.getTraceScale() }));
						countNodes.add(new ParameterVector(ph.getOrigin(),
								new double[] { current.getCount() }));
					}

					// Adjust the sign of the tangential curvature
					// for the first helicoid.
					if (i == 0 && hp.KT < -kT_threshold) {
						hp.KT = Math.abs(hp.KT);
					}

//					System.out.println(hp);
					helicoidNodes.add(new ParameterVector(ph.getOrigin(), hp
							.asArray()));
				}

				// Average distance to control helicoids
				avgdist /= controlHelicoids.size();
				
//				System.out.println("Min distance = " + cdist);
//				System.out.println("Average distance = " + avgdist);

				double ld = 1;
				if (lengthDropoff.isChecked()) {
					ld = Math.sqrt(avgdist) / 5;
				} else {
					ld = lengthDropoff.getValue();
				}

				if (helicoidNodes.size() == 0) {
					// System.err.println("Piece has no corresponding helicoid");
					break;
				}

				if (countNodes.size() == 0) {
					// System.err.println("Piece has no corresponding count node");
					break;
				}

				// Interpolate the helicoid parameter for the current piece
				hp.set(hinterpolator.interpolate(node, helicoidNodes).data);

				int iscount = (int) Math.rint(hinterpolator.interpolate(node,
						countNodes).data[0]);

				// ld proportional to average distance to guide hairs
				// cdist is the distance to the closest guide hair
//				double irscale = (1 - ld * Math.log(1 + cdist));
				double irscale = (1 - ld * Math.pow(Math.log(1 + cdist),2));
//				double irscale = 1;
				irscale *= hinterpolator.interpolate(node, scaleNodes).data[0];
				
				// Incorporate 5% length randomness
//				irscale = irscale + 0.05 * irscale * 2 * (-0.5 + rand.nextDouble());
				
				// double irscale = hinterpolator.interpolate(node,
				// scaleNodes).data[0];

				// System.out.println("Piece " + i + " scale = " + irscale);
				/*
				 * TODO: maybe interpolate the full length instead?
				 */

				// System.out.println(irscale);

				/*
				 * Set the length, trace scale, and count
				 */
				if (i == completePieces - 1) {
					boolean traceChange = true;

					if (!traceChange) {
						generator.getParameterControl().setTraceScale(res * irscale);
						generator.getParameterControl().setTargetCount(iscount);
						generator.getParameterControl().setStepSize(ssize);
					} else {
						generator.getParameterControl().setTraceScale(res * irscale);
						generator.getParameterControl().setStepCount(iscount);
						generator.getParameterControl().setStepSize(ssize);
					}

				} else {
					generator.getParameterControl().setTargetCount(iscount);
					generator.getParameterControl().setTraceScale(irscale);
					generator.getParameterControl().setStepSize(ssize);
				}

				/*
				 * Generate the piece
				 */
				Helicoid piece = generator.generate(hp, p);

				if (i == 0) {
					double flipm = finterpolator.interpolate(node, flipNodes).data[0];
					if (flipm < 0) {
						frame.setRotation(new AxisAngle4d(1, 0, 0, Math
								.abs(flipm) * Math.PI));
						// frame.setRotation(new AxisAngle4d(0, 1, 0, 0));

						Matrix4d eframe = piece.getEndFrame();
						// piece.getEndFrame().mul(frame);
						// piece.getEndFrame().mul(frame, eframe);
						piece.getEndFrame().mul(eframe, frame);
					}
				}

				// generator.getParameterControl().setOrientationFrame(piece.getEndFrame());

				p.set(piece.getTip());

				// Add the new piece and align
				phelicoid.add(piece, true);

				// the next piece is not re-aligned with respect to world
				// coordinates
				if (i == 0) {
					frame.setIdentity();
					generator.setOrientationFrame(frame);
				}
			}

			interpolatedHelicoids.add(phelicoid);
			
//			if (interpolatedHelicoids.size() % (nodes.size() / 5) == 0) {
//				System.out.println("Interpolated "
//						+ interpolatedHelicoids.size() + "/" + nodes.size());
//			}
		}

		// Adjust parameters
		for (PiecewiseHelicoid ph : interpolatedHelicoids) {
			// Add any adjustment here
		}

		generator.getParameterControl().setTraceScale(tscale);
		generator.getParameterControl().setStepSize(ssize);
		generator.getParameterControl().setStepCount(scount);

	}

	private List<OrientedPoint3d> getOrientedNodes(List<Point3d> nodes) {
		List<OrientedPoint3d> pts = new LinkedList<OrientedPoint3d>();
		Matrix4d iden = new Matrix4d();
		iden.setIdentity();

		for (Point3d p : nodes) {
			pts.add(new OrientedPoint3d(p, iden));
		}

		return pts;

	}

	/**
	 * Interpolate control helicoids at locations defined by nodes and stores
	 * the result in interpolatedHelicoids.
	 * 
	 * @param controlHelicoids
	 * @param nodes
	 * @param interpolatedHelicoids
	 * @param sign
	 * @return
	 */
	public List<Helicoid> interpolate(List<Helicoid> controlHelicoids,
			List<Point3d> nodes, List<Helicoid> interpolatedHelicoids, int sign) {

		List<PiecewiseHelicoid> phs = new LinkedList<PiecewiseHelicoid>();
		
		for (Helicoid h: controlHelicoids) {
			PiecewiseHelicoid ph = new PiecewiseHelicoid();
			ph.add(h);
		}
		
		List<PiecewiseHelicoid> iphs = new LinkedList<PiecewiseHelicoid>();
		interpolatePieceByPiece(phs, nodes, iphs);
		
		for (PiecewiseHelicoid ph : iphs) {
			interpolatedHelicoids.add(ph.get(0));
		}

		return interpolatedHelicoids;
		
	}

	public List<Helicoid> interpolate(List<Helicoid> controlHelicoids, List<Point3d> nodes) {

		List<PiecewiseHelicoid> phs = new LinkedList<PiecewiseHelicoid>();
		List<Helicoid> interpolatedHelicoids = new LinkedList<Helicoid>();
		for (Helicoid h: controlHelicoids) {
			PiecewiseHelicoid ph = new PiecewiseHelicoid();
			ph.add(h);
		}
		
		List<PiecewiseHelicoid> iphs = new LinkedList<PiecewiseHelicoid>();
		interpolatePieceByPiece(phs, nodes, iphs);
		
		for (PiecewiseHelicoid ph : iphs) {
			interpolatedHelicoids.add(ph.get(0));
		}

		return interpolatedHelicoids;
		
	}

	public void interpolateEuclidPiecewise(
			List<PiecewiseHelicoid> controlHelicoids, List<Point3d> nodes,
			List<PiecewiseHelicoid> interpolatedHelicoids) {

		List<Polyline> ch = new LinkedList<Polyline>();
		List<Polyline> ih = new LinkedList<Polyline>();
		for (PiecewiseHelicoid h : controlHelicoids) {
			ch.add(h.toStrand());
		}

		interpolateEuclidHairs(ch, nodes, ih);

		interpolatedHelicoids.clear();
		for (Polyline h : ih) {
			Helicoid hel = new Helicoid(h);
			PiecewiseHelicoid ph = new PiecewiseHelicoid();
			ph.add(hel);
			interpolatedHelicoids.add(ph);
		}

	}

	public void interpolateEuclid(List<Helicoid> controlHelicoids,
			List<Point3d> nodes, List<Helicoid> interpolatedHelicoids) {
		List<Polyline> ch = new LinkedList<Polyline>();
		List<Polyline> ih = new LinkedList<Polyline>();
		for (Helicoid h : controlHelicoids) {
			ch.add(h);
		}

		interpolateEuclidHairs(ch, nodes, ih);

		interpolatedHelicoids.clear();
		for (Polyline h : ih) {
			Helicoid hel = new Helicoid(h);
			interpolatedHelicoids.add(hel);
		}
	}

	/**
	 * Interpolate control helicoids at locations defined by nodes and stores
	 * the result in interpolatedHelicoids.
	 * 
	 * @param controlHelicoids
	 * @param nodes
	 * @param interpolatedHelicoids
	 * @return
	 */
	public List<Polyline> interpolateEuclidHairs(List<Polyline> controlHairs,
			List<Point3d> nodes, List<Polyline> interpolatedHairs) {
		if (nodes.size() == 0)
			return interpolatedHairs;

		interpolatedHairs.clear();

		// List containing vectors
		List<ParameterVector> vlist = new LinkedList<ParameterVector>();

		// Find the shortest helicoid
		int shortest = Integer.MAX_VALUE;
		for (Polyline h : controlHairs) {
			if (h.getCount() < shortest) {
				shortest = h.getCount();
			}
		}

		// Helicoids are built from the ground up
		// The first point is the node itself
		List<List<Point3d>> pts = new LinkedList<List<Point3d>>();
		for (int i = 0; i < nodes.size(); i++) {
			List<Point3d> h = new LinkedList<Point3d>();
			h.add(new Point3d(nodes.get(i)));
			pts.add(h);
		}

		// Target vector length
		double length = generator.getParameterControl().getStepSize();

		// For each segment, interpolate
		Vector3d segment = new Vector3d();
		for (int i = 0; i < shortest - 1; i++) {
			for (Polyline h : controlHairs) {

				// The shorted helicoids will have their end vectors repeated
				// over at each iteration
				if (h.getCount() < i + 2) {
					segment.sub(h.getPoint(h.getCount() - 1),
							h.getPoint(h.getCount() - 2));
				}
				// Otherwise just find next segment
				else {
					segment.sub(h.getPoint(i + 1), h.getPoint(i));
				}

				double[] value = new double[] { segment.x, segment.y, segment.z };

				vlist.add(new ParameterVector(new Point3d(h.getPoint(i)), value));
			}

			for (int k = 0; k < nodes.size(); k++) {
				Point3d p = pts.get(k).get(i);

				ParameterVector v = hinterpolator.interpolate(p, vlist, true);

				segment.set(v.data[0], v.data[1], v.data[2]);
				segment.normalize();
				segment.scale(length);

				// Start from previous point
				Point3d pt = new Point3d();
				pt.set(pts.get(k).get(i));

				// Add increment
				pt.add(segment);

				// Add new point
				pts.get(k).add(pt);
			}

		}

		// Finally construct helicoid from points
		// use zero helicoid parameters.
		for (int i = 0; i < nodes.size(); i++) {
			Helicoid h = new Helicoid(pts.get(i), new HelicoidParameter());
			interpolatedHairs.add(h);
		}

		return interpolatedHairs;
	}

	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();

		VerticalFlowPanel vfpfi = new VerticalFlowPanel();
		vfpfi.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Frame Interpolation"));
		vfpfi.add(finterpolator.getControls());
		vfpfi.add(kTthresh.getSliderControlsExtended("enabled"));
		vfpfi.add(lengthScale.getSliderControlsExtended("enabled"));

		CollapsiblePanel cpfi = new CollapsiblePanel(vfpfi.getPanel());

		VerticalFlowPanel vfphi = new VerticalFlowPanel();
		vfphi.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Helicoid Interpolation"));
		CollapsiblePanel cphi = new CollapsiblePanel(vfphi.getPanel());
		vfphi.add(lengthDropoff.getSliderControlsExtended("auto"));
		vfphi.add(hinterpolator.getControls());

		vfp.add(cpfi);
		vfp.add(cphi);

		cpfi.collapse();
		cphi.collapse();

		return vfp.getPanel();
	}

	public void addParameterListener(ParameterListener il) {
		lengthDropoff.addParameterListener(il);
		kTthresh.addParameterListener(il);
		finterpolator.addListener(il);
		hinterpolator.addListener(il);
	}

	public void setFlippingEnabled(boolean b) {
		kTthresh.setChecked(true);
	}

	public void setLengthDropoffEnabled(boolean b) {
		lengthDropoff.setChecked(false);
		
	}

}
