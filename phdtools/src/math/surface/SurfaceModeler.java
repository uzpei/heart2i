package math.surface;

import gl.math.FlatMatrix4d;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.ParameterListener;
import tools.geom.BoundingBox;

public class SurfaceModeler {

    private Surface surface;

    private IntParameter samplingSize = new IntParameter("sampling divisions",
            2, 0, 50);

    private DoubleParameter samplingRandomness = new DoubleParameter("sampling randomness",
            0.01, 0, 1);

    private BooleanParameter displaySampling = new BooleanParameter("display sampling", false);

    private BooleanParameter display = new BooleanParameter("display", true);

    public enum SurfaceType { plane, hemisphere, mesh };
    
    
    private FlatMatrix4d transformation = new FlatMatrix4d();
    {
    	transformation.getBackingMatrix().setIdentity();
    }
    
    public void setTransformation(Matrix4d t) {
    	transformation.setBackingMatrix(t);
    }
    
    public void rotateX() {
    	rotate(new AxisAngle4d(1, 0, 0, 3*Math.PI / 2));
    }
    public void rotateY() {
    	rotate(new AxisAngle4d(0, 1, 0, Math.PI / 2));
    }
    public void rotateZ() {
    	rotate(new AxisAngle4d(0, 0, 1, Math.PI / 2));
    }
    
    public void rotate(AxisAngle4d a) {
    	Matrix4d t = new Matrix4d();
    	t.setIdentity();
    	t.setRotation(a);
    	
    	transformation.setBackingMatrix(t);
    }
    
    /**
     * Create a surface modeler. Implementation includes computation of the
     * convex hull and bounding box, and particle methods for sampling the
     * surface.
     */
    public SurfaceModeler(SurfaceType s) {
        Surface sf;
        switch (s) {
        case plane :
            surface = new PlanarSurface();
            break;
        case hemisphere :
            surface = new HemiSurface();
            break;
        case mesh :
            break;
        default :
            surface = new PlanarSurface();
        }
        
        samplingRandomness.setChecked(false);
    }

    public SurfaceModeler() {
    	this(SurfaceType.plane);
    }

    public JPanel getControls() {
    	return getControls(true);
    }
    /**
     * @param handler
     * @return The UI controls for this class.
     */
    public JPanel getControls(boolean useSampling) {
        VerticalFlowPanel p = new VerticalFlowPanel();
        p.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createEtchedBorder(), "Surface Modeler"));

        p.add(display.getControls());

        if (useSampling) {
            p.add(displaySampling.getControls());
        }
        
        p.add(surface.getControls());

        if (useSampling) {
            p.add(samplingSize.getSliderControls());
            p.add(samplingRandomness.getSliderControlsExtended());
        }
        
        CollapsiblePanel cp = new CollapsiblePanel(p.getPanel());
//        cp.collapse();

        return cp;
    }

    private List<Point3d> pt3d = null;

    /**
     * Samples the surface.
     * 
     * @param points
     * @return A sampling of the surface contained in the convex hull of the
     *         follicles.
     */
    public List<Point3d> getSampling(List<Point3d> points) {
        // For now just find the bounding box and add sample points

        List<Point2d> samples = BoundingBox.getGridSampling(samplingSize
                .getValue(), points);

        // 2D to 3D
        pt3d = new ArrayList<Point3d>();
        for (Point2d p : samples) {
            pt3d.add(new Point3d(p.x, 0, p.y));
        }

        return pt3d;
    }
    
    public void randomizeSampling(List<Point3d> pts) {
        double dist = samplingRandomness.getValue();
        Random rand = new Random();
        for (Point3d p : pts) {
            p.x += -dist + 2*dist*rand.nextGaussian();   
            p.y += -dist + 2*dist*rand.nextGaussian();   
            p.z += -dist + 2*dist*rand.nextGaussian();   
        }
    }
    
    public void setSamplingDivisions(int v) {
        samplingSize.setValue(v);
    }

    public int getSamplingDivisions() {
        return samplingSize.getValue();
    }

    public List<Point3d> getSampling() {
        return getSampling(samplingSize.getValue());
    }

    public List<Point3d> getSampling(int density) {
        List<Point3d> points = new LinkedList<Point3d>();
        
        if (surface instanceof PlanarSurface) {
            points = ((PlanarSurface) surface).getExtrema();
            List<Point2d> samples = BoundingBox.getGridSampling(density, points);

            // 2D to 3D
            points.clear();
            for (Point2d p : samples) {
                points.add(new Point3d(p.x, 0, p.y));
            }
        }
        
        if (samplingRandomness.isChecked())
        randomizeSampling(points);
        
        return points;
    }

    public void getSampling(List<Point3d> points, List<Matrix4d> ms) {
        points.clear();
        points.addAll(getSampling());
    }

    public void display(GLAutoDrawable drawable) {
    	if (!display.getValue()) return;
    	
    	GL2 gl = drawable.getGL().getGL2();
    	gl.glPushMatrix();

    	gl.glMultMatrixd(transformation.asArray(), 0);
    	
        surface.display(drawable);

        pt3d = new ArrayList<Point3d>();
        pt3d = getSampling();

        gl.glDisable(GL2.GL_LIGHTING);

        if (displaySampling.getValue()) {
            gl.glPointSize(5);
            gl.glColor3f(1, 0, 0);
            gl.glBegin(GL.GL_POINTS);
            for (Point3d p : pt3d) {
                gl.glVertex3d(p.x, p.y, p.z);
            }
            gl.glEnd();
        }
        
        gl.glPopMatrix();
    }

    public Surface getSurface() {
        return surface;
    }

    public void addParameterListener(ParameterListener l) {
        surface.addParameterListener(l);
        samplingSize.addParameterListener(l);
        samplingRandomness.addParameterListener(l);
    }

	public double getSamplingDistance(List<Point3d> points, int density) {
		return BoundingBox.getSamplingDistance(points, density);
	}

    public void setDimension(double x, double y) {
        if (surface instanceof PlanarSurface) {
            PlanarSurface ps = (PlanarSurface) surface;
            ps.setSize(x, y);
        }
    }
    
    
    public void setVisible(boolean b) {
    	display.setValue(b);
    }
}
