package math.surface;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.ParameterListener;
import tools.geom.BoundingBox;

public class PlanarSurface implements Surface {

    /**
     * The origin of this plane (X).
     */
    private DoubleParameter originX = new DoubleParameter("Origin X", 0, -50, 50);

    /**
     * The origin of this plane (Y).
     */
    private DoubleParameter originY = new DoubleParameter("Origin Y", 0, -50, 50);

    /**
     * The origin of this plane (Z).
     */
    private DoubleParameter originZ = new DoubleParameter("Origin Z", 0, -50, 50);

    private DoubleParameter sizex = new DoubleParameter("Size X", 5, 0.05, 50);
    private DoubleParameter sizey = new DoubleParameter("Size Y", 5, 0.05, 50);

    private Dimension dimension = new Dimension(0, 0);
    
    public void setSize(double x, double y) {
        sizex.setValue(x);
        sizey.setValue(y);
    }
    
    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

//        float[] tableColor = {275f/255f, 190f/255f, 190f/255f, 1f};
        float[] tableColor = { 0.3f, 0.5f, 0.75f, 1 };
        float[] whiteColor = new float[] { 1f, 1f, 1f, 1 };

        gl.glDisable(GL.GL_CULL_FACE);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE, tableColor, 0);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_SPECULAR, whiteColor, 0);
        gl.glMaterialf(GL.GL_FRONT_AND_BACK, GL.GL_SHININESS, 20);

//        gl.glDisable(GL.GL_LIGHTING);
//        gl.glColor4f(tableColor[0], tableColor[1], tableColor[2], 1f);
        
        gl.glPushMatrix();
//        gl.glRotated(90, 0, 0, 1);
        double xsize = sizex.getValue() / 2;
        double ysize = sizey.getValue() / 2;
        double z = originZ.getValue();
        
        gl.glTranslated(originX.getValue(), originY.getValue(), z);
        gl.glBegin(GL.GL_QUADS);
        gl.glNormal3f(0, 1, 0);
        gl.glVertex3d(-xsize, -ysize, 0);
        gl.glVertex3d(-xsize, ysize, 0);
        gl.glVertex3d(xsize, ysize, 0);
        gl.glVertex3d(xsize, -ysize, 0);
        gl.glEnd();
        
        gl.glPopMatrix();
        
    }

    @Override
    public JPanel getControls() {
        /*
         * First create the panels.
         */
        ArrayList<JPanel> panels = new ArrayList<JPanel>();
//        panels.add(originX.getSliderControls(false));
//        panels.add(originY.getSliderControls(false));
//        panels.add(originZ.getSliderControls(false));
        panels.add(sizex.getSliderControls(false));
        panels.add(sizey.getSliderControls(false));
        
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Plane"));
        for (JPanel p : panels) vfp.add(p);

        return vfp.getPanel();
    }

    @Override
    public String getName() {
        return "Plane";
    }

    /**
     * @return the extrema of this plane. In order: (-x,-y), (x,-y), (-x,y), (x,y)
     */
    public List<Point3d> getExtrema() {
        List<Point3d> extrema = new LinkedList<Point3d>();
        
        double width = sizex.getValue();
        double height = sizey.getValue();
        
        extrema.add(new Point3d(-width / 2, -height / 2, 0));
        extrema.add(new Point3d(width / 2, -height / 2, 0));
        extrema.add(new Point3d(-width / 2, height / 2, 0));
        extrema.add(new Point3d(width / 2, height / 2, 0));
        return extrema;
    }
    
    @Override
    public void addParameterListener(ParameterListener l) {
        sizex.addParameterListener(l);
        sizey.addParameterListener(l);
    }
 
    public List<Point3d> getSampling(double sampling, List<Point3d> pts, List<Matrix4d> ms) {
        double width = sizex.getValue();
        double height = sizey.getValue();
        
        // For now just find the bounding box and add sample points
        List<Point3d> follicles = new LinkedList<Point3d>();
        follicles.add(new Point3d(-width / 2, 0, -height / 2));
        follicles.add(new Point3d(width / 2, 0, -height / 2));
        follicles.add(new Point3d(-width / 2, 0, height / 2));
        follicles.add(new Point3d(width / 2, 0, height / 2));
        
        List<Point2d> samples = BoundingBox.getGridSampling((int) sampling, follicles);

        // 2D to 3D
        for (Point2d p : samples) {
            Point3d p3 = new Point3d(p.x, 0, p.y);
            pts.add(p3);
            Matrix4d  m = new Matrix4d();
            m.setIdentity();
//            m.setRotation(new AxisAngle4d(0, 0, 1, Math.PI/2));
//            m.setRotation(new AxisAngle4d(0, 0, 1, Math.PI/2));
            m.setTranslation(new Vector3d(p3));
            ms.add(m);
        }

        return pts;
    }

}
