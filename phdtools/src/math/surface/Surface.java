package math.surface;

import java.util.List;

import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import swing.parameters.ParameterListener;


public interface Surface {
        
    public JPanel getControls();
    
    public void display(GLAutoDrawable drawable);

    public String getName();
    
    public void addParameterListener(ParameterListener l);

}
