package math.surface;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import swing.parameters.ParameterListener;

import com.sun.opengl.util.GLUT;

public class HemiSurface implements Surface {

    /**
     * The origin of this plane (X).
     */
    private DoubleParameter originX = new DoubleParameter("Origin X", 0, -10, 10);

    /**
     * The origin of this plane (Y).
     */
    private DoubleParameter originY = new DoubleParameter("Origin Y", 0, -10, 10);

    /**
     * The origin of this plane (Z).
     */
    private DoubleParameter originZ = new DoubleParameter("Origin Z", 0, -10, 10);

    private DoubleParameter radius = new DoubleParameter("radius", 5, 1, 10);
    
    private Dimension dimension = new Dimension(0, 0);
    
    private GLUT glut = new GLUT();
    
    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        float[] tableColor = {275f/255f, 190f/255f, 190f/255f, 1f};
        float[] whiteColor = new float[] { 1f, 1f, 1f, 1 };

        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE, tableColor, 0);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_SPECULAR, whiteColor, 0);
        gl.glMaterialf(GL.GL_FRONT_AND_BACK, GL.GL_SHININESS, 20);

        gl.glEnable(GL.GL_LIGHTING);
        gl.glColor4f(tableColor[0], tableColor[1], tableColor[2], 1f);
        
        gl.glPushMatrix();
        gl.glTranslated(originX.getValue(), originY.getValue(), originZ.getValue());
        gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, tableColor, 0);

        glut.glutSolidSphere(radius.getValue(), 20, 20);

        gl.glPopMatrix();
        
        gl.glDisable(GL.GL_LIGHTING);
        float[] planeColor = { 0.04f, 0.3f, 0.6f};
        gl.glColor4f(planeColor[0], planeColor[1], planeColor[2], 0.8f);
        gl.glPushMatrix();
        double s = 5*radius.getValue();
        double xsize = s;
        double ysize = originY.getValue()/2;
        double zsize = s;
        
        gl.glTranslated(originX.getValue() - xsize/2, 0, originZ.getValue() - zsize/2);
        gl.glBegin(GL.GL_QUADS);
        gl.glNormal3f(0, 1, 0);
        gl.glVertex3d(xsize, ysize, 0);
        gl.glVertex3d(xsize, ysize, zsize);
        gl.glVertex3d(0, ysize, zsize);
        gl.glVertex3d(0, ysize, 0);
        gl.glEnd();
        
        gl.glPopMatrix();

    }

    @Override
    public JPanel getControls() {
        /*
         * First create the panels.
         */
        ArrayList<JPanel> panels = new ArrayList<JPanel>();
        panels.add(originX.getSliderControls(false));
        panels.add(originY.getSliderControls(false));
        panels.add(originZ.getSliderControls(false));
        panels.add(radius.getSliderControls(false));
        
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Planar"));
        for (JPanel p : panels) vfp.add(p);

        return vfp.getPanel();
    }


    @Override
    public String getName() {
        return "Plane";
    }
    @Override
    public void addParameterListener(ParameterListener l) {
        radius.addParameterListener(l);
        originX.addParameterListener(l);
        originY.addParameterListener(l);
        originZ.addParameterListener(l);
    }

    public List<Point3d> getSampling(double sampling, List<Point3d> pts, List<Matrix4d> ms) {
        pts.clear();
        ms.clear();
        
        
        sampling += 0.5;
        double maxtheta = Math.PI / 7;
        double dtheta = sampling / maxtheta;
        double dphi = sampling / 2*Math.PI;
        
        double r = radius.getValue();
        Point3d o = new Point3d(originX.getValue(), originY.getValue(), originZ.getValue());
        Vector3d T = new Vector3d();
        Vector3d B = new Vector3d();
        Vector3d N = new Vector3d();
        
        for (double theta = maxtheta; theta < Math.PI/2; theta += dtheta) {
            for (double phi = 0; phi < 2*Math.PI; phi += dphi) {
                Point3d p = new Point3d();
                
                p.x += r * Math.cos(theta) * Math.cos(phi);
                p.z += r * Math.cos(theta) * Math.sin(phi);
                p.y += r * Math.sin(theta);
                N.set(p);
                p.add(o);
                pts.add(p);
                
                N.normalize();
                B.set(0, 0, 1);
                T.cross(N, B);
                B.cross(T, N);
                T.normalize();
                B.normalize();
                
                // N = x, B = z, T = y
                Matrix4d frame = new Matrix4d();
                frame.setIdentity();
                frame.setColumn(0, T.x, T.y, T.z, 0);
                frame.setColumn(1, N.x, N.y, N.z, 0);
                frame.setColumn(2, B.x, B.y, B.z, 0);
                frame.setColumn(3, p.x, p.y, p.z, 1);
                ms.add(frame);
            }
        }
        
        return pts;
    }
}
