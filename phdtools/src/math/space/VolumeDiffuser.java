package math.space;

import gl.renderer.NodeScene;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.vecmath.Point3i;

import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import tools.frame.VolumeSampler;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox;

public class VolumeDiffuser {

	/**
	 * Diffuse information from a data volumes within holes.
	 * @param data
	 * @param dataMask
	 * @param holes
	 */
	public static IntensityVolume diffuse(IntensityVolume data, IntensityVolumeMask dataMask, IntensityVolumeMask holes) {
		return diffuse(Arrays.asList(data), dataMask, holes).get(0);
	}
	
	/**
	 * Diffuse information from a list of data volumes within holes.
	 * @param data
	 * @param dataMask
	 * @param holes
	 */
	public static List<IntensityVolume> diffuse(final List<IntensityVolume> data, final IntensityVolumeMask dataMask, final IntensityVolumeMask holes) {
		
		final int volumeCount = data.size();
		
		final List<IntensityVolume> filled = new ArrayList<IntensityVolume>(volumeCount);
		for (int i = 0; i < volumeCount; i++)
			filled.add(new IntensityVolume(holes.getDimension()));

		// Diffuse within a 1-ball 
		final List<Point3i> neighbors = VolumeSampler.sample(NeighborhoodShape.Isotropic, 3);

		/*
		 * Initial hole mask
		 */
		final IntensityVolumeMask holesUpdated = new IntensityVolumeMask(holes);
		final IntensityVolumeMask dataUpdated = new IntensityVolumeMask(dataMask);

		VoxelBox boxHoles = new VoxelBox(holesUpdated);
		final VoxelBox boxData = new VoxelBox(dataUpdated);

		/*
		 *  Iterate until there are no more holes or we have reached the maximal iteration count
		 */
		// Since we diffuse 1 voxel every iteration, we should in principle never have to iterate
		// more than L times, where L is the largest dimension of the volume
		int maxIterations = MathToolset.roundInt(dataMask.getDimensionLength());
		int its = 0;
		
		// Iteratively diffuse
		while (boxHoles.countVoxels() > 0 && its < maxIterations) {
			boxHoles.voxelProcess(new PerVoxelMethodUnchecked() {
				
				@Override
				public void process(int x, int y, int z) {
					// This is the whole we are filling
					Point3i hole = new Point3i(x, y, z);
					
					final double[] accumulator = new double[volumeCount];
					final int[] count = new int[volumeCount];
					
					// Aggregate information from available data surrounding this hole
					boxData.neighborhoodProcess(hole, neighbors, new PerVoxelMethodUnchecked() {
						
						@Override
						public void process(int x, int y, int z) {
							for (int volumeIndex = 0; volumeIndex < volumeCount; volumeIndex++)
								accumulator[volumeIndex] += data.get(volumeIndex).get(x, y, z);
							
							count[0]++;
						}
					});
					
					if (count[0] > 0) {
						// Fill hole
						for (int volumeIndex = 0; volumeIndex < volumeCount; volumeIndex++)
							filled.get(volumeIndex).set(hole, accumulator[volumeIndex] / count[0]);
						
						// Update masks (interchange data and hole)
						holesUpdated.set(hole, 0);
						dataUpdated.set(hole, 1);
					}
				}
			});
			
			// Update the hole voxel box
			boxHoles = new VoxelBox(holesUpdated);
		}
		
		return filled;
	}
	
	public static void main(String[] args) {
	}
}
