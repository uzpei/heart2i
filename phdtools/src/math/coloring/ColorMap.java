package math.coloring;

import gl.material.Colour;
import gl.renderer.Screen;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import tools.geom.MathToolset;

public class ColorMap {
	public enum Map {
		CUSTOM, GREY, JET, HOT, COOL, COOLWARM, SPECTRA, MSH_BLRED, MSH_PURPORG, MSH_GRPURP, MSH_BLYEL, MSH_GRRED 
	};

	public static final int DEFAULT_NUMSAMPLES = 511;
	private int numSamples;
	private int numColorFrames;
	private Map map;
	
	private double[][] colorValues;
	public static final Map DEFAULT_MAP = Map.MSH_BLRED;
	
	public ColorMap() {
		this(DEFAULT_MAP);
	}
	
	public ColorMap(Map map) {
		this(map, DEFAULT_NUMSAMPLES);
	}

	public ColorMap(Map map, int numSamples) {
		constructMap(map, numSamples);
	}
	
	public static ColorMap randomize(int numpColors) {
		List<Color> colorList = new ArrayList<Color>();
		Random rand = new Random();
		for (int i = 0; i < numpColors; i++) {
			if (i == numpColors / 2)
				colorList.add(Color.white);
			else
				colorList.add(new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));
		}
		
		return new ColorMap(colorList, DEFAULT_NUMSAMPLES);
	}
	
	/**
	 * Interpolate between a list of colors
	 * 
	 * @param colorFrames
	 */
	public ColorMap(List<Color> colorFrames, int numSamples) {
		construct(colorFrames, numSamples);
		map = Map.CUSTOM;
	}
	
	private void construct(List<Color> colorFrames, int numSamples) {
		numColorFrames = colorFrames.size();
		
		int numIntervals = numColorFrames - 1;
		int intervalLength = (int) Math.ceil(numSamples / (double) numIntervals); 
		
		int sample = 0;
		colorValues = new double[numSamples][3];
		for (int colorIndex = 1; colorIndex < numColorFrames; colorIndex++) {
			Color left = colorFrames.get(colorIndex - 1);
			Color right = colorFrames.get(colorIndex);

			// Create interval in range [left, right]
			double[][] interval = new double[3][];
			interval[0] = MathToolset.linearSpace(left.getRed() / 255d, right.getRed() / 255d, intervalLength);
			interval[1] = MathToolset.linearSpace(left.getGreen() / 255d, right.getGreen() / 255d, intervalLength);
			interval[2] = MathToolset.linearSpace(left.getBlue() / 255d, right.getBlue() / 255d, intervalLength);
			
			// If we are not dealing with the last interval, exclude right boundary
			// i.e. obtain the range [left, right[
			if (colorIndex < numColorFrames - 1)
				for (int rgbIndex = 0; rgbIndex < 3; rgbIndex++)
					interval[rgbIndex] = Arrays.copyOf(interval[rgbIndex], intervalLength - 1);

			// Interpolate values within intervals
			for (int intervalSample = 0; intervalSample < interval[0].length; intervalSample++)
				colorValues[sample++] = new double[] { interval[0][intervalSample], interval[1][intervalSample], interval[2][intervalSample] };
			
//			// Intervals are from left boundary to the second last to the right boundary
//			// For the last interval we also use the right boundary.
//			for (int intervalSample = 0; intervalSample < (colorIndex < numColors - 1 ? intervalLength : intervalLength + 1); intervalSample++) {
//				double s = intervalSample / (double) intervalLength;
//				
//				colors[sample++] = new double[] { };
//			}
		}
		
		// Only use colors that were defined
		colorValues = Arrays.copyOf(colorValues, sample);
		this.numSamples = colorValues.length;
	}
	
	public int getNumSamples() {
		return numSamples;
	}
	
	public int getNumColorFrames() {
		return numColorFrames;
	}
	
	public Map getMap() {
		return map;
	}
	
	public double[][] getColorArray() {
		return colorValues;
	}
	
	// The following performs a "gamma correction" specified by the sRGB color
	// space. sRGB is defined by a canonical definition of a display monitor and
	// has been standardized by the International Electrotechnical Commission
	// (IEC
	// 61966-2-1). The nonlinearity of the correction is designed to make the
	// colors more perceptually uniform. This color space has been adopted by
	// several applications including Adobe Photoshop and Microsoft Windows
	// color
	// management. OpenGL is agnostic on its RGB color space, but it is
	// reasonable
	// to assume it is close to this one.
	public static double[] sRGBtoXYZ(double[] RGB) {
		double R = RGB[0];
		double G = RGB[1];
		double B = RGB[2];

		if (R > 0.04045)
			R = Math.pow((R + 0.055) / 1.055, 2.4);
		else
			R = R / 12.92;

		if (G > 0.04045)
			G = Math.pow((G + 0.055) / 1.055, 2.4);
		else
			G = G / 12.92;

		if (B > 0.04045)
			B = Math.pow((B + 0.055) / 1.055, 2.4);
		else
			B = B / 12.92;

		// Observer. = 2 deg, Illuminant = D65
		double X = R * 0.4124 + G * 0.3576 + B * 0.1805;
		double Y = R * 0.2126 + G * 0.7152 + B * 0.0722;
		double Z = R * 0.0193 + G * 0.1192 + B * 0.9505;

		return new double[] { X, Y, Z };
	}

	public static double[] XYZtosRGB(double[] XYZ) {
		double X = XYZ[0];
		double Y = XYZ[1];
		double Z = XYZ[2];

		// double ref_X = 0.9505; //Observer = 2 deg Illuminant = D65
		// double ref_Y = 1.000;
		// double ref_Z = 1.089;

		double R = X * 3.2406 + Y * -1.5372 + Z * -0.4986;
		double G = X * -0.9689 + Y * 1.8758 + Z * 0.0415;
		double B = X * 0.0557 + Y * -0.2040 + Z * 1.0570;

		// The following performs a "gamma correction" specified by the sRGB
		// color
		// space. sRGB is defined by a canonical definition of a display monitor
		// and
		// has been standardized by the International Electrotechnical
		// Commission (IEC
		// 61966-2-1). The nonlinearity of the correction is designed to make
		// the
		// colors more perceptually uniform. This color space has been adopted
		// by
		// several applications including Adobe Photoshop and Microsoft Windows
		// color
		// management. OpenGL is agnostic on its RGB color space, but it is
		// reasonable
		// to assume it is close to this one.
		if (R > 0.0031308)
			R = 1.055 * (Math.pow(R, (1 / 2.4))) - 0.055;
		else
			R = 12.92 * (R);
		if (G > 0.0031308)
			G = 1.055 * (Math.pow(G, (1 / 2.4))) - 0.055;
		else
			G = 12.92 * (G);
		if (B > 0.0031308)
			B = 1.055 * (Math.pow(B, (1 / 2.4))) - 0.055;
		else
			B = 12.92 * (B);

		// Clip colors. ideally we would do something that is perceptually
		// closest
		// (since we can see colors outside of the display gamut), but this
		// seems to
		// work well enough.
		double maxVal = R;
		if (maxVal < G)
			maxVal = G;
		if (maxVal < B)
			maxVal = B;
		if (maxVal > 1.0) {
			R /= maxVal;
			G /= maxVal;
			B /= maxVal;
		}
		if (R < 0)
			R = 0;
		if (G < 0)
			G = 0;
		if (B < 0)
			B = 0;

		return new double[] { R, G, B };
	}

	public static double[] XYZtoLab(double[] XYZ) {
		double X = XYZ[0];
		double Y = XYZ[1];
		double Z = XYZ[2];
		
		double ref_X = 0.9505;
		  double ref_Y = 1.000;
		  double ref_Z = 1.089;
		  double var_X = X / ref_X;  //ref_X = 0.9505  Observer= 2 deg, Illuminant= D65
		  double var_Y = Y / ref_Y;  //ref_Y = 1.000
		  double var_Z = Z / ref_Z;  //ref_Z = 1.089
		 
		  if ( var_X > 0.008856 ) var_X = Math.pow(var_X, 1.0/3.0);
		  else                    var_X = ( 7.787 * var_X ) + ( 16.0 / 116.0 );
		  if ( var_Y > 0.008856 ) var_Y = Math.pow(var_Y, 1.0/3.0);
		  else                    var_Y = ( 7.787 * var_Y ) + ( 16.0 / 116.0 );
		  if ( var_Z > 0.008856 ) var_Z = Math.pow(var_Z, 1.0/3.0);
		  else                    var_Z = ( 7.787 * var_Z ) + ( 16.0 / 116.0 );
		 
		  double L = ( 116 * var_Y ) - 16;
		  double a = 500 * ( var_X - var_Y );
		  double b = 200 * ( var_Y - var_Z );	
	
		  return new double[] { L, a, b };
	}

	public static double[] LabtoXYZ(double[] Lab) {
		double L = Lab[0];
		double a = Lab[1];
		double b = Lab[2];

		// LAB to XYZ
		double var_Y = (L + 16) / 116;
		double var_X = a / 500 + var_Y;
		double var_Z = var_Y - b / 200;

		if (Math.pow(var_Y, 3) > 0.008856)
			var_Y = Math.pow(var_Y, 3);
		else
			var_Y = (var_Y - 16.0 / 116.0) / 7.787;

		if (Math.pow(var_X, 3) > 0.008856)
			var_X = Math.pow(var_X, 3);
		else
			var_X = (var_X - 16.0 / 116.0) / 7.787;

		if (Math.pow(var_Z, 3) > 0.008856)
			var_Z = Math.pow(var_Z, 3);
		else
			var_Z = (var_Z - 16.0 / 116.0) / 7.787;
		double ref_X = 0.9505;
		double ref_Y = 1.000;
		double ref_Z = 1.089;
		double x = ref_X * var_X; // ref_X = 0.9505 Observer= 2 deg Illuminant=
									// D65
		double y = ref_Y * var_Y; // ref_Y = 1.000
		double z = ref_Z * var_Z; // ref_Z = 1.089
		return new double[] { x, y, z };
	}
	
	public static double[] LabtoMsh(double[] Lab) {
		double L = Lab[0];
		double a = Lab[1];
		double b = Lab[2];

		double M = Math.sqrt(L * L + a * a + b * b);
		double s = (M > 0.001) ? Math.acos(L / M) : 0.0;
		double h = (s > 0.001) ? Math.atan2(b, a) : 0.0;

		return new double[] { M, s, h };
	}

	public static double[] MshToLab(double[] Msh) {
		double M = Msh[0];
		double s = Msh[1];
		double h = Msh[2];

		double L = M * Math.cos(s);
		double a = M * Math.sin(s) * Math.cos(h);
		double b = M * Math.sin(s) * Math.sin(h);

		return new double[] { L, a, b };
	}
	
	public static double[] YIQtoRGB(double[] YIQ) {
		double Y = YIQ[0];
		double I = YIQ[1];
		double Q = YIQ[2];
		
		double R = 1.0 * Y + 0.956 * I + 0.621 * Q;
		double G = 1.0 * Y - 0.272 * I - 0.647 * Q;
		double B = 1.0 * Y - 1.105 * I + 1.702 * Q;
		
		return new double[] { R, G, B };
	}
	
	public static double[] RGBtoYIQ(double[] RGB) {
		double R = RGB[0];
		double G = RGB[1];
		double B = RGB[2];
		
		double Y = 0.299 * R + 0.587 * G + 0.114 * B;
		double I = 0.596 * R - 0.275 * G - 0.321 * B;
		double Q = 0.212 * R - 0.523 * G + 0.311 * B;
		
		return new double[] { Y, I, Q };
	}


	public static double[] sRGBtoLab(double[] RGB) {
		return XYZtoLab(sRGBtoXYZ(RGB));
	}

	public static double[] LabtosRGB(double[] Lab) {
		return XYZtosRGB((LabtoXYZ(Lab)));
	}

	/**
	 * For the case when interpolating from a saturated color to an unsaturated
	 * color, find a hue for the unsaturated color that makes sense.
	 * 
	 * @param msh
	 * @param unsatM
	 * @return
	 */
	public static double adjustHue(double[] msh, double unsatM) {
		if (msh[0] >= unsatM - 0.1) {
			// The best we can do is hold hue constant.
			return msh[2];
		} else {
			// This equation is designed to make the perceptual change of the
			// interpolation to be close to constant.
			double hueSpin = (msh[1]
					* Math.sqrt(unsatM * unsatM - msh[0] * msh[0]) / (msh[0] * Math
					.sin(msh[1])));
			// Spin hue away from 0 except in purple hues.
			if (msh[2] > -0.3 * Math.PI) {
				return msh[2] + hueSpin;
			} else {
				return msh[2] - hueSpin;
			}
		}
	}

	/**
	 * Interpolate a diverging color map. Based on
	 * "Diverging Color Maps for Scientiﬁc Visualization" by Kenneth Moreland
	 * 
	 * @param s
	 * @param rgb1
	 * @param rgb2
	 * @param result
	 * @return
	 */
	public static double[] interpolateDiverging(double s, double[] rgb1,
			double[] rgb2) {
		double[] lab1 = sRGBtoLab(rgb1);
		double[] lab2 = sRGBtoLab(rgb2);

		double[] msh1 = LabtoMsh(lab1);
		double[] msh2 = LabtoMsh(lab2);

		// If the endpoints are distinct saturated colors, then place white in
		// between
		// them.
		if ((msh1[1] > 0.05) && (msh2[1] > 0.05)
				&& (angleDiff(msh1[2], msh2[2]) > 0.33 * Math.PI)) {
			// Insert the white midpoint by setting one end to white and
			// adjusting the
			// scalar value.
			if (s < 0.5) {
				msh2[0] = 95.0;
				msh2[1] = 0.0;
				msh2[2] = 0.0;
				s = 2.0 * s;
			} else {
				msh1[0] = 95.0;
				msh1[1] = 0.0;
				msh1[2] = 0.0;
				s = 2.0 * s - 1.0;
			}
		}

		// If one color has no saturation, then its hue value is invalid. In
		// this
		// case, we want to set it to something logical so that the
		// interpolation of
		// hue makes sense.
		if ((msh1[1] < 0.05) && (msh2[1] > 0.05)) {
			msh1[2] = adjustHue(msh2, msh1[0]);
		} else if ((msh2[1] < 0.05) && (msh1[1] > 0.05)) {
			msh2[2] = adjustHue(msh1, msh2[0]);
		}

		double[] mshTmp = new double[3];
		mshTmp[0] = (1 - s) * msh1[0] + s * msh2[0];
		mshTmp[1] = (1 - s) * msh1[1] + s * msh2[1];
		mshTmp[2] = (1 - s) * msh1[2] + s * msh2[2];

		// Now convert back to RGB
		double[] labTmp = MshToLab(mshTmp);

		return LabtosRGB(labTmp);
	}

	/**
	 * Given two angular orientations, returns the smallest angle between the
	 * two.
	 * 
	 * @param a1
	 * @param a2
	 * @return
	 */
	public static double angleDiff(double a1, double a2) {
		double adiff = a1 - a2;
		if (adiff < 0.0)
			adiff = -adiff;
		while (adiff >= 2 * Math.PI) {
			adiff -= 2 * Math.PI;
		}
		if (adiff > Math.PI)
			adiff = 2 * Math.PI - adiff;

		return adiff;
	}
	
	private void constructMap(Map map, int numSamples) {
		switch(map) {
		case GREY:
			construct(Arrays.asList(Color.black, Color.white), numSamples);
			break;
		case JET:
			construct(Arrays.asList(Color.blue, Color.cyan, Color.yellow, Color.red), numSamples);
			break;
		case HOT:
			construct(Arrays.asList(Color.black, Color.red, Color.yellow, Color.white), numSamples);
			break;
		case COOL:
			construct(Arrays.asList(Color.black, Color.blue, Color.cyan, Color.white), numSamples);
			break;
		case SPECTRA:
			construct(Arrays.asList(Colour.asColor(Colour.darkBlue), Color.cyan, Color.white, Color.yellow, Color.red), numSamples);
			break;
		case COOLWARM:
			construct(Arrays.asList(Color.blue, Color.white, Color.red), numSamples);
			break;
		case MSH_BLRED:
			colorValues = divergingMap(numSamples, new double[] { 0.230, 0.299, 0.754 }, new double[] { 0.706, 0.016, 0.150 });
			numColorFrames = 3;
			break;
		case MSH_PURPORG:
			colorValues = divergingMap(numSamples, new double[] { 0.436, 0.308, 0.631 }, new double[] { 0.759, 0.334, 0.046 });
			numColorFrames = 3;
			break;
		case MSH_GRPURP:
			colorValues = divergingMap(numSamples, new double[] { 0.085, 0.532, 0.201 }, new double[] { 0.436, 0.308, 0.631 });
			numColorFrames = 3;
			break;
		case MSH_BLYEL:
			colorValues = divergingMap(numSamples, new double[] { 0.217, 0.525, 0.910 }, new double[] { 0.677, 0.492, 0.093 });
			numColorFrames = 3;
			break;
		case MSH_GRRED:
			colorValues = divergingMap(numSamples, new double[] { 0.085, 0.532, 0.201 }, new double[] { 0.758, 0.214, 0.233 });
			numColorFrames = 3;
			break;
		case CUSTOM:
			// Do nothing, values should have already been set in constructor
			if (colorValues == null)
				throw new RuntimeException("Attempted to construct a color map with customized values but values were not set.");
		}
		
		// Adjust number of samples to make use of all colors
		this.numSamples = colorValues.length;
		this.map = map;
	}

	private static double[][] divergingMap(int numSamples, double[] left, double[] right) {
		// Color M s h
		double[][] map = new double[numSamples][3];
		for (int i = 1; i <= numSamples; i++) {
			map[i-1] = interpolateDiverging(i / (double) numSamples, left, right);
		}
		
		return map;
	}
	
	@Override
	public String toString() {
		String s = "";
		double[][] colors = getColorArray();
		for (int i = 0; i < colors.length; i++) {
			double[] color = colors[i];
			s += "[" + i + "] " + Arrays.toString(color) + "\n";
		}
		
		return s;
	}
	
	/**
	 * @param value
	 * @param min
	 * @param max
	 * @return the color corresponding to a scalar within an inclusive range [min, max]
	 */
	public Color colorize(double value, double min, double max) {
		// Clamp value to [min, max]
		MathToolset.clamp(value, min, max);
		
		// Map value to [0, 1]
		double parameter = MathToolset.clamp(Math.abs(value - min) / (max - min), 0, 1);
		
		// Compute corresponding color index
		int colorIndex = MathToolset.roundInt(parameter * (numSamples - 1));
		
		// Return corresponding color
		return new Color((float) colorValues[colorIndex][0], (float) colorValues[colorIndex][1], (float) colorValues[colorIndex][2]);
	}
	
	/**
	 * @param color
	 * @return black or white depending on which yields the largest brightness contrast
	 */
	public static Color contrastWhiteOrBlack(Color color) {
		float[] HSB = Color.RGBtoHSB(color.getRed(), color.getGreen(),color.getBlue(), null);
		return new Color(Color.HSBtoRGB(0f, 0f, HSB[2] > 0.5 ? 0 : 1));
	}

	/**
	 * @param color
	 * @return the color which yields the largest brightness and saturation contrast
	 */
	public static Color contrast(Color color) {
		float[] HSB = Color.RGBtoHSB(color.getRed(), color.getGreen(),color.getBlue(), null);
//		return new Color(Color.HSBtoRGB(HSB[0] + 0.5f, 1 - HSB[1], 1 - HSB[2]));

//		System.out.println("HSB (in) = " + HSB[0] + "," + HSB[1] + "," + HSB[2]);

		float H = HSB[0] + 0.5f;
		float S = HSB[1];
		float B = S > 0 ? HSB[2] : (HSB[2] > 0.5 ? 0 : 1);
		
		// map to [0, 1]
		H = H > 1 ? (float) (H - Math.floor(H)) : H;
		S = S > 1 ? (float) (S - Math.floor(S)) : S;
		B = B > 1 ? (float) (B - Math.floor(B)) : B;
		
//		System.out.println("HSB (out) = " + H + "," + S + "," + B);
		return new Color(Color.HSBtoRGB(H, S, B));
	}
	
	public static void main(String args[]) {
		int testCase = 3;
		
		ColorMap map = null;
		if (testCase == 0) {
			// Generate numSamples / 10 random colors
			List<Color> colorList = new ArrayList<Color>();
			Random rand = new Random();
			int n = 9;
			for (int i = 0; i < n; i++) {
				if (i == n / 2)
					colorList.add(Color.white);
				else
					colorList.add(new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));
			}
			
			map = new ColorMap(colorList, DEFAULT_NUMSAMPLES);
		}
		else if (testCase == 1) {
//			colorList = Arrays.asList(Color.blue, Color.cyan, Color.yellow, Color.red);
//			colorList = Arrays.asList(Color.blue, Color.white, Color.red);
			map = new ColorMap(Arrays.asList(Color.black, Color.red, Color.yellow, Color.white), DEFAULT_NUMSAMPLES);
		}
		else if (testCase == 2) {
			map = new ColorMap(Map.HOT);
		}
		else if (testCase == 3) {
			map = new ColorMap(Map.COOLWARM);
		}
		else if (testCase == 4) {
			map = new ColorMap(Map.MSH_BLRED);
		}
		else if (testCase == 5) {
			map = new ColorMap(Map.MSH_BLYEL);
		}

		// Return first, middle, and last color
		System.out.println(map.colorize(0, 0, 1));
		System.out.println(map.colorize(0.5, 0, 1));
		System.out.println(map.colorize(1, 0, 1));
		
		JFrame frame = new JFrame();
		Dimension dimFrame = new Dimension(Screen.getMaxSupportedSize().width, Screen.getMaxSupportedSize().height / 2);
		frame.setPreferredSize(dimFrame);
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		for (Map cmap : ColorMap.Map.values()) {
			if (cmap != ColorMap.Map.CUSTOM) {
				JPanel colorPanel = new JPanel(new GridBagLayout());
				JColorGradient gradient = new JColorGradient(new ColorMap(cmap).getColorArray());
//				Dimension dimGradient = new Dimension(dimFrame.width, dimFrame.height / (ColorMap.Map.values().length - 1));
//				gradient.setPreferredSize(dimGradient);
//				gradient.setMinimumSize(dimGradient);
				GridBagConstraints gbc;
				
				
				gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.weightx = 0;
				gbc.anchor = GridBagConstraints.EAST;
				gbc.fill = GridBagConstraints.VERTICAL;
				colorPanel.add(new JLabel(cmap.toString(), JLabel.RIGHT), gbc);
				
				gbc = new GridBagConstraints();
				gbc.gridx = 1;
				gbc.weightx = 1;
				gbc.anchor = GridBagConstraints.WEST;
				gbc.fill = GridBagConstraints.BOTH;
				colorPanel.add(gradient, gbc);
				vfp.add(colorPanel);
			}
		}
		frame.add(vfp.getPanel());
//		frame.add(new JColorGradient(map.getColorArray()));
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
