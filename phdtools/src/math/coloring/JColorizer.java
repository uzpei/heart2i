package math.coloring;

import gl.renderer.Screen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;

import math.coloring.ColorMap.Map;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.geom.MathToolset;

public class JColorizer extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5795848402222369661L;

	/*
	 *  Set constants
	 */
	private final static int labelValuePadX = 3;
	private final static int valueTickWidth = 4; 
	private final static double valueRatio = 0.5;
	private final static double valueLblRatio = 0.5;
	private final static Dimension pickerDimensions = new Dimension(100, 25);
	private final static Insets mapPickerInsets = new Insets(0, 0, 0, 0);
//	private final static Point padLeftGradient = new Point(8, 18);
	private final static Point padLeftGradient = new Point(valueTickWidth, 1);
//	private final static Point padRightGradient = new Point(padLeftGradient.x, padLeftGradient.y - 10);
	private final static Point padRightGradient = new Point(1 + pickerDimensions.width, padLeftGradient.y);
	private final static Dimension preferredSize = new Dimension(250, 25);
	private final static Font labelFont = new Font("Verdana", Font.PLAIN, 10);
	
	private List<ChangeListener> changeListeners = new LinkedList<ChangeListener>();

	private ColorMap colorMap;
	private JColorGradient colorGradient;
	private double min, max;
	private EnumComboBox<ColorMap.Map> mapChooser;
	
	public JColorizer() {
		this(new ColorMap(), 0, 1);
	}

	public JColorizer(ColorMap.Map map) {
		this(new ColorMap(map), 0, 1);
	}
	
	private void assembleControls() {
		removeAll();

		// Add color map combo box
		JPanel mapPickerControls = mapChooser.getControls();
		if (mapPickerControls.isVisible()) {
			GridBagConstraints gbc = new GridBagConstraints();
	        gbc.anchor = GridBagConstraints.EAST;
	        gbc.fill = GridBagConstraints.NONE;
	        gbc.gridx = 0;
	        gbc.gridy = 0;
	        gbc.weightx = 1;
	        gbc.weighty = 1;
	        gbc.insets = mapPickerInsets;
			mapChooser.setName("");
			mapChooser.setSelected(colorMap.getMap());

			add(mapPickerControls, gbc);
		}
		
	}
	
	public JColorizer(ColorMap map, double min, double max) {
		super.setPreferredSize(preferredSize);
		super.setMinimumSize(preferredSize);
		
		mapChooser = new EnumComboBox<ColorMap.Map>(map.getMap());
		
		this.colorMap = map;
		this.min = min;
		this.max = max;
		colorGradient = new JColorGradient(map);
		
		// Add map picker
		super.setLayout(new GridBagLayout());
		mapChooser.setName("");
		mapChooser.setSelected(colorMap.getMap());

		// Set default dimension
		JPanel mapPickerControls = mapChooser.getControls();
		mapPickerControls.setPreferredSize(pickerDimensions);
		mapPickerControls.setMaximumSize(pickerDimensions);

		// Add the control
		mapPickerControls.setVisible(true);
		assembleControls();
		
		final JColorizer me = this;
		mapChooser.addParameterListener(new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				// Refresh paint
				ColorMap.Map map = mapChooser.getSelected();

				if (map == ColorMap.Map.CUSTOM) {
					// Do nothing for now with custom maps
				}
				else {
					colorMap = new ColorMap(map, colorMap.getNumSamples());
					colorGradient = new JColorGradient(colorMap);
				}
				
				notifyListeners();
				
				me.repaint();
			}
		});
		
		// Create border
//		SwingTools.createBorder(this, "Color map");
		
		repaint();
	}
	
	public void addChangeListener(ChangeListener l) {
		changeListeners.add(l);
	}
	
	private void notifyListeners() {
		for (ChangeListener l : changeListeners) {
			l.stateChanged(null);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Dimension size = getSize();

		BufferedImage canvas = new BufferedImage(size.width, size.height,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = canvas.createGraphics();
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);

		/*
		 *  Set display precision
		 */
		// Max number of digits, including integer and fractional part
		// i.e. max = xxxx, min = .xxxx
		int maxDigits = 3;
		DecimalFormat nf = null;
		
		// Calculate number of integer digits
		double bounds = Math.abs(max - min);
		int countInteger = String.valueOf(MathToolset.roundInt(bounds)).length();
		
		// if this is a large or very small number, use scientific notation
		if (Math.abs(max - min) >= Math.pow(10, maxDigits) || bounds <= Math.pow(10, -maxDigits)) {
			nf = new DecimalFormat("0E0");
		}		
		// For anything in between, give priority to integer part, and then leave rest as decimals
		else {
			int countDecimals = maxDigits - countInteger;
			
			// Assemble notation
			String notation = "";
			for (int i = 0; i < countInteger; i++)
				notation += "#";
			notation += ".";
			for (int i = 0; i < countDecimals; i++)
				notation += "#";
					
			nf = new DecimalFormat(notation);
		}

		Dimension dimGradient;
		// Set gradient drawing region
		if (mapChooser.getControls().isVisible()) {
			dimGradient = new Dimension(size.width - padLeftGradient.x - padRightGradient.x, size.height - padLeftGradient.y - padRightGradient.y);			
		}
		else {
			dimGradient = new Dimension(size.width - padLeftGradient.x - padRightGradient.x + pickerDimensions.width - 2, size.height - padLeftGradient.y - padRightGradient.y);
		}
		
		
		// Paint over the color gradient
		BufferedImage imgGradient = colorGradient.getGradientImage(dimGradient);
//		g.drawImage(img, padLeftGradient.x, padLeftGradient.y, this);
		graphics.drawImage(imgGradient, padLeftGradient.x, padLeftGradient.y, this);

		// Draw rectangle around gradient
		graphics.setColor(Color.black);
		graphics.drawRect(padLeftGradient.x - 1, padLeftGradient.y - 1,dimGradient.width + 1, dimGradient.height + 1);
		
		// Draw min/mid/max
		graphics.setFont(labelFont);

		int numTicks = colorMap.getNumColorFrames();
		double delta = (max - min) / (numTicks - 1);
				
		for (int i = 0; i < numTicks; i++) {
			double xiv = MathToolset.roundInt(padLeftGradient.x + i * (dimGradient.width+1) / ((double) colorMap.getNumColorFrames() - 1));
			int xi = MathToolset.roundInt(xiv);
			int yi = padLeftGradient.y;
			
			// Pick a constrasting color
			Color color = colorMap.colorize(i / ((double) colorMap.getNumColorFrames() - 1), 0, 1);
			Color colorContrast = ColorMap.contrastWhiteOrBlack(color);
//			System.out.println(color + " => " + colorContrast);
			
//			g.setColor(color);
//			g.fillRect(xi - valueTickWidth / 2, yi, valueTickWidth, dimGradient.height);
			graphics.setColor(colorContrast);
//			g.fillRect(xi - (int) Math.floor(valueTickWidth / 2), yi, valueTickWidth, dimGradient.height);
			// Add extra pixel since the gradient is shifted by 1 within the boundary rectangle
			graphics.fillOval(-1 + MathToolset.roundInt(xiv - valueTickWidth / 2d), MathToolset.roundInt(-1 + yi + - (dimGradient.height + 1) /2d + valueRatio * (dimGradient.height +1) + dimGradient.height / 2d - (valueTickWidth / 2d)), valueTickWidth, valueTickWidth);
			
			double value =  min + i * delta;
			String s = nf.format(value);
			
			Dimension textSize = getLabelTextSize(s, labelFont);
			int yiValueLabel = MathToolset.roundInt(yi + dimGradient.height / 2d + textSize.height / 4d - valueLblRatio * (dimGradient.height +1) + valueLblRatio * (dimGradient.height +1));
			if (i < colorMap.getNumColorFrames() - 1)
				graphics.drawChars(s.toCharArray(), 0, s.length(), xi + labelValuePadX, yiValueLabel);
			else
				graphics.drawChars(s.toCharArray(), 0, s.length(), xi - labelValuePadX - textSize.width, yiValueLabel);
		}
		
		g.drawImage(canvas, 0, 0, this);
	}

	private Dimension getLabelTextSize(String text, Font font) {
		FontMetrics metrics = getFontMetrics(font);
		int adv = metrics.stringWidth(text);
		int hgt = metrics.getHeight();
		return new Dimension(adv, hgt);
	}
	
	public ColorMap getColorMap() {
		return colorMap;
	}
	
	public void setMinMax(double min, double max) {
		this.min = min;
		this.max = max;

//		System.out.println("min = " + min + ", max = " + max);
//		this.revalidate();
//		this.invalidate();
		this.repaint();
	}

	public Color colorize(double value) {
		return colorize(value, min, max);
	}
	public Color colorize(double value, double min, double max) {
		return colorMap.colorize(value, min, max);
	}

	public Map getMap() {
		return mapChooser.getSelected();
	}

	public void setMap(ColorMap.Map map) {
		mapChooser.setSelected(map);
	}

	public void setPickerVisible(boolean b) {
		mapChooser.getControls().setVisible(b);
		assembleControls();
		invalidate();
	}


	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setPreferredSize(new Dimension(500, Screen.getMaxSupportedSize().height/2));
		frame.setLayout(new BorderLayout());
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.createBorder("Color maps");
		
		for (Map cmap : ColorMap.Map.values()) {
			if (cmap != ColorMap.Map.CUSTOM) {
				final JColorizer colorizer = new JColorizer(new ColorMap(cmap), 0, 1);
				vfp.add(colorizer);
			}
		}
        
		frame.add(vfp.getPanel(), BorderLayout.CENTER);
		frame.setLocation((Screen.getMaxSupportedSize().width - frame.getPreferredSize().width) / 2, (Screen.getMaxSupportedSize().height - frame.getPreferredSize().height) / 2);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
