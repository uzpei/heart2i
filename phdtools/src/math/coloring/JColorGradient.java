package math.coloring;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

public class JColorGradient extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6252852643214177808L;

	protected BufferedImage colorScale;
	private double[][] rgbColors;
	
	private int updatedWidth = -1;
	private int updatedHeight = -1;

	public JColorGradient(ColorMap.Map map) {
		this(new ColorMap(map));
	}
	
	public JColorGradient(ColorMap map) {
		this(map.getColorArray());
	}
	
	public JColorGradient(double[][] rgbColors) {
		this.rgbColors = rgbColors;
		
	}
	
	public double[][] getColors() {
		return rgbColors;
	}
	
	public Color getPaintedColor(int i, int j) {
		return new Color(colorScale.getRGB(i, j));
	}
	
	public Color getColor(int index) {
		return new Color((float) rgbColors[index][0],(float) rgbColors[index][1],(float) rgbColors[index][2]);
	}
	
	public int getNumColors() {
		return rgbColors.length;
	}

	private void updateColorScale(Dimension size) {

		// Canvas is too small
		if (size.width < 1 || size.height < 1)
			return;
		
		// No need to update if dimensions haven't changed
		if (size.width == updatedWidth && size.height == updatedHeight)
			return;

//		System.out.println(size);

		updatedWidth = size.width;
		updatedHeight = size.height;

		BufferedImage img = new BufferedImage(size.width, size.height,
				BufferedImage.TYPE_INT_ARGB);

		Graphics2D graphs = img.createGraphics();
		graphs.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		graphs.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);

		int n = rgbColors.length;
		double dx = updatedWidth / (double) n;
		int dxi = (int) Math.ceil(dx);
		for (int colorIndex = 0; colorIndex < n; colorIndex++) {
			double[] colorArray = rgbColors[colorIndex];
			Color color = new Color((float) colorArray[0], (float) colorArray[1], (float) colorArray[2]);
			graphs.setPaint(color);
			graphs.fillRect((int) Math.ceil(dx * colorIndex), 0, dxi, updatedHeight);
		}

		// Set color scale
		colorScale = img;
	}

	public BufferedImage getGradientImage(Dimension size) {
		updateColorScale(size);
		return colorScale;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(getGradientImage(getSize()), 0, 0, this);
	}
}
