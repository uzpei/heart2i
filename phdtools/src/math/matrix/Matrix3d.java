package math.matrix;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelVectorField;

public class Matrix3d implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5245229845396265731L;

	private String name = "No Name";

	protected double[][][] elements;
	
	private int[] dimension;

	protected final double EPSILON = 1e-8;

	public int[] getDimension() {
		return dimension;
	}
	public Matrix3d(int[] dim) {
		elements = new double[dim[0]][dim[1]][dim[2]];
		
		dimension = dim;
	}

	public Matrix3d() {
		// Do nothing... will need to set dims manually
	}
	
	public Matrix3d(double[][][] elements) {
		setByReference(elements);
	}
	
	public Matrix3d(IntensityVolume intensityVolume) {
		this(intensityVolume.getData());
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public double get(int i, int j, int k) {
		return elements[i][j][k];
	}

	public double get(Point3d p) {
		return elements[MathToolset.roundInt(p.x)][MathToolset.roundInt(p.y)][MathToolset.roundInt(p.z)];
	}

	public double get(Point3i p) {
		return elements[p.x][p.y][p.z];
	}

	public double get(int[] ijk) {
		return elements[ijk[0]][ijk[1]][ijk[2]];
	}

	public void set(int i, int j, int k, double v) {
		elements[i][j][k] = v;
	}
	
	public void set(Point3i pt, double v) {
		elements[pt.x][pt.y][pt.z] = v;
	}

	/**
	 * TODO: change the name of this method
	 * @param elements
	 */
	public void setByReference(double[][][] elements) {
		this.elements = elements;
		dimension = new int[] { elements.length, elements[0].length, elements[0][0].length };
	}
	
	public Matrix3d toBinary() {
//		normalize();
		
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					if (Math.abs(elements[i][j][k]) <= EPSILON)
						elements[i][j][k] = 0;
					else
						elements[i][j][k] = 1;
				}
			}
		}
		
		return this;
	}

	/**
	 * Negative all volume intensities: v = - v;
	 */
	public void negate() {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = -elements[i][j][k];
				}
			}
		}

	}

	public static void fillWith(double v, double[][][] data) {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				for (int k = 0; k < data[0][0].length; k++) {
					data[i][j][k] = v;
				}
			}
		}
	}
	
	public void fillWith(double v) {
		for (int i = 0; i < dimension[0]; i++)
			for (int j = 0; j < dimension[1]; j++)
				for (int k = 0; k < dimension[2]; k++)
					elements[i][j][k] = v;
	}

	public double getMagnitudeMaximum() {
		double max = 0;
		double v;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					v = Math.abs(elements[i][j][k]);
					if (v > max) max = v;
				}
			}
		}
		
		return max;
	}

	public double getMagnitudeMinimum() {
		double min = 0;
		double v;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					v = elements[i][j][k];
					if (v < min) min = v;
				}
			}
		}
		
		return min;
	}

	public double getMagnitudeAverage() {
		double total = 0;
		int z = 0;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					if (!Double.isNaN(elements[i][j][k])) {
						z++;
						total += Math.abs(elements[i][j][k]); 
					}
				}
			}
		}
		
		return total / z;
	}

	/**
	 * Normalize the volume to the range [0, 1]
	 * @return
	 */
	public Matrix3d normalize() {
		double[] mm = getminMax();
//		System.out.println("Before: " + Arrays.toString(mm));
		
		// Shift to positive value
		sub(mm[0]);
		scale(1d / (mm[1] - mm[0]));

		mm = getminMax();
//		System.out.println("After: " + Arrays.toString(mm));

		return this;
	}
	
	public double[] getminMax() {
		double max = -Double.MAX_VALUE;
		double min = Double.MAX_VALUE;
		double v;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					v = elements[i][j][k];
					if (Double.isNaN(v)) continue;
					
					if (v < min) min = v; 
					if (v > max) max = v; 
 				}
			}
		}
		
//		System.out.println("min = " + min);
//		System.out.println("max = " + max);

		return new double[] { min, max };
	}

	/**
	 * Clamp the absolute value volume intensities to this value.
	 * All values larger or smaller than +-v are set to +- v.
	 * @param v
	 * @return
	 */
	public Matrix3d clamp(double v) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					if (Math.abs(elements[i][j][k]) > v)
						elements[i][j][k] = Math.signum(elements[i][j][k]) * v;
				}
			}
		}
		
		return this;
	}

	/**
	 * Threshold this volume. All values smaller than v are set to u.
	 * @param v
	 * @return
	 */
	public Matrix3d threshold(double v, double u) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					if (elements[i][j][k] < v)
						elements[i][j][k] = u;
				}
			}
		}
		
		return this;
	}
	
	public Matrix3d randomize() {
		return randomize(-1, 1);
	}

	public Matrix3d randomize(double min, double max) {
		Random rand = new Random();
		double delta = max - min;
		
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = min + delta * rand.nextDouble();
 				}
			}
		}
		
		return this;
	}

	public Matrix3d round() {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = MathToolset.roundInt(elements[i][j][k]); 
				}
			}
		}
		
		return this;
	}

	public Matrix3d abs() {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = Math.abs(elements[i][j][k]); 
				}
			}
		}
		
		return this;
	}

	public Matrix3d absOut() {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = Math.abs(elements[i][j][k]); 
				}
			}
		}
		
		return result;
	}
	
	public Matrix3d orOut(Matrix3d other) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = Math.abs(elements[i][j][k]) > EPSILON || Math.abs(other.elements[i][j][k]) > EPSILON ? 1 : 0; 
				}
			}
		}
		
		return result;
	}

	public boolean equals(Matrix3d other) {
		boolean equal = true;
		int diffs = 0;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					equal &= Math.abs(other.elements[i][j][k] - elements[i][j][k]) <= EPSILON;
					if (!equal) break;
				}
				if (!equal) break;
			}
			if (!equal) break;
		}
		
		return equal;
	}
	
	public int countDifferences(Matrix3d other) {
		int diffs = 0;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					if (Math.abs(other.elements[i][j][k] - elements[i][j][k]) > EPSILON)
						diffs++;
				}
			}
		}
		
		return diffs;
	}

	/**
	 * apply NOT voxel-wise operator in place 
	 * @return
	 */
	public Matrix3d not() {
		int[] dim = getDimension();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					elements[i][j][k] = Math.abs(elements[i][j][k]) <= EPSILON ? 1 : 0;
				}
			}
		}
		
		return this;
	}
	
	/**
	 * apply NOT voxel-wise operator and return a new matrix with the result 
	 * @return
	 */
	public Matrix3d notOut() {
		Matrix3d result = new Matrix3d(dimension);
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = Math.abs(elements[i][j][k]) <= EPSILON ? 1 : 0;
				}
			}
		}
		
		return result;
	}
	/**
	 * Hadamar product
	 * @param other
	 * @return
	 */
	public Matrix3d hp(Matrix3d other) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = this.elements[i][j][k] * other.elements[i][j][k];
				}
			}
		}
		
		return this;
	}
	
	/**
	 * Hadamar product
	 * @param other
	 * @return
	 */
	public Matrix3d hpOut(Matrix3d other) {
		Matrix3d result = new Matrix3d(dimension);
		
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = this.elements[i][j][k] * other.elements[i][j][k];
				}
			}
		}
		
		return result;
	}

	public Matrix3d scaleOut(double v) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = v*this.elements[i][j][k];
				}
			}
		}
		
		return result;
	}

	public Matrix3d scale(double v) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = v*this.elements[i][j][k];
				}
			}
		}
		return this;
	}

	public Matrix3d add(Matrix3d other) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = this.elements[i][j][k] + other.elements[i][j][k];
				}
			}
		}
		
		return this;
	}

	public Matrix3d addOut(Matrix3d other) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = this.elements[i][j][k] + other.elements[i][j][k];
				}
			}
		}
		
		return result;
	}

	/*
	 * Subtraction methods
	 */
	public Matrix3d sub(Matrix3d other) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = this.elements[i][j][k]  - other.elements[i][j][k];
				}
			}
		}
		
		return this;
	}

	public Matrix3d subOut(Matrix3d other) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = this.elements[i][j][k]  - other.elements[i][j][k];
				}
			}
		}
		
		return result;
	}

	public static Matrix3d subfast(Matrix3d m1, Matrix3d m2, List<int[]> indices) {
		int[] dimension = m1.dimension;
		Matrix3d result = new Matrix3d(dimension);

		for (int[] index : indices) {
			result.elements[index[0]][index[1]][index[2]] = m1.elements[index[0]][index[1]][index[2]]  - m2.elements[index[0]][index[1]][index[2]];
		}
		
		return result;
	}

	public Matrix3d addOut(double v) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = this.elements[i][j][k] + v;
				}
			}
		}
		
		return result;
	}

	public Matrix3d add(double v) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] += v;
				}
			}
		}
		
		return this;
	}

	public Matrix3d sub(double v) {
		return add(-v);
	}

	public Matrix3d subOut(double v) {
		return addOut(-v);
	}

	public Matrix3d copy() {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = this.elements[i][j][k];
				}
			}
		}
		
		return result;
	}

	public Matrix3d square() {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = this.elements[i][j][k] * this.elements[i][j][k];
				}
			}
		}
		
		return this;
	}

	public Matrix3d squareOut() {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = this.elements[i][j][k] * this.elements[i][j][k];
				}
			}
		}
		
		return result;
	}

	public Matrix3d exp(double v) {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = Math.pow(this.elements[i][j][k], v);
				}
			}
		}
		
		return this;
	}

	public double norm() {
		double norm = 0;
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					norm += this.elements[i][j][k] * this.elements[i][j][k];
				}
			}
		}
		
		return Math.sqrt(norm);
	}

	public Matrix3d expOut(double v) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = Math.pow(this.elements[i][j][k], v);
				}
			}
		}
		
		return result;
	}

	public Matrix3d sqrt() {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = Math.sqrt(this.elements[i][j][k]);
				}
			}
		}
		
		return this;
	}

	public Matrix3d sqrtOut() {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = Math.sqrt(this.elements[i][j][k]);
				}
			}
		}
		
		return result;
	}

	public Matrix3d sign() {
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					this.elements[i][j][k] = Math.signum(this.elements[i][j][k]);
				}
			}
		}
		
		return this;
	}

	public Matrix3d signOut() {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					result.elements[i][j][k] = Math.signum(this.elements[i][j][k]);
				}
			}
		}
		
		return result;
	}

	/**
	 * Compute 1st order central differences in the direction <code>d</code> and put the result in a new matrix.
	 * @return the 1st order central difference
	 */
	public Matrix3d dOut(int[] d) {
//		Matrix3d result = new Matrix3d(dimension);
//
//		for (int i = 1; i < dimension[0] - 1; i++) {
//			for (int j = 1; j < dimension[1] - 1; j++) {
//				for (int k = 1; k < dimension[2] - 1; k++) {
//					result.elements[i][j][k] = (this.elements[i + d[0]][j + d[1]][k + d[2]] - this.elements[i - d[0]][j - d[1]][k - d[2]]) / 2;
//				}
//			}
//		}
		return dOutBoundaryRepeated(d, null);
//		return result;
	}

	public double dBoundaryRepeated(int i, int j, int k, int[] d) {
		return dBoundaryRepeated(i, j, k, d, null);
	}
	
	public double dBoundaryRepeated(int i, int j, int k, int[] d, IntensityVolumeMask mask) {
		// Compute forward and backward coordinates
		int fi = i + d[0];
		int fj = j + d[1];
		int fk = k + d[2];
		
		int bi = i - d[0];
		int bj = j - d[1];
		int bk = k - d[2];
		
		/*
		 *  Validate forward and backward coordinates.
		 *  Central values are used for invalid coordinates.
		 */
		boolean foundForward = true;
		boolean foundBackward = true;
		
		if (!contains(fi, fj, fk) || (mask != null && mask.isMasked(fi, fj, fk)) || Double.isNaN(elements[fi][fj][fk])) {
			fi = i;
			fj = j;
			fk = k;
			foundForward = false;
		}
		if (!contains(bi, bj, bk) || (mask != null && mask.isMasked(bi, bj, bk)) || Double.isNaN(elements[bi][bj][bk])) {
			bi = i;
			bj = j;
			bk = k;
			foundBackward = false;
		}
		
		// debug some info
//		if (validForward && validBackward) {
//			System.out.println(i + "," + j + "," + k + " is valid.");
//		}
//		else if (validForward) {
//			System.out.println(i + "," + j + "," + k + " is only valid forwards.");
//		}
//		else if (validBackward) {
//			System.out.println(i + "," + j + "," + k + " is only valid backwards.");
//		}
//		else {
//			System.out.println(i + "," + j + "," + k + " is NOT valid.");
//		}
		
		// If both the forward and backward elementare not found, assume no variation (derivative = 0)
		if (!foundBackward && !foundForward) {
			return 0;
		}
		else
			return (this.elements[fi][fj][fk] - this.elements[bi][bj][bk]) / 2;	
		}

	/**
	 * Compute 1st order central differences in the direction <code>d</code> and put the result in a new matrix.
	 * Values are repeated on the external boundary or if a NaN is found.
	 * @param d
	 * @return the 1st order central difference
	 */
	public Matrix3d dOutBoundaryRepeated(int[] d, IntensityVolumeMask mask) {
		Matrix3d result = new Matrix3d(dimension);

		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					// If this element is masked its derivative is meaningless.
					if (mask != null && mask.isMasked(i, j, k))
						result.elements[i][j][k] = Double.NaN;
					else
						result.elements[i][j][k] = dBoundaryRepeated(i, j, k, d, mask);
				}
			}
		}
		
		return result;
	}

	/**
	 * Compute 1st order central differences in place
	 * @return this matrix
	 */
	public Matrix3d d(int[] d) {
		Matrix3d diff = dOut(d);
		this.elements = diff.elements;
		return this;
	}

	public double d(int x, int y, int z, int[] d) {
		return (this.elements[x + d[0]][y + d[1]][z + d[2]] - this.elements[x - d[0]][y - d[1]][z - d[2]]) / 2;
	}
	
	public double d(Point3i p, int[] d) {
		return (this.elements[p.x + d[0]][p.y + d[1]][p.z + d[2]] - this.elements[p.x - d[0]][p.y - d[1]][p.z - d[2]]) / 2;
	}

	/**
	 * @return the 1st order central difference
	 */
	public Matrix3d dx() {
		return d(FrameAxis.X.getAxisArray());
	}

	/**
	 * @return the 1st order central difference in x.
	 */
	public Matrix3d dxOut() {
		return dOut(FrameAxis.X.getAxisArray());
	}

	public double dxOut(Point3i p) {
		return (elements[p.x + 1][p.y][p.z] - elements[p.x - 1][p.y][p.z]) / 2;
	}
	
	public double dyOut(Point3i p) {
		return (elements[p.x][p.y + 1][p.z] - elements[p.x][p.y - 1][p.z]) / 2;
	}
	
	public double dzOut(Point3i p) {
		return (elements[p.x][p.y][p.z + 1] - elements[p.x][p.y][p.z - 1]) / 2;
	}

	/**
	 * @return the 1st order central difference
	 */
	public Matrix3d dy() {
		return d(FrameAxis.Y.getAxisArray());
	}

	public Matrix3d dyOut() {
		return dOut(FrameAxis.Y.getAxisArray());
	}

	/**
	 * @return the 1st order central difference
	 */
	public Matrix3d dz() {
		return d(FrameAxis.Z.getAxisArray());
	}

	public Matrix3d dzOut() {
		return dOut(FrameAxis.Z.getAxisArray());
	}

	/**
	 * Fill the oustide boundary (of size <code>s</code>) with value <code>border</code>
	 * @param border
	 * @param v
	 */
	public void fillBoundary(int border, double v) {
		// Six faces to fill

		// Bottom
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < border; k++) {
					elements[i][j][k] = v;
				}
			}
		}

		// Up
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = dimension[2] - border; k < dimension[2]; k++) {
					elements[i][j][k] = v;
				}
			}
		}

		// Left
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = 0; j < border; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = v;
				}
			}
		}

		// Right
		for (int i = 0; i < dimension[0]; i++) {
			for (int j = dimension[1] - border; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = v;
				}
			}
		}

		// Front
		for (int i = 0; i < border; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = v;
				}
			}
		}

		// Back
		for (int i = dimension[0] - border; i < dimension[0]; i++) {
			for (int j = 0; j < dimension[1]; j++) {
				for (int k = 0; k < dimension[2]; k++) {
					elements[i][j][k] = v;
				}
			}
		}


	}

	/**
	 * Resize this volume in-place into a square grid
	 * Dimensions are expanded from the center
	 */
	public Matrix3d toSquareGrid() {
		// Get the max dimension
		int[] dim = getDimension();
		int dm = Math.max(Math.max(dim[0], dim[1]), dim[2]);
		
		Point3d pc = new Point3d(dim[0]/2d, dim[1]/2d, dim[2]/2d);
		Point3d pcn = new Point3d(dm/2d, dm/2d, dm/2d);
		Vector3d dv = new Vector3d();
		dv.sub(pcn, pc);
		
		double[][][] data = new double[dm][dm][dm];
		Point3d pt = new Point3d();
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					pt.set(i, j, k);
					pt.add(dv);
					Point3i p = MathToolset.tuple3dTo3i(pt);
					
					data[p.x][p.y][p.z] = elements[i][j][k];
 				}
			}
		}

		setByReference(data);
		
		return this;
	}
	
	public double[][][] getData() {
		return elements;
	}

	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return whether <code>x,y,z</code> is located within this volume
	 */
	public boolean contains(int x, int y, int z) {
		if (x < 0 || y < 0 || z < 0) 
			return false;
		
		if (x >= getDimension()[0] || y >= getDimension()[1] || z >= getDimension()[2])
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
		return name;
//		String s = "";
//		String delim = "\t";
//		String indent = "";
//		
//		for (int k = 0; k < dimension[2]; k++) {
//			for (int i = 0; i < dimension[0]; i++) {
//				s += indent;
//				for (int j = 0; j < dimension[1]; j++) {
//					s += elements[i][j][k] + delim;
//				}
//				s += "\n";
//			}
//			indent += delim;
//			s += "\n";
//		}
//	
//		return s;
	}
	
	public String toStringArray() {
		String s = "";
		String delim = "\t";
		String indent = "";
		
		for (int k = 0; k < dimension[2]; k++) {
			for (int i = 0; i < dimension[0]; i++) {
				s += indent;
				for (int j = 0; j < dimension[1]; j++) {
					s += elements[i][j][k] + delim;
				}
				s += "\n";
			}
			indent += delim;
			s += "\n";
		}
	
		return s;
	}

	public IntensityVolume asVolume() {
		
		IntensityVolume volume = new IntensityVolume(this.elements);
		volume.setName(getName());
		return volume;
	}
	
	public IntensityVolume asVolumeCopy() {
		return new IntensityVolume(this.copy().elements);
	}
	
	/**
	 * Compute the gradient of a matrix v
	 * @param v
	 * @param mask can be null
	 * @return
	 */
	public static Matrix3d[] gradient(Matrix3d v, IntensityVolumeMask mask) {
		Matrix3d[] gradient = new Matrix3d[3];
		
		// Calculate the gradient of a 3d matrix
		gradient[0] = v.dOutBoundaryRepeated(FrameAxis.X.getAxisArray(), mask);
		gradient[1] = v.dOutBoundaryRepeated(FrameAxis.Y.getAxisArray(), mask);
		gradient[2] = v.dOutBoundaryRepeated(FrameAxis.Z.getAxisArray(), mask);

		return gradient;
	}
	
	public static double[] gradient(Matrix3d v, Point3i p) {
		return gradient(v, p, null);
	}
	
	/**
	 * Compute the gradient of a matrix v
	 * @param v
	 * @return
	 */
	public static double[] gradient(Matrix3d v, Point3i p, IntensityVolumeMask mask) {
		double[] gradient = new double[3];
		
		// Calculate the gradient of a 3d matrix
		gradient[0] = v.dBoundaryRepeated(p.x, p.y, p.z, FrameAxis.X.getAxisArray(), mask);
		gradient[1] = v.dBoundaryRepeated(p.x, p.y, p.z, FrameAxis.Y.getAxisArray(), mask);
		gradient[2] = v.dBoundaryRepeated(p.x, p.y, p.z, FrameAxis.Z.getAxisArray(), mask);
		return gradient;
	}

	public static Matrix3d[][] jacobian(Matrix3d[] E) {
		return jacobian(E, (IntensityVolumeMask) null);
	}

	public static double[][] jacobian(VoxelVectorField f, Point3i p) {
		return  Matrix3d.jacobian(new Matrix3d[] { f.getX(), f.getY(), f.getZ() }, p);
	}
	
	public static double[][] jacobian(Matrix3d[] E, Point3i p) {
		double[][] jp = new double[3][3];

		jp[0] = Matrix3d.gradient(E[0], p);
		jp[1] = Matrix3d.gradient(E[1], p);
		jp[2] = Matrix3d.gradient(E[2], p);
		
		return jp;
	}


	public static Matrix3d[][] jacobian(Matrix3d[] E, IntensityVolumeMask mask) {
		
		Matrix3d[][] jacobian = new Matrix3d[3][3];
		jacobian[0] = Matrix3d.gradient(E[0], mask);
		jacobian[1] = Matrix3d.gradient(E[1], mask);
		jacobian[2] = Matrix3d.gradient(E[2], mask);
		
		return jacobian;
	}

	public void destroy() {
		elements = null;
	}
	
	public int getMaxDimension() {
        int[] dim = getDimension();
        return Math.max(Math.max(dim[0], dim[1]), dim[2]);
	}
	
	public static void main(String[] args) {
		int[] dims = new int[] { 2, 2, 2 };
		Matrix3d m = new Matrix3d(dims);
		m.randomize();
		
		System.out.println(m.equals(m));
	}
	
	public double[][][] getElements() {
		return elements;
	}
}
