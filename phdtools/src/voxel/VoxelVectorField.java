package voxel;

import java.util.Arrays;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.DoubleParameter;
import system.object.Triplet;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethod;
import volume.PerVoxelMethodUnchecked;
import voxel.VoxelBox.CuttingPlane;
import diffgeom.Differentiable;
import diffgeom.Differentiator;
import extension.minc.MINCImage;
import gl.geometry.FancyArrow;
import gl.material.GLMaterial;
import heart.Heart;

/**
 * Combines information from individual vector components to produce a volume allowing per-voxel orientation queries.
 * 
 * @author epiuze
 */
public class VoxelVectorField {

//	private MINCImage ex, ey, ez;
	private IntensityVolume v1, v2, v3;
	
	private String title;
	
    /**
     * Voxel box for displaying data.
     */
    private VoxelBox voxelBox;
    
    
    /**
     * Whether we created the voxel box.
     */
    boolean boxOwner = false;

    private boolean highDefinition = false;
    private boolean drawTip = false;
	private boolean helixColoring = false;
	private int cylinderResolution = 16;
	
	private Vector3d vcm = new Vector3d();
	private Vector3d vz = new Vector3d(0, 0, 1);
	private Vector3d vtan = new Vector3d();
	private double raddeg = 180d / Math.PI;

    /**
     * Scale factor.
     */
	private DoubleParameter drawScale = new DoubleParameter("Scale", 1, 0, 10);
	{
		drawScale.setChecked(true);
	}

	private DoubleParameter cylinderRadius = new DoubleParameter("Radius", 0.1, 0, 5);

	public VoxelVectorField(VoxelBox box) {
		boxOwner = box == null;
		voxelBox = box;
		
		if (box != null) {
			v1 = new IntensityVolume(box.getDimension());
			v2 = new IntensityVolume(box.getDimension());
			v3 = new IntensityVolume(box.getDimension());
		}
		this.color = new float[] { 1,0,0 };
	}
	
	public VoxelVectorField(VoxelVectorField other) {
		setTitle(other.title);
		this.v1 = new IntensityVolume(other.v1);
		this.v2 = new IntensityVolume(other.v2);
		this.v3 = new IntensityVolume(other.v3);
		
		if (other.voxelBox != null)
			this.voxelBox = new VoxelBox(other.voxelBox);
	}

	public VoxelVectorField(VoxelVectorField other, VoxelBox box) {
		setTitle(other.title);
		
		this.v1 = new IntensityVolume(other.v1);
		this.v2 = new IntensityVolume(other.v2);
		this.v3 = new IntensityVolume(other.v3);
		this.color = other.color;
		this.voxelBox = box;
	}

	public VoxelVectorField(String title, IntensityVolume v1, IntensityVolume v2, IntensityVolume v3, VoxelBox voxelBox) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;

		setTitle(title);
		
   		boxOwner = voxelBox == null;
   		this.voxelBox = voxelBox;

    	// Can map the data in the appropriate range.
    	// FIXME: no! The data might be scale dependent so if we do this
    	// we lose the information.
//		remap(1);
	}

	public VoxelVectorField(String title, IntensityVolume v1, IntensityVolume v2, IntensityVolume v3) {
		this(title, v1, v2, v3, null);
	}
	
	/**
	 * Create a new VoxelVectorField from three MINC images.
	 * @param title A descriptive name for this vector field.
	 */
	public VoxelVectorField(String title, MINCImage ex, MINCImage ey, MINCImage ez) {
		this(title, ex, ey, ez, null);
	}
	/**
	 * Create a new VoxelVectorField from three MINC images.
	 * @param title A descriptive name for this vector field.
	 * @param box The VoxelBox to reuse. If set to null, a new box will be created.
	 */
	public VoxelVectorField(String title, MINCImage ex, MINCImage ey, MINCImage ez, VoxelBox box) {
		this (title, new IntensityVolume(ex.getVolume()), new IntensityVolume(ey.getVolume()), new IntensityVolume(ez.getVolume()), null);
	}

	/**
	 * Replace this vector field with the orthogonal one to the one given as argument. 
	 * @param orthogonalComponent
	 * @param box
	 */
	public void orthogonalize(final VoxelVectorField orthogonalComponent, VoxelBox box) {
		final Vector3d v = new Vector3d();
		final Vector3d vOrtho = new Vector3d();
		
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				// Get our vector at this location
				getVector3d(x, y, z, v);
				
				// Get the orthogonal component at this location
				orthogonalComponent.getVector3d(x, y, z, vOrtho);
				
				// Orthogonalize our vector and normalize
				// i.e. v = v - <v,v_ortho> * v_ortho
				v.scaleAdd(-v.dot(vOrtho), vOrtho, v);
				v.normalize();
				
				v1.set(x, y, z, v.x);
				v2.set(x, y, z, v.y);
				v3.set(x, y, z, v.z);
			}
		});
	}
	
	/**
	 * @return the divergence of this field
	 */
	public VoxelVectorField computeCurl(final double h) {

		final Point3i dim = new Point3i(getDimension());
		
		// Invalid voxels will be evaluated at this value
		
		int x = 0;
		int y = 1;
		int z = 2;
		double boundaryValue = 0;
		
		// U1
		IntensityVolume dFzdy = diff(z, y, h, boundaryValue);
		IntensityVolume dFydz = diff(y, z, h, boundaryValue);
		IntensityVolume delx = dFzdy.sub(dFydz).asVolume();

		// U2
		IntensityVolume dFxdz = diff(x, z, h, boundaryValue);
		IntensityVolume dFzdx = diff(z, x, h, boundaryValue);
		IntensityVolume dely = dFxdz.sub(dFzdx).asVolume();

		// U3
		IntensityVolume dFydx = diff(y, x, h, boundaryValue);
		IntensityVolume dFxdy = diff(x, y, h, boundaryValue);
		IntensityVolume delz = dFydx.sub(dFxdy).asVolume();

		VoxelVectorField curl = new VoxelVectorField("curl", delx, dely, delz);
		return curl;
	}

	/**
	 * @return the divergence of this field
	 */
	public IntensityVolume computeDivergence(double h) {

		Point3i dim = new Point3i(getDimension());
		double[][][] div = new double[dim.x][dim.y][dim.z];
		
		IntensityVolume[] g = computeGradient(h);

    	IntensityVolume divergence = new IntensityVolume(div);

    	divergence.add(g[0]);
    	divergence.add(g[1]);
    	divergence.add(g[2]);
		
    	return divergence;
	}

	/**
	 * @return the divergence of this field
	 */
	public IntensityVolume[] computeGradient(final double h) {
		double boundaryValue = 0;
		
		IntensityVolume dx = diff(0, 0, h, boundaryValue);
		IntensityVolume dy = diff(1, 1, h, boundaryValue);
		IntensityVolume dz = diff(2, 2, h, boundaryValue);
		
		return new IntensityVolume[] { dx, dy, dz };
	}

	/**
	 * @param box
	 * @param dFi the index of the component that is derived (0, 1 or 2)
	 * @param dxj the index of the component with respect to which the derivative is taken (0, 1 or 2)
	 * @param h
	 * @return
	 */
	public IntensityVolume diff(final int dFi, final int dxj, final double h, final double boundaryValue) {

		final Point3i dim = new Point3i(getDimension());
		IntensityVolume[] F = new IntensityVolume[] { v1, v2, v3 };
		final IntensityVolume Fi = F[dFi];
		final int[] p = new int[3];

		final IntensityVolume dxidxj = new IntensityVolume(dim.x, dim.y, dim.z);
		for (int i = 0; i < dim.x; i++) {
			for (int j = 0; j < dim.y; j++) {
				for (int k = 0; k < dim.z; k++) {
					p[0] = i;
					p[1] = j;
					p[2] = k;

					double dx = Differentiator.d(new Differentiable() {
						@Override
						public double F(double xi) {
							p[dxj] = MathToolset.roundInt(xi);
							if (getVoxelBox().dimensionContains(p))
								return Fi.getIntensity(p[0], p[1], p[2]);
							else
								return boundaryValue;
						}
					}, p[dxj], h);

					dxidxj.set(i, j, k, dx);
				}
			}
		}
		
		return dxidxj;
	}

	/**
	 * @return a VoxelFrameField containing the computed Frenet frame at each voxel
	 */
	public VoxelFrameField computeFrenetField() {

		final Vector3d tk = new Vector3d();
		final Vector3d tkm1 = new Vector3d();
		final Point3i p = new Point3i();
		final Vector3d n = new Vector3d();
		final Vector3d b = new Vector3d();
		
		// Intensity volumes
		final IntensityVolume[] ix = new IntensityVolume[3];
		final IntensityVolume[] iy = new IntensityVolume[3];
		final IntensityVolume[] iz = new IntensityVolume[3];
		
		int[] vdim = getVoxelBox().getDimension();
		
		double[][][] data;
		for (int i = 0; i < 3; i++) {
			data = new double[vdim[0]][vdim[1]][vdim[2]];
			ix[i] = new IntensityVolume(data);

			data = new double[vdim[0]][vdim[1]][vdim[2]];
			iy[i] = new IntensityVolume(data);
			
			data = new double[vdim[0]][vdim[1]][vdim[2]];
			iz[i] = new IntensityVolume(data);
		}
		
		VoxelBox box = getVoxelBox();
		
		box.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
				p.set(x, y, z);
				
				// This is our tangent t_k
				getVector3d(x, y, z, tk);
				
				// Try to get the backward tangent tkm1
				p.x -= MathToolset.roundInt(tk.x);
				p.y -= MathToolset.roundInt(tk.y);
				p.z -= MathToolset.roundInt(tk.z);
				
				if (getVoxelBox().dimensionContains(p)) {
					getVector3d(p.x, p.y, p.z, tkm1);
					
					// Only valid if tkm1 is non-zero
					if (tkm1.length() > 1e-4) {
						// Compute full Frenet frame and store it
						b.cross(tkm1, tk);
						n.cross(b, tk);
						
						tk.normalize();
						b.normalize();
						n.normalize();

						// T
						ix[0].set(x, y, z, tk.x);
						ix[1].set(x, y, z, tk.y);
						ix[2].set(x, y, z, tk.z);

						// B
						iy[0].set(x, y, z, b.x);
						iy[1].set(x, y, z, b.y);
						iy[2].set(x, y, z, b.z);
						
						// N
						iz[0].set(x, y, z, n.x);
						iz[1].set(x, y, z, n.y);
						iz[2].set(x, y, z, n.z);
					}
				}
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});
		
		VoxelVectorField vT = new VoxelVectorField("T", ix[0], ix[1], ix[2]);
		vT.getVoxelBox().cut(box.getCuttingPlane());

		VoxelVectorField vB = new VoxelVectorField("B", iy[0], iy[1], iy[2]);
		vB.getVoxelBox().cut(box.getCuttingPlane());

		VoxelVectorField vN = new VoxelVectorField("N", iz[0], iz[1], iz[2]);
		vN.getVoxelBox().cut(box.getCuttingPlane());
		
		VoxelFrameField field = new VoxelFrameField(vT, vB, vN);
		return field;
	}

	public void set(VoxelVectorField other) {
		voxelBox.set(other.voxelBox);
		v1 = other.v1;
		v2 = other.v2;
		v3 = other.v3;
		color = Arrays.copyOf(other.color, other.color.length);
		
		title = other.title;
		
		if (vfp != null)
			vfp.getPanel().invalidate();
	}
	
	private void createBox() {
		Point3i dim = getDimension();
		
		voxelBox = new VoxelBox(new Vector3d(dim.x, dim.y, dim.z), new Point3d(0, 0, 0));
		voxelBox.cut(CuttingPlane.FIT);
	}
	
	public void setVoxelBox(VoxelBox box) {
		this.voxelBox = box;
		boxOwner = false;
	}
	
	public void setVolumetricData(MINCImage ix, MINCImage iy, MINCImage iz) {
		setVolumetricData(new IntensityVolume(ix.getVolume()), new IntensityVolume(iy.getVolume()), v3 = new IntensityVolume(iz.getVolume()));
	}
 
	public void setVolumetricData(IntensityVolume v1, IntensityVolume v2, IntensityVolume v3) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
		
//		remap(1);
	}

	public void flip() {
		v1.negate();
		v2.negate();
		v3.negate();
	}

	/**
	 * @return a vector where each component represents its coordinate dimension.
	 */
	public Point3i getDimension() {
		return v1.getDimensions();
	}
	
	public void display(GLAutoDrawable drawable, List<Point3i> voxels) {
		if (!drawScale.isChecked()) return;
		
		gl = drawable.getGL().getGL2();

		// Setup GL
		gl.glDisable(GL2.GL_LIGHTING);
		
		gl.glLineWidth(1.0f);
		gl.glBegin(GL2.GL_LINES);
		
		// Compute mean voxel
		
		Vector3d v = new Vector3d();
		for (Point3i p : voxels) {
			int x = p.x;
			int y = p.y;
			int z = p.z;
			
			getVector3d(x, y, z, v);
			v.scale(drawScale.getValue());
			
//			gl.glVertex3d(x, y, z);
//			gl.glVertex3d(x + v.x, y + v.y, z + v.z);
			gl.glVertex3d(x - 0.5 * v.x, y - 0.5 * v.y, z - 0.5 * v.z);
			gl.glVertex3d(x + 0.5 * v.x, y + 0.5 * v.y, z + 0.5 * v.z);
		}
		gl.glEnd();
	}

	public void display(final GLAutoDrawable drawable) {
		if (!drawScale.isChecked() || v1 == null || v2 == null || v3 == null) return;
		gl = drawable.getGL().getGL2();

		// Setup GL
		gl.glDisable(GL2.GL_LIGHTING);

		// Store depth buffer state
		// if we want to restore it later
		byte[] buffer = new byte[32];
		gl.glGetBooleanv(GL2.GL_DEPTH_TEST, buffer, 0);
		boolean restoreDepthTest = (buffer[0]!=0);

		if (!highDefinition)
			gl.glDisable(GL2.GL_DEPTH_TEST);
		
		final Vector3d v = new Vector3d();
		final double scale = drawScale.getValue();

		// Create the box if necessary
    	if (voxelBox == null) {
    		boxOwner = true;
    		createBox();
    	}

		final Point3d pa = new Point3d();
		final Point3d pb = new Point3d();
		final Point3d p = new Point3d();
		final double radius = cylinderRadius.getValue();
//		System.out.println(radius);
		
		float am = 0.5f;
		float dm = 1f;
		final float lightAmbient[] = { am, am, am };
		final float lightDiffuse[] = { dm, dm, dm };
//		final int res = 16;
//		final int res = 12;
		final int res = cylinderResolution;
		
		final boolean drawCaps = true;

    	if (highDefinition) {
    		gl.glEnable(GL2.GL_LIGHTING);

    		// Set bright and shiny
//    		gl.glMaterialf(GL2.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 1f);
//    		gl.glMaterialf(GL2.GL_FRONT_AND_BACK, GL2.GL_EMISSION, 1f);
    	        
			GLMaterial.CHROME.apply(gl);

    		if (glu == null)
    			glu = new GLU();
    		
    		if (quadric == null) {
    			quadric = glu.gluNewQuadric();
    		}
    		
    		// GLU_NONE, GLU_FLAT or GLU_SMOOTH
    		glu.gluQuadricNormals(quadric, GLU.GLU_SMOOTH);
    		
    		// GLU_POINT, GLU_LINE, GLU_FILL or GLU_SILHOUETTE
    		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
    		
    		// GLU_OUTSIDE or GLU_INSIDE
    		glu.gluQuadricOrientation(quadric, GLU.GLU_OUTSIDE);

    		// FIXME: why do we need this?!
    		// Move before drawing list of points?
    		
//    		GLMaterial.PEARL.apply(gl);

//    		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightAmbient, 0);
//    		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightDiffuse, 0);

    		float[] colorbck;
    		if (color.length == 4)
        		colorbck = new float[] { color[0], color[1], color[2], color[3] };
    		else
        		colorbck = new float[] { color[0], color[1], color[2] };

    		gl.glEnable(GL2.GL_LIGHTING);

    		PerVoxelMethod coloringMethod = null;
    		
    		if (helixColoring) {
        		color = new float[4];
        		
    			coloringMethod = new PerVoxelMethod() {
        			
        			/**
        			 * Method called during the per-voxel processing. Used to display the voxel.
        			 */
        			@Override
        			public void process(int x, int y, int z) {
        				if (gl == null) return;
        								
        				getVector3d(x, y, z, v);
        				
        				x += offset.x + 0.5;
        				y += offset.y + 0.5;
        				z += offset.z + 0.5;

//        				applyHelixColor(gl, x, y, z, v, color);

        				p.set(x + 0.5, y + 0.5, z + 0.5);
        				pa.scaleAdd(-0.5, v, p);
        				pb.scaleAdd(0.5, v, p);
        				
//        				drawCylinder(drawable, pa, pb, scale, radius, res, drawCaps, lightAmbient);
        				drawCylinder(drawable, pa, pb, scale, radius, res, drawCaps, drawTip, color);
        			}
        			
        			@Override
        			public boolean isValid(Point3d origin, Vector3d span) {
        				return true;
        			}
        		};
    		}
    		else {
    			color = new float[] { color[0], color[1], color[2], 1 };
    			final Color3f color2 = new Color3f(color);
    			coloringMethod = new PerVoxelMethod() {
        			
        			/**
        			 * Method called during the per-voxel processing. Used to display the voxel.
        			 */
        			@Override
        			public void process(int x, int y, int z) {
        				if (gl == null) return;
        								
        				getVector3d(x, y, z, v);
        				
        				x += offset.x + 0.5;
        				y += offset.y + 0.5;
        				z += offset.z + 0.5;

        				p.set(x + 0.5, y + 0.5, z + 0.5);
        				pa.scaleAdd(0, v, p);
        				pb.scaleAdd(1, v, p);
//        				pb.scaleAdd(0.5, v, p);
        				
//        				drawCylinder(drawable, pa, pb, scale, radius, res, drawCaps, lightAmbient);
        				drawCylinder(drawable, pa, pb, scale, radius, res, drawCaps, drawTip, color2);
        			}
        			
        			@Override
        			public boolean isValid(Point3d origin, Vector3d span) {
        				return true;
        			}
        		};
    		}
    		// Begin per voxel computation. This method will call our process method.
    		voxelBox.voxelProcess(coloringMethod);

    		color = colorbck;
    	}
    	else {
    		// Begin per voxel computation. This method will call our process method.
    		gl.glLineWidth(2.0f);
    		gl.glBegin(GL2.GL_LINES);
//    		new VoxelBox(getDimensions()).voxelProcess(new PerVoxelMethod() {
    		voxelBox.voxelProcess(new PerVoxelMethod() {
    			
    			/**
    			 * Method called during the per-voxel processing. Used to display the voxel.
    			 */
    			@Override
    			public void process(int x, int y, int z) {
    				if (gl == null) return;
    								
    				getVector3d(x, y, z, v);
    				
    				v.scale(scale);

    				x += offset.x;
    				y += offset.y;
    				z += offset.z;
    				
    				gl.glColor4d(1, 1, 1, 0.1);
//    				gl.glColor4d(color[0], color[1], color[2], 1);
    				gl.glVertex3d(x - 0.5 * v.x + 0.5, y - 0.5 * v.y + 0.5, z - 0.5 * v.z + 0.5);
    				gl.glColor4d(color[0], color[1], color[2], 1);
    				gl.glVertex3d(x + 0.5 * (1 + v.x), y + 0.5 * (1 + v.y), z + 0.5 * (1 + v.z));
    			}
    			
    			@Override
    			public boolean isValid(Point3d origin, Vector3d span) {
    				return true;
    			}
    		});
    		gl.glEnd();
    	}
		
		if (restoreDepthTest)
			gl.glEnable(GL2.GL_DEPTH_TEST);
	}
	
	private Point3d cm = new Point3d();
	private Vector3d n = new Vector3d();
	private float lightDiffuse[] = { 1.0f, 1.0f, 1.0f };
	private double rtodeg = 180 / Math.PI;
	private static GLU glu = null;
	private GLUquadric quadric = null;
	private FancyArrow fa = new FancyArrow();

	private void drawCylinder(GLAutoDrawable drawable, Point3d pa, Point3d pb, double length, double radius, int res, boolean drawCaps, boolean drawTip, Color3f color) {
		GL2 gl = drawable.getGL().getGL2();
		fa.set(pa, pb, color, radius);
		fa.setFast(true);
		fa.draw(gl);
	}
	
	private void drawCylinder(GLAutoDrawable drawable, Point3d pa, Point3d pb, double length, double radius, int res, boolean drawCaps, boolean drawTip, float color[]) {
		GL2 gl = drawable.getGL().getGL2();
//		FancyArrow fa = new FancyArrow(pa, pb, new Color3f(color), radius);
		fa.set(pa, pb, new Color3f(color), radius);
		fa.draw(gl);
		
//		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, color, 0);
//		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, color, 0);
//		n.sub(pb, pa);
//		Cylinder cyl = new Cylinder(cylinderResolution, pa, n, radius, pa.distance(pb) / 2);
//		cyl.display(gl);
//
//		// VCM is in the direction of the cylinder
//		vcm.sub(pb, pa);
//
//		// CM is the center of the cylinder
//		cm.scaleAdd(0.5, vcm, pa);
//
//		// n is the bottom tip of the scaled cylinder
//		n.sub(pa, pb);
//		n.scale(length / 2);
//		n.add(cm);
//
//		/*
//		 * Draw the cylinder
//		 */
//		gl.glPushMatrix();
//		
//		// Extract spherical angles
//		double theta = -Math.PI / 2 +  Math.acos(vcm.z / vcm.length());
//		double phi = Math.atan2(vcm.y, vcm.x);
//
//		gl.glTranslated(n.x, n.y, n.z);
//
//		// Orient the cylinder
//		gl.glRotated(rtodeg * phi, 0, 0, 1);
//		gl.glRotated(rtodeg * theta, 0, 1, 0);
//
//		gl.glRotated(90, 0, 1, 0);
//		
//		if (glu == null)
//			glu = new GLU();
//
//		// Draw cylinder
//		// base, top, height, slices, stacks
//		glu.gluCylinder(quadric, radius, radius, length, res, res);
//		
//		// Destroy quadric
////		glu.gluDeleteQuadric(quadric);
//		
//		gl.glPopMatrix();
//		if (drawCaps) {
//			// Top cap
//			gl.glPushMatrix();
//			vcm.scale(length);
//			gl.glTranslated(vcm.x, vcm.y, vcm.z);
//			gl.glTranslated(n.x, n.y, n.z);
//
//			// Orient arrow
////			gl.glRotated(rtodeg * phi, 0, 0, 1);
////			gl.glRotated(rtodeg * theta, 0, 1, 0);
////			gl.glRotated(90, 0, 1, 0);
//
//			if (drawTip) {
//				glu.gluDisk(quadric, 0, 2*radius, res, res);
//				JoglRenderer.glut.glutSolidCone(2*radius, 5 * radius, res, 5);
//			}
//			else {
//				glu.gluDisk(quadric, 0, radius, res, res);
//			}
//
//			gl.glPopMatrix();
//		
//			// Bottom cap
//			gl.glPushMatrix();
//			gl.glTranslated(n.x, n.y, n.z);
////			vcm.negate();
//			
//			gl.glRotated(rtodeg * phi, 0, 0, 1);
//			gl.glRotated(rtodeg * theta, 0, 1, 0);
//
//			gl.glRotated(-90, 0, 1, 0);
//
//			glu.gluDisk(quadric, 0, radius, res, res);
//
//			gl.glPopMatrix();
//		}
	}
	
	public void setHelixColoring(boolean b) {
		
		helixColoring = b;
	}
	
	private void applyHelixColor(GL2 gl, int x, int y, int z, Vector3d v, float[] color) {
//		// Extract helix angle
//		vcm.set(voxelBox.getMask().getCenterOfMass());
//		vcm.x -= x;
//		vcm.y -= y;
//		vcm.z -= z;
//		vtan.cross(vcm, vz);
//		vtan.normalize();
		
		// Helix angle is the angle between v and vtan
//		float ha = (float) (180 - raddeg * Math.acos(vtan.dot(v)));
		// Apply color map
//		gl.glColor3d((180 - ha) / 180, ha / 180, 0);
//		color[0] = (180f - ha) / 180f;
//		color[0] = (float)Math.cos(2 * Math.PI * (180f - ha) / 180f);
//		color[1] = (float)Math.sin(2 * Math.PI * ha / 180f);
		
		float ha = (float) Math.asin(v.z);
		double r = 0.6;
		color[1] = (float) (Math.cos(r*(ha + Math.PI / 2)));
		color[0] = (float) (Math.cos(r*(ha - Math.PI / 2)));
		color[3] = 1;
		
//		float ha = (float) (raddeg * Math.acos(v.z));
//		color[0] = (180f - ha) / 180f;
//		color[1] = ha / 180f;
//		color[2] = 0;
//		color[3] = 1;
	}
	
	private float[] color = new float[] { 1f, 0f, 0f };
	public void setColor(double[] color) {
		if (color.length == 4)
			this.color = new float[] { (float) color[0], (float) color[1], (float) color[2], (float) color[3] };
		else
			this.color = new float[] { (float) color[0], (float) color[1], (float) color[2] };
	}

	public Point3d getCenter() {
		
		int[] dim = v1.getDimension();

		Point3d p = new Point3d();
		Vector3d v = new Vector3d();
		
		int count = 0;
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					getVector3d(i, j, k, v);
					
					if (v.length() < 1e-4) continue;
					
					count++;
					p.x += i;
					p.y += j;
					p.z += k;
				}
			}
		}
		
		p.scale(1d / count);
		
		return p;
	}
	
	private VerticalFlowPanel vfp;
	
    public JPanel getControls() {

        vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title, TitledBorder.LEFT, TitledBorder.CENTER));
        
        if (boxOwner) {

        	if (voxelBox == null) {
        		createBox();
        	}

        	CollapsiblePanel cp = voxelBox.getControls();
        	cp.expand();
            vfp.add(cp);
        }
        
        vfp.add(drawScale.getSliderControlsExtended("draw"));
//        vfp.add(cylinderRadius.getSliderControls());

//        JButton btnExport = new JButton("Export to obj");
//        vfp.add(btnExport);
        return vfp.getPanel();
    }
    
    public VoxelBox getVoxelBox() {
    	return voxelBox;
    }

    private GL2 gl = null;
    
    /**
     * Compute the voxel-wise cross product of two images and returns the result as an image.
     * @param i1
     * @param i2
     * @return i3, where i3 = i1 x i2
     */
    public static VoxelVectorField cross(VoxelVectorField i1, VoxelVectorField i2) {
    	// Images should have the same dimension
    	if (!i1.getDimension().equals(i2.getDimension())) {
    		throw new RuntimeException("Cannot compute cross product: images have different dimensions.");
    	}
    	
    	VoxelVectorField i3 = new VoxelVectorField(i1.getVoxelBox());
    	
		// Assume all images have same dimension
    	Point3i d = i1.getDimension();
		double[][][] datax = new double[d.x][d.y][d.z];
		double[][][] datay = new double[d.x][d.y][d.z];
		double[][][] dataz = new double[d.x][d.y][d.z];
		
		Vector3d v1 = new Vector3d();
		Vector3d v2 = new Vector3d();
		Vector3d v3 = new Vector3d();
    	for (int x = 0; x < d.x; x++) {
        	for (int y = 0; y < d.y; y++) {
            	for (int z = 0; z < d.z; z++) {
            		i1.getVector3d(x, y, z, v1);
            		i2.getVector3d(x, y, z, v2);
            		
            		// Check for orthogonality
//            		if (v1.dot(v2) != 0) {
//            			System.err.println("Voxel (" + x + "" + y + "" + z +") not orthogonal");
//            			continue;
//            		}
            		
        			v3.cross(v1, v2);
        			datax[x][y][z] = v3.x;
        			datay[x][y][z] = v3.y;
        			dataz[x][y][z] = v3.z;
            	}
        	}
    	}
    	
    	i3.setVolumetricData(new IntensityVolume(datax), new IntensityVolume(datay), new IntensityVolume(dataz));
    	
    	return i3;
    }

	/** 
	 * Make this vector field aligned towards center points.
	 */
	public void enforceCenterAlignment(Heart heart) {

		final List<Triplet<Boolean, Point3d, Double>> centers = heart.getGeometry().getCenterline();
		
		final Vector3d v = new Vector3d();
		final Vector3d vc = new Vector3d();
		VoxelBox box = new VoxelBox(heart.getMask());
		System.out.println(box.getVolume().size())	;
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				vc.set(x, y, z);
				vc.negate();
				vc.add(centers.get(z).getCenter());
				getVector3d(x, y, z, v);
				set(x, y, z, v);
			}
		});
	}
    /**
     * Create an orthogonal field to this one based on cylindrical (radial) coordinates (z-axis = cylinder centerline) 
     * @param vtangent A voxel vector field
     * @param center the center of the cylindrical (can be any point on the midline)
     * @return i3, where i3 = i1 x i2
     */
    public static VoxelVectorField orthogonalizeCylindrical(VoxelVectorField vtangent, Point3d x0, Point3d x1, double z0, double z1) {
    	
    	System.out.println("Computing ortho frame...");
    	// Create the data containers
    	Point3i d = vtangent.getDimension();
		double[][][] datax = new double[d.x][d.y][d.z];
		double[][][] datay = new double[d.x][d.y][d.z];
		double[][][] dataz = new double[d.x][d.y][d.z];
		
		Vector3d v = new Vector3d();
		Vector3d n = new Vector3d();
		Point3d p = new Point3d();
		Point3d pc = new Point3d();
		double dz = Math.abs(z1 - z0);
		v.sub(x1, x0);
		
    	for (int x = 0; x < d.x; x++) {
        	for (int y = 0; y < d.y; y++) {
            	for (int z = 0; z < d.z; z++) {
            		if (vtangent.getVoxelBox().isOutside(x, y, z))
            			continue;
            		
//            		// Vector normal to the cylinder
//            		w.sub(center, p);
//            		w.normalize();
            		p.set(x, y, z);
            		
            		double zp = Math.abs((z - z0) / dz);
            		
//            		System.out.println(zp);
            		pc.scaleAdd(zp, v, x0);
            		n.sub(pc, p);
            		n.z = 0;
            		n.normalize();
            		
            		// Project onto normal space of the vector field
            		// n = w - (w . v) v
            		vtangent.getVector3d(x, y, z, v);
            		n.scaleAdd(-n.dot(v), v, n);
            		n.normalize();

            		// Fill in the data
        			datax[x][y][z] = n.x;
        			datay[x][y][z] = n.y;
        			dataz[x][y][z] = n.z;
            	}
        	}
    	}

    	VoxelVectorField vnorm = new VoxelVectorField("Ortho", new IntensityVolume(datax), new IntensityVolume(datay), new IntensityVolume(dataz), vtangent.getVoxelBox());

    	System.out.println("Done.");
    	
    	return vnorm;
    }

    private Vector3d world = new Vector3d(1, 0, 0);

    public Vector3d getVector3d(int x, int y, int z) {
    	return getVector3d(x, y, z, new Vector3d());
	}

    public Vector3d getVector3d(int x, int y, int z, Vector3d result) {
		result.set(v1.getIntensity(x, y, z), v2.getIntensity(x, y, z), v3.getIntensity(x, y, z));
		
		// FIXME: hack to get consistent orientations
//		result.scale(Math.signum(result.dot(world)));
		
		// No need to normalize as 
//		v.normalize();
		// Remap intensity image into vector image
		
//		// Normalize to 2
//		v.scale(2.0 / (255));
//		
//		// Rescale from -1 to 1
//		v.sub(new Vector3d(1, 1 ,1));
		return result;
	}
	
//	public Vector3d getVector3d(int x, int y, int z) {
//		Vector3d v = new Vector3d();
//		getVector3d(x, y, z, v);
//		
//		return v;
//	}
	
	public void getVectorArray(int x, int y, int z, double[] array) {
		array[0] = v1.getIntensity(x, y, z);
		array[1] = v2.getIntensity(x, y, z);
		array[2] = v3.getIntensity(x, y, z);
	}
	
	public void setTitle(String title) {
		this.title = title;
		
		if (title != null && v1 != null && v2 != null && v3 != null) {
			this.v1.setName(title + "x");
			this.v2.setName(title + "y");
			this.v3.setName(title + "z");
		}
	}

	public Vector3d getVector3d(Point3i p) {
		return getVector3d(p.x, p.y, p.z);
	}

	public Vector3d getVector3d(Point3d p, Vector3d v) {
		return getVector3d((int)Math.round(p.x), (int)Math.round(p.y), (int)Math.round(p.z), v);
	}


//	public Vector3d getVector3d(Point3d p) {
//		return getVector3d((int)Math.round(p.x), (int)Math.round(p.y), (int)Math.round(p.z));
//	}
	
	public void setVisible(boolean visible) {
		drawScale.setChecked(visible);
	}
	
	public boolean isVisible() {
		return drawScale.isChecked();
	}
	
	public void setDrawScale(double scale) {
		this.drawScale.setValue(scale);
	}

	private Point3d offset = new Point3d();
	
	/**
	 * Extract a bounding box and center the volume
	 */
	public void crop() {
		
		Point3d[] bb = getTightBoundingBox();
		
		bb[0].negate();
		offset.set(bb[0]);
	}
	
	public Point3d[] getTightBoundingBox() {
		final double threshold = 1e-6;
		
		final Point3d max = new Point3d(Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE);
		final Point3d min = new Point3d(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);

		final Vector3d v = new Vector3d();

    	if (voxelBox == null) {
    		boxOwner = true;
    		createBox();
    	}

		// Begin per voxel computation. This method will call our process method.
		voxelBox.voxelProcess(new PerVoxelMethod() {
			
			@Override
			public void process(int x, int y, int z) {
								
				getVector3d(x, y, z, v);
				
				// In order to determine bounding volume, only keep 
				// vectors with a thresholded magnitude
				
				if (v.length() >= threshold) {
					if (x > max.x) max.x = x; 
					if (y > max.y) max.y = y; 
					if (z > max.z) max.z = z; 
					
					if (x < min.x) min.x = x; 
					if (y < min.y) min.y = y; 
					if (z < min.z) min.z = z; 
				}
			}
			
			@Override
			public boolean isValid(Point3d origin, Vector3d span) {
				return true;
			}
		});

		return new Point3d[] { min, max };
	}
	
	public IntensityVolume[] getVolumes() {
		return new IntensityVolume[] { v1, v2, v3 };
	}

	public void destroy() {
		v1.destroy();
		v2.destroy();
		v3.destroy();
		
		v1 = null;
		v2 = null;
		v3 = null;
		voxelBox = null;
	}

	public void setHighDef(boolean v) {
		highDefinition = v;
	}

	public void setDrawTip(boolean v) {
		drawTip = v;
	}

	public void setDrawRadius(double radius) {
		cylinderRadius.setValue(radius);		
	}

	public void setDrawResolution(int res) {
		cylinderResolution = res;
	}

	public void normalize() {
		normalize(new VoxelBox(getDimensions()));
	}
	
	public void normalize(VoxelBox box) {
		final Vector3d v = new Vector3d();
		
		box.voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				// Get the vector from this location and normalize it
				getVector3d(x, y, z, v);
				v.normalize();
				
//				if (MathToolset.isNan(v)) {
//					System.nanoTime();
//				}
				
				// Replace
				getVolumes()[0].set(x, y, z, v.x);
				getVolumes()[1].set(x, y, z, v.y);
				getVolumes()[2].set(x, y, z, v.z);
			}
		});
	}

	public void nanify(IntensityVolumeMask mask) {
		getVolumes()[0].nanify(mask);
		getVolumes()[1].nanify(mask);
		getVolumes()[2].nanify(mask);
	}

	public IntensityVolume getX() {
		return v1;
	}
	public IntensityVolume getY() {
		return v2;
	}
	public IntensityVolume getZ() {
		return v3;
	}

	public void set(Point3i voxel, Vector3d v) {
		v1.set(voxel.x, voxel.y, voxel.z, v.x);
		v2.set(voxel.x, voxel.y, voxel.z, v.y);
		v3.set(voxel.x, voxel.y, voxel.z, v.z);
	}

	public void set(int x, int y, int z, Vector3d v) {
		v1.set(x, y, z, v.x);
		v2.set(x, y, z, v.y);
		v3.set(x, y, z, v.z);
	}

	public VoxelVectorField voxelCopy(VoxelBox box) {
		IntensityVolume v1c = v1.extract(box);
		IntensityVolume v2c = v2.extract(box);
		IntensityVolume v3c = v3.extract(box);
		VoxelBox boxc = new VoxelBox(box.getMask().extract(box));
		return new VoxelVectorField(title, v1c, v2c, v3c, boxc);
	}
	
	public String getTitle() {
		return title;
	}

	public int[] getDimensions() {
		Point3i dims = getDimension();
		return new int[] { dims.x, dims.y, dims.z };
	}
}
