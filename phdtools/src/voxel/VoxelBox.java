package voxel;

import gl.geometry.GLPickable;
import gl.geometry.primitive.Cube;
import gl.renderer.TrackBallCamera;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import swing.component.CollapsiblePanel;
import swing.component.EnumComboBox;
import swing.component.VerticalFlowPanel;
import swing.event.Interactor;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameterPoint3d;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.parameters.ParameterManager;
import tools.ScenePicker;
import tools.frame.CoordinateFrame;
import tools.frame.CoordinateFrameSample;
import tools.frame.VolumeSampler;
import tools.frame.OrientationFrame.FrameAxis;
import tools.frame.OrientationFrame.SamplingStyle;
import tools.geom.MathToolset;
import volume.IntensityVolumeMask;
import volume.JIntensityVolumeSlicer;
import volume.PerVoxelMethod;
import volume.PerVoxelMethodUnchecked;
import volume.VoxelVolume;

public class VoxelBox extends ParameterManager implements VoxelVolume, GLPickable, Interactor {

	public enum CuttingPlane { 
		SAGITTAL,
		APEXIAL,
		CORONAL,
		TRANSVERSE,
		TRANSSAGITTAL,
//		TRANSCORONAL,
		CUT,
		FIT,
		ALL,
		THREEQUARTER
	}
	
	private ParameterManager parameterManager = new ParameterManager();
	
    private final static int SliceThickness = 1;

    private JIntensityVolumeSlicer volumeSlicer;
    
	private LinkedList<Point3i> voxelList = new LinkedList<Point3i>();

	private IntParameter vspanx;
	private IntParameter vspany;
	private IntParameter vspanz;
	
	private IntParameter voriginx;
	private IntParameter voriginy;
	private IntParameter voriginz;
	
	private DoubleParameterPoint3d rotation = new DoubleParameterPoint3d("Rotation", new Point3d(), new Point3d(-180, -180, -180), new Point3d(180, 180, 180));

	private IntParameter animateZ = new IntParameter("animate (ms)", 100, 0, 2000);

	private BooleanParameter rotate = new BooleanParameter("rotated", false);
	
	private IntParameter vspacing;

    private BooleanParameter draw = new BooleanParameter("draw", true);

    private BooleanParameter drawCOM = new BooleanParameter("COM", false);

    private BooleanParameter drawCells = new BooleanParameter("cells", false);
    
	private IntensityVolumeMask maskCustomized;

	private IntensityVolumeMask mask;

	private double normalization;

	/**
	 * 
	 */
	private int[] dimension;
	
	/**
	 * Create a new VoxelBox with default dimension (100, 100, 100) and located at the origin
	 */
	public VoxelBox() {
		this(new Vector3d(1, 1, 1), new Point3d(0, 0, 0));
	}
	
	/**
	 * Create a new VoxelBox with this dimension and centered at the origin
	 * @param d
	 */
	public VoxelBox(int[] d) {
		this(new Vector3d(d[0], d[1], d[2]), new Point3d());
	}

	public VoxelBox(Vector3d span, Point3d origin) {
		createParameters(MathToolset.tuple3dTo3i(origin), MathToolset.tuple3dTo3i(span));

		mask = new IntensityVolumeMask(getDimension());
		mask.fillWith(1);
		
		setParameterListener();
		
		normalization = 1 / Math.max(Math.max(getSpan3d().x, getSpan3d().y), getSpan3d().z);
		
		updateVoxelList();
	}

	private void setAbsolute(Vector3d span, Point3d origin) {
		createParameters(MathToolset.tuple3dTo3i(origin), MathToolset.tuple3dTo3i(span));

		setParameterListener();
		
		normalization = 1 / Math.max(Math.max(getSpan3d().x, getSpan3d().y), getSpan3d().z);
		
		updateVoxelList();
	}
	
	public VoxelBox(VoxelBox other) {
		this(other.dimension);
		this.cuttingPlane = other.cuttingPlane;

		// Copy the mask
		setMask(new IntensityVolumeMask(other.mask));
		
		// Update list of masked voxels
		computeVoxelListUpdate();
	}

	public VoxelBox(VoxelBox other, IntensityVolumeMask mask) {
		this(other.dimension);
		this.cuttingPlane = other.cuttingPlane;

		// Copy the mask
		setMask(mask);
		
		// Apply the cut
		applyCut();
		
		// Update list of masked voxels
		computeVoxelListUpdate();
	}

	public VoxelBox(IntensityVolumeMask mask) {
		this(mask.getDimension());
		setMask(mask);
		computeVoxelListUpdate();
	}

	/**
	 * Set this voxel box to have the dimensions of another.
	 * @param other
	 */
	public void set(VoxelBox other) {
		hold(true);
		
//		parameterManager = other.parameterManager;
		
		// The volume dimension is the initial span
		dimension = other.dimension; 

		// Set origin min/max
		// Leave 1 voxel out for a 1-voxel span
		voriginx.set(other.voriginx);
		voriginy.set(other.voriginy);
		voriginz.set(other.voriginz);

		// Set the initial span
		vspanx.set(other.vspanx);
		vspany.set(other.vspany);
		vspanz.set(other.vspanz);
		
		vspacing.set(other.vspacing);
		
		normalization = other.normalization;

		if (other.pmin != null) pmin = new Point3i(other.pmin);
		if (other.pmax != null) pmax = new Point3i(other.pmax);

		mask = other.mask;
		maskCustomized = new IntensityVolumeMask(mask);
		
//		mask.setVoxelBox(this);
		
		hold(false);
		
		updateVoxelList();
	}
	
	private List<Point3i> customizedSamples = new LinkedList<Point3i>();
	
	public void setCustomized(Point3i pt, Matrix4d frame, SamplingStyle samplingStyle, int samples, boolean useNaturalFrameSampling) {
		CoordinateFrameSample cfs = new CoordinateFrameSample(frame);
		cfs.presampleInteger(samples, samplingStyle, useNaturalFrameSampling);
		
		customizedSamples.clear();
		for (Point3i pts : cfs.getIntegerSampling()) {
			Point3i p = new Point3i(pt);
			p.add(pts);
			
			customizedSamples.add(p);
		}
		
		setCustomizedSampling(true);
	}
	
	public void resumeNormalSampling() {
		setCustomizedSampling(false);
	}

	public void setMask(IntensityVolumeMask mask) {
		this.mask = mask;
		maskCustomized = new IntensityVolumeMask(mask);
		
		updateVoxelList();
		
		volumeSlicer = new JIntensityVolumeSlicer(mask, new Dimension(300, 100));
	}
	
	private void createParameters(Point3i origin, Point3i span) {

		// The volume dimension is the initial span
		dimension = new int[] { span.x, span.y, span.z }; 

		// Set origin min/max
		// Leave 1 voxel out for a 1-voxel span
		voriginx = new IntParameter("x", origin.x, origin.x, dimension[0] - 1);
		voriginy = new IntParameter("y", origin.y, origin.y, dimension[1] - 1);
		voriginz = new IntParameter("z", origin.z, origin.z, dimension[2] - 1);

		// Set the initial span
		vspanx = new IntParameter("x", dimension[0], 1, dimension[0]);
		vspany = new IntParameter("y", dimension[1], 1, dimension[1]);
		vspanz = new IntParameter("z", dimension[2], 1, dimension[2]);

		// Spacing
		vspacing = new IntParameter("spacing", 1, 1, Math.min(Math.min(span.x, span.y), span.z));
	}
	
	/**
	 * @return the normalization constant to map the maximum voxel in this box to 1. 
	 */
	public double getNormalization() {
    	return normalization;
	}

	private void setParameterListener() {
		parameterManager.removeParameterListeners();
		parameterManager.removeParameters();

		parameterManager.addParameter(vspacing);
		parameterManager.addParameter(voriginx);
		parameterManager.addParameter(voriginy);
		parameterManager.addParameter(voriginz);
		parameterManager.addParameter(vspanx);
		parameterManager.addParameter(vspany);
		parameterManager.addParameter(vspanz);

		ParameterListener l = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				updateParameters(true);
			}
		};
		
		parameterManager.addParameterListener(l);
	}
	
	/**
	 * Set span limits and update the voxel array.
	 */
	private void updateParameters(boolean notifyListeners) {

		boolean previousVoxelState = voxelListUpdateBlocked;
		voxelListUpdateBlocked = true;
		
		Point3i span = getTruncatedSpan();
		
		parameterManager.hold(true);

		Point3i maxSpan = getMaxSpan();
		vspanx.setMaximum(maxSpan.x);
		vspany.setMaximum(maxSpan.y);
		vspanz.setMaximum(maxSpan.z);
		
		vspanx.setValue(span.x);
		vspany.setValue(span.y);
		vspanz.setValue(span.z);

		parameterManager.hold(false);
		
		voxelListUpdateBlocked = previousVoxelState;
		
		updateVoxelList();

		if (notifyListeners) 
			super.notifyListeners();
	}

	private void set(Point3i origin, Point3i span, boolean notify) {

		boolean previousVoxelState = voxelListUpdateBlocked;
		voxelListUpdateBlocked = true;
		
		parameterManager.hold(true);
		voriginx.setValue(origin.x);
		voriginy.setValue(origin.y);
		voriginz.setValue(origin.z);
		vspanx.setValue(span.x);
		vspany.setValue(span.y);
		vspanz.setValue(span.z);
		parameterManager.hold(false);

		voxelListUpdateBlocked = previousVoxelState;
		
		// Force update 
		updateVoxelList();
		
		updateParameters(notify);
		
		// Update the volume slice
		if (volumeSlicer != null)
			volumeSlicer.set(getOrigin(), getSpan(), false);
	}


	public void setSpan(Vector3d span) {
		setSpan(MathToolset.tuple3dTo3i(span));
		updateVoxelList();
	}

	public void setOrigin(Point3i origin) {
		set(origin, getSpan(), true);
	}

	public void setSpan(Point3i span) {
		set(getOrigin(), span, true);
	}

	private Point3d drawOrigin = new Point3d();
	private Vector3d drawSpan = new Vector3d();
	
	private GLAutoDrawable gldrawable;
	
	public void display(GLAutoDrawable drawable) {

		if (ownInteractor) {
	    	TrackBallCamera.enable(!(altDown && shiftDown));
			doPick(awtmousePoint, mouseDown, mouseButton);
		}
		
		display(drawable, false);
		
	}
	
	private void display(GLAutoDrawable drawable, boolean selectionMode) {
        gldrawable = drawable;
        
		GL2 gl = drawable.getGL().getGL2();
		
		if (drawCOM.getValue()) {
			Point3d com = getMask().getCenterOfMass(this);
			gl.glPointSize(20f);
			gl.glColor4d(0, 0, 1, 0.8);
			gl.glBegin(GL2.GL_POINTS);
			gl.glVertex3d(com.x, com.y, com.z);
			gl.glEnd();
		}

        gl.glDisable(GL2.GL_LIGHTING);
        gl.glDisable(GL2.GL_CULL_FACE);

        // Draw the bounding box
        if (draw.getValue()) {
            gl.glPushMatrix();
            
            if (rotate.getValue()) {
                CoordinateFrame frame = new CoordinateFrame(getRotation());
            	frame.applyTransform(gl);
            }

        	gl.glLineWidth(2f);
//        	gl.glColor4d(1, 0, 0, 0.5);
        	gl.glColor4d(1, 0, 0, 1);
        	
        	Point3d porigin = getOrigin3d();
        	Vector3d span = getSpan3d();
        	
        	if (selectionMode)
        		Cube.drawPickableWireframe(drawable, porigin, getSpan3d());
        	else {
        		Cube.drawWireframe(drawable, porigin, getSpan3d());
//        		Cube.drawPickableWireframe(drawable, getOrigin3d(), getSpan3d());

            	if (isPicking() && pickedName > 0 && !mouseDown) {
            		double ps = 1; 
                	gl.glColor4d(ps, ps, ps, 0.15);
                	double scale = 2 + 1e-3;
            		gl.glPushMatrix();
            		gl.glTranslated(porigin.x + span.x / scale, porigin.y + span.y / scale, porigin.z + span.z / scale);
            		gl.glScaled(span.x / scale, span.y / scale, span.z / scale);
            		Cube.drawFace(drawable, pickedName);
            		gl.glPopMatrix();
            	}
        	}

        	if (!selectionMode && isPicking()) {
    			if (!mouseDown) ScenePicker.pick(drawable, mousePoint, this);

    			if (pickedName == pickedNamePrevious && mouseDown) {
    	        	Vector3d delta = new Vector3d();
    				double s = 1;
    				double inc = -mousePointDelta.y;
    				
    				if (Math.abs(inc) > 0) {
        				switch (pickedName) {
        				case (1):
        					delta.x = s*inc;
        					break;
        				case (2):
        					delta.x = s*inc;
        					break;
        				case (3):
        					delta.z = s*inc;
        					break;
        				case (4):
        					delta.z = s*inc;
        					break;
        				case (5):
        					delta.y = s*inc;
        					break;
        				case (6):
        					delta.y = s*inc;
        					break;
        				}
        				
        				if (mouseButton == MouseEvent.BUTTON1) {
        					delta.add(getSpan3d());
        					setSpan(delta);
        				}
        				else if (mouseButton == MouseEvent.BUTTON3) {
        					delta.add(getOrigin3d());
        					setOrigin(MathToolset.tuple3dTo3i(delta));
        				}
        				
        				mousePointDelta.y = 0;
        				mousePointPrevious.x = mousePoint.x;
        				mousePointPrevious.y = mousePoint.y;
    				}
    			}
        	}

        	gl.glPopMatrix();
        }

        // Animation
        if (animating) {
        	Point3i po = getOrigin();
        	setOrigin(new Point3i(po.x, po.y, animateIndex));
        }
        
		// Draw the volume voxels
        if (drawCells.getValue()) {
        	gl.glLineWidth(1f);
//        	gl.glColor4d(1, 1, 1, 0.2);
        	gl.glColor4d(0.3, 0.3, 0.3, 0.5);
        	drawSpan.set(1, 1, 1);
        	
        	if (rotate.getValue()) {
        		voxelProcessRotated(new PerVoxelMethod() {
        			@Override
        			public boolean isValid(Point3d origin, Vector3d span) {
        				return true;
        			}
        			@Override
        			public void process(int x, int y, int z) {
        				drawOrigin.set(x, y, z);
        				Cube.drawWireframe(gldrawable, drawOrigin, drawSpan);
        			}
        		});
        	}
        	else {
        		voxelProcess(new PerVoxelMethod() {
        			@Override
        			public boolean isValid(Point3d origin, Vector3d span) {
        				return true;
        			}
        			@Override
        			public void process(int x, int y, int z) {
        				drawOrigin.set(x, y, z);
        				Cube.drawWireframe(gldrawable, drawOrigin, drawSpan);
        			}
        		});
        		
        	}
        }
	}
	

	public void setDrawCells(boolean b) {
		drawCells.setValue(b);
	}
	
	private boolean isPicking = false;
	
	private int pickedName = 0;
	private int pickedNamePrevious = 0;
	
	public void setPicking(boolean picking) {
		isPicking = picking;
	}
	
	public boolean isPicking() {
		return isPicking;
	}

	@Override
	public void renderInSelectionMode(GLAutoDrawable drawable) {
		display(drawable, true);
	}

	@Override
	public void processHit(Integer name) {
		if (name != null && !mouseDown) {
//			 System.out.println(mousePoint + ": picked name = " + name);
			pickedNamePrevious = pickedName;
			pickedName = name;
		} else {
			
			// Do nothing, keep previous valid
		}
	}

	private VerticalFlowPanel vfp;
	private CollapsiblePanel cp;
	
	public CollapsiblePanel getControls() {
		if (cp != null) {
			return cp;
		}
		
		vfp = new VerticalFlowPanel();
		
		String title = "Voxel box";
        
//		vfp.add(maskCustomized.getControls());
		JPanel pbool = new JPanel(new GridLayout(1,4));
		pbool.add(draw.getControls());
		pbool.add(drawCOM.getControls());
		pbool.add(drawCells.getControls());
		pbool.add(rotate.getControls());
		vfp.add(pbool);

		vfp.add(animateZ.getSliderControls(true, "run", false));
		
		animateZ.addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				setAnimate(animateZ.isChecked());
			}
		});

		
		CollapsiblePanel cprot = new CollapsiblePanel(rotation.getControls());
		cprot.collapse();
		vfp.add(cprot);
		
//		VerticalFlowPanel porigin = new VerticalFlowPanel();
//		porigin.setBorder(BorderFactory.createTitledBorder(BorderFactory
//                .createEtchedBorder(), "Origin", TitledBorder.LEFT, TitledBorder.CENTER));
//
//        porigin.add(voriginx.getSliderControls());
//        porigin.add(voriginy.getSliderControls());
//        porigin.add(voriginz.getSliderControls());
//        vfp.add(porigin.getPanel());
//
//		VerticalFlowPanel pspan = new VerticalFlowPanel();
//        pspan.setBorder(BorderFactory.createTitledBorder(BorderFactory
//                .createEtchedBorder(), "Span", TitledBorder.LEFT, TitledBorder.CENTER));
//
//        
//		pspan.add(vspanx.getSliderControls());
//		pspan.add(vspany.getSliderControls());
//		pspan.add(vspanz.getSliderControls());
//		pspan.add(vspacing.getSliderControls());
//		vfp.add(pspan.getPanel());
		
		if (volumeSlicer == null)
			volumeSlicer = new JIntensityVolumeSlicer(mask, new Dimension(300, 100));

		vfp.add(volumeSlicer);
		volumeSlicer.getParameterManager().addParameterListener(new ParameterListener() {
			
			@Override
			public void parameterChanged(Parameter parameter) {
				
				if (isHolding())
					return;
				
				Point3i[] c = volumeSlicer.getCoordinates();
				hold(true);
				voriginx.setValue(c[0].x);
				voriginy.setValue(c[0].y);
				voriginz.setValue(c[0].z);
				vspanx.setValue(c[1].x);
				vspany.setValue(c[1].y);
				vspanz.setValue(c[1].z);
				hold(false);
				notifyListeners();
			}
		});
		
		// Add cutting plane controls
		EnumComboBox<CuttingPlane> cuttingControls = new EnumComboBox<VoxelBox.CuttingPlane>("Cutting Plane", CuttingPlane.FIT);
//		EnumButtonControl<CuttingPlane> cuttingControls = new EnumButtonControl<CuttingPlane>("Cutting Plane", CuttingPlane.class);
		vfp.add(cuttingControls.getControls());
		
//		for (final CuttingPlane p : CuttingPlane.values()) {
//			cuttingControls.setActionListener(p, new ActionListener() {
//				
//				@Override
//				public void actionPerformed(ActionEvent e) {
//					cut(p);
//				}
//			});
//		}
		
		cp = new CollapsiblePanel(vfp.getPanel(), title);
//		cp.collapse();
		return cp;
	}
	
	private Point3i[] sliceSagittal() {
		Point3i origin = new Point3i(getDimension()[0] / 2, 0, 0); 
		Point3i span = new Point3i(SliceThickness, getDimension()[1], getDimension()[2]);
		
		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceCoronal() {
		Point3i origin = new Point3i(0, getDimension()[1] / 2, 0); 
		Point3i span = new Point3i(getDimension()[0], SliceThickness, getDimension()[2]);

		return new Point3i[] { origin, span };
	}
	
	private Point3i[] sliceSmallCut() {
		Point3i span = new Point3i(10,5, 1);
//		Point3i span = new Point3i(10,10, 1);
//		Point3i origin = new Point3i(10, 10, 51);
		Point3i origin = new Point3i(17, 49, getDimension()[2] / 2);
		
		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceApexial() {
		double zfactor = 0.7;
		Point3i span = new Point3i(getDimension()[0], getDimension()[1], MathToolset.roundInt(getDimension()[2] * zfactor));
		Point3i origin = new Point3i(0, 0, 0);
		
		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceTransverse() {
		Point3i origin = new Point3i(0, 0, MathToolset.roundInt(getDimension()[2] * 0.4)); 
		Point3i span = new Point3i(getDimension()[0], getDimension()[1], SliceThickness);

		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceCorner() {
		double s = 2.0 / 3.0;
		Point3i span = MathToolset.tuple3dTo3i(new Vector3d(vspanx.getValue() * s, vspany.getValue() * s, vspanz.getValue() * s));
		
		return new Point3i[] { getOrigin(), span };
	}
	
	private Point3i[] sliceTransSagittal() {
		Point3i origin = new Point3i(getDimension()[0] / 2, 0, getDimension()[2] / 2); 
		Point3i span = new Point3i(SliceThickness, getDimension()[1], SliceThickness);

		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceThreeQuarter() {
		Point3i origin = new Point3i(getDimension()[0] / 2, 0, getDimension()[2] / 2); 
		Point3i span = new Point3i(SliceThickness, getDimension()[1], SliceThickness);

		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceFull() {
		Point3i origin = new Point3i(0, 0, 0); 
		Point3i span = new Point3i(getDimension()[0], getDimension()[1], getDimension()[2]);

		return new Point3i[] { origin, span };
	}

	private Point3i[] sliceTransCoronal() {

		Point3i origin = new Point3i(0, getDimension()[1] / 2, getDimension()[2] / 2); 
		Point3i span = new Point3i(getDimension()[0], SliceThickness, SliceThickness);

		return new Point3i[] { origin, span };
	}
	
	public void sliceAxis(FrameAxis axis, double percentage) {
		Point3i span = getSpan();
		Point3i origin = getOrigin();
		
		// Cut-off a percentage of this axis
		if (axis == FrameAxis.X)
			span.x = MathToolset.roundInt(percentage * span.x);
		else if (axis == FrameAxis.Y)
			span.y = MathToolset.roundInt(percentage * span.y);
		else if (axis == FrameAxis.Z)
			span.z = MathToolset.roundInt(percentage * span.z);
		
		set(origin, span, false);
		
		updateVoxelList();
		
		super.notifyListeners();
	}
	
	private Point3i pmin = null;
	private Point3i pmax = null;
	
	/**
	 * Fit the voxel box to the mask.
	 */
	private Point3i[] boxToFit() {
		if (!maskCustomized.isEnabled()) return new Point3i[] { getOrigin(), getSpan() };
		
		if (pmin == null || pmax == null) {
			setMinMax();
		}
		
		Point3i origin = new Point3i(pmin.x - 1, pmin.y - 1, pmin.z - 1);
		Point3i span = new Point3i(pmax.x - pmin.x + 3, pmax.y - pmin.y + 3, pmax.z - pmin.z + 3);

		return new Point3i[] { origin, span };
	}
	
	private void setMinMax() {
		Point3i dim = maskCustomized.getDimensions();
		pmin = new Point3i(dim.x, dim.y, dim.z);
		pmax = new Point3i(0, 0, 0);
		
		for (int x = 0; x < dim.x; x++) {
			for (int y = 0; y < dim.y; y++) {
				for (int z = 0; z < dim.z; z++) {
					if (maskCustomized.getIntensity(x, y, z) != 0) {
						pmin.x = Math.min(pmin.x, x);
						pmin.y = Math.min(pmin.y, y);
						pmin.z = Math.min(pmin.z, z);

						pmax.x = Math.max(pmax.x, x);
						pmax.y = Math.max(pmax.y, y);
						pmax.z = Math.max(pmax.z, z);
					}
				}
			}
		}
	}

	private CuttingPlane cuttingPlane;
	
	public CuttingPlane getCuttingPlane() {
		return cuttingPlane;
	}
	
	public void applyCut() {
		cut(getCuttingPlane());
	}
	

	public void setSpacing(int spacing) {
		vspacing.setValue(spacing);
	}
	
	public void cut(CuttingPlane plane) {
		if (plane == null) return;
				
		boolean voxelPreviousBlocked = voxelListUpdateBlocked;
		voxelListUpdateBlocked = true;
		
		cuttingPlane = plane;

		Point3i[] coordinate = null;
		
		boolean customizedSampling = false;
		maskCustomized = new IntensityVolumeMask(mask.copy());
		
		coordinate = boxToFit();

//		System.out.println("Setting coordinates (1)");
		set(coordinate[0], coordinate[1], false);

		switch (plane) {
		case SAGITTAL:
			coordinate = sliceSagittal();
			break;

		case CORONAL:
			coordinate = sliceCoronal();
			break;
			
		case TRANSVERSE:
			coordinate = sliceTransverse();
			break;
			
//		case TRANSCORONAL:
//			coordinate = sliceTransCoronal();
//			break;
		case CUT:
			coordinate = sliceSmallCut();
			break;
		case APEXIAL:
			coordinate = sliceApexial();
			break;
		case TRANSSAGITTAL:
			coordinate = sliceTransSagittal();
			break;

		case FIT:
			coordinate = boxToFit();
			break;

		case THREEQUARTER:
			// Combine volume mask
//			maskCustomized.cutThreeQuarter();
			VoxelBox vbox = new VoxelBox(mask);
			
			maskCustomized = new IntensityVolumeMask(mask.getDimension());
			IntensityVolumeMask bmask;
			
			// Extract short axis
			vbox.cut(CuttingPlane.TRANSVERSE);
			bmask = new IntensityVolumeMask(mask);
			bmask.apply(vbox);
			maskCustomized.add(bmask);
			
			// Extract long axis (X)
//			vbox.cut(CuttingPlane.SAGITTAL);
//			bmask = new IntensityVolumeMask(mask);
//			bmask.apply(vbox);
//			maskCustomized.add(bmask);

			// Extract long axis (Y)
			vbox.cut(CuttingPlane.CORONAL);
			bmask = new IntensityVolumeMask(mask);
			bmask.apply(vbox);
			maskCustomized.add(bmask);

			customizedSampling = true;
			break;

		case ALL:
			coordinate = sliceFull();
			break;
		
		default:
			cut(CuttingPlane.FIT);
		}
		
//		System.out.println("Setting coordinates (2)");
		if (!customizedSampling) {
			set(coordinate[0], coordinate[1], false);
		}
		
		voxelListUpdateBlocked = voxelPreviousBlocked;
		updateVoxelList();

//		System.out.println("Notifying listeners");
		super.notifyListeners();
	}
	
	private boolean voxelListUpdateBlocked = false;
	
	private boolean voxelListUpdateRequested = false;
	
	public void updateVoxelList() {
		if (voxelListUpdateBlocked) return;
		
		voxelListUpdateRequested = true;
	}
	
	/**
	 * Update the voxel list with updated mask and coordinates.
	 */
	public void computeVoxelListUpdate() {
		if (!voxelListUpdateRequested) return;
		
		voxelListUpdateRequested = false;

		// Wait until we are done processing
		while (voxelIsProcessing) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		voxelIsProcessing = true;

		Point3d origin = getOrigin3d();
		
		// Use a truncated span in case it hasn't been updated yet.
		Point3i span = getTruncatedSpan();
		
		voxelList.clear();
		
//		System.out.println("Updating voxel list...");
		
		if (maskCustomized == null || !maskCustomized.isEnabled()) {
			double mx = origin.x + span.x;
			double my = origin.y + span.y;
			double mz = origin.z + span.z;
			int spacing = vspacing.getValue();
			for (int x = (int) origin.x; x < mx; x+= spacing)
				for (int y = (int) origin.y; y < my; y+=spacing)
					for (int z = (int) origin.z; z < mz; z+=spacing)
						voxelList.add(new Point3i(x, y, z));
		}
		else {
			double mx = origin.x + span.x;
			double my = origin.y + span.y;
			double mz = origin.z + span.z;
			int spacing = vspacing.getValue();
			for (int x = (int) origin.x; x < mx; x+= spacing)
				for (int y = (int) origin.y; y < my; y+=spacing)
					for (int z = (int) origin.z; z < mz; z+=spacing)
						if (!isOutside(x, y, z)) {
							voxelList.add(new Point3i(x, y, z));
						}
		}
		
		voxelIsProcessing = false;
	}
	
	public Point3d getOrigin3d() {
		return new Point3d(voriginx.getValue(), voriginy.getValue(), voriginz.getValue());
	}
	
	public Vector3d getSpan3d() {
		return new Vector3d(vspanx.getValue(), vspany.getValue(), vspanz.getValue());
	}

	public Point3i getOrigin() {
		return new Point3i(voriginx.getValue(), voriginy.getValue(), voriginz.getValue());
	}

	public Point3i getSpan() {
		return new Point3i(vspanx.getValue(), vspany.getValue(), vspanz.getValue());
	}

	/**
	 * @return the true volume span lying within the volume bounds.
	 */
	private Point3i getTruncatedSpan() {
		
		Point3i span = getSpan();
		Point3i maxspan = getMaxSpan();
		
		if (span.x >= maxspan.x)
			span.x = maxspan.x;
		if (span.y >= maxspan.y)
			span.y = maxspan.y;
		if (span.z >= maxspan.z)
			span.z = maxspan.z;
		
		return span;
	}
	
	private Point3i getMaxSpan() {
		Point3i origin = getOrigin();
//		return new Point3i(dimensionSpan[0] - origin.x - 1, dimensionSpan[1] - origin.y - 1, dimensionSpan[2] - origin.z - 1);
		return new Point3i(dimension[0] - origin.x, dimension[1] - origin.y, dimension[2] - origin.z);
	}
	
	private boolean voxelIsProcessing = false;

	/**
	 * Process the neighborhood at a point p
	 * @param perVoxelMethodUnchecked
	 */
	public void neighborhoodProcess(Point3i p, NeighborhoodShape shape, int neighborhoodRadius, PerVoxelMethodUnchecked perVoxelMethod) {
		List<Point3i> neighbors = VolumeSampler.sample(shape, neighborhoodRadius);
		for (Point3i n : neighbors) {
			if (!isMaskedUnbounded(p.x + n.x, p.y + n.y, p.z + n.z))
				perVoxelMethod.process(p.x + n.x, p.y + n.y, p.z + n.z);
		}
	}

	/**
	 * Process the neighborhood at a point p
	 * @param perVoxelMethodUnchecked
	 */
	public void neighborhoodProcess(Point3i p, List<Point3i> tentativeNeighbors, PerVoxelMethodUnchecked perVoxelMethod) {
		for (Point3i n : tentativeNeighbors) {
			if (!isMaskedUnbounded(p.x + n.x, p.y + n.y, p.z + n.z))
				perVoxelMethod.process(p.x + n.x, p.y + n.y, p.z + n.z);
		}
	}

	/**
	 * Run an operation on each voxel contained in this box
	 * @param pvm
	 */
	@Override
	public void voxelProcess(PerVoxelMethod pvm) {

		while (voxelIsProcessing) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		voxelIsProcessing = true;
		
		if (rotate.getValue()) {
			voxelProcessRotated(pvm);
			return;
		}
		
		if (customizedSampling) {
			voxelProcessedCustomized(pvm);
			return;
		}
		
		Point3d origin = getOrigin3d();
		
		// Use a truncated span in case it hasn't been updated yet.
		Point3i span = getTruncatedSpan();
		
		if (!pvm.isValid(origin, MathToolset.tuple3iToVector3d(span))) return;
		
		if (maskCustomized != null && !maskCustomized.isEnabled()) {
			double mx = origin.x + span.x;
			double my = origin.y + span.y;
			double mz = origin.z + span.z;
			int spacing = vspacing.getValue();
			for (int x = (int) origin.x; x < mx; x+= spacing)
				for (int y = (int) origin.y; y < my; y+=spacing)
					for (int z = (int) origin.z; z < mz; z+=spacing)
						pvm.process(x, y, z);
		}
		else {
//			double mx = origin.x + span.x;
//			double my = origin.y + span.y;
//			double mz = origin.z + span.z;
//			int spacing = vspacing.getValue();
//			for (int x = (int) origin.x; x < mx; x+= spacing)
//				for (int y = (int) origin.y; y < my; y+=spacing)
//					for (int z = (int) origin.z; z < mz; z+=spacing)
//						if (!isMasked(x, y, z)) {
//							pvm.process(x, y, z);
//						}
			if (voxelListUpdateRequested) {
				voxelIsProcessing = false;
				computeVoxelListUpdate();
			}
			voxelIsProcessing = true;
			for (Point3i p : voxelList) {
				pvm.process(p.x, p.y, p.z); 
			}
			voxelIsProcessing = false;
		}
	}

	private void voxelProcessedCustomized(PerVoxelMethod pvm) {
		for (Point3i p : customizedSamples) {
			pvm.process(p.x, p.y, p.z);
		}
	}
	
	private Matrix4d getRotation() {
		Point3d center = getCenter();

		Matrix4d full = new Matrix4d();
		full.setIdentity();
		Matrix4d r = new Matrix4d();
		// T
		r.setIdentity();
		r.setColumn(3, center.x, center.y, center.z, 1);
		full.mul(r);

		// X rotation
		r.set(new AxisAngle4d(new Vector3d(1, 0, 0), rotation.getPoint3d().x / 180d * Math.PI));
		full.mul(r);

		// Y rotation
		r.set(new AxisAngle4d(new Vector3d(0, 1, 0), rotation.getPoint3d().y / 180d * Math.PI));
		full.mul(r);

		// Z rotation
		r.set(new AxisAngle4d(new Vector3d(0, 0, 1), rotation.getPoint3d().z / 180d * Math.PI));
		full.mul(r);
		
		// T-1
		r.setIdentity();
		r.setColumn(3, -center.x, -center.y, -center.z, 1);
		full.mul(r);
		
		return full;
	}
	
	private void voxelProcessRotated(PerVoxelMethod pvm) {
		Point3d origin = getOrigin3d();
		
		// Use a truncated span in case it hasn't been updated yet.
		Point3i span = getTruncatedSpan();
		
		if (!pvm.isValid(origin, MathToolset.tuple3iToVector3d(span))) return;

		Matrix4d full = getRotation();
		
		Point3d p = new Point3d();
		Point3i pi = new Point3i();
		
		double mx = origin.x + span.x;
		double my = origin.y + span.y;
		double mz = origin.z + span.z;
		int spacing = vspacing.getValue();
		for (int x = (int) origin.x; x < mx; x+= spacing)
			for (int y = (int) origin.y; y < my; y+=spacing)
				for (int z = (int) origin.z; z < mz; z+=spacing) {
					// Transform the coordinate
					p.set(x, y, z);
					full.transform(p);
					
					pi.x = MathToolset.roundInt(p.x);
					pi.y = MathToolset.roundInt(p.y);
					pi.z = MathToolset.roundInt(p.z);
					
					if (!isOutside(x, y, z) && dimensionContains(pi)) {
						pvm.process(pi.x, pi.y, pi.z);
//						pvm.process(x, y, z);
					}
					
				}
	}
	
	private boolean customizedSampling = false;
	
	private boolean isCustomized() {
		return customizedSampling;
	}
	
	private void setCustomizedSampling(boolean b) {
		customizedSampling = b;
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return whether this voxel is hidden by the mask.
	 */
	public boolean isOutside(int x, int y, int z) {
//		if (!mask.isEnabled()) return false;
		
		return maskCustomized.isMasked(x, y, z);
	}
	
	public boolean isMasked(double x, double y, double z) {
		return isOutside(MathToolset.roundInt(x), MathToolset.roundInt(y), MathToolset.roundInt(z));
	}

	public boolean isMaskedUnbounded(int x, int y, int z) {
		// First make sure this voxel lies within the full volume
		if (x >= dimension[0] || y >= dimension[1] || z >= dimension[2]) {
			return true;
		}
		else if (x < 0 || y < 0 || z < 0) {
			return true;
		}
		else {
			return maskCustomized.isMasked(x, y, z);
		}
	}

	public Point3d getCenter() {
		Point3d p = new Point3d();
		p.set(vspanx.getValue(), vspany.getValue(), vspanz.getValue());
//		p.set(MathToolset.tuple3iToPoint3d(getMaxSpan()));
		p.scale(0.5);
		p.x += voriginx.getValue();
		p.y += voriginy.getValue();
		p.z += voriginz.getValue();
		
		return p;
	}
	
	public Point3d getFullVolumeCenter() {
		Point3d p = new Point3d();
//		p.set(MathToolset.tuple3iToPoint3d(getMaxSpan()));
		p.set(MathToolset.tuple3iToPoint3d(new Point3i(getDimension())));
		p.scale(0.5);
		
		return p;
	}


	public Point3i getMinimum() {
		Point3d min = getOrigin3d();
		return new Point3i((int) min.x, (int) min.y, (int) min.z);
	}

	public Point3i getMaximum() {
		Point3d max = getOrigin3d();
		max.add(getSpan3d());
		
		return new Point3i((int) max.x, (int) max.y, (int) max.z);
	}

	public List<Point3i> getVolumeUnchecked() {
		return voxelList;
	}
	
	public List<Point3i> getVolume() {
		voxelListUpdateRequested = true;
		computeVoxelListUpdate();
		
		final List<Point3i> pts = new LinkedList<Point3i>();
		for (Point3i pt : voxelList) {
			pts.add(new Point3i(pt));
		}
//		// Construct all frames
//		voxelProcess(new PerVoxelMethod() {
//			@Override
//			public boolean isValid(Point3d origin, Vector3d span) {
//				return true;
//			}
//
//			@Override
//			public void process(int x, int y, int z) {
//				pts.add(new Point3i(x, y, z));
//			}
//		});
//		Point3i origin = getOrigin();
//		Point3i span = getSpan();
//		
//		double mx = origin.x + span.x;
//		double my = origin.y + span.y;
//		double mz = origin.z + span.z;
//		int spacing = vspacing.getValue();
//		for (int x = (int) origin.x; x < mx; x+= spacing)
//			for (int y = (int) origin.y; y < my; y+=spacing)
//				for (int z = (int) origin.z; z < mz; z+=spacing)
//					if (!isMasked(x, y, z)) {
//						Point3i pt = new Point3i(x, y , z);
//						System.out.println(pt);
//						pts.add(pt);
//					}
		
		return pts;
	}

	public IntensityVolumeMask getMask() {
		return maskCustomized;
	}

	@Override
	public int[] getDimension() {
		return dimension;
	}

	public void setVisible(boolean b) {
		draw.setValue(b);
	}

	public void applyViewNormalization(GL2 gl) {
		double n = getNormalization();
		gl.glScaled(n, n, n);
	}
	
	public void applyViewNormalizedTranslation(GL2 gl) {
        int[] dim = getDimension();
        Point3d d = new Point3d(dim[0], dim[1], dim[2]);
    	d.scale(0.5 * getNormalization());

        gl.glTranslated(-d.x, -d.y, -d.z);

        applyViewNormalization(gl);

	}

	/**
	 * Place the voxel box at the origin with a max side length of 1
	 * @param gl
	 */
	public void applyViewTransformation(GL2 gl) {
//        Point3d pcenter = getCenter();
        Point3d pcenter = getFullVolumeCenter();
        pcenter.negate();
        
        int maxdim = getMaxDimension();;
        gl.glScaled(1d / maxdim, 1d / maxdim, 1d / maxdim);
        gl.glTranslated(pcenter.x, pcenter.y, pcenter.z);
        
	}
	
	public int getMaxDimension() {
        int[] dim = getDimension();
        return Math.max(Math.max(dim[0], dim[1]), dim[2]);
	}

	/**
	 * @param p
	 * @return whether p is located within the voxel volume
	 */
	public boolean contains(Point3i p) {
		if (p.x < voriginx.getValue() || p.y < voriginy.getValue() || p.z < voriginz.getValue()) 
			return false;
		
		if (p.x >= voriginx.getValue() + vspanx.getValue() || p.y >= voriginy.getValue()  + vspany.getValue() || p.z >= voriginz.getValue()  + vspanz.getValue())
			return false;
		
		return true;
	}

	public boolean dimensionContains(int[] p) {
		if (p[0] < 0 || p[1] < 0 || p[2] < 0) 
			return false;
		
		if (p[0] >= getDimension()[0] || p[1] >= getDimension()[1] || p[2] >= getDimension()[2])
			return false;
		
		return true;
	}

	public boolean dimensionContains(int x, int y, int z) {
		if (x < 0 || y < 0 || z < 0) 
			return false;
		
		if (x >= getDimension()[0] || y >= getDimension()[1] || z >= getDimension()[2])
			return false;
		
		return true;
	}

	/**
	 * @param x,y,z
	 * @return whether (x,y,z) is located within the voxel volume
	 */
	public boolean contains(double x, double y, double z) {
		if (x < voriginx.getValue() || y < voriginy.getValue() || z < voriginz.getValue()) 
			return false;
		
		if (x >= voriginx.getValue() + vspanx.getValue() || y >= voriginy.getValue()  + vspany.getValue() || z >= voriginz.getValue()  + vspanz.getValue())
			return false;
		
		return true;
	}

	/**
	 * @param x,y,z
	 * @return whether (x,y,z) is located within the voxel volume
	 */
	public boolean contains(Point3d p) {
		if (p.x < voriginx.getValue() || p.y < voriginy.getValue() || p.z < voriginz.getValue()) 
			return false;
		
		if (p.x >= voriginx.getValue() + vspanx.getValue() || p.y >= voriginy.getValue()  + vspany.getValue() || p.z >= voriginz.getValue()  + vspanz.getValue())
			return false;
		
		return true;
	}

	/**
	 * @param p
	 * @return whether p is located within the voxel volume
	 */
	public boolean dimensionContains(Point3i p) {
		if (p.x < 0 || p.y < 0 || p.z < 0) 
			return false;
		
		if (p.x >= getDimension()[0] || p.y >= getDimension()[1] || p.z >= getDimension()[2])
			return false;
		
		return true;
	}
	
	public double getSpanX() {
		return vspanx.getValue();
	}
	public double getSpanY() {
		return vspany.getValue();
	}
	public double getSpanZ() {
		return vspanz.getValue();
	}
	
	public void doPick(Point p, boolean mouseDown, int mouseButton) {
		this.mouseDown = mouseDown;
		this.mouseButton = mouseButton;
		
		if (mousePoint == null) 
			mousePointPrevious = new Point(p);
		else
			mousePointPrevious = new Point(mousePoint);
			
		mousePoint = p;
		
		mousePointDelta.x = mousePoint.x - mousePointPrevious.x;
		mousePointDelta.y = mousePoint.y - mousePointPrevious.y;
	}
	
	private boolean altDown = false;
	private boolean shiftDown = false;
	private boolean mouseDown = false;
	private boolean mouseLeftDown = false;
	private Point mousePoint = new Point();
	private Point mousePointPrevious = new Point();
	private Point mousePointDelta = new Point();
	private int mouseButton = 0;
	private boolean ownInteractor;
	private Point awtmousePoint = new Point();
	private KeyAdapter ka;
	private MouseMotionAdapter mml;
	private MouseListener ml;
	private Component component;

	@Override
	public void detach(Component component) {
		component.removeKeyListener(ka);
		component.removeMouseMotionListener(mml);
		component.removeMouseListener(ml);
		this.component = null;
	}

	@Override
	public void attach(Component component) {
		
		ownInteractor = true;
		
		createInteractorsIfNecessary();

		this.component = component; 
		component.addKeyListener(ka);
		component.addMouseMotionListener(mml);
		component.addMouseListener(ml);
	}
	
	public void attachInteractor(Interactor other) {
		if (component != null) {
			other.attach(component);
		}
	}
	
	private void createInteractorsIfNecessary() {
		if (ka == null) {
			ka = new KeyAdapter() {

				@Override
				public void keyReleased(KeyEvent e) {
					setPicking(e.isShiftDown() && e.isAltDown());

					shiftDown = e.isShiftDown();
					altDown = e.isAltDown();
				}
				
				@Override
				public void keyPressed(KeyEvent e) {

					setPicking(e.isShiftDown() && e.isAltDown());

			 		shiftDown = e.isShiftDown();
					altDown = e.isAltDown();
					
					
					if (e.getKeyCode() == KeyEvent.VK_F) {
						//
					}
					else if (e.getKeyCode() == KeyEvent.VK_1) {
						cut(CuttingPlane.FIT);
					}
					else if (e.getKeyCode() == KeyEvent.VK_2) {
						cut(CuttingPlane.TRANSVERSE);
					}
					else if (e.getKeyCode() == KeyEvent.VK_3) {
						cut(CuttingPlane.CORONAL);
					}
					else if (e.getKeyCode() == KeyEvent.VK_4) {
						cut(CuttingPlane.SAGITTAL);
					}
					else if (e.getKeyCode() == KeyEvent.VK_5) {
						cut(CuttingPlane.CUT);
					}
					else if (e.getKeyCode() == KeyEvent.VK_6) {
						cut(CuttingPlane.ALL);
					}
					else if (e.getKeyCode() == KeyEvent.VK_7) {
						cut(CuttingPlane.THREEQUARTER);
					}
					else if (e.getKeyCode() == KeyEvent.VK_8) {
						cut(CuttingPlane.THREEQUARTER);
						int oz = MathToolset.roundInt(getDimension()[2] * 0.4);
						setOrigin(new Point3i(0, 1, oz));
						setSpan(new Point3i(getDimension()[0], getDimension()[1] / 2, 13));
					}
				}
			};
		}
		if (mml == null) {
			mml = new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent e) {
					awtmousePoint = e.getPoint();
				}
				
				@Override
				public void mouseDragged(MouseEvent e) {
					awtmousePoint = e.getPoint();
				}
			};
		}
			
		if (ml == null) {
			ml = new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					mouseDown = false;
					mouseButton = e.getButton();
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					mouseDown = true;				
					mouseButton = e.getButton();
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			};
		}
	}
	
	private boolean animating = false;
	private int animateIndex = 0;
	private int animateInitialIndex = 0;
	private Point3i animateInitialSpan;
	private Thread animationThread = null;
	private boolean animationStopRequest = false;
	
	private void setAnimate(boolean animate) {
		
		// Only process animation halting for now...
		if (!animate)
			animating = false;
		
		// Try to stop previous thread
		if (!animating && animationThread != null && animationThread.isAlive()) {
			animationStopRequest = true;
			
			// Wait for thread to terminate
			while (animationThread.isAlive()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		if (animate && animationThread != null && animationThread.isAlive()) {
			// Do nothing, the thread is already running
			return;
		}
		else {
//			animateInitialIndex = getOrigin().z;
//			animateInitialSpan = getSpan();
			cut(CuttingPlane.FIT);
			animateInitialIndex = getOrigin().z;
			animateInitialSpan = getSpan();
			
			setSpan(new Point3i(animateInitialSpan.x, animateInitialSpan.y, 1));
			
			animateIndex = animateInitialIndex;
			animationStopRequest = false;
			
			animationThread = new Thread(new Runnable() {
				
				@Override
				public void run() {

					while (!animationStopRequest) {
						try {
							Thread.sleep(animateZ.getValue());
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if (animateIndex > animateInitialIndex + animateInitialSpan.z) {
							animateIndex = animateInitialIndex;
						}
						animateIndex++;
					}
				}
			});

			animating = animate;

			animationThread.start();
		}
	}

	/**
	 * TODO: change the name of this method
	 * @param gl
	 */
	public void applyHeartNormalization(GL2 gl) {
        Point3d pcenter = getCenter();
        pcenter.negate();
        Point3i span = getSpan();
        double md = Math.max(Math.max(span.x, span.y), span.z);
        gl.glScaled(1d / md, 1d / md, 1d / md);
        gl.glTranslated(pcenter.x, pcenter.y, pcenter.z);
	}

	public Vector3d[] getNormalizedBounds() {
		Point3d origin = getOrigin3d();
		Vector3d span = getSpan3d();
		int[] dims = getDimension();
		
		span.x = span.x / dims[0];
		span.y = span.y / dims[1];
		span.z = span.z / dims[2];
		
		origin.x = origin.x / dims[0];
		origin.y = origin.y / dims[1];
		origin.z = origin.z / dims[2];
		
		return new Vector3d[] { new Vector3d(origin), span } ;
	}
	
	@Override
	public String toString() {
		String s = "";
		s += "Full volume dimension = " + Arrays.toString(dimension) + "\n"; 
		s += "voxel spacing = " + vspacing.getValue() + "\n"; 		
		s += "Cutting plane = " + getCuttingPlane().toString() + "\n";
		s += "Origin = " + getOrigin().toString() + "\n";
		s += "Span = " + getSpan().toString() + "\n";
		return s;
	}

	public int countVoxels() {
		computeVoxelListUpdate();
		return voxelList.size();
	}

	public void destroy() {
		voxelList.clear();
		volumeSlicer = null;
		mask.destroy();
		maskCustomized.destroy();
	}

	public void setRegion(VoxelBox other) {
		set(other.getOrigin(), other.getSpan(), false);
	}

	public void set(NeighborhoodShape shape) {
		int sliceThickness = 1;
		boolean notify = true;
		
		switch (shape) {
		case Isotropic:
			set(new Point3i(0, 0, 0), new Point3i(dimension[0], dimension[1], dimension[2]), notify);
			break;
		case IsotropicXY:
			set(new Point3i(0, 0, dimension[2] / 2), new Point3i(dimension[0], dimension[1], sliceThickness), notify);
			break;
		case IsotropicXZ:
			set(new Point3i(0, dimension[1] / 2, 0), new Point3i(dimension[0], sliceThickness, dimension[1]), notify);
			break;
		case IsotropicYZ:
			set(new Point3i(dimension[0] / 2, 0, 0), new Point3i(sliceThickness, dimension[1], dimension[2]), notify);
			break;
		case HemisphericalBackwards:
			set(new Point3i(0, 0, dimension[2] / 2), new Point3i(dimension[0], dimension[1], sliceThickness), notify);
			break;
		case HemisphericalForward:
			set(new Point3i(0, 0, dimension[2] / 2), new Point3i(dimension[0], dimension[1], sliceThickness), notify);
			break;
		case SixNeighbors:
			set(new Point3i(0, 0, dimension[2] / 2), new Point3i(dimension[0], dimension[1], sliceThickness), notify);
			break;
		default:
			break;
		}
	}
}


