package voxel;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class VoxelOrientation extends Voxel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3789407675891482015L;

	/**
	 * The orientation of this voxel.
	 */
	public Vector3d v = new Vector3d();
	
	public VoxelOrientation(int x, int y, int z, Vector3d v) {
		super(x, y, z);
		
		this.v.set(v);
	}

	public VoxelOrientation(Point3d p, Vector3d v) {
		this((int) Math.round(p.x), (int) Math.round(p.y), (int) Math.round(p.z), v);
	}
}
