package voxel;

import javax.vecmath.Point3d;

/**
 * Class representing regular voxels.
 * TODO: add neighbor functionality
 * @author piuze
 *
 */
public class Voxel extends Point3d {
	/**
	 * Automatically generated serial ID.
	 */
	private static final long serialVersionUID = -8785713988035563740L;

	/**
	 * The default span for a voxel (1mm)
	 */
	private static final double DEFAULT_SPAN = 1e-3;
	
	/**
	 * The span of this voxel, in meters.
	 */
	private double span;
	
	/**
	 * Create a new voxel using the default span (1mm). 
	 */
	public Voxel() {
		span = DEFAULT_SPAN;
	}
	
	public Voxel(double span) {
		this.span = span;
	}
	
	public Voxel(int x, int y, int z) {
		super(x,y,z);
	}

	/**
	 * @return the span of this voxel, in meters.
	 */
	public double getSpan() {
		return span;
	}
}
