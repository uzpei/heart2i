package voxel;

import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.IntParameterPoint3i;

public class VoxelTracer {
	
	private IntParameterPoint3i p0;
	private IntParameterPoint3i p1;
//	private DoubleParameter centers = new DoubleParameter("centerline", 0.5, 0, 1);
//	private Point3d c0, c1;
	private VoxelBox box;
	
	public VoxelTracer(VoxelBox box, Point3i origin, Point3i end) {
		this.box = box;
		
		p0 = new IntParameterPoint3i("ray origin", origin, box.getMinimum(), box.getMaximum());
		p1 = new IntParameterPoint3i("ray end", end, box.getMinimum(), box.getMaximum());
		
		update(origin, end);
	}
	
	public void update(Point3i origin, Point3i end) {
		p0.set(origin, box.getMinimum(), box.getMaximum());
		p1.set(end, box.getMinimum(), box.getMaximum());
	}
	
	public void updateBox() {
		p0.setBounds(box.getMinimum(), box.getMaximum());
		p1.setBounds(box.getMinimum(), box.getMaximum());
	}

	public List<Point3i> intersect() {

		List<Point3i> list = new LinkedList<Point3i>();

		// Origin and end of ray
		Point3d pt0 = new Point3d(p0.getPoint3i().x, p0.getPoint3i().y, p0.getPoint3i().z);
		Point3d pt1 = new Point3d(p1.getPoint3i().x, p1.getPoint3i().y, p1.getPoint3i().z);
		
		// Direction of ray
		Vector3d v = new Vector3d();
		v.sub(pt1, pt0);
		
		Point3d p = new Point3d();
		double dt = 1 / Math.ceil(v.length());
		Point3i currentp = new Point3i();
		Point3i previousp = new Point3i();
		for (double t = 0; t <= 1; t += dt) {
			p.scaleAdd(t, v, pt0);
			
			// Get the current voxel
			currentp.set((int) p.x, (int) p.y, (int) p.z);
			
			if (box.isMasked(p.x, p.y, p.z)) continue;

			
			// If this is a new voxel, add it
			if (!previousp.equals(currentp)) {
				list.add(new Point3i(currentp));
				
				previousp.set(currentp);
			}
		}
		
		return list;
	}
	
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glDisable(GL2.GL_LIGHTING);

		// Draw ray
		Point3d pt0 = new Point3d(p0.getPoint3i().x, p0.getPoint3i().y, p0.getPoint3i().z);
		Point3d pt1 = new Point3d(p1.getPoint3i().x, p1.getPoint3i().y, p1.getPoint3i().z);
		
		gl.glLineWidth(2.0f);
		gl.glColor4d(1, 1, 0, 0.7);
		
		gl.glEnable(GL2.GL_LINE_STIPPLE);
		gl.glLineStipple(1, (short) 0xf0f0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex3d(pt0.x, pt0.y, pt0.z);
		gl.glVertex3d(pt1.x, pt1.y, pt1.z);
		gl.glEnd();
		gl.glDisable(GL2.GL_LINE_STIPPLE);
		

		// Draw origin of ray
		gl.glColor4d(0.1, 0.1, 0.1, 0.7);
		gl.glPointSize(10f);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex3d(pt0.x, pt0.y, pt0.z);
		gl.glEnd();
		
		// Display intersection
		List<Point3i> intersect = intersect();
		gl.glPointSize(5f);
		gl.glColor4d(0, 1, 0, 0.7);
		gl.glBegin(GL2.GL_POINTS);
//		System.out.println("---");
		for (Point3i p : intersect) { 
//			System.out.println(p);
			gl.glVertex3d(p.x, p.y, p.z);
		}
		gl.glEnd();
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Voxel raytracer"));
		
		vfp.add(p0.getControls());
		vfp.add(p1.getControls());

        CollapsiblePanel cp = new CollapsiblePanel(vfp.getPanel(), "Voxel raytracer");
        cp.collapse();
		return cp;
	}

	public Vector3d getDirection() {
		// Origin and end of ray
		Point3d pt0 = new Point3d(p0.getPoint3i().x, p0.getPoint3i().y, p0.getPoint3i().z);
		Point3d pt1 = new Point3d(p1.getPoint3i().x, p1.getPoint3i().y, p1.getPoint3i().z);
		
		// Direction of ray
		Vector3d v = new Vector3d();
		v.sub(pt1, pt0);
		
		return v;
	}

	public Point3d getOrigin() {
		return new Point3d(p0.getPoint3i().x, p0.getPoint3i().y, p0.getPoint3i().z);
	}
	public Point3d getTarget() {
		return new Point3d(p1.getPoint3i().x, p1.getPoint3i().y, p1.getPoint3i().z);
	}

	public void update(Point3d origin, Point3d target) {
		update(toPoint3i(origin), toPoint3i(target));
	}
	
	private Point3i toPoint3i(Point3d p) {
		return new Point3i(rint(p.x), rint(p.y), rint(p.z));
	}
	
	private int rint(double d) {
		return (int) Math.round(d);
	}
}
