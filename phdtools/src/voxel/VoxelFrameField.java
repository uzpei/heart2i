package voxel;

import gl.geometry.GLGeometry;
import heart.Heart;
import heart.Heart.ImageType;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import swing.component.CollapsiblePanel;
import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame;
import tools.frame.VolumeSampler;
import tools.frame.OrientationFrame.FrameAxis;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import volume.PerVoxelMethod;
import volume.PerVoxelMethodUnchecked;
import volume.VoxelVolume;
import diffgeom.DifferentialOneForm.MovingAxis;
import diffgeom.fitting.experiment.CartanFittingParameter.NeighborhoodShape;
import extension.matlab.MatlabIO;

public class VoxelFrameField implements VoxelVolume, GLGeometry {
	public static boolean verbose = false;
	
	/**
	 * x axes of the field.
	 */
	private VoxelVectorField f1;

	/**
	 * y axes of the field.
	 */
	private VoxelVectorField f2;

	/**
	 * z axes of the field.
	 */
	private VoxelVectorField f3;
	
//	private FramedVoxel[][][] frameField;
	
	private IntensityVolumeMask mask;
	
	private int[] dim;
	
	private BooleanParameter draw = new BooleanParameter("draw", true);
	private BooleanParameter drawHelix = new BooleanParameter("helix coloring", false);

	private BooleanParameter drawTip = new BooleanParameter("draw tip", false);
	private BooleanParameter highDef = new BooleanParameter("high def", false);

	private DoubleParameter drawRadius = new DoubleParameter("radius", 0.1, 0, 0.5); 
	private IntParameter cylResolution = new IntParameter("res", 16, 1, 32);
	
	private boolean[] drawState = new boolean[4];
	
	public VoxelFrameField(VoxelVectorField x, VoxelVectorField y, VoxelVectorField z) {
		this(x, y, z, null);
	}
	
	/**
	 * Create a new FrameField from these images.
	 * @param x
	 * @param y
	 * @param z
	 */
	public VoxelFrameField(VoxelVectorField x, VoxelVectorField y, VoxelVectorField z, IntensityVolumeMask mask) {
		f1 = x;
		f2 = y;
		f3 = z;
		
		x.setTitle("f1");
		y.setTitle("f2");
		z.setTitle("f3");
		
		f1.setColor(new double[] { 1, 0, 0 });
		f2.setColor(new double[] { 0, 1, 0 });
		f3.setColor(new double[] { 0, 0, 1 });
	
		this.mask = mask;
		
		Point3i dx = f1.getDimension();
		Point3i dy = f2.getDimension();
		Point3i dz = f3.getDimension();
		if (!dx.equals(dy) || !dx.equals(dz) || !dy.equals(dz)) {
			throw new RuntimeException("Incompatible dimensions in initializing frame field.");
		}
		dim = new int[] { dx.x, dx.y, dx.z };
		
		draw.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				if (getF1().isVisible()) drawState[0] = getF1().isVisible();
				if (getF2().isVisible()) drawState[1] = getF2().isVisible();
				if (getF3().isVisible()) drawState[2] = getF3().isVisible();
				
				getF1().setVisible(draw.getValue() && drawState[0]);
				getF2().setVisible(draw.getValue() && drawState[1]);
				getF3().setVisible(draw.getValue() && drawState[2]);
			}	
		});
		
		drawHelix.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				getF1().setHelixColoring(drawHelix.getValue());
//				getFieldY().setHelixColoring(drawHelix.getValue());
//				getFieldZ().setHelixColoring(drawHelix.getValue());
			}	
		});

		highDef.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				getF1().setHighDef(highDef.getValue());
				getF2().setHighDef(highDef.getValue());
				getF3().setHighDef(highDef.getValue());
			}	
		});

		drawRadius.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				getF1().setDrawRadius(drawRadius.getValue());
				getF2().setDrawRadius(drawRadius.getValue());
				getF3().setDrawRadius(drawRadius.getValue());
			}	
		});

		cylResolution.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				getF1().setDrawResolution(cylResolution.getValue());
				getF2().setDrawResolution(cylResolution.getValue());
				getF3().setDrawResolution(cylResolution.getValue());
			}	
		});

		drawTip.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				getF1().setDrawTip(drawTip.getValue());
				getF2().setDrawTip(drawTip.getValue());
				getF3().setDrawTip(drawTip.getValue());
			}	
		});


		// Only draw first vector by default
		if (getF2() != null && getF3() != null) {
			getF2().setVisible(false);
			getF3().setVisible(false);
		}
		
		createFrameField();
	};

	public VoxelFrameField(VoxelFrameField other, VoxelBox box) {
		f1 = new VoxelVectorField(other.f1, box);
		f2 = new VoxelVectorField(other.f2, box);
		f3 = new VoxelVectorField(other.f3, box);
				
		this.mask = new IntensityVolumeMask(other.mask);
		
		dim = Arrays.copyOf(other.dim, other.dim.length);
		
		draw.addParameterListener(new ParameterListener() {
		@Override
			public void parameterChanged(Parameter parameter) {
				getF1().setVisible(draw.getValue());
				getF2().setVisible(draw.getValue());
				getF3().setVisible(draw.getValue());
			}	
		});
		
		createFrameField();
	}
	
	public VoxelFrameField(int[] dims, IntensityVolumeMask mask) {
		this(new VoxelVectorField("f1", new IntensityVolume(dims), new IntensityVolume(dims), new IntensityVolume(dims), new VoxelBox(dims)), new VoxelVectorField("f2", new IntensityVolume(dims), new IntensityVolume(dims), new IntensityVolume(dims), new VoxelBox(dims)), new VoxelVectorField("f3", new IntensityVolume(dims), new IntensityVolume(dims), new IntensityVolume(dims), new VoxelBox(dims)), mask);
	}
	
	public VoxelFrameField(int[] dims) {
		this(dims, null);
	}

	public void enforceCylindricalConsistency(Point3d center) {
		VoxelFrameField.enforceCylindricalConsistency(getF1(), getF2(), getF3(), mask, center);
	}
	
	/**
	 * Assume a midline going through center and running from z=-0.5 to z=0.5
	 * @param ET
	 * @param EN
	 * @param EB
	 * @param mask
	 * @param center
	 */
	public static void enforceCylindricalConsistency(VoxelVectorField ET, VoxelVectorField EN, VoxelVectorField EB, IntensityVolumeMask mask, Point3d center) {
		
		Point3d pa = new Point3d(center);
		Point3d pb = new Point3d(center);
		pa.z -= 0.5;
		pb.z += 0.5;

		// n = pb - pa
		Vector3d n = new Vector3d();
		n.sub(pb, pa);

		// N = (pb - pa) / |(pb - pa)|
		Vector3d N = new Vector3d(n);
		N.normalize();

		Vector3d v = new Vector3d();
		Vector3d ca = new Vector3d();
		Vector3d rcrossv = new Vector3d();
		Point3i dim = ET.getDimension();
		Point3d p = new Point3d();
		Vector3d r = new Vector3d();
		Point3d c = new Point3d();
		for (int i = 0; i < dim.x; i++) {
			for (int j = 0; j < dim.y; j++) {
				for (int k = 0; k < dim.z; k++) {
					if (mask != null && mask.isMasked(i, j, k)) continue;

					// Get original vector
					ET.getVector3d(i, j, k, v);

					p.set(i, j, k);

					ca.sub(p, pa);
					
					// Compute closest point on midline
					c.scaleAdd(ca.dot(n), N, pa);
					
					// Compute r
					r.sub(p, c);
					
					// Compute r x v
					rcrossv.cross(r, v);
					
					// Compute signum = (r x v) . N
					double flip = -Math.signum(rcrossv.dot(n));
					
					if (flip < 0) {
						v.scale(-1);
						ET.getVolumes()[0].set(i, j, k, v.x);
						ET.getVolumes()[1].set(i, j, k, v.y);
						ET.getVolumes()[2].set(i, j, k, v.z);

						// Flip second vector to preserve right-handed coordinates
						EN.getVector3d(i, j, k, v);
						v.scale(-1);
						EN.getVolumes()[0].set(i, j, k, v.x);
						EN.getVolumes()[1].set(i, j, k, v.y);
						EN.getVolumes()[2].set(i, j, k, v.z);
					}
				}
			}
		}
	}
	
	/**
	 * Compute a per-slice center and define cylindrical consistency with respect to this center. 
	 * The FrameField is updated in-place.
	 */
	public static void enforceCylindricalConsistency(VoxelFrameField frameField, VoxelBox box, FrameAxis axis, IntensityVolumeMask mask) {
		Point3i origin = box.getOrigin();
		Point3i span = box.getSpan();
		
		// Save a copy to restore later
		Point3i originBck = new Point3i(origin);
		Point3i spanBck = new Point3i(span);
		
		int[] dim = frameField.getDimension();
		
		Vector3d v = new Vector3d();
		Vector3d ca = new Vector3d();
		Vector3d rcrossv = new Vector3d();
		Point3d p = new Point3d();
		Vector3d r = new Vector3d();
		Point3d c = new Point3d();
		
		VoxelVectorField ET = frameField.getF1();
		VoxelVectorField EN = frameField.getF2();
		VoxelVectorField EB = frameField.getF3();

		// Go slice-by-slice along axis of choice
		if (axis == FrameAxis.Z) {
			span.z = 1;
			box.setSpan(span);
			
			// Go slice-by-slice along z direction
			for (int z = 0; z < dim[2]; z++) {
				origin.z = z;
				box.setOrigin(origin);
				
				// This is our COM for this slice
				Point3d center = box.getMask().getCenterOfMass(box);
				
				Point3d pa = new Point3d(center);
				Point3d pb = new Point3d(center);
				pa.z -= 0.5;
				pb.z += 0.5;
				
				// n = pb - pa
				Vector3d n = new Vector3d();
				n.sub(pb, pa);

				// N = (pb - pa) / |(pb - pa)|
				Vector3d N = new Vector3d(n);
				N.normalize();

				for (int x = 0; x < dim[0]; x++) {
					for (int y = 0; y < dim[1]; y++) {
						if (mask.isMasked(x, y, z)) continue;

						// Get original vector
						ET.getVector3d(x, y, z, v);

						p.set(x, y, z);

						ca.sub(p, pa);
						
						// Compute closest point on midline
						c.scaleAdd(ca.dot(n), N, pa);
						
						// Compute r
						r.sub(p, c);
						
						// Compute r x v
						rcrossv.cross(r, v);
						
						// Compute signum = (r x v) . N
						double flip = -Math.signum(rcrossv.dot(n));
						
						if (flip < 0) {
							// Flip first vector
							ET.getVector3d(x, y, z, v);
							v.scale(-1);
							ET.getVolumes()[0].set(x, y, z, v.x);
							ET.getVolumes()[1].set(x, y, z, v.y);
							ET.getVolumes()[2].set(x, y, z, v.z);

							// Flip second vector
							EN.getVector3d(x, y, z, v);
							v.scale(-1);
							EN.getVolumes()[0].set(x, y, z, v.x);
							EN.getVolumes()[1].set(x, y, z, v.y);
							EN.getVolumes()[2].set(x, y, z, v.z);
							
							// Flip third
//							EB.getVector3d(x, y, z, v);
//							v.scale(-1);
//							EB.getVolumes()[0].set(x, y, z, v.x);
//							EB.getVolumes()[1].set(x, y, z, v.y);
//							EB.getVolumes()[2].set(x, y, z, v.z);
						}
					}
				}

			}
		}
		
		box.setOrigin(originBck);
		box.setSpan(spanBck);
	}

	private static void project(Point3d center, Point3d p, Vector3d projected) {
		// Get the projection
		projected.sub(p, center);
		projected.set(projected.y, -projected.x, 0);
		projected.normalize();
	}

	public void smoothNormals() {
		VoxelFrameField.smoothNormals(getF1(), getF2(), getF3(), mask);
	}
	
	/**
	 * Smooth normals and enforce orthogonality
	 * @param ET
	 * @param EN
	 * @param EB
	 * @param mask
	 */
	public static void smoothNormals(VoxelVectorField ET, VoxelVectorField EN, VoxelVectorField EB, IntensityVolume mask) {
		
		Vector3d v = new Vector3d();
		
		IntensityVolume[] ivt = ET.getVolumes();
		IntensityVolume[] ivn = EN.getVolumes();
		IntensityVolume[] ivb = EB.getVolumes();
		
		int[] dim = ivt[0].getDimension();
		
//		for (int i = 0; i < dim[0]; i++) {
//			for (int j = 0; j < dim[1]; j++) {
//				for (int k = 0; k < dim[2]; k++) {
//					if (mask != null && mask.isMasked(i, j, k)) continue;
//
//					// Average within an isotropic neighborhood of size 3
//				}
//			}
//		}
	}
	
	public VoxelFrameField extractField(VoxelBox box) {
		IntensityVolume[] volumes;;
		
		volumes = f1.getVolumes();
		VoxelVectorField fxe = new VoxelVectorField("", volumes[0].extract(box), volumes[1].extract(box), volumes[2].extract(box), box);

		volumes = f2.getVolumes();
		VoxelVectorField fye = new VoxelVectorField("", volumes[0].extract(box), volumes[1].extract(box), volumes[2].extract(box), box);
		
		volumes = f3.getVolumes();
		VoxelVectorField fze = new VoxelVectorField("", volumes[0].extract(box), volumes[1].extract(box), volumes[2].extract(box), box);
		
		return new VoxelFrameField(fxe, fye, fze, getMask().extract(box));
	}

	public VoxelVectorField[] extract(VoxelBox box) {
		IntensityVolume[] volumes;;
		
		volumes = f1.getVolumes();
		VoxelVectorField fxe = new VoxelVectorField("", volumes[0].extract(box), volumes[1].extract(box), volumes[2].extract(box), box);

		volumes = f2.getVolumes();
		VoxelVectorField fye = new VoxelVectorField("", volumes[0].extract(box), volumes[1].extract(box), volumes[2].extract(box), box);
		
		volumes = f3.getVolumes();
		VoxelVectorField fze = new VoxelVectorField("", volumes[0].extract(box), volumes[1].extract(box), volumes[2].extract(box), box);
		
		return new VoxelVectorField[] { fxe, fye, fze };
	}

	public void set(VoxelFrameField other) {
		dim = other.dim;
		
		f1.set(other.f1);
		f2.set(other.f2);
		f3.set(other.f3);
		
//		frameField = other.frameField;
	}
	
	private void createFrameField() {
//		frameField = new FramedVoxel[dim[0]][dim[1]][dim[2]];
		if (verbose) System.out.print("Creating voxel frame field...");
		Vector3d ex = new Vector3d();
		Vector3d ey = new Vector3d();
		Vector3d ez = new Vector3d();
		
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (mask != null && mask.isMasked(i, j, k)) continue;
					
//					if (k % 10000 == 0) {
//						System.out.println(i + "," + j + "," + k);
//					}
					
					f1.getVector3d(i, j, k, ex);
					f2.getVector3d(i, j, k, ey);
					f3.getVector3d(i, j, k, ez);
					
//					frameField[i][j][k] = new FramedVoxel(i, j, k, ex, ey, ez); 
				}
			}
		}
		if (verbose) System.out.println("Done creating frame field.");
	}
	
	/**
	 * @return the vector field containing the x axis of the frames.
	 */
	public VoxelVectorField getF1() {
		return f1;
	}

	public void getF1(Point3i p, Vector3d f) {
		f1.getVector3d(p.x, p.y, p.z, f);
	}
	
	public void getF2(Point3i p, Vector3d f) {
		f2.getVector3d(p.x, p.y, p.z, f);
	}
	
	public void getF3(Point3i p, Vector3d f) {
		f3.getVector3d(p.x, p.y, p.z, f);
	}
	
	/**
	 * @return the vector field containing the y axis of the frames.
	 */
	public VoxelVectorField getF2() {
		return f2;
	}
	/**
	 * @return the vector field containing the z axis of the frames.
	 */
	public VoxelVectorField getF3() {
		return f3;
	}
	
//	public FramedVoxel get(Voxel voxel) {
//		return get((int)Math.round(voxel.x), (int)Math.round(voxel.y), (int)Math.round(voxel.z));
//	}

	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return the FramedVoxel at this location.
	 */
//	public FramedVoxel get(int x, int y, int z) {
//		return frameField[x][y][z];
//	}

	public void displayX(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        if (getF1() != null) {
    		gl.glColor3d(1, 0, 0);
    		getF1().display(drawable);
        }
	}

	public void displayY(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        if (getF2() != null) {
    		gl.glColor3d(0, 1, 0);
    		getF2().display(drawable);
        }
	}

	public void displayZ(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        if (getF3() != null) {
    		gl.glColor3d(0, 0, 1);
    		getF3().display(drawable);
        }
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		displayX(drawable);
		displayY(drawable);
		displayZ(drawable);
	}
	
	public void display(GLAutoDrawable drawable, List<Point3i> voxels) {
		GL2 gl = drawable.getGL().getGL2();
		
        if (getF1() != null) {
    		gl.glColor3d(1, 0, 0);
    		getF1().display(drawable, voxels);
        }

        if (getF2() != null) {
    		gl.glColor3d(0, 1, 0);
    		getF2().display(drawable, voxels);
        }

        if (getF3() != null) {
    		gl.glColor3d(0, 0, 1);
    		getF3().display(drawable, voxels);
        }
	}
	
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		String title = "Voxel Frame Field";
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title, TitledBorder.LEFT, TitledBorder.CENTER));
        
        JPanel bpanel = new JPanel(new GridLayout(3,3));
        
        JButton btnFlipx = new JButton("x-flip");
        btnFlipx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Flip the x axis
				getF1().flip();
				
				// Also need to flip another axis to preserve right-handed coordinates
				// Choose y
				getF2().flip();
			}
		});
        
        JButton btnFlipy = new JButton("y-flip");
        btnFlipy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Flip the y axis
				getF2().flip();
				
				// Also need to flip another axis to preserve right-handed coordinates
				// Choose x
				getF1().flip();
			}
		});

        JButton btnFlipz = new JButton("z-flip");
        btnFlipz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Flip the z axis
				getF3().flip();
				
				// Also need to flip another axis to preserve right-handed coordinates
				// Choose x, y
				getF1().flip();
				getF2().flip();
			}
		});
        
        bpanel.add(btnFlipx);
        bpanel.add(btnFlipy);
        bpanel.add(btnFlipz);
        bpanel.add(draw.getControls());
        bpanel.add(drawHelix.getControls());
        bpanel.add(highDef.getControls());
        bpanel.add(drawTip.getControls());
        
        vfp.add(bpanel);
        vfp.add(drawRadius.getSliderControls());
        vfp.add(cylResolution.getSliderControls());
        
        vfp.add(getF1().getControls());
        vfp.add(getF2().getControls());
        vfp.add(getF3().getControls());

        CollapsiblePanel cp = new CollapsiblePanel(vfp.getPanel(), title);
//        cp.collapse();
		return cp;
      
//        return vfp.getPanel();
	}

	public void setVisible(boolean b) {
		draw.setValue(b);
	}

	@Override
	public int[] getDimension() {
		return new int[] { getF1().getDimension().x, getF2().getDimension().y, getF3().getDimension().z };
	}

	@Override
	public void voxelProcess(PerVoxelMethod pvm) {
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (mask == null || !mask.isMasked(i, j, k))
						pvm.process(i, j, k);
				}
			}
		}
	}

	public void voxelProcessParallel(final PerVoxelMethod pvm, int numThreads) {
		// Create list of locations to proces
		List<Point3i> pts = new ArrayList<Point3i>(dim[0] * dim[1] * dim[2]);
		for (int i = 0; i < dim[0]; i++) {
			for (int j = 0; j < dim[1]; j++) {
				for (int k = 0; k < dim[2]; k++) {
					if (mask == null || !mask.isMasked(i, j, k))
						pts.add(new Point3i(i, j, k));
				}
			}
		}

        // Number of points per thread
        int nums = pts.size() / numThreads;

        Thread[] threads = new Thread[numThreads];
        
        for (int thread = 0; thread < numThreads; thread++) {
            // Upper bound on the voxel indices
            // The last core gets the odd cut
        	int lim1 = thread * nums;
        	int lim2 = (thread != numThreads - 1) ? (thread+1) * nums : pts.size();
        	
        	final List<Point3i> samples = pts.subList(lim1, lim2);
        	threads[thread] = new Thread(new Runnable() {
				@Override
				public void run() {
					for (Point3i p : samples) {
						pvm.process(p.x, p.y, p.z);
					}
				}
			});
        }
        
        for (Thread thread : threads) {
        	thread.start();
        }
        
    	boolean resume = true;
        while (resume) {
        	for (Thread thread : threads) {
        		resume = resume || thread.isAlive();
        	}
        	try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}

	public void flip() {
		getF1().flip();
		getF2().flip();
		getF3().flip();
	}

	public boolean isVisible() {
		return draw.getValue();
	}

	public VoxelVectorField getField(MovingAxis axis) {
		if (axis == MovingAxis.F1)
			return getF1();
		else if (axis == MovingAxis.F2)
			return getF2();
		else if (axis == MovingAxis.F3)
			return getF3();
		
		return null;
	}

	public VoxelVectorField getField(int axis) {
		return getField(MovingAxis.fromInt(axis));
	}

	public void export(String filename, VoxelBox box) {
		export(filename, box, ImageType.MAT);
	}
	
	/**
	 * Export this frame field to the specified image type. NOTE: MINC is not supported at this time.
	 * @param filename
	 * @param box
	 * @param imageType
	 */
	public void export(String filename, VoxelBox box, ImageType imageType) {
    	IntensityVolume[] e1 = new IntensityVolume[] {
    			getF1().getVolumes()[0].extract(box),
    			getF1().getVolumes()[1].extract(box),
    			getF1().getVolumes()[2].extract(box)
    	};
    	
    	IntensityVolume[] e2 = new IntensityVolume[] {
    			getF2().getVolumes()[0].extract(box),
    			getF2().getVolumes()[1].extract(box),
    			getF2().getVolumes()[2].extract(box)
    	};

    	IntensityVolume[] e3 = new IntensityVolume[] {
    			getF3().getVolumes()[0].extract(box),
    			getF3().getVolumes()[1].extract(box),
    			getF3().getVolumes()[2].extract(box)
    	};

//    	IntensityVolume maskExtracted = mask.extract(box);
    	
		String fileExt = imageType.getFileType();

//		MatlabIO.testVolumeIO(e1x);
//		if (true) return;
		
		if (imageType == ImageType.TXT) {
			for (int component = 0; component < 3; component++) {
				e1[component].exportToAscii(filename + Heart.fileT[component] + fileExt);
				e2[component].exportToAscii(filename + Heart.fileN[component] + fileExt);
				e3[component].exportToAscii(filename + Heart.fileB[component] + fileExt);
			}
		}
		else if (imageType == ImageType.MAT) {
			for (int component = 0; component < 3; component++) {
				MatlabIO.save(filename + Heart.fileT[component] + fileExt,
						e1[component]);
				MatlabIO.save(filename + Heart.fileN[component] + fileExt,
						e2[component]);
				MatlabIO.save(filename + Heart.fileB[component] + fileExt,
						e3[component]);
			}
		}
	}

	public void destroy() {
		f1.destroy();
		f2.destroy();
		f3.destroy();
		
		f1 = null;
		f2 = null;
		f3 = null;
		
		mask.destroy();
		mask = null;
	}
	
	public void setHelixColoring(boolean b) {
		drawHelix.setValue(b);
	}

	public void applyMask(IntensityVolumeMask mask) {
		mask.applyMask(f1.getVolumes()[0]);
		mask.applyMask(f1.getVolumes()[1]);
		mask.applyMask(f1.getVolumes()[2]);
		mask.applyMask(f2.getVolumes()[1]);
		mask.applyMask(f2.getVolumes()[0]);
		mask.applyMask(f2.getVolumes()[2]);
		mask.applyMask(f3.getVolumes()[0]);
		mask.applyMask(f3.getVolumes()[1]);
		mask.applyMask(f3.getVolumes()[2]);
		mask.applyMask(this.mask);
	}

	public void setVoxelBox(VoxelBox voxelBox) {
    	getF1().setVoxelBox(voxelBox);
    	getF2().setVoxelBox(voxelBox);
    	getF3().setVoxelBox(voxelBox);
	}

	public void nanify(IntensityVolumeMask mask) {
		getF1().nanify(mask);
		getF2().nanify(mask);
		getF3().nanify(mask);
	}

	public void set(final OrientationFrame frame) {
		new VoxelBox(dim).voxelProcess(new PerVoxelMethodUnchecked() {
			
			@Override
			public void process(int x, int y, int z) {
				getField(MovingAxis.F1).set(x, y, z, frame.getAxis(FrameAxis.X));
				getField(MovingAxis.F2).set(x, y, z, frame.getAxis(FrameAxis.Y));
				getField(MovingAxis.F3).set(x, y, z, frame.getAxis(FrameAxis.Z));
			}
		});
	}
	public void set(Point3i voxel, CoordinateFrame frame) {
		getF1().set(voxel, frame.getAxis(FrameAxis.X));
		getF2().set(voxel, frame.getAxis(FrameAxis.Y));
		getF3().set(voxel, frame.getAxis(FrameAxis.Z));
	}

	public void set(int x, int y, int z, OrientationFrame frame) {
		getF1().set(x, y, z, frame.getAxis(FrameAxis.X));
		getF2().set(x, y, z, frame.getAxis(FrameAxis.Y));
		getF3().set(x, y, z, frame.getAxis(FrameAxis.Z));
	}

	public void set(Point3i voxel, OrientationFrame frame) {
		getF1().set(voxel, frame.getAxis(FrameAxis.X));
		getF2().set(voxel, frame.getAxis(FrameAxis.Y));
		getF3().set(voxel, frame.getAxis(FrameAxis.Z));
	}

	public void set(int x, int y, int z, Vector3d[] frameAxes) {
		getF1().set(x, y, z, frameAxes[0]);
		getF2().set(x, y, z, frameAxes[1]);
		getF3().set(x, y, z, frameAxes[2]);
	}

	public void set(Point3i voxel, Vector3d[] frameAxes) {
		getF1().set(voxel, frameAxes[0]);
		getF2().set(voxel, frameAxes[1]);
		getF3().set(voxel, frameAxes[2]);
	}
	
	public void setMask(IntensityVolumeMask mask) {
		this.mask = mask;
		getF1().getVoxelBox().setMask(mask);
		getF2().getVoxelBox().setMask(mask);
		getF3().getVoxelBox().setMask(mask);
	}
	
	public VoxelFrameField perturb(final double mag) {
		int[] dims = getDimension();
//		VoxelBox box = getFieldX().getVoxelBox();
		
		final VoxelFrameField frameField = new VoxelFrameField(dims, mask);

		final Matrix4d R1 = new Matrix4d();
		final Matrix4d R2 = new Matrix4d();
		final Matrix4d R3 = new Matrix4d();
		final Random rand = new Random();
		new VoxelBox(getMask()).voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
//				CoordinateFrame frame = new CoordinateFrame(new OrientationFrame().randomize(), new Point3d(x, y, z));
//				Vector3d[] current = get(x, y, z);
//				OrientationFrame frame = new OrientationFrame(current);
//				OrientationFrame Rx = OrientationFrame.GetRotationMatrix(FrameAxis.X, mag);
//				OrientationFrame Rz = OrientationFrame.GetRotationMatrix(FrameAxis.Z, mag);
//				frame.premultiply(Rx.getFrame()).premultiply(Rz.getFrame());
//				frameField.set(x, y,z, frame);
				R1.set(new AxisAngle4d(FrameAxis.X.getAxis(), (1 - 2*rand.nextDouble()) * mag));
				R2.set(new AxisAngle4d(FrameAxis.Y.getAxis(), (1 - 2*rand.nextDouble()) * mag));
				R3.set(new AxisAngle4d(FrameAxis.X.getAxis(), (1 - 2*rand.nextDouble()) * mag));
				
				R1.mul(R2);
				R1.mul(R3);
				
				// Apply rotation
				Vector3d[] f = get(x, y, z);
				R1.transform(f[0]);
				R1.transform(f[1]);
				R1.transform(f[2]);
				set(x, y,z, f);
			}
		});
		
//		frameField.setVoxelBox(box);
		
		return frameField;
	}
	public static VoxelFrameField randomize(int[] dims) {
		IntensityVolumeMask mask = new IntensityVolumeMask(dims);
		mask.fillWith(1);
		final VoxelFrameField frameField = new VoxelFrameField(dims, mask);
		
		frameField.voxelProcess(new PerVoxelMethodUnchecked() {
			@Override
			public void process(int x, int y, int z) {
				CoordinateFrame frame = new CoordinateFrame(new OrientationFrame().randomize(), new Point3d(x, y, z));
				frameField.set(new Point3i(x, y, z), frame);
			}
		});
		
		return frameField;
	}

	public Vector3d[] get(int x, int y, int z) {
		return new Vector3d[] { getF1().getVector3d(x, y, z), getF2().getVector3d(x, y, z), getF3().getVector3d(x, y, z) };
	}

	public Vector3d[] get(Point3i p) {
		return new Vector3d[] { getF1().getVector3d(p), getF2().getVector3d(p), getF3().getVector3d(p) };
	}

	public VoxelFrameField voxelCopy() {
//		public VoxelFrameField(VoxelVectorField x, VoxelVectorField y, VoxelVectorField z, IntensityVolumeMask mask) {
		
		VoxelVectorField f1c = new VoxelVectorField(f1.getTitle(), f1.getX().asVolumeCopy(), f1.getY().asVolumeCopy(), f1.getZ().asVolumeCopy(), new VoxelBox(f1.getVoxelBox()));
		VoxelVectorField f2c = new VoxelVectorField(f2.getTitle(), f2.getX().asVolumeCopy(), f2.getY().asVolumeCopy(), f2.getZ().asVolumeCopy(), new VoxelBox(f2.getVoxelBox()));
		VoxelVectorField f3c = new VoxelVectorField(f3.getTitle(), f3.getX().asVolumeCopy(), f3.getY().asVolumeCopy(), f3.getZ().asVolumeCopy(), new VoxelBox(f3.getVoxelBox()));
		
		VoxelFrameField copy = new VoxelFrameField(f1c, f2c, f3c);
		copy.setVisible(isVisible());
		f1c.setVisible(f1.isVisible());
		f2c.setVisible(f2.isVisible());
		f3c.setVisible(f3.isVisible());
		
		copy.setMask(new IntensityVolumeMask(getMask()));
		
		return copy;
	}

	public OrientationFrame getFrame(Point3i p) {
		Vector3d[] F = get(p);
		
		return new OrientationFrame(F[0], F[1], F[2]);
	}

	public IntensityVolumeMask getMask() {
		return mask;
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// nothing to do
	}

	@Override
	public String getName() {
		return "voxel frame field";
	}

	public void setRegion(VoxelBox box) {
		getF1().getVoxelBox().setRegion(box);
		getF2().getVoxelBox().setRegion(box);
		getF3().getVoxelBox().setRegion(box);
	}

	public void fixAlignment() {
		
		// Run a few time to make sure fixes diffuse everywhere
		// TODO: pass this as parameter
		int numRepeats = 10;
		
		for (int i = 0; i < numRepeats; i++) {
			int neighborhoodSize = 3;
			final List<Point3i> neighbors = VolumeSampler.sample(NeighborhoodShape.Isotropic, neighborhoodSize);
			VoxelBox box = new VoxelBox(getMask());
			final Point3i q = new Point3i();
			final Vector3d f1p = new Vector3d();
			final Vector3d f2p = new Vector3d();
			final Vector3d f1q = new Vector3d();
			for (Point3i p : box.getVolume()) {
				getF1(p, f1p);
				getF2(p, f2p);

				final IntegerWrapper innerSum = new IntegerWrapper(0);
				box.neighborhoodProcess(p, neighbors, new PerVoxelMethodUnchecked() {
					@Override
					public void process(int x, int y, int z) {
						// Process this neighbor
						q.set(x, y, z);
						
						// Fetch neighbor's frame
						getF1(q, f1q);
						
						// Accumulate inner products
						innerSum.add((int) Math.signum(f1p.dot(f1q)));
					}
				});
				
				// Apply alignment to both X and Y (to preserve right-handedness)
				double direction = Math.signum(MathToolset.epsilon + innerSum.get());
				f1p.scale(direction);
				f2p.scale(direction);
				
				getF1().set(p, f1p);
				getF2().set(p, f2p);
			}			
		}
	}
	
	private static class IntegerWrapper {
		private int i;
		public IntegerWrapper(int i) {
			this.i = i;
		}
		public int get() {
			return i;
		}
		public void add(int i) {
			this.i += i;
		}
		public void inc() {
			i++;
		}
		
		public void dec() {
			i--;
		}
	}
}
