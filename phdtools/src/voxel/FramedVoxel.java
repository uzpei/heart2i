package voxel;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import tools.frame.CoordinateFrame;
import tools.frame.OrientationFrame.FrameAxis;

/**
 * Class representing a voxel associated with a coordinate system.
 * @author piuze
 */
public class FramedVoxel extends Voxel {

	/**
	 * Automatically generated serial ID.
	 */
	private static final long serialVersionUID = 8168503181708976163L;

	/**
	 * The coordinate system at this voxel.
	 */
	private CoordinateFrame frame;
	
	private static final Matrix4d identity = new Matrix4d(); {
		identity.setIdentity();
	}
	
	/**
	 * Create a voxel with a coordinate system set to the identity matrix.
	 */
	public FramedVoxel() {
		this(identity);
	}
	
	/**
	 * Create a voxel with this coordinate system.
	 * @param frame
	 */
	public FramedVoxel(Matrix4d frame) {
		this(new CoordinateFrame(frame));
	}
	
	public FramedVoxel(CoordinateFrame frame) {
		this.frame = new CoordinateFrame(frame);
		this.x = frame.getTranslation().x;
		this.y = frame.getTranslation().y;
		this.z = frame.getTranslation().z;
	}

	/**
	 * Create a voxel with these vectors as basis for its coordinate system.
	 * @param ex The x basis vector.
	 * @param ey The y basis vector.
	 * @param ez The z basis vector.
	 */
	public FramedVoxel(double x, double y, double z, Vector3d ex, Vector3d ey, Vector3d ez) {
		this(new CoordinateFrame(ex, ey, ez, new Point3d(x, y, z)));
	}
	
	/**
	 * @return the frame associated to this voxel.
	 */
	public CoordinateFrame getFrame() {
		return frame;
	}

	public double dot(FramedVoxel other) {
		return this.x * other.x + this.y * other.y + this.z * other.z;
	}
}
