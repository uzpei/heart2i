package picking;

import gl.renderer.JoglRenderer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.IntParameter;
import tools.loader.PolygonSoup;
import tools.loader.Vertex;

import com.jogamp.common.nio.Buffers;

/**
 * @author piuze
 */
public class ModelPicker {
    private int pickedVertexIndex = -1;

    private GLU glu = new GLU();

    private int selectionHits = 0;

    private int pickbufferSize = 512;

    private IntParameter pickSize = new IntParameter("Picking size", 10, 0, 100);

    private IntParameter pointSize = new IntParameter("Point size", 10, 1, 100);

    private BooleanParameter pointMode = new BooleanParameter("Display picking scene",
            true);
    
    private PolygonSoup model = null;

    public ModelPicker(PolygonSoup pmodel) {
        model = pmodel;
    }
    
    /**
     * Hits are stored in this selection buffer.
     */
    private IntBuffer selectionBuffer = ByteBuffer.allocateDirect(
            Buffers.SIZEOF_INT * pickbufferSize).order(
            ByteOrder.nativeOrder()).asIntBuffer();

    private int pickModel(GLAutoDrawable drawable, int x, int y) {
        initPicking(drawable, x, y);
        drawSelectionModel(drawable);
        return stopPicking(drawable);
    }

    /**
     * Perform picking on this drawing pass
     * @param drawable
     * @param model 
     * @pre size must have been set
     */
    private void initPicking(GLAutoDrawable drawable, int x, int y) {
        GL2 gl = drawable.getGL().getGL2();
        JoglRenderer.beginFixedPipelineMode(gl);

        int[] viewport = new int[4];
        double[] currentProjectionMatrix = new double[16];
        gl.glGetDoublev(GL2.GL_PROJECTION_MATRIX, currentProjectionMatrix, 0);

        /*
         * Enter SELECT MODE
         */
        gl.glSelectBuffer(selectionBuffer.capacity(), selectionBuffer);
        gl.glRenderMode(GL2.GL_SELECT);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();

        gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
        glu.gluPickMatrix(x, viewport[3] - y,
                pickSize.getValue(), pickSize.getValue(), viewport, 0);

//        glu.gluPickMatrix(x, viewport[3] - y,
//                10, 10, viewport, 0);

        gl.glMultMatrixd(currentProjectionMatrix, 0);

        gl.glMatrixMode(GL2.GL_MODELVIEW);

        gl.glInitNames();

    }

    private int stopPicking(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glFlush();

        // Restore rendering mode
        selectionHits = gl.glRenderMode(GL2.GL_RENDER);

        if (selectionHits != 0)
            pickedVertexIndex = processHits();
        else 
            pickedVertexIndex = -1;

        JoglRenderer.endFixedPipelineMode(gl);
        
        return pickedVertexIndex;
    }

    private int processHits() {

        // Buffer order:
        // 0 = number of names detected
    	// 1 = minimum depth value detected, 2 = maximum depth value detected
    	// 3=name[1], 4=name[2], ... name[#names]
    	int numCount = selectionBuffer.get(0);
    	if (numCount == 0)
    		return - 1;
    	
        int minDepth = selectionBuffer.get(1);
        int maxDepth = selectionBuffer.get(2);
        pickedVertexIndex = selectionBuffer.get(3);
//        for (int i = 0; i < selectionHits; i++) {
//            if (selectionBuffer.get(4 * i + 1) > closestDistance) {
//                pickedVertexIndex = selectionBuffer.get(4 * i + 3);
//                closestDistance = selectionBuffer.get(4 + i + 1);
//            }
//        }

//        System.out.println("Picked vertex " + pickedVertexIndex
//                    + " out of " + selectionHits + " other primitives.");
        
        return pickedVertexIndex;
    }
    
    public void drawSelectionModel(GLAutoDrawable drawable) {
        model.drawVertices(drawable, pointSize.getValue());
    }

    /**
     * Do picking on this GL pass.
     * @param drawable
     * @param x 
     * @param y 
     * @return null or the vertex that was picked.
     */
    public Vertex pick(GLAutoDrawable drawable, int x, int y) {
        return pick(drawable, model, x, y, new float[] { 0, 0, 0.7f, 0.7f });
    }

    /**
     * Do picking on this GL pass and draw the picked location.
     * @param drawable
     * @param x 
     * @param y 
     * @return null or the vertex that was picked.
     */
    public Vertex pick(GLAutoDrawable drawable, int x, int y, float[] color) {
        return pick(drawable, model, x, y, color);
    }

    /**
     * Do picking on this GL pass.
     * @param drawable
     * @param pmodel 
     * @param x 
     * @param y 
     * @return null or the vertex that was picked.
     */
    public Vertex pick(GLAutoDrawable drawable, PolygonSoup pmodel, int x, int y, float[] color) {
        model = pmodel;

        if (model == null) return null;

        int pickedIndex = pickModel(drawable, x, y);
        
        if (pointMode.getValue()) drawSelectionModel(drawable);

        if (pickedIndex >= 0) {
            Vertex vertex = new Vertex(model.getVertices().get(pickedIndex));
            
            model.drawVertex(drawable, vertex, color);

            return vertex;
        }
        else {
            return null;
        }
    }

    /**
     * @return UI controls for this class. 
     */
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Picking"));
        vfp.add(pointMode.getControls());
        vfp.add(pointSize.getSliderControls());
        vfp.add(pickSize.getSliderControls());
        
        return vfp.getPanel();
    }
    
    public PolygonSoup getModel() {
        return model;
    }
}
