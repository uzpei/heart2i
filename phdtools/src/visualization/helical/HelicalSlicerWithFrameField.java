package visualization.helical;

import gl.geometry.FancyArrow;
import gl.geometry.VertexBufferObject.Vertex;
import gl.material.GLMaterial;

import java.awt.Color;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import system.object.Triplet;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;
import voxel.VoxelVectorField;

public class HelicalSlicerWithFrameField extends HelicalSlicer {

	private VoxelVectorField vectorField;
	private boolean useSphericalColors;
	private double vectorScale = 5;
	private boolean sampleVectors = true;
	private Point3d pv = new Point3d();
	private FancyArrow fancyArrow;
	private Point3d center = new Point3d();
	
	public HelicalSlicerWithFrameField(IntensityVolume volume, IntensityVolumeMask mask, VoxelBox box, VoxelVectorField vectorField) {
		super (volume, mask, box);
		this.vectorField = vectorField;
	}	
	
	@Override
	protected void assign(Point3d p, GLMaterial material) {
		if (useSphericalColors) {
			// Look up fiber orientation
			Point3i pi = MathToolset.tuple3dTo3i(p);
			double vx = vectorField.getVolumes()[0].get(pi.x, pi.y, pi.z);
			double vy = vectorField.getVolumes()[1].get(pi.x, pi.y, pi.z);
			double vz = vectorField.getVolumes()[2].get(pi.x, pi.y, pi.z);
			float thetaNormalized = (float) (Math.acos(vz) / Math.PI);
			double phi = Math.abs(Math.atan2(vy, vx) / (2*Math.PI));
			float phiNormalized = (float) (phi >= 0 ? phi : phi + 1);
			
//			System.out.println(phi + " -> " + phiNormalized);
			
			/*
			 *  This is the symmetrical Pierpaoli representation
			 */
			Color c = new Color(Color.HSBtoRGB(thetaNormalized, 1f, 1f));
//			Color c = new Color(Color.HSBtoRGB(thetaNormalized, 1f - phiNormalized, 1f));
			material.diffuse[0] = c.getRed() / 255f;
			material.diffuse[1] = c.getGreen() / 255f;
			material.diffuse[2] = c.getBlue() / 255f;
			material.diffuse[3] = (float) transparency;
		}
		else {
			super.assign(p, material);
		}
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		if (displayBlocked)
			return;

		super.display(drawable);
		vectorScale = 5;
		GL2 gl = drawable.getGL().getGL2();

		gl.glPushMatrix();
		super.box.applyHeartNormalization(gl);

		if (vectorField != null && sampleVectors) {
			fancyArrow = new FancyArrow(new Point3d(), new Point3d(), new Color3f(1, 1, 1), 0.5);
			Vector3d v = new Vector3d();
			Point3d pScaled = new Point3d();
			Vector3d vWall = new Vector3d();
			double proximity = 0.1;
			
			for (Triplet<Point3d, Point3d, Point3d> point : helix) {
				Point3d p = point.getCenter();
				if (p == null || mask.isMasked(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z))) continue;
				
				// Draw outer
				vWall.sub(point.getCenter(), point.getLeft());
				pScaled.scaleAdd(proximity, vWall, point.getLeft());
				vectorField.getVector3d(pScaled, v);
				drawArrow(gl, point.getLeft(), v, vectorScale);
				
				// Draw center
				vectorField.getVector3d(point.getCenter(), v);
				drawArrow(gl, point.getCenter(), v, vectorScale);

				// Draw inner
				vWall.sub(point.getCenter(), point.getRight());
				pScaled.scaleAdd(proximity, vWall, point.getRight());
				vectorField.getVector3d(pScaled, v);
				drawArrow(gl, point.getRight(), v, vectorScale);
			}
		}
		else {
			fancyArrow = new FancyArrow(new Point3d(), new Point3d(), new Color3f(1, 1, 1), 0.5);
			Vector3d v = new Vector3d();
			
//			for (Quad q : super.helicalQuads) {
//				Point3d p = q.getPoint();
//				vectorField.getVector3d(p, v);
//				drawArrow(gl, p, v, vectorScale);
//			}
			for (Vertex vertex : super.vbo.getVertices()) {
				Point3d p = new Point3d(vertex.x, vertex.y, vertex.z);
				vectorField.getVector3d(p, v);
				drawArrow(gl, p, v, vectorScale);
			}
			
		}
		gl.glPopMatrix();
	}
	
	private void drawArrow(GL2 gl, Point3d p, Vector3d v, double scale) {
//		if (v.length() > 1 + 1e-2) return;
//		v.normalize();
		
		center.set(p);
		v.scale(scale);
		pv.scaleAdd(0.5, v, center);
		center.scaleAdd(-0.5, v, center);
		
		fancyArrow.setFrom(center);
		fancyArrow.setTo(pv);
		fancyArrow.draw(gl);
//		fancyArrow.drawCoarse(gl);
	}
}
