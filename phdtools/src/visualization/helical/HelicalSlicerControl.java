package visualization.helical;

import gl.geometry.GLObject;
import gl.renderer.GLViewerConfiguration;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;
import heart.HeartManager;
import heart.HeartTools;

import java.awt.GridLayout;

import javax.swing.JPanel;

import swing.component.VerticalFlowPanel;
import swing.parameters.BooleanParameter;
import swing.parameters.DoubleParameter;
import swing.parameters.IntParameter;
import swing.parameters.Parameter;
import swing.parameters.ParameterListener;
import swing.text.GreekToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;

public class HelicalSlicerControl extends HelicalSlicer implements GLObject {

	private DoubleParameter phaseC = new DoubleParameter("phase", 0, 0, 360);
	private IntParameter zSamplingC = new IntParameter("z sampling", zSampling, 0, 300);
	private IntParameter thetaSamplingC = new IntParameter(GreekToolset.GreekLetter.THETA.toString() + " sampling", thetaSampling, 0, 100);
	private IntParameter NC = new IntParameter("subplanar resolution", N, 1, 48);
	private DoubleParameter transparencyC = new DoubleParameter("transparency", 1, 0, 1);
	private BooleanParameter interpolateC = new BooleanParameter("linear filtering", false);
	private BooleanParameter interpolateQ = new BooleanParameter("interpolate", false);
	private BooleanParameter draw = new BooleanParameter("draw", true);
	private BooleanParameter drawHelix = new BooleanParameter("helix", false);
	private BooleanParameter drawOutline = new BooleanParameter("outline", false);
	private BooleanParameter drawAll = new BooleanParameter("draw everything (slow)", false);

	public HelicalSlicerControl(IntensityVolume volume, IntensityVolumeMask mask) {
		this(volume, mask, new VoxelBox(mask));
	}
	
	public HelicalSlicerControl(IntensityVolume volume, IntensityVolumeMask mask, VoxelBox box) {
		super(volume, mask, box);
		createControls();
	}

	public HelicalSlicerControl() {
		super();
		createControls();
	}
	
	private void createControls() {
		ParameterListener stateListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				phase = phaseC.getValue();
				zSampling = zSamplingC.getValue();
				thetaSampling = thetaSamplingC.getValue();
				
				// Update volume
				sliceVolume();
			}
		};
		
		ParameterListener geometryListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				N = NC.getValue();
				transparency = transparencyC.getValue();
				precomputeDisplayList();
			}
		};
		
		ParameterListener displayListener = new ParameterListener() {
			@Override
			public void parameterChanged(Parameter parameter) {
				displayHelix = drawHelix.getValue();
				display = draw.getValue();
				displayAll = drawAll.getValue();
				displayOutline = drawOutline.getValue();
				useColorInterpolation = interpolateC.getValue();
				useLinearInterpolation = interpolateQ.getValue();
			}
		};
		
		phaseC.addParameterListener(stateListener);
		zSamplingC.addParameterListener(stateListener);
		thetaSamplingC.addParameterListener(stateListener);
		NC.addParameterListener(geometryListener);
		transparencyC.addParameterListener(geometryListener);
		interpolateC.addParameterListener(displayListener);
		interpolateQ.addParameterListener(displayListener);
		drawHelix.addParameterListener(displayListener);
		drawAll.addParameterListener(displayListener);
		draw.addParameterListener(displayListener);
		drawOutline.addParameterListener(displayListener);
	}

	@Override
	public JPanel getControls() {
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		vfp.add(colorMap);
		vfp.add(phaseC.getSliderControls());
		vfp.add(zSamplingC.getSliderControls());
		vfp.add(thetaSamplingC.getSliderControls());
		vfp.add(NC.getSliderControls());
//		vfp.add(transparencyC.getSliderControls());
//		vfp.add(interpolateC.getControls());
//		vfp.add(interpolateQ.getControls());
		
		JPanel boolPanel = new JPanel(new GridLayout(2,3));
		boolPanel.add(draw.getControls());
		boolPanel.add(drawHelix.getControls());
		boolPanel.add(drawOutline.getControls());
		boolPanel.add(drawAll.getControls());
		vfp.add(boolPanel);
//		vfp.add(drawHelix.getControls());
//		vfp.add(draw.getControls());
//		vfp.add(drawOutline.getControls());
		return vfp.getPanel();
	}

	@Override
	public void reload(GLViewerConfiguration config) {
	}
	
	public static void main(String[] args) {
		NodeScene scene = new NodeScene();
//    	Heart heart = new HeartManager(false).loadHeart(new HeartDefinition("rat07052008", Species.Rat,"./data/heart/rat_samples/sampleSubset/"));
//    	Heart heart = new HeartManager(false).loadHeart(new HeartDefinition("rat07052008", Species.Rat,"./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/"));
		Heart heart = HeartTools.heartSynthetic();
    	HelicalSlicerControl slicer = new HelicalSlicerControl(heart.getFrameField().getF1().getX(), heart.getMask(), heart.getVoxelBox());
		scene.addControlNode(slicer);
		scene.addNode(slicer);
	}
	
	public boolean isVisible() {
		return draw.getValue();
	}

	public void setVisible(boolean b) {
		draw.setValue(b);
	}

	public IntensityVolumeMask getMask() {
		return mask;
	}

	public IntensityVolume getVolume() {
		return volume;
	}

	public VoxelBox getVoxelBox() {
		return box;
	}

	public void setDrawAll(boolean b) {
		drawAll.setValue(b);
	}
}
