package visualization.helical;

import gl.geometry.GLGeometry;
import gl.geometry.VertexBufferObject;
import gl.geometry.primitive.Quad;
import gl.geometry.primitive.QuadNonPlanar;
import gl.material.GLMaterial;
import gl.renderer.NodeScene;
import heart.Heart;
import heart.Heart.Species;
import heart.HeartDefinition;
import heart.HeartManager;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import math.coloring.ColorMap;
import math.coloring.JColorizer;
import system.object.Pair;
import system.object.Triplet;
import tools.geom.MathToolset;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelBox;

public class HelicalSlicer implements GLGeometry {

	protected boolean displayBlocked = false;
	protected JColorizer colorMap;
    protected double phase = 0;
	protected int zSampling = 100;
	protected int thetaSampling = 30;
	protected int N = 25;
	protected double transparency = 1;
	
	protected List<Triplet<Point3d, Point3d, Point3d>> helix;
	protected List<Triplet<Boolean, Point3d, Double>> centerline;
	protected IntensityVolume volume;
	protected IntensityVolumeMask mask;
	protected VoxelBox box;
	protected List<Quad> helicalQuads;

	protected VertexBufferObject vbo;
	protected VertexBufferObject vboAll;
	
	/**
	 * Default values
	 */
	protected boolean useColorInterpolation = false;
	protected boolean useLinearInterpolation = false;
	protected boolean displayHelix = false;
	protected boolean displayAll = false;
	protected boolean display = true;
	protected boolean displayOutline = false;
	public static final ColorMap.Map DefaultMap = ColorMap.Map.SPECTRA;

	public HelicalSlicer(ColorMap.Map map) {
		colorMap = new JColorizer(map);
		colorMap.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				precomputeDisplayList();
			}
		});
	}

	public HelicalSlicer() {
		this(DefaultMap);
	}

	/**
	 * Use the whole volume (no mask)
	 * @param volume
	 */
	public HelicalSlicer(IntensityVolume volume) {
		this(volume, new IntensityVolumeMask(volume.getDimension(), true));
	}

	public HelicalSlicer(IntensityVolume volume, ColorMap.Map map) {
		this(volume, new IntensityVolumeMask(volume.getDimension(), true), map);
	}

	public HelicalSlicer(IntensityVolume volume, IntensityVolumeMask mask) {
		this(volume, mask, new VoxelBox(mask), DefaultMap);
	}

	public HelicalSlicer(IntensityVolume volume, IntensityVolumeMask mask, ColorMap.Map map) {
		this(volume, mask, new VoxelBox(mask), map);
	}

	public HelicalSlicer(IntensityVolume volume, IntensityVolumeMask mask, VoxelBox box, ColorMap.Map map) {
		this(map);
		
		updateVolumes(volume, mask, box);
	}
	
	public HelicalSlicer(IntensityVolume volume, IntensityVolumeMask mask,VoxelBox box) {
		this(volume, mask, box, DefaultMap);
	}

	public void updateVolumes(IntensityVolume volume, IntensityVolumeMask mask, VoxelBox box) {
		this.volume = volume;
		this.mask = mask;
		this.box = box;
		
		sliceVolume();
	}
	
	public void sliceVolume() {
		
		if (volume == null || mask == null || box == null)
			return;
		
		boolean displayBlockedCached = displayBlocked;
		displayBlocked = true;

		double[] minMax = volume.getminMax();
		colorMap.setMinMax(minMax[0], minMax[1]);
		
		// Make sure we work with udpated geometry
		centerline = box.getMask().computeCenterlineAlongWorldZ();

		// Helical parameters
		helix = new ArrayList<Triplet<Point3d, Point3d, Point3d>>();

		// Compute minimum and maximum z planes
		double z0 = Double.MAX_VALUE, zf = Double.MIN_VALUE;
		
		for (Triplet<Boolean, Point3d, Double> slice : centerline) {
			if (!slice.getLeft())
				continue;
			if (slice.getCenter().z > zf)
				zf = slice.getCenter().z;
			if (slice.getCenter().z < z0)
				z0 = slice.getCenter().z;
		}
		
		// Use min of slice found and voxelbox 
//		zf = Math.min(zf, heart.getVoxelBox().getOrigin().x + heart.getVoxelBox().getSpan().z);
//		zf = heart.getVoxelBox().getOrigin().x + heart.getVoxelBox().getSpan().z;
//		zf = 65;
//		System.out.println("z0 = " + z0 + ", zf = " + zf);
		// Assemble helix
		double theta0 = phase;
		double dtheta = 2 * Math.PI / thetaSampling;
		double z = 0;
		double theta = theta0;
		double dz = (zf - z0) / zSampling;
		
		for (Triplet<Boolean, Point3d, Double> slice : centerline) {
			if (!slice.getLeft() || z > zf)
				continue;

			if (dz < MathToolset.epsilonEighth)
				continue;

			Point3d centroid = slice.getCenter();
			double radius = slice.getRight();
			
			// Set the current slice
//			Point3i boxOrigin = box.getOrigin();
//			boxOrigin.z = MathToolset.roundInt(centroid.z);
//			box.setOrigin(boxOrigin);

			// Continue if we have reached the next slice
			while (z < centroid.z + 1) {
				z += dz;
				theta += dtheta;

				// Current helical point
				Point3d pHelix = new Point3d();
				pHelix.x = centroid.x + radius * Math.cos(theta);
				pHelix.y = centroid.y + radius * Math.sin(theta);
				pHelix.z = z;
				
				// 1) Raytrace along both planar helix normals (in both directions) until we find a hit
				// 2) Once a hit is found, mark this location x0 and completely discard the other normal direction
				// 3) Keep tracing until we are through the heart wall
				// 4) Mark this location x1
				// 5) Find the midpoint (x1 - x0) / 2 and use this as the helical center
				// f(u) = ( acos(u), asin(u), bu )
				
				Pair<Point3d, Point3d> boundary1 = trace(mask, box, centroid, new Vector3d(Math.cos(theta), Math.sin(theta), 0));
				Pair<Point3d, Point3d> boundary2 = trace(mask, box, centroid, new Vector3d(-Math.cos(theta), -Math.sin(theta), 0));
				Point3d mid1 = new Point3d();
				Point3d mid2 = new Point3d();

				// Compute midpoints
				// (only if both exist)
				
				double d1 = Double.MAX_VALUE;
				double d2 = Double.MAX_VALUE;
				
				if (boundary1 != null) {
					mid1.add(boundary1.getLeft(), boundary1.getRight());
					mid1.scale(0.5);
					d1 = pHelix.distance(mid1);
				}

				if (boundary2 != null) {
					mid2.add(boundary2.getLeft(), boundary2.getRight());
					mid2.scale(0.5);
					d2 = pHelix.distance(boundary2.getLeft());
				}
				
				// Compute the mid-point boundary
				Pair<Point3d, Point3d> boundary = null;
				Point3d mid = null;
				if (boundary1 != null && boundary2 != null) {
					mid = mid1;
					boundary = d1 < d2 ? boundary1 : boundary2;
				}
				else if (boundary1 != null) {
					mid = mid1;
					boundary = boundary1;
				}
				else if (boundary2 != null) {
					mid = mid2;
					boundary = boundary2;
				}
				
				if (boundary != null && mid != null) {
					helix.add(new Triplet<Point3d, Point3d, Point3d>(boundary.getLeft(), mid, boundary.getRight()));
				}
				else {
					helix.add(new Triplet<Point3d, Point3d, Point3d>(null, null, null));
				}
			}
		}

		// Apply one smoothing iteration
		for (int i = 0; i < 4; i++)
			applySmoothing();
		
		precomputeDisplayList();
		
		displayBlocked = displayBlockedCached;
	}
	
	/**
	 * Return intersection points by starting at the centroid and moving in the input direction.
	 * @param centroid
	 * @param direction
	 * @return
	 */
	private Pair<Point3d, Point3d> trace(IntensityVolumeMask mask, VoxelBox box, Point3d centroid, Vector3d direction) {
		double stepSize = 0.5;
		
		// Need to fine-tune this... could be a major speedup
		int maxSteps = MathToolset.roundInt(0.5 * box.getMaxDimension() / stepSize);
		int step = 0;

		Point3d point = new Point3d();
		boolean[] intersected = new boolean[] { false, false };
		Pair<Point3d, Point3d> boundary = new Pair<Point3d, Point3d>(new Point3d(), new Point3d());
		while(step < maxSteps) {
			point.scaleAdd(step * stepSize, direction, centroid);

			// Check for intersection
			if (checkHit(mask, point)) {
				
				// This is the first hit
				if (!intersected[0]) {
					intersected[0] = true;
					boundary.getLeft().set(point);
				}
				else {
					// Else mark this location and continue
					intersected[1] = true;
					boundary.getRight().set(point);
				}
			}
			// Could terminate abruptly using the distance transform
			// if (step * stepSize > maxDistanceTransform)
			// 	terminate = true;
			
			step++;
		}
		
		if (intersected[0] && intersected[1])
			return boundary;
		else
			return null;
	}
	
	private boolean checkHit(IntensityVolumeMask mask, Point3d p) {
		if (!mask.hasPoint(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z)))
			return false;
		
		return !mask.isMasked(MathToolset.roundInt(p.x), MathToolset.roundInt(p.y), MathToolset.roundInt(p.z));
	}
	
	public boolean validate(Triplet<Point3d, Point3d, Point3d> point) {
		return point.getLeft() != null && point.getCenter() != null && point.getRight() != null;
	}

	/**
	 * Apply smoothing to the helical geometry.
	 */
	public void applySmoothing() {
		if (helix == null)
			return;
		
		ListIterator<Triplet<Point3d, Point3d, Point3d>> iterator = helix.listIterator();
		Point3d avg = new Point3d();
		while (iterator.hasNext()) {
			if (iterator.hasPrevious() && iterator.hasNext()) {
				// Fetch previous
				Triplet<Point3d, Point3d, Point3d> previous = iterator.previous();
				// Fetch current
				Triplet<Point3d, Point3d, Point3d> current = iterator.next();
				// Fetch next
				Triplet<Point3d, Point3d, Point3d> next = iterator.next();
				
				if (validate(previous) && validate(current) && validate(next)) {
					// Average previous and next
					avg.set(0,0,0);
					avg.add(previous.getLeft());
					avg.add(current.getLeft());
					avg.add(next.getLeft());
					avg.scale(1d/3d);
					current.getLeft().set(avg);
					
					avg.set(0,0,0);
					avg.add(previous.getCenter());
					avg.add(current.getCenter());
					avg.add(next.getCenter());
					avg.scale(1d/3d);
					current.getCenter().set(avg);
					
					avg.set(0,0,0);
					avg.add(previous.getRight());
					avg.add(current.getRight());
					avg.add(next.getRight());
					avg.scale(1d/3d);
					current.getRight().set(avg);					
				}
				
				// Return to current
				iterator.previous();
			}
			
			// Go to next
			iterator.next();
		}
	}
	
	public void precomputeDisplayList() {

		if (helix == null)
			return;
		
		boolean displayBlockedCached = displayBlocked;
		displayBlocked = true;
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Quad> helicalQuads = new LinkedList<Quad>();
		
		Triplet<Point3d, Point3d, Point3d> previous = null;
		
//		int materialCount = useColorInterpolation ? 4 : 1;
		int materialCount = 4;
		
		GLMaterial baseMaterial = new GLMaterial(0, 0, 0, 0);
		
		for (Triplet<Point3d, Point3d, Point3d> point : helix) {
			Point3d pLeft = point.getLeft();
			Point3d pMid = point.getCenter();
			Point3d pRight = point.getRight();
			
			if (pLeft == null || pMid == null || pRight == null)
				continue;

			if (previous != null) {
				Point3d prevLeft = previous.getLeft();
				Point3d prevRight = previous.getRight();

				QuadNonPlanar quad = new QuadNonPlanar(prevLeft, pLeft, pRight, prevRight);
				int Ni = N;
				int Nj = Ni;
				Quad[][] grid = quad.subdivide(Ni, Nj);
				for (int gi = 0; gi < Ni; gi++)
					for (int gj = 0; gj < Nj; gj++) {
						Point3d p = grid[gi][gj].getPoint();
						Point3i pi = MathToolset.tuple3dTo3i(p);
						if (!mask.hasPoint(pi) || mask.isMasked(pi)) 
							continue;

						
						GLMaterial[] materials = new GLMaterial[materialCount];
						for (int i = 0; i < materialCount; i++)
							materials[i] = new GLMaterial(baseMaterial);

//						if (useColorInterpolation) {
//							assign(grid[gi][gj].p1, materials[0]);
//							assign(grid[gi][gj].p2, materials[1]);
//							assign(grid[gi][gj].p3, materials[2]);
//							assign(grid[gi][gj].p4, materials[3]);
//							grid[gi][gj].setMaterials(materials);
//						}
//						else {
//							assign(grid[gi][gj].getPoint(), materials[0]);
//							grid[gi][gj].setMaterials(materials);
//							grid[gi][gj].setColor(materials[0].diffuse);
//						}
						assign(grid[gi][gj].p1, materials[0]);
						assign(grid[gi][gj].p2, materials[1]);
						assign(grid[gi][gj].p3, materials[2]);
						assign(grid[gi][gj].p4, materials[3]);
						grid[gi][gj].setMaterials(materials);
						grid[gi][gj].setColor(materials[0].diffuse);

						helicalQuads.add(grid[gi][gj]);
					}
				
			}
			
			previous = point;
		}
		
		this.helicalQuads = helicalQuads;
		
		vbo = new VertexBufferObject(volume, asVoxelMask(), colorMap.getMap());
		
		vboAll = new VertexBufferObject(volume.extract(box), mask.extract(box), colorMap.getMap());
		
		displayBlocked = displayBlockedCached;
	}
	
	/**
	 * Returns a unique list of voxels traversed by this helical volume.
	 * @return
	 */
	public List<Point3i> asVoxels() {
		HashSet<Point3i> voxels = new HashSet<Point3i>();
		List<Point3i> voxelPts = new LinkedList<Point3i>();
		
		for (Quad quad : helicalQuads) {
			Point3i pt = MathToolset.tuple3dTo3i(quad.getPoint());
			if (!voxels.contains(pt)) {
				voxelPts.add(pt);
			}
			voxels.add(pt);
		}

		return voxelPts;
	}
	
	public IntensityVolumeMask asVoxelMask() {
		IntensityVolumeMask hmask = new IntensityVolumeMask(volume.getDimension()); 
		for (Point3i pt : asVoxels()) {
			hmask.set(pt, 1);
		}
		
		return hmask;
	}
	
	protected void assign(Point3d p, GLMaterial material) {
		double data = volume.get(MathToolset.roundInt(p.x),
				MathToolset.roundInt(p.y), MathToolset.roundInt(p.z));
		Color c = colorMap.colorize(data);
		if (c == null)
			return;
		material.diffuse[0] = c.getRed() / 255f;
		material.diffuse[1] = c.getGreen() / 255f;
		material.diffuse[2] = c.getBlue() / 255f;
		material.diffuse[3] = (float) transparency;
	}

	public VoxelBox getBox() {
		return box;
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		if (displayBlocked || helix == null || helicalQuads == null || box == null)
			return;
		
		GL2 gl = drawable.getGL().getGL2();
		

		gl.glPushMatrix();
		box.applyHeartNormalization(gl);

//		WorldAxis.display(gl);

//		gl.glDisable(GL2.GL_LIGHTING);
		
		if (displayHelix) {
			// Draw midpoints
			gl.glDisable(GL2.GL_LIGHTING);
			gl.glPointSize(10f);
			gl.glBegin(GL2.GL_POINTS);
			int i = 0;
			for (Triplet<Point3d, Point3d, Point3d> point : helix) {
				Point3d p = point.getCenter();
				if (p == null)
					continue;

				double lambda = i++ / ((double) helix.size());
				gl.glColor4d(1 - lambda, 0, lambda, 0.8);
				gl.glVertex3d(p.x, p.y, p.z);
			}
			gl.glEnd();
		}

		if (display && helicalQuads != null) {
			
			if (vbo != null && !useLinearInterpolation) {
				if (displayAll) {
//					box.applyHeartNormalization(gl);
//			        Point3d pcenter = box.getCenter();
//			        pcenter.negate();
//			        Point3i span = box.getSpan();
//			        double md = Math.max(Math.max(span.x, span.y), span.z);
//			        gl.glScaled(1d / md, 1d / md, 1d / md);
//			        gl.glTranslated(0, 0, pcenter.z);

					vboAll.display(drawable);
				}
				else
					vbo.display(drawable);
					
			}
			else if (useLinearInterpolation) {
				if (useColorInterpolation)
					gl.glEnable(GL2.GL_LIGHTING);
				else {
					gl.glDisable(GL2.GL_CULL_FACE);
					gl.glDisable(GL2.GL_LIGHTING);
				}

				for (Quad quad : helicalQuads) {
					// gl.glColor3d(1,s 1, 1);
					if (quad != null)
						if (useColorInterpolation)
							quad.displayMaterials(drawable);
						else {
							quad.display(drawable, displayOutline);
						}
				}
			}
		}

		gl.glPopMatrix();
	}
	@Override
	public void init(GLAutoDrawable drawable) {
		// Nothing to do
	}

	@Override
	public String getName() {
		return "helical slicer";
	}
	
	public JColorizer getColorizer() {
		return colorMap;
	}

	public static void main(String[] args) {
		NodeScene scene = new NodeScene();
		
    	Heart heart = new HeartManager(false).loadHeart(new HeartDefinition("rat07052008", Species.Rat,
				"./data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/"));
//    	Heart heart = new HeartManager(false).loadHeart(new HeartDefinition("rat07052008", Species.Rat,
//				"./data/heart/rat_samples/sampleSubset/"));
    	
		HelicalSlicer slicer = new HelicalSlicer(heart.getFrameField().getF1().getX(), heart.getMask(), heart.getVoxelBox());
		slicer.sliceVolume();
		scene.addNode(slicer);
		
		scene.addControl(slicer.getColorizer());
	}
	
	public void setDisplayAll(boolean b) {
		this.displayAll = b;
	}
}
