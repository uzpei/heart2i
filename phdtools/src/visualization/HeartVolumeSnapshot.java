package visualization;

import gl.geometry.GLSimpleGeometry;
import gl.renderer.NodeScene;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JLabel;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3d;

import projects.ipmi2015.VectorColumn;
import app.dti.DiffusionSplitter;
import math.coloring.ColorMap;
import math.coloring.ColorMap.Map;
import swing.component.VerticalFlowPanel;
import swing.component.histogram.Histogram;
import swing.component.histogram.JHistogram;
import tools.geom.MathToolset;
import visualization.helical.HelicalSlicer;
import visualization.helical.HelicalSlicerWithFrameField;
import volume.IntensityVolume;
import volume.IntensityVolumeMask;
import voxel.VoxelFrameField;

public class HeartVolumeSnapshot {

	public boolean shotRequest = false;
	
	public static void main(String[] args) {
		int n = 50;
		int sx = n;
		int sy = n;
		int sz = n;
		String filepath = "./test.png";
		double scale = 20;
//		HeartVolumeSnapshot.save(IntensityVolume.createRandomizedVolume(sx, sy, sz), Map.HOT, -1, 1, scale, filepath);
		IntensityVolume volume = IntensityVolume.createRandomizedVolume(sx, sy, sz);
		HeartVolumeSnapshot.save(volume, Map.HOT, 0, 1, scale, filepath, false);
	}

	public static void save(final IntensityVolume volume, ColorMap.Map map, double min, double max, final double scale, String filepath, boolean closeWhenFinished) {
		save(volume, map, min, max, scale, true, filepath, closeWhenFinished);
	}
	
	public static void save(final IntensityVolume volume, ColorMap.Map map, double min, double max, final double scale, boolean helicalVolume, String filepath, boolean closeWhenFinished) {
		IntensityVolumeMask mask = new IntensityVolumeMask(volume);
		int sx = mask.getDimension()[0];
		int sy = mask.getDimension()[1];
		int sz = mask.getDimension()[2];
		
		// Remove x > sx/2 && y > sy/2 && z > sz / 2
		for (int x = sx / 2; x < sx; x++)
			for (int y = 0; y < sy; y++)
				for (int z = sz / 2; z < sz; z++)
					mask.set(x, y, z, 0);
		

		save(volume, mask, map, min, max, scale, false, filepath, closeWhenFinished);
	}
	
	public static void save(final IntensityVolume volume, final IntensityVolumeMask mask, ColorMap.Map map, double min, double max, final double scale, String filepath, boolean closeWhenFinished) {
		save(volume, mask, map, min, max, scale, true, filepath, closeWhenFinished);
	}

	public static void save(final IntensityVolume volume, final IntensityVolumeMask mask, ColorMap.Map map, double min, double max, final double scale, boolean helicalSampling, String filepath, boolean closeWhenFinished) {
		save(volume, mask, map, min, max, scale, helicalSampling, filepath, closeWhenFinished, null);
	}
	
	public static void save(final IntensityVolume volume, final IntensityVolumeMask mask, ColorMap.Map map, double min, double max, final double scale, boolean helicalSampling, String filepath, boolean closeWhenFinished, final VectorColumn column) {
		// Create control panel
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		// Create one histogram for display (with values thresholded within range [min, max]
		// and one for computing stats (unbounded)
		double[] data = volume.flattenCompact(mask);
		Histogram histogram = new Histogram(data);
		JHistogram histogramVisualization = new JHistogram(new Histogram(MathToolset.threshold(data, min, max)), new Dimension(300, 200), true);
		histogramVisualization.setShowGrids(true);
		vfp.add(histogramVisualization);

		// Create the helical slicer
//		final HelicalSlicer slicer = new HelicalSlicer(volume, map);
		final HelicalSlicer slicer = new HelicalSlicer(volume, mask, map);
		slicer.setDisplayAll(!helicalSampling);
		slicer.getColorizer().setMinMax(min, max);
		slicer.getColorizer().setPickerVisible(false);
		
		// Add some info
		VerticalFlowPanel vfpStats = new VerticalFlowPanel("Statistics");
		JLabel lblCount = new JLabel("voxel count = " + MathToolset.toString(histogram.getData().length, 2));
		JLabel lblMean = new JLabel("mean = " + MathToolset.toString(histogram.getMean(), 2));
		JLabel lblStd = new JLabel("std = " + MathToolset.toString(histogram.getStd(), 2));
		JLabel lblMinMax = new JLabel("min/max = " + MathToolset.toString(histogram.getMinValue(), 2) + "/" + MathToolset.toString(histogram.getMaxValue(), 2));
		vfpStats.add(lblCount);
		vfpStats.add(lblMean);
		vfpStats.add(lblStd);
		vfpStats.add(lblMinMax);
		vfpStats.add(slicer.getColorizer());
		vfp.add(vfpStats.getPanel());

//		vfp.add(slicer.getColorizer());
		
		// Create the scene
		NodeScene scene = new NodeScene(true, 0, false, false);

		// Setup UI
        scene.getRenderer().getCamera().setRotation(290, 270);
        scene.getRenderer().setShowLogger(false);
        
        // Add geometry
		scene.addNode(new GLSimpleGeometry() {
			@Override
			public void display(GLAutoDrawable drawable) {
				// Zoom out such that the whole volume fits
				GL2 gl = drawable.getGL().getGL2();
				double gscale = scale / volume.getMaxDimension();
				gl.glScaled(gscale, gscale, gscale);
				slicer.display(drawable);
				
				gl.glPushMatrix();
				slicer.getBox().applyHeartNormalization(gl);
				column.display(drawable);
				gl.glPopMatrix();
			}
		});
		
		// Add controls
		scene.addControl(vfp.getPanel());
		
		try {
			// Wait a few frames to make sure everything is in order
			scene.getRenderer().waitForFrames(10);

			/*
			 * Save snapshot
			 */
			// extract filename and filepath (when applicable)
			int extind = filepath.lastIndexOf('.');
			String filename = filepath;
			String fileext = "";
			if (extind > -1) {
				filename = filepath.substring(0, extind);
				fileext = filepath.substring(extind, filepath.length());
			}

			// Wait a few frames to make sure the image got saved
			int numBufferWait = 5;
			scene.getRenderer().setScreenshotControlRequest(filename + "_controls" + fileext);
			scene.getRenderer().waitForFrames(numBufferWait);
			
			scene.getRenderer().setControlVisibility(false);
			scene.getRenderer().waitForFrames(numBufferWait);
			scene.getRenderer().setScreenshotRequest(filename + fileext);
			scene.getRenderer().waitForFrames(numBufferWait);
			
			/*
			 * Save colormap
			 */
			Container c = slicer.getColorizer();
			BufferedImage im = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
			c.paint(im.getGraphics());
			try {
				ImageIO.write(im, "png", new File(filename + "_color.png"));
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			// Done
			if (closeWhenFinished)
				scene.getRenderer().stop(false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @param volume
	 * @param map
	 * @param min
	 * @param max
	 * @param scale [0, 100]
	 * @param filepath
	 */
	public static void save(final VoxelFrameField frameField, final IntensityVolume backgroundVolume, final double scale, final double min, final double max, String filepath, boolean closeWhenFinished) {
		// Create control panel
		VerticalFlowPanel vfp = new VerticalFlowPanel();
		
		// Create the helical slicer
		VerticalFlowPanel vfpStats = new VerticalFlowPanel("Statistics");
		final HelicalSlicerWithFrameField slicer = new HelicalSlicerWithFrameField(backgroundVolume, frameField.getMask(), frameField.getF1().getVoxelBox(), frameField.getF1());
//		slicer.getColorizer().setMap(ColorMap.Map.JET);
		slicer.getColorizer().setMinMax(min, max);
		slicer.getColorizer().setPickerVisible(false);

		vfpStats.add(slicer.getColorizer());
		vfp.add(vfpStats.getPanel());
		
//		vfp.add(slicer.getColorizer());
		
		// Create the scene
		NodeScene scene = new NodeScene(true);
		
		// Setup UI
        scene.getRenderer().getCamera().setRotation(290, 270);
        scene.getRenderer().setShowLogger(false);
        
        // Add geometry
		scene.addNode(new GLSimpleGeometry() {
			@Override
			public void display(GLAutoDrawable drawable) {
				// Zoom out such that the whole volume fits
				GL2 gl = drawable.getGL().getGL2();
				double gscale = 1;
				gscale = scale / frameField.getMask().getMaxDimension();
				gl.glScaled(gscale, gscale, gscale);
				
				if (backgroundVolume != null)
					slicer.display(drawable);
				
				// TODO: remove this:
				frameField.getF1().getVoxelBox().applyHeartNormalization(gl);
				frameField.display(drawable);
			}
		});
		
		// Add controls
		scene.addControl(vfp.getPanel());
		
		try {
			// Wait a few frames to make sure everything is in order
			scene.getRenderer().waitForFrames(5);

			// Save
//			scene.getRenderer().setScreenshotControlRequest(filepath);
			scene.getRenderer().setScreenshotRequest(filepath);
			
			// Wait a few frames to make sure the image got saved
			scene.getRenderer().waitForFrames(10);

			// Done
			if (closeWhenFinished)
				scene.getRenderer().stop(false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
