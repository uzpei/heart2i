package visualization;

import gl.geometry.GLSimpleGeometry;
import gl.renderer.JoglRenderer.SizePreset;
import gl.renderer.NodeScene;
import heart.HeartTools;

import javax.media.opengl.GLAutoDrawable;

import diffgeom.DifferentialOneForm.MovingAxis;
import voxel.VoxelBox;
import voxel.VoxelFrameField;

public class FrameFieldVisualizer extends NodeScene {

	private VoxelFrameField field;
	
	public FrameFieldVisualizer(VoxelFrameField frameField) {
		super(true);
		super.getRenderer().updateSize(SizePreset.Medium);
		field = frameField;
		field.getField(MovingAxis.F1).setVisible(true);
		field.getField(MovingAxis.F2).setVisible(true);
		field.getField(MovingAxis.F3).setVisible(true);
		
		super.addNode(new GLSimpleGeometry() {
			
			@Override
			public void display(GLAutoDrawable drawable) {
				draw(drawable);
			}
		});
	}
	
	private void draw(GLAutoDrawable drawable) {
//		field = HeartTools.heartSynthetic().getFrameField();
//		field.getField(MovingAxis.F1).setVisible(true);
//		field.getField(MovingAxis.F2).setVisible(true);
//		field.getField(MovingAxis.F3).setVisible(true);

		VoxelBox box = field.getF1().getVoxelBox();
		box.applyViewTransformation(drawable.getGL().getGL2());
		box.display(drawable);
		field.display(drawable);
	}
	
	public static void main(String[] args) {
		new FrameFieldVisualizer(HeartTools.heartSynthetic().getFrameField());
	}
}
