+++ DifferentialOneForm.connectionFormsDefault

	2) set fitting parameter:  contains information for our optimizer
	CartanFittingParameter fittingParameter = new CartanFittingParameter(null, fittingMethod, useSeeds, numThreads, iterations, errorThreshold, 0, regularization, model, neighborhoodShape, nsize);
	fittingParameter.setBounds(null);
	
	3) create data placeholder for the optimization: fit parameter, frame field, and computation window
	FittingData fittingData = new FittingData(fittingParameter, frameField, new VoxelBox(frameField.getMask()));

	4) this will pass the parameters and also store the final result of our optimization
	FittingExperimentResult fittingResult = new FittingExperimentResult(fittingData);
	
	5) Launch fit & wait for completion
	CartanFitter.setVerboseLevel(3);
	CartanFitter fitter = new CartanFitter(fittingMethod);
	fitter.setOptimizerType(type);
	fitter.launchFit(fittingResult, true);

	6) validate
	PamiExperimentRunner.validate(fittingResult);
	final List<CartanParameterNode> nodes = fittingResult.getResultNodes();

+++ CartanFiter

	1) Get fitting data from the FittingExperimentResult
	
	2) Find voxels to compute
	
	3) Use direct computations as seeds
	
	4) CartanWorker worker = new CartanWorker(core + 1, fittingData.createOptimizer(optimizerType), seedpoints);
	
	5) Start all workers (worke.start(completion listener))
	
	
+++ CartanWorker: 
		
	1) Run: optimize all connections 
	for (CartanParameterNode seed : seeds) {
		CartanParameterNode cartanFit = optimizer.optimize(seed);
		fittingResult.add(cartanFit);
	}
	
	
	
	