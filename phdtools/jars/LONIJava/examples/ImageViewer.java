/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * Example that illustrates how to use the LONI Image I/O plugins to display
 * the first image from a file.
 * <p>
 * NOTE:  Running requires the plugin corresponding to the format of the
 *        file whose image is being displayed.
 *
 * @version 21 April 2005
 */
public class ImageViewer
{
  /**
   * Constructs an Image Viewer.
   *
   * @param fileName Name of the file to read an image from.
   */
  public ImageViewer(String fileName)
    {
      try {

	// Test the file existence
	File file = new File(fileName);
	if ( !file.exists() || !file.isFile() || !file.canRead() ) {
	  System.out.println("Cannot read \"" + fileName + "\"");
	  System.exit(1);
	}

	// Find an Image Reader for the file
	ImageReader imageReader = _getImageReader(file);
	if (imageReader == null) {
	  System.out.println("Cannot decode \"" + fileName + "\"");
	  System.exit(1);
	}

	// Read the first image
	BufferedImage bufferedImage = imageReader.read(0);

        // Create a JFrame for the image
        JFrame frame = new JFrame(fileName);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Place the components in a Box Layout
        Box box = new Box(BoxLayout.Y_AXIS);
        frame.getContentPane().add(box);

        // Add the image
        box.add( new ImageComponent(bufferedImage) );

        // Display the JFrame
        frame.pack();
        frame.setSize(bufferedImage.getWidth(), bufferedImage.getHeight());
        frame.setVisible(true);
      }

      catch (Exception e) {
	e.printStackTrace();
	System.exit(1);
      }
    }

  /**
   * Main method.
   *
   * @param args Command line parameters.
   */
  public static void main(String[] args) 
    {
      if (args.length < 1) {
	System.out.println("ARGS:  <file>");
	System.exit(1);
      }

      // Read and display the first image from the file
      new ImageViewer(args[0]);
    }

  /**
   * Gets an Image Reader that can decode the specified file.
   *
   * @param file File to find an Image Reader for.
   *
   * @return Image Reader that can decode the specified file, or null if none
   *         was found.
   *
   * @throws IOException If an I/O error occurs.
   */
  private ImageReader _getImageReader(File file) throws IOException
    {
      Object input = new FileImageInputStream(file);
      Object origInput = input;

      // First look for a magic number in the header
      Iterator iter = ImageIO.getImageReaders(input);

      // No Image Reader is found
      if ( !iter.hasNext() ) {

	// Then look in the directory of the file (e.g., ANALYZE, AFNI)
	input = file;
	iter = ImageIO.getImageReaders(input);

	// No Image Reader is found
	if( !iter.hasNext() ) {
	  input = origInput;

	  // Then look at the file name suffix
	  String temp = file.getName();
	  String[] strings = temp.split("\\.");
	  if (strings.length > 1){
	    iter = ImageIO.getImageReadersBySuffix(strings[strings.length-1]);
	  }

	  // No Image Reader found
	  if ( !iter.hasNext() ) { return null; }
	}
      }

      // Set the Input Stream of the first Image Reader returned
      ImageReader imageReader = (ImageReader)iter.next();
      imageReader.setInput(input);

      // Return the Image Reader
      return imageReader;
    }

  /** JComponent used to display a Buffered Image. */
  private class ImageComponent extends JComponent
  {
    /** Buffered Image displayed in the JComponent. */
    private BufferedImage _bufferedImage;

    /**
     * Constructs an Image Component.
     *
     * @param bufferedImage Buffered Image displayed in the JComponent.
     */
    public ImageComponent(BufferedImage bufferedImage)
      {
	_bufferedImage = bufferedImage;
      }

    /**
     * Paints the image into the specified Graphics.
     *
     * @param g Graphics to paint into.
     */
    public void paint(Graphics g)
      {
	Graphics2D g2d = (Graphics2D)g;
	g2d.drawImage(_bufferedImage, 0, 0, null);
      }
  }
}
