import javax.vecmath.Vector3d;

/**
 * Example that illustrates how to use the LONI Image I/O plugins to read a MINC
 * file.
 * 
 * @author epiuze
 */
public class MINCVoxelVectorImage {

	private MINCImage ex, ey, ez;
	
	// Test some data
	public static void main(String[] args) {
		new MINCVoxelVectorImage("./data/e1x.mnc", "./data/e1y.mnc",
				"./data/e1z.mnc");
	}

	public MINCVoxelVectorImage(String filenameX, String filenameY, String filenameZ) {
		ex = new MINCImage(filenameX);
		ey = new MINCImage(filenameY);
		ez = new MINCImage(filenameZ);
	}
	
	public Vector3d getVector3d(int x, int y, int z) {
		return new Vector3d (ex.getIntensity(x, y, z), ey.getIntensity(x, y, z), ez.getIntensity(x, y, z));
	}

}
