/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import edu.ucla.loni.jdicom.plugin.DicomImageReaderSpi;
import edu.ucla.loni.jdicom.plugin.DicomStreamMetadataFormat;
import java.io.File;
import java.io.FileInputStream;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Example that illustrates how to use the LONI DICOM Image I/O plugin to
 * print a table of metadata from a DICOM file.
 * <p>
 * NOTE:  Compiling and running requires the DICOM plugin.
 *
 * @version 2 February 2007
 */
public class DicomMetadataTablePrinter
{
  /**
   * Constructs a DICOM Metadata Table Printer.
   *
   * @param fileName Name of the DICOM file to read metadata from.
   */
  public DicomMetadataTablePrinter(String fileName)
    {
      try {

	// Test the file existence
	File file = new File(fileName);
	if ( !file.exists() || !file.isFile() || !file.canRead() ) {
	  System.out.println("Cannot read \"" + fileName + "\"");
	  System.exit(1);
	}

	// Verify the file is a DICOM file
	FileInputStream inputStream = new FileInputStream(file);
	DicomImageReaderSpi spi = new DicomImageReaderSpi();
	if ( !spi.canDecodeInput(inputStream) ) {
	  System.out.println("Not a DICOM file.");
	  System.exit(1);
	}

	// Create the DICOM reader
	ImageReader reader = spi.createReaderInstance(null);
	inputStream = new FileInputStream(file);
	reader.setInput(inputStream, true, false);

	// Get the metadata
	IIOMetadata metadata = reader.getStreamMetadata();
	String formatName = metadata.getNativeMetadataFormatName();
	Node root = metadata.getAsTree(formatName);

	// Print the metadata in a tabular format
	Node dataSetNode = root.getFirstChild();
	_printTable(dataSetNode, 0);
      }

      catch (Exception e) {
	e.printStackTrace();
	System.exit(1);
      }
    }

  /**
   * Main method.
   *
   * @param args Command line parameters.
   */
  public static void main(String[] args) 
    {
      if (args.length < 1) {
	System.out.println("ARGS:  <DICOM file>");
	System.exit(1);
      }

      // Read metadata from the file and print as a table
      new DicomMetadataTablePrinter(args[0]);
    }

  /**
   * Prints the specified data set in tabular form.
   *
   * @param dataSetNode Data set Node.
   * @param level Level of indentation.
   */
  private void _printTable(Node dataSetNode, int level)
    {
      DicomStreamMetadataFormat fmt = DicomStreamMetadataFormat.getInstance();

      // Loop through the data elements
      NodeList elementNodes = dataSetNode.getChildNodes();
      for (int i = 0; i < elementNodes.getLength(); i++) {
	Node elementNode = elementNodes.item(i);

	// Tag
	_indent(level);
	System.out.print( elementNode.getNodeName() );
	System.out.print("\t");

	// VR
	NamedNodeMap attrs = elementNode.getAttributes();
	Node vrNode = attrs.getNamedItem(fmt.VR_NAME);
	String vr = vrNode.getNodeValue();
	System.out.print(vr);
	System.out.print("\t");

	// Description
	String desc = "<UNKNOWN>";
	try {desc = fmt.getElementDescription(elementNode.getNodeName(),null);}
	catch (Exception e) {}
	System.out.print(desc);
	System.out.print("\t");

	// VR = SQ
	if ( "SQ".equals(vr) ) {

	  // Print subtable
	  NodeList nestedNodes = elementNode.getChildNodes();
	  for (int j = 0; j < nestedNodes.getLength(); j++) {
	    System.out.println();
	    System.out.println();
	    _printTable(nestedNodes.item(j), level+1);
	  }
	}

	// VR != SQ
	else {

	  // Values
	  NodeList valueNodes = elementNode.getChildNodes();
	  for (int j = 0; j < valueNodes.getLength(); j++) {
	    NamedNodeMap vAttrs = valueNodes.item(j).getAttributes();
	    Node valueNode = vAttrs.getNamedItem(fmt.VALUE_ATTRIBUTE_NAME);

	    // VR = OB,OW,UN
	    if ( vr.startsWith("OB") || vr.startsWith("OW") ||
		 vr.startsWith("UN") )
	      {
		System.out.print("<BINARY>");
	      }
	    else { System.out.print( valueNode.getNodeValue() ); }

	    // Delimit
	    if (j < valueNodes.getLength()-1) { System.out.print("|"); }
	  }
	}

	System.out.println();
      }
    }

  /**
   * Prints spaces to indent a line.
   *
   * @param level Level requiring spaces; two spaces are printed at each level.
   */
  private void _indent(int level) 
    {
      for (int i = 0; i < level; i++) {
	System.out.print("  ");
      }
    } 
}
