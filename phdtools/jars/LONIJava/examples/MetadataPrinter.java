/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.FileImageInputStream;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Example that illustrates how to use the LONI Image I/O plugins to print an
 * XML tree of metadata from a file.
 * <p>
 * NOTE:  Running requires the plugin corresponding to the format of the
 *        file whose metadata is being printed.
 *
 * @version 21 April 2005
 */
public class MetadataPrinter
{
  /**
   * Constructs a Metadata Printer.
   *
   * @param fileName Name of the file to read metadata from.
   */
  public MetadataPrinter(String fileName)
    {
      try {

	// Test the file existence
	File file = new File(fileName);
	if ( !file.exists() || !file.isFile() || !file.canRead() ) {
	  System.out.println("Cannot read \"" + fileName + "\"");
	  System.exit(1);
	}

	// Find an Image Reader for the file
	ImageReader imageReader = _getImageReader(file);
	if (imageReader == null) {
	  System.out.println("Cannot decode \"" + fileName + "\"");
	  System.exit(1);
	}

	// Print the stream metadata
	System.out.println("STREAM METADATA:");
	_printTree( imageReader.getStreamMetadata() );

	// Print the image metadata
	for (int i = 0; i < imageReader.getNumImages(true); i++) {
	  System.out.println("\nIMAGE " + i + " METADATA:");
	  _printTree( imageReader.getImageMetadata(i) );
	}
      }

      catch (Exception e) {
	e.printStackTrace();
	System.exit(1);
      }
    }

  /**
   * Main method.
   *
   * @param args Command line parameters.
   */
  public static void main(String[] args) 
    {
      if (args.length < 1) {
	System.out.println("ARGS:  <file>");
	System.exit(1);
      }

      // Read metadata from the file and print as an XML tree
      new MetadataPrinter(args[0]);
    }

  /**
   * Gets an Image Reader that can decode the specified file.
   *
   * @param file File to find an Image Reader for.
   *
   * @return Image Reader that can decode the specified file, or null if none
   *         was found.
   *
   * @throws IOException If an I/O error occurs.
   */
  private ImageReader _getImageReader(File file) throws IOException
    {
      Object input = new FileImageInputStream(file);
      Object origInput = input;

      // First look for a magic number in the header
      Iterator iter = ImageIO.getImageReaders(input);

      // No Image Reader is found
      if ( !iter.hasNext() ) {

	// Then look in the directory of the file (e.g., ANALYZE, AFNI)
	input = file;
	iter = ImageIO.getImageReaders(input);

	// No Image Reader is found
	if( !iter.hasNext() ) {
	  input = origInput;

	  // Then look at the file name suffix
	  String temp = file.getName();
	  String[] strings = temp.split("\\.");
	  if (strings.length > 1){
	    iter = ImageIO.getImageReadersBySuffix(strings[strings.length-1]);
	  }

	  // No Image Reader found
	  if ( !iter.hasNext() ) { return null; }
	}
      }

      // Set the Input Stream of the first Image Reader returned
      ImageReader imageReader = (ImageReader)iter.next();
      imageReader.setInput(input);

      // Return the Image Reader
      return imageReader;
    }

  /**
   * Prints the native metadata in the specified II/O Metadata.
   *
   * @param metadata II/O Metadata to read from.
   */
  private void _printTree(IIOMetadata metadata)
    {
      // No metadata
      if (metadata == null) { return; }

      // Get the name of the native metadata tree
      String formatName = metadata.getNativeMetadataFormatName();

      // Extract the native metadata as a DOM Node
      Node root = metadata.getAsTree(formatName);

      // Print the DOM Node
      _printTree(root, 0);
    }

  /**
   * Display the metadata in XML format.  Copied from java.sun.com.
   *
   * @param node of the tree.
   * @param level of the node, like root, child, grandchild...
   */
  private void _printTree(Node node, int level) 
    {
      // emit open tag
      _indent(level);
      System.out.print("<" + node.getNodeName());

      // print attribute values
      NamedNodeMap map = node.getAttributes();
      if (map != null) { 
	int length = map.getLength();
	for (int i = 0; i < length; i++) {
	  Node attr = map.item(i);
	  System.out.print(" " + attr.getNodeName() +
			   "=\"" + attr.getNodeValue() + "\"");
	}
      }

      Node child = node.getFirstChild();
      if (child != null) {

	// close current tag
	System.out.println(">");

	// emit child tags recursively
	while (child != null) { 
	  _printTree(child, level + 1);
	  child = child.getNextSibling();
	}

	// emit close tag
	_indent(level);
	System.out.println("</" + node.getNodeName() + ">");
      } else {
	System.out.println("/>");
      }
    }

  /**
   * Indent a few spaces for sublevel data.  Copied from java.sun.com.
   *
   * @param level of the node, like root, child, grandchild... 
   */
  private void _indent(int level) 
    {
      for (int i = 0; i < level; i++) {
	System.out.print("  ");
      }
    } 

}
