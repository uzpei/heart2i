import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.media.jai.opimage.MinCRIF;

import edu.ucla.loni.minc.plugin.MincImageReader;
import edu.ucla.loni.minc.plugin.MincImageReaderSpi;
import edu.ucla.loni.minc.plugin.MincMetadataFormat;


public class MINCImage {
	private BufferedImage[] slices;
	
	private MincImageReader reader;

	/**
	 * 
	 * @param filenameX
	 *            Name of the file to read an image from.
	 */

	public MINCImage(String filename) {
		System.out.println("Creating MINC image from " + filename + " ...");
		try {
			File file = new File(filename);

			// Get the metadata
			createReader(file);
			createData(reader);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private void createReader(File file) throws Exception {
		// Test the existence of the files
		if (!file.exists() || !file.isFile() || !file.canRead()) {
			System.out.println("Cannot read \"" + file.getName() + "\"");
			System.exit(1);
		}

		// Verify the file is a MINC file
		MincImageReaderSpi spi = new MincImageReaderSpi();
		ImageInputStream inputStream = ImageIO.createImageInputStream(file);
		if (!spi.canDecodeInput(inputStream)) {
			System.out.println("Not a MINC file.");
			System.exit(1);
		}

		// Create the MINC reader
		reader = (MincImageReader) spi
				.createReaderInstance(null);
		inputStream = ImageIO.createImageInputStream(file);
		reader.setInput(inputStream, true, false);
	}


	private void createData(MincImageReader reader) throws Exception {
		// Print some data
		System.out.println("------------------");
		int numImages = reader.getNumImages(true);
		System.out.println("Image format = " + reader.getFormatName());
		System.out.println("Number of images detected (third dimension) = "
				+ numImages);

		int minIndex = reader.getMinIndex();
		int height = reader.getHeight(minIndex);
		int width = reader.getWidth(minIndex);
		System.out.println("Image height for first slice = " + height);
		System.out.println("Image width  for first slice = " + width);

		slices = new BufferedImage[numImages];
		
		for (int z = 0; z < numImages; z++) {
			System.out.println("Processing slice #" + (z + 1) + "/" + numImages);
			BufferedImage img = reader.read(z);
			slices[z] = img;
		}
	}
	
	public BufferedImage[] getSlices() {
		return slices;
	}
	
	/**
	 * @param x coordinate
	 * @param y coordinate
	 * @param z coordinate
	 * @return the intensity at this voxel location.
	 */
	public int getIntensity(int x, int y, int z) {
		return (slices[z].getRGB(x, y) & 0x00FF0000) >> 16;
	}

	private void printData(MincImageReader reader) throws Exception {
		
		int numImages = reader.getNumImages(true);
		int minIndex = reader.getMinIndex();
		int height = reader.getHeight(minIndex);
		int width = reader.getWidth(minIndex);

		// Search for non-zero pixels
		for (int z = 0; z < numImages; z++) {
			System.out.println("Slice #" + (z + 1) + "/" + numImages);
			BufferedImage img = reader.read(z);
			// Raster raster = img.getData();
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					// Use the red channel as intensity
					int intensity = (img.getRGB(x, y) & 0x00FF0000) >> 16;
					System.out.println(intensity);
				}
			}
		}
	}
	/**
	 * Prints the specified data set in tabular form.
	 * 
	 * @param dataSetNode
	 *            Data set Node.
	 */
	public void printMetadata() {
		try {
			IIOMetadata metadata = reader.getStreamMetadata();
			String formatName = metadata.getNativeMetadataFormatName();
			Node dataSetNode = metadata.getAsTree(formatName);

			// Loop through the data elements
			NodeList elementNodes = dataSetNode.getChildNodes();
			for (int i = 0; i < elementNodes.getLength(); i++) {
				Node elementNode = elementNodes.item(i);
				printnode("", elementNode);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printnode(String padding, Node e) {
		if (e == null)
			return;

		String pad = "  ";

		System.out.print(padding + e.getNodeName());
		if (e.getNodeValue() != null)
			System.out.println(" = " + e.getNodeValue());
		else
			System.out.println();

		// Print attributes
		NamedNodeMap nm = e.getAttributes();
		for (int j = 0; j < nm.getLength(); j++) {
			Node n = nm.item(j);

			printnode(padding + pad, n);
		}

		// Print child nodes
		NodeList elementNodes = e.getChildNodes();
		for (int i = 0; i < elementNodes.getLength(); i++) {
			Node elementNode = elementNodes.item(i);
			printnode(padding + pad, elementNode);
		}
	}
}
