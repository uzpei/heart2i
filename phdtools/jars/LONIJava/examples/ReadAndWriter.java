/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Example that illustrates how to use the LONI Image I/O plugins to read
 * metadata and image data from a file and write them to another file.
 * <p>
 * NOTE:  Running requires the plugin corresponding to the format of the
 *        file whose metadata and image data are being rewritten.
 *
 * @version 21 April 2005
 */
public class ReadAndWriter
{
  /**
   * Constructs a Read-and-Writer.
   *
   * @param srcFileName Name of the file to read metadata and image data from.
   * @param tgtFileName Name of the file to write metadata and image data to.
   */
  public ReadAndWriter(String srcFileName, String tgtFileName)
    {
      try {

	// Test the source file existence
	File srcFile = new File(srcFileName);
	if ( !srcFile.exists() || !srcFile.isFile() || !srcFile.canRead() ) {
	  System.out.println("Cannot read \"" + srcFileName + "\"");
	  System.exit(1);
	}

	// Find an Image Reader for the source file
	ImageReader imageReader = _getImageReader(srcFile);
	if (imageReader == null) {
	  System.out.println("Cannot decode \"" + srcFileName + "\"");
	  System.exit(1);
	}

	// Find an Image Writer for the target file (same type as source)
	ImageWriter imageWriter = _getImageWriter(imageReader, tgtFileName);
	if (imageWriter == null) {
	  System.out.println("Cannot encode \"" + tgtFileName + "\"");
	  System.exit(1);
	}

	// Get empty stream metadata
	IIOMetadata tgtMetadata = imageWriter.getDefaultStreamMetadata(null);

	// If available, read the stream metadata
	IIOMetadata srcMetadata = imageReader.getStreamMetadata();
	if (srcMetadata != null) {
	  String formatName = srcMetadata.getNativeMetadataFormatName();
	  Node root = srcMetadata.getAsTree(formatName);

	  // YOU CAN CHANGE THE STREAM METADATA HERE --> root

	  // Merge the source metadata with the empty target metadata
	  tgtMetadata.mergeTree(formatName, root);
	}

	// Write the stream metadata
	imageWriter.prepareWriteSequence(tgtMetadata);

	// Read/write the image data
	for (int i = 0; i < imageReader.getNumImages(true); i++) {

	  // Get empty image metadata
	  tgtMetadata = imageWriter.getDefaultImageMetadata(null, null);

	  // If available, read the image metadata
	  srcMetadata = imageReader.getImageMetadata(i);
	  if (srcMetadata != null) {
	    String formatName = srcMetadata.getNativeMetadataFormatName();
	    Node root = srcMetadata.getAsTree(formatName);

	    // YOU CAN CHANGE THE IMAGE METADATA HERE --> root

	    // Merge the source metadata with the empty target metadata
	    tgtMetadata.mergeTree(formatName, root);
	  }

	  // Read/write the image data and image metadata
	  BufferedImage srcImage = imageReader.read(i);
	  IIOImage iioImage = new IIOImage(srcImage, null, tgtMetadata);
	  imageWriter.writeToSequence(iioImage, null);
	}

	// Finish writing
	imageWriter.endWriteSequence();
	imageWriter.dispose();
      }

      catch (Exception e) {
	e.printStackTrace();
	System.exit(1);
      }
    }

  /**
   * Main method.
   *
   * @param args Command line parameters.
   */
  public static void main(String[] args) 
    {
      if (args.length < 2) {
	System.out.println("ARGS:  <source file> <target file>");
	System.exit(1);
      }

      // Read metadata and image data from the source and write to the target
      new ReadAndWriter(args[0], args[1]);
    }

  /**
   * Gets an Image Reader that can decode the specified file.
   *
   * @param file File to find an Image Reader for.
   *
   * @return Image Reader that can decode the specified file, or null if none
   *         was found.
   *
   * @throws IOException If an I/O error occurs.
   */
  private ImageReader _getImageReader(File file) throws IOException
    {
      Object input = new FileImageInputStream(file);
      Object origInput = input;

      // First look for a magic number in the header
      Iterator iter = ImageIO.getImageReaders(input);

      // No Image Reader is found
      if ( !iter.hasNext() ) {

	// Then look in the directory of the file (e.g., ANALYZE, AFNI)
	input = file;
	iter = ImageIO.getImageReaders(input);

	// No Image Reader is found
	if( !iter.hasNext() ) {
	  input = origInput;

	  // Then look at the file name suffix
	  String temp = file.getName();
	  String[] strings = temp.split("\\.");
	  if (strings.length > 1){
	    iter = ImageIO.getImageReadersBySuffix(strings[strings.length-1]);
	  }

	  // No Image Reader found
	  if ( !iter.hasNext() ) { return null; }
	}
      }

      // Set the Input Stream of the first Image Reader returned
      ImageReader imageReader = (ImageReader)iter.next();
      imageReader.setInput(input);

      // Return the Image Reader
      return imageReader;
    }

  /**
   * Gets an Image Writer that can write images read by the specified Image
   * Reader.
   *
   * @param imageReader Image Reader to find an Image Writer for.
   * @param baseFileName Base name used for the output files.
   *
   * @return Image Writer that can write images or null if none was found.
   *
   * @throws IOException If an I/O error occurs.
   */
  private ImageWriter _getImageWriter(ImageReader imageReader,
				      String baseFileName) throws IOException
    {
      // Find an Image Writer for the Image Reader
      ImageWriter imageWriter = ImageIO.getImageWriter(imageReader);
      if (imageWriter == null) { return null; }

      // Look through the allowed output types
      ImageWriterSpi writerSpi = imageWriter.getOriginatingProvider();
      Class[] outputTypes = writerSpi.getOutputTypes();
      for (int i = 0; i < outputTypes.length; i++) {

	// One output file
	if ( outputTypes[i] == ImageOutputStream.class ) {
	  File outputFile = new File(baseFileName);
	  imageWriter.setOutput( new FileImageOutputStream(outputFile) );
	  return imageWriter;
	}

	// Two output files (AFNI, ANALYZE, ...)
	if ( outputTypes[i] == Vector.class ) {
	  Vector outputStreams = new Vector();

	  // Create HDR and IMG output files
	  File hdrFile = new File(baseFileName + ".hdr");
	  File imgFile = new File(baseFileName + ".img");
	  outputStreams.add( new FileImageOutputStream(hdrFile) );
	  outputStreams.add( new FileImageOutputStream(imgFile) );

	  imageWriter.setOutput(outputStreams);
	  return imageWriter;
	}
      }

      // Unsupported output types
      return null;
    }
}
