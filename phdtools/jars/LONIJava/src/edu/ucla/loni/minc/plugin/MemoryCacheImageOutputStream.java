/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.minc.plugin;

import java.io.IOException;
import javax.imageio.stream.ImageOutputStreamImpl;

/**
 * Derivation of Sun's Memory Cache Image Output Stream that more conservatively
 * manages memory.
 *
 * @version 29 November 2005
 */
public class MemoryCacheImageOutputStream extends ImageOutputStreamImpl
{
    /** Memory Cache used to store data in memory. */
    private MemoryCache _cache;

    /** Constructs a Memory Cache Image Output Stream. */
    public MemoryCacheImageOutputStream()
    {
	super();

	_cache = new MemoryCache();
    }

    /**
     * Reads one byte from the stream.
     *
     * @return Byte from the stream, or -1 if the end of the stream is reached.
     *
     * @throws IOException If the stream is closed or if a reading error occurs.
     */
    public int read() throws IOException
    {
        checkClosed();

        bitOffset = 0;

        int val = _cache.read(streamPos);
        if (val != -1) {
            ++streamPos;
        }
        return val;
    }

    /**
     * Reads up to the specified number of bytes from the stream and stores
     * them in the byte array starting at the offset.
     *
     * @param b Byte array to copy into.
     * @param off Offset into the byte array to start copying.
     * @param len Number of bytes to read.
     *
     * @return Number of bytes actually read, or -1 if the end of the stream is
     *         reached.
     *
     * @throws IOException If a reading error occurs.
     * @throws IndexOutOfBoundsException If the offset or length is invalid, or
     *                                   if any portion of the data is not in
     *                                   the cache.
     * @throws NullPointerException If the byte array is null.
     */
    public int read(byte[] b, int off, int len) throws IOException
    {
	// Check offset and length validity
        if (off < 0 || len < 0 || off + len > b.length || off + len < 0) {
            throw new IndexOutOfBoundsException
                ("off < 0 || len < 0 || off + len > b.length!");
        }

        bitOffset = 0;

        if (len == 0) {
            return 0;
        }

        // check if we're already at/past EOF i.e.
        // no more bytes left to read from cache
        long bytesLeftInCache = _cache.getLength() - streamPos;
        if (bytesLeftInCache <= 0) {
            return -1; // EOF
        }

        // guaranteed by now that bytesLeftInCache > 0 && len > 0
        // and so the rest of the error checking is done by cache.read()
        len = (int)Math.min(bytesLeftInCache, (long)len);
        _cache.read(b, off, len, streamPos);
        streamPos += len;
        return len;
    }

    /**
     * Writes a single byte to the stream.
     *
     * @param b Integer whose 8 least significant bits will be written.
     *
     * @throws IOException If a writing error occurs.
     */
    public void write(int b) throws IOException
    {
        checkClosed();
        flushBits();
        _cache.write(b, streamPos);
        ++streamPos;
    }

    /**
     * Writes to the cache from a byte array.
     *
     * @param b Array of bytes containing data to be written.
     * @param off Starting offset within the data array.
     * @param len Number of bytes to be written.
     *
     * @throws IOException If a writing error occurs.
     * @throws IndexOutOfBoundsException If the offset or lengths are invalid.
     * @throws NullPointerException If the array is null.
     */
    public void write(byte[] b, int off, int len) throws IOException
    {
        checkClosed();
        flushBits();
        _cache.write(b, off, len, streamPos);
        streamPos += len;
    }

    /**
     * Gets the total length of the stream.
     *
     * @return Total length of the stream.
     */
    public long length()
    {
        return _cache.getLength();
    }

    /**
     * Determines if this stream caches data in order to allow seeking
     * backwards.
     *
     * @return True if this stream caches data in order to allow seeking
     *         backwards; false otherwise.
     */
    public boolean isCached()
    {
        return true;
    }

    /**
     * Determines if this stream maintains a file cache.
     *
     * @return True if this stream maintains a file cache; false otherwise.
     */
    public boolean isCachedFile()
    {
        return false;
    }

    /**
     * Determines if this stream maintains a memory cache.
     *
     * @return True if this stream maintains a memory cache; false otherwise.
     */
    public boolean isCachedMemory()
    {
        return true;
    }

    /** Closes this stream, flushes pending data, and releases resources. */
    public void close() throws IOException
    {
	_cache.reset();
	super.close();
    }
}
