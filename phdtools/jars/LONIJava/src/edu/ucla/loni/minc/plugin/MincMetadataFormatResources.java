/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.minc.plugin;

import java.util.ListResourceBundle;

/**
 * List Resource Bundle that provides descriptions for MINC elements.
 *
 * @version 16 January 2003
 */
public class MincMetadataFormatResources extends ListResourceBundle
{
  /** Constructs a MincMetadataFormatResources. */
  public MincMetadataFormatResources()
    {
    }

  /**
   * Gets the contents of the Resource Bundle.
   *
   * @return Object array of the contents, where each item of the array is a
   *         pair of Objects.  The first element of each pair is the key, which
   *         must be a String, and the second element is the value associated
   *         with that key.
   */
  public Object[][] getContents()
    {
      String formatName = MincMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      String dimensionsName = MincMetadataFormat.DIMENSIONS_NAME;
      String globalAttrsName = MincMetadataFormat.GLOBAL_ATTRIBUTES_NAME;
      String variablesName = MincMetadataFormat.VARIABLES_NAME;
      String attributesName = MincMetadataFormat.ATTRIBUTES_NAME;
      String typeName = MincMetadataFormat.TYPE_NAME;
      String attributeValueName = MincMetadataFormat.ATTRIBUTE_VALUE;
      String valueName = MincMetadataFormat.VALUE_NAME;
      String dataName = MincMetadataFormat.DATA_NAME;
      String dataArrayName = MincMetadataFormat.DATA_ARRAY_NAME;
      String dimensionName = MincMetadataFormat.DIMENSION_NAME;
      String dataElementName = MincMetadataFormat.DATA_ELEMENT_NAME;
      String miImageName = MincMetadataFormat.MI_IMAGE;

      Object[][] contents = {

	// Node name, followed by description
	{formatName, "MINC stream metadata."},
	{dimensionsName, "Dimensions used by all the variables."},
	{globalAttrsName, "Attributes that apply globally."},
	{variablesName, "Sets of attributes and data."},
	{attributesName, "Attributes of the variable."},
	{attributeValueName, "Attribute value."},
	{dataName, "Data of the variable."},
	{dataArrayName, "Array of data."},
	{dataElementName, "Element of data."},
	{miImageName, "Image variable"},

	// NetCDF standard attributes
	{"units", "Specifies the units of the variable values."},
	{"long_name", "A textual description of the variable for human " +
	 "consumption."},
	{"valid_range", "A vector of two numbers specifying the minimum and " +
	 "maximum valid values."},
	{"valid_max", "A number giving the valid maximum for the variable."},
	{"valid_min", "A number giving the valid minimum for the variable."},
	{"_FillValue", "Number to be used for filling in values in the " +
	 "variable that are not explicitly written."},
	{"title", "A global string that provides a description of what is " +
	 "in the file."},
	{"history", "A global string that should store one line for each " +
	 "program that has modified the file."},

	// MINC general attributes
	{"vartype", "A string identifying the type of variable."},
	{"varid", "A string that identifies the origin of the variable."},
	{"signtype", "Sign of the variable values."},
	{"parent", "A string identifying by name the parent variable of " +
	 "this variable."},
	{"children", "A newline-separated list of the children of this " +
	 "variable."},
	{"comments", "Any text that should be included with this variable."},
	{"version", "A string identifying the file version."},

	// Dimensions and dimension variables
	{"xspace", "Dimension and coordinate variable for x axis."},
	{"yspace", "Dimension and coordinate variable for y axis."},
	{"zspace", "Dimension and coordinate variable for z axis."},
	{"time", "Dimension and coordinate for variable time axis."},
	{"tfrequency", "Dimension and coordinate variable for temporal " +
	 "frequency."},
	{"xfrequency", "Dimension and coordinate variable for spatial " +
	 "frequency along the x axis."},
	{"yfrequency", "Dimension and coordinate variable for spatial " +
	 "frequency along the y axis."},
	{"zfrequency", "Dimension and coordinate variable for spatial " +
	 "frequency along the z axis."},
	{"vector_dimension", "Dimension only for components of a vector " +
	 "field."},
	{"xspace-width", "Width of samples along x axis."},
	{"yspace-width", "Width of samples along y axis."},
	{"zspace-width", "Width of samples along z axis."},
	{"time-width", "Width of samples along time axis."},
	{"tfrequency-width", "Width of samples along temporal frequency " +
	 "axis."},
	{"xfrequency-width", "Width of samples along x spatial frequency " +
	 "axis."},
	{"yfrequency-width", "Width of samples along y spatial frequency " +
	 "axis."},
	{"zfrequency-width", "Width of samples along z spatial frequency " +
	 "axis."},
	{"spacing", "Regular or irregular spacing of samples."},
	{"step", "A number indicating the step between samples for regular " +
	 "spacing."},
	{"start", "The coordinate of the index 0 of the dimension."},
	{"spacetype", "A string identifying the type of coordinate space."},
	{"alignment", "A string indicating the position of the coordinates " +
	 "relative to each sample."},
	{"direction_cosines", "A vector with 3 elements giving the " +
	 "direction cosines of the axes."},
	{"width", "For regularly dimension widths, this numeric attribute " +
	 "gives the FWHM width of all samples."},
	{"filtertype", "A string specifying the shape of the convolving " +
	 "filter."},

	// Root variable
	{"rootvariable", "Root of the data hierarchy."},

	// Image variable
	{"image", "Used to store the image data."},
	{"image-max", "A variable attribute that stores the real value " +
	 "maximum for the image."},
	{"image-min", "A variable attribute that stores the real value " +
	 "minimum for the image."},
	{"complete", "A boolean attribute that indicates whether the " +
	 "variable has been written in its entirety."},

	// Patient variable
	{"patient", "Group variable that serves to group together " +
	 "information about the patient."},
	{"full_name", "A string specifying the full name of the patient."},
	{"other_names", "A string giving other names for the patient."},
	{"identification", "A string specifying identification information " +
	 "for the patient."},
	{"other_ids", "A string giving other id's."},
	{"birthdate", "A string specifying the patient's birthdate."},
	{"sex", "A string specifying the patient's sex."},
	{"age", "A number giving the patient's age."},
	{"weight", "The patient's weight in kilograms."},
	{"size", "The patient's height or length in metres."},
	{"address", "A string giving the patient's address."},
	{"insurance_id", "A string giving the patient's insurance plan id."},

	// Study variable
	{"study", "Group variabe that contains information about the study."},
	{"start_time", "String giving time (and date) of start of the study."},
	{"start_year", "Integer giving year of start."},
	{"start_month", "Integer giving month (1-12) of start."},
	{"start_day", "Integer giving day (1-31) of start."},
	{"start_hour", "Integer giving hour (0-23) of start."},
	{"start_minute", "Integer giving minute (0-59) of start."},
	{"start_seconds", "Floating-point value giving seconds of start."},
	{"modality", "Imaging modality."},
	{"manufacturer", "String giving name of device manufacturer."},
	{"device_model", "String identifying device model."},
	{"institution", "String identifying institution."},
	{"department", "String identifying department."},
	{"station_id", "String identifying machine that generated the " +
	 "images."},
	{"referring_physician", "Name of patient's primary physician."},
	{"attending_physician", "Physician administering the examination."},
	{"radiologist", "Radiologist interpreting the examination."},
	{"operator", "Name of technologist operating scanner."},
	{"admitting_diagnosis", "String description of admitting diagnosis."},
	{"procedure", "String description of the procedure employed."},
	{"study_id", "String identifying study."},

	// Acquisition variable
	{"acquisition", "Group variable that contains information about the " +
	 "acquisition."},
	{"protocol", "String description of the protocol for image " +
	 "acquisition."},
	{"scanning_sequence", "String description of type of data taken."},
	{"repetition_time", "Time in seconds between pulse sequences."},
	{"echo_time", "Time in seconds between the middle of a 90 degree " +
	 "pulse and the middle of spin echo production."},
	{"inversion_time", "Time in seconds after the middle of the " +
	 "inverting RF pulse to the middle of the 90 degree pulse to " +
	 "detect the amount of longitudinal magnetization."},
	{"num_averages", "Number of times a given pulse sequence is " +
	 "repeated before any parameter is changed."},
	{"imaging_frequency", "Precession frequency in Hz of the imaged " +
	 "nucleus."},
	{"imaged_nucleus", "String specifying the nucleus that is resonant " +
	 "at the imaging frequency."},
	{"radionuclide", "String specifying the isotope administered."},
	{"contrast_agent", "String identifying the contrast or bolus agent."},
	{"radionuclide_halflife", "Half-life of radionuclide in seconds."},
	{"tracer", "String identifying tracer labelled with radionuclide " +
	 "that was injected."},
	{"injection_time", "String giving time (and date) of injection."},
	{"injection_year", "Integer giving year of injection."},
	{"injection_month", "Integer giving month (1-12) of injection."},
	{"injection_day", "Integer giving day (1-31) of injection."},
	{"injection_hour", "Integer giving hour (0-23) of injection."},
	{"injection_minute", "Integer giving minute (0-59) of injection."},
	{"injection_seconds", "Floating-point value giving seconds of " +
	 "injection."},
	{"injection_length", "Length of time of injection (in seconds)."},
	{"injection_dose", "Total dose of radionuclide or contrast agent " +
	 "injected."},
	{"dose_units", "String giving units of dose."},
	{"injection_volume", "Volume of injection in mL."},
	{"injection_route", "String identifying administration route of " +
	 "injection."},

	// Node name + "/" + attribute name, followed by description
	{"*/" + typeName, "Data type."},
	{attributeValueName + "/" + valueName, "Attribute value."},
	{dataElementName + "/" + valueName, "Data element value."},
	{dataArrayName + "/" + dimensionName, "Name of the array dimension."},

	// Dimension attributes
	{dimensionsName + "/xspace", "Dimension and coordinate variable " +
	 "for x axis."},
	{dimensionsName + "/yspace", "Dimension and coordinate variable " +
	 "for y axis."},
	{dimensionsName + "/zspace", "Dimension and coordinate variable " +
	 "for z axis."}
	};

      return contents;
    }
}
