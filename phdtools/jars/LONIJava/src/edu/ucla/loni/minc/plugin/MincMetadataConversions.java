/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.minc.plugin;

import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfStream;
import ucar.nc2.NetcdfStreamWriteable;
import ucar.nc2.Variable;
import ucar.ma2.Array;
import ucar.ma2.ArrayAbstract;

/**
 * Class that provides conversions between the NetCDF Stream classes and the
 * DOM Node representations of them.
 *
 * @version 30 June 2003
 */
public class MincMetadataConversions
{
  /** Name of the data type Attribute. */
  private static final String TYPE_NAME = MincMetadataFormat.TYPE_NAME;

  /** Name of the data value Attribute. */
  private static final String VALUE_NAME = MincMetadataFormat.VALUE_NAME;

  /** Name of the data dimension Attribute. */
  private static final String DIM_NAME = MincMetadataFormat.DIMENSION_NAME;

  /** Constructs a MincMetadataConversions. */
  private MincMetadataConversions()
    {
    }

  /**
   * Converts the NetCDF Stream to a tree.
   *
   * @param netcdfStream NetCDF Stream to convert.
   *
   * @return DOM Node representing the NetCDF Stream.
   */
  public static Node toTree(NetcdfStream netcdfStream)
    {
      // Create the root Node
      String rootName = MincMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      if (netcdfStream != null) {

	// Add the Dimensions Node
	String dimsName = MincMetadataFormat.DIMENSIONS_NAME;
	IIOMetadataNode dimsNode = new IIOMetadataNode(dimsName);
	rootNode.appendChild(dimsNode);

	// Use the Dimensions as attributes
	Iterator iter = netcdfStream.getDimensionIterator();
	while ( iter.hasNext() ) {
	  Dimension dimension = (Dimension)iter.next();

	  // Add the Dimension to the Dimension Node as an attribute
	  String name = dimension.getName();
	  String value = String.valueOf( dimension.getLength() );
	  dimsNode.setAttribute(name, value);
	}

	// Add the Global Attributes Node
	String gblAttrsName = MincMetadataFormat.GLOBAL_ATTRIBUTES_NAME;
	IIOMetadataNode gblAttrsNode = new IIOMetadataNode(gblAttrsName);
	rootNode.appendChild(gblAttrsNode);

	// Use the Attributes as children Nodes
	iter = netcdfStream.getGlobalAttributeIterator();
	while ( iter.hasNext() ) {
	  _addToTree( (Attribute)iter.next(), gblAttrsNode );
	}

	// Add the Variables Node
	String varsName = MincMetadataFormat.VARIABLES_NAME;
	IIOMetadataNode varsNode = new IIOMetadataNode(varsName);
	rootNode.appendChild(varsNode);

	// Use the Variables as children Nodes
	iter = netcdfStream.getVariableIterator();
	while ( iter.hasNext() ) {
	  Variable variable = (Variable)iter.next();
	  Array array = netcdfStream.getCachedData( variable.getName() );
	  _addToTree(variable, array, varsNode);
	}
      }

      // Return the root Node
      return rootNode;
    }

  /**
   * Converts the tree to a writable NetCDF Stream.
   *
   * @param root DOM Node representing the NetCDF Stream.
   *
   * @return Writable NetCDF Stream constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static NetcdfStreamWriteable toNetcdfStream(Node root)
    throws IIOInvalidTreeException
    {
      // Verify the name of the root Node
      String rootName = MincMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      _verifyName(root, rootName);

      // Create a Writable NetCDF Stream
      NetcdfStreamWriteable netcdfStream = new NetcdfStreamWriteable();

      // Convert the Dimensions Node
      Node dimsNode = root.getFirstChild();
      ArrayList dimensions = toDimensions(dimsNode, netcdfStream);

      // Convert the Global Attributes Node
      Node gblAttrsNode = dimsNode.getNextSibling();
      toGlobalAttributes(gblAttrsNode, netcdfStream);

      // Convert the Variables Node
      Node varsNode = gblAttrsNode.getNextSibling();
      toVariables(varsNode, dimensions, netcdfStream);

      // Return the Writable NetCDF Stream
      return netcdfStream;
    }

  /**
   * Converts the tree to a set of Dimensions.
   *
   * @param root DOM Node representing the Dimensions.
   * @param netcdfStream Writable NetCDF Stream for the Dimensions.
   *
   * @return Set of Dimensions constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static ArrayList toDimensions(Node root,
				       NetcdfStreamWriteable netcdfStream)
    throws IIOInvalidTreeException
    {
      // Verify the name of the root Node
      String dimsName = MincMetadataFormat.DIMENSIONS_NAME;
      _verifyName(root, dimsName);

      // Attempt to construct Dimensions from the Node attributes
      ArrayList dimensions = new ArrayList();
      try {
	NamedNodeMap attributes = root.getAttributes();

	// Construct a Dimension from each attribute
	for (int i = 0; i < attributes.getLength(); i++) {
	  Node attribute = attributes.item(i);

	  // Construct a Dimension
	  dimensions.add( _toDimension(attribute, netcdfStream) );
	}
      }

      // Unable to construct the Dimensions
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into Dimensions.";
	throw new IIOInvalidTreeException(msg, e, root);
      }

      // Return the Dimensions
      return dimensions;
    }

  /**
   * Converts the tree to a set of global Attributes.
   *
   * @param root DOM Node representing the global Attributes.
   * @param netcdfStream Writable NetCDF Stream for the global Attributes.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static void toGlobalAttributes(Node root,
					NetcdfStreamWriteable netcdfStream)
    throws IIOInvalidTreeException
    {
      // Verify the name of the root Node
      String gblAttrsName = MincMetadataFormat.GLOBAL_ATTRIBUTES_NAME;
      _verifyName(root, gblAttrsName);

      // Attempt to construct Attributes from the Node children
      try {
	NodeList children = root.getChildNodes();
	for (int i = 0; i < children.getLength(); i++) {
	  Node child = children.item(i);

	  // Construct the Attribute value
	  Object value = _toAttributeValue(child);

	  // Construct a global Attribute
	  String name = child.getNodeName();
	  if (value instanceof String) {
	    netcdfStream.addGlobalAttribute(name, (String)value);
	  }
	  else if (value instanceof Number) {
	    netcdfStream.addGlobalAttribute(name, (Number)value);
	  }
	  else {
	    netcdfStream.addGlobalAttribute(name, value);
	  }
	}
      }

      // Unable to construct the global Attributes
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into global Attributes.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to a set of Variables.
   *
   * @param root DOM Node representing the Variables.
   * @param allowedDimensions Allowed Dimensions of the Variables.
   * @param netcdfStream Writable NetCDF Stream for the Variables.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static void toVariables(Node root, ArrayList allowedDimensions,
				 NetcdfStreamWriteable netcdfStream)
    throws IIOInvalidTreeException
    {
      // Verify the name of the root Node
      String varsName = MincMetadataFormat.VARIABLES_NAME;
      _verifyName(root, varsName);

      // Attempt to construct Variables from the Node children
      try {
	NodeList variableNodes = root.getChildNodes();
	for (int i = 0; i < variableNodes.getLength(); i++) {
	  Node variableNode = variableNodes.item(i);

	  // Verify the name of the Attributes Node
	  Node attributesNode = variableNode.getFirstChild();
	  String attName = MincMetadataFormat.ATTRIBUTES_NAME;
	  _verifyName(attributesNode, attName);

	  // Verify the name of the Data Node
	  Node dataNode = attributesNode.getNextSibling();
	  String dataName = MincMetadataFormat.DATA_NAME;
	  _verifyName(dataNode, dataName);

	  // Get the data type
	  Node typeNode = dataNode.getAttributes().getNamedItem(TYPE_NAME);
	  Class dataType = _toClass( typeNode.getNodeValue() );

	  // Get the Dimensions of the Variable
	  Dimension[] dimensions = _getDimensions(variableNode,
						  allowedDimensions);

	  // Construct the Variable
	  String variableName = variableNode.getNodeName();
	  netcdfStream.addVariable(variableName, dataType, dimensions);

	  // Construct Attributes of the Variable from the Attributes Node
	  NodeList attributeNodes = attributesNode.getChildNodes();
	  for (int j = 0; j < attributeNodes.getLength(); j++) {
	    Node attributeNode = attributeNodes.item(j);

	    // Construct the Attribute value
	    Object value = _toAttributeValue(attributeNode);

	    // Construct an Attribute
	    if (value != null) {
	      String attributeName = attributeNode.getNodeName();
	      if (value instanceof String) {
		netcdfStream.addVariableAttribute(variableName, attributeName,
						  (String)value);
	      }
	      else if (value instanceof Number) {
		netcdfStream.addVariableAttribute(variableName, attributeName,
						  (Number)value);
	      }
	      else {
		netcdfStream.addVariableAttribute(variableName, attributeName,
						  value);
	      }
	    }
	  }

	  // Construct the Array of data from the Data Node
	  NodeList dataNodeChildren = dataNode.getChildNodes();
	  if ( dataNodeChildren.getLength() > 0 ) {
	    Object data = _toDataValue( dataNodeChildren.item(0), dataType );

	    // Scalar data value; convert to an array
	    ArrayAbstract dataArray;
	    if (data instanceof Number) {
	      Object array = java.lang.reflect.Array.newInstance(dataType, 1);
	      java.lang.reflect.Array.set(array, 0, data);
	      dataArray = ArrayAbstract.factory(dataType, new int[0], array);
	    }

	    // Array data value
	    else { dataArray = ArrayAbstract.factory(data); }

	    // Add the data Array
	    netcdfStream.addVariableArray(variableName, dataArray);
	  }
	}
      }

      // Unable to construct the Variables
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into Variables.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to a Dimension.
   *
   * @param root DOM Node representing the Dimension.
   * @param netcdfStream Writable NetCDF Stream for the Dimension.
   *
   * @return Dimension constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   */
  private static Dimension _toDimension(Node root,
					NetcdfStreamWriteable netcdfStream)
    throws IIOInvalidTreeException
    {
      try {

	// Get the Dimension name and size
	String name = root.getNodeName();
	int size = Integer.parseInt( root.getNodeValue() );

	// Construct a Dimension
	return netcdfStream.addDimension(name, size);
      }

      // Unable to construct the Dimension
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node \"" + root.getNodeName() +
	             "\" into a Dimension.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to an Attribute value.
   *
   * @param root DOM Node representing the Attribute value.
   *
   * @return Attribute value constructed from the DOM Node, or null if the
   *         value specified is null.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   */
  private static Object _toAttributeValue(Node root)
    throws IIOInvalidTreeException
    {
     try {

       // Get the Attribute type
       Node attrNode = root.getAttributes().getNamedItem(TYPE_NAME);
       Class type = _toClass( attrNode.getNodeValue() );

       // Get all the values of the child Nodes
       ArrayList values = new ArrayList();
       NodeList children = root.getChildNodes();
       for (int i = 0; i < children.getLength(); i++) {
	 Node child = children.item(i);
	 attrNode = child.getAttributes().getNamedItem(VALUE_NAME);
	 values.add( attrNode.getNodeValue() );
       }

       // Null value
       if ( values.size() == 0 ) { return null; }

       // String value
       String firstValue = (String)values.get(0);
       if (type == String.class) { return firstValue; }

       // Number value
       else if ( values.size() == 1 ) { return _toValue(firstValue, type); }

       // Array
       else { 
	 Object a = java.lang.reflect.Array.newInstance(type, values.size());
	 for (int i = 0; i < values.size(); i++) {
	   String value = (String)values.get(i);
	   java.lang.reflect.Array.set(a, i, _toValue(value, type));
	 }

	 // Return the array
	 return a;
       }
     }

     // Unable to construct the Attribute value
     catch (Exception e) {
       String msg = "Cannot convert the DOM Node \"" + root.getNodeName() +
	            "\" into an Attribute value.";
       throw new IIOInvalidTreeException(msg, e, root);
     }
    }

  /**
   * Gets the Dimensions of the Variable tree.
   *
   * @param root DOM Node representing the Variable.
   * @param allowedDimensions Allowed Dimensions for the Variable.
   *
   * @return Dimensions of the Variable inferred from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   */
  private static Dimension[] _getDimensions(Node root,
					    ArrayList allowedDimensions)
    throws IIOInvalidTreeException
    {
      try {

	// Get the Data Node of the Variable Node
	Node attributesNode = root.getFirstChild();
	Node dataNode = attributesNode.getNextSibling();

	// Get the names of all the Dimensions
	String arrayName = MincMetadataFormat.DATA_ARRAY_NAME;
	ArrayList dimensionNames = new ArrayList();
	while( _getChildNames(dataNode).contains(arrayName) ) {

	  // Get the first attribute Node of the first Array Node
	  dataNode = dataNode.getChildNodes().item(0);
	  Node attrNode = dataNode.getAttributes().item(0);

	  // Add the value of this attribute Node to the Dimension names
	  dimensionNames.add( attrNode.getNodeValue() );
	}

	// Match the Dimension names to the names of the allowed Dimensions
	Dimension[] dimensions = new Dimension[ dimensionNames.size() ];
	for (int i = 0; i < dimensionNames.size(); i++) {
	  String name = (String)dimensionNames.get(i);

	  // Find the Dimension corresponding to the name
	  for (int j = 0; j < allowedDimensions.size(); j++) {
	    Dimension dimension = (Dimension)allowedDimensions.get(j);
	    if ( dimension.getName().equals(name) ) {
	      dimensions[i] = dimension;
	    }
	  }
	}

	// Return the Dimensions of the Variable
	return dimensions;
      }

      // Unable to get the Dimensions of the Variable
      catch (Exception e) {
	String msg = "Unable to get the Dimensions of the Variable \"" +
	              root.getNodeName() + "\".";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to a Number or a Java primitive array.
   *
   * @param root DOM Node representing the data.
   * @param type Type of data in the tree.
   *
   * @return Number or a Java primitive array constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   */
  private static Object _toDataValue(Node root, Class type)
    throws IIOInvalidTreeException
    {
      try {

	// Node is an Element
	String elementName = MincMetadataFormat.DATA_ELEMENT_NAME;
	if ( root.getNodeName().equals(elementName) ) {
	  
	  // Convert the Element value to a Number
	  Node valueNode = root.getAttributes().getNamedItem(VALUE_NAME);
	  String value = valueNode.getNodeValue();
	  return _toValue(value, type);
	}

	// Node is an Array
	String arrayName = MincMetadataFormat.DATA_ARRAY_NAME;
	if ( root.getNodeName().equals(arrayName) ) {

	  // Convert all the children of the Array Node
	  ArrayList childrenValues = new ArrayList();
	  NodeList childrenNodes = root.getChildNodes();
	  for (int i = 0; i < childrenNodes.getLength(); i++) {
	    childrenValues.add( _toDataValue(childrenNodes.item(i), type) );
	  }

	  Object array;
	  int size = childrenValues.size();

	  // No children; create an array with a zero value
	  if (size == 0) {
	    array = java.lang.reflect.Array.newInstance(type, 1);
	  }

	  // Children are Numbers; create a 1-D array
	  else if ( childrenValues.get(0) instanceof Number ) {
	    array = java.lang.reflect.Array.newInstance(type, size);
	  }

	  // Children are arrays; create a n-D array
	  else {
	    Class arrayType = childrenValues.get(0).getClass();
	    array = java.lang.reflect.Array.newInstance(arrayType, size);
	  }

	  // Set the child values in the array
	  for (int i = 0; i < size; i++) {
	    java.lang.reflect.Array.set(array, i, childrenValues.get(i));
	  }

	  // Return the array
	  return array;
	}

	// Unknown Node
	String msg = "Unable to convert the Node \"" + root.getNodeName() +
	             "\".";
	throw new IllegalArgumentException(msg);
      }

      // Unable to construct the Data value
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node to a Data value.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the Attribute to a tree and adds it to the given root Node.
   *
   * @param attribute Attribute to convert.
   * @param root Root Node to add the converted tree to.
   */
  private static void _addToTree(Attribute attribute, IIOMetadataNode root)
    {
      // Create an Attribute Node as a root child Node
      IIOMetadataNode aNode = new IIOMetadataNode( attribute.getName() );
      root.appendChild(aNode);

      // Add the Attribute type as an attribute to the Attribute Node
      String type = _toString( attribute.getValueType() );
      aNode.setAttribute(TYPE_NAME, type);

      // Add each Attribute value as a child Node
      for (int i = 0; i < attribute.getLength(); i++) {

	// Create an Attribute Value Node
	String valueName = MincMetadataFormat.ATTRIBUTE_VALUE;
	IIOMetadataNode vNode = new IIOMetadataNode(valueName);
	aNode.appendChild(vNode);

	// Add the Attribute value as an attribute
	if ( attribute.isString() ) {
	  vNode.setAttribute(VALUE_NAME, attribute.getValue().toString());
	}
	else {
	  vNode.setAttribute(VALUE_NAME,
			     attribute.getNumericValue(i).toString());
	}
      }
    }

  /**
   * Converts the Variable to a tree and adds it to the given root Node.
   *
   * @param variable Variable to convert.
   * @param array Array of data for the Variable.
   * @param root Root Node to add the converted tree to.
   */
  private static void _addToTree(Variable variable, Array array,
				 IIOMetadataNode root)
    {
      // Create a Variable Node as a root child Node
      IIOMetadataNode varNode = new IIOMetadataNode( variable.getName() );
      root.appendChild(varNode);

      // Add an Attributes Node
      String attName = MincMetadataFormat.ATTRIBUTES_NAME;
      IIOMetadataNode attNode = new IIOMetadataNode(attName);
      varNode.appendChild(attNode);

      // Add the Attributes as children Nodes
      Iterator iter = variable.getAttributeIterator();
      while ( iter.hasNext() ) {
	_addToTree( (Attribute)iter.next(), attNode );
      }

      // Add a Data Node
      String dataName = MincMetadataFormat.DATA_NAME;
      IIOMetadataNode dataNode = new IIOMetadataNode(dataName);
      varNode.appendChild(dataNode);

      // Add the Variable type as an attribute to the Data Node
      String type = _toString( variable.getElementType() );
      dataNode.setAttribute(TYPE_NAME, type);

      // Add the Array data as child Nodes
      _addToTree(array, variable.getDimensions(), dataNode );
    }

  /**
   * Converts the Array to a tree and adds it to the given root Node.
   *
   * @param array Array to convert.
   * @param dimensions Dimensions of the Array.
   * @param root Root Node to add the converted tree to.
   */
  private static void _addToTree(Array array, ArrayList dimensions,
				 IIOMetadataNode root)
    {
      // Array is a scalar
      if ( dimensions.size() == 0 ) {

	// Check for a scalar value
	if (array != null) {
	
	  // Create an Element Node for the scalar value
	  String elementName = MincMetadataFormat.DATA_ELEMENT_NAME;
	  IIOMetadataNode eNode = new IIOMetadataNode(elementName);
	  root.appendChild(eNode);

	  // Add the scalar value as an attribute
	  String value = array.getObject(array.getIndex()).toString();
	  eNode.setAttribute(VALUE_NAME, value);
	}
      }

      // Array is not a scalar
      else {

	// Create an Array Node
	String arrayName = MincMetadataFormat.DATA_ARRAY_NAME;
	IIOMetadataNode aNode = new IIOMetadataNode(arrayName);
	root.appendChild(aNode);

	// Add the Dimension name as an attribute
	String size = ((Dimension)dimensions.get(0)).getName();
	aNode.setAttribute(DIM_NAME, size);

	// Decrease the number of Dimensions
	ArrayList lowerDimensions = (ArrayList)dimensions.clone();
	lowerDimensions.remove(0);

	// If present, create children from the subArrays
	if (array != null) {
	  int[] lengths = array.getShape();
	  for (int i = 0; i < lengths[0]; i++) {
	    _addToTree(array.slice(0, i), lowerDimensions, aNode);
	  }
	}

	// Otherwise just create a Node for the lower Dimensions
	else { _addToTree(null, lowerDimensions, aNode); }
      }
    }

  /**
   * Verifies the name of the DOM root Node.
   *
   * @param root DOM Node to verify the name of.
   * @param name Name the DOM Node should have.
   *
   * @throws IIOInvalidTreeException If the name of the DOM Node does not equal
   *                                 the specified name.
   * @throws IllegalArgumentException If the root Node is null.
   */
  private static void _verifyName(Node root, String name)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      if ( !root.getNodeName().equals(name) ) {
	String msg = "Root node must be named \"" + name + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }
    }

  /**
   * Gets a list of the names of all the child Nodes of the given Node.
   *
   * @param root DOM Node whose child Nodes are investigated.
   *
   * @return List of the names of all the child Nodes of the given Node.
   */
  private static ArrayList _getChildNames(Node root)
    {
      ArrayList names = new ArrayList();

      // Get a list of all the names
      NodeList nodeList = root.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
	String name = nodeList.item(i).getNodeName();

	// No duplicate names
	if ( !names.contains(name) ) { names.add(name); }
      }

      // Return the list of names
      return names;
    }

  /**
   * Gets the value represented in the string.
   *
   * @param stringValue String containing the value.
   * @param type Class describing the data type.
   *
   * @return Value extracted from the string.
   *
   * @throws NumberFormatException If the string cannot be parsed.
   */
  private static Object _toValue(String stringValue, Class type)
    {
      // String
      if (type == String.class) { return stringValue; }

      // Character
      if (type == char.class) {
	char value = (char)Integer.parseInt(stringValue);
	return new Character(value);
      }

      // Primitive
      if (type == byte.class) { return Byte.valueOf(stringValue); }
      if (type == short.class) { return Short.valueOf(stringValue); }
      if (type == int.class) { return Integer.valueOf(stringValue); }
      if (type == double.class) { return Double.valueOf(stringValue); }
      if (type == float.class) { return Float.valueOf(stringValue); }

      // Type not recognized
      String msg = "Unable to parse a string of type \"" + type.getName() +
	           "\".";
      return new NumberFormatException(msg);
    }

  /**
   * Gets a standardized name for the Class.
   *
   * @param c Class to get a standardized name for.
   *
   * @return Standardized name for the Class.
   */
  private static String _toString(Class c)
    {
      // String
      if (c == String.class) { return "string"; }

      return c.getName();
    }

  /**
   * Gets the Class from the standardized name.
   *
   * @param name Standardized name to get the Class for.
   *
   * @return Class from the standardized name.
   *
   * @throws IllegalArgumentException If the name is not recognized.
   */
  private static Class _toClass(String name)
    {
      // String
      if ( name.equals("string") ) { return String.class; }

      // Primitive types
      if ( name.equals("byte") ) { return byte.class; }
      if ( name.equals("char") ) { return char.class; }
      if ( name.equals("double") ) { return double.class; }
      if ( name.equals("float") ) { return float.class; }
      if ( name.equals("int") ) { return int.class; }
      if ( name.equals("long") ) { return long.class; }
      if ( name.equals("short") ) { return short.class; }

      // Unknown name
      String msg = "Cannot convert \"" + name + "\" into a Class.";
      throw new IllegalArgumentException(msg);
    }
}
