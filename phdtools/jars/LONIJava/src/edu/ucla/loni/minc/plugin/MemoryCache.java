/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.minc.plugin;

import java.io.IOException;

/**
 * Derivation of Sun's Memory Cache that more conservatively manages memory.
 *
 * @version 29 November 2005
 */
public class MemoryCache
{
    /** Size of the buffer blocks. */
    private static final int BUFFER_LENGTH = 8192;

    /** Cache of buffer blocks. */
    private Object[] _cache;

    /** Largest position written to the cache so far. */
    private long _length;

    /** Constructs a Memory Cache. */
    public MemoryCache()
    {
	_cache = new Object[0];
	_length = 0;
    }

    /**
     * Overwrites or appends a single byte to the cache.
     *
     * @param b Integer whose 8 least significant bits will be written.
     * @param pos Cache position at which to begin writing.
     *
     * @throws IOException If a writing error occurs.
     * @throws IndexOutOfBoundsException If the position is negative.
     */
    public void write(int b, long pos) throws IOException
    {
	// Check the position validity
        if (pos < 0) { throw new ArrayIndexOutOfBoundsException("pos < 0"); }

	// Adding the bytes can increase the size of the cache
	if (pos + 1 > _length) { _length = pos + 1; }

        // Copy the byte
        byte[] buf = _getCacheBlock(pos/BUFFER_LENGTH, true);
        int offset = (int)(pos % BUFFER_LENGTH);
        buf[offset] = (byte)b;
    }

    /**
     * Overwrites and/or appends the cache from a byte array.
     *
     * @param b Array of bytes containing data to be written.
     * @param off Starting offset within the data array.
     * @param len Number of bytes to be written.
     * @param pos Cache position at which to begin writing.
     *
     * @throws IOException If a writing error occurs.
     * @throws IndexOutOfBoundsException If the offset or lengths are invalid.
     * @throws NullPointerException If the array is null.
     */
    public void write(byte[] b, int off, int len, long pos) throws IOException
    {
	// No array
        if (b == null) { throw new NullPointerException("b == null!"); }

        // Check offset/length validity
        if ((off < 0) || (len < 0) || (pos < 0) ||
            (off + len > b.length) || (off + len < 0))
	    {
		throw new IndexOutOfBoundsException();
	    }

	// Adding the bytes can increase the size of the cache
	if (pos + len > _length) { _length = pos + len; }

        // Copy the data into the cache, block by block
        int offset = (int)(pos % BUFFER_LENGTH);
        while (len > 0) {
            byte[] buf = _getCacheBlock(pos/BUFFER_LENGTH, true);
            int nbytes = Math.min(len, BUFFER_LENGTH - offset);
            System.arraycopy(b, off, buf, offset, nbytes);

            pos += nbytes;
            off += nbytes;
            len -= nbytes;
            offset = 0; // Always after the first time
        }
    }

    /**
     * Gets the byte at the position.
     *
     * @param pos Cache position at which to read the byte.
     *
     * @return Byte at the position, or -1 if it does not exist.
     *
     * @throws IOException If a reading error occurs.
     */
    public int read(long pos) throws IOException
    {
	// Reading beyond the cached data
        if (pos >= _length) { return -1; }

	// Get the corresponding cache block
	byte[] block = _getCacheBlock(pos/BUFFER_LENGTH, false);

	// Check to see if the cache block exists
	if (block == null) { return -1; }

	// Return the byte
	return block[(int)(pos % BUFFER_LENGTH)] & 0xff;
    }

    /**
     * Copies bytes from the cache, starting at the cache position, into the
     * array at the offset.
     *
     * @param b Byte array to copy into.
     * @param off Offset into the byte array to start copying.
     * @param len Number of bytes to read.
     * @param pos Cache position at which to read the data.
     *
     * @throws IOException If a reading error occurs.
     * @throws IndexOutOfBoundsException If the offset or length is invalid, or
     *                                   if any portion of the data is not in
     *                                   the cache.
     * @throws NullPointerException If the byte array is null.
     */
    public void read(byte[] b, int off, int len, long pos) throws IOException
    {
	// No array
        if (b == null) { throw new NullPointerException("b == null!"); }

	// Check offset and length validity
        if ((off < 0) || (len < 0) || (pos < 0) ||
            (off + len > b.length) || (off + len < 0))
	    {
		throw new IndexOutOfBoundsException();
	    }
        if (pos + len > _length) {
            throw new IndexOutOfBoundsException();
        }
        
        long index = pos/BUFFER_LENGTH;
        int offset = (int)pos % BUFFER_LENGTH;
        while (len > 0) {
            int nbytes = Math.min(len, BUFFER_LENGTH - offset);
            byte[] buf = _getCacheBlock(index++, false);

	    // Cache block does not exist
	    if (buf == null) {
		System.out.println("Missing data in (" + pos + ", " +
				   (pos+len) + ")");
	    }

            System.arraycopy(buf, offset, b, off, nbytes);

            len -= nbytes;
            off += nbytes;
            offset = 0; // Always after the first time
        }
    }

    /**
     * Gets the total length of data that has been cached.
     *
     * @return Total length of data that has been cached.
     */
    public long getLength()
    {
        return _length;
    }

    /** Erases the entire cache contents and resets the length to 0. */
    public void reset() {
        _cache = new Object[0];
        _length = 0;
    }

    /**
     * Gets the specified cache block.
     *
     * @param blockNum Number of the cache block.
     * @param createNew True if a new cache block is to be created if it does
     *                  not exist; false otherwise.
     *
     * @return Numbered cache block, or null if it does not exist.
     */
    private byte[] _getCacheBlock(long blockNum, boolean createNew)
	throws IOException
    {
	// 16 terabyte limit
	if (blockNum > Integer.MAX_VALUE) {
	    throw new IOException("Cache addressing limit exceeded!");
	}
	int blockNumber = (int)blockNum;

	// Increase the size of the block array if necessary
	if (blockNumber >= _cache.length-1) {
	    Object[] oldCache = _cache;
	    _cache = new Object[blockNumber+1];
	    System.arraycopy(oldCache, 0, _cache, 0, oldCache.length);
	}

	// Create the cache block (if requested) if it does not exist
	if (_cache[blockNumber] == null && createNew) {
	    try {
		_cache[blockNumber] = new byte[BUFFER_LENGTH];
	    }
	    catch (OutOfMemoryError e) {
		throw new IOException("No memory left for cache!");
	    }
	}

	// Return the cache block
	return (byte[])_cache[blockNumber];
    }
}
