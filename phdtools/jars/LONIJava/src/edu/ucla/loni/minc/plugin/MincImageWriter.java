/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.minc.plugin;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;
import ucar.ma2.ArrayAbstract;
import ucar.nc2.NetcdfStreamWriteable;

/**
 * Image Writer for parsing and decoding MINC images.
 *
 * @version 30 January 2006
 */
public class MincImageWriter extends ImageWriter
{
    /** Dimensions, Variables, and Attributes in the sequence being written. */
    private NetcdfStreamWriteable _stream;

    /** Number of images in the sequence being written. */
    private int _numberOfImagesWritten;

    /**
     * Constructs an MincImageWriter.
     *
     * @param originatingProvider The ImageWriterSpi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  MINC Image Writer Spi.
     */
    public MincImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an MINC Image Writer Spi
	if ( !(originatingProvider instanceof MincImageWriterSpi) ) {
	    String msg = "Originating provider must an MINC Image Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank metadata object
	return new MincMetadata(null);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding an
     * image of the given type.
     *
     * @param imageType An ImageTypeSpecifier indicating the format of the image
     *                  to be written later.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					       ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing stream metadata, used to
     *               initialize the state of the returned object.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize Minc Metadata
	if (inData instanceof MincMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing image metadata, used to
     *               initialize the state of the returned object.
     * @param imageType An ImageTypeSpecifier indicating the layout and color
     *                  information of the image with which the metadata will be
     *                  associated.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If either of inData or imageType is
     *                                  null.
     */
    public IIOMetadata convertImageMetadata(IIOMetadata inData,
					    ImageTypeSpecifier imageType,
					    ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns true if the methods that take an IIOImage parameter are capable
     * of dealing with a Raster (as opposed to RenderedImage) source image.
     *
     * @return True if Raster sources are supported.
     */
    public boolean canWriteRasters()
    {
	return true;
    }

    /**
     * Appends a complete image stream containing a single image and associated
     * stream and image metadata and thumbnails to the output.
     *
     * @param streamMetadata An IIOMetadata object representing stream metadata,
     *                       or null to use default values.
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam, or null to use a default
     *              ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, if the stream
     *                                  metadata cannot be converted, or if the
     *                                  image type is unrecognized.
     * @throws IllegalStateException If the output has not been set.
     */
    public void write(IIOMetadata streamMetadata, IIOImage image,
		      ImageWriteParam param) throws IOException
    {
	// Write a sequence with 1 image
	prepareWriteSequence(streamMetadata);
	writeToSequence(image, param);
	endWriteSequence();
    }

    /**
     * Returns true if the writer is able to append an image to an image
     * stream that already contains header information and possibly prior
     * images.
     *
     * @return True If images may be appended sequentially.
     */
    public boolean canWriteSequence()
    {
	return true;
    }

    /**
     * Prepares a stream to accept a series of subsequent writeToSequence calls,
     * using the provided stream metadata object.  The metadata will be written
     * to the stream if it should precede the image data.
     *
     * @param streamMetadata A stream metadata object.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the stream metadata is null or cannot
     *                                  be converted.
     * @throws IllegalStateException If the output has not been set.
     */
    public void prepareWriteSequence(IIOMetadata streamMetadata)
	throws IOException
    {
	// Convert the metadata to MINC Metadata
	IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the stream metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct a NetCDF stream from the metadata
	try {
	    String rootName = MincMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	    Node tree = metadata.getAsTree(rootName);
	    _stream = MincMetadataConversions.toNetcdfStream(tree);
	}
      
	catch (Exception e) {
	    String msg = "Unable to construct a NetCDF STream from the " +
		"metadata.";
	    throw new IllegalArgumentException(msg);
	}

	// Write all the NetCDF parameters to the output stream
	_stream.create( _getOutputStream() );
	_numberOfImagesWritten = 0;
    }

    /**
     * Appends a single image and possibly associated metadata and thumbnails,
     * to the output.  The supplied ImageReadParam is ignored.
     *
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam or null to use a default ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null or if the image
     *                                  type is not recognized.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void writeToSequence(IIOImage image, ImageWriteParam param)
	throws IOException
    { 
	// Check for the NetCDF stream
	if (_stream == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Check for a null image
	if (image == null) {
	    String msg = "Cannot write a null image.";
	    throw new IllegalArgumentException(msg);
	}

	// Get a Raster from the image
	Raster raster = image.getRaster();
	if (raster == null) {
	    RenderedImage renderedImage = image.getRenderedImage();

	    // Rendered Image is a Buffered Image, get the Raster directly
	    if (renderedImage instanceof BufferedImage) {
		raster = ((BufferedImage)renderedImage).getRaster();
	    }

	    // Otherwise get a copy of the Raster from the Rendered Image
	    raster = renderedImage.getData();
	}

	// Update the Listeners
	processImageStarted(_numberOfImagesWritten);

	// Color image data
	MincMetadata metadata = new MincMetadata(_stream);
	if ( metadata.hasColorImages() ) {
	    ArrayAbstract array = null;
	    DataBuffer dataBuffer = raster.getDataBuffer();

	    // ARGB format
	    if (dataBuffer instanceof DataBufferInt) {
		array = _convertIntToArray(raster);
	    }

	    // RGB format
	    else if (dataBuffer instanceof DataBufferByte) {
		byte[] data = ((DataBufferByte)dataBuffer).getData();
		int[] shape = {1, raster.getHeight(), raster.getWidth()/3, 3};
		array = ArrayAbstract.factory(byte.class, shape, data);
	    }

	    // Unrecognized type
	    else {
		String msg = "Unable to write the IIOImage.";
		throw new IllegalArgumentException(msg);
	    }

	    // Write the Array to the image Variable 
	    int[] offsets = {_numberOfImagesWritten, 0, 0, 0};
	    _stream.write(MincMetadataFormat.MI_IMAGE, offsets, array);
	}

	// Grayscale data
	else {
	    ArrayAbstract array = null;
	    int[] shape = {1, raster.getHeight(), raster.getWidth()};
	    DataBuffer dataBuffer = raster.getDataBuffer();

	    if (dataBuffer instanceof DataBufferByte) {
		byte[] data = ((DataBufferByte)dataBuffer).getData();
		array = ArrayAbstract.factory(byte.class, shape, data);
	    }
	    else if (dataBuffer instanceof DataBufferShort) {
		short[] data = ((DataBufferShort)dataBuffer).getData();
		array = ArrayAbstract.factory(short.class, shape, data);
	    }
	    else if (dataBuffer instanceof DataBufferUShort) {
		short[] data = ((DataBufferUShort)dataBuffer).getData();
		array = ArrayAbstract.factory(short.class, shape, data);
	    }
	    else if (dataBuffer instanceof DataBufferInt) {
		int[] data = ((DataBufferInt)dataBuffer).getData();
		array = ArrayAbstract.factory(int.class, shape, data);
	    }
	    else if (dataBuffer instanceof DataBufferFloat) {
		float[] data = ((DataBufferFloat)dataBuffer).getData();
		array = ArrayAbstract.factory(float.class, shape, data);
	    }
	    else if (dataBuffer instanceof DataBufferDouble) {
		double[] data = ((DataBufferDouble)dataBuffer).getData();
		array = ArrayAbstract.factory(double.class, shape, data);
	    }

	    // Unrecognized type
	    else {
		String msg = "Unable to write the IIOImage.";
		throw new IllegalArgumentException(msg);
	    }

	    // Write the Array to the image Variable 
	    int[] offsets = {_numberOfImagesWritten, 0, 0};
	    _stream.write(MincMetadataFormat.MI_IMAGE, offsets, array);
	}

	// Update the Listeners
	_numberOfImagesWritten++;
	if ( abortRequested() ) { processWriteAborted(); }
	else { processImageComplete(); }
    }

    /**
     * Completes the writing of a sequence of images begun with
     * prepareWriteSequence.  Any stream metadata that should come at the end of
     * the sequence of images is written out, and any header information at the
     * beginning of the sequence is patched up if necessary.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void endWriteSequence() throws IOException
    {
	// Check for the NetCDF stream
	if (_stream == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Flush the NetCDF stream and reset it
	_stream.flush();
	_stream = null;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageWriter.  Otherwise, the writer may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the output stream
	try { if (output != null) { ((ImageOutputStream)output).close(); } }
	catch (Exception e) {}

	output = null;
    }

    /**
     * Gets the output stream.
     *
     * @return Image Output Stream to write to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream
	return (ImageOutputStream)output;
    }

    /**
     * Converts the integer Raster of ARGB data into an Array Abstract.
     *
     * @param raster Raster of ARGB values.
     *
     * @return Array Abstract of 3-byte RGB values.
     */
    private ArrayAbstract _convertIntToArray(Raster raster)
    {
	// Get the integer data
	DataBuffer dataBuffer = raster.getDataBuffer();
	int[] intArray = ((DataBufferInt)dataBuffer).getData();

	// Separate the RGB components of each integer
	byte[] byteArray = new byte[3*intArray.length];
	for (int i = 0; i < intArray.length; i++) {
	    int v = intArray[i];
	    byteArray[3*i]   = (byte)((v >>> 16) & 0xFF);
	    byteArray[3*i+1] = (byte)((v >>> 8) & 0xFF);
	    byteArray[3*i+2] = (byte)((v >>> 0) & 0xFF);
	}

	// Return a 4-d array
	int[] shape = {1, raster.getHeight(), raster.getWidth(), 3};
	return ArrayAbstract.factory(byte.class, shape, byteArray);
    }
}
