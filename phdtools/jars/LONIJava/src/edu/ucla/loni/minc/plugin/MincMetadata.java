/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.minc.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import java.util.Iterator;
import java.util.LinkedHashMap;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfStream;
import ucar.nc2.NetcdfStreamWriteable;
import ucar.nc2.Variable;

/**
 * IIO Metadata for metadata of the MINC image format.
 *
 * @version 15 January 2009
 */
public class MincMetadata extends AppletFriendlyIIOMetadata
{
    /** Dimensions, Variables, and Attributes in the original metadata. */
    private NetcdfStream _originalStream;

    /** Dimensions, Variables, and Attributes in the current metadata. */
    private NetcdfStream _currentStream;

    /**
     * Constructs a MincMetadata from the given NetCDF parameters.
     *
     * @param netcdfStream NetCDF parameters containing the Dimensions,
     *                     Variables, and Attributes.
     */
    public MincMetadata(NetcdfStream netcdfStream)
    {
	super(true, MincMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      MincMetadataFormat.class.getName(),
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalStream = netcdfStream;
	_currentStream = netcdfStream;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {

	    // If the stream is writable, first need to prepare its variables
	    if (_currentStream instanceof NetcdfStreamWriteable) {
		NetcdfStreamWriteable wStream =
		    (NetcdfStreamWriteable)_currentStream;

		// Prepare variables if still in define mode
		if ( wStream.isInDefineMode() ) {
		    try {
			MemoryCacheImageOutputStream mStream
			    = new MemoryCacheImageOutputStream();

			// Convert prototype vars into real ones by writing
			// to memory
			wStream.create(mStream);
		    }

		    // Won't happen with a memory output stream
		    catch (Exception e) { throw new InternalError(); }
		}
	    }

	    // Return a DOM tree constructed from the current metadata
	    return MincMetadataConversions.toTree(_currentStream);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current NetCDF Stream, convert the root
	Node r;
	if (_currentStream == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Get the current metadata tree Nodes
	    Node netcdfNode = getAsTree(formatName);
	    Node dimensionsNode = netcdfNode.getFirstChild();
	    Node globalAttrsNode = dimensionsNode.getNextSibling();
	    Node variablesNode = globalAttrsNode.getNextSibling();

	    // Loop through the root Node children and merge
	    NodeList nodeList = root.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node childNode = nodeList.item(i);
		String childName = childNode.getNodeName();

		// Replace or add Dimensions attributes
		if ( MincMetadataFormat.DIMENSIONS_NAME.equals(childName) ) {
		    dimensionsNode = _mergeNodeAttributes(dimensionsNode,
							  childNode);
		}

		// Replace or add Global Attributes Nodes
		if(MincMetadataFormat.GLOBAL_ATTRIBUTES_NAME.equals(childName)){
		    globalAttrsNode = _mergeNodeChildren(globalAttrsNode,
							 childNode);
		}

		// Replace or add Variables Nodes
		if ( MincMetadataFormat.VARIABLES_NAME.equals(childName) ) {
		    variablesNode = _mergeNodeChildren(variablesNode,
						       childNode);
		}
	    }

	    // Recreate the root tree
	    r = new IIOMetadataNode(nativeMetadataFormatName);
	    r.appendChild(dimensionsNode);
	    r.appendChild(globalAttrsNode);
	    r.appendChild(variablesNode);
	}

	// Set a new current NetCDF Stream
	_currentStream = MincMetadataConversions.toNetcdfStream(r);
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current metadata to the original
	_currentStream = _originalStream;
    }

    /**
     * Gets the number of images.
     *
     * @return Number of images, or -1 if it cannot be determined.
     */
    public int getNumberOfImages()
    {
	try {
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);

	    // For a 3-d volume, number of images is first dimension
	    if (imageVariable.getRank() == 3 || hasColorImages()) {
		return imageVariable.getDimension(0).getLength();
	    }

	    // For a 4-d volume, number of images is first * second dimension
	    if (imageVariable.getRank() == 4) {
		return imageVariable.getDimension(0).getLength() *
		    imageVariable.getDimension(1).getLength();
	    }

	    // Unsupported volume
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the width of each image.
     *
     * @return Width of each image, or -1 if it cannot be determined.
     */
    public int getImageWidth()
    {
	try {
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);

	    // 3-d volume --> dim 2; 4-d volume --> dim 3
	    int dim = 2;
	    if (imageVariable.getRank() == 4 && !hasColorImages()) { dim = 3; }
	    return imageVariable.getDimension(dim).getLength();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the height of each image.
     *
     * @return Height of each image, or -1 if it cannot be determined.
     */
    public int getImageHeight()
    {
	try {
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);

	    // 3-d volume --> dim 1; 4-d volume --> dim 2
	    int dim = 1;
	    if (imageVariable.getRank() == 4 && !hasColorImages()) { dim = 2; }
	    return imageVariable.getDimension(dim).getLength();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the number of bits per pixel used by each image.
     *
     * @return Number of bits per pixel used by each image, or -1 if it cannot
     *         be determined.
     */
    public int getBitsPerPixel()
    {
	try {

	    // Color images must have 24 bits (3 bytes) per pixel
	    if ( hasColorImages() ) { return 24; }

	    // Return the number of bits per pixel
	    Class type = getDataType();
	    if      (type == Byte.TYPE)    { return 8; }
	    else if (type == Short.TYPE)   { return 16; }
	    else if (type == Integer.TYPE) { return 32; }
	    else if (type == Float.TYPE)   { return 32; }
	    else if (type == Double.TYPE)  { return 64; }

	    // Unknown type
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the data type of each image.
     *
     * @return Data type of each image (as defined by the MINC file format),
     *         or null if it cannot be determined.
     */
    public Class getDataType()
    {
	try {

	    // Get the image data type
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);
	    return imageVariable.getElementType();
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Determines whether or not the image data has signed values.
     *
     * @return True if the image data has signed values; false otherwise.
     */
    public boolean hasSignedData()
    {
	try {
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);

	    // Get the signed type attribute
	    String signedName = MincMetadataFormat.MI_IMAGE_SIGNTYPE;
	    Attribute signedAttribute = imageVariable.findAttribute(signedName);
	    if (signedAttribute == null) { return false; }

	    // Determine if the data is signed
	    return "signed__".equals(signedAttribute.getValue());
	}

	// Unable to make the determination
	catch (Exception e) { return false; }
    }

    /**
     * Determines whether or not each image is a color image.
     *
     * @return True if the images are color images; false otherwise.
     */
    public boolean hasColorImages()
    {
	try {
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);

	    // Must have 4-dimensions
	    if (imageVariable.getRank() != 4) { return false; }

	    // Must have the vector dimension as the fastest varying dimension
	    Dimension fastestDim = imageVariable.getDimension(3);
	    String dimName = fastestDim.getName();
	    if ( !MincMetadataFormat.MI_VECTOR_DIMENSION.equals(dimName) ) {
		return false;
	    }

	    // Must have 3 components
	    if (fastestDim.getLength() != 3) { return false; }

	    // Color images
	    return true;
	}

	// Unable to make the determination
	catch (Exception e) { return false; }
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// Color images
	if ( hasColorImages() ) { return Utilities.getRgbChromaNode(); }

	// Otherwise support non-inverted grayscale
	return Utilities.getGrayScaleChromaNode(false);
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	// Color images
	if ( hasColorImages() ) { return Utilities.getRgbDataNode(); }

	// Get the number of bits per pixel
	int bitsPerPixel = getBitsPerPixel();
	if (bitsPerPixel == -1) { return null; }

	// Integral gray scale data
	Class type = getDataType();
	if (type == Byte.TYPE || type == Short.TYPE || type == Integer.TYPE) {
	    return Utilities.getGrayScaleDataNode(bitsPerPixel);
	}

	// Real gray scale data
	return Utilities.getGrayScaleDataNode(bitsPerPixel, false);
    }

    /**
     * Merges the attributes of the new Node with the original Node.  If a new
     * attribute has the same name as an original attribute, the original
     * attribute is replaced with the new attribute.  If a new attribute does
     * not have the same name as an original attribute, it is added to the list
     * of original attributes.
     *
     * @param origNode Node containing the original attributes.
     * @param newNode Node containing the new attributes.
     *
     * @return Node with merged attributes.
     */
    private Node _mergeNodeAttributes(Node origNode, Node newNode)
    {
	// Create a merged Node with the original Node name
	IIOMetadataNode mergedNode =
	    new IIOMetadataNode(origNode.getNodeName());

	// Copy the original Node attributes to the merged Node
	NamedNodeMap map = origNode.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		mergedNode.setAttribute(attr.getNodeName(),attr.getNodeValue());
	    }
	}

	// Merge the new Node attributes with the merged Node attributes
	map = newNode.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		mergedNode.setAttribute(attr.getNodeName(),attr.getNodeValue());
	    }
	}

	// Return the merged Node
	return mergedNode;
    }

    /**
     * Merges the child Nodes of the new Node with the original Node.  If a new
     * child Node has the same name as an original child Node, the original
     * child Node is replaced with the new child Node.  If a new child Node
     * does not have the same name as an original child Node, it is added to
     * the children of the original Node.
     *
     * @param origNode Node containing the original child Nodes.
     * @param newNode Node containing the new child Nodes.
     *
     * @return Node with merged child Nodes.
     */
    private Node _mergeNodeChildren(Node origNode, Node newNode)
    {
	// Construct a map between the child names and the child Nodes
	LinkedHashMap map = new LinkedHashMap();

	// Add the original Node children to the map
	NodeList children = origNode.getChildNodes();
	for (int i = 0; i < children.getLength(); i++) {
	    Node child = children.item(i);
	    map.put( child.getNodeName(), child );
	}

	// Merge the new Node children to the map
	children = newNode.getChildNodes();
	for (int i = 0; i < children.getLength(); i++) {
	    Node child = children.item(i);
	    map.put( child.getNodeName(), child );
	}

	// Create a merged Node with the original Node name
	IIOMetadataNode mergedNode =
	    new IIOMetadataNode(origNode.getNodeName());

	// Add the merged child Nodes to the merged Node
	Iterator iter = map.keySet().iterator();
	while ( iter.hasNext() ) {
	    String childName = (String)iter.next();
	    mergedNode.appendChild( (Node)map.get(childName) );
	}

	// Return the merged Node
	return mergedNode;
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	// Image dimensions
	int imageWidth = getImageWidth();
	int imageHeight = getImageHeight();
	int numberOfImages = getNumberOfImages();

	// Pixel dimensions and slice thickness
	float pixelWidth = 0;
	float pixelHeight = 0;
	float sliceThickness = 0;
	try {
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _currentStream.findVariable(imageName);

	    String stepName = MincMetadataFormat.MI_STEP_SIZE;

	    // Width:  3-d volume --> dim 2; 4-d volume --> dim 3
	    int dim = 2;
	    if (imageVariable.getRank() == 4 && !hasColorImages()) { dim = 3; }
	    String dimName = imageVariable.getDimension(dim).getName();
	    Variable dimVariable = _currentStream.findVariable(dimName);
	    Attribute stepAttr = dimVariable.findAttribute(stepName);
	    pixelWidth = stepAttr.getNumericValue().floatValue();

	    // Height:  3-d volume --> dim 1; 4-d volume --> dim 2
	    dim = 1;
	    if (imageVariable.getRank() == 4 && !hasColorImages()) { dim = 2; }
	    dimName = imageVariable.getDimension(dim).getName();
	    dimVariable = _currentStream.findVariable(dimName);
	    stepAttr = dimVariable.findAttribute(stepName);
	    pixelHeight = stepAttr.getNumericValue().floatValue();

	    // Length:  3-d volume --> dim 0; 4-d volume --> dim 1
	    dim = 0;
	    if (imageVariable.getRank() == 4 && !hasColorImages()) { dim = 1; }
	    dimName = imageVariable.getDimension(dim).getName();
	    dimVariable = _currentStream.findVariable(dimName);
	    stepAttr = dimVariable.findAttribute(stepName);
	    sliceThickness = stepAttr.getNumericValue().floatValue();
	}
	catch (Exception e) {}

	// Bits per pixel
	int bitsPerPixel = getBitsPerPixel();

	// Data type
	String dataType = "Undetermined";
	Class type = getDataType();
	if ( hasColorImages() ) { dataType = BasicMetadataFormat.BYTE_TYPE; }
	else if (type == Short.TYPE && hasSignedData()) {
	    dataType = BasicMetadataFormat.SIGNED_SHORT_TYPE;
	}
	else if (bitsPerPixel == 8) {
	    dataType = BasicMetadataFormat.BYTE_TYPE;
	}
	else if (bitsPerPixel == 16) {
	    dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE;
	}
	else if (type == Integer.TYPE) {
	    dataType = BasicMetadataFormat.SIGNED_INT_TYPE;
	}
	else if (type == Float.TYPE) {
	    dataType = BasicMetadataFormat.FLOAT_TYPE;
	}
	else if (type == Double.TYPE) {
	    dataType = BasicMetadataFormat.DOUBLE_TYPE;
	}

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;
	if ( hasColorImages() ) {colorType = BasicMetadataFormat.RGB_TYPE;}

	// Return the basic tree based upon available information
	if (pixelWidth == 0 && pixelHeight == 0 && sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, dataType,
						  bitsPerPixel, colorType);
	}
	if (sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, pixelWidth,
						  pixelHeight, dataType,
						  bitsPerPixel, colorType);
	}
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }
}
