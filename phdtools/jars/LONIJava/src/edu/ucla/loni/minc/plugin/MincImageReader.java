/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.minc.plugin;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

import ucar.ma2.Array;
import ucar.ma2.ArrayAbstract;
import ucar.ma2.MultiArray;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfStream;
import ucar.nc2.Variable;
import edu.ucla.loni.imageio.Utilities;

/**
 * Image Reader for parsing and decoding MINC images.
 *
 * @version 4 January 2006
 */
public class MincImageReader extends ImageReader
{
    /** Dimensions, Variables, and Attributes decoded from the input source. */
    private NetcdfStream _netcdfStream;

    /**
     * Constructs a MincImageReader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not a
     *                                  Minc Image Reader Spi.
     */
    public MincImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be a Minc Image Reader Spi
	if ( !(originatingProvider instanceof MincImageReaderSpi) ) {
	    String msg = "Originating provider must be a MINC Image Reader " +
		"SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The ImageInputStream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of the classes returned by the
     *                                  originating service provider's
     *                                  getInputTypes method, or is not an
     *                                  ImageInputStream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	super.setInput(input, seekForwardOnly, ignoreMetadata);

	// Remove any decoded NetCDF parameters
	_netcdfStream = null;
    }

    /**
     * Returns the number of images, not including thumbnails, available from
     * the current input source.
     * 
     * @param allowSearch If true, the true number of images will be returned
     *                    even if a search is required.  If false, the reader
     *                    may return -1 without performing the search.
     *
     * @return The number of images or -1 if allowSearch is false and a search
     *         would be required.
     *
     * @throws IllegalStateException If the input source has not been set.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    public int getNumImages(boolean allowSearch) throws IOException
    {
	MincMetadata metadata = new MincMetadata( _getNetcdfParams() );
	int numberOfImages = metadata.getNumberOfImages();

	// Unable to determine the number of images
	if (numberOfImages == -1) {
	    String msg = "Unable to determine the number of images.";
	    throw new IOException(msg);
	}

	return numberOfImages;
    }

    /**
     * Returns the width in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The width of the image in pixels.
     *
     * @throws IOException If an error occurs reading the width information from
     *                     the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getWidth(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image width
	MincMetadata metadata = new MincMetadata( _getNetcdfParams() );
	int width = metadata.getImageWidth();

	// Unable to determine the image width
	if (width == -1) {
	    String msg = "Unable to determine the image width.";
	    throw new IOException(msg);
	}

	return width;
    }

    /**
     * Returns the height in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The height of the image in pixels.
     *
     * @throws IOException If an error occurs reading the height information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getHeight(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image height
	MincMetadata metadata = new MincMetadata( _getNetcdfParams() );
	int height = metadata.getImageHeight();

	// Unable to determine the image height
	if (height == -1) {
	    String msg = "Unable to determine the image height.";
	    throw new IOException(msg);
	}

	return height;
    }

    /**
     * Returns an Iterator containing possible image types to which the given
     * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
     * legal image type will be returned.
     *
     * @param imageIndex The index of the image to be retrieved.
     *
     * @return An Iterator containing at least one ImageTypeSpecifier
     *         representing suggested image types for decoding the current given
     *         image.
     *
     * @throws IOException If an error occurs reading the format information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public Iterator getImageTypes(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image parameters
	MincMetadata metadata = new MincMetadata( _getNetcdfParams() );
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);
	int bitsPerPixel = metadata.getBitsPerPixel();
	Class type = metadata.getDataType();

	// Color images
	ArrayList list = new ArrayList(1);
	if ( metadata.hasColorImages() ) {
	    list.add( Utilities.getRgbImageType(width, height) );
	    return list.iterator();
	}

	// Determine the appropriate Image Type Specifier
	if (type == Short.TYPE && metadata.hasSignedData()) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_SHORT) );
	}
	else if (bitsPerPixel == 8 || bitsPerPixel == 16) {
	    list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
	}
	else if (type == Integer.TYPE) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_INT) );
	}
	else if (type == Float.TYPE) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_FLOAT) );
	}
	else if (type == Double.TYPE) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_DOUBLE) );
	}
	
	// Check if the image data type is supported
	if (list.size() == 0) {
	    String msg = "Unsupported MINC image type:  data type = " + type +
		"; bits per voxel = " + bitsPerPixel + ".";
	    throw new IOException(msg);
	}

	// Return the Image Type Specifier
	return list.iterator();
    }

    /**
     * Returns an IIOMetadata object representing the metadata associated with
     * the input source as a whole (i.e., not associated with any particular
     * image), or null if the reader does not support reading metadata, is set
     * to ignore metadata, or if no metadata is available.
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     */
    public IIOMetadata getStreamMetadata() throws IOException
    {
	return new MincMetadata( _getNetcdfParams() );
    }

    /**
     * Returns an IIOMetadata object containing metadata associated with the
     * given image, or null if the reader does not support reading metadata, is
     * set to ignore metadata, or if no metadata is available.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved. 
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
	// Not applicable
	return null;
    }

    /**
     * Reads the image indexed by imageIndex and returns it as a complete
     * BufferedImage.  The supplied ImageReadParam is ignored.
     *
     * @param imageIndex The index of the image to be retrieved.
     * @param param An ImageReadParam used to control the reading process, or
     *              null.
     *
     * @return The desired portion of the image as a BufferedImage.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public BufferedImage read(int imageIndex, ImageReadParam param)
	throws IOException
    {
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// Update the Listeners
	processImageStarted(imageIndex);

	// Determine the corresponding array of image data
	String imageName = MincMetadataFormat.MI_IMAGE;
	Variable imageVariable = _getNetcdfParams().findVariable(imageName);

	// Raw image data requested for 3-d color volume 
	MincMetadata metadata = new MincMetadata( _getNetcdfParams() );
	if ( metadata.hasColorImages() && Utilities.returnRawBytes(param) ) {
	    int imageSize = 3*width*height;

	    // Allocate room for the image data
	    BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	    DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	    byte[] imageBytes = ((DataBufferByte)db).getData();

	    // Copy the image data and return
	    _copyColorData(imageVariable, imageIndex, width, imageBytes);
	    return bufferedImage;
	}

	// Create a Buffered Image for the image data
	Iterator iter = getImageTypes(0);
	BufferedImage bufferedImage = getDestination(null, iter, width, height);
	DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

	// 3-d color volume
	if ( metadata.hasColorImages() ) {

	    // Copy the image data
	    byte[] imageBytes = new byte[3*width*height];
	    _copyColorData(imageVariable, imageIndex, width, imageBytes);

	    // Update the Listeners
	    if ( abortRequested() ) { processReadAborted(); }
	    else { processImageComplete(); }

	    // Return the color image
	    return Utilities.getRgbImage(imageBytes, bufferedImage.getRaster(),
					 true);
	}

	// 3-d volume
	MultiArray imageArray = null;
	if (imageVariable.getRank() == 3) {
	    imageArray = imageVariable.sliceMA(0, imageIndex);
	}

	// 4-d volume
	else if (imageVariable.getRank() == 4) {
	    int length = imageVariable.getDimension(1).getLength();
	    int lengthIndex = imageIndex % length;
	    int timeIndex = imageIndex / length;

	    // Index the 4-d array two times to get a 2-d array
	    imageArray = imageVariable.sliceMA(0, timeIndex);
	    imageArray = imageArray.sliceMA(0, lengthIndex);
	}

	// Read the image data into an Array
	Array dataArray = imageArray.read();

	// 8-bit gray image pixel data
	WritableRaster dstRaster = bufferedImage.getRaster();
	if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	    byte[] srcBuffer = (byte[])((ArrayAbstract)dataArray).getStorage();
	    bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
	}

	// 16-bit gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_USHORT ||
		 dataBuffer.getDataType() == DataBuffer.TYPE_SHORT)
	    {
		short[] srcBuffer =
		    (short[])((ArrayAbstract)dataArray).getStorage();

		// Signed or unsigned data
		if ( metadata.hasSignedData() ) {
		    bufferedImage = Utilities.getGrayShortImage(srcBuffer,
								dstRaster);
		}
		else {
		    bufferedImage = Utilities.getGrayUshortImage(srcBuffer,
								 dstRaster);
		}
	    }

	// Integer gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_INT) {
//		System.out.println("int gray image pixel data");
	    int[] srcBuffer = (int[])((ArrayAbstract)dataArray).getStorage();
	    bufferedImage = Utilities.getGrayIntImage(srcBuffer, dstRaster);
	}

	// Float gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_FLOAT) {
//		System.out.println("float gray image pixel data");
	    float[] srcBuffer =(float[])((ArrayAbstract)dataArray).getStorage();
	    bufferedImage = Utilities.getGrayFloatImage(srcBuffer, dstRaster);
	}

	// Double gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_DOUBLE) {
//		System.out.println("double gray image pixel data");
	    double[] srcBuffer =
		(double[])((ArrayAbstract)dataArray).getStorage();
	    bufferedImage = Utilities.getGrayDoubleImage(srcBuffer, dstRaster);
	}

	// Update the Listeners
	if ( abortRequested() ) { processReadAborted(); }
	else { processImageComplete(); }

	// Return the requested image
	return bufferedImage;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageReader.  Otherwise, the reader may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the input stream
	try { if (input != null) { ((ImageInputStream)input).close(); } }
	catch (Exception e) {}

	input = null;
    }

    /**
     * Checks whether or not the specified image index is valid.
     *
     * @param imageIndex The index of the image to check.
     *
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private void _checkImageIndex(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	if ( imageIndex >= getNumImages(false) ) {
	    String msg = "Image index of " + imageIndex + " >= the total " +
		"number of images (" + getNumImages(false) + ")";
	    throw new IndexOutOfBoundsException(msg);
	}
    }

    /**
     * Copies 3d color image data from the image Variable to the byte array.
     *
     * @param imageVariable Image Variable containing 3d color data.
     * @param imageIndex Index of the image slice.
     * @param width Width of the image slice.
     * @param imageBytes Byte array to copy the 3d color data to.
     *
     * @throws IOException If an error occurs while copying.
     */
    private void _copyColorData(Variable imageVariable, int imageIndex,
				int width, byte[] imageBytes)
	throws IOException
    {
	// Reduce the 3-d image array to a 1-d array
	MultiArray imageArray = imageVariable.sliceMA(0, imageIndex);
	int[] shape = imageArray.getShape();
	for (int i = 0; i < shape[0]; i++) {
	    Array line = imageArray.sliceMA(0, i).read();
	    byte[] bLine = (byte[])((ArrayAbstract)line).getStorage();
	    System.arraycopy(bLine, 0, imageBytes, 3*i*width, bLine.length);
	}
    }

    /**
     * Reads the NetCDF parameters from the input source.  If the parameters
     * have already been decoded, no additional read is performed.
     *
     * @return NetCDF parameters decoded from the input source.
     * 
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     */
    private NetcdfStream _getNetcdfParams() throws IOException
    {
	// Decode the NetCDF parameters if needed
	if (_netcdfStream == null) {

	    // No input source to decode from
	    if (input == null) {
		String msg = "No input source has been set.";
		throw new IllegalStateException(msg);
	    }

	    // Decode the NetCDF parameters
	    _netcdfStream = new NetcdfStream( (ImageInputStream)input );

	    // Read in the Arrays of the Variables
	    Iterator iter = _netcdfStream.getVariableIterator();
	    while ( iter.hasNext() ) {
		Variable variable = (Variable)iter.next();
		String varName = variable.getName();

		// Do not cache data of the MINC image variable
		if ( !MincMetadataFormat.MI_IMAGE.equals(varName) ) {
		    _netcdfStream.cacheData(varName);
		}
	    }

	    // Get the MINC image variable
	    String imageName = MincMetadataFormat.MI_IMAGE;
	    Variable imageVariable = _netcdfStream.findVariable(imageName);
	    if (imageVariable == null) {
		String msg = "Cannot find the MINC image variable.";
		throw new IOException(msg);
	    }

	    // Check that the MINC image variable is 3- or 4-dimensional
	    if (imageVariable.getRank() != 3 && imageVariable.getRank() != 4) {
		String msg = "MINC image variable has " +
		    imageVariable.getRank() + " dimensions; it should have 3 " +
		    "or 4.";
		throw new IOException(msg);
	    }

	    // Check for a 3-dimensional color MINC image
	    if (imageVariable.getRank() == 4) {
		Dimension fastestDim = imageVariable.getDimension(3);

		// Fastest varying dimension is the vector dimension
		String dimName = fastestDim.getName();
		if ( MincMetadataFormat.MI_VECTOR_DIMENSION.equals(dimName) ) {

		    // Dimension must have RGB byte values
		    if (imageVariable.getElementType() != Byte.TYPE) {
			String msg = "No support for non-byte color images.";
			throw new IOException(msg);
		    }

		    if (fastestDim.getLength() != 3) {
			String msg="Color images must have 3 (RGB) components.";
			throw new IOException(msg);
		    }
		}
	    }
	}
      
	return _netcdfStream;
    }
}
