/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.raw.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * Raw Metadata class.
 *
 * @version 3 July 2003
 */
public class RawMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_raw_1.0";

  /** Single instance of the RawMetadataFormat. */
  private static RawMetadataFormat _format = new RawMetadataFormat();

  /** Constructs a RawMetadataFormat. */
  private RawMetadataFormat()
    {
      // Create the root with no children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_EMPTY);
    }

  /**
   * Gets an instance of the RawMetadataFormat.
   *
   * @return The single instance of the RawMetadataFormat.
   */
  public static RawMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
