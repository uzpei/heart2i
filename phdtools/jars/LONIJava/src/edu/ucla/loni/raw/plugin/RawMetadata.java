/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.raw.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.Node;

/**
 * IIO Metadata for metadata of the RAW image format.
 *
 * @version 3 June 2004
 */
public class RawMetadata extends AppletFriendlyIIOMetadata
{
  /** Constructs a Raw Metadata. */
  public RawMetadata()
    {
      // Do not support the standard metadata format
      super(false, RawMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    RawMetadataFormat.class.getName(), null, null);
    }

  /**
   * Returns true if this object does not support the mergeTree, setFromTree,
   * and reset methods.  
   *
   * @return True if this IIOMetadata object cannot be modified; false o/w.
   */
  public boolean isReadOnly()
    {
      return false;
    }

  /**
   * Returns an XML DOM Node object that represents the root of a tree of
   * metadata contained within this object according to the conventions
   * defined by a given metadata format.
   *
   * @param formatName Name of the desired metadata format.
   *
   * @return An XML DOM Node object forming the root of a tree.
   *
   * @throws IllegalArgumentException If formatName is not one of the allowed
   *                                  metadata format names.
   */
  public Node getAsTree(String formatName)
    {
      // Format name must match the native metadata format name
      if ( !nativeMetadataFormatName.equals(formatName) ) {
	String msg = "The format name \"" + formatName + "\" is not a " +
	             "supported metadata format name.";
	throw new IllegalArgumentException(msg);
      }

      // Create the root Node
      String rootName = RawMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      // Return the DOM tree
      return rootNode;
    }

  /**
   * Alters the internal state of this IIOMetadata object from a tree of XML
   * DOM Nodes whose syntax is defined by the given metadata format.  The
   * previous state is altered only as necessary to accomodate the nodes that
   * are present in the given tree.
   *
   * @param formatName Name of the desired metadata format.
   * @param root An XML DOM Node object forming the root of a tree.
   */
  public void mergeTree(String formatName, Node root)
    {
      // Nothing to do
      return;
    }

  /**
   * Resets all the data stored in this object to default values, usually to
   * the state this object was in immediately after construction, though the
   * precise semantics are plug-in specific.  Note that there are many possible
   * default values, depending on how the object was created.
   */
  public void reset()
    {
      // Nothing to do
      return;
    }
}
