/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.raw.plugin;

import edu.ucla.loni.imageio.Utilities;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for reading a raw image into a buffered image.
 *
 * @version 14 October 2004
 */
public class RawImageReader extends ImageReader
{
  /**
   * Constructs a Raw Image Reader.
   *
   * @param originatingProvider The Image Reader SPI that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not a
   *                                  Raw Image Reader Spi.
   */
  public RawImageReader(ImageReaderSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be a Raw Image Reader Spi
      if ( !(originatingProvider instanceof RawImageReaderSpi) ) {
	String msg = "Originating provider must be an Raw Image Reader " +
	             "SPI.";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Returns the number of images, not including thumbnails, available from the
   * current input source.
   * 
   * @param allowSearch If true, the true number of images will be returned
   *                    even if a search is required.  If false, the reader may
   *                    return -1 without performing the search.
   *
   * @return The number of images or -1 if allowSearch is false and a search
   *         would be required.
   */
  public int getNumImages(boolean allowSearch)
    {
      return 1;
    }

  /**
   * Returns the width in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The width of the image in pixels.
   *
   * @throws IOException If an error occurs reading the width information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getWidth(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the total length of the input stream
      ImageInputStream inputStream = _getInputStream();
      int length = (int)inputStream.length();
      if (length == -1) {
	String msg = "Unable to determine the image size.";
	throw new IOException(msg);
      }

      // Return the total length as the image width
      return length;
    }

  /**
   * Returns the height in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The height of the image in pixels.
   *
   * @throws IOException If an error occurs reading the height information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getHeight(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      return 1;
    }

  /**
   * Returns an Iterator containing possible image types to which the given
   * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
   * legal image type will be returned.
   *
   * @param imageIndex The index of the image to be retrieved.
   *
   * @return An Iterator containing at least one ImageTypeSpecifier
   *         representing suggested image types for decoding the current given
   *         image.
   *
   * @throws IOException If an error occurs reading the format information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public Iterator getImageTypes(int imageIndex) throws IOException
    {
      // Get image parameters
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);
      int bitsPerPixel = 8;

      // Return the Image Type Specifier
      ArrayList list = new ArrayList(1);
      list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
      return list.iterator();
    }

  /**
   * Returns an IIOMetadata object representing the metadata associated with
   * the input source as a whole (i.e., not associated with any particular
   * image), or null if the reader does not support reading metadata, is set
   * to ignore metadata, or if no metadata is available.
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   */
  public IIOMetadata getStreamMetadata() throws IOException
    {
      return new RawMetadata();
    }

  /**
   * Returns an IIOMetadata object containing metadata associated with the
   * given image, or null if the reader does not support reading metadata, is
   * set to ignore metadata, or if no metadata is available.
   *
   * @param imageIndex Index of the image whose metadata is to be retrieved. 
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
      // Not applicable
      return null;
    }

  /**
   * Reads the image indexed by imageIndex and returns it as a complete
   * BufferedImage.  The supplied ImageReadParam is ignored.
   *
   * @param imageIndex The index of the image to be retrieved.
   * @param param An ImageReadParam used to control the reading process, or
   *              null.
   *
   * @return The desired portion of the image as a BufferedImage.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public BufferedImage read(int imageIndex, ImageReadParam param)
    throws IOException
    {
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Update the Listeners
      processImageStarted(imageIndex);

      // Create a Buffered Image for the image data
      Iterator iter = getImageTypes(0);
      BufferedImage bufferedImage = getDestination(null, iter, width, height);

      // Read the image data into an array
      byte[] srcBuffer = new byte[width*height];
      ImageInputStream imageStream = _getInputStream();
      imageStream.readFully(srcBuffer);

      // 8-bit gray image pixel data
      WritableRaster dstRaster = bufferedImage.getRaster();
      bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);

      // Update the Listeners
      if ( abortRequested() ) { processReadAborted(); }
      else { processImageComplete(); }

      // Return the requested image
      return bufferedImage;
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageReader.  Otherwise, the reader may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the input stream
      try { if (input != null) { ((ImageInputStream)input).close(); } }
      catch (Exception e) {}

      input = null;
    }

  /**
   * Checks whether or not the specified image index is valid.
   *
   * @param imageIndex The index of the image to check.
   *
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  private void _checkImageIndex(int imageIndex)
    {
      // Check the bounds of the image index
      if ( imageIndex >= getNumImages(false) ) {
	String msg = "Image index of " + imageIndex + " >= the total number " +
	             "of images (" + getNumImages(false) + ")";
	throw new IndexOutOfBoundsException(msg);
      }
    }

  /**
   * Gets the input stream for the pixel data.
   *
   * @return Image Input Stream to read Raw image pixel data from.
   *
   * @throws IllegalStateException If the input has not been set.
   */
  private ImageInputStream _getInputStream()
    {
      // No input has been set
      if (input == null) {
	String msg = "No input has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Input Stream for image pixel data
      return (ImageInputStream)input;
    }
}
