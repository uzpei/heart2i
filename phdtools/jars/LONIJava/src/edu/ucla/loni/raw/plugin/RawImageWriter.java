/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.raw.plugin;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;

/**
 * Image Writer for writing raw images.
 *
 * @version 14 October 2004
 */
public class RawImageWriter extends ImageWriter
{
  /** Number of images in the sequence currently being written. */
  private int _numberOfImagesWritten;

  /**
   * Constructs a Raw Image Writer.
   *
   * @param originatingProvider The ImageWriterSpi that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not a
   *                                  Raw Image Writer Spi.
   */
  public RawImageWriter(ImageWriterSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be a Raw Image Writer SPI
      if ( !(originatingProvider instanceof RawImageWriterSpi) ) {
	String msg = "Originating provider must a Raw Image Writer SPI.";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Returns an IIO Metadata object containing default values for encoding a
   * stream of images.
   *
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIO Metadata object.
   */
  public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
      // Return a blank metadata object
      return new RawMetadata();
    }

  /**
   * Returns an IIO Metadata object containing default values for encoding an
   * image of the given type.
   *
   * @param imageType An ImageTypeSpecifier indicating the format of the image
   *                  to be written later.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object.
   */
  public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					     ImageWriteParam param)
    {
      // Not applicable
      return null;
    }

  /**
   * Returns an IIOMetadata object that may be used for encoding and optionally
   * modified using its document interfaces or other interfaces specific to the
   * writer plug-in that will be used for encoding.
   *
   * @param inData An IIOMetadata object representing stream metadata, used to
   *               initialize the state of the returned object.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object, or null if the plug-in does not provide
   *         metadata encoding capabilities.
   *
   * @throws IllegalArgumentException If inData is null.
   */
  public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					   ImageWriteParam param)
    {
      // Only recognize Raw Metadata
      if (inData instanceof RawMetadata) { return inData; }

      // Otherwise perform no conversion
      return null;
    }

  /**
   * Returns an IIOMetadata object that may be used for encoding and optionally
   * modified using its document interfaces or other interfaces specific to the
   * writer plug-in that will be used for encoding.
   *
   * @param inData An IIOMetadata object representing image metadata, used to
   *               initialize the state of the returned object.
   * @param imageType An ImageTypeSpecifier indicating the layout and color
   *                  information of the image with which the metadata will be
   *                  associated.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object, or null if the plug-in does not provide
   *         metadata encoding capabilities.
   *
   * @throws IllegalArgumentException If either of inData or imageType is null.
   */
  public IIOMetadata convertImageMetadata(IIOMetadata inData,
					  ImageTypeSpecifier imageType,
					  ImageWriteParam param)
    {
      // Not applicable
      return null;
    }

  /**
   * Returns true if the methods that take an IIOImage parameter are capable
   * of dealing with a Raster (as opposed to RenderedImage) source image.
   *
   * @return True if Raster sources are supported.
   */
  public boolean canWriteRasters()
    {
      return true;
    }

  /**
   * Appends a complete image stream containing a single image and associated
   * stream and image metadata and thumbnails to the output.
   *
   * @param streamMetadata An IIOMetadata object representing stream metadata,
   *                       or null to use default values.
   * @param image An IIOImage object containing an image, thumbnails, and
   *              metadata to be written.
   * @param param An ImageWriteParam, or null to use a default ImageWriteParam.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the image is null, if the stream
   *                                  metadata cannot be converted, or if the
   *                                  image type is unrecognized.
   * @throws IllegalStateException If the output has not been set.
   */
  public void write(IIOMetadata streamMetadata, IIOImage image,
		    ImageWriteParam param) throws IOException
    {
      // Write a sequence with 1 image
      prepareWriteSequence(streamMetadata);
      writeToSequence(image, param);
      endWriteSequence();
    }

  /**
   * Returns true if the writer is able to append an image to an image
   * stream that already contains header information and possibly prior
   * images.
   *
   * @return True If images may be appended sequentially.
   */
  public boolean canWriteSequence()
    {
      return true;
    }

  /**
   * Prepares a stream to accept a series of subsequent writeToSequence calls,
   * using the provided stream metadata object.  The metadata will be written
   * to the stream if it should precede the image data.
   *
   * @param streamMetadata A stream metadata object.
   *
   * @throws IllegalArgumentException If the stream metadata is null or cannot
   *                                  be converted.
   */
  public void prepareWriteSequence(IIOMetadata streamMetadata)
    {
      // Convert the metadata to Raw Metadata
      IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

      // Unable to convert the metadata
      if (metadata == null) {
	String msg = "Unable to convert the stream metadata for encoding.";
	throw new IllegalArgumentException(msg);
      }

      _numberOfImagesWritten = 0;
    }

  /**
   * Appends a single image and possibly associated metadata and thumbnails,
   * to the output.  The supplied ImageReadParam is ignored.
   *
   * @param image An IIOImage object containing an image, thumbnails, and
   *              metadata to be written.
   * @param param An ImageWriteParam or null to use a default ImageWriteParam.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the image is null or if the image type
   *                                  is not recognized.
   * @throws IllegalStateException If the output has not been set.
   */
  public void writeToSequence(IIOImage image, ImageWriteParam param)
    throws IOException
    { 
      // Check for a null image
      if (image == null) {
	String msg = "Cannot write a null image.";
	throw new IllegalArgumentException(msg);
      }

      // Get a Raster from the image
      Raster raster = image.getRaster();
      if (raster == null) {
	RenderedImage renderedImage = image.getRenderedImage();

	// If the Rendered Image is a Buffered Image, get the Raster directly
	if (renderedImage instanceof BufferedImage) {
	  raster = ((BufferedImage)renderedImage).getRaster();
	}

	// Otherwise get a copy of the Raster from the Rendered Image
	raster = renderedImage.getData();
      }

      // Update the Listeners
      processImageStarted(_numberOfImagesWritten);

      ImageOutputStream outputStream = _getOutputStream();
      DataBuffer dataBuffer = raster.getDataBuffer();

      // Write 8 bit image data
      if (dataBuffer instanceof DataBufferByte) {
	outputStream.write( ((DataBufferByte)dataBuffer).getData() );
      }

      // Write double image data
      else if (dataBuffer instanceof DataBufferDouble) {
	double[] data = ((DataBufferDouble)dataBuffer).getData();
	outputStream.writeDoubles(data, 0, data.length);
      }

      // Write float image data
      else if (dataBuffer instanceof DataBufferFloat) {
	float[] data = ((DataBufferFloat)dataBuffer).getData();
	outputStream.writeFloats(data, 0, data.length);
      }

      // Write integer image data
      else if (dataBuffer instanceof DataBufferInt) {
	int[] data = ((DataBufferInt)dataBuffer).getData();
	outputStream.writeInts(data, 0, data.length);
      }

      // Write 16 bit signed image data
      else if (dataBuffer instanceof DataBufferShort) {
	short[] data = ((DataBufferShort)dataBuffer).getData();
	outputStream.writeShorts(data, 0, data.length);
      }

      // Write 16 bit unsigned image data
      else if (dataBuffer instanceof DataBufferUShort) {
	short[] data = ((DataBufferUShort)dataBuffer).getData();
	outputStream.writeShorts(data, 0, data.length);
      }

      // Unrecognized type
      else {
	String msg = "Unable to write the IIOImage.";
	throw new IllegalArgumentException(msg);
      }

      // Update the Listeners
      _numberOfImagesWritten++;
      if ( abortRequested() ) { processWriteAborted(); }
      else { processImageComplete(); }
    }

  /**
   * Completes the writing of a sequence of images begun with
   * prepareWriteSequence.  Any stream metadata that should come at the end of
   * the sequence of images is written out, and any header information at the
   * beginning of the sequence is patched up if necessary.
   */
  public void endWriteSequence()
    {
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageWriter.  Otherwise, the writer may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the output stream
      try { if (output != null) { ((ImageOutputStream)output).close(); } }
      catch (Exception e) {}

      output = null;
    }

  /**
   * Gets the output stream.
   *
   * @return Image Output Stream to write to.
   *
   * @throws IllegalStateException If the output has not been set.
   */
  private ImageOutputStream _getOutputStream()
    {
      // No output has been set
      if (output == null) {
	String msg = "No output has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Output Stream for the metadata
      return (ImageOutputStream)output;
    }
}
