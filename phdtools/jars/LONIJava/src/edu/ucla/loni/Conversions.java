/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni;

/**
 * Utility methods used to convert between data types.
 *
 * @version 22 March 2005
 */
public class Conversions
{
  /** Mapping from 4 bit values to a hexadecimal number. */
  private static char[] _hex = {'0', '1', '2', '3', '4', '5', '6', '7',
				'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

  /** Mapping from character values to their hexadecimal evaluations. */
  private static int[] _eval = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
				0, 0, 0, 0, 0, 0, 0, 10, 11, 12,
				13, 14, 15};

  /** Constructs a Conversions. */
  private Conversions()
    {
    }

  /**
   * Creates a String representation of the byte array as a series of 2-digit
   * hexadecimal numbers.
   *
   * @param bytes Byte array to convert to a String.
   *
   * @return String representation of the byte array as a series of 2-digit
   *         hexadecimal numbers.
   */
  public static final String toHexString(byte[] bytes)
    {
      // Allocate room for two characters per byte
      char[] hexArray = new char[2*bytes.length];

      // Convert each byte
      for (int i = 0; i < bytes.length; i++) {

	// Convert each 4 bit value
	hexArray[2*i] = _hex[(bytes[i] & 0xF0) >> 4];
	hexArray[2*i+1] = _hex[bytes[i] & 0x0F];
      }

      // Return the String representation
      return new String(hexArray);
    }

  /**
   * Creates a byte array from the series of 2-digit hexadecimal numbers.
   *
   * @param hexString String of 2-digit hexadecimal numbers.
   *
   * @return Byte array from the series of 2-digit hexadecimal numbers.
   *
   * @throws IllegalArgumentException If the String contains an odd number of
   *                                  characters.
   */
  public static final byte[] toByteArray(String hexString)
    {
      // Check to see if the String has an odd length
      if ( hexString.length() % 2 == 1 ) {
	String msg = "Hexadecimal string does not have an even length.";
	throw new IllegalArgumentException(msg);
      }

      // Allocate room for the bytes
      byte[] byteArray = new byte[hexString.length()/2];

      // Convert each 2 String characters to a byte
      for (int i = 0; i < hexString.length(); i+=2) {
 	int digit = ((_eval[hexString.charAt(i)-48] << 4) & 0xF0) +
	              _eval[hexString.charAt(i+1)-48];

	byteArray[i/2] = (byte)digit;
      }

      // Return the byte array
      return byteArray;
    }
}
