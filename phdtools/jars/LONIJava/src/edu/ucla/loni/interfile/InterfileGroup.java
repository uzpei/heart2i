/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile;

import java.util.Iterator;
import java.util.Vector;

/**
 * Group of Key-Value Pairs and other Groups.
 *
 * @version 21 July 2005
 */
public class InterfileGroup
{
    /** Key-Value Pairs and Interfile Groups in this Group. */
    private Vector _elements;

    /** Constructs an Interfile Group. */
    public InterfileGroup()
    {
	_elements = new Vector();
    }

    /**
     * Gets all the Key-Value Pairs and Interfile Groups in this Group.
     *
     * @return All the Key-Value Pairs and Interfile Groups, in the order they
     *         were added to this Group.
     */
    public Iterator getElements()
    {
	return _elements.iterator();
    }

    /**
     * Gets all the Key-Value Pairs in this Group.
     *
     * @return All the Key-Value Pairs in this Group.
     */
    public Iterator getKeyValuePairs()
    {
	Vector pairs = new Vector();

	// Filter all the Key-Value Pairs
	Iterator iter = _elements.iterator();
	while ( iter.hasNext() ) {
	    Object element = iter.next();
	    if (element instanceof KeyValuePair) { pairs.add(element); }
	}

	return pairs.iterator();
    }

    /**
     * Gets all the values of the Key-Value Pairs in this Group that have the
     * specified key.
     *
     * @param key Key to find values for.
     * @param ignoreCase True if upper/lower case distinctions are to be
     *                   ignored in key matching, false otherwise.
     * @param searchSubgroups True if all the Interfile Groups in this Group are
     *                        to be searched; false if only this Group is to be
     *                        searched.
     *
     * @return All the values of the Key-Value Pairs that have the specified
     *         key.
     */
    public String[] getValues(String key, boolean ignoreCase,
			      boolean searchSubgroups)
    {
	Vector values = new Vector();

	// Determine the Groups to search
	Iterator iter = getKeyValuePairs();
	if (searchSubgroups) { iter = getElements(); }

	// Search for the key
	while ( iter.hasNext() ) {
	    Object element = iter.next();

	    // Check the key of a Key-Value Pair
	    if (element instanceof KeyValuePair) {
		KeyValuePair pair = (KeyValuePair)element;

		// Case insensitive
		if (ignoreCase) {
		    if ( pair.getKey().equalsIgnoreCase(key) ) {
			values.add( pair.getValue() );
		    }
		}

		// Case sensitive
		else if ( pair.getKey().equals(key) ) {
		    values.add( pair.getValue() );
		}
	    }

	    // Search the values of a subgroup
	    else if (element instanceof InterfileGroup) {
		InterfileGroup subgroup = (InterfileGroup)element;
		String[] subValues = subgroup.getValues(key, ignoreCase, true);
		for (int i = 0; i < subValues.length; i++) {
		    values.add(subValues[i]);
		}
	    }
	}

	// Return all the values matching the key
	return (String[])values.toArray(new String[0]);
    }

    /**
     * Adds a Key-Value Pair to this Group.
     *
     * @param pair Key-Value Pair.
     *
     * @throws NullPointerException If the Pair is null.
     */
    public void addKeyValuePair(KeyValuePair pair)
    {
	// Check the Pair
	if (pair == null) { throw new NullPointerException("Null pair."); }

	_elements.add(pair);
    }

    /**
     * Gets all the Interfile Groups in this Group.
     *
     * @return All the Interfile Groups in this Group.
     */
    public Iterator getGroups()
    {
	Vector groups = new Vector();

	// Filter all the Interfile Groups
	Iterator iter = _elements.iterator();
	while ( iter.hasNext() ) {
	    Object element = iter.next();
	    if (element instanceof InterfileGroup) { groups.add(element); }
	}

	return groups.iterator();
    }

    /**
     * Adds an Interfile Group to this Group.
     *
     * @param group Interfile Group.
     *
     * @throws NullPointerException If the Group is null.
     */
    public void addGroup(InterfileGroup group)
    {
	// Check the Group
	if (group == null) { throw new NullPointerException("Null group."); }

	_elements.add(group);
    }
}
