/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile;

/**
 * Interfile key and value pair.
 *
 * @version 29 June 2005
 */
public class KeyValuePair implements Comparable
{
  /** Key. */
  private String _key;

  /** Value. */
  private String _value;

  /** Comment or null if there is no comment. */
  private String _comment;

  /** True if the Key-Value Pair is required; false if it is optional. */
  private boolean _isRequired;

  /**
   * Constructs a Key-Value Pair.
   *
   * @param key Key.
   * @param value Value.
   * @param comment Comment or null if there is no comment.
   * @param isRequired True if the Key-Value Pair is required; false if it is
   *                   optional.
   *
   * @throws NullPointerException If the key or value is null.
   */
  public KeyValuePair(String key, String value, String comment,
		      boolean isRequired)
    {
      // Check the key
      if (key == null) {
	throw new NullPointerException("Key cannot be null.");
      }

      // Check the value
      if (value == null) {
	throw new NullPointerException("Value cannot be null.");
      }

      _key = key;
      _value = value;
      _comment = comment;
      _isRequired = isRequired;
    }

  /**
   * Get the key.
   *
   * @return Key.
   */
  public String getKey()
    {
      return _key;
    }

  /**
   * Get the value.
   *
   * @return Value.
   */
  public String getValue()
    {
      return _value;
    }

  /**
   * Get the comment.
   *
   * @return Comment or null if there is no comment.
   */
  public String getComment()
    {
      return _comment;
    }

  /**
   * Determines if the Key-Value Pair is required.
   *
   * @return True if the Key-Value Pair is required; false if it is optional.
   */
  public boolean isRequired()
    {
      return _isRequired;
    }

  /**
   * Determines whether or not the specified Object is equal to this Object.
   *
   * @param obj Object to compare to.
   *
   * @return True if the specified Object is equal to this Object; false o/w.
   */
  public boolean equals(Object obj)
    {
      // Object is a Key-Value Pair
      if (obj instanceof KeyValuePair) {
	KeyValuePair thatPair = (KeyValuePair)obj;

	// Compare the keys
	return _getComparableKey().equals( thatPair._getComparableKey() );
      }

      return super.equals(obj);
    }

  /**
   * Determines if the specified Object is less than, equal to, or greater than
   * this Object.
   *
   * @param obj Object to compare to.
   *
   * @return A negative integer, zero, or a positive integer as this Object
   *	     is less than, equal to, or greater than the specified Object.
   *
   * @throws ClassCastException If the Object is not a Key-Value Pair.
   */
  public int compareTo(Object obj)
    {
      KeyValuePair thatPair = (KeyValuePair)obj;

      // Compare the keys
      return _getComparableKey().compareTo( thatPair._getComparableKey() );
    }

  /**
   * Gets a string representation of this Object.
   *
   * @return String representation of this Object.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      buffer.append("Key = \"" + getKey() + "\" ");
      buffer.append("Value = \"" + getValue() + "\" ");
      buffer.append("Is Required = " + isRequired() + " ");
      if (getComment() != null) {
	buffer.append("Comment = \"" + getComment() + "\" ");
      }

      return buffer.toString();
    } 

  /**
   * Gets the key as a string that can be compared to other keys.
   *
   * @return Key that can be compared to other keys.
   */
  private String _getComparableKey()
    {
      // Replace tabs and underscores with spaces
      String key = _key.replace('\t', ' ');
      key = key.replace('_', ' ');

      // Remove leading and trailing spaces
      key = key.trim();

      // Return the key as lower case
      return key.toLowerCase();
    }
}
