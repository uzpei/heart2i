/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile;

import java.util.Iterator;
import javax.imageio.IIOException;
import javax.imageio.stream.ImageOutputStream;

/**
 * Class that encodes an Interfile Group.
 *
 * @version 9 August 2005
 */
public class InterfileGroupEncoder
{
    /** Constructs an Interfile Group Encoder. */
    public InterfileGroupEncoder()
    {
    }

    /**
     * Encodes the specified Interfile Group into the given stream.
     *
     * @param group Interfile Group to encode.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IIOException If an I/O error occurs.
     */
    public void encode(InterfileGroup group, ImageOutputStream outputStream)
	throws IIOException
    {
	try {
	    KeyValuePairEncoder pairEncoder = new KeyValuePairEncoder();

	    // Encode each Group element
	    Iterator iter = group.getElements();
	    while ( iter.hasNext() ) {
		Object element = iter.next();

		// Key-Value Pair
		if (element instanceof KeyValuePair) {
		    String pair = pairEncoder.encode( (KeyValuePair)element );
		    outputStream.writeBytes(pair);
		}

		// Interfile Group
		else if (element instanceof InterfileGroup) {
		    encode((InterfileGroup)element, outputStream);
		}
	    }
	}

	catch (Exception e) {
	    String msg = "Unable to encode the group.";
	    throw new IIOException(msg, e);
	}
    }
}
