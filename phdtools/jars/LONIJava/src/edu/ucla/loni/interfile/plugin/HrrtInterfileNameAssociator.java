/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.FileNameAssociator;
import java.util.Vector;

/**
 * Interfile Name Associator for generating file name associations between HRRT
 * Interfile files.
 *
 * @version 2 August 2005
 */
public class HrrtInterfileNameAssociator extends InterfileNameAssociator
{
    /** Constructs an HRRT Interfile Name Associator. */
    public HrrtInterfileNameAssociator()
    {
    }

    /**
     * Gets all the allowed file names that can be associated with the specified
     * file name.
     *
     * @param fileName Name of the file to get associated file names for.
     *
     * @return All the allowed file names that can be associated with the
     *         specified file name.  An empty array is returned if there are no
     *         associated file names.
     */
    public String[] getAssociatedFileNames(String fileName)
    {
	// No name 
	if (fileName == null) { return new String[0]; }

	// File is the metadata file
	if ( isMetadataFile(fileName) ) {
	    Vector names = new Vector();

	    // Pair FILE.hdr with FILE.i
	    String[] names2 = _getSuffixReplacements(fileName, "hdr", "i");
	    for (int i = 0; i < names2.length; i++) { names.add(names2[i]); }

	    // Pair FILE.hdr with FILE
	    names.add( fileName.substring(0, fileName.length()-4) );

	    // Return all the allowed file names
	    return (String[])names.toArray(new String[0]);
	}
	    
	// File is the image pixel file
	else {
	    Vector names = new Vector();

	    // Pair FILE.i with FILE.hdr
	    if ( fileName.toLowerCase().endsWith(".i") ) {
		String[] n = _getSuffixReplacements(fileName, "i", "hdr");
		for (int i = 0; i < n.length; i++) { names.add(n[i]); }
	    }

	    // Pair FILE with FILE.hdr
	    names.add(fileName + ".hdr");
	    names.add(fileName + ".HDR");

	    // Return all the allowed file names
	    return (String[])names.toArray(new String[0]);
	}
    }
}
