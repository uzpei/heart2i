/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.interfile.InterfileGroup;
import edu.ucla.loni.interfile.InterfileGroupDecoder;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for parsing and decoding Interfile images.
 *
 * @version 9 August 2005
 */
public class InterfileImageReader extends ImageReader
{
    /** Interfile Group decoded from the input source. */
    protected InterfileGroup _interfileGroup;

    /**
     * Constructs an Interfile Image Reader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  Interfile Image Reader Spi.
     */
    public InterfileImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an Interfile Image Reader Spi
	if ( !(originatingProvider instanceof InterfileImageReaderSpi) ) {
	    String msg = "Originating provider must be an Interfile Image " +
		         "Reader SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The Image Input Stream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of the classes returned by the
     *                                  originating service provider's
     *                                  getInputTypes method, or is not an
     *                                  Image Input Stream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	// Convert the input source into an Image or Interfile Input Stream
	input = InterfileImageReaderSpi.convertToInput(input);

	super.setInput(input, seekForwardOnly, ignoreMetadata);

	// Remove any decoded Interfile Group
	_interfileGroup = null;
    }

    /**
     * Returns the number of images, not including thumbnails, available from
     * the current input source.
     * 
     * @param allowSearch If true, the true number of images will be returned
     *                    even if a search is required.  If false, the reader
     *                    may return -1 without performing the search.
     *
     * @return The number of images or -1 if allowSearch is false and a search
     *         would be required.
     *
     * @throws IllegalStateException If the input source has not been set.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    public int getNumImages(boolean allowSearch) throws IOException
    {
	int numberOfImages = _getMetadata().getNumberOfImages();

	// Unable to determine the number of images
	if (numberOfImages == -1) {
	    String msg = "Unable to determine the number of images.";
	    throw new IOException(msg);
	}

	return numberOfImages;
    }

    /**
     * Returns the width in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The width of the image in pixels.
     *
     * @throws IOException If an error occurs reading the width information from
     *                     the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getWidth(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image width
	int width = _getMetadata().getImageWidth();

	// Unable to determine the image width
	if (width == -1) {
	    String msg = "Unable to determine the image width.";
	    throw new IOException(msg);
	}

	return width;
    }

    /**
     * Returns the height in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The height of the image in pixels.
     *
     * @throws IOException If an error occurs reading the height information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getHeight(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image height
	int height = _getMetadata().getImageHeight();

	// Unable to determine the image height
	if (height == -1) {
	    String msg = "Unable to determine the image height.";
	    throw new IOException(msg);
	}

	return height;
    }

    /**
     * Returns an Iterator containing possible image types to which the given
     * image may be decoded, in the form of Image Type Specifiers.  At least one
     * legal image type will be returned.
     *
     * @param imageIndex The index of the image to be retrieved.
     *
     * @return An Iterator containing at least one Image Type Specifier
     *         representing suggested image types for decoding the current given
     *         image.
     *
     * @throws IOException If an error occurs reading the format information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public Iterator getImageTypes(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image parameters
	InterfileMetadata metadata = _getMetadata();
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);
	int bitsPerPixel = metadata.getBitsPerPixel();
	String type = metadata.getDataType();

	// Determine the appropriate Image Type Specifier
	ArrayList list = new ArrayList(1);
	if (bitsPerPixel == 8 || bitsPerPixel == 16) {
	    list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
	}
	else if (bitsPerPixel == 32 && type != null) {
	    String lowerType = type.toLowerCase();
	    if (lowerType.indexOf("integer") != -1) {
		list.add( Utilities.getGrayImageType(DataBuffer.TYPE_INT) );
	    }
	    else if (lowerType.indexOf("float") != -1) {
		list.add( Utilities.getGrayImageType(DataBuffer.TYPE_FLOAT) );
	    }
	}

	// Check if the image data type is supported
	if (list.size() == 0) {
	    String msg = "Unsupported Interfile image type:  data type = " +
		         type + "; bits per voxel = " + bitsPerPixel + ".";
	    throw new IOException(msg);
	}

	// Return the Image Type Specifier
	return list.iterator();
    }

    /**
     * Returns an II/O Metadata object representing the metadata associated with
     * the input source as a whole (i.e., not associated with any particular
     * image), or null if the reader does not support reading metadata, is set
     * to ignore metadata, or if no metadata is available.
     *
     * @return An II/O Metadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     */
    public IIOMetadata getStreamMetadata() throws IOException
    {
	return _getMetadata();
    }

    /**
     * Returns an II/O Metadata object containing metadata associated with the
     * given image, or null if the reader does not support reading metadata, is
     * set to ignore metadata, or if no metadata is available.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved. 
     *
     * @return An II/O Metadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
	// Not applicable
	return null;
    }

    /**
     * Reads the image indexed by imageIndex and returns it as a complete
     * Buffered Image.  The supplied Image Read Param is ignored.
     *
     * @param imageIndex The index of the image to be retrieved.
     * @param param An Image Read Param used to control the reading process, or
     *              null.
     *
     * @return The desired portion of the image as a Buffered Image.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public BufferedImage read(int imageIndex, ImageReadParam param)
	throws IOException
    {
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// Update the Listeners
	processImageStarted(imageIndex);

	// Compute the size of the image in bytes
	ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
	int dataType = spec.getSampleModel().getDataType();
	int imageSize = width*height;
	if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }
	if (dataType == DataBuffer.TYPE_INT) { imageSize *= 4; }
	if (dataType == DataBuffer.TYPE_FLOAT) { imageSize *= 4; }

	// Set the pixel data Image Input Stream position to the image beginning
	long offset = imageIndex*imageSize;
	ImageInputStream imageStream = _getPixelDataInputStream(offset);

	// Return the raw image data if required
	if ( Utilities.returnRawBytes(param) ) {
	    BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	    DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	    imageStream.read( ((DataBufferByte)db).getData() );
	    return bufferedImage;
	}

	// Create a Buffered Image for the image data
	Iterator iter = getImageTypes(0);
	BufferedImage bufferedImage = getDestination(null, iter, width, height);
	DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

	// Read the image data into an array
	byte[] srcBuffer = new byte[imageSize];
	imageStream.readFully(srcBuffer);

	// Determine if the data needs to be byte swapped
	boolean isByteSwapped = _getMetadata().isByteSwapped();

	// 8-bit gray image pixel data
	WritableRaster dstRaster = bufferedImage.getRaster();
	if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	    bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
	}

	// 16-bit gray image pixel data (byte swap if necessary)
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_USHORT) {
	    bufferedImage =
		Utilities.getGrayUshortImage(srcBuffer, dstRaster, 16, false,
					     isByteSwapped, 1, 0);
	}

	// General gray image pixel data (byte swap if necessary)
	else {
	    bufferedImage = Utilities.getGrayImage(srcBuffer, dstRaster,
						   isByteSwapped);
	}

	// Update the Listeners
	if ( abortRequested() ) { processReadAborted(); }
	else { processImageComplete(); }

	// Return the requested image
	return bufferedImage;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this Image Reader.  Otherwise, the reader may continue to hold
     * on to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the input stream
	try {
	    if (input instanceof InterfileInputStream) {
		((InterfileInputStream)input).close();
	    }
	    else if (input instanceof ImageInputStream) {
		((ImageInputStream)input).close();
	    }
	}

	catch (Exception e) {}

	input = null;
    }

    /**
     * Gets an II/O Metadata object that represents metadata from the input
     * source.
     *
     * @return II/O Metadata object that represents metadata from the input
     * 
     * @throws IOException If an error occurs during reading.
     *         source.
     */
    protected InterfileMetadata _getMetadata() throws IOException
    {
	return new InterfileMetadata(_getGroup());
    }

    /**
     * Reads the Interfile Group from the input source.  If the Interfile Group
     * has already been decoded, no additional read is performed.
     *
     * @return Interfile Group decoded from the input source.
     * 
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     */
    protected InterfileGroup _getGroup() throws IOException
    {
	// Return the Group if it has already been decoded
	if (_interfileGroup != null) { return _interfileGroup; }

	// No input has been set
	if (input == null) {
	    String msg = "No input has been set.";
	    throw new IllegalStateException(msg);
	}

	// Get the metadata Image Input Stream
	ImageInputStream imageStream;
	if (input instanceof InterfileInputStream) {
	  InterfileInputStream inputStream = (InterfileInputStream)input;
	  imageStream = inputStream.getMetadataInputStream();
	}
	else { imageStream = (ImageInputStream)input; }

	// Skip past the INTERFILE line
	String line = imageStream.readLine();
	while (line.toUpperCase().indexOf("INTERFILE") == -1) {
	    line = imageStream.readLine();
	}

	// Read and return the encoded Interfile Group
	InterfileGroupDecoder decoder = new InterfileGroupDecoder(imageStream);
	_interfileGroup = decoder.getGroup();
	return _interfileGroup;
    }

    /**
     * Checks whether or not the specified image index is valid.
     *
     * @param imageIndex The index of the image to check.
     *
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private void _checkImageIndex(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	if ( imageIndex >= getNumImages(false) ) {
	    String msg = "Image index of " + imageIndex + " >= the total " +
		         "number of images (" + getNumImages(false) + ")";
	    throw new IndexOutOfBoundsException(msg);
	}
    }

    /**
     * Gets the input stream for the pixel data.
     *
     * @param offset Offset from the stream beginning at which to set the input
     *               stream.
     *
     * @return Image Input Stream to read Interfile image pixel data from.
     *
     * @throws IOException If an error occurs while setting the Input Stream.
     * @throws IllegalStateException If the input has not been set.
     */
    private ImageInputStream _getPixelDataInputStream(long offset)
	throws IOException
    {
	// No input has been set
	if (input == null) {
	    String msg = "No input has been set.";
	    throw new IllegalStateException(msg);
	}

	// Get the pixel data Input Stream
	ImageInputStream imageStream;
	if (input instanceof InterfileInputStream) {
	    InterfileInputStream inputStream = (InterfileInputStream)input;
	    imageStream = inputStream.getPixelDataInputStream();
	}
	else { imageStream = (ImageInputStream)input; }

	// Set the Input Stream to the appropriate offset
	offset += _getMetadata().getDataOffset();
	imageStream.seek(offset);

	// Return the Image Input Stream for image pixel data
	return imageStream;
    }
}
