/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.interfile.InterfileGroup;

/**
 * Interfile Metadata for the HRRT Interfile format.
 *
 * @version 2 August 2005
 */
public class HrrtInterfileMetadata extends InterfileMetadata
{
    /**
     * Constructs an HRRT Interfile Metadata from the given Interfile Group.
     *
     * @param group Interfile Group containing the metadata.
     */
    public HrrtInterfileMetadata(InterfileGroup group)
    {
	super(group, HrrtInterfileMetadataFormat.HRRT_METADATA_FORMAT_NAME,
	      HrrtInterfileMetadataFormat.class.getName());
    }

    /**
     * Determines if the image pixel data is byte swapped.
     *
     * @return True if the image pixel data is byte swapped; false otherwise.
     */
    public boolean isByteSwapped()
    {
	// HRRT image data is always byte swapped
	return true;
    }
}
