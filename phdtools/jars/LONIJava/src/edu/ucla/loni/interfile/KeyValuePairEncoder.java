/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile;

/**
 * Class that encodes a Key-Value Pair.
 *
 * @version 30 June 2005
 */
public class KeyValuePairEncoder
{
  /** Constructs a Key-Value Pair Encoder. */
  public KeyValuePairEncoder()
    {
    }

  /**
   * Encodes the specified Key-Value Pair.
   *
   * @param pair Key-Value Pair to encode.
   *
   * @return Encoded Key-Value Pair.
   *
   * @throws NullPointerException If the Pair is null.
   */
  public String encode(KeyValuePair pair)
    {
      // Check the pair
      if (pair == null) {
	throw new NullPointerException("Null Key-Value Pair");
      }

      // Add a "!" if the Pair is required
      StringBuffer buffer = new StringBuffer();
      if ( pair.isRequired() ) { buffer.append("!"); }

      // Add the key and value
      buffer.append(pair.getKey() + " := " + pair.getValue());

      // Add the comment if it exists
      String comment = pair.getComment();
      if (comment != null) { buffer.append(" ; " + comment); }

      // Terminate with a <CR> character and line feed <LF> character
      buffer.append("\r\n");

      // Return the encoded Pair
      return buffer.toString();
    }
}
