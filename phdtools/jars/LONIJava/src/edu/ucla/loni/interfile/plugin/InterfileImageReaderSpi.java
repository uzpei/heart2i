/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.DualStreamImageReaderSpi;
import edu.ucla.loni.imageio.FileNameAssociator;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

/**
 * Dual Stream Image Reader SPI (Service Provider Interface) for the Interfile
 * image format.
 *
 * @version 9 August 2005
 */
public class InterfileImageReaderSpi extends DualStreamImageReaderSpi
{
    /** Name of the vendor who supplied this plug-in. */
    private static String _vendorName = "Laboratory of Neuro Imaging (LONI)";

    /** Version of this plug-in. */
    private static String _version = "1.0";

    /** Names of the formats read by this plug-in. */
    private static String[] _formatNames = {"interfile", "INTERFILE"};

    /** Names of commonly used suffixes for files in the supported formats. */
    private static String[] _fileSuffixes = {"hdr", "HDR"};

    /** MIME types of supported formats. */
    private static String[] _mimeTypes = {"image/hdr"};

    /** Name of the associated Image Reader. */
    private static String _reader = InterfileImageReader.class.getName();

    /** Allowed input type classes for the plugin-in. */
    private static Class[] _inputTypes = {InterfileInputStream.class,
					  ImageInputStream.class, File.class,
					  Vector.class};

    /** Names of the associated Image Writer SPI's. */
    private static String[] _writeSpis =
    {InterfileImageWriterSpi.class.getName()};

    /** Constructs an Interfile Image Reader SPI. */
    public InterfileImageReaderSpi()
    {
	this(_formatNames, _reader, _inputTypes, _writeSpis,
	     InterfileMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	     InterfileMetadataFormat.class.getName());
    }

    /**
     * Constructs an Interfile Image Reader SPI.
     *
     * @param formatNames Names of the formats read by this plug-in.
     * @param reader Name of the associated Image Reader.
     * @param inputTypes Allowed input type classes for the plugin-in.
     * @param writeSpis Names of the associated Image Writer SPI's.
     * @param nativeMetadataFormatName Name of the metadata format.
     * @param metadataFormatClassName Name of the metadata format class.
     */
    protected InterfileImageReaderSpi(String[] formatNames, String reader,
				      Class[] inputTypes, String[] writeSpis,
				      String nativeMetadataFormatName,
				      String metadataFormatClassName)
    {
	super(_vendorName, _version, formatNames, _fileSuffixes, _mimeTypes,
	      reader, inputTypes, writeSpis,

	      // Support one native stream metatdata format
	      true, nativeMetadataFormatName, metadataFormatClassName,
	      null, null,

	      // Do not support native image metatdata formats
	      false, null, null, null, null);
    }

    /**
     * Gets an input stream from the specified source suitable for decoding by
     * the Interfile Image Reader.
     *
     * @param source Source object from which to get an input stream.
     *
     * @return Interfile Input Stream or Image Input Stream if the source is
     *         recognized, or null if the source is unrecognizable.
     */
    public static Object convertToInput(Object src)
    {
	// Determine if the source can be converted to an Interfile Input Stream
	InterfileNameAssociator asstr = new InterfileNameAssociator();
	Object convertSrc = InterfileInputStream.getInputStream(src, asstr);
	if (convertSrc != null) { return convertSrc; }

	// Check for one Image Input Stream in a Vector
	if (src instanceof Vector) {
	    Vector streams = (Vector)src;

	    // Must have one Image Input Stream
	    if (streams.size() == 1) {
		Object stream = streams.get(0);
		if (stream instanceof ImageInputStream) { return stream; }
	    }
	}

	// Check for a File
	if (src instanceof File) {
	    try { return new FileImageInputStream( (File)src ); }
	    catch (Exception e) { return null; }
	}

	// Cannot convert
	return null;
    }

    /**
     * Determines if the supplied source object appears to be in a supported
     * format.  After the determination is made, the source is reset to its
     * original state if that operation is allowed.
     *
     * @param source Source Object to examine.
     *
     * @return True if the supplied source object appears to be in a supported
     *         format; false otherwise.
     *
     * @throws IOException If an I/O error occurs while reading a stream.
     * @throws IllegalArgumentException If the source is null.
     */
    public boolean canDecodeInput(Object source) throws IOException
    {
	ImageInputStream imageStream = null;

	// Check for a null argument
	if (source == null) {
	    String msg = "Cannot have a null source.";
	    throw new IllegalArgumentException(msg);
	}

	// Convert the source to an input stream
	Object stream = convertToInput(source);

	// Can not decode
	if (stream == null) { return false; }

	// Get the metadata Input Stream of an Interfile Input Stream
	if (stream instanceof InterfileInputStream) {
	    InterfileInputStream interfileStream = (InterfileInputStream)stream;
	    imageStream = interfileStream.getMetadataInputStream();
	}
	else { imageStream = (ImageInputStream)stream; }

	// Mark the current position
	imageStream.mark();

	// Read the first 100 bytes
	byte[] headerBytes = new byte[100];
	imageStream.readFully(headerBytes);

	// Reset the position of the stream
	imageStream.reset();

	// Search for "INTERFILE"
	String header = new String(headerBytes);
	header = header.toUpperCase();
	int interfileIndex = header.indexOf("INTERFILE");

	// Not an Interfile stream
	if (interfileIndex == -1) { return false; }

	// Check the remaining substring for a leading ":="
	String suffix = header.substring(interfileIndex + 9).trim();
	return suffix.startsWith(":=");
    }

    /**
     * Returns a brief, human-readable description of this service provider and
     * its associated implementation.
     *
     * @param locale Locale for which the return value should be localized.
     *
     * @return Description of this service provider.
     */
    public String getDescription(Locale locale)
    {
	return "Interfile image reader";
    }

    /**
     * Returns an instance of the Image Reader implementation associated with
     * this service provider.  The returned object will be in an initial state
     * as if its reset method had been called.
     *
     * @param extension A plug-in specific extension object, which may be null.
     *
     * @return An Image Reader instance.
     *
     * @throws IllegalArgumentException If the Image Reader finds the extension
     *                                  object to be unsuitable.
     */
    public ImageReader createReaderInstance(Object extension)
    {
	return new InterfileImageReader(this);
    }

    /**
     * Gets the File Name Associator that generates file name associations
     * between metadata files and image pixel files.
     *
     * @return File Name Associator for this format.
     */
    public FileNameAssociator getFileNameAssociator()
    {
	return new InterfileNameAssociator();
    }
}
