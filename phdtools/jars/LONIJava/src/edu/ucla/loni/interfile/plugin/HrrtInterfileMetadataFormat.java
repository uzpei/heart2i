/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

/**
 * Interfile Metadata Format that describes the tree structure of HRRT (High
 * Resolution Research Tomograph) interfile metadata.
 *
 * @version 2 August 2005
 */
public class HrrtInterfileMetadataFormat extends InterfileMetadataFormat
{
    /** Name of the HRRT interfile metadata format. */
    public static final String HRRT_METADATA_FORMAT_NAME =
	"edu_ucla_loni_interfile_hrrt_1.0";

    /** Single instance of the HRRT Interfile Metadata Format. */
    private static HrrtInterfileMetadataFormat _format =
	new HrrtInterfileMetadataFormat();

    /** Constructs an HRRT Interfile Metadata Format. */
    private HrrtInterfileMetadataFormat()
    {
	super(HRRT_METADATA_FORMAT_NAME);
    }

    /**
     * Gets an instance of the HRRT Interfile Metadata Format.
     *
     * @return The single instance of the HRRT Interfile Metadata Format.
     */
    public static InterfileMetadataFormat getInstance()
    {
	return _format;
    }
}
