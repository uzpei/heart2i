/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * Interfile Metadata class.
 *
 * @version 29 July 2005
 */
public class InterfileMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
    /** Name of the native metadata format. */
    public static final String NATIVE_METADATA_FORMAT_NAME =
	"edu_ucla_loni_interfile_1.0";

    /** Name of the value attribute. */
    public static final String VALUE_NAME = "value";

    /** Name of the comment attribute. */
    public static final String COMMENT_NAME = "comment";

    /** Name of the required attribute. */
    public static final String REQUIRED_NAME = "isRequired";

    /** Name of the extension attribute. */
    public static final String EXTENSION_NAME = "extension";

    /** Single instance of the Interfile Metadata Format. */
    private static InterfileMetadataFormat _format =
	new InterfileMetadataFormat(NATIVE_METADATA_FORMAT_NAME);

   /**
     * Constructs an Interfile Metadata Format with the specified format name.
     *
     * @param formatName Name of the Interfile metadata format.
     */
    protected InterfileMetadataFormat(String formatName)
    {
	// Create the root with 0 or more instances of its children
	super(formatName, 0, Integer.MAX_VALUE);
    }

    /**
     * Gets a list of pre-defined Interfile headings.
     *
     * @return Headings defined in Interfile (without values) that are used to
     *         enhance readability.
     */
    public static String[] getHeadings()
    {
	String[] headings = {"GENERAL DATA", "GENERAL IMAGE DATA",
			     "STATIC STUDY (General)",
			     "Static Study (each frame)",
			     "DYNAMIC STUDY (general)",
			     "Dynamic Study (each frame group)",
			     "GATED STUDY (general)",
			     "Gated Study (each time window)",
			     "SPECT STUDY (general)",
			     "SPECT STUDY (acquired data)",
			     "SPECT STUDY (reconstructed data)", "CURVE DATA"};

	return headings;
    }

    /**
     * Determines whether or not the specified string is an Interfile heading.
     *
     * @param s String to test.
     *
     * @return True if the string is an Interfile heading; false otherwise.
     */
    public static boolean isHeading(String s)
    {
	// Null string
	if (s == null) { return false; }

	// Test each heading
	String[] headings = getHeadings();
	for (int i = 0; i < headings.length; i++) {
	    if ( headings[i].equals(s) ) { return true; }
	}

	// String is not a heading
	return false;
    }

    /**
     * Gets an instance of the Interfile Metadata Format.
     *
     * @return The single instance of the Interfile Metadata Format.
     */
    public static InterfileMetadataFormat getInstance()
    {
	return _format;
    }

    /**
     * Returns true if the element (and the subtree below it) is allowed to
     * appear in a metadata document for an image of the given type, defined by
     * an ImageTypeSpecifier.
     *
     * @param elementName The name of the element being queried.
     * @param imageType An ImageTypeSpecifier indicating the type of the image
     *                  that will be associated with the metadata.
     *
     * @return True if the node is meaningful for images of the given type.
     */
    public boolean canNodeAppear(String elementName,
				 ImageTypeSpecifier imageType)
    {
	// Not implemented
	return true;
    }

  /**
   * Returns a String containing a description of the named element, or null.
   *
   * @param elementName Name of the element.
   * @param locale Locale for which localization will be attempted.
   *
   * @return The element description, or null if it doesn't exist.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   */
  public String getElementDescription(String elementName, Locale locale)
    {
      return _getDescription(elementName);
    }

  /**
   * Returns a String containing a description of the named attribute, or
   * null.
   *
   * @param elementName Name of the element.
   * @param attrName Name of the attribute.
   * @param locale Locale for which localization will be attempted, or null.
   *
   * @return The attribute description, or null if it doesn't exist.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   * @throws IllegalArgumentException If attrName is null, or is not a legal
   *                                  legal attribute name for this element.
   */
  public String getAttributeDescription(String elementName, String attrName,
					Locale locale)
    {
	// Any element is allowed to have these attributes
	if ( attrName.equals(VALUE_NAME) || attrName.equals(COMMENT_NAME) ||
	     attrName.equals(REQUIRED_NAME) || attrName.equals(EXTENSION_NAME) )
	    {
		elementName = "*";
	    }

      // Get the description
      String key = elementName + "/" + attrName;
      return _getDescription(key);
    }

  /**
   * Gets a description for the key.
   *
   * @param key Key to find a description for.
   *
   * @return Description for the key, or null if there isn't one.
   */
  private String _getDescription(String key)
    {
      // Get the description from the resource bundle
      try {
	String name = getResourceBaseName();
	Locale locale = Locale.getDefault();	
	ResourceBundle bundle = ResourceBundle.getBundle(name, locale);

	// Find the description
	return bundle.getString(key);
      }

      // Return null if no resource bundle is available
      catch (MissingResourceException e) { return null; }
    }
}
