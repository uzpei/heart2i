/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.Conversions;
import edu.ucla.loni.interfile.InterfileGroup;
import edu.ucla.loni.interfile.KeyValuePair;
import java.util.Iterator;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between Interfile Groups and the DOM Node
 * representations of them.
 *
 * @version 9 August 2005
 */
public class InterfileMetadataConversions
{
    /** Constructs an Interfile Metadata Conversions. */
    private InterfileMetadataConversions()
    {
    }

    /**
     * Converts the tree to an Interfile Group.
     *
     * @param root DOM Node representing an Interfile Group.
     * @param rootName Allowed name of the root node.
     *
     * @return Interfile Group constructed from the DOM Node.
     *
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     * @throws IllegalArgumentException If the root Node is null.
     */
    public static InterfileGroup toInterfileGroup(Node root, String rootName)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check that the root Node name matches a required name
	if ( !root.getNodeName().equals(rootName) ) {
	    String msg = "Root node must be named \"" + rootName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// Attempt to construct an Interfile Group from the Node
	try {
	    InterfileGroup group = new InterfileGroup();

	    // Convert the child Nodes to Group elements
	    NodeList childNodes = root.getChildNodes();
	    for (int i = 0; i < childNodes.getLength(); i++) {
		Node childNode = childNodes.item(i);

		// Convert childless Nodes to Key-Value Pairs
		if (childNode.getFirstChild() == null) {
		    group.addKeyValuePair( _toKeyValuePair(childNode) );
		}

		// Convert Nodes with children to Interfile Groups
		else { group.addGroup( _toInterfileGroup(childNode) ); }
	    }

	    return group;
	}

	// Unable to construct an Interfile Group
	catch (Exception e) {
	    String msg = "Cannot convert the DOM Node into an Interfile Group.";
	    throw new IIOInvalidTreeException(msg, e, root);
	}
    }

    /**
     * Converts the Interfile Group to a tree.
     *
     * @param group Interfile Group to convert.
     * @param rootName Name of the tree root node.
     *
     * @return DOM Node representing the Interfile Group.
     */
    public static Node toTree(InterfileGroup group, String rootName)
    {
	// Create the root Node
	IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

	if (group != null) {

	    // Add all the Group elements as child Nodes
	    Iterator iter = group.getElements();
	    while ( iter.hasNext() ) {
		Object element = iter.next();

		// Group element is a Key-Value Pair
		if (element instanceof KeyValuePair) {
		    Node pairNode = _toTree( (KeyValuePair)element );
		    rootNode.appendChild(pairNode);
		}

		// Group element is a sub-Group
		else if (element instanceof InterfileGroup) {
		    Node subgroupNode = _toTree( (InterfileGroup)element );
		    if (subgroupNode != null) {
			rootNode.appendChild(subgroupNode);
		    }
		}
	    }
	}

	// Return the root Node
	return rootNode;
    }

    /**
     * Converts the specified key of a Key-Value Pair into its XML
     * representation.
     *
     * @param key Key of a Key-Value Pair.
     *
     * @return XML representation of the specified key.
     */
    public static String getKeyAsXml(String key)
    {
	boolean isHeading = InterfileMetadataFormat.isHeading(key);

	// If the key has an extension, parse it out
	String extension = _getExtension(key);
	if ( !extension.equals("") ) {
	    key = key.substring(0, key.length() - extension.length()).trim();
	}

	// Make the key a legal XML key
	return _toXmlName(key, isHeading);
    }

    /**
     * Converts the tree to an Interfile Group.
     *
     * @param root DOM Node representing an Interfile Group.
     *
     * @return Interfile Group constructed from the DOM Node.
     *
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    private static InterfileGroup _toInterfileGroup(Node root)
	throws IIOInvalidTreeException
    {
	// Attempt to construct an Interfile Group from the Node
	try {
	    InterfileGroup group = new InterfileGroup();

	    // Convert the root to a Key-Value Pair
	    group.addKeyValuePair( _toKeyValuePair(root) );

	    // Convert the child Nodes to Group elements
	    NodeList childNodes = root.getChildNodes();
	    for (int i = 0; i < childNodes.getLength(); i++) {
		Node childNode = childNodes.item(i);

		// Convert childless Nodes to Key-Value Pairs
		if (childNode.getFirstChild() == null) {
		    group.addKeyValuePair( _toKeyValuePair(childNode) );
		}

		// Convert Nodes with children to Interfile Groups
		else { group.addGroup( _toInterfileGroup(childNode) ); }
	    }

	    return group;
	}

	// Unable to construct the Interfile Group
	catch (Exception e) {
	    String msg = "Cannot convert the DOM Node into an Interfile Group.";
	    throw new IIOInvalidTreeException(msg, e, root);
	}
    }

    /**
     * Converts the tree to a Key-Value Pair.
     *
     * @param root DOM Node representing a Key-Value Pair.
     *
     * @return Key-Value Pair constructed from the DOM Node.
     *
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    private static KeyValuePair _toKeyValuePair(Node root)
	throws IIOInvalidTreeException
    {
	// Attempt to construct a Key-Value Pair from the Node
	try {
	    NamedNodeMap atts = root.getAttributes();

	    // Convert the key from its XML representation
	    String key = _toName( root.getNodeName() );

	    // Add the extension to the key if it exists
	    String name = InterfileMetadataFormat.EXTENSION_NAME;
	    Node extensionItem = atts.getNamedItem(name);
	    if (extensionItem != null) {
		key += extensionItem.getNodeValue();
	    }

	    // Get the value if it exists
	    String value = "";
	    name = InterfileMetadataFormat.VALUE_NAME;
	    Node valueItem = atts.getNamedItem(name);
	    if (valueItem != null) { value = valueItem.getNodeValue(); }

	    // Get the required attribute if it exists
	    boolean isRequired = false;
	    name = InterfileMetadataFormat.REQUIRED_NAME;
	    Node requiredItem = atts.getNamedItem(name);
	    if (requiredItem != null) {
		isRequired=requiredItem.getNodeValue().equalsIgnoreCase("true");
	    }

	    // Get the comment if it exists
	    String comment = null;
	    name = InterfileMetadataFormat.COMMENT_NAME;
	    Node commentItem = atts.getNamedItem(name);
	    if (commentItem != null) {
		comment = commentItem.getNodeValue();
	    }

	    // Return a Key-Value Pair
	    return new KeyValuePair(key, value, comment, isRequired);
	}

	// Unable to construct the Key-Value Pair
	catch (Exception e) {
	    String msg = "Cannot convert the DOM Node into a Key-Value Pair.";
	    throw new IIOInvalidTreeException(msg, e, root);
	}
    }

    /**
     * Converts the Interfile Group to a tree.
     *
     * @param group Interfile Group to convert.
     *
     * @return DOM Node representing the Interfile Group, or null if the Group
     *         is empty.
     */
    private static Node _toTree(InterfileGroup group)
    {
	// Get the Signature Key-Value Pair
	KeyValuePair sigPair = _getSignaturePair(group);
	if (sigPair == null) { return null; }

	// Create the Group Node
	Node groupNode = _toTree(sigPair);

	// Add all the Group elements as child Nodes
	Iterator iter = group.getElements();
	while ( iter.hasNext() ) {
	    Object element = iter.next();

	    // Group element is a Key-Value Pair
	    if (element instanceof KeyValuePair && element != sigPair) {
		Node pairNode = _toTree( (KeyValuePair)element );
		groupNode.appendChild(pairNode);
	    }

	    // Group element is a sub-Group
	    else if (element instanceof InterfileGroup) {
		Node subgroupNode = _toTree( (InterfileGroup)element );
		if (subgroupNode != null) {groupNode.appendChild(subgroupNode);}
	    }
	}

	// Return the Group Node
	return groupNode;
    }

    /**
     * Gets the "signature" Key-Value Pair of the specified Group.  This is the
     * Key-Value Pair used to label the the Group.
     *
     * @param group Interfile Group.
     *
     * @return Signature Key-Value Pair of the specified Group, or null if no
     *         such Pair exists.
     */
    private static KeyValuePair _getSignaturePair(InterfileGroup group)
    {
	Iterator iter = group.getElements();

	// Search for a signature Key-Value Pair
	while ( iter.hasNext() ) {
	    Object element = iter.next();

	    // Group element is a Key-Value Pair
	    if (element instanceof KeyValuePair) {return (KeyValuePair)element;}

	    // Group element is a sub-Group
	    else if (element instanceof InterfileGroup) {
		InterfileGroup subGroup = (InterfileGroup)element;

		// Try to get a signature Key-Value Pair from the sub-Group
		KeyValuePair pair = _getSignaturePair(subGroup);
		if (pair != null) { return pair; }
	    }
	}

	// No signature Key-Value Pair found
	return null;
    }

    /**
     * Converts the Key-Value Pair to a tree.
     *
     * @param pair Key-Value Pair to convert.
     *
     * @return DOM Node representing the Key-Value Pair.
     */
    private static Node _toTree(KeyValuePair pair)
    {
	String key = pair.getKey();

	// Create the Node using a legal XML key
	String xmlName = getKeyAsXml(key);
	IIOMetadataNode pairNode = new IIOMetadataNode(xmlName);

	// Add the extension attribute if it exists
	String extension = _getExtension(key);
	if ( !extension.equals("") ) {
	    String name = InterfileMetadataFormat.EXTENSION_NAME;
	    pairNode.setAttribute(name, extension);
	}

	// Add the value attribute if its not a heading that is missing a value
	String name = InterfileMetadataFormat.VALUE_NAME;
	String value = pair.getValue();
	boolean isHeading = InterfileMetadataFormat.isHeading(key);
	if ( !(isHeading && value.equals("")) ) {
	    pairNode.setAttribute(name, value);
	}

	// Add the required attribute if the Pair is required
	name = InterfileMetadataFormat.REQUIRED_NAME;
	if ( pair.isRequired() ) {
	    pairNode.setAttribute(name, "" + pair.isRequired());
	}

	// Add the comment attribute if it exists
	String comment = pair.getComment();
	if (comment != null) {
	    name = InterfileMetadataFormat.COMMENT_NAME;
	    pairNode.setAttribute(name, comment);
	}

	// Return the Key-Value Pair Node
	return pairNode;
    }

    /**
     * Gets the extension of the specified name.  A extension is a numerical
     * suffix or a string surrounded by brackets at the end of the name.
     *
     * @param name Name to get the extension of.
     *
     * @return Extension of the name.
     */
    private static String _getExtension(String name)
    {
	// Empty name has no extension
	if ( name.equals("") ) { return ""; }

	// Start with the last character
	int index = name.length()-1;
	char lastChar = name.charAt(index);

	// Numerical suffix
	if ( Character.isDigit(lastChar) ) {

	    // Determine the index of the suffix beginning
	    while ( Character.isDigit(name.charAt(index)) ) { index--; }
	    index++;
	}

	// Right parenthesis
	else if (lastChar == ')') {

	    // Find the left parenthesis
	    index = name.lastIndexOf('(');

	    // No left parenthesis
	    if (index == -1) { return ""; }
	}

	// Right bracket
	else if (lastChar == ']') {

	    // Find the left bracket
	    index = name.lastIndexOf('[');

	    // No left bracket
	    if (index == -1) { return ""; }
	}

	// No extension
	else { return ""; }

	// Also include any spaces before the suffix
	index--;
	while ( Character.isWhitespace(name.charAt(index)) ) { index--; }
	index++;

	// Return the extension
	return name.substring(index);
    }

    /**
     * Converts the specified legal XML name to its original representation.
     *
     * @param xmlName Legal XML name to convert.
     *
     * @return Original name.
     */
    private static String _toName(String xmlName)
    {
	String name = xmlName;

	// Convert -C####C- to its original character
	int index;
	while ( (index = name.indexOf("-C")) != -1) {

	    // Get the hex string between the C's
	    String hex = name.substring(index+2, index+6);

	    // Convert the hex string
	    byte[] b = Conversions.toByteArray(hex);
	    String origC = new String(b);

	    // Replace -C####C- with the original character
	    name = name.substring(0, index) + origC + name.substring(index+8);
	}

	// Right parenthesis
	name = name.replaceAll("-R-", ")");

	// Left parenthesis
	name = name.replaceAll("-L-", "(");

	// Forward slash
	name = name.replaceAll("-B-", "/");

	// Underscore -> space
	name = name.replace('_', ' ');

	// Remove any leading space
	name = name.trim();

	// Return the original name
	return name;
    }

    /**
     * Converts the specified name into a legal XML name.
     *
     * @param name Name to convert.
     * @param isHeading True if the name belongs to a heading; false otherwise.
     *
     * @return Legal XML name.
     */
    private static String _toXmlName(String name, boolean isHeading)
    {
	String xmlName = name;

	// Convert all upper case characters to lower case (except headings)
	if (!isHeading) { xmlName = xmlName.toLowerCase(); }

	// Add a space if the first character is illegal in XML
	if ( xmlName.equals("") || !_canBeInitialXmlChar(xmlName.charAt(0)) ) {
	    xmlName = " " + xmlName;
	}

	// Loop through each character and encode illegal characters
	StringBuffer encodedName = new StringBuffer();
	for (int i = 0; i < xmlName.length(); i++) {
	    char c = xmlName.charAt(i);

	    // Underscore -> space
	    if (c == '_') { c = ' '; }

	    // Space -> underscore
	    else if ( Character.isWhitespace(c) ) { c = '_'; }

	    // Legal XML character
	    if ( _canBeNonInitialXmlChar(c) ) { encodedName.append(c); }

	    // Forward slash
	    else if (c == '/') { encodedName.append("-B-"); }

	    // Left parenthesis
	    else if (c == '(') { encodedName.append("-L-"); }

	    // Right parenthesis
	    else if (c == ')') { encodedName.append("-R-"); }

	    // General case:  -C####C-
	    else {
		byte[] b = new byte[2];
		b[0] = (byte)((c >>> 8) & 0xFF);
		b[1] = (byte)((c >>> 0) & 0xFF);

		// Create a 4-digit hexadecimal number
		String hex = Conversions.toHexString(b);
		while (hex.length() < 4) { hex = "0" + hex; }

		// Add the encoded hexadecimal number
		encodedName.append("-C" + hex + "C-");
	    }
	}

	// Return the encoded name
	return encodedName.toString();
    }

    /**
     * Determines if the specified character can be a non-initial character of
     * a legal XML name.
     *
     * @param c Character to test.
     *
     * @return True if the specified character can be a non-initial character of
     *         a legal XML name; false otherwise.
     */
    private static boolean _canBeNonInitialXmlChar(char c)
    {
	// Initial XML character
	if ( _canBeInitialXmlChar(c) ) { return true; }

	// Numbers are legal
	if ( Character.isDigit(c) ) { return true; }

	// Hyphen is legal
	if (c == '-') { return true; }

	// Period is legal
	if (c == '.') { return true; }

	// Not legal
	return false;
    }

    /**
     * Determines whether or not the specified character can be the initial
     * character of a legal XML name.
     *
     * @param c Character to test.
     *
     * @return True if the character can be the initial character of a legal XML
     *         name; false otherwise.
     */
    private static boolean _canBeInitialXmlChar(char c)
    {
	// Letters are legal
	if ( Character.isLetter(c) ) { return true; }

	// Colon is legal
	if (c == ':') { return true; }

	// Underscore is legal
	if (c == '_') { return true; }

	// Not legal
	return false;
    }
}
