/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.interfile.InterfileGroup;
import java.util.Iterator;
import java.util.Vector;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for metadata of the Interfile image format.
 *
 * @version 15 January 2009
 */
public class InterfileMetadata extends AppletFriendlyIIOMetadata
{
    /** Interfile Group containing the original metadata. */
    private InterfileGroup _originalGroup;

    /** Interfile Group containing the current metadata. */
    private InterfileGroup _currentGroup;

    /**
     * Constructs an Interfile Metadata from the given Interfile Group.
     *
     * @param group Interfile Group containing the metadata.
     */
    public InterfileMetadata(InterfileGroup group)
    {
	this(group, InterfileMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	     InterfileMetadataFormat.class.getName());
    }

    /**
     * Constructs an Interfile Metadata from the given Interfile Group.
     *
     * @param group Interfile Group containing the metadata.
     * @param formatName Name of the Interfile format.
     * @param formatClassName Name of the Interfile format class.
     */
    protected InterfileMetadata(InterfileGroup group, String formatName,
				String formatClassName)
    {
	super(true, formatName, formatClassName,
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalGroup = group;
	_currentGroup = group;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return InterfileMetadataConversions.toTree(_currentGroup,
						       formatName);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this II/O Metadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current Interfile Group, convert the root
	Node r;
	if (_currentGroup == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Recreate the root tree
	    r = new IIOMetadataNode(nativeMetadataFormatName);

	    // Get the current metadata tree Nodes
	    Node currentRoot = getAsTree(formatName);
	    NodeList currentNodeList = currentRoot.getChildNodes();

	    // Get the new metadata tree Nodes
	    NodeList nodeList = root.getChildNodes();

	    // Add Nodes to the new root tree
	    for (int i = 0; i < currentNodeList.getLength(); i++) {
		Node currentNode = currentNodeList.item(i);

		// Determine all current and newly matching Nodes
		Node[] cNodes = _getMatchingNodes(currentNode, currentNodeList);
		Node[] nNodes = _getMatchingNodes(currentNode, nodeList);

		// If one current and new Node, replace the current
		if (cNodes.length == 1 && nNodes.length == 1) {
		    cNodes = new Node[0];
		}

		// Add the current Nodes to the new root tree
		for (int j = 0; j < cNodes.length; j++) {
		    r.appendChild(cNodes[i]);
		}

		// Add the new Nodes to the new root tree
		for (int j = 0; j < nNodes.length; j++) {
		    r.appendChild(nNodes[i]);
		}
	    }
	}

	// Set a new current Interfile Group
	_currentGroup =
	    InterfileMetadataConversions.toInterfileGroup(r,root.getNodeName());
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current Interfile Group to the original 
	_currentGroup = _originalGroup;
    }

    /**
     * Gets the number of images.
     *
     * @return Number of images, or -1 if it cannot be determined.
     */
    public int getNumberOfImages()
    {
	try {

	    // Look for the matrix size
	    String key = "matrix size [3]";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    // Look for the total number of images
	    key = "total number of images";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    // Look for the number of images in the window
	    key = "number of images/window";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    key = "number of images in window";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    key = "number of images/energy window";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    // Unknown
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the width of each image.
     *
     * @return Width of each image, or -1 if it cannot be determined.
     */
    public int getImageWidth()
    {
	try {

	    // Look for the matrix size
	    String key = "matrix size [1]";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    key = "matrix size (x)";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    // Unknown
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the height of each image.
     *
     * @return Height of each image, or -1 if it cannot be determined.
     */
    public int getImageHeight()
    {
	try {

	    // Look for the matrix size
	    String key = "matrix size [2]";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    key = "matrix size (y)";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Integer.parseInt(values[0]); }

	    // Unknown
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the number of bits per pixel used by each image.
     *
     * @return Number of bits per pixel used by each image, or -1 if it cannot
     *         be determined.
     */
    public int getBitsPerPixel()
    {
	try {

	    // Look for the number of bytes per pixel
	    String key = "number of bytes per pixel";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return 8*Integer.parseInt(values[0]); }

	    // Unknown type
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Determines if the image pixel data is byte swapped.
     *
     * @return True if the image pixel data is byte swapped; false otherwise.
     */
    public boolean isByteSwapped()
    {
	try {

	    // Look for the image data byte order
	    String key = "imagedata byte order";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) {

		// Byte swapped if little endian
		return values[0].equalsIgnoreCase("LITTLEENDIAN");
	    }

	    // Unknown
	    return false;
	}

	// Unable to make the determination
	catch (Exception e) { return false; }
    }

    /**
     * Gets the data type of each image.
     *
     * @return Data type of each image, or null if it cannot be determined.
     */
    public String getDataType()
    {
	try {

	    // Look for the number format
	    String key = "number format";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return values[0]; }

	    // Unknown format
	    return null;
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the offset (in bytes) to the image data.
     *
     * @return Offset (in bytes) to the image data, or zero if it cannot be
     *         determined.
     */
    public long getDataOffset()
    {
	try {

	    // Look for the data offset in bytes
	    String key = "data offset in bytes";
	    String[] values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) { return Long.parseLong(values[0]); }

	    // Look for the data starting block
	    key = "data starting block";
	    values = _currentGroup.getValues(key, true, true);
	    if (values.length > 0) {
		return 2048*Long.parseLong(values[0]);
	    }

	    // Unknown offset
	    return 0;
	}

	// Unable to make the determination
	catch (Exception e) { return 0; }
    }

    /**
     * Returns an II/O Metadata Node representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return II/O Metadata Node representing the chroma information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// Only support non-inverted grayscale
	return Utilities.getGrayScaleChromaNode(false);
    }

    /**
     * Returns an II/O Metadata Node representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return II/O Metadata Node representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	try {

	    int bitsPerPixel = getBitsPerPixel();

	    // No information available
	    if (bitsPerPixel == -1) { return null; }

	    // Integral gray scale data
	    if (bitsPerPixel == 8 || bitsPerPixel == 16) {
		return Utilities.getGrayScaleDataNode(bitsPerPixel);
	    }

	    // Real gray scale data
	    else { return Utilities.getGrayScaleDataNode(bitsPerPixel, false); }
	}
      
	// No information available
	catch (Exception e) { return null; }
    }

    /**
     * Returns an II/O Metadata Node representing the dimension format
     * information of the standard javax_imageio_1.0 metadata format, or null
     * if no such information is available.
     *
     * @return II/O Metadata Node representing the dimension information, or
     *         null if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Look for the scaling factors
	    String key = "scaling factor (mm/pixel) [1]";
	    String[] xValues = _currentGroup.getValues(key, true, true);
	    if (xValues.length == 0) { return null; }

	    key = "scaling factor (mm/pixel) [2]";
	    String[] yValues = _currentGroup.getValues(key, true, true);
	    if (yValues.length == 0) { return null; }

	    // Return the dimension node
	    float pixelWidth = Float.parseFloat(xValues[0]);
	    float pixelHeight = Float.parseFloat(yValues[0]);
	    return Utilities.getDimensionNode(pixelWidth, pixelHeight);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets all Nodes in the specified Node list that match the specified Node.
     *
     * @param nodeToMatch Node to find matching Nodes for.
     *
     * @return All Nodes in the specified Node list that match the Node.
     */
    private Node[] _getMatchingNodes(Node nodeToMatch, NodeList nodeList)
    {
	Vector matchingNodes = new Vector();

	// Get the Node name
	String matchNodeName = nodeToMatch.getNodeName();

	// Get the value of the extension attribute
	String extName = InterfileMetadataFormat.EXTENSION_NAME;
	String matchExtValue = null;
	NamedNodeMap matchMap = nodeToMatch.getAttributes();
	if (matchMap != null) {
	    Node matchExtNode = matchMap.getNamedItem(extName);
	    if (matchExtNode != null) {
		matchExtValue = matchExtNode.getNodeValue();
	    }
	}

	// Find all Matching Nodes
	for (int i = 0; i < nodeList.getLength(); i++) {
	    Node node = nodeList.item(i);

	    // Match the Node name
	    if (matchNodeName.equals( node.getNodeName() )) {
		NamedNodeMap map = node.getAttributes();
		if (map != null) {
		    Node extNode = map.getNamedItem(extName);

		    // Match a missing extension value
		    if (matchExtValue == null) {
			if (extNode == null) { matchingNodes.add(node); }
		    }

		    // Match an extension value
		    else if (extNode != null) {
			String extValue = extNode.getNodeValue();
			if ( matchExtValue.equals(extNode.getNodeValue()) ) {
			    matchingNodes.add(node);
			}
		    }
		}
	    }
	}

	// Return all the matching Nodes
	return (Node[])matchingNodes.toArray(new Node[0]);
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	// Image dimensions
	int imageWidth = getImageWidth();
	int imageHeight = getImageHeight();
	int numberOfImages = getNumberOfImages();

	// Pixel width
	float pixelWidth = 0;
	String key = "scaling factor (mm/pixel) [1]";
	String[] xValues = _currentGroup.getValues(key, true, true);
	if (xValues.length > 0) { pixelWidth = Float.parseFloat(xValues[0]); }

	// Pixel height
	float pixelHeight = 0;
	key = "scaling factor (mm/pixel) [2]";
	String[] yValues = _currentGroup.getValues(key, true, true);
	if (yValues.length > 0) { pixelHeight = Float.parseFloat(yValues[0]); }

	// Slice thickness
	float sliceThickness = 0;
	key = "scaling factor (mm/pixel) [3]";
	String[] zValues = _currentGroup.getValues(key, true, true);
	if (zValues.length > 0) {
	    sliceThickness = Float.parseFloat(zValues[0]);
	}

	// Bits per pixel
	int bitsPerPixel = getBitsPerPixel();

	// Data type
	String dataType = "Undetermined";
	if (bitsPerPixel == 8) { dataType = BasicMetadataFormat.BYTE_TYPE; }
	if (bitsPerPixel == 16) {
	    dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE;
	}

	String type = getDataType();
	if (bitsPerPixel == 32 && type != null) {
	    String lowerType = type.toLowerCase();
	    if (lowerType.indexOf("integer") != -1) {
		dataType = BasicMetadataFormat.SIGNED_INT_TYPE;
	    }
	    else if (lowerType.indexOf("float") != -1) {
		dataType = BasicMetadataFormat.FLOAT_TYPE;
	    }
	}

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;

	// Return the basic tree based upon available information
	if (pixelWidth == 0 && pixelHeight == 0 && sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, dataType,
						  bitsPerPixel, colorType);
	}
	if (sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, pixelWidth,
						  pixelHeight, dataType,
						  bitsPerPixel, colorType);
	}
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }
}
