/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.DualImageOutputStream;
import edu.ucla.loni.interfile.InterfileGroup;
import edu.ucla.loni.interfile.InterfileGroupEncoder;
import java.io.IOException;
import java.util.Vector;
import javax.imageio.ImageWriteParam;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageOutputStream;

/**
 * Interfile Image Writer for the HRRT Interfile format.
 *
 * @version 25 July 2006
 */
public class HrrtInterfileImageWriter extends InterfileImageWriter
{
    /**
     * Constructs an HRRT Interfile Image Writer.
     *
     * @param originatingProvider The Image Writer Spi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  HRRT Interfile Image Writer Spi.
     */
    public HrrtInterfileImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an HRRT Interfile Image Writer Spi
	if ( !(originatingProvider instanceof HrrtInterfileImageWriterSpi) ) {
	    String msg = "Originating provider must an HRRT Interfile Image " +
		"Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the destination to the given ImageOutputStream or other Object.  The
     * destination is assumed to be ready to accept data, and will not be closed
     * at the end of each write. If output is null, any currently set output
     * will be removed.
     *
     * @param output A Dual Image Output Stream or a Vector of two Image Output
     *               Streams to use for future writing.
     *
     * @throws IllegalArgumentException If output is not an instance of one of
     *                                  the classes returned by the originating
     *                                  service provider's getOutputTypes
     *                                  method.
     */
    public void setOutput(Object output)
    {
	// Convert a Vector into a Dual Image Output Stream
	if (output instanceof Vector) {
	    output = new DualImageOutputStream( (Vector)output );
	}

	super.setOutput(output);
    }

    /**
     * Returns an II/O Metadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An Image Write Param that will be used to encode the image,
     *              or null.
     *
     * @return An II/O Metadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank metadata object
	return new HrrtInterfileMetadata(null);
    }

    /**
     * Returns an II/O Metadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An II/O Metadata object representing stream metadata, used
     *               to initialize the state of the returned object.
     * @param param An Image Write Param that will be used to encode the image,
     *              or null.
     *
     * @return An II/O Metadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize HRRT Interfile Metadata
	if (inData instanceof HrrtInterfileMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Writes the Interfile Group to the Output Stream.
     *
     * @param group Interfile Group to write to the Output Stream.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    protected void _writeGroup(InterfileGroup group,
			       ImageOutputStream outputStream)
	throws IOException
    {
	// Write the INTERFILE line
	outputStream.writeBytes("!INTERFILE\r\n");

	// Write the Interfile Group
	InterfileGroupEncoder encoder = new InterfileGroupEncoder();
	encoder.encode(group, outputStream);

	// Write the END OF INTERFILE line (with ctrl-z)
	outputStream.writeBytes("!END OF INTERFILE\r\n");
	outputStream.writeBytes("\u001A");
    }
}
