/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.DualImageOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;
import javax.imageio.ImageWriter;

/**
 * Interfile Image Writer SPI for the HRRT Interfile image format.
 *
 * @version 9 August 2005
 */
public class HrrtInterfileImageWriterSpi extends InterfileImageWriterSpi
{
    /** Names of the formats written by this plug-in. */
    private static String[] _formatNames = {"HRRT interfile", "HRRT INTERFILE"};

    /** Name of the associated Image Writer. */
    private static String _writer = HrrtInterfileImageWriter.class.getName();

    /** Allowed output type classes for the plugin-in. */
    private static Class[] _outputTypes = {DualImageOutputStream.class,
					   Vector.class};

    /** Names of the associated Image Reader SPI's. */
    private static String[] _readSpis =
    {HrrtInterfileImageReaderSpi.class.getName()};

    /** Constructs an HRRT Interfile Image Writer Spi. */
    public HrrtInterfileImageWriterSpi()
    {
	super(_formatNames, _writer, _outputTypes, _readSpis,
	      HrrtInterfileMetadataFormat.HRRT_METADATA_FORMAT_NAME,
	      HrrtInterfileMetadataFormat.class.getName());
    }

    /**
     * Returns a brief, human-readable description of this service provider and
     * its associated implementation.
     *
     * @param locale Locale for which the return value should be localized.
     *
     * @return Description of this service provider.
     */
    public String getDescription(Locale locale)
    {
	return "HRRT Interfile image writer";
    }

    /**
     * Returns an instance of the Image Writer implementation associated with
     * this service provider.  The returned object will initially be in an
     * initial state as if its reset method had been called.
     *
     * @param extension A plug-in specific extension object, which may be null.
     *
     * @return An Image Writer instance.
     *
     * @throws IOException If the attempt to instantiate the writer fails.
     * @throws IllegalArgumentException If the Image Writer finds the extension
     *                                  object to be unsuitable.
     */
    public ImageWriter createWriterInstance(Object extension) throws IOException
    {
	return new HrrtInterfileImageWriter(this);
    }
}
