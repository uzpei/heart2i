/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.DualImageOutputStream;
import edu.ucla.loni.interfile.InterfileGroup;
import edu.ucla.loni.interfile.InterfileGroupEncoder;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Vector;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Image Writer for parsing and encoding Interfile images.
 *
 * @version 9 August 2005
 */
public class InterfileImageWriter extends ImageWriter
{
    /** Interfile Group of the image sequence currently being written. */
    private InterfileGroup _group;

    /** Number of images in the sequence currently being written. */
    private int _numberOfImagesWritten;

    /**
     * Constructs an Interfile Image Writer.
     *
     * @param originatingProvider The Image Writer Spi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  Interfile Image Writer Spi.
     */
    public InterfileImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an Interfile Image Writer Spi
	if ( !(originatingProvider instanceof InterfileImageWriterSpi) ) {
	    String msg = "Originating provider must an Interfile Image " +
		         "Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the destination to the given ImageOutputStream or other Object.  The
     * destination is assumed to be ready to accept data, and will not be closed
     * at the end of each write. If output is null, any currently set output
     * will be removed.
     *
     * @param output A Dual Image Output Stream or a Vector of two Image Output
     *               Streams to use for future writing.
     *
     * @throws IllegalArgumentException If output is not an instance of one of
     *                                  the classes returned by the originating
     *                                  service provider's getOutputTypes
     *                                  method.
     */
    public void setOutput(Object output)
    {
	// Vector of Image Output Streams
	if (output instanceof Vector) {
	    Vector streams = (Vector)output;

	    // Reset the output if one Image Output Stream
	    if (streams.size() == 1) { output = streams.firstElement(); }

	    // Otherwise convert to a Dual Image Output Stream
	    else { output = new DualImageOutputStream(streams); }
	}

	super.setOutput(output);
    }

    /**
     * Returns an II/O Metadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An Image Write Param that will be used to encode the image,
     *              or null.
     *
     * @return An II/O Metadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank metadata object
	return new InterfileMetadata(null);
    }

    /**
     * Returns an II/O Metadata object containing default values for encoding an
     * image of the given type.
     *
     * @param imageType An Image Type Specifier indicating the format of the
     *                  image to be written later.
     * @param param An Image Write Param that will be used to encode the image,
     *              or null.
     *
     * @return An II/O Metadata object.
     */
    public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					       ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns an II/O Metadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An II/O Metadata object representing stream metadata, used
     *               to initialize the state of the returned object.
     * @param param An Image Write Param that will be used to encode the image,
     *              or null.
     *
     * @return An II/O Metadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize Interfile Metadata
	if (inData instanceof InterfileMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns an II/O Metadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An II/O Metadata object representing image metadata, used
     *               to initialize the state of the returned object.
     * @param imageType An Image Type Specifier indicating the layout and color
     *                  information of the image with which the metadata will be
     *                  associated.
     * @param param An Image Write Param that will be used to encode the image,
     *              or null.
     *
     * @return An II/O Metadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If either of inData or imageType is
     *                                  null.
     */
    public IIOMetadata convertImageMetadata(IIOMetadata inData,
					    ImageTypeSpecifier imageType,
					    ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns true if the methods that take an II/O Image parameter are capable
     * of dealing with a Raster (as opposed to Rendered Image) source image.
     *
     * @return True if Raster sources are supported.
     */
    public boolean canWriteRasters()
    {
	return true;
    }

    /**
     * Appends a complete image stream containing a single image and associated
     * stream and image metadata and thumbnails to the output.
     *
     * @param streamMetadata An II/O Metadata object representing stream
     *                       metadata, or null to use default values.
     * @param image An II/O Image object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An Image Write Param, or null to use a default
     *              Image Write Param.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, if the stream
     *                                  metadata cannot be converted, or if the
     *                                  image type is unrecognized.
     * @throws IllegalStateException If the output has not been set.
     */
    public void write(IIOMetadata streamMetadata, IIOImage image,
		      ImageWriteParam param) throws IOException
    {
	// Write a sequence with 1 image
	prepareWriteSequence(streamMetadata);
	writeToSequence(image, param);
	endWriteSequence();
    }

    /**
     * Returns true if the writer is able to append an image to an image
     * stream that already contains header information and possibly prior
     * images.
     *
     * @return True If images may be appended sequentially.
     */
    public boolean canWriteSequence()
    {
	return true;
    }

    /**
     * Prepares a stream to accept a series of subsequent writeToSequence calls,
     * using the provided stream metadata object.  The metadata will be written
     * to the stream if it should precede the image data.
     *
     * @param streamMetadata A stream metadata object.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the stream metadata is null or cannot
     *                                  be converted.
     * @throws IllegalStateException If the output has not been set.
     */
    public void prepareWriteSequence(IIOMetadata streamMetadata)
	throws IOException
    {
	// Convert the metadata to Interfile Metadata
	IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the stream metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct a stream Interfile Group from the metadata
	try {

	    // Get the Interfile Group
	    String root = metadata.getNativeMetadataFormatName();
	    Node tree = metadata.getAsTree(root);
	    _group = InterfileMetadataConversions.toInterfileGroup(tree, root);
	}
      
	catch (Exception e) {
	    String msg = "Unable to construct an Interfile Group from the " +
		         "metadata.";
	    throw new IllegalArgumentException(msg);
	}

	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Determine the Image Output Stream to write metadata to
	ImageOutputStream metadataStream;
	if (output instanceof DualImageOutputStream) {
	    DualImageOutputStream dualStream = (DualImageOutputStream)output;
	    metadataStream = dualStream.getMetadataOutputStream();
	}
	else { metadataStream = (ImageOutputStream)output; }

	// Write the Interfile Group to the Image Output Stream
	_writeGroup(_group, metadataStream);

	// Advance the pixel data Image Output Stream if required
	long offset = ((InterfileMetadata)metadata).getDataOffset();
	_getPixelDataOutputStream().seek(offset);

	_numberOfImagesWritten = 0;
    }

    /**
     * Appends a single image and possibly associated metadata and thumbnails,
     * to the output.  The supplied Image Read Param is ignored.
     *
     * @param image An II/O Image object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An Image Write Param or null to use a default Image Write
     *              Param.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null or if the image
     *                                  type is not recognized.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void writeToSequence(IIOImage image, ImageWriteParam param)
	throws IOException
    { 
	// Check for the Interfile Group
	if (_group == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Check for a null image
	if (image == null) {
	    String msg = "Cannot write a null image.";
	    throw new IllegalArgumentException(msg);
	}

	// Get a Raster from the image
	Raster raster = image.getRaster();
	if (raster == null) {
	    RenderedImage renderedImage = image.getRenderedImage();

	    // If the Rendered Image is a Buffered Image, get it directly
	    if (renderedImage instanceof BufferedImage) {
		raster = ((BufferedImage)renderedImage).getRaster();
	    }

	    // Otherwise get a copy of the Raster from the Rendered Image
	    raster = renderedImage.getData();
	}

	// Update the Listeners
	processImageStarted(_numberOfImagesWritten);

	// Write byte data
	ImageOutputStream outputStream = _getPixelDataOutputStream();
	DataBuffer dataBuffer = raster.getDataBuffer();
	if (dataBuffer instanceof DataBufferByte) {
	    outputStream.write( ((DataBufferByte)dataBuffer).getData() );
	}

	// Write short data
	else if (dataBuffer instanceof DataBufferShort) {
	    short[] data = ((DataBufferShort)dataBuffer).getData();
	    outputStream.writeShorts(data, 0, data.length);
	}

	// Write unsigned short data
	else if (dataBuffer instanceof DataBufferUShort) {
	    short[] data = ((DataBufferUShort)dataBuffer).getData();
	    outputStream.writeShorts(data, 0, data.length);
	}

	// Write integer data
	else if (dataBuffer instanceof DataBufferInt) {
	    int[] data = ((DataBufferInt)dataBuffer).getData();
	    outputStream.writeInts(data, 0, data.length);
	}

	// Write float data
	else if (dataBuffer instanceof DataBufferFloat) {
	    float[] data = ((DataBufferFloat)dataBuffer).getData();
	    outputStream.writeFloats(data, 0, data.length);
	}

	// Write double data
	else if (dataBuffer instanceof DataBufferDouble) {
	    double[] data = ((DataBufferDouble)dataBuffer).getData();
	    outputStream.writeDoubles(data, 0, data.length);
	}

	// Unrecognized type
	else {
	    String msg = "Unable to write the II/O Image.";
	    throw new IllegalArgumentException(msg);
	}

	// Update the Listeners
	_numberOfImagesWritten++;
	if ( abortRequested() ) { processWriteAborted(); }
	else { processImageComplete(); }
    }

    /**
     * Completes the writing of a sequence of images begun with
     * prepareWriteSequence.  Any stream metadata that should come at the end of
     * the sequence of images is written out, and any header information at the
     * beginning of the sequence is patched up if necessary.
     */
    public void endWriteSequence()
    {
	// Reset the Interfile Group
	_group = null;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this Image Writer.  Otherwise, the writer may continue to hold
     * on to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the output stream
	try {

	    // Two Output Streams
	    if (output instanceof DualImageOutputStream) {
		((DualImageOutputStream)output).close();
	    }

	    // One Output Stream
	    else if (output instanceof ImageOutputStream) {
		((ImageOutputStream)output).close();
	    }
	}
	catch (Exception e) {}

	output = null;
    }

    /**
     * Writes the Interfile Group to the Output Stream.
     *
     * @param group Interfile Group to write to the Output Stream.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    protected void _writeGroup(InterfileGroup group,
			       ImageOutputStream outputStream)
	throws IOException
    {
	// Write the INTERFILE line
	outputStream.writeBytes("!INTERFILE := \r\n");

	// Write the Interfile Group
	InterfileGroupEncoder encoder = new InterfileGroupEncoder();
	encoder.encode(group, outputStream);

	// Write the END OF INTERFILE line (with ctrl-z)
	outputStream.writeBytes("!END OF INTERFILE := \r\n");
	outputStream.writeBytes("\u001A");
    }

    /**
     * Gets the Output Stream for the pixel data.
     *
     * @return Image Output Stream to write Interfile image pixel data to.
     *
     * @throws IOException If an error occurs while setting the Input Stream.
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getPixelDataOutputStream() throws IOException
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Two Output Streams
	if (output instanceof DualImageOutputStream) {
	    return ((DualImageOutputStream)output).getPixelDataOutputStream();
	}

	// One Output Stream
	return (ImageOutputStream)output;
    }
}
