/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;
import javax.imageio.IIOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Class that decodes an encoded Interfile Group.
 *
 * @version 17 August 2005
 */
public class InterfileGroupDecoder
{
  /** Interfile Group decoded. */
  private InterfileGroup _group;

  /** Key-Value Pair mapped to the number of times its key occurs. */
  private TreeMap _pairOccurrences;

  /**
   * Constructs an Interfile Group Decoder.
   *
   * @param inputStream Image Input Stream to read from.
   *
   * @throws IIOException If an I/O error occurs.
   */
  public InterfileGroupDecoder(ImageInputStream inputStream)
    throws IIOException
    {
      // Read and decode lines from the stream
      Vector pairs = new Vector();
      try {
	String line = inputStream.readLine();

	// Read lines until hit ctrl-Z, end of file, or "END OF INTERFILE"
	char ctrlZ = '\u001A';
	String endMarker = "END OF INTERFILE";
	while ( line != null && line.toUpperCase().indexOf(endMarker) == -1 &&
		line.indexOf(ctrlZ) == -1 )
	  {
	      // Add key-value pairs, even if they are commented out
	      if ( line.indexOf(":=") != -1 ) {
		  KeyValuePairDecoder decoder = new KeyValuePairDecoder(line);
		  pairs.add( decoder.getPair() );
	      }

	      // Get the next line
	      line = inputStream.readLine();
	  }
      }

      catch (Exception e) {
	String msg = "Unable to decode key-value lines.";
	throw new IIOException(msg, e);
      }

      // Create an Interfile Group from the Key-Value Pairs
      InterfileGroupDecoder decoder = new InterfileGroupDecoder(pairs);
      _group = decoder.getGroup();
    }

  /**
   * Constructs an Interfile Group Decoder.
   *
   * @param pairs Key-Value Pairs for the Group.
   */
  private InterfileGroupDecoder(Vector pairs)
    {
      // Create an empty Interfile Group
      _group = new InterfileGroup();

      // Map each Key-Value Pair to the number of times its key occurs
      _pairOccurrences = _getOccurrences(pairs);

      // Add the Key-Value Pairs to the Group
      pairs = (Vector)pairs.clone();
      while ( !pairs.isEmpty() ) { _addPairsToGroup(pairs); }
    }

  /**
   * Gets the decoded Interfile Group.
   *
   * @return Decoded Interfile Group.
   */
  public InterfileGroup getGroup()
    {
      return _group;
    }

  /**
   * Gets a sorted map of Key-Value Pairs mapped to the number of times they
   * occur in the specified list.
   *
   * @param pairs Key-Value Pairs.
   *
   * @return Sorted map of Key-Value Pairs mapped to the number of times they
   *         occur.
   */
  private TreeMap _getOccurrences(Vector pairs)
    {
      TreeMap pairOccurrences = new TreeMap();

      // Map each Key-Value Pair to the number of times its key occurs
      Iterator iter = pairs.iterator();
      while ( iter.hasNext() ) {
	KeyValuePair pair = (KeyValuePair)iter.next();

	// Get the number of occurrences
	Integer occur = (Integer)pairOccurrences.get(pair);

	// Increase the number of occurrences by 1
	if (occur == null) { occur = new Integer(1); }
	else { occur = new Integer(occur.intValue() + 1); }

	// Set the number of occurrences
	pairOccurrences.put(pair, occur);
      }

      // Return the occurrences
      return pairOccurrences;
    }

  /**
   * Adds Key-Value Pairs to the Group.  If a Pair is added to the Group, it is
   * removed from the Pair list.  This method first adds unique Pairs and then
   * forms subgroups.  This method should be called until there are no more
   * Key-Value Pairs in the list.
   *
   * @param pairs Key-Value Pairs.
   */
  private void _addPairsToGroup(Vector pairs)
    {
      // Start adding unique Key-Value Pairs to the Group
      int occur = _addUniquePairsToGroup(pairs);
      if (occur == -1) { return; }

      // Break the remaining Key-Value Pairs into occur-1 Groups
      Vector addedPairs = new Vector();
      for (int i = 0; i < occur-1; i++) { 
	addedPairs.addAll( _addSubGroup(pairs) );
      }

      // Map each added Key-Value Pair to the number of times its key occurs
      TreeMap addedOccurrences = _getOccurrences(addedPairs);

      // Determine how many times more these Pairs will occur
      TreeMap remainOccurrences = _getSubtractedMap(addedOccurrences,
						    _pairOccurrences);

      // Determine the Pairs that belong to the last Group
      Vector lastPairs = new Vector();
      Iterator iter = pairs.iterator();
      while (iter.hasNext() && remainOccurrences.size() > 0) {
	KeyValuePair pair = (KeyValuePair)iter.next();

	// Add the Pair
	lastPairs.add(pair);
	iter.remove();

	// Update the occurrences of the Pairs in the previous Groups
	Integer occurrence = (Integer)remainOccurrences.get(pair);
	if (occurrence != null) {
	  occur = occurrence.intValue() - 1;

	  // No more occurrences
	  if (occur == 0) { remainOccurrences.remove(pair); }
	  else { remainOccurrences.put(pair, new Integer(occur)); }
	}
      }

      // Add the last Group
      InterfileGroupDecoder decoder = new InterfileGroupDecoder(lastPairs);
      _group.addGroup( decoder.getGroup() );
    }

  /**
   * Adds Key-Value Pairs to the Group until a Pair that occurs more than once
   * is reached.  If a Pair is added to the Group, it is removed from the Pair
   * list.
   *
   * @param pairs Key-Value Pairs.
   *
   * @return Number of occurrences of the Pair that occurs more than once, or
   *         -1 if all Pairs were added to the Group.
   */
  private int _addUniquePairsToGroup(Vector pairs)
    {
      // Add unique Key-Value Pairs to the Group
      Iterator iter = pairs.iterator();
      while ( iter.hasNext() ) {
	KeyValuePair pair = (KeyValuePair)iter.next();

	// Get the number of times the Pair occurs
	int occur = ((Integer)_pairOccurrences.get(pair)).intValue();

	// If the Pair is unique, add it to the Group and remove from the list
	if (occur == 1) {
	  _group.addKeyValuePair(pair);
	  iter.remove();
	}

	// Pair is not unique, return the number of occurrences
	else { return occur; }
      }

      // All Pairs have been added to the Group
      return -1;
    }

  /**
   * Creates a Group for the Key-Value Pairs in the specified list up to the
   * reoccurrence of the first Key-Value Pair in the list.  These Pairs are
   * removed from the Pair list.
   *
   * @param pairs Key-Value Pairs.
   *
   * @return Key-Value Pairs that were added to the new Group.
   */
  private Vector _addSubGroup(Vector pairs)
    {
      Vector addedPairs = new Vector();
      KeyValuePair firstPair = (KeyValuePair)pairs.firstElement();

      // Put all the Key-Value Pairs up to the reoccurrence into a list
      boolean stopAdding = false;
      Iterator iter = pairs.iterator();
      while (iter.hasNext() && !stopAdding) {
	KeyValuePair pair = (KeyValuePair)iter.next();

	// Found the reoccurrence of the first Pair
	if (firstPair.equals(pair) && firstPair != pair) { stopAdding = true; }

	// Otherwise move the Key-Value Pair to the new list
	else {
	  addedPairs.add(pair);
	  iter.remove();
	}
      }

      // Create and add the new Group
      InterfileGroupDecoder decoder = new InterfileGroupDecoder(addedPairs);
      _group.addGroup( decoder.getGroup() );

      // Return the Key-Value Pairs that were added
      return addedPairs;
    }

  /**
   * Subtracts the number of occurrences in the second map from the number of
   * occurrences in the first map.
   *
   * @param map1 First Map.
   * @param map2 Second Map.
   *
   * @return Map of subtracted occurrences.
   */
  private TreeMap _getSubtractedMap(TreeMap map1, TreeMap map2)
    {
      TreeMap subtractMap = new TreeMap();

      // Subtract 2 from 1
      Iterator iter = map1.keySet().iterator();
      while ( iter.hasNext() ) {
	KeyValuePair pair = (KeyValuePair)iter.next();

	// Get the number of occurrences
	int occur1 = ((Integer)map1.get(pair)).intValue();
	int occur2 = ((Integer)map2.get(pair)).intValue();

	// Set the subtracted occurrences
	if (occur2-occur1 > 0) {
	  subtractMap.put(pair, new Integer(occur2-occur1));
	}
      }

      // Return the subtracted occurrences
      return subtractMap;
    }
}
