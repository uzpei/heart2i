/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile;

/**
 * Class that decodes an encoded Key-Value Pair.
 *
 * @version 29 June 2005
 */
public class KeyValuePairDecoder
{
  /** Key-Value Pair decoded. */
  private KeyValuePair _pair;

  /**
   * Constructs a Key-Value Pair Decoder.
   *
   * @param line Line containing an encoded Key-Value Pair.
   *
   * @throws IllegalArgumentException If the line is not an encoded Key-Value
   *                                  Pair.
   * @throws NullPointerException If the line is null.
   */
  public KeyValuePairDecoder(String line)
    {
      // Check the line
      if (line == null) {
	throw new NullPointerException("Null Key-Value Pair line");
      }

      // Determine the position of the key-value operator
      int operatorIndex = line.indexOf(":=");
      if (operatorIndex < 0) {
	String msg = "No key-value operator in the line:  " + line;
	throw new IllegalArgumentException(msg);
      }

      // Get the key (remove leading and trailing spaces) and the value
      String key = line.substring(0, operatorIndex);
      key = key.trim();
      String value = line.substring(operatorIndex+2);

      // Determine requiredness
      boolean isRequired = false;
      int bangIndex = key.indexOf("!");
      if (bangIndex > -1) {
	isRequired = true;

	// Remove the "!" in front of the key
	key = key.substring(1);
      }

      // Determine if there is a comment (remove leading and trailing spaces)
      String comment = null;
      int commentIndex = value.indexOf(";");
      if (commentIndex > -1) {

	// Parse the comment and value
	comment = value.substring(commentIndex+1).trim();
	value = value.substring(0, commentIndex);
      }

      // Remove leading and trailing spaces
      value = value.trim();

      _pair = new KeyValuePair(key, value, comment, isRequired);
    }

  /**
   * Gets the decoded Key-Value Pair.
   *
   * @return Decoded Key-Value Pair.
   */
  public KeyValuePair getPair()
    {
      return _pair;
    }
}
