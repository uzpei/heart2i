/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import java.io.IOException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;

/**
 * Interfile Image Reader for the HRRT Interfile format.
 *
 * @version 2 August 2005
 */
public class HrrtInterfileImageReader extends InterfileImageReader
{
    /**
     * Constructs an HRRT Interfile Image Reader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  HRRT Interfile Image Reader Spi.
     */
    public HrrtInterfileImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an HRRT Interfile Image Reader Spi
	if ( !(originatingProvider instanceof HrrtInterfileImageReaderSpi) ) {
	    String msg = "Originating provider must be an HRRT Interfile " +
		         "Image Reader SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The Image Input Stream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of the classes returned by the
     *                                  originating service provider's
     *                                  getInputTypes method, or is not an
     *                                  Image Input Stream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	// Convert the input source into an Interfile Input Stream
	HrrtInterfileNameAssociator asstr = new HrrtInterfileNameAssociator();
	input = InterfileInputStream.getInputStream(input, asstr);

	super.setInput(input, seekForwardOnly, ignoreMetadata);

	// Remove any decoded Interfile Group
	_interfileGroup = null;
    }

    /**
     * Gets an II/O Metadata object that represents metadata from the input
     * source.
     *
     * @return II/O Metadata object that represents metadata from the input
     * 
     * @throws IOException If an error occurs during reading.
     *         source.
     */
    protected InterfileMetadata _getMetadata() throws IOException
    {
	return new HrrtInterfileMetadata(_getGroup());
    }
}
