/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import edu.ucla.loni.imageio.FileNameAssociator;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

/**
 * Interfile Image Reader SPI for the HRRT Interfile image format.
 *
 * @version 9 August 2005
 */
public class HrrtInterfileImageReaderSpi extends InterfileImageReaderSpi
{
    /** Names of the formats read by this plug-in. */
    private static String[] _formatNames = {"HRRT interfile", "HRRT INTERFILE"};

    /** Name of the associated Image Reader. */
    private static String _reader = HrrtInterfileImageReader.class.getName();

    /** Allowed input type classes for the plugin-in. */
    private static Class[] _inputTypes = {InterfileInputStream.class,
					  File.class, Vector.class};

    /** Names of the associated Image Writer SPI's. */
    private static String[] _writeSpis =
    {HrrtInterfileImageWriterSpi.class.getName()};

    /** Constructs an HRRT Interfile Image Reader SPI. */
    public HrrtInterfileImageReaderSpi()
    {
	super(_formatNames, _reader, _inputTypes, _writeSpis,
	      HrrtInterfileMetadataFormat.HRRT_METADATA_FORMAT_NAME,
	      HrrtInterfileMetadataFormat.class.getName());
    }

    /**
     * Determines if the supplied source object appears to be in a supported
     * format.  After the determination is made, the source is reset to its
     * original state if that operation is allowed.
     *
     * @param source Source Object to examine.
     *
     * @return True if the supplied source object appears to be in a supported
     *         format; false otherwise.
     *
     * @throws IOException If an I/O error occurs while reading a stream.
     * @throws IllegalArgumentException If the source is null.
     */
    public boolean canDecodeInput(Object source) throws IOException
    {
	// Check for a null argument
	if (source == null) {
	    String msg = "Cannot have a null source.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct an Interfile Input Stream from the source
	HrrtInterfileNameAssociator asstr = new HrrtInterfileNameAssociator();
	InterfileInputStream inputStream =
	    InterfileInputStream.getInputStream(source, asstr);
	if (inputStream == null) { return false; }

	// Mark the current position of the metadata Image Input Stream
	ImageInputStream imageStream = inputStream.getMetadataInputStream();
	imageStream.mark();

	// Read the first 100 bytes
	byte[] headerBytes = new byte[100];
	imageStream.readFully(headerBytes);

	// Reset the position of the stream
	imageStream.reset();

	// Search for "INTERFILE"
	String header = new String(headerBytes);
	header = header.toUpperCase();
	int interfileIndex = header.indexOf("INTERFILE");

	// Not an Interfile stream
	if (interfileIndex == -1) { return false; }

	// Check the remaining substring for a leading ":="
	String suffix = header.substring(interfileIndex + 9).trim();
	return !suffix.startsWith(":=");
    }

    /**
     * Returns a brief, human-readable description of this service provider and
     * its associated implementation.
     *
     * @param locale Locale for which the return value should be localized.
     *
     * @return Description of this service provider.
     */
    public String getDescription(Locale locale)
    {
	return "HRRT Interfile image reader";
    }

    /**
     * Returns an instance of the Image Reader implementation associated with
     * this service provider.  The returned object will be in an initial state
     * as if its reset method had been called.
     *
     * @param extension A plug-in specific extension object, which may be null.
     *
     * @return An Image Reader instance.
     *
     * @throws IllegalArgumentException If the Image Reader finds the extension
     *                                  object to be unsuitable.
     */
    public ImageReader createReaderInstance(Object extension)
    {
	return new HrrtInterfileImageReader(this);
    }

    /**
     * Gets the File Name Associator that generates file name associations
     * between metadata files and image pixel files.
     *
     * @return File Name Associator for this format.
     */
    public FileNameAssociator getFileNameAssociator()
    {
	return new HrrtInterfileNameAssociator();
    }
}
