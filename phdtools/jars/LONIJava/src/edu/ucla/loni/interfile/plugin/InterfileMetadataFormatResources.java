/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.interfile.plugin;

import java.util.ListResourceBundle;

/**
 * List Resource Bundle that provides descriptions for Interfile elements.
 *
 * @version 2 August 2005
 */
public class InterfileMetadataFormatResources extends ListResourceBundle
{
    /** Name of the Interfile metadata format. */
    private String _formatName;

    /** Description of the Interfile metadata format. */
    private String _formatDesc;

    /** Constructs an Interfile Metadata Format Resources. */
    public InterfileMetadataFormatResources()
    {
	this(InterfileMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	     "Interfile stream metadata.");
    }

    /**
     * Constructs an Interfile Metadata Format Resources.
     *
     * @param formatName Name of the Interfile metadata format.
     * @param formatDesc Description of the Interfile metadata format.
     */
    protected InterfileMetadataFormatResources(String formatName,
					       String formatDesc)
    {
	_formatName = formatName;
	_formatDesc = formatDesc;
    }

  /**
   * Gets the contents of the Resource Bundle.
   *
   * @return Object array of the contents, where each item of the array is a
   *         pair of Objects.  The first element of each pair is the key, which
   *         must be a String, and the second element is the value associated
   *         with that key.
   */
  public Object[][] getContents()
    {
	Object[][] contents = {

	// Node name, followed by description
	{_formatName, _formatDesc},
	{_toXml("imaging modality"),
	 "Only nucmed is defined for the purpose of this document."},
	{_toXml("originating system"), "Eg.GAMMA-11, MDS, ADAC, etc."},
	{_toXml("version of keys"), "Future versions shall increment."},
	{_toXml("date of keys"), "Date of version 3.3 in date format."},
	{_toXml("conversion program"), "Name of program used."},
	{_toXml("program author"), "Your chance of fame and fortune."},
	{_toXml("program version"), "To keep track of conversion programs."},
	{_toXml("program date"), "Date of program."},
	{_toXml("GENERAL DATA"), "Required but can be treated as comment."},
	{_toXml("original institution"), "Name of hospital etc."},
	{_toXml("contact person"), "Another chance of fame (and fortune?)."},
	{_toXml("data description"), "Whatever you want."},
	{_toXml("data starting block"),
	 "The value is the offset in blocks of 2048 bytes in either the \n" +
	 "administrative or the data file depending on the key value for \n" +
	 "name of data file."},
	{_toXml("data offset in bytes"),
	 "The value is the offset in bytes in either the \n" +
	 "administrative or the data file depending on the key value for \n" +
	 "name of data file."},
	{_toXml("name of data file"),
	 "<NULL> if no image data exists.  " +
	 "Key is a name of the file where the data are present, either when\n" +
	 "in a separate binary data file, or when in a combined \n" +
	 "administrative/binary data file."},
	{_toXml("patient name"), "Last name, first name (recommended)."},
	{_toXml("patient ID"), "As used in your hospital."},
	{_toXml("patient dob"), "Date of birth."},
	{_toXml("patient sex"), "Default is Unknown!"},
	{_toXml("study ID"), "As local conditions dictate."},
	{_toXml("exam type"), "Description of procedure as above."},
	{_toXml("data compression"),
	 "Name of algorithm if present- e.g. JPEG, etc."},
	{_toXml("data encode"), "Name of method of encoding if present- e.g. " +
	 "uuencode etc."},
	{_toXml("GENERAL IMAGE DATA"), "Required but treated as comment."},
	{_toXml("type of data"),
	 "Static|Dynamic|Gated|Tomographic|Curve|ROI|Other."},
	{_toXml("total number of images "),
	 "How many images are there altogether in total in the associated \n" +
	 "data file (for all windows etc.). This overrides any other way of\n" +
	 "calculating the total number of images."},
	{_toXml("study date"),
	 "Date of the first image included in the data file."},
	{_toXml("study time"), "Time for the start of first image specified."},
	{_toXml("imagedata byte order"), "BIGENDIAN|LITTLEENDIAN - " +
	 "BIGENDIAN is the default if unspecified."},
	{_toXml("number of energy windows"),"Defaulted to one if unspecified."},
	{_toXml("energy window"), "ASCII text- for example \"Tc99m\"."},
	{_toXml("energy window lower level"),
	 "Value of lower energy level in keV for the corresponding window."},
	{_toXml("energy window upper level"),
	 "Value of upper energy level in keV for the corresponding window."},
	{_toXml("flood corrected"), "Corrected if unspecified."},
	{_toXml("decay corrected"), "Not corrected if unspecified."},
	{_toXml("STATIC STUDY (General)"),
	 "Label to indicate that this is the static definition."},
	{_toXml("number of images/energy window"),
	 "Number of images in THIS energy window."},
	{_toXml("Static Study (each frame)"),
	 "Included at the beginning of the definition of " +
	 "each new static frame."},
	{_toXml("image number"), "Starts from 1 and increments though all \n" +
	 "windows to its maximum value which equals the \n" +
	 "total number of images in the file."},
	{_toXml("matrix size [1]"),
	 "Matrix size ([1] = x = number of columns,\n" +
	 "[2] = y = number of rows)."},
	{_toXml("number format"), "signed integer|unsigned integer " +
	 "|long float|short float|bit|ASCII."},
	{_toXml("number of bytes per pixel"),
	 "E.g. 1|2|4.. [this key ignored for bit data]."},
	{_toXml("scaling factor (mm/pixel) [1]"), "Size of pixel " +
	 "([1] = x = across, [2] = y = down)."},
	{_toXml("image duration (sec)"),
	 "E.g. 120.0  i.e. normally a float, for each image."},
	{_toXml("image start time"), "Time for each image."},
	{_toXml("label"), "E.g. Anterior."},
	{_toXml("maximum pixel count"),"For scaling purposes, for each image."},
	{_toXml("total counts"),
	 "Either an integer or a float, for each image."},
	{_toXml("DYNAMIC STUDY (general)"),
	 "Label to indicate that this is a dynamic study."},
	{_toXml("number of frame groups"), "Defaults to 1."},
	{_toXml("Dynamic Study (each frame group)"),
	 "Repeated for each group of frames as " +
	 "indication of the start of the definition of the new group."},
	{_toXml("frame group number"), "Numbering starts from 1."},
	{_toXml("number of images this frame group"),
	 "For each frame group (for each energy window)."},
	{_toXml("pause between images (sec)"), "E.g. 0.0, default is 0.0."},
	{_toXml("pause between frame groups (sec)"),
	 "E.g. 5.0 default 0.0, time between last " +
	 "frame group (or start of study) and this frame group."},
	{_toXml("maximum pixel count in group"),
	 "E.g. 1234 (for scaling purposes) " +
	 "maximum pixel for all frames in this group and this window."},
	{_toXml("GATED STUDY (general)"), "Flag to indicate a gated study."},
	{_toXml("study duration (elapsed) sec"),
	 "E.g. 300, total elapsed time for whole study."},
	{_toXml("number of cardiac cycles (observed)"),
	 "Total number of cycles if known, for this energy window."},
	{_toXml("number of time windows"),
	 "Defaults to 1 if unspecified- number of different " +
	 "sets of time intervals."},
	{_toXml("Gated Study (each time window)"), "Gated study."},
	{_toXml("time window number"), "Starting from 1."},
	{_toXml("number of images in time window"), "E.g. 24."},
	{_toXml("framing method"),
	 "Forward|Backward|Mixed|Other - Default is forward."},
	{_toXml("time window lower limit (sec)"),
	 "Float normally expected, for THIS time window."},
	{_toXml("time window upper limit (sec)"), "Float normally expected."},
	{_toXml("% R-R cycles acquired this window"), "If known."},
	{_toXml("number of cardiac cycles (acquired)"), "E.g. 356, if known."},
	{_toXml("study duration (acquired) sec"),
	 "Total acquisition time duration for " +
	 "this window only (if it can be computed!!) as \n" +
	 "opposed to total acquisition time (when different)."},
	{_toXml("maximum pixel count"),
	 "For scaling purposes for all images in this " +
	 "time window (and energy window) only."},
	{_toXml("R-R histogram"), "Flag to indicate that one exists."},
	{_toXml("SPECT STUDY (general)"),
	 "Flag to indicate tomographic data with no effect as such."},
	{_toXml("number of detector heads"), "Default=1 if unspecified."},
	{_toXml("number of images/energy window"),
	 "Total number of images (for all heads) for THIS energy window."},
	{_toXml("process status"), "Reconstructed Acquired|Reconstructed."},
	{_toXml("number of projections"),
	 "For example- 64.  Note this is the actual number of images per \n" +
	 "head per energy window if the data are acquired, but NOT if the\n" +
	 "data are reconstructed where the number of images is specified\n" +
	 "separately as number of slices."},
	{_toXml("extent of rotation"), "E.g. 180, 360."},
	{_toXml("time per projection (sec)"), "Important for Acquired data."},
	{_toXml("study duration (sec)"),
	 "E.g. 1280.0, for acquired data should be equal " +
	 "to the product of number of projections and \n" +
	 "time per projection, but could be different."},
	{_toXml("patient orientation"), "head_in|feet_in|other."},
	{_toXml("patient rotation"), "prone|supine|other."},
	{_toXml("SPECT STUDY (acquired data)"), "Spect Study."},
	{_toXml("direction of rotation"),
	 "CW = clockwise, CCW = counter clockwise."},
	{_toXml("start angle"),
	 "0 is top-dead-centre, in degrees in orientation as specified above."},
	{_toXml("first projection angle in data set"),
	 "In degrees expressed with respect to anterior-angles in direction\n" +
	 "as specified CW or CCW."},
	{_toXml("acquisition mode"), "stepped|continuous."},
	{_toXml("Centre_of_rotation"),
	 "Corrected|Single_value|For_every_angle - default is \"Corrected\".\n"+
	 "\"Corrected\" corresponds to a null centre of rotation \n" +
	 "correction, as previous required by Interfile, that \n" +
	 "is, no centre of rotation information is to be \n" +
	 "passed. The key \"Single value\" indicates the \n" +
	 "conventional definition of the centre of rotation \n" +
	 "offset to be a single value specified for all angles, \n" +
	 "specified for each head given a multiple head acquisition. \n" +
	 "The key \"For_every_angle\" indicates that the centre  \n" +
	 "of rotation offset will be  specified for each head \n" +
	 "and every angle. This is not currently implemented in \n" +
	 "this Interfile definition, but will be introduced in V4. \n" +
	 "The mathematical centre of rotation is assumed to be \n" +
	 "in the exact middle of the projection for example at \n" +
	 "x= 32.5 y=32.5 for a 64x64 image where the count \n" +
	 "starts from 1. Note that the choice of coordinates \n" +
	 "does not matter, the only constraint being the \n" +
	 "assumption that all projections have a length which \n" +
	 "is an even number of pixels. The centre of rotation \n" +
	 "is specified as the offset from that position to the \n" +
	 "perpendicular dropped from a point on the axis of \n" +
	 "rotation onto the head."},
	{_toXml("X_offset"),
	 "X offset for all angles in mm. \n" +
	 "x_offset is the x offset between the perpendicular \n" +
	 "dropped from the centre of rotation and the dead \n" +
	 "centre of the matrix. \n" +
	 "The positive direction for the offset is considered \n" +
	 "to be that of the increasing projection index, e.g. \n" +
	 "for a projection with pixels of size 6mm, which \n" +
	 "should be centred at 32.5, an offset of +6mm \n" +
	 "indicates that the centre of rotation is at 33.5 \n" +
	 "Note that since offset is specified in mm, the pixel \n" +
	 "size must be known."},
	{_toXml("Y_offset"),
	 "Y offset for all angles in mm. \n" +
	 "y_offset is the y offset between that perpendicular \n" +
	 "dropped from the centre of rotation from that point \n" +
	 "on the axis of rotation where the y_offset is \n" +
	 "considered to be zero, and the centre of the camera's \n" +
	 "field of view. Thus y_offset is the RELATIVE shift of \n" +
	 "the y-axis with respect to some arbitrary position, \n" +
	 "normally that from the centre of the filed of view of \n" +
	 "the first at the top dead centre position. Thus for \n" +
	 "a single head, this value would normally be expected \n" +
	 "to be equal to zero."},
	{_toXml("Radius"),
	 "Radial distance to centre of rotation in mm, for this head."},
	{_toXml("orbit"), "Circular Circular|non-circular."},
	{_toXml("preprocessed"), "Preprocessing method."},
	{_toXml("SPECT STUDY (reconstructed data)"), "Spect study."},
	{_toXml("method of reconstruction"), "Method of reconstruction."},
	{_toXml("number of slices"),
	 "Number of images in this set for this head and this energy window."},
	{_toXml("number of reference frame"),
	 "If unspecified the frame number originally used for defining slice " +
	 "positions.  0=default."},
	{_toXml("slice orientation"),
	 "Transverse|Coronal|Sagittal|Other. Default is transverse if\n" +
	 "unspecified."},
	{_toXml("slice thickness (pixels)"), "If unspecified 1=default."},
	{_toXml("centre-centre slice separation (pixels)"),
	 "E.g.1,2,3,4...as distinct from slice thickness."},
	{_toXml("center-center slice separation (pixels)"),
	 "E.g.1,2,3,4...as distinct from slice thickness."},
	{_toXml("filter name"), "E.g. Hann, Hamming, Butterworth."},
	{_toXml("filter parameters"), "Nyquist freq etc."},
	{_toXml("z-axis filter"), "Method [1,2,1] etc."},
	{_toXml("ttenuation correction coefficient/cm"),
	 "Default 0 means not done if unspecified."},
	{_toXml("method of attenuation correction"),
	 "Method of attenuation correction."},
	{_toXml("scatter corrected "), "Scatter corrected."},
	{_toXml("method of scatter correction"),
	 "Method of scatter correction."},
	{_toXml("oblique reconstruction"), "Oblique reconstruction."},
	{_toXml("oblique orientation"),
	 "Free text [Note ACR-NEMA convention preferred]."},
	{_toXml("CURVE DATA"),
	 "Label to indicate that this is the curve definition \n" +
	 "curves should always be kept in separate data files \n" +
	 "and not together with the administrative data."},
	{_toXml("Curve_dimensions"),
	 "How many dimensions- ONLY 2 is permitted in V3.3. \n" +
	 "Even if a single vector of values is required \n" +
	 "both matrix size[1] and [2] must be defined \n" +
	 "although one of them should take the value 1. \n" +
	 "A set of x,y values is 2 dimensional with normally \n" +
	 "matrix size[1] or matrix size[2] equal to 2. \n" +
	 "and the other matrix size specifying \n" +
	 "the number of PAIRS of values present (see Fig 1). \n" +
	 "Matrix sizes greater than 2 for BOTH dimensions are not \n" +
	 "recommended."},
	{_toXml("Type_of_curve"),
	 "What kind of curve is it, for example time activity curve \n" +
	 "ROI indicates that this is a list of vectors corresponding\n" +
	 "to an ROI."},
	{_toXml("Label[1]"),
	 "A text label for the corresponding axis e.g. \"counts\"."},
	{_toXml("Units[1]"),
	 "Units of measurement for the corresponding axis e.g. " +
	 "\"units[1]:=counts/sec\"."},
	{_toXml("Min[1]"),
	 "Minimum of set of values as indicated in units as defined,\n" +
	 "optional."},
	{_toXml("Max[1]"), "Maximum value as above, optional."},

	// Node name + "/" + attribute name, followed by description
	{"*/" + InterfileMetadataFormat.VALUE_NAME, "Value."},
	{"*/" + InterfileMetadataFormat.COMMENT_NAME, "Comment."},
	{"*/" + InterfileMetadataFormat.REQUIRED_NAME,
	 "True if the key-value is required; false otherwise."},
	{"*/" + InterfileMetadataFormat.EXTENSION_NAME,
	 "Numerical suffix or a string possibly surrounded by brackets at\n" +
	 "the end of the key."}
	};

      return contents;
    }

    /**
     * Converts the key to its XML representation.
     *
     * @param key Key to convert.
     *
     * @return XML representation.
     */
    private String _toXml(String key)
    {
	return InterfileMetadataConversions.getKeyAsXml(key);
    }
}
