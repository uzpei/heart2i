/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

/**
 * GE Subheader that describes the history of the series.
 *
 * @version 15 October 2002
 */
public class GESeriesInfo extends GESubheader
{
    /** ID of the Suite ID for this Series. */
    public static final String SUITE_ID = "suiteID";

    /** ID of the Unique Flag. */
    public static final String UNIQUE_FLAG = "uniqueFlag";

    /** ID of the Disk ID for this Series. */
    public static final String DISK_ID = "diskID";

    /** ID of the Exam Number. */
    public static final String EXAM_NUMBER = "examNumber";

    /** ID of the Series Number. */
    public static final String SERIES_NUMBER = "seriesNumber";

    /** ID of the Allocation Series Data/Time stamp. */
    public static final String ALLOC_TIME_STAMP = "allocTimeStamp";

    /** ID of the Actual Series Data/Time stamp. */
    public static final String ACTUAL_TIME_STAMP = "actualTimeStamp";

    /** ID of the Series Description. */
    public static final String SERIES_DESCRIPTION = "seriesDescription";

    /** ID of the Primary Receiver Suite and Host. */
    public static final String PRIMARY_RECIEVER_HOST = "primaryRecieverHost";

    /** ID of the Archiver Suite and Host. */
    public static final String ARCHIVER_HOST = "archiverHost";

    /** ID of the Series Type. */
    public static final String SERIES_TYPE = "seriesType";

    /** ID of the Series from which prescribed. */
    public static final String SERIES_SOURCE = "seriesSource";

    /** ID of the Most-like Plane (for L/S). */
    public static final String PLANE = "plane";

    /** ID of the Scout or Axial (for CT). */
    public static final String SCAN_TYPE = "scanType";

    /** ID of the Patient Position. */
    public static final String POSITION = "position";

    /** ID of the Patient Entry. */
    public static final String ENTRY = "entry";

    /** ID of the Anatomical reference. */
    public static final String ANATOM_REFERENCE = "anatomReference";

    /** ID of the Horizontal Landmark. */
    public static final String HORIZONTAL_LANDMARK = "horizontalLandmark";

    /** ID of the Scan Protocol Name. */
    public static final String PROTOCOL_NAME = "protocolName";

    /** ID of the Non-zero if > 0 image used contrast(L/S). */
    public static final String CONTRAST = "contrast";

    /** ID of the RAS letter for first scan location (L/S). */
    public static final String START_RAS = "startRas";

    /** ID of the First scan location (L/S). */
    public static final String START_LOC = "startLoc";

    /** ID of the RAS letter for last scan location (L/S). */
    public static final String END_RAS = "endRas";

    /** ID of the Last scan location (L/S). */
    public static final String END_LOC = "endLoc";

    /** ID of the Last Pulse Sequence Used (L/S). */
    public static final String PULSE_SEQUENCE = "pulseSequence";

    /** ID of the Image Sort Order (L/S). */
    public static final String SORT_ORDER = "sortOrder";

    /** ID of the Landmark Counter. */
    public static final String LANDMARK_CNTR = "landmarkCntr";

    /** ID of the Number of Acquisitions. */
    public static final String NUMBER_AQUISITIONS = "numberAquisitions";

    /** ID of the Starting number for baselines. */
    public static final String BASELINE_START = "baselineStart";

    /** ID of the Ending number for baselines. */
    public static final String BASELINE_END = "baselineEnd";

    /** ID of the Starting number for enhanced scans. */
    public static final String ENHANCED_START = "enhancedStart";

    /** ID of the Ending number for enhanced scans. */
    public static final String ENHANCED_END = "enhancedEnd";

    /** ID of the Date/Time of Last Change. */
    public static final String LAST_MOD = "lastMod";

    /** ID of the Process that allocated this record. */
    public static final String ALLOC_KEY = "allocKey";

    /** ID of the Indicates number of updates to header. */
    public static final String HEADER_UPDATE_CNT = "headerUpdateCnt";

    /** ID of the Genesis Version - Created. */
    public static final String VERSION_CREATED = "versionCreated";

    /** ID of the Genesis Version - Now. */
    public static final String VERSION_NOW = "versionNow";

    /** ID of the PixelData size - as stored. */
    public static final String STORED_PIXEL_SIZE = "storedPixelSize";

    /** ID of the PixelData size - Compressed. */
    public static final String COMP_PIXEL_DATA_SIZE = "compPixelDataSize";

    /** ID of the PixelData size - UnCompressed. */
    public static final String UNCOMP_PIXEL_DATA_SIZE = "uncompPixelDataSize";

    /** ID of the Series Record checksum. */
    public static final String CHECK_SUM = "checkSum";

    /** ID of the Series Complete Flag. */
    public static final String COMPLETE = "complete";

    /** ID of the Number of Images Archived. */
    public static final String NUM_ARCHIVED = "numArchived";

    /** ID of the Last Image Number Used. */
    public static final String LAST_IMAGE = "lastImage";

    /** ID of the Number of Images Existing. */
    public static final String NUMBER_IMAGES = "numberImages";

    /** ID of the Size of Image Key for this Series. */
    public static final String IMAGE_KEY_LEN = "imageKeyLen";

    /** ID of the Image Keys for this Series. */
    public static final String IMAGE_KEY = "imageKey";

    /** ID of the Number of Unstored Images. */
    public static final String NUMBER_UNST_IMAGES = "numberUnstImages";

    /** ID of the Size of Unstored Image Keys for this Series. */
    public static final String UNST_IMAGE_KEY_LEN = "unstImageKeyLen";

    /** ID of the Unstored Image Keys for this Series. */
    public static final String UNST_IMAGE_KEY = "unstImageKey";

    /** ID of the Number of Unarchived Images. */
    public static final String NUMBER_UNARCH_IMAGES = "numberUnarchImages";

    /** ID of the Size of Unarchived Image Keys for this Series. */
    public static final String UNARCH_IMAGE_KEY_LEN = "unarchImageKeyLen";

    /** ID of the Unarchived Image Keys for this Series. */
    public static final String UNARCH_IAMGE_KEY = "unarchImageKey";

    /** ID of the Echo 1 Alpha Value. */
    public static final String ECHO1_ALPHA = "echo1Alpha";

    /** ID of the Echo 1 Beta Value. */
    public static final String ECHO1_BETA = "echo1Beta";

    /** ID of the Echo 1 Window Value. */
    public static final String ECHO1_WINDOW = "echo1Window";

    /** ID of the Echo 1 Level Value. */
    public static final String ECHO1_LEVEL = "echo1Level";

    /** ID of the Echo 2 Alpha Value. */
    public static final String ECHO2_ALPHA = "echo2Alpha";

    /** ID of the Echo 2 Beta Value. */
    public static final String ECHO2_BETA = "echo2Beta";

    /** ID of the Echo 2 Window Value. */
    public static final String ECHO2_WINDOW = "echo2Window";

    /** ID of the Echo 2 Level Value. */
    public static final String ECHO2_LEVEL = "echo2Level";

    /** ID of the Echo 3 Alpha Value. */
    public static final String ECHO3_ALPHA = "echo3Alpha";

    /** ID of the Echo 3 Beta Value. */
    public static final String ECHO3_BETA = "echo3Beta";

    /** ID of the Echo 3 Window Value. */
    public static final String ECHO3_WINDOW = "echo3Window";

    /** ID of the Echo 3 Level Value. */
    public static final String ECHO3_LEVEL = "echo3Level";

    /** ID of the Echo 4 Alpha Value. */
    public static final String ECHO4_ALPHA = "echo4Alpha";

    /** ID of the Echo 4 Beta Value. */
    public static final String ECHO4_BETA = "echo4Beta";

    /** ID of the Echo 4 Window Value. */
    public static final String ECHO4_WINDOW = "echo4Window";

    /** ID of the Echo 4 Level Value. */
    public static final String ECHO4_LEVEL = "echo4Level";

    /** ID of the Echo 5 Alpha Value. */
    public static final String ECHO5_ALPHA = "echo5Alpha";

    /** ID of the Echo 5 Beta Value. */
    public static final String ECHO5_BETA = "echo5Beta";

    /** ID of the Echo 5 Window Value. */
    public static final String ECHO5_WINDOW = "echo5Window";

    /** ID of the Echo 5 Level Value. */
    public static final String ECHO5_LEVEL = "echo5Level";

    /** ID of the Echo 6 Alpha Value. */
    public static final String ECHO6_ALPHA = "echo6Alpha";

    /** ID of the Echo 6 Beta Value. */
    public static final String ECHO6_BETA = "echo6Beta";

    /** ID of the Echo 6 Window Value. */
    public static final String ECHO6_WINDOW = "echo6Window";

    /** ID of the Echo 6 Level Value. */
    public static final String ECHO6_LEVEL = "echo6Level";

    /** ID of the Echo 7 Alpha Value. */
    public static final String ECHO7_ALPHA = "echo7Alpha";

    /** ID of the Echo 7 Beta Value. */
    public static final String ECHO7_BETA = "echo7Beta";

    /** ID of the Echo 7 Window Value. */
    public static final String ECHO7_WINDOW = "echo7Window";

    /** ID of the Echo 7 Level Value. */
    public static final String ECHO7_LEVEL = "echo7Level";

    /** ID of the Echo 8 Alpha Value. */
    public static final String ECHO8_ALPHA = "echo8Alpha";

    /** ID of the Echo 8 Beta Value. */
    public static final String ECHO8_BETA = "echo8Beta";

    /** ID of the Echo 8 Window Value. */
    public static final String ECHO8_WINDOW = "echo8Window";

    /** ID of the Echo 8 Level Value. */
    public static final String ECHO8_LEVEL = "echo8Level";

    /**
     * Constructs a GESeriesInfo.
     *
     * @param suiteID Suite ID. 
     * @param uniqueFlag  Unique Flag.
     * @param diskID Disk ID for this Series.
     * @param examNumber Exam Number.
     * @param seriesNumber Series Number.
     * @param allocTimeStamp Allocation Series Data/Time stamp.
     * @param actualTimeStamp Actual Series Data/Time stamp.
     * @param seriesDescription Series Description.
     * @param primaryRecieverHost Primary Receiver Suite and Host.
     * @param archiverHost Archiver Suite and Host.
     * @param seriesType Series Type.
     * @param seriesSource Series from which prescribed.
     * @param plane Most-like Plane (for L/S).
     * @param scanType Scout or Axial (for CT).
     * @param position Patient Position.
     * @param entry Patient Entry.
     * @param anatomReference Anatomical reference.
     * @param horizontalLandmark Horizontal Landmark.
     * @param protocolName Scan Protocol Name. 
     * @param contrast Non-zero if > 0 image used contrast(L/S).
     * @param startRas RAS letter for first scan location (L/S).
     * @param startLoc First scan location (L/S).
     * @param endRas RAS letter for last scan location (L/S).
     * @param endLoc Last scan location (L/S).
     * @param pulseSequence Last Pulse Sequence Used (L/S).
     * @param sortOrder Image Sort Order (L/S).
     * @param landmarkCntr Landmark Counter.
     * @param numberAquisitions Number of Acquisitions.
     * @param baselineStart Starting number for baselines.
     * @param baselineEnd Ending number for baselines.
     * @param enhancedStart Starting number for enhanced scans.
     * @param enhancedEnd Ending number for enhanced scans.
     * @param lastMod Date/Time of Last Change.
     * @param allocKey Process that allocated this record.
     * @param headerUpdateCnt Indicates number of updates to header.
     * @param versionCreated Genesis Version - Created.
     * @param versionNow Genesis Version - Now.
     * @param storedPixelSize PixelData size - as stored.
     * @param compPixelDataSize PixelData size - Compressed.
     * @param uncompPixelDataSize PixelData size - UnCompressed.
     * @param checkSum Series Record checksum.
     * @param complete Series Complete Flag.
     * @param numArchived Number of Images Archived.
     * @param lastImage Last Image Number Used.
     * @param numberImages Number of Images Existing.
     * @param imageKeyLen Size of Image Key for this Series.
     * @param imageKey Image Keys for this Series.
     * @param numberUnstImages Number of Unstored Images.
     * @param unstImageKeyLen Size of Unstored Image Keys for this Series.
     * @param unstImageKey Unstored Image Keys for this Series.
     * @param numberUnarchImages Number of Unarchived Images.
     * @param unarchImageKeyLen Size of Unarchived Image Keys for this Series.
     * @param unarchImageKey Unarchived Image Keys for this Series.
     * @param echo1Alpha Echo 1 Alpha Value.
     * @param echo1Beta Echo 1 Beta Value.
     * @param echo1Window Echo 1 Window Value.
     * @param echo1Level Echo 1 Level Value.
     * @param echo2Alpha Echo 2 Alpha Value.
     * @param echo2Beta Echo 2 Beta Value.
     * @param echo2Window Echo 2 Window Value.
     * @param echo2Level Echo 2 Level Value.
     * @param echo3Alpha Echo 3 Alpha Value.
     * @param echo3Beta Echo 3 Beta Value.
     * @param echo3Window Echo 3 Window Value.
     * @param echo3Level Echo 3 Level Value.
     * @param echo4Alpha Echo 4 Alpha Value.
     * @param echo4Beta Echo 4 Beta Value.
     * @param echo4Window Echo 4 Window Value.
     * @param echo4Level Echo 4 Level Value.
     * @param echo5Alpha Echo 5 Alpha Value.
     * @param echo5Beta Echo 5 Beta Value.
     * @param echo5Window Echo 5 Window Value.
     * @param echo5Level Echo 5 Level Value.
     * @param echo6Alpha Echo 6 Alpha Value.
     * @param echo6Beta Echo 6 Beta Value.
     * @param echo6Window Echo 6 Window Value.
     * @param echo6Level Echo 6 Level Value.
     * @param echo7Alpha Echo 7 Alpha Value.
     * @param echo7Beta Echo 7 Beta Value.
     * @param echo7Window Echo 7 Window Value.
     * @param echo7Level Echo 7 Level Value.
     * @param echo8Alpha Echo 8 Alpha Value.
     * @param echo8Beta Echo 8 Beta Value.
     * @param echo8Window Echo 8 Window Value.
     * @param echo8Level Echo 8 Level Value.
     *
     * @throws IllegalArgumentException If any byte array is larger than its
     *                                  specified maximum length.
     */
    public GESeriesInfo(byte [] suiteID, short uniqueFlag, char diskID, 
			   short examNumber, short seriesNumber, 
			   int allocTimeStamp, int actualTimeStamp, 
			   byte [] seriesDescription, 
			   byte [] primaryRecieverHost, byte [] archiverHost, 
			   short seriesType, short seriesSource, short plane, 
			   short scanType, int position, int entry, 
			   byte [] anatomReference, float horizontalLandmark,
			   byte [] protocolName, short contrast, 
			   char startRas, float startLoc, char endRas, 
			   float endLoc, short pulseSequence, short sortOrder,
			   int landmarkCntr, short numberAquisitions, 
			   short baselineStart, short baselineEnd, 
			   short enhancedStart, short enhancedEnd, 
			   int lastMod, byte [] allocKey, 
			   int headerUpdateCnt, byte [] versionCreated, 
			   byte [] versionNow, float storedPixelSize, 
			   float compPixelDataSize, float uncompPixelDataSize,
			   int checkSum, int complete, int numArchived, 
			   int lastImage, int numberImages,
			   int imageKeyLen, byte [] imageKey, 
			   int numberUnstImages, int unstImageKeyLen, 
			   byte [] unstImageKey, int numberUnarchImages, 
			   int unarchImageKeyLen, byte [] unarchImageKey, 
			   float echo1Alpha, float echo1Beta, 
			   short echo1Window, short echo1Level, 
			   float echo2Alpha, float echo2Beta, 
			   short echo2Window, short echo2Level, 
			   float echo3Alpha, float echo3Beta, 
			   short echo3Window, short echo3Level, 
			   float echo4Alpha, float echo4Beta, 
			   short echo4Window, short echo4Level, 
			   float echo5Alpha, float echo5Beta, 
			   short echo5Window, short echo5Level, 
			   float echo6Alpha, float echo6Beta, 
			   short echo6Window, short echo6Level,
			   float echo7Alpha, float echo7Beta,
			   short echo7Window, short echo7Level, 
			   float echo8Alpha, float echo8Beta, 
			   short echo8Window, short echo8Level)
    {
	super(85);
	
	_addElement(SUITE_ID, _createString(suiteID));
	_addElement(UNIQUE_FLAG, new Short(uniqueFlag));
	_addElement(DISK_ID, new Character(diskID));
	_addElement(EXAM_NUMBER, new Short(examNumber));
	_addElement(SERIES_NUMBER, new Short(seriesNumber));
	_addElement(ALLOC_TIME_STAMP, new Integer(allocTimeStamp));
	_addElement(ACTUAL_TIME_STAMP, new Integer(actualTimeStamp));
	_addElement(SERIES_DESCRIPTION, _createString(seriesDescription));
	_addElement(PRIMARY_RECIEVER_HOST, _createString(primaryRecieverHost));
	_addElement(ARCHIVER_HOST, _createString(archiverHost));
	_addElement(SERIES_TYPE, new Short(seriesType));
	_addElement(SERIES_SOURCE, new Short(seriesSource));
	_addElement(PLANE, new Short(plane));
	_addElement(SCAN_TYPE, new Short(scanType));
	_addElement(POSITION, new Integer(position));
	_addElement(ENTRY, new Integer(entry));
	_addElement(ANATOM_REFERENCE, _createString(anatomReference));
	_addElement(HORIZONTAL_LANDMARK, new Float(horizontalLandmark));
	_addElement(PROTOCOL_NAME, _createString(protocolName));
	_addElement(CONTRAST, new Short(contrast));
	_addElement(START_RAS, new Character(startRas));
	_addElement(START_LOC, new Float(startLoc));
	_addElement(END_RAS, new Character(endRas));
	_addElement(END_LOC, new Float(endLoc));
	_addElement(PULSE_SEQUENCE, new Short(pulseSequence));
	_addElement(SORT_ORDER, new Short(sortOrder));
	_addElement(LANDMARK_CNTR, new Integer(landmarkCntr));
	_addElement(NUMBER_AQUISITIONS, new Short(numberAquisitions));
	_addElement(BASELINE_START, new Short(baselineStart));
	_addElement(BASELINE_END, new Short(baselineEnd));
	_addElement(ENHANCED_START, new Short(enhancedStart));
	_addElement(ENHANCED_END, new Short(enhancedEnd));
	_addElement(LAST_MOD, new Integer(lastMod));
	_addElement(ALLOC_KEY, _createString(allocKey));
	_addElement(HEADER_UPDATE_CNT, new Integer(headerUpdateCnt));
	_addElement(VERSION_CREATED, _createString(versionCreated));
	_addElement(VERSION_NOW, _createString(versionNow));
	_addElement(STORED_PIXEL_SIZE, new Float(storedPixelSize));
	_addElement(COMP_PIXEL_DATA_SIZE,  new Float(compPixelDataSize));
	_addElement(UNCOMP_PIXEL_DATA_SIZE,  new Float(uncompPixelDataSize));
	_addElement(CHECK_SUM, new Integer(checkSum));
	_addElement(COMPLETE, new Integer(complete));
	_addElement(NUM_ARCHIVED, new Integer(numArchived));
	_addElement(LAST_IMAGE, new Integer(lastImage));
	_addElement(NUMBER_IMAGES, new Integer(numberImages));
	_addElement(IMAGE_KEY_LEN, new Integer(imageKeyLen));
	_addElement(IMAGE_KEY, _createString(imageKey));
	_addElement(NUMBER_UNST_IMAGES, new Integer(numberUnstImages));
	_addElement(UNST_IMAGE_KEY_LEN, new Integer(unstImageKeyLen));
	_addElement(UNST_IMAGE_KEY, _createString(unstImageKey));
	_addElement(NUMBER_UNARCH_IMAGES, new Integer(numberUnarchImages));
	_addElement(UNARCH_IMAGE_KEY_LEN, new Integer(unarchImageKeyLen));
	_addElement(UNARCH_IAMGE_KEY, _createString(unarchImageKey));
	_addElement(ECHO1_ALPHA,  new Float(echo1Alpha));
	_addElement(ECHO1_BETA,  new Float(echo1Beta));
	_addElement(ECHO1_WINDOW, new Short(echo1Window));
	_addElement(ECHO1_LEVEL, new Short(echo1Level));
	_addElement(ECHO2_ALPHA,  new Float(echo2Alpha));
	_addElement(ECHO2_BETA,  new Float(echo2Beta));
	_addElement(ECHO2_WINDOW, new Short(echo2Window));
	_addElement(ECHO2_LEVEL, new Short(echo2Level));
	_addElement(ECHO3_ALPHA,  new Float(echo3Alpha));
	_addElement(ECHO3_BETA,  new Float(echo3Beta));
	_addElement(ECHO3_WINDOW, new Short(echo3Window));
	_addElement(ECHO3_LEVEL, new Short(echo3Level));
	_addElement(ECHO4_ALPHA,  new Float(echo4Alpha));
	_addElement(ECHO4_BETA,  new Float(echo4Beta));
	_addElement(ECHO4_WINDOW, new Short(echo4Window));
	_addElement(ECHO4_LEVEL, new Short(echo4Level));
	_addElement(ECHO5_ALPHA,  new Float(echo5Alpha));
	_addElement(ECHO5_BETA,  new Float(echo5Beta));
	_addElement(ECHO5_WINDOW, new Short(echo5Window));
	_addElement(ECHO5_LEVEL, new Short(echo5Level));
	_addElement(ECHO6_ALPHA,  new Float(echo6Alpha));
	_addElement(ECHO6_BETA,  new Float(echo6Beta));
	_addElement(ECHO6_WINDOW, new Short(echo6Window));
	_addElement(ECHO6_LEVEL, new Short(echo6Level));
	_addElement(ECHO7_ALPHA,  new Float(echo7Alpha));
	_addElement(ECHO7_BETA,  new Float(echo7Beta));
	_addElement(ECHO7_WINDOW, new Short(echo7Window));
	_addElement(ECHO7_LEVEL, new Short(echo7Level));
	_addElement(ECHO8_ALPHA,  new Float(echo8Alpha));
	_addElement(ECHO8_BETA,  new Float(echo8Beta));
	_addElement(ECHO8_WINDOW, new Short(echo8Window));
	_addElement(ECHO8_LEVEL, new Short(echo8Level));
    }
}
