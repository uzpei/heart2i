/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

/**
 * Header that contains elements for the GE 5.0 format.
 *
 * @version 10 October 2002
 */
public class GEHeader
{
  /** GE Subheader that describes the history of the suite. */
  private GESuiteInfo _geSuiteInfo;

  /** GE Subheader that describes the history of the exam. */
  private GEExamInfo _geExamInfo;

  /** GE Subheader that describes the history of the series. */
  private GESeriesInfo _geSeriesInfo;

  /** GE Subheader that describes the image sizes. */
  private GEImageInfo _geImageInfo;

  /**
   * Constructs an GEHeader.
   *
   * @param geSuite GE Subheader that describes the history of the
   *                    suite; this may be null.
   * @param geEXam GE Subheader that describes the history of the
   *                    exam; this may be null.
   * @param geSeries GE Subheader that describes the history of the
   *                    series; this may be null.
   * @param imageInfo GE Subheader that describes the image sizes.
   *
   * @throws NullPointerException If the Info is null.
   */
  public GEHeader(GESuiteInfo geSuiteInfo, GEExamInfo geExamInfo, 
		  GESeriesInfo geSeriesInfo, 
		  GEImageInfo geImageInfo)
    {
      // Check for a null Info
      if (geImageInfo == null || geExamInfo == null || 
	  geSeriesInfo == null || geSuiteInfo == null) {
	String msg = "One or more info structures is null.";
	throw new NullPointerException(msg);
      }

      _geSuiteInfo = geSuiteInfo;
      _geExamInfo = geExamInfo;
      _geSeriesInfo = geSeriesInfo;
      _geImageInfo = geImageInfo;
    }

  /**
   * Gets the Suite history.
   *
   * @return GE Subheader that describes the history of the suite, or null
   *         if it does not exist.
   */
  public GESuiteInfo getGESuiteInfo()
    {
      return _geSuiteInfo;
    }

  /**
   * Gets the Exam history.
   *
   * @return GE Subheader that describes the history of the exam, or null
   *         if it does not exist.
   */
  public GEExamInfo getGEExamInfo()
    {
      return _geExamInfo;
    }

  /**
   * Gets the Series history.
   *
   * @return GE Subheader that describes the history of the series, or null
   *         if it does not exist.
   */
  public GESeriesInfo getGESeriesInfo()
    {
      return _geSeriesInfo;
    }

  /**
   * Gets the Image Info.
   *
   * @return GE Subheader that describes the image sizes.
   */
  public GEImageInfo getGEImageInfo()
    {
      return _geImageInfo;
    }

  /**
   * Gets the contents of the GE Header.
   *
   * @return String representation of the contents of the GE Header.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Write the Suite Info if it exists
      if ( getGESuiteInfo() != null ) {
	buffer.append("\nSuite Info:  \n");
	buffer.append( getGESuiteInfo().toString() );
      }

      // Write the Data Info if it exists
      if ( getGEExamInfo() != null ) {
	buffer.append("\nExam Info:  \n");
	buffer.append( getGEExamInfo().toString() );
      }

      // Write the Data Info if it exists
      if ( getGESeriesInfo() != null ) {
	buffer.append("\nSeries Info:  \n");
	buffer.append( getGESeriesInfo().toString() );
      }

      // Write the Image Info
      buffer.append("\nImage Info:  \n");
      buffer.append( getGEImageInfo().toString() );

      return buffer.toString();
    }
}
