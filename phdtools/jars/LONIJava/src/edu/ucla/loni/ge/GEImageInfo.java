/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

/**
 * GE Subheader that describes the dimensions of the image.
 *
 * @version 15 July 2003
 */
public abstract class GEImageInfo extends GESubheader
{
    /** ID of the suite ID for this image. */
    public static final String SUITE_ID = "suiteID";
    
    /** ID of the Unique Flag. */
    public static final String UNIQUE_FLAG = "uniqueFlag";
    
    /** ID of the Disk ID for this Image. */
    public static final String DISK_ID = "diskID";
    
    /** ID of the Exam number for this image. */
    public static final String EXAM_NUMBER = "examNumber";
    
    /** ID of the Series Number for this image. */
    public static final String SERIES_NUM = "seriesNum";
    
    /** ID of the Image Number. */
    public static final String IMAGE_NUM = "imageNum";
    
    /** ID of the Allocation Image date/time stamp. */
    public static final String ALLOC_DATE_TIME = "AllocDateTime";
    
    /** ID of the Actual Image date/time stamp. */
    public static final String ACTUAL_DATE_TIME = "ActualDateTime";
    
    /** ID of the Duration of scan (secs). */
    public static final String SCAN_TIME = "scanTime";
    
    /** ID of the Slice Thickness (mm). */
    public static final String SLICE_THICK = "sliceThick";
    
    /** ID of the Image matrix size - X. */
    public static final String I_MATRIX_X = "iMatrixX";
    
    /** ID of the Image matrix size - Y. */
    public static final String I_MATRIX_Y = "iMatrixY";
    
    /** ID of the Display field of view - X (mm). */
    public static final String DISPLAY_FIELD_X = "displayFieldX";
    
    /** ID of the Display field of view - Y (if different). */
    public static final String DISPLAY_FIELD_Y = "displayFieldY";
    
    /** ID of the Image dimension - X. */
    public static final String DIMENSION_X = "dimensionX";
    
    /** ID of the Image dimension - Y. */
    public static final String DIMENSION_Y = "dimensionY";
    
    /** ID of the Image pixel size - X. */
    public static final String PIXEL_SIZE_X = "pixelSizeX";
    
    /** ID of the Image pixel size - Y. */
    public static final String PIXEL_SIZE_Y = "pixelSizeY";
    
    /** ID of the Pixel Data ID. */
    public static final String PIXEL_DATA_ID = "pixelDataID";
    
    /** ID of the IV Contrast Agent. */
    public static final String CONTRAST_IV = "contrastIV";
    
    /** ID of the Oral Contrast Agent. */
    public static final String CONTRAST_ORAL = "contrastOral";
    
    /** ID of the Image Contrast Mode. */
    public static final String CONTRAST_MODE = "contrastMode";
    
    /** ID of the Series from which prescribed. */
    public static final String SERIES_RX = "seriesRX";
    
    /** ID of the Image from which prescribe. */
    public static final String IMAGE_RX = "imageRX";
    
    /** ID of the Screen Format(8/16 bit). */
    public static final String SCREEN_FORMAT = "screenFormat";
    
    /** ID of the Plane Type. */
    public static final String PLANE = "plane";
    
    /** ID of the Spacing between scans (mm?). */
    public static final String SCAN_SPACING = "scanSpacing";
    
    /** ID of the Image compression type for allocation. */
    public static final String COMPRESSION = "compression";
    
    /** ID of the Scout Type (AP or lateral). */
    public static final String SCOUT_TYPE = "scoutType";
    
    /** ID of the RAS letter of image location. */
    public static final String LOCATION_RAS = "locationRAS";
    
    /** ID of the Image location. */
    public static final String IMAGE_LOCATION = "imageLocation";
    
    /** ID of the Center R coord of plane image. */
    public static final String CENTER_R = "centerR";
    
    /** ID of the Center A coord of plane image. */
    public static final String CENTER_A = "centerA";
    
    /** ID of the Center S coord of plane image. */
    public static final String CENTER_S = "centerS";
    
    /** ID of the Normal R coord. */
    public static final String NORM_R = "normR";
    
    /** ID of the Normal A coord. */
    public static final String NORM_A = "normA";
    
    /** ID of the Normal S coord. */
    public static final String NORM_S = "normS";
    
    /** ID of the R Coord of Top Left Hand Corner. */
    public static final String TOP_LEFT_R = "TopLeftR";
    
    /** ID of the A Coord of Top Left Hand Corner. */
    public static final String TOP_LEFT_A = "TopLeftA";
    
    /** ID of the S Coord of Top Left Hand Corner. */
    public static final String TOP_LEFT_S = "TopLeftS";
    
    /** ID of the R Coord of Top Right Hand Corner. */
    public static final String TOP_RIGHT_R = "TopRightR";
    
    /** ID of the A Coord of Top Right Hand Corner. */
    public static final String TOP_RIGHT_A = "TopRightA";
    
    /** ID of the S Coord of Top Right Hand Corner. */
    public static final String TOP_RIGHT_S = "TopRightS";
    
    /** ID of the R Coord of Bottom Right Hand Corner. */
    public static final String BOT_RIGHT_R = "BotRightR";
    
    /** ID of the A Coord of Bottom Right Hand Corner. */
    public static final String BOT_RIGHT_A = "BotRightA";
    
    /** ID of the S Coord of Bottom Right Hand Corner. */
    public static final String BOT_RIGHT_S = "BotRightS";
    
    /** ID of the Foreign Image Revision. */
    public static final String FOREIGN_IMAGE_REV = "foreignImageRev";    
    
    /** ID of the Allocation Key. */
    public static final String ALLOC_KEY = "allocKey";
    
    /** ID of the Date/Time of Last Change. */
    public static final String LAST_MOD = "lastMod";
    
    /** ID of the Genesis Version - Created. */
    public static final String VERSION_CREATION = "versionCreation";
    
    /** ID of the Genesis Version - Now. */
    public static final String VERSION_NOW = "versionNow";
    
    /** ID of the PixelData size - as stored. */
    public static final String PIXEL_DATA_SIZE_A = "pixelDataSizeA";
    
    /** ID of the PixelData size - Compressed. */
    public static final String PIXEL_DATA_SIZE_C = "pixelDataSizeC";
    
    /** ID of the PixelData size - UnCompressed. */
    public static final String PIXEL_DATA_SIZE_U = "pixelDataSizeU";
    
    /** ID of the AcqRecon record checksum. */
    public static final String CHECK_SUM = "checkSum";
    
    /** ID of the Image Archive Flag. */
    public static final String ARCHIVE = "archive";
    
    /** ID of the Image Complete Flag. */
    public static final String COMPLETE = "complete";
    
    /** ID of the Reference Image Field. */
    public static final String REFERENCE_FIELD = "referenceField";
    
    /** ID of the Summary Image Field. */
    public static final String SUMMARY_FIELD = "summaryField";
    
    /** ID of the Window Value. */
    public static final String WINDOW = "window";
    
    /** ID of the Level Value. */
    public static final String LEVEL = "level";
    
    /** ID of the Integer Slop Field 1. */
    public static final String SLOP_INT1 = "slopInt1";
    
    /** ID of the Integer Slop Field 2. */
    public static final String SLOP_INT2 = "slopInt2";
    
    /** ID of the Integer Slop Field 3. */
    public static final String SLOP_INT3 = "slopInt3";
    
    /** ID of the Integer Slop Field 4. */
    public static final String SLOP_INT4 = "slopInt4";
    
    /** ID of the Integer Slop Field 5. */
    public static final String SLOP_INT5 = "slopInt5";
    
    /** ID of the Float Slop Field 1. */
    public static final String SLOP_FLOAT1 = "slopFloat1";
    
    /** ID of the Float Slop Field 1. */
    public static final String SLOP_FLOAT2 = "slopFloat2";
    
    /** ID of the Float Slop Field 1. */
    public static final String SLOP_FLOAT3 = "slopFloat3";
    
    /** ID of the Float Slop Field 1. */
    public static final String SLOP_FLOAT4 = "slopFloat4";
    
    /** ID of the Float Slop Field 1. */
    public static final String SLOP_FLOAT5 = "slopFloat5";
    
    /** ID of the String Slop Field 1. */
    public static final String SLOP_STR1 = "slopStr1";
    
    /** ID of the String Slop Field 2. */
    public static final String SLOP_STR2 = "slopStr2";
    
    
    /**
     * Constructs a GEImageInfo.
     *
     * @param numberFields Number of fields in image info implementation.
     * @param suiteID suite ID for this image.
     * @param uniqueFlag Unique Flag
     * @param diskID Disk ID for this Image
     * @param examNumber Exam number for this image
     * @param seriesNum Series Number for this image
     * @param imageNum Image Number
     * @param AllocDateTime Allocation Image date/time stamp
     * @param ActualDateTime Actual Image date/time stamp
     * @param scanTime Duration of scan (secs)
     * @param sliceThick Slice Thickness (mm)
     * @param iMatrixX Image matrix size - X
     * @param iMatrixY Image matrix size - Y
     * @param displayFieldX Display field of view - X (mm)
     * @param displayFieldY Display field of view - Y (if different)
     * @param dimensionX Image dimension - X
     * @param dimensionY Image dimension - Y
     * @param pixelSizeX Image pixel size - X
     * @param pixelSizeY Image pixel size - Y
     * @param pixelDataID Pixel Data ID
     * @param contrastIV IV Contrast Agent
     * @param contrastOral Oral Contrast Agent
     * @param contrastMode Image Contrast Mode
     * @param seriesRX Series from which prescribed
     * @param imageRX Image from which prescribe
     * @param screenFormat Screen Format(8/16 bit)
     * @param plane Plane Type
     * @param scanSpacing Spacing between scans (mm?)
     * @param compression Image compression type for allocation
     * @param scoutType Scout Type (AP or lateral)
     * @param locationRAS RAS letter of image location
     * @param imageLocation Image location
     * @param centerR Center R coord of plane image
     * @param centerA Center A coord of plane image
     * @param centerS Center S coord of plane image
     * @param normR Normal R coord
     * @param normA Normal A coord
     * @param normS Normal S coord
     * @param TopLeftR R Coord of Top Left Hand Corner
     * @param TopLeftA A Coord of Top Left Hand Corner
     * @param TopLeftS S Coord of Top Left Hand Corner
     * @param TopRightR R Coord of Top Right Hand Corner
     * @param TopRightA A Coord of Top Right Hand Corner
     * @param TopRightS S Coord of Top Right Hand Corner
     * @param BotRightR R Coord of Bottom Right Hand Corner
     * @param BotRightA A Coord of Bottom Right Hand Corner
     * @param BotRightS S Coord of Bottom Right Hand Corner
     * @param foreignImageRev Foreign Image Revision
     * @param allocKey Allocation Key
     * @param lastMod Date/Time of Last Change
     * @param versionCreation Genesis Version - Created
     * @param versionNow Genesis Version - Now
     * @param pixelDataSizeA PixelData size - as stored
     * @param pixelDataSizeC PixelData size - Compressed
     * @param pixelDataSizeU PixelData size - UnCompressed
     * @param checkSum AcqRecon record checksum 
     * @param archive Image Archive Flag
     * @param complete Image Complete Flag
     * @param referenceField Reference Image Field
     * @param summaryField Summary Image Field
     * @param window Window Value
     * @param level Level Value
     * @param slopInt1 Integer Slop Field 1
     * @param slopInt2 Integer Slop Field 2
     * @param slopInt3 Integer Slop Field 3
     * @param slopInt4 Integer Slop Field 4
     * @param slopInt5 Integer Slop Field 5
     * @param slopFloat1 Float Slop Field 1
     * @param slopFloat2 Float Slop Field 2
     * @param slopFloat3 Float Slop Field 3
     * @param slopFloat4 Float Slop Field 4
     * @param slopFloat5 Float Slop Field 5
     * @param slopStr1 
     * @param slopStr2 
     */
    public GEImageInfo(int numberFields, byte [] suiteID, short uniqueFlag, 
			    char diskID, short examNumber, 
			    short seriesNum, short imageNum, 
			    int AllocDateTime, int ActualDateTime,
			    float scanTime, float sliceThick, 
			    short iMatrixX, short iMatrixY, 
			    float displayFieldX, 
			    float displayFieldY, 
			    float dimensionX, float dimensionY,
			    float pixelSizeX, float pixelSizeY,
			    byte [] pixelDataID,
			    byte [] contrastIV, 
			    byte [] contrastOral, 
			    short contrastMode, short seriesRX, 
			    short imageRX, short screenFormat, 
			    short plane, float scanSpacing,
			    short compression, short scoutType, 
			    char locationRAS, float imageLocation,
			    float centerR, float centerA, 
			    float centerS, float normR, 
			    float normA, float normS, 
			    float TopLeftR, float TopLeftA, 
			    float TopLeftS, float TopRightR, 
			    float TopRightA, float TopRightS, 
			    float BotRightR, float BotRightA, 
			    float BotRightS, 
			    byte [] foreignImageRev)
    {
	super(numberFields);

	_addElement(SUITE_ID, _createString(suiteID));
	_addElement(UNIQUE_FLAG, new Short(uniqueFlag));
	_addElement(DISK_ID, new Character(diskID));
	_addElement(EXAM_NUMBER, new Short(examNumber));
	_addElement(SERIES_NUM, new Short(seriesNum));
	_addElement(IMAGE_NUM, new Short(imageNum));
	_addElement(ALLOC_DATE_TIME, new Integer(AllocDateTime));
	_addElement(ACTUAL_DATE_TIME, new Integer(ActualDateTime));
	_addElement(SCAN_TIME, new Float(scanTime));
	_addElement(SLICE_THICK, new Float(sliceThick));
	_addElement(I_MATRIX_X, new Short(iMatrixX));
	_addElement(I_MATRIX_Y, new Short(iMatrixY));
	_addElement(DISPLAY_FIELD_X, new Float(displayFieldX));
	_addElement(DISPLAY_FIELD_Y, new Float(displayFieldY));
	_addElement(DIMENSION_X, new Float(dimensionX));
	_addElement(DIMENSION_Y, new Float(dimensionY));
	_addElement(PIXEL_SIZE_X, new Float(pixelSizeX));
	_addElement(PIXEL_SIZE_Y, new Float(pixelSizeY));
	_addElement(PIXEL_DATA_ID, _createString(pixelDataID));
	_addElement(CONTRAST_IV, _createString(contrastIV));
	_addElement(CONTRAST_ORAL, _createString(contrastOral));
	_addElement(CONTRAST_MODE, new Short(contrastMode));
	_addElement(SERIES_RX, new Short(seriesRX));
	_addElement(IMAGE_RX, new Short(imageRX));
	_addElement(SCREEN_FORMAT, new Short(screenFormat));
	_addElement(PLANE, new Short(plane));
	_addElement(SCAN_SPACING, new Float(scanSpacing));
	_addElement(COMPRESSION, new Short(compression));
	_addElement(SCOUT_TYPE, new Short(scoutType));
	_addElement(LOCATION_RAS, new Character(locationRAS));
	_addElement(IMAGE_LOCATION, new Float(imageLocation));
	_addElement(CENTER_R, new Float(centerR));
	_addElement(CENTER_A, new Float(centerA));
	_addElement(CENTER_S, new Float(centerS));
	_addElement(NORM_R, new Float(normR));
	_addElement(NORM_A, new Float(normA));
	_addElement(NORM_S, new Float(normS));
	_addElement(TOP_LEFT_R, new Float(TopLeftR));
	_addElement(TOP_LEFT_A, new Float(TopLeftA));
	_addElement(TOP_LEFT_S, new Float(TopLeftS));
	_addElement(TOP_RIGHT_R, new Float(TopRightR));
	_addElement(TOP_RIGHT_A, new Float(TopRightA));
	_addElement(TOP_RIGHT_S, new Float(TopRightS));
	_addElement(BOT_RIGHT_R, new Float(BotRightR));
	_addElement(BOT_RIGHT_A, new Float(BotRightA));
	_addElement(BOT_RIGHT_S, new Float(BotRightS));
	_addElement(FOREIGN_IMAGE_REV, _createString(foreignImageRev));
    }
}
