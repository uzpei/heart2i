/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import edu.ucla.loni.ge.GECTImageInfo;
import edu.ucla.loni.ge.GEExamInfo;
import edu.ucla.loni.ge.GEImageInfo;
import edu.ucla.loni.ge.GEMRImageInfo;
import edu.ucla.loni.ge.GESeriesInfo;
import edu.ucla.loni.ge.GESuiteInfo;
import java.util.Vector;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * GE Metadata class.
 *
 * @version 3 July 2003
 */
public class GEMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_ge_1.0";

  /** Name of the Header Key Node. */
  public static final String GE_SUITE_INFO_NAME = "GE_SUITE_INFO";

  /** Name of the Image Dimension Node. */
  public static final String GE_EXAM_INFO_NAME = "GE_EXAM_INFO";

  /** Name of the Data History Node. */
  public static final String GE_SERIES_INFO_NAME = "GE_SERIES_INFO";

  /** Name of the Data History Node. */
  public static final String GE_IMAGE_INFO_NAME = "GE_IMAGE_INFO";

  /** Name of the Data History Node. */
  public static final String GE_MR_IMAGE_INFO_NAME = "GE_MR_IMAGE_INFO";

  /** Name of the Data History Node. */
  public static final String GE_CT_IMAGE_INFO_NAME = "GE_CT_IMAGE_INFO";

  /** Single instance of the GEMetadataFormat. */
  private static GEMetadataFormat _format = new GEMetadataFormat();

  /** Constructs a GEMetadataFormat. */
  private GEMetadataFormat()
    {
      // Create the root with required children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_ALL);

      // Add the GE Suite Info child with no children
      addElement(GE_SUITE_INFO_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the GE Suite Info attributes
      addAttribute(GE_SUITE_INFO_NAME, GESuiteInfo.SUITE_ID, DATATYPE_STRING,
		   true, null);
      addAttribute(GE_SUITE_INFO_NAME, GESuiteInfo.UNIQUE_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SUITE_INFO_NAME, new String(GESuiteInfo.DISK_ID), 
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SUITE_INFO_NAME, GESuiteInfo.PRODUCT_ID, DATATYPE_STRING,
		   true, null);
      addAttribute(GE_SUITE_INFO_NAME, GESuiteInfo.VERSION_CR, DATATYPE_STRING,
		   true, null);
      addAttribute(GE_SUITE_INFO_NAME, GESuiteInfo.VERSION_CU, DATATYPE_STRING,
		   true, null);
      addAttribute(GE_SUITE_INFO_NAME, GESuiteInfo.CHECK_SUM, DATATYPE_INTEGER,
		   true, null);

      // Add the GE Exam Info child with no children
      addElement(GE_EXAM_INFO_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the GE Exam Info attributes
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SUITE_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.UNIQUE_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, new String(GEExamInfo.DISK_ID),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.EXAM_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.HOSPITAL_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DETECTOR,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_CELLS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.ZERO_CELL,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.CELL_SPACE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SRC_TO_DET,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SRC_TO_ISO,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.TUBE_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DAS_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_DECON_KERNALS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DECON_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DECON_DENSITY,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DECON_STEP_SIZE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DECON_SHIFT_CNT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.MAG_STRENGTH,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_AGE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_AGE_NOT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_SEX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_WEIGHT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.TRAUMA,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_HISTORY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.REQ_NUMBER,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.EXAM_DATE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.REF_PHYSICIAN,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.DIAGRAD,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.OPERATOR,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.EXAM_DESCRIPTION,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.EXAM_TYPE,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.EXAM_FORMAT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.FIRST_AXIAL_TIME,
		   DATATYPE_DOUBLE, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.HOST,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.LAST_MOD,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PROTOCOL_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.ALLOC_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.UPDATA_CNT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.VERSION_CREATION,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.VERSION_NOW,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.CHECK_SUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.COMPLETE_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SERIES_CNT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_ARCHIVED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_SERIES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SERIES_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SERIES_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_UNST_SERIES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.UNST_SERIES_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.UNST_SERIES_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_UNARCH_SERIES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.UNARCH_SERIES_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.UNARCH_SERIES_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.NUMBER_PROS_SERIES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PROS_SERIES_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PROS_SERIES_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.MODEL_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.MODEL_CNT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.MODEL_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.MODEL_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.PATIENT_STATUS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SYSTEM_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.SERVICE_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_EXAM_INFO_NAME, GEExamInfo.MOBLE_LOCATION,
		   DATATYPE_INTEGER, true, null);

      // Add the GE Series Info child with no children
      addElement(GE_SERIES_INFO_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the GE Series Info attributes
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SUITE_ID,
		   DATATYPE_STRING,true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.UNIQUE_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, new String(GESeriesInfo.DISK_ID),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.EXAM_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SERIES_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ALLOC_TIME_STAMP,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ACTUAL_TIME_STAMP,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SERIES_DESCRIPTION,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.PRIMARY_RECIEVER_HOST,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ARCHIVER_HOST,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SERIES_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SERIES_SOURCE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.PLANE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SCAN_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.POSITION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ENTRY,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ANATOM_REFERENCE,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.HORIZONTAL_LANDMARK,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.PROTOCOL_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.CONTRAST,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, new String(GESeriesInfo.START_RAS),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.START_LOC,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, new String(GESeriesInfo.END_RAS),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.END_LOC,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.PULSE_SEQUENCE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.SORT_ORDER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.LANDMARK_CNTR,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.NUMBER_AQUISITIONS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.BASELINE_START,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.BASELINE_END,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ENHANCED_START,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ENHANCED_END,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.LAST_MOD,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ALLOC_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.HEADER_UPDATE_CNT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.VERSION_CREATED,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.VERSION_NOW,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.STORED_PIXEL_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.COMP_PIXEL_DATA_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.UNCOMP_PIXEL_DATA_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.CHECK_SUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.COMPLETE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.NUM_ARCHIVED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.LAST_IMAGE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.NUMBER_IMAGES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.IMAGE_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.IMAGE_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.NUMBER_UNST_IMAGES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.UNST_IMAGE_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.UNST_IMAGE_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.NUMBER_UNARCH_IMAGES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.UNARCH_IMAGE_KEY_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.UNARCH_IAMGE_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO1_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO1_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO1_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO1_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO2_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO2_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO2_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO2_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO3_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO3_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO3_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO3_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO4_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO4_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO4_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO4_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO5_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO5_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO5_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO5_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO6_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO6_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO6_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO6_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO7_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO7_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO7_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO7_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO8_ALPHA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO8_BETA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO8_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_SERIES_INFO_NAME, GESeriesInfo.ECHO8_LEVEL,
		   DATATYPE_INTEGER, true, null);

      // Add the GE Image Info child with selection of children
      addElement(GE_IMAGE_INFO_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_CHOICE);

      // Add the GE MR Image Info child with no children
      addElement(GE_MR_IMAGE_INFO_NAME, GE_IMAGE_INFO_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the GE MR Image Info attributes
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SUITE_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.UNIQUE_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, new String(GEImageInfo.DISK_ID),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.EXAM_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SERIES_NUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.IMAGE_NUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.ALLOC_DATE_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.ACTUAL_DATE_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SCAN_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLICE_THICK,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.I_MATRIX_X,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.I_MATRIX_Y,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.DISPLAY_FIELD_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.DISPLAY_FIELD_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.DIMENSION_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.DIMENSION_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PIXEL_SIZE_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PIXEL_SIZE_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CONTRAST_IV,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CONTRAST_ORAL,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CONTRAST_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SERIES_RX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.IMAGE_RX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SCREEN_FORMAT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PLANE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SCAN_SPACING,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.COMPRESSION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SCOUT_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, 
		   new String(GEImageInfo.LOCATION_RAS),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.IMAGE_LOCATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CENTER_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CENTER_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CENTER_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.NORM_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.NORM_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.NORM_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.TOP_LEFT_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.TOP_LEFT_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.TOP_LEFT_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.TOP_RIGHT_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.TOP_RIGHT_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.TOP_RIGHT_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.BOT_RIGHT_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.BOT_RIGHT_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.BOT_RIGHT_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.FOREIGN_IMAGE_REV,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.REP_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.INV_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.ECHO_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.ECHO_TIME2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.NUM_ECHO,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.ECHO_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.TABLE_DELTA,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.NUM_EXCITATIONS,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CONTINUOUS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.HEART_RATE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.TIME_DELAY,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.AVERAGE_SAR,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PEAK_SAR,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.MONITOR_SAR,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.TRIGGER_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CARD_REP_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.IMAGES_PER_CYCLE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.TRANSMIT_GAIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.RECV_GAIN_ANALOG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.RECV_GAIN_DIGITAL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.FLIP_ANGLE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.MIN_DELAY,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CARDIAC_PHASE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SWAP_PHASE_FREQ,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PAUSE_INTERVAL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PAUSE_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.OBLIQUE_PLANE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SLICE_OFF,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.TRANSMIT_FREQ,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.AUTO_CENTER_FREQ,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.AUTO_TRANS_GAIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PRESCAN_R1_ANALOG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PRESCAN_R2_DIGITAL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER_BITMAP,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CENTER_FREQ,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.IMAGEING_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.IMAGEING_OPTIONS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PULSE_SEQ,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PULSE_SEQ_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PULSE_SEQ_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PSD_DATE_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PSD_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.COIL_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.COIL_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SRF_COIL_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SRF_COIL_EXTR,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.RAW_RUN_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CAL_FIELD_STRENGTH,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SUPP_TECH,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.VAR_BANDWIDTH,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.NUM_SLICE_IN_SCAN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.GRAPHIC_PRESCRIBED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.INTER_DELAY,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER0,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER5,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER6,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER7,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER8,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER9,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER10,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER11,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER12,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER13,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER14,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER15,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER16,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER17,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER18,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER19,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER20,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER21,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER22,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PROJ_ANGLE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_TYPE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.ALLOC_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.LAST_MOD,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.VERSION_CREATION,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.VERSION_NOW,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_SIZE_A,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_SIZE_C,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_SIZE_U,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.CHECK_SUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.ARCHIVE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.COMPLETE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_BITS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SRF_COIL_INTENSITY,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_X_LOC_R,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_X_LOC_L,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_Y_LOC_A,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_Y_LOC_P,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_Z_LOC_S,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_Z_LOC_I,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_X_THICK,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_Y_THICK,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.SAT_Z_THICK,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.FLOW_AXIS_CONTRAST,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.VELOCITY_CONTRAST,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.THICK_DISCLAIMER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PRESCAN_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CHANGE_BITMAP,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.IMAGE_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.COLLAPSE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER23,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.USER24,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PROJECTION_ALG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PROJECTION_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.X_AXIS_ROT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.Y_AXIS_ROT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.Z_AXIS_ROT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.MIN_PIXEL1,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.MAX_PIXEL1,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.MIN_PIXEL2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.MAX_PIXEL2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.ECHO_TRAIN_LEN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.FRACTION_ECHO,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.PREP_PULSE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.CARDIAC_PHASE_NUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEMRImageInfo.VARIABLE_ECHO,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, 
		   new String(GEImageInfo.REFERENCE_FIELD),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, 
		   new String(GEImageInfo.SUMMARY_FIELD),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT1,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT3,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT4,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT5,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT5,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_STR1,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_MR_IMAGE_INFO_NAME, GEImageInfo.SLOP_STR2,
		   DATATYPE_STRING, true, null);

      // Add the GE CT Image Info child with no children
      addElement(GE_CT_IMAGE_INFO_NAME, GE_IMAGE_INFO_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the GE CT Image Info attributes      
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SUITE_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.UNIQUE_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, new String(GEImageInfo.DISK_ID),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.EXAM_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SERIES_NUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.IMAGE_NUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.ALLOC_DATE_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.ACTUAL_DATE_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SCAN_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLICE_THICK,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.I_MATRIX_X,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.I_MATRIX_Y,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.DISPLAY_FIELD_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.DISPLAY_FIELD_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.DIMENSION_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.DIMENSION_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PIXEL_SIZE_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PIXEL_SIZE_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CONTRAST_IV,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CONTRAST_ORAL,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CONTRAST_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SERIES_RX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.IMAGE_RX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SCREEN_FORMAT,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PLANE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SCAN_SPACING,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.COMPRESSION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SCOUT_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, 
		   new String(GEImageInfo.LOCATION_RAS),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.IMAGE_LOCATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CENTER_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CENTER_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CENTER_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.NORM_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.NORM_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.NORM_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.TOP_LEFT_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.TOP_LEFT_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.TOP_LEFT_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.TOP_RIGHT_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.TOP_RIGHT_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.TOP_RIGHT_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.BOT_RIGHT_R,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.BOT_RIGHT_A,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.BOT_RIGHT_S,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.FOREIGN_IMAGE_REV,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TABLE_START_LOC,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TABLE_END_LOC,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TABLE_SPEED, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TABLE_HEIGHT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.MID_SCAN_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.MID_SCAN_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.KVOLT, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.MAMP, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.GANTRY_TILT, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.AZIMUTH,  
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.GANTRY_VEL,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.GANTRY_FILTER, 
		      DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TRIGGER_ON_POS,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.DEGREE_ROT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.XRAY_ON,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.XRAY_OFF, 
		     DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.NUM_TRIGGERS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.INPUT_VIEWS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.ANGLE_VIEW1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TRIGGER_FREQ,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TRIGGER_SRC,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.FPA_GAIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCAN_TYPE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.OUT_SRC, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.AD_INPUT, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CAL_MODE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CAL_FREQ, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REG_XM, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.AUTO_ZERO,
		       DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.AXIAL_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PHANT_SIZE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PHANT_TYPE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.FILTER_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.RECON_ALG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PERIS_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.ITERBONE_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.STAT_FLAG, 
		     DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.COMPUTE_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SEG_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SEGS_TOTAL, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.ISCAN_DELAY,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCAN_FIELD, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCAN_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.VIEW_START_CHAN,
		      DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.VIEW_COMP_FACTOR,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.OUT_VIEWS,
		      DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.OVER_RANGES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TOTAL_REF_CHAN, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCAN_DATA_SIZE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN0,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN1,
		      DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN3,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN4,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN5, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.POST_PROC_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.XM_PATTERN, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.ROT_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.RAW_DATA_FLAG,
		     DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CT_SCALE_FACTOR, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CT_WATER_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CT_BONE_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BBH_COEF1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BBH_COEF2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BBH_COEF3,
		      DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BBH_NUM_BLEND,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.FIRST_CHAN, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.NUM_CHAN, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CHAN_INCR, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.FIRST_VIEW, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.NUM_VIEW, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.VIEW_INCR, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.WINDOW_RANGE,
		       DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCALE_MIN,
		      DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCALE_MAX, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.DATA_MODE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.QCAL_FILE, 
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.CAL_MOD_FILE,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.WORDS_PER_VIEW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.RL_RAS, 
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.AP_RAS, 
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCOUT_START_RAS,
		      DATATYPE_STRING, true, null); 
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCOUT_END_RAS, 
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCOUT_ANATOM_REF, 
		     DATATYPE_STRING, true, null); 
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PPS_SCALE_WINDOW, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PPS_QCAL_FLAG,  
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PPS_PCAL_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PPS_THETA_FIX, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PPS_BEAM_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SPOT_SIZE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SPOT_POS, 
		      DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.RECON_DATA_SET,  
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.DET_CELL_FIELD, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SCAN_START_TIME,  
		   DATATYPE_DOUBLE, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.GANTRY_DIRECTION, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.ROTOR_SPEED,  
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.TRIG_MODE, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.SI_TILT,  
		     DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.R_RECON_CENTER, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.A_RECON_CENTER, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BACK_PROJ_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.FATQ_EST_FLAG, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.Z_AVG, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.LEFT_REF_AVG, 
		      DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.LEFT_REF_MAX, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.RIGHT_REF_AVG,  
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.RIGHT_REF_MAX, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.ALLOC_KEY,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.LAST_MOD,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.VERSION_CREATION,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.VERSION_NOW,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_SIZE_A,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_SIZE_C,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.PIXEL_DATA_SIZE_U,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.CHECK_SUM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.ARCHIVE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.COMPLETE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BIOPSY_POS, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BIOPSY_T_LOC, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BIOPSY_REF_LOC, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.REF_CHAN, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.BP_COEF,  
		     DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.PSPEED_CORR, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.OVERRANGE_CORR, 
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GECTImageInfo.DYN_Z_ALPHA, 
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, 
		   new String(GEImageInfo.REFERENCE_FIELD),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, 
		   new String(GEImageInfo.SUMMARY_FIELD),
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT1,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT3,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT4,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_INT5,
		   DATATYPE_INTEGER, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_FLOAT5,
		   DATATYPE_FLOAT, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_STR1,
		   DATATYPE_STRING, true, null);
      addAttribute(GE_CT_IMAGE_INFO_NAME, GEImageInfo.SLOP_STR2,
		   DATATYPE_STRING, true, null);
    }
    
    /**
   * Gets an instance of the GEMetadataFormat.
   *
   * @return The single instance of the GEMetadataFormat.
   */
  public static GEMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
