/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge.plugin;

import edu.ucla.loni.ge.GECTImageInfo;
import edu.ucla.loni.ge.GEExamInfo;
import edu.ucla.loni.ge.GEHeader;
import edu.ucla.loni.ge.GEImageInfo;
import edu.ucla.loni.ge.GEMRImageInfo;
import edu.ucla.loni.ge.GESeriesInfo;
import edu.ucla.loni.ge.GESuiteInfo;
import edu.ucla.loni.imageio.Utilities;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for parsing and decoding GE version 5 images.
 *
 * @version 14 June 2006
 */
public class GEImageReader extends ImageReader
{
  /** GE Header decoded from the input source. */
  private GEHeader _geHeader;

  /**
   * Constructs a GEImageReader.
   *
   * @param originatingProvider The Image Reader SPI that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not a
   *                                  GE Image Reader Spi.
   */
  public GEImageReader(ImageReaderSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be a GE Image Reader Spi
      if ( !(originatingProvider instanceof GEImageReaderSpi) ) {
	String msg = "Originating provider must be a GE Image Reader SPI.";
	throw new IllegalArgumentException(msg);
      }
    }
    
  /**
   * Sets the input source to use to the given ImageInputStream or other
   * Object.  The input source must be set before any of the query or read
   * methods are used.  If the input is null, any currently set input source
   * will be removed.
   *
   * @param input The ImageInputStream or other Object to use for future
   *              decoding.
   * @param seekForwardOnly If true, images and metadata may only be read in
   *                        ascending order from this input source.
   * @param ignoreMetadata If true, metadata may be ignored during reads.
   *
   * @throws IllegalArgumentException If the input is not an instance of one of
   *                                  the classes returned by the originating
   *                                  service provider's getInputTypes method,
   *                                  or is not an ImageInputStream.
   * @throws IllegalStateException If the input source is an Input Stream that
   *                               doesn't support marking and seekForwardOnly
   *                               is false.
   */
  public void setInput(Object input, boolean seekForwardOnly,
		       boolean ignoreMetadata)
    {
      super.setInput(input, seekForwardOnly, ignoreMetadata);

      // Remove any decoded GE Header
      _geHeader = null;
    }

  /**
   * Returns the number of images, not including thumbnails, available from the
   * current input source.
   * 
   * @param allowSearch If true, the true number of images will be returned
   *                    even if a search is required.  If false, the reader may
   *                    return -1 without performing the search.
   *
   * @return The number of images or -1 if allowSearch is false and a search
   *         would be required.
   *
   * @throws IOException If an error occurs reading the information from the
   *                     input source.
   */
  public int getNumImages(boolean allowSearch) throws IOException
    {
      return 1; 
    }

  /**
   * Returns the width in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The width of the image in pixels.
   *
   * @throws IOException If an error occurs reading the width information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getWidth(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Width is the I_MATRIX_X attribute of the GE Image Info
      GEHeader geHeader = _getGEHeader();
      GEImageInfo imageInfo = geHeader.getGEImageInfo();
      Object value = imageInfo.getElementValue(GEImageInfo.I_MATRIX_X);

      // Return the number of columns
      return ((Number)value).intValue();
    }

  /**
   * Returns the height in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The height of the image in pixels.
   *
   * @throws IOException If an error occurs reading the height information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getHeight(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Height is the I_MATRIX_Y attribute of the GE Image Info
      GEHeader geHeader = _getGEHeader();
      GEImageInfo imageInfo = geHeader.getGEImageInfo();
      Object value = imageInfo.getElementValue(GEImageInfo.I_MATRIX_Y);

      // Return the number of rows
      return ((Number)value).intValue();
    }

  /**
   * Returns an Iterator containing possible image types to which the given
   * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
   * legal image type will be returned.
   *
   * @param imageIndex The index of the image to be retrieved.
   *
   * @return An Iterator containing at least one ImageTypeSpecifier
   *         representing suggested image types for decoding the current given
   *         image.
   *
   * @throws IOException If an error occurs reading the format information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public Iterator getImageTypes(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);
      
      // Bits per pixel is the SCREEN_FORMAT of the GE Image Info
      GEHeader geHeader = _getGEHeader();
      GEImageInfo imageInfo = geHeader.getGEImageInfo();
      Object value = imageInfo.getElementValue(GEImageInfo.SCREEN_FORMAT);
      short bitsPerPixel = ((Number)value).shortValue();

      // Only support 8 and 16 bit pixels
      if (bitsPerPixel != 8 && bitsPerPixel != 16) {
	String msg = "Unable to recognize the image type.";
	throw new IOException(msg);
      }

      // Get image parameters
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Return the Image Type Specifier
      ArrayList list = new ArrayList(1);
      list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
      return list.iterator();
    }

  /**
   * Returns an IIOMetadata object representing the metadata associated with
   * the input source as a whole (i.e., not associated with any particular
   * image), or null if the reader does not support reading metadata, is set
   * to ignore metadata, or if no metadata is available.
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   */
  public IIOMetadata getStreamMetadata() throws IOException
    {
      return new GEMetadata( _getGEHeader() );
    }

  /**
   * Returns an IIOMetadata object containing metadata associated with the
   * given image, or null if the reader does not support reading metadata, is
   * set to ignore metadata, or if no metadata is available.
   *
   * @param imageIndex Index of the image whose metadata is to be retrieved. 
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
      // Not applicable
      return null;
    }

  /**
   * Reads the image indexed by imageIndex and returns it as a complete
   * BufferedImage.  The supplied ImageReadParam is ignored.
   *
   * @param imageIndex The index of the image to be retrieved.
   * @param param An ImageReadParam used to control the reading process, or
   *              null.
   *
   * @return The desired portion of the image as a BufferedImage.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public BufferedImage read(int imageIndex, ImageReadParam param)
    throws IOException
    {
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Update the Listeners
      processImageStarted(imageIndex);

      // Compute the size of the image in bytes
      ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
      int dataType = spec.getSampleModel().getDataType();
      int imageSize = width*height;
      if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }

      // Set the Image Input Stream position to the image beginning
      ImageInputStream imageStream = _getInputStream();
      imageStream.reset();
      imageStream.mark();

      // Return the raw image data if required
      if ( Utilities.returnRawBytes(param) ) {
	BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	imageStream.read( ((DataBufferByte)db).getData() );
	return bufferedImage;
      }

      // Create a Buffered Image for the image data
      Iterator iter = getImageTypes(0);
      BufferedImage bufferedImage = getDestination(null, iter, width, height);
      DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

      // Read the image data into an array
      byte[] srcBuffer = new byte[imageSize];
      imageStream.readFully(srcBuffer);

      // 8-bit gray image pixel data
      WritableRaster dstRaster = bufferedImage.getRaster();
      if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
      }

      // 16-bit gray image pixel data
      else {
	bufferedImage = Utilities.getGrayUshortImage(srcBuffer, dstRaster);
      }

      // Update the Listeners
      if ( abortRequested() ) { processReadAborted(); }
      else { processImageComplete(); }

      // Return the requested image
      return bufferedImage;
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageReader.  Otherwise, the reader may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the input stream
      try { if (input != null) { ((ImageInputStream)input).close(); } }
      catch (Exception e) {}

      input = null;
    }

  /**
   * Checks whether or not the specified image index is valid.
   *
   * @param imageIndex The index of the image to check.
   *
   * @throws IOException If an error occurs reading the information from the
   *                     input source.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  private void _checkImageIndex(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      if ( imageIndex >= getNumImages(false) ) {
	String msg = "Image index of " + imageIndex + " >= the total number " +
	             "of images (" + getNumImages(false) + ")";
	throw new IndexOutOfBoundsException(msg);
      }
    }

  /**
   * Gets the input stream.
   *
   * @return Image Input Stream to read data from.
   *
   * @throws IllegalStateException If the input has not been set.
   */
  private ImageInputStream _getInputStream()
    {
      // No input has been set
      if (input == null) {
	String msg = "No input has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Input Stream
      return (ImageInputStream)input;
    }

  /**
   * Reads the GE Header from the input source.  If the GE Header has
   * already been decoded, no additional read is performed.
   *
   * @return GE Header decoded from the input source.
   * 
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   */
  private GEHeader _getGEHeader() throws IOException
    {
      // Decode the GE Header if needed
      if (_geHeader == null) {
	ImageInputStream imageStream = _getInputStream();

	// Read in the bytes before the GE subheader offsets and sizes
	byte[] preHeaderBytes = new byte[124];
	imageStream.readFully(preHeaderBytes);

	// Read in the offset and size bytes for the GE subheaders
	byte[] offsetBytes = new byte[32];
	imageStream.readFully(offsetBytes);
	DataInputStream inputStream
	  = new DataInputStream( new ByteArrayInputStream(offsetBytes) );

	// Get the offsets and sizes for the GE subheaders
	int suiteInfoOffset = inputStream.readInt();
	int suiteInfoSize = inputStream.readInt();
	int examInfoOffset = inputStream.readInt();
	int examInfoSize = inputStream.readInt();
	int seriesInfoOffset = inputStream.readInt();
	int seriesInfoSize = inputStream.readInt();
	int imageInfoOffset = inputStream.readInt();
	int imageInfoSize = inputStream.readInt();

	// Skip to the suite bytes
	byte[] unusedBytes = new byte[suiteInfoOffset - 156];
	imageStream.readFully(unusedBytes);

	// Get the GE Suite Info
	byte[] suiteBytes = new byte[suiteInfoSize];
	imageStream.readFully(suiteBytes);
	GESuiteInfo suiteInfo =
	  _getGESuiteInfo( new ByteArrayInputStream(suiteBytes) );

	// Skip to the exam bytes
	unusedBytes = new byte[examInfoOffset-suiteInfoOffset-suiteInfoSize];
	imageStream.readFully(unusedBytes);

	// Get the GE Exam Info
	byte[] examBytes = new byte[examInfoSize];
	imageStream.readFully(examBytes);
	GEExamInfo examInfo =
	  _getGEExamInfo( new ByteArrayInputStream(examBytes) );

	// Skip to the series bytes
	unusedBytes = new byte[seriesInfoOffset-examInfoOffset-examInfoSize];
	imageStream.readFully(unusedBytes);

	// Get the GE Series Info
	byte[] seriesBytes = new byte[seriesInfoSize];
	imageStream.readFully(seriesBytes);
	GESeriesInfo seriesInfo =
	  _getGESeriesInfo( new ByteArrayInputStream(seriesBytes) );

	// Skip to the image bytes
	unusedBytes = new byte[imageInfoOffset-seriesInfoOffset-
			      seriesInfoSize];
	imageStream.readFully(unusedBytes);

	// Get the GE Image Info
	String modality = (String)examInfo.getElementValue(examInfo.EXAM_TYPE);
	byte[] imageBytes = new byte[imageInfoSize];
	imageStream.readFully(imageBytes);
	GEImageInfo imageInfo =
	  _getGEImageInfo( new ByteArrayInputStream(imageBytes), modality );

	// Skip to the pixel data and mark its location
	unusedBytes = new byte[7904 - imageInfoOffset - imageInfoSize];
	imageStream.readFully(unusedBytes);
	imageStream.mark();

	// Create the GE Header
	_geHeader = new GEHeader(suiteInfo, examInfo, seriesInfo, imageInfo);
      }

      // Return the GE Header
      return _geHeader;
    }

  /**
   * Reads a GE Suite Info from the specified input stream.
   *
   * @param inputStream Input Stream to read from.
   *
   * @return GE Suite Info decoded from the specified input stream.
   * 
   * @throws IOException If an error occurs during reading.
   */
  private GESuiteInfo _getGESuiteInfo(InputStream inputStream)
    throws IOException
    {
      DataInputStream stream = new DataInputStream(inputStream);

      // Define the byte arrays
      byte[] suiteID = new byte[4];
      byte[] productID = new byte[13];
      byte[] versionCr = new byte[2];
      byte[] versionCu = new byte[2];

      // Read the info from the stream
      stream.readFully(suiteID);
      short uniqueFlag = stream.readShort();
      char diskID = (char)stream.readByte();
      stream.readFully(productID);
      stream.readFully(versionCr);
      stream.readFully(versionCu);
      int checkSum = stream.readInt();

      // Return a GE Suite Info
      return new GESuiteInfo(suiteID, uniqueFlag, diskID, productID, versionCr,
			     versionCu, checkSum);
    }

  /**
   * Reads a GE Exam Info from the specified input stream.
   *
   * @param inputStream Input Stream to read from.
   *
   * @return GE Exam Info decoded from the specified input stream.
   * 
   * @throws IOException If an error occurs during reading.
   */
  private GEExamInfo _getGEExamInfo(InputStream inputStream)
    throws IOException
    {
      DataInputStream stream = new DataInputStream(inputStream);

      // Define the byte arrays
      byte[] suiteID = new byte[4];
      byte[] hospitalName = new byte[32];
      byte[] patientID = new byte[12];
      byte[] patientName = new byte[25];
      byte[] patientHistory = new byte[61];
      byte[] reqNumber = new byte[13];
      byte[] refPhysician = new byte[33];
      byte[] diagrad = new byte[33];
      byte[] operator = new byte[4];
      byte[] examDescription = new byte[23];
      byte[] examType = new byte[3];
      byte[] host = new byte[9];
      byte[] allocKey = new byte[13];
      byte[] versionCreation = new byte[2];
      byte[] versionNow = new byte[2];
      byte[] seriesKey = new byte[4];
      byte[] unstSeriesKey = new byte[4];
      byte[] unarchSeriesKey = new byte[4];
      byte[] prosSeriesKey = new byte[4];
      byte[] modelKey = new byte[4];
      byte[] systemID = new byte[16];
      byte[] serviceID = new byte[16];

      // Read the info from the stream
      stream.readFully(suiteID);
      short uniqueFlag = stream.readShort();
      char diskID = (char)stream.readByte();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      short examNumber = stream.readShort();
      stream.readFully(hospitalName);

      // Skip 2 bytes
      stream.readFully( new byte[2] );

      short detector = stream.readShort();
      int numberCells = stream.readInt();
      float zeroCell = stream.readFloat();
      float cellSpace = stream.readFloat();
      float srcToDet = stream.readFloat();
      float srcToIso = stream.readFloat();
      short tubeType = stream.readShort();
      short dasType = stream.readShort();
      short numberDeconKernals = stream.readShort();
      short deconLen = stream.readShort();
      short deconDensity = stream.readShort();
      short deconStepSize = stream.readShort();
      short deconShiftCnt = stream.readShort();
      int magStrength = stream.readInt();
      stream.readFully(patientID);

      // Skip 1 byte
      stream.readFully( new byte[1] );

      stream.readFully(patientName);
      short patientAge = stream.readShort();
      short patientAgeNot = stream.readShort();
      short patientSex = stream.readShort();
      int patientWeight = stream.readInt();
      short trauma = stream.readShort();
      stream.readFully(patientHistory);
      stream.readFully(reqNumber);
      int examDate = stream.readInt();
      stream.readFully(refPhysician);
      stream.readFully(diagrad);
      stream.readFully(operator);
      stream.readFully(examDescription);
      stream.readFully(examType);
      short examFormat = stream.readShort();
      double firstAxialTime = stream.readDouble();
      stream.readFully(host);

      // Skip 1 byte
      stream.readFully( new byte[1] );

      int lastMod = stream.readInt();
      short protocolFlag = stream.readShort();
      stream.readFully(allocKey);
      int updataCnt = stream.readInt();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      stream.readFully(versionCreation);
      stream.readFully(versionNow);
      int checkSum = stream.readInt();
      int completeFlag = stream.readInt();
      int seriesCnt = stream.readInt();
      int numberArchived = stream.readInt();
      int numberSeries = stream.readInt();
      int seriesKeyLen = stream.readInt();
      stream.readFully(seriesKey);
      int numberUnstSeries = stream.readInt();
      int unstSeriesKeyLen = stream.readInt();
      stream.readFully(unstSeriesKey);
      int numberUnarchSeries = stream.readInt();
      int unarchSeriesKeyLen = stream.readInt();
      stream.readFully(unarchSeriesKey);
      int numberProsSeries = stream.readInt();
      int prosSeriesKeyLen = stream.readInt();
      stream.readFully(prosSeriesKey);
      int modelNumber = stream.readInt();
      int modelCnt = stream.readInt();
      int modelKeyLen = stream.readInt();
      stream.readFully(modelKey);
      short patientStatus = stream.readShort();
      stream.readFully(systemID);
      stream.readFully(serviceID);
      int mobleLocation = stream.readInt();

      // Set all the bytes after the first zeroed byte equal to zero
      _zeroArray(suiteID);
      _zeroArray(hospitalName);
      _zeroArray(patientID);
      _zeroArray(patientName);
      _zeroArray(patientHistory);
      _zeroArray(reqNumber);
      _zeroArray(refPhysician);
      _zeroArray(diagrad);
      _zeroArray(operator);
      _zeroArray(examDescription);
      _zeroArray(examType);
      _zeroArray(host);
      _zeroArray(allocKey);
      _zeroArray(versionCreation);
      _zeroArray(versionNow);
      _zeroArray(seriesKey);
      _zeroArray(unstSeriesKey);
      _zeroArray(unarchSeriesKey);
      _zeroArray(prosSeriesKey);
      _zeroArray(modelKey);
      _zeroArray(systemID);
      _zeroArray(serviceID);

      // Return a GE Exam Info
      return new GEExamInfo(suiteID, uniqueFlag, diskID, examNumber,
			    hospitalName, detector, numberCells, zeroCell,
			    cellSpace, srcToDet, srcToIso, tubeType, dasType, 
			    numberDeconKernals, deconLen, deconDensity,
			    deconStepSize, deconShiftCnt, magStrength, 
			    patientID, patientName, patientAge, patientAgeNot,
			    patientSex, patientWeight, trauma, patientHistory,
			    reqNumber, examDate, refPhysician, diagrad,
			    operator, examDescription, examType, examFormat, 
			    firstAxialTime, host, lastMod, protocolFlag, 
			    allocKey, updataCnt, versionCreation, versionNow, 
			    checkSum, completeFlag, seriesCnt, numberArchived, 
			    numberSeries, seriesKeyLen, seriesKey,
			    numberUnstSeries, unstSeriesKeyLen, unstSeriesKey,
			    numberUnarchSeries, unarchSeriesKeyLen, 
			    unarchSeriesKey, numberProsSeries,
			    prosSeriesKeyLen, prosSeriesKey, modelNumber,
			    modelCnt, modelKeyLen, modelKey, patientStatus,
			    systemID, serviceID, mobleLocation);
    }

  /**
   * Reads a GE Series Info from the specified input stream.
   *
   * @param inputStream Input Stream to read from.
   *
   * @return GE Series Info decoded from the specified input stream.
   * 
   * @throws IOException If an error occurs during reading.
   */
  private GESeriesInfo _getGESeriesInfo(InputStream inputStream)
    throws IOException
    {
      DataInputStream stream = new DataInputStream(inputStream);

      // Define the byte arrays
      byte[] suiteID = new byte[4];
      byte[] seriesDescription = new byte[30];
      byte[] primaryRecieverHost = new byte[9];
      byte[] archiverHost = new byte[9];
      byte[] anatomReference = new byte[3];
      byte[] protocolName = new byte[25];
      byte[] allocKey = new byte[13];
      byte[] versionCreated = new byte[2];
      byte[] versionNow = new byte[2];
      byte[] imageKey = new byte[4];
      byte[] unstImageKey = new byte[4];
      byte[] unarchImageKey = new byte[4];

      // Read the info from the stream
      stream.readFully(suiteID);
      short uniqueFlag = stream.readShort();
      char diskID = (char)stream.readByte();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      short examNumber = stream.readShort();
      short seriesNumber = stream.readShort();
      int allocTimeStamp = stream.readInt();
      int actualTimeStamp = stream.readInt();
      stream.readFully(seriesDescription); 
      stream.readFully(primaryRecieverHost);
      stream.readFully(archiverHost);
      short seriesType = stream.readShort();
      short seriesSource = stream.readShort();
      short plane = stream.readShort();
      short scanType = stream.readShort();
      int position = stream.readInt();
      int entry = stream.readInt();
      stream.readFully(anatomReference);

      // Skip 1 byte
      stream.readFully( new byte[1] );

      float horizontalLandmark = stream.readFloat();
      stream.readFully(protocolName);

      // Skip 1 byte
      stream.readFully( new byte[1] );

      short contrast = stream.readShort();
      char startRas = (char)stream.readByte();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      float startLoc = stream.readFloat();
      char endRas = (char)stream.readByte();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      float endLoc = stream.readFloat();
      short pulseSequence = stream.readShort();
      short sortOrder = stream.readShort();
      int landmarkCntr = stream.readInt();
      short numberAquisitions = stream.readShort();
      short baselineStart = stream.readShort();
      short baselineEnd = stream.readShort();
      short enhancedStart = stream.readShort();
      short enhancedEnd = stream.readShort();
      int lastMod = stream.readInt();
      stream.readFully(allocKey);

      // Skip 1 byte
      stream.readFully( new byte[1] );

      int headerUpdateCnt = stream.readInt();
      stream.readFully(versionCreated);
      stream.readFully(versionNow);
      float storedPixelSize = stream.readFloat();
      float compPixelDataSize = stream.readFloat();
      float uncompPixelDataSize = stream.readFloat();
      int checkSum = stream.readInt(); 
      int complete = stream.readInt();
      int numArchived = stream.readInt();
      int lastImage = stream.readInt();
      int numberImages = stream.readInt();
      int imageKeyLen = stream.readInt();
      stream.readFully(imageKey);
      int numberUnstImages = stream.readInt();
      int unstImageKeyLen = stream.readInt();
      stream.readFully(unstImageKey);
      int numberUnarchImages = stream.readInt();
      int unarchImageKeyLen = stream.readInt(); 
      stream.readFully(unarchImageKey);
      float echo1Alpha = stream.readFloat(); 
      float echo1Beta = stream.readFloat();
      short echo1Window = stream.readShort();
      short echo1Level = stream.readShort();
      float echo2Alpha = stream.readFloat(); 
      float echo2Beta = stream.readFloat();
      short echo2Window = stream.readShort();
      short echo2Level = stream.readShort();
      float echo3Alpha = stream.readFloat(); 
      float echo3Beta = stream.readFloat();
      short echo3Window = stream.readShort();
      short echo3Level = stream.readShort();
      float echo4Alpha = stream.readFloat(); 
      float echo4Beta = stream.readFloat();  
      short echo4Window = stream.readShort();
      short echo4Level = stream.readShort();
      float echo5Alpha = stream.readFloat();
      float echo5Beta = stream.readFloat();
      short echo5Window = stream.readShort();
      short echo5Level = stream.readShort();
      float echo6Alpha = stream.readFloat();
      float echo6Beta = stream.readFloat();
      short echo6Window = stream.readShort();
      short echo6Level = stream.readShort();
      float echo7Alpha = stream.readFloat();
      float echo7Beta = stream.readFloat();
      short echo7Window = stream.readShort(); 
      short echo7Level = stream.readShort();
      float echo8Alpha = stream.readFloat();
      float echo8Beta = stream.readFloat();
      short echo8Window = stream.readShort();
      short echo8Level = stream.readShort();

      // Set all the bytes after the first zeroed byte equal to zero
      _zeroArray(suiteID);
      _zeroArray(seriesDescription);
      _zeroArray(primaryRecieverHost);
      _zeroArray(archiverHost);
      _zeroArray(anatomReference);
      _zeroArray(protocolName);
      _zeroArray(allocKey);
      _zeroArray(versionCreated);
      _zeroArray(versionNow);
      _zeroArray(imageKey);
      _zeroArray(unstImageKey);
      _zeroArray(unarchImageKey);

      // Return a GE Series Info
      return new GESeriesInfo(suiteID, uniqueFlag, diskID, examNumber,
			      seriesNumber, allocTimeStamp, actualTimeStamp, 
			      seriesDescription, primaryRecieverHost,
			      archiverHost, seriesType, seriesSource, plane, 
			      scanType, position, entry, anatomReference,
			      horizontalLandmark, protocolName, contrast, 
			      startRas, startLoc, endRas, endLoc,
			      pulseSequence, sortOrder, landmarkCntr,
			      numberAquisitions, baselineStart, baselineEnd, 
			      enhancedStart, enhancedEnd, lastMod, allocKey, 
			      headerUpdateCnt, versionCreated, versionNow,
			      storedPixelSize, compPixelDataSize,
			      uncompPixelDataSize, checkSum, complete,
			      numArchived, lastImage, numberImages,
			      imageKeyLen, imageKey, numberUnstImages,
			      unstImageKeyLen, unstImageKey,
			      numberUnarchImages, unarchImageKeyLen,
			      unarchImageKey, echo1Alpha, echo1Beta,
			      echo1Window, echo1Level, echo2Alpha, echo2Beta, 
			      echo2Window, echo2Level, echo3Alpha, echo3Beta, 
			      echo3Window, echo3Level, echo4Alpha, echo4Beta, 
			      echo4Window, echo4Level, echo5Alpha, echo5Beta, 
			      echo5Window, echo5Level, echo6Alpha, echo6Beta, 
			      echo6Window, echo6Level, echo7Alpha, echo7Beta,
			      echo7Window, echo7Level, echo8Alpha, echo8Beta, 
			      echo8Window, echo8Level);
    }

  /**
   * Reads a GE Image Info from the specified input stream.
   *
   * @param inputStream Input Stream to read from.
   * @param modality Modality of the image.
   *
   * @return GE Image Info decoded from the specified input stream.
   * 
   * @throws IOException If an error occurs during reading.
   */
  private GEImageInfo _getGEImageInfo(InputStream inputStream, String modality)
    throws IOException
    {
      DataInputStream stream = new DataInputStream(inputStream);

      // Define the byte arrays
      byte[] suiteID = new byte[4];
      byte[] pixelDataID = new byte[14];
      byte[] contrastIV = new byte[17];
      byte[] contrastOral = new byte[17];
      byte[] foreignImageRev = new byte[4];

      // Read the info from the stream
      stream.readFully(suiteID);
      short uniqueFlag = stream.readShort();
      char diskID = (char)stream.readByte();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      short examNumber = stream.readShort();
      short seriesNum = stream.readShort();
      short imageNum = stream.readShort();
      int allocDateTime = stream.readInt();
      int actualDateTime = stream.readInt();
      float scanTime = stream.readFloat();
      float sliceThick = stream.readFloat();
      short iMatrixX = stream.readShort();
      short iMatrixY = stream.readShort();
      float displayFieldX = stream.readFloat();
      float displayFieldY = stream.readFloat();
      float dimensionX = stream.readFloat();
      float dimensionY = stream.readFloat();
      float pixelSizeX = stream.readFloat();
      float pixelSizeY = stream.readFloat();
      stream.readFully(pixelDataID);
      stream.readFully(contrastIV);
      stream.readFully(contrastOral);
      short contrastMode = stream.readShort();
      short seriesRX = stream.readShort();
      short imageRX = stream.readShort();
      short screenFormat = stream.readShort();
      short plane = stream.readShort();
      float scanSpacing = stream.readFloat();
      short compression = stream.readShort();
      short scoutType = stream.readShort();
      char locationRAS = (char)stream.readByte();

      // Skip 1 byte
      stream.readFully( new byte[1] );

      float imageLocation = stream.readFloat();
      float centerR = stream.readFloat();
      float centerA = stream.readFloat();
      float centerS = stream.readFloat();
      float normR = stream.readFloat();
      float normA = stream.readFloat();
      float normS = stream.readFloat();
      float topLeftR = stream.readFloat();
      float topLeftA = stream.readFloat();
      float topLeftS = stream.readFloat();
      float topRightR = stream.readFloat();
      float topRightA = stream.readFloat();
      float topRightS = stream.readFloat();
      float botRightR = stream.readFloat();
      float botRightA = stream.readFloat();
      float botRightS = stream.readFloat();
      stream.readFully(foreignImageRev);

      // Set all the bytes after the first zeroed byte equal to zero
      _zeroArray(suiteID);
      _zeroArray(pixelDataID);
      _zeroArray(contrastIV);
      _zeroArray(contrastOral);
      _zeroArray(foreignImageRev);

      // Detect an MR image
      if ( modality.substring(0, 2).equals("MR") ) {

	// Define the byte arrays
	byte[] pulseSeqName = new byte[33];
	byte[] pSDName = new byte[13];
	byte[] coilName = new byte[17];
	byte[] allocKey = new byte[13];
	byte[] versionCreation = new byte[2];
	byte[] versionNow = new byte[2];
	byte[] projectionName = new byte[13];
	byte[] slopStr1 = new byte[16];
	byte[] slopStr2 = new byte[16];

	// Read the info from the stream
	int repTime = stream.readInt();
	int invTime = stream.readInt();
	int echoTime = stream.readInt();
	int echoTime2 = stream.readInt();
	short numEcho = stream.readShort();
	short echoNumber = stream.readShort();
	float tableDelta = stream.readFloat();
	float numExcitations = stream.readFloat();
	short continuous = stream.readShort();
	short heartRate = stream.readShort();
	int timeDelay = stream.readInt();
	float averageSAR = stream.readFloat();
	float peakSAR = stream.readFloat();
	short monitorSAR = stream.readShort();
	short triggerWindow = stream.readShort();
	float cardRepTime = stream.readFloat();
	short imagesPerCycle = stream.readShort();
	short transmitGain = stream.readShort();
	short recvGainAnalog = stream.readShort();
	short recvGainDigital = stream.readShort();
	short flipAngle = stream.readShort();
	int minDelay = stream.readInt();
	short cardiacPhase = stream.readShort();
	short swapPhaseFreq = stream.readShort();
	short pauseInterval = stream.readShort();
	float pauseTime = stream.readFloat();
	int obliquePlane = stream.readInt();
	int sliceOff = stream.readInt();
	int transmitFreq = stream.readInt();
	int autoCenterFreq = stream.readInt();
	short autoTransGain = stream.readShort();
	short prescanR1Analog = stream.readShort();
	short prescanR2Digital = stream.readShort();
	int userBitmap = stream.readInt();
	short centerFreq = stream.readShort();
	short imagingMode = stream.readShort();
	int imagingOptions = stream.readInt();
	short pulseSeq = stream.readShort();
	short pulseSeqMode = stream.readShort();
	stream.readFully(pulseSeqName);

	// Skip 1 byte
	stream.readFully( new byte[1] );

	int pSDDateTime = stream.readInt();
	stream.readFully(pSDName);

	// Skip 1 byte
	stream.readFully( new byte[1] );

	short coilType = stream.readShort();
	stream.readFully(coilName);

	// Skip 1 byte
	stream.readFully( new byte[1] );

	short srfCoilType = stream.readShort();
	short srfCoilExtr = stream.readShort();
	int rawRunNumber = stream.readInt();
	int calFieldStrength = stream.readInt();
	short suppTech = stream.readShort();
	float varBandwidth = stream.readFloat();
	short numSliceInScan = stream.readShort();
	short graphicPrescibed = stream.readShort();
	int interDelay = stream.readInt();
	float user0 = stream.readFloat();
	float user1 = stream.readFloat();
	float user2 = stream.readFloat();
	float user3 = stream.readFloat();
	float user4 = stream.readFloat();
	float user5 = stream.readFloat();
	float user6 = stream.readFloat();
	float user7 = stream.readFloat();
	float user8 = stream.readFloat();
	float user9 = stream.readFloat();
	float user10 = stream.readFloat();
	float user11 = stream.readFloat();
	float user12 = stream.readFloat();
	float user13 = stream.readFloat();
	float user14 = stream.readFloat();
	float user15 = stream.readFloat();
	float user16 = stream.readFloat();
	float user17 = stream.readFloat();
	float user18 = stream.readFloat();
	float user19 = stream.readFloat();
	float user20 = stream.readFloat();
	float user21 = stream.readFloat();
	float user22 = stream.readFloat();
	float projAngle = stream.readFloat();
	float sATType = stream.readFloat();
	stream.readFully(allocKey);

	// Skip 1 byte
	stream.readFully( new byte[1] );

	int lastMod = stream.readInt();
	stream.readFully(versionCreation);
	stream.readFully(versionNow);
	int pixelDataSizeA = stream.readInt();
	int pixelDataSizeC = stream.readInt();
	int pixelDataSizeU = stream.readInt();
	int checkSum = stream.readInt();
	int archive = stream.readInt();
	int complete = stream.readInt();
	short sATBits = stream.readShort();
	short srfCoilIntensity = stream.readShort();
	short satXlocR = stream.readShort();
	short satXlocL = stream.readShort();
	short satYlocA = stream.readShort();
	short satYlocP = stream.readShort();
	short satZlocS = stream.readShort();
	short satZlocI = stream.readShort();
	short satXThick = stream.readShort();
	short satYThick = stream.readShort();
	short satZThick = stream.readShort();
	short flowAxisContrast = stream.readShort();
	short velocityContrast = stream.readShort();
	short thickDisclaimer = stream.readShort();
	short prescanFlag = stream.readShort();
	short changeBitmap = stream.readShort();
	short imageType = stream.readShort();
	short collapse = stream.readShort();
	float user23 = stream.readFloat();
	float user24 = stream.readFloat();
	short projectionAlg = stream.readShort();
	stream.readFully(projectionName);
	float xAxisRot = stream.readFloat();
	float yAxisRot = stream.readFloat();
	float zAxisRot = stream.readFloat();
	int minPixel1 = stream.readInt();
	int maxPixel1 = stream.readInt();
	int minPixel2 = stream.readInt();
	int maxPixel2 = stream.readInt();
	short echoTrainLen = stream.readShort();
	short fractionEcho = stream.readShort();
	short prepPulse = stream.readShort();
	short cardiacPhaseNum = stream.readShort();
	short variableEcho = stream.readShort();
	char referenceField = (char)stream.readByte();
	char summaryField = (char)stream.readByte();
	short window = stream.readShort();
	short level = stream.readShort();
	int slopInt1 = stream.readInt();
	int slopInt2 = stream.readInt();
	int slopInt3 = stream.readInt();
	int slopInt4 = stream.readInt();
	int slopInt5 = stream.readInt();
	float slopFloat1 = stream.readFloat();
	float slopFloat2 = stream.readFloat();
	float slopFloat3 = stream.readFloat();
	float slopFloat4 = stream.readFloat();
	float slopFloat5 = stream.readFloat();
	stream.readFully(slopStr1);
	stream.readFully(slopStr2);

	// Set all the bytes after the first zeroed byte equal to zero
	_zeroArray(pSDName);
	_zeroArray(coilName);
	_zeroArray(allocKey);
	_zeroArray(versionCreation);
	_zeroArray(versionNow);
	_zeroArray(projectionName);
	_zeroArray(slopStr1);
	_zeroArray(slopStr2);

	// Return a GE Series Info
	return new GEMRImageInfo(suiteID, uniqueFlag, diskID, examNumber, 
				 seriesNum, imageNum, allocDateTime,
				 actualDateTime, scanTime, sliceThick,
				 iMatrixX, iMatrixY, displayFieldX,
				 displayFieldY, dimensionX, dimensionY,
				 pixelSizeX, pixelSizeY, pixelDataID,
				 contrastIV, contrastOral, contrastMode,
				 seriesRX, imageRX, screenFormat, 
				 plane, scanSpacing, compression, scoutType, 
				 locationRAS, imageLocation, centerR, centerA, 
				 centerS, normR, normA, normS, topLeftR,
				 topLeftA, topLeftS, topRightR, topRightA,
				 topRightS, botRightR, botRightA, botRightS, 
				 foreignImageRev, repTime, invTime, echoTime, 
				 echoTime2, numEcho, echoNumber, tableDelta, 
				 numExcitations, continuous, heartRate,
				 timeDelay, averageSAR, peakSAR, monitorSAR,
				 triggerWindow, cardRepTime, imagesPerCycle, 
				 transmitGain, recvGainAnalog, recvGainDigital,
				 flipAngle, minDelay, cardiacPhase,
				 swapPhaseFreq, pauseInterval, pauseTime,
				 obliquePlane, sliceOff, transmitFreq,
				 autoCenterFreq, autoTransGain,
				 prescanR1Analog, prescanR2Digital, userBitmap,
				 centerFreq, imagingMode, imagingOptions,
				 pulseSeq, pulseSeqMode, pulseSeqName,
				 pSDDateTime, pSDName, coilType, coilName,
				 srfCoilType, srfCoilExtr, rawRunNumber,
				 calFieldStrength, suppTech, varBandwidth,
				 numSliceInScan, graphicPrescibed, interDelay,
				 user0, user1, user2, user3, user4, user5,
				 user6, user7, user8, user9, user10, user11,
				 user12, user13, user14, user15, user16,
				 user17, user18, user19, user20, 
				 user21, user22, projAngle, sATType, allocKey,
				 lastMod, versionCreation, versionNow,
				 pixelDataSizeA, pixelDataSizeC,
				 pixelDataSizeU, checkSum, archive, complete,
				 sATBits, srfCoilIntensity, satXlocR, satXlocL,
				 satYlocA, satYlocP, satZlocS, satZlocI,
				 satXThick, satYThick, satZThick,
				 flowAxisContrast, velocityContrast,
				 thickDisclaimer, prescanFlag,
				 changeBitmap, imageType, collapse, user23,
				 user24, projectionAlg,  projectionName,
				 xAxisRot, yAxisRot, zAxisRot, minPixel1,
				 maxPixel1, minPixel2, maxPixel2, echoTrainLen,
				 fractionEcho, prepPulse, cardiacPhaseNum, 
				 variableEcho, referenceField, summaryField,
				 window, level, slopInt1, slopInt2, slopInt3,
				 slopInt4, slopInt5, slopFloat1, slopFloat2, 
				 slopFloat3, slopFloat4, slopFloat5,  slopStr1,
				 slopStr2);
      }

      else if ( modality.substring(0, 2).equals("CT") ) {

	// Define the byte arrays
	byte[] allocKey = new byte[13];
	byte[] versionCreation = new byte[2];
	byte[] versionNow = new byte[2];
	byte[] qcalFile = new byte[13];
	byte[] calModFile = new byte[13];
	byte[] scoutAnatomRef = new byte[3];
	byte[] slopStr1 = new byte[16];
	byte[] slopStr2 = new byte[16];

	// Read the info from the stream
	float tableStartLoc = stream.readFloat();
	float tableEndLoc = stream.readFloat();
	float tableSpeed = stream.readFloat();
	float tableHeight = stream.readFloat();
	float midScanTime = stream.readFloat();
	short midScanFlag = stream.readShort();
	int kvolt = stream.readInt();
	int mamp = stream.readInt();
	float gantryTilt = stream.readFloat();
	int azimuth = stream.readInt();
	float gantryVel = stream.readFloat();
	int gantryFilter = stream.readInt();
	float triggerOnPos = stream.readFloat();
	float degreeRot = stream.readFloat();
	float xrayOn = stream.readFloat();
	float xrayOff = stream.readFloat();
	int numTriggers = stream.readInt();
	short inputViews = stream.readShort();
	float angleView1 = stream.readFloat();
	float triggerFreq = stream.readFloat();
	int triggerSrc = stream.readInt();
	int fpaGain = stream.readInt();
	int scanType = stream.readInt();
	int outSrc = stream.readInt();
	int adInput = stream.readInt();
	int calMode = stream.readInt();
	int calFreq = stream.readInt();
	int regXM = stream.readInt();
	int autoZero = stream.readInt();
	short axialType = stream.readShort();
	short phantSize = stream.readShort();
	short phantType = stream.readShort();
	short filterType = stream.readShort();
	short reconAlg = stream.readShort();
	short perisFlag = stream.readShort();
	short iterboneFlag = stream.readShort();
	short statFlag = stream.readShort();
	short computeType = stream.readShort();
	short segNumber = stream.readShort();
	short segsTotal = stream.readShort();
	float iscanDelay = stream.readFloat();
	float scanField = stream.readFloat();
	short scanNumber = stream.readShort();
	short viewStartChan = stream.readShort();
	short viewCompFactor = stream.readShort();
	short outViews = stream.readShort();
	short overRanges = stream.readShort();
	short totalRefChan = stream.readShort();
	int scanDataSize = stream.readInt();
	short refChan0 = stream.readShort();
	short refChan1 = stream.readShort();
	short refChan2 = stream.readShort();
	short refChan3 = stream.readShort();
	short refChan4 = stream.readShort();
	short refChan5 = stream.readShort();
	short postProcFlag = stream.readShort();
	int xmPattern = stream.readInt();	   
	short rotType = stream.readShort();
	short rawDataFlag = stream.readShort();
	float ctScaleFactor = stream.readFloat();
	short ctWaterNumber = stream.readShort();
	short ctBoneNumber = stream.readShort();
	float bbhCoef1 = stream.readFloat();
	float bbhCoef2 = stream.readFloat();
	float bbhCoef3 = stream.readFloat();
	short bbhNumBlend = stream.readShort();
	int firstChan = stream.readInt();
	int numChan = stream.readInt();
	int chanIncr = stream.readInt();
	int firstView = stream.readInt();
	int numView = stream.readInt();
	int viewIncr = stream.readInt();
	int windowRange = stream.readInt();
	float scaleMin = stream.readFloat();
	float scaleMax = stream.readFloat();
	int dataMode = stream.readInt();
	stream.readFully(qcalFile);
	stream.readFully(calModFile);
	short wordsPerView = stream.readShort();
	char rlRAS = (char)stream.readByte();
	char apRAS = (char)stream.readByte();
	char scoutStartRAS = (char)stream.readByte();
	char scoutEndRAS = (char)stream.readByte();
	stream.readFully(scoutAnatomRef);

	// Skip 1 byte
	stream.readFully( new byte[1] );

	short ppsScaleWindow = stream.readShort();
	short ppsQcalFlag = stream.readShort();
	short ppsPcalFlag = stream.readShort();
	short ppsThetaFix = stream.readShort();
	short ppsBeamFlag = stream.readShort();
	short spotSize = stream.readShort();
	short spotPos = stream.readShort();
	short reconDataSet = stream.readShort();
	short detCellField = stream.readShort();
	double scanStartTime = stream.readDouble();
	short gantryDirection = stream.readShort();
	short rotorSpeed = stream.readShort();
	short trigMode = stream.readShort();
	float siTilt = stream.readFloat();
	float rReconCenter = stream.readFloat();
	float aReconCenter = stream.readFloat();
	short backProjFlag = stream.readShort();
	short fatqEstFlag = stream.readShort();
	float zAvg = stream.readFloat();
	float leftRefAvg = stream.readFloat();
	float leftRefMax = stream.readFloat();
	float rightRefAvg = stream.readFloat();
	float rightRefMax = stream.readFloat();
	stream.readFully(allocKey);

	// Skip 1 byte
	stream.readFully( new byte[1] );

	int lastMod = stream.readInt();
	stream.readFully(versionCreation);
	stream.readFully(versionNow);
	int pixelDataSizeA = stream.readInt();
	int pixelDataSizeC = stream.readInt();
	int pixelDataSizeU = stream.readInt();
	int checkSum = stream.readInt();
	int archive = stream.readInt();
	int complete = stream.readInt();
	short biopsyPos = stream.readShort();
	float boipsyTLoc = stream.readFloat();
	float biopsyRefLoc = stream.readFloat();
	short refChan = stream.readShort();
	float bpCoef = stream.readFloat();
	short pspeedCorr = stream.readShort();
	short overrangeCorr = stream.readShort();
	float dynZAlpha = stream.readFloat();
	char referenceField = (char)stream.readByte();
	char summaryField = (char)stream.readByte();
	short window = stream.readShort();
	short level = stream.readShort();
	int slopInt1 = stream.readInt();
	int slopInt2 = stream.readInt();
	int slopInt3 = stream.readInt();
	int slopInt4 = stream.readInt();
	int slopInt5 = stream.readInt();
	float slopFloat1 = stream.readFloat();
	float slopFloat2 = stream.readFloat();
	float slopFloat3 = stream.readFloat();
	float slopFloat4 = stream.readFloat();
	float slopFloat5 = stream.readFloat();
	stream.readFully(slopStr1);
	stream.readFully(slopStr2);

	// Set all the bytes after the first zeroed byte equal to zero
	_zeroArray(allocKey);
	_zeroArray(versionCreation);
	_zeroArray(versionNow);
	_zeroArray(qcalFile);
	_zeroArray(calModFile);
	_zeroArray(scoutAnatomRef);
	_zeroArray(slopStr1);
	_zeroArray(slopStr2);

	// Return a GE Series Info
	new GECTImageInfo(suiteID, uniqueFlag, diskID, examNumber, seriesNum,
			  imageNum, allocDateTime, actualDateTime, scanTime,
			  sliceThick, iMatrixX, iMatrixY, displayFieldX, 
			  displayFieldY, dimensionX, dimensionY, pixelSizeX,
			  pixelSizeY, pixelDataID, contrastIV, contrastOral, 
			  contrastMode, seriesRX, imageRX, screenFormat, 
			  plane, scanSpacing, compression, scoutType, 
			  locationRAS, imageLocation, centerR, centerA,
			  centerS, normR, normA, normS, topLeftR, topLeftA, 
			  topLeftS, topRightR, topRightA, topRightS, 
			  botRightR, botRightA, botRightS, foreignImageRev,
			  tableStartLoc, tableEndLoc, tableSpeed, tableHeight, 
			  midScanTime, midScanFlag, kvolt, mamp, gantryTilt,
			  azimuth, gantryVel, gantryFilter, triggerOnPos,
			  degreeRot, xrayOn, xrayOff, numTriggers, inputViews,
			  angleView1, triggerFreq, triggerSrc, fpaGain, 
			  scanType, outSrc, adInput, calMode, calFreq, regXM, 
			  autoZero, axialType, phantSize, phantType,
			  filterType, reconAlg, perisFlag, iterboneFlag,
			  statFlag, computeType, segNumber, segsTotal,
			  iscanDelay, scanField, scanNumber, viewStartChan,
			  viewCompFactor, outViews, overRanges, totalRefChan,
			  scanDataSize, refChan0, refChan1, refChan2, refChan3,
			  refChan4, refChan5, postProcFlag, xmPattern, rotType,
			  rawDataFlag, ctScaleFactor, ctWaterNumber,
			  ctBoneNumber, bbhCoef1, bbhCoef2, bbhCoef3, 
			  bbhNumBlend, firstChan, numChan, chanIncr, firstView,
			  numView, viewIncr, windowRange, scaleMin, scaleMax,
			  dataMode, qcalFile, calModFile, wordsPerView, rlRAS,
			  apRAS, scoutStartRAS, scoutEndRAS, scoutAnatomRef,
			  ppsScaleWindow, ppsQcalFlag, ppsPcalFlag,
			  ppsThetaFix, ppsBeamFlag, spotSize, spotPos,
			  reconDataSet, detCellField, scanStartTime,
			  gantryDirection,rotorSpeed, trigMode, siTilt, 
			  rReconCenter, aReconCenter, backProjFlag,
			  fatqEstFlag, zAvg, leftRefAvg, leftRefMax,
			  rightRefAvg, rightRefMax, allocKey, lastMod,
			  versionCreation, versionNow, pixelDataSizeA,
			  pixelDataSizeC, pixelDataSizeU, checkSum, archive, 
			  complete, biopsyPos, boipsyTLoc, biopsyRefLoc,
			  refChan, bpCoef, pspeedCorr, overrangeCorr, 
			  dynZAlpha, referenceField, summaryField, window,
			  level, slopInt1, slopInt2, slopInt3, slopInt4,
			  slopInt5, slopFloat1, slopFloat2, slopFloat3,
			  slopFloat4, slopFloat5,  slopStr1, slopStr2);
      }

      // Unknown image modality
      throw new IllegalArgumentException("Unknown image modality:  " +
					 modality);
    }

  /**
   * Sets all the bytes after the first zeroed byte equal to zero.
   *
   * @param byteArray Array of bytes to alter.
   */
  private void _zeroArray(byte[] byteArray)
    {
      boolean startZeroing = false;
      for (int i = 0; i < byteArray.length; i++) {
	
	// Detect the first zero
	if (byteArray[i] == 0) { startZeroing = true; }

	// Zero the array element if required
	if (startZeroing) { byteArray[i] = (byte)0; }
      }
    }
}
