/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

/**
 * GE Subheader that describes the MR Image Info.
 *
 * @version 15 July 2003
 */
public class GEMRImageInfo extends GEImageInfo
{   
    /** ID of the Pulse repetition time(usec). */
    public static final String REP_TIME = "repTime";
    
    /** ID of the Pulse inversion time(usec). */
    public static final String INV_TIME = "invTime";
    
    /** ID of the Pulse echo time(usec). */
    public static final String ECHO_TIME = "echoTime";
    
    /** ID of the Second echo echo (usec). */
    public static final String ECHO_TIME2 = "echoTime2";
    
    /** ID of the Number of echoes. */
    public static final String NUM_ECHO = "numEcho";
    
    /** ID of the Echo Number. */
    public static final String ECHO_NUMBER = "echoNumber";
    
    /** ID of the Table Delta. */
    public static final String TABLE_DELTA = "tableDelta";
    
    /** ID of the Number of Excitations. */
    public static final String NUM_EXCITATIONS = "numExcitations";
    
    /** ID of the Continuous Slices Flag. */
    public static final String CONTINUOUS = "continuous";
    
    /** ID of the Cardiac Heart Rate (bpm). */
    public static final String HEART_RATE = "heartRate";
    
    /** ID of the Delay time after trigger (usec). */
    public static final String TIME_DELAY = "timeDelay";
    
    /** ID of the Average SAR. */
    public static final String AVERAGE_SAR = "averageSAR";
    
    /** ID of the Peak SAR. */
    public static final String PEAK_SAR = "peakSAR";
    
    /** ID of the Monitor SAR flag. */
    public static final String MONITOR_SAR = "monitorSAR";
    
    /** ID of the Trigger window (% of R-R interval). */
    public static final String TRIGGER_WINDOW = "triggerWindow";
    
    /** ID of the Cardiac repetition time. */
    public static final String CARD_REP_TIME = "cardRepTime";
    
    /** ID of the Images per cardiac cycle. */
    public static final String IMAGES_PER_CYCLE = "imagesPerCycle";
    
    /** ID of the Actual Transmit Gain (.1 db). */
    public static final String TRANSMIT_GAIN = "transmitGain";
    
    /** ID of the Actual Receive Gain Analog (.1 db). */
    public static final String RECV_GAIN_ANALOG = "recvGainAnalog";
    
    /** ID of the Actual Receive Gain Digital (.1 db). */
    public static final String RECV_GAIN_DIGITAL = "recvGainDigital";
    
    /** ID of the Flip Angle for GRASS scans (deg.). */
    public static final String FLIP_ANGLE = "flipAngle";
    
    /** ID of the Minimum Delay after Trigger (uSec). */
    public static final String MIN_DELAY = "minDelay";
    
    /** ID of the Total Cardiac Phase prescribed. */
    public static final String CARDIAC_PHASE = "cardiacPhase";
    
    /** ID of the Swap Phase/Frequency Axis. */
    public static final String SWAP_PHASE_FREQ = "swapPhaseFreq";
    
    /** ID of the Pause Interval (slices). */
    public static final String PAUSE_INTERVAL = "pauseInterval";
    
    /** ID of the Pause Time. */
    public static final String PAUSE_TIME = "pauseTime";
    
    /** ID of the Oblique Plane. */
    public static final String OBLIQUE_PLANE = "obliquePlane";
    
    /** ID of the Slice Offsets on Freq axis. */
    public static final String SLICE_OFF = "sliceOff";
    
    /** ID of the Center Frequency (0.1 Hz). */
    public static final String TRANSMIT_FREQ = "transmitFreq";
    
    /** ID of the Auto Center Frequency (0.1 Hz). */
    public static final String AUTO_CENTER_FREQ = "autoCenterFreq";
    
    /** ID of the Auto Transmit Gain (0.1 dB). */
    public static final String AUTO_TRANS_GAIN = "autoTransGain";
    
    /** ID of the PreScan R1 - Analog. */
    public static final String PRESCAN_R1_ANALOG = "prescanR1Analog";
    
    /** ID of the PreScan R2 - Digital. */
    public static final String PRESCAN_R2_DIGITAL = "prescanR2Digital";
    
    /** ID of the Bitmap defining user CVs. */
    public static final String USER_BITMAP = "userBitmap";
    
    /** ID of the Center Frequency Method. */
    public static final String CENTER_FREQ = "centerFreq";
    
    /** ID of the Imaging Mode. */
    public static final String IMAGEING_MODE = "imagingMode";
    
    /** ID of the Imaging Options. */
    public static final String IMAGEING_OPTIONS = "imagingOptions";
    
    /** ID of the Pulse Sequence. */
    public static final String PULSE_SEQ = "pulseSeq";
    
    /** ID of the Pulse Sequence Mode. */
    public static final String PULSE_SEQ_MODE = "pulseSeqMode";
    
    /** ID of the Pulse Sequence Name. */
    public static final String PULSE_SEQ_NAME = "pulseSeqName";
    
    /** ID of the PSD Creation Date and Time. */
    public static final String PSD_DATE_TIME = "PSDDateTime";
    
    /** ID of the PSD name from inside PSD. */
    public static final String PSD_NAME = "PSDName";
    
    /** ID of the Coil Type. */
    public static final String COIL_TYPE = "coilType";
    
    /** ID of the Coil Name. */
    public static final String COIL_NAME = "coilName";
    
    /** ID of the Surface Coil Type. */
    public static final String SRF_COIL_TYPE = "srfCoilType";
    
    /** ID of the Extremity Coil Flag. */
    public static final String SRF_COIL_EXTR = "srfCoilExtr";
    
    /** ID of the RawData Run Number. */
    public static final String RAW_RUN_NUMBER = "rawRunNumber";
    
    /** ID of the Calibrated Field Strength (x10 uGauss). */
    public static final String CAL_FIELD_STRENGTH = "calFieldStrength";
    
    /** ID of the SAT fat/water/none. */
    public static final String SUPP_TECH = "suppTech";
    
    /** ID of the Variable Bandwidth (Hz). */
    public static final String VAR_BANDWIDTH = "varBandwidth";
    
    /** ID of the Number of slices in this scan group. */
    public static final String NUM_SLICE_IN_SCAN = "numSliceInScan";
    
    /** ID of the Graphically prescribed. */
    public static final String GRAPHIC_PRESCRIBED = "graphicPrescibed";
    
    /** ID of the Interimage/interloc delay (uSec). */
    public static final String INTER_DELAY = "interDelay";
    
    /** ID of the User Variable 0. */
    public static final String USER0 = "user0";
    
    /** ID of the User Variable 1. */
    public static final String USER1 = "user1";
    
    /** ID of the User Variable 2. */
    public static final String USER2 = "user2";
    
    /** ID of the User Variable 3. */
    public static final String USER3 = "user3";
    
    /** ID of the User Variable 4. */
    public static final String USER4 = "user4";
    
    /** ID of the User Variable 5. */
    public static final String USER5 = "user5";
    
    /** ID of the User Variable 6. */
    public static final String USER6 = "user6";
    
    /** ID of the User Variable 7. */
    public static final String USER7 = "user7";
    
    /** ID of the User Variable 8. */
    public static final String USER8 = "user8";
    
    /** ID of the User Variable 9. */
    public static final String USER9 = "user9";
    
    /** ID of the User Variable 10. */
    public static final String USER10 = "user10";
    
    /** ID of the User Variable 11. */
    public static final String USER11 = "user11";
    
    /** ID of the User Variable 12. */
    public static final String USER12 = "user12";
    
    /** ID of the User Variable 13. */
    public static final String USER13 = "user13";
    
    /** ID of the User Variable 14. */
    public static final String USER14 = "user14";
    
    /** ID of the User Variable 15. */
    public static final String USER15 = "user15";
    
    /** ID of the User Variable 16. */
    public static final String USER16 = "user16";
    
    /** ID of the User Variable 17. */
    public static final String USER17 = "user17";
    
    /** ID of the User Variable 18. */
    public static final String USER18 = "user18";
    
    /** ID of the User Variable 19. */
    public static final String USER19 = "user19";
    
    /** ID of the User Variable 20. */
    public static final String USER20 = "user20";
    
    /** ID of the User Variable 21. */
    public static final String USER21 = "user21";
    
    /** ID of the User Variable 22. */
    public static final String USER22 = "user22";
    
    /** ID of the Projection Angle. */
    public static final String PROJ_ANGLE = "projAngle";
    
    /** ID of the Concat Sat Type Flag. */
    public static final String SAT_TYPE = "SATType";

    /** ID of the Bitmap of SAT selections. */
    public static final String SAT_BITS = "SATBits";
    
    /** ID of the Surface Coil Intensity Correction Flag. */
    public static final String SRF_COIL_INTENSITY = "srfCoilIntensity";
    
    /** ID of the R-side SAT pulse loc rel to lndmrk. */
    public static final String SAT_X_LOC_R = "satXlocR";
    
    /** ID of the L-side SAT pulse loc rel to lndmrk. */
    public static final String SAT_X_LOC_L = "satXlocL";
    
    /** ID of the A-side SAT pulse loc rel to lndmrk. */
    public static final String SAT_Y_LOC_A = "satYlocA";
    
    /** ID of the P-side SAT pulse loc rel to lndmrk. */
    public static final String SAT_Y_LOC_P = "satYlocP";
    
    /** ID of the S-side SAT pulse loc rel to lndmrk. */
    public static final String SAT_Z_LOC_S = "satZlocS";
   
    /** ID of the I-side SAT pulse loc rel to lndmrk. */
    public static final String SAT_Z_LOC_I = "satZlocI";
    
    /** ID of the Thickness of X-axis SAT pulse. */
    public static final String SAT_X_THICK = "satXThick";
    
    /** ID of the Thickness of Y-axis SAT pulse. */
    public static final String SAT_Y_THICK = "satYThick";
    
    /** ID of the Thickness of Z-axis SAT pulse. */
    public static final String SAT_Z_THICK = "satZThick";
    
    /** ID of the Phase contrast flow axis. */
    public static final String FLOW_AXIS_CONTRAST = "flowAxisContrast";
    
    /** ID of the Phase contrast velocity encoding. */
    public static final String VELOCITY_CONTRAST = "velocityContrast";
    
    /** ID of the Slice Thickness. */
    public static final String THICK_DISCLAIMER = "thickDisclaimer";
    
    /** ID of the Auto/Manual Prescan flag. */
    public static final String PRESCAN_FLAG = "prescanFlag";
    
    /** ID of the Bitmap of changed values. */
    public static final String CHANGE_BITMAP = "changeBitmap";
    
    /** ID of the Magnitude, Phase, Imaginary, or Real. */
    public static final String IMAGE_TYPE = "imageType";
    
    /** ID of the Collapse Image. */
    public static final String COLLAPSE = "collapse";
    
    /** ID of the User Variable 23. */
    public static final String USER23 = "user23";
    
    /** ID of the User Variable 24. */
    public static final String USER24 = "user24";
    
    /** ID of the Projection Algorithm. */
    public static final String PROJECTION_ALG = "projectionAlg";
    
    /** ID of the Projection Algorithm Name. */
    public static final String PROJECTION_NAME = "projectionName";
    
    /** ID of the X Axis Rotation. */
    public static final String X_AXIS_ROT = "xAxisRot";
    
    /** ID of the Y Axis Rotation. */
    public static final String Y_AXIS_ROT = "yAxisRot";
    
    /** ID of the Z Axis Rotation. */
    public static final String Z_AXIS_ROT = "zAxisRot";
    
    /** ID of the Lower Range of Pixels 1. */
    public static final String MIN_PIXEL1 = "minPixel1";
    
    /** ID of the Upper Range of Pixels 1. */
    public static final String MAX_PIXEL1 = "maxPixel1";
    
    /** ID of the Lower Range of Pixels 2. */
    public static final String MIN_PIXEL2 = "minPixel2";
    
    /** ID of the Upper Range of Pixels 2. */
    public static final String MAX_PIXEL2 = "maxPixel2";
    
    /** ID of the Echo Train Length for Fast Spin Echo. */
    public static final String ECHO_TRAIN_LEN = "echoTrainLen";
    
    /** ID of the Fractional Echo - Effective TE Flag. */
    public static final String FRACTION_ECHO = "fractionEcho";
    
    /** ID of the Preporatory Pulse Option. */
    public static final String PREP_PULSE = "prepPulse";
    
    /** ID of the Cardiac Phase Number. */
    public static final String CARDIAC_PHASE_NUM = "cardiacPhaseNum";
    
    /** ID of the Variable Echo Flag. */
    public static final String VARIABLE_ECHO = "variableEcho";

    /**
     * Constructs a GEMRImageInfo.
     *
     * @param numberFields Number of fields in image info implementation.
     * @param suiteID suite ID for this image.
     * @param uniqueFlag Unique Flag
     * @param diskID Disk ID for this Image
     * @param examNumber Exam number for this image
     * @param seriesNum Series Number for this image
     * @param imageNum Image Number
     * @param AllocDateTime Allocation Image date/time stamp
     * @param ActualDateTime Actual Image date/time stamp
     * @param scanTime Duration of scan (secs)
     * @param sliceThick Slice Thickness (mm)
     * @param iMatrixX Image matrix size - X
     * @param iMatrixY Image matrix size - Y
     * @param displayFieldX Display field of view - X (mm)
     * @param displayFieldY Display field of view - Y (if different)
     * @param dimensionX Image dimension - X
     * @param dimensionY Image dimension - Y
     * @param pixelSizeX Image pixel size - X
     * @param pixelSizeY Image pixel size - Y
     * @param pixelDataID Pixel Data ID
     * @param contrastIV IV Contrast Agent
     * @param contrastOral Oral Contrast Agent
     * @param contrastMode Image Contrast Mode
     * @param seriesRX Series from which prescribed
     * @param imageRX Image from which prescribe
     * @param screenFormat Screen Format(8/16 bit)
     * @param plane Plane Type
     * @param scanSpacing Spacing between scans (mm?)
     * @param compression Image compression type for allocation
     * @param scoutType Scout Type (AP or lateral)
     * @param locationRAS RAS letter of image location
     * @param imageLocation Image location
     * @param centerR Center R coord of plane image
     * @param centerA Center A coord of plane image
     * @param centerS Center S coord of plane image
     * @param normR Normal R coord
     * @param normA Normal A coord
     * @param normS Normal S coord
     * @param TopLeftR R Coord of Top Left Hand Corner
     * @param TopLeftA A Coord of Top Left Hand Corner
     * @param TopLeftS S Coord of Top Left Hand Corner
     * @param TopRightR R Coord of Top Right Hand Corner
     * @param TopRightA A Coord of Top Right Hand Corner
     * @param TopRightS S Coord of Top Right Hand Corner
     * @param BotRightR R Coord of Bottom Right Hand Corner
     * @param BotRightA A Coord of Bottom Right Hand Corner
     * @param BotRightS S Coord of Bottom Right Hand Corner
     * @param foreignImageRev Foreign Image Revision
     * @param repTime Pulse repetition time(usec)
     * @param invTime Pulse inversion time(usec)
     * @param echoTime Pulse echo time(usec)
     * @param echoTime2 Second echo echo (usec)
     * @param numEcho Number of echoes
     * @param echoNumber Echo Number
     * @param tableDelta Table Delta
     * @param numExcitations Number of Excitations
     * @param continuous Continuous Slices Flag
     * @param heartRate Cardiac Heart Rate (bpm)
     * @param timeDelay Delay time after trigger (usec)
     * @param averageSAR Average SAR
     * @param peakSAR Peak SAR
     * @param monitorSAR Monitor SAR flag
     * @param triggerWindow Trigger window (% of R-R interval)
     * @param cardRepTime Cardiac repetition time
     * @param imagesPerCycle Images per cardiac cycle
     * @param transmitGain Actual Transmit Gain (.1 db)
     * @param recvGainAnalog Actual Receive Gain Analog (.1 db)
     * @param recvGainDigital Actual Receive Gain Digital (.1 db)
     * @param flipAngle Flip Angle for GRASS scans (deg.)
     * @param minDelay Minimum Delay after Trigger (uSec)
     * @param cardiacPhase Total Cardiac Phase prescribed
     * @param swapPhaseFreq Swap Phase/Frequency Axis
     * @param pauseInterval Pause Interval (slices)
     * @param pauseTime Pause Time
     * @param obliquePlane Oblique Plane
     * @param sliceOff Slice Offsets on Freq axis
     * @param transmitFreq Center Frequency (0.1 Hz)
     * @param autoCenterFreq Auto Center Frequency (0.1 Hz)
     * @param autoTransGain Auto Transmit Gain (0.1 dB)
     * @param prescanR1Analog PreScan R1 - Analog
     * @param prescanR2Digital PreScan R2 - Digital
     * @param userBitmap Bitmap defining user CVs
     * @param centerFreq Center Frequency Method
     * @param imagingMode Imaging Mode
     * @param imagingOptions Imaging Options
     * @param pulseSeq Pulse Sequence
     * @param pulseSeqMode Pulse Sequence Mode
     * @param pulseSeqName Pulse Sequence Name
     * @param PSDDateTime PSD Creation Date and Time
     * @param PSDName PSD name from inside PSD
     * @param coilType Coil Type
     * @param coilName Coil Name
     * @param srfCoilType Surface Coil Type
     * @param srfCoilExtr Extremity Coil Flag
     * @param rawRunNumber RawData Run Number
     * @param calFieldStrength Calibrated Field Strength (x10 uGauss)
     * @param suppTech SAT fat/water/none
     * @param varBandwidth Variable Bandwidth (Hz)
     * @param numSliceInScan Number of slices in this scan group
     * @param graphicPrescibed Graphically prescribed
     * @param interDelay Interimage/interloc delay (uSec)
     * @param user0 User Variable 0
     * @param user1 User Variable 1
     * @param user2 User Variable 2
     * @param user3 User Variable 3
     * @param user4 User Variable 4
     * @param user5 User Variable 5
     * @param user6 User Variable 6
     * @param user7 User Variable 7
     * @param user8 User Variable 8
     * @param user9 User Variable 9
     * @param user10 User Variable 10
     * @param user11 User Variable 11
     * @param user12 User Variable 12
     * @param user13 User Variable 13
     * @param user14 User Variable 14
     * @param user15 User Variable 15
     * @param user16 User Variable 16
     * @param user17 User Variable 17
     * @param user18 User Variable 18
     * @param user19 User Variable 19
     * @param user20 User Variable 20
     * @param user21 User Variable 21
     * @param user22 User Variable 22
     * @param projAngle Projection Angle
     * @param SATType Concat Sat Type Flag
     * @param allocKey Allocation Key
     * @param lastMod Date/Time of Last Change
     * @param versionCreation Genesis Version - Created
     * @param versionNow Genesis Version - Now
     * @param pixelDataSizeA PixelData size - as stored
     * @param pixelDataSizeC PixelData size - Compressed
     * @param pixelDataSizeU PixelData size - UnCompressed
     * @param checkSum AcqRecon record checksum 
     * @param archive Image Archive Flag
     * @param complete Image Complete Flag
     * @param SATBits Bitmap of SAT selections
     * @param srfCoilIntensity Surface Coil Intensity Correction Flag
     * @param satXlocR R-side SAT pulse loc rel to lndmrk
     * @param satXlocL L-side SAT pulse loc rel to lndmrk
     * @param satYlocA A-side SAT pulse loc rel to lndmrk
     * @param satYlocP P-side SAT pulse loc rel to lndmrk
     * @param satZlocS S-side SAT pulse loc rel to lndmrk
     * @param satZlocI I-side SAT pulse loc rel to lndmrk
     * @param satXThick Thickness of X-axis SAT pulse
     * @param satYThick Thickness of Y-axis SAT pulse
     * @param satZThick Thickness of Z-axis SAT pulse
     * @param flowAxisContrast Phase contrast flow axis
     * @param velocityContrast Phase contrast velocity encoding
     * @param thickDisclaimer Slice Thickness
     * @param prescanFlag Auto/Manual Prescan flag
     * @param changeBitmap Bitmap of changed values
     * @param imageType Magnitude, Phase, Imaginary, or Real
     * @param collapse Collapse Image
     * @param user23 User Variable 23
     * @param user24 User Variable 24
     * @param projectionAlg Projection Algorithm
     * @param projectionName Projection Algorithm Name
     * @param xAxisRot X Axis Rotation
     * @param yAxisRot Y Axis Rotation
     * @param zAxisRot Z Axis Rotation
     * @param minPixel1 Lower Range of Pixels 1
     * @param maxPixel1 Upper Range of Pixels 1
     * @param minPixel2 Lower Range of Pixels 2
     * @param maxPixel2 Upper Range of Pixels 2
     * @param echoTrainLen Echo Train Length for Fast Spin Echo
     * @param fractionEcho Fractional Echo - Effective TE Flag
     * @param prepPulse Preporatory Pulse Option
     * @param cardiacPhaseNum Cardiac Phase Number
     * @param variableEcho Variable Echo Flag
     * @param referenceField Reference Image Field
     * @param summaryField Summary Image Field
     * @param window Window Value
     * @param level Level Value
     * @param slopInt1 Integer Slop Field 1
     * @param slopInt2 Integer Slop Field 2
     * @param slopInt3 Integer Slop Field 3
     * @param slopInt4 Integer Slop Field 4
     * @param slopInt5 Integer Slop Field 5
     * @param slopFloat1 Float Slop Field 1
     * @param slopFloat2 Float Slop Field 2
     * @param slopFloat3 Float Slop Field 3
     * @param slopFloat4 Float Slop Field 4
     * @param slopFloat5 Float Slop Field 5
     * @param slopStr1 
     * @param slopStr2 
     */
    public GEMRImageInfo(byte [] suiteID, short uniqueFlag, 
			    char diskID, short examNumber, 
			    short seriesNum, short imageNum, 
			    int AllocDateTime, int ActualDateTime,
			    float scanTime, float sliceThick, 
			    short iMatrixX, short iMatrixY, 
			    float displayFieldX, 
			    float displayFieldY, 
			    float dimensionX, float dimensionY,
			    float pixelSizeX, float pixelSizeY,
			    byte [] pixelDataID,
			    byte [] contrastIV, 
			    byte [] contrastOral, 
			    short contrastMode, short seriesRX, 
			    short imageRX, short screenFormat, 
			    short plane, float scanSpacing,
			    short compression, short scoutType, 
			    char locationRAS, float imageLocation,
			    float centerR, float centerA, 
			    float centerS, float normR, 
			    float normA, float normS, 
			    float TopLeftR, float TopLeftA, 
			    float TopLeftS, float TopRightR, 
			    float TopRightA, float TopRightS, 
			    float BotRightR, float BotRightA, 
			    float BotRightS, 
			    byte [] foreignImageRev, int repTime,	
			    int invTime, int echoTime, 
			    int echoTime2, short numEcho, 
			    short echoNumber, float tableDelta, 
			    float numExcitations, short continuous,
			    short heartRate, int timeDelay, 
			    float averageSAR, float peakSAR, 
			    short monitorSAR, short triggerWindow,
			    float cardRepTime, 
			    short imagesPerCycle, 
			    short transmitGain, 
			    short recvGainAnalog, 
			    short recvGainDigital, 
			    short flipAngle, int minDelay, 
			    short cardiacPhase, 
			    short swapPhaseFreq, 
			    short pauseInterval, float pauseTime,
			    int obliquePlane, int sliceOff, 
			    int transmitFreq, int autoCenterFreq,
			    short autoTransGain, 
			    short prescanR1Analog, 
			    short prescanR2Digital, 
			    int userBitmap, short centerFreq, 
			    short imagingMode, int imagingOptions,
			    short pulseSeq, short pulseSeqMode,
			    byte [] pulseSeqName, int PSDDateTime,
			    byte [] PSDName, short coilType,
			    byte [] coilName, short srfCoilType, 
			    short srfCoilExtr, int rawRunNumber, 
			    int calFieldStrength, short suppTech,
			    float varBandwidth, short numSliceInScan, 
			    short graphicPrescibed, int interDelay,
			    float user0, float user1, float user2,
			    float user3, float user4, float user5,
			    float user6, float user7, float user8,
			    float user9, float user10,
			    float user11, float user12, 
			    float user13, float user14, 
			    float user15, float user16, 
			    float user17, float user18, 
			    float user19, float user20, 
			    float user21, float user22, 
			    float projAngle, float SATType,
			    byte [] allocKey, int lastMod,
			    byte [] versionCreation, 
			    byte [] versionNow, int pixelDataSizeA,
			    int pixelDataSizeC, int pixelDataSizeU,
			    int checkSum, int archive, 
			    int complete, short SATBits, 
			    short srfCoilIntensity, short satXlocR, 
			    short satXlocL, short satYlocA, 
			    short satYlocP, short satZlocS, 
			    short satZlocI, short satXThick, 
			    short satYThick, short satZThick, 
			    short flowAxisContrast, 
			    short velocityContrast, 
			    short thickDisclaimer, short prescanFlag,
			    short changeBitmap, short imageType,
			    short collapse, float user23, float user24,
			    short projectionAlg, byte [] projectionName,
			    float xAxisRot, float yAxisRot, 
			    float zAxisRot, int minPixel1, int maxPixel1,
			    int minPixel2, int maxPixel2, 
			    short echoTrainLen, short fractionEcho, 
			    short prepPulse, short cardiacPhaseNum, 
			    short variableEcho, char referenceField,
			    char summaryField, short window,
			    short level, int slopInt1, int slopInt2, 
			    int slopInt3, int slopInt4, int slopInt5, 
			    float slopFloat1, float slopFloat2, 
			    float slopFloat3, float slopFloat4, 
			    float slopFloat5, byte [] slopStr1,
			    byte [] slopStr2)
    {
	super(185, suiteID, uniqueFlag, diskID, examNumber,
				    seriesNum, imageNum, 
				    AllocDateTime, ActualDateTime,
				    scanTime, sliceThick, 
				    iMatrixX, iMatrixY, 
				    displayFieldX, 
				    displayFieldY, 
				    dimensionX, dimensionY,
				    pixelSizeX, pixelSizeY,
				    pixelDataID,
				    contrastIV, 
				    contrastOral, 
				    contrastMode, seriesRX, 
				    imageRX, screenFormat, 
				    plane, scanSpacing,
				    compression, scoutType, 
				    locationRAS, imageLocation,
				    centerR, centerA, 
				    centerS, normR, 
				    normA, normS, 
				    TopLeftR, TopLeftA, 
				    TopLeftS, TopRightR, 
				    TopRightA, TopRightS, 
				    BotRightR, BotRightA, 
				    BotRightS, 
				    foreignImageRev);

	_addElement(REP_TIME, new Integer(repTime));
	_addElement(INV_TIME, new Integer(invTime));
	_addElement(ECHO_TIME, new Integer(echoTime));
	_addElement(ECHO_TIME2, new Integer(echoTime2));
	_addElement(NUM_ECHO, new Short(numEcho));
	_addElement(ECHO_NUMBER, new Short(echoNumber));
	_addElement(TABLE_DELTA, new Float(tableDelta));
	_addElement(NUM_EXCITATIONS, new Float(numExcitations));
	_addElement(CONTINUOUS, new Short(continuous));
	_addElement(HEART_RATE, new Short(heartRate));
	_addElement(TIME_DELAY, new Integer(timeDelay));
	_addElement(AVERAGE_SAR, new Float(averageSAR));
	_addElement(PEAK_SAR, new Float(peakSAR));
	_addElement(MONITOR_SAR, new Short(monitorSAR));
	_addElement(TRIGGER_WINDOW, new Short(triggerWindow));
	_addElement(CARD_REP_TIME, new Float(cardRepTime));
	_addElement(IMAGES_PER_CYCLE, new Short(imagesPerCycle));
	_addElement(TRANSMIT_GAIN, new Short(transmitGain));
	_addElement(RECV_GAIN_ANALOG, new Short(recvGainAnalog));
	_addElement(RECV_GAIN_DIGITAL, new Short(recvGainDigital));
	_addElement(FLIP_ANGLE, new Short(flipAngle));
	_addElement(MIN_DELAY, new Integer(minDelay));
	_addElement(CARDIAC_PHASE, new Short(cardiacPhase));
	_addElement(SWAP_PHASE_FREQ, new Short(swapPhaseFreq));
	_addElement(PAUSE_INTERVAL, new Short(pauseInterval));
	_addElement(PAUSE_TIME, new Float(pauseTime));
	_addElement(OBLIQUE_PLANE, new Integer(obliquePlane));
	_addElement(SLICE_OFF, new Integer(sliceOff));
	_addElement(TRANSMIT_FREQ, new Integer(transmitFreq));
	_addElement(AUTO_CENTER_FREQ, new Integer(autoCenterFreq));
	_addElement(AUTO_TRANS_GAIN, new Short(autoTransGain));
	_addElement(PRESCAN_R1_ANALOG, new Short(prescanR1Analog));
	_addElement(PRESCAN_R2_DIGITAL, new Short(prescanR2Digital));
	_addElement(USER_BITMAP, new Integer(userBitmap));
	_addElement(CENTER_FREQ, new Short(centerFreq));
	_addElement(IMAGEING_MODE, new Short(imagingMode));
	_addElement(IMAGEING_OPTIONS, new Integer(imagingOptions));
	_addElement(PULSE_SEQ, new Short(pulseSeq));
	_addElement(PULSE_SEQ_MODE, new Short(pulseSeqMode));
	_addElement(PULSE_SEQ_NAME, _createString(pulseSeqName));
	_addElement(PSD_DATE_TIME, new Integer(PSDDateTime));
	_addElement(PSD_NAME, _createString(PSDName));
	_addElement(COIL_TYPE, new Short(coilType));
	_addElement(COIL_NAME, _createString(coilName));
	_addElement(SRF_COIL_TYPE, new Short(srfCoilType));
	_addElement(SRF_COIL_EXTR, new Short(srfCoilExtr));
	_addElement(RAW_RUN_NUMBER, new Integer(rawRunNumber));
	_addElement(CAL_FIELD_STRENGTH, new Integer(calFieldStrength));
	_addElement(SUPP_TECH, new Short(suppTech));
	_addElement(VAR_BANDWIDTH, new Float(varBandwidth));
	_addElement(NUM_SLICE_IN_SCAN, new Short(numSliceInScan));
	_addElement(GRAPHIC_PRESCRIBED, new Short(graphicPrescibed));
	_addElement(INTER_DELAY, new Integer(interDelay));
	_addElement(USER0, new Float(user0));
	_addElement(USER1, new Float(user1));
	_addElement(USER2, new Float(user2));
	_addElement(USER3, new Float(user3));
	_addElement(USER4, new Float(user4));
	_addElement(USER5, new Float(user5));
	_addElement(USER6, new Float(user6));
	_addElement(USER7, new Float(user7));
	_addElement(USER8, new Float(user8));
	_addElement(USER9, new Float(user9));
	_addElement(USER10, new Float(user10));
	_addElement(USER11, new Float(user11));
	_addElement(USER12, new Float(user12));
	_addElement(USER13, new Float(user13));
	_addElement(USER14, new Float(user14));
	_addElement(USER15, new Float(user15));
	_addElement(USER16, new Float(user16));
	_addElement(USER17, new Float(user17));
	_addElement(USER18, new Float(user18));
	_addElement(USER19, new Float(user19));
	_addElement(USER20, new Float(user20));
	_addElement(USER21, new Float(user21));
	_addElement(USER22, new Float(user22));
	_addElement(PROJ_ANGLE, new Float(projAngle));
	_addElement(SAT_TYPE, new Float(SATType));
	_addElement(ALLOC_KEY, _createString(allocKey));
	_addElement(LAST_MOD, new Integer(lastMod));
	_addElement(VERSION_CREATION, _createString(versionCreation));
	_addElement(VERSION_NOW, _createString(versionNow));
	_addElement(PIXEL_DATA_SIZE_A, new Integer(pixelDataSizeA));
	_addElement(PIXEL_DATA_SIZE_C, new Integer(pixelDataSizeC));
	_addElement(PIXEL_DATA_SIZE_U, new Integer(pixelDataSizeU));
	_addElement(CHECK_SUM, new Integer(checkSum));
	_addElement(ARCHIVE, new Integer(archive));
	_addElement(COMPLETE, new Integer(complete));
	_addElement(SAT_BITS, new Short(SATBits));
	_addElement(SRF_COIL_INTENSITY, new Short(srfCoilIntensity));
	_addElement(SAT_X_LOC_R, new Short(satXlocR));
	_addElement(SAT_X_LOC_L, new Short(satXlocL));
	_addElement(SAT_Y_LOC_A, new Short(satYlocA));
	_addElement(SAT_Y_LOC_P, new Short(satYlocP));
	_addElement(SAT_Z_LOC_S, new Short(satZlocS));
	_addElement(SAT_Z_LOC_I, new Short(satZlocI));
	_addElement(SAT_X_THICK, new Short(satXThick));
	_addElement(SAT_Y_THICK, new Short(satYThick));
	_addElement(SAT_Z_THICK, new Short(satZThick));
	_addElement(FLOW_AXIS_CONTRAST, new Short(flowAxisContrast));
	_addElement(VELOCITY_CONTRAST, new Short(velocityContrast));
	_addElement(THICK_DISCLAIMER, new Short(thickDisclaimer));
	_addElement(PRESCAN_FLAG, new Short(prescanFlag));
	_addElement(CHANGE_BITMAP, new Short(changeBitmap));
	_addElement(IMAGE_TYPE, new Short(imageType));
	_addElement(COLLAPSE, new Short(collapse));
	_addElement(USER23, new Float(user23));
	_addElement(USER24, new Float(user24));
	_addElement(PROJECTION_ALG, new Short(projectionAlg));
	_addElement(PROJECTION_NAME, _createString(projectionName));
	_addElement(X_AXIS_ROT, new Float(xAxisRot));
	_addElement(Y_AXIS_ROT, new Float(yAxisRot));
	_addElement(Z_AXIS_ROT, new Float(zAxisRot));
	_addElement(MIN_PIXEL1, new Integer(minPixel1));
	_addElement(MAX_PIXEL1, new Integer(maxPixel1));
	_addElement(MIN_PIXEL2, new Integer(minPixel2));
	_addElement(MAX_PIXEL2, new Integer(maxPixel2));
	_addElement(ECHO_TRAIN_LEN, new Short(echoTrainLen));
	_addElement(FRACTION_ECHO, new Short(fractionEcho));
	_addElement(PREP_PULSE, new Short(prepPulse));
	_addElement(CARDIAC_PHASE_NUM, new Short(cardiacPhaseNum));
	_addElement(VARIABLE_ECHO, new Short(variableEcho));
	_addElement(REFERENCE_FIELD, new Character(referenceField));
	_addElement(SUMMARY_FIELD, new Character(summaryField));
	_addElement(WINDOW, new Short(window));
	_addElement(LEVEL, new Short(level));
	_addElement(SLOP_INT1, new Integer(slopInt1));
	_addElement(SLOP_INT2, new Integer(slopInt2));
	_addElement(SLOP_INT3, new Integer(slopInt3));
	_addElement(SLOP_INT4, new Integer(slopInt4));
	_addElement(SLOP_INT5, new Integer(slopInt5));
	_addElement(SLOP_FLOAT1, new Float(slopFloat1));
	_addElement(SLOP_FLOAT2, new Float(slopFloat2));
	_addElement(SLOP_FLOAT3, new Float(slopFloat3));
	_addElement(SLOP_FLOAT4, new Float(slopFloat4));
	_addElement(SLOP_FLOAT5, new Float(slopFloat5));
	_addElement(SLOP_STR1, _createString(slopStr1));
	_addElement(SLOP_STR2, _createString(slopStr2));
    }
}
