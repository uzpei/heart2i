/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

/**
 * GE Subheader that describes the history of the exam.
 *
 * @version 10 October 2002
 */
public class GEExamInfo extends GESubheader
{
    /** ID of the suite ID. */
    public static final String SUITE_ID = "suiteID";

    /** ID of the Unique Flag */
    public static final String UNIQUE_FLAG = "uniqueFlag";

    /** ID of the Disk ID for this Exam */
    public static final String DISK_ID = "diskID";

    /** ID of the Exam Number */
    public static final String EXAM_NUMBER = "examNumber";

    /** ID of the Hospital Name */
    public static final String HOSPITAL_NAME= "hospitalName";

    /** ID of the Detector Type */
    public static final String DETECTOR = "detector";

    /** ID of the Number of cells in det */
    public static final String NUMBER_CELLS = "numberCells";

    /** ID of the Cell number at theta */
    public static final String ZERO_CELL = "zeroCell";

    /** ID of the Cell spacing */
    public static final String CELL_SPACE = "cellSpace";

    /** ID of the Distance from source to detector */
    public static final String SRC_TO_DET= "srcToDet";

    /** ID of the Distance from source to iso */
    public static final String SRC_TO_ISO = "srcToIso";

    /** ID of the Tube type */
    public static final String TUBE_TYPE = "tubeType";

    /** ID of the DAS type */
    public static final String DAS_TYPE = "dasType";

    /** ID of the Number of Decon Kernals */
    public static final String NUMBER_DECON_KERNALS = "numberDeconKernals";

    /** ID of the Number of elements in a Decon Kernal */
    public static final String DECON_LEN= "deconLen";

    /** ID of the Decon Kernal density */
    public static final String DECON_DENSITY= "deconDensity";

    /** ID of the Decon Kernal stepsize */
    public static final String DECON_STEP_SIZE = "deconStepSize";

    /** ID of the Decon Kernal Shift Count */
    public static final String DECON_SHIFT_CNT = "deconShiftCnt";

    /** ID of the Magnet strength (in gauss) */
    public static final String MAG_STRENGTH = "magStrength";

    /** ID of the Patient ID for this Exam */
    public static final String PATIENT_ID = "patientID";

    /** ID of the Patient Name */
    public static final String PATIENT_NAME = "patientName";

    /** ID of the Patient Age (years, months or days) */
    public static final String PATIENT_AGE = "patientAge";

    /** ID of the Patient Age Notation */
    public static final String PATIENT_AGE_NOT = "patientAgeNot";

    /** ID of the Patient Sex */
    public static final String PATIENT_SEX = "patientSex";

    /** ID of the Patient Weight */
    public static final String PATIENT_WEIGHT = "patientWeight";

    /** ID of the Trauma Flag */
    public static final String TRAUMA = "trauma";

    /** ID of the Patient History */
    public static final String PATIENT_HISTORY = "patientHistory";

    /** ID of the Requisition Number */
    public static final String REQ_NUMBER = "reqNumber";

    /** ID of the Exam date/time stamp */
    public static final String EXAM_DATE = "examDate";

    /** ID of the Referring Physician */
    public static final String REF_PHYSICIAN = "refPhysician";

    /** ID of the Diagnostician/Radiologist */
    public static final String DIAGRAD = "diagrad";

    /** ID of the Operator */
    public static final String OPERATOR = "operator";

    /** ID of the Exam Description */
    public static final String EXAM_DESCRIPTION = "examDescription";

    /** ID of the Exam Type */
    public static final String EXAM_TYPE = "examType";

    /** ID of the Exam Format */
    public static final String EXAM_FORMAT = "examFormat";

    /** ID of the Start time(secs) of first axial in exam */
    public static final String FIRST_AXIAL_TIME = "firstAxialTime";

    /** ID of the Creator Suite and Host */
    public static final String HOST = "host";

    /** ID of the Date/Time of Last Change */
    public static final String LAST_MOD = "lastMod";

    /** ID of the Non-Zero indicates Protocol Exam */
    public static final String PROTOCOL_FLAG = "protocolFlag";

    /** ID of the Process that allocated this record */
    public static final String ALLOC_KEY = "allocKey";

    /** ID of the Indicates number of updates to header */
    public static final String UPDATA_CNT = "updataCnt";

    /** ID of the Genesis Version - Created */
    public static final String VERSION_CREATION = "versionCreation";

    /** ID of the Genesis Version - Now */
    public static final String VERSION_NOW = "versionNow";

    /** ID of the Exam Record Checksum */
    public static final String CHECK_SUM = "checkSum";

    /** ID of the Exam Complete Flag */
    public static final String COMPLETE_FLAG = "completeFlag";

    /** ID of the Last Series Number Used */
    public static final String SERIES_CNT = "seriesCnt";

    /** ID of the Number of Series Archived */
    public static final String NUMBER_ARCHIVED = "numberArchived";

    /** ID of the Number of Series Existing */
    public static final String NUMBER_SERIES = "numberSeries";

    /** ID of the Number Series Keys for this Exam */
    public static final String SERIES_KEY_LEN = "seriesKeyLen";

    /** ID of the Series Keys for this Exam */
    public static final String SERIES_KEY = "seriesKey";

    /** ID of the Number Unstored Series for this Exam */
    public static final String NUMBER_UNST_SERIES = "numberUnstSeries";

    /** ID of the Number of Unstored Series Keys for this Exam */
    public static final String UNST_SERIES_KEY_LEN = "unstSeriesKeyLen";

    /** ID of the Unstored Series Keys for this Exam */
    public static final String UNST_SERIES_KEY = "unstSeriesKey";

    /** ID of the Number of Unarchived Series for this Exam */
    public static final String NUMBER_UNARCH_SERIES = "numberUnarchSeries";

    /** ID of the Number of Unarchived Series Keys for this Exam */
    public static final String UNARCH_SERIES_KEY_LEN = "unarchSeriesKeyLen";

    /** ID of the Unarchived Series Keys for this Exam */
    public static final String UNARCH_SERIES_KEY = "unarchSeriesKey";

    /** ID of the Number Prospective/Scout Series for this Exam */
    public static final String NUMBER_PROS_SERIES = "numberProsSeries";

    /** ID of the Number Prospective/Scout Series Keys for this Exam */
    public static final String PROS_SERIES_KEY_LEN = "prosSeriesKeyLen";

    /** ID of the Prospective/Scout Series Keys for this Exam */
    public static final String PROS_SERIES_KEY = "prosSeriesKey";

    /** ID of the Last Model Number used */
    public static final String MODEL_NUMBER = "modelNumber";

    /** ID of the Number of ThreeD Models */
    public static final String MODEL_CNT = "modelCnt";

    /** ID of the Number ThreeD Model Keys for Exam */
    public static final String MODEL_KEY_LEN = "modelKeyLen";

    /** ID of the ThreeD Model Keys for Exam */
    public static final String MODEL_KEY = "modelKey";

    /** ID of the Patient Status */
    public static final String PATIENT_STATUS = "patientStatus";

    /** ID of the Unique System ID */
    public static final String SYSTEM_ID = "systemID";

    /** ID of the Unique Service ID */
    public static final String SERVICE_ID = "serviceID";

    /** ID of the Mobile Location Number */
    public static final String MOBLE_LOCATION = "mobleLocation";


  /**
   * Constructs a GEExamInfo.
   *
   * @param suiteID Suite ID. 
   * @param uniqueFlag Unique Flag.
   * @param diskID Disk ID for this Exam.
   * @param examNumber Exam Number.
   * @param hospitalName Hospital Name. 
   * @param detector Detector Type.
   * @param numberCells Number of cells in det. 
   * @param zeroCell Cell number at theta.
   * @param cellSpace Cell spacing. 
   * @param srcToDet Distance from source to detector.
   * @param srcToIso Distance from source to iso. 
   * @param tubeType Tube type.
   * @param dasType DAS type. 
   * @param numberDeconKernals Number of Decon Kernals.
   * @param deconLen Number of elements in a Decon Kernal. 
   * @param deconDensity Decon Kernal density.
   * @param deconStepSize Decon Kernal stepsize. 
   * @param deconShiftCnt Decon Kernal Shift Count.
   * @param magStrength Magnet strength (in gauss). 
   * @param patientID Patient ID for this Exam.
   * @param patientName Patient Name. 
   * @param patientAge Patient Age (years, months or days).
   * @param patientAgeNot Patient Age Notation. 
   * @param patientSex Patient Sex.
   * @param patientWeight Patient Weight. 
   * @param trauma Trauma Flag.
   * @param patientHistory Patient History. 
   * @param reqNumber Requisition Number.
   * @param examDate Exam date/time stamp. 
   * @param refPhysician Referring Physician.
   * @param diagrad Diagnostician/Radiologist. 
   * @param operator Operator.
   * @param examDescription Exam Description. 
   * @param examType Exam Type.
   * @param examFormat Exam Format. 
   * @param firstAxialTime Start time(secs) of first axial in exam.
   * @param host Creator Suite and Host. 
   * @param lastMod Date/Time of Last Change.
   * @param protocolFlag Non-Zero indicates Protocol Exam. 
   * @param allocKey Process that allocated this record.
   * @param updataCnt Indicates number of updates to header. 
   * @param versionCreation Genesis Version - Created.
   * @param versionNow Genesis Version - Now. 
   * @param checkSum Exam Record Checksum.
   * @param completeFlag Exam Complete Flag. 
   * @param seriesCnt Last Series Number Used.
   * @param numberArchived Number of Series Archived. 
   * @param numberSeries Number of Series Existin.
   * @param seriesKeyLen Number Series Keys for this Exam. 
   * @param seriesKey Series Keys for this Exam.
   * @param numberUnstSeries Number Unstored Series for this Exam. 
   * @param unstSeriesKeyLen Number of Unstored Series Keys for this Exam.
   * @param unstSeriesKey Unstored Series Keys for this Exam. 
   * @param numberUnarchSeries Number of Unarchived Series for this Exam.
   * @param unarchSeriesKeyLen Number of Unarchived Series Keys for this Exam. 
   * @param unarchSeriesKey Unarchived Series Keys for this Exam.
   * @param prosSeriesKeyLen Number Prospective/Scout Series Keys this Exam. 
   * @param prosSeriesKey Prospective/Scout Series Keys for this Exam.
   * @param modelNumber Last Model Number used. 
   * @param modelCnt Number of ThreeD Models.
   * @param modelKeyLen Number ThreeD Model Keys for Exam. 
   * @param modelKey ThreeD Model Keys for Exam.
   * @param patientStatus Patient Status. 
   * @param systemID Unique System ID.
   * @param serviceID Unique Service ID. 
   * @param mobleLocation Mobile Location Number.
   *
   *  @throws IllegalArgumentException If any byte array is larger than its
   *                                  specified maximum length.
   */
    public GEExamInfo(byte [] suiteID, short uniqueFlag, char diskID,
			 short examNumber, byte [] hospitalName, 
			 short detector, int numberCells, float zeroCell,
			 float cellSpace, float srcToDet, float srcToIso,
			 short tubeType, short dasType, 
			 short numberDeconKernals, short deconLen, 
			 short deconDensity, short deconStepSize, 
			 short deconShiftCnt, int magStrength, 
			 byte [] patientID, byte [] patientName, 
			 short patientAge, short patientAgeNot, 
			 short patientSex, int patientWeight, 
			 short trauma, byte [] patientHistory, 
			 byte [] reqNumber, int examDate, 
			 byte [] refPhysician, byte [] diagrad, 
			 byte [] operator, byte [] examDescription, 
			 byte [] examType, short examFormat, 
			 double firstAxialTime, byte [] host, 
			 int lastMod, short protocolFlag, 
			 byte [] allocKey, int updataCnt, 
			 byte [] versionCreation, byte [] versionNow, 
			 int checkSum, int completeFlag, 
			 int seriesCnt, int numberArchived, 
			 int numberSeries, int seriesKeyLen, 
			 byte [] seriesKey, int numberUnstSeries, 
			 int unstSeriesKeyLen, byte [] unstSeriesKey, 
			 int numberUnarchSeries, 
			 int unarchSeriesKeyLen, 
			 byte [] unarchSeriesKey, int numberProsSeries, 
			 int prosSeriesKeyLen, byte [] prosSeriesKey, 
			 int modelNumber, int modelCnt, 
			 int modelKeyLen, byte [] modelKey, 
			 short patientStatus, byte [] systemID, 
			 byte [] serviceID, int mobleLocation)
    {
      super(67);

      _addElement(SUITE_ID, _createString(suiteID));
      _addElement(UNIQUE_FLAG, new Short(uniqueFlag));
      _addElement(DISK_ID, new Character(diskID));
      _addElement(EXAM_NUMBER, new Short(examNumber));
      _addElement(HOSPITAL_NAME, _createString(hospitalName));
      _addElement(DETECTOR, new Short(detector));
      _addElement(NUMBER_CELLS, new Integer(numberCells));
      _addElement(ZERO_CELL, new Float(zeroCell));
      _addElement(CELL_SPACE, new Float(cellSpace));
      _addElement(SRC_TO_DET, new Float(srcToDet));
      _addElement(SRC_TO_ISO, new Float(srcToIso));
      _addElement(TUBE_TYPE, new Short(tubeType));
      _addElement(DAS_TYPE, new Short(dasType));
      _addElement(NUMBER_DECON_KERNALS, new Short(numberDeconKernals));
      _addElement(DECON_LEN, new Short(deconLen));
      _addElement(DECON_DENSITY, new Short(deconDensity));
      _addElement(DECON_STEP_SIZE, new Short(deconStepSize));
      _addElement(DECON_SHIFT_CNT, new Short(deconShiftCnt));
      _addElement(MAG_STRENGTH, new Integer(magStrength));
      _addElement(PATIENT_ID, _createString(patientID));
      _addElement(PATIENT_NAME, _createString(patientName));
      _addElement(PATIENT_AGE, new Short(patientAge));
      _addElement(PATIENT_AGE_NOT, new Short(patientAgeNot));
      _addElement(PATIENT_SEX, new Short(patientSex));
      _addElement(PATIENT_WEIGHT, new Integer(patientWeight));
      _addElement(TRAUMA, new Short(trauma));
      _addElement(PATIENT_HISTORY, _createString(patientHistory));
      _addElement(REQ_NUMBER, _createString(reqNumber));
      _addElement(EXAM_DATE, new Integer(examDate));
      _addElement(REF_PHYSICIAN, _createString(refPhysician));
      _addElement(DIAGRAD, _createString(diagrad));
      _addElement(OPERATOR, _createString(operator));
      _addElement(EXAM_DESCRIPTION, _createString(examDescription));
      _addElement(EXAM_TYPE, _createString(examType));
      _addElement(EXAM_FORMAT, new Short(examFormat));
      _addElement(FIRST_AXIAL_TIME, new Double(firstAxialTime));
      _addElement(HOST, _createString(host));
      _addElement(LAST_MOD, new Integer(lastMod));
      _addElement(PROTOCOL_FLAG, new Short(protocolFlag));
      _addElement(ALLOC_KEY, _createString(allocKey));
      _addElement(UPDATA_CNT, new Integer(updataCnt));
      _addElement(VERSION_CREATION, _createString(versionCreation));
      _addElement(VERSION_NOW, _createString(versionNow));
      _addElement(CHECK_SUM, new Integer(checkSum));
      _addElement(COMPLETE_FLAG, new Integer(completeFlag));
      _addElement(SERIES_CNT, new Integer(seriesCnt));
      _addElement(NUMBER_ARCHIVED, new Integer(numberArchived));
      _addElement(NUMBER_SERIES, new Integer(numberSeries));
      _addElement(SERIES_KEY_LEN, new Integer(seriesKeyLen));
      _addElement(SERIES_KEY, _createString(seriesKey));
      _addElement(NUMBER_UNST_SERIES, new Integer(numberUnstSeries));
      _addElement(UNST_SERIES_KEY_LEN, new Integer(unstSeriesKeyLen));
      _addElement(UNST_SERIES_KEY, _createString(unstSeriesKey));
      _addElement(NUMBER_UNARCH_SERIES, new Integer(numberUnarchSeries));
      _addElement(UNARCH_SERIES_KEY_LEN, new Integer(unarchSeriesKeyLen));
      _addElement(UNARCH_SERIES_KEY, _createString(unarchSeriesKey));
      _addElement(NUMBER_PROS_SERIES, new Integer(numberProsSeries));
      _addElement(PROS_SERIES_KEY_LEN, new Integer(prosSeriesKeyLen));
      _addElement(PROS_SERIES_KEY, _createString(prosSeriesKey));
      _addElement(MODEL_NUMBER, new Integer(modelNumber));
      _addElement(MODEL_CNT, new Integer(modelCnt));
      _addElement(MODEL_KEY_LEN, new Integer(modelKeyLen));
      _addElement(MODEL_KEY, _createString(modelKey));
      _addElement(PATIENT_STATUS, new Short(patientStatus));
      _addElement(SYSTEM_ID, _createString(systemID));
      _addElement(SERVICE_ID, _createString(serviceID));
      _addElement(MOBLE_LOCATION, new Integer(mobleLocation));
    }
}
