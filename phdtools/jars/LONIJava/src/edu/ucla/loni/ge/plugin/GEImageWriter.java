/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge.plugin;

import edu.ucla.loni.ge.GECTImageInfo;
import edu.ucla.loni.ge.GEExamInfo;
import edu.ucla.loni.ge.GEHeader;
import edu.ucla.loni.ge.GEImageInfo;
import edu.ucla.loni.ge.GEMRImageInfo;
import edu.ucla.loni.ge.GESeriesInfo;
import edu.ucla.loni.ge.GESuiteInfo;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Vector;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Image Writer for parsing and encoding GE 5.0 images.
 *
 * @version 14 October 2004
 */
public class GEImageWriter extends ImageWriter
{
  /** GE Header of the image sequence currently being written. */
  private GEHeader _streamHeader;

  /** True if one image has been written to the current sequence; false o/w. */
  private boolean _imageIsWritten;

  /**
   * Constructs a GE Image Writer.
   *
   * @param originatingProvider The ImageWriterSpi that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not an
   *                                  GE Image Writer Spi.
   */
  public GEImageWriter(ImageWriterSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be an GE Image Writer Spi
      if ( !(originatingProvider instanceof GEImageWriterSpi) ) {
	String msg = "Originating provider must an GE Image Writer SPI.";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Returns an IIOMetadata object containing default values for encoding a
   * stream of images.
   *
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object.
   */
  public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
      // Return a blank metadata object
      return new GEMetadata(null);
    }

  /**
   * Returns an IIOMetadata object containing default values for encoding an
   * image of the given type.
   *
   * @param imageType An ImageTypeSpecifier indicating the format of the image
   *                  to be written later.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object.
   */
  public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					     ImageWriteParam param)
    {
      // Not applicable
      return null;
    }

  /**
   * Returns an IIOMetadata object that may be used for encoding and optionally
   * modified using its document interfaces or other interfaces specific to the
   * writer plug-in that will be used for encoding.
   *
   * @param inData An IIOMetadata object representing stream metadata, used to
   *               initialize the state of the returned object.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object, or null if the plug-in does not provide
   *         metadata encoding capabilities.
   *
   * @throws IllegalArgumentException If inData is null.
   */
  public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					   ImageWriteParam param)
    {
      // Only recognize GE Metadata
      if (inData instanceof GEMetadata) { return inData; }

      // Otherwise perform no conversion
      return null;
    }

  /**
   * Returns an IIOMetadata object that may be used for encoding and optionally
   * modified using its document interfaces or other interfaces specific to the
   * writer plug-in that will be used for encoding.
   *
   * @param inData An IIOMetadata object representing image metadata, used to
   *               initialize the state of the returned object.
   * @param imageType An ImageTypeSpecifier indicating the layout and color
   *                  information of the image with which the metadata will be
   *                  associated.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object, or null if the plug-in does not provide
   *         metadata encoding capabilities.
   *
   * @throws IllegalArgumentException If either of inData or imageType is null.
   */
  public IIOMetadata convertImageMetadata(IIOMetadata inData,
					  ImageTypeSpecifier imageType,
					  ImageWriteParam param)
    {
      // Not applicable
      return null;
    }

  /**
   * Returns true if the methods that take an IIOImage parameter are capable
   * of dealing with a Raster (as opposed to RenderedImage) source image.
   *
   * @return True if Raster sources are supported.
   */
  public boolean canWriteRasters()
    {
      return true;
    }

  /**
   * Appends a complete image stream containing a single image and associated
   * stream and image metadata and thumbnails to the output.
   *
   * @param streamMetadata An IIOMetadata object representing stream metadata,
   *                       or null to use default values.
   * @param image An IIOImage object containing an image, thumbnails, and
   *              metadata to be written.
   * @param param An ImageWriteParam, or null to use a default ImageWriteParam.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the image is null, if the stream
   *                                  metadata cannot be converted, or if the
   *                                  image type is unrecognized.
   * @throws IllegalStateException If the output has not been set.
   */
  public void write(IIOMetadata streamMetadata, IIOImage image,
		    ImageWriteParam param) throws IOException
    {
      // Write a sequence with 1 image
      prepareWriteSequence(streamMetadata);
      writeToSequence(image, param);
      endWriteSequence();
    }

  /**
   * Returns true if the writer is able to append an image to an image
   * stream that already contains header information and possibly prior
   * images.
   *
   * @return True If images may be appended sequentially.
   */
  public boolean canWriteSequence()
    {
      return true;
    }

  /**
   * Prepares a stream to accept a series of subsequent writeToSequence calls,
   * using the provided stream metadata object.  The metadata will be written
   * to the stream if it should precede the image data.
   *
   * @param streamMetadata A stream metadata object.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the stream metadata is null or cannot
   *                                  be converted.
   */
  public void prepareWriteSequence(IIOMetadata streamMetadata)
    throws IOException
    {
      // Convert the metadata to GE Metadata
      IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

      // Unable to convert the metadata
      if (metadata == null) {
	String msg = "Unable to convert the stream metadata for encoding.";
	throw new IllegalArgumentException(msg);
      }

      // Construct a stream GE Header from the metadata
      try {
	String rootName = GEMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	Node tree = metadata.getAsTree(rootName);
	_streamHeader = GEMetadataConversions.toGEHeader(tree);
      }
      
      catch (Exception e) {
	String msg = "Unable to construct an GE Header from the metadata.";
	throw new IllegalArgumentException(msg);
      }

      // Define the subheader offsets and sizes
      int preHeaderSize = 124;
      int suiteInfoOffset = 2304;
      int suiteInfoSize = 114;
      int examInfoOffset = 2418;
      int examInfoSize = 1024;
      int seriesInfoOffset = 3442;
      int seriesInfoSize = 1020;
      int imageInfoOffset = 4462;
      int imageInfoSize = 1022;
      int pixelDataOffset = 7904;

      // Allocate memory for the header
      ByteArrayOutputStream outputStream =
	new ByteArrayOutputStream(pixelDataOffset);

      // Write the preheader
      outputStream.write( _getPreHeaderBytes(_streamHeader.getGEImageInfo(),
					     preHeaderSize, pixelDataOffset) );

      // Write the subheader offsets and sizes (32 bytes)
      DataOutputStream dataStream = new DataOutputStream(outputStream);
      dataStream.writeInt(suiteInfoOffset);
      dataStream.writeInt(suiteInfoSize);
      dataStream.writeInt(examInfoOffset);
      dataStream.writeInt(examInfoSize);
      dataStream.writeInt(seriesInfoOffset);
      dataStream.writeInt(seriesInfoSize);
      dataStream.writeInt(imageInfoOffset);
      dataStream.writeInt(imageInfoSize);

      // Skip to the suite bytes
      byte[] zeroes = new byte[suiteInfoOffset - preHeaderSize - 32];
      outputStream.write(zeroes);

      // Write the GE Suite Info
      outputStream.write( _getSuiteInfoBytes(_streamHeader.getGESuiteInfo(),
					  suiteInfoSize) );

      // Write the GE Exam Info
      outputStream.write( _getExamInfoBytes(_streamHeader.getGEExamInfo(),
					 examInfoSize) );

      // Write the GE Series Info
      outputStream.write( _getSeriesInfoBytes(_streamHeader.getGESeriesInfo(),
					   seriesInfoSize) );

      // Write the GE Image Info
      outputStream.write( _getImageInfoBytes(_streamHeader.getGEImageInfo(),
					  imageInfoSize) );

      // Skip to the pixel data
      zeroes = new byte[pixelDataOffset - imageInfoOffset - imageInfoSize];
      outputStream.write(zeroes);

      // Write the encoded bytes in memory
      byte[] encodedBytes = outputStream.toByteArray();
      byte[] bytes = new byte[pixelDataOffset];
      System.arraycopy(encodedBytes, 0, bytes, 0, encodedBytes.length);
      _getOutputStream().write(bytes);

      // Mark that an image can now be written
      _imageIsWritten = false;
    }

  /**
   * Appends a single image and possibly associated metadata and thumbnails,
   * to the output.  The supplied ImageReadParam is ignored.
   *
   * @param image An IIOImage object containing an image, thumbnails, and
   *              metadata to be written.
   * @param param An ImageWriteParam or null to use a default ImageWriteParam.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the image is null or if the image type
   *                                  is not recognized.
   * @throws IllegalStateException If the output has not been set,
   *                               prepareWriteSequence has not been called, or
   *                               one image has already been written.
   */
  public void writeToSequence(IIOImage image, ImageWriteParam param)
    throws IOException
    { 
      // Check for the stream GE Header
      if (_streamHeader == null) {
	String msg = "The image sequence has not been prepared.";
	throw new IllegalStateException(msg);
      }

      // Check for a null image
      if (image == null) {
	String msg = "Cannot write a null image.";
	throw new IllegalArgumentException(msg);
      }

      // Check to see if an image has already been written
      if (_imageIsWritten) {
	String msg = "Cannot write more than 1 image.";
	throw new IllegalStateException(msg);
      }

      // Get a Raster from the image
      Raster raster = image.getRaster();
      if (raster == null) {
	RenderedImage renderedImage = image.getRenderedImage();

	// If the Rendered Image is a Buffered Image, get the Raster directly
	if (renderedImage instanceof BufferedImage) {
	  raster = ((BufferedImage)renderedImage).getRaster();
	}

	// Otherwise get a copy of the Raster from the Rendered Image
	raster = renderedImage.getData();
      }

      // Update the Listeners
      processImageStarted(0);

      // Write an 8 bit grayscale image
      ImageOutputStream outputStream = _getOutputStream();
      DataBuffer dataBuffer = raster.getDataBuffer();
      if (dataBuffer instanceof DataBufferByte) {
	outputStream.write( ((DataBufferByte)dataBuffer).getData() );
      }

      // Write a 16 bit grayscale image
      else if (dataBuffer instanceof DataBufferUShort) {
	short[] data = ((DataBufferUShort)dataBuffer).getData();
	int offset = 0;
	int length = data.length;

	outputStream.writeShorts(data, offset, length);
      }

      // Unrecognized type
      else {
	String msg = "Unable to write the IIOImage.";
	throw new IllegalArgumentException(msg);
      }

      // Update the Listeners
      _imageIsWritten = true;
      if ( abortRequested() ) { processWriteAborted(); }
      else { processImageComplete(); }
    }

  /**
   * Completes the writing of a sequence of images begun with
   * prepareWriteSequence.  Any stream metadata that should come at the end of
   * the sequence of images is written out, and any header information at the
   * beginning of the sequence is patched up if necessary.
   *
   * @throws IllegalStateException If the output has not been set, or
   *                               prepareWriteSequence has not been called.
   */
  public void endWriteSequence()
    {
      // Reset the stream GE Header
      _streamHeader = null;
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageWriter.  Otherwise, the writer may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the output stream
      try { if (output != null) { ((ImageOutputStream)output).close(); } }
      catch (Exception e) {}

      output = null;
    }

  /**
   * Gets the output stream.
   *
   * @return Image Output Stream to write to.
   *
   * @throws IllegalStateException If the output has not been set.
   */
  private ImageOutputStream _getOutputStream()
    {
      // No output has been set
      if (output == null) {
	String msg = "No output has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Output Stream for the metadata
      return (ImageOutputStream)output;
    }

  /**
   * Gets the preheader encoded in a byte array.
   *
   * @param imageInfo GE Image Info to encode values from.
   * @param arraySize Size of the byte array to return.
   * @param pixelDataOffset Number of bytes from the start of the stream to
   *                        the pixel data.
   *
   * @return Byte array containing the encoded preheader.
   *
   * @throws IOException If an error occurs while encoding.
   */
  private byte[] _getPreHeaderBytes(GEImageInfo imageInfo, int arraySize,
				    int pixelDataOffset)
    throws IOException
    {
      // Allocate memory for the preheader
      ByteArrayOutputStream outStream = new ByteArrayOutputStream(arraySize);

      // Write the magic number
      int magicNumber = 0x494d4746;
      _writeNumber( new Integer(magicNumber), outStream );

      // Write the pixel data offset
      _writeNumber( new Integer(pixelDataOffset), outStream );

      // Write the image width and height
      String name = GEImageInfo.I_MATRIX_X;
      int width = ((Short)imageInfo.getElementValue(name)).intValue();
      name = GEImageInfo.I_MATRIX_Y;
      int height = ((Short)imageInfo.getElementValue(name)).intValue();

      _writeNumber( new Integer(width), outStream );
      _writeNumber( new Integer(height), outStream );

      // Write the number of bits per pixel
      name = GEImageInfo.SCREEN_FORMAT;
      int bitsPerPixel = ((Short)imageInfo.getElementValue(name)).intValue();
      _writeNumber( new Integer(bitsPerPixel), outStream );

      // Write the flag for no compression and rectangular
      _writeNumber( new Integer(1), outStream );

      // Copy the bytes to an array of the required size
      byte[] encodedBytes = outStream.toByteArray();
      byte[] bytes = new byte[arraySize];
      System.arraycopy(encodedBytes, 0, bytes, 0, encodedBytes.length);

      // Return the encoded bytes
      return bytes;
    }

  /**
   * Gets the Suite Info encoded in a byte array.
   *
   * @param suiteInfo GE Suite Info to encode.
   * @param arraySize Size of the byte array to return.
   *
   * @return Byte array containing the encoded Suite Info.
   *
   * @throws IOException If an error occurs while encoding.
   */
  private byte[] _getSuiteInfoBytes(GESuiteInfo suiteInfo, int arraySize)
    throws IOException
    {
      // Allocate memory for the Suite Info
      ByteArrayOutputStream outStream = new ByteArrayOutputStream(arraySize);

      // Write all the elements
      Iterator iter = suiteInfo.getElementNames();
      while ( iter.hasNext() ) {
	String name = (String)iter.next();
	Object value = suiteInfo.getElementValue(name);

	// Element value is a String
	if (value instanceof String) {
	  int size = 0;
	  if ( GESuiteInfo.SUITE_ID.equals(name) ) { size = 4; }
	  if ( GESuiteInfo.PRODUCT_ID.equals(name) ) { size = 13; }
	  if ( GESuiteInfo.VERSION_CR.equals(name) ) { size = 2; }
	  if ( GESuiteInfo.VERSION_CU.equals(name) ) { size = 2; }

	  // Write the string value
	  _writeString( (String)value, size, outStream );
	}

	// Element value is a Character
	else if (value instanceof Character) {
	  _writeCharacter( (Character)value, outStream );
	}

	// Element value is a Number
	else { _writeNumber( (Number)value, outStream ); }
      }

      // Copy the bytes to an array of the required size
      byte[] encodedBytes = outStream.toByteArray();
      byte[] bytes = new byte[arraySize];
      System.arraycopy(encodedBytes, 0, bytes, 0, encodedBytes.length);

      // Return the encoded bytes
      return bytes;
    }

  /**
   * Gets the Exam Info encoded in a byte array.
   *
   * @param examInfo GE Exam Info to encode.
   * @param arraySize Size of the byte array to return.
   *
   * @return Byte array containing the encoded Exam Info.
   *
   * @throws IOException If an error occurs while encoding.
   */
  private byte[] _getExamInfoBytes(GEExamInfo examInfo, int arraySize)
    throws IOException
    {
      // Allocate memory for the Exam Info
      ByteArrayOutputStream outStream = new ByteArrayOutputStream(arraySize);

      // Write all the elements
      Iterator iter = examInfo.getElementNames();
      while ( iter.hasNext() ) {
	String name = (String)iter.next();
	Object value = examInfo.getElementValue(name);

	// Element value is a String
	if (value instanceof String) {
	  int size = 0;
	  if ( GEExamInfo.SUITE_ID.equals(name) ) { size = 4; }
	  if ( GEExamInfo.HOSPITAL_NAME.equals(name) ) { size = 32; }
	  if ( GEExamInfo.PATIENT_ID.equals(name) ) { size = 12; }
	  if ( GEExamInfo.PATIENT_NAME.equals(name) ) { size = 25; }
	  if ( GEExamInfo.PATIENT_HISTORY.equals(name) ) { size = 61; }
	  if ( GEExamInfo.REQ_NUMBER.equals(name) ) { size = 13; }
	  if ( GEExamInfo.REF_PHYSICIAN.equals(name) ) { size = 33; }
	  if ( GEExamInfo.DIAGRAD.equals(name) ) { size = 33; }
	  if ( GEExamInfo.OPERATOR.equals(name) ) { size = 4; }
	  if ( GEExamInfo.EXAM_DESCRIPTION.equals(name) ) { size = 23; }
	  if ( GEExamInfo.EXAM_TYPE.equals(name) ) { size = 3; }
	  if ( GEExamInfo.HOST.equals(name) ) { size = 9; }
	  if ( GEExamInfo.ALLOC_KEY.equals(name) ) { size = 13; }
	  if ( GEExamInfo.VERSION_CREATION.equals(name) ) { size = 2; }
	  if ( GEExamInfo.VERSION_NOW.equals(name) ) { size = 2; }
	  if ( GEExamInfo.SERIES_KEY.equals(name) ) { size = 4; }
	  if ( GEExamInfo.UNST_SERIES_KEY.equals(name) ) { size = 4; }
	  if ( GEExamInfo.UNARCH_SERIES_KEY.equals(name) ) { size = 4; }
	  if ( GEExamInfo.PROS_SERIES_KEY.equals(name) ) { size = 4; }
	  if ( GEExamInfo.MODEL_KEY.equals(name) ) { size = 4; }
	  if ( GEExamInfo.SYSTEM_ID.equals(name) ) { size = 16; }
	  if ( GEExamInfo.SERVICE_ID.equals(name) ) { size = 16; }

	  // Write the string value
	  _writeString( (String)value, size, outStream );
	}

	// Element value is a Character
	else if (value instanceof Character) {
	  _writeCharacter( (Character)value, outStream );
	}

	// Element value is a Number
	else { _writeNumber( (Number)value, outStream ); }

	// Skip bytes if necessary after the element
	if ( name.equals(GEExamInfo.DISK_ID) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GEExamInfo.HOSPITAL_NAME) ) {
	  outStream.write(new byte[2]);
	}
	else if ( name.equals(GEExamInfo.PATIENT_ID) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GEExamInfo.HOST) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GEExamInfo.UPDATA_CNT) ) {
	  outStream.write(new byte[1]);
	}
      }

      // Copy the bytes to an array of the required size
      byte[] encodedBytes = outStream.toByteArray();
      byte[] bytes = new byte[arraySize];
      System.arraycopy(encodedBytes, 0, bytes, 0, encodedBytes.length);

      // Return the encoded bytes
      return bytes;
    }

  /**
   * Gets the Series Info encoded in a byte array.
   *
   * @param seriesInfo GE Series Info to encode.
   * @param arraySize Size of the byte array to return.
   *
   * @return Byte array containing the encoded Series Info.
   *
   * @throws IOException If an error occurs while encoding.
   */
  private byte[] _getSeriesInfoBytes(GESeriesInfo seriesInfo, int arraySize)
    throws IOException
    {
      // Allocate memory for the Series Info
      ByteArrayOutputStream outStream = new ByteArrayOutputStream(arraySize);

      // Write all the elements
      Iterator iter = seriesInfo.getElementNames();
      while ( iter.hasNext() ) {
	String name = (String)iter.next();
	Object value = seriesInfo.getElementValue(name);

	// Element value is a String
	if (value instanceof String) {
	  int size = 0;
	  if ( GESeriesInfo.SUITE_ID.equals(name) ) { size = 4; }
	  if ( GESeriesInfo.SERIES_DESCRIPTION.equals(name) ) { size = 30; }
	  if ( GESeriesInfo.PRIMARY_RECIEVER_HOST.equals(name) ) { size = 9; }
	  if ( GESeriesInfo.ARCHIVER_HOST.equals(name) ) { size = 9; }
	  if ( GESeriesInfo.ANATOM_REFERENCE.equals(name) ) {size = 3;}
	  if ( GESeriesInfo.PROTOCOL_NAME.equals(name) ) { size = 25; }
	  if ( GESeriesInfo.ALLOC_KEY.equals(name) ) { size = 13; }
	  if ( GESeriesInfo.VERSION_CREATED.equals(name) ) { size = 2; }
	  if ( GESeriesInfo.VERSION_NOW.equals(name) ) { size = 2; }
	  if ( GESeriesInfo.IMAGE_KEY.equals(name) ) { size = 4; }
	  if ( GESeriesInfo.UNST_IMAGE_KEY.equals(name) ) { size = 4; }
	  if ( GESeriesInfo.UNARCH_IAMGE_KEY.equals(name) ) {size = 4;}

	  // Write the string value
	  _writeString( (String)value, size, outStream );
	}

	// Element value is a Character
	else if (value instanceof Character) {
	  _writeCharacter( (Character)value, outStream );
	}

	// Element value is a Number
	else { _writeNumber( (Number)value, outStream ); }

	// Skip bytes if necessary after the element
	if ( name.equals(GESeriesInfo.DISK_ID) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GESeriesInfo.ANATOM_REFERENCE) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GESeriesInfo.PROTOCOL_NAME) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GESeriesInfo.START_RAS) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GESeriesInfo.END_RAS) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GEMRImageInfo.ALLOC_KEY) ) {
	  outStream.write(new byte[1]);
	}
      }

      // Copy the bytes to an array of the required size
      byte[] encodedBytes = outStream.toByteArray();
      byte[] bytes = new byte[arraySize];
      System.arraycopy(encodedBytes, 0, bytes, 0, encodedBytes.length);

      // Return the encoded bytes
      return bytes;
    }

  /**
   * Gets the Image Info encoded in a byte array.
   *
   * @param imageInfo GE Image Info to encode.
   * @param arraySize Size of the byte array to return.
   *
   * @return Byte array containing the encoded Image Info.
   *
   * @throws IOException If an error occurs while encoding.
   */
  private byte[] _getImageInfoBytes(GEImageInfo imageInfo, int arraySize)
    throws IOException
    {
      // Allocate memory for the Image Info
      ByteArrayOutputStream outStream = new ByteArrayOutputStream(arraySize);

      // Write all the elements
      Iterator iter = imageInfo.getElementNames();
      while ( iter.hasNext() ) {
	String name = (String)iter.next();
	Object value = imageInfo.getElementValue(name);

	// Element value is a String
	if (value instanceof String) {
	  int size = 0;
	  if ( GEImageInfo.SUITE_ID.equals(name) ) { size = 4; }
	  if ( GEImageInfo.PIXEL_DATA_ID.equals(name) ) { size = 14; }
	  if ( GEImageInfo.CONTRAST_IV.equals(name) ) { size = 17; }
	  if ( GEImageInfo.CONTRAST_ORAL.equals(name) ) { size = 17; }
	  if ( GEImageInfo.FOREIGN_IMAGE_REV.equals(name) ) {size = 4;}
	  if ( GEImageInfo.ALLOC_KEY.equals(name) ) { size = 13; }
	  if ( GEImageInfo.VERSION_CREATION.equals(name) ) {size = 2;}
	  if ( GEImageInfo.VERSION_NOW.equals(name) ) { size = 2; }
	  if ( GEImageInfo.SLOP_STR1.equals(name) ) { size = 16; }
	  if ( GEImageInfo.SLOP_STR2.equals(name) ) { size = 16; }

	  if (imageInfo instanceof GEMRImageInfo) {
	    if ( GEMRImageInfo.PULSE_SEQ_NAME.equals(name) ) { size = 33; }
	    if ( GEMRImageInfo.PSD_NAME.equals(name) ) { size = 13; }
	    if ( GEMRImageInfo.COIL_NAME.equals(name) ) { size = 17; }
	    if ( GEMRImageInfo.PROJECTION_NAME.equals(name) ) { size = 13; }
	  }

	  if (imageInfo instanceof GECTImageInfo) {
	    if ( GECTImageInfo.QCAL_FILE.equals(name) ) { size = 13; }
	    if ( GECTImageInfo.CAL_MOD_FILE.equals(name) ) { size = 13; }
	    if ( GECTImageInfo.SCOUT_ANATOM_REF.equals(name) ) { size = 3; }
	  }

	  // Write the string value
	  _writeString( (String)value, size, outStream );
	}

	// Element value is a Character
	else if (value instanceof Character) {
	  _writeCharacter( (Character)value, outStream );
	}

	// Element value is a Number
	else { _writeNumber( (Number)value, outStream ); }

	// Skip bytes if necessary after the element
	if ( name.equals(GEImageInfo.DISK_ID) ) {
	  outStream.write(new byte[1]);
	}
	else if ( name.equals(GEImageInfo.LOCATION_RAS) ) {
	  outStream.write(new byte[1]);
	}

	if (imageInfo instanceof GEMRImageInfo) {
	  if ( name.equals(GEMRImageInfo.PULSE_SEQ_NAME) ) {
	    outStream.write(new byte[1]);
	  }
	  else if ( name.equals(GEMRImageInfo.PSD_NAME) ) {
	    outStream.write(new byte[1]);
	  }
	  else if ( name.equals(GEMRImageInfo.COIL_NAME) ) {
	    outStream.write(new byte[1]);
	  }
	  else if ( name.equals(GEMRImageInfo.ALLOC_KEY) ) {
	    outStream.write(new byte[1]);
	  }
	}

	if (imageInfo instanceof GECTImageInfo){
	  if ( name.equals(GECTImageInfo.SCOUT_ANATOM_REF) ) {
	    outStream.write(new byte[1]);
	  }
	  else if ( name.equals(GEMRImageInfo.ALLOC_KEY) ) {
	    outStream.write(new byte[1]);
	  }
	}
      }

      // Copy the bytes to an array of the required size
      byte[] encodedBytes = outStream.toByteArray();
      byte[] bytes = new byte[arraySize];
      System.arraycopy(encodedBytes, 0, bytes, 0, encodedBytes.length);

      // Return the encoded bytes
      return bytes;
    }

  /**
   * Writes the Number to the output stream.
   *
   * @param number Number to write to the output stream.
   * @param outputStream Output Stream to write to.
   *
   * @throws IOException If an error occurs during the writing.
   */
  private void _writeNumber(Number number, OutputStream outputStream)
    throws IOException
    {
      DataOutputStream dataStream = new DataOutputStream(outputStream);

      // Byte
      if (number instanceof Byte) {
	int value = ((Byte)number).byteValue() & 0xffffffff;
	dataStream.writeByte(value);
      }

      // Short
      else if (number instanceof Short) {
	int value = ((Short)number).shortValue() & 0xffffffff;
	dataStream.writeShort(value);
      }

      // Integer
      else if (number instanceof Integer) {
	dataStream.writeInt( ((Integer)number).intValue() );
      }

      // Float
      else if (number instanceof Float) {
	dataStream.writeFloat( ((Float)number).floatValue() );
      }

      // Double
      else if (number instanceof Double) {
	dataStream.writeDouble( ((Double)number).doubleValue() );
      }

      // Unknown Number
      else {
	String msg = "Unable to write a Number of type \"" +
	             number.getClass().getName() + "\".";
	throw new IOException(msg);
      }
    }

  /**
   * Writes the Character to the output stream.
   *
   * @param character Character to write to the output stream.
   * @param outputStream Output Stream to write to.
   *
   * @throws IOException If an error occurs during the writing.
   */
  private void _writeCharacter(Character character, OutputStream outputStream)
    throws IOException
    {
      DataOutputStream dataStream = new DataOutputStream(outputStream);
      int value = character.charValue() & 0xffffffff;
      dataStream.writeByte(value);
    }

  /**
   * Writes the String to the output stream.
   *
   * @param string String to write to the output stream.
   * @param length Length of the string to actually write.
   * @param outputStream Output Stream to write to.
   *
   * @throws IOException If an error occurs during the writing.
   */
  private void _writeString(String string, int length,
			    OutputStream outputStream)
    throws IOException
    {
      // Create a byte buffer of the specified length
      byte[] buffer = new byte[length];

      // Copy the bytes of the String to the byte buffer
      byte[] stringBytes = string.getBytes();
      int min = Math.min( buffer.length, stringBytes.length );
      System.arraycopy(stringBytes, 0, buffer, 0, min);

      // Write the bytes
      outputStream.write(buffer);
    }
}
