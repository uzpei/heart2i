/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ge.plugin;

import edu.ucla.loni.ge.GEHeader;
import edu.ucla.loni.ge.GEImageInfo;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for metadata of the GE 5.0 image format.
 *
 * @version 15 January 2009
 */
public class GEMetadata extends AppletFriendlyIIOMetadata
{
    /** GE Header containing the original metadata. */
    private GEHeader _originalGEHeader;

    /** GE Header containing the current metadata. */
    private GEHeader _currentGEHeader;

    /**
     * Constructs an GEMetadata from the given GE Header.
     *
     * @param geHeader GE Header containing the metadata.
     */
    public GEMetadata(GEHeader geHeader)
    {
	super(true, GEMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      GEMetadataFormat.class.getName(),
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalGEHeader = geHeader;
	_currentGEHeader = geHeader;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return GEMetadataConversions.toTree(_currentGEHeader);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current GE Header, convert the root
	Node r;
	if (_currentGEHeader == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Get the current metadata tree Nodes
	    Node geHeaderNode = getAsTree(formatName);
	    Node geSuiteInfoNode = geHeaderNode.getFirstChild();
	    Node geExamInfoNode = geSuiteInfoNode.getNextSibling();
	    Node geSeriesInfoNode = geExamInfoNode.getNextSibling();
	    Node geImageInfoNode = geSeriesInfoNode.getNextSibling();

	    // Loop through the root Node children and merge their attributes
	    NodeList nodeList = root.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node childNode = nodeList.item(i);
		String childName = childNode.getNodeName();
		NamedNodeMap childAttrs = childNode.getAttributes();

		// Replace Header Key attributes
		if ( GEMetadataFormat.GE_SUITE_INFO_NAME.equals(childName) ) {
		    geSuiteInfoNode = _getMergedNode(geSuiteInfoNode,
						     childAttrs);
		}

		// Replace Image Dimension attributes
		if ( GEMetadataFormat.GE_EXAM_INFO_NAME.equals(childName) ) {
		    geExamInfoNode = _getMergedNode(geExamInfoNode, childAttrs);
		}

		// Replace Image Dimension attributes
		if ( GEMetadataFormat.GE_SERIES_INFO_NAME.equals(childName) ) {
		    geSeriesInfoNode = _getMergedNode(geSeriesInfoNode,
						      childAttrs);
		}

		// Replace Image Dimension attributes
		if ( GEMetadataFormat.GE_IMAGE_INFO_NAME.equals(childName) ) {
		    geImageInfoNode = _getMergedNode(geImageInfoNode,
						     childAttrs);
		}
	    }

	    // Recreate the root tree
	    r = new IIOMetadataNode(nativeMetadataFormatName);
	    r.appendChild(geSuiteInfoNode);
	    r.appendChild(geExamInfoNode);
	    r.appendChild(geSeriesInfoNode);
	    r.appendChild(geImageInfoNode);
	}

	// Set a new current GE Header
	_currentGEHeader = GEMetadataConversions.toGEHeader(r);
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current GE Header to the original 
	_currentGEHeader = _originalGEHeader;
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// Only support non-inverted grayscale
	return Utilities.getGrayScaleChromaNode(false);
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	try {

	    // Bits per pixel is the SCREEN_FORMAT of the GE Image Info
	    GEImageInfo imageInfo = _currentGEHeader.getGEImageInfo();
	    Object value = imageInfo.getElementValue(GEImageInfo.SCREEN_FORMAT);
	    int bitsPerPixel = ((Number)value).intValue();

	    // Support grayscale data
	    return Utilities.getGrayScaleDataNode(bitsPerPixel);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Returns an IIOMetadataNode representing the dimension format information
     * of the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the dimension information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Pixel size is the PIXEL_SIZE_* of the GE Image Info
	    GEImageInfo imageInfo = _currentGEHeader.getGEImageInfo();
	    Object value = imageInfo.getElementValue(GEImageInfo.PIXEL_SIZE_X);
	    float pixelWidth = ((Number)value).floatValue();

	    value = imageInfo.getElementValue(GEImageInfo.PIXEL_SIZE_Y);
	    float pixelHeight = ((Number)value).floatValue();

	    // Return the dimension node
	    return Utilities.getDimensionNode(pixelWidth, pixelHeight);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	GEImageInfo imageInfo = _currentGEHeader.getGEImageInfo();

	// Image width
	Object value = imageInfo.getElementValue(GEImageInfo.I_MATRIX_X);
	int imageWidth = ((Number)value).intValue();

	// Image height
	value = imageInfo.getElementValue(GEImageInfo.I_MATRIX_Y);
	int imageHeight = ((Number)value).intValue();

	// Number of images
	int numberOfImages = 1;

	// Pixel width
	value = imageInfo.getElementValue(GEImageInfo.PIXEL_SIZE_X);
	float pixelWidth = ((Number)value).floatValue();

	// Pixel height
	value = imageInfo.getElementValue(GEImageInfo.PIXEL_SIZE_Y);
	float pixelHeight = ((Number)value).floatValue();

	// Slice thickness
	value = imageInfo.getElementValue(GEImageInfo.SLICE_THICK);
	float sliceThickness = ((Number)value).floatValue();

	// Bits per pixel
	value = imageInfo.getElementValue(GEImageInfo.SCREEN_FORMAT);
	int bitsPerPixel = ((Number)value).intValue();

	// Data type
	String dataType = "Undetermined";
	if (bitsPerPixel == 8) { dataType = BasicMetadataFormat.BYTE_TYPE; }
	if (bitsPerPixel == 16) {
	    dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE;
	}
	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;

	// Return the basic tree
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }

    /**
     * Merges the attributes with the specified Node.  If there are attribute
     * names not present in the attributes of the node, the attributes are
     * ignored.
     *
     * @param node Node to merge the attributes with.
     * @param attributes Attributes to merge.
     *
     * @return New Node containing the merged attributes.
     */
    private Node _getMergedNode(Node node, NamedNodeMap attributes)
    {
	// Create a new Node with the same name
	IIOMetadataNode mergedNode = new IIOMetadataNode( node.getNodeName() );

	// Copy the Node attributes to the new Node
	NamedNodeMap map = node.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		mergedNode.setAttribute( attr.getNodeName(),
					 attr.getNodeValue() );
	    }
	}

	// Merge the new attributes with the old
	if (attributes != null) {
	    for (int i = 0; i < attributes.getLength(); i++) {
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName();

		// Attribute name must be present in the new Node
		if ( mergedNode.hasAttribute(attributeName) ) {
		    mergedNode.setAttribute(attributeName,
					    attribute.getNodeValue());
		}
	    }
	}

	// Return the new merged Node
	return mergedNode;
    }
}
