/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

/**
 * GE Subheader that describes the CT Image Info.
 *
 * @version 15 July 2003
 */
public class GECTImageInfo extends GEImageInfo
{
    /** ID of the Table Start Location. */
    public static final String TABLE_START_LOC = "tableStartLoc";

    /** ID of the Table End Location. */
    public static final String TABLE_END_LOC = "tableEndLoc";

    /** ID of the Table Speed (mm/sec). */
    public static final String TABLE_SPEED = "tableSpeed";

    /** ID of the Table Height. */
    public static final String TABLE_HEIGHT = "tableHeight";

    /** ID of the Mid Scan Time. */
    public static final String MID_SCAN_TIME = "midScanTime";

    /** ID of the MidScan flag - does midstime apply for L/S. */
    public static final String MID_SCAN_FLAG = "midScanFlag";

    /** ID of the KVolt generator setting. */
    public static final String KVOLT = "kvolt";

    /** ID of the MAmp generator setting. */
    public static final String MAMP = "mamp";

    /** ID of the Gantry Tilt (degrees). */
    public static final String GANTRY_TILT = "gantryTilt";

    /** ID of the Degrees of Azimuth. */
    public static final String AZIMUTH = "azimuth";

    /** ID of the Gantry Velocity. */
    public static final String GANTRY_VEL = "gantryVel";

    /** ID of the Gantry Filter Position. */
    public static final String GANTRY_FILTER = "gantryFilter";

    /** ID of the Trigger on Position. */
    public static final String TRIGGER_ON_POS = "triggerOnPos";

    /** ID of the Degrees of rotation. */
    public static final String DEGREE_ROT = "degreeRot";

    /** ID of the X-Ray On Position. */
    public static final String XRAY_ON = "xrayOn";

    /** ID of the X-Ray Off Position. */
    public static final String XRAY_OFF = "xrayOff";

    /** ID of the Number of Triggers. */
    public static final String NUM_TRIGGERS = "numTriggers";

    /** ID of the Total input views. */
    public static final String INPUT_VIEWS = "inputViews";

    /** ID of the Angle of first view. */
    public static final String ANGLE_VIEW1 = "angleView1";

    /** ID of the Trigger frequency. */
    public static final String TRIGGER_FREQ = "triggerFreq";

    /** ID of the DAS trigger source. */
    public static final String TRIGGER_SRC = "triggerSrc";

    /** ID of the DAS fpa gain. */
    public static final String FPA_GAIN = "fpaGain";

    /** ID of the Scan Type. */
    public static final String SCAN_TYPE = "scanType";

    /** ID of the DAS output source. */
    public static final String OUT_SRC = "outSrc";

    /** ID of the DAS ad input. */
    public static final String AD_INPUT = "adInput";

    /** ID of the DAS cal mode. */
    public static final String CAL_MODE = "calMode";

    /** ID of the DAS cal frequency. */
    public static final String CAL_FREQ = "calFreq";

    /** ID of the DAS reg xm. */
    public static final String REG_XM = "regXM";

    /** ID of the DAS auto zero. */
    public static final String AUTO_ZERO = "autoZero";

    /** ID of the Axial Type. */
    public static final String AXIAL_TYPE = "axialType";

    /** ID of the Calibration phantom size. */
    public static final String PHANT_SIZE = "phantSize";

    /** ID of the Calibration phantom type. */
    public static final String PHANT_TYPE = "phantType";

    /** ID of the Calibration filter type. */
    public static final String FILTER_TYPE = "filterType";

    /** ID of the Recon Algorithm. */
    public static final String RECON_ALG = "reconAlg";

    /** ID of the Perstaltic flag. */
    public static final String PERIS_FLAG = "perisFlag";

    /** ID of the IterBone flag. */
    public static final String ITERBONE_FLAG = "iterboneFlag";

    /** ID of the Stat Recon flag. */
    public static final String STAT_FLAG = "statFlag";

    /** ID of the Compute Type. */
    public static final String COMPUTE_TYPE = "computeType";

    /** ID of the Segment Number. */
    public static final String SEG_NUMBER = "segNumber";

    /** ID of the Total Number of Segments Requested. */
    public static final String SEGS_TOTAL = "segsTotal";

    /** ID of the Inter scan delay (secs). */
    public static final String ISCAN_DELAY = "iscanDelay";

    /** ID of the Scan field of view (mm). */
    public static final String SCAN_FIELD = "scanField";

    /** ID of the Scan Number. */
    public static final String SCAN_NUMBER = "scanNumber";

    /** ID of the Starting Channel of View. */
    public static final String VIEW_START_CHAN = "viewStartChan";

    /** ID of the View Compression Factor. */
    public static final String VIEW_COMP_FACTOR = "viewCompFactor";

    /** ID of the Total Output Views. */
    public static final String OUT_VIEWS = "outViews";

    /** ID of the Number of Overranges. */
    public static final String OVER_RANGES = "overRanges";

    /** ID of the Total Number of Ref Channels. */
    public static final String TOTAL_REF_CHAN = "totalRefChan";

    /** ID of the data size for scan data. */
    public static final String SCAN_DATA_SIZE = "scanDataSize";

    /** ID of the z or q channel. */
    public static final String REF_CHAN0 = "refChan0";

    /** ID of the Reference channel 1. */
    public static final String REF_CHAN1 = "refChan1";

    /** ID of the Reference channel 2. */
    public static final String REF_CHAN2 = "refChan2";

    /** ID of the Reference channel 3. */
    public static final String REF_CHAN3 = "refChan3";

    /** ID of the Reference channel 4. */
    public static final String REF_CHAN4 = "refChan4";

    /** ID of the Reference channel 5. */
    public static final String REF_CHAN5 = "refChan5";

    /** ID of the Recon post processing flag. */
    public static final String POST_PROC_FLAG = "postProcFlag";

    /** ID of the DAS xm pattern. */
    public static final String XM_PATTERN = "xmPattern";

    /** ID of the Prescribed rotation type. */
    public static final String ROT_TYPE = "rotType";

    /** ID of the Save Raw Data Flag. */
    public static final String RAW_DATA_FLAG = "rawDataFlag";

    /** ID of the IBH Image scale factors. */
    public static final String CT_SCALE_FACTOR = "ctScaleFactor";

    /** ID of the CT Water Number. */
    public static final String CT_WATER_NUMBER = "ctWaterNumber";

    /** ID of the CT Bone Number. */
    public static final String CT_BONE_NUMBER = "ctBoneNumber";

    /** ID of the BBH coefficient 1. */
    public static final String BBH_COEF1 = "bbhCoef1";

    /** ID of the BBH coefficient 2. */
    public static final String BBH_COEF2 = "bbhCoef2";

    /** ID of the BBH coefficient 3. */
    public static final String BBH_COEF3 = "bbhCoef3";

    /** ID of the Num of BBH channels to blend. */
    public static final String BBH_NUM_BLEND = "bbhNumBlend";

    /** ID of the Starting channel. */
    public static final String FIRST_CHAN = "firstChan";

    /** ID of the Number of channels (1..512). */
    public static final String NUM_CHAN = "numChan";

    /** ID of the Increment between channels. */
    public static final String CHAN_INCR = "chanIncr";

    /** ID of the Starting view. */
    public static final String FIRST_VIEW = "firstView";

    /** ID of the Number of views. */
    public static final String NUM_VIEW = "numView";

    /** ID of the Increment between views. */
    public static final String VIEW_INCR = "viewIncr";

    /** ID of the Window Range (0..4095). */
    public static final String WINDOW_RANGE = "windowRange";

    /** ID of the Scaling value of the image data. */
    public static final String SCALE_MIN = "scaleMin";

    /** ID of the Scaling value of the image data. */
    public static final String SCALE_MAX = "scaleMax";

    /** ID of the Amount of processing that will be performed. */
    public static final String DATA_MODE = "dataMode";

    /** ID of the Source of the qcal vectors. */
    public static final String QCAL_FILE = "qcalFile";

    /** ID of the Source of the cal vectors. */
    public static final String CAL_MOD_FILE = "calModFile";

    /** ID of the Number of words per view. */
    public static final String WORDS_PER_VIEW = "wordsPerView";

    /** ID of the RAS letter for side of image. */
    public static final String RL_RAS = "rlRAS";

    /** ID of the RAS letter for anterior/posterior. */
    public static final String AP_RAS = "apRAS";

    /** ID of the RAS letter for scout start loc. */
    public static final String SCOUT_START_RAS = "scoutStartRAS";

    /** ID of the RAS letter for scout end loc. */
    public static final String SCOUT_END_RAS = "scoutEndRAS";

    /** ID of the Anatomical reference for scout. */
    public static final String SCOUT_ANATOM_REF = "scoutAnatomRef";

    /** ID of the PpScan window range for output Scaling. */
    public static final String PPS_SCALE_WINDOW = "ppsScaleWindow";

    /** ID of the PpScan Qcal modification flag. */
    public static final String PPS_QCAL_FLAG = "ppsQcalFlag";

    /** ID of the PpScan Pcal modification flag. */
    public static final String PPS_PCAL_FLAG = "ppsPcalFlag";

    /** ID of the PpScan Theta Fix (Angle Correction). */
    public static final String PPS_THETA_FIX = "ppsThetaFix";

    /** ID of the PpScan Beam Hardening Flag. */
    public static final String PPS_BEAM_FLAG = "ppsBeamFlag";

    /** ID of the tube focal spot size. */
    public static final String SPOT_SIZE = "spotSize";

    /** ID of the tube focal spot position. */
    public static final String SPOT_POS = "spotPos";

    /** ID of the Dependent on #views processed. */
    public static final String RECON_DATA_SET = "reconDataSet";

    /** ID of the Field of view in detector cells. */
    public static final String DET_CELL_FIELD = "detCellField";

    /** ID of the Start time(secs) of this scan. */
    public static final String SCAN_START_TIME = "scanStartTime";

    /** ID of the Gantry Rotation Direction. */
    public static final String GANTRY_DIRECTION = "gantryDirection";

    /** ID of the Tube Rotor Speed. */
    public static final String ROTOR_SPEED = "rotorSpeed";

    /** ID of the TGGC Trigger Mode. */
    public static final String TRIG_MODE = "trigMode";

    /** ID of the Rx'd gantry tilt - not annotated. */
    public static final String SI_TILT = "siTilt";

    /** ID of the R/L coordinate for target recon center. */
    public static final String R_RECON_CENTER = "rReconCenter";

    /** ID of the A/P coordinate for target recon center. */
    public static final String A_RECON_CENTER = "aReconCenter";

    /** ID of the Value of Back Projection button. */
    public static final String BACK_PROJ_FLAG = "backProjFlag";

    /** ID of the Set if fatq estimates were used. */
    public static final String FATQ_EST_FLAG = "fatqEstFlag";

    /** ID of the Z chan avg over views. */
    public static final String Z_AVG = "zAvg";

    /** ID of the avg of left ref chans over views. */
    public static final String LEFT_REF_AVG = "leftRefAvg";

    /** ID of the max left chan value over views. */
    public static final String LEFT_REF_MAX = "leftRefMax";

    /** ID of the avg of right ref chans over views. */
    public static final String RIGHT_REF_AVG = "rightRefAvg";

    /** ID of the max right chan value over views. */
    public static final String RIGHT_REF_MAX = "rightRefMax";

    /** ID of the Biopsy Position. */
    public static final String BIOPSY_POS = "biopsyPos";

    /** ID of the Biopsy T Location. */
    public static final String BIOPSY_T_LOC = "boipsyTLoc";

    /** ID of the Biopsy Ref Location. */
    public static final String BIOPSY_REF_LOC = "biopsyRefLoc";

    /** ID of the Reference Channel Used. */
    public static final String REF_CHAN = "refChan";

    /** ID of the Back Projector Coefficient. */
    public static final String BP_COEF = "bpCoef";

    /** ID of the Primary Speed Correction Used. */
    public static final String PSPEED_CORR = "pspeedCorr";

    /** ID of the Overrange Correction Used. */
    public static final String OVERRANGE_CORR = "overrangeCorr";

    /** ID of the Dynamic Z Alpha Value. */
    public static final String DYN_Z_ALPHA = "dynZAlpha";

    /**
     * Constructs a GECTImageInfo.
     *
     * @param numberFields Number of fields in image info implementation.
     * @param suiteID suite ID for this image.
     * @param uniqueFlag Unique Flag
     * @param diskID Disk ID for this Image
     * @param examNumber Exam number for this image
     * @param seriesNum Series Number for this image
     * @param imageNum Image Number
     * @param AllocDateTime Allocation Image date/time stamp
     * @param ActualDateTime Actual Image date/time stamp
     * @param scanTime Duration of scan (secs)
     * @param sliceThick Slice Thickness (mm)
     * @param iMatrixX Image matrix size - X
     * @param iMatrixY Image matrix size - Y
     * @param displayFieldX Display field of view - X (mm)
     * @param displayFieldY Display field of view - Y (if different)
     * @param dimensionX Image dimension - X
     * @param dimensionY Image dimension - Y
     * @param pixelSizeX Image pixel size - X
     * @param pixelSizeY Image pixel size - Y
     * @param pixelDataID Pixel Data ID
     * @param contrastIV IV Contrast Agent
     * @param contrastOral Oral Contrast Agent
     * @param contrastMode Image Contrast Mode
     * @param seriesRX Series from which prescribed
     * @param imageRX Image from which prescribe
     * @param screenFormat Screen Format(8/16 bit)
     * @param plane Plane Type
     * @param scanSpacing Spacing between scans (mm?)
     * @param compression Image compression type for allocation
     * @param scoutType Scout Type (AP or lateral)
     * @param locationRAS RAS letter of image location
     * @param imageLocation Image location
     * @param centerR Center R coord of plane image
     * @param centerA Center A coord of plane image
     * @param centerS Center S coord of plane image
     * @param normR Normal R coord
     * @param normA Normal A coord
     * @param normS Normal S coord
     * @param TopLeftR R Coord of Top Left Hand Corner
     * @param TopLeftA A Coord of Top Left Hand Corner
     * @param TopLeftS S Coord of Top Left Hand Corner
     * @param TopRightR R Coord of Top Right Hand Corner
     * @param TopRightA A Coord of Top Right Hand Corner
     * @param TopRightS S Coord of Top Right Hand Corner
     * @param BotRightR R Coord of Bottom Right Hand Corner
     * @param BotRightA A Coord of Bottom Right Hand Corner
     * @param BotRightS S Coord of Bottom Right Hand Corner
     * @param foreignImageRev Foreign Image Revision
     * @param tableStartLoc Table Start Location. 
     * @param tableEndLoc Table End Location
     * @param tableSpeed Table Speed (mm/sec)
     * @param tableHeight Table Height
     * @param midScanTime Mid Scan Time
     * @param midScanFlag MidScan flag - does midstime apply for L/S
     * @param kvolt KVolt generator setting
     * @param mamp MAmp generator setting
     * @param gantryTilt Gantry Tilt (degrees)
     * @param azimuth Degrees of Azimuth
     * @param gantryVel Gantry Velocity
     * @param gantryFilter Gantry Filter Position
     * @param triggerOnPos Trigger on Position
     * @param degreeRot Degrees of rotation
     * @param xrayOn X-Ray On Position
     * @param xrayOff X-Ray Off Position
     * @param numTriggers Number of Triggers
     * @param inputViews Total input views
     * @param angleView1 Angle of first view
     * @param triggerFreq Trigger frequency
     * @param triggerSrc DAS trigger source
     * @param fpaGain DAS fpa gain
     * @param scanType Scan Type
     * @param outSrc DAS output source
     * @param adInput DAS ad input
     * @param calMode DAS cal mode
     * @param calFreq DAS cal frequency
     * @param regXM DAS reg xm
     * @param autoZero DAS auto zero
     * @param axialType Axial Type
     * @param phantSize Calibration phantom size
     * @param phantType Calibration phantom type
     * @param filterType Calibration filter type
     * @param reconAlg Recon Algorithm
     * @param perisFlag Perstaltic flag
     * @param iterboneFlag IterBone flag
     * @param statFlag Stat Recon flag
     * @param computeType Compute Type
     * @param segNumber Segment Number
     * @param segsTotal Total Number of Segments Requested
     * @param iscanDelay Inter scan delay (secs)
     * @param scanField Scan field of view (mm)
     * @param scanNumber Scan Number
     * @param viewStartChan Starting Channel of View
     * @param viewCompFactor View Compression Factor
     * @param outViews Total Output Views
     * @param overRanges Number of Overranges
     * @param totalRefChan Total Number of Ref Channels
     * @param scanDataSize data size for scan data
     * @param refChan0 z or q channel
     * @param refChan1 Reference channel 1
     * @param refChan2 Reference channel 2
     * @param refChan3 Reference channel 3
     * @param refChan4 Reference channel 4
     * @param refChan5 Reference channel 5
     * @param postProcFlag Recon post processing flag
     * @param xmPattern DAS xm pattern
     * @param rotType Prescribed rotation type
     * @param rawDataFlag Save Raw Data Flag
     * @param ctScaleFactor IBH Image scale factors
     * @param ctWaterNumber CT Water Number
     * @param ctBoneNumber CT Bone Number
     * @param bbhCoef1 BBH coefficient 1
     * @param bbhCoef2 BBH coefficient 2
     * @param bbhCoef3 BBH coefficient 3
     * @param bbhNumBlend Num of BBH channels to blend
     * @param firstChan Starting channel
     * @param numChan Number of channels (1..512)
     * @param chanIncr Increment between channels
     * @param firstView Starting view
     * @param numView Number of views
     * @param viewIncr Increment between views
     * @param windowRange Window Range (0..4095)
     * @param scaleMin Scaling value of the image data
     * @param scaleMax Scaling value of the image data
     * @param dataMode Amount of processing that will be performed
     * @param qcalFile Source of the qcal vectors
     * @param calModFile Source of the cal vectors
     * @param wordsPerView Number of words per view
     * @param rlRAS RAS letter for side of image
     * @param apRAS RAS letter for anterior/posterior
     * @param scoutStartRAS RAS letter for scout start loc
     * @param scoutEndRAS RAS letter for scout end loc
     * @param scoutAnatomRef Anatomical reference for scout
     * @param ppsScaleWindow PpScan window range for output Scaling
     * @param ppsQcalFlag PpScan Qcal modification flag
     * @param ppsPcalFlag PpScan Pcal modification flag
     * @param ppsThetaFix PpScan Theta Fix (Angle Correction)
     * @param ppsBeamFlag PpScan Beam Hardening Flag
     * @param spotSize tube focal spot size
     * @param spotPos tube focal spot position
     * @param reconDataSet Dependent on #views processed
     * @param detCellField Field of view in detector cells
     * @param scanStartTime Start time(secs) of this scan
     * @param gantryDirection Gantry Rotation Direction
     * @param rotorSpeed Tube Rotor Speed
     * @param trigMode TGGC Trigger Mode
     * @param siTilt Rx'd gantry tilt - not annotated
     * @param rReconCenter R/L coordinate for target recon center
     * @param aReconCenter A/P coordinate for target recon center
     * @param backProjFlag Value of Back Projection button
     * @param fatqEstFlag Set if fatq estimates were used
     * @param zAvg Z chan avg over views
     * @param leftRefAvg avg of left ref chans over views
     * @param leftRefMax max left chan value over views
     * @param rightRefAvg avg of right ref chans over views
     * @param rightRefMax max right chan value over views
     * @param allocKey Allocation Key
     * @param lastMod Date/Time of Last Change
     * @param versionCreation Genesis Version - Created
     * @param versionNow Genesis Version - Now
     * @param pixelDataSizeA PixelData size - as stored
     * @param pixelDataSizeC PixelData size - Compressed
     * @param pixelDataSizeU PixelData size - UnCompressed
     * @param checkSum AcqRecon record checksum 
     * @param archive Image Archive Flag
     * @param complete Image Complete Flag
     * @param biopsyPos Biopsy Position
     * @param boipsyTLoc Biopsy T Location
     * @param biopsyRefLoc Biopsy Ref Location
     * @param refChan Reference Channel Used
     * @param bpCoef Back Projector Coefficient
     * @param pspeedCorr Primary Speed Correction Used
     * @param overrangeCorr Overrange Correction Used
     * @param dynZAlpha Dynamic Z Alpha Value
     * @param referenceField Reference Image Field
     * @param summaryField Summary Image Field
     * @param window Window Value
     * @param level Level Value
     * @param slopInt1 Integer Slop Field 1
     * @param slopInt2 Integer Slop Field 2
     * @param slopInt3 Integer Slop Field 3
     * @param slopInt4 Integer Slop Field 4
     * @param slopInt5 Integer Slop Field 5
     * @param slopFloat1 Float Slop Field 1
     * @param slopFloat2 Float Slop Field 2
     * @param slopFloat3 Float Slop Field 3
     * @param slopFloat4 Float Slop Field 4
     * @param slopFloat5 Float Slop Field 5
     * @param slopStr1 String Slop Field 1
     * @param slopStr2 String Slop Field 2
     */
    public GECTImageInfo(byte [] suiteID, short uniqueFlag, 
			 char diskID, short examNumber, 
			 short seriesNum, short imageNum, 
			 int AllocDateTime, int ActualDateTime,
			 float scanTime, float sliceThick, 
			 short iMatrixX, short iMatrixY, 
			 float displayFieldX, 
			 float displayFieldY, 
			 float dimensionX, float dimensionY,
			 float pixelSizeX, float pixelSizeY,
			 byte [] pixelDataID,
			 byte [] contrastIV, 
			 byte [] contrastOral, 
			 short contrastMode, short seriesRX, 
			 short imageRX, short screenFormat, 
			 short plane, float scanSpacing,
			 short compression, short scoutType, 
			 char locationRAS, float imageLocation,
			 float centerR, float centerA, 
			 float centerS, float normR, 
			 float normA, float normS, 
			 float TopLeftR, float TopLeftA, 
			 float TopLeftS, float TopRightR, 
			 float TopRightA, float TopRightS, 
			 float BotRightR, float BotRightA, 
			 float BotRightS, 
			 byte [] foreignImageRev,  
			 float tableStartLoc, float tableEndLoc, 
			 float tableSpeed, float tableHeight, 
			 float midScanTime, short midScanFlag, 
			 int kvolt, int mamp, float gantryTilt,
			 int azimuth, float gantryVel, int gantryFilter,
			 float triggerOnPos, float degreeRot, 
			 float xrayOn, float xrayOff, int numTriggers, 
			 short inputViews, float angleView1, 
			 float triggerFreq, int triggerSrc, int fpaGain, 
			 int scanType, int outSrc, int adInput, 
			 int calMode, int calFreq, int regXM, 
			 int autoZero, short axialType, short phantSize, 
			 short phantType, short filterType, 
			 short reconAlg, short perisFlag, 
			 short iterboneFlag, short statFlag, 
			 short computeType, short segNumber, 
			 short segsTotal, float iscanDelay, 
			 float scanField, short scanNumber, 
			 short viewStartChan, short viewCompFactor, 
			 short outViews, short overRanges, 
			 short totalRefChan, int scanDataSize, 
			 short refChan0, short refChan1, short refChan2, 
			 short refChan3, short refChan4, short refChan5, 
			 short postProcFlag, int xmPattern, short rotType, 
			 short rawDataFlag, float ctScaleFactor, 
			 short ctWaterNumber, short ctBoneNumber, 
			 float bbhCoef1, float bbhCoef2, float bbhCoef3, 
			 short bbhNumBlend, int firstChan, int numChan, 
			 int chanIncr, int firstView, int numView, 
			 int viewIncr, int windowRange, float scaleMin, 
			 float scaleMax, int dataMode, byte [] qcalFile,
			 byte [] calModFile, short wordsPerView, char rlRAS,
			 char apRAS, char scoutStartRAS, char scoutEndRAS, 
			 byte [] scoutAnatomRef, short ppsScaleWindow, 
			 short ppsQcalFlag, short ppsPcalFlag, 
			 short ppsThetaFix, short ppsBeamFlag, 
			 short spotSize, short spotPos, 
			 short reconDataSet, short detCellField, 
			 double scanStartTime, short gantryDirection,
			 short rotorSpeed, short trigMode, float siTilt, 
			 float rReconCenter, float aReconCenter, 
			 short backProjFlag, short fatqEstFlag, float zAvg, 
			 float leftRefAvg, float leftRefMax, 
			 float rightRefAvg, float rightRefMax, 
			 byte [] allocKey, int lastMod, 
			 byte [] versionCreation, 
			 byte [] versionNow, int pixelDataSizeA, 
			 int pixelDataSizeC, int pixelDataSizeU, 
			 int checkSum, int archive, int complete, 
			 short biopsyPos, float boipsyTLoc,
			 float biopsyRefLoc, short refChan, float bpCoef,
			 short pspeedCorr, short overrangeCorr, 
			 float dynZAlpha, char referenceField,
			 char summaryField, short window,
			 short level, int slopInt1, int slopInt2, 
			 int slopInt3, int slopInt4, int slopInt5, 
			 float slopFloat1, float slopFloat2, 
			 float slopFloat3, float slopFloat4, 
			 float slopFloat5, byte [] slopStr1,
			 byte [] slopStr2 ){

	super(188, suiteID, uniqueFlag, diskID, examNumber,
				    seriesNum, imageNum, 
				    AllocDateTime, ActualDateTime,
				    scanTime, sliceThick, 
				    iMatrixX, iMatrixY, 
				    displayFieldX, 
				    displayFieldY, 
				    dimensionX, dimensionY,
				    pixelSizeX, pixelSizeY,
				    pixelDataID,
				    contrastIV, 
				    contrastOral, 
				    contrastMode, seriesRX, 
				    imageRX, screenFormat, 
				    plane, scanSpacing,
				    compression, scoutType, 
				    locationRAS, imageLocation,
				    centerR, centerA, 
				    centerS, normR, 
				    normA, normS, 
				    TopLeftR, TopLeftA, 
				    TopLeftS, TopRightR, 
				    TopRightA, TopRightS, 
				    BotRightR, BotRightA, 
				    BotRightS, 
				    foreignImageRev);

	_addElement(TABLE_START_LOC, new Float(tableStartLoc));
	_addElement(TABLE_END_LOC, new Float(tableEndLoc));
	_addElement(TABLE_SPEED, new Float(tableSpeed));
	_addElement(TABLE_HEIGHT, new Float(tableHeight));
	_addElement(MID_SCAN_TIME, new Float(midScanTime));
	_addElement(MID_SCAN_FLAG, new Short(midScanFlag));
	_addElement(KVOLT, new Integer(kvolt));
	_addElement(MAMP, new Integer(mamp));
	_addElement(GANTRY_TILT, new Float(gantryTilt));
	_addElement(AZIMUTH, new Integer(azimuth));
	_addElement(GANTRY_VEL, new Float(gantryVel));
	_addElement(GANTRY_FILTER, new Integer(gantryFilter));
	_addElement(TRIGGER_ON_POS, new Float(triggerOnPos));
	_addElement(DEGREE_ROT, new Float(degreeRot));
	_addElement(XRAY_ON, new Float(xrayOn));
	_addElement(XRAY_OFF, new Float(xrayOff));
	_addElement(NUM_TRIGGERS, new Integer(numTriggers));
	_addElement(INPUT_VIEWS, new Short(inputViews));
	_addElement(ANGLE_VIEW1, new Float(angleView1));
	_addElement(TRIGGER_FREQ, new Float(triggerFreq));
	_addElement(TRIGGER_SRC, new Integer(triggerSrc));
	_addElement(FPA_GAIN, new Integer(fpaGain));
	_addElement(SCAN_TYPE, new Integer(scanType));
	_addElement(OUT_SRC, new Integer(outSrc));
	_addElement(AD_INPUT, new Integer(adInput));
	_addElement(CAL_MODE, new Integer(calMode));
	_addElement(CAL_FREQ, new Integer(calFreq));
	_addElement(REG_XM, new Integer(regXM));
	_addElement(AUTO_ZERO, new Integer(autoZero));
	_addElement(AXIAL_TYPE, new Short(axialType));
	_addElement(PHANT_SIZE, new Short(phantSize));
	_addElement(PHANT_TYPE, new Short(phantType));
	_addElement(FILTER_TYPE, new Short(filterType));
	_addElement(RECON_ALG, new Short(reconAlg));
	_addElement(PERIS_FLAG, new Short(perisFlag));
	_addElement(ITERBONE_FLAG, new Short(iterboneFlag));
	_addElement(STAT_FLAG, new Short(statFlag));
	_addElement(COMPUTE_TYPE, new Short(computeType));
	_addElement(SEG_NUMBER, new Short(segNumber));
	_addElement(SEGS_TOTAL, new Short(segsTotal));
	_addElement(ISCAN_DELAY, new Float(iscanDelay));
	_addElement(SCAN_FIELD, new Float(scanField));
	_addElement(SCAN_NUMBER, new Short(scanNumber));
	_addElement(VIEW_START_CHAN, new Short(viewStartChan));
	_addElement(VIEW_COMP_FACTOR, new Short(viewCompFactor));
	_addElement(OUT_VIEWS, new Short(outViews));
	_addElement(OVER_RANGES, new Short(overRanges));
	_addElement(TOTAL_REF_CHAN, new Short(totalRefChan));
	_addElement(SCAN_DATA_SIZE, new Integer(scanDataSize));
	_addElement(REF_CHAN0, new Short(refChan0));
	_addElement(REF_CHAN1, new Short(refChan1));
	_addElement(REF_CHAN2, new Short(refChan2));
	_addElement(REF_CHAN3, new Short(refChan3));
	_addElement(REF_CHAN4, new Short(refChan4));
	_addElement(REF_CHAN5, new Short(refChan5));
	_addElement(POST_PROC_FLAG, new Short(postProcFlag));
	_addElement(XM_PATTERN, new Integer(xmPattern));
	_addElement(ROT_TYPE, new Short(rotType));
	_addElement(RAW_DATA_FLAG, new Short(rawDataFlag));
	_addElement(CT_SCALE_FACTOR, new Float(ctScaleFactor));
	_addElement(CT_WATER_NUMBER, new Short(ctWaterNumber));
	_addElement(CT_BONE_NUMBER, new Short(ctBoneNumber));
	_addElement(BBH_COEF1, new Float(bbhCoef1));
	_addElement(BBH_COEF2, new Float(bbhCoef2));
	_addElement(BBH_COEF3, new Float(bbhCoef3));
	_addElement(BBH_NUM_BLEND, new Short(bbhNumBlend));
	_addElement(FIRST_CHAN, new Integer(firstChan));
	_addElement(NUM_CHAN, new Integer(numChan));
	_addElement(CHAN_INCR, new Integer(chanIncr));
	_addElement(FIRST_VIEW, new Integer(firstView));
	_addElement(NUM_VIEW, new Integer(numView));
	_addElement(VIEW_INCR, new Integer(viewIncr));
	_addElement(WINDOW_RANGE, new Integer(windowRange));
	_addElement(SCALE_MIN, new Float(scaleMin));
	_addElement(SCALE_MAX, new Float(scaleMax));
	_addElement(DATA_MODE, new Integer(dataMode));
	_addElement(QCAL_FILE, _createString(qcalFile));
	_addElement(CAL_MOD_FILE, _createString(calModFile));
	_addElement(WORDS_PER_VIEW, new Short(wordsPerView));
	_addElement(RL_RAS, new Character(rlRAS));
	_addElement(AP_RAS,  new Character(apRAS));
	_addElement(SCOUT_START_RAS,  new Character(scoutStartRAS));
	_addElement(SCOUT_END_RAS,  new Character(scoutEndRAS));
	_addElement(SCOUT_ANATOM_REF, _createString(scoutAnatomRef));
	_addElement(PPS_SCALE_WINDOW, new Short(ppsScaleWindow));
	_addElement(PPS_QCAL_FLAG, new Short(ppsQcalFlag));
	_addElement(PPS_PCAL_FLAG, new Short(ppsPcalFlag));
	_addElement(PPS_THETA_FIX, new Short(ppsThetaFix));
	_addElement(PPS_BEAM_FLAG, new Short(ppsBeamFlag));
	_addElement(SPOT_SIZE, new Short(spotSize));
	_addElement(SPOT_POS, new Short(spotPos));
	_addElement(RECON_DATA_SET, new Short(reconDataSet));
	_addElement(DET_CELL_FIELD, new Short(detCellField));
	_addElement(SCAN_START_TIME, new Double(scanStartTime));
	_addElement(GANTRY_DIRECTION, new Short(gantryDirection));
	_addElement(ROTOR_SPEED, new Short(rotorSpeed));
	_addElement(TRIG_MODE, new Short(trigMode));
	_addElement(SI_TILT, new Float(siTilt));
	_addElement(R_RECON_CENTER, new Float(rReconCenter));
	_addElement(A_RECON_CENTER, new Float(aReconCenter));
	_addElement(BACK_PROJ_FLAG, new Short(backProjFlag));
	_addElement(FATQ_EST_FLAG, new Short(fatqEstFlag));
	_addElement(Z_AVG, new Float(zAvg));
	_addElement(LEFT_REF_AVG, new Float(leftRefAvg));
	_addElement(LEFT_REF_MAX, new Float(leftRefMax));
	_addElement(RIGHT_REF_AVG, new Float(rightRefAvg));
	_addElement(RIGHT_REF_MAX, new Float(rightRefMax));
	_addElement(ALLOC_KEY, _createString(allocKey));
	_addElement(LAST_MOD, new Integer(lastMod));
	_addElement(VERSION_CREATION, _createString(versionCreation));
	_addElement(VERSION_NOW, _createString(versionNow));
	_addElement(PIXEL_DATA_SIZE_A, new Integer(pixelDataSizeA));
	_addElement(PIXEL_DATA_SIZE_C, new Integer(pixelDataSizeC));
	_addElement(PIXEL_DATA_SIZE_U, new Integer(pixelDataSizeU));
	_addElement(CHECK_SUM, new Integer(checkSum));
	_addElement(ARCHIVE, new Integer(archive));
	_addElement(COMPLETE, new Integer(complete));
	_addElement(BIOPSY_POS, new Short(biopsyPos));
	_addElement(BIOPSY_T_LOC, new Float(boipsyTLoc));
	_addElement(BIOPSY_REF_LOC, new Float(biopsyRefLoc));
	_addElement(REF_CHAN, new Short(refChan));
	_addElement(BP_COEF, new Float(bpCoef));
	_addElement(PSPEED_CORR, new Short(pspeedCorr));
	_addElement(OVERRANGE_CORR, new Short(overrangeCorr));
	_addElement(DYN_Z_ALPHA, new Float(dynZAlpha));
	_addElement(REFERENCE_FIELD, new Character(referenceField));
	_addElement(SUMMARY_FIELD, new Character(summaryField));
	_addElement(WINDOW, new Short(window));
	_addElement(LEVEL, new Short(level));
	_addElement(SLOP_INT1, new Integer(slopInt1));
	_addElement(SLOP_INT2, new Integer(slopInt2));
	_addElement(SLOP_INT3, new Integer(slopInt3));
	_addElement(SLOP_INT4, new Integer(slopInt4));
	_addElement(SLOP_INT5, new Integer(slopInt5));
	_addElement(SLOP_FLOAT1, new Float(slopFloat1));
	_addElement(SLOP_FLOAT2, new Float(slopFloat2));
	_addElement(SLOP_FLOAT3, new Float(slopFloat3));
	_addElement(SLOP_FLOAT4, new Float(slopFloat4));
	_addElement(SLOP_FLOAT5, new Float(slopFloat5));
	_addElement(SLOP_STR1, _createString(slopStr1));
	_addElement(SLOP_STR2, _createString(slopStr2));
    }
}
    
