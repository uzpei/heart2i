/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge.plugin;

import edu.ucla.loni.ge.GEHeader;
import edu.ucla.loni.ge.GESubheader;
import edu.ucla.loni.ge.GESuiteInfo;
import edu.ucla.loni.ge.GEExamInfo;
import edu.ucla.loni.ge.GESeriesInfo;
import edu.ucla.loni.ge.GEImageInfo;
import edu.ucla.loni.ge.GEMRImageInfo;
import edu.ucla.loni.ge.GECTImageInfo;
import java.util.Iterator;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between the GE Header classes and
 * DOM Node representations of them.
 *
 * @version 26 June 2002
 */
public class GEMetadataConversions
{
  /** Constructs an GEMetadataConversions. */
  private GEMetadataConversions()
    {
    }

  /**
   * Converts the tree to an GE Header.
   *
   * @param root DOM Node representing the GE Header.
   *
   * @return GE Header constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static GEHeader toGEHeader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = GEMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Convert the GE Suite Info Node
      Node suiteInfoNode = root.getFirstChild();
      GESuiteInfo suiteInfo = toGESuiteInfo(suiteInfoNode);

      // Convert the GE Exam Info Node
      Node examInfoNode = suiteInfoNode.getNextSibling();
      GEExamInfo examInfo = toGEExamInfo(examInfoNode);

      // Convert the GE Series Info Node
      Node seriesInfoNode = examInfoNode.getNextSibling();
      GESeriesInfo seriesInfo = toGESeriesInfo(seriesInfoNode);

      // Convert the GE Image Info Node
      Node imageInfoNode = seriesInfoNode.getNextSibling();
      GEImageInfo imageInfo = toGEImageInfo(imageInfoNode);

      // Return a new GE Header
      return new GEHeader(suiteInfo, examInfo, seriesInfo, imageInfo);
    }

  /**
   * Converts the tree to a GE Suite Info.
   *
   * @param root DOM Node representing the GE Suite Info.
   *
   * @return GE Suite Info constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static GESuiteInfo toGESuiteInfo(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the GE Suite Info Node name
      String suiteInfoName = GEMetadataFormat.GE_SUITE_INFO_NAME;
      if ( !root.getNodeName().equals(suiteInfoName) ) {
	String msg = "Root node must be named \"" + suiteInfoName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a GE Suite Info from the Node attributes
      GESuiteInfo suiteInfo;
      try {
	NamedNodeMap atts = root.getAttributes();

	// ID of the suite ID. 
	String id = GESuiteInfo.SUITE_ID;
	byte[] suiteID = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the Make Unique Flag 
	id = GESuiteInfo.UNIQUE_FLAG;
	short uniqueFlag = Short.parseShort(  atts.getNamedItem(id).
					      getNodeValue() );
	
	// ID of the Disk ID 
	id = GESuiteInfo.DISK_ID;
	char diskID = (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];

	// ID of the Product ID 
	id = GESuiteInfo.PRODUCT_ID;
	byte[] productID = atts.getNamedItem(id).getNodeValue().getBytes();
  
	// ID of the Genesis Version of Record Created
	id = GESuiteInfo.VERSION_CR;
	byte[] versionCr = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Genesis Version of Record Currently
	id = GESuiteInfo.VERSION_CU;
	byte[] versionCu = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Suite Record Checksum 
	id = GESuiteInfo.CHECK_SUM;
	int checkSum =  Integer.parseInt(atts.getNamedItem(id).getNodeValue());


	// Construct a GE Suite Info
	suiteInfo =  new GESuiteInfo(suiteID, uniqueFlag, diskID, productID, 
				     versionCr, versionCu, checkSum);

      }

      // Unable to construct a GE Suite Info
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a GE Suite Info.";
	throw new IIOInvalidTreeException(msg, e, root);
      }

      // Return the GE Suite Info
      return suiteInfo;
    }

  /**
   * Converts the tree to an GE Exam Info.
   *
   * @param root DOM Node representing the GE Exam Info.
   *
   * @return GE Exam Info constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static GEExamInfo toGEExamInfo(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the GE Exam Info Node name
      String examInfoName = GEMetadataFormat.GE_EXAM_INFO_NAME;
      if ( !root.getNodeName().equals(examInfoName) ) {
	String msg = "Root node must be named \"" + examInfoName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an GE Exam Info from the Node attributes
      GEExamInfo examInfo;
      try {
	NamedNodeMap atts = root.getAttributes();




	// ID of the suite ID. 
	String id = GEExamInfo.SUITE_ID;
	byte [] suiteID = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Unique Flag 
	id = GEExamInfo.UNIQUE_FLAG;
	short uniqueFlag = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	
	// ID of the Disk ID for this Exam 
	id = GEExamInfo.DISK_ID;
	char diskID = (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	
	// ID of the Exam Number 
	id = GEExamInfo.EXAM_NUMBER;
	short examNumber = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	
	// ID of the Hospital Name 
	id = GEExamInfo.HOSPITAL_NAME;
	byte [] hospitalName = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Detector Type 
	id = GEExamInfo.DETECTOR;
	short detector = Short.parseShort( atts.getNamedItem(id).
					   getNodeValue() );
	
	// ID of the Number of cells in det 
	id = GEExamInfo.NUMBER_CELLS;
	int numberCells = Integer.parseInt(atts.getNamedItem(id).
					   getNodeValue());
	
	// ID of the Cell number at theta 
	id = GEExamInfo.ZERO_CELL;
	float zeroCell = Float.parseFloat(atts.getNamedItem(id).
					  getNodeValue());
	
	// ID of the Cell spacing 
	id = GEExamInfo.CELL_SPACE;
	float cellSpace = Float.parseFloat(atts.getNamedItem(id).
					   getNodeValue());
	
	// ID of the Distance from source to detector 
	id = GEExamInfo.SRC_TO_DET;
	float srcToDet = Float.parseFloat(atts.getNamedItem(id).
					  getNodeValue());
	
	// ID of the Distance from source to iso 
	id = GEExamInfo.SRC_TO_ISO;
	float srcToIso = Float.parseFloat(atts.getNamedItem(id).
					  getNodeValue());
	
	// ID of the Tube type 
	id = GEExamInfo.TUBE_TYPE;
	short tubeType = Short.parseShort( atts.getNamedItem(id).
					   getNodeValue() );
	
	// ID of the DAS type 
	id = GEExamInfo.DAS_TYPE;
	short dasType = Short.parseShort( atts.getNamedItem(id).
					  getNodeValue() );
	
	// ID of the Number of Decon Kernals 
	id = GEExamInfo.NUMBER_DECON_KERNALS;
	short numberDeconKernals = Short.parseShort( atts.getNamedItem(id).
						     getNodeValue() );
	
	// ID of the Number of elements in a Decon Kernal 
	id = GEExamInfo.DECON_LEN;
	short deconLen = Short.parseShort( atts.getNamedItem(id).
					   getNodeValue() );
	
	// ID of the Decon Kernal density 
	id = GEExamInfo.DECON_DENSITY;
	short deconDensity = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue() );
	
	// ID of the Decon Kernal stepsize 
	id = GEExamInfo.DECON_STEP_SIZE;
	short deconStepSize = Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Decon Kernal Shift Count 
	id = GEExamInfo.DECON_SHIFT_CNT;
	short deconShiftCnt = Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Magnet strength (in gauss) 
	id = GEExamInfo.MAG_STRENGTH;
	int magStrength = Integer.parseInt(atts.getNamedItem(id).
					   getNodeValue());
	    
	// ID of the Patient ID for this Exam 
	id = GEExamInfo.PATIENT_ID;
	byte [] patientID = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Patient Name 
	id = GEExamInfo.PATIENT_NAME;
	byte [] patientName = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Patient Age (years, months or days) 
	id = GEExamInfo.PATIENT_AGE;
	short patientAge = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	
	// ID of the Patient Age Notation 
	id = GEExamInfo.PATIENT_AGE_NOT;
	short patientAgeNot = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	    
	// ID of the Patient Sex 
	id = GEExamInfo.PATIENT_SEX;
	short patientSex = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	    
	// ID of the Patient Weight 
	id = GEExamInfo.PATIENT_WEIGHT;
	int patientWeight = Integer.parseInt(atts.getNamedItem(id).
					   getNodeValue());
	    
	// ID of the Trauma Flag 
	id = GEExamInfo.TRAUMA;
	short trauma = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	    
	// ID of the Patient History 
	id = GEExamInfo.PATIENT_HISTORY;
	byte [] patientHistory = atts.getNamedItem(id).getNodeValue().
	                                               getBytes();

	// ID of the Requisition Number 
	id = GEExamInfo.REQ_NUMBER;
	byte [] reqNumber = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Exam date/time stamp 
	id = GEExamInfo.EXAM_DATE;
	int examDate = Integer.parseInt(atts.getNamedItem(id).
					   getNodeValue());
	    
	// ID of the Referring Physician 
	id = GEExamInfo.REF_PHYSICIAN;
	byte [] refPhysician = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Diagnostician/Radiologist 
	id = GEExamInfo.DIAGRAD;
	byte [] diagrad = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Operator 
	id = GEExamInfo.OPERATOR;
	byte [] operator =atts.getNamedItem(id).getNodeValue().getBytes();
	  
	// ID of the Exam Description 
	id = GEExamInfo.EXAM_DESCRIPTION;
	byte [] examDescription = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Exam Type 
	id = GEExamInfo.EXAM_TYPE;
	byte [] examType = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Exam Format 
	id = GEExamInfo.EXAM_FORMAT;
	short examFormat = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	
	// ID of the Start time(secs) of first axial in exam 
	id = GEExamInfo.FIRST_AXIAL_TIME;
	double firstAxialTime = Double.parseDouble(atts.getNamedItem(id).
						   getNodeValue());
	
	// ID of the Creator Suite and Host 
	id = GEExamInfo.HOST;
	byte [] host = atts.getNamedItem(id).getNodeValue().getBytes();
	  
	// ID of the Date/Time of Last Change 
	id = GEExamInfo.LAST_MOD;
	int lastMod = Integer.parseInt(atts.getNamedItem(id).
				       getNodeValue());
	
	// ID of the Non-Zero indicates Protocol Exam 
	id = GEExamInfo.PROTOCOL_FLAG;
	short protocolFlag = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue() );
	
	// ID of the Process that allocated this record 
	id = GEExamInfo.ALLOC_KEY;
	byte [] allocKey = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Indicates number of updates to header 
	id = GEExamInfo.UPDATA_CNT;
	int updataCnt = Integer.parseInt(atts.getNamedItem(id).
					 getNodeValue());
	
	// ID of the Genesis Version - Created 
	id = GEExamInfo.VERSION_CREATION;
	byte [] versionCreation = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Genesis Version - Now 
	id = GEExamInfo.VERSION_NOW;
	byte [] versionNow = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Exam Record Checksum 
	id = GEExamInfo.CHECK_SUM;
	int checkSum = Integer.parseInt(atts.getNamedItem(id).
					getNodeValue());
	
	// ID of the Exam Complete Flag 
	id = GEExamInfo.COMPLETE_FLAG;
	int completeFlag = Integer.parseInt(atts.getNamedItem(id).
					   getNodeValue());
	
	// ID of the Last Series Number Used 
	id = GEExamInfo.SERIES_CNT;
	int seriesCnt = Integer.parseInt(atts.getNamedItem(id).
					 getNodeValue());
	
	// ID of the Number of Series Archived 
	id = GEExamInfo.NUMBER_ARCHIVED;
	int numberArchived = Integer.parseInt(atts.getNamedItem(id).
					      getNodeValue());
	
	// ID of the Number of Series Existing 
	id = GEExamInfo.NUMBER_SERIES;
	int numberSeries = Integer.parseInt(atts.getNamedItem(id).
					    getNodeValue());
	
	// ID of the Number Series Keys for this Exam 
	id = GEExamInfo.SERIES_KEY_LEN;
	int seriesKeyLen = Integer.parseInt(atts.getNamedItem(id).
					    getNodeValue());
	
	// ID of the Series Keys for this Exam 
	id = GEExamInfo.SERIES_KEY;
	byte [] seriesKey = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Number Unstored Series for this Exam 
	id = GEExamInfo.NUMBER_UNST_SERIES;
	int numberUnstSeries = Integer.parseInt(atts.getNamedItem(id).
						getNodeValue());

	// ID of the Number of Unstored Series Keys for this Exam 
	id = GEExamInfo.UNST_SERIES_KEY_LEN;
	int unstSeriesKeyLen = Integer.parseInt(atts.getNamedItem(id).
						getNodeValue());
	
	// ID of the Unstored Series Keys for this Exam 
	id = GEExamInfo.UNST_SERIES_KEY;
	byte [] unstSeriesKey = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Number of Unarchived Series for this Exam 
	id = GEExamInfo.NUMBER_UNARCH_SERIES;
	int numberUnarchSeries = Integer.parseInt(atts.getNamedItem(id).
						  getNodeValue());
	
	// ID of the Number of Unarchived Series Keys for this Exam 
	id = GEExamInfo.UNARCH_SERIES_KEY_LEN;
	int unarchSeriesKeyLen = Integer.parseInt(atts.getNamedItem(id).
						  getNodeValue());
	
	// ID of the Unarchived Series Keys for this Exam 
	id = GEExamInfo.UNARCH_SERIES_KEY;
	byte [] unarchSeriesKey = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Number Prospective/Scout Series for this Exam 
	id = GEExamInfo.NUMBER_PROS_SERIES;
	int numberProsSeries = Integer.parseInt(atts.getNamedItem(id).
						getNodeValue());
	
	// ID of the Number Prospective/Scout Series Keys for this Exam 
	id = GEExamInfo.PROS_SERIES_KEY_LEN;
	int prosSeriesKeyLen = Integer.parseInt(atts.getNamedItem(id).
						getNodeValue());
	
	// ID of the Prospective/Scout Series Keys for this Exam 
	id = GEExamInfo.PROS_SERIES_KEY;
	byte [] prosSeriesKey = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Last Model Number used 
	id = GEExamInfo.MODEL_NUMBER;
	int  modelNumber = Integer.parseInt(atts.getNamedItem(id).
					    getNodeValue());
	
	// ID of the Number of ThreeD Models 
	id = GEExamInfo.MODEL_CNT;
	int modelCnt = Integer.parseInt(atts.getNamedItem(id).
					getNodeValue());
	
	// ID of the Number ThreeD Model Keys for Exam 
	id = GEExamInfo.MODEL_KEY_LEN;
	int modelKeyLen = Integer.parseInt(atts.getNamedItem(id).
					   getNodeValue());
	
	// ID of the ThreeD Model Keys for Exam 
	id = GEExamInfo.MODEL_KEY;
	byte [] modelKey = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Patient Status 
	id = GEExamInfo.PATIENT_STATUS;
	short patientStatus = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	
	// ID of the Unique System ID 
	id = GEExamInfo.SYSTEM_ID;	 
	byte [] systemID = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Unique Service ID 
	id = GEExamInfo.SERVICE_ID;
	byte [] serviceID = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Mobile Location Number 
	id = GEExamInfo.MOBLE_LOCATION;
	int mobleLocation = Integer.parseInt(atts.getNamedItem(id).
					     getNodeValue());



	// Construct an GE Exam Info
	examInfo = new GEExamInfo( suiteID, uniqueFlag, 
				    diskID, examNumber,  hospitalName, 
				    detector, numberCells, zeroCell,
				    cellSpace, srcToDet, srcToIso,
				    tubeType, dasType, 
				    numberDeconKernals, deconLen, 
				    deconDensity, deconStepSize, 
				    deconShiftCnt, magStrength, 
				    patientID,  patientName, 
				    patientAge, patientAgeNot, 
				    patientSex, patientWeight, 
				    trauma,  patientHistory, 
				    reqNumber, examDate, 
				    refPhysician,  diagrad, 
				    operator,  examDescription, 
				    examType, examFormat, 
				    firstAxialTime,  host, 
				    lastMod, protocolFlag, 
				    allocKey, updataCnt, 
				    versionCreation,  versionNow, 
				    checkSum, completeFlag, 
				    seriesCnt, numberArchived, 
				    numberSeries, seriesKeyLen, 
				    seriesKey, numberUnstSeries, 
				    unstSeriesKeyLen,  unstSeriesKey, 
				    numberUnarchSeries, 
				    unarchSeriesKeyLen, 
				    unarchSeriesKey, numberProsSeries, 
				    prosSeriesKeyLen,  prosSeriesKey, 
				    modelNumber, modelCnt, 
				    modelKeyLen,  modelKey, 
				    patientStatus,  systemID, 
				    serviceID, mobleLocation);
      }

      // Unable to construct an GE Exam Info
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an GE Exam Info.";
	throw new  IIOInvalidTreeException(msg, e, root);
      }

      // Return the GE Exam Info
      return examInfo;
    }

  /**
   * Converts the tree to an GE Series Info.
   *
   * @param root DOM Node representing the GE Series Info.
   *
   * @return GE Series Info constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static GESeriesInfo toGESeriesInfo(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the GE Series Info Node name
      String seriesInfoName = GEMetadataFormat.GE_SERIES_INFO_NAME;
      if ( !root.getNodeName().equals(seriesInfoName) ) {
	String msg = "Root node must be named \"" + seriesInfoName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an GE Series Info from the Node attributes
      GESeriesInfo seriesInfo;
      try {
	NamedNodeMap atts = root.getAttributes();

	// ID of the suite ID. 
	String id = GESeriesInfo.SUITE_ID;
	byte [] suiteID = atts.getNamedItem(id).getNodeValue().getBytes();
	

	// ID of the Unique Flag. 
	id = GESeriesInfo.UNIQUE_FLAG;
	short uniqueFlag = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );
	
	// ID of the Disk ID for this Series. 
	id = GESeriesInfo.DISK_ID;
	char diskID =  (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	
	// ID of the Exam Number. 
	id = GESeriesInfo.EXAM_NUMBER;
	short examNumber =  Short.parseShort( atts.getNamedItem(id).
					      getNodeValue() );
	
	// ID of the Series Number. 
	id = GESeriesInfo.SERIES_NUMBER;
	short seriesNumber =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Allocation Series Data/Time stamp. 
	id = GESeriesInfo.ALLOC_TIME_STAMP;
	int allocTimeStamp =  Integer.parseInt(atts.getNamedItem(id).
					       getNodeValue() );
	
	// ID of the Actual Series Data/Time stamp. 
	id = GESeriesInfo.ACTUAL_TIME_STAMP;
	int actualTimeStamp =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Series Description. 
	id = GESeriesInfo.SERIES_DESCRIPTION;
	byte [] seriesDescription = atts.getNamedItem(id).getNodeValue().
						getBytes(); 
	
	// ID of the Primary Receiver Suite and Host. 
	id = GESeriesInfo.PRIMARY_RECIEVER_HOST;
	byte [] primaryRecieverHost = atts.getNamedItem(id).getNodeValue().
						getBytes();
	
	// ID of the Archiver Suite and Host. 
	id = GESeriesInfo.ARCHIVER_HOST;
	byte [] archiverHost = atts.getNamedItem(id).getNodeValue().
						getBytes();
	    
	// ID of the Series Type. 
	id = GESeriesInfo.SERIES_TYPE;
	short seriesType =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Series from which prescribed. 
	id = GESeriesInfo.SERIES_SOURCE;
	short seriesSource =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Most-like Plane (for L/S). 
	id = GESeriesInfo.PLANE;
	short plane =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Scout or Axial (for CT). 
	id = GESeriesInfo.SCAN_TYPE;
	short scanType =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Patient Position. 
	id = GESeriesInfo.POSITION;
	int position =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Patient Entry. 
	id = GESeriesInfo.ENTRY;
	int entry =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Anatomical reference. 
	id = GESeriesInfo.ANATOM_REFERENCE;
	byte [] anatomReference = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Horizontal Landmark. 
	id = GESeriesInfo.HORIZONTAL_LANDMARK;
	float horizontalLandmark = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Scan Protocol Name. 
	id = GESeriesInfo.PROTOCOL_NAME;
	byte [] protocolName = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Non-zero if > 0 image used contrast(L/S). 
	id = GESeriesInfo.CONTRAST;
	short contrast =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the RAS letter for first scan location (L/S). 
	id = GESeriesInfo.START_RAS;
	char startRas =  (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	
	// ID of the First scan location (L/S). 
	id = GESeriesInfo.START_LOC;
	float startLoc =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the RAS letter for last scan location (L/S). 
	id = GESeriesInfo.END_RAS;
	char endRas =  (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	
	// ID of the Last scan location (L/S). 
	id = GESeriesInfo.END_LOC;
	float endLoc =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Last Pulse Sequence Used (L/S). 
	id = GESeriesInfo.PULSE_SEQUENCE;
	short pulseSequence =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Image Sort Order (L/S). 
	id = GESeriesInfo.SORT_ORDER;
	short sortOrder = Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Landmark Counter. 
	id = GESeriesInfo.LANDMARK_CNTR;
	int landmarkCntr =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Number of Acquisitions. 
	id = GESeriesInfo.NUMBER_AQUISITIONS;
	short numberAquisitions =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Starting number for baselines. 
	id = GESeriesInfo.BASELINE_START;
	short baselineStart =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Ending number for baselines. 
	id = GESeriesInfo.BASELINE_END;
	short baselineEnd =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Starting number for enhanced scans. 
	id = GESeriesInfo.ENHANCED_START;
	short enhancedStart =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Ending number for enhanced scans. 
	id = GESeriesInfo.ENHANCED_END;
	short enhancedEnd =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Date/Time of Last Change. 
	id = GESeriesInfo.LAST_MOD;
	int lastMod =  Integer.parseInt(atts.getNamedItem(id).getNodeValue() );
	
	// ID of the Process that allocated this record. 
	id = GESeriesInfo.ALLOC_KEY;
	byte [] allocKey = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Indicates number of updates to header. 
	id = GESeriesInfo.HEADER_UPDATE_CNT;
	int headerUpdateCnt =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Genesis Version - Created. 
	id = GESeriesInfo.VERSION_CREATED;
	byte [] versionCreated = atts.getNamedItem(id).getNodeValue().
	    getBytes();
	
	// ID of the Genesis Version - Now. 
	id = GESeriesInfo.VERSION_NOW;
	byte [] versionNow = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the PixelData size - as stored. 
	id = GESeriesInfo.STORED_PIXEL_SIZE;
	float storedPixelSize =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the PixelData size - Compressed. 
	id = GESeriesInfo.COMP_PIXEL_DATA_SIZE;
	float compPixelDataSize =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the PixelData size - UnCompressed. 
	id = GESeriesInfo.UNCOMP_PIXEL_DATA_SIZE;
	float uncompPixelDataSize = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Series Record checksum. 
	id = GESeriesInfo.CHECK_SUM;
	int checkSum = Integer.parseInt(atts.getNamedItem(id).getNodeValue()); 
	
	// ID of the Series Complete Flag. 
	id = GESeriesInfo.COMPLETE;
	int complete = Integer.parseInt(atts.getNamedItem(id).getNodeValue());
	
	// ID of the Number of Images Archived. 
	id = GESeriesInfo.NUM_ARCHIVED;
	int numArchived =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Last Image Number Used. 
	id = GESeriesInfo.LAST_IMAGE;
	int lastImage = Integer.parseInt(atts.getNamedItem(id).getNodeValue());
	
	// ID of the Number of Images Existing. 
	id = GESeriesInfo.NUMBER_IMAGES;
	int numberImages = Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Size of Image Key for this Series. 
	id = GESeriesInfo.IMAGE_KEY_LEN;
	int imageKeyLen =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Image Keys for this Series. 
	id = GESeriesInfo.IMAGE_KEY;
	byte [] imageKey = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the Number of Unstored Images. 
	id = GESeriesInfo.NUMBER_UNST_IMAGES;
	int numberUnstImages =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Size of Unstored Image Keys for this Series. 
	id = GESeriesInfo.UNST_IMAGE_KEY_LEN;
	int unstImageKeyLen =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Unstored Image Keys for this Series. 
	id = GESeriesInfo.UNST_IMAGE_KEY;
	byte [] unstImageKey = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Number of Unarchived Images. 
	id = GESeriesInfo.NUMBER_UNARCH_IMAGES;
	int numberUnarchImages =  Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Size of Unarchived Image Keys for this Series. 
	id = GESeriesInfo.UNARCH_IMAGE_KEY_LEN;
	int unarchImageKeyLen = Integer.parseInt(atts.getNamedItem(id).
						getNodeValue() ); 
	
	// ID of the Unarchived Image Keys for this Series. 
	id = GESeriesInfo.UNARCH_IAMGE_KEY;
	byte [] unarchImageKey = atts.getNamedItem(id).getNodeValue().
	    getBytes();

	// ID of the Echo 1 Alpha Value. 
	id = GESeriesInfo.ECHO1_ALPHA;
	float echo1Alpha = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() ); 
	
	// ID of the Echo 1 Beta Value. 
	id = GESeriesInfo.ECHO1_BETA;
	float echo1Beta =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 1 Window Value. 
	id = GESeriesInfo.ECHO1_WINDOW;
	short echo1Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 1 Level Value. 
	id = GESeriesInfo.ECHO1_LEVEL;
	short echo1Level =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 2 Alpha Value. 
	id = GESeriesInfo.ECHO2_ALPHA;
	float echo2Alpha = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() ); 

	// ID of the Echo 2 Beta Value. 
	id = GESeriesInfo.ECHO2_BETA;
	float echo2Beta =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Echo 2 Window Value. 
	id = GESeriesInfo.ECHO2_WINDOW;
	short echo2Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 2 Level Value. 
	id = GESeriesInfo.ECHO2_LEVEL;
	short echo2Level =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );

	// ID of the Echo 3 Alpha Value. 
	id = GESeriesInfo.ECHO3_ALPHA;
	float echo3Alpha = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() ); 
	
	// ID of the Echo 3 Beta Value. 
	id = GESeriesInfo.ECHO3_BETA;
	float echo3Beta =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 3 Window Value. 
	id = GESeriesInfo.ECHO3_WINDOW;
	short echo3Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	    
	// ID of the Echo 3 Level Value. 
	id = GESeriesInfo.ECHO3_LEVEL;
	short echo3Level =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 4 Alpha Value. 
	id = GESeriesInfo.ECHO4_ALPHA;
	float echo4Alpha = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() ); 
	
	// ID of the Echo 4 Beta Value. 
	id = GESeriesInfo.ECHO4_BETA;
	float echo4Beta =Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );  
	
	// ID of the Echo 4 Window Value. 
	id = GESeriesInfo.ECHO4_WINDOW;
	short echo4Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	    
	// ID of the Echo 4 Level Value. 
	id = GESeriesInfo.ECHO4_LEVEL;
	short echo4Level =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 5 Alpha Value. 
	id = GESeriesInfo.ECHO5_ALPHA;
	float echo5Alpha =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 5 Beta Value. 
	id = GESeriesInfo.ECHO5_BETA;
	float echo5Beta =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 5 Window Value. 
	id = GESeriesInfo.ECHO5_WINDOW;
	short echo5Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 5 Level Value. 
	id = GESeriesInfo.ECHO5_LEVEL;
	short echo5Level =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 6 Alpha Value. 
	id = GESeriesInfo.ECHO6_ALPHA;
	float echo6Alpha =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 6 Beta Value. 
	id = GESeriesInfo.ECHO6_BETA;
	float echo6Beta =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 6 Window Value. 
	id = GESeriesInfo.ECHO6_WINDOW;
	short echo6Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 6 Level Value. 
	id = GESeriesInfo.ECHO6_LEVEL;
	short echo6Level = Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 7 Alpha Value. 
	id = GESeriesInfo.ECHO7_ALPHA;
	float echo7Alpha =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 7 Beta Value. 
	id = GESeriesInfo.ECHO7_BETA;
	float echo7Beta = Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 7 Window Value. 
	id = GESeriesInfo.ECHO7_WINDOW;
	short echo7Window = Short.parseShort( atts.getNamedItem(id).
						getNodeValue() ); 
	
	// ID of the Echo 7 Level Value. 
	id = GESeriesInfo.ECHO7_LEVEL;
	short echo7Level =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 8 Alpha Value. 
	id = GESeriesInfo.ECHO8_ALPHA;
	float echo8Alpha =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 8 Beta Value. 
	id = GESeriesInfo.ECHO8_BETA;
	float echo8Beta =  Float.parseFloat(atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 8 Window Value. 
	id = GESeriesInfo.ECHO8_WINDOW;
	short echo8Window =  Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	
	// ID of the Echo 8 Level Value. 
	id = GESeriesInfo.ECHO8_LEVEL;
	short echo8Level = Short.parseShort( atts.getNamedItem(id).
						getNodeValue() );
	

	// Construct an GE Series Info
	seriesInfo = new GESeriesInfo( suiteID, uniqueFlag, diskID, 
					examNumber, seriesNumber, 
					allocTimeStamp, actualTimeStamp, 
					seriesDescription, 
					primaryRecieverHost,  archiverHost, 
					seriesType, seriesSource, plane, 
					scanType, position, entry, 
					anatomReference, horizontalLandmark,
					protocolName, contrast, 
					startRas, startLoc, endRas, 
					endLoc, pulseSequence, sortOrder,
					landmarkCntr, numberAquisitions, 
					baselineStart, baselineEnd, 
					enhancedStart, enhancedEnd, 
					lastMod,  allocKey, 
					headerUpdateCnt,  versionCreated, 
					versionNow, storedPixelSize, 
					compPixelDataSize, uncompPixelDataSize,
					checkSum, complete, numArchived, 
					lastImage, numberImages,
					imageKeyLen,  imageKey, 
					numberUnstImages, unstImageKeyLen, 
					unstImageKey, numberUnarchImages, 
					unarchImageKeyLen,  unarchImageKey, 
					echo1Alpha, echo1Beta, 
					echo1Window, echo1Level, 
					echo2Alpha, echo2Beta, 
					echo2Window, echo2Level, 
					echo3Alpha, echo3Beta, 
					echo3Window, echo3Level, 
					echo4Alpha, echo4Beta, 
					echo4Window, echo4Level, 
					echo5Alpha, echo5Beta, 
					echo5Window, echo5Level, 
					echo6Alpha, echo6Beta, 
					echo6Window, echo6Level,
					echo7Alpha, echo7Beta,
					echo7Window, echo7Level, 
					echo8Alpha, echo8Beta, 
					echo8Window, echo8Level);
      }

      // Unable to construct an GE Series Info
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an GE Series Info.";
	throw new  IIOInvalidTreeException(msg, e, root);
      }

      // Return the GE Series Info
      return seriesInfo;
    }

  /**
   * Converts the tree to an GE Image Info.
   *
   * @param root DOM Node representing the GE MR Image Info.
   *
   * @return GE Image Info constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static GEImageInfo toGEImageInfo(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Attempt to construct an GE Image Info from the Node attributes
      GEImageInfo imageInfo;
      try {
	NamedNodeMap atts = root.getAttributes();

	// ID of the suite ID. 
	String id = GEImageInfo.SUITE_ID;
	byte [] suiteID = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the Unique Flag. 
	id = GEImageInfo.UNIQUE_FLAG;
	short uniqueFlag = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue() );

	// ID of the Disk ID for this Image. 
	id = GEImageInfo.DISK_ID;
	char diskID = (char) (atts.getNamedItem(id).
					     getNodeValue().getBytes())[0];
	
	// ID of the Exam number for this image. 
	id = GEImageInfo.EXAM_NUMBER;
	short examNumber = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Series Number for this image. 
	id = GEImageInfo.SERIES_NUM;
	short seriesNum = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image Number. 
	id = GEImageInfo.IMAGE_NUM;
	short imageNum = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Allocation Image date/time stamp. 
	id = GEImageInfo.ALLOC_DATE_TIME;
	int AllocDateTime = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Actual Image date/time stamp. 
	id = GEImageInfo.ACTUAL_DATE_TIME;
	int ActualDateTime = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Duration of scan (secs). 
	id = GEImageInfo.SCAN_TIME;
	float scanTime = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Slice Thickness (mm). 
	id = GEImageInfo.SLICE_THICK;
	float sliceThick = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image matrix size - X. 
	id = GEImageInfo.I_MATRIX_X;
	short iMatrixX = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());

	// ID of the Image matrix size - Y. 
	id = GEImageInfo.I_MATRIX_Y;
	short iMatrixY = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Display field of view - X (mm). 
	id = GEImageInfo.DISPLAY_FIELD_X;
	float displayFieldX = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Display field of view - Y (if different). 
	id = GEImageInfo.DISPLAY_FIELD_Y;
	float displayFieldY = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image dimension - X. 
	id = GEImageInfo.DIMENSION_X;
	float dimensionX = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image dimension - Y. 
	id = GEImageInfo.DIMENSION_Y;
	float dimensionY = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image pixel size - X. 
	id = GEImageInfo.PIXEL_SIZE_X;
	float pixelSizeX = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image pixel size - Y. 
	id = GEImageInfo.PIXEL_SIZE_Y;
	float pixelSizeY = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());

	// ID of the Pixel Data ID. 
	id = GEImageInfo.PIXEL_DATA_ID;
	byte [] pixelDataID = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the IV Contrast Agent. 
	id = GEImageInfo.CONTRAST_IV;
	byte [] contrastIV = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Oral Contrast Agent. 
	id = GEImageInfo.CONTRAST_ORAL;
	byte [] contrastOral = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Image Contrast Mode. 
	id = GEImageInfo.CONTRAST_MODE;
	short contrastMode = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Series from which prescribed. 
	id = GEImageInfo.SERIES_RX;
	short seriesRX = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image from which prescribe. 
	id = GEImageInfo.IMAGE_RX;
	short imageRX = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Screen Format(8/16 bit). 
	id = GEImageInfo.SCREEN_FORMAT;
	short screenFormat = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Plane Type. 
	id = GEImageInfo.PLANE;
	short plane = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Spacing between scans (mm?). 
	id = GEImageInfo.SCAN_SPACING;
	float scanSpacing = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Image compression type for allocation. 
	id = GEImageInfo.COMPRESSION;
	short compression = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());

	// ID of the Scout Type (AP or lateral). 
	id = GEImageInfo.SCOUT_TYPE;
	short scoutType = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the RAS letter of image location. 
	id = GEImageInfo.LOCATION_RAS;
	char locationRAS = (char) (atts.getNamedItem(id).
					     getNodeValue().getBytes())[0];
	
	// ID of the Image location. 
	id = GEImageInfo.IMAGE_LOCATION;
	float imageLocation = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	// ID of the Center R coord of plane image. 
	id = GEImageInfo.CENTER_R;
	float centerR = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Center A coord of plane image. 
	id = GEImageInfo.CENTER_A;
	float centerA = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());

	// ID of the Center S coord of plane image. 
	id = GEImageInfo.CENTER_S;
	float centerS = Float.parseFloat( atts.getNamedItem(id).
					  getNodeValue());
	    
	// ID of the Normal R coord. 
	id = GEImageInfo.NORM_R;
	float normR = Float.parseFloat( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Normal A coord. 
	id = GEImageInfo.NORM_A;
	float normA = Float.parseFloat( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Normal S coord. 
	id = GEImageInfo.NORM_S;
	float normS = Float.parseFloat( atts.getNamedItem(id).getNodeValue());
	
	// ID of the R Coord of Top Left Hand Corner. 
	id = GEImageInfo.TOP_LEFT_R;
	float TopLeftR = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	// ID of the A Coord of Top Left Hand Corner. 
	id = GEImageInfo.TOP_LEFT_A;
	float TopLeftA = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the S Coord of Top Left Hand Corner. 
	id = GEImageInfo.TOP_LEFT_S;
	float TopLeftS = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the R Coord of Top Right Hand Corner. 
	id = GEImageInfo.TOP_RIGHT_R;
	float TopRightR = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the A Coord of Top Right Hand Corner. 
	id = GEImageInfo.TOP_RIGHT_A;
	float TopRightA = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the S Coord of Top Right Hand Corner. 
	id = GEImageInfo.TOP_RIGHT_S;
	float TopRightS = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the R Coord of Bottom Right Hand Corner. 
	id = GEImageInfo.BOT_RIGHT_R;
	float BotRightR = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the A Coord of Bottom Right Hand Corner. 
	id = GEImageInfo.BOT_RIGHT_A;
	float BotRightA = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the S Coord of Bottom Right Hand Corner. 
	id = GEImageInfo.BOT_RIGHT_S;
	float BotRightS = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Foreign Image Revision. 
	id = GEImageInfo.FOREIGN_IMAGE_REV;
	byte [] foreignImageRev = atts.getNamedItem(id).getNodeValue()
					     .getBytes();
	
	// ID of the Allocation Key. 
	id = GEImageInfo.ALLOC_KEY;
	byte [] allocKey = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the Date/Time of Last Change. 
	id = GEImageInfo.LAST_MOD;
	int lastMod = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Genesis Version - Created. 
	id = GEImageInfo.VERSION_CREATION;
	byte [] versionCreation = atts.getNamedItem(id).getNodeValue().
					     getBytes();
	
	// ID of the Genesis Version - Now. 
	id = GEImageInfo.VERSION_NOW;
	byte [] versionNow = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the PixelData size - as stored. 
	id = GEImageInfo.PIXEL_DATA_SIZE_A;
	int pixelDataSizeA = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the PixelData size - Compressed. 
	id = GEImageInfo.PIXEL_DATA_SIZE_C;
	int pixelDataSizeC = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	    
	// ID of the PixelData size - UnCompressed. 
	id = GEImageInfo.PIXEL_DATA_SIZE_U;
	int pixelDataSizeU = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the AcqRecon record checksum. 
	id = GEImageInfo.CHECK_SUM;
	int checkSum = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Image Archive Flag. 
	id = GEImageInfo.ARCHIVE;
	int archive = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Image Complete Flag. 
	id = GEImageInfo.COMPLETE;
	int complete = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Reference Image Field. 
	id = GEImageInfo.REFERENCE_FIELD;
	char referenceField = (char)(atts.getNamedItem(id).
					     getNodeValue().getBytes())[0];
	
	// ID of the Summary Image Field. 
	id = GEImageInfo.SUMMARY_FIELD;
	char summaryField = (char)(atts.getNamedItem(id).
					     getNodeValue().getBytes())[0];
	
	// ID of the Window Value. 
	id = GEImageInfo.WINDOW;
	short window = Short.parseShort( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Level Value. 
	id = GEImageInfo.LEVEL;
	short level = Short.parseShort( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Integer Slop Field 1. 
	id = GEImageInfo.SLOP_INT1;
	int slopInt1 = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Integer Slop Field 2. 
	id = GEImageInfo.SLOP_INT2;
	int slopInt2 = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
    
	// ID of the Integer Slop Field 3. 
	id = GEImageInfo.SLOP_INT3;
	int slopInt3 = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
	
	// ID of the Integer Slop Field 4. 
	id = GEImageInfo.SLOP_INT4;
	int slopInt4 = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
    
	// ID of the Integer Slop Field 5. 
	id = GEImageInfo.SLOP_INT5;
	int slopInt5 = Integer.parseInt( atts.getNamedItem(id).getNodeValue());
    
	// ID of the Float Slop Field 1. 
	id = GEImageInfo.SLOP_FLOAT1;
	float slopFloat1 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Float Slop Field 1. 
	id = GEImageInfo.SLOP_FLOAT2;
	float slopFloat2 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Float Slop Field 1. 
	id = GEImageInfo.SLOP_FLOAT3;
	float slopFloat3 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Float Slop Field 1. 
	id = GEImageInfo.SLOP_FLOAT4;
	float slopFloat4 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the Float Slop Field 1. 
	id = GEImageInfo.SLOP_FLOAT5;
	float slopFloat5 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	// ID of the String Slop Field 1. 
	id = GEImageInfo.SLOP_STR1;
	byte [] slopStr1 = atts.getNamedItem(id).getNodeValue().getBytes();
	
	// ID of the String Slop Field 2. 
	id = GEImageInfo.SLOP_STR2;
	byte [] slopStr2 = atts.getNamedItem(id).getNodeValue().getBytes();
	
	
	//	Node imageInfoNode = root.getFirstChild();
	//atts = imageInfoNode.getAttributes();
       
	// Check that the Node name matches the GE MR or CT Image Info name
	String mrImageInfoName = GEMetadataFormat.GE_MR_IMAGE_INFO_NAME;
	String ctImageInfoName = GEMetadataFormat.GE_CT_IMAGE_INFO_NAME;
	if ( root.getNodeName().equals(mrImageInfoName) ) {

	    // ID of the Pulse repetition time(usec). 
	    id = GEMRImageInfo.REP_TIME;
	    int repTime = Integer.parseInt( atts.getNamedItem(id).
					    getNodeValue());
	
	    // ID of the Pulse inversion time(usec). 
	    id = GEMRImageInfo.INV_TIME;
	    int invTime = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	
	    // ID of the Pulse echo time(usec). 
	    id = GEMRImageInfo.ECHO_TIME;
	    int echoTime = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Second echo echo (usec). 
	    id = GEMRImageInfo.ECHO_TIME2;
	    int echoTime2 = Integer.parseInt(atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Number of echoes. 
	    id = GEMRImageInfo.NUM_ECHO;
	    short numEcho = Short.parseShort(atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Echo Number. 
	    id = GEMRImageInfo.ECHO_NUMBER;
	    short echoNumber = Short.parseShort( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Table Delta. 
	    id = GEMRImageInfo.TABLE_DELTA;
	    float tableDelta = Float.parseFloat( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Number of Excitations. 
	    id = GEMRImageInfo.NUM_EXCITATIONS;
	    float numExcitations = Float.parseFloat( atts.getNamedItem(id).
						     getNodeValue());
	    
	    // ID of the Continuous Slices Flag. 
	    id = GEMRImageInfo.CONTINUOUS;
	    short continuous = Short.parseShort( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Cardiac Heart Rate (bpm). 
	    id = GEMRImageInfo.HEART_RATE;
	    short heartRate = Short.parseShort( atts.getNamedItem(id).
						getNodeValue());
	
	    // ID of the Delay time after trigger (usec). 
	    id = GEMRImageInfo.TIME_DELAY;
	    int timeDelay = Integer.parseInt( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Average SAR. 
	    id = GEMRImageInfo.AVERAGE_SAR;
	    float averageSAR = Float.parseFloat( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Peak SAR. 
	    id = GEMRImageInfo.PEAK_SAR;
	    float peakSAR = Float.parseFloat( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Monitor SAR flag. 
	    id = GEMRImageInfo.MONITOR_SAR;
	    short monitorSAR = Short.parseShort( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Trigger window (% of R-R interval). 
	    id = GEMRImageInfo.TRIGGER_WINDOW;
	    short triggerWindow = Short.parseShort( atts.getNamedItem(id).
						    getNodeValue());
	    
	    // ID of the Cardiac repetition time. 
	    id = GEMRImageInfo.CARD_REP_TIME;
	    float cardRepTime = Float.parseFloat( atts.getNamedItem(id).
						  getNodeValue());
	    
	    // ID of the Images per cardiac cycle. 
	    id = GEMRImageInfo.IMAGES_PER_CYCLE;
	    short imagesPerCycle = Short.parseShort( atts.getNamedItem(id).
						     getNodeValue());
	    
	    // ID of the Actual Transmit Gain (.1 db). 
	    id = GEMRImageInfo.TRANSMIT_GAIN;
	    short transmitGain = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Actual Receive Gain Analog (.1 db). 
	    id = GEMRImageInfo.RECV_GAIN_ANALOG;
	    short recvGainAnalog = Short.parseShort( atts.getNamedItem(id).
						     getNodeValue());
	    
	    // ID of the Actual Receive Gain Digital (.1 db). 
	    id = GEMRImageInfo.RECV_GAIN_DIGITAL;
	    short recvGainDigital = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Flip Angle for GRASS scans (deg.). 
	    id = GEMRImageInfo.FLIP_ANGLE;
	    short flipAngle = Short.parseShort( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the Minimum Delay after Trigger (uSec). 
	    id = GEMRImageInfo.MIN_DELAY;
	    int minDelay = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Total Cardiac Phase prescribed. 
	    id = GEMRImageInfo.CARDIAC_PHASE;
	    short cardiacPhase = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Swap Phase/Frequency Axis. 
	    id = GEMRImageInfo.SWAP_PHASE_FREQ;
	    short swapPhaseFreq = Short.parseShort( atts.getNamedItem(id).
						    getNodeValue());
	    
	    // ID of the Pause Interval (slices). 
	    id = GEMRImageInfo.PAUSE_INTERVAL;
	    short pauseInterval = Short.parseShort( atts.getNamedItem(id).
						    getNodeValue());
	    
	    // ID of the Pause Time. 
	    id = GEMRImageInfo.PAUSE_TIME;
	    float pauseTime = Float.parseFloat( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the Oblique Plane. 
	    id = GEMRImageInfo.OBLIQUE_PLANE;
	    int obliquePlane = Integer.parseInt( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Slice Offsets on Freq axis. 
	    id = GEMRImageInfo.SLICE_OFF;
	    int sliceOff = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Center Frequency (0.1 Hz). 
	    id = GEMRImageInfo.TRANSMIT_FREQ;
	    int transmitFreq = Integer.parseInt( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Auto Center Frequency (0.1 Hz). 
	    id = GEMRImageInfo.AUTO_CENTER_FREQ;
	    int autoCenterFreq = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Auto Transmit Gain (0.1 dB). 
	    id = GEMRImageInfo.AUTO_TRANS_GAIN;
	    short autoTransGain = Short.parseShort( atts.getNamedItem(id).
						    getNodeValue());
	    
	    // ID of the PreScan R1 - Analog. 
	    id = GEMRImageInfo.PRESCAN_R1_ANALOG;
	    short prescanR1Analog = Short.parseShort( atts.getNamedItem(id).
						      getNodeValue());
	    
	    // ID of the PreScan R2 - Digital. 
	    id = GEMRImageInfo.PRESCAN_R2_DIGITAL;
	    short prescanR2Digital = Short.parseShort( atts.getNamedItem(id).
						       getNodeValue());

	    // ID of the Bitmap defining user CVs. 
	    id = GEMRImageInfo.USER_BITMAP;
	    int userBitmap = Integer.parseInt( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the Center Frequency Method. 
	    id = GEMRImageInfo.CENTER_FREQ;
	    short centerFreq = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Imaging Mode. 
	    id = GEMRImageInfo.IMAGEING_MODE;
	    short imagingMode = Short.parseShort( atts.getNamedItem(id).
						  getNodeValue());
	    
	    // ID of the Imaging Options. 
	    id = GEMRImageInfo.IMAGEING_OPTIONS;
	    int imagingOptions = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Pulse Sequence. 
	    id = GEMRImageInfo.PULSE_SEQ;
	    short pulseSeq = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the Pulse Sequence Mode. 
	    id = GEMRImageInfo.PULSE_SEQ_MODE;
	    short pulseSeqMode = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Pulse Sequence Name. 
	    id = GEMRImageInfo.PULSE_SEQ_NAME;
	    byte [] pulseSeqName = atts.getNamedItem(id).getNodeValue().
					     getBytes();
	    
	    // ID of the PSD Creation Date and Time. 
	    id = GEMRImageInfo.PSD_DATE_TIME;
	    int PSDDateTime = Integer.parseInt( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the PSD name from inside PSD. 
	    id = GEMRImageInfo.PSD_NAME;
	    byte [] PSDName = atts.getNamedItem(id).getNodeValue().getBytes();
	    
	    // ID of the Coil Type. 
	    id = GEMRImageInfo.COIL_TYPE;
	    short coilType = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the Coil Name. 
	    id = GEMRImageInfo.COIL_NAME;
	    byte [] coilName = atts.getNamedItem(id).getNodeValue().getBytes();
	    
	    // ID of the Surface Coil Type. 
	    id = GEMRImageInfo.SRF_COIL_TYPE;
	    short srfCoilType = Short.parseShort( atts.getNamedItem(id).
						  getNodeValue());
	    
	    // ID of the Extremity Coil Flag. 
	    id = GEMRImageInfo.SRF_COIL_EXTR;
	    short srfCoilExtr = Short.parseShort( atts.getNamedItem(id).
						  getNodeValue());
	    
	    // ID of the RawData Run Number. 
	    id = GEMRImageInfo.RAW_RUN_NUMBER;
	    int rawRunNumber = Integer.parseInt( atts.getNamedItem(id).
						 getNodeValue());
	    
	    // ID of the Calibrated Field Strength (x10 uGauss). 
	    id = GEMRImageInfo.CAL_FIELD_STRENGTH;
	    int calFieldStrength = Integer.parseInt( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the SAT fat/water/none. 
	    id = GEMRImageInfo.SUPP_TECH;
	    short suppTech = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the Variable Bandwidth (Hz). 
	    id = GEMRImageInfo.VAR_BANDWIDTH;
	    float varBandwidth = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Number of slices in this scan group. 
	    id = GEMRImageInfo.NUM_SLICE_IN_SCAN;
	    short numSliceInScan = Short.parseShort( atts.getNamedItem(id).
						     getNodeValue());
	    
	    // ID of the Graphically prescribed. 
	    id = GEMRImageInfo.GRAPHIC_PRESCRIBED;
	    short graphicPrescibed = Short.parseShort( atts.getNamedItem(id).
						       getNodeValue());
	    
	    // ID of the Interimage/interloc delay (uSec). 
	    id = GEMRImageInfo.INTER_DELAY;
	    int interDelay = Integer.parseInt( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the User Variable 0. 
	    id = GEMRImageInfo.USER0;
	    float user0 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 1. 
	    id = GEMRImageInfo.USER1;
	    float user1 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 2. 
	    id = GEMRImageInfo.USER2;
	    float user2 = Float.parseFloat( atts.getNamedItem(id).
					    getNodeValue());
	    
	    // ID of the User Variable 3. 
	    id = GEMRImageInfo.USER3;
	    float user3 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 4. 
	    id = GEMRImageInfo.USER4;
	    float user4 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 5. 
	    id = GEMRImageInfo.USER5;
	    float user5 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	    // ID of the User Variable 6. 
	    id = GEMRImageInfo.USER6;
	    float user6 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 7. 
	    id = GEMRImageInfo.USER7;
	    float user7 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	    // ID of the User Variable 8. 
	    id = GEMRImageInfo.USER8;
	    float user8 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 9. 
	    id = GEMRImageInfo.USER9;
	    float user9 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 10. 
	    id = GEMRImageInfo.USER10;
	    float user10 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 11. 
	    id = GEMRImageInfo.USER11;
	    float user11 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 12. 
	    id = GEMRImageInfo.USER12;
	    float user12 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 13. 
	    id = GEMRImageInfo.USER13;
	    float user13 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 14. 
	    id = GEMRImageInfo.USER14;
	    float user14 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 15. 
	    id = GEMRImageInfo.USER15;
	    float user15 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 16. 
	    id = GEMRImageInfo.USER16;
	    float user16 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 17. 
	    id = GEMRImageInfo.USER17;
	    float user17 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	    // ID of the User Variable 18. 
	    id = GEMRImageInfo.USER18;
	    float user18 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 19. 
	    id = GEMRImageInfo.USER19;
	    float user19 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 20. 
	    id = GEMRImageInfo.USER20;
	    float user20 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 21. 
	    id = GEMRImageInfo.USER21;
	    float user21 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	
	    // ID of the User Variable 22. 
	    id = GEMRImageInfo.USER22;
	    float user22 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Projection Angle. 
	    id = GEMRImageInfo.PROJ_ANGLE;
	    float projAngle = Float.parseFloat( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the Concat Sat Type Flag. 
	    id = GEMRImageInfo.SAT_TYPE;
	    float SATType = Float.parseFloat( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Bitmap of SAT selections. 
	    id = GEMRImageInfo.SAT_BITS;
	    short SATBits = Short.parseShort( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Surface Coil Intensity Correction Flag. 
	    id = GEMRImageInfo.SRF_COIL_INTENSITY;
	    short srfCoilIntensity = Short.parseShort( atts.getNamedItem(id).
						       getNodeValue());
	    
	    // ID of the R-side SAT pulse loc rel to lndmrk. 
	    id = GEMRImageInfo.SAT_X_LOC_R;
	    short satXlocR = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the L-side SAT pulse loc rel to lndmrk. 
	    id = GEMRImageInfo.SAT_X_LOC_L;
	    short satXlocL = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the A-side SAT pulse loc rel to lndmrk. 
	    id = GEMRImageInfo.SAT_Y_LOC_A;
	    short satYlocA = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the P-side SAT pulse loc rel to lndmrk. 
	    id = GEMRImageInfo.SAT_Y_LOC_P;
	    short satYlocP = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the S-side SAT pulse loc rel to lndmrk. 
	    id = GEMRImageInfo.SAT_Z_LOC_S;
	    short satZlocS = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	
	    // ID of the I-side SAT pulse loc rel to lndmrk. 
	    id = GEMRImageInfo.SAT_Z_LOC_I;
	    short satZlocI = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Thickness of X-axis SAT pulse. 
	    id = GEMRImageInfo.SAT_X_THICK;
	    short satXThick = Short.parseShort( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the Thickness of Y-axis SAT pulse. 
	    id = GEMRImageInfo.SAT_Y_THICK;
	    short satYThick = Short.parseShort( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the Thickness of Z-axis SAT pulse. 
	    id = GEMRImageInfo.SAT_Z_THICK;
	    short satZThick = Short.parseShort( atts.getNamedItem(id).
						getNodeValue());
	    
	    // ID of the Phase contrast flow axis. 
	    id = GEMRImageInfo.FLOW_AXIS_CONTRAST;
	    short flowAxisContrast = Short.parseShort( atts.getNamedItem(id).
						       getNodeValue());
	    
	    // ID of the Phase contrast velocity encoding. 
	    id = GEMRImageInfo.VELOCITY_CONTRAST;
	    short velocityContrast = Short.parseShort( atts.getNamedItem(id).
						       getNodeValue());
	    
	    // ID of the Slice Thickness. 
	    id = GEMRImageInfo.THICK_DISCLAIMER;
	    short thickDisclaimer = Short.parseShort( atts.getNamedItem(id).
						      getNodeValue());
	    
	    // ID of the Auto/Manual Prescan flag. 
	    id = GEMRImageInfo.PRESCAN_FLAG;
	    short prescanFlag = Short.parseShort( atts.getNamedItem(id).
						  getNodeValue());
	    
	    // ID of the Bitmap of changed values. 
	    id = GEMRImageInfo.CHANGE_BITMAP;
	    short changeBitmap = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	
	    // ID of the Magnitude, Phase, Imaginary, or Real. 
	    id = GEMRImageInfo.IMAGE_TYPE;
	    short imageType = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Collapse Image. 
	    id = GEMRImageInfo.COLLAPSE;
	    short collapse = Short.parseShort( atts.getNamedItem(id).
					       getNodeValue());
	
	    // ID of the User Variable 23. 
	    id = GEMRImageInfo.USER23;
	    float user23 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the User Variable 24. 
	    id = GEMRImageInfo.USER24;
	    float user24 = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Projection Algorithm. 
	    id = GEMRImageInfo.PROJECTION_ALG;
	    short projectionAlg = Short.parseShort( atts.getNamedItem(id).
						    getNodeValue());
	
	    // ID of the Projection Algorithm Name. 
	    id = GEMRImageInfo.PROJECTION_NAME;
	    byte [] projectionName = atts.getNamedItem(id).getNodeValue().
		getBytes();
	    
	    // ID of the X Axis Rotation. 
	    id = GEMRImageInfo.X_AXIS_ROT;
	    float xAxisRot = Float.parseFloat( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Y Axis Rotation. 
	    id = GEMRImageInfo.Y_AXIS_ROT;
	    float yAxisRot = Float.parseFloat( atts.getNamedItem(id).
					       getNodeValue());
	    
	    // ID of the Z Axis Rotation. 
	    id = GEMRImageInfo.Z_AXIS_ROT;
	    float zAxisRot = Float.parseFloat( atts.getNamedItem(id).
					       getNodeValue());
	
	    // ID of the Lower Range of Pixels 1. 
	    id = GEMRImageInfo.MIN_PIXEL1;
	    int minPixel1 = Integer.parseInt( atts.getNamedItem(id).
					      getNodeValue());
	
	    // ID of the Upper Range of Pixels 1. 
	    id = GEMRImageInfo.MAX_PIXEL1;
	    int maxPixel1 = Integer.parseInt( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Lower Range of Pixels 2. 
	    id = GEMRImageInfo.MIN_PIXEL2;
	    int minPixel2 = Integer.parseInt( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Upper Range of Pixels 2. 
	    id = GEMRImageInfo.MAX_PIXEL2;
	    int maxPixel2 = Integer.parseInt( atts.getNamedItem(id).
					      getNodeValue());
	    
	    // ID of the Echo Train Length for Fast Spin Echo. 
	    id = GEMRImageInfo.ECHO_TRAIN_LEN;
	    short echoTrainLen = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Fractional Echo - Effective TE Flag. 
	    id = GEMRImageInfo.FRACTION_ECHO;
	    short fractionEcho = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Preporatory Pulse Option. 
	    id = GEMRImageInfo.PREP_PULSE;
	    short prepPulse = Short.parseShort( atts.getNamedItem(id).
						getNodeValue());
	
	    // ID of the Cardiac Phase Number. 
	    id = GEMRImageInfo.CARDIAC_PHASE_NUM;
	    short cardiacPhaseNum = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	    
	    // ID of the Variable Echo Flag. 
	    id = GEMRImageInfo.VARIABLE_ECHO;
	    short variableEcho = Short.parseShort( atts.getNamedItem(id).
					     getNodeValue());
	    
	    imageInfo = new GEMRImageInfo( suiteID, uniqueFlag, 
				    diskID, examNumber, 
				    seriesNum, imageNum, 
				    AllocDateTime, ActualDateTime,
				    scanTime, sliceThick, 
				    iMatrixX, iMatrixY, 
				    displayFieldX, 
				    displayFieldY, 
				    dimensionX, dimensionY,
				    pixelSizeX, pixelSizeY,
				    pixelDataID,
				    contrastIV, 
				    contrastOral, 
				    contrastMode, seriesRX, 
				    imageRX, screenFormat, 
				    plane, scanSpacing,
				    compression, scoutType, 
				    locationRAS, imageLocation,
				    centerR, centerA, 
				    centerS, normR, 
				    normA, normS, 
				    TopLeftR, TopLeftA, 
				    TopLeftS, TopRightR, 
				    TopRightA, TopRightS, 
				    BotRightR, BotRightA, 
				    BotRightS, 
				    foreignImageRev, repTime,	
				    invTime, echoTime, 
				    echoTime2, numEcho, 
				    echoNumber, tableDelta, 
				    numExcitations, continuous,
				    heartRate, timeDelay, 
				    averageSAR, peakSAR, 
				    monitorSAR, triggerWindow,
				    cardRepTime, 
				    imagesPerCycle, 
				    transmitGain, 
				    recvGainAnalog, 
				    recvGainDigital, 
				    flipAngle, minDelay, 
				    cardiacPhase, 
				    swapPhaseFreq, 
				    pauseInterval, pauseTime,
				    obliquePlane, sliceOff, 
				    transmitFreq, autoCenterFreq,
				    autoTransGain, 
				    prescanR1Analog, 
				    prescanR2Digital, 
				    userBitmap, centerFreq, 
				    imagingMode, imagingOptions,
				    pulseSeq, pulseSeqMode,
				    pulseSeqName, PSDDateTime,
				    PSDName, coilType,
				    coilName, srfCoilType, 
				    srfCoilExtr, rawRunNumber, 
				    calFieldStrength, suppTech,
				    varBandwidth, numSliceInScan, 
				    graphicPrescibed, interDelay,
				    user0, user1, user2,
				    user3, user4, user5,
				    user6, user7, user8,
				    user9, user10,
				    user11, user12, 
				    user13, user14, 
				    user15, user16, 
				    user17, user18, 
				    user19, user20, 
				    user21, user22, 
				    projAngle, SATType,
				    allocKey, lastMod,
				    versionCreation, 
				    versionNow, pixelDataSizeA,
				    pixelDataSizeC, pixelDataSizeU,
				    checkSum, archive, 
				    complete, SATBits, 
				    srfCoilIntensity, satXlocR, 
				    satXlocL, satYlocA, 
				    satYlocP, satZlocS, 
				    satZlocI, satXThick, 
				    satYThick, satZThick, 
				    flowAxisContrast, 
				    velocityContrast, 
				    thickDisclaimer, prescanFlag,
				    changeBitmap, imageType,
				    collapse, user23, user24,
				    projectionAlg,  projectionName,
				    xAxisRot, yAxisRot, 
				    zAxisRot, minPixel1, maxPixel1,
				    minPixel2, maxPixel2, 
				    echoTrainLen, fractionEcho, 
				    prepPulse, cardiacPhaseNum, 
				    variableEcho, referenceField,
				    summaryField, window,
				    level, slopInt1, slopInt2, 
				    slopInt3, slopInt4, slopInt5, 
				    slopFloat1, slopFloat2, 
				    slopFloat3, slopFloat4, 
				    slopFloat5,  slopStr1,
				    slopStr2);

	} else if( root.getNodeName().equals(ctImageInfoName) ) {


	    // ID of the Table Start Location. 
	    id = GECTImageInfo.TABLE_START_LOC;
	    float tableStartLoc = Float.parseFloat( atts.getNamedItem(id).
						    getNodeValue());
	    
	    // ID of the Table End Location. 
	    id = GECTImageInfo.TABLE_END_LOC;
	    float tableEndLoc = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Table Speed (mm/sec). 
	    id = GECTImageInfo.TABLE_SPEED;
	    float tableSpeed = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Table Height. 
	    id = GECTImageInfo.TABLE_HEIGHT;
	    float tableHeight = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Mid Scan Time. 
	    id = GECTImageInfo.MID_SCAN_TIME;
	    float midScanTime = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the MidScan flag - does midstime apply for L/S. 
	    id = GECTImageInfo.MID_SCAN_FLAG;
	    short midScanFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the KVolt generator setting. 
	    id = GECTImageInfo.KVOLT;
	    int kvolt = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the MAmp generator setting. 
	    id = GECTImageInfo.MAMP;
	    int mamp = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Gantry Tilt (degrees). 
	    id = GECTImageInfo.GANTRY_TILT;
	    float gantryTilt = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Degrees of Azimuth. 
	    id = GECTImageInfo.AZIMUTH;
	    int azimuth = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Gantry Velocity. 
	    id = GECTImageInfo.GANTRY_VEL;
	    float gantryVel = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Gantry Filter Position. 
	    id = GECTImageInfo.GANTRY_FILTER;
	    int gantryFilter = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Trigger on Position. 
	    id = GECTImageInfo.TRIGGER_ON_POS;
	    float triggerOnPos = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Degrees of rotation. 
	    id = GECTImageInfo.DEGREE_ROT;
	    float degreeRot = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the X-Ray On Position. 
	    id = GECTImageInfo.XRAY_ON;
	    float xrayOn = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the X-Ray Off Position. 
	    id = GECTImageInfo.XRAY_OFF;
	    float xrayOff = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Number of Triggers. 
	    id = GECTImageInfo.NUM_TRIGGERS;
	    int numTriggers = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Total input views. 
	    id = GECTImageInfo.INPUT_VIEWS;
	    short inputViews = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Angle of first view. 
	    id = GECTImageInfo.ANGLE_VIEW1;
	    float angleView1 = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Trigger frequency. 
	    id = GECTImageInfo.TRIGGER_FREQ;
	    float triggerFreq = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS trigger source. 
	    id = GECTImageInfo.TRIGGER_SRC;
	    int triggerSrc = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS fpa gain. 
	    id = GECTImageInfo.FPA_GAIN;
	    int fpaGain = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Scan Type. 
	    id = GECTImageInfo.SCAN_TYPE;
	    int scanType = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS output source. 
	    id = GECTImageInfo.OUT_SRC;
	    int outSrc = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS ad input. 
	    id = GECTImageInfo.AD_INPUT;
	    int adInput = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS cal mode. 
	    id = GECTImageInfo.CAL_MODE;
	    int calMode = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS cal frequency. 
	    id = GECTImageInfo.CAL_FREQ;
	    int calFreq = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS reg xm. 
	    id = GECTImageInfo.REG_XM;
	    int regXM = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS auto zero. 
	    id = GECTImageInfo.AUTO_ZERO;
	    int autoZero = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Axial Type. 
	    id = GECTImageInfo.AXIAL_TYPE;
	    short axialType = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Calibration phantom size. 
	    id = GECTImageInfo.PHANT_SIZE;
	    short phantSize = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Calibration phantom type. 
	    id = GECTImageInfo.PHANT_TYPE;
	    short phantType = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Calibration filter type. 
	    id = GECTImageInfo.FILTER_TYPE;
	    short filterType = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Recon Algorithm. 
	    id = GECTImageInfo.RECON_ALG;
	    short reconAlg = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Perstaltic flag. 
	    id = GECTImageInfo.PERIS_FLAG;
	    short perisFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the IterBone flag. 
	    id = GECTImageInfo.ITERBONE_FLAG;
	    short iterboneFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Stat Recon flag. 
	    id = GECTImageInfo.STAT_FLAG;
	    short statFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Compute Type. 
	    id = GECTImageInfo.COMPUTE_TYPE;
	    short computeType = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Segment Number. 
	    id = GECTImageInfo.SEG_NUMBER;
	    short segNumber = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Total Number of Segments Requested. 
	    id = GECTImageInfo.SEGS_TOTAL;
	    short segsTotal = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Inter scan delay (secs). 
	    id = GECTImageInfo.ISCAN_DELAY;
	    float iscanDelay = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Scan field of view (mm). 
	    id = GECTImageInfo.SCAN_FIELD;
	    float scanField = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Scan Number. 
	    id = GECTImageInfo.SCAN_NUMBER;
	    short scanNumber = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Starting Channel of View. 
	    id = GECTImageInfo.VIEW_START_CHAN;
	    short viewStartChan = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the View Compression Factor. 
	    id = GECTImageInfo.VIEW_COMP_FACTOR;
	    short viewCompFactor = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Total Output Views. 
	    id = GECTImageInfo.OUT_VIEWS;
	    short outViews = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Number of Overranges. 
	    id = GECTImageInfo.OVER_RANGES;
	    short overRanges = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Total Number of Ref Channels. 
	    id = GECTImageInfo.TOTAL_REF_CHAN;
	    short totalRefChan = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the data size for scan data. 
	    id = GECTImageInfo.SCAN_DATA_SIZE;
	    int scanDataSize = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the z or q channel. 
	    id = GECTImageInfo.REF_CHAN0;
	    short refChan0 = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Reference channel 1. 
	    id = GECTImageInfo.REF_CHAN1;
	    short refChan1 = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Reference channel 2. 
	    id = GECTImageInfo.REF_CHAN2;
	    short refChan2 = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Reference channel 3. 
	    id = GECTImageInfo.REF_CHAN3;
	    short refChan3 = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Reference channel 4. 
	    id = GECTImageInfo.REF_CHAN4;
	    short refChan4 = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Reference channel 5. 
	    id = GECTImageInfo.REF_CHAN5;
	    short refChan5 = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Recon post processing flag. 
	    id = GECTImageInfo.POST_PROC_FLAG;
	    short postProcFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the DAS xm pattern. 
	    id = GECTImageInfo.XM_PATTERN;
	    int xmPattern = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Prescribed rotation type. 
	    id = GECTImageInfo.ROT_TYPE;
	    short rotType = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Save Raw Data Flag. 
	    id = GECTImageInfo.RAW_DATA_FLAG;
	    short rawDataFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the IBH Image scale factors. 
	    id = GECTImageInfo.CT_SCALE_FACTOR;
	    float ctScaleFactor = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the CT Water Number. 
	    id = GECTImageInfo.CT_WATER_NUMBER;
	    short ctWaterNumber = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the CT Bone Number. 
	    id = GECTImageInfo.CT_BONE_NUMBER;
	    short ctBoneNumber = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the BBH coefficient 1. 
	    id = GECTImageInfo.BBH_COEF1;
	    float bbhCoef1 = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the BBH coefficient 2. 
	    id = GECTImageInfo.BBH_COEF2;
	    float bbhCoef2 = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the BBH coefficient 3. 
	    id = GECTImageInfo.BBH_COEF3;
	    float bbhCoef3 = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Num of BBH channels to blend. 
	    id = GECTImageInfo.BBH_NUM_BLEND;
	    short bbhNumBlend = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Starting channel. 
	    id = GECTImageInfo.FIRST_CHAN;
	    int firstChan = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Number of channels (1..512). 
	    id = GECTImageInfo.NUM_CHAN;
	    int numChan = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Increment between channels. 
	    id = GECTImageInfo.CHAN_INCR;
	    int chanIncr = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Starting view. 
	    id = GECTImageInfo.FIRST_VIEW;
	    int firstView = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Number of views. 
	    id = GECTImageInfo.NUM_VIEW;
	    int numView = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Increment between views. 
	    id = GECTImageInfo.VIEW_INCR;
	    int viewIncr = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Window Range (0..4095). 
	    id = GECTImageInfo.WINDOW_RANGE;
	    int windowRange = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Scaling value of the image data. 
	    id = GECTImageInfo.SCALE_MIN;
	    float scaleMin = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Scaling value of the image data. 
	    id = GECTImageInfo.SCALE_MAX;
	    float scaleMax = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Amount of processing that will be performed. 
	    id = GECTImageInfo.DATA_MODE;
	    int dataMode = Integer.parseInt( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Source of the qcal vectors. 
	    id = GECTImageInfo.QCAL_FILE;
	    byte [] qcalFile = atts.getNamedItem(id).getNodeValue().
		getBytes();
	    
	    // ID of the Source of the cal vectors. 
	    id = GECTImageInfo.CAL_MOD_FILE;
	    byte [] calModFile = atts.getNamedItem(id).getNodeValue().
						   getBytes();
	    
	    // ID of the Number of words per view. 
	    id = GECTImageInfo.WORDS_PER_VIEW;
	    short wordsPerView = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the RAS letter for side of image. 
	    id = GECTImageInfo.RL_RAS;
	    char rlRAS = (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	    
	    // ID of the RAS letter for anterior/posterior. 
	    id = GECTImageInfo.AP_RAS;
	    char apRAS = (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	    
	    // ID of the RAS letter for scout start loc. 
	    id = GECTImageInfo.SCOUT_START_RAS;
	    char scoutStartRAS = (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	    
	    // ID of the RAS letter for scout end loc. 
	    id = GECTImageInfo.SCOUT_END_RAS;
	    char scoutEndRAS = (char) (atts.getNamedItem(id).
			      getNodeValue().getBytes())[0];
	    
	    // ID of the Anatomical reference for scout. 
	    id = GECTImageInfo.SCOUT_ANATOM_REF;
	    byte [] scoutAnatomRef = atts.getNamedItem(id).getNodeValue().
						   getBytes();
	    
	    // ID of the PpScan window range for output Scaling. 
	    id = GECTImageInfo.PPS_SCALE_WINDOW;
	    short ppsScaleWindow = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the PpScan Qcal modification flag. 
	    id = GECTImageInfo.PPS_QCAL_FLAG;
	    short ppsQcalFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the PpScan Pcal modification flag. 
	    id = GECTImageInfo.PPS_PCAL_FLAG;
	    short ppsPcalFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the PpScan Theta Fix (Angle Correction). 
	    id = GECTImageInfo.PPS_THETA_FIX;
	    short ppsThetaFix = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the PpScan Beam Hardening Flag. 
	    id = GECTImageInfo.PPS_BEAM_FLAG;
	    short ppsBeamFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the tube focal spot size. 
	    id = GECTImageInfo.SPOT_SIZE;
	    short spotSize = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the tube focal spot position. 
	    id = GECTImageInfo.SPOT_POS;
	    short spotPos = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Dependent on #views processed. 
	    id = GECTImageInfo.RECON_DATA_SET;
	    short reconDataSet = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Field of view in detector cells. 
	    id = GECTImageInfo.DET_CELL_FIELD;
	    short detCellField = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Start time(secs) of this scan. 
	    id = GECTImageInfo.SCAN_START_TIME;
	    double scanStartTime = Double.parseDouble( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Gantry Rotation Direction. 
	    id = GECTImageInfo.GANTRY_DIRECTION;
	    short gantryDirection = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
    
	    // ID of the Tube Rotor Speed. 
	    id = GECTImageInfo.ROTOR_SPEED;
	    short rotorSpeed = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the TGGC Trigger Mode. 
	    id = GECTImageInfo.TRIG_MODE;
	    short trigMode = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Rx'd gantry tilt - not annotated. 
	    id = GECTImageInfo.SI_TILT;
	    float siTilt = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the R/L coordinate for target recon center. 
	    id = GECTImageInfo.R_RECON_CENTER;
	    float rReconCenter = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the A/P coordinate for target recon center. 
	    id = GECTImageInfo.A_RECON_CENTER;
	    float aReconCenter = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Value of Back Projection button. 
	    id = GECTImageInfo.BACK_PROJ_FLAG;
	    short backProjFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Set if fatq estimates were used. 
	    id = GECTImageInfo.FATQ_EST_FLAG;
	    short fatqEstFlag = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Z chan avg over views. 
	    id = GECTImageInfo.Z_AVG;
	    float zAvg = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the avg of left ref chans over views. 
	    id = GECTImageInfo.LEFT_REF_AVG;
	    float leftRefAvg = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the max left chan value over views. 
	    id = GECTImageInfo.LEFT_REF_MAX;
	    float leftRefMax = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the avg of right ref chans over views. 
	    id = GECTImageInfo.RIGHT_REF_AVG;
	    float rightRefAvg = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the max right chan value over views. 
	    id = GECTImageInfo.RIGHT_REF_MAX;
	    float rightRefMax = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Biopsy Position. 
	    id = GECTImageInfo.BIOPSY_POS;
	    short biopsyPos = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Biopsy T Location. 
	    id = GECTImageInfo.BIOPSY_T_LOC;
	    float boipsyTLoc = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Biopsy Ref Location. 
	    id = GECTImageInfo.BIOPSY_REF_LOC;
	    float biopsyRefLoc = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Reference Channel Used. 
	    id = GECTImageInfo.REF_CHAN;
	    short refChan = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());
	    
	    // ID of the Back Projector Coefficient. 
	    id = GECTImageInfo.BP_COEF;
	    float bpCoef = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Primary Speed Correction Used. 
	    id = GECTImageInfo.PSPEED_CORR;
	    short pspeedCorr = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Overrange Correction Used. 
	    id = GECTImageInfo.OVERRANGE_CORR;
	    short overrangeCorr = Short.parseShort( atts.getNamedItem(id).
						   getNodeValue());

	    // ID of the Dynamic Z Alpha Value. 
	    id = GECTImageInfo.DYN_Z_ALPHA;
	    float dynZAlpha = Float.parseFloat( atts.getNamedItem(id).
						   getNodeValue());

	    
	    imageInfo = new GECTImageInfo( suiteID, uniqueFlag, 
					   diskID, examNumber, 
					   seriesNum, imageNum, 
					   AllocDateTime, ActualDateTime,
					   scanTime, sliceThick, 
					   iMatrixX, iMatrixY, 
					   displayFieldX, 
					   displayFieldY, 
					   dimensionX, dimensionY,
					   pixelSizeX, pixelSizeY,
					   pixelDataID,
					   contrastIV, 
					   contrastOral, 
					   contrastMode, seriesRX, 
					   imageRX, screenFormat, 
					   plane, scanSpacing,
					   compression, scoutType, 
					   locationRAS, imageLocation,
					   centerR, centerA, 
					   centerS, normR, 
					   normA, normS, 
					   TopLeftR, TopLeftA, 
					   TopLeftS, TopRightR, 
					   TopRightA, TopRightS, 
					   BotRightR, BotRightA, 
					   BotRightS, foreignImageRev,
					   tableStartLoc, tableEndLoc, 
					   tableSpeed, tableHeight, 
					   midScanTime, midScanFlag, 
					   kvolt, mamp, gantryTilt,
					   azimuth, gantryVel, gantryFilter,
					   triggerOnPos, degreeRot, 
					   xrayOn, xrayOff, numTriggers, 
					   inputViews, angleView1, 
					   triggerFreq, triggerSrc, fpaGain, 
					   scanType, outSrc, adInput, 
					   calMode, calFreq, regXM, 
					   autoZero, axialType, phantSize, 
					   phantType, filterType, 
					   reconAlg, perisFlag, 
					   iterboneFlag, statFlag, 
					   computeType, segNumber, 
					   segsTotal, iscanDelay, 
					   scanField, scanNumber, 
					   viewStartChan, viewCompFactor, 
					   outViews, overRanges, 
					   totalRefChan, scanDataSize, 
					   refChan0, refChan1, refChan2, 
					   refChan3, refChan4, refChan5, 
					   postProcFlag, xmPattern, rotType, 
					   rawDataFlag, ctScaleFactor, 
					   ctWaterNumber, ctBoneNumber, 
					   bbhCoef1, bbhCoef2, bbhCoef3, 
					   bbhNumBlend, firstChan, numChan, 
					   chanIncr, firstView, numView, 
					   viewIncr, windowRange, scaleMin, 
					   scaleMax, dataMode, qcalFile,
					   calModFile, wordsPerView, rlRAS,
					   apRAS, scoutStartRAS, scoutEndRAS, 
					   scoutAnatomRef, ppsScaleWindow, 
					   ppsQcalFlag, ppsPcalFlag, 
					   ppsThetaFix, ppsBeamFlag, 
					   spotSize, spotPos, 
					   reconDataSet, detCellField, 
					   scanStartTime, gantryDirection,
					   rotorSpeed, trigMode, siTilt, 
					   rReconCenter, aReconCenter, 
					   backProjFlag, fatqEstFlag, zAvg, 
					   leftRefAvg, leftRefMax, 
					   rightRefAvg, rightRefMax,
					   allocKey, lastMod,
					   versionCreation, 
					   versionNow, pixelDataSizeA,
					   pixelDataSizeC, pixelDataSizeU,
					   checkSum, archive, 
					   complete,biopsyPos, boipsyTLoc,
					   biopsyRefLoc, refChan, bpCoef,
					   pspeedCorr, overrangeCorr, 
					   dynZAlpha, 
					   referenceField,
					   summaryField, window,
					   level, slopInt1, slopInt2, 
					   slopInt3, slopInt4, slopInt5, 
					   slopFloat1, slopFloat2, 
					   slopFloat3, slopFloat4, 
					   slopFloat5,  slopStr1,
					   slopStr2);
    
	} else {
	    String msg = "Root node must be named \"" + mrImageInfoName + 
		"\" or \"" + ctImageInfoName + "\". ";
	throw new IIOInvalidTreeException(msg, root);
	}

      }
      // Unable to construct an GE Image Info
      catch (Exception e) {
	  e.printStackTrace();
	  String msg = "Cannot convert the DOM Node into an GE Image Info.";
	throw new  IIOInvalidTreeException(msg, e, root);
      }

      // Return the GE MR Image Info
      return imageInfo;
    }


    /**
   * Converts the GE Header to a tree.
   *
   * @param geHeader GE Header to convert to a tree.
   *
   * @return DOM Node representing the GE Header.
   */
  public static Node toTree(GEHeader geHeader)
    {
      // Create the root Node
      String rootName = GEMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      if (geHeader != null) {

	// Add the GE Suite Info
	String name = GEMetadataFormat.GE_SUITE_INFO_NAME;
	Node node = _toTree(name, geHeader.getGESuiteInfo());
	rootNode.appendChild(node);

	// Add the GE Exam Info
	name = GEMetadataFormat.GE_EXAM_INFO_NAME;
	node = _toTree(name, geHeader.getGEExamInfo());
	rootNode.appendChild(node);

	// Add the GE Series Info
	name = GEMetadataFormat.GE_SERIES_INFO_NAME;
	node = _toTree(name, geHeader.getGESeriesInfo());
	rootNode.appendChild(node);

	// Add the GE MR or CT Image Info
	if (geHeader.getGEImageInfo() instanceof GEMRImageInfo) {
	  name = GEMetadataFormat.GE_MR_IMAGE_INFO_NAME;
	}
	else { name = GEMetadataFormat.GE_CT_IMAGE_INFO_NAME; }
	node = _toTree(name, geHeader.getGEImageInfo());
	rootNode.appendChild(node);
      }

      // Return the root Node
      return rootNode;
    }

  /**
   * Converts the GE Subheader to a tree.
   *
   * @param treeName Name of the tree.
   * @param geSubheader GE Subheader from which to construct the
   *                         tree.
   *
   * @return DOM Node representing the GE Subheader.
   */
  private static Node _toTree(String treeName,
			      GESubheader geSubheader)
    {
      // Create the root Node
      IIOMetadataNode rootNode = new IIOMetadataNode(treeName);

      // Add each element as an attribute
      Iterator iter = geSubheader.getElementNames();
      while ( iter.hasNext() ) {
	String elementName = (String)iter.next();
	Object elementValue = geSubheader.getElementValue(elementName);
	
	// Set the attribute
	rootNode.setAttribute(elementName, elementValue.toString());
      }

      // Return the root Node
      return rootNode;
    }
}
