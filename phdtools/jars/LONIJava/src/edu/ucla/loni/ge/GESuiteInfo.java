/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ge;

import edu.ucla.loni.ge.GESubheader;

/**
 * GE Subheader that describes the history of the Suite.
 *
 * @version 10 October 2002
 */
public class GESuiteInfo extends GESubheader
{
  /** ID of the suite ID. */
  public static final String SUITE_ID = "suiteID";

  /** ID of the Make Unique Flag */
  public static final String UNIQUE_FLAG = "uniqueFlag";

  /** ID of the Disk ID */
  public static final String DISK_ID = "diskID";

  /** ID of the Product ID */
  public static final String PRODUCT_ID = "productID";

  /** ID of the Genesis Version of Record Created*/
  public static final String VERSION_CR = "versionCr";

  /** ID of the Genesis Version of Record Currently*/
  public static final String VERSION_CU = "versionCu";

  /** ID of the Suite Record Checksum */
  public static final String CHECK_SUM = "checkSum";

  /**
   * Constructs a GESuiteInfo.
   *
   * @param suiteID Suite ID. 
   * @param uniqueFlag Make Unique Flag.
   * @param diskID Disk ID.
   * @param productID Product ID.
   * @param versionCr Genesis Version of Record Created.
   * @param versionCu Genesis Version of Record Currently.
   * @param checkSum Suite Record Checksum.
   *
   *  @throws IllegalArgumentException If any byte array is larger than its
   *                                  specified maximum length.
   */
    public GESuiteInfo(byte [] suiteID, short uniqueFlag, char diskID,
			  byte [] productID, byte [] versionCr, 
			  byte [] versionCu, int checkSum)
    {
	super(7);
	
	_addElement(SUITE_ID, _createString(suiteID));
	_addElement(UNIQUE_FLAG, new Short(uniqueFlag) );
	_addElement(DISK_ID, new Character(diskID) );
	_addElement(PRODUCT_ID, _createString(productID));
	_addElement(VERSION_CR, _createString(versionCr));
	_addElement(VERSION_CU, _createString(versionCu));
	_addElement(CHECK_SUM, new Integer(checkSum) );
    }
}
