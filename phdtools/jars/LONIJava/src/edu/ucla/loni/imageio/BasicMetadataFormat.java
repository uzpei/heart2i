/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.imageio;

import java.util.Vector;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the basic metadata attributes.
 *
 * @version 13 January 2009
 */
public class BasicMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
    /** Name of the native metadata format. */
    public static final String NATIVE_METADATA_FORMAT_NAME =
	"edu_ucla_loni_imageio_basic_1.0";

    /** Width of an image in pixels. */
    public static final String IMAGE_WIDTH = "image_width";

    /** Height of an image in pixels. */
    public static final String IMAGE_HEIGHT = "image_height";

    /** Number of images. */
    public static final String NUMBER_OF_IMAGES = "number_of_images";

    /** Width of an image pixel in millimeters. */
    public static final String PIXEL_WIDTH = "pixel_width";

    /** Height of an image pixel in millimeters. */
    public static final String PIXEL_HEIGHT = "pixel_height";

    /** Distance between consecutive image slices in millimeters. */
    public static final String SLICE_THICKNESS = "slice_thickness";

    /** Data type. */
    public static final String DATA_TYPE = "data_type";

    /** Byte data type. */
    public static final String BYTE_TYPE = "Byte";

    /** Unsigned short data type. */
    public static final String UNSIGNED_SHORT_TYPE = "Unsigned Short";

    /** Signed short data type. */
    public static final String SIGNED_SHORT_TYPE = "Signed Short";

    /** Unsigned integer data type. */
    public static final String UNSIGNED_INT_TYPE = "Unsigned Integer";

    /** Signed integer data type. */
    public static final String SIGNED_INT_TYPE = "Signed Integer";

    /** Unsigned long data type. */
    public static final String UNSIGNED_LONG_TYPE = "Unsigned Long";

    /** Signed long data type. */
    public static final String SIGNED_LONG_TYPE = "Signed Long";

    /** Float data type. */
    public static final String FLOAT_TYPE = "Float";

    /** Double data type. */
    public static final String DOUBLE_TYPE = "Double";

    /** Number of bits in an image pixel. */
    public static final String BITS_PER_PIXEL = "bits_per_pixel";

    /** Color type. */
    public static final String COLOR_TYPE = "color_type";

    /** Gray color type. */
    public static final String GRAY_TYPE = "Grayscale";

    /** RGB color type. */
    public static final String RGB_TYPE = "RGB";

    /** Single instance of the NIFTI Metadata Format. */
    private static BasicMetadataFormat _format = new BasicMetadataFormat();

    /** Constructs a Basic Metadata Format. */
    private BasicMetadataFormat()
    {
	// Create the root with no children
	super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_EMPTY);

	// Add the attributes
	addAttribute(NATIVE_METADATA_FORMAT_NAME, IMAGE_WIDTH,
		     DATATYPE_INTEGER, true, null);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, IMAGE_HEIGHT,
		     DATATYPE_INTEGER, true, null);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, NUMBER_OF_IMAGES,
		     DATATYPE_INTEGER, true, null);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, PIXEL_WIDTH,
		     DATATYPE_DOUBLE, true, null);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, PIXEL_HEIGHT,
		     DATATYPE_DOUBLE, true, null);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, SLICE_THICKNESS,
		     DATATYPE_DOUBLE, true, null);

	Vector allowedValues = new Vector();
	allowedValues.add(BYTE_TYPE);
	allowedValues.add(UNSIGNED_SHORT_TYPE);
	allowedValues.add(SIGNED_SHORT_TYPE);
	allowedValues.add(UNSIGNED_INT_TYPE);
	allowedValues.add(SIGNED_INT_TYPE);
	allowedValues.add(UNSIGNED_LONG_TYPE);
	allowedValues.add(SIGNED_LONG_TYPE);
	allowedValues.add(FLOAT_TYPE);
	allowedValues.add(DOUBLE_TYPE);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, DATA_TYPE, DATATYPE_STRING,
		     true, null, allowedValues);

	addAttribute(NATIVE_METADATA_FORMAT_NAME, BITS_PER_PIXEL,
		     DATATYPE_INTEGER, true, null);

	allowedValues = new Vector();
	allowedValues.add(GRAY_TYPE);
	allowedValues.add(RGB_TYPE);
	addAttribute(NATIVE_METADATA_FORMAT_NAME, COLOR_TYPE, DATATYPE_STRING,
		     true, null, allowedValues);
    }

    /**
     * Gets an instance of the Basic Metadata Format.
     *
     * @return The single instance of the Basic Metadata Format.
     */
    public static BasicMetadataFormat getInstance()
    {
	return _format;
    }

    /**
     * Returns true if the element (and the subtree below it) is allowed to
     * appear in a metadata document for an image of the given type, defined by
     * an ImageTypeSpecifier.
     *
     * @param elementName The name of the element being queried.
     * @param imageType An ImageTypeSpecifier indicating the type of the image
     *                  that will be associated with the metadata.
     *
     * @return True if the node is meaningful for images of the given type.
     */
    public boolean canNodeAppear(String elementName,
				 ImageTypeSpecifier imageType)
    {
	// Not implemented
	return true;
    }
}
