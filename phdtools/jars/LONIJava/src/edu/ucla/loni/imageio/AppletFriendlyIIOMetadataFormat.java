/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.imageio.metadata.IIOMetadataFormatImpl;

/**
 * II/O Metadata Format Impl designed to work inside of applets.
 *
 * @param 2 July 2003
 */
public abstract class AppletFriendlyIIOMetadataFormat
                extends IIOMetadataFormatImpl
{
  /**
   * Constructs an Applet Friendly IIO Metadata Format instance with the given
   * root element name and child policy (other than CHILD_POLICY_REPEAT).
   *
   * Additional elements, and their attributes and Object reference information
   * may be added using the various add methods.
   *
   * @param rootName The name of the root element.
   * @param childPolicy One of the CHILD_POLICY_* constants, other than
   *                    CHILD_POLICY_REPEAT.
   *
   * @throws IllegalArgumentException If rootName is null.
   * @throws IllegalArgumentException if childPolicy is not one of the
   *                                  predefined constants.
   */
  public AppletFriendlyIIOMetadataFormat(String rootName, int childPolicy)
    {
      super(rootName, childPolicy);
    }

  /**
   * Constructs an Applet Friendly IIO Metadata Format instance with the given
   * root element name and a child policy of CHILD_POLICY_REPEAT.
   *
   * Additional elements, and their attributes and Object reference information
   * may be added using the various add methods.
   *
   * @param rootName The name of the root element.
   * @param minChildren The minimum number of children of the node.
   * @param maxChildren The maximum number of children of the node.
   *
   * @throws IllegalArgumentException If rootName is null.
   * @throws IllegalArgumentException If minChildren is negative or larger
   *                                  than maxChildren.
   */
  public AppletFriendlyIIOMetadataFormat(String rootName, int minChildren,
					 int maxChildren)
    {
      super(rootName, minChildren, maxChildren);
    }

  /**
   * Returns a String containing a description of the named element, or null.
   * The desciption will be localized for the supplied Locale if possible.
   *
   * <p> The default implementation will first locate a ResourceBundle using
   * the current resource base name set by setResourceBaseName and the supplied
   * Locale, using the fallback mechanism described in the comments for
   * ResourceBundle.getBundle.  If a ResourceBundle is found, the element name
   * will be used as a key to its getString method, and the result returned.
   * If no ResourceBundle is found, or no such key is present, null will be
   * returned.
   * 
   * <p> If locale is null, the current default Locale returned by
   * Locale.getLocale will be used.
   *
   * @param elementName The name of the element.
   * @param locale The Locale for which localization will be attempted.
   *
   * @return The element description.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   */
  public String getElementDescription(String elementName, Locale locale)
    {
      // Try super class
      String description = super.getElementDescription(elementName, locale);
      if (description != null) { return description; }

      // Applets require a local invocation of the same super class method
      return _getResource(elementName, locale);
    }

  /**
   * Returns a String containing a description of the named attribute, or
   * null.  The desciption will be localized for the supplied Locale if
   * possible.
   *
   * <p> The default implementation will first locate a ResourceBundle using
   * the current resource base name set by setResourceBaseName and the supplied
   * Locale, using the fallback mechanism described in the comments for
   * ResourceBundle.getBundle.  If a ResourceBundle is found, the element name
   * followed by a "/" character followed by the attribute name
   * (elementName + "/" + attrName) will be used as a key to its getString
   * method, and the result returned.  If no ResourceBundle is found, or no
   * such key is present, null will be returned.
   * 
   * <p> If locale is null, the current default Locale returned by
   * Locale.getLocale will be used.
   *
   * @param elementName The name of the element.
   * @param attrName The name of the attribute.
   * @param locale The Locale for which localization will be attempted, or
   *               null.
   *
   * @return The attribute description.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   * @throws IllegalArgumentException If attrName is null or is not a legal
   *                                  attribute name for this element.
   */
  public String getAttributeDescription(String elementName, String attrName,
					Locale locale)
    {
      // Try super class
      String description = super.getAttributeDescription(elementName, attrName,
							 locale);
      if (description != null) { return description; }

      // Applets require a local invocation of the same super class method
      String key = elementName + "/" + attrName;
      return _getResource(key, locale);
    }

  /**
   * Gets the requested resource.
   *
   * @param key Key that indexes the resource.
   * @param locale The Locale for which localization will be attempted, or
   *               null.
   *
   * @return Requested resource.
   */
  private String _getResource(String key, Locale locale)
    {
      // Same as super class method
      if (locale == null) {
	locale = Locale.getDefault();
      }

      try {
	String baseName = getResourceBaseName();
	ResourceBundle bundle = ResourceBundle.getBundle(baseName, locale);
	return bundle.getString(key);
      }
      catch (MissingResourceException e) { return null; }
    }
}
