/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;

/**
 * Component Color Model that linearly scales pixel values to a gray scale
 * color space.
 *
 * @version 15 February 2005
 */
public class LinearScaleGrayColorModel extends ComponentColorModel
{
  /** Bound below which all pixel values are mapped to (0,0,0). */
  private double _lowerBound;

  /** Bound above which all pixel values are mapped to (255,255,255). */
  private double _upperBound;

  /**
   * Constructs a Linear-Scale Gray Color Model.
   *
   * @param dataType Data Buffer type.
   *
   * @throws IllegalArgumentException If the data type is not a Data Buffer
   *                                  type.
   */
  public LinearScaleGrayColorModel(int dataType)
    {
      super(ColorSpace.getInstance(ColorSpace.CS_GRAY),
	    _getNumberOfBits(dataType), false, false, Transparency.OPAQUE,
	    dataType);

      _lowerBound = 0;
      _upperBound = 255;
    }

  /**
   * Sets the lower bound.
   *
   * @param lowerBound Bound below which all pixel values are mapped to
   *                   (0,0,0).
   */
  public void setLowerBound(double lowerBound)
    {
      _lowerBound = lowerBound;
    }

  /**
   * Sets the upper bound.
   *
   * @param upperBound Bound above which all pixel values are mapped to
   *                   (255,255,255).
   */
  public void setUpperBound(double upperBound)
    {
      _upperBound = upperBound;
    }

  /**
   * Gets the red color component of the specified pixel.
   *
   * @param inData Pixel to get the red color component of.
   *
   * @return Red color component of the specified pixel.
   *
   * @throws ClassCastException If the pixel is not an array of the data type
   *                            supported by this class.
   */
  public int getRed(Object inData)
    {
      double pixel = 0;

      // Get the pixel value
      if (inData instanceof byte[]) { pixel = ((byte[])inData)[0]; }
      else if (inData instanceof short[]) { pixel = ((short[])inData)[0]; }
      else if (inData instanceof int[]) { pixel = ((int[])inData)[0]; }
      else if (inData instanceof float[]) { pixel = ((float[])inData)[0]; }
      else if (inData instanceof double[]) { pixel = ((double[])inData)[0]; }
      else { throw new ClassCastException("Unsupported pixel type"); }

      // Return out of bound values
      if (pixel < _lowerBound) { return 0; }
      if (pixel > _upperBound) { return 255; }
      if (_lowerBound == _upperBound) { return 0; }

      // Compute the linearly scaled value
      double slope = (pixel - _lowerBound)/(_upperBound - _lowerBound);
      return (int)(255*slope + 0.5);
    }

  /**
   * Gets the green color component of the specified pixel.
   *
   * @param inData Pixel to get the green color component of.
   *
   * @return Green color component of the specified pixel.
   *
   * @throws ClassCastException If the pixel is not an array of the data type
   *                            supported by this class.
   */
  public int getGreen(Object inData)
    {
      return getRed(inData);
    }

  /**
   * Gets the blue color component of the specified pixel.
   *
   * @param inData Pixel to get the blue color component of.
   *
   * @return Blue color component of the specified pixel.
   *
   * @throws ClassCastException If the pixel is not an array of the data type
   *                            supported by this class.
   */
  public int getBlue(Object inData)
    {
      return getRed(inData);
    }

  /**
   * Gets the number of bits for the given data type.
   *
   * @param dataType Data Buffer type.
   *
   * @return Number of bits for the given data type.
   */
  private static int[] _getNumberOfBits(int dataType)
    {
      int[] numberOfBits = new int[1];
      numberOfBits[0] = DataBuffer.getDataTypeSize(dataType);
      return numberOfBits;
    }
}
