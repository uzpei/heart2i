/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.imageio;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.LookupOp;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.ShortLookupTable;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Array;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadataNode;

/**
 * Utility methods used by the Image I/O plugins.
 *
 * @version 6 October 2008
 */
public class Utilities
{
    /** Buffered Image for 8-bit grayscale data. */
    private static int GRAY_BYTE_IMAGE = 0;

    /** Buffered Image for 16-bit grayscale data. */
    private static int GRAY_USHORT_IMAGE = 1;

    /** Buffered Image for 32-bit RGB data. */
    private static int RGB_IMAGE = 2;

    /** Base name for the jViewBox hints added to Buffered Images. */
    private static String JVIEWBOX_BASE_NAME = "org_medtoolbox_jviewbox_2.0";

    /** Name for the jViewBox minimum pixel value hint. */
    private static String JVIEWBOX_MIN_VALUE = JVIEWBOX_BASE_NAME +
	"->Image_data->minimum";

    /** Name for the jViewBox maximum pixel value hint. */
    private static String JVIEWBOX_MAX_VALUE = JVIEWBOX_BASE_NAME +
	"->Image_data->maximum";

    /** Constructs a Utilities. */
    private Utilities()
    {
    }

    /**
     * Gets the chroma node for a grayscale image in the java Image I/O standard
     * metadata format.
     *
     * @param isInverted True if the pixel values are inverted; false o/w.
     *
     * @return Chroma node for a grayscale image.
     */
    public static IIOMetadataNode getGrayScaleChromaNode(boolean isInverted)
    {
	IIOMetadataNode chromaNode = new IIOMetadataNode("Chroma");

	// Gray color space
	IIOMetadataNode colorSpaceNode = new IIOMetadataNode("ColorSpaceType");
	colorSpaceNode.setAttribute("name", "GRAY");
	chromaNode.appendChild(colorSpaceNode);

	// One channel
	IIOMetadataNode numChannelsNode = new IIOMetadataNode("NumChannels");
	numChannelsNode.setAttribute("value", "1");
	chromaNode.appendChild(numChannelsNode);

	// True if smaller values represent darker shades
	IIOMetadataNode blackIsZeroNode = new IIOMetadataNode("BlackIsZero");
	String blackIsZero = Boolean.toString(!isInverted).toUpperCase();
	blackIsZeroNode.setAttribute("value", blackIsZero);
	chromaNode.appendChild(blackIsZeroNode);

	// Return the chroma node
	return chromaNode;
    }

    /**
     * Gets the chroma node for an RGB image in the java Image I/O standard
     * metadata format.
     *
     * @return Chroma node for an RGB image.
     */
    public static IIOMetadataNode getRgbChromaNode()
    {
	IIOMetadataNode chromaNode = new IIOMetadataNode("Chroma");

	// RGB color space
	IIOMetadataNode colorSpaceNode = new IIOMetadataNode("ColorSpaceType");
	colorSpaceNode.setAttribute("name", "RGB");
	chromaNode.appendChild(colorSpaceNode);

	// Three channels
	IIOMetadataNode numChannelsNode = new IIOMetadataNode("NumChannels");
	numChannelsNode.setAttribute("value", "3");
	chromaNode.appendChild(numChannelsNode);

	// Smaller values represent darker shades
	IIOMetadataNode blackIsZeroNode = new IIOMetadataNode("BlackIsZero");
	blackIsZeroNode.setAttribute("value", "TRUE");
	chromaNode.appendChild(blackIsZeroNode);

	// Return the chroma node
	return chromaNode;
    }

    /**
     * Gets the data node for a grayscale image in the java Image I/O standard
     * metadata format.
     *
     * @param bitsPerSample Number of bits allocated for each pixel sample.
     *
     * @return Data node for a grayscale image.
     */
    public static IIOMetadataNode getGrayScaleDataNode(int bitsPerSample)
    {
	return getGrayScaleDataNode(bitsPerSample, true);
    }

    /**
     * Gets the data node for a grayscale image in the java Image I/O standard
     * metadata format.
     *
     * @param bitsPerSample Number of bits allocated for each pixel sample.
     * @param isIntegral True if the data is integral; false if not.
     *
     * @return Data node for a grayscale image.
     */
    public static IIOMetadataNode getGrayScaleDataNode(int bitsPerSample,
						       boolean isIntegral)
    {
	IIOMetadataNode dataNode = new IIOMetadataNode("Data");

	// Unsigned integer image samples
	IIOMetadataNode formatNode = new IIOMetadataNode("SampleFormat");
	if (isIntegral) { formatNode.setAttribute("value", "UnsignedIntegral");}
	else { formatNode.setAttribute("value", "Real"); }
	dataNode.appendChild(formatNode);

	// Number of bits allocated per sample
	IIOMetadataNode bitsNode = new IIOMetadataNode("BitsPerSample");
	bitsNode.setAttribute("value", Integer.toString(bitsPerSample));
	dataNode.appendChild(bitsNode);

	// Return the data node
	return dataNode;
    }

    /**
     * Gets the data node for an RGB image in the java Image I/O standard
     * metadata format.
     *
     * @return Data node for an RGB image.
     */
    public static IIOMetadataNode getRgbDataNode()
    {
	IIOMetadataNode dataNode = new IIOMetadataNode("Data");

	// Organization of image samples (3 to an integer)
	IIOMetadataNode configNode = new IIOMetadataNode("PlanarConfiguration");
	configNode.setAttribute("value", "PixelInterleaved");
	dataNode.appendChild(configNode);

	// Unsigned integer image samples
	IIOMetadataNode formatNode = new IIOMetadataNode("SampleFormat");
	formatNode.setAttribute("value", "UnsignedIntegral");
	dataNode.appendChild(formatNode);

	// Number of bits allocated per sample
	IIOMetadataNode bitsNode = new IIOMetadataNode("BitsPerSample");
	bitsNode.setAttribute("value", "8 8 8");
	dataNode.appendChild(bitsNode);

	// Number of bits used per sample
	IIOMetadataNode usedNode =
	    new IIOMetadataNode("SignificantBitsPerSample");
	usedNode.setAttribute("value", "8 8 8");
	dataNode.appendChild(usedNode);

	// Return the data node
	return dataNode;
    }

    /**
     * Gets the dimension node for an image in the java Image I/O standard
     * metadata format.
     *
     * @param pixelWidth Width of a pixel in millimeters; if this is zero then
     *                   no pixel width is assumed.
     * @param pixelHeight Height of a pixel in millimeters; if this is zero then
     *                    no pixel height is assumed.
     *
     * @return Dimension node for an image.
     */
    public static IIOMetadataNode getDimensionNode(float pixelWidth,
						   float pixelHeight)
    {
	IIOMetadataNode dimNode = new IIOMetadataNode("Dimension");

	// Use the absolute quantities
	pixelWidth = Math.abs(pixelWidth);
	pixelHeight = Math.abs(pixelHeight);

	// Width of a pixel divided by its height
	if (pixelWidth != 0 && pixelHeight != 0) {
	    IIOMetadataNode aspNode = new IIOMetadataNode("PixelAspectRatio");
	    aspNode.setAttribute("value",
				 Float.toString(pixelWidth/pixelHeight));
	    dimNode.appendChild(aspNode);
	}	

	// Desired orientation of the image in terms of flips and cc rotations
	IIOMetadataNode orientNode = new IIOMetadataNode("ImageOrientation");
	orientNode.setAttribute("value", "Normal");
	dimNode.appendChild(orientNode);

	// Width of a pixel (in millimeters) as it should be rendered on media
	if (pixelWidth != 0) {
	    IIOMetadataNode widthNode =
		new IIOMetadataNode("HorizontalPixelSize");
	    widthNode.setAttribute("value", Float.toString(pixelWidth));
	    dimNode.appendChild(widthNode);
	}

	// Height of a pixel (in millimeters) as it should be rendered on media
	if (pixelHeight != 0) {
	    IIOMetadataNode heightNode =
		new IIOMetadataNode("VerticalPixelSize");
	    heightNode.setAttribute("value", Float.toString(pixelHeight));
	    dimNode.appendChild(heightNode);
	}

	// Return the dimension node
	return dimNode;
    }

    /**
     * Gets the Image Type Specifier for grayscale images.  This Specifier is
     * optimal for JViewBox.
     *
     * @param width Width of the image.
     * @param height Height of the image.
     * @param bitsPerPixel Number of bits used to represent each pixel value.
     *
     * @return Image Type Specifier for grayscale images.
     *
     * @throws IllegalArgumentException If the width or height is less than one,
     *                                  or if the bits per pixel not supported.
     */
    public static ImageTypeSpecifier getGrayImageType(int width, int height,
						      int bitsPerPixel)
    {
	// Check that the image width and height are not less than 1
	if (width < 1 || height < 1) {
	    String msg = "Image width or height is less than 1.";
	    throw new IllegalArgumentException(msg);
	}

	// Check that the number of bits per pixel is supported
	if (bitsPerPixel < 1 || bitsPerPixel > 16) {
	    String msg = bitsPerPixel + " bits per pixel is not supported.";
	    throw new IllegalArgumentException(msg);
	}

	// Determine the data type
	int dataType = DataBuffer.TYPE_USHORT;
	if (bitsPerPixel <= 8) { dataType = DataBuffer.TYPE_BYTE; }

	// Store the sample of each pixel into 1 data array element
	int pixelStride = 1;
	int scanlineStride = width;
	int[] offset = new int[1];
	SampleModel sm =
	    new PixelInterleavedSampleModel(dataType, width, height,
					    pixelStride, scanlineStride,offset);

	// Create an indexed map for each color component
	byte[] map = new byte[1 << bitsPerPixel];
	for (int i = 0; i < map.length; i++) {
	    map[i] = (byte)(255*i/(double)map.length);
	}

	// Use the indexed map to define the red, green, and blue colors
	ColorModel cm = new IndexColorModel(bitsPerPixel, map.length,
					    map, map, map);

	// Return the grayscale Image Type Specifier
	return new ImageTypeSpecifier(cm, sm);
    }

    /**
     * Gets the Image Type Specifier for grayscale images.
     *
     * @param dataType Data type of the image pixels; this is a type defined in
     *                 the Data Buffer class.
     *
     * @return Image Type Specifier for grayscale images.
     *
     * @throws IllegalArgumentException If the data type is not supported.
     */
    public static ImageTypeSpecifier getGrayImageType(int dataType)
    {
	// Create the Color Model (map values linearly to gray scale)
	ColorModel cm = new LinearScaleGrayColorModel(dataType);

	// Store the sample of each pixel into 1 data array element
	int[] offset = new int[1];
	SampleModel sm = new PixelInterleavedSampleModel(dataType, 1, 1, 1, 1,
							 offset);

	// Return the grayscale Image Type Specifier
	return new ImageTypeSpecifier(cm, sm);
    }

    /**
     * Gets the Image Type Specifier for RGB images.
     *
     * @param width Width of the image.
     * @param height Height of the image.
     *
     * @return Image Type Specifier for RGB images.
     *
     * @throws IllegalArgumentException If the width or height is less than one.
     */
    public static ImageTypeSpecifier getRgbImageType(int width, int height)
    {
	// Check that the image width and height are not less than 1
	if (width < 1 || height < 1) {
	    String msg = "Image width or height is less than 1.";
	    throw new IllegalArgumentException(msg);
	}

	// Store each image pixel sample in an integer
	int dataType = DataBuffer.TYPE_INT;
	int[] masks = {0x00ff0000, 0x0000ff00, 0x000000ff};

	// Store the 3 color samples of each pixel into 1 data array element 
	int scanlineStride = width;
	SampleModel sm =
	    new SinglePixelPackedSampleModel(dataType, width, height,
					     scanlineStride, masks);

	// Define the 2nd 8 bits as red, 3rd as green, and 4th as blue
	ColorModel cm = new DirectColorModel(24, masks[0], masks[1], masks[2]);

	// Return the RGB Image Type Specifier
	return new ImageTypeSpecifier(cm, sm);
    }

    /**
     * Determines if the parameters indicate that image data is to be read as an
     * undecoded array of bytes.
     *
     * @return True if image data is to be read as an undecoded array of bytes;
     *         false otherwise.
     */
    public static boolean returnRawBytes(ImageReadParam param)
    {
	// Check for the LONI flag
	if (param instanceof LoniImageReadParam) {
	    LoniImageReadParam loniParam = (LoniImageReadParam)param;
	    return loniParam.returnRawBytes();
	}

	return false;
    }

    /**
     * Creates a Buffered Image for 8-bit grayscale data.  Eight bits per image
     * pixel is used and no inversion is performed.
     *
     * @param srcBuffer Buffer of byte image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for 8-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the byte
     *                                  type.
     */
    public static BufferedImage getGrayByteImage(byte[] srcBuffer,
						 WritableRaster dstRaster)
    {
	return getGrayByteImage(srcBuffer, dstRaster, 8, false);
    }

    /**
     * Creates a Buffered Image for 8-bit grayscale data.
     *
     * @param srcBuffer Buffer of byte image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     * @param bitsUsedPerPixel Number of lower bits actually used to represent
     *                         each image pixel value.
     * @param isInverted True if the mapping is onto the byte range (0, 255);
     *                   false if the mapping is onto the byte range (255, 0).
     *
     * @return Buffered Image for 8-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the bits used per pixel is not valid,
     *                                  if the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the byte
     *                                  type.
     */
    public static BufferedImage getGrayByteImage(byte[] srcBuffer,
						 WritableRaster dstRaster,
						 int bitsUsedPerPixel,
						 boolean isInverted)
    {
	// Check the bits used per pixel
	if (bitsUsedPerPixel < 0 || bitsUsedPerPixel > 8) {
	    String msg = bitsUsedPerPixel + " bits used per pixel is not " +
		"supported.";
	    throw new IllegalArgumentException(msg);
	}

	// Check the size of the source buffer
	int width = dstRaster.getWidth();
	int height = dstRaster.getHeight();
	if (srcBuffer.length < width*height) {
	    String msg = "Source buffer of " + srcBuffer.length + " bytes is " +
		"smaller than the destination buffer of " + width*height;
	    throw new IllegalArgumentException(msg);
	}

	// Verify that the destination Raster holds a byte buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_BYTE) {
	    String msg = "Destination buffer needs to be of the byte type.";
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination byte buffer
	byte[] dstBuffer = ((DataBufferByte)dstDataBuffer).getData();
	_copyBytes(srcBuffer, dstBuffer, bitsUsedPerPixel);

	// Return the Buffered Image
	Range arrayRange = _computeMinAndMax(dstBuffer, bitsUsedPerPixel);
	return _createGrayImage(dstRaster, arrayRange, bitsUsedPerPixel,
				isInverted);
    }

    /**
     * Creates a Buffered Image for 16-bit grayscale data.  Sixteen bits per
     * image pixel is used and no inversion, mapping, or byte swapping is
     * performed.
     *
     * @param srcBuffer Buffer of byte image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for 16-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  unsigned short type.
     */
    public static BufferedImage getGrayUshortImage(byte[] srcBuffer,
						   WritableRaster dstRaster)
    {
	return getGrayUshortImage(srcBuffer, dstRaster, 16, false, false, 1, 0);
    }

    /**
     * Creates a Buffered Image for 16-bit grayscale data.  Sixteen bits per
     * image pixel is used and no inversion, mapping, or byte swapping is
     * performed.
     *
     * @param srcBuffer Buffer of short image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for 16-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  unsigned short type.
     */
    public static BufferedImage getGrayUshortImage(short[] srcBuffer,
						   WritableRaster dstRaster)
    {
	return getGrayUshortImage(srcBuffer, dstRaster, 16, false, 1, 0);
    }

    /**
     * Creates a Buffered Image for 16-bit grayscale data.
     *
     * @param srcBuffer Buffer of byte image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     * @param bitsUsedPerPixel Number of lower bits actually used to represent
     *                         each image pixel value.
     * @param isInverted True if the mapping is onto the byte range (0, 255);
     *                   false if the mapping is onto the byte range (255, 0).
     * @param isByteSwapped True if every 2 consecutive bytes should be swapped
     *                      before combining them into an unsigned short; false
     *                      otherwise.
     * @param slope Factor multiplied to each short.
     * @param offset Amount added to each short after slope multiplication.
     *
     * @return Buffered Image for 16-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the bits used per pixel is not valid,
     *                                  if the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  unsigned short type.
     */
    public static BufferedImage getGrayUshortImage(byte[] srcBuffer,
						   WritableRaster dstRaster,
						   int bitsUsedPerPixel,
						   boolean isInverted,
						   boolean isByteSwapped,
						   double slope, int offset)
    {
	// Check the size of the source buffer
	int width = dstRaster.getWidth();
	int height = dstRaster.getHeight();
	if (srcBuffer.length < 2*width*height) {
	    String msg = "Source buffer of " + srcBuffer.length + " bytes is " +
		"smaller than the dest buffer of " + 2*width*height;
	    throw new IllegalArgumentException(msg);
	}

	// Verify that the destination Raster holds an unsigned short buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_USHORT) {
	    String msg = "Destination buffer needs to be of the ushort type.";
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	short[] dstBuffer = ((DataBufferUShort)dstDataBuffer).getData();
	_copyBytes(srcBuffer, dstBuffer, isByteSwapped);

	// Create the Buffered Image
	return getGrayUshortImage(dstBuffer, dstRaster, bitsUsedPerPixel,
				  isInverted, slope, offset);
    }

    /**
     * Creates a Buffered Image for 16-bit grayscale data.
     *
     * @param srcBuffer Buffer of short image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     * @param bitsUsedPerPixel Number of lower bits actually used to represent
     *                         each image pixel value.
     * @param isInverted True if the mapping is onto the byte range (0, 255);
     *                   false if the mapping is onto the byte range (255, 0).
     * @param slope Factor multiplied to each short.
     * @param offset Amount added to each short after slope multiplication.
     *
     * @return Buffered Image for 16-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the bits used per pixel is not valid,
     *                                  if the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  unsigned short type.
     */
    public static BufferedImage getGrayUshortImage(short[] srcBuffer,
						   WritableRaster dstRaster,
						   int bitsUsedPerPixel,
						   boolean isInverted,
						   double slope, int offset)
    {
	// Check the bits used per pixel
	if (bitsUsedPerPixel < 0 || bitsUsedPerPixel > 16) {
	    String msg = bitsUsedPerPixel + " bits used per pixel is not " +
		"supported.";
	    throw new IllegalArgumentException(msg);
	}

	// Check the size of the source buffer
	int width = dstRaster.getWidth();
	int height = dstRaster.getHeight();
	if (srcBuffer.length < width*height) {
	    String msg = "Source buffer of " + srcBuffer.length + " bytes is " +
		"smaller than the dest buffer of " + width*height;
	    throw new IllegalArgumentException(msg);
	}

	// Verify that the destination Raster holds an unsigned short buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_USHORT) {
	    String msg = "Destination buffer needs to be of the ushort type.";
	    throw new IllegalArgumentException(msg);
	}

	// Copy the shorts from the source buffer to the destination buffer
	short[] dstBuffer = ((DataBufferUShort)dstDataBuffer).getData();
	if (srcBuffer != dstBuffer) {
	    System.arraycopy(srcBuffer, 0, dstBuffer, 0, dstBuffer.length);
	}

	// Remap the image pixel values using the slope and offset
	Range arrayRange = _remap(dstBuffer, slope, offset, bitsUsedPerPixel);

	// Return the Buffered Image
	return _createGrayImage(dstRaster, arrayRange, bitsUsedPerPixel,
				isInverted);
    }

    /**
     * Creates a Buffered Image for signed 16-bit grayscale data.  Sixteen bits
     * per image pixel is used and no inversion, mapping, or byte swapping is
     * performed.
     *
     * @param srcBuffer Buffer of signed 16-bit image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     * @param isByteSwapped True if every 2 consecutive bytes should be swapped
     *                      before combining them into an unsigned short; false
     *                      otherwise.
     *
     * @return Buffered Image for signed 16-bit grayscale data using the
     *         destination Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  short type.
     */
    public static BufferedImage getGrayShortImage(byte[] srcBuffer,
						  WritableRaster dstRaster,
						  boolean isByteSwapped)
    {
	// Check the size of the source buffer
	int width = dstRaster.getWidth();
	int height = dstRaster.getHeight();
	if (srcBuffer.length < 2*width*height) {
	    String msg = "Source buffer of " + srcBuffer.length + " bytes is " +
		"smaller than the dest buffer of " + 2*width*height;
	    throw new IllegalArgumentException(msg);
	}

	// Verify that the destination Raster holds a short buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_SHORT) {
	    String msg = "Destination buffer needs to be of the short type.";
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	short[] dstBuffer = ((DataBufferShort)dstDataBuffer).getData();
	_copyBytes(srcBuffer, dstBuffer, isByteSwapped);

	// Create the Buffered Image
	return getGrayShortImage(dstBuffer, dstRaster);
    }

    /**
     * Creates a Buffered Image for signed 16-bit grayscale data.
     *
     * @param srcBuffer Buffer of signed 16-bit image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for signed 16-bit grayscale data using the
     *         destination Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  short type.
     */
    public static BufferedImage getGrayShortImage(short[] srcBuffer,
						  WritableRaster dstRaster)
    {
	// Verify that the destination Raster holds a short buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_SHORT) {
	    String msg = "Destination buffer needs to be of the short type.";
	    throw new IllegalArgumentException(msg);
	}

	// Check the size of the source buffer
	short[] dstBuffer = ((DataBufferShort)dstDataBuffer).getData();
	if (srcBuffer.length < dstBuffer.length) {
	    String msg = "Source buffer of size " + srcBuffer.length + " is " +
		"smaller than the destination buffer of size " +
		dstBuffer.length;
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	if (srcBuffer != dstBuffer) {
	    System.arraycopy(srcBuffer, 0, dstBuffer, 0, dstBuffer.length);
	}

	// Determine the maximum and minimum
	int min = Integer.MAX_VALUE;
	int max = Integer.MIN_VALUE;
	for (int i = 0; i < dstBuffer.length; i++) {
	    if (dstBuffer[i] < min) { min = dstBuffer[i]; }
	    if (dstBuffer[i] > max) { max = dstBuffer[i]; }
	}

	// Return the Buffered Image
	return _createGrayImage(dstRaster, min, max);
    }

    /**
     * Creates a Buffered Image for 32-bit grayscale data.
     *
     * @param srcBuffer Buffer of integer image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for 32-bit grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  integer type.
     */
    public static BufferedImage getGrayIntImage(int[] srcBuffer,
						WritableRaster dstRaster)
    {
	// Verify that the destination Raster holds an integer buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_INT) {
	    String msg = "Destination buffer needs to be of the integer type.";
	    throw new IllegalArgumentException(msg);
	}

	// Check the size of the source buffer
	int[] dstBuffer = ((DataBufferInt)dstDataBuffer).getData();
	if (srcBuffer.length < dstBuffer.length) {
	    String msg = "Source buffer of size " + srcBuffer.length + " is " +
		"smaller than the destination buffer of size " +
		dstBuffer.length;
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	System.arraycopy(srcBuffer, 0, dstBuffer, 0, dstBuffer.length);

	// Determine the maximum and minimum
	int min = Integer.MAX_VALUE;
	int max = Integer.MIN_VALUE;
	for (int i = 0; i < dstBuffer.length; i++) {
	    if (dstBuffer[i] < min) { min = dstBuffer[i]; }
	    if (dstBuffer[i] > max) { max = dstBuffer[i]; }
	}

	// Return the Buffered Image
	return _createGrayImage(dstRaster, min, max);
    }

    /**
     * Creates a Buffered Image for float grayscale data.
     *
     * @param srcBuffer Buffer of float image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for float grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  Raster is not of the float type.
     */
    public static BufferedImage getGrayFloatImage(float[] srcBuffer,
						  WritableRaster dstRaster)
    {
	// Verify that the destination Raster holds a float buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_FLOAT) {
	    String msg = "Destination buffer needs to be of the float type.";
	    throw new IllegalArgumentException(msg);
	}

	// Check the size of the source buffer
	float[] dstBuffer = ((DataBufferFloat)dstDataBuffer).getData();
	if (srcBuffer.length < dstBuffer.length) {
	    String msg = "Source buffer of size " + srcBuffer.length + " is " +
		"smaller than the destination buffer of size " +
		dstBuffer.length;
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	System.arraycopy(srcBuffer, 0, dstBuffer, 0, dstBuffer.length);

	// Determine the maximum and minimum
	float min = Float.POSITIVE_INFINITY;
	float max = Float.NEGATIVE_INFINITY;
	for (int i = 0; i < dstBuffer.length; i++) {
	    if (dstBuffer[i] < min) { min = dstBuffer[i]; }
	    if (dstBuffer[i] > max) { max = dstBuffer[i]; }
	}

	// Return the Buffered Image
	return _createGrayImage(dstRaster, min, max);
    }

    /**
     * Creates a Buffered Image for double grayscale data.
     *
     * @param srcBuffer Buffer of double image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     *
     * @return Buffered Image for double grayscale data using the destination
     *         Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  Raster is not of the double type.
     */
    public static BufferedImage getGrayDoubleImage(double[] srcBuffer,
						   WritableRaster dstRaster)
    {
	// Verify that the destination Raster holds a double buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_DOUBLE) {
	    String msg = "Destination buffer needs to be of the double type.";
	    throw new IllegalArgumentException(msg);
	}

	// Check the size of the source buffer
	double[] dstBuffer = ((DataBufferDouble)dstDataBuffer).getData();
	if (srcBuffer.length < dstBuffer.length) {
	    String msg = "Source buffer of size " + srcBuffer.length + " is " +
		"smaller than the destination buffer of size " +
		dstBuffer.length;
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	System.arraycopy(srcBuffer, 0, dstBuffer, 0, dstBuffer.length);

	// Determine the maximum and minimum
	double min = Double.POSITIVE_INFINITY;
	double max = Double.NEGATIVE_INFINITY;
	for (int i = 0; i < dstBuffer.length; i++) {
	    if (dstBuffer[i] < min) { min = dstBuffer[i]; }
	    if (dstBuffer[i] > max) { max = dstBuffer[i]; }
	}

	// Return the Buffered Image
	return _createGrayImage(dstRaster, min, max);
    }

    /**
     * Creates a Buffered Image for grayscale data.  The minimum and maximum
     * data values determine the linear mapping used when viewing.  This
     * Buffered Image is not optimal for JViewBox.
     *
     * @param srcBuffer Buffer of byte image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     * @param isByteSwapped True if consecutive bytes should be swapped before
     *                      combining them into the destination data type; false
     *                      otherwise.
     *
     * @return Buffered Image for grayscale data using the destination Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer.
     */
    public static BufferedImage getGrayImage(byte[] srcBuffer,
					     WritableRaster dstRaster,
					     boolean isByteSwapped)
    {
	// Determine the destination data type
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	int dstDataType = dstDataBuffer.getDataType();

	// Check the size of the source buffer
	int width = dstRaster.getWidth();
	int height = dstRaster.getHeight();
	int dstTypeBytes = DataBuffer.getDataTypeSize(dstDataType)/8;
	if (srcBuffer.length < dstTypeBytes*width*height) {
	    String msg = "Source buffer of " + srcBuffer.length + " bytes is " +
		"smaller than the dest buffer of " +
		dstTypeBytes*width*height;
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	double[] minMax = new double[2];
	if (dstDataType == DataBuffer.TYPE_BYTE) {
	    byte[] dstBuffer = ((DataBufferByte)dstDataBuffer).getData();
	    _copyBytes(srcBuffer, dstBuffer, 8);
	    minMax[0] = 0;
	    minMax[1] = 255;
	}
	else if (dstDataType == DataBuffer.TYPE_USHORT) {
	    short[] dstBuffer = ((DataBufferUShort)dstDataBuffer).getData();
	    minMax = _copyBytes(srcBuffer, dstBuffer, isByteSwapped);
	}
	else if (dstDataType == DataBuffer.TYPE_SHORT) {
	    short[] dstBuffer = ((DataBufferShort)dstDataBuffer).getData();
	    minMax = _copyBytes(srcBuffer, dstBuffer, isByteSwapped);
	    }
	else if (dstDataType == DataBuffer.TYPE_INT) {
	    int[] dstBuffer = ((DataBufferInt)dstDataBuffer).getData();
	    minMax = _copyBytes(srcBuffer, dstBuffer, isByteSwapped);
	}
	else if (dstDataType == DataBuffer.TYPE_FLOAT) {
	    float[] dstBuffer = ((DataBufferFloat)dstDataBuffer).getData();
	    minMax = _copyBytes(srcBuffer, dstBuffer, isByteSwapped);
	}
	else if (dstDataType == DataBuffer.TYPE_DOUBLE) {
	    double[] dstBuffer = ((DataBufferDouble)dstDataBuffer).getData();
	    minMax = _copyBytes(srcBuffer, dstBuffer, isByteSwapped);
	}

	// Return the Buffered Image
	return _createGrayImage(dstRaster, minMax[0], minMax[1]);
    }

    /**
     * Creates a Buffered Image for 32-bit RGB data.
     *
     * @param srcBuffer Buffer of byte image pixel data.
     * @param dstRaster Writable Raster to fill with image pixel data.
     * @param isInterleaved True if every 3 consecutive bytes correspond to one
     *                      image pixel value, false if the RGB bands are
     *                      grouped together.
     *
     * @return Buffered Image for 32-bit RGB data using the destination Raster.
     *
     * @throws IllegalArgumentException If the source buffer is smaller than the
     *                                  destination buffer, or if the
     *                                  destination Raster is not of the
     *                                  Raster is not of the int type.
     */
    public static BufferedImage getRgbImage(byte[] srcBuffer,
					    WritableRaster dstRaster,
					    boolean isInterleaved)
    {
	// Check the size of the source buffer
	int width = dstRaster.getWidth();
	int height = dstRaster.getHeight();
	if (srcBuffer.length < 3*width*height) {
	    String msg = "Source buffer of " + srcBuffer.length + " bytes is " +
		"smaller than the dest buffer of " + 3*width*height;
	    throw new IllegalArgumentException(msg);
	}

	// Verify that the destination Raster holds an unsigned short buffer
	DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
	if (dstDataBuffer.getDataType() != DataBuffer.TYPE_INT) {
	    String msg = "Destination buffer needs to be of the int type.";
	    throw new IllegalArgumentException(msg);
	}

	// Copy the source bytes to the destination buffer
	int[] dstBuffer = ((DataBufferInt)dstDataBuffer).getData();
	if (isInterleaved) { _copyRgbInterleaved(srcBuffer, dstBuffer); }
	else { _copyRgbBanded(srcBuffer, dstBuffer); }

	// Define the 2nd 8 bits as red, 3rd as green, and 4th as blue
	int[] masks = {0x00ff0000, 0x0000ff00, 0x000000ff};
	ColorModel cm = new DirectColorModel(24, masks[0], masks[1], masks[2]);

	// Return the Buffered Image
	return new BufferedImage(cm, dstRaster, false, new Hashtable());
    }

    /**
     * Creates a Buffered Image to be used to return an undecoded image.  The
     * properties of the Buffered Image are to be ignored as they serve only
     * to wrap the image data in the byte array.
     *
     * @param dataSize Size of the byte array in the Buffered Image.
     *
     * @return Buffered Image for use in returning an undecoded image.
     *
     * @throws IllegalArgumentException If the data size is negative.
     */
    public static BufferedImage getRawByteImage(int dataSize)
    {
	// Check the data size
	if (dataSize < 0) { 
	    throw new IllegalArgumentException("Negative data size.");
	}

	// Return a fake Buffered Image
	return new BufferedImage(dataSize, 1, BufferedImage.TYPE_BYTE_GRAY);
    }

    /**
     * Decodes the specified array of bytes into a Buffered Image.  The encoding
     * is assumed to have been performed with the encodeImage method of this
     * class.
     *
     * @param array Array of bytes containing an encoded Buffered Image.
     *
     * @return Buffered Image decoded from the array of bytes.
     *
     * @throws IllegalArgumentException If the byte array cannot be decoded.
     */
    public static BufferedImage decodeImage(byte[] array)
    {
	try {
	    ByteArrayInputStream byteStream = new ByteArrayInputStream(array);
	    DataInputStream dataStream = new DataInputStream(byteStream);

	    // Determine if the pixels are compressed
	    boolean compressPixels = dataStream.readBoolean();

	    // Get the image width and height
	    int width = dataStream.readInt();
	    int height = dataStream.readInt();
	    if (width <= 0 || height <= 0) {
		String msg = "Invalid image width (" + width +
		    ") and/or height (" + height + ").";
		throw new IllegalArgumentException(msg);
	    }

	    // Get the image type
	    int imageType = dataStream.readInt();
	    if (imageType != GRAY_BYTE_IMAGE &&
		imageType != GRAY_USHORT_IMAGE &&
		imageType != RGB_IMAGE)
		{
		    String msg = "Invalid image type:  " + imageType + ".";
		    throw new IllegalArgumentException(msg);
		}

	    // Get the bits used per pixel and the image minimum and maximum
	    int bitsPerPixel = dataStream.readInt();
	    int min = dataStream.readInt();
	    int max = dataStream.readInt();

	    // Get an Input Stream to the pixel data
	    DataInputStream pixelStream = dataStream;
	    if (compressPixels) {

		// Initialize an array to read into
		int size = width*height;
		if (imageType == GRAY_USHORT_IMAGE) { size *= 2; }
		if (imageType == RGB_IMAGE) { size *= 4; }
		byte[] pixelData = new byte[size];

		// Read the pixels
		ZipInputStream zipStream = new ZipInputStream(dataStream);
		ZipEntry entry = zipStream.getNextEntry();
		DataInputStream dataZipStream = new DataInputStream(zipStream);
		dataZipStream.readFully(pixelData);
		zipStream.closeEntry();
		zipStream.close();

		// Reset the Input Stream to the pixel data
		ByteArrayInputStream baStream =
		    new ByteArrayInputStream(pixelData);
		pixelStream = new DataInputStream(baStream);
	    }

	    // Create the Writable Raster for the decoded Buffered Image
	    ImageTypeSpecifier specifier;
	    if (imageType == RGB_IMAGE) {
		specifier = getRgbImageType(width, height);
	    }
	    else { specifier = getGrayImageType(width, height, bitsPerPixel); }
	    BufferedImage tempImage = specifier.createBufferedImage(width,
								    height);
	    WritableRaster raster = tempImage.getRaster();

	    // Create the decoded Buffered Image
	    DataBuffer dataBuffer = raster.getDataBuffer();

	    if (imageType == GRAY_BYTE_IMAGE) {

		// Copy the pixel data into the Writable Raster
		byte[] pixels = ((DataBufferByte)dataBuffer).getData();
		pixelStream.readFully(pixels);

		// Return the decoded Buffered Image
		Range arrayRange = new Range();
		arrayRange.adjust(min);
		arrayRange.adjust(max);
		return _createGrayImage(raster, arrayRange, bitsPerPixel,false);
	    }

	    else if (imageType == GRAY_USHORT_IMAGE) {

		// Copy the pixel data into the Writable Raster
		short[] pixels = ((DataBufferUShort)dataBuffer).getData();
		for (int i = 0; i < pixels.length; i++) {
		    pixels[i] = pixelStream.readShort();
		}

		// Return the decoded Buffered Image
		Range arrayRange = new Range();
		arrayRange.adjust(min);
		arrayRange.adjust(max);
		return _createGrayImage(raster, arrayRange, bitsPerPixel,false);
	    }

	    else if (imageType == RGB_IMAGE) {

		// Copy the pixel data into the Writable Raster
		int[] pixels = ((DataBufferInt)dataBuffer).getData();
		for (int i = 0; i < pixels.length; i++) {
		    pixels[i] = pixelStream.readInt();
		}

		// Return the decoded Buffered Image
		int[] mks = {0x00ff0000, 0x0000ff00, 0x000000ff};
		ColorModel cm = new DirectColorModel(24, mks[0], mks[1],mks[2]);
		return new BufferedImage(cm, raster, false, new Hashtable());
	    }

	    return null;
	}

	catch (Exception e) {
	    String msg = "Unable to decode the byte array.";
	    IllegalArgumentException throwable =
		new IllegalArgumentException(msg);
	    throwable.initCause(e);
	    throw throwable;
	}
    }

    /**
     * Encodes the specified Buffered Image into an array of bytes.
     *
     * @param image Buffered Image to encode.
     * @param compressPixels True if the image pixels are to be compressed
     *                       during the encoding; false otherwise.
     *
     * @return Byte array containing the encoded Buffered Image.
     *
     * @throws IllegalArgumentException If encoding of the Buffered Image is not
     *                                  supported or cannot be performed.
     */
    public static byte[] encodeImage(BufferedImage image,
				     boolean compressPixels)
    {
	try {

	    // Initialize the internal byte array
	    int size = 1 + 8 + 8 + 8 + image.getWidth()*image.getHeight();
	    ByteArrayOutputStream byteStream = new ByteArrayOutputStream(size);
	    DataOutputStream dataStream = new DataOutputStream(byteStream);

	    // 1 byte for compression
	    dataStream.writeBoolean(compressPixels);

	    // 8 bytes for the image width and height
	    dataStream.writeInt( image.getWidth() );
	    dataStream.writeInt( image.getHeight() );

	    // Determine the image type
	    int imageType = -1;
	    ColorModel cm = image.getColorModel();
	    DataBuffer dataBuffer = image.getRaster().getDataBuffer();
	    int dataType = dataBuffer.getDataType();
	    if (cm instanceof IndexColorModel) {
		if (dataType == DataBuffer.TYPE_BYTE) {
		    imageType = GRAY_BYTE_IMAGE;
		}
		else if (dataType == DataBuffer.TYPE_USHORT) {
		    imageType = GRAY_USHORT_IMAGE;
		}
	    }
	    if (cm instanceof DirectColorModel &&
		dataType == DataBuffer.TYPE_INT)
		{
		    imageType = RGB_IMAGE;
		}

	    // Encoding not supported
	    if (imageType == -1) {
		String msg = "Encoding of the Buffered Image is not supported.";
		throw new IllegalArgumentException(msg);
	    }

	    // 8 bytes for the image type and bits used per pixel
	    dataStream.writeInt(imageType);
	    dataStream.writeInt( cm.getPixelSize() );

	    // Get the image minimum and maximum pixel value from the Image
	    int min;
	    int max;
	    Object minValue = image.getProperty(JVIEWBOX_MIN_VALUE);
	    Object maxValue = image.getProperty(JVIEWBOX_MAX_VALUE);
	    if (minValue instanceof Integer && maxValue instanceof Integer) {
		min = ((Integer)minValue).intValue();
		max = ((Integer)maxValue).intValue();
	    }

	    // Image min and max not available so compute them
	    else {
		Range range;
		if (imageType == GRAY_BYTE_IMAGE) {
		    DataBufferByte byteBuffer = (DataBufferByte)dataBuffer;
		    range = _computeMinAndMax(byteBuffer.getData(), 8);
		}
		else if (imageType == GRAY_USHORT_IMAGE) {
		    DataBufferUShort shortBuffer = (DataBufferUShort)dataBuffer;
		    range = _computeMinAndMax(shortBuffer.getData(), 16);
		}
		else {
		    range = new Range();
		    range.adjust(0);
		}

		min = range.getMinimum();
		max = range.getMaximum();
	    }

	    // 8 bytes for the image minimum and maximum
	    dataStream.writeInt(min);
	    dataStream.writeInt(max);

	    // Write uncompressed image pixels
	    if (!compressPixels) {

		if (imageType == GRAY_BYTE_IMAGE) {
		    byte[] array = ((DataBufferByte)dataBuffer).getData();
		    byteStream.write(array, 0, array.length);
		}

		else if (imageType == GRAY_USHORT_IMAGE) {
		    short[] array = ((DataBufferUShort)dataBuffer).getData();
		    for (int i = 0; i < array.length; i++) {
			dataStream.writeShort(array[i]);
		    }
		}

		else if (imageType == RGB_IMAGE) {
		    int[] array = ((DataBufferInt)dataBuffer).getData();
		    for (int i = 0; i < array.length; i++) {
			dataStream.writeInt(array[i]);
		    }
		}
	    }

	    // Write compressed image pixels
	    else {

		// Get the image pixel as a byte array
		Object pixelData = null;
		if (imageType == GRAY_BYTE_IMAGE) {
		    pixelData = ((DataBufferByte)dataBuffer).getData();
		}
		else if (imageType == GRAY_USHORT_IMAGE) {
		    pixelData = ((DataBufferUShort)dataBuffer).getData();
		}
		else if (imageType == RGB_IMAGE) {
		    pixelData = ((DataBufferInt)dataBuffer).getData();
		}
		byte[] array = _getByteArray(pixelData);

		// Compress the pixels
		ZipOutputStream zipStream = new ZipOutputStream(dataStream);
		ZipEntry entry = new ZipEntry("Pixel Data");

		// Write the pixels
		zipStream.putNextEntry(entry);
		zipStream.write(array);
		zipStream.closeEntry();

		zipStream.close();
	    }

	    // Return the encoded pixels
	    return byteStream.toByteArray();
	}

	catch (Exception e) {
	    String msg = "Unable to decode the byte array.";
	    IllegalArgumentException throwable =
		new IllegalArgumentException(msg);
	    throwable.initCause(e);
	    throw throwable;
	}
    }

    /**
     * Gets a byte array from the specified array.
     *
     * @param array Array from which to get an array of bytes.
     *
     * @return Byte array containing data from the specified array, or null if
     *         no byte array could be obtained.
     */
    private static byte[] _getByteArray(Object array)
    {
	// Byte array
	if (array instanceof byte[]) { return (byte[])array; }

	// Short array
	else if (array instanceof short[]) {
	    short[] shortArray = (short[])array;

	    // Create a byte array to hold the data
	    byte[] byteArray = new byte[2*shortArray.length];

	    // Add the shorts to the byte array
	    for (int i = 0; i < shortArray.length; i++) {
		int v = shortArray[i];
		byteArray[2*i] =   (byte)((v >>> 8) & 0xFF);
		byteArray[2*i+1] = (byte)((v >>> 0) & 0xFF);
	    }

	    // Return the byte array
	    return byteArray;
	}

	// Integer array
	else if (array instanceof int[]) {
	    int[] intArray = (int[])array;

	    // Create a byte array to hold the data
	    byte[] byteArray = new byte[4*intArray.length];

	    // Add the integers to the byte array
	    for (int i = 0; i < intArray.length; i++) {
		int v = intArray[i];
		byteArray[4*i] =   (byte)((v >>> 24) & 0xFF);
		byteArray[4*i+1] = (byte)((v >>> 16) & 0xFF);
		byteArray[4*i+2] = (byte)((v >>> 8) & 0xFF);
		byteArray[4*i+3] = (byte)((v >>> 0) & 0xFF);
	    }

	    // Return the byte array
	    return byteArray;
	}

	// Conversion not supported
	return null;
    }

    /**
     * Reads each byte from the first byte array and writes it to the second
     * byte array after masking it by the largest allowed byte value.
     *
     * @param byteArray1 First byte array that is read from.
     * @param byteArray2 Second byte array that is written to.  This array must
     *                   be equal to or greater in size than the first array.
     * @param bitsUsedPerByte Number of lower bits used (<=8) to represent each
     *                        byte value.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static void _copyBytes(byte[] byteArray1, byte[] byteArray2,
				   int bitsUsedPerByte)
    {
	try {

	    // Perform a system copy if no masking is needed
	    if (bitsUsedPerByte == 8) {
		System.arraycopy(byteArray1, 0, byteArray2, 0,
				 byteArray1.length);
		return;
	    }

	    // Determine the largest allowed value
	    int largestValue = (1 << bitsUsedPerByte) - 1;

	    // Mask each byte with the largest allowed value
	    for (int i = 0; i < byteArray1.length; i++) {
	  
		// Ensure that no pixel is greater than the maximum specified
		byteArray2[i] = (byte)(byteArray1[i] & largestValue);
	    }
	}

	catch (Exception e) {
	    String msg = "Unable to mask bytes.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Reads every 2 consecutive bytes from the byte array and writes them as an
     * unsigned short in the short array.
     *
     * @param byteArray Array of bytes where each 2 consecutive bytes defines
     *                  the value of one pixel.  This array must have a size
     *                  that is divisible by 2.
     * @param shortArray Array of shorts to write each pixel value.  This array
     *                   must be equal to or more than 1/2 the size of the byte
     *                   array.
     * @param isByteSwapped True if the 2 consecutive bytes should be swapped
     *                      before combining them into an unsigned short; false
     *                      otherwise.
     *
     * @return Minimum and maximum unsigned short values.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static double[] _copyBytes(byte[] byteArray, short[] shortArray,
				       boolean isByteSwapped)
    {
	try {
	    short min = Short.MAX_VALUE;
	    short max = Short.MIN_VALUE;

	    // Group each 2 consecutive bytes into a short
	    for (int i = 0; i < byteArray.length; i+=2) {
		int combined;

		// Byte swap every two bytes into one short if required
		if (isByteSwapped) {
		    combined = ((byteArray[i+1] << 8) & 0xff00) + 
		        (byteArray[i]         & 0x00ff);
		}

		// Else combine every two bytes into one short
		else {
		    combined = ((byteArray[i] << 8) & 0xff00) + 
	                (byteArray[i+1]     & 0x00ff);
		}

		shortArray[i/2] = (short)combined;
		if (shortArray[i/2] < min) { min = shortArray[i/2]; }
		if (shortArray[i/2] > max) { max = shortArray[i/2]; }
	    }

	    // Return the minimum and maximum
	    double[] minMax = new double[2];
	    minMax[0] = min;
	    minMax[1] = max;
	    return minMax;
	}

	catch (Exception e) {
	    String msg = "Unable to convert bytes to shorts.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Reads every 4 consecutive bytes from the byte array and writes them as an
     * integer in the integer array.
     *
     * @param byteArray Array of bytes where each 4 consecutive bytes defines
     *                  the value of one pixel.  This array must have a size
     *                  that is divisible by 4.
     * @param intArray Array of integers to write each pixel value.  This array
     *                 must be equal to or more than 1/4 the size of the byte
     *                 array.
     * @param isByteSwapped True if the 4 consecutive bytes should be swapped
     *                      before combining them into an integer; false o/w.
     *
     * @return Minimum and maximum integer values.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static double[] _copyBytes(byte[] byteArray, int[] intArray,
				       boolean isByteSwapped)
    {
	try {
	    int min = Integer.MAX_VALUE;
	    int max = Integer.MIN_VALUE;

	    // Group each 4 consecutive bytes into an integer
	    for (int i = 0; i < byteArray.length; i+=4) {
		int combined;

		// Byte swap every four bytes into one integer if required
		if (isByteSwapped) {
		    combined = ((byteArray[i+3] << 24) & 0xff000000) +
			((byteArray[i+2] << 16) & 0x00ff0000) +
			((byteArray[i+1] << 8)  & 0x0000ff00) +
		        (byteArray[i]          & 0x000000ff);
		}

		// Else combine every four bytes into one integer
		else {
		    combined = ((byteArray[i]   << 24) & 0xff000000) +
			((byteArray[i+1] << 16) & 0x00ff0000) +
			((byteArray[i+2] << 8)  & 0x0000ff00) +
		        (byteArray[i+3]        & 0x000000ff);
		}

		intArray[i/4] = combined;
		if (intArray[i/4] < min) { min = intArray[i/4]; }
		if (intArray[i/4] > max) { max = intArray[i/4]; }
	    }

	    // Return the minimum and maximum
	    double[] minMax = new double[2];
	    minMax[0] = min;
	    minMax[1] = max;
	    return minMax;
	}

	catch (Exception e) {
	    String msg = "Unable to convert bytes to integers.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Reads every 4 consecutive bytes from the byte array and writes them as a
     * float in the float array.
     *
     * @param byteArray Array of bytes where each 4 consecutive bytes defines
     *                  the value of one pixel.  This array must have a size
     *                  that is divisible by 4.
     * @param floatArray Array of floats to write each pixel value.  This array
     *                   must be equal to or more than 1/4 the size of the byte
     *                   array.
     * @param isByteSwapped True if the 4 consecutive bytes should be swapped
     *                      before combining them into a float; false otherwise.
     *
     * @return Minimum and maximum integer values.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static double[] _copyBytes(byte[] byteArray, float[] floatArray,
				       boolean isByteSwapped)
    {
	try {
	    float min = Float.POSITIVE_INFINITY;
	    float max = Float.NEGATIVE_INFINITY;

	    // Group each 4 consecutive bytes into an integer
	    for (int i = 0; i < byteArray.length; i+=4) {
		int combined;

		// Byte swap every four bytes into one integer if required
		if (isByteSwapped) {
		    combined = ((byteArray[i+3] << 24) & 0xff000000) +
			((byteArray[i+2] << 16) & 0x00ff0000) +
			((byteArray[i+1] << 8)  & 0x0000ff00) +
		        (byteArray[i]          & 0x000000ff);
		}

		// Else combine every four bytes into one integer
		else {
		    combined = ((byteArray[i]   << 24) & 0xff000000) +
			((byteArray[i+1] << 16) & 0x00ff0000) +
			((byteArray[i+2] << 8)  & 0x0000ff00) +
		        (byteArray[i+3]        & 0x000000ff);
		}

		floatArray[i/4] = Float.intBitsToFloat(combined);
		if (floatArray[i/4] < min) { min = floatArray[i/4]; }
		if (floatArray[i/4] > max) { max = floatArray[i/4]; }
	    }

	    // Return the minimum and maximum
	    double[] minMax = new double[2];
	    minMax[0] = min;
	    minMax[1] = max;
	    return minMax;
	}

	catch (Exception e) {
	    String msg = "Unable to convert bytes to floats.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Reads every 8 consecutive bytes from the byte array and writes them as a
     * double in the double array.
     *
     * @param byteArray Array of bytes where each 8 consecutive bytes defines 
     *                  the value of one pixel.  This array must have a size
     *                  that is divisible by 8.
     * @param doubleArray Array of doubles to write each pixel value.  This
     *                    array must be equal to or more than 1/8 the size of
     *                    the byte array.
     * @param isByteSwapped True if the 8 consecutive bytes should be swapped
     *                      before combining them into a double; false o/w..
     *
     * @return Minimum and maximum integer values.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static double[] _copyBytes(byte[] byteArray, double[] doubleArray,
				       boolean isByteSwapped)
    {
	try {
	    double min = Double.POSITIVE_INFINITY;
	    double max = Double.NEGATIVE_INFINITY;

	    // Group each 8 consecutive bytes into a long
	    for (int i = 0; i < byteArray.length; i+=8) {
		long combined;

		// Byte swap every eight bytes into one long if required
		if (isByteSwapped) {
		    combined =
			(((long)byteArray[i+7] << 56) & 0xff00000000000000L) +
			(((long)byteArray[i+6] << 48) & 0x00ff000000000000L) +
			(((long)byteArray[i+5] << 40) & 0x0000ff0000000000L) +
			(((long)byteArray[i+4] << 32) & 0x000000ff00000000L) +
			(((long)byteArray[i+3] << 24) & 0x00000000ff000000L) +
			(((long)byteArray[i+2] << 16) & 0x0000000000ff0000L) +
			(((long)byteArray[i+1] << 8)  & 0x000000000000ff00L) +
			( (long)byteArray[i]          & 0x00000000000000ffL);
		}

		// Else combine every eight bytes into one long
		else {
		    combined =
			(((long)byteArray[i]   << 56) & 0xff00000000000000L) +
			(((long)byteArray[i+1] << 48) & 0x00ff000000000000L) +
			(((long)byteArray[i+2] << 40) & 0x0000ff0000000000L) +
			(((long)byteArray[i+3] << 32) & 0x000000ff00000000L) +
			(((long)byteArray[i+4] << 24) & 0x00000000ff000000L) +
			(((long)byteArray[i+5] << 16) & 0x0000000000ff0000L) +
			(((long)byteArray[i+6] << 8)  & 0x000000000000ff00L) +
			( (long)byteArray[i+7]        & 0x00000000000000ffL);
		}

		doubleArray[i/8] = Double.longBitsToDouble(combined);
		if (doubleArray[i/8] < min) { min = doubleArray[i/8]; }
		if (doubleArray[i/8] > max) { max = doubleArray[i/8]; }
	    }

	    // Return the minimum and maximum
	    double[] minMax = new double[2];
	    minMax[0] = min;
	    minMax[1] = max;
	    return minMax;
	}

	catch (Exception e) {
	    String msg = "Unable to convert bytes to doubles.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Reads every 3 consecutive bytes from the byte array and writes them as an
     * RGB integer in the integer array.
     *
     * @param byteArray Array of bytes where each 3 consecutive bytes is an RGB
     *                  triplet (R1, G1, B1, R2, G2, B2, ....) that defines the
     *                  value of one pixel.  This array must have a size that is
     *                  divisible by 3.
     * @param intArray Array of integers to write each pixel value.  This array
     *                 must be more than 1/3 the size of the byte array.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static void _copyRgbInterleaved(byte[] byteArray, int[] intArray)
    {
	try {
	    int numberOfInts = byteArray.length/3;

	    // Group each 3 consecutive bytes into an integer
	    for (int i = 0, j = 0; i < numberOfInts; i++, j+=3) {

		// Put 3 adjacent color components into one integer
		intArray[i] = ((0xff            << 24) & 0xff000000) +
		    ((byteArray[j]    << 16) & 0x00ff0000) +
		    ((byteArray[j+1]  <<  8) & 0x0000ff00) +
		    ((byteArray[j+2]       ) & 0x000000ff);
	    }
	}

	catch (Exception e) {
	    String msg = "Unable to convert RGB interleaved bytes to ints.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Reads 3 bytes in each of the 3 corresponding bands from the byte array
     * and writes them as an RGB integer in the integer array.
     *
     * @param byteArray Array of bytes grouped into 3 bands where each band
     *                  groups together (R1, R2, ... , G1, G2, ... , B1, B2, ..)
     *                  one of the color components that define the pixel
     *                  values.  This array must have a size that is divisible
     *                  by 3.
     * @param intArray Array of integers to write each pixel value.  This array
     *                 must be more than 1/3 the size of the byte array.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static void _copyRgbBanded(byte[] byteArray, int[] intArray)
    {
	try {
	    int numberOfInts = byteArray.length/3;

	    // Group each 3 corresponding bytes into an integer
	    int redStart = 0;
	    int greenStart = numberOfInts;
	    int blueStart = 2*numberOfInts;
	    for (int i = 0; i < numberOfInts; i++) {
	      
		// Put 3 color components from the same pixel into one integer
		intArray[i] = ((0xff                     << 24) & 0xff000000) +
		    ((byteArray[i+redStart]    << 16) & 0x00ff0000) +
		    ((byteArray[i+greenStart]  <<  8) & 0x0000ff00) +
		    ((byteArray[i+blueStart]        ) & 0x000000ff);
	    }
	}

	catch (Exception e) {
	    String msg = "Unable to convert RGB banded bytes to ints.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Computes the minimum and maximum values of the given array.
     *
     * @param array Byte, short, or integer array.
     * @param bitsUsedPerValue Number of lower bits used to represent values in
     *                         the array.
     *
     * @return Minimum and maximum pixel values of the array.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static Range _computeMinAndMax(Object array, int bitsUsedPerValue)
    {
	try {
	    int maxAllowedValue = (1 << bitsUsedPerValue) - 1;
	    int length = Array.getLength(array);

	    // Initialize the array range
	    Range arrayRange = new Range();

	    // Byte array
	    if (array instanceof byte[]) {
		for (int i = 0; i < length; i++) {

		    // Convert to an unsigned value
		    int arrayValue = Array.getByte(array, i) & maxAllowedValue;
		    arrayRange.adjust(arrayValue);
		}
	    }

	    // Short array
	    else if (array instanceof short[]) {
		for (int i = 0; i < length; i++) {

		    // Convert to an unsigned value
		    int arrayValue = Array.getShort(array, i) & maxAllowedValue;
		    arrayRange.adjust(arrayValue);
		}
	    }

	    // Integer array
	    else {
		for (int i = 0; i < length; i++) {
		    int arrayValue = Array.getInt(array, i);
		    arrayRange.adjust(arrayValue);
		}
	    }

	    // Return the array range
	    return arrayRange;
	}

	catch (Exception e) {
	    String msg = "Unable to determine the minimum and maximum.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Creates a Buffered Image for grayscale data; a linear lookup table is
     * created that maps the image pixel values to the full range of byte
     * values.  All values that lie outside this range are mapped according to
     * the closest extremum.
     *
     * @param raster Writable Raster containing the image pixel data.
     * @param arrayRange Range of image pixel data values.
     * @param bitsUsedPerPixel Number of lower bits actually used to represent
     *                         each image pixel value.
     * @param isInverted True if the mapping is onto the byte range (0, 255);
     *                   false if the mapping is onto the byte range (255, 0).
     *
     * @return Buffered Image for grayscale data.
     *
     * @throws IllegalArgumentException If the Buffered Image cannot be created.
     */
    private static BufferedImage _createGrayImage(WritableRaster raster,
						  Range arrayRange,
						  int bitsUsedPerPixel,
						  boolean isInverted)
    {
	try {

	    // Buffered image requires color model and raster to be of the
	    // same tranfer type
	    if (raster.getTransferType() == DataBuffer.TYPE_USHORT &&
		bitsUsedPerPixel <= 8)
		{
		    // Make sure color model is ushort type
		    bitsUsedPerPixel = 10;
		}

	    // Allocate room for the look up table
	    int size = 1 << bitsUsedPerPixel;
	    byte[] lookUpTable = new byte[size];

	    // Map integers before the minimum value to 0
	    int minValue = arrayRange.getMinimum();
	    for (int i = 0; i < minValue; i++) { lookUpTable[i] = 0; }

	    // Map integers between the minimum and maximum values linearly
	    int maxValue = arrayRange.getMaximum();
	    if (minValue < maxValue) {
		double slope = 255.0/(double)(maxValue-minValue);
		for (int i = minValue; i < maxValue; i++) {
		    lookUpTable[i] = (byte)(slope*(i-minValue));
		}
	    }

	    // Map integers above the maximum value to 255
	    for (int i = maxValue; i < size; i++) {
		lookUpTable[i] = (byte)255;
	    }

	    // Invert if required
	    if (isInverted) {
		for (int i = 0; i < size; i++) {
		    lookUpTable[i] = (byte)( ~lookUpTable[i] );
		}
	    }

	    // Create a Color Model using the look-up table
	    ColorModel colorModel 
		= new IndexColorModel(bitsUsedPerPixel, lookUpTable.length,
				      lookUpTable, lookUpTable, lookUpTable);

	    // ******
	    // Add jViewBox hints
	    // ******
	    Hashtable properties = new Hashtable();

	    // Add the minimum and maximum pixel value
	    String minimum = Integer.toString(minValue);
	    String maximum = Integer.toString(maxValue);
	    properties.put(JVIEWBOX_MIN_VALUE, minimum);
	    properties.put(JVIEWBOX_MAX_VALUE, maximum);

	    // Add the lookup table properties
	    String window = Integer.toString(maxValue - minValue);
	    String level = Integer.toString( (maxValue + minValue)/2 );
	    properties.put(JVIEWBOX_BASE_NAME+"->Lookup_table->Type", "LINEAR");
	    properties.put(JVIEWBOX_BASE_NAME+"->Lookup_table->Window", window);
	    properties.put(JVIEWBOX_BASE_NAME+"->Lookup_table->Level", level);

	    // Return the Buffered Image
	    return new BufferedImage(colorModel, raster, false, properties);
	}

	catch (Exception e) {
	    String msg = "Unable to create the grayscale Buffered Image.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /**
     * Creates a Buffered Image for grayscale data.  The minimum and maximum
     * data values determine the linear mapping used when viewing.  This
     * Buffered Image is not optimal for JViewBox.
     *
     * @param raster Writable Raster containing the image pixel data.
     * @param minimum Minimum data value in the Raster.
     * @param maximum Maximum data value in the Raster.
     *
     * @return Buffered Image for grayscale data.
     */
    private static BufferedImage _createGrayImage(WritableRaster raster,
						  double minimum,
						  double maximum)
    {
	// Determine the data type
	DataBuffer dataBuffer = raster.getDataBuffer();
	int dataType = dataBuffer.getDataType();

	// Create a grayscale Color Model
	LinearScaleGrayColorModel cm = new LinearScaleGrayColorModel(dataType);
	cm.setLowerBound(minimum);
	cm.setUpperBound(maximum);

	// Return the Buffered Image
	return new BufferedImage(cm, raster, false, new Hashtable());
    }

    /**
     * Changes the value of each short in the array using a linear map.
     *
     * @param shortArray Array of shorts to apply the map to.
     * @param slope Factor multiplied to each short.
     * @param offset Amount added to each short after slope multiplication.
     * @param bitsUsedPerShort Number of lower bits used (<=16) to represent
     *                         each short value.
     *
     * @return Minimum and maximum pixel values of the remapped array.
     *
     * @throws IllegalArgumentException If the operation cannot be accomplished.
     */
    private static Range _remap(short[] shortArray, double slope, int offset,
				int bitsUsedPerShort)
    {
	try {
	    int largestValue = (1 << bitsUsedPerShort) - 1;

	    // Data is 16-bit unsigned and slope and offset do not change values
	    if (bitsUsedPerShort == 16 && slope == 1.0 && offset == 0) {
		Range pixelRange = new Range();
		for (int i = 0; i < shortArray.length; i++) {

		    // Convert to an unsigned integer and record
		    int intValue = shortArray[i] & 0xffff;
		    pixelRange.adjust(intValue);
		}

		return pixelRange;
	    }
	
	    // If the slope and offset do not change values, quickly bit mask
	    if (slope == 1.0 && offset == 0) {
		Range pixelRange = new Range();
		for (int i = 0; i < shortArray.length; i++) {

		    // Convert to an unsigned integer
		    int intValue = shortArray[i] & largestValue;

		    // Convert to an unsigned short
		    short shortValue = (short)intValue;
		    if (shortValue < 0) { shortValue = 0; }

		    pixelRange.adjust(shortValue);
		    shortArray[i] = shortValue;
		}

		return pixelRange;
	    }
	
	    // Otherwise create a table that maps all the possible short values
	    short[] table = new short[65536];
	    for (int i = 0; i < 65536; i++) {

		// Calculate the linear mapping (casting can make it negative)
		int mapped = (short)(slope*i + offset);

		// Ensure that the short is not negative
		if (mapped < 0) { mapped = 0; }

		// Ensure that the short is not greater than the maximum
		if (mapped > largestValue) { mapped = largestValue; }

		// Set the linear mapping
		table[i] = (short)mapped;
	    }

	    // Create a WritableRaster of the shorts
	    int size = shortArray.length;
	    DataBufferUShort dataBuffer = new DataBufferUShort(shortArray,size);
	    WritableRaster writableRaster =
		Raster.createInterleavedRaster(dataBuffer, size, 1, size, 1,
					       new int[1], null);

	    // Apply the table values to each short
	    ShortLookupTable lut = new ShortLookupTable(0, table);
	    LookupOp op = new LookupOp(lut, null);
	    op.filter(writableRaster, writableRaster);

	    // Return the new minimum and maximum
	    return _computeMinAndMax(shortArray, bitsUsedPerShort);
	}

	catch (Exception e) {
	    String msg = "Unable to map shorts.";
	    IllegalArgumentException exception =
		new IllegalArgumentException(msg);
	    exception.initCause(e);
	    throw exception;
	}
    }

    /** Range (minimum and maximum) of pixel values. */
    private static class Range
    {
	/** Minimum pixel value. */
	private int _minimum;

	/** Maximum pixel value. */
	private int _maximum;

	/**
	 * Constructs a Range.
	 *
	 * @param minimum Minimum pixel value.
	 * @param maximum Maximum pixel value.
	 */
	public Range(int minimum, int maximum)
	{
	    _minimum = minimum;
	    _maximum = maximum;
	}

	/** Constructs a Range. */
	public Range()
	{
	    this(Integer.MAX_VALUE, Integer.MIN_VALUE);
	}

	/**
	 * Gets the minimum pixel value of the range.
	 *
	 * @return Minimum pixel value.
	 */
	public int getMinimum()
	{
	    return _minimum;
	}

	/**
	 * Gets the maximum pixel value of the range.
	 *
	 * @return Maximum pixel value.
	 */
	public int getMaximum()
	{
	    return _maximum;
	}

	/**
	 * Readjusts the minimum and maximum pixel values.
	 *
	 * @param value Value to use to reset the minimum and maximum pixel
	 *              values.
	 */
	public void adjust(int value)
	{
	    if (value < _minimum) { _minimum = value; }
	    if (value > _maximum) { _maximum = value; }
	}
    }
}
