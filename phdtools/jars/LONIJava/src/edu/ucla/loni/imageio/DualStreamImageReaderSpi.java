/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import javax.imageio.spi.ImageReaderSpi;

/**
 * Image Reader SPI for sources whose metadata and image pixel data are divided
 * into two Image Input Streams.
 *
 * @version 15 July 2005
 */
public abstract class DualStreamImageReaderSpi extends ImageReaderSpi
{
    /**
     * Constructs a Dual Stream Image Reader SPI.
     *
     * @param vendorName Vendor name.
     * @param version Version identifier.
     * @param names Format names.
     * @param suffixes Common file suffixes.
     * @param mimeTypes MIME types for the format.
     * @param readerClassName Fully-qualified name of the associated Image
     *                        Reader class.
     * @param inputTypes Legal input types.
     * @param writerSpiNames All associated Image Writers.
     * @param supportsStandardStreamMetadataFormat True if a stream metadata
     *                                             object can use trees
     *                                             described by the standard
     *                                             metadata format.
     * @param nativeStreamMetadataFormatName Native stream metadata format name.
     * @param nativeStreamMetadataFormatClassName Used to instantiate a metadata
     *                                            format object.
     * @param extraStreamMetadataFormatNames Extra stream metadata format names.
     * @param extraStreamMetadataFormatClassNames Used to instantiate a metadata
     *                                            format object.
     * @param supportsStandardImageMetadataFormat True if an image metadata
     *                                            object can use trees described
     *                                            by the standard metadata
     *                                            format.
     * @param nativeImageMetadataFormatName Native image metadata format name.
     * @param nativeImageMetadataFormatClassName Used to instantiate a metadata
     *                                           format object.
     * @param extraImageMetadataFormatNames Extra image metadata format names.
     * @param extraImageMetadataFormatClassNames Used to instantiate a metadata
     *                                           format object.
     *
     * @throws IllegalArgumentException If the vendor name, version, names,
     *                                  reader class name, or input types are
     *                                  null.
     * @throws IllegalArgumentException If the reader class name or input types
     *                                  has zero length.
     */
    protected DualStreamImageReaderSpi(String vendorName, String version,
				   String[] names, String[] suffixes,
				   String[] mimeTypes, String readerClassName,
				   Class[] inputTypes,
				   String[] writerSpiNames,
				   boolean supportsStandardStreamMetadataFormat,
				   String nativeStreamMetadataFormatName,
				   String nativeStreamMetadataFormatClassName,
				   String[] extraStreamMetadataFormatNames,
				   String[] extraStreamMetadataFormatClassNames,
				   boolean supportsStandardImageMetadataFormat,
				   String nativeImageMetadataFormatName,
				   String nativeImageMetadataFormatClassName,
				   String[] extraImageMetadataFormatNames,
				   String[] extraImageMetadataFormatClassNames)
    {
	super(vendorName, version, names, suffixes, mimeTypes, readerClassName,
	      inputTypes, writerSpiNames, supportsStandardStreamMetadataFormat,
	      nativeStreamMetadataFormatName,
	      nativeStreamMetadataFormatClassName,
	      extraStreamMetadataFormatNames,
	      extraStreamMetadataFormatClassNames,
	      supportsStandardImageMetadataFormat,
	      nativeImageMetadataFormatName,
	      nativeImageMetadataFormatClassName, extraImageMetadataFormatNames,
	      extraImageMetadataFormatClassNames);
    }

    /**
     * Gets the File Name Associator that generates file name associations
     * between metadata files and image pixel files.
     *
     * @return File Name Associator for this format.
     */
    public abstract FileNameAssociator getFileNameAssociator();
}
