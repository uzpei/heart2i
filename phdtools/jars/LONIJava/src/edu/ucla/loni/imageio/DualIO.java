/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import java.util.Iterator;
import java.util.Vector;
import javax.imageio.spi.IIORegistry;
import javax.imageio.spi.ImageReaderSpi;

/**
 * Class that contains convenience methods for managing sources whose metadata
 * and image pixel data are divided into two Image Input Streams.
 *
 * @version 15 July 2005
 */
public class DualIO
{
    /** Constructs a Dual I/O. */
    private DualIO()
    {
    }

    /**
     * Gets all the File Name Associators that make associations between
     * metadata files and image pixel files.
     *
     * @return All existing File Name Associators.
     */
    public static FileNameAssociator[] getFileNameAssociators()
    {
	try {

	    // Get the all the Image Reader SPI's
	    IIORegistry registry = IIORegistry.getDefaultInstance();
	    Iterator iter = registry.getServiceProviders(ImageReaderSpi.class,
							 true);

	    // Accumulate all the Dual Stream Image Reader SPI's
	    Vector dualRdrs = new Vector();
	    while ( iter.hasNext() ) {
		ImageReaderSpi spi = (ImageReaderSpi)iter.next();
		if (spi instanceof DualStreamImageReaderSpi) {
		    dualRdrs.add(spi);
		}
	    }

	    // Return all the File Name Associators
	    FileNameAssociator[] atrs = new FileNameAssociator[dualRdrs.size()];
	    for (int i = 0; i < atrs.length; i++) {
		DualStreamImageReaderSpi dualSpi =
		    (DualStreamImageReaderSpi)dualRdrs.get(i);

		atrs[i] = dualSpi.getFileNameAssociator();
	    }
	    return atrs;
	}

	catch (Exception e) {
	    throw new Error("Failed to get File Name Associators", e);
	}
    }
}
