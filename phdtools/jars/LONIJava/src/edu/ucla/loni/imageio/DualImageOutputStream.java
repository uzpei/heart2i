/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import java.io.IOException;
import java.util.Vector;
import javax.imageio.stream.ImageOutputStream;

/**
 * Class that manages two Image Output Streams; one for the metadata and one for
 * the image pixel data.
 *
 * @version 14 July 2005
 */
public class DualImageOutputStream
{
    /** Image Output Stream for the metadata. */
    private ImageOutputStream _metadataOutputStream;

    /** Image Output Stream for the image pixel data. */
    private ImageOutputStream _pixelDataOutputStream;

    /**
     * Constructs a Dual Image Output Stream for the two Image Output Streams.
     *
     * @param metadataOutputStream Image Output Stream for the metadata.
     * @param pixelDataOutputStream Image Output Stream for the image pixel data.
     *
     * @throws NullPointerException If either Image Output Stream is null.
     */
    public DualImageOutputStream(ImageOutputStream metadataOutputStream,
				 ImageOutputStream pixelDataOutputStream)
    {
	// Check for null Image Output Streams
	if (metadataOutputStream == null || pixelDataOutputStream == null) {
	    String msg = "Null Image Output Stream encountered.";
	    throw new NullPointerException(msg);
	}

	_metadataOutputStream = metadataOutputStream;
	_pixelDataOutputStream = pixelDataOutputStream;
    }

    /**
     * Constructs a Dual Image Output Stream from the Vector of two Image Output
     * Streams.
     *
     * @param streams Vector of two Image Output Streams; the first is for the
     *                metadata and the second is for the image pixel data.
     *
     * @throws IllegalArgumentException If the Vector does not contain two Image
     *                                  Output Streams.
     */
    public DualImageOutputStream(Vector streams)
    {
	// Check for two Objects in the Vector
	if ( streams.size() < 2 ) {
	    String msg = "Must have two Image Output Streams.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for two Image Output Streams
	if ( !(streams.get(0) instanceof ImageOutputStream) ||
	     !(streams.get(1) instanceof ImageOutputStream) )
	    {
		String msg = "Must have two Image Output Streams.";
		throw new IllegalArgumentException(msg);
	    }

	_metadataOutputStream = (ImageOutputStream)streams.get(0);
	_pixelDataOutputStream = (ImageOutputStream)streams.get(1);
    }

    /**
     * Gets the metadata output stream.
     *
     * @return Image Output Stream for the metadata.
     */
    public ImageOutputStream getMetadataOutputStream()
    {
	return _metadataOutputStream;
    }

    /**
     * Gets the pixel data output stream.
     *
     * @return Image Output Stream for the image pixel data.
     */
    public ImageOutputStream getPixelDataOutputStream()
    {
	return _pixelDataOutputStream;
    }

    /**
     * Closes this output stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void close() throws IOException
    {
	_metadataOutputStream.close();
	_pixelDataOutputStream.close();
    }
}
