/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import javax.imageio.ImageReadParam;

/**
 * Image Read Param used to specify LONI-specific image decoding.
 *
 * @version 14 April 2005
 */
public class LoniImageReadParam extends ImageReadParam
{
  /** True if the image is to be read as an undecoded byte array; false o/w. */
  private boolean _returnRawBytes;

  /** Constructs a LONI Image Read Param. */
  public LoniImageReadParam()
    {
      super();

      _returnRawBytes = false;
    }

  /**
   * Sets whether or not the image is to be read as an undecoded array of
   * bytes.  The image bytes are read unaltered from the input stream and
   * wrapped up in a "fake" Buffered Image.  The properties of the Buffered
   * Image should be ignored.
   *
   * @param returnRawBytes True if the image is to be read as an undecoded
   *                       byte array; false o/w.
   */
  public void setReturnRawBytes(boolean returnRawBytes)
    {
      _returnRawBytes = true;
    }

  /**
   * Determines whether or not the image is to be read as an undecoded array of
   * bytes.
   *
   * @return True if the image is to be read as an undecoded byte array;
   *         false o/w.
   */
  public boolean returnRawBytes()
    {
      return _returnRawBytes;
    }
}
