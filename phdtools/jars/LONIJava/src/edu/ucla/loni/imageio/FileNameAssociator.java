/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

/**
 * Class that generates file name associations for image files whose metadata
 * and image pixel data are separated into two files.
 *
 * @version 15 July 2005
 */
public abstract class FileNameAssociator
{
    /**
     * Determines whether or not this file contains metadata or image pixel
     * data.
     *
     * @return True if this file contains metadata, false otherwise.
     */
    public abstract boolean isMetadataFile(String fileName);

    /**
     * Gets all the allowed file names that can be associated with the specified
     * file name.
     *
     * @param fileName Name of the file to get associated file names for.
     *
     * @return All the allowed file names that can be associated with the
     *         specified file name.  An empty array is returned if there are no
     *         associated file names.
     */
    public abstract String[] getAssociatedFileNames(String fileName);

    /**
     * Gets upper and lower case suffix replacements of the specified file name
     * using the given source and target suffixes.
     *
     * @param fileName File name get replacements for.
     * @param srcSuffix Suffix to replace.
     * @param tgtSuffix Suffix to replace with.
     *
     * @return Upper and lower case suffix replacements of the specified file
     *         name.
     */
    protected String[] _getSuffixReplacements(String fileName, String srcSuffix,
					      String tgtSuffix)
    {
	String[] newFileNames = new String[2];

	// No file name
	if (fileName == null) { return new String[0]; }

	// No source suffix
	if ( !fileName.toLowerCase().endsWith(srcSuffix.toLowerCase()) ) {
	    return new String[0];
	}

	// Determine lengths
	int length = fileName.length();
	int sl = srcSuffix.length();

	// Create the upper and lower case file names
	String lowerName = fileName.substring(0, length-sl) +
	    tgtSuffix.toLowerCase();
	String upperName = fileName.substring(0, length-sl) +
	    tgtSuffix.toUpperCase();

	// Order the new file names according to case
	String upperSrcSuffix = srcSuffix.toUpperCase();
	if ( fileName.substring(length-sl, length).equals(upperSrcSuffix) ) {
	    newFileNames[0] = upperName;
	    newFileNames[1] = lowerName;
	}
	else {
	    newFileNames[0] = lowerName;
	    newFileNames[1] = upperName;
	}

	return newFileNames;
    }
}
