/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.imageio;

import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.Node;

/**
 * Factory that creates DOM Node representations of LONI Basic metadata.
 *
 * @version 14 January 2009
 */
public class BasicMetadataTreeMaker
{
    /** Constructs a Basic Metadata Tree Maker. */
    private BasicMetadataTreeMaker()
    {
    }

    /**
     * Creates a Tree from the LONI Basic metadata.
     *
     * @param imageWidth Width of an image in pixels.
     * @param imageHeight Height of an image in pixels.
     * @param numberOfImages Number of images.
     * @param dataType Data type.
     * @param bitsPerPixel Number of bits in an image pixel.
     * @param colorType Color type.
     *
     * @return DOM Node representing the metadata.
     */
    public static Node getTree(int imageWidth, int imageHeight,
			       int numberOfImages, String dataType,
			       int bitsPerPixel, String colorType)
    {
	// Create the root Node
	String rootName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

	// Add the attributes
	rootNode.setAttribute(BasicMetadataFormat.IMAGE_WIDTH, imageWidth+"");
	rootNode.setAttribute(BasicMetadataFormat.IMAGE_HEIGHT, imageHeight+"");
	rootNode.setAttribute(BasicMetadataFormat.NUMBER_OF_IMAGES,
			      numberOfImages+"");
	rootNode.setAttribute(BasicMetadataFormat.DATA_TYPE, dataType);
	rootNode.setAttribute(BasicMetadataFormat.BITS_PER_PIXEL,
			      bitsPerPixel+"");
	rootNode.setAttribute(BasicMetadataFormat.COLOR_TYPE, colorType);

	// Return the root Node
	return rootNode;
    }

    /**
     * Creates a Tree from the LONI Basic metadata.
     *
     * @param imageWidth Width of an image in pixels.
     * @param imageHeight Height of an image in pixels.
     * @param numberOfImages Number of images.
     * @param pixelWidth Width of an image pixel in millimeters.
     * @param pixelHeight Height of an image pixel in millimeters.
     * @param dataType Data type.
     * @param bitsPerPixel Number of bits in an image pixel.
     * @param colorType Color type.
     *
     * @return DOM Node representing the metadata.
     */
    public static Node getTree(int imageWidth, int imageHeight,
			       int numberOfImages, double pixelWidth,
			       double pixelHeight, String dataType,
			       int bitsPerPixel, String colorType)
    {
	Node node = getTree(imageWidth, imageHeight, numberOfImages, dataType,
			    bitsPerPixel, colorType);

	// Add pixel spacing
	IIOMetadataNode rootNode = (IIOMetadataNode)node;
	rootNode.setAttribute(BasicMetadataFormat.PIXEL_WIDTH, pixelWidth+"");
	rootNode.setAttribute(BasicMetadataFormat.PIXEL_HEIGHT, pixelHeight+"");

	// Return the root Node
	return rootNode;
    }

    /**
     * Creates a Tree from the LONI Basic metadata.
     *
     * @param imageWidth Width of an image in pixels.
     * @param imageHeight Height of an image in pixels.
     * @param numberOfImages Number of images.
     * @param pixelWidth Width of an image pixel in millimeters.
     * @param pixelHeight Height of an image pixel in millimeters.
     * @param sliceThickness Distance between consecutive image slices in
     *                       millimeters.
     * @param dataType Data type.
     * @param bitsPerPixel Number of bits in an image pixel.
     * @param colorType Color type.
     *
     * @return DOM Node representing the metadata.
     */
    public static Node getTree(int imageWidth, int imageHeight,
			       int numberOfImages, double pixelWidth,
			       double pixelHeight, double sliceThickness,
			       String dataType, int bitsPerPixel,
			       String colorType)
    {
	Node node = getTree(imageWidth, imageHeight, numberOfImages, pixelWidth,
			    pixelHeight, dataType, bitsPerPixel, colorType);

	// Add slice thickness
	IIOMetadataNode rootNode = (IIOMetadataNode)node;
	rootNode.setAttribute(BasicMetadataFormat.SLICE_THICKNESS,
			      sliceThickness+"");

	// Return the root Node
	return rootNode;
    }
}
