/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.imageio;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

/**
 * Class that manages two Image Input Streams; one for the metadata and one for
 * the image pixel data.
 *
 * @version 14 July 2005
 */
public abstract class DualImageInputStream
{
    /** Image Input Stream for the metadata. */
    private ImageInputStream _metadataInputStream;

    /** Image Input Stream for the image pixel data. */
    private ImageInputStream _pixelDataInputStream;

    /**
     * Constructs a Dual Image Input Stream for the two Image Input Streams.
     *
     * @param metadataInputStream Image Input Stream for the metadata.
     * @param pixelDataInputStream Image Input Stream for the image pixel data.
     *
     * @throws NullPointerException If either Image Input Stream is null.
     */
    protected DualImageInputStream(ImageInputStream metadataInputStream,
				   ImageInputStream pixelDataInputStream)
    {
	// Check for null Image Input Streams
	if (metadataInputStream == null || pixelDataInputStream == null) {
	    String msg = "Null Image Input Stream encountered.";
	    throw new NullPointerException(msg);
	}

	_metadataInputStream = metadataInputStream;
	_pixelDataInputStream = pixelDataInputStream;
    }

    /**
     * Constructs a Dual Image Input Stream from the Vector of two Image Input
     * Streams.
     *
     * @param streams Vector of two Image Input Streams; the first is for the
     *                metadata and the second is for the image pixel data.
     *
     * @throws IllegalArgumentException If the Vector does not contain two Image
     *                                  Input Streams.
     */
    protected DualImageInputStream(Vector streams)
    {
	// Check for two Objects in the Vector
	if ( streams.size() < 2 ) {
	    String msg = "Must have two Image Input Streams.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for two Image Input Streams
	if ( !(streams.get(0) instanceof ImageInputStream) ||
	     !(streams.get(1) instanceof ImageInputStream) )
	    {
		String msg = "Must have two Image Input Streams.";
		throw new IllegalArgumentException(msg);
	    }

	_metadataInputStream = (ImageInputStream)streams.get(0);
	_pixelDataInputStream = (ImageInputStream)streams.get(1);
    }

    /**
     * Constructs a Dual Image Input Stream from the File.  The Associator is
     * used to search for the file's associated file, with the associated file
     * names tried in the order they are received from the Associator.
     *
     * @param file Metadata or image pixel data File.
     * @param associator File Name Associator used to find the associated file.
     *
     * @throws IllegalArgumentException If the file associated with the given
     *                                  file cannot be determined, or if the
     *                                  files cannot be accessed.
     */
    protected DualImageInputStream(File file, FileNameAssociator associator)
    {
	// Get the file names associated with this file
	String fileName = file.getAbsolutePath();
	String[] assocFileNames = associator.getAssociatedFileNames(fileName);

	// Find an associated file
	File assocFile = null;
	for (int i = 0; i < assocFileNames.length && assocFile == null; i++) {
	    File f = new File(assocFileNames[i]);
	    if ( f.exists() ) { assocFile = f; }
	}

	// No associated file
	if (assocFile == null) {
	    String msg = "Unable to find a companion file to \"" + fileName +
		         "\".";
	    throw new IllegalArgumentException(msg);
	}

	// Determine the metadata and image files
	File metadataFile = file;
	File imagefile = assocFile;
	if ( !associator.isMetadataFile(fileName) ) {
	    metadataFile = assocFile;
	    imagefile = file;
	}

	// Construct input streams from the files
	try {
	    _metadataInputStream = new FileImageInputStream(metadataFile);
	    _pixelDataInputStream = new FileImageInputStream(imagefile);
	}
	catch (Exception e) {
	    String msg = "Unable to read from \"" +
		metadataFile.getAbsolutePath() + "\" and \"" +
		imagefile.getAbsolutePath() + "\".";

	    IllegalArgumentException iae = new IllegalArgumentException(msg);
	    iae.initCause(e);
	    throw iae;
	}
    }

    /**
     * Gets the metadata input stream.
     *
     * @return Image Input Stream for the metadata.
     */
    public ImageInputStream getMetadataInputStream()
    {
	return _metadataInputStream;
    }

    /**
     * Gets the pixel data input stream.
     *
     * @return Image Input Stream for the image pixel data.
     */
    public ImageInputStream getPixelDataInputStream()
    {
	return _pixelDataInputStream;
    }

    /**
     * Closes this input stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void close() throws IOException
    {
	_metadataInputStream.close();
	_pixelDataInputStream.close();
    }
}
