/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.imageio;

import java.lang.reflect.Method;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataFormat;
import javax.imageio.metadata.IIOMetadataFormatImpl;

/**
 * II/O Metadata class designed to work inside of applets.
 *
 * @version 10 May 2007
 */
public abstract class AppletFriendlyIIOMetadata extends IIOMetadata
{
  /**
   * Constructs an Applet Friendly IIO Metadata.
   * 
   * The subclass is responsible for suppying values for all protected
   * instance variables that will allow any non-overridden default
   * implemtations of methods to satisfy their contracts.  For example,
   * extraMetadataFormatNames should not have length 0.
   */
  protected AppletFriendlyIIOMetadata()
    {
      super();
    }

  /**
   * Constructs an Applet Friendly IIO Metadata with the given format names
   * and format class names, as well as a boolean indicating whether the
   * standard format is supported.
   *
   * <p> This constructor does not attempt to check the class names
   * for validity.  Invalid class names may cause exceptions in
   * subsequent calls to getMetadataFormat.
   *
   * @param standardMetadataFormatSupported True if this object can return or
   *                                        accept a DOM tree using the
   *                                        standard metadata format.
   * @param nativeMetadataFormatName The name of the native metadata format,
   *                                 as a String, or null if there is no native
   *                                 format.
   * @param nativeMetadataFormatClassName The name of the class of the native
   *                                      metadata format, or null if there is
   *                                      no native format.
   * @param extraMetadataFormatNames An array of Strings indicating additional
   *                                 formats supported by this object, or null
   *                                 if there are none.
   * @param extraMetadataFormatClassNames An array of Strings indicating the
   *                                      class names of any additional formats
   *                                      supported by this object, or null if
   *                                      there are none.
   *
   * @throws IllegalArgumentException If extraMetadataFormatNames has length 0.
   * @throws IllegalArgumentException If extraMetadataFormatNames and
   *                                  extraMetadataFormatClassNames are neither
   *                                  both null, nor of the same length.
   */
  protected AppletFriendlyIIOMetadata(boolean standardMetadataFormatSupported,
				      String nativeMetadataFormatName,
				      String nativeMetadataFormatClassName,
				      String[] extraMetadataFormatNames,
				      String[] extraMetadataFormatClassNames)
    {
      super(standardMetadataFormatSupported, nativeMetadataFormatName,
	    nativeMetadataFormatClassName, extraMetadataFormatNames,
	    extraMetadataFormatClassNames);
    }

  /**
   * Returns an IIOMetadataFormat object describing the given metadata format,
   * or null if no description is available.  The supplied name must be one of
   * those returned by getMetadataFormatNames (i.e., either the  native format
   * name, the standard format name, or one of those returned by
   * getExtraMetadataFormatNames).
   *
   * <p> The default implementation checks the name against the global
   * standard metadata format name, and returns that format if it is supported.
   * Otherwise, it checks against the native format names followed by any
   * additional format names.  If a match is found, it retrieves the name of
   * the IIOMetadataFormat class from nativeMetadataFormatClassName or
   * extraMetadataFormatClassNames as appropriate, and constructs an instance
   * of that class using its getInstance method.
   *
   * @param formatName The desired metadata format.
   *
   * @return An IIOMetadataFormat object.
   *
   * @throws IllegalArgumentException If formatName is null or is not one of
   *                                  the names recognized by the plug-in.
   * @throws IllegalStateException If the class corresponding to the format
   *                               name cannot be loaded.
   */
  public IIOMetadataFormat getMetadataFormat(String formatName)
    {
      // Code is directly copied from super class
      if (formatName == null) {
	throw new IllegalArgumentException("formatName == null!");
      }

      if (standardFormatSupported && formatName.equals
	  (IIOMetadataFormatImpl.standardMetadataFormatName)) {
	return IIOMetadataFormatImpl.getStandardFormatInstance();
      }

      String formatClassName = null;
      if (formatName.equals(nativeMetadataFormatName)) {
	formatClassName = nativeMetadataFormatClassName; 
      }
      else if (extraMetadataFormatNames != null) {
	for (int i = 0; i < extraMetadataFormatNames.length; i++) {
	  if (formatName.equals(extraMetadataFormatNames[i])) {
	    formatClassName = extraMetadataFormatClassNames[i];
	    break;  // out of for
	  }
	}
      }
      if (formatClassName == null) {
	throw new IllegalArgumentException("Unsupported format name");
      }
      try {

	// Find the Metadata Format class using the System Class Loader
	Class c = null;
	try {
	  c = Class.forName(formatClassName, true,
			    ClassLoader.getSystemClassLoader());
	}

	// Metadata Format class not found; try the Class Loader for this
	// class (different from the System Class Loader for applets)
	catch (ClassNotFoundException cnfe) {
	  c = Class.forName(formatClassName, true,
			    getClass().getClassLoader());
	}

	// Use reflection to create the instance of the Metadata Format class
	Method meth = c.getMethod("getInstance", (Class[])null);
	return (IIOMetadataFormat) meth.invoke(null, (Object[])null);
      }

      catch (Exception e) {
	RuntimeException ex = new IllegalStateException("Can't obtain format");
	ex.initCause(e);
	throw ex;
      }   
    }
}
