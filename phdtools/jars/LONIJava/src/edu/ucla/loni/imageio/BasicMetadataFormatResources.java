/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.imageio;

import java.util.ListResourceBundle;

/**
 * List Resource Bundle that provides descriptions for NIFTI 1.0 fields.
 *
 * @version 12 January 2009
 */
public class BasicMetadataFormatResources extends ListResourceBundle
{
    /** Constructs a Basic Metadata Format Resources. */
    public BasicMetadataFormatResources()
    {
    }

    /**
     * Gets the contents of the Resource Bundle.
     *
     * @return Object array of the contents, where each item of the array is a
     *         pair of Objects.  The first element of each pair is the key,
     *         which must be a String, and the second element is the value
     *         associated with that key.
     */
    public Object[][] getContents()
    {
	String rootName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;

	Object[][] contents = {

	    // Node name + "/" + attribute name, followed by description
	    {rootName + "/" + BasicMetadataFormat.IMAGE_WIDTH,
	     "Width of an image in pixels."},
	    {rootName + "/" + BasicMetadataFormat.IMAGE_HEIGHT,
	     "Height of an image in pixels."},
	    {rootName + "/" + BasicMetadataFormat.NUMBER_OF_IMAGES,
	     "Number of images."},
	    {rootName + "/" + BasicMetadataFormat.PIXEL_WIDTH,
	     "Width of an image pixel in millimeters."},
	    {rootName + "/" + BasicMetadataFormat.PIXEL_HEIGHT,
	     "Height of an image pixel in millimeters."},
	    {rootName + "/" + BasicMetadataFormat.SLICE_THICKNESS,
	     "Distance between consecutive image slices in millimeters."},
	    {rootName + "/" + BasicMetadataFormat.DATA_TYPE,
	     "Data type."},
	    {rootName + "/" + BasicMetadataFormat.BITS_PER_PIXEL,
	     "Number of bits in an image pixel."},
	    {rootName + "/" + BasicMetadataFormat.COLOR_TYPE,
	     "Color type."}
	};

	return contents;
    }
}
