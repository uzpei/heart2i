/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.nifti.plugin;

import edu.ucla.loni.analyze.plugin.AnalyzeInputStream;
import edu.ucla.loni.analyze.plugin.AnalyzeNameAssociator;
import edu.ucla.loni.imageio.DualStreamImageReaderSpi;
import edu.ucla.loni.imageio.FileNameAssociator;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;
import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

/**
 * Dual Stream Image Reader SPI (Service Provider Interface) for the NIFTI 1.0
 * image format.
 *
 * @version 15 July 2005
 */
public class NiftiImageReaderSpi extends DualStreamImageReaderSpi
{
  /** Name of the vendor who supplied this plug-in. */
  private static String _vendorName = "Laboratory of Neuro Imaging (LONI)";

  /** Version of this plug-in. */
  private static String _version = "1.0";

  /** Names of the formats read by this plug-in. */
  private static String[] _formatNames = {"nifti", "NIFTI", "NIFTI-1"};

  /** Names of commonly used suffixes for files in the supported formats. */
  private static String[] _fileSuffixes = {"hdr", "HDR", "nii", "NII"};

  /** MIME types of supported formats. */
  private static String[] _mimeTypes = {"image/hdr", "image/nii"};

  /** Name of the associated Image Reader. */
  private static String _reader = NiftiImageReader.class.getName();

  /** Allowed input type classes for the plugin-in. */
  private static Class[] _inputTypes = {AnalyzeInputStream.class, File.class,
                                        ImageInputStream.class, Vector.class};

  /** Names of the associated Image Writer SPI's. */
  private static String[] _writeSpis = {NiftiImageWriterSpi.class.getName()};

  /** Constructs a NIFTI Image Reader SPI. */
  public NiftiImageReaderSpi()
    {
      super(_vendorName, _version, _formatNames, _fileSuffixes, _mimeTypes,
	    _reader, _inputTypes, _writeSpis,

	    // Support one native stream metatdata format
	    true, 
	    NiftiMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    NiftiMetadataFormat.class.getName(), null, null,

	    // Do not support native image metatdata formats
	    false, null, null, null, null);
    }

  /**
   * Gets an input stream from the specified source suitable for decoding by
   * the NIFTI Image Reader.
   *
   * @param source Source object from which to get an input stream.
   *
   * @return Analyze Input Stream or Image Input Stream if the source is
   *         recognized, or null if the source is unrecognizable.
   */
  public static Object convertToInput(Object source)
    {
      // Determine if the source can be converted to an Analyze Input Stream
      Object convertSource = AnalyzeInputStream.getAnalyzeInputStream(source);
      if (convertSource != null) { return convertSource; }

      // Assume the source is from an "n+1" NIFTI source
      //
      // Check for one Image Input Stream in a Vector
      if (source instanceof Vector) {
	Vector streams = (Vector)source;

	// Must have one Image Input Stream
	if (streams.size() == 1) {
	  Object stream = streams.elementAt(0);
	  if (stream instanceof ImageInputStream) { return stream; }
	}
      }

      // Check for a File
      if (source instanceof File) {
	try { return new FileImageInputStream( (File)source ); }
	catch (Exception e) { return null; }
      }

      // Check for an Image Input Stream
      if (source instanceof ImageInputStream) { return source; }

      // Cannot convert
      return null;
    }

  /**
   * Determines if the specified Image Input Stream has the structure expected
   * by the NIFTI format.  After the determination is made, the Stream is reset
   * to its original state.
   *
   * @param imageStream Image Input Stream to examine.
   *
   * @return True if the Image Input Stream has the structure expected by the
   *         NIFTI format; false otherwise.
   *
   * @throws IOException If an I/O error occurs while reading a stream.
   */
  public static boolean isNiftiFormat(ImageInputStream imageStream)
    throws IOException
    {
      // Get the NIFTI magic number
      String magicNumber = getNiftiMagicNumber(imageStream);

      // Must not be null
      if (magicNumber != null) { return true; }
      return false;
    }

  /**
   * Gets the NIFTI magic number from the specified Image Input Stream.  The
   * Stream is reset to its original state when this method finishes.
   *
   * @param imageStream Image Input Stream to get the magic number from.
   *
   * @return NIFTI magic number decoded from the Image Input Stream, or null
   *         if none could be decoded.
   *
   * @throws IOException If an I/O error occurs while reading a stream.
   */
  public static String getNiftiMagicNumber(ImageInputStream imageStream)
    throws IOException
    {
      // Mark the current position
      imageStream.mark();

      // Read the header (348 bytes)
      byte[] header = new byte[348];
      imageStream.readFully(header);

      // Reset the position of the stream
      imageStream.reset();

      // Check if the last 4 bytes correspond to the NIFTI magic number "ni1\0"
      if ( header[344] == 0x6E && header[345] == 0x69 &&
	   header[346] == 0x31 && header[347] == 0x00 ) { return "ni1"; }

      // Check if the last 4 bytes correspond to the NIFTI magic number "n+1\0"
      if ( header[344] == 0x6E && header[345] == 0x2B &&
	   header[346] == 0x31 && header[347] == 0x00 ) { return "n+1"; }

      // Unknown magic number
      return null;
    }

  /**
   * Determines if the supplied source object appears to be in a supported
   * format.  After the determination is made, the source is reset to its
   * original state if that operation is allowed.
   *
   * @param source Source Object to examine.
   *
   * @return True if the supplied source object appears to be in a supported
   *         format; false otherwise.
   *
   * @throws IOException If an I/O error occurs while reading a stream.
   * @throws IllegalArgumentException If the source is null.
   */
  public boolean canDecodeInput(Object source) throws IOException
    {
      ImageInputStream imageStream = null;

      // Check for a null argument
      if (source == null) {
	String msg = "Cannot have a null source.";
	throw new IllegalArgumentException(msg);
      }

      // Convert the source to an input stream
      Object stream = convertToInput(source);

      // Can not decode
      if (stream == null) { return false; }

      // Get the metadata Input Stream of an Analyze Input Stream
      if (stream instanceof AnalyzeInputStream) {
	AnalyzeInputStream analyzeStream = (AnalyzeInputStream)stream;
	imageStream = analyzeStream.getMetadataInputStream();
      }
      else { imageStream = (ImageInputStream)stream; }

      // Get the NIFTI magic number
      String magicNumber = getNiftiMagicNumber(imageStream);
      if (magicNumber == null) { return false; }

      // Analyze Input Stream requires two Streams
      if (stream instanceof AnalyzeInputStream && magicNumber.equals("ni1")) {
	return true;
      }

      // Single Image Input Stream
      if (stream instanceof ImageInputStream && magicNumber.equals("n+1")) {
	return true;
      }

      // Incorrect number of Streams
      return false;
    }

  /**
   * Returns a brief, human-readable description of this service provider and
   * its associated implementation.
   *
   * @param locale Locale for which the return value should be localized.
   *
   * @return Description of this service provider.
   */
  public String getDescription(Locale locale)
    {
      return "NIFTI image reader";
    }

  /**
   * Returns an instance of the ImageReader implementation associated with
   * this service provider.  The returned object will be in an initial state as
   * if its reset method had been called.
   *
   * @param extension A plug-in specific extension object, which may be null.
   *
   * @return An ImageReader instance.
   *
   * @throws IllegalArgumentException If the ImageReader finds the extension
   *                                  object to be unsuitable.
   */
  public ImageReader createReaderInstance(Object extension)
    {
      return new NiftiImageReader(this);
    }

    /**
     * Gets the File Name Associator that generates file name associations
     * between metadata files and image pixel files.
     *
     * @return File Name Associator for this format.
     */
    public FileNameAssociator getFileNameAssociator()
    {
	return new AnalyzeNameAssociator();
    }
}
