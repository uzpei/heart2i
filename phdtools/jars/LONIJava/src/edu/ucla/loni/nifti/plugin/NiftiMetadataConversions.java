/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.nifti.plugin;

import edu.ucla.loni.nifti.NiftiHeader;
import java.util.Iterator;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between the NIFTI Header class and the
 * DOM Node representation of it.
 *
 * @version 29 October 2003
 */
public class NiftiMetadataConversions
{
  /** Constructs a NIFTI Metadata Conversions. */
  private NiftiMetadataConversions()
    {
    }

  /**
   * Converts the tree to a NIFTI Header.
   *
   * @param root DOM Node representing the NIFTI Header.
   *
   * @return NIFTI Header constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static NiftiHeader toNiftiHeader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = NiftiMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a NIFTI Header from the Node attributes
      NiftiHeader header;
      try {
	NamedNodeMap atts = root.getAttributes();

	// Size of the header in bytes
	String id = NiftiHeader.SIZEOF_HDR;
	int sizeOfHdr = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// Unused
	id = NiftiHeader.DATA_TYPE;
	byte[] dataType = atts.getNamedItem(id).getNodeValue().getBytes();

	// Unused
	id = NiftiHeader.DB_NAME;
	byte[] dbName = atts.getNamedItem(id).getNodeValue().getBytes();

	// Unused
	id = NiftiHeader.EXTENTS;
	int extents = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// Unused
	id = NiftiHeader.SESSION_ERROR;
	short error = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Unused
	id = NiftiHeader.REGULARITY;
	byte regular = Byte.parseByte( atts.getNamedItem(id).getNodeValue() );

	// MRI slice ordering
	id = NiftiHeader.DIM_INFO;
	byte dimInfo = Byte.parseByte( atts.getNamedItem(id).getNodeValue() );

	// Number of dimensions
	id = NiftiHeader.DIM_0;
	short dim0 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Image X dimension
	id = NiftiHeader.DIM_1;
	short dim1 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Image Y dimension
	id = NiftiHeader.DIM_2;
	short dim2 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Image Z dimension
	id = NiftiHeader.DIM_3;
	short dim3 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Time points
	id = NiftiHeader.DIM_4;
	short dim4 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Fifth file dimension
	id = NiftiHeader.DIM_5;
	short dim5 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Sixth file dimension
	id = NiftiHeader.DIM_6;
	short dim6 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Seventh file dimension
	id = NiftiHeader.DIM_7;
	short dim7 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// First intent parameter
	id = NiftiHeader.INTENT_P1;
	float intP1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Second intent parameter
	id = NiftiHeader.INTENT_P2;
	float intP2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Third intent parameter
	id = NiftiHeader.INTENT_P3;
	float intP3 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// NIFTI_INTENT_* code
	id = NiftiHeader.INTENT_CODE;
	short iCode = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// Defines the data type
	id = NiftiHeader.DATATYPE;
	short dtype = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// Number of bits per voxel
	id = NiftiHeader.BIT_PIX;
	short bitPix = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// First slice index
	id = NiftiHeader.SLICE_START;
	short sStart = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// Unused
	id = NiftiHeader.PIX_DIM_0;
	float pixDim0 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Voxel width
	id = NiftiHeader.PIX_DIM_1;
	float pixDim1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Voxel height
	id = NiftiHeader.PIX_DIM_2;
	float pixDim2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Slice thickness
	id = NiftiHeader.PIX_DIM_3;
	float pixDim3 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Time
	id = NiftiHeader.PIX_DIM_4;
	float pixDim4 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Real world measurement of the fifth file dimension
	id = NiftiHeader.PIX_DIM_5;
	float pixDim5 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Real world measurement of the sixth file dimension
	id = NiftiHeader.PIX_DIM_6;
	float pixDim6 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Real world measurement of the seventh file dimension
	id = NiftiHeader.PIX_DIM_7;
	float pixDim7 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Byte offset to the image data in a ".nii" file
	id = NiftiHeader.VOX_OFFSET;
	float voxOff = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Slope factor for scaling each voxel value
	id = NiftiHeader.SCL_SLOPE;
	float slope = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Offset for scaling each voxel value
	id = NiftiHeader.SCL_INTER;
	float inter = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Last slice index
	id = NiftiHeader.SLICE_END;
	short sEnd = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// Slice timing order
	id = NiftiHeader.SLICE_CODE;
	byte sliceCode = Byte.parseByte(atts.getNamedItem(id).getNodeValue());

	// Units of pixDim[1..4]
	id = NiftiHeader.XYZT_UNITS;
	byte xyztUnits = Byte.parseByte(atts.getNamedItem(id).getNodeValue());

	// Maximum display intensity
	id = NiftiHeader.CAL_MAX;
	float calMax = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Minimum display intensity
	id = NiftiHeader.CAL_MIN;
	float calMin = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Time for one slice
	id = NiftiHeader.SLICE_DURATION;
	float sDur = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Time axis shift
	id = NiftiHeader.T_OFFSET;
	float tOffset = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Unused
	id = NiftiHeader.GL_MAX;
	int glMax = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// Unused
	id = NiftiHeader.GL_MIN;
	int glMin = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// Description
	id = NiftiHeader.DESCRIP;
	byte[] descrip = atts.getNamedItem(id).getNodeValue().getBytes();

	// Auxiliary file name
	id = NiftiHeader.AUX_FILE;
	byte[] auxFile = atts.getNamedItem(id).getNodeValue().getBytes();

	// NIFTI_XFORM_* code
	id = NiftiHeader.QFORM_CODE;
	short qform = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// NIFTI_XFORM_* code
	id = NiftiHeader.SFORM_CODE;
	short sform = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// Quaternion b parameter
	id = NiftiHeader.QUATERN_B;
	float quatB = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Quaternion c parameter
	id = NiftiHeader.QUATERN_C;
	float quatC = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Quaternion d parameter
	id = NiftiHeader.QUATERN_D;
	float quatD = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Quaternion x shift
	id = NiftiHeader.QOFFSET_X;
	float qoffX = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Quaternion y shift
	id = NiftiHeader.QOFFSET_Y;
	float qoffY = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Quaternion z shift
	id = NiftiHeader.QOFFSET_Z;
	float qoffZ = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the first row and first column of the affine transform
	id = NiftiHeader.SROW_X_1;
	float srowX1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the first row and second column of the affine transform
	id = NiftiHeader.SROW_X_2;
	float srowX2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the first row and third column of the affine transform
	id = NiftiHeader.SROW_X_3;
	float srowX3 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the first row and fourth column of the affine transform
	id = NiftiHeader.SROW_X_4;
	float srowX4 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the second row and first column of the affine transform
	id = NiftiHeader.SROW_Y_1;
	float srowY1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the second row and second column of the affine transform
	id = NiftiHeader.SROW_Y_2;
	float srowY2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the second row and third column of the affine transform
	id = NiftiHeader.SROW_Y_3;
	float srowY3 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the second row and fourth column of the affine transform
	id = NiftiHeader.SROW_Y_4;
	float srowY4 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the third row and first column of the affine transform
	id = NiftiHeader.SROW_Z_1;
	float srowZ1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the third row and second column of the affine transform
	id = NiftiHeader.SROW_Z_2;
	float srowZ2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the third row and third column of the affine transform
	id = NiftiHeader.SROW_Z_3;
	float srowZ3 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Element in the third row and fourth column of the affine transform
	id = NiftiHeader.SROW_Z_4;
	float srowZ4 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// Name or meaning of the voxel data
	id = NiftiHeader.INTENT_NAME;
	byte[] intentName = atts.getNamedItem(id).getNodeValue().getBytes();

	// Magic number
	id = NiftiHeader.MAGIC;
	byte[] magic = atts.getNamedItem(id).getNodeValue().getBytes();

	// Construct a new NIFTI Header
	header = new NiftiHeader(sizeOfHdr, dataType, dbName, extents,
				 error, regular, dimInfo, dim0, dim1, dim2,
				 dim3, dim4, dim5, dim6, dim7, intP1, intP2,
				 intP3, iCode, dtype, bitPix, sStart, pixDim0,
				 pixDim1, pixDim2, pixDim3, pixDim4, pixDim5,
				 pixDim6, pixDim7, voxOff, slope, inter, sEnd,
				 sliceCode, xyztUnits, calMax, calMin, sDur,
				 tOffset, glMax, glMin, descrip, auxFile,
				 qform, sform, quatB, quatC, quatD, qoffX,
				 qoffY, qoffZ, srowX1, srowX2, srowX3, srowX4,
				 srowY1, srowY2, srowY3, srowY4, srowZ1,
				 srowZ2, srowZ3, srowZ4, intentName, magic);
      }

      // Unable to construct a NIFTI Header
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a NIFTI Header.";
	throw new IIOInvalidTreeException(msg, e, root);
      }

      // Return the NIFTI Header
      return header;
    }

  /**
   * Converts the NIFTI Header to a tree.
   *
   * @param niftiHeader NIFTI Header to convert to a tree.
   *
   * @return DOM Node representing the NIFTI Header.
   */
  public static Node toTree(NiftiHeader niftiHeader)
    {
      // Create the root Node
      String rootName = NiftiMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      // Add each field as an attribute
      if (niftiHeader != null) {
	Iterator iter = niftiHeader.getFieldNames();
	while ( iter.hasNext() ) {
	  String fieldName = (String)iter.next();
	  Object fieldValue = niftiHeader.getFieldValue(fieldName);

	  // Set the attribute
	  rootNode.setAttribute(fieldName, fieldValue.toString());
	}
      }

      // Return the root Node
      return rootNode;
    }
}
