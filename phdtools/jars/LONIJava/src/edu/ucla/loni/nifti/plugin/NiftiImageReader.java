/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.nifti.plugin;

import edu.ucla.loni.analyze.plugin.AnalyzeInputStream;
import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.nifti.NiftiHeader;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.MemoryCacheImageInputStream;

/**
 * Image Reader for parsing and decoding NIFTI 1.0 images.
 *
 * @version 4 January 2006
 */
public class NiftiImageReader extends ImageReader
{
    /** NIFTI Header decoded from the input source. */
    private NiftiHeader _niftiHeader;

    /** True if the encoded NIFTI Header is byte swapped; false otherwise. */
    private boolean _isByteSwapped;

    /**
     * Constructs a NiftiImageReader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not a
     *                                  NIFTI Image Reader Spi.
     */
    public NiftiImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be a NIFTI Image Reader Spi
	if ( !(originatingProvider instanceof NiftiImageReaderSpi) ) {
	    String msg = "Originating provider must be a NIFTI Image Reader " +
		"SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The ImageInputStream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of the classes returned by the
     *                                  originating service provider's
     *                                  getInputTypes method, or is not an
     *                                  ImageInputStream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	// Convert the input source into an Image or Analyze Input Stream
	input = NiftiImageReaderSpi.convertToInput(input);

	super.setInput(input, seekForwardOnly, ignoreMetadata);

	// Remove any decoded NIFTI Header
	_niftiHeader = null;
    }

    /**
     * Returns the number of images, not including thumbnails, available from
     * the current input source.
     * 
     * @param allowSearch If true, the true number of images will be returned
     *                    even if a search is required.  If false, the reader
     *                    may return -1 without performing the search.
     *
     * @return The number of images or -1 if allowSearch is false and a search
     *         would be required.
     *
     * @throws IllegalStateException If the input source has not been set.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    public int getNumImages(boolean allowSearch) throws IOException
    {
	NiftiMetadata metadata = new NiftiMetadata( _getNiftiHeader() );
	int numberOfImages = metadata.getNumberOfImages();

	// Unable to determine the number of images
	if (numberOfImages == -1) {
	    String msg = "Unable to determine the number of images.";
	    throw new IOException(msg);
	}

	return numberOfImages;
    }

    /**
     * Returns the width in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The width of the image in pixels.
     *
     * @throws IOException If an error occurs reading the width information from
     *                     the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getWidth(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image width
	NiftiMetadata metadata = new NiftiMetadata( _getNiftiHeader() );
	int width = metadata.getImageWidth();

	// Unable to determine the image width
	if (width == -1) {
	    String msg = "Unable to determine the image width.";
	    throw new IOException(msg);
	}

	return width;
    }

    /**
     * Returns the height in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The height of the image in pixels.
     *
     * @throws IOException If an error occurs reading the height information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getHeight(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image height
	NiftiMetadata metadata = new NiftiMetadata( _getNiftiHeader() );
	int height = metadata.getImageHeight();

	// Unable to determine the image height
	if (height == -1) {
	    String msg = "Unable to determine the image height.";
	    throw new IOException(msg);
	}

	return height;
    }

    /**
     * Returns an Iterator containing possible image types to which the given
     * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
     * legal image type will be returned.
     *
     * @param imageIndex The index of the image to be retrieved.
     *
     * @return An Iterator containing at least one ImageTypeSpecifier
     *         representing suggested image types for decoding the current given
     *         image.
     *
     * @throws IOException If an error occurs reading the format information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public Iterator getImageTypes(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image parameters
	NiftiMetadata metadata = new NiftiMetadata( _getNiftiHeader() );
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);
	int bitsPerPixel = metadata.getBitsPerPixel();
	int type = metadata.getDataType();

	// Color images
	ArrayList list = new ArrayList(1);
	if ( metadata.hasColorImages() ) {
	    list.add( Utilities.getRgbImageType(width, height) );
	    return list.iterator();
	}

	// Determine the appropriate Image Type Specifier
	if (type == 4) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_SHORT) );
	}
	else if (bitsPerPixel == 8 || bitsPerPixel == 16) {
	    list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
	}
	else if ((type == 8 || type == 768) && bitsPerPixel == 32) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_INT) );
	}
	else if (type == 16 && bitsPerPixel == 32) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_FLOAT) );
	}
	else if (type == 64 && bitsPerPixel == 64) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_DOUBLE) );
	}

	// Check if the image data type is supported
	if (list.size() == 0) {
	    String msg = "Unsupported NIFTI image type:  data type = " + type +
		"; bits per voxel = " + bitsPerPixel + ".";
	    throw new IOException(msg);
	}

	// Return the Image Type Specifier
	return list.iterator();
    }

    /**
     * Returns an IIOMetadata object representing the metadata associated with
     * the input source as a whole (i.e., not associated with any particular
     * image), or null if the reader does not support reading metadata, is set
     * to ignore metadata, or if no metadata is available.
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     */
    public IIOMetadata getStreamMetadata() throws IOException
    {
	return new NiftiMetadata( _getNiftiHeader() );
    }

    /**
     * Returns an IIOMetadata object containing metadata associated with the
     * given image, or null if the reader does not support reading metadata, is
     * set to ignore metadata, or if no metadata is available.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved. 
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
	// Not applicable
	return null;
    }

    /**
     * Reads the image indexed by imageIndex and returns it as a complete
     * BufferedImage.  The supplied ImageReadParam is ignored.
     *
     * @param imageIndex The index of the image to be retrieved.
     * @param param An ImageReadParam used to control the reading process, or
     *              null.
     *
     * @return The desired portion of the image as a BufferedImage.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public BufferedImage read(int imageIndex, ImageReadParam param)
	throws IOException
    {
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// Update the Listeners
	processImageStarted(imageIndex);

	// Compute the size of the image in bytes
	NiftiMetadata metadata = new NiftiMetadata( _getNiftiHeader() );
	ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
	int dataType = spec.getSampleModel().getDataType();
	int imageSize = width*height;

	if ( metadata.hasColorImages() ) { imageSize *= 3; }
	else if (dataType == DataBuffer.TYPE_SHORT) { imageSize *= 2; }
	else if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }
	else if (dataType == DataBuffer.TYPE_INT) { imageSize *= 4; }
	else if (dataType == DataBuffer.TYPE_FLOAT) { imageSize *= 4; }
	else if (dataType == DataBuffer.TYPE_DOUBLE) { imageSize *= 8; }

	// Set the pixel data Image Input Stream position to the image beginning
	long offset = imageIndex*imageSize;
	ImageInputStream imageStream = _getPixelDataInputStream(offset);

	// Return the raw image data if required
	if ( Utilities.returnRawBytes(param) ) {
	    BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	    DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	    byte[] data = ((DataBufferByte)db).getData();
	    imageStream.read(data);

	    // Byte swap the data if the header is byte swapped
	    if (_isByteSwapped) { _byteSwap(data, dataType); }

	    return bufferedImage;
	}

	// Create a Buffered Image for the image data
	Iterator iter = getImageTypes(0);
	BufferedImage bufferedImage = getDestination(null, iter, width, height);
	DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

	// Read the image data into an array
	byte[] srcBuffer = new byte[imageSize];
	imageStream.readFully(srcBuffer);

	// RGB color (3-byte) image pixel data
	WritableRaster dstRaster = bufferedImage.getRaster();
	if ( metadata.hasColorImages() ) {
	    bufferedImage = Utilities.getRgbImage(srcBuffer, dstRaster, true);
	}

	// 8-bit gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	    bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
	}

	// 16-bit gray image pixel data (byte swap if necessary)
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_USHORT) {
	    bufferedImage =
		Utilities.getGrayUshortImage(srcBuffer, dstRaster, 16, false,
					     _isByteSwapped, 1, 0);
	}

	// General gray image pixel data (byte swap if necessary)
	else {
	    bufferedImage = Utilities.getGrayImage(srcBuffer, dstRaster,
						   _isByteSwapped);
	}

	// Update the Listeners
	if ( abortRequested() ) { processReadAborted(); }
	else { processImageComplete(); }

	// Return the requested image
	return bufferedImage;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageReader.  Otherwise, the reader may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the input stream
	try {
	    if (input instanceof AnalyzeInputStream) {
		((AnalyzeInputStream)input).close();
	    }
	    else if (input instanceof ImageInputStream) {
		((ImageInputStream)input).close();
	    }
	}

	catch (Exception e) {}

	input = null;
    }

    /**
     * Reads the NIFTI Header from the input source.  If the NIFTI Header has
     * already been decoded, no additional read is performed.
     *
     * @return NIFTI Header decoded from the input source.
     * 
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     */
    private NiftiHeader _getNiftiHeader() throws IOException
    {
	// Decode the NIFTI Header if needed
	if (_niftiHeader == null) {

	    // No input has been set
	    if (input == null) {
		String msg = "No input has been set.";
		throw new IllegalStateException(msg);
	    }

	    // Get the metadata Image Input Stream
	    ImageInputStream imageStream;
	    if (input instanceof AnalyzeInputStream) {
		AnalyzeInputStream inputStream = (AnalyzeInputStream)input;
		imageStream = inputStream.getMetadataInputStream();
	    }
	    else { imageStream = (ImageInputStream)input; }

	    // Read the encoded NIFTI header into a byte array and reset Stream
	    byte[] headerBytes = new byte[348];
	    imageStream.mark();
	    imageStream.readFully(headerBytes);
	    imageStream.reset();

	    // Create a new Image Input Stream that reads from the bytes
	    ByteArrayInputStream bStream =new ByteArrayInputStream(headerBytes);
	    MemoryCacheImageInputStream mStream =
		new MemoryCacheImageInputStream(bStream);

	    // Read the number of dimensions
	    mStream.mark();
	    mStream.skipBytes(40);
	    short dim0 = mStream.readShort();
	    mStream.reset();

	    // Determine if bytes should be swapped
	    _isByteSwapped = false;
	    if (dim0 < 0 || dim0 > 7) {
		_isByteSwapped = true;
		mStream.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    }

	    // Define the NIFTI Header field arrays
	    byte[] dataType = new byte[10];
	    byte[] dbName = new byte[18];
	    short[] dim = new short[8];
	    float[] intentP = new float[3];
	    float[] pixDim = new float[8];
	    byte[] descrip = new byte[80];
	    byte[] auxFile = new byte[24];
	    float[] quatern = new float[3];
	    float[] qoffset = new float[3];
	    float[] srow = new float[12];
	    byte[] intentName = new byte[16];
	    byte[] magic = new byte[4];

	    // Read the NIFTI Header fields
	    int sizeOfHdr = mStream.readInt();
	    mStream.readFully(dataType);
	    mStream.readFully(dbName);
	    int extents = mStream.readInt();
	    short sessionError = mStream.readShort();
	    byte regular = mStream.readByte();
	    byte dimInfo = mStream.readByte();
	    mStream.readFully(dim, 0, dim.length);
	    mStream.readFully(intentP, 0, intentP.length);
	    short intentCode = mStream.readShort();
	    short datatype = mStream.readShort();
	    short bitPix = mStream.readShort();
	    short sliceStart = mStream.readShort();
	    mStream.readFully(pixDim, 0, pixDim.length);
	    float voxOffset = mStream.readFloat();
	    float sclSlope = mStream.readFloat();
	    float sclInter = mStream.readFloat();
	    short sliceEnd = mStream.readShort();
	    byte sliceCode = mStream.readByte();
	    byte xyztUnits = mStream.readByte();
	    float calMax = mStream.readFloat();
	    float calMin = mStream.readFloat();
	    float sliceDuration = mStream.readFloat();
	    float tOffset = mStream.readFloat();
	    int glMax = mStream.readInt();
	    int glMin = mStream.readInt();
	    mStream.readFully(descrip);
	    mStream.readFully(auxFile);
	    short qformCode = mStream.readShort();
	    short sformCode = mStream.readShort();
	    mStream.readFully(quatern, 0, quatern.length);
	    mStream.readFully(qoffset, 0, qoffset.length);
	    mStream.readFully(srow, 0, srow.length);
	    mStream.readFully(intentName);
	    mStream.readFully(magic);

	    // Construct the NIFTI Header
	    _niftiHeader =
		new NiftiHeader(sizeOfHdr, dataType, dbName, extents,
				sessionError, regular, dimInfo, dim[0], dim[1],
				dim[2], dim[3], dim[4], dim[5], dim[6], dim[7],
				intentP[0], intentP[1], intentP[2], intentCode,
				datatype, bitPix, sliceStart, pixDim[0],
				pixDim[1], pixDim[2], pixDim[3], pixDim[4],
				pixDim[5], pixDim[6], pixDim[7], voxOffset,
				sclSlope, sclInter, sliceEnd, sliceCode,
				xyztUnits, calMax, calMin, sliceDuration,
				tOffset, glMax, glMin, descrip, auxFile,
				qformCode, sformCode, quatern[0], quatern[1],
				quatern[2], qoffset[0], qoffset[1], qoffset[2],
				srow[0], srow[1], srow[2], srow[3], srow[4],
				srow[5], srow[6], srow[7], srow[8], srow[9],
				srow[10], srow[11], intentName, magic);
	}

	return _niftiHeader;
    }

    /**
     * Checks whether or not the specified image index is valid.
     *
     * @param imageIndex The index of the image to check.
     *
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private void _checkImageIndex(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	if ( imageIndex >= getNumImages(false) ) {
	    String msg = "Image index of " + imageIndex + " >= the total " +
		"number of images (" + getNumImages(false) + ")";
	    throw new IndexOutOfBoundsException(msg);
	}
    }

    /**
     * Gets the input stream for the pixel data.
     *
     * @param offset Offset from the stream beginning at which to set the input
     *               stream.
     *
     * @return Image Input Stream to read NIFTI image pixel data from.
     *
     * @throws IOException If an error occurs while setting the Input Stream.
     * @throws IllegalStateException If the input has not been set.
     */
    private ImageInputStream _getPixelDataInputStream(long offset)
	throws IOException
    {
	// No input has been set
	if (input == null) {
	    String msg = "No input has been set.";
	    throw new IllegalStateException(msg);
	}

	// Get the pixel data Input Stream
	ImageInputStream imageStream;
	if (input instanceof AnalyzeInputStream) {
	    AnalyzeInputStream inputStream = (AnalyzeInputStream)input;
	    imageStream = inputStream.getPixelDataInputStream();
	}
	else { imageStream = (ImageInputStream)input; }

	// Get the magic number
	NiftiHeader header = _getNiftiHeader();
	String magicNumber = (String)header.getFieldValue(NiftiHeader.MAGIC);

	// Metadata and pixel data are in the same Stream
	if (magicNumber != null && magicNumber.equals("n+1")) {

	    // Add the voxel offset to the overall offset
	    Object voxOffsetField =header.getFieldValue(NiftiHeader.VOX_OFFSET);
	    offset += ((Float)voxOffsetField).intValue();
	}

	// Set the Input Stream to the appropriate offset
	imageStream.seek(offset);

	// Return the Image Input Stream for image pixel data
	return imageStream;
    }

    /**
     * Byte swaps the specified array.
     *
     * @param b Byte array to swap.
     * @param type Data type to swap.
     */
    private void _byteSwap(byte[] b, int type)
    {
	// Byte swap shorts
	if (type == DataBuffer.TYPE_USHORT) {
	    for (int i = 0; i < b.length; i+=2) {
		byte b1 = b[i];

		b[i] = b[i+1];
		b[i+1] = b1;
	    }
	}

	// Byte swap integers or floats
	else if (type == DataBuffer.TYPE_INT || type == DataBuffer.TYPE_FLOAT) {
	    for (int i = 0; i < b.length; i+=4) {
		byte b1 = b[i];
		byte b2 = b[i+1];

		b[i] = b[i+3];
		b[i+1] = b[i+2];
		b[i+2] = b2;
		b[i+3] = b1;
	    }
	}

	// Byte swap doubles
	else if (type == DataBuffer.TYPE_DOUBLE) {
	    for (int i = 0; i < b.length; i+=8) {
		byte b1 = b[i];
		byte b2 = b[i+1];
		byte b3 = b[i+2];
		byte b4 = b[i+3];

		b[i] = b[i+7];
		b[i+1] = b[i+6];
		b[i+2] = b[i+5];
		b[i+3] = b[i+4];
		b[i+4] = b4;
		b[i+5] = b3;
		b[i+6] = b2;
		b[i+7] = b1;
	    }
	}
    }
}
