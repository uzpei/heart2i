/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.nifti;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * Header structure that contains the fields of the NIFTI-1 image format.
 *
 * @version 29 October 2003
 */
public class NiftiHeader
{
  /** Size of the header in bytes. */
  public static final String SIZEOF_HDR = "sizeof_hdr";

  /** Unused. */
  public static final String DATA_TYPE = "data_type";

  /** Unused. */
  public static final String DB_NAME = "db_name";

  /** Unused. */
  public static final String EXTENTS = "extents";

  /** Unused. */
  public static final String SESSION_ERROR = "session_error";

  /** Unused. */
  public static final String REGULARITY = "regular";

  /** MRI slice ordering; contains frequency, phase, and slice dimensions. */
  public static final String DIM_INFO = "dim_info";

  /** Number of dimensions. */
  public static final String DIM_0 = "dim_0";

  /** Image X dimension; number of pixels in an image row. */
  public static final String DIM_1 = "dim_1";

  /** Image Y dimension; number of pixel rows in a slice. */
  public static final String DIM_2 = "dim_2";

  /** Image Z dimension; number of slices in a volume. */
  public static final String DIM_3 = "dim_3";

  /** Time points; number of volumes in the file. */
  public static final String DIM_4 = "dim_4";

  /** Fifth file dimension. */
  public static final String DIM_5 = "dim_5";

  /** Sixth file dimension. */
  public static final String DIM_6 = "dim_6";

  /** Seventh file dimension. */
  public static final String DIM_7 = "dim_7";

  /** First intent parameter. */
  public static final String INTENT_P1 = "intent_p1";

  /** Second intent parameter. */
  public static final String INTENT_P2 = "intent_p2";

  /** Third intent parameter. */
  public static final String INTENT_P3 = "intent_p3";

  /** NIFTI_INTENT_* code. */
  public static final String INTENT_CODE = "intent_code";

  /** Defines the data type. */
  public static final String DATATYPE = "datatype";

  /** Number of bits per voxel. */
  public static final String BIT_PIX = "bitpix";

  /** First slice index. */
  public static final String SLICE_START = "slice_start";

  /** Unused. */
  public static final String PIX_DIM_0 = "pixdim_0";

  /** Voxel width. */
  public static final String PIX_DIM_1 = "pixdim_1";

  /** Voxel height. */
  public static final String PIX_DIM_2 = "pixdim_2";

  /** Slice thickness. */
  public static final String PIX_DIM_3 = "pixdim_3";

  /** Time. */
  public static final String PIX_DIM_4 = "pixdim_4";

  /** Real world measurement of the fifth file dimension. */
  public static final String PIX_DIM_5 = "pixdim_5";

  /** Real world measurement of the sixth file dimension. */
  public static final String PIX_DIM_6 = "pixdim_6";

  /** Real world measurement of the seventh file dimension. */
  public static final String PIX_DIM_7 = "pixdim_7";

  /** Byte offset to the image data in a ".nii" file. */
  public static final String VOX_OFFSET = "vox_offset";

  /** Slope factor for scaling each voxel value. */
  public static final String SCL_SLOPE = "scl_slope";

  /** Offset for scaling each voxel value. */
  public static final String SCL_INTER = "scl_inter";

  /** Last slice index. */
  public static final String SLICE_END = "slice_end";

  /** Slice timing order. */
  public static final String SLICE_CODE = "slice_code";

  /** Units of pixDim[1..4]; contains spatial and temporal units. */
  public static final String XYZT_UNITS = "xyzt_units";

  /** Maximum display intensity. */
  public static final String CAL_MAX = "cal_max";

  /** Minimum display intensity. */
  public static final String CAL_MIN = "cal_min";

  /** Time for one slice. */
  public static final String SLICE_DURATION = "slice_duration";

  /** Time axis shift. */
  public static final String T_OFFSET = "toffset";

  /** Unused. */
  public static final String GL_MAX = "glmax";

  /** Unused. */
  public static final String GL_MIN = "glmin";

  /** Description. */
  public static final String DESCRIP = "descrip";

  /** Auxiliary file name. */
  public static final String AUX_FILE = "aux_file";

  /** NIFTI_XFORM_* code. */
  public static final String QFORM_CODE = "qform_code";

  /** NIFTI_XFORM_* code. */
  public static final String SFORM_CODE = "sform_code";

  /** Quaternion b parameter. */
  public static final String QUATERN_B = "quatern_b";

  /** Quaternion c parameter. */
  public static final String QUATERN_C = "quatern_c";

  /** Quaternion d parameter. */
  public static final String QUATERN_D = "quatern_d";

  /** Quaternion x shift. */
  public static final String QOFFSET_X = "qoffset_x";

  /** Quaternion y shift. */
  public static final String QOFFSET_Y = "qoffset_y";

  /** Quaternion z shift. */
  public static final String QOFFSET_Z = "qoffset_z";

  /** Element in the first row and first column of the affine transform. */
  public static final String SROW_X_1 = "srow_x_1";

  /** Element in the first row and second column of the affine transform. */
  public static final String SROW_X_2 = "srow_x_2";

  /** Element in the first row and third column of the affine transform. */
  public static final String SROW_X_3 = "srow_x_3";

  /** Element in the first row and fourth column of the affine transform. */
  public static final String SROW_X_4 = "srow_x_4";

  /** Element in the second row and first column of the affine transform. */
  public static final String SROW_Y_1 = "srow_y_1";

  /** Element in the second row and second column of the affine transform. */
  public static final String SROW_Y_2 = "srow_y_2";

  /** Element in the second row and third column of the affine transform. */
  public static final String SROW_Y_3 = "srow_y_3";

  /** Element in the second row and fourth column of the affine transform. */
  public static final String SROW_Y_4 = "srow_y_4";

  /** Element in the third row and first column of the affine transform. */
  public static final String SROW_Z_1 = "srow_z_1";

  /** Element in the third row and second column of the affine transform. */
  public static final String SROW_Z_2 = "srow_z_2";

  /** Element in the third row and third column of the affine transform. */
  public static final String SROW_Z_3 = "srow_z_3";

  /** Element in the third row and fourth column of the affine transform. */
  public static final String SROW_Z_4 = "srow_z_4";

  /** Name or meaning of the voxel data. */
  public static final String INTENT_NAME = "intent_name";

  /** Magic number. */
  public static final String MAGIC = "magic";

  /** Field names mapped to their values. */
  private LinkedHashMap _fields;

  /**
   * Creates a NIFTI Header.
   *
   * @param sizeOfHdr The size of the header in bytes.
   * @param dataType Unused; this must be 10 bytes or less.
   * @param dbName Unused; this must be 18 bytes or less.
   * @param extents Unused.
   * @param sessionError Unused.
   * @param regular Unused.
   * @param dimInfo MRI slice ordering; contains frequency, phase, and slice
   *                dimensions.
   * @param dim0 Number of dimensions.
   * @param dim1 Image X dimension; number of pixels in an image row.
   * @param dim2 Image Y dimension; number of pixel rows in a slice.
   * @param dim3 Image Z dimension; number of slices in a volume.
   * @param dim4 Time points; number of volumes in the file.
   * @param dim5 Fifth file dimension.
   * @param dim6 Sixth file dimension.
   * @param dim7 Seventh file dimension.
   * @param intentP1 First intent parameter.
   * @param intentP2 Second intent parameter.
   * @param intentP3 Third intent parameter.
   * @param intentCode NIFTI_INTENT_* code.
   * @param datatype Defines the data type.
   * @param bitpix Number of bits per voxel.
   * @param sliceStart First slice index.
   * @param pixDim0 Unused.
   * @param pixDim1 Voxel width.
   * @param pixDim2 Voxel height.
   * @param pixDim3 Slice thickness.
   * @param pixDim4 Time.
   * @param pixDim5 Real world measurement of the fifth file dimension.
   * @param pixDim6 Real world measurement of the sixth file dimension.
   * @param pixDim7 Real world measurement of the seventh file dimension.
   * @param voxOffset Byte offset to the image data in a ".nii" file.
   * @param sclSlope Slope factor for scaling each voxel value.
   * @param sclInter Offset for scaling each voxel value.
   * @param sliceEnd Last slice index.
   * @param sliceCode Slice timing order.
   * @param xyztUnits Units of pixDim[1..4]; contains spatial and temporal
   *                  units.
   * @param calMax Maximum display intensity.
   * @param calMin Minimum display intensity.
   * @param sliceDuration Time for one slice.
   * @param tOffset Time axis shift.
   * @param glMax Unused.
   * @param glMin Unused.
   * @param descrip Description; this must be 80 bytes or less.
   * @param auxFile Auxiliary file name; this must be 24 bytes or less.
   * @param qformCode NIFTI_XFORM_* code.
   * @param sformCode NIFTI_XFORM_* code.
   * @param quaternB Quaternion b parameter.
   * @param quaternC Quaternion c parameter.
   * @param quaternD Quaternion d parameter.
   * @param qoffsetX Quaternion x shift.
   * @param qoffsetY Quaternion y shift.
   * @param qoffsetZ Quaternion z shift.
   * @param srowX1 Element in the first row and first column of the affine
   *               transform.
   * @param srowX2 Element in the first row and second column of the affine
   *               transform.
   * @param srowX3 Element in the first row and third column of the affine
   *               transform.
   * @param srowX4 Element in the first row and fourth column of the affine
   *               transform.
   * @param srowY1 Element in the second row and first column of the affine
   *               transform.
   * @param srowY2 Element in the second row and second column of the affine
   *               transform.
   * @param srowY3 Element in the second row and third column of the affine
   *               transform.
   * @param srowY4 Element in the second row and fourth column of the affine
   *               transform.
   * @param srowZ1 Element in the third row and first column of the affine
   *               transform.
   * @param srowZ2 Element in the third row and second column of the affine
   *               transform.
   * @param srowZ3 Element in the third row and third column of the affine
   *               transform.
   * @param srowZ4 Element in the third row and fourth column of the affine
   *               transform.
   * @param intentName Name or meaning of the voxel data; this must be 16 bytes
   *                   or less.
   * @param magic Magic number; this must be 4 bytes or less.
   */
  public NiftiHeader(int sizeOfHdr, byte[] dataType, byte[] dbName,
		     int extents, short sessionError, byte regular,
		     byte dimInfo, short dim0, short dim1, short dim2,
		     short dim3, short dim4, short dim5, short dim6,
		     short dim7, float intentP1, float intentP2,
		     float intentP3, short intentCode, short datatype,
		     short bitPix, short sliceStart, float pixDim0,
		     float pixDim1, float pixDim2, float pixDim3,
		     float pixDim4, float pixDim5, float pixDim6,
		     float pixDim7, float voxOffset, float sclSlope,
		     float sclInter, short sliceEnd, byte sliceCode,
		     byte xyztUnits, float calMax, float calMin,
		     float sliceDuration, float tOffset, int glMax, int glMin,
		     byte[] descrip, byte[] auxFile, short qformCode,
		     short sformCode, float quaternB, float quaternC,
		     float quaternD, float qoffsetX, float qoffsetY,
		     float qoffsetZ, float srowX1, float srowX2, float srowX3,
		     float srowX4, float srowY1, float srowY2, float srowY3,
		     float srowY4, float srowZ1, float srowZ2, float srowZ3,
		     float srowZ4, byte[] intentName, byte[] magic)
    {
      _fields = new LinkedHashMap(66);

      _addField(SIZEOF_HDR, new Integer(sizeOfHdr));
      _addField(DATA_TYPE, _createString(dataType));
      _addField(DB_NAME, _createString(dbName));
      _addField(EXTENTS, new Integer(extents));
      _addField(SESSION_ERROR, new Short(sessionError));
      _addField(REGULARITY, new Byte(regular));
      _addField(DIM_INFO, new Byte(dimInfo));
      _addField(DIM_0, new Short(dim0));
      _addField(DIM_1, new Short(dim1));
      _addField(DIM_2, new Short(dim2));
      _addField(DIM_3, new Short(dim3));
      _addField(DIM_4, new Short(dim4));
      _addField(DIM_5, new Short(dim5));
      _addField(DIM_6, new Short(dim6));
      _addField(DIM_7, new Short(dim7));
      _addField(INTENT_P1, new Float(intentP1));
      _addField(INTENT_P2, new Float(intentP2));
      _addField(INTENT_P3, new Float(intentP3));
      _addField(INTENT_CODE, new Short(intentCode));
      _addField(DATATYPE, new Short(datatype));
      _addField(BIT_PIX, new Short(bitPix));
      _addField(SLICE_START, new Short(sliceStart));
      _addField(PIX_DIM_0, new Float(pixDim0));
      _addField(PIX_DIM_1, new Float(pixDim1));
      _addField(PIX_DIM_2, new Float(pixDim2));
      _addField(PIX_DIM_3, new Float(pixDim3));
      _addField(PIX_DIM_4, new Float(pixDim4));
      _addField(PIX_DIM_5, new Float(pixDim5));
      _addField(PIX_DIM_6, new Float(pixDim6));
      _addField(PIX_DIM_7, new Float(pixDim7));
      _addField(VOX_OFFSET, new Float(voxOffset));
      _addField(SCL_SLOPE, new Float(sclSlope));
      _addField(SCL_INTER, new Float(sclInter));
      _addField(SLICE_END, new Short(sliceEnd));
      _addField(SLICE_CODE, new Byte(sliceCode));
      _addField(XYZT_UNITS, new Byte(xyztUnits));
      _addField(CAL_MAX, new Float(calMax));
      _addField(CAL_MIN, new Float(calMin));
      _addField(SLICE_DURATION, new Float(sliceDuration));
      _addField(T_OFFSET, new Float(tOffset));
      _addField(GL_MAX, new Integer(glMax));
      _addField(GL_MIN, new Integer(glMin));
      _addField(DESCRIP, _createString(descrip));
      _addField(AUX_FILE, _createString(auxFile));
      _addField(QFORM_CODE, new Short(qformCode));
      _addField(SFORM_CODE, new Short(sformCode));
      _addField(QUATERN_B, new Float(quaternB));
      _addField(QUATERN_C, new Float(quaternC));
      _addField(QUATERN_D, new Float(quaternD));
      _addField(QOFFSET_X, new Float(qoffsetX));
      _addField(QOFFSET_Y, new Float(qoffsetY));
      _addField(QOFFSET_Z, new Float(qoffsetZ));
      _addField(SROW_X_1, new Float(srowX1));
      _addField(SROW_X_2, new Float(srowX2));
      _addField(SROW_X_3, new Float(srowX3));
      _addField(SROW_X_4, new Float(srowX4));
      _addField(SROW_Y_1, new Float(srowY1));
      _addField(SROW_Y_2, new Float(srowY2));
      _addField(SROW_Y_3, new Float(srowY3));
      _addField(SROW_Y_4, new Float(srowY4));
      _addField(SROW_Z_1, new Float(srowZ1));
      _addField(SROW_Z_2, new Float(srowZ2));
      _addField(SROW_Z_3, new Float(srowZ3));
      _addField(SROW_Z_4, new Float(srowZ4));
      _addField(INTENT_NAME, _createString(intentName));
      _addField(MAGIC, _createString(magic));
    }

  /**
   * Gets the names of all the fields.
   *
   * @return Names of all the fields.
   */
  public Iterator getFieldNames()
    {
      return _fields.keySet().iterator();
    }

  /**
   * Gets the value of the specified field.
   *
   * @param fieldName Name of the field.
   *
   * @return Object that represents the value of the named field.
   *
   * @throws IllegalArgumentException If the named field does not exist.
   */
  public Object getFieldValue(String fieldName)
    {
      Object value = _fields.get(fieldName);

      // Check that the field exists
      if (value == null) {
	String msg = "Unable to find an field named \"" + fieldName + "\".";
	throw new IllegalArgumentException(msg);
      }

      // Return the value
      return value;
    }

  /**
   * Gets the contents as a String.
   *
   * @return String representation of the contents.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Append each field name and value to the buffer
      Iterator iter = getFieldNames();
      while ( iter.hasNext() ) {
	String name = (String)iter.next();
	Object value = getFieldValue(name);

	// Create a string representation
	buffer.append(name + " = \"" + value + "\" \n");
      }

      return buffer.toString();
    }

  /**
   * Adds a field to the NIFTI Header.  If the field already exists,
   * it is replaced using the new field value.
   *
   * @param name Name of the field.
   * @param value Value of the field.
   */
  protected void _addField(String name, Object value)
    {
      _fields.put(name, value);
    }

  /**
   * Creates a String from the byte array.
   *
   * @param byteArray Array of bytes to construct a String from.
   *
   * @return String created from the byte array.
   */
  protected String _createString(byte[] byteArray)
    {
      // Determine the location of the first null character
      int loc;
      for (loc = 0; loc < byteArray.length && byteArray[loc] != 0; loc++) {}

      // Return a String
      return new String(byteArray, 0, loc);
    }
}
