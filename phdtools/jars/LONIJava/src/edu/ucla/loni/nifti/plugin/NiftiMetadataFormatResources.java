/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.nifti.plugin;

import edu.ucla.loni.nifti.NiftiHeader;
import java.util.ListResourceBundle;

/**
 * List Resource Bundle that provides descriptions for NIFTI 1.0 fields.
 *
 * @version 31 October 2003
 */
public class NiftiMetadataFormatResources extends ListResourceBundle
{
  /** Constructs a NIFTI Metadata Format Resources. */
  public NiftiMetadataFormatResources()
    {
    }

  /**
   * Gets the contents of the Resource Bundle.
   *
   * @return Object array of the contents, where each item of the array is a
   *         pair of Objects.  The first element of each pair is the key, which
   *         must be a String, and the second element is the value associated
   *         with that key.
   */
  public Object[][] getContents()
    {
      String rootName = NiftiMetadataFormat.NATIVE_METADATA_FORMAT_NAME;

      Object[][] contents = {

	// Node name + "/" + attribute name, followed by description
	{rootName + "/" + NiftiHeader.SIZEOF_HDR,
	 "The size of the header in bytes."},
	{rootName + "/" + NiftiHeader.DATA_TYPE,
	 "Unused."},
	{rootName + "/" + NiftiHeader.DB_NAME,
	 "Unused."},
	{rootName + "/" + NiftiHeader.EXTENTS,
	 "Unused."},
	{rootName + "/" + NiftiHeader.SESSION_ERROR,
	 "Unused."},
	{rootName + "/" + NiftiHeader.REGULARITY,
	 "Unused."},
	{rootName + "/" + NiftiHeader.DIM_INFO,
	 "MRI slice ordering; contains frequency, phase, and slice " +
	 "dimensions."},
	{rootName + "/" + NiftiHeader.DIM_0,
	 "Number of dimensions."},
	{rootName + "/" + NiftiHeader.DIM_1,
	 "Image X dimension; number of pixels in an image row."},
	{rootName + "/" + NiftiHeader.DIM_2,
	 "Image Y dimension; number of pixel rows in a slice."},
	{rootName + "/" + NiftiHeader.DIM_3,
	 "Image Z dimension; number of slices in a volume."},
	{rootName + "/" + NiftiHeader.DIM_4,
	 "Time points; number of volumes in the file."},
	{rootName + "/" + NiftiHeader.DIM_5,
	 "Fifth file dimension."},
	{rootName + "/" + NiftiHeader.DIM_6,
	 "Sixth file dimension."},
	{rootName + "/" + NiftiHeader.DIM_7,
	 "Seventh file dimension."},
	{rootName + "/" + NiftiHeader.INTENT_P1,
	 "First intent parameter."},
	{rootName + "/" + NiftiHeader.INTENT_P2,
	 "Second intent parameter."},
	{rootName + "/" + NiftiHeader.INTENT_P3,
	 "Third intent parameter."},
	{rootName + "/" + NiftiHeader.INTENT_CODE,
	 "NIFTI_INTENT_* code."},
	{rootName + "/" + NiftiHeader.DATATYPE,
	 "Defines the data type."},
	{rootName + "/" + NiftiHeader.BIT_PIX,
	 "Number of bits per voxel."},
	{rootName + "/" + NiftiHeader.SLICE_START,
	 "First slice index."},
	{rootName + "/" + NiftiHeader.PIX_DIM_0,
	 "Unused."},
	{rootName + "/" + NiftiHeader.PIX_DIM_1,
	 "Voxel width."},
	{rootName + "/" + NiftiHeader.PIX_DIM_2,
	 "Voxel height."},
	{rootName + "/" + NiftiHeader.PIX_DIM_3,
	 "Slice thickness."},
	{rootName + "/" + NiftiHeader.PIX_DIM_4,
	 "Time."},
	{rootName + "/" + NiftiHeader.PIX_DIM_5,
	 "Real world measurement of the fifth file dimension."},
	{rootName + "/" + NiftiHeader.PIX_DIM_6,
	 "Real world measurement of the sixth file dimension."},
	{rootName + "/" + NiftiHeader.PIX_DIM_7,
	 "Real world measurement of the seventh file dimension."},
	{rootName + "/" + NiftiHeader.VOX_OFFSET,
	 "Byte offset to the image data in a .nii file."},
	{rootName + "/" + NiftiHeader.SCL_SLOPE,
	 "Slope factor for scaling each voxel value."},
	{rootName + "/" + NiftiHeader.SCL_INTER,
	 "Offset for scaling each voxel value."},
	{rootName + "/" + NiftiHeader.SLICE_END,
	 "Last slice index."},
	{rootName + "/" + NiftiHeader.SLICE_CODE,
	 "Slice timing order."},
	{rootName + "/" + NiftiHeader.XYZT_UNITS,
	 "Units of pixDim[1..4]; contains spatial and temporal units."},
	{rootName + "/" + NiftiHeader.CAL_MAX,
	 "Maximum display intensity."},
	{rootName + "/" + NiftiHeader.CAL_MIN,
	 "Minimum display intensity."},
	{rootName + "/" + NiftiHeader.SLICE_DURATION,
	 "Time for one slice."},
	{rootName + "/" + NiftiHeader.T_OFFSET,
	 "Time axis shift."},
	{rootName + "/" + NiftiHeader.GL_MAX,
	 "Unused."},
	{rootName + "/" + NiftiHeader.GL_MIN,
	 "Unused."},
	{rootName + "/" + NiftiHeader.DESCRIP,
	 "Description."},
	{rootName + "/" + NiftiHeader.AUX_FILE,
	 "Auxiliary file name."},
	{rootName + "/" + NiftiHeader.QFORM_CODE,
	 "NIFTI_XFORM_* code."},
	{rootName + "/" + NiftiHeader.SFORM_CODE,
	 "NIFTI_XFORM_* code."},
	{rootName + "/" + NiftiHeader.QUATERN_B,
	 "Quaternion b parameter."},
	{rootName + "/" + NiftiHeader.QUATERN_C,
	 "Quaternion c parameter."},
	{rootName + "/" + NiftiHeader.QUATERN_D,
	 "Quaternion d parameter."},
	{rootName + "/" + NiftiHeader.QOFFSET_X,
	 "Quaternion x shift."},
	{rootName + "/" + NiftiHeader.QOFFSET_Y,
	 "Quaternion y shift."},
	{rootName + "/" + NiftiHeader.QOFFSET_Z,
	 "Quaternion z shift."},
	{rootName + "/" + NiftiHeader.SROW_X_1,
	 "Element in the first row and first column of the affine transform."},
	{rootName + "/" + NiftiHeader.SROW_X_2,
	 "Element in the first row and second column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_X_3,
	 "Element in the first row and third column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_X_4,
	 "Element in the first row and fourth column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Y_1,
	 "Element in the second row and first column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Y_2,
	 "Element in the second row and second column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Y_3,
	 "Element in the second row and third column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Y_4,
	 "Element in the second row and fourth column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Z_1,
	 "Element in the third row and first column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Z_2,
	 "Element in the third row and second column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Z_3,
	 "Element in the third row and third column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.SROW_Z_4,
	 "Element in the third row and fourth column of the affine " +
	 "transform."},
	{rootName + "/" + NiftiHeader.INTENT_NAME,
	 "Name or meaning of the voxel data."},
	{rootName + "/" + NiftiHeader.MAGIC,
	 "Magic number."}
      };

      return contents;
    }
}
