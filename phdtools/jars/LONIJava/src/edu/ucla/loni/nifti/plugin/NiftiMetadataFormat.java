/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.nifti.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import edu.ucla.loni.nifti.NiftiHeader;
import java.util.Vector;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * NIFTI Metadata class.
 *
 * @version 13 November 2006
 */
public class NiftiMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_nifti_1.0";

  /** Single instance of the NIFTI Metadata Format. */
  private static NiftiMetadataFormat _format = new NiftiMetadataFormat();

  /** Constructs a NIFTI Metadata Format. */
  private NiftiMetadataFormat()
    {
      // Create the root with no children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_EMPTY);

      // Add the attributes
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SIZEOF_HDR,
		   DATATYPE_INTEGER, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DATA_TYPE,
		   DATATYPE_STRING, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DB_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.EXTENTS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SESSION_ERROR,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.REGULARITY,
		   DATATYPE_INTEGER, true, null, "-128", "127", true, true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_INFO,
		   DATATYPE_INTEGER, true, null, "-128", "127", true, true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_0,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_1,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_2,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_3,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_4,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_5,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_6,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DIM_7,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.INTENT_P1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.INTENT_P2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.INTENT_P3,
		   DATATYPE_FLOAT, true, null);

      Vector acceptableValues = new Vector();
      acceptableValues.add("0");
      acceptableValues.add("2");
      acceptableValues.add("3");
      acceptableValues.add("4");
      acceptableValues.add("5");
      acceptableValues.add("6");
      acceptableValues.add("7");
      acceptableValues.add("8");
      acceptableValues.add("9");
      acceptableValues.add("10");
      acceptableValues.add("11");
      acceptableValues.add("12");
      acceptableValues.add("13");
      acceptableValues.add("14");
      acceptableValues.add("15");
      acceptableValues.add("16");
      acceptableValues.add("17");
      acceptableValues.add("18");
      acceptableValues.add("19");
      acceptableValues.add("20");
      acceptableValues.add("21");
      acceptableValues.add("22");
      acceptableValues.add("1001");
      acceptableValues.add("1002");
      acceptableValues.add("1003");
      acceptableValues.add("1004");
      acceptableValues.add("1005");
      acceptableValues.add("1006");
      acceptableValues.add("1007");
      acceptableValues.add("1008");
      acceptableValues.add("1009");
      acceptableValues.add("1010");
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.INTENT_CODE,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      acceptableValues = new Vector();
      acceptableValues.add("0");
      acceptableValues.add("1");
      acceptableValues.add("2");
      acceptableValues.add("4");
      acceptableValues.add("8");
      acceptableValues.add("16");
      acceptableValues.add("32");
      acceptableValues.add("64");
      acceptableValues.add("128");
      acceptableValues.add("255");
      acceptableValues.add("256");
      acceptableValues.add("512");
      acceptableValues.add("768");
      acceptableValues.add("1024");
      acceptableValues.add("1280");
      acceptableValues.add("1536");
      acceptableValues.add("1792");
      acceptableValues.add("2048");
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DATATYPE,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.BIT_PIX,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SLICE_START,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_0,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_5,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_6,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.PIX_DIM_7,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.VOX_OFFSET,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SCL_SLOPE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SCL_INTER,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SLICE_END,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,true);

      acceptableValues = new Vector();
      acceptableValues.add("0");
      acceptableValues.add("1");
      acceptableValues.add("2");
      acceptableValues.add("3");
      acceptableValues.add("4");
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SLICE_CODE,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.XYZT_UNITS,
		   DATATYPE_INTEGER, true, null, "-128", "127", true, true);

      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.CAL_MAX,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.CAL_MIN,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SLICE_DURATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.T_OFFSET,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.GL_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.GL_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.DESCRIP,
		   DATATYPE_STRING, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.AUX_FILE,
		   DATATYPE_STRING, true, null);

      acceptableValues = new Vector();
      acceptableValues.add("0");
      acceptableValues.add("1");
      acceptableValues.add("2");
      acceptableValues.add("3");
      acceptableValues.add("4");
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QFORM_CODE,
		   DATATYPE_INTEGER, true, null, acceptableValues);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SFORM_CODE,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QUATERN_B,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QUATERN_C,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QUATERN_D,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QOFFSET_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QOFFSET_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.QOFFSET_Z,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_X_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_X_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_X_3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_X_4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Y_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Y_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Y_3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Y_4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Z_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Z_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Z_3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.SROW_Z_4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.INTENT_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(NATIVE_METADATA_FORMAT_NAME, NiftiHeader.MAGIC,
		   DATATYPE_STRING, true, null);
    }

  /**
   * Gets an instance of the NIFTI Metadata Format.
   *
   * @return The single instance of the NIFTI Metadata Format.
   */
  public static NiftiMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
