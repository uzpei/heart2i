/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import edu.ucla.loni.jdicom.decoder.ByteSwappedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Decodes a DICOM set of Data Elements from a Siemens SV10 encoded stream.
 *
 * @version 28 August 2007
 */
public class SV10DataSetDecoder
{
    /** Decoded set of SV10 Data Elements. */
    private DataSet _dataSet;

    /**
     * Constructs an SV10 Data Set Decoder.
     *
     * @param value Bytes of the SV10 DICOM element value.
     *
     * @throws IOException If an I/O error occurs.
     */
    public SV10DataSetDecoder(byte[] value) throws IOException
    {
	// Initialize the DataSet
	_dataSet = new DataSet();

	// Verify the magic number "SV10" (4 bytes)
	if (value[0] != 'S' || value[1] != 'V' || value[2] != '1' ||
	    value[3] != '0')
	    {
		throw new IOException("Unknown magic number != SV10");
	    }

	// Skip '4 3 2 1' (4 bytes)

	// Read the number of encoded elements (4 bytes)
	ByteArrayInputStream baStream = new ByteArrayInputStream(value, 8, 4);
	ByteSwappedInputStream bsStream = new ByteSwappedInputStream(baStream);
	int nElems = bsStream.readInt(true);

	// Skip '77' (4 bytes)

	// Decode all the Data Elements
	baStream = new ByteArrayInputStream(value, 16, value.length-16);
	for (int i = 0; i < nElems; i++) {

	    // Decode the Data Element
	    SV10DataElementDecoder dr = new SV10DataElementDecoder(baStream);
	    DataElement elem = dr.getDataElement();

	    // Decode the ASCCONV text
	    if ( elem.getName().equals("MrPhoenixProtocol") ||
		 elem.getName().equals("MrProtocol") )
		{
		    Enumeration enu = elem.getValues();
		    if ( enu.hasMoreElements() ) {
			String ascconvText = (String)enu.nextElement();

			// Decode the ASCCONV text
			try {
			    AscconvDataElementDecoder decoder =
				new AscconvDataElementDecoder(ascconvText);

			    // Add the decoded Data Elements to a Data Set
			    DataSet ds = new DataSet();
			    ds.addDataElement(decoder.getPreAscconvElement());
			    ds.addDataElement(decoder.getAscconvElement());
			    ds.addDataElement(decoder.getPostAscconvElement());

			    // Replace the Data Element
			    elem = new DataElement(elem.getDataElementTag(),
						   "SQ", elem.getName());
			    elem.addDataSet(ds);
			}
			catch (Exception e) {}
		    }
		}

	    _dataSet.addDataElement(elem);

	    // Move to the beginning of the next encoded Data Element
	    int ptr = value.length - baStream.available();
	    while (value[ptr] == 0 || value[ptr] == -51) {
		ptr++;
		if (ptr >= value.length) { return; }
	    }

	    // Reset the stream
	    baStream = new ByteArrayInputStream(value, ptr, value.length-ptr);
	}
    }

    /**
     * Gets the decoded DataSet.
     *
     * @return DataSet decoded from the encoded stream.
     */
    public DataSet getDataSet()
    {
	return _dataSet;
    }
}
