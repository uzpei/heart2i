/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.plugin;

import edu.ucla.loni.Conversions;
import edu.ucla.loni.jdicom.DataDictionary;
import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import java.util.Enumeration;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between DICOM Data Sets and DOM Node
 * representations of them.
 *
 * @version 10 May 2007
 */
public class DicomMetadataConversions
{
  /** Constructs a DicomMetadataConversions. */
  private DicomMetadataConversions()
    {
    }

  /**
   * Converts the Data Set to a tree.
   *
   * @param rootName Name of the root Node of the tree.
   * @param dataSet Data Set to convert to a tree.
   *
   * @return DOM Node representing the Data Set.
   */
  public static Node toTree(String rootName, DataSet dataSet)
    {
      // Create the root Node
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      if (dataSet != null) {

	// Add the Data Set Node
	Node dataSetNode = _toTree(dataSet);
	rootNode.appendChild(dataSetNode);
      }

      // Return the root Node
      return rootNode;
    }

  /**
   * Converts the tree to a Data Set.
   *
   * @param root DOM Node representing the Data Set.
   *
   * @return Data Set constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static DataSet toDataSet(Node root) throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Return the primary Data Set
      return _toDataSet( root.getFirstChild() );
    }

  /**
   * Converts the Data Set to a tree.
   *
   * @param dataSet Data Set to convert to a tree.
   *
   * @return DOM Node representing the Data Set.
   */
  private static Node _toTree(DataSet dataSet)
    {
      // Create the Data Set Node
      String dataSetName = DicomMetadataFormat.DATA_SET_NAME;
      IIOMetadataNode dataSetNode = new IIOMetadataNode(dataSetName);

      // Add each Data Element as a child Node
      Enumeration enun = dataSet.getDataElements();
      while ( enun.hasMoreElements() ) {
	DataElement dataElement = (DataElement)enun.nextElement();

	// Create a Data Element Node from the Tag
	String tag = dataElement.getDataElementTag();
	IIOMetadataNode dataElementNode = new IIOMetadataNode(tag);
	dataSetNode.appendChild(dataElementNode);

	// Add the Value Representation as an attribute
	String vrName = DicomMetadataFormat.VR_NAME;
	String vr = dataElement.getValueRepresentation();
	dataElementNode.setAttribute(vrName, vr);

	// Add each Data Element value as a child Node
	Enumeration enumValues = dataElement.getValues();
	while ( enumValues.hasMoreElements() ) {
	  Object value = enumValues.nextElement();

	  // Add the Data Element value as a child Node
	  String valueNodeName = DicomMetadataFormat.VALUE_NODE_NAME;
	  IIOMetadataNode valueNode = new IIOMetadataNode(valueNodeName);
	  dataElementNode.appendChild(valueNode);

	  // If the value is a byte array, convert it to a Hex String
	  String stringValue;
	  if ( value.getClass().isArray() && value instanceof byte[] ) {
	    stringValue = Conversions.toHexString( (byte[])value );
	  }

	  // Otherwise convert it directly to a String
	  else { stringValue = value.toString(); }

	  // Add the String value as an attribute
	  String valueAttributeName = DicomMetadataFormat.VALUE_ATTRIBUTE_NAME;
	  valueNode.setAttribute(valueAttributeName, stringValue);
	}

	// Add each nested Data Set as a child Node
	Enumeration enumSets = dataElement.getDataSets();
	while ( enumSets.hasMoreElements() ) {
	  DataSet nestedDataSet = (DataSet)enumSets.nextElement();
	  dataElementNode.appendChild( _toTree(nestedDataSet) );
	}
      }

      // Return the Data Set Node
      return dataSetNode;
    }

  /**
   * Converts the tree to a Data Set.
   *
   * @param root DOM Node representing the Data Set.
   *
   * @return Data Set constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  private static DataSet _toDataSet(Node root) throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the Data Set Node name
      String dataSetName = DicomMetadataFormat.DATA_SET_NAME;
      if ( !root.getNodeName().equals(dataSetName) ) {
	String msg = "Root node must be named \"" + dataSetName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Create an empty Data Set
      DataSet dataSet = new DataSet();
      
      // Convert all the child Nodes to Data Elements
      NodeList childNodes = root.getChildNodes();
      for (int i = 0; i < childNodes.getLength(); i++) {
	DataElement dataElement = _toDataElement( childNodes.item(i) );
	dataSet.addDataElement(dataElement);
      }

      // Return the Data Set
      return dataSet;
    }

  /**
   * Converts the tree to a Data Element.
   *
   * @param root DOM Node representing the Data Element.
   *
   * @return Data Element constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  private static DataElement _toDataElement(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Attempt to construct a Data Element from the Node
      try {

	// Get the Tag from the Node name
	String tag = root.getNodeName();

	// Get the Value Representation from the Node attribute
	NamedNodeMap attributes = root.getAttributes();
	String vrName = DicomMetadataFormat.VR_NAME;
	String vr = attributes.getNamedItem(vrName).getNodeValue();

	// Get the Data Element name from the Data Dictionary
	DataDictionary dictionary = DataDictionary.getInstance();
	String name = dictionary.getName(tag);

	// Construct a Data Element
	DataElement dataElement = new DataElement(tag, vr, name);

	// Get the Values and Data Sets from the child Nodes
	NodeList childNodes = root.getChildNodes();
	for (int i = 0; i < childNodes.getLength(); i++) {
	  Node childNode = childNodes.item(i);

	  // Data Element has a Value
	  String childNodeName = childNode.getNodeName();
	  String valueNodeName = DicomMetadataFormat.VALUE_NODE_NAME;
	  if ( valueNodeName.equals(childNodeName) ) {

	    // Get the String Value from the Value attribute
	    NamedNodeMap atts = childNode.getAttributes();
	    String valueName = DicomMetadataFormat.VALUE_ATTRIBUTE_NAME;
	    String stringValue = atts.getNamedItem(valueName).getNodeValue();

	    // Convert the String Value to an Object and add it to the Element
	    Object objectValue = _convertToObject(vr, stringValue);
	    dataElement.addValue(objectValue);
	  }

	  // Data Element has a nested Data Set
	  String dataSetName = DicomMetadataFormat.DATA_SET_NAME;
	  if ( dataSetName.equals(childNodeName) ) {
	    DataSet dataSet = _toDataSet(childNode);
	    dataElement.addDataSet(dataSet);
	  }
	}

	// Return the Data Element
	return dataElement;
      }

      // Unable to construct a Data Element
      catch (Exception e) {
	String msg = "Unable to convert the DOM Node \"" + root.getNodeName() +
	             "\" to a Data Element.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the String value to an Object according to the specified Value
   * Representation.
   *
   * @param vr Value Representation that determines the type of Object.
   * @param value String value to convert into an Object.
   *
   * @return Object created from the String value.
   *
   * @throws NumberFormatException If the String value represents a number that
   *                               cannot be parsed.
   */
  private static Object _convertToObject(String vr, String value)
    {
      // Object is a String
      if ( vr.equals("AE") || vr.equals("AS") || vr.equals("AT") ||
	   vr.equals("CS") || vr.equals("DA") || vr.equals("DS") ||
	   vr.equals("DT") || vr.equals("IS") || vr.equals("LO") ||
	   vr.equals("LT") || vr.equals("PN") || vr.equals("SH") ||
	   vr.equals("ST") || vr.equals("TM") || vr.equals("UT") ||
	   vr.equals("UI") )
	{
	  return value;
	}

      // Object is a Double
      else if ( vr.equals("FD") ) { return Double.valueOf(value); }

      // Object is a Float
      else if ( vr.equals("FL") ) { return Float.valueOf(value); }

      // Object is an Integer
      else if ( vr.equals("SL") || vr.equals("US") ) {
	return Integer.valueOf(value);
      }

      // Object is a Short
      else if ( vr.equals("SS") ) { return Short.valueOf(value); }

      // Object is a Long
      else if ( vr.equals("UL") ) { return Long.valueOf(value); }

      // Object is a byte array
      else { return Conversions.toByteArray(value); }
    }
}
