/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.encoder;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Data Output Stream that includes the functionality of swapping bytes that
 * are written to the stream.
 *
 * @version 15 October 2002
 */
public class ByteSwappedOutputStream extends DataOutputStream
{
  /**
   * Constructs a ByteSwappedOutputStream for the Output Stream.
   *
   * @param outputStream Output Stream to write bytes to.
   */
  public ByteSwappedOutputStream(OutputStream outputStream)
    {
      super(outputStream);
    }

  /**
   * Writes <code>b.length</code> bytes to this output stream. 
   * 
   * @param b The buffer from which the data is written.
   * @param inc Number of bytes to swap the data by.
   *
   * @throws IOException If an I/O error occurs.
   */
  public synchronized void write(byte b[], int inc) throws IOException
    {
      write(b, 0, b.length, inc);
    }

  /**
   * Writes <code>len</code> bytes from the specified byte array 
   * starting at offset <code>off</code> to the underlying output stream. 
   * If no exception is thrown, the counter <code>written</code> is 
   * incremented by <code>len</code>.
   *
   * @param b The buffer from which the data is written.
   * @param off The start offset of the data.
   * @param len The number of bytes to write.
   * @param inc Number of bytes to swap the data by.
   *
   * @throws IOException If an I/O error occurs.
   */
  public synchronized void write(byte b[], int off, int len, int inc)
    throws IOException
    {
      // No byte swapping needed
      if (inc < 1) { out.write(b, off, len); }

      // Byte swapping needed
      else {

	// Make a copy of the buffer so it is not modified
	byte[] bCopy = new byte[len];
	System.arraycopy(b, off, bCopy, 0, len);

	// Byte swap the buffer copy
	_byteSwap(bCopy, 0, len, inc);

	// Write the byte swapped buffer copy and record the written byte count
	out.write(bCopy);
	_incCount(len);
      }
    }

  /**
   * Writes a <code>short</code> to the underlying output stream as two
   * bytes. If no exception is thrown, the counter <code>written</code> is
   * incremented by <code>2</code>.
   *
   * @param v The <code>short</code> to be written.
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeShort(int v, boolean byteSwap) throws IOException
    {
      // No byte swapping needed
      if (!byteSwap) { writeShort(v); }

      // Byte swap before writing
      else {

	// Get the two bytes swapped
	byte[] b = new byte[2];
	b[0] = (byte)((v >>> 0) & 0xFF);
	b[1] = (byte)((v >>> 8) & 0xFF);

	// Write the bytes
	write(b);
      }
    }

  /**
   * Writes an <code>int</code> to the underlying output stream as four
   * bytes. If no exception is thrown, the counter <code>written</code> is
   * incremented by <code>4</code>.
   *
   * @param v The <code>int</code> to be written.
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeInt(int v, boolean byteSwap) throws IOException
    {
      // No byte swapping needed
      if (!byteSwap) { writeInt(v); }	

      // Byte swap before writing
      else {

	// Get the four bytes swapped
	byte[] b = new byte[4];
	b[0] = (byte)((v >>>  0) & 0xFF);
	b[1] = (byte)((v >>>  8) & 0xFF);
	b[2] = (byte)((v >>> 16) & 0xFF);
	b[3] = (byte)((v >>> 24) & 0xFF);

	// Write the bytes
	write(b);
      }
    }

  /**
   * Writes a <code>long</code> to the underlying output stream as eight
   * bytes. In no exception is thrown, the counter <code>written</code> is
   * incremented by <code>8</code>.
   *
   * @param v The <code>long</code> to be written.
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeLong(long v, boolean byteSwap) throws IOException
    {
      // No byte swapping needed
      if (!byteSwap) { writeLong(v); }	

      // Byte swap before writing
      else {

	// Get the eight bytes swapped
	byte[] b = new byte[8];
	b[0] = (byte)((v >>>  0) & 0xFF);
	b[1] = (byte)((v >>>  8) & 0xFF);
	b[2] = (byte)((v >>> 16) & 0xFF);
	b[3] = (byte)((v >>> 24) & 0xFF);
	b[4] = (byte)((v >>> 32) & 0xFF);
	b[5] = (byte)((v >>> 40) & 0xFF);
	b[6] = (byte)((v >>> 48) & 0xFF);
	b[7] = (byte)((v >>> 56) & 0xFF);

	// Write the bytes
	write(b);
      }
    }

  /**
   * Converts the float argument to an <code>int</code> using the 
   * <code>floatToIntBits</code> method in class <code>Float</code>, 
   * and then writes that <code>int</code> value to the underlying 
   * output stream as a 4-byte quantity. If no exception is thrown, the
   * counter <code>written</code> is incremented by <code>4</code>.
   *
   * @param v The <code>float</code> value to be written.
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeFloat(float v, boolean byteSwap) throws IOException
    {
      // No byte swapping needed
      if (!byteSwap) { writeFloat(v); }

      else { writeInt(Float.floatToIntBits(v), true); }
    }

  /**
   * Converts the double argument to a <code>long</code> using the 
   * <code>doubleToLongBits</code> method in class <code>Double</code>, 
   * and then writes that <code>long</code> value to the underlying 
   * output stream as an 8-byte quantity. If no exception is thrown, the
   * counter <code>written</code> is incremented by <code>8</code>.
   *
   * @param v The <code>double</code> value to be written.
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeDouble(double v, boolean byteSwap) throws IOException
    {
      // No byte swapping needed
      if (!byteSwap) { writeDouble(v); }

      else { writeLong(Double.doubleToLongBits(v), true); }
    }

  /**
   * Writes the four output bytes of the Data Element tag.
   *
   * @param tag 8 digit hexadecimal value to be written.
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @throws IOException If an I/O error occurs.
   * @throws NumberFormatException If the tag cannot be converted to an
   *                               integer.
   */
  public void writeDataElementTag(String tag, boolean byteSwap)
    throws IOException
    {
      // Convert the tag to an integer
      int v = (int)Long.parseLong(tag, 16);

      // No byte swapping needed
      if (!byteSwap) { writeInt(v); }

      // Byte swap before writing
      else {

	// Get the four bytes and swap every two
	byte[] b = new byte[4];
	b[0] = (byte)((v >>> 16) & 0xFF);
	b[1] = (byte)((v >>> 24) & 0xFF);
	b[2] = (byte)((v >>>  0) & 0xFF);
	b[3] = (byte)((v >>>  8) & 0xFF);

	// Write the bytes
	write(b);
      }
    }

  /**
   * Byte swaps the data of the given length in the specified array starting
   * at the given offset and by swapping the given number of bytes.
   *
   * @param b Buffer containing the data.
   * @param off Offset from the beginning of the buffer to where the data
   *            starts.
   * @param len Length of the data in the buffer.
   * @param inc Number of bytes to swap the data by.
   */
  private void _byteSwap(byte[] b, int off, int len, int inc)
    {
      // Byte swapping is needed
      if (len >= inc && inc > 1) {

	// Only byte swap data which evenly divides by inc
	int dataLength = len - len%inc;

	// Byte swap the data in blocks of size equal to inc
	for (int block = off; block < off+dataLength; block += inc) {

	  // Byte swap the contents of each data block
	  for (int j = 0; j < inc/2; j++) {

	    // Swap the byte order
	    byte temp = b[block+j];
	    b[block+j] = b[block+inc-j-1];
	    b[block+inc-j-1] = temp;
	  }
	}
      }
    }

  /**
   * Increases the written counter by the specified value until it reaches
   * Integer.MAX_VALUE.
   *
   * @param value Number of bytes to increase the written counter by.
   */
  private void _incCount(int value)
    {
      int temp = written + value;
      if (temp < 0) {
	temp = Integer.MAX_VALUE;
      }
      written = temp;
    }
}
