/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

/**
 * Class that represents a node in an ASCCONV tree.
 *
 * @version 24 August 2007
 */
public class AscconvNode
{
    /** Name of the ASCCONV Node. */
    private String _name;

    /**
     * Constructs an ASCCONV Node.
     *
     * @param name Name of the ASCCONV Node.
     */
    protected AscconvNode(String name)
    {
	_name = name;
    }

    /**
     * Gets the name of the ASCCONV Node.
     *
     * @return Name of the ASCCONV Node.
     */
    public String getName()
    {
	return _name;
    }
}
