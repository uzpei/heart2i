/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder;

import edu.ucla.loni.jdicom.DataSet;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;
import java.util.Enumeration;

/**
 * Decodes the Values of a DICOM Data Element from an encoded stream.
 *
 * @version 15 October 2007
 */
public class ValueFieldDecoder
{
  /**
   * Decoded Values of a DICOM Data Element.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 7: </b>
   * <em>
   * VALUE: A component of a Value Field. A Value Field may consist of one
   * or more of these components.
   * </em>
   */
  private Vector _values = new Vector(1, 1);

  /** Length (in bytes) of the encoded Value Field. */
  private int _valueFieldLength;

  /**
   * Constructs a ValueFieldDecoder which reads the given number of encoded
   * data bytes from the specified MeasuredInputStream.  The specified
   * Value Representation indicates how to decode the encoded bytes.
   * The given DataElements are those which have already been decoded from
   * the stream.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT FIELDS 
   *     [Section 7.1.1], Page 26: </b>
   * <em>
   * Value Field: An even number of bytes containing the Value(s) of the
   * Data Element.  The data type of Value(s) stored in this field is
   * specified by the Data Element's VR.
   * </em>
   * @param stream MeasuredInputStream to read encoded data from.
   * @param valueFieldLength Length (in bytes) of the encoded Value Field.
   * @param valueRepresentation Value Representation indicating how the
   *                            Values in the Value Field are encoded.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   * @param dataElements DataElements which have already been decoded from
   *                     the stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  public ValueFieldDecoder(MeasuredInputStream stream, int valueFieldLength,
			   String valueRepresentation, int byteOrdering,
			   Vector dataElements)
    throws IOException
    {
      _valueFieldLength = valueFieldLength;

      // Decode a multiple-valued Value Field of strings
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Pages 15 - 21
      String vr = valueRepresentation;
      if ( vr.equals("AE") || vr.equals("AS") || vr.equals("CS") ||
	   vr.equals("DA") || vr.equals("DS") || vr.equals("DT") ||
	   vr.equals("IS") || vr.equals("LO") || vr.equals("PN") ||
	   vr.equals("SH") || vr.equals("TM") || vr.equals("UI") )
	{
	  // Use the backslash as the delimiter
	  //
	  // PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
	  // [Section 6.4], Page 23:
	  // When a Data Element has multiple Values, those Values shall be
	  // delimited as follows:  For character strings, the character
	  // 5CH (BACKSLASH "\" in the case of the repertoire ISO IR-6) shall
	  // be used as a delimiter between Values.
	  _decodeStringValues( new DataInputStream(stream), '\\' );

	  // Remove any NULL padding from the last UI encoded Value
	  //
	  // PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
	  // [Section 6.4], Page 23:
	  // Only the last UID Value in a multiple valued Data Element with
	  // a VR of UI shall be padded with a single trailing NULL (00H)
	  // character when necessary to ensure that the entire Value Field
	  // (including "\" delimiters) is of even length.
	  if ( vr.equals("UI") && !_values.isEmpty() ) {
	    String lastValue = (String)_values.lastElement();
	    String newLastValue = lastValue.replace( (char)0, ' ').trim();

	    // Replace the old last Value with the new last Value
	    _values.set( _values.size()-1, newLastValue );
	  }
	}

      // Decode a single-valued Value Field of strings
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Pages 15 - 21
      else if ( vr.equals("LT") || vr.equals("ST") || vr.equals("UT") )
	{
	  // Delimiter is the end of the string
	  _decodeStringValues( new DataInputStream(stream), '\0' );
	}

      // Decode a multiple-valued Value Field of attribute tags
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 15
      else if ( vr.equals("AT") )
	{
	  _decodeTagValues( new ByteSwappedInputStream(stream),
			    byteOrdering );
	}

      // Decode a multiple-valued Value Field of doubles
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 16
      else if ( vr.equals("FD") )
	{
	  _decodeDoubleValues( new ByteSwappedInputStream(stream),
			       byteOrdering );
	}

      // Decode a multiple-valued Value Field of floats
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 16
      else if ( vr.equals("FL") )
	{
	  _decodeFloatValues( new ByteSwappedInputStream(stream),
			      byteOrdering );
	}

      // Decode a multiple-valued Value Field of signed longs
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 19
      else if ( vr.equals("SL") )
	{
	  _decodeSignedLongValues( new ByteSwappedInputStream(stream),
				   byteOrdering );
	}

      // Decode a multiple-valued Value Field of signed shorts
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 19
      else if ( vr.equals("SS") )
	{
	  _decodeSignedShortValues( new ByteSwappedInputStream(stream),
				    byteOrdering );
	}

      // Decode a multiple-valued Value Field of unsigned longs
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 20
      else if ( vr.equals("UL") )
	{
	  _decodeUnsignedLongValues( new ByteSwappedInputStream(stream),
				     byteOrdering );
	}

      // Decode a multiple-valued Value Field of unsigned shorts
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 21

      else if ( vr.equals("US") )
	{
	  _decodeUnsignedShortValues( new ByteSwappedInputStream(stream),
				      byteOrdering );
	}

      // Decode a multiple-valued Value Field of Items, where each Item
      // contains a DataSet
      //
      // PS 3.5-1998, NESTING OF DATA SETS
      // [Section 7.5], Page 29
      // The VR identified "SQ" shall be used for Data Elements with a Value
      // consisting of a Sequence of zero or more Items, where each Item
      // contains a set of Data Elements.
      else if ( vr.equals("SQ") )
	{
	  _decodeDataSetValues(stream, byteOrdering, dataElements);
	}

      // Decode a multiple-valued Value Field of Items, where each Item
      // contains a Pixel Data Stream Fragment
      //
      // PS 3.5-1998, TRANSFER SYNTAXES FOR ENCAPSULATION OF ENCODED PIXEL DATA
      // [Section A.4], Page 44
      // Data Element (7FE0,0010) Pixel Data has the Value Representation OB
      // and is a sequence of bytes resulting from one of the encoding
      // processes. It contains the encoded pixel data stream fragmented into
      // one or more Item(s).
      // The Length of the Data Element (7FE0,0010) shall be set to the Value
      // for Undefined Length (FFFFFFFFH).
      else if ( vr.equals("OB") && _valueFieldLength == 0xFFFFFFFF )
	{
	  _decodeFragmentValues(stream, byteOrdering);
	}

      // Read a single-valued Value Field of unrecognized type into an array
      //
      // PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
      // [Section 6.4], Page 23
      // Data Elements with a VR of SQ, OW, OB or UN shall always have a
      // Value Multiplicity of one.
      else
	{
	  ByteSwappedInputStream bStream = new ByteSwappedInputStream(stream);

	  if (_valueFieldLength >= 0) {

	    // Create a byte array
	    int numberOfSwappedBytes = 0;
	    byte[] value = new byte[_valueFieldLength];

	    // Swap the bytes if necessary
	    //
	    // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
	    // [Table 6.2-1], Page 17
	    if ( vr.equals("OW") &&
		 byteOrdering == DataElementDecoder.LITTLE_ENDIAN)
	      {
		numberOfSwappedBytes = 2;
	      }

	    // Read the bytes and save the value
	    bStream.readFully(value, numberOfSwappedBytes);
	    _values.addElement(value);
	  }

	  // Unable to skip over the encoded Value Field
	  else {
	    String message = "ValueFieldDecoder: can't decode the data " +
	      "element of VR = " + vr + " after the data element \n" +
	      dataElements.lastElement() + "because it has a negative " +
	      "Value Field Length.";

	    throw new IllegalArgumentException(message);
	  }
	}
    }

  /**
   * Gets the decoded Values.
   *
   * @return Values decoded from the encoded stream.
   */
  public Enumeration getValues()
    {
      return _values.elements();
    }

  /**
   * Decodes string Values from an encoded Value Field which are separated
   * by the given character delimiter.
   * <p>
   * <b> PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
   *     [Section 7.3], Page 28: </b>
   * <em>
   * In a character string consisting of multiple 8-bit single byte
   * codes, the characters will be encoded in the order of occurrence
   * in the string (left to right).
   * </em>
   *
   * @param dStream DataInputStream to read encoded data from.
   * @param delimiter Character which separates encoded string Values in the
   *                  Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeStringValues(DataInputStream dStream, char delimiter)
    throws IOException
    {
      // Read the encoded Value Field
      byte[] valueField = new byte[_valueFieldLength];
      dStream.readFully(valueField);

      // Examine each character of the Value Field
      StringBuffer buffer = new StringBuffer();
      for (int i = 0; i < _valueFieldLength; i++) {
	char currentCharacter = (char)valueField[i];

	// If the current character is the delimiter, save the current buffer
	// and restart another one
	if (currentCharacter == delimiter) {
	  _values.addElement( buffer.toString().replace('\0',' ') );
	  buffer = new StringBuffer();
	}

	// Otherwise append it to the buffer
	else { buffer.append(currentCharacter); }
      }

      // Save the last buffer (remove null characters)
      _values.addElement( buffer.toString().replace('\0',' ') );
    }

  /**
   * Decodes attribute tag Values from an encoded Value Field with the given
   * byte ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 15: </b>
   * <em>
   * AT Attribute Tag - Ordered pair of 16-bit unsigned integers that is the
   * value of a Data Element Tag.  4 bytes fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeTagValues(ByteSwappedInputStream bStream, 
				int byteOrdering) throws IOException
    {
      // Read every 4 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=4) {

	// Read the tag and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	String tag = bStream.readDataElementTag(byteOrdering==littleEndian);

	// Save the tag
	_values.addElement(tag);
      }
    }

  /**
   * Decodes double Values from an encoded Value Field with the given byte
   * ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 16: </b>
   * <em>
   * FD Floating Point Double - Double precision binary floating point number
   * represented in IEEE 754:1985 64-bit Floating Point Number Format.  8
   * bytes fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeDoubleValues(ByteSwappedInputStream bStream, 
				   int byteOrdering) throws IOException
    {
      // Read every 8 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=8) {

	// Read the double and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	double value = bStream.readDouble(byteOrdering==littleEndian);

	// Save the value
	_values.addElement( new Double(value) );
      }
    }

  /**
   * Decodes float Values from an encoded Value Field with the given byte
   * ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 16: </b>
   * <em>
   * FL Floating Point Single - Single precision binary floating point number
   * represented in IEEE 754:1985 32-bit Floating Point Number Format.  4
   * bytes fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeFloatValues(ByteSwappedInputStream bStream, 
				  int byteOrdering) throws IOException
    {
      // Read every 4 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=4) {

	// Read the float and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	float value = bStream.readFloat(byteOrdering==littleEndian);

	// Save the value
	_values.addElement( new Float(value) );
      }
    }

  /**
   * Decodes signed long Values from an encoded Value Field with the given byte
   * ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 19: </b>
   * <em>
   * SL Signed Long - Signed binary integer 32 bits long.  Represents an
   * integer, n, in the range:  - (2^31 - 1 ) <= n <= (2^31 - 1).  4 bytes
   * fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeSignedLongValues(ByteSwappedInputStream bStream, 
				       int byteOrdering) throws IOException
    {
      // Read every 4 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=4) {

	// Read the integer and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	int value = bStream.readInt(byteOrdering==littleEndian);

	// Save the value
	_values.addElement( new Integer(value) );
      }
    }

  /**
   * Decodes signed short Values from an encoded Value Field with the given
   * byte ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 19: </b>
   * <em>
   * SS Signed Short - Signed binary integer 16 bits long in 2's complement
   * form. Represents an integer n in the range:  - (2^15 - 1 ) <= n <=
   * (2^15 - 1).  2 bytes fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeSignedShortValues(ByteSwappedInputStream bStream, 
					int byteOrdering) throws IOException
    {
      // Read every 2 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=2) {

	// Read the short and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	short value = bStream.readShort(byteOrdering==littleEndian);

	// Save the value
	_values.addElement( new Short(value) );
      }
    }

  /**
   * Decodes unsigned long Values from an encoded Value Field with the given
   * byte ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 20: </b>
   * <em>
   * UL Unsigned Long - Unsigned binary integer 32 bits long.  Represents an
   * integer n in the range:  0 <= n < 2^32.  4 bytes fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeUnsignedLongValues(ByteSwappedInputStream bStream, 
					 int byteOrdering) throws IOException
    {
      // Read every 4 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=4) {

	// Read the unsigned integer and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	long value = bStream.readUnsignedInt(byteOrdering==littleEndian);

	// Save the value
	_values.addElement( new Long(value) );
      }
    }

  /**
   * Decodes unsigned short Values from an encoded Value Field with the given
   * byte ordering.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 21: </b>
   * <em>
   * US Unsigned Short - Unsigned binary integer 16 bits long.  Represents
   * integer n in the range:  0 <= n < 2^16.  2 bytes fixed.
   * </em>
   * 
   * @param bStream ByteSwappedInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeUnsignedShortValues(ByteSwappedInputStream bStream, 
					  int byteOrdering) throws IOException
    {
      // Read every 2 bytes of the encoded Value Field
      for (int i = 0; i < _valueFieldLength; i+=2) {

	// Read the unsigned short and swap the bytes if necessary
	int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
	int value = bStream.readUnsignedShort(byteOrdering==littleEndian);

	// Save the value
	_values.addElement( new Integer(value) );
      }
    }

  /**
   * Decodes DataSet Values from an encoded Value Field with the given byte
   * ordering.
   * <p>
   * <b> PS 3.5-1998, ITEM ENCODING RULES
   *     [Section 7.5.1], Page 30: </b>
   * <em>
   * Each Item of a Data Element of Value Representation SQ shall be encoded
   * as a DICOM Standard Data Element with a specific Data Element Tag of
   * Value (FFFE,E000). The Item Tag is followed by a 4 byte Item Length
   * field.
   * <p>
   * Each Item Value shall contain a DICOM Data Set composed of Data Elements.
   * </em>
   * 
   * @param stream MeasuredInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   * @param dataElements DataElements which have already been decoded from
   *                     the stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeDataSetValues(MeasuredInputStream stream, 
				    int byteOrdering, Vector dataElements)
    throws IOException
    {
      // NOTE:  encoded Value Field only contains one sequence of Items
      //
      // PS 3.5-1998, NESTING OF DATA SETS
      // [Section 7.5], Page 30:
      // Data Elements with a VR of SQ may contain multiple Items but shall
      // always have a Value Multiplicity of one (ie. a single Sequence).

      int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
      ByteSwappedInputStream bStream = new ByteSwappedInputStream(stream);

      // Decode all the Items of the encoded Value Field
      long startingOffset = stream.getOffset();
      while (true) {

	// Check number of bytes read against the encoded Value Field length
	// to determine the end of the Sequence
	//
	// PS 3.5-1998, DELIMITATION OF THE SEQUENCE OF ITEMS
	// [Section 7.5.2], Page 30:
	// Explicit Length: The number of bytes (even) contained in the Data
	// Element Value (following but not including the Data Element Length
	// Field) is encoded as a 32-bit unsigned integer value (see
	// Section 7.1). This length shall include the total length resulting
	// from the sequence of zero or more items conveyed by this Data
	// Element. This Data Element Length shall be equal to 00000000H if
	// the sequence of Items contains zero Items.
	long endOfValueFieldOffset = startingOffset + _valueFieldLength;
	if (_valueFieldLength >= 0 &&
	    stream.getOffset() >= endOfValueFieldOffset)
	  {
	    return;
	  }

	// Decode the next tag; this can be an Item tag or a Sequence
	// Delimitation Item tag
	String tag = bStream.readDataElementTag(byteOrdering==littleEndian);

	// Decode the next length; this can be the length of an Item Value
	// or the length associated with a Sequence Delimitation Item tag
	int length = bStream.readInt(byteOrdering==littleEndian);

	// Sequence Delimitation Item tag means the end of the Value Field
	//
	// PS 3.5-1998, DELIMITATION OF THE SEQUENCE OF ITEMS
	// [Section 7.5.2], Page 30:
	// Undefined Length: The Data Element Length Field shall contain a
	// Value FFFFFFFFH to indicate an Undefined Sequence length. It shall
	// be used in conjunction with a Sequence Delimitation Item. A
	// Sequence Delimitation Item shall be included after the last Item
	// in the sequence. Its Item Tag shall be (FFFE,E0DD) with an Item
	// Length of 00000000H. No Value shall be present.
	if ( _valueFieldLength == 0xFFFFFFFF && tag.equals("FFFEE0DD") ) {
	  return;
	}

	// Decode the Item Value Field if its length is known
	//
	// PS 3.5-1998, ITEM ENCODING RULES
	// [Section 7.5.1], Page 30:
	// Explicit Length: The number of bytes (even) contained in the
	// Sequence Item Value (following but not including the Item Length
	// Field) is encoded as a 32-bit unsigned integer value (see
	// Section 7.1). This length shall include the total length of all
	// Data Elements conveyed by this Item. This Item Length shall be
	// equal to 00000000H if the Item contains no Data Set.
	if (length > 0) {
	  DataSetDecoder decoder = new DataSetDecoder(stream, dataElements,
						      length);
	  
	  // Save the DataSet as a Value from the Value Field
	  _values.addElement( decoder.getDataSet() );
	}

	// Create an empty Data Set if the Item is empty
	if (length == 0) { _values.addElement( new DataSet() ); }

	// Decode the Item Value Field if its length is not known
	//
	// PS 3.5-1998, ITEM ENCODING RULES
	// [Section 7.5.1], Page 30:
	// Undefined Length: The Item Length Field shall contain the value
	// FFFFFFFFH to indicate an undefined Item length. It shall be used
	// in conjunction with an Item Delimitation Data Element.  This Item
	// Delimitation Data Element has a Data Element Tag of (FFFE,E00D)
	// and shall follow the Data Elements encapsulated in the Item. No
	// Value shall be present in the Item Delimitation Data Element and
	// its Length shall be 00000000H.
	if (length == 0xFFFFFFFF) {
	  DataSetDecoder decoder = new DataSetDecoder(stream, dataElements,
						      "FFFEE00D");

	  // Save the DataSet as a Value from the Value Field
	  _values.addElement( decoder.getDataSet() );
	}
      }
    }

  /**
   * Decodes Fragment Values from an encoded Value Field.
   * <p>
   * <b> PS 3.5-1998, TRANSFER SYNTAXES FOR ENCAPSULATION OF ENCODED PIXEL DATA
   *     [Section A.4], Page 44: </b>
   * <em>
   * Each Data Stream Fragment encoded according to the specific encoding
   * process shall be encapsulated as a DICOM Item with a specific Data
   * Element Tag of Value (FFFE,E000). The Item Tag is followed by a 4 byte
   * Item Length field encoding the explicit number of bytes of the Item.
   * </em>
   *
   * @param stream MeasuredInputStream to read encoded data from.
   * @param byteOrdering Byte ordering of the encoded Value Field.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _decodeFragmentValues(MeasuredInputStream stream,
				     int byteOrdering)
    throws IOException
    {
      // NOTE:  OB encoded Value Field only contains one sequence of Items
      //
      // PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
      // [Section 6.4], Page 23
      // Data Elements with a VR of SQ, OW, OB or UN shall always have a
      // Value Multiplicity of one.

      int littleEndian = DataElementDecoder.LITTLE_ENDIAN;
      ByteSwappedInputStream bStream = new ByteSwappedInputStream(stream);

      // Decode all the Items of the encoded Value Field
      boolean haveReachedBasicOffsetTableItem = false;
      while (true) {

	// Decode the next tag; this can be an Item tag or a Sequence
	// Delimitation Item tag
	String tag = bStream.readDataElementTag(byteOrdering==littleEndian);

	// Decode the next length; this can be the length of an Item Value
	// or the length associated with a Sequence Delimitation Item tag
	int length = bStream.readInt(byteOrdering==littleEndian);

	// Sequence Delimitation Item tag means the end of the Value Field
	//
	// PS 3.5-1998, TRANSFER SYNTAXES FOR ENCAPSULATION OF ENCODED PIXEL
	// DATA [Section A.4], Page 45:
	// This Sequence of Items is terminated by a Sequence Delimiter Item
	// with the Tag (FFFE,E0DD) and an Item Length Field of Value
	// (00000000H) (i.e., no Value Field shall be present).
	if ( tag.equals("FFFEE0DD") ) { return; }

	// Throw away the Basic Offset Table Item
	//
	// PS 3.5-1998, TRANSFER SYNTAXES FOR ENCAPSULATION OF ENCODED PIXEL
	// DATA [Section A.4], Page 45:
	// The first Item in the Sequence of Items before the encoded Pixel
	// Data Stream shall be a Basic Offset Table item.
	if (!haveReachedBasicOffsetTableItem) {
	  haveReachedBasicOffsetTableItem = true;

	  // Skip over the Basic Offset Table Item
	  bStream.skipFully(length);
	}

	else {

	  // Create a byte array (no byte swapping) and save the value
	  byte[] value = new byte[length];
	  bStream.readFully(value, 0);
	  _values.addElement(value);
	}
      }
    }
}
