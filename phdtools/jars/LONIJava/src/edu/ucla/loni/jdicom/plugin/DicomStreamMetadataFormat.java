/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.plugin;

/**
 * DICOM Metadata Format that describes the tree structure of metadata in the
 * DICOM Stream Metadata class.
 *
 * @version 24 August 2007
 */
public class DicomStreamMetadataFormat extends DicomMetadataFormat
{
    /** Name of the native metadata format. */
    public static final String NATIVE_METADATA_FORMAT_NAME =
	"edu_ucla_loni_jdicom_stream_1.0";

    /** Name of the version 1.1 metadata format. */
    public static final String METADATA_1_1_FORMAT_NAME =
	"edu_ucla_loni_jdicom_stream_1.1";

    /** Single instance of the DicomStreamMetadataFormat. */
    private static DicomStreamMetadataFormat _format =
	new DicomStreamMetadataFormat();

    /** Constructs a DicomStreamMMetadataFormat. */
    private DicomStreamMetadataFormat()
    {
	super(NATIVE_METADATA_FORMAT_NAME);
    }

    /**
     * Gets an instance of the DicomStreamMetadataFormat.
     *
     * @return The single instance of the DicomStreamMetadataFormat.
     */
    public static DicomStreamMetadataFormat getInstance()
    {
	return _format;
    }
}
