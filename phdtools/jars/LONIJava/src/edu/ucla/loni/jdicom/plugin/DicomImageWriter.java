/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.plugin;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.spi.ImageWriterSpi;
import org.w3c.dom.Node;

/**
 * Image Writer for parsing and encoding part 10 DICOM images.
 *
 * @version 10 May 2007
 */
public class DicomImageWriter extends ImageWriter
{
    /** Data Set of the image sequence currently being written. */
    private DataSet _dataSet;

    /** Output stream of the image sequence currently being written. */
    private DicomImageSequenceOutputStream _outputStream;

    /** Image metadata of the image sequence currently being written. */
    private Vector _imageMetadata;

    /** Number of images in the sequence currently being written. */
    private int _numberOfImagesWritten;

    /**
     * Constructs a DicomImageWriter.
     *
     * @param originatingProvider The ImageWriterSpi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not a
     *                                  DICOM Image Writer Spi.
     */
    public DicomImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be a DICOM Image Writer Spi
	if ( !(originatingProvider instanceof DicomImageWriterSpi) ) {
	    String msg = "Originating provider must a DICOM Image Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank stream metadata object
	String name = DicomStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	String className = DicomStreamMetadataFormat.class.getName();
	return new DicomMetadata(name, className, null);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding an
     * image of the given type.
     *
     * @param imageType An ImageTypeSpecifier indicating the format of the image
     *                  to be written later.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					       ImageWriteParam param)
    {
	// Return a blank image metadata object
	String name = DicomImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	String className = DicomImageMetadataFormat.class.getName();
	return new DicomMetadata(name, className, null);
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing stream metadata, used to
     *               initialize the state of the returned object.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize Dicom Metadata
	if (inData instanceof DicomMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing image metadata, used to
     *               initialize the state of the returned object.
     * @param imageType An ImageTypeSpecifier indicating the layout and color
     *                  information of the image with which the metadata will be
     *                  associated.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If either of inData or imageType is
     *                                  null.
     */
    public IIOMetadata convertImageMetadata(IIOMetadata inData,
					    ImageTypeSpecifier imageType,
					    ImageWriteParam param)
    {
	// Only recognize Dicom Metadata
	if (inData instanceof DicomMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns true if the methods that take an IIOImage parameter are capable
     * of dealing with a Raster (as opposed to RenderedImage) source image.
     *
     * @return True if Raster sources are supported.
     */
    public boolean canWriteRasters()
    {
	return true;
    }

    /**
     * Appends a complete image stream containing a single image and associated
     * stream and image metadata and thumbnails to the output.
     *
     * @param streamMetadata An IIOMetadata object representing stream metadata,
     *                       or null to use default values.
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam, or null to use a default
     *              ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, if the stream
     *                                  metadata cannot be converted, or if the
     *                                  image type is unrecognized.
     * @throws IllegalStateException If the output has not been set.
     */
    public void write(IIOMetadata streamMetadata, IIOImage image,
		      ImageWriteParam param) throws IOException
    {
	// Write a sequence with 1 image
	prepareWriteSequence(streamMetadata);
	writeToSequence(image, param);
	endWriteSequence();
    }

    /**
     * Returns true if the writer is able to append an image to an image
     * stream that already contains header information and possibly prior
     * images.
     *
     * @return True If images may be appended sequentially.
     */
    public boolean canWriteSequence()
    {
	return true;
    }

    /**
     * Prepares a stream to accept a series of subsequent writeToSequence calls,
     * using the provided stream metadata object.  The metadata will be written
     * to the stream if it should precede the image data.
     *
     * @param streamMetadata A stream metadata object.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the stream metadata is null or cannot
     *                                  be converted.
     * @throws IllegalStateException If the output has not been set.
     */
    public void prepareWriteSequence(IIOMetadata streamMetadata)
	throws IOException
    {
	// Convert the metadata to DICOM Metadata
	IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the stream metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct a Data Set from the metadata
	try {
	    String name = DicomStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	    Node tree = metadata.getAsTree(name);
	    _dataSet = DicomMetadataConversions.toDataSet(tree);
	}
      
	catch (Exception e) {
	    String msg="Unable to construct a Data Set from the metadata tree.";
	    IllegalArgumentException excep = new IllegalArgumentException(msg);
	    excep.initCause(e);
	    throw excep;
	}

	// For MR images, reset the image metadata Element
	if ( _getStringValue("00080060", _dataSet).equals("MR") ) {

	    // Remove any existing image metadata Element
	    _dataSet.removeDataElement("52009230");

	    // Create and add the Element with an empty item
	    DataElement dataElement = _getImageMetadataElement( new Vector() );
	    _dataSet.addDataElement(dataElement);
	}

	// Create a new stream for the image sequence
	String tSyntax = _getStringValue("00020010", _dataSet);
	ImageOutputStream ioStream = _getOutputStream();
	_outputStream = new DicomImageSequenceOutputStream(ioStream, tSyntax);

	// Write the sequence header
	_outputStream.writeSequenceHeader(_dataSet);

	_imageMetadata = new Vector();
	_numberOfImagesWritten = 0;
    }

    /**
     * Appends a single image and possibly associated metadata and thumbnails,
     * to the output.  The supplied ImageReadParam is ignored.
     *
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam or null to use a default ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null or if the image
     *                                  type is not recognized.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void writeToSequence(IIOImage image, ImageWriteParam param)
	throws IOException
    {
	// Check for the Data Set
	if (_dataSet == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Check for a null image
	if (image == null) {
	    String msg = "Cannot write a null image.";
	    throw new IllegalArgumentException(msg);
	}

	// Store the image metadata
	_imageMetadata.addElement( image.getMetadata() );

	// Get a Data Buffer from the image
	Raster raster = image.getRaster();
	if (raster == null) {
	    RenderedImage renderedImage = image.getRenderedImage();

	    // If the Rendered Image is a Buffered Image, get it Raster directly
	    if (renderedImage instanceof BufferedImage) {
		raster = ((BufferedImage)renderedImage).getRaster();
	    }

	    // Otherwise get a copy of the Raster from the Rendered Image
	    raster = renderedImage.getData();
	}
	DataBuffer dataBuffer = raster.getDataBuffer();

	// Update the Listeners
	processImageStarted(_numberOfImagesWritten);

	// First image of the sequence
	if (_numberOfImagesWritten == 0) {

	    // Determine and set the VR
	    String vr = "OB";
	    if (dataBuffer instanceof DataBufferByte) {
		int imageWidth = _getIntValue("00280011", _dataSet);
		int imageHeight = _getIntValue("00280010", _dataSet);
		int imageSize = imageWidth*imageHeight;

		// Byte data corresponds to more than 1 byte per pixel
		if (imageSize > 0 && dataBuffer.getSize()/imageSize > 1) {
		    vr = "OW";
		}
	    }
	    else { vr = "OW"; }
	    _outputStream.setValueRepresentation(vr);

	    // Write the pixel data header with the length of one image
	    int bytesPerPixel = 1;
	    if (dataBuffer instanceof DataBufferUShort) { bytesPerPixel = 2; }
	    if (dataBuffer instanceof DataBufferInt) { bytesPerPixel = 3; }
	    int pixelLength = bytesPerPixel*dataBuffer.getSize();
	    _outputStream.writePixelDataHeader(pixelLength);
	}

	// Write the pixel data
	_outputStream.writePixelData(dataBuffer);

	// Update the Listeners
	_numberOfImagesWritten++;
	if ( abortRequested() ) { processWriteAborted(); }
	else { processImageComplete(); }
    }

    /**
     * Completes the writing of a sequence of images begun with
     * prepareWriteSequence.  Any stream metadata that should come at the end of
     * the sequence of images is written out, and any header information at the
     * beginning of the sequence is patched up if necessary.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void endWriteSequence() throws IOException
    {
	// Check for the Data Set
	if (_dataSet == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Add image metadata to MR images
	if ( _getStringValue("00080060", _dataSet).equals("MR") &&
	     _imageMetadata.size() > 0 )
	    {
		// Must rewrite sequence if image metadata exists
		if ( _imageMetadata.size() > 1 &&
		     _imageMetadata.firstElement() != null)
		    {
			// Replace the existing image metadata Data Element
			_dataSet.removeDataElement("52009230");
			DataElement element =
			    _getImageMetadataElement(_imageMetadata);
			_dataSet.addDataElement(element);

			// Update the sequence header
			_outputStream.updateSequenceHeader(_dataSet);

			// Update the pixel data header
			int size = (int)_outputStream.getPixelDataSize();
			_outputStream.updatePixelDataHeader(size);
		    }
	    }

	// More than one image requires correcting the pixel data header
	else if (_numberOfImagesWritten > 1) {
	    int size = (int)_outputStream.getPixelDataSize();
	    _outputStream.updatePixelDataHeader(size);
	}

	// Write the sequence footer
	_outputStream.writeSequenceFooter(_dataSet);

	// Reset the Data Set
	_dataSet = null;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageWriter.  Otherwise, the writer may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the output stream
	try { if (output != null) { ((ImageOutputStream)output).close(); } }
	catch (Exception e) {}

	output = null;
    }

    /**
     * Gets a Data Element for representing image metadata.  If no image
     * metadata is provided, an empty Data Set is added to the Data Element.
     *
     * @param imageMetadata II/O Metadata to represent.
     *
     * @return Data Element for representing image metadata.
     *
     * @throws IllegalArgumentException If the image metadata cannot be
     *                                  interpreted.
     */
    private DataElement _getImageMetadataElement(Vector imageMetadata)
    {
	// Create the image metadata Data Element
	String tag = "52009230";
	String name = "Per-frame Functional Groups Sequence";
	String vr = "SQ";
	DataElement dataElement = new DataElement(tag, vr, name);

	// If there is no image metadata, add an empty Data Set
	if ( imageMetadata.isEmpty() ) {
	    dataElement.addDataSet( new DataSet() );
	}

	// Otherwise, add the metadata to the Data Element
	name = DicomImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	Enumeration enun = imageMetadata.elements();
	while ( enun.hasMoreElements() ) {
	    IIOMetadata metadata = (IIOMetadata)enun.nextElement();

	    // If the metadata is null, add an empty Data Set
	    if (metadata == null) { dataElement.addDataSet( new DataSet() ); }

	    // Otherwise convert the II/O Metadata to a Data Set and add it
	    else {
		try {
		    Node tree = metadata.getAsTree(name);
		    DataSet dataSet = DicomMetadataConversions.toDataSet(tree);
		    dataElement.addDataSet(dataSet);
		}
	
		catch (Exception e) {
		    String msg = "Unable to make a Data Set from the " +
			"metadata tree.";
		    IllegalArgumentException iae =
			new IllegalArgumentException(msg);
		    iae.initCause(e);
		    throw iae;
		}
	    }
	}

	// Return the image metadata Data Element
	return dataElement;
    }

    /**
     * Gets the string value of the specified Data Element Tag.
     *
     * @param dataElementTag Tag of the Data Element to get the value for.
     * @param dataSet Data Set to get the Data Element value from.
     *
     * @return String value of the Data Element, or "" if no value exists.
     */
    private String _getStringValue(String dataElementTag, DataSet dataSet)
    {
	try {
	    Enumeration enun =
		dataSet.getDataElement(dataElementTag).getValues();
	    String value = enun.nextElement().toString();

	    // Return value without beginning & ending spaces (and no leading +)
	    value = value.trim();
	    if ( value.charAt(0) == '+') { value = value.substring(1); }
	    return value;
	}

	// Return a blank string if no string value exists
	catch (Exception e) { return ""; }
    }

    /**
     * Gets the integer value of the specified Data Element Tag.
     *
     * @param dataElementTag Tag of the Data Element to get the value for.
     * @param dataSet Data Set to get the Data Element value from.
     *
     * @return Integral value of the Data Element, or -1 if no value exists.
     */
    private int _getIntValue(String dataElementTag, DataSet dataSet)
    {
	// Get the value as a String
	String value = _getStringValue(dataElementTag, dataSet);

	// Return the value as an integer
	try { return Integer.parseInt(value); }
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the output stream.
     *
     * @return Image Output Stream to write to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream
	return (ImageOutputStream)output;
    }
}
