/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader SPI (Service Provider Interface) for the DICOM image format.
 *
 * @version 17 October 2005
 */
public class DicomImageReaderSpi extends ImageReaderSpi
{
  /** Name of the vendor who supplied this plug-in. */
  private static String _vendorName = "Laboratory of Neuro Imaging (LONI)";

  /** Version of this plug-in. */
  private static String _version = "1.0";

  /** Names of the formats read by this plug-in. */
  private static String[] _formatNames = {"dicom", "DICOM"};

  /** Names of commonly used suffixes for files in the supported formats. */
  private static String[] _fileSuffixes = {"dcm", "DCM"};

  /** MIME types of supported formats. */
  private static String[] _mimeTypes = {"image/dcm"};

  /** Name of the associated Image Reader. */
  private static String _reader = DicomImageReader.class.getName();

  /** Allowed input type classes for the plugin-in. */
  private static Class[] _inputTypes = {ImageInputStream.class,
					InputStream.class};

  /** Names of the associated Image Writer SPI's. */
  private static String[] _writerSpis = {DicomImageWriterSpi.class.getName()};

  /** Constructs a DicomImageReaderSpi. */
  public DicomImageReaderSpi()
    {
      super(_vendorName, _version, _formatNames, _fileSuffixes, _mimeTypes,
	    _reader, _inputTypes, _writerSpis,

	    // Support one native stream metatdata format
	    false, 
	    DicomStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    DicomStreamMetadataFormat.class.getName(), null, null,

	    // Support one native image metatdata format
	    false, 
	    DicomImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    DicomImageMetadataFormat.class.getName(), null, null);
    }

  /**
   * Determines if the supplied source object appears to be in a supported
   * format.  After the determination is made, the source is reset to its
   * original state if that operation is allowed.
   *
   * @param source Source Object to examine.
   *
   * @return True if the supplied source object appears to be in a supported
   *         format; false otherwise.
   *
   * @throws IOException If an I/O error occurs while reading a stream.
   * @throws IllegalArgumentException If the source is null.
   */
  public boolean canDecodeInput(Object source) throws IOException
    {
      InputStream inputStream;

      // Check for a null argument
      if (source == null) {
	String msg = "Cannot have a null source.";
	throw new IllegalArgumentException(msg);
      }

      // Adapt an Image Input Stream
      else if (source instanceof ImageInputStream) {
	inputStream = new InputStreamAdapter((ImageInputStream)source);
      }

      // Accept an Input Stream
      else if (source instanceof InputStream) {
	inputStream = (InputStream)source;
      }

      // Otherwise can not decode
      else { return false; }

      // Create a Data Input Stream out of the Input Stream
      DataInputStream dataStream = new DataInputStream(inputStream);

      // Mark the current position of the Data Input Stream if applicable
      if ( dataStream.markSupported() ) { dataStream.mark(128+4); }

      // Read in ID information
      byte[] filePreamble = new byte[128];
      byte[] idBytes = new byte[4];
      try {

	// Read in the 128 byte File Preamble
	dataStream.readFully(filePreamble);

	// Read in the ID characters
	dataStream.readFully(idBytes);
      }

      // End of file indicates that the file cannot be decoded; reset the bytes
      catch (EOFException eof) {
	  filePreamble = new byte[128];
	  idBytes = new byte[4];
      }

      // Reset the position of the Data Input Stream if applicable
      if ( dataStream.markSupported() ) { dataStream.reset(); }

      // ID characters must equal "DICM"
      String idString = new String(idBytes);
      if ( idString.equals("DICM") ) { return true; }

      // Search the File Preamble for "ISO_IR"
      for (int i = 0; i < filePreamble.length-6; i++) {
	  if ( filePreamble[i] == 73 && filePreamble[i+1] == 83 &&
	       filePreamble[i+2] == 79 && filePreamble[i+3] == 95 &&
	       filePreamble[i+4] == 73 && filePreamble[i+5] == 82 )
	      {
		  return true;
	      }
      }

      // Search the File Preamble for "ORIGINAL"
      for (int i = 0; i < filePreamble.length-8; i++) {
	  if ( filePreamble[i] == 79 && filePreamble[i+1] == 82 &&
	       filePreamble[i+2] == 73 && filePreamble[i+3] == 71 &&
	       filePreamble[i+4] == 73 && filePreamble[i+5] == 78 &&
	       filePreamble[i+6] == 65 && filePreamble[i+7] == 76)
	      {
		  return true;
	      }
      }

      // Not a DICOM file
      return false;
    }

  /**
   * Returns a brief, human-readable description of this service provider and
   * its associated implementation.
   *
   * @param locale Locale for which the return value should be localized.
   *
   * @return Description of this service provider.
   */
  public String getDescription(Locale locale)
    {
      return "Dicom image reader";
    }

  /**
   * Returns an instance of the ImageReader implementation associated with
   * this service provider.  The returned object will be in an initial state as
   * if its reset method had been called.
   *
   * @param extension A plug-in specific extension object, which may be null.
   *
   * @return An ImageReader instance.
   *
   * @throws IllegalArgumentException If the ImageReader finds the extension
   *                                  object to be unsuitable.
   */
  public ImageReader createReaderInstance(Object extension)
    {
      return new DicomImageReader(this);
    }
}
