/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

/**
 * Class that represents a chunk of data in a stream.
 *
 * @version 25 October 2002
 */
public class DataBlock
{
  /** Offset in the stream to the start of the block of data. */
  private long _offset;

  /** Size of the block of data. */
  private long _size;

  /**
   * Constructs a DataBlock.
   *
   * @param offset Offset in the stream to the start of the block of data.
   * @param size Size of the block of data.
   */
  public DataBlock(long offset, long size)
    {
      _offset = offset;
      _size = size;
    }

  /**
   * Gets the stream offset.
   *
   * @return Offset in the stream to the start of the block of data.
   */
  public long getOffset()
    {
      return _offset;
    }

  /**
   * Gets the block size.
   *
   * @return Size of the block of data.
   */
  public long getSize()
    {
      return _size;
    }
}
