/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

import java.util.Iterator;
import java.util.Vector;

/**
 * ASCCONV Node that represents a root in an ASCCONV tree.
 *
 * @version 24 August 2007
 */
public class AscconvRootNode extends AscconvNode
{
    /** Root index. */
    private int _index;

    /** Child Nodes. */
    private Vector _childNodes;

    /**
     * Constructs an ASCCONV Root Node.
     *
     * @param name Name of the ASCCONV Node.
     */
    public AscconvRootNode(String name)
    {
	this(name, -1);
    }

    /**
     * Constructs an ASCCONV Root Node with the specified index.
     *
     * @param name Name of the ASCCONV Node.
     * @param index Root index.
     */
    public AscconvRootNode(String name, int index)
    {
	super(name);

	_index = index;
	_childNodes = new Vector();
    }

    /**
     * Gets the root index.
     *
     * @return Root index, or -1 if there is no index.
     */
    public int getIndex()
    {
	return _index;
    }

    /**
     * Adds a child Node.
     *
     * @param childNode Child Node to add.
     */
    public void add(AscconvNode childNode)
    {
	_childNodes.add(childNode);
    }

    /**
     * Gets the first leaf Node with the given name.
     *
     * @param name Name of the leaf Node.
     *
     * @return First leaf Node with the given name, or null it does not exist.
     */
    public AscconvLeafNode getLeafNode(String name)
    {
	// Search for the leaf
	Iterator iter = _childNodes.iterator();
	while ( iter.hasNext() ) {
	    AscconvNode childNode = (AscconvNode)iter.next();
	    if (childNode instanceof AscconvLeafNode) {
		if ( childNode.getName().equals(name) ) {
		    return (AscconvLeafNode)childNode;
		}
	    }
	}

	// Child not found
	return null;
    }

    /**
     * Gets all the leaf Nodes.
     *
     * @return All the leaf Nodes.
     */
    public AscconvLeafNode[] getLeafNodes()
    {
	Vector leafNodes = new Vector();

	// Find all the leaves
	Iterator iter = _childNodes.iterator();
	while ( iter.hasNext() ) {
	    AscconvNode childNode = (AscconvNode)iter.next();
	    if (childNode instanceof AscconvLeafNode) {
		leafNodes.add(childNode);
	    }
	}

	return (AscconvLeafNode[])leafNodes.toArray(new AscconvLeafNode[0]);
    }

    /**
     * Gets the first root Node with the given name and index.
     *
     * @param name Name of the root Node.
     * @param index Index of the root Node.
     *
     * @return First root Node with the given name and index, or null if it does
     *         exist.
     */
    public AscconvRootNode getRootNode(String name, int index)
    {
	// Search for the root
	Iterator iter = _childNodes.iterator();
	while ( iter.hasNext() ) {
	    AscconvNode childNode = (AscconvNode)iter.next();
	    if (childNode instanceof AscconvRootNode) {
		AscconvRootNode rootNode = (AscconvRootNode)childNode;

		// Check the name and index
		String rootName = rootNode.getName();
		int rootIndex = rootNode.getIndex();
		if (name.equals(rootName) && index == rootIndex) {
		    return rootNode;
		}
	    }
	}

	// Child not found
	return null;
    }

    /**
     * Gets all the root Nodes.
     *
     * @return Groups of root Nodes.
     */
    public AscconvNodeGroup[] getRootNodes()
    {
	Vector groups = new Vector();

	// Find all the roots
	Iterator iter = _childNodes.iterator();
	while ( iter.hasNext() ) {
	    AscconvNode childNode = (AscconvNode)iter.next();
	    if (childNode instanceof AscconvRootNode) {
		AscconvRootNode rootNode = (AscconvRootNode)childNode;
		String rootName = rootNode.getName();

		// Find a Group for the root
		AscconvNodeGroup rootGroup = null;
		Iterator gIter = groups.iterator();
		while (gIter.hasNext() && rootGroup == null) {
		    AscconvNodeGroup group = (AscconvNodeGroup)gIter.next();
		    if ( group.getName().equals(rootName) ) {
			rootGroup = group;
		    }
		}

		// No Group, create a new one
		if (rootGroup == null) {
		    rootGroup = new AscconvNodeGroup(rootName);
		    groups.add(rootGroup);
		}

		// Add the root to the Group
		rootGroup.add(rootNode);
	    }
	}

	return (AscconvNodeGroup[])groups.toArray(new AscconvNodeGroup[0]);
    }

    /**
     * Gets a string representation of this Object.
     *
     * @return String representation of this Object.
     */
    public String[] toStrings()
    {
	Vector lines = new Vector();

	// Name
	String rootName = getName();
	if (_index != -1) { rootName += "[" + _index + "]"; }

	// Children
	Iterator iter = _childNodes.iterator();
	while ( iter.hasNext() ) {
	    AscconvNode childNode = (AscconvNode)iter.next();

	    // Leaf
	    if (childNode instanceof AscconvLeafNode) {
		AscconvLeafNode leafNode = (AscconvLeafNode)childNode;
		String[] values = leafNode.getValues();
		for (int i = 0; i < values.length; i++) {
		    String line = rootName + "." + leafNode.getName() +
			"[" + i + "] = " + values[i];
		    lines.add(line);
		}
	    }

	    // Root
	    else {
		AscconvRootNode rootNode = (AscconvRootNode)childNode;
		String[] subLines = rootNode.toStrings();
		for (int i = 0; i < subLines.length; i++) {
		    String line = rootName + "." + subLines[i];
		    lines.add(line);
		}
	    }
	}

	return (String[])lines.toArray(new String[0]);
    }
}
