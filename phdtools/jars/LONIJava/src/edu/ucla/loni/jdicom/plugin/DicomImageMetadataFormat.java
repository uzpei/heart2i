/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

/**
 * DICOM Metadata Format that describes the tree structure of metadata in the
 * DICOM Image Metadata class.
 *
 * @version 3 May 2003
 */
public class DicomImageMetadataFormat extends DicomMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_jdicom_image_1.0";

  /** Single instance of the DicomImageMetadataFormat. */
  private static DicomImageMetadataFormat _format =
  new DicomImageMetadataFormat();

  /** Constructs a DicomImageMMetadataFormat. */
  private DicomImageMetadataFormat()
    {
      super(NATIVE_METADATA_FORMAT_NAME);
    }

  /**
   * Gets an instance of the DicomImageMetadataFormat.
   *
   * @return The single instance of the DicomImageMetadataFormat.
   */
  public static DicomImageMetadataFormat getInstance()
    {
      return _format;
    }
}
