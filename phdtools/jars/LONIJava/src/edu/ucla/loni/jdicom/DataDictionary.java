/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Registry of DICOM Data Elements.  For each Data Element, a Data Element
 * Tag, description, Value Representation, and Value Multiplicity is defined.
 * <p>
 * <b> PS 3.6-1998, DATA DICTIONARY </b>
 *
 * @version 29 October 2002
 */
public class DataDictionary
{
  /**
   * Data structure representing the registry of DICOM Data Elements.
   * <p>
   * <b> PS 3.6-1998, DATA DICTIONARY [Section 6], Page 5 </b>
   */
  private TreeMap _registry;

  /** Single instance of the Data Dictionary. */
  private static DataDictionary _dataDictionary = new DataDictionary();

  /** Constructs a DataDictionary. */
  private DataDictionary()
    {
      // Load the registry
      _loadRegistry();
    }

  /**
   * Gets the instance of the Data Dictionary.
   *
   * @return Single instance of the Data Dictionary.
   */
  public static DataDictionary getInstance()
    {
      return _dataDictionary;
    }

  /**
   * Gets the Value Representation of the Data Element corresponding to
   * the Data Element Tag.
   *
   * @param dataElementTag Tag that uniquely identifies the Data Element.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   *
   * @return Two character code that specifies the data type and format of
   *         the Values of the Data Element, or null if it does not exist.
   */
  public String getValueRepresentation(String dataElementTag)
    {
      // Get the matching vector from the registry
      String[] values = _get(dataElementTag);
      if (values == null) { return null; }

      // VR is the first object in the Vector
      return values[0];
    }

  /**
   * Gets the name of the Data Element corresponding to the Data Element Tag.
   *
   * @param dataElementTag Tag that uniquely identifies the Data Element.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   *
   * @return Description of the Data Element, or null if it does not exist.
   */
  public String getName(String dataElementTag)
    {
      // Get the matching vector from the registry
      String[] values = _get(dataElementTag);
      if (values == null) { return null; }

      // Name is the second object in the Vector
      return values[1];
    }

  /**
   * Gets the Value Multiplicity of the Data Element corresponding to the
   * Data Element Tag.
   * <p>
   * <b> PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
   *     [Section 6.4], Page 23: </b>
   * <em>
   * The Value Multiplicity of a Data Element specifies the number of Values
   * that can be encoded in the Value Field of that Data Element. The VM of
   * each Data Element is specified explicitly in PS 3.6. If the number
   * of Values that may be encoded in an element is variable, it shall be
   * represented by two numbers separated by a dash; e.g., "1-10" means that
   * there may be 1 to 10 Values in the element.
   * </em>
   *
   * @param dataElementTag Tag that uniquely identifies the Data Element.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   *
   * @return Code which represents the number of Values that may be contained
   *         in the Data Element, or null if it does not exist.
   */
  public String getValueMultiplicity(String dataElementTag)
    {
      // Get the matching vector from the registry
      String[] values = _get(dataElementTag);
      if (values == null) { return null; }

      // VM is the third object in the Vector
      return values[2];
    }

  /**
   * Gets all the Data Element Tags defined by the Data Dictionary.
   *
   * @return All the Data Element Tags defined by the Data Dictionary listed in
   *         ascending order.  Each tag is represented as an 8 digit
   *         hexadecimal number.  The first 4 digits represent the Group Number
   *         and the last 4 digits represent the Element Number.
   */
  public String[] getDataElementTags()
    {
      // Get all the Data Element Tags as Objects
      Object[] keys = _registry.keySet().toArray();

      // Create a String array from the Object array
      String[] dataElementTags = new String[keys.length];
      System.arraycopy(keys, 0, dataElementTags, 0, keys.length);

      // Return the Data Element Tags
      return dataElementTags;
    }

  /** 
   * Gets the contents of the DataDictionary.
   *
   * @return String representation of the contents of the DataDictionary.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Write the header
      buffer.append("TAG" + "\t" + "PROPERTIES" + "\n");

      // Write each tag and its corresponding values
      Iterator it = _registry.entrySet().iterator();
      while ( it.hasNext() ) {
	Entry e = (Entry)it.next();

	// Write the tag
	buffer.append( e.getKey() );

	// Write the Name
	String[] values = (String[])e.getValue();
	buffer.append("\t" + values[1] + "\n");

	// Write the VR
	buffer.append("\t" + "VR:  " + values[0] + "\n");

	// Write the VM
	buffer.append("\t" + "VM:  " + values[2] + "\n");
      }

      return buffer.toString();
    }

  /**
   * Adds an entry to the registry of DICOM Data Elements.
   *
   * @param dataElementTag Tag that uniquely identifies the Data Element.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   * @param valueRepresentation Two character code that specifies the data
   *                            type and format of the Values of the
   *                            Data Element.
   * @param name Description of the Data Element.
   * @param valueMultiplicity Code which represents the number of Values
   *                          that may be contained in the Data Element.
   */
  private void _add(String dataElementTag, String valueRepresentation,
		    String name, String valueMultiplicity)
    {
      // Pair the VR, name, and VM
      String values[] = new String[3];
      values[0] = valueRepresentation;
      values[1] = name;
      values[2] = valueMultiplicity;

      // Add the pairing using the tag as the key
      _registry.put(dataElementTag, values);
    }

  /**
   * Gets an entry from the registry of DICOM Data Elements.
   *
   * @param dataElementTag Tag that uniquely identifies the Data Element.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   *
   * @return Object array containing the Value Representation, name, and
   *         Value Multiplicity corresponding to the Data Element Tag, or null
   *         if this array does not exist.
   */
  private String[] _get(String dataElementTag)
    {
      String tag = dataElementTag;

      // If the tag is from group 5000 or group 6000, replace the last two
      // hexadecimal digits of the Group Number with "xx"
      if ( tag.startsWith("50") || tag.startsWith("60") ) {
	StringBuffer tagBuffer = new StringBuffer(tag);
	tagBuffer.replace(2, 4, "xx");
	tag = tagBuffer.toString();
      }

      // Check the registry for a match on the tag
      Object o = _registry.get(tag);
      if (o == null) { return null; }

      return (String [])o;
    }

  /** Loads the registry of DICOM Data Elements. */
  private void _loadRegistry()
    {
      // Initialize a new registry
      _registry = new TreeMap();

      // Group 2 Elements
      _add("00020000", "UL", "Group Length", "1");
      _add("00020001", "OB", "File Meta Information Version", "1");
      _add("00020002", "UI", "Media Storage SOP Class UID", "1");
      _add("00020003", "UI", "Media Storage SOP Instance UID", "1");
      _add("00020010", "UI", "Transfer Syntax UID", "1");
      _add("00020012", "UI", "Implementation Class UID", "1");
      _add("00020013", "SH", "Implementation Version Name", "1");
      _add("00020016", "AE", "Source Application Entity Title", "1");
      _add("00020100", "UI", "Private Information Creator UID", "1");
      _add("00020102", "OB", "Private Information", "1");

      // Group 4 Elements
      _add("00040000", "UL", "Group Length", "1");
      _add("00041130", "CS", "File-set ID", "1");
      _add("00041141", "CS", "File-set Descriptor File ID", "1-8");
      _add("00041142", "CS",
	   "Specific Character Set of File-set Descriptor File", "1");
      _add("00041200", "UL",
	   "Offset of First Directory Record of the Root Directory Entity",
	   "1");
      _add("00041202", "UL",
	   "Offset of Last Directory Record of the Root Directory Entity",
	   "1");
      _add("00041212", "US", "File-set Consistency Flag", "1");
      _add("00041220", "SQ", "Directory Record Sequence", "1");
      _add("00041400", "UL", "Offset of the Next Directory Record", "1");
      _add("00041410", "US", "Record In-use Flag", "1");
      _add("00041420", "UL",
	   "Offset of Referenced Lower-Level Directory Entity", "1");
      _add("00041430", "CS", "Directory Record Type", "1");
      _add("00041432", "UI", "Private Record UID", "1");
      _add("00041500", "CS", "Referenced File ID", "1-8");
      _add("00041504", "UL", "MRDR Directory Record Offset", "1");
      _add("00041510", "UI", "Referenced SOP Class UID in File", "1");
      _add("00041511", "UI", "Referenced SOP Instance UID in File", "1");
      _add("00041512", "UI", "Referenced Transfer Syntax UID in File", "1");
      _add("00041600", "UL", "Number of References", "1");

      // Group 8 Elements
      _add("00080000", "UL", "Group Length", "1");
      _add("00080005", "CS", "Specific Character Set", "1-n");
      _add("00080008", "CS", "Image Type", "1-n");
      _add("00080012", "DA", "Instance Creation Date", "1");
      _add("00080013", "TM", "Instance Creation Time", "1");
      _add("00080014", "UI", "Instance Creator UID", "1");
      _add("00080016", "UI", "SOP Class UID", "1");
      _add("00080018", "UI", "SOP Instance UID", "1");
      _add("00080020", "DA", "Study Date", "1");
      _add("00080021", "DA", "Series Date", "1");
      _add("00080022", "DA", "Acquisition Date", "1");
      _add("00080023", "DA", "Image Date", "1");
      _add("00080024", "DA", "Overlay Date", "1");
      _add("00080025", "DA", "Curve Date", "1");
      _add("00080030", "TM", "Study Time", "1");
      _add("00080031", "TM", "Series Time", "1");
      _add("00080032", "TM", "Acquisition Time", "1");
      _add("00080033", "TM", "Image Time", "1");
      _add("00080034", "TM", "Overlay Time", "1");
      _add("00080035", "TM", "Curve Time", "1");
      _add("00080050", "SH", "Accession Number", "1");
      _add("00080052", "CS", "Query/Retrieve Level", "1");
      _add("00080054", "AE", "Retrieve AE Title", "1-n");
      _add("00080058", "UI", "Failed SOP Instance UID List", "1-n");
      _add("00080060", "CS", "Modality", "1");
      _add("00080061", "CS", "Modalities in Study", "1-n");
      _add("00080064", "CS", "Conversion Type", "1");
      _add("00080070", "LO", "Manufacturer", "1");
      _add("00080080", "LO", "Institution Name", "1");
      _add("00080081", "ST", "Institution Address", "1");
      _add("00080082", "SQ", "Institution Code Sequence", "1");
      _add("00080090", "PN", "Referring Physician's Name", "1");
      _add("00080092", "ST", "Referring Physician's Address", "1");
      _add("00080094", "SH", "Referring Physician's Telephone Numbers", "1-n");
      _add("00080100", "SH", "Code Value", "1");
      _add("00080102", "SH", "Coding Scheme Designator", "1");
      _add("00080104", "LO", "Code Meaning", "1");
      _add("00081010", "SH", "Station Name", "1");
      _add("00081030", "LO", "Study Description", "1");
      _add("00081032", "SQ", "Procedure Code Sequence", "1");
      _add("0008103E", "LO", "Series Description", "1");
      _add("00081040", "LO", "Institutional Department Name", "1");
      _add("00081048", "PN", "Physician(s) of Record", "1-n");
      _add("00081050", "PN", "Performing Physician's Name", "1-n");
      _add("00081060", "PN", "Name of Physician(s) Reading Study", "1-n");
      _add("00081070", "PN", "Operators' Name", "1-n");
      _add("00081080", "LO", "Admitting Diagnoses Description", "1-n");
      _add("00081084", "SQ", "Admitting Diagnosis Code Sequence", "1");
      _add("00081090", "LO", "Manufacturer's Model Name", "1");
      _add("00081100", "SQ", "Referenced Results Sequence", "1");
      _add("00081110", "SQ", "Referenced Study Sequence", "1");
      _add("00081111", "SQ", "Referenced Study Component Sequence", "1");
      _add("00081115", "SQ", "Referenced Series Sequence", "1");
      _add("00081120", "SQ", "Referenced Patient Sequence", "1");
      _add("00081125", "SQ", "Referenced Visit Sequence", "1");
      _add("00081130", "SQ", "Referenced Overlay Sequence", "1");
      _add("00081140", "SQ", "Referenced Image Sequence", "1");
      _add("00081145", "SQ", "Referenced Curve Sequence", "1");
      _add("00081150", "UI", "Referenced SOP Class UID", "1");
      _add("00081155", "UI", "Referenced SOP Instance UID", "1");
      _add("00081160", "IS", "Referenced Frame Number", "1");
      _add("00081195", "UI", "Transaction UID", "1");
      _add("00081197", "US", "Failure Reason", "1");
      _add("00081198", "SQ", "Failed SOP Sequence", "1");
      _add("00081199", "SQ", "Referenced SOP Sequence", "1");
      _add("00082111", "ST", "Derivation Description", "1");
      _add("00082112", "SQ", "Source Image Sequence", "1");
      _add("00082120", "SH", "Stage Name", "1");
      _add("00082122", "IS", "Stage Number", "1");
      _add("00082124", "IS", "Number of Stages", "1");
      _add("00082128", "IS", "View Number", "1");
      _add("00082129", "IS", "Number of Event Timers", "1");
      _add("0008212A", "IS", "Number of Views in Stage", "1");
      _add("00082130", "DS", "Event Elapsed Time(s)", "1-n");
      _add("00082132", "LO", "Event Timer Name(s)", "1-n");
      _add("00082142", "IS", "Start Trim", "1");
      _add("00082143", "IS", "Stop Trim", "1");
      _add("00082144", "IS", "Recommended Display Frame Rate", "1");
      _add("00082218", "SQ", "Anatomic Region Sequence", "1");
      _add("00082220", "SQ", "Anatomic Region Modifier Sequence", "1");
      _add("00082228", "SQ", "Primary Anatomic Structure Sequence", "1");
      _add("00082229", "SQ", "Anatomic Structure, Space or Region Sequence",
	   "1");
      _add("00082230", "SQ", "Primary Anatomic Structure Modifier Sequence",
	   "1");
      _add("00082240", "SQ", "Transducer Position Sequence", "1");
      _add("00082242", "SQ", "Transducer Position Modifier Sequence", "1");
      _add("00082244", "SQ", "Transducer Orientation Sequence", "1");
      _add("00082246", "SQ", "Transducer Orientation Modifier Sequence", "1");
      _add("00089007", "CS", "Frame Type", "4");
      _add("00089121", "SQ", "Referenced Raw Data Sequence", "1");
      _add("00089123", "UI", "Creator-Version UID", "1");
      _add("00089124", "SQ", "Derivation Image Sequence", "1");
      _add("00089092", "SQ", "Referring Image Evidence Sequence", "1");
      _add("00089154", "SQ", "Source Image Evidence Sequence", "1");
      _add("00089205", "CS", "Pixel Presentation", "1");
      _add("00089206", "CS", "Volumetric Properties", "1");
      _add("00089207", "CS", "Volume Based Calculation Technique", "1");
      _add("00089208", "CS", "Complex Image Component", "1");
      _add("00089209", "CS", "Acquisition Contrast", "1");
      _add("00089215", "SQ", "Derivation Code Sequence", "1");
      _add("00089237", "SQ",
	   "Referenced Grayscale Presentation State Sequence", "1");

      // Group 10 Elements
      _add("00100000", "UL", "Group Length", "1");
      _add("00100010", "PN", "Patient's Name", "1");
      _add("00100020", "LO", "Patient ID", "1");
      _add("00100021", "LO", "Issuer of Patient ID", "1");
      _add("00100030", "DA", "Patient's Birth Date", "1");
      _add("00100032", "TM", "Patient's Birth Time", "1");
      _add("00100040", "CS", "Patient's Sex", "1");
      _add("00100050", "SQ", "Patient's Insurance Plan Code Sequence", "1");
      _add("00101000", "LO", "Other Patient IDs", "1-n");
      _add("00101001", "PN", "Other Patient Names", "1-n");
      _add("00101005", "PN", "Patient's Birth Name", "1");
      _add("00101010", "AS", "Patient's Age", "1");
      _add("00101020", "DS", "Patient's Size", "1");
      _add("00101030", "DS", "Patient's Weight", "1");
      _add("00101040", "LO", "Patient's Address", "1");
      _add("00101060", "PN", "Patient's Mother's Birth Name", "1");
      _add("00101080", "LO", "Military Rank", "1");
      _add("00101081", "LO", "Branch of Service", "1");
      _add("00101090", "LO", "Medical Record Locator", "1");
      _add("00102000", "LO", "Medical Alerts", "1-n");
      _add("00102110", "LO", "Contrast Allergies", "1-n");
      _add("00102150", "LO", "Country of Residence", "1");
      _add("00102152", "LO", "Region of Residence", "1");
      _add("00102154", "SH", "Patient's Telephone Numbers", "1-n");
      _add("00102160", "SH", "Ethnic Group", "1");
      _add("00102180", "SH", "Occupation", "1");
      _add("001021A0", "CS", "Smoking Status", "1");
      _add("001021B0", "LT", "Additional Patient History", "1");
      _add("001021C0", "US", "Pregnancy Status", "1");
      _add("001021D0", "DA", "Last Menstrual Date", "1");
      _add("001021F0", "LO", "Patient's Religious Preference", "1");
      _add("00104000", "LT", "Patient Comments", "1");

      // Group 18 Elements
      _add("00180000", "UL", "Group Length", "1");
      _add("00180010", "LO", "Contrast/Bolus Agent", "1");
      _add("00180012", "SQ", "Contrast/Bolus Agent Sequence", "1");
      _add("00180014", "SQ", "Contrast/Bolus Administration Route Sequence",
	   "1");
      _add("00180015", "CS", "Body Part Examined", "1");
      _add("00180020", "CS", "Scanning Sequence", "1-n");
      _add("00180021", "CS", "Sequence Variant", "1-n");
      _add("00180022", "CS", "Scan Options", "1-n");
      _add("00180023", "CS", "MR Acquisition Type", "1");
      _add("00180024", "SH", "Sequence Name", "1");
      _add("00180025", "CS", "Angio Flag", "1");
      _add("00180026", "SQ", "Intervention Drug Information Sequence", "1");
      _add("00180027", "TM", "Intervention Drug Stop Time", "1");
      _add("00180028", "DS", "Intervention Drug Dose", "1");
      _add("00180029", "SQ", "Intervention Drug Code Sequence", "1");
      _add("0018002A", "SQ", "Additional Drug Sequence", "1");
      _add("00180031", "LO", "Radiopharmaceutical", "1");
      _add("00180034", "LO", "Intervention Drug Name", "1");
      _add("00180035", "TM", "Intervention Drug Start Time", "1");
      _add("00180036", "SQ", "Interventional Therapy Sequence", "1");
      _add("00180037", "CS", "Therapy Type", "1");
      _add("00180038", "CS", "Interventional Status", "1");
      _add("00180039", "CS", "Therapy Description", "1");
      _add("00180040", "IS", "Cine Rate", "1");
      _add("00180050", "DS", "Slice Thickness", "1");
      _add("00180060", "DS", "KVP", "1");
      _add("00180070", "IS", "Counts Accumulated", "1");
      _add("00180071", "CS", "Acquisition Termination Condition", "1");
      _add("00180072", "DS", "Effective Series Duration", "1");
      _add("00180073", "CS", "Acquisition Start Condition", "1");
      _add("00180074", "IS", "Acquisition Start Condition Data", "1");
      _add("00180075", "IS", "Acquisition Termination Condition Data", "1");
      _add("00180080", "DS", "Repetition Time", "1");
      _add("00180081", "DS", "Echo Time", "1");
      _add("00180082", "DS", "Inversion Time", "1");
      _add("00180083", "DS", "Number of Averages", "1");
      _add("00180084", "DS", "Imaging Frequency", "1");
      _add("00180085", "SH", "Imaged Nucleus", "1");
      _add("00180086", "IS", "Echo Number(s)", "1-n");
      _add("00180087", "DS", "Magnetic Field Strength", "1");
      _add("00180088", "DS", "Spacing Between Slices", "1");
      _add("00180089", "IS", "Number of Phase Encoding Steps", "1");
      _add("00180090", "DS", "Data Collection Diameter", "1");
      _add("00180091", "IS", "Echo Train Length", "1");
      _add("00180093", "DS", "Percent Sampling", "1");
      _add("00180094", "DS", "Percent Phase Field of View", "1");
      _add("00180095", "DS", "Pixel Bandwidth", "1");
      _add("00181000", "LO", "Device Serial Number", "1");
      _add("00181004", "LO", "Plate ID", "1");
      _add("00181010", "LO", "Secondary Capture Device ID", "1");
      _add("00181011", "LO", "Hardcopy Creation Device ID", "1");
      _add("00181012", "DA", "Date of Secondary Capture", "1");
      _add("00181014", "TM", "Time of Secondary Capture", "1");
      _add("00181016", "LO", "Secondary Capture Device Manufacturer", "1");
      _add("00181017", "LO", "Hardcopy Device Manufacturer", "1");
      _add("00181018", "LO",
	   "Secondary Capture Device Manufacturer's Model Name", "1");
      _add("00181019", "LO", "Secondary Capture Device Software Version(s)",
	   "1-n");
      _add("0018101A", "LO", "Hardcopy Device Software Version", "1-n");
      _add("0018101B", "LO", "Hardcopy Device Manfuacturer's Model Name", "1");
      _add("00181020", "LO", "Software Version(s)", "1-n");
      _add("00181022", "SH", "Video Image Format Acquired", "1");
      _add("00181023", "LO", "Digital Image Format Acquired", "1");
      _add("00181030", "LO", "Protocol Name", "1");
      _add("00181040", "LO", "Contrast/Bolus Route", "1");
      _add("00181041", "DS", "Contrast/Bolus Volume", "1");
      _add("00181042", "TM", "Contrast/Bolus Start Time", "1");
      _add("00181043", "TM", "Contrast/Bolus Stop Time", "1");
      _add("00181044", "DS", "Contrast/Bolus Total Dose", "1");
      _add("00181045", "IS", "Syringe Counts", "1");
      _add("00181046", "DS", "Contrast Flow Rate(s)", "1-n");
      _add("00181047", "DS", "Contrast Flow Duration(s)", "1-n");
      _add("00181048", "CS", "Contrast/Bolus Ingredient", "1");
      _add("00181049", "DS", "Contrast/Bolus Ingredient Concentration", "1");
      _add("00181050", "DS", "Spatial Resolution", "1");
      _add("00181060", "DS", "Trigger Time", "1");
      _add("00181061", "LO", "Trigger Source or Type", "1");
      _add("00181062", "IS", "Nominal Interval", "1");
      _add("00181063", "DS", "Frame Time", "1");
      _add("00181064", "LO", "Framing Type", "1");
      _add("00181065", "DS", "Frame Time Vector", "1-n");
      _add("00181066", "DS", "Frame Delay", "1");
      _add("00181070", "LO", "Radiopharmaceutical Route", "1");
      _add("00181071", "DS", "Radiopharmaceutical Volume", "1");
      _add("00181072", "TM", "Radiopharmaceutical Start Time", "1");
      _add("00181073", "TM", "Radiopharmaceutical Stop Time", "1");
      _add("00181074", "DS", "Radionuclide Total Dose", "1");
      _add("00181075", "DS", "Radionuclide Half Life", "1");
      _add("00181076", "DS", "Radionuclide Positron Fraction", "1");
      _add("00181077", "DS", "Radiopharmaceutical Specific Activity", "1");
      _add("00181080", "CS", "Beat Rejection Flag", "1");
      _add("00181081", "IS", "Low R-R Value", "1");
      _add("00181082", "IS", "High R-R Value", "1");
      _add("00181083", "IS", "Intervals Acquired", "1");
      _add("00181084", "IS", "Intervals Rejected", "1");
      _add("00181085", "LO", "PVC Rejection", "1");
      _add("00181086", "IS", "Skip Beats", "1");
      _add("00181088", "IS", "Heart Rate", "1");
      _add("00181090", "IS", "Cardiac Number of Images", "1");
      _add("00181094", "IS", "Trigger Window", "1");
      _add("00181100", "DS", "Reconstruction Diameter", "1");
      _add("00181110", "DS", "Distance Source to Detector", "1");
      _add("00181111", "DS", "Distance Source to Patient", "1");
      _add("00181114", "DS", "Estimated Radiographic Magnification Factor",
	   "1");
      _add("00181120", "DS", "Gantry/Detector Tilt", "1");
      _add("00181121", "DS", "Gantry/Detector Slew", "1");
      _add("00181130", "DS", "Table Height", "1");
      _add("00181131", "DS", "Table Traverse", "1");
      _add("00181134", "DS", "Table Motion", "1");
      _add("00181135", "DS", "Table Vertical Increment", "1-n");
      _add("00181136", "DS", "Table Lateral Increment", "1-n");
      _add("00181137", "DS", "Table Longitudinal Increment", "1-n");
      _add("00181138", "DS", "Table Angle", "1");
      _add("00181140", "CS", "Rotation Direction", "1");
      _add("00181141", "DS", "Angular Position", "1");
      _add("00181142", "DS", "Radial Position", "1-n");
      _add("00181143", "DS", "Scan Arc", "1");
      _add("00181144", "DS", "Angular Step", "1");
      _add("00181145", "DS", "Center of Rotation Offset", "1");
      _add("00181147", "CS", "Field of View Shape", "1");
      _add("00181149", "IS", "Field of View Dimension(s)", "1-2");
      _add("00181150", "IS", "Exposure Time", "1");
      _add("00181151", "IS", "X-ray Tube Current", "1");
      _add("00181152", "IS", "Exposure", "1");
      _add("00181153", "IS", "Exposure in uAs", "1");
      _add("00181154", "DS", "Average Pulse Width", "1");
      _add("00181155", "CS", "Radiation Setting", "1");
      _add("0018115A", "CS", "Radiation Mode", "1");
      _add("0018115E", "DS", "Image Area Dose Product", "1");
      _add("00181160", "SH", "Filter Type", "1");
      _add("00181161", "LO", "Type of Filters", "1-n");
      _add("00181162", "DS", "Intensifier Size", "1");
      _add("00181164", "DS", "Imager Pixel Spacing", "2");
      _add("00181166", "CS", "Grid", "1");
      _add("00181170", "IS", "Generator Power", "1");
      _add("00181180", "SH", "Collimator/grid Name", "1");
      _add("00181181", "CS", "Collimator Type", "1");
      _add("00181182", "IS", "Focal Distance", "1-2");
      _add("00181183", "DS", "X Focus Center", "1-2");
      _add("00181184", "DS", "Y Focus Center", "1-2");
      _add("00181190", "DS", "Focal Spot(s)", "1-n");
      _add("00181200", "DA", "Date of Last Calibration", "1-n");
      _add("00181201", "TM", "Time of Last Calibration", "1-n");
      _add("00181210", "SH", "Convolution Kernel", "1-n");
      _add("00181242", "IS", "Actual Frame Duration", "1");
      _add("00181243", "IS", "Count Rate", "1");
      _add("00181244", "US", "Preferred Playback Sequencing", "1");
      _add("00181250", "SH", "Receive Coil Name", "1");
      _add("00181251", "SH", "Transmit Coil Name", "1");
      _add("00181260", "SH", "Plate Type", "1");
      _add("00181261", "LO", "Phosphor Type", "1");
      _add("00181300", "DS", "Scan Velocity", "1");
      _add("00181301", "CS", "Whole Body Technique", "1-n");
      _add("00181302", "IS", "Scan Length", "1");
      _add("00181310", "US", "Acquisition Matrix", "4");
      _add("00181312", "CS", "Phase Encoding Direction", "1");
      _add("00181314", "DS", "Flip Angle", "1");
      _add("00181315", "CS", "Variable Flip Angle Flag", "1");
      _add("00181316", "DS", "SAR", "1");
      _add("00181318", "DS", "dB/dt", "1");
      _add("00181400", "LO", "Acquisition Device Processing Description", "1");
      _add("00181401", "LO", "Acquisition Device Processing Code", "1");
      _add("00181402", "CS", "Cassette Orientation", "1");
      _add("00181403", "CS", "Cassette Size", "1");
      _add("00181404", "US", "Exposures on Plate", "1");
      _add("00181405", "IS", "Relative X-ray Exposure", "1");
      _add("00181450", "CS", "Column Angulation", "1");
      _add("00181460", "DS", "Tomo Layer Height", "1");
      _add("00181470", "DS", "Tomo Angle", "1");
      _add("00181480", "DS", "Tomo Time", "1");
      _add("00181500", "CS", "Positioner Motion", "1");
      _add("00181510", "DS", "Positioner Primary Angle", "1");
      _add("00181511", "DS", "Positioner Secondary Angle", "1");
      _add("00181520", "DS", "Positioner Primary Angle Increment", "1-n");
      _add("00181521", "DS", "Positioner Secondary Angle Increment", "1-n");
      _add("00181530", "DS", "Detector Primary Angle", "1");
      _add("00181531", "DS", "Detector Secondary Angle", "1");
      _add("00181600", "CS", "Shutter Shape", "1-3");
      _add("00181602", "IS", "Shutter Left Vertical Edge", "1");
      _add("00181604", "IS", "Shutter Right Vertical Edge", "1");
      _add("00181606", "IS", "Shutter Upper Horizontal Edge", "1");
      _add("00181608", "IS", "Shutter Lower Horizontal Edge", "1");
      _add("00181610", "IS", "Center of Circular Shutter", "2");
      _add("00181612", "IS", "Radius of Circular Shutter", "1");
      _add("00181620", "IS", "Vertices of the Polygonal Shutter", "2-2n");
      _add("00181700", "CS", "Collimator Shape", "1-3");
      _add("00181702", "IS", "Collimator Left Vertical Edge", "1");
      _add("00181704", "IS", "Collimator Right Vertical Edge", "1");
      _add("00181706", "IS", "Collimator Upper Horizontal Edge", "1");
      _add("00181708", "IS", "Collimator Lower Horizontal Edge", "1");
      _add("00181710", "IS", "Center of Circular Collimator", "2");
      _add("00181712", "IS", "Radius of Circular Collimator", "1");
      _add("00181720", "IS", "Vertices of the Polygonal Collimator", "2-2n");
      _add("00185000", "SH", "Output Power", "1");
      _add("00185010", "LO", "Transducer Data", "3");
      _add("00185012", "DS", "Focus Depth", "1");
      _add("00185020", "LO", "Processing Function", "1");
      _add("00185021", "LO", "Postprocessing Function", "1");
      _add("00185022", "DS", "Mechanical Index", "1");
      _add("00185024", "DS", "Thermal Index", "1");
      _add("00185026", "DS", "Cranial Thermal Index", "1");
      _add("00185027", "DS", "Soft Tissue Thermal Index", "1");
      _add("00185028", "DS", "Soft Tissue-focus Thermal Index", "1");
      _add("00185029", "DS", "Soft Tissue-surface Thermal Index", "1");
      _add("00185050", "IS", "Depth of Scan Field", "1");
      _add("00185100", "CS", "Patient Position", "1");
      _add("00185101", "DS", "View Position", "1");
      _add("00185210", "DS", "Image Transformation Matrix", "6");
      _add("00185212", "DS", "Image Translation Vector", "3");
      _add("00186000", "DS", "Sensitivity", "1");
      _add("00186011", "SQ", "Sequence of Ultrasound Regions", "1");
      _add("00186012", "US", "Region Spatial Format", "1");
      _add("00186014", "US", "Region Data Type", "1");
      _add("00186016", "UL", "Region Flags", "1");
      _add("00186018", "UL", "Region Location Min X0", "1");
      _add("0018601A", "UL", "Region Location Min Y0", "1");
      _add("0018601C", "UL", "Region Location Max X1", "1");
      _add("0018601E", "UL", "Region Location Max Y1", "1");
      _add("00186020", "SL", "Reference Pixel X0", "1");
      _add("00186022", "SL", "Reference Pixel Y0", "1");
      _add("00186024", "US", "Physical Units X Direction", "1");
      _add("00186026", "US", "Physical Units Y Direction", "1");
      _add("00186028", "FD", "Reference Pixel Physical Value X", "1");
      _add("0018602A", "FD", "Reference Pixel Physical Value Y", "1");
      _add("0018602C", "FD", "Physical Delta X", "1");
      _add("0018602E", "FD", "Physical Delta Y", "1");
      _add("00186030", "UL", "Transducer Frequency", "1");
      _add("00186031", "CS", "Transducer Type", "1");
      _add("00186032", "UL", "Pulse Repetition Frequency", "1");
      _add("00186034", "FD", "Doppler Correction Angle", "1");
      _add("00186036", "FD", "Steering Angle", "1");
      _add("00186038", "UL", "Doppler Sample Volume X Position", "1");
      _add("0018603A", "UL", "Doppler Sample Volume Y Position", "1");
      _add("0018603C", "UL", "TM-Line Position X0", "1");
      _add("0018603E", "UL", "TM-Line Position Y0", "1");
      _add("00186040", "UL", "TM-Line Position X1", "1");
      _add("00186042", "UL", "TM-Line Position Y1", "1");
      _add("00186044", "US", "Pixel Component Organization", "1");
      _add("00186046", "UL", "Pixel Component Mask", "1");
      _add("00186048", "UL", "Pixel Component Range Start", "1");
      _add("0018604A", "UL", "Pixel Component Range Stop", "1");
      _add("0018604C", "US", "Pixel Component Physical Units", "1");
      _add("0018604E", "US", "Pixel Component Data Type", "1");
      _add("00186050", "UL", "Number of Table Break Points", "1");
      _add("00186052", "UL", "Table of X Break Points", "1-n");
      _add("00186054", "FD", "Table of Y Break Points", "1-n");
      _add("00186056", "UL", "Number of Table Entries", "1");
      _add("00186058", "UL", "Table of Pixel Values", "1-n");
      _add("0018605A", "FL", "Table of Parameter Values", "1-n");
      _add("00189004", "CS", "Content Qualification", "1");
      _add("00189005", "SH", "Pulse Sequence Name", "1");
      _add("00189006", "SQ", "MR Imaging Modifier Sequence", "1");
      _add("00189008", "CS", "Echo Pulse Sequence", "1");
      _add("00189009", "CS", "Inversion Recovery", "1");
      _add("00189010", "CS", "Flow Compensation", "1");
      _add("00189011", "CS", "Multiple Spin Echo", "1");
      _add("00189012", "CS", "Multi-planar Excitation", "1");
      _add("00189014", "CS", "Phase Contrast", "1");
      _add("00189015", "CS", "Time of Flight Contrast", "1");
      _add("00189016", "CS", "Spoiling", "1");
      _add("00189017", "CS", "Steady State Pulse Sequence", "1");
      _add("00189018", "CS", "Echo Planar Pulse Sequence", "1");
      _add("00189019", "FD", "Tag Angle First Axis", "1");
      _add("00189020", "CS", "Magnetization Transfer", "1");
      _add("00189021", "CS", "T2 Preparation", "1");
      _add("00189022", "CS", "Blood Signal Nulling", "1");
      _add("00189024", "CS", "Saturation Recovery", "1");
      _add("00189025", "CS", "Spectrally Selected Suppression", "1");
      _add("00189026", "CS", "Spectrally Selected Excitation", "1");
      _add("00189027", "CS", "Spatial Pre-saturation", "1");
      _add("00189028", "CS", "Tagging", "1");
      _add("00189029", "CS", "Oversampling Phase", "1");
      _add("00189030", "FD", "Tag Spacing First Dimension", "1");
      _add("00189032", "CS", "Geometry of k-Space Traversal", "1");
      _add("00189033", "CS", "Segmented k-Space Traversal", "1");
      _add("00189034", "CS", "Rectilinear Phase Encode Reordering", "1");
      _add("00189035", "FD", "Tag Thickness", "1");
      _add("00189036", "CS", "Partial Fourier Direction", "1");
      _add("00189037", "CS", "Gating Synchronization Technique", "1");
      _add("00189041", "LO", "Receive Coil Manufacturer Name", "1");
      _add("00189042", "SQ", "MR Receive Coil Sequence", "1");
      _add("00189043", "CS", "Receive Coil Type", "1");
      _add("00189044", "CS", "Quadrature Receive Coil", "1");
      _add("00189045", "SQ", "Multi-Coil Definition Sequence", "1");
      _add("00189046", "LO", "Multi-Coil Configuration", "1");
      _add("00189047", "SH", "Multi-Coil Element Name", "1");
      _add("00189048", "CS", "Multi-Coil Element Used", "1");
      _add("00189049", "SQ", "MR Transmit Coil Sequence", "1");
      _add("00189050", "LO", "Transmit Coil Manufacturer Name", "1");
      _add("00189051", "CS", "Transmit Coil Type", "1");
      _add("00189052", "FD", "Spectral Width", "1-2");
      _add("00189053", "FD", "Chemical Shift Reference", "1-2");
      _add("00189054", "CS", "Volume Localization Technique", "1");
      _add("00189058", "US", "MR Acquisition Frequency Encoding Steps", "1");
      _add("00189059", "CS", "De-coupling", "1");
      _add("00189060", "CS", "De-coupled Nucleus", "1-2");
      _add("00189061", "FD", "De-coupling Frequency", "1-2");
      _add("00189062", "CS", "De-coupling Method", "1");
      _add("00189063", "FD", "De-coupling Chemical Shift Reference", "1-2");
      _add("00189064", "CS", "k-space Filtering", "1");
      _add("00189065", "CS", "Time Domain Filtering", "1-2");
      _add("00189066", "US", "Number of Zero fills", "1-2");
      _add("00189067", "CS", "Baseline Correction", "1");
      _add("00189070", "FD", "Cardiac R-R Interval Specified", "1");
      _add("00189073", "FD", "Acquisition Duration", "1");
      _add("00189074", "DT", "Frame Acquisition Datetime", "1");
      _add("00189075", "CS", "Diffusion Directionality", "1");
      _add("00189076", "SQ", "Diffusion Gradient Direction Sequence", "1");
      _add("00189077", "CS", "Parallel Acquisition", "1");
      _add("00189078", "CS", "Parallel Acquisition Technique", "1");
      _add("00189079", "FD", "Inversion Times", "1-n");
      _add("00189080", "ST", "Metabolite Map Description", "1");
      _add("00189081", "CS", "Partial Fourier", "1");
      _add("00189082", "FD", "Effective Echo Time", "1");
      _add("00189084", "SQ", "Chemical Shift Sequence", "1");
      _add("00189085", "CS", "Cardiac Signal Source", "1");
      _add("00189087", "FD", "Diffusion b-value", "1");
      _add("00189089", "FD", "Diffusion Gradient Orientation", "3");
      _add("00189090", "FD", "Velocity Encoding Direction", "3");
      _add("00189091", "FD", "Velocity Encoding Minimum Value", "1");
      _add("00189093", "US", "Number of k-Space Trajectories", "1");
      _add("00189094", "CS", "Coverage of k-Space", "1");
      _add("00189095", "UL", "Spectroscopy Acquisition Phase Rows", "1");
      _add("00189096", "FD", "Parallel Reduction Factor In-plane", "1");
      _add("00189098", "FD", "Transmitter Frequency", "1-2");
      _add("00189100", "CS", "Resonant Nucleus", "1-2");
      _add("00189101", "CS", "Frequency Correction", "1");
      _add("00189103", "SQ", "MR Spectroscopy FOV/Geometry Sequence", "1");
      _add("00189104", "FD", "Slab Thickness", "1");
      _add("00189105", "FD", "Slab Orientation", "3");
      _add("00189106", "FD", "Mid Slab Position", "3");
      _add("00189107", "SQ", "MR Spatial Saturation Sequence", "1");
      _add("00189112", "SQ", "MR Timing and Related Parameters Sequence", "1");
      _add("00189114", "SQ", "MR Echo Sequence", "1");
      _add("00189115", "SQ", "MR Modifier Sequence", "1");
      _add("00189117", "SQ", "MR Diffusion Sequence", "1");
      _add("00189118", "SQ", "Cardiac Trigger Sequence", "1");
      _add("00189119", "SQ", "MR Averages Sequence", "1");
      _add("00189125", "SQ", "MR FOV/Geometry Sequence", "1");
      _add("00189127", "UL", "Spectroscopy Acquisition Data Columns", "1");
      _add("00189126", "SQ", "Volume Localization Sequence", "1");
      _add("00189147", "CS", "Diffusion Anisotropy Type", "1");
      _add("00189151", "DT", "Frame Reference Datetime", "1");
      _add("00189152", "SQ", "Metabolite Map Sequence", "1");
      _add("00189155", "FD", "Parallel Reduction Factor out-of-plane", "1");
      _add("00189159", "UL",
	   "Spectroscopy Acquisition Out-of-plane Phase Steps", "1");
      _add("00189166", "CS", "Bulk Motion Status", "1");
      _add("00189168", "FD", "Parallel Reduction Factor Second In-plane", "1");
      _add("00189169", "CS", "Cardiac Beat Rejection Technique", "1");
      _add("00189170", "CS", "Respiratory Motion Compensation", "1");
      _add("00189171", "CS", "Respiratory Signal Source", "1");
      _add("00189172", "CS", "Bulk Motion Compensation Technique", "1");
      _add("00189173", "CS", "Bulk Motion Signal", "1");
      _add("00189174", "CS", "Applicable Safety Standard Agency", "1");
      _add("00189175", "LO", "Applicable Safety Standard Version", "1");
      _add("00189176", "SQ", "Operation Mode Sequence", "1");
      _add("00189177", "CS", "Operating Mode Type", "1");
      _add("00189178", "CS", "Operation Mode", "1");
      _add("00189179", "CS", "Specific Absorption Rate Definition", "1");
      _add("00189180", "CS", "Gradient Output Type", "1");
      _add("00189181", "FD", "Specific Absorption Rate Value", "1");
      _add("00189182", "FD", "Gradient Output", "1");
      _add("00189183", "CS", "Flow Compensation Direction", "1");
      _add("00189184", "FD", "Tagging Delay", "1");
      _add("00189195", "FD", "Chemical Shifts Minimum Integration Limit", "1");
      _add("00189196", "FD", "Chemical Shifts Maximum Integration Limit", "1");
      _add("00189197", "SQ", "MR Velocity Encoding Sequence", "1");
      _add("00189198", "CS", "First Order Phase Correction", "1");
      _add("00189199", "CS", "Water Referenced Phase Correction", "1");
      _add("00189200", "CS", "MR Spectroscopy Acquisition Type", "1");
      _add("00189214", "CS", "Respiratory Motion Status", "1");
      _add("00189217", "FD", "Velocity Encoding Maximum Value", "1");
      _add("00189218", "SS", "Tag Spacing Second Dimension", "1");
      _add("00189219", "SS", "Tag Angle Second Axis", "1");
      _add("00189220", "FD", "Frame Acquisition Duration", "1");
      _add("00189226", "SQ", "MR Image Frame Type Sequence", "1");
      _add("00189227", "SQ", "MR Spectroscopy Frame Type Sequence", "1");
      _add("00189231", "US", "MR Acquisition Phase Encoding Steps in-plane",
	   "1");
      _add("00189232", "US",
	   "MR Acquisition Phase Encoding Steps out-of-plane", "1");
      _add("00189234", "UL", "Spectroscopy Acquisition Phase Columns", "1");
      _add("00189236", "CS", "Cardiac Motion Status", "1");
      _add("00189239", "SQ", "Specific Absorption Rate Sequence", "1");

      // Group 20 Elements
      _add("00200000", "UL", "Group Length", "1");
      _add("0020000D", "UI", "Study Instance UID", "1");
      _add("0020000E", "UI", "Series Instance UID", "1");
      _add("00200010", "SH", "Study ID", "1");
      _add("00200011", "IS", "Series Number", "1");
      _add("00200012", "IS", "Acquisition Number", "1");
      _add("00200013", "IS", "Image Number", "1");
      _add("00200020", "CS", "Patient Orientation", "2");
      _add("00200022", "IS", "Overlay Number", "1");
      _add("00200024", "IS", "Curve Number", "1");
      _add("00200026", "IS", "Lookup Table Number", "1");
      _add("00200032", "DS", "Image Position (Patient)", "3");
      _add("00200037", "DS", "Image Orientation (Patient)", "6");
      _add("00200052", "UI", "Frame of Reference UID", "1");
      _add("00200060", "CS", "Laterality", "1");
      _add("00200100", "IS", "Temporal Position Identifier", "1");
      _add("00200105", "IS", "Number of Temporal Positions", "1");
      _add("00200110", "DS", "Temporal Resolution", "1");
      _add("00201000", "IS", "Series in Study", "1");
      _add("00201002", "IS", "Images in Acquisition", "1");
      _add("00201004", "IS", "Acquisitions in Study", "1");
      _add("00201040", "LO", "Position Reference Indicator", "1");
      _add("00201041", "DS", "Slice Location", "1");
      _add("00201070", "IS", "Other Study Numbers", "1-n");
      _add("00201200", "IS", "Number of Patient Related Studies", "1");
      _add("00201202", "IS", "Number of Patient Related Series", "1");
      _add("00201204", "IS", "Number of Patient Related Images", "1");
      _add("00201206", "IS", "Number of Study Related Series", "1");
      _add("00201208", "IS", "Number of Study Related Images", "1");
      _add("00201209", "IS", "Number of Series Related Images", "1");
      _add("00204000", "LT", "Image Comments", "1");
      _add("00209056", "SH", "Stack ID", "1");
      _add("00209057", "UL", "In-Stack Position Number", "1");
      _add("00209071", "SQ", "Frame Anatomy Sequence", "1");
      _add("00209072", "CS", "Frame Laterality", "1");
      _add("00209111", "SQ", "Frame Content Sequence", "1");
      _add("00209113", "SQ", "Plane Position Sequence", "1");
      _add("00209116", "SQ", "Plane Orientation Sequence", "1");
      _add("00209128", "UL", "Temporal Position Index", "1");
      _add("00209153", "FD", "Trigger Delay Time", "1");
      _add("00209156", "US", "Frame Acquisition Number", "1");
      _add("00209157", "UL", "Dimension Index Values", "1-n");
      _add("00209158", "LT", "Frame Comments", "1");
      _add("00209161", "UI", "Concatenation UID", "1");
      _add("00209162", "US", "In-concatenation Number", "1");
      _add("00209163", "US", "In-concatenation Total Number", "1");
      _add("00209164", "UI", "Dimension Organization UID", "1");
      _add("00209165", "AT", "Dimension Index Pointer", "1");
      _add("00209167", "AT", "Functional Group Sequence Pointer", "1");
      _add("00209213", "LO", "Dimension Index Private Creator", "1");
      _add("00209221", "SQ", "Dimension Organization Sequence", "1");
      _add("00209222", "SQ", "Dimension Sequence", "1");
      _add("00209228", "UL", "Concatenation Frame Offset Number", "1");
      _add("00209238", "LO", "Functional Group Private Creator", "1");

      // Group 28 Elements
      _add("00280000", "UL", "Group Length", "1");
      _add("00280002", "US", "Samples per Pixel", "1");
      _add("00280004", "CS", "Photometric Interpretation", "1");
      _add("00280006", "US", "Planar Configuration", "1");
      _add("00280008", "IS", "Number of Frames", "1");
      _add("00280009", "AT", "Frame Increment Pointer", "1-n");
      _add("00280010", "US", "Rows", "1");
      _add("00280011", "US", "Columns", "1");
      _add("00280012", "US", "Planes", "1");
      _add("00280014", "US", "Ultrasound Color Data Present", "1");
      _add("00280030", "DS", "Pixel Spacing", "2");
      _add("00280031", "DS", "Zoom Factor", "2");
      _add("00280032", "DS", "Zoom Center", "2");
      _add("00280034", "IS", "Pixel Aspect Ratio", "2");
      _add("00280051", "CS", "Corrected Image", "1-n");
      _add("00280100", "US", "Bits Allocated", "1");
      _add("00280101", "US", "Bits Stored", "1");
      _add("00280102", "US", "High Bit", "1");
      _add("00280103", "US", "Pixel Representation", "1");
      _add("00280106", "US or SS", "Smallest Image Pixel Value", "1");
      _add("00280107", "US or SS", "Largest Image Pixel Value", "1");
      _add("00280108", "US or SS", "Smallest Pixel Value in Series", "1");
      _add("00280109", "US or SS", "Largest Pixel Value in Series", "1");
      _add("00280110", "US or SS", "Smallest Image Pixel Value in Plane", "1");
      _add("00280111", "US or SS", "Largest Image Pixel Value in Plane", "1");
      _add("00280120", "US or SS", "Pixel Padding Value", "1");
      _add("00281040", "CS", "Pixel Intensity Relationship", "1");
      _add("00281050", "DS", "Window Center", "1-n");
      _add("00281051", "DS", "Window Width", "1-n");
      _add("00281052", "DS", "Rescale Intercept", "1");
      _add("00281053", "DS", "Rescale Slope", "1");
      _add("00281054", "LO", "Rescale Type", "1");
      _add("00281055", "LO", "Window Center & Width Explanation LO", "1-n");
      _add("00281090", "CS", "Recommended Viewing Mode", "1");
      _add("00281101", "US/US or SS/US",
	   "Red Palette Color Lookup Table Descriptor", "3");
      _add("00281102", "US/US or SS/US",
	   "Green Palette Color Lookup Table Descriptor", "3");
      _add("00281103", "US/US or SS/US",
	   "Blue Palette Color Lookup Table Descriptor", "3");
      _add("00281199", "UI", "Palette Color Lookup Table UID", "1");
      _add("00281201", "US or SS / OW",
	   "Red Palette Color Lookup Table Data", "1-n / 1");
      _add("00281202", "US or SS / OW",
	   "Green Palette Color Lookup Table Data", "1-n / 1");
      _add("00281203", "US or SS / OW",
	   "Blue Palette Color Lookup Table Data", "1-n / 1");
      _add("00281221", "OW",
	   "Segmented Red Palette Color Lookup Table Data", "1");
      _add("00281222", "OW",
	   "Segmented Green Palette Color Lookup Table Data", "1");
      _add("00281223", "OW",
	   "Segmented Blue Palette Color Lookup Table Data", "1");
      _add("00282110", "CS", "Lossy Image Compression", "1");
      _add("00283000", "SQ", "Modality LUT Sequence", "1");
      _add("00283002", "US/US or SS/US", "LUT Descriptor", "3");
      _add("00283003", "LO", "LUT Explanation", "1");
      _add("00283004", "LO", "Modality LUT Type", "1");
      _add("00283006", "US or SS", "LUT Data", "1-n");
      _add("00283010", "SQ", "VOI LUT Sequence", "1");
      _add("00285000", "SQ", "Bi-Plane Acquisition Sequence", "1");
      _add("00286010", "US", "Representative Frame Number", "1");
      _add("00286020", "US", "Frame Numbers of Interest (FOI)", "1-n");
      _add("00286022", "LO", "Frame(s) of Interest Description", "1-n");
      _add("00286030", "US", "Mask Pointer(s)", "1-n");
      _add("00286040", "US", "R Wave Pointer", "1-n");
      _add("00286100", "SQ", "Mask Subtraction Sequence", "1");
      _add("00286101", "CS", "Mask Operation", "1");
      _add("00286102", "US", "Applicable Frame Range", "2-2n");
      _add("00286110", "US", "Mask Frame Numbers", "1-n");
      _add("00286112", "US", "Contrast Frame Averaging", "1");
      _add("00286114", "FL", "Mask Sub-pixel Shift", "2");
      _add("00286120", "SS", "TID Offset", "1");
      _add("00286190", "ST", "Mask Operation Explanation", "1");
      _add("00289001", "UL", "Data Point Rows", "1");
      _add("00289002", "UL", "Data Point Columns", "1");
      _add("00289003", "CS", "Signal Domain", "1-2");
      _add("00289099", "US", "Largest Monochrome Pixel Value", "1");
      _add("00289108", "CS", "Data Representation", "1");
      _add("00289110", "SQ", "Pixel Matrix Sequence", "1");
      _add("00289132", "SQ", "Frame VOI LUT Sequence", "1");
      _add("00289145", "SQ", "Pixel Value Transformation Sequence", "1");
      _add("00289235", "CS", "Signal Domain Rows", "1");

      // Group 32 Elements
      _add("00320000", "UL", "Group Length", "1");
      _add("0032000A", "CS", "Study Status ID", "1");
      _add("0032000C", "CS", "Study Priority ID", "1");
      _add("00320012", "LO", "Study ID Issuer", "1");
      _add("00320032", "DA", "Study Verified Date", "1");
      _add("00320033", "TM", "Study Verified Time", "1");
      _add("00320034", "DA", "Study Read Date", "1");
      _add("00320035", "TM", "Study Read Time", "1");
      _add("00321000", "DA", "Scheduled Study Start Date", "1");
      _add("00321001", "TM", "Scheduled Study Start Time", "1");
      _add("00321010", "DA", "Scheduled Study Stop Date", "1");
      _add("00321011", "TM", "Scheduled Study Stop Time", "1");
      _add("00321020", "LO", "Scheduled Study Location", "1");
      _add("00321021", "AE", "Scheduled Study Location AE Title(s)", "1-n");
      _add("00321030", "LO", "Reason for Study", "1");
      _add("00321032", "PN", "Requesting Physician", "1");
      _add("00321033", "LO", "Requesting Service", "1");
      _add("00321040", "DA", "Study Arrival Date", "1");
      _add("00321041", "TM", "Study Arrival Time", "1");
      _add("00321050", "DA", "Study Completion Date", "1");
      _add("00321051", "TM", "Study Completion Time", "1");
      _add("00321055", "CS", "Study Component Status ID", "1");
      _add("00321060", "LO", "Requested Procedure Description", "1");
      _add("00321064", "SQ", "Requested Procedure Code Sequence", "1");
      _add("00321070", "LO", "Requested Contrast Agent", "1");
      _add("00324000", "LT", "Study Comments", "1");

      // Group 38 Elements
      _add("00380000", "UL", "Group Length", "1");
      _add("00380004", "SQ", "Referenced Patient Alias Sequence", "1");
      _add("00380008", "CS", "Visit Status ID", "1");
      _add("00380010", "LO", "Admission ID", "1");
      _add("00380011", "LO", "Issuer of Admission ID", "1");
      _add("00380016", "LO", "Route of Admissions", "1");
      _add("0038001A", "DA", "Scheduled Admission Date", "1");
      _add("0038001B", "TM", "Scheduled Admission Time", "1");
      _add("0038001C", "DA", "Scheduled Discharge Date", "1");
      _add("0038001D", "TM", "Scheduled Discharge Time", "1");
      _add("0038001E", "LO", "Scheduled Patient Institution Residence", "1");
      _add("00380020", "DA", "Admitting Date", "1");
      _add("00380021", "TM", "Admitting Time", "1");
      _add("00380030", "DA", "Discharge Date", "1");
      _add("00380032", "TM", "Discharge Time", "1");
      _add("00380040", "LO", "Discharge Diagnosis Description", "1");
      _add("00380044", "SQ", "Discharge Diagnosis Code Sequence", "1");
      _add("00380050", "LO", "Special Needs", "1");
      _add("00380300", "LO", "Current Patient Location", "1");
      _add("00380400", "LO", "Patient's Institution Residence", "1");
      _add("00380500", "LO", "Patient State", "1");
      _add("00384000", "LT", "Visit Comments", "1");

      // Group 40 Elements
      _add("00400000", "UL", "Group Length", "1");
      _add("00400001", "AE", "Scheduled Station AE Title", "1-n");
      _add("00400002", "DA", "Scheduled Procedure Step Start Date", "1");
      _add("00400003", "TM", "Scheduled Procedure Step Start Time", "1");
      _add("00400004", "DA", "Scheduled Procedure Step End Date", "1");
      _add("00400005", "TM", "Scheduled Procedure Step End Time", "1");
      _add("00400006", "PN", "Scheduled Performing Physician's Name", "1");
      _add("00400007", "LO", "Scheduled Procedure Step Description", "1");
      _add("00400008", "SQ", "Scheduled Action Item Code Sequence", "1");
      _add("00400009", "SH", "Scheduled Procedure Step ID", "1");
      _add("00400010", "SH", "Scheduled Station Name", "1-n");
      _add("00400011", "SH", "Scheduled Procedure Step Location", "1");
      _add("00400012", "LO", "Pre-Medication", "1");
      _add("00400020", "CS", "Scheduled Procedure Step Status", "1");
      _add("00400100", "SQ", "Scheduled Procedure Step Sequence", "1");
      _add("00400220", "SQ", "Referenced Standalone SOP Instance Sequence",
	   "1");
      _add("00400241", "AE", "Performed Station AE Title", "1");
      _add("00400242", "SH", "Performed Station Name", "1");
      _add("00400243", "SH", "Performed Location", "1");
      _add("00400244", "DA", "Performed Procedure Step Start Date", "1");
      _add("00400245", "TM", "Performed Procedure Step Start Time", "1");
      _add("00400250", "DA", "Performed Procedure Step End Date", "1");
      _add("00400251", "TM", "Performed Procedure Step End Time", "1");
      _add("00400252", "CS", "Performed Procedure Step Status", "1");
      _add("00400253", "CS", "Performed Procedure Step ID", "1");
      _add("00400254", "LO", "Performed Procedure Step Description", "1");
      _add("00400255", "LO", "Performed Procedure Type Description", "1");
      _add("00400260", "SQ", "Performed Action Item Sequence", "1");
      _add("00400270", "SQ", "Scheduled Step Attributes Sequence", "1");
      _add("00400275", "SQ", "Request Attributes Sequence", "1");
      _add("00400280", "ST", "Comments on the Performed Procedure Steps", "1");
      _add("00400293", "SQ", "Quantity Sequence", "1");
      _add("00400294", "DS", "Quantity", "1");
      _add("00400295", "SQ", "Measuring Units Sequence", "1");
      _add("00400296", "SQ", "Billing Item Sequence", "1");
      _add("00400300", "US", "Total Time of Fluoroscopy", "1");
      _add("00400301", "US", "Total Number of Exposures", "1");
      _add("00400302", "US", "Entrance Dose", "1");
      _add("00400303", "US", "Exposed Area", "1-2");
      _add("00400306", "DS", "Distance Source to Entrance", "1");
      _add("00400310", "ST", "Comments on Radiation Dose", "1");
      _add("00400320", "SQ", "Billing Procedure Step Sequence", "1");
      _add("00400321", "SQ", "Film Consumption Sequence", "1");
      _add("00400324", "SQ", "Billing Supplies and Devices Sequence", "1");
      _add("00400330", "SQ", "Referenced Procedure Step Sequence", "1");
      _add("00400340", "SQ", "Performed Series Sequence", "1");
      _add("00400400", "LT", "Comments on the Scheduled Procedure Step", "1");
      _add("00401001", "SH", "Requested Procedure ID", "1");
      _add("00401002", "LO", "Reason for the Requested Procedure", "1");
      _add("00401003", "SH", "Requested Procedure Priority", "1");
      _add("00401004", "LO", "Patient Transport Arrangements", "1");
      _add("00401005", "LO", "Requested Procedure Location", "1");
      _add("00401006", "SH", "Placer Order Number / Procedure", "1");
      _add("00401007", "SH", "Filler Order Number / Procedure", "1");
      _add("00401008", "LO", "Confidentiality Code", "1");
      _add("00401009", "SH", "Reporting Priority", "1");
      _add("00401010", "PN", "Names of Intended Recipients of Results", "1-n");
      _add("00401400", "LT", "Requested Procedure Comments", "1");
      _add("00402001", "LO", "Reason for the Imaging Service Request", "1");
      _add("00402004", "DA", "Issue Date of Imaging Service Request", "1");
      _add("00401005", "LO", "Issue Time of Imaging Service Request", "1");
      _add("00401006", "SH", "Placer Order Number / Imaging Service Request",
	   "1");
      _add("00401007", "SH", "Filler Order Number / Imaging Service Request",
	   "1");
      _add("00402008", "PN", "Order Entered By", "1");
      _add("00402009", "SH", "Order Enterer's Location", "1");
      _add("00402010", "SH", "Order Callback Phone Number", "1");
      _add("00402400", "LT", "Imaging Service Request Comments", "1");
      _add("00403001", "LO",
	   "Confidentiality Constraint on Patient Data Description", "1");
      _add("00409096", "SQ", "Real World Value Mapping Sequence", "1");
      _add("00409210", "SS", "LUT Label", "1");
      _add("00409211", "US/SS", "Real World Value LUT Last Value Mapped", "1");
      _add("00409212", "FD", "Real World Value LUT Data", "1-n");
      _add("00409216", "US/SS", "Real World Value LUT First Value Mapped",
	   "1");
      _add("00409224", "FD", "Real World Value Intercept", "1");
      _add("00409225", "FD", "Real World Value Slope", "1");

      // Group 50 Elements
      _add("00500000", "UL", "Group Length", "1");
      _add("00500004", "CS", "Calibration Image", "1");
      _add("00500010", "SQ", "Device Sequence", "1");
      _add("00500014", "DS", "Device Length", "1");
      _add("00500016", "DS", "Device Diameter", "1");
      _add("00500017", "CS", "Device Diameter Units", "1");
      _add("00500018", "DS", "Device Volume", "1");
      _add("00500019", "DS", "Inter-marker Distance", "1");
      _add("00500020", "LO", "Device Description", "1");

      // Group 54 Elements
      _add("00540000", "UL", "Group Length", "1");
      _add("00540010", "US", "Energy Window Vector", "1-n");
      _add("00540011", "US", "Number of Energy Windows", "1");
      _add("00540012", "SQ", "Energy Window Information Sequence", "1");
      _add("00540013", "SQ", "Energy Window Range Sequence", "1");
      _add("00540014", "DS", "Energy Window Lower Limit", "1");
      _add("00540015", "DS", "Energy Window Upper Limit", "1");
      _add("00540016", "SQ", "Radiopharmaceutical Information Sequence", "1");
      _add("00540017", "IS", "Residual Syringe Counts", "1");
      _add("00540018", "SH", "Energy Window Name", "1");
      _add("00540020", "US", "Detector Vector", "1-n");
      _add("00540021", "US", "Number of Detectors", "1");
      _add("00540022", "SQ", "Detector Information Sequence", "1");
      _add("00540030", "US", "Phase Vector", "1-n");
      _add("00540031", "US", "Number of Phases", "1");
      _add("00540032", "SQ", "Phase Information Sequence", "1");
      _add("00540033", "US", "Number of Frames in Phase", "1");
      _add("00540036", "IS", "Phase Delay", "1");
      _add("00540038", "IS", "Pause Between Frames", "1");
      _add("00540050", "US", "Rotation Vector", "1-n");
      _add("00540051", "US", "Number of Rotations", "1");
      _add("00540052", "SQ", "Rotation Information Sequence", "1");
      _add("00540053", "US", "Number of Frames in Rotation", "1");
      _add("00540060", "US", "R-R Interval Vector", "1-n");
      _add("00540061", "US", "Number of R-R Intervals", "1");
      _add("00540062", "SQ", "Gated Information Sequence", "1");
      _add("00540063", "SQ", "Data Information Sequence", "1");
      _add("00540070", "US", "Time Slot Vector", "1-n");
      _add("00540071", "US", "Number of Time Slots", "1");
      _add("00540072", "SQ", "Time Slot Information Sequence", "1");
      _add("00540073", "DS", "Time Slot Time", "1");
      _add("00540080", "US", "Slice Vector", "1-n");
      _add("00540081", "US", "Number of Slices", "1");
      _add("00540090", "US", "Angular View Vector", "1-n");
      _add("00540100", "US", "Time Slice Vector", "1-n");
      _add("00540101", "US", "Number of Time Slices", "1");
      _add("00540200", "DS", "Start Angle", "1");
      _add("00540202", "CS", "Type of Detector Motion", "1");
      _add("00540210", "IS", "Trigger Vector", "1-n");
      _add("00540211", "US", "Number of Triggers in Phase", "1");
      _add("00540220", "SQ", "View Code Sequence", "1");
      _add("00540222", "SQ", "View Angulation Modifier Code Sequence", "1");
      _add("00540300", "SQ", "Radionuclide Code Sequence", "1");
      _add("00540302", "SQ", "Administration Route Code Sequence", "1");
      _add("00540304", "SQ", "Radiopharmaceutical Code Sequence", "1");
      _add("00540306", "SQ", "Calibration Data Sequence", "1");
      _add("00540308", "US", "Energy Window Number", "1");
      _add("00540400", "SH", "Image ID", "1");
      _add("00540410", "SQ", "Patient Orientation Code Sequence", "1");
      _add("00540412", "SQ", "Patient Orientation Modifier Code Sequence",
	   "1");
      _add("00540414", "SQ", "Patient Gantry Relationship Code Sequence", "1");
      _add("00541000", "CS", "Series Type", "2");
      _add("00541001", "CS", "Units", "1");
      _add("00541002", "CS", "Counts Source", "1");
      _add("00541004", "CS", "Reprojection Method", "1");
      _add("00541100", "CS", "Randoms Correction Method", "1");
      _add("00541101", "LO", "Attenuation Correction Method", "1");
      _add("00541102", "CS", "Decay Correction", "1");
      _add("00541103", "LO", "Reconstruction Method", "1");
      _add("00541105", "LO", "Scatter Correction Method", "1");
      _add("00541200", "DS", "Axial Acceptance", "1");
      _add("00541201", "IS", "Axial Mash", "2");
      _add("00541202", "IS", "Transverse Mash", "1");
      _add("00541203", "DS", "Detector Element Size", "2");
      _add("00541210", "DS", "Coincidence Window Width", "1");
      _add("00541220", "CS", "Secondary Counts Type", "1-n");
      _add("00541300", "DS", "Frame Reference Time", "1");
      _add("00541310", "IS", "Primary (Prompts) Counts Accumulated", "1");
      _add("00541311", "IS", "Secondary Counts Accumulated", "1-n");
      _add("00541320", "DS", "Slice Sensitivity Factor", "1");
      _add("00541321", "DS", "Decay Factor", "1");
      _add("00541322", "DS", "Dose Calibration Factor", "1");
      _add("00541323", "DS", "Scatter Fraction Factor", "1");
      _add("00541324", "DS", "Dead Time Factor", "1");
      _add("00541330", "US", "Image Index", "1");
      _add("00541400", "CS", "Counts Included", "1-n");
      _add("00541401", "CS", "Dead Time Correction Flag", "1");

      // Group 88 Elements
      _add("00880000", "UL", "Group Length", "1");
      _add("00880130", "SH", "Storage Media File-set ID", "1");
      _add("00880140", "UI", "Storage Media File-set UID", "1");
      _add("00880200", "SQ", "Icon Image Sequence", "1");
      _add("00880904", "LO", "Topic Title", "1");
      _add("00880906", "ST", "Topic Subject", "1");
      _add("00880910", "LO", "Topic Author", "1");
      _add("00880912", "LO", "Topic Key Words", "1-32");

      // Group 2000 Elements
      _add("20000000", "UL", "Group Length", "1");
      _add("00880130", "SH", "Number of Copies", "1");
      _add("20000020", "CS", "Print Priority", "1");
      _add("20000030", "CS", "Medium Type", "1");
      _add("20000040", "CS", "Film Destination", "1");
      _add("20000050", "LO", "Film Session Label", "1");
      _add("20000060", "IS", "Memory Allocation", "1");
      _add("20000062", "CS", "Color Image Printing Flag", "1");
      _add("20000063", "CS", "Collation Flag", "1");
      _add("20000065", "CS", "Annotation Flag", "1");
      _add("20000067", "CS", "Image Overlay Flag", "1");
      _add("20000069", "CS", "Presentation LUT Flag", "1");
      _add("2000006A", "CS", "Image Box Presentation LUT Flag", "1");
      _add("20000500", "SQ", "Referenced Film Box Sequence", "1");
      _add("20000510", "SQ", "Referenced Stored Print Sequence", "1");

      // Group 2010 Elements
      _add("20100000", "UL", "Group Length", "1");
      _add("20100010", "ST", "Image Display Format", "1");
      _add("20100030", "CS", "Annotation Display Format ID", "1");
      _add("20100040", "CS", "Film Orientation", "1");
      _add("20100050", "CS", "Film Size ID", "1");
      _add("20100060", "CS", "Magnification Type", "1");
      _add("20100080", "CS", "Smoothing Type", "1");
      _add("20100100", "CS", "Border Density", "1");
      _add("20100110", "CS", "Empty Image Density", "1");
      _add("20100120", "US", "Min Density", "1");
      _add("20100130", "US", "Max Density", "1");
      _add("20100140", "CS", "Trim", "1");
      _add("20100150", "ST", "Configuration Information", "1");
      _add("2010015E", "US", "Illumination", "1");
      _add("20100160", "US", "Reflected Ambient Light", "1");
      _add("20100500", "SQ", "Referenced Film Session Sequence", "1");
      _add("20100510", "SQ", "Referenced Image Box Sequence", "1");
      _add("20100520", "SQ", "Referenced Basic Annotation Box Sequence", "1");


      // Group 2020 Elements
      _add("20200000", "UL", "Group Length", "1");
      _add("20200010", "US", "Image Position", "1");
      _add("20200020", "CS", "Polarity", "1");
      _add("20200030", "DS", "Requested Image Size", "1");
      _add("20200110", "SQ", "Basic Grayscale Image Sequence", "1");
      _add("20200111", "SQ", "Basic Color Image Sequence", "1");
      _add("20200130", "SQ", "Referenced Image Overlay Box Sequence", "1");
      _add("20200140", "SQ", "Referenced VOI LUT Box Sequence", "1");

      // Group 2030 Elements
      _add("20300000", "UL", "Group Length", "1");
      _add("20300010", "US", "Annotation Position", "1");
      _add("20300020", "LO", "Text String", "1");

      // Group 2040 Elements
      _add("20400000", "UL", "Group Length", "1");
      _add("20400010", "SQ", "Referenced Overlay Plane Sequence", "1");
      _add("20400011", "US", "Referenced Overlay Plane Groups", "1-99");
      _add("20400060", "CS", "Overlay Magnification Type", "1");
      _add("20400070", "CS", "Overlay Smoothing Type", "1");
      _add("20400080", "CS", "Overlay Foreground Density", "1");
      _add("20400090", "CS", "Overlay Mode", "1");
      _add("20400100", "CS", "Threshold Density", "1");
      
      // Group 2050 Elements
      _add("20500010", "SQ", "Presentation LUT Sequence", "1");
      _add("20500020", "CS", "Presentation LUT Shape", "1");
      _add("20500500", "SQ", "Referenced Presentation LUT Sequence", "1");

      // Group 2100 Elements
      _add("21000000", "UL", "Group Length", "1");
      _add("21000010", "SH", "Print Job ID", "1");
      _add("21000020", "CS", "Execution Status", "1");
      _add("21000030", "CS", "Execution Status Info", "1");
      _add("21000040", "DA", "Creation Date", "1");
      _add("21000050", "TM", "Creation Time", "1");
      _add("21000070", "AE", "Originator", "1");
      _add("21000140", "AE", "Destination AE", "1");
      _add("21000160", "SH", "Owner ID", "1");
      _add("21000170", "IS", "Number of Films", "1");
      _add("21000500", "SQ", "Referenced Print Job Sequence", "1");

      // Group 2110 Elements
      _add("21100000", "UL", "Group Length", "1");
      _add("21100010", "CS", "Printer Status", "1");
      _add("21100020", "CS", "Printer Status Info", "1");
      _add("21100030", "LO", "Printer Name", "1");
      _add("21100099", "SH", "Print Queue ID", "1");

      // Group 2120 Elements
      _add("21200010", "CS", "Queue Status", "1");
      _add("21200050", "SQ", "Print Job Description Sequence", "1");
      _add("21200070", "SQ", "Referenced Print Job Sequence", "1");

      // Group 2130 Elements
      _add("21300010", "SQ", "Print Management Capabilities Sequence", "1");
      _add("21300015", "SQ", "Printer Characteristics Sequence", "1");
      _add("21300030", "SQ", "Film Box Content Sequence", "1");
      _add("21300040", "SQ", "Image Box Content Sequence", "1");
      _add("21300050", "SQ", "Annotation Content Sequence", "1");
      _add("21300060", "SQ", "Image Overlay Box Content Sequence", "1");
      _add("21300080", "SQ", "Presentation LUT Content Sequence", "1");
      _add("213000A0", "SQ", "Proposed Study Sequence", "1");
      _add("213000C0", "SQ", "Original Image Sequence", "1");

      // Group 3002 Elements
      _add("30020000", "UL", "Group Length", "1");
      _add("30020002", "SH", "RT Image Label", "1");
      _add("30020003", "LO", "RT Image Name", "1");
      _add("30020004", "ST", "RT Image Description", "1");
      _add("3002000A", "CS", "Reported Values Origin", "1");
      _add("3002000C", "CS", "RT Image Plane", "1");
      _add("3002000E", "DS", "X-Ray Image Receptor Angle", "1");
      _add("30020010", "DS", "RT Image Orientation", "1");
      _add("30020011", "DS", "Image Plane Pixel Spacing", "1");
      _add("30020012", "DS", "RT Image Position", "1");
      _add("30020020", "SH", "Radiation Machine Name", "1");
      _add("30020022", "DS", "Radiation Machine SAD", "1");
      _add("30020024", "DS", "Radiation Machine SSD", "1");
      _add("30020026", "DS", "RT Image SID", "1");
      _add("30020028", "DS", "Source to Reference Object Distance", "1");
      _add("30020029", "IS", "Fraction Number", "1");
      _add("30020030", "SQ", "Exposure Sequence", "1");
      _add("30020032", "DS", "Meterset Exposure", "1");

      // Group 3004 Elements
      _add("30040000", "UL", "Group Length", "1");
      _add("30040001", "CS", "DVH Type", "1");
      _add("30040002", "CS", "Dose Units", "1");
      _add("30040004", "CS", "Dose Type", "1");
      _add("30040006", "LO", "Dose Comment", "1");
      _add("30040008", "DS", "Normalization Point", "3");
      _add("3004000A", "CS", "Dose Summation Type", "1");
      _add("3004000C", "DS", "Grid Frame Offset Vector", "2-n");
      _add("3004000E", "DS", "Dose Grid Scaling", "1");
      _add("30040010", "SQ", "RT Dose ROI Sequence", "1");
      _add("30040012", "DS", "Dose Value", "1");
      _add("30040040", "DS", "DVH Normalization Point", "3");
      _add("30040042", "DS", "DVH Normalization Dose Value", "1");
      _add("30040050", "SQ", "DVH Sequence", "1");
      _add("30040052", "DS", "DVH Dose Scaling", "1");
      _add("30040054", "CS", "DVH Volume Units", "1");
      _add("30040056", "IS", "DVH Number of Bins", "1");
      _add("30040058", "DS", "DVH Data", "2-2n");
      _add("30040060", "SQ", "DVH Referenced ROI Sequence", "1");
      _add("30040062", "CS", "DVH ROI Contribution Type", "1");
      _add("30040070", "DS", "DVH Minimum Dose", "1");
      _add("30040072", "DS", "DVH Maximum Dose", "1");
      _add("30040074", "DS", "DVH Mean Dose", "1");

      // Group 3006 Elements
      _add("30060000", "UL", "Group Length", "1");
      _add("30060002", "SH", "Structure Set Label", "1");
      _add("30060004", "LO", "Structure Set Name", "1");
      _add("30060006", "ST", "Structure Set Description", "1");
      _add("30060008", "DA", "Structure Set Date", "1");
      _add("30060009", "TM", "Structure Set Time", "1");
      _add("30060010", "SQ", "Referenced Frame of Reference Sequence", "1");
      _add("30060012", "SQ", "RT Referenced Study Sequence", "1");
      _add("30060014", "SQ", "RT Referenced Series Sequence", "1");
      _add("30060016", "SQ", "Contour Image Sequence", "1");
      _add("30060020", "SQ", "Structure Set ROI Sequence", "1");
      _add("30060022", "IS", "ROI Number", "1");
      _add("30060024", "UI", "Referenced Frame of Reference UID", "1");
      _add("30060026", "LO", "ROI Name", "1");
      _add("30060028", "ST", "ROI Description", "1");
      _add("3006002A", "IS", "ROI Display Color", "3");
      _add("3006002C", "DS", "ROI Volume", "1");
      _add("30060030", "SQ", "RT Related ROI Sequence", "1");
      _add("30060033", "CS", "RT ROI Relationship", "1");
      _add("30060036", "CS", "ROI Generation Algorithm", "1");
      _add("30060038", "LO", "ROI Generation Description", "1");
      _add("30060039", "SQ", "ROI Contour Sequence", "1");
      _add("30060040", "SQ", "Contour Sequence", "1");
      _add("30060042", "CS", "Contour Geometric Type", "1");
      _add("30060044", "DS", "Contour Slab Thickness", "1");
      _add("30060045", "DS", "Contour Offset Vector", "3");
      _add("30060046", "IS", "Number of Contour Points", "1");
      _add("30060050", "DS", "Contour Data", "3-3n");
      _add("30060080", "SQ", "RT ROI Observations Sequence", "1");
      _add("30060082", "IS", "Observation Number", "1");
      _add("30060084", "IS", "Referenced ROI Number", "1");
      _add("30060085", "SH", "ROI Observation Label", "1");
      _add("30060086", "SQ", "RT ROI Identification Code Sequence", "1");
      _add("30060088", "ST", "ROI Observation Description", "1");
      _add("300600A0", "SQ", "Related RT ROI Observations Sequence", "1");
      _add("300600A4", "CS", "RT ROI Interpreted Type", "1");
      _add("300600A6", "PN", "ROI Interpreter", "1");
      _add("300600B0", "SQ", "ROI Physical Properties Sequence", "1");
      _add("300600B2", "CS", "ROI Physical Property", "1");
      _add("300600B4", "DS", "ROI Physical Property Value", "1");
      _add("300600C0", "SQ", "Frame of Reference Relationship Sequence", "1");
      _add("300600C2", "UI", "Related Frame of Reference UID", "1");
      _add("300600C4", "CS", "Frame of Reference Transformation Type", "1");
      _add("300600C6", "DS", "Frame of Reference Transformation Matrix", "16");
      _add("300600C8", "LO", "Frame of Reference Transformation Comment", "1");

      // Group 300A Elements
      _add("300A0000", "UL", "Group Length", "1");
      _add("300A0002", "SH", "RT Plan Label", "1");
      _add("300A0003", "LO", "RT Plan Name", "1");
      _add("300A0004", "ST", "RT Plan Description", "1");
      _add("300A0006", "DA", "RT Plan Date", "1");
      _add("300A0007", "TM", "RT Plan Time", "1");
      _add("300A0009", "LO", "Treatment Protocols", "1-n");
      _add("300A000A", "CS", "Treatment Intent" ,"1");
      _add("300A000B", "LO", "Treatment Sites", "1-n");
      _add("300A000C", "CS", "RT Plan Geometry", "1");
      _add("300A000E", "ST", "Prescription Description", "1");
      _add("300A0010", "SQ", "Dose Reference Sequence", "1");
      _add("300A0012", "IS", "Dose Reference Number", "1");
      _add("300A0014", "CS", "Dose Reference Structure Type", "1");
      _add("300A0016", "LO", "Dose Reference Description", "1");
      _add("300A0018", "DS", "Dose Reference Point Coordinates", "3");
      _add("300A001A", "DS", "Nominal Prior Dose", "1");
      _add("300A0020", "CS", "Dose Reference Type", "1");
      _add("300A0021", "DS", "Constraint Weight", "1");
      _add("300A0022", "DS", "Delivery Warning Dose", "1");
      _add("300A0023", "DS", "Delivery Maximum Dose", "1");
      _add("300A0025", "DS", "Target Minimum Dose", "1");
      _add("300A0026", "DS", "Target Prescription Dose", "1");
      _add("300A0027", "DS", "Target Maximum Dose", "1");
      _add("300A0028", "DS", "Target Underdose Volume Fraction", "1");
      _add("300A002A", "DS", "Organ at Risk Full-volume Dose", "1");
      _add("300A002B", "DS", "Organ at Risk Limit Dose", "1");
      _add("300A002C", "DS", "Organ at Risk Maximum Dose", "1");
      _add("300A002D", "DS", "Organ at Risk Overdose Volume Fraction", "1");
      _add("300A0040", "SQ", "Tolerance Table Sequence", "1");
      _add("300A0042", "IS", "Tolerance Table Number", "1");
      _add("300A0043", "SH", "Tolerance Table Label", "1");
      _add("300A0044", "DS", "Gantry Angle Tolerance", "1");
      _add("300A0046", "DS", "Beam Limiting Device Angle Tolerance", "1");
      _add("300A0048", "SQ", "Beam Limiting Device Tolerance Sequence", "1");
      _add("300A004A", "DS", "Beam Limiting Device Position Tolerance", "1");
      _add("300A004C", "DS", "Patient Support Angle Tolerance", "1");
      _add("300A004E", "DS", "Table Top Eccentric Angle Tolerance", "1");
      _add("300A0051", "DS", "Table Top Vertical Position Tolerance", "1");
      _add("300A0052", "DS", "Table Top Longitudinal Position Tolerance", "1");
      _add("300A0053", "DS", "Table Top Lateral Position Tolerance", "1");
      _add("300A0055", "CS", "RT Plan Relationship", "1");
      _add("300A0070", "SQ", "Fraction Group Sequence", "1");
      _add("300A0071", "IS", "Fraction Group Number", "1");
      _add("300A0078", "IS", "Number of Fractions Planned", "1");
      _add("300A0079", "IS", "Number of Fractions Per Day", "1");
      _add("300A007A", "IS", "Repeat Fraction Cycle Length", "1");
      _add("300A007B", "LT", "Fraction Pattern", "1");
      _add("300A0080", "IS", "Number of Beams", "1");
      _add("300A0082", "DS", "Beam Dose Specification Point", "3");
      _add("300A0084", "DS", "Beam Dose", "1");
      _add("300A0086", "DS", "Beam Meterset", "1");
      _add("300A00A0", "IS", "Number of Brachy Application Setups", "1");
      _add("300A00A2", "DS",
	   "Brachy Application Setup Dose Specification Point", "3");
      _add("300A00A4", "DS", "Brachy Application Setup Dose", "1");
      _add("300A00B0", "SQ", "Beam Sequence", "1");
      _add("300A00B2", "SH", "Treatment Machine Name", "1");
      _add("300A00B3", "CS", "Primary Dosimeter Unit", "1");
      _add("300A00B4", "DS", "Source-Axis Distance", "1");
      _add("300A00B6", "SQ", "Beam Limiting Device Sequence", "1");
      _add("300A00B8", "CS", "RT Beam Limiting Device Type", "1");
      _add("300A00BA", "DS", "Source to Beam Limiting Device Distance", "1");
      _add("300A00BC", "IS", "Number of Leaf/Jaw Pairs", "1");
      _add("300A00BE", "DS", "Leaf Position Boundaries", "3-n");
      _add("300A00C0", "IS", "Beam Number", "1");
      _add("300A00C2", "LO", "Beam Name", "1");
      _add("300A00C3", "ST", "Beam Description", "1");
      _add("300A00C4", "CS", "Beam Type", "1");
      _add("300A00C6", "CS", "Radiation Type", "1");
      _add("300A00C8", "IS", "Reference Image Number", "1");
      _add("300A00CA", "SQ", "Planned Verification Image Sequence", "1");
      _add("300A00CC", "LO", "Imaging Device-Specific Acquisition Parameters",
	   "1-n");
      _add("300A00CE", "CS", "Treatment Delivery Type", "1");
      _add("300A00D0", "IS", "Number of Wedges", "1");
      _add("300A00D1", "SQ", "Wedge Sequence", "1");
      _add("300A00D2", "IS", "Wedge Number", "1");
      _add("300A00D3", "CS", "Wedge Type", "1");
      _add("300A00D4", "SH", "Wedge ID", "1");
      _add("300A00D5", "IS", "Wedge Angle", "1");
      _add("300A00D6", "DS", "Wedge Factor", "1");
      _add("300A00D8", "DS", "Wedge Orientation", "1");
      _add("300A00DA", "DS", "Source to Wedge Tray Distance", "1");
      _add("300A00E0", "IS", "Number of Compensators", "1");
      _add("300A00E1", "SH", "Material ID", "1");
      _add("300A00E2", "DS", "Total Compensator Tray Factor", "1");
      _add("300A00E3", "SQ", "Compensator Sequence", "1");
      _add("300A00E4", "IS", "Compensator Number", "1");
      _add("300A00E5", "SH", "Compensator ID", "1");
      _add("300A00E6", "DS", "Source to Compensator Tray Distance", "1");
      _add("300A00E7", "IS", "Compensator Rows", "1");
      _add("300A00E8", "IS", "Compensator Columns", "1");
      _add("300A00E9", "DS", "Compensator Pixel Spacing", "2");
      _add("300A00EA", "DS", "Compensator Position", "2");
      _add("300A00EB", "DS", "Compensator Transmission Data", "1-n");
      _add("300A00EC", "DS", "Compensator Thickness Data", "1-n");
      _add("300A00ED", "IS", "Number of Boli", "1");
      _add("300A00F0", "IS", "Number of Blocks", "1");
      _add("300A00F2", "DS", "Total Block Tray Factor", "1");
      _add("300A00F4", "SQ", "Block Sequence", "1");
      _add("300A00F5", "SH", "Block Tray ID", "1");
      _add("300A00F6", "DS", "Source to Block Tray Distance", "1");
      _add("300A00F8", "CS", "Block Type", "1");
      _add("300A00FA", "CS", "Block Divergence", "1");
      _add("300A00FC", "IS", "Block Number", "1");
      _add("300A00FE", "LO", "Block Name", "1");
      _add("300A0100", "DS", "Block Thickness", "1");
      _add("300A0102", "DS", "Block Transmission", "1");
      _add("300A0104", "IS", "Block Number of Points", "1");
      _add("300A0106", "DS", "Block Data", "2-2n");
      _add("300A0107", "SQ", "Applicator Sequence", "1");
      _add("300A0108", "SH", "Applicator ID", "1");
      _add("300A0109", "CS", "Applicator Type", "1");
      _add("300A010A", "LO", "Applicator Description", "1");
      _add("300A010C", "DS", "Cumulative Dose Reference Coefficient", "1");
      _add("300A010E", "DS", "Final Cumulative Meterset Weight", "1");
      _add("300A0110", "IS", "Number of Control Points", "1");
      _add("300A0111", "SQ", "Control Point Sequence", "1");
      _add("300A0112", "IS", "Control Point Index", "1");
      _add("300A0114", "DS", "Nominal Beam Energy", "1");
      _add("300A0115", "DS", "Dose Rate Set", "1");
      _add("300A0116", "SQ", "Wedge Position Sequence", "1");
      _add("300A0118", "CS", "Wedge Position", "1");
      _add("300A011A", "SQ", "Beam Limiting Device Position Sequence", "1");
      _add("300A011C", "DS", "Leaf/Jaw Positions", "2-2n");
      _add("300A011E", "DS", "Gantry Angle", "1");
      _add("300A011F", "CS", "Gantry Rotation Direction", "1");
      _add("300A0120", "DS", "Beam Limiting Device Angle", "1");
      _add("300A0121", "CS", "Beam Limiting Device Rotation Direction", "1");
      _add("300A0122", "DS", "Patient Support Angle", "1");
      _add("300A0123", "CS", "Patient Support Rotation Direction", "1");
      _add("300A0124", "DS", "Table Top Eccentric Axis Distance", "1");
      _add("300A0125", "DS", "Table Top Eccentric Angle", "1");
      _add("300A0126", "CS", "Table Top Eccentric Rotation Direction", "1");
      _add("300A0128", "DS", "Table Top Vertical Position", "1");
      _add("300A0129", "DS", "Table Top Longitudinal Position", "1");
      _add("300A012A", "DS", "Table Top Lateral Position", "1");
      _add("300A012C", "DS", "Isocenter Position", "3");
      _add("300A012E", "DS", "Surface Entry Point", "3");
      _add("300A0130", "DS", "Source to Surface Distance", "1");
      _add("300A0134", "DS", "Cumulative Meterset Weight", "1");
      _add("300A0180", "SQ", "Patient Setup Sequence", "1");
      _add("300A0182", "IS", "Patient Setup Number", "1");
      _add("300A0184", "LO", "Patient Additional Position", "1");
      _add("300A0190", "SQ", "Fixation Device Sequence", "1");
      _add("300A0192", "CS", "Fixation Device Type", "1");
      _add("300A0194", "SH", "Fixation Device Label", "1");
      _add("300A0196", "ST", "Fixation Device Description", "1");
      _add("300A0198", "SH", "Fixation Device Position", "1");
      _add("300A01A0", "SQ", "Shielding Device Sequence", "1");
      _add("300A01A2", "CS", "Shielding Device Type", "1");
      _add("300A01A4", "SH", "Shielding Device Label", "1");
      _add("300A01A6", "ST", "Shielding Device Description", "1");
      _add("300A01A8", "SH", "Shielding Device Position", "1");
      _add("300A01B0", "CS", "Setup Technique", "1");
      _add("300A01B2", "ST", "Setup Technique Description", "1");
      _add("300A01B4", "SQ", "Setup Device Sequence", "1");
      _add("300A01B6", "CS", "Setup Device Type", "1");
      _add("300A01B8", "SH", "Setup Device Label", "1");
      _add("300A01BA", "ST", "Setup Device Description", "1");
      _add("300A01BC", "DS", "Setup Device Parameter", "1");
      _add("300A01D0", "ST", "Setup Reference Description", "1");
      _add("300A01D2", "DS", "Table Top Vertical Setup Displacement", "1");
      _add("300A01D4", "DS", "Table Top Longitudinal Setup Displacement", "1");
      _add("300A01D6", "DS", "Table Top Lateral Setup Displacement", "1");
      _add("300A0200", "CS", "Brachy Treatment Technique", "1");
      _add("300A0202", "CS", "Brachy Treatment Type", "1");
      _add("300A0206", "SQ", "Treatment Machine Sequence", "1");
      _add("300A0210", "SQ", "Source Sequence", "1");
      _add("300A0212", "IS", "Source Number", "1");
      _add("300A0214", "CS", "Source Type", "1");
      _add("300A0216", "LO", "Source Manufacturer", "1");
      _add("300A0218", "DS", "Active Source Diameter", "1");
      _add("300A021A", "DS", "Active Source Length", "1");
      _add("300A0222", "DS", "Source Encapsulation Nominal Thickness", "1");
      _add("300A0224", "DS", "Source Encapsulation Nominal Transmission", "1");
      _add("300A0226", "LO", "Source Isotope Name", "1");
      _add("300A0228", "DS", "Source Isotope Half Life", "1");
      _add("300A022A", "DS", "Reference Air Kerma Rate", "1");
      _add("300A022C", "DA", "Air Kerma Rate Reference Date", "1");
      _add("300A022E", "TM", "Air Kerma Rate Reference Time", "1");
      _add("300A0230", "SQ", "Application Setup Sequence", "1");
      _add("300A0232", "CS", "Application Setup Type", "1");
      _add("300A0234", "IS", "Application Setup Number", "1");
      _add("300A0236", "LO", "Application Setup Name", "1");
      _add("300A0238", "LO", "Application Setup Manufacturer", "1");
      _add("300A0240", "IS", "Template Number", "1");
      _add("300A0242", "SH", "Template Type", "1");
      _add("300A0244", "LO", "Template Name", "1");
      _add("300A0250", "DS", "Total Reference Air Kerma", "1");
      _add("300A0260", "SQ", "Brachy Accessory Device Sequence", "1");
      _add("300A0262", "IS", "Brachy Accessory Device Number", "1");
      _add("300A0263", "SH", "Brachy Accessory Device ID", "1");
      _add("300A0264", "CS", "Brachy Accessory Device Type", "1");
      _add("300A0266", "LO", "Brachy Accessory Device Name", "1");
      _add("300A026A", "DS", "Brachy Accessory Device Nominal Thickness", "1");
      _add("300A026C", "DS", "Brachy Accessory Device Nominal Transmission",
	   "1");
      _add("300A0280", "SQ", "Channel Sequence", "1");
      _add("300A0282", "IS", "Channel Number", "1");
      _add("300A0284", "DS", "Channel Length", "1");
      _add("300A0286", "DS", "Channel Total Time", "1");
      _add("300A0288", "CS", "Source Movement Type", "1");
      _add("300A028A", "IS", "Number of Pulses", "1");
      _add("300A028C", "DS", "Pulse Repetition Interval", "1");
      _add("300A0290", "IS", "Source Applicator Number", "1");
      _add("300A0291", "SH", "Source Applicator ID", "1");
      _add("300A0292", "CS", "Source Applicator Type", "1");
      _add("300A0294", "LO", "Source Applicator Name", "1");
      _add("300A0296", "DS", "Source Applicator Length", "1");
      _add("300A0298", "LO", "Source Applicator Manufacturer", "1");
      _add("300A029C", "DS", "Source Applicator Wall Nominal Thickness" ,"1");
      _add("300A029E", "DS", "Source Applicator Wall Nominal Transmission",
	   "1");
      _add("300A02A0", "DS", "Source Applicator Step Size", "1");
      _add("300A02A2", "IS", "Transfer Tube Number", "1");
      _add("300A02A4", "DS", "Transfer Tube Length", "1");
      _add("300A02B0", "SQ", "Channel Shield Sequence", "1");
      _add("300A02B2", "IS", "Channel Shield Number", "1");
      _add("300A02B3", "SH", "Channel Shield ID", "1");
      _add("300A02B4", "LO", "Channel Shield Name", "1");
      _add("300A02B8", "DS", "Channel Shield Nominal Thickness", "1");
      _add("300A02BA", "DS", "Channel Shield Nominal Transmission", "1");
      _add("300A02C8", "DS", "Final Cumulative Time Weight", "1");
      _add("300A02D0", "SQ", "Brachy Control Point Sequence", "1");
      _add("300A02D2", "DS", "Control Point Relative Position", "1");
      _add("300A02D4", "DS", "Control Point 3D Position", "3");
      _add("300A02D6", "DS", "Cumulative Time Weight", "1");

      // Group 300C Elements
      _add("300C0000", "UL", "Group Length", "1");
      _add("300C0002", "SQ", "Referenced RT Plan Sequence", "1");
      _add("300C0004", "SQ", "Referenced Beam Sequence", "1");
      _add("300C0006", "IS", "Referenced Beam Number", "1");
      _add("300C0007", "IS", "Referenced Reference Image Number", "1");
      _add("300C0008", "DS", "Start Cumulative Meterset Weight", "1");
      _add("300C0009", "DS", "End Cumulative Meterset Weight", "1");
      _add("300C000A", "SQ", "Referenced Brachy Application Setup Sequence",
	   "1");
      _add("300C000C", "IS", "Referenced Brachy Application Setup Number",
	   "1");
      _add("300C000E", "IS", "Referenced Source Number", "1");
      _add("300C0020", "SQ", "Referenced Fraction Group Sequence", "1");
      _add("300C0022", "IS", "Referenced Fraction Group Number", "1");
      _add("300C0040", "SQ", "Referenced Verification Image Sequence", "1");
      _add("300C0042", "SQ", "Referenced Reference Image Sequence", "1");
      _add("300C0050", "SQ", "Referenced Dose Reference Sequence", "1");
      _add("300C0051", "IS", "Referenced Dose Reference Number", "1");
      _add("300C0055", "SQ", "Brachy Referenced Dose Reference Sequence", "1");
      _add("300C0060", "SQ", "Referenced Structure Set Sequence", "1");
      _add("300C006A", "IS", "Referenced Patient Setup Number", "1");
      _add("300C0080", "SQ", "Referenced Dose Sequence", "1");
      _add("300C00A0", "IS", "Referenced Tolerance Table Number", "1");
      _add("300C00B0", "SQ", "Referenced Bolus Sequence", "1");
      _add("300C00C0", "IS", "Referenced Wedge Number", "1");
      _add("300C00D0", "IS", "Referenced Compensator Number", "1");
      _add("300C00E0", "IS", "Referenced Block Number", "1");
      _add("300C00F0", "IS", "Referenced Control Point Index", "1");

      // Group 300E Elements
      _add("300E0000", "UL", "Group Length", "1");
      _add("300E0002", "CS", "Approval Status", "1");
      _add("300E0004", "DA", "Review Date", "1");
      _add("300E0005", "TM", "Review Time", "1");
      _add("300E0008", "PN", "Reviewer Name", "1");

      // Group 4008 Elements
      _add("40080000", "UL", "Group Length", "1");
      _add("40080040", "SH", "Results ID", "1");
      _add("40080042", "LO", "Results ID Issuer", "1");
      _add("40080050", "SQ", "Referenced Interpretation Sequence", "1");
      _add("40080100", "DA", "Interpretation Recorded Date", "1");
      _add("40080101", "TM", "Interpretation Recorded Time", "1");
      _add("40080102", "PN", "Interpretation Recorder", "1");
      _add("40080103", "LO", "Reference to Recorded Sound", "1");
      _add("40080108", "DA", "Interpretation Transcription Date", "1");
      _add("40080109", "TM", "Interpretation Transcription Time", "1");
      _add("4008010A", "PN", "Interpretation Transcriber", "1");
      _add("4008010B", "ST", "Interpretation Text", "1");
      _add("4008010C", "PN", "Interpretation Author", "1");
      _add("40080111", "SQ", "Interpretation Approver Sequence", "1");
      _add("40080112", "DA", "Interpretation Approval Date", "1");
      _add("40080113", "TM", "Interpretation Approval Time", "1");
      _add("40080114", "PN", "Physician Approving Interpretation", "1");
      _add("40080115", "LT", "Interpretation Diagnosis Description", "1");
      _add("40080117", "SQ", "Interpretation Diagnosis Code Sequence", "1");
      _add("40080118", "SQ", "Results Distribution List Sequence", "1");
      _add("40080119", "PN", "Distribution Name", "1");
      _add("4008011A", "LO", "Distribution Address", "1");
      _add("40080200", "SH", "Interpretation ID", "1");
      _add("40080202", "LO", "Interpretation ID Issuer", "1");
      _add("40080210", "CS", "Interpretation Type ID", "1");
      _add("40080212", "CS", "Interpretation Status ID", "1");
      _add("40080300", "ST", "Impressions", "1");
      _add("40084000", "ST", "Results Comments", "1");

      // Group 5000 Elements (xx means any two numbers)
      _add("50xx0000", "UL", "Group Length", "1");
      _add("50xx0005", "US", "Curve Dimensions", "1");
      _add("50xx0010", "US", "Number of Points", "1");
      _add("50xx0020", "CS", "Type of Data", "1");
      _add("50xx0022", "LO", "Curve Description", "1");
      _add("50xx0030", "SH", "Axis Units", "1-n");
      _add("50xx0040", "SH", "Axis Labels", "1-n");
      _add("50xx0103", "US", "Data Value Representation", "1");
      _add("50xx0104", "US", "Minimum Coordinate Value", "1-n");
      _add("50xx0105", "US", "Maximum Coordinate Value", "1-n");
      _add("50xx0106", "SH", "Curve Range", "1-n");
      _add("50xx0110", "US", "Curve Data Descriptor", "1-n");
      _add("50xx0112", "US", "Coordinate Start Value", "1");
      _add("50xx0114", "US", "Coordinate Step Value", "1");
      _add("50xx2000", "US", "Audio Type", "1");
      _add("50xx2002", "US", "Audio Sample Format", "1");
      _add("50xx2004", "US", "Number of Channels", "1");
      _add("50xx2006", "UL", "Number of Samples", "1");
      _add("50xx2008", "UL", "Sample Rate", "1");
      _add("50xx200A", "UL", "Total Time", "1");
      _add("50xx200C", "OW/OB", "Audio Sample Data", "1");
      _add("50xx200E", "LT", "Audio Comments", "1");
      _add("50xx2500", "LO", "Curve Label", "1");
      _add("50xx2600", "SQ", "Referenced Overlay Sequence", "1");
      _add("50xx2610", "US", "Referenced Overlay Group", "1");
      _add("50xx3000", "OW/OB", "Curve Data", "1");

      // Group 5200 Elements
      _add("52000000", "UL", "Group Length", "1");
      _add("52009229", "SQ", "Shared Functional Groups Sequence", "1");
      _add("52009230", "SQ", "Per-frame Functional Groups Sequence", "1");

      // Group 5600 Elements
      _add("56000000", "UL", "Group Length", "1");
      _add("56000010", "OF", "First Order Phase Correction Angle", "1");
      _add("56000020", "OF", "Spectroscopy Data", "1");

      // Group 6000 Elements (xx means any two numbers)
      _add("60xx0000", "UL", "Group Length", "1");
      _add("60xx0010", "US", "Overlay Rows", "1");
      _add("60xx0011", "US", "Overlay Columns", "1");
      _add("60xx0012", "US", "Overlay Planes", "1");
      _add("60xx0015", "IS", "Number of Frames in Overlay", "1");
      _add("60xx0022", "LO", "Overlay Description", "1");
      _add("60xx0040", "CS", "Overlay Type", "1");
      _add("60xx0045", "LO", "Overlay Subtype", "1");
      _add("60xx0050", "SS", "Overlay Origin", "2");
      _add("60xx0051", "US", "Image Frame Origin", "1");
      _add("60xx0052", "US", "Overlay Plane Origin", "1");
      _add("60xx0100", "US", "Overlay Bits Allocated", "1");
      _add("60xx0102", "US", "Overlay Bit Position", "1");
      _add("60xx1301", "IS", "ROI Area", "1");
      _add("60xx1302", "DS", "ROI Mean", "1");
      _add("60xx1303", "DS", "ROI Standard Deviation", "1");
      _add("60xx1500", "LO", "Overlay Label", "1");
      _add("60xx3000", "OW", "Overlay Data", "1");

      // Group 7FE0 Elements
      _add("7FE00000", "UL", "Group Length", "1");
      _add("7FE00010", "OW/OB", "Pixel Data", "1");

      // Group FFFC Element
      _add("FFFCFFFC", "OB", "Data Set Trailing Padding", "1");

      // SQ Elements
      _add("FFFEE000", "OB", "Item", "1");
      _add("FFFEE00D", "OB", "Item Delimitation Item", "1");
      _add("FFFEE0DD", "OB", "Sequence Delimitation Item", "1");
    }
}
