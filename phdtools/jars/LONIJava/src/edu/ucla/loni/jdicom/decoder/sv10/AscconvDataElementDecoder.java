/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.StringReader;

/**
 * Decodes a DICOM Data Element from a Siemens ASCCONV string.
 *
 * @version 28 August 2007
 */
public class AscconvDataElementDecoder
{
    /** Text before the ASCCONV text. */
    private String _preAscconvText;

    /** Decoded ASCCONV Root Node. */
    private AscconvRootNode _rootNode;

    /** Text after the ASCCONV text. */
    private String _postAscconvText;

    /**
     * Constructs an ASCCONV Data Element Decoder.
     *
     * @param text ASCCONV text to decode.
     *
     * @throws IOException If an I/O error occurs.
     */
    public AscconvDataElementDecoder(String text) throws IOException
    {
	// Initialize
	_preAscconvText = "";
	_postAscconvText = "";

	// Search for the ASCCONV delimiting strings
	String beginDelim = "### ASCCONV BEGIN ###";
	String endDelim = "### ASCCONV END ###";
	int startIndex = text.indexOf(beginDelim);
	int endIndex = text.indexOf(endDelim);

	// No ASCCONV information
	if (startIndex == -1 || endIndex == -1) {
	    throw new IOException("No ASCCONV Information");
	}

	// Get the pre and post ASCCONV text
	_preAscconvText = text.substring(0, startIndex);
	_postAscconvText = text.substring(endIndex + endDelim.length(),
					  text.length());

	// Get the ASCCONV text
	String ascconv = text.substring(startIndex + beginDelim.length(),
					endIndex);

	// Create the root node
	_rootNode = new AscconvRootNode("ASCCONV");

	// Add the ASCCONV elements to the root node
	BufferedReader reader = new BufferedReader(new StringReader(ascconv));
	String line = reader.readLine();
	while (line != null) {

	    // Search for " = "
	    String equalDelim = " = ";
	    int equalIndex = line.indexOf(equalDelim);

	    // Add the element name and value
	    if (equalIndex != -1) {
		String name = line.substring(0, equalIndex).trim();
		String value = line.substring(equalIndex + equalDelim.length(),
					      line.length());
		_addToNode(name, value, _rootNode);
	    }

	    // Get the next line
	    line = reader.readLine();
	}
    }

    /**
     * Gets a Data Element that contains the text before the ASCCONV text.
     *
     * @return Data Element that contains the text before the ASCCONV text.
     */
    public DataElement getPreAscconvElement()
    {
	String name = "PreAscconv";
	String vr = "UN";
	String desc = "Text preceding the ASCCONV delimiter.";

	DataElement elem = new DataElement(name, vr, desc);
	elem.addValue(_preAscconvText);
	return elem;
    }

    /**
     * Gets the Data Element decoded from the ASCCONV text.
     *
     * @return Data Element decoded from the ASCCONV text.
     */
    public DataElement getAscconvElement()
    {
	AscconvNodeGroup group = new AscconvNodeGroup( _rootNode.getName() );
	group.add(_rootNode);
	return _toDataElement(group);
    }

    /**
     * Gets a Data Element that contains the text before the ASCCONV text.
     *
     * @return Data Element that contains the text before the ASCCONV text.
     */
    public DataElement getPostAscconvElement()
    {
	String name = "PostAscconv";
	String vr = "UN";
	String desc = "Text following the ASCCONV delimiter.";

	DataElement elem = new DataElement(name, vr, desc);
	elem.addValue(_postAscconvText);
	return elem;
    }

    /**
     * Converts the ASCCONV Root Nodes to a Data Element.
     *
     * @param group ASCCONV Node Group of Root Nodes.
     *
     * @return Data Element created from the Root Nodes.
     */
    private static DataElement _toDataElement(AscconvNodeGroup group)
    {
	String name = group.getName();
	String vr = "SQ";
	String desc = name;

	// Create the Data Element
	DataElement elem = new DataElement(name, vr, desc);

	// Add a Data Set for each Root Node
	AscconvRootNode[] rootNodes = group.getRootNodes();
	for (int i = 0; i < rootNodes.length; i++) {
	    DataSet dataSet = new DataSet();

	    // Add the leaf Data Elements
	    AscconvLeafNode[] leafNodes = rootNodes[i].getLeafNodes();
	    for (int j = 0; j < leafNodes.length; j++) {
		dataSet.addDataElement( _toDataElement(leafNodes[j]) );
	    }

	    // Add the root Data Elements
	    AscconvNodeGroup[] groups = rootNodes[i].getRootNodes();
	    for (int j = 0; j < groups.length; j++) {
		dataSet.addDataElement( _toDataElement(groups[j]) );
	    }

	    elem.addDataSet(dataSet);
	}

	return elem;
    }

    /**
     * Converts the ASCCONV Leaf Node to a Data Element.
     *
     * @param leafNode Leaf Node to convert.
     *
     * @return Data Element created from the Leaf Node.
     */
    private static DataElement _toDataElement(AscconvLeafNode leafNode)
    {
	String name = leafNode.getName();
	String vr = "LT";
	String desc = name;

	// Create the Data Element
	DataElement elem = new DataElement(name, vr, desc);

	// Add the values
	String[] values = leafNode.getValues();
	for (int i = 0; i < values.length; i++) { elem.addValue(values[i]); }

	return elem;
    }

    /**
     * Adds the ASCCONV element to the root node.
     *
     * @param name Name of the ASCCONV element.
     * @param value Value of the ASCCONV element.
     * @param rootNode Root Node to add to.
     */
    private static void _addToNode(String name, String value,
				   AscconvRootNode rootNode)
    {
	// If no period, then is a leaf
	int periodIndex = name.indexOf('.');
	if (periodIndex == -1) {

	    // Remove any trailing [#]
	    int leftBrkIndex = name.indexOf('[');
	    if (leftBrkIndex != -1) {
		name = name.substring(0, leftBrkIndex);
	    }

	    // Add to or create a new leaf
	    AscconvLeafNode leafNode = rootNode.getLeafNode(name);
	    if (leafNode == null) {
		leafNode = new AscconvLeafNode(name);
		rootNode.add(leafNode);
	    }

	    // Add the value to the leaf
	    leafNode.addValue(value);
	    return;
	}

	// If there is a period, separate:  name = rootName + . + subName
	String rootName = name.substring(0, periodIndex);
	String subName = name.substring(periodIndex+1, name.length());

	// Check for a root index:  rootName = ABC[index]
	int rootIndex = -1;
	int leftBrkIndex = rootName.indexOf('[');
	int rightBrkIndex = rootName.indexOf(']');
	if (leftBrkIndex != -1 && rightBrkIndex != -1) {
	    String num = rootName.substring(leftBrkIndex+1, rightBrkIndex);
	    try {

		// Get the root index
		rootIndex = Integer.parseInt(num);

		// Remove the root index from the root name
		rootName = rootName.substring(0, leftBrkIndex);
	    }
	    catch (Exception e) {}
	}

	// Add or create a new root
	AscconvRootNode childNode = rootNode.getRootNode(rootName, rootIndex);
	if (childNode == null) {
	    childNode = new AscconvRootNode(rootName, rootIndex);
	    rootNode.add(childNode);
	}

	// Add the value to the root
	_addToNode(subName, value, childNode);
    }
}
