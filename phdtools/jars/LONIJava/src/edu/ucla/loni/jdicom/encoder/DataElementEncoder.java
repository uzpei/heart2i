/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.encoder;

import edu.ucla.loni.jdicom.DataElement;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Encodes a DICOM unit of information to a stream.
 * <p>
 * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
 *     [Section 3.10], Page 5: </b>
 * <em> 
 * DATA ELEMENT:  A unit of information as defined by a single entry in the
 * data dictionary. An encoded Information Object Definition (IOD) Attribute
 * that is composed of, at a minimum, three fields: a Data Element Tag, a 
 * Value Length, and a Value Field. For some specific Transfer Syntaxes, a
 * Data Element also contains a VR Field where the Value Representation of
 * that Data Element is specified explicitly.
 * </em>
 *
 * @version 10 May 2007
 */
public class DataElementEncoder
{
  /**
   * Little Endian byte ordering.
   * <p>
   * <b> PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
   *     [Section 7.3], Page 28: </b>
   * <em>
   * <p>
   * Little Endian byte ordering is defined as follows:
   * <p> <ul>
   * <li> In a binary number consisting of multiple bytes (e.g. a 32-bit
   *      unsigned integer value, the Group Number, the Element Number,
   *      etc.), the least significant byte shall be encoded first; with the
   *      remaining bytes encoded in increasing order of significance.
   * <li> In a character string consisting of multiple 8-bit single byte
   *      codes, the characters will be encoded in the order of occurrence
   *      in the string (left to right).
   * </ul>
   * </em>
   */
  public static final int LITTLE_ENDIAN = 0;

  /**
   * Big Endian byte ordering.
   * <p>
   * <b> PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
   *     [Section 7.3], Page 28: </b>
   * <em>
   * <p>
   * Big Endian byte ordering is defined as follows:
   * <p> <ul>
   * <li> In a binary number consisting of multiple bytes, the most
   *      significant byte shall be encoded first; with the remaining bytes
   *      encoded in decreasing order of significance.
   * <li> In a character string consisting of multiple 8-bit single byte
   *      codes, the characters will be encoded in the order of occurrence
   *      in the string (left to right).
   * </ul> 
   * </em>
   */
  public static final int BIG_ENDIAN = 1;

  /**
   * Implicit VR structure.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT STRUCTURE WITH IMPLICIT VR
   *     [Section 7.1.3], Page 27: </b>
   * <em> 
   * When using the Implicit VR structure the Data Element shall be
   * constructed of three consecutive fields:  Data Element Tag, Value
   * Length, and Value.
   * </em>
   */
  public static final int IMPLICIT_VR = 0;

  /**
   * Explicit VR structure.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT STRUCTURE WITH EXPLICIT VR
   *     [Section 7.1.2], Page 26: </b>
   * <em> 
   * When using the Explicit VR structures, the Data Element shall be
   * constructed of four consecutive fields:  Data Element Tag, VR, Value
   * Length, and Value.
   * </em>
   */
  public static final int EXPLICIT_VR = 1;

  /** Byte Swapped Output Stream to write encoded data to. */
  private ByteSwappedOutputStream _byteSwappedStream;

  /** Byte ordering for the encoded DataElements. */
  private int _byteOrdering;

  /** Structure for the encoded DataElements. */
  private int _structure;

  /** UID of the Transfer Syntax that determines the encoding. */
  private String _transferSyntaxUID;

  /**
   * Constructs a DataElementEncoder that writes encoded data to the specified
   * output stream.
   *
   * @param outputStream Output Stream to write encoded data to.
   * @param transferSyntaxUID UID of the Transfer Syntax that determines the
   *                          byte ordering and structure during encoding.
   *
   * @throws IllegalArgumentException If the Transfer Syntax UID is invalid.
   */
  public DataElementEncoder(OutputStream outputStream,
			    String transferSyntaxUID)
    {
      _byteSwappedStream = new ByteSwappedOutputStream(outputStream);
      _transferSyntaxUID = transferSyntaxUID;

      // Transfer Syntax value implies Little Endian byte ordering and
      // Explicit VR structure
      //
      // PS 3.5-1998, DICOM LITTLE ENDIAN TRANSFER SYNTAX (EXPLICIT VR)
      // [Section A.2], Page 43:
      // This DICOM Explicit VR Little Endian Transfer Syntax shall be
      // identified by a UID of Value "1.2.840.10008.1.2.1."
      //
      // PS 3.5-1998, TRANSFER SYNTAXES FOR ENCAPSULATION OF ENCODED PIXEL DATA
      // [Section A.4], Page 44:
      // a) The Data Elements contained in the Data Set structure shall be
      // encoded with Explicit VR (with a VR Field) as specified in
      // Section 7.1.2.
      // b) The encoding of the overall Data Set structure (Data Element Tags,
      // Value Length, etc.) shall be in Little Endian as specified in
      // Section 7.3.
      if ( transferSyntaxUID.equals("1.2.840.10008.1.2.1") ) {
	_byteOrdering = LITTLE_ENDIAN;
	_structure = EXPLICIT_VR;
      }

      // Transfer Syntax value implies Big Endian byte ordering and
      // Explicit VR structure
      //
      // PS 3.5-1998, DICOM BIG ENDIAN TRANSFER SYNTAX (EXPLICIT VR)
      // [Section A.3], Page 44:
      // This DICOM Explicit VR Big Endian Transfer Syntax shall be identified
      // by a UID of Value "1.2.840.10008.1.2.2."
      else if ( transferSyntaxUID.equals("1.2.840.10008.1.2.2") ) {
	_byteOrdering = BIG_ENDIAN;
	_structure = EXPLICIT_VR;
      }

      // Transfer Syntax value implies Little Endian byte ordering and
      // Implicit VR structure
      //
      // PS 3.5-1998, DICOM IMPLICIT VR LITTLE ENDIAN TRANSFER SYNTAX
      // [Section A.1], Page 42:
      // This DICOM Implicit VR Little Endian Transfer Syntax shall be
      // identified by a UID of Value "1.2.840.10008.1.2."
      else if ( transferSyntaxUID.equals("1.2.840.10008.1.2") ) {
	_byteOrdering = LITTLE_ENDIAN;
	_structure = IMPLICIT_VR;
      }

      // Transfer Syntax value is unrecognized
      else {
	String msg = "Unable to recognize the Transfer Syntax UID = \"" +
	             transferSyntaxUID + "\".";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Writes the specified Data Element to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT FIELDS 
   *     [Section 7.1.1], Page 25: </b>
   * <em>
   * A Data Element is made up of fields. Three fields are common to all
   * three Data Element structures; these are the Data Element Tag, Value
   * Length, and Value Field. A fourth field, Value Representation, is only
   * present in the two Explicit VR Data Element structures.
   * </em>
   * <p> <pre>
   * Encoded Data Element:
   * +---------------------------------------+
   * | TAG | VR | VALUE LENGTH | VALUE FIELD |  (VR optional)
   * +---------------------------------------+
   * </pre>
   *
   * @param dataElement Data Element to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeDataElement(DataElement dataElement) throws IOException
    {
      writeDataElement(dataElement, true);
    }

  /**
   * Writes the specified Data Element to the Output Stream, stopping before
   * the Value Length if required.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT FIELDS 
   *     [Section 7.1.1], Page 25: </b>
   * <em>
   * A Data Element is made up of fields. Three fields are common to all
   * three Data Element structures; these are the Data Element Tag, Value
   * Length, and Value Field. A fourth field, Value Representation, is only
   * present in the two Explicit VR Data Element structures.
   * </em>
   * <p> <pre>
   * Encoded Data Element:
   * +---------------------------------------+
   * | TAG | VR | VALUE LENGTH | VALUE FIELD |  (VR optional)
   * +---------------------------------------+
   * </pre>
   *
   * @param dataElement Data Element to write to the Output Stream.
   * @param writeValue True if the Value Length and Value Field of the Data
   *                   Element should be written; false if not.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeDataElement(DataElement dataElement, boolean writeValue)
    throws IOException
    {
      // Write the Data Element Tag to the stream
      String tag = dataElement.getDataElementTag();
      boolean byteSwap = _byteOrdering==LITTLE_ENDIAN;
      _byteSwappedStream.writeDataElementTag(tag, byteSwap);

      // If necessary, write the Value Representation to the stream
      String vr = dataElement.getValueRepresentation();
      boolean hasShortValueLength = false;
      if (_structure == EXPLICIT_VR) {

	// Encode the Value Representation without byte swapping
	//
	// PS 3.5-1998, DATA ELEMENT FIELDS
	// [Section 7.1.1], Page 25:
	// Value Representation: A two-byte character string containing the
	// VR of the Data Element.
	//
	// PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
	// [Section 7.3], Page 28:
	// In a character string consisting of multiple 8-bit single byte
	// codes, the characters will be encoded in the order of occurrence
	// in the string (left to right).
	_byteSwappedStream.write( vr.getBytes() );

	// Write 2 zeroed bytes if the VR is OB, OW, SQ, UN, or UT
	//
	// PS 3.5-1998, DATA ELEMENT STRUCTURE WITH EXPLICIT VR
	// [Section 7.1.2], Page 26:
	// For VRs of OB, OW, SQ and UN the 16 bits following the two character
	// VR Field are reserved for use by later versions of the DICOM
	// Standard. These reserved bytes shall be set to 0000H and shall not
	// be used or decoded (Table 7.1-1). The Value Length Field is a 32-bit
	// unsigned integer.
	// For VRs of UT the 16 bits following the two character VR Field are
	// reserved for use by later versions of the DICOM Standard. These
	// reserved bytes shall be set to 0000H and shall not be used or
	// decoded. The Value Length Field is a 32-bit unsigned integer.
	if (vr.equals("OB") || vr.equals("OW") || vr.equals("SQ") ||
	    vr.equals("UN") || vr.equals("UT") )
	  {
	    _byteSwappedStream.write( new byte[2] );
	  }

	// Otherwise the value length is 16 bits (not 32 bits)
	//
	// PS 3.5-1998, DATA ELEMENT STRUCTURE WITH EXPLICIT VR
	// [Section 7.1.2], Page 26:
	// For all other VRs the Value Length Field is the 16-bit unsigned
	// integer following the two character VR Field (Table 7.1-2).
	else { hasShortValueLength = true; }
      }

      // Write the Value Length and Value Field to the stream if required
      if (writeValue) {
	ValueFieldEncoder encoder = new ValueFieldEncoder(_byteSwappedStream,
							  byteSwap,
							  _transferSyntaxUID,
							  hasShortValueLength);

	// Write the Data Sets if the VR is SQ
	//
	// PS 3.5-1998, NESTING OF DATA SETS
	// [Section 7.5], Page 29:
	// The VR identified "SQ" shall be used for Data Elements with a Value
	// consisting of a Sequence of zero or more Items, where each Item
	// contains a set of Data Elements.
	if ( vr.equals("SQ") ) {
	  encoder.writeValues(vr, dataElement.getDataSets());
	}

	// Otherwise write the values
	else { encoder.writeValues(vr, dataElement.getValues()); }
      }
    }
}
