/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder;

import java.util.Vector;
import java.io.EOFException;
import java.io.IOException;
import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;

/**
 * Decodes a DICOM set of Data Elements from an encoded stream.
 *
 * @version 17 January 2006
 */
public class DataSetDecoder
{
    /**
     * Decoded DICOM set of Data Elements.
     * <p>
     * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
     *     [Section 3.10], Page 5: </b>
     * <em> 
     * DATA SET:  Exchanged information consisting of a structured set of 
     * Attribute values directly or indirectly related to Information Objects. 
     * The value of each Attribute in a Data Set is expressed as a Data Element.
     * A collection of Data Elements ordered by increasing Data Element Tag
     * number that is an encoding of the values of Attributes of a real world
     * object.
     * </em>
     *
     * @supplierCardinality 0..1
     * @clientCardinality 1
     * @label decodes
     */
    private DataSet _dataSet;

    /** Length (in bytes) of the encoded DataSet. */
    private int _dataSetLength = 0;

    /** 8 digit hexadecimal number marking the end of the encoded DataSet. */
    private String _dataElementDelimiterTag = null;

    /** 8 digit hexadecimal number marking where the decoding should stop. */
    private String _dataElementStopAtTag = null;

    /**
     * Constructs a DataSetDecoder which reads encoded data from the specified
     * MeasuredInputStream.  The decoding ends when an EOFException is thrown.
     *
     * @param stream MeasuredInputStream to read encoded data from.
     *
     * @throws IOException If an I/O error occurs.
     */
    public DataSetDecoder(MeasuredInputStream stream) throws IOException
    {
	// Decode without stopping at a specific Data Element Tag
	this(stream, null);
    }

    /**
     * Constructs a DataSetDecoder which reads encoded data from the specified
     * MeasuredInputStream.  The decoding ends when an EOFException is thrown
     * or at the beginning of the encoded Value Field of the specified Data
     * Element Tag.
     *
     * @param stream MeasuredInputStream to read encoded data from.
     * @param dataElementStopAtTag 8 digit hexadecimal number marking where the
     *                             decoding should stop.  If this is null, the
     *                             decoding is not stopped.
     *
     * @throws IOException If an I/O error occurs.
     */
    public DataSetDecoder(MeasuredInputStream stream,
			  String dataElementStopAtTag) throws IOException
    {
	// End the decoding when this Data Element Tag is reached
	_dataElementStopAtTag = dataElementStopAtTag;

	// Decode the DataSet assuming no DataElements have yet been decoded
	_decodeDataSet(stream, new Vector(100, 20));
    }

    /**
     * Constructs a DataSetDecoder which reads encoded data from the specified
     * MeasuredInputStream until the given number of bytes have been read
     * from the stream or an EOFException is thrown.  The given DataElements
     * are those which have already been decoded from the stream.
     * <p>
     * <b> PS 3.5-1998, ITEM ENCODING RULES
     *     [Section 7.5.1], Page 30: </b>
     * <em> 
     * Each Item of a Data Element of Value Representation SQ shall be encoded
     * as a DICOM Standard Data Element with a specific Data Element Tag of
     * Value (FFFE,E000). The Item Tag is followed by a 4 byte Item Length
     * field encoded in one of the following two ways:
     * <p>
     * a) Explicit Length: The number of bytes (even) contained in the Sequence
     * Item Value (following but not including the Item Length Field) is encoded
     * as a 32-bit unsigned integer value (see Section 7.1). This length shall
     * include the total length of all Data Elements conveyed by this Item. This
     * Item Length shall be equal to 00000000H if the Item contains no Data Set.
     * </em>
     *
     * @param stream MeasuredInputStream to read encoded data from.
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     * @param dataSetLength Length (in bytes) of the encoded DataSet.
     *
     * @throws IOException If an I/O error occurs.
     */
    public DataSetDecoder(MeasuredInputStream stream, Vector dataElements,
			  int dataSetLength) throws IOException
    {
	// End the decoding after reading this many bytes
	_dataSetLength = dataSetLength;

	// Decode the DataSet
	if (_dataSetLength > 0) { _decodeDataSet(stream, dataElements); }
    }

    /**
     * Constructs a DataSetDecoder which reads encoded data from the specified
     * MeasuredInputStream until the given delimiter is reached or an
     * EOFException is thrown.  The given DataElements are those which have
     * already been decoded from the stream.
     * <p>
     * <b> PS 3.5-1998, ITEM ENCODING RULES
     *     [Section 7.5.1], Page 30: </b>
     * <em> 
     * Each Item of a Data Element of Value Representation SQ shall be encoded
     * as a DICOM Standard Data Element with a specific Data Element Tag of
     * Value (FFFE,E000). The Item Tag is followed by a 4 byte Item Length
     * field encoded in one of the following two ways:
     * <p>
     * b) Undefined Length: The Item Length Field shall contain the value
     * FFFFFFFFH to indicate an undefined Item length. It shall be used in
     * conjunction with an Item Delimitation Data Element.  This Item
     * Delimitation Data Element has a Data Element Tag of (FFFE,E00D) and shall
     * follow the Data Elements encapsulated in the Item. No Value shall be
     * present in the Item Delimitation Data Element and its Length shall be
     * 00000000H.
     * </em>
     *
     * @param stream MeasuredInputStream to read encoded data from.
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     * @param dataElementDelimiterTag 8 digit hexadecimal number marking the
     *                                end of the encoded DataSet.
     *
     * @throws IOException If an I/O error occurs.
     */
    public DataSetDecoder(MeasuredInputStream stream, Vector dataElements,
			  String dataElementDelimiterTag) throws IOException
    {
	// End the decoding upon reaching this tag
	_dataElementDelimiterTag = dataElementDelimiterTag;

	// Decode the DataSet
	if (_dataElementDelimiterTag != null) {
	    _decodeDataSet(stream, dataElements);
	}
    }

    /**
     * Gets the decoded DataSet.
     *
     * @return DataSet decoded from the encoded stream.
     */
    public DataSet getDataSet()
    {
	return _dataSet;
    }

    /**
     * Decodes a DataSet from the specified MeasuredInputStream using the
     * DataElements which have already been decoded so far.
     *
     * @param stream MeasuredInputStream to read encoded data from.
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void _decodeDataSet(MeasuredInputStream stream, Vector dataElements)
	throws IOException
    {
	// Create an empty DataSet
	_dataSet = new DataSet();

	// Get a copy of the DataElements decoded so far
	Vector elements = (Vector)dataElements.clone();

	// Get the maximum amount of available memory
	long maxMemory = Runtime.getRuntime().maxMemory();

	try {
	    long startingOffset = stream.getOffset();

	    // Decode all the DataElements of the DataSet
	    while (true) {

		// Check number of bytes read against the encoded DataSet length
		long endOfDataSetOffset = startingOffset + _dataSetLength;
		if (_dataSetLength > 0 &&
		    stream.getOffset() >= endOfDataSetOffset) { return; }

		// Decode the next DataElement
		DataElementDecoder ded =
		    new DataElementDecoder(stream, elements,
					   _dataElementStopAtTag, maxMemory);
		DataElement element = ded.getDataElement();

		// Check if this DataElement is where the decoding should stop
		String elementTag = element.getDataElementTag();
		if (elementTag.equals(_dataElementStopAtTag)) {
		    return;
		}

		// Check if this DataElement is the delimiter
		if (elementTag.equals(_dataElementDelimiterTag) ) {
		    return;
		}

		// Update the DataSet and DataElements decoded so far
		_dataSet.addDataElement(element);
		elements.addElement(element);
	    }
	}

	// If the stream ends, stop decoding
	catch (EOFException e) { return; }
    }
}
