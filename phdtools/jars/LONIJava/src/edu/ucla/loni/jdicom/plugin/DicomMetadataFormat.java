/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import edu.ucla.loni.jdicom.DataDictionary;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * DICOM Metadata classes.
 *
 * @version 3 July 2003
 */
public abstract class DicomMetadataFormat
                extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the Data Set Node. */
  public static final String DATA_SET_NAME = "DATA_SET";

  /** Name of the Value Representation Attribute. */
  public static final String VR_NAME = "vr";

  /** Name of the Value Node. */
  public static final String VALUE_NODE_NAME = "VALUE";

  /** Name of the Value Attribute. */
  public static final String VALUE_ATTRIBUTE_NAME = "value";

  /**
   * Constructs a DicomMetadataFormat.
   *
   * @param rootName Name of the root element.
   *
   * @throws IllegalArgumentException If the root name is null.
   */
  protected DicomMetadataFormat(String rootName)
    {
      // Create the root with one required 1 child
      super(rootName, CHILD_POLICY_ALL);

      // Add one Data Set child that can have missing children
      addElement(DATA_SET_NAME, rootName, CHILD_POLICY_SOME);

      // Create one Data Element child for each allowable Data Element Tag
      DataDictionary dd = DataDictionary.getInstance();
      String[] tags = dd.getDataElementTags();
      for (int i = 0; i < tags.length; i++) {

	// Get the Value Representation and Value Multiplicity for the Tag
	String vr = dd.getValueRepresentation(tags[i]);
	String vm = dd.getValueMultiplicity(tags[i]);

	// A VR of "SQ" implies zero or more nested Data Sets
	if ( vr.equals("SQ") ) {
	  addElement(tags[i], DATA_SET_NAME, 0, Integer.MAX_VALUE);
	  addChildElement(DATA_SET_NAME, tags[i]);
	}

	// Only one Value allowed
	else if ( vm.equals("1") ) {
	  addElement(tags[i], DATA_SET_NAME, CHILD_POLICY_ALL);
	}

	// Many Values allowed
	else { addElement(tags[i], DATA_SET_NAME, _getMin(vm), _getMax(vm)); }

	// Add the VR attribute
	addAttribute(tags[i], VR_NAME, DATATYPE_STRING, true, null);

	// If Values are allowed, add a Value child
	if ( !vr.equals("SQ") ) {
	  addElement(VALUE_NODE_NAME, tags[i], CHILD_POLICY_EMPTY);
	  addAttribute(VALUE_NODE_NAME, VALUE_ATTRIBUTE_NAME, _getDataType(vr),
		       true, null);
	}
      }

      // Set the resource base name for element descriptions
      setResourceBaseName( DicomMetadataFormatResources.class.getName() );
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }

  /**
   * Returns a String containing a description of the named attribute, or
   * null.
   *
   * @param elementName Name of the element.
   * @param attrName Name of the attribute.
   * @param locale Locale for which localization will be attempted, or null.
   *
   * @return The attribute description, or null if it doesn't exist.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   * @throws IllegalArgumentException If attrName is null, or is not a legal
   *                                  legal attribute name for this element.
   */
  public String getAttributeDescription(String elementName, String attrName,
					Locale locale)
    {
      // Allow any element to have the VR attribute
      if ( attrName.equals(VR_NAME) ) {

	// Set the Locale
	if (locale == null) { locale = Locale.getDefault(); }

	// Get the description from the resource bundle
	try {
	  String name = getResourceBaseName();
	  ResourceBundle bundle = ResourceBundle.getBundle(name, locale);

	  // Allow any element name
	  String key = "*/" + attrName;
	  return bundle.getString(key);
	}

	// Return null if no resource bundle is available
	catch (MissingResourceException e) { return null; }
      }

      return super.getAttributeDescription(elementName, attrName, locale);
   }

  /**
   * Gets the minimum number of allowed values.
   *
   * @param vm Value Multiplicity expression.
   *
   * @return Minimum number of allowed values extracted from the VM.
   */
  private int _getMin(String vm)
    {
      // Split the VM using the hyphen
      String[] components = vm.split("-");

      // Return the first component as an integer
      return Integer.valueOf(components[0]).intValue();
    }

  /**
   * Gets the maximum number of allowed values.
   *
   * @param vm Value Multiplicity expression.
   *
   * @return Maximum number of allowed values extracted from the VM.
   */
  private int _getMax(String vm)
    {
      // Split the VM using the hyphen
      String[] components = vm.split("-");

      // If there is only one component, return it the minimum
      if ( components.length == 1 ) { return _getMin(vm); }

      // Otherwise try to convert the second component to an integer
      try { return Integer.valueOf(components[1]).intValue(); }

      // Can't convert so must be unlimited
      catch (NumberFormatException nfe) { return Integer.MAX_VALUE; }
    }

  /**
   * Gets the data type corresponding to values of the specified VR.
   *
   * @param vr Value Representation to get a data type for.
   *
   * @return Data type corresponding to values of the specified VR.
   */
  private int _getDataType(String vr)
    {
      // String
      if ( vr.equals("AE") || vr.equals("AS") || vr.equals("CS") ||
	   vr.equals("DA") || vr.equals("DS") || vr.equals("DT") ||
	   vr.equals("IS") || vr.equals("LO") || vr.equals("PN") ||
	   vr.equals("SH") || vr.equals("TM") || vr.equals("UI") ||
	   vr.equals("LT") || vr.equals("ST") || vr.equals("UT") ||
	   vr.equals("AT") )
	{
	  return DATATYPE_STRING;
	}

      // Double
      if ( vr.equals("FD") ) { return DATATYPE_DOUBLE; }

      // Float
      if ( vr.equals("FL") ) { return DATATYPE_FLOAT; }

      // Integer
      if ( vr.equals("SL") || vr.equals("SS") || vr.equals("UL") ||
	   vr.equals("US") )
	{
	  return DATATYPE_INTEGER;
	}

      // Otherwise use a String
      return DATATYPE_STRING;
    }
}
