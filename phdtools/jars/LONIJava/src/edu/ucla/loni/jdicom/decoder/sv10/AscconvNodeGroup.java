/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

import java.util.Vector;

/**
 * Group of ASCCONV root nodes that share the same name.
 *
 * @version 24 August 2007
 */
public class AscconvNodeGroup
{
    /** Name of the group. */
    private String _name;

    /** Root Nodes. */
    private Vector _rootNodes;

    /**
     * Constructs an ASCCONV Node Group.
     *
     * @param name Name of the group.
     */
    public AscconvNodeGroup(String name)
    {
	_name = name;
	_rootNodes = new Vector();
    }

    /**
     * Gets the name of the group.
     *
     * @return Name of the group.
     */
    public String getName()
    {
	return _name;
    }

    /**
     * Adds the Root Node to the Group.
     *
     * @param rootNode ASCCONV Root Node to add to the Group.
     *
     * @return True if the Node was added; false otherwise.
     */
    public boolean add(AscconvRootNode rootNode)
    {
	// Add the Node if the name is the same as the Group
	if ( _name.equals(rootNode.getName()) ) {
	    _rootNodes.add(rootNode);
	    return true;
	}

	// Node was not added
	return false;
    }

    /**
     * Gets all the Root Nodes in the Group.
     *
     * @return All the Root Nodes in the Group.
     */
    public AscconvRootNode[] getRootNodes()
    {
	return (AscconvRootNode[])_rootNodes.toArray(new AscconvRootNode[0]);
    }
}
