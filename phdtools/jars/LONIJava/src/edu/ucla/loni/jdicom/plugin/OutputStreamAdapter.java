/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.stream.ImageOutputStream;

/**
 * Class that adapts the Image Output Stream interface into the Output Stream
 * class.
 *
 * @version 17 October 2002
 */
public class OutputStreamAdapter extends OutputStream
{
  /** Image Output Stream to adapt. */
  private ImageOutputStream _imageOutputStream;

  /**
   * Creates an OutputStreamAdapter.
   *
   * @param imageOutputStream Image Output Stream to adapt.
   *
   * @throws NullPointerException If the Image Output Stream is null.
   */
  public OutputStreamAdapter(ImageOutputStream imageOutputStream)
    {
      super();

      // Check for a null stream
      if (imageOutputStream == null) {
	String msg = "A null Image Output Stream is not allowed.";
	throw new NullPointerException(msg);
      }

      _imageOutputStream = imageOutputStream;
    }

  /**
   * Writes the specified byte to this output stream. The byte to be written
   * is the eight low-order bits of the argument <code>b</code>.
   *
   * @param b The byte to write.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void write(int b) throws IOException
    {
      _imageOutputStream.write(b);
    }

  /**
   * Writes a sequence of bytes to the stream at the current position.
   *
   * @param b An array of bytes to be written.
   * @param off The start offset in the data.
   * @param len The number of bytes to write.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void write(byte b[], int off, int len) throws IOException
    {
      try { _imageOutputStream.write(b, off, len); }

      catch (Exception e) {
	String msg = "Unable to write the byte array.";
	IOException exception = new IOException(msg);
	exception.initCause(e);
	throw exception;
      }
    }

  /**
   * Flushes this output stream and forces any buffered output bytes 
   * to be written out.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void flush() throws IOException
    {
      _imageOutputStream.flush();
    }

  /**
   * Closes this output stream and releases any system resources associated
   * with this stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void close() throws IOException
    {
      _imageOutputStream.close();
    }
}
