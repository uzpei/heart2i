/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.decoder;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;

/**
 * DataInputStream which provides the functionality of swapping bytes that are
 * read from the stream.
 *
 * @version 9 October 2002
 */
public class ByteSwappedInputStream extends DataInputStream
{
  /**
   * Constructs a ByteSwappedInputStream for the InputStream.
   *
   * @param inputStream InputStream to read bytes from.
   */
  public ByteSwappedInputStream(InputStream inputStream)
    {
      super(inputStream);
    }

  /**
   * Reads up to <code>byte.length</code> bytes of data from this 
   * input stream into an array of bytes. This method blocks until some 
   * input is available. 
   *
   * @param b The buffer into which the data is read.
   * @param inc Number of bytes to swap the data by.
   *
   * @return The total number of bytes read into the buffer, or
   *         <code>-1</code> if there is no more data because the end of
   *         the stream has been reached.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int read(byte[] b, int inc) throws IOException
    {
      return read(b, 0, b.length, inc);
    }

  /**
   * Reads up to <code>len</code> bytes of data from this input stream 
   * into an array of bytes. This method blocks until some input is 
   * available. 
   *
   * @param b The buffer into which the data is read.
   * @param off The start offset of the data.
   * @param len The maximum number of bytes read.
   * @param inc Number of bytes to swap the data by.
   *
   * @return The total number of bytes read into the buffer, or
   *         <code>-1</code> if there is no more data because the end of
   *         the stream has been reached.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int read(byte[] b, int off, int len, int inc) throws IOException
    {
      int bytesRead = read(b, off, len);
      _byteSwap(b, off, bytesRead, inc);
      return bytesRead;
    }

  /**
   * Reads <code>byte.length</code> bytes of data from this 
   * input stream into an array of bytes. This method blocks until all the 
   * input is available. 
   *
   * @param b The buffer into which the data is read.
   * @param inc Number of bytes to swap the data by.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void readFully(byte[] b, int inc) throws IOException
    {
      readFully(b, 0, b.length, inc);
    }

  /**
   * Reads <code>len</code> bytes of data from this input stream 
   * into an array of bytes. This method blocks until all the input is 
   * available. 
   *
   * @param b The buffer into which the data is read.
   * @param off The start offset of the data.
   * @param len The number of bytes to read.
   * @param inc Number of bytes to swap the data by.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void readFully(byte[] b, int off, int len, int inc) throws IOException
    {
      readFully(b, off, len);
      _byteSwap(b, off, len, inc);
    }

  /**
   * Skips over <code>n</code> bytes of data from this input stream,
   * discarding the skipped bytes.  This method blocks until all the input has 
   * been skipped. 
   *
   * @param n Number of bytes to be skipped.
   *
   * @throws EOFException If this stream reaches the end before skipping
   *                      all the bytes.
   * @throws IOException If an I/O error occurs.
   */
  public void skipFully(long n) throws IOException
    {
      long totalBytesSkipped = 0;

      // Skip over the bytes until skipped over the requested amount
      while (totalBytesSkipped < n) {
	long bytesSkipped = skip(n-totalBytesSkipped);

	// If bytes were skipped, add them to the total
	if (bytesSkipped > 0) { totalBytesSkipped += bytesSkipped; }

	// If no bytes were skipped, check for the end of the stream
	else {
	  int nextByte = read();

	  if (nextByte < 0) { throw new EOFException(); }
	  else { totalBytesSkipped++; }
	}
      }
    }

  /**
   * Reads two input bytes and returns a <code>short</code> value.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return 16-bit value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public short readShort(boolean byteSwap) throws IOException
    {
      if (!byteSwap) { return readShort(); }

      // Read and swap 2 bytes
      byte[] value = new byte[2];
      readFully(value, 2);

      // Read a short from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      return dis.readShort();
    }

  /**
   * Reads two input bytes and returns an <code>int</code> value in the
   * range <code>0</code> through <code>65535</code>.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return Unsigned 16-bit value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int readUnsignedShort(boolean byteSwap) throws IOException
    {
      if (!byteSwap) { return readUnsignedShort(); }

      // Read and swap 2 bytes
      byte[] value = new byte[2];
      readFully(value, 2);

      // Read an unsigned short from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      return dis.readUnsignedShort();
    }

  /**
   * Reads an input <code>char</code> and returns the <code>char</code> value.
   * A Unicode <code>char</code> is made up of two bytes.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return Unicode <code>char</code> read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public char readChar(boolean byteSwap) throws IOException
    {
      if (!byteSwap) { return readChar(); }

      // Read and swap 2 bytes
      byte[] value = new byte[2];
      readFully(value, 2);

      // Read a char from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      return dis.readChar();
    }

  /**
   * Reads four input bytes and returns an <code>int</code> value.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return <code>int</code> value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int readInt(boolean byteSwap) throws IOException
    {
      if (!byteSwap) { return readInt(); }

      // Read and swap 4 bytes
      byte[] value = new byte[4];
      readFully(value, 4);

      // Read an int from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      return dis.readInt();
    }

  /**
   * Reads four input bytes and returns a <code>long</code> value in the
   * range <code>0</code> through <code>2^32</code>.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return Unsigned 32-bit value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public long readUnsignedInt(boolean byteSwap) throws IOException
    {
      // Read 4 bytes into the back of an 8 byte array and swap every 4 bytes
      byte[] value = new byte[8];
      readFully(value, 4, 4, byteSwap ? 4 : 1);

      // Read a long from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      return dis.readLong();
    }

  /**
   * Reads eight input bytes and returns a <code>long</code> value.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return <code>long</code> value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public long readLong(boolean byteSwap) throws IOException
    {
      if (!byteSwap) { return readLong(); }

      // Read and swap 8 bytes
      byte[] value = new byte[8];
      readFully(value, 8);

      // Read a long from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      return dis.readLong();
    }

  /**
   * Reads four input bytes and returns a <code>float</code> value.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return <code>float</code> value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public float readFloat(boolean byteSwap) throws IOException
    {
      return Float.intBitsToFloat( readInt(byteSwap) );
    }

  /**
   * Reads eight input bytes and returns a <code>double</code> value.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return <code>double</code> value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public double readDouble(boolean byteSwap) throws IOException
    {
      return Double.longBitsToDouble( readLong(byteSwap) );
    }

  /**
   * Reads four input bytes and returns DataElement tag.
   *
   * @param byteSwap If true, the value is byte swapped; if false it is not.
   *
   * @return 8 digit hexadecimal value read.
   *
   * @throws IOException If an I/O error occurs.
   */
  public String readDataElementTag(boolean byteSwap) throws IOException
    {
      // Read 4 bytes and swap every 2 bytes
      byte[] value = new byte[4];
      readFully(value, byteSwap ? 2 : 1);

      // Read an int from the byte array
      ByteArrayInputStream bis = new ByteArrayInputStream(value);
      DataInputStream dis = new DataInputStream(bis);
      int tag = dis.readInt();

      // Convert the integer tag to a hexadecimal String tag
      String stringTag = Integer.toHexString(tag).toUpperCase();

      // Pad the beginning of the hexadecimal tag with zeroes to 8 digits
      while (stringTag.length() < 8) {
	stringTag = "0" + stringTag;
      }

      return stringTag;
    }

  /**
   * Byte swaps the data of the given length in the specified array starting
   * at the given offset and by swapping the given number of bytes.
   *
   * @param b Buffer containing the data.
   * @param off Offset from the beginning of the buffer to where the data
   *            starts.
   * @param len Length of the data in the buffer.
   * @param inc Number of bytes to swap the data by.
   */
  private void _byteSwap(byte[] b, int off, int len, int inc)
    {
      // Byte swapping is needed
      if (len >= inc && inc > 1) {

	// Only byte swap data which evenly divides by inc
	int dataLength = len - len%inc;

	// Byte swap the data in blocks of size equal to inc
	for (int block = off; block < off+dataLength; block += inc) {

	  // Byte swap the contents of each data block
	  for (int j = 0; j < inc/2; j++) {

	    // Swap the byte order
	    byte temp = b[block+j];
	    b[block+j] = b[block+inc-j-1];
	    b[block+inc-j-1] = temp;
	  }
	}
      }
    }
}
