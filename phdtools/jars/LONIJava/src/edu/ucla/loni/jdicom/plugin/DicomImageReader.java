/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.plugin;

import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import edu.ucla.loni.jdicom.decoder.DataSetDecoder;
import edu.ucla.loni.jdicom.decoder.MeasuredInputStream;
import edu.ucla.loni.jdicom.encoder.DataElementEncoder;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Image Reader for parsing and decoding part 10 DICOM images.
 *
 * @version 13 January 2009
 */
public class DicomImageReader extends ImageReader
{
    /** Data Set decoded from the input source. */
    private DataSet _dataSet;

    /** Index of the next image to decode from the input source. */
    private int _nextImageIndex;

    /**
     * Constructs a DicomImageReader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not a
     *                                  Dicom Image Reader Spi.
     */
    public DicomImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be a Dicom Image Reader Spi
	if ( !(originatingProvider instanceof DicomImageReaderSpi) ) {
	    String msg="Originating provider must be a DICOM Image Reader SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }
    
    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The ImageInputStream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of
     *                                  the classes returned by the originating
     *                                  service provider's getInputTypes method,
     *                                  or is not an ImageInputStream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	// Input Streams must support marking if they can seek backwards
	if (input instanceof InputStream) {
	    InputStream inputStream = (InputStream)input;
	    if ( !inputStream.markSupported() && seekForwardOnly == false) {
		String msg = "The Input Stream must support marking if it is " +
		    "to seek backwards.";
		throw new IllegalStateException(msg);
	    }
	}

	// Adapt an Image Input Stream
	if (input instanceof ImageInputStream) {
	    input = new InputStreamAdapter((ImageInputStream)input);
	}

	super.setInput(input, seekForwardOnly, ignoreMetadata);
	_nextImageIndex = 0;

	// Remove any decoded Data Set
	_dataSet = null;
    }

    /**
     * Returns the number of images, not including thumbnails, available from
     * the current input source.
     * 
     * @param allowSearch If true, the true number of images will be returned
     *                    even if a search is required.  If false, the reader
     *                    may return -1 without performing the search.
     *
     * @return The number of images or -1 if allowSearch is false and a search
     *         would be required.
     *
     * @throws IllegalStateException If the input source has not been set, or if
     *                               the input has been specified with
     *                               seekForwardOnly and allowsearch set to
     *                               true.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    public int getNumImages(boolean allowSearch) throws IOException
    {
	// Check for an illegal state
	if (allowSearch == true && seekForwardOnly == true) {
	    String msg = "Cannot both allow a search and only seek forward.";
	    throw new IllegalStateException(msg);
	}

	// Get the number of images
	DicomMetadata metadata = (DicomMetadata)getStreamMetadata();
	int numberOfImages = metadata.getNumberOfImages();

	// Unable to determine the number of images
	if (numberOfImages == -1) {
	    String msg = "Unable to determine the number of images.";
	    throw new IOException(msg);
	}

	return numberOfImages;
    }

    /**
     * Returns the width in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The width of the image in pixels.
     *
     * @throws IOException If an error occurs reading the width information from
     *                     the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getWidth(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image width
	DicomMetadata metadata = (DicomMetadata)getStreamMetadata();
	int width = metadata.getImageWidth();

	// Unable to determine the image width
	if (width == -1) {
	    String msg = "Unable to determine the image width.";
	    throw new IOException(msg);
	}

	return width;
    }

    /**
     * Returns the height in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The height of the image in pixels.
     *
     * @throws IOException If an error occurs reading the height information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getHeight(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image height
	DicomMetadata metadata = (DicomMetadata)getStreamMetadata();
	int height = metadata.getImageHeight();

	// Unable to determine the image height
	if (height == -1) {
	    String msg = "Unable to determine the image height.";
	    throw new IOException(msg);
	}

	return height;
    }

    /**
     * Returns an Iterator containing possible image types to which the given
     * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
     * legal image type will be returned.
     *
     * @param imageIndex The index of the image to be retrieved.
     *
     * @return An Iterator containing at least one ImageTypeSpecifier
     *         representing suggested image types for decoding the current given
     *         image.
     *
     * @throws IOException If an error occurs reading the format information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public Iterator getImageTypes(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get image parameters
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// RGB image with uncompressed data
	DicomMetadata metadata = (DicomMetadata)getStreamMetadata();
	if ( metadata.hasRgbImages() ) {
	    ArrayList list = new ArrayList(1);
	    list.add( Utilities.getRgbImageType(width, height) );
	    return list.iterator();
	}

	// Grayscale image with uncompressed data
	else if ( metadata.hasGrayScaleImages() ) {
	    int bitsPerPixel = metadata.getBitsPerPixel();
	    if (bitsPerPixel != -1) {
		ArrayList list = new ArrayList(1);

		// 16 bit signed grayscale data
		if (bitsPerPixel == 16 && metadata.hasSignedData()) {
		    list.add(Utilities.getGrayImageType(DataBuffer.TYPE_SHORT));
		    return list.iterator();
		}

		// Unsigned grayscale data
		list.add( Utilities.getGrayImageType(width, height,
						     bitsPerPixel) );
		return list.iterator();
	    }
	}

	// Unable to recognize the image type
	String msg = "Unable to recognize the image type.";
	throw new IOException(msg);
    }

    /**
     * Returns an IIOMetadata object representing the metadata associated with
     * the input source as a whole (i.e., not associated with any particular
     * image), or null if the reader does not support reading metadata, is set
     * to ignore metadata, or if no metadata is available.
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     */
    public IIOMetadata getStreamMetadata() throws IOException
    {
	// Create an empty stream Data Set
	DataSet streamDataSet = new DataSet();

	// Copy the Data Elements of the decoded Data Set
	Enumeration enun = _getDataSet().getDataElements();
	while ( enun.hasMoreElements() ) {
	    DataElement dataElement = (DataElement)enun.nextElement();

	    // Do not copy the image metadata Data Element
	    if ( !dataElement.getDataElementTag().equals("52009230") ) {
		streamDataSet.addDataElement(dataElement);
	    }
	}

	// Create and return the stream DICOM Metadata
	String name = DicomStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	String className = DicomStreamMetadataFormat.class.getName();
	String extraName = DicomStreamMetadataFormat.METADATA_1_1_FORMAT_NAME;
	String extraClassName = DicomStreamMetadataFormat.class.getName();
	return new DicomMetadata(name, className, extraName, extraClassName,
				 streamDataSet);
    }

    /**
     * Returns an IIOMetadata object containing metadata associated with the
     * given image, or null if the reader does not support reading metadata, is
     * set to ignore metadata, or if no metadata is available.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved. 
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image metadata Data Element
	DataElement dataElement = _getDataSet().getDataElement("52009230");

	// No image metadata found
	if (dataElement == null) { return null; }

	// Find the nested Data Set that implicitly corresponds to the index
	int implicitIndex = 0;
	Enumeration enm = dataElement.getDataSets();
	while ( enm.hasMoreElements() ) {
	    DataSet imageDataSet = (DataSet)enm.nextElement();

	    // Found the implicit index
	    if (implicitIndex == imageIndex) {

		// No image metadata found
		Enumeration enumElements = imageDataSet.getDataElements();
		if ( !enumElements.hasMoreElements() ) { return null; }

		// Return the corresponding image DICOM Metadata
		String name =
		    DicomImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
		String className = DicomImageMetadataFormat.class.getName();
		return new DicomMetadata(name, className, imageDataSet);
	    }

	    implicitIndex++;
	}

	// No corresponding Data Set found
	return null;
    }

    /**
     * Reads the image indexed by imageIndex and returns it as a complete
     * BufferedImage.
     *
     * @param imageIndex The index of the image to be retrieved.
     * @param param An ImageReadParam used to control the reading process, or
     *              null.
     *
     * @return The desired portion of the image as a BufferedImage.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public BufferedImage read(int imageIndex, ImageReadParam param)
	throws IOException
    {
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// If the input source can only be read once, check the minimum index
	if (seekForwardOnly == true && imageIndex < minIndex) {
	    String msg = "Cannot read the image with index " + imageIndex +
		" because the input source has already passed it.";
	    throw new IndexOutOfBoundsException(msg);
	}

	// Adjust the input source position if required
	if (imageIndex != _nextImageIndex) {
	    InputStream inputStream = _getInputStream();

	    // If marking is supported, reset the position to the 1st image
	    if ( inputStream.markSupported() ) {
		inputStream.reset();
		inputStream.mark(Integer.MAX_VALUE/2);
		_nextImageIndex = 0;
	    }

	    // Calculate the encoded size of each image in bytes
	    ImageTypeSpecifier spec=(ImageTypeSpecifier)getImageTypes(0).next();
	    int dataType = spec.getSampleModel().getDataType();
	    int imageSize = width*height;
	    if (dataType == DataBuffer.TYPE_SHORT) { imageSize *= 2; }
	    if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }
	    if (dataType == DataBuffer.TYPE_INT) { imageSize *= 3; }

	    // Skip to the requested image
	    long bytesToSkip = (imageIndex-_nextImageIndex)*imageSize;
	    long bytesSkipped = 0;
	    while (bytesSkipped < bytesToSkip) { 
		bytesSkipped += inputStream.skip(bytesToSkip-bytesSkipped);
	    }
	}

	// Update the Listeners
	processImageStarted(imageIndex);

	// Read a Buffered Image from the input source
	BufferedImage bufferedImage = _readBufferedImage(width, height, param);
	_nextImageIndex = imageIndex + 1;

	// Reset the minimum image index if required
	if (seekForwardOnly == true) { minIndex = imageIndex + 1; }

	// Update the Listeners
	if ( abortRequested() ) { processReadAborted(); }
	else { processImageComplete(); }

	// Return the requested image
	return bufferedImage;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageReader.  Otherwise, the reader may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the input stream
	try { if (input != null) { ((InputStream)input).close(); } }
	catch (Exception e) {}

	input = null;
    }

    /**
     * Reads pixel data from the input source and creates a Buffered Image.
     *
     * @param width Width of the image to read.
     * @param height Height of the image to read.
     * @param param An ImageReadParam used to control the reading process, or
     *              null.
     *
     * @return Buffered Image containing pixel data from the input source.
     *
     * @throws IOException If an error occurs during reading, or if the width or
     *                     height is less than 1.
     * @throws IllegalStateException If the input source has not been set.
     */
    private BufferedImage _readBufferedImage(int width, int height,
					     ImageReadParam param)
	throws IOException
    {
	InputStream inputStream = _getInputStream();

	// Check that the image width and height are not less than 1
	if (width < 1 || height < 1) {
	    String msg = "Image width or height is less than 1.";
	    throw new IOException(msg);
	}

	// Get the data type and image data size
	ImageTypeSpecifier its = (ImageTypeSpecifier)getImageTypes(0).next();
	int dataType = its.getSampleModel().getDataType();
	int dataSize = 2*width*height;
	if (dataType == DataBuffer.TYPE_INT) { dataSize = 3*width*height; }
	if (dataType == DataBuffer.TYPE_BYTE) { dataSize = width*height; }

	// Return the raw image data if required
	if ( Utilities.returnRawBytes(param) ) {
	    BufferedImage bufferedImage = Utilities.getRawByteImage(dataSize);
	    DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	    inputStream.read( ((DataBufferByte)db).getData() );
	    return bufferedImage;
	}

	// Create a Buffered Image for the image data
	Iterator iter = getImageTypes(0);
	BufferedImage bufferedImage = getDestination(null, iter, width, height);
	DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

	// RGB image with uncompressed data
	if (dataType == DataBuffer.TYPE_INT) {

	    // Read the image data
	    byte[] srcBuffer = new byte[dataSize];
	    inputStream.read(srcBuffer);

	    // Determine the color component ordering
	    int planarConfiguration = _getIntValue("00280006", 0);
	    boolean isInterleaved = (planarConfiguration == 0);

	    // Return the RGB image
	    return Utilities.getRgbImage(srcBuffer, bufferedImage.getRaster(),
					 isInterleaved);
	}

	// 8-bit grayscale image with uncompressed data
	else if (dataType == DataBuffer.TYPE_BYTE) {

	    // Read the image data
	    byte[] srcBuffer = new byte[dataSize];
	    inputStream.read(srcBuffer);

	    // Determine whether the pixels need to be inverted
	    String photoInterpret = _getStringValue("00280004", "MONOCHROME2");
	    boolean isInverted = photoInterpret.equals("MONOCHROME1");

	    // Return the gray byte image
	    int bitsUsedPerByte = _getIntValue("00280101", 0);
	    return Utilities.getGrayByteImage(srcBuffer,
					      bufferedImage.getRaster(),
					      bitsUsedPerByte, isInverted);
	}

	// 16-bit grayscale image with signed pixel data values
	else if (dataType == DataBuffer.TYPE_SHORT) {

	    // Read the image data
	    byte[] srcBuffer = new byte[dataSize];
	    inputStream.read(srcBuffer);

	    // Byte swap using the transfer syntax (default of Implict VR)
	    String transSyntax = _getStringValue("00020010",
						 "1.2.840.10008.1.2");
	    boolean isByteSwapped = !transSyntax.equals("1.2.840.10008.1.2.2");

	    // Return the gray short image
	    return Utilities.getGrayImage(srcBuffer, bufferedImage.getRaster(),
					  isByteSwapped);
	}

	// 16-bit grayscale image with uncompressed data
	else {

	    // Read the image data
	    byte[] srcBuffer = new byte[dataSize];
	    inputStream.read(srcBuffer);

	    // Determine whether the pixels need to be inverted
	    String photoInterpret = _getStringValue("00280004", "MONOCHROME2");
	    boolean isInverted = photoInterpret.equals("MONOCHROME1");

	    // Byte swap using the transfer syntax (default of Implict VR)
	    String transSyntax = _getStringValue("00020010",
						 "1.2.840.10008.1.2");
	    boolean isByteSwapped = !transSyntax.equals("1.2.840.10008.1.2.2");

	    // If the image modality is CT, convert from Hounsfield Units
	    float slope = 1;
	    int intercept = 0;
	    String modality = _getStringValue("00080060", "?");
	    if ( modality.equals("CT") ) {
		slope = _getFloatValue("00281053", 1);
		intercept = _getIntValue("00281052", 0) + 1024;
	    }

	    // Return the gray ushort image
	    int bitsUsedPerShort = _getIntValue("00280101", 0);
	    return Utilities.getGrayUshortImage(srcBuffer,
						bufferedImage.getRaster(),
						bitsUsedPerShort, isInverted,
						isByteSwapped, slope,intercept);
	}
    }

    /**
     * Gets the Data Set from the input source.  If the Data Set has not yet
     * been acquired, it is decoded from the input source.
     *
     * @throws IOException If an I/O error occurs.
     * @throws IllegalStateException If the input source has not been set.
     */
    private DataSet _getDataSet() throws IOException
    {
	// Decode the Data Set if needed
	if (_dataSet == null) {

	    // Create a Data Input Stream out of the Input Stream
	    InputStream inputStream = _getInputStream();
	    DataInputStream dStream = new DataInputStream(inputStream);

	    // Read in the 128 byte File Preamble
	    byte[] filePreamble = new byte[128];
	    dStream.readFully(filePreamble);

	    // Read in the ID characters
	    byte[] idBytes = new byte[4];
	    dStream.readFully(idBytes);

	    // Input is not encoded in part 10
	    InputStream trueStream = inputStream;
	    String idString = new String(idBytes);
	    if ( !idString.equals("DICM") ) {

		// Create the default DICOM transfer syntax Data Element
		String tag = "00020010";
		String vr = "UI";
		String name = "Transfer Syntax UID";
		DataElement tSyntax = new DataElement(tag, vr, name);

		// First Data Element is a group 8 Element and it is encoded
		// using the illegal DICOM transfer syntax
		String value = "1.2.840.10008.1.2";
		if (filePreamble[0] == 0 && filePreamble[1] == 8) {
		    value = "BIG_ENDIAN_IMPLICIT_VR";
		}
		tSyntax.addValue(value);

		// Write the Data Element to a byte buffer
		String group2Value = "1.2.840.10008.1.2.1";
		ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
		DataElementEncoder encoder =
		    new DataElementEncoder(baoStream, group2Value);
		encoder.writeDataElement(tSyntax);
		byte[] tSyntaxBytes = baoStream.toByteArray();

		// Create a buffer that holds the new header
		int tSyntaxLength = tSyntaxBytes.length;
		byte[] headerBytes = new byte[128 + 4 + tSyntaxLength];
		System.arraycopy(tSyntaxBytes,0,headerBytes,0,tSyntaxLength);
		System.arraycopy(filePreamble,0,headerBytes,tSyntaxLength,128);
		System.arraycopy(idBytes,0,headerBytes,tSyntaxLength + 128, 4);

		// Prepend the header buffer to the Input Stream
		trueStream = new PrependedInputStream(trueStream, headerBytes);
	    }

	    // Decode the Data Set up to the pixel data
	    MeasuredInputStream mStream = new MeasuredInputStream(trueStream);
	    DataSetDecoder decoder = new DataSetDecoder(mStream, "7FE00010");
	    _dataSet = decoder.getDataSet();

	    // Remove default DICOM transfer syntax Data Element if not part 10
	    if ( !idString.equals("DICM") ) {
		_dataSet.removeDataElement("00020010");
	    }

	    // Mark the current stream location if applicable
	    if ( inputStream.markSupported() ) {
		inputStream.mark(Integer.MAX_VALUE/2);
	    }
	}

	// Return the Data Set
	return _dataSet;
    }

    /**
     * Gets an Input Stream to the source.
     *
     * @return Input Stream to use to read source data from.
     *
     * @throws IllegalStateException If the input source has not been set.
     */
    private InputStream _getInputStream()
    {
	// No input source to decode from
	if (input == null) {
	    String msg = "No input source has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Input Stream
	return (InputStream)input;
    }

    /**
     * Gets the String value of the first Value of the specified Data Element
     * from the decoded Data Set.  If this value is not available, the given
     * default value is returned.    If the first non-space character is '+',
     * it is removed.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue String value used as the default.
     *
     * @throws IOException If an I/O error occurs.
     * @throws IllegalStateException If the input source has not been set.
     */
    private String _getStringValue(String dataElementTag, String defaultValue)
	throws IOException
    {
	// Attempt to get the value
	try {
	    Enumeration enun =
		_getDataSet().getDataElement(dataElementTag).getValues();

	    String value = enun.nextElement().toString();

	    // Return value w/o beginning and ending spaces (and no leading +)
	    value = value.trim();
	    if ( value.charAt(0) == '+') { value = value.substring(1); }
	    return value;
	}

	// On all Exceptions, return the default value
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Gets the integer value of the first child Value Node of the specified
     * Data Element Node.  If this value is not available, the given default
     * value is returned.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue Integer value used as the default.

     *
     * @throws IOException If an I/O error occurs.
     * @throws IllegalStateException If the input source has not been set.
     */
    private int _getIntValue(String dataElementTag, int defaultValue)
	throws IOException
    {
	// Get the value as a String
	String value = _getStringValue(dataElementTag,
				       Integer.toString(defaultValue));

	// Return the value as an integer
	try { return Integer.parseInt(value); }
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Gets the float value of the first child Value Node of the specified
     * Data Element Node.  If this value is not available, the given default
     * value is returned.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue Float value used as the default.
     *
     * @throws IOException If an I/O error occurs.
     * @throws IllegalStateException If the input source has not been set.
     */
    private float _getFloatValue(String dataElementTag, float defaultValue)
	throws IOException
    {
	// Get the value as a String
	String value = _getStringValue(dataElementTag,
				       Float.toString(defaultValue));

	// Return the value as an float
	try { return Float.parseFloat(value); }
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Checks whether or not the specified image index is valid.
     *
     * @param imageIndex The index of the image to check.
     *
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private void _checkImageIndex(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	if ( imageIndex >= getNumImages(false) ) {
	    String msg = "Image index of " + imageIndex +
		" >= the total number " +
		"of images (" + getNumImages(false) + ")";
	    throw new IndexOutOfBoundsException(msg);
	}
    }
}
