/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.decoder;

import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.IOException;

/**
 * InputStream which keeps track of the offset between a specified byte in the
 * stream and the current position in the stream.  By default, the specified
 * byte is the first byte available to the MeasuredInputStream upon its
 * creation.
 *
 * @version 14 December 2000
 */
public class MeasuredInputStream extends FilterInputStream
{
  /** Number of bytes from the specified byte in the stream. */
  private long _offset = 0;

  /**
   * Constructs a MeasuredInputStream for the InputStream.  By default, bytes
   * are measured from the first byte available in the InputStream.
   *
   * @param inputStream InputStream to measure bytes in.
   */
  public MeasuredInputStream(InputStream inputStream)
    {
      super(inputStream);
    }

  /**
   * Gets the offset (in bytes) from the specified byte in the stream.
   *
   * @return Number of bytes from the specified byte to the current position
   *         in the stream.
   */
  public long getOffset()
    {
      return _offset;
    }

  /**
   * Sets the current offset (in bytes) from the specified byte in the stream.
   *
   * @param offset Number of bytes from the specified byte to the current
   *               position in the stream.  An offset of zero, for example,
   *               would set the specified byte to the current stream position.
   */
  public void setOffset(long offset)
    {
      _offset = offset;
    }

  /**
   * Reads the next byte of data from the input stream. The value byte is
   * returned as an <code>int</code> in the range <code>0</code> to
   * <code>255</code>. If no byte is available because the end of the stream
   * has been reached, the value <code>-1</code> is returned. This method
   * blocks until input data is available, the end of the stream is detected,
   * or an exception is thrown.
   *
   * @return The next byte of data, or <code>-1</code> if the end of the
   *         stream is reached.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int read() throws IOException
    {
      int oneByte = super.read();
      if (oneByte >= 0) { _offset++; }
      return oneByte;
    }

  /**
   * Reads up to <code>len</code> bytes of data from this input stream 
   * into an array of bytes. This method blocks until some input is 
   * available. 
   *
   * @param b The buffer into which the data is read.
   * @param off The start offset of the data.
   * @param len The maximum number of bytes read.
   *
   * @return The total number of bytes read into the buffer, or
   *         <code>-1</code> if there is no more data because the end of
   *         the stream has been reached.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int read(byte[] b, int off, int len) throws IOException
    {
      int bytesRead = super.read(b, off, len);
      if (bytesRead > 0) { _offset += bytesRead; }
      return bytesRead;
    }

  /**
   * Skips over and discards <code>n</code> bytes of data from the 
   * input stream. The <code>skip</code> method may, for a variety of 
   * reasons, end up skipping over some smaller number of bytes, 
   * possibly <code>0</code>. The actual number of bytes skipped is 
   * returned. 
   *
   * @param n The number of bytes to be skipped.
   *
   * @return The actual number of bytes skipped.
   *
   * @throws IOException If an I/O error occurs.
   */
  public long skip(long n) throws IOException
    {
      long bytesSkipped = super.skip(n);
      if (bytesSkipped > 0) { _offset += bytesSkipped; }
      return bytesSkipped;
    }

  /**
   * Tests if this input stream supports the <code>mark</code> 
   * and <code>reset</code> methods.  Currently use of these
   * methods is not supported.
   *
   * @return <code>true</code> if this stream type supports the
   *         <code>mark</code> and <code>reset</code> method;
   *         <code>false</code> otherwise.
   */
  public boolean markSupported()
    {
      return false;
    }
}
