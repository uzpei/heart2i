/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import edu.ucla.loni.jdicom.decoder.sv10.SV10DataSetDecoder;
import java.lang.reflect.Array;
import java.util.Enumeration;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for metadata of the part 10 DICOM image format.
 *
 * @version 13 January 2009
 */
public class DicomMetadata extends AppletFriendlyIIOMetadata
{
    /** Data Set containing the original metadata. */
    private DataSet _originalDataSet;

    /** Data Set containing the current metadata. */
    private DataSet _currentDataSet;

    /**
     * Constructs a DicomMetadata from the given Data Set.
     *
     * @param nativeMetadataFormatName Name of the native metadata format.
     * @param nativeMetadataFormatClassName Name of the class of the native
     *                                      metadata format.
     * @param dataSet Data Set containing the metadata.
     */
    public DicomMetadata(String nativeMetadataFormatName,
			 String nativeMetadataFormatClassName, DataSet dataSet)
    {
	this(nativeMetadataFormatName, nativeMetadataFormatClassName,
	     null, null, dataSet);
    }

    /**
     * Constructs a DicomMetadata from the given Data Set.
     *
     * @param nativeMetadataFormatName Name of the native metadata format.
     * @param nativeMetadataFormatClassName Name of the class of the native
     *                                      metadata format.
     * @param extraMetadataFormatName Name of the extra metadata format.
     * @param extraMetadataFormatClassName Name of the class of the extra
     *                                     metadata format.
     * @param dataSet Data Set containing the metadata.
     */
    public DicomMetadata(String nativeMetadataFormatName,
			 String nativeMetadataFormatClassName,
			 String extraMetadataFormatName,
			 String extraMetadataFormatClassName,
			 DataSet dataSet)
    {
	super(true, nativeMetadataFormatName, nativeMetadataFormatClassName,
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
			   extraMetadataFormatName},
	      new String[]{BasicMetadataFormat.class.getName(),
			   extraMetadataFormatClassName});

	_originalDataSet = dataSet;
	_currentDataSet = dataSet;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return DicomMetadataConversions.toTree(formatName, _currentDataSet);
	}

	// Metadata 1.1 format name
	String format1Name = DicomStreamMetadataFormat.METADATA_1_1_FORMAT_NAME;
	if ( format1Name.equals(formatName) ) {

	    // Create a copy of the Data Set
	    DataSet dataSet = new DataSet();
	    Enumeration enun = _currentDataSet.getDataElements();
	    while ( enun.hasMoreElements() ) {
		DataElement dataElement = (DataElement)enun.nextElement();

		// Siemens' private SV10 tags
		String tag = dataElement.getDataElementTag();
		if ( "00291010".equals(tag) || "00291020".equals(tag) ) {
		    try {

			// Get the byte data in the tag
			Enumeration enun2 = dataElement.getValues();
			byte[] b = (byte[])enun2.nextElement();

			// Decode the bytes into a Data Set
			SV10DataSetDecoder decoder = new SV10DataSetDecoder(b);
			DataSet sv10DataSet = decoder.getDataSet();

			// Create a new Data Element
			String vr = "SQ";
			String desc = "Siemens private SV 10 information.";
			dataElement = new DataElement(tag, vr, desc);
			dataElement.addDataSet(sv10DataSet);
		    }
		    catch (Exception e) {}
		}

		dataSet.addDataElement(dataElement);
	    }

	    // Convert the Data Set copy
	    return DicomMetadataConversions.toTree(formatName, dataSet);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current Data Set, convert the root
	Node r;
	if (_currentDataSet == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Create a new root Node
	    r = new IIOMetadataNode(nativeMetadataFormatName);

	    // Create a new primary Data Set Node
	    String dataSetName = DicomMetadataFormat.DATA_SET_NAME;
	    IIOMetadataNode dataSetNode = new IIOMetadataNode(dataSetName);
	    r.appendChild(dataSetNode);

	    // Add all the Data Element Nodes of the root Node
	    NodeList nodeList = root.getFirstChild().getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node dataElementNode = nodeList.item(i);
		dataSetNode.appendChild( _cloneNode(dataElementNode) );
	    }

	    // Add all the current Data Element Nodes that aren't yet present
	    nodeList = getAsTree(formatName).getFirstChild().getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node dataElementNode = nodeList.item(i);

		// Add the Data Element Node if it doesn't exist
		if ( !_hasChild(dataElementNode.getNodeName(), dataSetNode) ) {
		    dataSetNode.appendChild( _cloneNode(dataElementNode) );
		}
	    }
	}

	// Set a new current Data Set
	_currentDataSet = DicomMetadataConversions.toDataSet(r);
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current Data Set to the original
	_currentDataSet = _originalDataSet;
    }

    /**
     * Gets the number of images.
     *
     * @return Number of images, or -1 if it cannot be determined.
     */
    public int getNumberOfImages()
    {
	// Return the number of frames (default of 1)
	return _getIntValue("00280008", 1);
    }

    /**
     * Gets the width of each image.
     *
     * @return Width of each image, or -1 if it cannot be determined.
     */
    public int getImageWidth()
    {
	// Return the number of columns (default of -1)
	return _getIntValue("00280011", -1);
    }

    /**
     * Gets the height of each image.
     *
     * @return Height of each image, or -1 if it cannot be determined.
     */
    public int getImageHeight()
    {
	// Return the number of rows (default of -1)
	return _getIntValue("00280010", -1);
    }

    /**
     * Gets the number of bits per pixel used by each image.
     *
     * @return Number of bits per pixel used by each image, or -1 if it cannot
     *         be determined.
     */
    public int getBitsPerPixel()
    {
	// Get the number of bits per pixel allocated for image data (def -1)
	return _getIntValue("00280100", -1);
    }

    /**
     * Determines whether or not each image has signed values.
     *
     * @return True if each image has signed values; false otherwise.
     */
    public boolean hasSignedData()
    {
	// Check only PET data
	String modality = _getStringValue("00080060", "?");
	if ( !"PT".equals(modality) ) { return false; }

	// Use the pixel representation (default to unsigned)
	int pr = _getIntValue("00280103", 0);
	return pr == 1;
    }

    /**
     * Determines whether or not each image is a grayscale image.
     *
     * @return True if the images are grayscale; false otherwise.
     */
    public boolean hasGrayScaleImages()
    {
	// Get the transfer syntax (default for Implicit VR)
	String ts = _getStringValue("00020010", "1.2.840.10008.1.2");

	// Get the photometric interpretation (default of grayscale)
	String pi = _getStringValue("00280004", "MONOCHROME2");

	// Grayscale image with uncompressed data
	if ( (ts.equals("1.2.840.10008.1.2") ||
	      ts.equals("1.2.840.10008.1.2.1") ||
	      ts.equals("1.2.840.10008.1.2.2")) &&
	     (pi.equals("MONOCHROME1") || pi.equals("MONOCHROME2")) )
	    {
		return true;
	    }

	return false;
    }

    /**
     * Determines whether or not each image is an RGB image.
     *
     * @return True if the images are RGB images; false otherwise.
     */
    public boolean hasRgbImages()
    {
	// Get the transfer syntax (default for Implicit VR)
	String ts = _getStringValue("00020010", "1.2.840.10008.1.2");

	// Get the photometric interpretation (default of grayscale)
	String pi = _getStringValue("00280004", "MONOCHROME2");

	// RGB image with uncompressed data
	if ( (ts.equals("1.2.840.10008.1.2") ||
	      ts.equals("1.2.840.10008.1.2.1") ||
	      ts.equals("1.2.840.10008.1.2.2")) && pi.equals("RGB") )
	    {
		return true;
	    }

	return false;
    }

    /**
     * Determines whether or not each image is inverted.
     *
     * @return True if each image is inverted; false otherwise.
     */
    public boolean isInverted()
    {
	// Get the photometric interpretation (default of grayscale)
	String pi = _getStringValue("00280004", "MONOCHROME2");

	// Determine whether the pixels need to be inverted
	if ( pi.equals("MONOCHROME1") ) { return true; }
	return false;
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// RGB image with uncompressed data
	if ( hasRgbImages() ) { return Utilities.getRgbChromaNode(); }

	// Grayscale image with uncompressed data
	if ( hasGrayScaleImages() ) {
	    return Utilities.getGrayScaleChromaNode( isInverted() );
	}

	// Unknown image type
	return null;
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	// RGB image with uncompressed data
	if ( hasRgbImages() ) { return Utilities.getRgbDataNode(); }

	// Grayscale image with uncompressed data
	if ( hasGrayScaleImages() ) {
	    int bitsPerPixel = getBitsPerPixel();

	    // No information available
	    if (bitsPerPixel == -1) { return null; }

	    // Otherwise support grayscale data
	    return Utilities.getGrayScaleDataNode(bitsPerPixel);
	}

	// Unknown image type
	return null;
    }

    /**
     * Returns an IIOMetadataNode representing the dimension format information
     * of the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the dimension information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Get the pixel spacing
	    Enumeration enun =
		_currentDataSet.getDataElement("00280030").getValues();

	    // Convert the strings to floats
	    float spacingX = Float.parseFloat( enun.nextElement().toString() );
	    float spacingY = Float.parseFloat( enun.nextElement().toString() );

	    // return the Dimension Node
	    return Utilities.getDimensionNode(spacingX, spacingY);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata.
     */
    private Node _getBasicTree()
    {
	// Image dimensions
	int imageWidth = getImageWidth();
	int imageHeight = getImageHeight();
	int numberOfImages = getNumberOfImages();

	// Pixel dimensions
	float pixelWidth = 0;
	float pixelHeight = 0;
	try {
	    Enumeration enun =
		_currentDataSet.getDataElement("00280030").getValues();

	    // Convert the strings to floats
	    pixelWidth = Float.parseFloat( enun.nextElement().toString() );
	    pixelHeight = Float.parseFloat( enun.nextElement().toString() );
	}
	catch (Exception e) {}

	// Slice thickness
	double sliceThickness = _getDoubleValue("00180050", 0);

	// Bits per pixel
	int bitsPerPixel = getBitsPerPixel();

	// Data type
	String dataType = "Undetermined";
	if ( hasRgbImages() ) { dataType = BasicMetadataFormat.BYTE_TYPE; }
	if (hasGrayScaleImages() && bitsPerPixel != -1) {
	    if (bitsPerPixel == 16 && hasSignedData()) {
		dataType = BasicMetadataFormat.SIGNED_SHORT_TYPE;
	    }
	    else if (bitsPerPixel <= 8) {
		dataType = BasicMetadataFormat.BYTE_TYPE;
	    }
	    else if (bitsPerPixel <= 16) {
		dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE;
	    }
	}

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;
	if ( hasRgbImages() ) {colorType = BasicMetadataFormat.RGB_TYPE;}

	// Return the basic tree based upon available information
	if (pixelWidth == 0 && pixelHeight == 0 && sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, dataType,
						  bitsPerPixel, colorType);
	}
	if (sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, pixelWidth,
						  pixelHeight, dataType,
						  bitsPerPixel, colorType);
	}
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }

    /**
     * Gets the String value of the first Value of the specified Data Element
     * from the decoded Data Set.  If this value is not available, the given
     * default value is returned.    If the first non-space character is '+',
     * it is removed.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue String value used as the default.
     *
     * @throws IllegalStateException If the input source has not been set.
     */
    private String _getStringValue(String dataElementTag, String defaultValue)
    {
	// Attempt to get the value
	try {
	    Enumeration enun =
		_currentDataSet.getDataElement(dataElementTag).getValues();

	    String value = enun.nextElement().toString();

	    // Return value w/o beginning and ending spaces (and no leading +)
	    value = value.trim();
	    if ( value.charAt(0) == '+') { value = value.substring(1); }
	    return value;
	}

	// On all Exceptions, return the default value
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Gets the integer value of the first child Value Node of the specified
     * Data Element Node.  If this value is not available, the given default
     * value is returned.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue Integer value used as the default.

     *
     * @throws IllegalStateException If the input source has not been set.
     */
    private int _getIntValue(String dataElementTag, int defaultValue)
    {
	// Get the value as a String
	String value = _getStringValue(dataElementTag,
				       Integer.toString(defaultValue));

	// Return the value as an integer
	try { return Integer.parseInt(value); }
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Gets the float value of the first child Value Node of the specified
     * Data Element Node.  If this value is not available, the given default
     * value is returned.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue Float value used as the default.

     *
     * @throws IllegalStateException If the input source has not been set.
     */
    private float _getFloatValue(String dataElementTag, float defaultValue)
    {
	// Get the value as a String
	String value = _getStringValue(dataElementTag,
				       Float.toString(defaultValue));

	// Return the value as an float
	try { return Float.parseFloat(value); }
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Gets the double value of the first child Value Node of the specified
     * Data Element Node.  If this value is not available, the given default
     * value is returned.
     *
     * @param dataElementTag Tag that uniquely identifies the DataElement.  This
     *                       tag is represented as an 8 digit hexadecimal
     *                       number.
     *                       The first 4 digits represent the Group Number and
     *                       the last 4 digits represent the Element Number.
     * @param defaultValue Double value used as the default.

     *
     * @throws IllegalStateException If the input source has not been set.
     */
    private double _getDoubleValue(String dataElementTag, double defaultValue)
    {
	// Get the value as a String
	String value = _getStringValue(dataElementTag,
				       Double.toString(defaultValue));

	// Return the value as an double
	try { return Double.parseDouble(value); }
	catch (Exception e) { return defaultValue; }
    }

    /**
     * Determines whether or not the named Node is a child of the specified root
     * Node.
     *
     * @param childNodeName Name of the child Node.
     * @param root DOM Node containing the child Nodes to search.
     *
     * @return True if the named Node is a child of the specified root Node;
     *         false otherwise.
     */
    private boolean _hasChild(String childNodeName, Node root)
    {
	// Search the child Nodes of the root
	NodeList nodeList = root.getChildNodes();
	for (int i = 0; i < nodeList.getLength(); i++) {
	    Node childNode = nodeList.item(i);

	    // Match the name of the child Node
	    if ( childNode.getNodeName().equals(childNodeName) ) {
		return true;
	    }
	}

	// Named child Node not found
	return false;
    }

    /**
     * Creates a copy of the specified Node.
     *
     * @param node Node to create a copy of.
     *
     * @return Copy of the specified Node.
     */
    private Node _cloneNode(Node node)
    {
	// Create a Node with the same name
	IIOMetadataNode clonedNode = new IIOMetadataNode( node.getNodeName() );

	// Copy the attributes of the Node
	NamedNodeMap map = node.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		clonedNode.setAttribute(attr.getNodeName(),attr.getNodeValue());
	    }
	}

	// Copy the child Nodes of the Node
	NodeList nodeList = node.getChildNodes();
	for (int i = 0; i < nodeList.getLength(); i++) {
	    Node childNode = nodeList.item(i);
	    clonedNode.appendChild( _cloneNode(childNode) );
	}

	// Return the cloned Node
	return clonedNode;
    }
}
