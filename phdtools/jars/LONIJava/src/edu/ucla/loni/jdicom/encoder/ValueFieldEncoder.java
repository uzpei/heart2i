/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.encoder;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;

/**
 * Decodes the Values of a DICOM Data Element to a stream.
 * <p>
 * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
 *     [Section 3.10], Page 7: </b>
 * <em>
 * VALUE FIELD: The field within a Data Element that contains the Value(s) of
 * that Data Element.
 * </em>
 *
 * @version 10 May 2007
 */
public class ValueFieldEncoder
{
  /** Byte Swapped Output Stream to write encoded data to. */
  private ByteSwappedOutputStream _byteSwappedStream;

  /**
   * True if the Value Length is 16 bits in size; false otherwise.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 7: </b>
   * <em>
   * VALUE LENGTH: The field within a Data Element that contains the length of
   * the Value Field of the Data Element.
   * </em>
   */
  private boolean _hasShortValueLength;

  /** True if the encoding byte ordering requires byte swapping; false o/w. */
  private boolean _byteSwap;

  /** UID of the Transfer Syntax that determines the encoding. */
  private String _transferSyntaxUID;

  /**
   * Constructs a ValueFieldEncoder that writes encoded data to the specified
   * output stream.
   *
   * @param outputStream Output Stream to write encoded data to.
   * @param byteSwap True if the encoding byte ordering requires byte swapping;
   *                 false o/w.
   * @param transferSyntaxUID UID of the Transfer Syntax that determines the
   *                          encoding.
   * @param hasShortValueLength True if the Value Length is 16 bits in size;
   *                            false otherwise.
   */
  public ValueFieldEncoder(OutputStream outputStream, boolean byteSwap,
			   String transferSyntaxUID,
			   boolean hasShortValueLength)
    {
      _byteSwappedStream = new ByteSwappedOutputStream(outputStream);
      _byteSwap = byteSwap;
      _transferSyntaxUID = transferSyntaxUID;
      _hasShortValueLength = hasShortValueLength;
    }

  /**
   * Writes the specified Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 7: </b>
   * <em>
   * VALUE: A component of a Value Field. A Value Field may consist of one or
   * more of these components.
   * </em>
   *
   * @param valueRepresentation Value Representation indicating how the
   *                            Values in the Value Field are encoded.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void writeValues(String valueRepresentation, Enumeration values)
    throws IOException
    {
      String vr = valueRepresentation;

      // Create a byte buffer to encode into (initial size 8096 bytes)
      ByteArrayOutputStream baStream = new ByteArrayOutputStream(8096);
      ByteSwappedOutputStream bsStream = new ByteSwappedOutputStream(baStream);

      // Write a Value Field of Strings to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Pages 15 - 21
      int valueLength;
      byte[] value;
      if ( vr.equals("AE") || vr.equals("AS") || vr.equals("CS") ||
	   vr.equals("DA") || vr.equals("DS") || vr.equals("DT") ||
	   vr.equals("IS") || vr.equals("LO") || vr.equals("LT") ||
	   vr.equals("PN") || vr.equals("SH") || vr.equals("ST") ||
	   vr.equals("TM") || vr.equals("UI") || vr.equals("UT") )
	{
	  _writeStringValues(bsStream, vr, values);

	  valueLength = baStream.size();
	  value = baStream.toByteArray();
	}

      // Write a Value Field of attribute tags to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 15
      else if ( vr.equals("AT") ) {
	_writeTagValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
      }

      // Write a Value Field of doubles to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 16
      else if ( vr.equals("FD") ) {
	_writeDoubleValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
      }

      // Write a Value Field of floats to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 16
      else if ( vr.equals("FL") ) {
	_writeFloatValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of signed longs to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 19
      else if ( vr.equals("SL") ) {
	_writeSignedLongValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of signed shorts to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 19
      else if ( vr.equals("SS") ) {
	_writeSignedShortValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of unsigned longs to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 20
      else if ( vr.equals("UL") ) {
	_writeUnsignedLongValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of unsigned shorts to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 21
      else if ( vr.equals("US") ) {
	_writeUnsignedShortValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of Items, where each Item contains a Data Set
      //
      // PS 3.5-1998, NESTING OF DATA SETS
      // [Section 7.5], Page 29
      // The VR identified "SQ" shall be used for Data Elements with a Value
      // consisting of a Sequence of zero or more Items, where each Item
      // contains a set of Data Elements.
      else if ( vr.equals("SQ") ) {
	_writeDataSetValues(bsStream, values);

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of 16 bit words to the byte buffer
      //
      // PS 3.5-1998, DICOM VALUE REPRESENTATIONS
      // [Table 6.2-1], Page 17
      else if ( vr.equals("OW") ) {

	// Write the 16 bit word array
	//
	// PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
	// [Section 6.4], Page 23
	// Data Elements with a VR of SQ, OW, OB or UN shall always have a
	// Value Multiplicity of one.
	bsStream.write( (byte[])values.nextElement(), _byteSwap ? 2 : 1 );

	valueLength = baStream.size();
	value = baStream.toByteArray();
       }

      // Write a Value Field of primitive byte types
      else {

	// Get the byte array
	//
	// PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
	// [Section 6.4], Page 23
	// Data Elements with a VR of SQ, OW, OB or UN shall always have a
	// Value Multiplicity of one.
	byte[] byteArray = (byte[])values.nextElement();

	// Pad an OB byte array if needed
	if ( vr.equals("OB") && byteArray.length % 2 == 1 ) {

	  // Create a byte array with even length and copy the contents
	  //
	  // PS 3.5-1998, VALUE REPRESENTATION (VR)
	  // [Section 6.2], Page 14
	  // Values with a VR of OB shall be padded with a single trailing NULL
	  // byte value (00H) when necessary to achieve even length.
	  byte[] newByteArray = new byte[ byteArray.length + 1 ];
	  System.arraycopy(byteArray, 0, newByteArray, 0, byteArray.length);
	  byteArray = newByteArray;
	}

	// No need to byte swap
	//
	// PS 3.5-1998, DICOM VALUE REPRESENTATIONS
	// [Table 6.2-1], Page 17
	// OB is a VR which is insensitive to Little/Big Endian byte ordering
	// (see Section 7.3).
	//
	// PS 3.5-1998, UNKNOWN (UN) VALUE REPRESENTATION
	// [Section 6.2.2], Page 22
	// As long as the VR is unknown the Value Field is insensitive to
	// Little/Big Endian byte ordering and shall not be 'byte-swapped'
	// (see section 7.3).
	valueLength = byteArray.length;
	value = byteArray;
      }

      // Write the value length to the stream
      if (_hasShortValueLength) {
	_byteSwappedStream.writeShort(valueLength, _byteSwap);
      }
      else {
	_byteSwappedStream.writeInt(valueLength, _byteSwap);
      }
				      
      // Write the value to the stream
      _byteSwappedStream.write(value);
    }

  /**
   * Writes the String Values to the Output Stream.
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param vr Value Representation indicating how the Values in the Value
   *           Field are encoded.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeStringValues(ByteSwappedOutputStream bsStream, String vr,
				  Enumeration values) throws IOException
    {
      // Write each String to the stream
      while ( values.hasMoreElements() ) {
	String value = (String)values.nextElement();
	
	// Encode the String without byte swapping
	//
	// PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
	// [Section 7.3], Page 28:
	// In a character string consisting of multiple 8-bit single byte
	// codes, the characters will be encoded in the order of occurrence
	// in the string (left to right).
	bsStream.write( value.getBytes() );

	// Use the backslash as the delimiter
	//
	// PS 3.5-1998, VALUE MULTIPLICITY (VM) AND DELIMITATION
	// [Section 6.4], Page 23:
	// When a Data Element has multiple Values, those Values shall be
	// delimited as follows:  For character strings, the character
	// 5CH (BACKSLASH "\" in the case of the repertoire ISO IR-6) shall
	// be used as a delimiter between Values.
	if ( values.hasMoreElements() ) {
	  String delimiter = "\\";
	  bsStream.write( delimiter.getBytes() );
	}
      }

      // Add an extra character if the number of encoded bytes is odd
      //
      // PS 3.5-1998, VALUE REPRESENTATION (VR)
      // [Section 6.2], Page 14:
      // Values with VRs constructed of character strings, except in the case
      // of the VR UI, shall be padded with SPACE characters (20H, in the
      // Default Character Repertoire) when necessary to achieve even length.
      // Values with a VR of UI shall be padded with a single trailing NULL
      // (00H) character when necessary to achieve even length.
      if ( bsStream.size() % 2 == 1 ) {
	char[] extraCharacter = new char[1];

	if ( vr.equals("UI") ) { extraCharacter[0] = (char)0x00; }
	else { extraCharacter[0] = (char)0x20; }

	String padding = new String(extraCharacter);
	bsStream.write( padding.getBytes() );
      }
    }

  /**
   * Writes the attribute tag Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 15: </b>
   * <em>
   * AT Attribute Tag - Ordered pair of 16-bit unsigned integers that is the
   * value of a Data Element Tag.  4 bytes fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeTagValues(ByteSwappedOutputStream bsStream,
			       Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	String value = (String)values.nextElement();
	bsStream.writeDataElementTag(value, _byteSwap);
      }
    }

  /**
   * Writes double Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 16: </b>
   * <em>
   * FD Floating Point Double - Double precision binary floating point number
   * represented in IEEE 754:1985 64-bit Floating Point Number Format.  8
   * bytes fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeDoubleValues(ByteSwappedOutputStream bsStream,
				  Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	Double value = (Double)values.nextElement();
	bsStream.writeDouble(value.doubleValue(), _byteSwap);
      }
    }

  /**
   * Writes float Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 16: </b>
   * <em>
   * FL Floating Point Single - Single precision binary floating point number
   * represented in IEEE 754:1985 32-bit Floating Point Number Format.  4
   * bytes fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeFloatValues(ByteSwappedOutputStream bsStream,
				 Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	Float value = (Float)values.nextElement();
	bsStream.writeFloat(value.floatValue(), _byteSwap);
      }
    }

  /**
   * Writes signed long Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 19: </b>
   * <em>
   * SL Signed Long - Signed binary integer 32 bits long.  Represents an
   * integer, n, in the range:  - (2^31 - 1 ) <= n <= (2^31 - 1).  4 bytes
   * fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeSignedLongValues(ByteSwappedOutputStream bsStream,
				      Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	Integer value = (Integer)values.nextElement();
	bsStream.writeInt(value.intValue(), _byteSwap);
      }
    }

  /**
   * Writes signed short Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 19: </b>
   * <em>
   * SS Signed Short - Signed binary integer 16 bits long in 2's complement
   * form. Represents an integer n in the range:  - (2^15 - 1 ) <= n <=
   * (2^15 - 1).  2 bytes fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeSignedShortValues(ByteSwappedOutputStream bsStream,
				       Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	Short value = (Short)values.nextElement();
	bsStream.writeShort(value.shortValue(), _byteSwap);
      }
    }

  /**
   * Writes unsigned long Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 20: </b>
   * <em>
   * UL Unsigned Long - Unsigned binary integer 32 bits long.  Represents an
   * integer n in the range:  0 <= n < 2^32.  4 bytes fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeUnsignedLongValues(ByteSwappedOutputStream bsStream,
					Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	Long value = (Long)values.nextElement();
	bsStream.writeInt(value.intValue(), _byteSwap);
      }
    }

  /**
   * Writes unsigned short Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, DICOM VALUE REPRESENTATIONS
   *     [Table 6.2-1], Page 21: </b>
   * <em>
   * US Unsigned Short - Unsigned binary integer 16 bits long.  Represents
   * integer n in the range:  0 <= n < 2^16.  2 bytes fixed.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeUnsignedShortValues(ByteSwappedOutputStream bsStream,
					 Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	Integer value = (Integer)values.nextElement();
	bsStream.writeShort(value.shortValue(), _byteSwap);
      }
    }

  /**
   * Writes Data Set Values to the Output Stream.
   * <p>
   * <b> PS 3.5-1998, ITEM ENCODING RULES
   *     [Section 7.5.1], Page 30: </b>
   * <em>
   * Each Item of a Data Element of Value Representation SQ shall be encoded
   * as a DICOM Standard Data Element with a specific Data Element Tag of
   * Value (FFFE,E000). The Item Tag is followed by a 4 byte Item Length
   * field.
   * <p>
   * Each Item Value shall contain a DICOM Data Set composed of Data Elements.
   * </em>
   *
   * @param bsStream Byte Swapped Output Stream to write encoded data to.
   * @param values Values to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   */
  private void _writeDataSetValues(ByteSwappedOutputStream bsStream,
				   Enumeration values) throws IOException
    {
      // Write each value to the stream
      while ( values.hasMoreElements() ) {
	DataSet dataSet = (DataSet)values.nextElement();

	// Write the Data Element Tag of the Item
	//
	// PS 3.5-1998, ITEM ENCODING RULES
	// [Section 7.5.1], Page 30:
	// There are three special SQ related Data Elements that are not ruled
	// by the VR encoding rules conveyed by the Transfer Syntax. They shall
	// be encoded as Implicit VR. These special Data Elements are Item
	// (FFFE,E000), Item Delimitation Item (FFFE,E00D), and Sequence
	// Delimitation Item (FFFE,E0DD).
	bsStream.writeDataElementTag("FFFEE000", _byteSwap);

	// Create a byte buffer to encode into (initial size 8096 bytes)
	ByteArrayOutputStream baStream = new ByteArrayOutputStream(8096);

	// Write the Data Set to the byte buffer
	DataElementEncoder encoder =
	  new DataElementEncoder(baStream, _transferSyntaxUID);

	Enumeration enun = dataSet.getDataElements();
	while ( enun.hasMoreElements() ) {
	  encoder.writeDataElement( (DataElement)enun.nextElement() );
	}

	// Write the Item Length to the stream
	//
	// PS 3.5-1998, ITEM ENCODING RULES
	// [Section 7.5.1], Page 30:
	// Explicit Length: The number of bytes (even) contained in the
	// Sequence Item Value (following but not including the Item Length
	// Field) is encoded as a 32-bit unsigned integer value (see
	// Section 7.1). This length shall include the total length of all
	// Data Elements conveyed by this Item. This Item Length shall be
	// equal to 00000000H if the Item contains no Data Set.
	bsStream.writeInt(baStream.size(), _byteSwap);

	// Write the encoded Item to the stream
	bsStream.write( baStream.toByteArray() );
      }
    }
}
