/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.decoder.ByteSwappedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Decodes a Data Element from a Siemens SV10 encoded stream.
 *
 * @version 28 August 2007
 */
public class SV10DataElementDecoder
{
    /** Decoded Data Element. */
    private DataElement _dataElement;

    /**
     * Constructs an SV10 Data Element Decoder.
     *
     * @param stream Stream to read decoded data from.
     *
     * @throws IOException If an I/O error occurs.
     */
    public SV10DataElementDecoder(InputStream stream) throws IOException
    {
	ByteSwappedInputStream dStream = new ByteSwappedInputStream(stream);

	// Read the name of the element (64 bytes)
	String elemName = _readString(dStream, 64);

	// Read the multiplicity (4 bytes)
	int nValues = dStream.readInt(true);

	// Read the value representation (4 bytes)
	String vr = _readString(dStream, 4);

	// Skip the number assigned to each DICOM VR (4 bytes)
	if (dStream.skipBytes(4) != 4) {
	    throw new IOException("Unable to skip DICOM VR #");
	}

	// Create the Data Element
	_dataElement = new DataElement(elemName, vr, elemName);

	// Values exist (4 bytes)
	int nValues2 = dStream.readInt(true);
	if (nValues2 != 0) {

	    // Correct the number of values
	    if (nValues == 0) { nValues = nValues2; }

	    // Skip unknown (4 bytes)
	    if (dStream.skipBytes(4) != 4) {
		throw new IOException("Unable to skip past 4 pre-field");
	    }

	    // Read the values
	    for (int i = 0; i < nValues; i++) {

		// Skip unknown field (12 bytes)
		if (dStream.skipBytes(12) != 12) {
		    throw new IOException("Unable to skip past 12 pre-field");
		}

		// Read the number of bytes in the value (4 bytes)
		int nValue = dStream.readInt(true);

		// No value even though there should be one
		if (nValue == 0) { break; }

		// Read the value
		String value = _readString(dStream, nValue);
		_dataElement.addValue(value);

		// Skip to complete 4 byte increments
		if ((nValue % 4) != 0) {
		    int skip = 4 - (nValue % 4);
		    if (dStream.skipBytes(skip) != skip) {
			throw new IOException("Unable to skip to 4 byte pad");
		    }
		}
	    }
	}
    }

    /**
     * Gets the decoded DataElement.
     *
     * @return DataElement decoded from the encoded stream.
     */
    public DataElement getDataElement()
    {
	return _dataElement;
    }

    /**
     * Gets a string from the stream.
     *
     * @param dStream Stream to read the string from.
     * @param length Length of the encoded string.
     *
     * @return Decoded string from the stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    private String _readString(DataInputStream dStream, int length)
	throws IOException
    {
	// Read the string bytes
	byte[] b = new byte[length];
	dStream.readFully(b);

	// Search for the /0 (garbage characters follow it)
	int zeroIndex = b.length - 1;
	for (int i = 0; i < b.length; i++) {
	    if (b[i] == 0) {
		zeroIndex = i;
		break;
	    }
	}

	// Return the string
	return new String(b, 0, zeroIndex);
    }
}
