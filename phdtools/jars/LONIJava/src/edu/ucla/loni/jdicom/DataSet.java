/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom;

import java.io.Serializable;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Vector;

/**
 * DICOM set of Data Elements.
 * <p>
 * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
 *     [Section 3.10], Page 5: </b>
 * <em> 
 * DATA SET:  Exchanged information consisting of a structured set of 
 * Attribute values directly or indirectly related to Information Objects. 
 * The value of each Attribute in a Data Set is expressed as a Data Element.
 * A collection of Data Elements ordered by increasing Data Element Tag number
 * that is an encoding of the values of Attributes of a real world object.
 * </em>
 *
 * @version 10 May 2007
 */
public class DataSet implements Serializable
{
  /** 
   * DataElements that are part of the DataSet.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 5: </b>
   * <em> 
   * DATA ELEMENT:  A unit of information as defined by a single entry in the
   * data dictionary. An encoded Information Object Definition (IOD) Attribute
   * that is composed of, at a minimum, three fields: a Data Element Tag, a 
   * Value Length, and a Value Field. For some specific Transfer Syntaxes, a
   * Data Element also contains a VR Field where the Value Representation of
   * that Data Element is specified explicitly.
   * </em>
   */
  private Vector _dataElements = new Vector(50, 5);

  /** Flag indicating that the Data Elements need sorting. */
  private boolean _elementsNeedSorting;

  /** Constructs a DataSet. */
  public DataSet()
    {
      _elementsNeedSorting = false;
    }

  /**
   * Adds a DataElement to the DataSet.
   *
   * @param dataElement DataElement to add to the DataSet.
   */
  public void addDataElement(DataElement dataElement)
    {
      _elementsNeedSorting = true;
      _dataElements.addElement(dataElement);
    }

  /** 
   * Gets the DataElements in the DataSet.
   *
   * @return DataElements contained in the DataSet.
   */
  public Enumeration getDataElements()
    {
      // Sort the Data Elements
      _sortDataElements();

      return _dataElements.elements();
    }

  /**
   * Gets the DataElement in the DataSet with the given Tag.
   *
   * @param dataElementTag Tag that uniquely identifies the DataElement.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   *
   * @return DataElement in the DataSet with the given Tag, or null if such
   *         a DataElement does not exist.
   */
  public DataElement getDataElement(String dataElementTag)
    {
      // Sort the Data Elements
      _sortDataElements();

      // Search the DataElements for a DataElement with a matching tag
      int index = Collections.binarySearch(_dataElements, dataElementTag);

      // No matching DataElement found
      if (index < 0) { return null; }

      return (DataElement)_dataElements.get(index);
    }

  /**
   * Gets the number of DataElements in the DataSet.
   *
   * @return Number of DataElements in the DataSet.
   */
  public int getNumberOfDataElements()
    {
      return _dataElements.size();
    }

  /**
   * Removes the DataElement in the DataSet corresponding to the given Tag.
   *
   * @param dataElementTag Tag that uniquely identifies the DataElement.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   *
   * @return DataElement in the DataSet that was removed, or null if no such
   *         DataElement exists.
   */
  public DataElement removeDataElement(String dataElementTag)
    {
      // Search for a DataElement with a matching tag
      DataElement dataElement = getDataElement(dataElementTag);

      // Remove the DataElement
      if (dataElement != null) { _dataElements.remove(dataElement); }

      // Return the DataElement that was removed
      return dataElement;
    }

  /** 
   * Gets the contents of the DataSet.
   *
   * @return String representation of the contents of the DataSet.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Append the contents of each DataElement to the buffer
      Enumeration enun = getDataElements();
      while ( enun.hasMoreElements() ) {
	buffer.append( enun.nextElement() + "\n");
      }

      return buffer.toString();
    }

  /** Sorts the Data Elements. */
  private void _sortDataElements()
    {
      // Data Elements are already sorted
      if (!_elementsNeedSorting) { return; }

      // Sort the Data Elements
      Collections.sort(_dataElements);
      _elementsNeedSorting = false;
    }
}
