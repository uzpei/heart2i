/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

import java.util.ListResourceBundle;
import edu.ucla.loni.jdicom.DataDictionary;

/**
 * List Resource Bundle that provides descriptions for DICOM Data Element
 * names.
 *
 * @version 13 February 2003
 */
public class DicomMetadataFormatResources extends ListResourceBundle
{
  /** Constructs a DicomMetadataFormatResources. */
  public DicomMetadataFormatResources()
    {
    }

  /**
   * Gets the contents of the Resource Bundle.
   *
   * @return Object array of the contents, where each item of the array is a
   *         pair of Objects.  The first element of each pair is the key, which
   *         must be a String, and the second element is the value associated
   *         with that key.
   */
  public Object[][] getContents()
    {
      // Get the allowed Data Element Tags
      DataDictionary dd = DataDictionary.getInstance();
      String[] tags = dd.getDataElementTags();

      // Fill an Object array with the Tags and their names
      Object[][] contents = new Object[tags.length+6][2];
      for (int i = 0; i < tags.length; i++) {
	contents[i][0] = tags[i];
	contents[i][1] = dd.getName(tags[i]) + ".";
      }
      int index = tags.length;

      // Stream metadata Node
      String name = DicomStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      contents[index][0] = name;
      contents[index][1] = "DICOM stream metadata.";
      index++;

      // Image metadata Node
      name = DicomImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      contents[index][0] = name;
      contents[index][1] = "DICOM image metadata.";
      index++;

      // Data Set Node
      name = DicomMetadataFormat.DATA_SET_NAME;
      contents[index][0] = name;
      contents[index][1] = "Set of data elements.";
      index++;

      // Value Representation attribute
      name = "*/" + DicomMetadataFormat.VR_NAME;
      contents[index][0] = name;
      contents[index][1] = "Value representation.";
      index++;

      // Value Node
      String valueName = DicomMetadataFormat.VALUE_NODE_NAME;
      contents[index][0] = valueName;
      contents[index][1] = "Data element value.";
      index++;

      // Value attribute
      name = DicomMetadataFormat.VALUE_ATTRIBUTE_NAME;
      contents[index][0] = valueName + "/" + name;
      contents[index][1] = "Data element value.";

      // Return the Object array
      return contents;
    }
}
