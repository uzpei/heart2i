/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.jdicom.plugin;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Filter Input Stream that enables a byte buffer to be read before reading its
 * internal Input Stream.
 *
 * @version 29 October 2002
 */
public class PrependedInputStream extends FilterInputStream
{
  /** Array of bytes to prepend to the Input Stream. */
  private byte[] _byteArray;

  /** Number of bytes read from the stream. */
  private int _pos;

  /**
   * Constructs a PrependedInputStream.
   *
   * @param in Input Stream to be prepended.
   * @param byteArray Array of bytes to prepend to the Input Stream.
   */
  public PrependedInputStream(InputStream in, byte[] byteArray)
    {
      super(in);
      _byteArray = byteArray;
    }

  /**
   * Reads the next byte of data from this input stream.
   *
   * @return The next byte of data, or -1 if the end of the stream is reached.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int read() throws IOException
    {
      // Return a byte from the array if all those bytes haven't been read
      if (_pos < _byteArray.length) {
	int b = _byteArray[_pos];
	_pos++;
	return b;
      }

      // Otherwise return a byte from the Input Stream
      return in.read();
    }

  /**
   * Reads up to len bytes of data from this input stream into an array of
   * bytes.
   *
   * @param b Buffer into which the data is read.
   * @param off Start offset of the data.
   * @param len Maximum number of bytes read.
   *
   * @return Total number of bytes read into the buffer, or -1 if there is no
   *         more data because the end of the stream has been reached.
   *
   * @throws IOException If an I/O error occurs.
   */
  public int read(byte b[], int off, int len) throws IOException
    {
      // If all the bytes in the array have been read, use the Input Stream
      if (_pos >= _byteArray.length) { return in.read(b, off, len); }

      // Otherwise, get the bytes one by one
      for (int i = 0; i < len; i++) {
	b[i+off] = (byte)read();
	if (b[i+off] == -1) { return -1; }
      }

      // Return the number of bytes read
      return len;
    }

  /**
   * Skips over and discards n bytes of data from the input stream.
   *
   * @param n Number of bytes to be skipped.
   *
   * @return Actual number of bytes skipped.
   *
   * @throws IOException If an I/O error occurs.
   */
  public long skip(long n) throws IOException
    {
      // If all the bytes in the array have been read, use the Input Stream
      if (_pos >= _byteArray.length) { return in.skip(n); }

      // Otherwise, read the bytes one by one
      for (int i = 0; i < n; i++) { read(); }
      return n;
    }

  /**
   * Marks the current position in this input stream.
   *
   * @param readlimit Maximum limit of bytes that can be read before the mark
   *                  position becomes invalid.
   */
  public synchronized void mark(int readlimit)
    {
    }

  /**
   * Repositions this stream to the position at the time the mark method was
   * last called on this input stream. 
   *
   * @throws IOException If the stream has not been marked or if the mark has
   *                     been invalidated.
   */
  public synchronized void reset() throws IOException
    {
    }

  /**
   * Tests if this input stream supports the mark and reset< methods. 
   *
   * @return True if this stream type supports the mark and reset methods;
   *         false otherwise.
   */
  public boolean markSupported()
    {
      return false;
    }
}
