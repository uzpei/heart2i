/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Vector;

/**
 * DICOM unit of information.
 * <p>
 * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
 *     [Section 3.10], Page 5: </b>
 * <em> 
 * DATA ELEMENT:  A unit of information as defined by a single entry in the
 * data dictionary. An encoded Information Object Definition (IOD) Attribute
 * that is composed of, at a minimum, three fields: a Data Element Tag, a 
 * Value Length, and a Value Field. For some specific Transfer Syntaxes, a
 * Data Element also contains a VR Field where the Value Representation of
 * that Data Element is specified explicitly.
 * </em>
 *
 * @version 10 May 2007
 */
public class DataElement implements Comparable, Serializable
{
  /**
   * Tag that uniquely identifies the DataElement.  This tag is represented
   * as an 8 digit hexadecimal number.  The first 4 digits represent the
   * Group Number and the last 4 digits represent the Element Number.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 5: </b>
   * <em> 
   * DATA ELEMENT TAG:  A unique identifier for a Data Element composed of an
   * ordered pair of numbers (a Group Number followed by an Element Number).
   * </em>
   */
  private String _dataElementTag;

  /**
   * Two character code that specifies the data type and format of the Values
   * of the DataElement.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT FIELDS [Section 7.1.1], Page 25: </b>
   * <em>
   * Value Representation: A two-byte character string containing the VR 
   * of the Data Element. The VR for a given Data Element Tag shall be as 
   * defined by the Data Dictionary as specified in PS 3.6. The two character
   * VR shall be encoded using characters from the DICOM default character set.
   * </em>
   */
  private String _valueRepresentation;

  /**
   * Values contained in the DataElement.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 7: </b>
   * <em>
   * VALUE: A component of a Value Field. A Value Field may consist of one
   * or more of these components.
   * </em>
   */
  private Vector _values = new Vector(1, 1);

  /**
   * DataSets contained in the DataElement.
   * <p>
   * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
   *     [Section 3.10], Page 6: </b>
   * <em>
   * NESTED DATA SET:  A Data Set contained within a Data Element of another
   * Data Set. Data Sets can be nested recursively. Only Data Elements with
   * Value Representation Sequence of Items may, themselves, contain Data Sets.
   *
   * @associates <{org.medtoolbox.jdicom.DataSet}>
   * @link aggregationByValue
   * @clientCardinality 1
   * @supplierCardinality 0..*
   */
  private Vector _nestedDataSets = new Vector(1, 1);

  /**
   * Description of the DataElement.
   * <p>
   * <b> PS 3.6-1998, REGISTRY OF DICOM DATA ELEMENTS [Section 6], Page 5 </b>
   */
  private String _name;

  /**
   * Constructs a DataElement with the specified tag, value representation,
   * and description.
   *
   * @param dataElementTag Tag that uniquely identifies the DataElement.
   *                       This tag is represented as an 8 digit hexadecimal
   *                       number.  The first 4 digits represent the Group
   *                       Number and the last 4 digits represent the Element
   *                       Number.
   * @param valueRepresentation Two character code that specifies the data
   *                            type and format of the Values of the
   *                            DataElement.
   * @param name Description of the DataElement.
   */
  public DataElement(String dataElementTag, String valueRepresentation,
		     String name)
    {
      _dataElementTag = dataElementTag;
      _valueRepresentation = valueRepresentation;
      _name = name;
    }

  /**
   * Adds a value to the DataElement.
   *
   * @param value Value to be contained in the DataElement.
   */
  public void addValue(Object value)
    {
      _values.addElement(value);
    }

  /**
   * Gets the Values contained in the DataElement.
   *
   * @return Values contained in the DataElement.
   */
  public Enumeration getValues()
    {
      return _values.elements();
    }

  /**
   * Adds a DataSet to the DataElement.
   * 
   * @param dataSet DataSet to be contained in the DataElement.
   */
  public void addDataSet(DataSet dataSet)
    {
      _nestedDataSets.addElement(dataSet);
    }

  /**
   * Gets the DataSets in the DataElement.
   *
   * @return DataSets contained in the DataElement.
   */
  public Enumeration getDataSets()
    {
      return _nestedDataSets.elements();
    }

  /**
   * Gets the Tag for the DataElement.
   *
   * @return Tag that uniquely identifies the DataElement.  This
   *         tag is represented as an 8 digit hexadecimal number.  The first
   *         4 digits represent the Group Number and the last 4 digits
   *         represent the Element Number.
   */
  public String getDataElementTag()
    {
      return _dataElementTag;
    }

  /**
   * Gets the Value Representation for the DataElement.
   *
   * @return Two character code that specifies the data type and format of
   *         the Values of the DataElement.
   */
  public String getValueRepresentation()
    {
      return _valueRepresentation;
    }

  /**
   * Gets the name of the DataElement.
   *
   * @return Description of the DataElement.
   */
  public String getName()
    {
      return _name;
    }

  /**
   * Compares this object with the specified object for order.  Returns a
   * negative integer, zero, or a positive integer as this object is less
   * than, equal to, or greater than the specified object.
   *
   * @param object Object to compare to this one.  This Object must be a
   *               String (Data Element Tag) or another DataElement.
   *
   * @return A negative integer, zero, or a positive integer as this object
   *	     is less than, equal to, or greater than the specified object.
   *
   * @throws ClassCastException If the object is not a String or DataElement.
   */
  public int compareTo(Object object)
    {
      // Comparison object is another DataElement
      if (object instanceof DataElement) {

	// Compare Data Element Tags
	String objectTag = ((DataElement)object).getDataElementTag();
	return getDataElementTag().compareTo(objectTag);
      }

      // Comparison object is a Data Element Tag
      return getDataElementTag().compareTo( (String)object );
    }

  /** 
   * Gets the contents of the DataElement.
   *
   * @return String representation of the contents of the DataElement.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Write the tag
      buffer.append( getDataElementTag() );

      // Write the Name
      buffer.append("\t" + getName() + "\n");

      // Write the VR
      buffer.append("\t\t" + "VR:  " + getValueRepresentation() + "\n");

      // Write the Values
      Enumeration enun = getValues();

      // Write "null" if there are no values
      if ( !enun.hasMoreElements() ) {
	buffer.append("\t\t" + "VALUE:  <null>" + "\n");
      }

      // Write each Value
      else {
	while ( enun.hasMoreElements() ) {
	  Object value = enun.nextElement();

	  // Value is an array; write its length
	  if ( value.getClass().isArray() ) {
	    buffer.append("\t\t" + "UNDECODABLE:  " + Array.getLength(value) +
			  " bytes\n");
	  }

	  // Write the decoded Value
	  else {
	    buffer.append("\t\t" + "VALUE:  \"" + value + "\"\n");
	  }
	}
      }

      // Write the nested data sets
      enun = getDataSets();
      while ( enun.hasMoreElements() ) {
	buffer.append("\n" + "----- NESTED DATA SET:" + "\n\n");
	buffer.append( enun.nextElement() );
	buffer.append("-----" + "\n");
      }

      return buffer.toString();
    }
}
