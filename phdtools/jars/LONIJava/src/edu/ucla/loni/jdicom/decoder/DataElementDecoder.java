/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder;

import edu.ucla.loni.jdicom.DataDictionary;
import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * Decodes a DICOM unit of information from an encoded stream.
 *
 * @version 10 May 2007
 */
public class DataElementDecoder
{
    /**
     * Little Endian byte ordering.
     * <p>
     * <b> PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
     *     [Section 7.3], Page 28: </b>
     * <em>
     * <p>
     * Little Endian byte ordering is defined as follows:
     * <p> <ul>
     * <li> In a binary number consisting of multiple bytes (e.g. a 32-bit
     *      unsigned integer value, the Group Number, the Element Number,
     *      etc.), the least significant byte shall be encoded first; with the
     *      remaining bytes encoded in increasing order of significance.
     * <li> In a character string consisting of multiple 8-bit single byte
     *      codes, the characters will be encoded in the order of occurrence
     *      in the string (left to right).
     * </ul>
     * </em>
     */
    public static final int LITTLE_ENDIAN = 0;

    /**
     * Big Endian byte ordering.
     * <p>
     * <b> PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
     *     [Section 7.3], Page 28: </b>
     * <em>
     * <p>
     * Big Endian byte ordering is defined as follows:
     * <p> <ul>
     * <li> In a binary number consisting of multiple bytes, the most
     *      significant byte shall be encoded first; with the remaining bytes
     *      encoded in decreasing order of significance.
     * <li> In a character string consisting of multiple 8-bit single byte
     *      codes, the characters will be encoded in the order of occurrence
     *      in the string (left to right).
     * </ul> 
     * </em>
     */
    public static final int BIG_ENDIAN = 1;

    /**
     * Implicit VR structure.
     * <p>
     * <b> PS 3.5-1998, DATA ELEMENT STRUCTURE WITH IMPLICIT VR
     *     [Section 7.1.3], Page 27: </b>
     * <em> 
     * When using the Implicit VR structure the Data Element shall be
     * constructed of three consecutive fields:  Data Element Tag, Value
     * Length, and Value.
     * </em>
     */
    public static final int IMPLICIT_VR = 0;

    /**
     * Explicit VR structure.
     * <p>
     * <b> PS 3.5-1998, DATA ELEMENT STRUCTURE WITH EXPLICIT VR
     *     [Section 7.1.2], Page 26: </b>
     * <em> 
     * When using the Explicit VR structures, the Data Element shall be
     * constructed of four consecutive fields:  Data Element Tag, VR, Value
     * Length, and Value.
     * </em>
     */
    public static final int EXPLICIT_VR = 1;

    /**
     * Decoded DICOM unit of information.
     * <p>
     * <b> PS 3.5-1998, DICOM DATA STRUCTURES AND ENCODING DEFINITIONS
     *     [Section 3.10], Page 5: </b>
     * <em> 
     * DATA ELEMENT:  A unit of information as defined by a single entry in the
     * data dictionary. An encoded Information Object Definition (IOD) Attribute
     * that is composed of, at a minimum, three fields: a Data Element Tag, a 
     * Value Length, and a Value Field. For some specific Transfer Syntaxes, a
     * Data Element also contains a VR Field where the Value Representation of
     * that Data Element is specified explicitly.
     * </em>
     *
     * @clientCardinality 1
     * @supplierCardinality 0..1
     * @label decodes
     */
    private DataElement _dataElement;

    /** Byte ordering of the encoded DataElement. */
    private int _byteOrdering;

    /** Structure of the encoded DataElement. */
    private int _structure;

    /**
     * Constructs a DataElementDecoder which reads encoded data from the
     * specified MeasuredInputStream.  The given DataElements are those which
     * have already been decoded from the stream.
     * <p>
     * <b> PS 3.5-1998, DATA ELEMENT FIELDS 
     *     [Section 7.1.1], Page 25: </b>
     * <em>
     * A Data Element is made up of fields. Three fields are common to all
     * three Data Element structures; these are the Data Element Tag, Value
     * Length, and Value Field. A fourth field, Value Representation, is only
     * present in the two Explicit VR Data Element structures.
     * </em>
     * <p> <pre>
     * Encoded Data Element:
     * +---------------------------------------+
     * | TAG | VR | VALUE LENGTH | VALUE FIELD |  (VR optional)
     * +---------------------------------------+
     * </pre>
     * 
     * @param stream MeasuredInputStream to read encoded data from.
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     * @param maxMemory Number of bytes available for decoding.
     *
     * @throws IOException If an I/O error occurs.
     */
    public DataElementDecoder(MeasuredInputStream stream, Vector dataElements,
			      long maxMemory)
	throws IOException
    {
	// Do not stop decoding at the Value Field
	this(stream, dataElements, null, maxMemory);
    }

    /**
     * Constructs a DataElementDecoder which reads encoded data from the
     * specified MeasuredInputStream.  If the Data Element Tag of the Data
     * Element being decoded is equal to the specified Data Element Tag, the
     * decoding will stop at the beginnning of the encoded Value Field.  The
     * given DataElements are those which have already been decoded from the
     * stream.
     * <p>
     * <b> PS 3.5-1998, DATA ELEMENT FIELDS 
     *     [Section 7.1.1], Page 25: </b>
     * <em>
     * A Data Element is made up of fields. Three fields are common to all
     * three Data Element structures; these are the Data Element Tag, Value
     * Length, and Value Field. A fourth field, Value Representation, is only
     * present in the two Explicit VR Data Element structures.
     * </em>
     * <p> <pre>
     * Encoded Data Element:
     * +---------------------------------------+
     * | TAG | VR | VALUE LENGTH | VALUE FIELD |  (VR optional)
     * +---------------------------------------+
     * </pre>
     * 
     * @param stream MeasuredInputStream to read encoded data from.
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     * @param dataElementStopAtTag 8 digit hexadecimal number which stops the
     *                             decoding at the encoded Value Field when it
     *                             is equal to the decoded Data Element Tag.  If
     *                             this number is null, decoding will not be
     *                             stopped.
     * @param maxMemory Number of bytes available for decoding.
     *
     * @throws IOException If an I/O error occurs.
     */
    public DataElementDecoder(MeasuredInputStream stream, Vector dataElements,
			      String dataElementStopAtTag, long maxMemory)
	throws IOException
    {
	// Deduce the byte ordering and structure from the previous DataElements
	_analyzeTransferSyntax(dataElements);

	// Decode the Data Element Tag (may correct byte ordering and structure)
	String tag = _decodeDataElementTag(stream);

	// Determine the Value Representation
	String vr = new String();

	// Explicit VR:  Decode the Value Representation without byte swapping
	//
	// PS 3.5-1998, DATA ELEMENT FIELDS
	// [Section 7.1.1], Page 25:
	// Value Representation: A two-byte character string containing the
	// VR of the Data Element.
	//
	// PS 3.5-1998, BIG ENDIAN VERSUS LITTLE ENDIAN BYTE ORDERING
	// [Section 7.3], Page 28:
	// In a character string consisting of multiple 8-bit single byte
	// codes, the characters will be encoded in the order of occurrence
	// in the string (left to right).
	if (_structure == EXPLICIT_VR) {
	    byte[] vrBytes = new byte[2];
	    DataInputStream dStream = new DataInputStream(stream);
	    dStream.readFully(vrBytes);
	    vr = new String(vrBytes);
	}

	// Implicit VR:  Get the Value Representation from the Data Dictionary
	//
	// PS 3.5-1998, DATA ELEMENT FIELDS
	// [Section 7.1.1], Page 25:
	// The VR for a given Data Element Tag shall be as defined by the
	// Data Dictionary as specified in PS 3.6.
	else if (_structure == IMPLICIT_VR) {
	    DataDictionary dd = DataDictionary.getInstance();
	    vr = dd.getValueRepresentation(tag);

	    // Determine the exact VR if it is ambiguous in the Data Dictionary
	    vr = _getExactValueRepresentation(vr, tag, dataElements);
	}

	// Determine the Data Element Name
	DataDictionary dd = DataDictionary.getInstance();
	String name = dd.getName(tag);
	if (name == null) { name = "Non-Standard Attribute"; }

	// Decode the Value Length
	int valueLength = _decodeDataElementValueLength(stream, vr);

	// Check that there is enough memory to decode the value
	if (valueLength > maxMemory) {
	    String msg = "Encoded value for tag " + tag + " is larger than " +
		"the number of available bytes (" + valueLength + " > " +
		maxMemory;
	    throw new IOException(msg);
	}

	// Implicit VR:  Set vr="SQ" if the value length is -1
	//
	// PS 3.5-1998, ITEM ENCODING RULES
	// [Section 7.5.1], Page 30:
	// Each Item of a Data Element of Value Representation SQ shall be
	// encoded as a DICOM Standard Data Element with a specific Data Element
	// Tag of Value (FFFE,E000). The Item Tag is followed by a 4 byte
	// Item Length field encoded in one of the following two ways...
	// b) Undefined Length: The Item Length Field shall contain the
	// value FFFFFFFFH to indicate an undefined Item length.
	if (_structure == IMPLICIT_VR && valueLength == 0xFFFFFFFF) {
	    vr = "SQ";
	}

	// Create an empty DataElement
	_dataElement = new DataElement(tag, vr, name);

	// Check to see if the Values of the Data Element should be decoded
	if ( tag.equals(dataElementStopAtTag) ) { return; }

	// Decode the Value Field
	ValueFieldDecoder decoder =
	    new ValueFieldDecoder(stream, valueLength, vr,
				  _byteOrdering, dataElements);

	// Save the decoded Values in the decoded DataElement
	Enumeration enun = decoder.getValues();
	while ( enun.hasMoreElements() ) {

	    // If the VR is SQ, then the Values are necessarily DataSets
	    if ( vr.equals("SQ") ) {
		_dataElement.addDataSet( (DataSet)enun.nextElement() );
	    }
	
	    // Otherwise save the Values
	    else {
		_dataElement.addValue( enun.nextElement() );
	    }
	}
    }

    /**
     * Gets the decoded DataElement.
     *
     * @return DataElement decoded from the encoded stream.
     */
    public DataElement getDataElement()
    {
	return _dataElement;
    }

    /**
     * Analyzes the Transfer Syntax that is possibly found in the specified
     * DataElements which have already been decoded.  From the Transfer Syntax,
     * information about the byte ordering and structure of the encoded
     * DataElement can be obtained.
     * <p>
     * <b> PS 3.5-1998, TRANSFER SYNTAX 
     *     [Section 10], Page 40: </b>
     * <em> 
     * A Transfer Syntax is a set of encoding rules able to unambiguously
     * represent one or more Abstract Syntaxes. In particular, it allows
     * communicating Application Entities to negotiate common encoding
     * techniques they both support (e.g., byte ordering, compression, etc.).
     * </em>
     *
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     *
     * @throws NoSuchElementException If a valueless Transfer Syntax is
     *                                specified.
     * @throws IllegalArgumentException If an unknown Transfer Syntax is found.
     */
    private void _analyzeTransferSyntax(Vector dataElements)
    {
	// Search the previously decoded DataElements for the Transfer Syntax
	int index = Collections.binarySearch(dataElements, "00020010");

	// If the Transfer Syntax is not found, assume that the Transfer
	// Syntax is the one defined for all Group 2 Data Elements
	//
	// PS 3.10-1996, DICOM FILE META INFORMATION
	// [Section 7.1], Page 14:
	// Except for the 128 byte preamble and the 4 byte prefix, the File
	// Meta Information shall be encoded using the Explicit VR Little
	// Endian Transfer Syntax (UID=1.2.840.10008.1.2.1) as defined in
	// DICOM PS 3.5.
	if (index < 0) {
	    _byteOrdering = LITTLE_ENDIAN;
	    _structure = EXPLICIT_VR;
	    return;
	}

	// Get the value of the Transfer Syntax
	DataElement transferSyntax = (DataElement)dataElements.get(index);
	Enumeration enun = transferSyntax.getValues();

	// Check that there is a value
	if ( !enun.hasMoreElements() ) {
	    throw new NoSuchElementException("DataElementDecoder: No value " +
					     "for the Transfer Syntax is " +
					     "found.");
	}
	String transferSyntaxTag = (String)enun.nextElement();

	// Transfer Syntax value implies Little Endian byte ordering and
	// Explicit VR structure
	//
	// PS 3.5-1998, DICOM LITTLE ENDIAN TRANSFER SYNTAX (EXPLICIT VR)
	// [Section A.2], Page 43:
	// This DICOM Explicit VR Little Endian Transfer Syntax shall be
	// identified by a UID of Value "1.2.840.10008.1.2.1."
	//
	// PS 3.5-1998, TRANSFER SYNTAXES FOR ENCAPSULATION OF ENCODED PIXEL
	//              DATA
	// [Section A.4], Page 44:
	// a) The Data Elements contained in the Data Set structure shall be
	// encoded with Explicit VR (with a VR Field) as specified in
	// Section 7.1.2.
	// b) The encoding of the overall Data Set structure (Data Element Tags,
	// Value Length, etc.) shall be in Little Endian as specified in
	// Section 7.3.
	//
	// PS 3.5-1998, JPEG IMAGE COMPRESSION
	// [Section A.4.1], Page 47:
	// A DICOM Transfer Syntax for JPEG Image Compression shall be
	// identified by a UID value, appropriate to its JPEG coding process,
	// chosen from Table A.4-3.
	//
	// PS 3.5-1998, RLE COMPRESSION
	// [Section A.4.2], Page 48:
	// This transfer Syntax is identified by the UID value
	// 1.2.840.10008.1.2.5.
	if ( transferSyntaxTag.equals("1.2.840.10008.1.2.1") ||
	     transferSyntaxTag.startsWith("1.2.840.10008.1.2.4") ||
	     transferSyntaxTag.startsWith("1.2.840.10008.1.2.5") )
	    {
		_byteOrdering = LITTLE_ENDIAN;
		_structure = EXPLICIT_VR;
		return;
	    }

	// Transfer Syntax value implies Big Endian byte ordering and
	// Explicit VR structure
	//
	// PS 3.5-1998, DICOM BIG ENDIAN TRANSFER SYNTAX (EXPLICIT VR)
	// [Section A.3], Page 44:
	// This DICOM Explicit VR Big Endian Transfer Syntax shall be identified
	// by a UID of Value "1.2.840.10008.1.2.2."
	if ( transferSyntaxTag.equals("1.2.840.10008.1.2.2") ) {
	    _byteOrdering = BIG_ENDIAN;
	    _structure = EXPLICIT_VR;
	    return;
	}

	// Transfer Syntax value implies Little Endian byte ordering and
	// Implicit VR structure
	//
	// PS 3.5-1998, DICOM IMPLICIT VR LITTLE ENDIAN TRANSFER SYNTAX
	// [Section A.1], Page 42:
	// This DICOM Implicit VR Little Endian Transfer Syntax shall be
	// identified by a UID of Value "1.2.840.10008.1.2."
	if ( transferSyntaxTag.equals("1.2.840.10008.1.2") ) {
	    _byteOrdering = LITTLE_ENDIAN;
	    _structure = IMPLICIT_VR;
	    return;
	}

	// This option is included to handle the remaining case, which is a
	// violation of the DICOM standard, yet is still encountered
	if ( transferSyntaxTag.equals("BIG_ENDIAN_IMPLICIT_VR") ) {
	    _byteOrdering = BIG_ENDIAN;
	    _structure = IMPLICIT_VR;
	    return;
	}

	// Transfer Syntax value is unrecognized
	throw new IllegalArgumentException("DataElementDecoder: unknown " +
					   "Transfer Syntax " +
					   transferSyntaxTag + " found.");
    }

    /**
     * Decodes the Data Element Tag from the specified stream.
     * <p>
     * <b> PS 3.5-1998, DATA ELEMENT FIELDS
     *     [Section 7.1.1], Page 25: </b>
     * <em>
     * Data Element Tag: An ordered pair of 16-bit unsigned integers
     * representing the Group Number followed by Element Number.
     * </em>
     *
     * @param stream MeasuredInputStream to read encoded data from.
     *
     * @return Tag that uniquely identifies the DataElement.  This
     *         tag is represented as an 8 digit hexadecimal number.  The first
     *         4 digits represent the Group Number and the last 4 digits
     *         represent the Element Number.
     *
     * @throws IOException If an I/O error occurs.
     */
    private String _decodeDataElementTag(MeasuredInputStream stream)
	throws IOException
    {
	// Read the 4 byte tag
	DataInputStream dStream = new DataInputStream(stream);
	byte[] tagBytes = new byte[4];
	dStream.readFully(tagBytes);

	// Group 2 Data Elements have only one Transfer Syntax
	//
	// PS 3.10-1996, DICOM FILE META INFORMATION
	// [Section 7.1], Page 14:
	// Except for the 128 byte preamble and the 4 byte prefix, the File
	// Meta Information shall be encoded using the Explicit VR Little
	// Endian Transfer Syntax (UID=1.2.840.10008.1.2.1) as defined in
	// DICOM PS 3.5.
	if (tagBytes[0] == 2 && tagBytes[1] == 0) {
	    _byteOrdering = LITTLE_ENDIAN;
	    _structure = EXPLICIT_VR;
	}

	// Reread the tag from the byte array, swapping if necessary
	ByteArrayInputStream baStream = new ByteArrayInputStream(tagBytes);
	ByteSwappedInputStream bsStream = new ByteSwappedInputStream(baStream);
	String tag = bsStream.readDataElementTag(_byteOrdering==LITTLE_ENDIAN);

	// Item tags and delimiters are always encoded as Implicit VR
	//
	// PS 3.5-1998, NESTING OF DATA SETS
	// [Section 7.5], Page 30:
	// There are three special SQ related Data Elements that are not ruled
	// by the VR encoding rules conveyed by the Transfer Syntax. They shall
	// be encoded as Implicit VR. These special Data Elements are Item
	// (FFFE,E000), Item Delimitation Item (FFFE,E00D), and Sequence
	// Delimitation Item (FFFE,E0DD).
	if ( tag.equals("FFFEE000") || tag.equals("FFFEE00D") ||
	     tag.equals("FFFEE0DD") )
	    {
		_structure = IMPLICIT_VR;
	    }

	return tag;
    }

    /**
     * Determines the exact Value Representation for those Value Representations
     * that are ambiguously defined by the Data Dictionary.  This method
     * assumes that the structure of the encoded Data Element is Implicit VR
     * (since the Data Dictionary is only used to determine the VR when this
     * is true).
     * <p>
     * <b> PS 3.6-1998, DATA DICTIONARY </b>
     *
     * @param valueRepresentation Value Representation that is ambiguously
     *                            defined by the Data Dictionary.
     * @param dataElementTag Data Element Tag corresponding to the Value
     *                       Representation.  This tag is represented as an
     *                       8 digit hexadecimal number.
     * @param dataElements DataElements which have already been decoded from
     *                     the stream.
     *
     * @return Exact Value Representation.
     *
     * @throws IllegalArgumentException If the encoded structure is not
     *                                  Implicit VR.
     */
    private String _getExactValueRepresentation(String valueRepresentation,
						String dataElementTag,
						Vector dataElements)
    {
	// Encoded structure must be Implicit VR
	if (_structure != IMPLICIT_VR) {
	    throw new IllegalArgumentException("DataElementDecoder: encoded " +
					       "structure must be Implicit VR" +
					       " in" +
					       " _getExactValueRepresentation");
	}

	// Check if the VR is not ambiguous
	if (valueRepresentation != null && valueRepresentation.length() == 2) {
	    return valueRepresentation;
	}

	// Data Elements containing pixel values have their VR dependent upon
	// the value of the Pixel Representation
	//
	// PS 3.5-1998, DETAILED EXAMPLE OF PIXEL DATA ENCODING
	// [Section D.1], Page 53:
	// An individual pixel may consist of one or more Pixel Sample Values
	// (e.g. color or multi-planar images).  Each Pixel Sample Value can be
	// expressed as either a binary 2's complement integer or a binary
	// unsigned integer, as specified by the Pixel Representation Data
	// Element (0028, 0103).
	if ( dataElementTag.equals("00280106") ||
	     dataElementTag.equals("00280107") ||
	     dataElementTag.equals("00280108") ||
	     dataElementTag.equals("00280109") ||
	     dataElementTag.equals("00280110") ||
	     dataElementTag.equals("00280111") ||
	     dataElementTag.equals("00280120") ||
	     dataElementTag.equals("00281101") ||
	     dataElementTag.equals("00281102") ||
	     dataElementTag.equals("00281103") ||
	     dataElementTag.equals("00283002") ||
	     dataElementTag.equals("00283006") )
	    {
		// Search the decoded DataElements for the Pixel Representation
		int index = Collections.binarySearch(dataElements, "00280103");

		// Unable to find the Pixel Representation
		if (index < 0) { return "UN"; }

		// Get the Pixel Representation
		DataElement pixelRep = (DataElement)dataElements.get(index);
		Enumeration enun = pixelRep.getValues();

		// Unable to find the Pixel Representation
		if ( !enun.hasMoreElements() ) { return "UN"; }

		// Set the VR based upon the value of the Pixel Representation
		//
		// PS 3.3-1998, IMAGE PIXEL MODULE ATTRIBUTES
		// [Table C.7-9], Page 121:
		// Pixel Representation - Data representation of the pixel
		//                        samples.
		// Each sample shall have the same pixel representation.
		// Enumerated Values:  0000H = unsigned integer,
		//                     0001H = 2's complement
		int pixelRepValue = ((Integer)enun.nextElement()).intValue();

		if (pixelRepValue == 0) {
		    return "US";
		}
		else if (pixelRepValue == 1) {
		    return "SS";
		}
		else {
		    return "UN";
		}
	    }

	// Audio Sample Data Elements have their VR dependent upon the value
	// of the Audio Sample Format
	if ( dataElementTag.startsWith("50") &&
	     dataElementTag.endsWith("200C") )
	    {
		// Construct the tag of the corresponding Audio Sample Format
		String xx = dataElementTag.substring(2, 4);
		String formatTag = "50" + xx + "200C";

		// Search the decoded DataElements for the Audio Sample Format
		int index = Collections.binarySearch(dataElements, formatTag);

		// Unable to find the Audio Sample Format
		if (index < 0) { return "UN"; }

		// Get the Audio Sample Format
		DataElement format = (DataElement)dataElements.get(index);
		Enumeration enun = format.getValues();

		// Unable to find the Audio Sample Format
		if ( !enun.hasMoreElements() ) { return "UN"; }

		// Set the VR based upon the value of the Audio Sample Format
		//
		// PS 3.3-1998, AUDIO MODULE ATTRIBUTES
		// [Table C.10-3], Page 336:
		// Format of the Audio Sample Data.  Enumerated Values:
		// 0000H = 16 bit 2's complement Stored LSB first
		// 0001H = 8 bit 2's complement
		int formatValue = ((Integer)enun.nextElement()).intValue();

		if (formatValue == 0) {
		    return "OW";
		}
		else if (formatValue == 1) {
		    return "OB";
		}
		else {
		    return "UN";
		}
	    }

	// Palette Lookup Table Data Elements always have a VR of OW
	//
	// PS 3.5-1998, DICOM IMPLICIT VR LITTLE ENDIAN TRANSFER SYNTAX
	// [Section A.1], Page 42:
	// Data Elements (0028,1201), (0028,1202),(0028,1203) Red, Green, Blue
	// Palette Lookup Table Data have the Value Representation OW
	if ( dataElementTag.equals("00281201") ||
	     dataElementTag.equals("00281202") ||
	     dataElementTag.equals("00281203") )
	    {
		return "OW";
	    }

	// Curve Data Elements have a VR of OB
	//
	// PS 3.5-1998, DICOM IMPLICIT VR LITTLE ENDIAN TRANSFER SYNTAX
	// [Section A.1], Page 42:
	// Data Element (50xx,3000) Curve Data has the Value Representation OB
	if ( dataElementTag.startsWith("50") &&
	     dataElementTag.endsWith("3000") )
	    {
		return "OB";
	    }

	// Pixel Data Element has a VR of OW
	//
	// PS 3.5-1998, DICOM IMPLICIT VR LITTLE ENDIAN TRANSFER SYNTAX
	// [Section A.1], Page 42:
	// Data Element (7FE0,0010) Pixel Data has the Value Representation OW
	if ( dataElementTag.equals("7FE00010") ) { return "OW"; }

	// The VR is unknown
	//
	// PS 3.5-1998, UNKNOWN (UN) VALUE REPRESENTATION
	// [Section 6.2.2], Page 22:
	// The Unknown (UN) VR shall only be used for Private Attribute Data
	// Elements and Standard Data Elements previously encoded as some DICOM
	// VR other than UN using the DICOM Default Transfer Syntax (Implicit VR
	// Little Endian), and whose Value Representation is currently unknown.
	return "UN";
    }

    /**
     * Decodes the Data Element Value Length from the specified stream.
     * <p>
     * <b> PS 3.5-1998, DATA ELEMENT FIELDS
     *     [Section 7.1.1], Pages 25 -26: </b>
     * <em>
     * Value Length; either: <ul>
     * <li> A 16 or 32-bit (dependent on VR and whether VR is explicit or
     *      implicit) unsigned integer containing the Explicit Length of the
     *      Value Field as the number of bytes (even) that make up the Value.
     *      It does not include the length of the Data Element Tag, Value
     *      Representation, and Value Length Fields.
     * <li> A 32-bit Length Field set to Undefined Length (FFFFFFFFH). Undefined
     *      Lengths may be used for Data Elements having the Value
     *      Representation (VR) Sequence of Items (SQ). For Data Elements with
     *      Value Representation OW or OB Undefined Length may be used depending
     *      on the negotiated Transfer Syntax (see Section 10 and Annex A).
     * </ul> 
     * </em>
     *
     * @param stream MeasuredInputStream to read encoded data from.
     * @param valueRepresentation Value Representation of the Data Element.
     *
     * @return Length of the Value Field.
     *
     * @throws IOException If an I/O error occurs.
     */
    private int _decodeDataElementValueLength(MeasuredInputStream stream,
					      String valueRepresentation)
	throws IOException
    {
	ByteSwappedInputStream bStream = new ByteSwappedInputStream(stream);

	// Decode the Value Field Length for an Explicit VR structure with VR
	// equal to OB, OW, SQ, UN, or UT
	//
	// PS 3.5-1998, DATA ELEMENT STRUCTURE WITH EXPLICIT VR
	// [Section 7.1.2], Page 26:
	// For VRs of OB, OW, SQ and UN the 16 bits following the two character
	// VR Field are reserved for use by later versions of the DICOM
	// Standard. These reserved bytes shall be set to 0000H and shall not be
	// used or decoded (Table 7.1-1). The Value Length Field is a 32-bit
	// unsigned integer.
	// For VRs of UT the 16 bits following the two character VR Field are
	// reserved for use by later versions of the DICOM Standard. These
	// reserved bytes shall be set to 0000H and shall not be used or
	// decoded. The Value Length Field is a 32-bit unsigned integer.
	if (_structure == EXPLICIT_VR &&
	    ( valueRepresentation.equals("OB") ||
	      valueRepresentation.equals("OW") ||
	      valueRepresentation.equals("SQ") ||
	      valueRepresentation.equals("UN") ||
	      valueRepresentation.equals("UT") ))
	    {
		// Skip the reserved 16 bits after the VR
		byte[] reservedBits = new byte[2];
		bStream.readFully(reservedBits);

		// Decode the 32 bit Value Length
		return bStream.readInt(_byteOrdering==LITTLE_ENDIAN);
	    }

	// Decode the Value Field Length for Explicit VR structure with other VR
	//
	// PS 3.5-1998, DATA ELEMENT STRUCTURE WITH EXPLICIT VR
	// [Section 7.1.2], Page 26:
	// For all other VRs the Value Length Field is the 16-bit unsigned
	// integer following the two character VR Field (Table 7.1-2).
	else if (_structure == EXPLICIT_VR) {
	    return bStream.readUnsignedShort(_byteOrdering==LITTLE_ENDIAN);
	}

	// Decode the Value Field Length for Implicit VR structure
	//
	// PS 3.5-1998, DATA ELEMENT STRUCTURE WITH IMPLICIT VR
	// [Section 7.1.3], Page 27:
	// Value Length:  32-bit unsigned integer.
	else if (_structure == IMPLICIT_VR) {
	    return bStream.readInt(_byteOrdering==LITTLE_ENDIAN);
	}

	// Unknown decoding
	return 0;
    }
}

