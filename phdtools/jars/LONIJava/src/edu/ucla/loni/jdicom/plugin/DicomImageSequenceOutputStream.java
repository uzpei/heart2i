/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.plugin;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import edu.ucla.loni.jdicom.encoder.DataElementEncoder;
import edu.ucla.loni.jdicom.encoder.DataSetEncoder;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferUShort;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import javax.imageio.stream.ImageOutputStream;

/**
 * Stream for writing sequences of DICOM images to an output source.  An
 * encoded DICOM image sequence consists of:
 * <p>
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + Preamble and "DICM"
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + Data Elements Before (7FE0, 0000) Element      +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + (7FE0, 0010) Element Tag, VR, and Value Length +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + Pixel Data for Image 1                         +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + Pixel Data for Image 2                         +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + ...                                            +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + Pixel Data for Image N                         +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 * + Data Elements After (7FE0, 0010) Element       +
 * ++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * @version 10 May 2007
 */
public class DicomImageSequenceOutputStream
{
  /** Output stream to which the DICOM image sequence is written. */
  private ImageOutputStream _outputStream;

  /** Transfer syntax that determines byte ordering while writing. */
  private String _transferSyntax;

  /** True if byte swapping should occur while writing; false otherwise. */
  private boolean _byteSwap;

  /** Value Representation for the pixel data that is written. */
  private String _vr;

  /** Preamble, "DICM", and Data Elements Before (7FE0, 0000) Element. */
  private DataBlock _sequenceHeader;

  /** (7FE0, 0010) Element Tag, VR, and Value Length. */
  private DataBlock _pixelDataHeader;

  /** Pixel data for the images. */
  private ArrayList _pixelData;

  /** Data Elements After (7FE0, 0010) Element. */
  private DataBlock _sequenceFooter;

  /**
   * Constructs a DicomImageSequenceOutputStream.
   *
   * @param outputStream Output stream to which the DICOM image sequence is
   *                     written.
   * @param transferSyntax Transfer syntax that determines byte ordering while
   *                       writing.
   */
  public DicomImageSequenceOutputStream(ImageOutputStream outputStream,
					String transferSyntax)
    {
      _outputStream = outputStream;
      _transferSyntax = transferSyntax;

      // Byte swapping must occur for little endian byte ordering
      _byteSwap = !_transferSyntax.equals("1.2.840.10008.1.2.2");

      _pixelData = new ArrayList();
    }

  /**
   * Writes the sequence header to the current position of the output stream.
   *
   * @param dataSet Data Set containing the Data Elements to write.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the Transfer Syntax value cannot be
   *                                  determined.
   */
  public void writeSequenceHeader(DataSet dataSet) throws IOException
    {
      // Record the current stream offset
      long offset = _outputStream.getStreamPosition();

      // Write the sequence header
      OutputStream oStream = new OutputStreamAdapter(_outputStream);
      _writeSequenceHeader(oStream, dataSet);

      // Record the size of the sequence header block
      long size = _outputStream.getStreamPosition() - offset;
      _sequenceHeader = new DataBlock(offset, size);
    }

  /**
   * Updates the pixel data header with new information and returns to the
   * adjusted current position of the output stream.
   *
   * @param dataSet Data Set containing the Data Elements to write.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the Transfer Syntax value cannot be
   *                                  determined.
   */
  public void updateSequenceHeader(DataSet dataSet) throws IOException
    {
      // Get the current stream position
      long offset = _outputStream.getStreamPosition();

      // Determine the size of the new sequence header
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      _writeSequenceHeader(outputStream, dataSet);
      int newSize = outputStream.size();

      // Compute how many bytes the new sequence header differs from the old
      int delta = (int)(newSize - _sequenceHeader.getSize());

      // Relocate the sequence footer to make room for the new sequence header
      if (_sequenceFooter != null) {
	_sequenceFooter = _relocateDataBlock(_sequenceFooter, delta);
      }

      // Relocate the pixel data to make room for the new sequence header
      for (int i = _pixelData.size() - 1; i >= 0; i--) {
	DataBlock dataBlock = (DataBlock)_pixelData.get(i);
	dataBlock = _relocateDataBlock(dataBlock, delta);
	_pixelData.set(i, dataBlock);
      }

      // Relocate the pixel data header to make room for the new header
      if (_pixelDataHeader != null) {
	_pixelDataHeader = _relocateDataBlock(_pixelDataHeader, delta);
      }

      // Write the new sequence header
      _outputStream.seek( _sequenceHeader.getOffset() );
      writeSequenceHeader(dataSet);

      // Return to the adjusted original stream position
      _outputStream.seek(offset + delta);
    }

  /**
   * Sets the Value Representation for the pixel data that is written.
   *
   * @param vr Value Representation for the pixel data that is written.
   */
  public void setValueRepresentation(String vr)
    {
      _vr = vr;
    }

  /**
   * Writes the pixel data header to the current position of the output stream.
   *
   * @param valueLength Value Length for the pixel data Element.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalStateException If the Value Representation has not been
   *                               set in the stream.
   */
  public void writePixelDataHeader(int valueLength) throws IOException
    {
      // Check to see if the Value Representation has been set
      if (_vr == null) {
	String msg = "Value Representation has not been set.";
	throw new IllegalStateException(msg);
      }

      // Record the current stream offset
      long offset = _outputStream.getStreamPosition();

      // Create the pixel Data Element
      DataElement dataElement = new DataElement("7FE00010", _vr, "Pixel Data");

      // Write the pixel Data Element up to its Value Length
      OutputStream oStream = new OutputStreamAdapter(_outputStream);
      DataElementEncoder encoder = new DataElementEncoder(oStream,
							  _transferSyntax);
      encoder.writeDataElement(dataElement, false);

      // Write the Value Length
      if (_byteSwap) { _outputStream.setByteOrder(ByteOrder.LITTLE_ENDIAN); }
      _outputStream.writeInt(valueLength);
      _outputStream.setByteOrder(ByteOrder.BIG_ENDIAN);

      // Record the size of the pixel data header block
      long size = _outputStream.getStreamPosition() - offset;
      _pixelDataHeader = new DataBlock(offset, size);
    }

  /**
   * Updates the pixel data header with new information and returns to the
   * current position of the output stream.
   *
   * @param valueLength New Value Length for the pixel data Element.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalStateException If the Value Representation has not been
   *                               set in the stream.
   */
  public void updatePixelDataHeader(int valueLength) throws IOException
    {
      // Get the current stream position
      long offset = _outputStream.getStreamPosition();

      // Move to the beginning of the pixel data header
      _outputStream.seek( _pixelDataHeader.getOffset() );

      // Overwrite the pixel data header with the new Value Length
      writePixelDataHeader(valueLength);

      // Return to the previous stream position
      _outputStream.seek(offset);
    }

  /**
   * Writes pixel data to the current position of the output stream.
   *
   * @param dataBuffer Data Buffer containing the pixel data.
   *
   * @throws IOException If an error occurs during writing.
   */
  public void writePixelData(DataBuffer dataBuffer) throws IOException
    {
      // Record the current stream offset
      long offset = _outputStream.getStreamPosition();

      // Write an 8 bit grayscale image
      if (dataBuffer instanceof DataBufferByte) {
	_outputStream.write( ((DataBufferByte)dataBuffer).getData() );
      }

      // Write a 16 bit grayscale image
      else if (dataBuffer instanceof DataBufferUShort) {
	short[] data = ((DataBufferUShort)dataBuffer).getData();

	// Write the image, byte swapping if necessary
	if (_byteSwap) { _outputStream.setByteOrder(ByteOrder.LITTLE_ENDIAN); }
	_outputStream.writeShorts(data, 0, data.length);
	_outputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
      }

      // Write a 32 bit RGB color image
      else if (dataBuffer instanceof DataBufferInt) {
	int[] data = ((DataBufferInt)dataBuffer).getData();
	_outputStream.write( _separateRgbComponents(data) );
      }

      // Unrecognized type
      else {
	String msg = "Unable to recognize the image type.";
	throw new IOException(msg);
      }

      // Record the size of the pixel data block
      long size = _outputStream.getStreamPosition() - offset;
      _pixelData.add( new DataBlock(offset, size) );
    }

  /**
   * Gets the size of all the pixel data that has been written.
   *
   * @return Size of all the pixel data that has been written.
   */
  public long getPixelDataSize()
    {
      // Add the sizes of all the pixel data Data Blocks
      long totalSize = 0;
      Iterator iter = _pixelData.iterator();
      while ( iter.hasNext() ) {
	DataBlock dataBlock = (DataBlock)iter.next();
	totalSize += dataBlock.getSize();
      }

      // Return the size of all the pixel data Data Blocks
      return totalSize;
    }

  /**
   * Writes the sequence footer to the current position of the output stream.
   *
   * @param dataSet Data Set containing the Data Elements to write.
   *
   * @throws IOException If an error occurs during writing.
   */
  public void writeSequenceFooter(DataSet dataSet) throws IOException
    {
      // Record the current stream offset
      long offset = _outputStream.getStreamPosition();

      // Get the Data Element encoder
      OutputStream oStream = new OutputStreamAdapter(_outputStream);
      DataElementEncoder encoder = new DataElementEncoder(oStream,
							  _transferSyntax);

      // Write all the Data Elements after the pixel Data Element
      Enumeration enun = dataSet.getDataElements();
      while ( enun.hasMoreElements() ) {
	DataElement dataElement = (DataElement)enun.nextElement();
	if ( dataElement.getDataElementTag().compareTo("7FE00010") > 0 ) {
	  encoder.writeDataElement(dataElement);
	}
      }

      // Record the size of the sequence footer block
      long size = _outputStream.getStreamPosition() - offset;
      _sequenceFooter = new DataBlock(offset, size);
    }

  /**
   * Writes the sequence header to the specified Output Stream.
   *
   * @param outputStream Output Stream to write the sequence header to.
   * @param dataSet Data Set containing the Data Elements to write.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the Transfer Syntax value cannot be
   *                                  determined.
   */
  private void _writeSequenceHeader(OutputStream outputStream, DataSet dataSet)
    throws IOException
    {
      // Write a blank 128 byte preamble
      byte[] preamble = new byte[128];
      outputStream.write(preamble);

      // Write the 4 character identification
      String id = "DICM";
      outputStream.write( id.getBytes() );

      // Write all the Data Elements before the 7FE0 Group Length Element
      DataSetEncoder encoder = new DataSetEncoder(outputStream, "7FE00000");
      encoder.writeDataSet(dataSet);
    }

  /**
   * Relocates the specified Data Block to another part of the stream.
   *
   * @param dataBlock Data Block to relocate.
   * @param delta Number of bytes to relocate the Data Block by.
   *
   * @return Data Block that has been relocated.
   *
   * @throws IOException If an error occurs during reading or writing.
   */
  private DataBlock _relocateDataBlock(DataBlock dataBlock, int delta)
    throws IOException
    {
      // Move to the beginning of the Data Block
      _outputStream.seek( dataBlock.getOffset() );

      // Read the Data Block into memory
      byte[] contents = new byte[ (int)dataBlock.getSize() ];
      _outputStream.readFully(contents);

      // Move the required number of bytes
      _outputStream.seek( dataBlock.getOffset() + delta );

      // Write the Data Block contents back to the stream
      _outputStream.write(contents);

      // Return the updated Data Block
      return new DataBlock(dataBlock.getOffset() + delta, contents.length);
    }

  /**
   * Separates the RGB components of each integer pixel into 3 bytes.
   *
   * @param intArray Array of integers where each element represents an ARGB
   *                 pixel.
   *
   * @return Byte array where each consecutive 3 bytes represents the RGB
   *         components of an ARGB pixel in the integer array.
   */
  private byte[] _separateRgbComponents(int[] intArray)
    {
      // Create a byte array to hold the RGB components
      byte[] byteArray = new byte[3*intArray.length];

      // Separate the RGB components of each integer pixel
      for (int i = 0; i < intArray.length; i++) {
	int pixel = intArray[i];

	byteArray[3*i]   = (byte)( (pixel >> 16) & 0x000000ff );
	byteArray[3*i+1] = (byte)( (pixel >>  8) & 0x000000ff );
	byteArray[3*i+2] = (byte)( (pixel      ) & 0x000000ff );
      }

      // Return the RGB components
      return byteArray;
    }
}
