/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.encoder;

import edu.ucla.loni.jdicom.DataElement;
import edu.ucla.loni.jdicom.DataSet;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Encodes a DICOM set of Data Elements to a stream.
 *
 * @version 10 May 2007
 */
public class DataSetEncoder
{
  /** Output Stream to write encoded data to. */
  private OutputStream _outputStream;

  /** 8 digit hexadecimal Data Element Tag marking where writing is stopped. */
  private String _dataElementStopAtTag;

  /**
   * Constructs a DataSetEncoder that writes encoded data to the specified
   * output stream.
   *
   * @param outputStream Output Stream to write encoded data to.
   */
  public DataSetEncoder(OutputStream outputStream)
    {
     this(outputStream, null);
    }

  /**
   * Constructs a DataSetEncoder that writes encoded data to the specified
   * output stream.  If the specified Data Element Tag is not null, then no
   * Data Element of any Data Set will be written if its Data Element Tag is
   * equal to or greater than the specified Tag.
   *
   * @param outputStream Output Stream to write encoded data to.
   * @param dataElementStopAtTag 8 digit hexadecimal number marking where the
   *                             writing of a Data Set should stop.  If this is
   *                             null, all Data Sets are fully written.
   */
  public DataSetEncoder(OutputStream outputStream, String dataElementStopAtTag)
    {
      _outputStream = outputStream;
      _dataElementStopAtTag = dataElementStopAtTag;
    }

  /**
   * Writes the specified Data Set to the Output Stream.  The Data Set must
   * contain a Transfer Syntax Data Element from which byte ordering and
   * structure of encoded Data Elements can be deduced.
   *
   * @param dataSet Data Set to write to the Output Stream.
   *
   * @throws IOException If an I/O error occurs.
   * @throws IllegalArgumentException If the Data Set does not contain a
   *                                  valid Transfer Syntax Data Element.
   */
  public void writeDataSet(DataSet dataSet) throws IOException
    {
      // Get the Transfer Syntax Data Element
      DataElement transferSyntax = dataSet.getDataElement("00020010");
      if (transferSyntax == null) {
	String msg = "Unable to find a Transfer Syntax Data Element in the " +
	             "Data Set.";
	throw new IllegalArgumentException(msg);
      }

      // Get the Transfer Syntax UID
      Enumeration enun = transferSyntax.getValues();
      if ( !enun.hasMoreElements() ) {
	String msg = "Unable to find a value for the Transfer Syntax Data " +
	             "Element.";
	throw new IllegalArgumentException(msg);
      }
      String transferSyntaxUID = (String)enun.nextElement();

      // Write the Data Set
      writeDataSet(dataSet, transferSyntaxUID);
    }

  /**
   * Writes the specified Data Set to the Output Stream using the specified
   * Transfer Syntax.
   * <p>
   * <b> PS 3.5-1998, TRANSFER SYNTAX 
   *     [Section 10], Page 40: </b>
   * <em> 
   * A Transfer Syntax is a set of encoding rules able to unambiguously
   * represent one or more Abstract Syntaxes. In particular, it allows
   * communicating Application Entities to negotiate common encoding
   * techniques they both support (e.g., byte ordering, compression, etc.).
   * </em>
   *
   * @param dataSet Data Set to write to the Output Stream.
   * @param transferSyntaxUID UID of the Transfer Syntax.
   *
   * @throws IOException If an I/O error occurs.
   * @throws IllegalArgumentException If the Transfer Syntax UID is invalid.
   */
  public void writeDataSet(DataSet dataSet, String transferSyntaxUID)
    throws IOException
    {
      // Separate the Data Elements of the Data Set into their Groups
      Vector groups = _getGroups(dataSet);

      // Write each Group of Data Elements to the stream
      Enumeration enun = groups.elements();
      while ( enun.hasMoreElements() ) {
	_writeGroup( (DataSet)enun.nextElement(), transferSyntaxUID );
      }
    }

  /**
   * Writes the Data Set to the Output Stream as a group of Data Elements.  All
   * the Data Element Tags in the Data Set are assumed to have the same Group
   * Number.
   * <p>
   * <b> PS 3.5-1998, DATA ELEMENT FIELDS
   *     [Section 7.1.1], Page 25: </b>
   * <em>
   * Data Element Tag: An ordered pair of 16-bit unsigned integers
   * representing the Group Number followed by Element Number.
   * </em>
   *
   * @param dataSet Data Set whose Data Elements are written to the Output
   *                Stream as a group.
   * @param transferSyntaxUID UID of the Transfer Syntax that determines the
   *                          byte ordering and structure during encoding.
   *
   * @throws IOException If an I/O error occurs.
   * @throws IllegalArgumentException If the Transfer Syntax UID is invalid.
   */
  private void _writeGroup(DataSet dataSet, String transferSyntaxUID)
    throws IOException
    {
      // Get the first Data Element of the Data Set
      Enumeration enun = dataSet.getDataElements();
      if ( !enun.hasMoreElements() ) { return; }
      DataElement dataElement = (DataElement)enun.nextElement();

      // Get the Group and Element number of the Group
      String dataElementTag = dataElement.getDataElementTag();
      String groupNumber = dataElementTag.substring(0, 4);

      // Ensure the Transfer Syntax is as required for Group 2 Data Elements
      //
      // PS 3.10-1996, DICOM FILE META INFORMATION
      // [Section 7.1], Page 14:
      // Except for the 128 byte preamble and the 4 byte prefix, the File
      // Meta Information shall be encoded using the Explicit VR Little
      // Endian Transfer Syntax (UID=1.2.840.10008.1.2.1) as defined in
      // DICOM PS 3.5.
      String encodeUID = transferSyntaxUID;
      if ( groupNumber.equals("0002") ) { encodeUID = "1.2.840.10008.1.2.1"; }

      // Remove any Group Length Element
      dataSet.removeDataElement(groupNumber + "0000");

      // Add a temporary Group Length Element with length of zero
      dataSet.addDataElement( _createGroupLength(groupNumber, 0) );

      // Create a byte buffer to encode into (initial size 8096 bytes)
      ByteArrayOutputStream bStream = new ByteArrayOutputStream(8096);
      DataElementEncoder encoder = new DataElementEncoder(bStream, encodeUID);

      // Encode the Data Elements in the byte buffer
      enun = dataSet.getDataElements();
      while ( enun.hasMoreElements() ) {
	dataElement = (DataElement)enun.nextElement();
	dataElementTag = dataElement.getDataElementTag();

	// Ensure the Transfer Syntax Data Element has the UID as a value
	//
	// PS 3.6-1998, REGISTRY OF DICOM DATA ELEMENTS [Section 6], Page 5
	if ( dataElementTag.equals("00020010") ) {
	  dataElement = new DataElement("00020010", "UI",
					"Transfer Syntax UID");
	  dataElement.addValue(transferSyntaxUID);
	}

	encoder.writeDataElement(dataElement);
      }

      // Get the Data Set byte buffer contents
      byte[] dataSetBuffer = bStream.toByteArray();

      // Get the size of the Group Length Element
      bStream.reset();
      encoder.writeDataElement( _createGroupLength(groupNumber, 0) );
      int elementSize = bStream.size();

      // Encode the Group Length Element with the correct length
      //
      // PS 3.10-1996, DICOM FILE META INFORMATION
      // [Table 7.1-1], Page 13:
      // Number of bytes following this File Meta Element (end of the Value
      // field) up to and including the last File Meta Element of the Group 2
      // File Meta Information
      bStream.reset();
      int groupSize = dataSetBuffer.length - elementSize;
      encoder.writeDataElement( _createGroupLength(groupNumber, groupSize) );

      // Update the Data Set buffer and write it to the Output Stream
      byte[] lengthBuffer = bStream.toByteArray();
      System.arraycopy(lengthBuffer, 0, dataSetBuffer, 0, lengthBuffer.length);
      _outputStream.write(dataSetBuffer);
    }

  /**
   * Separates the Data Set into its Groups of Data Elements.
   *
   * @param dataSet Data Set containing the Data Elements to separate.
   *
   * @return One Data Set for each Data Element Group.
   */
  private Vector _getGroups(DataSet dataSet)
    {
      Vector groups = new Vector();
      DataSet currentGroup = null;
      String currentGroupNumber = "*";
	
      // Separate all the Data Elements into Groups
      Enumeration enun = dataSet.getDataElements();
      while ( enun.hasMoreElements() ) {
	DataElement dataElement = (DataElement)enun.nextElement();

	// If the Data Element has a Tag >= can be written, return
	String dataElementTag = dataElement.getDataElementTag();
	if ( _dataElementStopAtTag != null &&
	     _dataElementStopAtTag.compareTo(dataElementTag) <= 0 )
	  {
	    return groups;
	  }

	// Start a new Group of Data Elements
	if ( !dataElementTag.startsWith(currentGroupNumber) ) {
	  currentGroupNumber = dataElementTag.substring(0, 4);

	  // Create a new Group
	  currentGroup = new DataSet();
	  groups.addElement(currentGroup);
	}

	// Add the Data Element to the current Group
	currentGroup.addDataElement(dataElement);
      }

      // Return the Groups
      return groups;
    }

  /**
   * Creates a Group Length Data Element.
   * <p>
   * <b> PS 3.5-1998, GROUP LENGTH
   *     [Section 7.1.1], Page 25: </b>
   * <em>
   * The Group Length (gggg,0000) Standard Data Element shall be an optional
   * (Type 3 Data Element Type) Data Element in DICOM V3.0 (see Section 7.4
   * for a description of Data Element Types). It provides an optional
   * optimization scheme in partial parsing of Data Sets by allowing the
   * skipping of entire groups of Data Elements. Implementations may or may
   * not choose to encode explicit Group Length in a Data Set. All
   * implementations shall be able to accept or ignore such elements.
   * </em>
   *
   * @param groupNumber 4 digit hexadecimal number that identifies the Group.
   * @param length Length (in bytes) of the Group.
   *
   * @return Group Length Data Element with the specified parameters.
   */
  private DataElement _createGroupLength(String groupNumber, long length)
    {
      // Set the Data Element fields
      //
      // PS 3.6-1998, REGISTRY OF DICOM DATA ELEMENTS [Section 6], Page 5
      String tag = groupNumber + "0000";
      String vr = "UL";
      String name = "Group Length";

      // Create and return the Group Length Data Element
      DataElement groupLength = new DataElement(tag, vr, name);
      groupLength.addValue( new Long(length) );
      return groupLength;
    }
}
