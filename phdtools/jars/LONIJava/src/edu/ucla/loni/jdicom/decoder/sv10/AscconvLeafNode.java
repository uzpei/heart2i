/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.jdicom.decoder.sv10;

import java.util.Vector;

/**
 * ASCCONV Node that represents a leaf in an ASCCONV tree.
 *
 * @version 24 August 2007
 */
public class AscconvLeafNode extends AscconvNode
{
    /** Values of the Node. */
    private Vector _values;

    /**
     * Constructs an ASCCONV Leaf Node.
     *
     * @param name Name of the ASCCONV Node.
     */
    public AscconvLeafNode(String name)
    {
	super(name);

	_values = new Vector();
    }

    /**
     * Adds a value to the Node.
     *
     * @param value Value to add to the Node.
     */
    public void addValue(String value)
    {
	_values.add(value);
    }

    /**
     * Gets the values of the Node.
     *
     * @return Values of the Node.
     */
    public String[] getValues()
    {
	return (String[])_values.toArray(new String[0]);
    }
}
