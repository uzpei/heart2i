/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.pacs.PacsElement;
import edu.ucla.loni.pacs.PacsElements;
import edu.ucla.loni.pacs.PacsSubheader;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for image metadata of the UCLA PACS image format.
 *
 * @version 19 May 2004
 */
public class PacsImageMetadata extends AppletFriendlyIIOMetadata
{
  /** PACS image Subheader containing original metadata. */
  private PacsSubheader _originalImageSubheader;

  /** PACS image Subheader containing current metadata. */
  private PacsSubheader _currentImageSubheader;

  /** PACS image appendix Subheader containing original metadata. */
  private PacsSubheader _originalImageAppendixSubheader;

  /** PACS image appendix Subheader containing current metadata. */
  private PacsSubheader _currentImageAppendixSubheader;

  /**
   * Constructs a PACS Image Metadata from the given PACS Subheaders.
   *
   * @param imageHeader PACS image Subheader containing metadata.
   * @param imageAppendixHeader PACS image appendix Subheader containing
   *                            metadata.
   */
  public PacsImageMetadata(PacsSubheader imageHeader,
			   PacsSubheader imageAppendixHeader)
    {
      super(false, PacsImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    PacsImageMetadataFormat.class.getName(), null, null);

      _originalImageSubheader = imageHeader;
      _currentImageSubheader = imageHeader;
      _originalImageAppendixSubheader = imageAppendixHeader;
      _currentImageAppendixSubheader = imageAppendixHeader;
    }

  /**
   * Returns true if this object does not support the mergeTree, setFromTree,
   * and reset methods.  
   *
   * @return True if this IIOMetadata object cannot be modified; false o/w.
   */
  public boolean isReadOnly()
    {
      return false;
    }

  /**
   * Returns an XML DOM Node object that represents the root of a tree of
   * metadata contained within this object according to the conventions
   * defined by a given metadata format.
   *
   * @param formatName Name of the desired metadata format.
   *
   * @return An XML DOM Node object forming the root of a tree.
   *
   * @throws IllegalArgumentException If formatName is not one of the allowed
   *                                  metadata format names.
   */
  public Node getAsTree(String formatName)
    {
      // Native metadata format name
      if ( nativeMetadataFormatName.equals(formatName) ) {
	return PacsMetadataConversions.toImageTree(_currentImageSubheader,
					       _currentImageAppendixSubheader);
      }

      // Unknown format name
      String msg = "The format name \"" + formatName + "\" is not a " +
	           "supported metadata format name.";
      throw new IllegalArgumentException(msg);
    }

  /**
   * Alters the internal state of this IIOMetadata object from a tree of XML
   * DOM Nodes whose syntax is defined by the given metadata format.  The
   * previous state is altered only as necessary to accomodate the nodes that
   * are present in the given tree.
   *
   * @param formatName Name of the desired metadata format.
   * @param root An XML DOM Node object forming the root of a tree.
   *
   * @throws IllegalArgumentException If formatName is not one of the allowed
   *                                  metadata format names, or if the root is
   *                                  null.
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   */
  public void mergeTree(String formatName, Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check for a matching root Node
      if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	String msg = "Root node must be named \"" + nativeMetadataFormatName +
	             "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // If there is no current metadata, convert the root
      Node r;
      if (_currentImageSubheader == null ||
	  _currentImageAppendixSubheader == null)
	{
	  r = root;
	}

      // Otherwise merge the root with the current metadata tree Nodes
      else {

	// Get the current metadata tree Nodes
	Node imageNode = getAsTree(formatName);
	Node imgNode = imageNode.getFirstChild();
	Node imgAppNode = imgNode.getNextSibling();

	// Loop through the root Node children and merge their attributes
	NodeList nodeList = root.getChildNodes();
	for (int i = 0; i < nodeList.getLength(); i++) {
	  Node childNode = nodeList.item(i);
	  String childName = childNode.getNodeName();
	  NamedNodeMap childAttrs = childNode.getAttributes();

	  // Replace Image Subheader attributes
	  String imgName = PacsImageMetadataFormat.PACS_IMG_SUBHEADER_NAME;
	  if ( imgName.equals(childName) ) {
	    imgNode = _getMergedNode(imgNode, childAttrs);
	  }

	  // Replace Image Appendix Subheader attributes
	  imgName = PacsImageMetadataFormat.PACS_IMGX_SUBHEADER_NAME;
	  if ( imgName.equals(childName) ) {
	    imgAppNode = _getMergedNode(imgAppNode, childAttrs);
	  }
	}

	// Recreate the root tree
	r = new IIOMetadataNode(nativeMetadataFormatName);
	r.appendChild(imgNode);
	r.appendChild(imgAppNode);
      }

      // Set the new current Image PACS Subheader
      Node r1 = r.getFirstChild();
      _currentImageSubheader =
	PacsMetadataConversions.toImagePacsSubheader(r1);

      // Set the new current Image Appendix PACS Subheader
      Node r2 = r1.getNextSibling();
      _currentImageAppendixSubheader =
	PacsMetadataConversions.toImageAppendixPacsSubheader(r2);
    }

  /**
   * Resets all the data stored in this object to default values, usually to
   * the state this object was in immediately after construction, though the
   * precise semantics are plug-in specific.  Note that there are many possible
   * default values, depending on how the object was created.
   */
  public void reset()
    {
      // Reset the current PACS Subheaders to the originals
      _currentImageSubheader = _originalImageSubheader;
      _currentImageAppendixSubheader = _originalImageAppendixSubheader;
    }

  /**
   * Merges the attributes with the specified Node.  If there are attribute
   * names not present in the attributes of the node, the attributes are
   * ignored.
   *
   * @param node Node to merge the attributes with.
   * @param attributes Attributes to merge.
   *
   * @return New Node containing the merged attributes.
   */
  private Node _getMergedNode(Node node, NamedNodeMap attributes)
    {
      // Create a new Node with the same name
      IIOMetadataNode mergedNode = new IIOMetadataNode( node.getNodeName() );

      // Copy the Node attributes to the new Node
      NamedNodeMap map = node.getAttributes();
      if (map != null) {
	for (int i = 0; i < map.getLength(); i++) {
	  Node attr = map.item(i);
	  mergedNode.setAttribute( attr.getNodeName(), attr.getNodeValue() );
	}
      }

      // Merge the new attributes with the old
      if (attributes != null) {
	for (int i = 0; i < attributes.getLength(); i++) {
	  Node attribute = attributes.item(i);
	  String attributeName = attribute.getNodeName();

	  // Attribute name must be present in the new Node
	  if ( mergedNode.hasAttribute(attributeName) ) {
	    mergedNode.setAttribute(attributeName, attribute.getNodeValue());
	  }
	}
      }

      // Return the new merged Node
      return mergedNode;
    }
}
