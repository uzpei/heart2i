/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElement;
import edu.ucla.loni.pacs.PacsElements;
import edu.ucla.loni.pacs.PacsHeader;
import edu.ucla.loni.pacs.PacsSubheader;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes a PACS Header from an Image Input Stream.
 * <p>
 * The encoding of a PACS Header that contains data on N images in the
 * UCLA PACS V5 format can be schematically illustrated as:
 * <p>
 * <pre>
 * +------------+
 * | 1024 bytes |   MAIN SUBHEADER            General information data
 * +------------+
 * |  512 bytes |   MODALITY SUBHEADER        Modality-specific data
 * +------------+
 * |  168 bytes |   IMAGE APPENDIX SUBHEADER  Image #1-specific data Appendix
 * +------------+
 * |  292 bytes |   FREE TEXT                 Unused space
 * +------------+
 * |   52 bytes |   IMAGE SUBHEADER           Image #1-specific data
 * +------------+
 * |  168 bytes |   IMAGE APPENDIX SUBHEADER  Image #2-specific data Appendix
 * +------------+
 * |  292 bytes |   FREE TEXT                 Unused space
 * +------------+
 * |   52 bytes |   IMAGE SUBHEADER           Image #2-specific data
 * +------------+
 *       .
 *       .
 *       .
 * +------------+
 * |  168 bytes |   IMAGE APPENDIX SUBHEADER  Image #N-specific data Appendix
 * +------------+
 * |  292 bytes |   FREE TEXT                 Unused space
 * +------------+
 * |   52 bytes |   IMAGE SUBHEADER           Image #N-specific data
 * +------------+
 * </pre>
 *
 * @version 6 April 2004
 */
public class PacsHeaderDecoder
{
  /** PACS Header decoded from the Image Input Stream. */
  private PacsHeader _pacsHeader;

  /**
   * Constructs a PACS Header Decoder which uses the specified Image Input
   * Stream to decode the PACS Header.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public PacsHeaderDecoder(ImageInputStream in) throws IOException
    {
      try {

	// Decode the Main Subheader
	MainSubheaderDecoder mainDecoder = new MainSubheaderDecoder(in);
	PacsSubheader mainSubheader = mainDecoder.getSubheader();

	// Decode the Modality Subheader
	String peName = PacsElements.STUDY_MODALITY;
	PacsElement pe = mainSubheader.getPacsElement(peName);
	String modality = (String)pe.getValue();

	PacsSubheader modalitySubheader = null;
	if ( modality.startsWith("MR") ) {
	  MRSubheaderDecoder modalityDecoder = new MRSubheaderDecoder(in);
	  modalitySubheader = modalityDecoder.getSubheader();
	}
	else if ( modality.startsWith("CT") ) {
	  CTSubheaderDecoder modalityDecoder = new CTSubheaderDecoder(in);
	  modalitySubheader = modalityDecoder.getSubheader();
	}
	else if ( modality.startsWith("CR") ) {
	  CRSubheaderDecoder modalityDecoder = new CRSubheaderDecoder(in);
	  modalitySubheader = modalityDecoder.getSubheader();
	}
	else if ( modality.startsWith("XA") ) {
	  XASubheaderDecoder modalityDecoder = new XASubheaderDecoder(in);
	  modalitySubheader = modalityDecoder.getSubheader();
	}

	// Create the PACS Header
	_pacsHeader = new PacsHeader(mainSubheader, modalitySubheader);

	// Decode the Image Subheaders
	peName = PacsElements.NO_IMG_IN_FILE;
	pe = mainSubheader.getPacsElement(peName);
	int numberOfImages = ((Integer)pe.getValue()).intValue();
	for (int i = 0; i < numberOfImages; i++) {

	  // Decode the next Image Appendix Subheader
	  ImageAppendixSubheaderDecoder imgxDecoder =
	    new ImageAppendixSubheaderDecoder(in);
	  _pacsHeader.addImageAppendixSubheader( imgxDecoder.getSubheader() );

	  // Skip the free text
	  in.skipBytes(292);

	  // Decode the next Image Subheader
	  ImageSubheaderDecoder imgDecoder = new ImageSubheaderDecoder(in);
	  _pacsHeader.addImageSubheader( imgDecoder.getSubheader() );
	}
      }
      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode the PACS Header.");
	ioe.initCause(e);
	throw ioe;
      }
    }

  /**
   * Gets the decoded PACS Header.
   *
   * @return PACS Header decoded from the Image Input Stream.
   */
  public PacsHeader getPacsHeader()
    {
      return _pacsHeader;
    }
}
