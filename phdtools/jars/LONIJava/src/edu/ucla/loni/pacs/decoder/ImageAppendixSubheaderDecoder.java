/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes an Image Appendix PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public class ImageAppendixSubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs an Image Appendix Subheader Decoder which uses the specified
   * Image Input Stream to decode the Image Appendix PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public ImageAppendixSubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {
	String name = PacsElements.VERSION;
	String desc = PacsElements.VERSION_DESC;
	_readString(name, desc, 8);

	name = PacsElements.WINDOW_LEVEL;
	desc = PacsElements.WINDOW_LEVEL_DESC;
	_readInt(name, desc);

	name = PacsElements.WINDOW_WIDTH;
	desc = PacsElements.WINDOW_WIDTH_DESC;
	_readInt(name, desc);

	name = PacsElements.MR_REPETITION_TIME;
	desc = PacsElements.MR_REPETITION_TIME_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_ECHO_TIME;
	desc = PacsElements.MR_ECHO_TIME_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION;
	desc = PacsElements.PAT_ORIENTATION_DESC;
	_readString(name, desc, 16);

	name = PacsElements.IMG_NUMBER;
	desc = PacsElements.IMG_NUMBER_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_ROWS;
	desc = PacsElements.IMG_ROWS_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_COLS;
	desc = PacsElements.IMG_COLS_DESC;
	_readInt(name, desc);

	name = PacsElements.X_PIX_SIZE;
	desc = PacsElements.X_PIX_SIZE_DESC;
	_readFloat(name, desc);

	name = PacsElements.Y_PIX_SIZE;
	desc = PacsElements.Y_PIX_SIZE_DESC;
	_readFloat(name, desc);

	name = PacsElements.FIELD_OF_VIEW;
	desc = PacsElements.FIELD_OF_VIEW_DESC;
	_readString(name, desc, 16);

	name = PacsElements.IMG_MIN;
	desc = PacsElements.IMG_MIN_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_MAX;
	desc = PacsElements.IMG_MAX_DESC;
	_readInt(name, desc);

	name = PacsElements.RECON_ALGORITHM;
	desc = PacsElements.RECON_ALGORITHM_DESC;
	_readInt(name, desc);

	name = PacsElements.THICK;
	desc = PacsElements.THICK_DESC;
	_readFloat(name, desc);
 
	// 3D rendering fields - VI03
	name = PacsElements.IMG_POS_PAT_X;
	desc = PacsElements.IMG_POS_PAT_X_DESC;
	_readFloat(name, desc);

	name = PacsElements.IMG_POS_PAT_Y;
	desc = PacsElements.IMG_POS_PAT_Y_DESC;
	_readFloat(name, desc);

	name = PacsElements.IMG_POS_PAT_Z;
	desc = PacsElements.IMG_POS_PAT_Z_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION_X1;
	desc = PacsElements.PAT_ORIENTATION_X1_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION_Y1;
	desc = PacsElements.PAT_ORIENTATION_Y1_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION_Z1;
	desc = PacsElements.PAT_ORIENTATION_Z1_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION_X2;
	desc = PacsElements.PAT_ORIENTATION_X2_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION_Y2;
	desc = PacsElements.PAT_ORIENTATION_Y2_DESC;
	_readFloat(name, desc);

	name = PacsElements.PAT_ORIENTATION_Z2;
	desc = PacsElements.PAT_ORIENTATION_Z2_DESC;
	_readFloat(name, desc);

	// XA/RF compatibility fields - VI04
	name = PacsElements.XA_FRAME_NUMBER;
	desc = PacsElements.XA_FRAME_NUMBER_DESC;
	_readInt(name, desc);

	name = PacsElements.XA_ORIG_FRAME_NO;
	desc = PacsElements.XA_ORIG_FRAME_NO_DESC;
	_readInt(name, desc);

	// Frame rate info - VI04
	name = PacsElements.XA_FRAME_INTERVAL;
	desc = PacsElements.XA_FRAME_INTERVAL_DESC;
	_readFloat(name, desc);

	// XA positioner info - VI04
	name = PacsElements.XA_PRIM_ANGLE_INCREMENT;
	desc = PacsElements.XA_PRIM_ANGLE_INCREMENT_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_SEC_ANGLE_INCREMENT;
	desc = PacsElements.XA_SEC_ANGLE_INCREMENT_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_CURRENT_PRIM_ANGLE;
	desc = PacsElements.XA_CURRENT_PRIM_ANGLE_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_CURRENT_SEC_ANGLE;
	desc = PacsElements.XA_CURRENT_SEC_ANGLE_DESC;
	_readFloat(name, desc);

	// XA table info - VI04
	name = PacsElements.XA_TABLE_VERTIC_INCREMENT;
	desc = PacsElements.XA_TABLE_VERTIC_INCREMENT_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_TABLE_LATERAL_INCREMENT;
	desc = PacsElements.XA_TABLE_LATERAL_INCREMENT_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_TABLE_LONGIT_INCREMENT;
	desc = PacsElements.XA_TABLE_LONGIT_INCREMENT_DESC;
	_readFloat(name, desc);
      }

      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode an appendix " +
					  "subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
