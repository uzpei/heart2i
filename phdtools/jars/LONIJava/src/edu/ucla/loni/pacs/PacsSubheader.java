/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.pacs;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Nested data set for UCLA PACS medical image data.  This subset is contained
 * inside the primary data set (PacsHeader) and holds data which can describe
 * the main data subset (Main Subheader), the modality-specific (MR, CT, CR,
 * or XA) data subset (Modality Subheader), or one of the image-specific data
 * subsets (Image and Image Appendix Subheaders).
 *
 * @version 10 May 2007
 */
public class PacsSubheader
{
  /** Data elements of the PacsSubheader. */
  private Vector _pacsElements = new Vector(10, 10);

  /** Constructs a PacsSubheader. */
  public PacsSubheader()
    {
    }

  /**
   * Adds a data element to the PacsSubheader.
   *
   * @param pacsElement Data element for the PacsSubheader.
   */
  public void addPacsElement(PacsElement pacsElement)
    {
      _pacsElements.addElement(pacsElement);
    }

  /**
   * Gets the data elements of the PacsSubheader.
   *
   * @return PacsElements contained in the PacsSubheader.
   */
  public Enumeration getPacsElements()
    {
      return _pacsElements.elements();
    }

  /**
   * Gets the data element of the PacsSubheader referred to by the ID.
   *
   * @param id ID string that identifies a PacsElement of the PacsSubheader.
   *
   * @return PacsElement corresponding to the ID string, or null if it
   *         does not exist.
   */
  public PacsElement getPacsElement(String id)
    {
      // Search each PacsElement for a matching ID
      Enumeration enun = getPacsElements();
      while ( enun.hasMoreElements() ) {
	PacsElement pe = (PacsElement)enun.nextElement();
	if ( id.equals( pe.getID() ) ) {
	  return pe;
	}
      }

      // No PacsElement that matches the ID
      return null;
    }

  /**
   * Gets the contents of the PacsSubheader.
   *
   * @return String representation of the contents of the PacsSubheader.
   */
  public String toString()
    {
      // Buffer to fill with contents
      StringBuffer contents = new StringBuffer();

      // Fill the buffer with the String representation of each data element
      Enumeration enun = getPacsElements();
      while ( enun.hasMoreElements() ) {
	contents.append( enun.nextElement() );
      }

      return contents.toString();
    }
}
