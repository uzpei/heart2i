/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElement;
import edu.ucla.loni.pacs.PacsSubheader;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Abstract decoder of a PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public abstract class SubheaderDecoder
{
  /** PACS Subheader decoded from the Image Input Stream. */
  private PacsSubheader _subheader;

  /** Image Input Stream to decode the PACS Subheader from. */
  private ImageInputStream _in;

  /**
   * Constructs a Subheader Decoder which uses the specified Image Input Stream
   * to decode a PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   */
  protected SubheaderDecoder(ImageInputStream in)
    {
      _in = in;
      _subheader = new PacsSubheader();
    }

  /**
   * Gets the decoded PACS Subheader.
   *
   * @return PACS Subheader decoded from the Image Input Stream.
   */
  public PacsSubheader getSubheader()
    {
      return _subheader;
    }

  /**
   * Reads a data element string from the Image Input Stream and adds it to the
   * PACS Subheader.
   *
   * @param id String which identifies the data.
   * @param description Description of what the data refers to.
   * @param length Number of characters in the data element string.
   *
   * @throws IOException If an I/O error occurs.
   */
  protected void _readString(String id, String description, int length)
    throws IOException
    {
      // Read the string data
      byte buffer[] = new byte[length];
      _in.readFully(buffer);

      // Find the index of the first zero byte
      int index = 0;
      while (index < length && buffer[index] != 0) { index++; }
	
      // Create a new PacsElement for the data and save it in the Subheader
      PacsElement pe = new PacsElement(id, description,
				       new String(buffer, 0, index));
      _subheader.addPacsElement(pe);
    }

  /**
   * Reads a data element integer from the Image Input Stream and adds it to
   * the PACS Subheader.
   *
   * @param id String which identifies the data.
   * @param description Description of what the data refers to.
   *
   * @throws IOException If an I/O error occurs.
   */
  protected void _readInt(String id, String description) throws IOException
    {
      // Read the integer data
      int i = _in.readInt();

      // Create a new PacsElement for the data and save it in the Subheader
      PacsElement pe = new PacsElement(id, description, new Integer(i));
      _subheader.addPacsElement(pe);
    }

  /**
   * Reads a data element float from the Image Input Stream and adds it to the
   * PACS Subheader.
   *
   * @param id String which identifies the data.
   * @param description Description of what the data refers to.
   *
   * @throws IOException If an I/O error occurs.
   */
  protected void _readFloat(String id, String description) throws IOException
    {
      // Read the float data
      float f = _in.readFloat();

      // Create a new PacsElement for the data and save it in the Subheader
      PacsElement pe = new PacsElement(id, description, new Float(f));
      _subheader.addPacsElement(pe);
    }
}
