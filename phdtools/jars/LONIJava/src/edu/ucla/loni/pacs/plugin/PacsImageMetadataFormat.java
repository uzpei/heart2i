/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import edu.ucla.loni.pacs.PacsElements;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata of a
 * UCLA PACS image.
 *
 * @version 6 April 2004
 */
public class PacsImageMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_pacs_image_1.0";

  /** Name of the UCLA PACS Image Subheader Node. */
  public static final String PACS_IMG_SUBHEADER_NAME = "PACS_SUBHEADER_IMG";

  /** Name of the UCLA PACS Image Appendix Subheader Node. */
  public static final String PACS_IMGX_SUBHEADER_NAME = "PACS_SUBHEADER_IMGX";

  /** Single instance of the PACS Image Metadata Format. */
  private static PacsImageMetadataFormat _format =
  new PacsImageMetadataFormat();

  /** Constructs a PACS Image Metadata Format. */
  private PacsImageMetadataFormat()
    {
      // Create the root with required children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_ALL);

      // Add the image subheader child with no children
      addElement(PACS_IMG_SUBHEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the image subheader attributes
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.IMG_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.IMG_ROWS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.IMG_COLS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.X_PIX_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.Y_PIX_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.FIELD_OF_VIEW,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.IMG_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.IMG_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.RECON_ALGORITHM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMG_SUBHEADER_NAME, PacsElements.THICK,
		   DATATYPE_FLOAT, true, null);

      // Add the image appendix subheader child with no children
      addElement(PACS_IMGX_SUBHEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the image appendix subheader attributes
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.VERSION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.WINDOW_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.WINDOW_WIDTH,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.MR_REPETITION_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.MR_ECHO_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_ROWS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_COLS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.X_PIX_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.Y_PIX_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.FIELD_OF_VIEW,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.RECON_ALGORITHM,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.THICK,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_POS_PAT_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_POS_PAT_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.IMG_POS_PAT_Z,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION_X1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION_Y1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION_Z1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION_X2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION_Y2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.PAT_ORIENTATION_Z2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.XA_FRAME_NUMBER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.XA_ORIG_FRAME_NO,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.XA_FRAME_INTERVAL,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME,
		   PacsElements.XA_PRIM_ANGLE_INCREMENT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME,
		   PacsElements.XA_SEC_ANGLE_INCREMENT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME,
		   PacsElements.XA_CURRENT_PRIM_ANGLE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME, PacsElements.XA_CURRENT_SEC_ANGLE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME,
		   PacsElements.XA_TABLE_VERTIC_INCREMENT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME,
		   PacsElements.XA_TABLE_LATERAL_INCREMENT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_IMGX_SUBHEADER_NAME,
		   PacsElements.XA_TABLE_LONGIT_INCREMENT,
		   DATATYPE_FLOAT, true, null);

      // Set the resource base name for element descriptions
      setResourceBaseName( PacsMetadataFormatResources.class.getName() );
    }

  /**
   * Gets an instance of the PACS Image Metadata Format.
   *
   * @return The single instance of the PACS Image Metadata Format.
   */
  public static PacsImageMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
