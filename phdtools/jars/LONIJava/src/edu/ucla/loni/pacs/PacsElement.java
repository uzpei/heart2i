/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs;

import java.io.Serializable;

/**
 * Data element of UCLA PACS medical image data.  This element is composed of
 * a UCLA PACS ID string, a description of the data the ID string refers to,
 * and the actual data value.
 *
 * @version 11 March 2004
 */
public class PacsElement implements Serializable
{
  /** ID string which identifies the data of the PacsElement. */
  private String _id;

  /** Description of what the data of the PacsElement refers to. */
  private String _description;

  /** Data value of the PacsElement. */
  private Object _value;

  /**
   * Constructs a PacsElement with the specified ID string, description, and
   * value.
   *
   * @param id ID string which identifies the data of the PacsElement.
   * @param description Description of what the data of the PacsElement
   *                    refers to.
   * @param value Data value of the PacsElement.
   */
  public PacsElement(String id, String description, Object value)
    {
      _id = id;
      _description = description;
      _value = value;
    }

  /**
   * Gets the ID string of the PacsElement.
   *
   * @return ID string which identifies the data of the PacsElement.
   */
  public String getID()
    {
      return _id;
    }

  /**
   * Gets the description of the PacsElement.
   *
   * @return Description of what the data of the PacsElement refers to.
   */
  public String getDescription()
    {
      return _description;
    }

  /**
   * Gets the value of the PacsElement.
   *
   * @return Data value of the PacsElement.
   */
  public Object getValue()
    {
      return _value;
    }

  /**
   * Gets the contents of the PacsElement.
   *
   * @return String representation of the contents of the PacsSubheader.
   */
  public String toString()
    {
      // Represent the contents with the description and value
      return getDescription() + " -- " + getValue() + "\n";
    }    
}
