/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes a CR PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public class CRSubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs a CR Subheader Decoder which uses the specified Image Input
   * Stream to decode the CR PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public CRSubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {

	// Source characteristics
	String name = PacsElements.CR_SENSITIVITY;
	String desc = PacsElements.CR_SENSITIVITY_DESC;
	_readString(name, desc, 12);

	name = PacsElements.CR_RANGE;
        desc = PacsElements.CR_RANGE_DESC;
	_readString(name, desc, 12);

	// Detector characteristics
	name = PacsElements.CR_PRE_SAMPLE_MODE;
        desc = PacsElements.CR_PRE_SAMPLE_MODE_DESC;
	_readString(name, desc, 16);

	// Image pre-processing parameters
	name = PacsElements.CR_PREPROC_PARAMETERS;
        desc = PacsElements.CR_PREPROC_PARAMETERS_DESC;
	_readString(name, desc, 64);

	// Display parameters
	name = PacsElements.CR_DISP_WIN_CENTER;
        desc = PacsElements.CR_DISP_WIN_CENTER_DESC;
	_readInt(name, desc);

	name = PacsElements.CR_DISP_WIN_WIDTH;
        desc = PacsElements.CR_DISP_WIN_WIDTH_DESC;
	_readInt(name, desc);

	// Max/min gray level in study
	name = PacsElements.CR_STUDY_MIN;
        desc = PacsElements.CR_STUDY_MIN_DESC;
	_readInt(name, desc);

	name = PacsElements.CR_STUDY_MAX;
        desc = PacsElements.CR_STUDY_MAX_DESC;
	_readInt(name, desc);

	// Extra filler space
	name = PacsElements.CR_FREE_TEXT;
        desc = PacsElements.CR_FREE_TEXT_DESC;
	_readString(name, desc, 392);
      }

      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode a CR subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
