/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.pacs;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Primary data set for UCLA PACS medical image data.  This set
 * contains one main data subset (Main Subheader) and one modality-specific
 * (MR, CT, CR, or XA) data subset (Modality Subheader).  These subsets hold
 * data which describe a set of images.  For each individual image,
 * this set also contains two data subsets (Image and Image Appendix
 * Subheaders) which describe the specific image.
 *
 * @version 10 May 2007
 */
public class PacsHeader
{
  /** Main data subset. */
  private PacsSubheader _mainSubheader;

  /** Modality-specific (MR, CT, CR, or XA) data subset. */
  private PacsSubheader _modalitySubheader;

  /** Image-specific data subsets. */
  private Vector _imageSubheaders;

  /** Image-specific data subset appendicies. */
  private Vector _imageAppendixSubheaders;

  /**
   * Constructs a PacsHeader using the specified Main and Modality data
   * subsets.
   *
   * @param mainSubheader Main data subset.
   * @param modalitySubheader Modality-specific (MR, CT, CR, or XA) data
   *                          subset.
   */
  public PacsHeader(PacsSubheader mainSubheader, 
		    PacsSubheader modalitySubheader)
    {
      _mainSubheader = mainSubheader;
      _modalitySubheader = modalitySubheader;
      _imageSubheaders = new Vector();
      _imageAppendixSubheaders = new Vector();
    }

  /**
   * Gets the Main Subheader.
   *
   * @return Main data subset.
   */
  public PacsSubheader getMainSubheader()
    {
      return _mainSubheader;
    }

  /**
   * Gets the Modality Subheader.
   *
   * @return Modality-specific (MR, CT, CR, or XA) data subset.
   */
  public PacsSubheader getModalitySubheader()
    {
      return _modalitySubheader;
    }

  /**
   * Adds an Image Subheader to the PacsHeader.
   *
   * @param imageSubheader Image-specific data subset.
   */
  public void addImageSubheader(PacsSubheader imageSubheader)
    {
      _imageSubheaders.addElement(imageSubheader);
    }

  /**
   * Adds an Image Appendix Subheader to the PacsHeader.
   *
   * @param imageAppendixSubheader Image-specific data subset appendix.
   */
  public void addImageAppendixSubheader(PacsSubheader imageAppendixSubheader)
    {
      _imageAppendixSubheaders.addElement(imageAppendixSubheader);
    }

  /**
   * Gets the Image Subheaders in the PacsHeader.
   *
   * @return Image-specific data subsets as PacsSubheaders.
   */
  public Enumeration getImageSubheaders()
    {
      return _imageSubheaders.elements();
    }

  /**
   * Gets the indexed Image Subheader in the PACS Header.
   *
   * @param index Index of the Image Subheader.
   *
   * @return Indexed Image Subheader in the PACS Header.
   *
   * @throws ArrayIndexOutOfBoundsException If the index is invalid.
   */
  public PacsSubheader getImageSubheader(int index)
    {
      return (PacsSubheader)_imageSubheaders.elementAt(index);
    }

  /**
   * Gets the Image Appendix Subheaders in the PacsHeader.
   *
   * @return Image-specific data subset appendices as PacsSubheaders.
   */
  public Enumeration getImageAppendixSubheaders()
    {
      return _imageAppendixSubheaders.elements();
    }

  /**
   * Gets the indexed Image Appendix Subheader in the PACS Header.
   *
   * @param index Index of the Image Appendix Subheader.
   *
   * @return Indexed Image Appendix Subheader in the PACS Header.
   *
   * @throws ArrayIndexOutOfBoundsException If the index is invalid.
   */
  public PacsSubheader getImageAppendixSubheader(int index)
    {
      return (PacsSubheader)_imageAppendixSubheaders.elementAt(index);
    }

  /**
   * Gets the contents of the PacsHeader.
   *
   * @return String representation of the contents of the PacsHeader.
   */
  public String toString()
    {
      // Buffer to fill with contents
      StringBuffer contents = new StringBuffer();

      // Main Subheader contents
      contents.append("\nMain Subheader: \n");
      contents.append( getMainSubheader() );

      // Modality Subheader contents
      contents.append("\nModality Subheader: \n");
      contents.append( getModalitySubheader() );

      // Image Subheader contents
      contents.append("\nImage Subheaders: \n");
      Enumeration enun = getImageSubheaders();
      while ( enun.hasMoreElements() ) {
	contents.append( enun.nextElement() );
      }

      // Image Appendix Subheader contents
      contents.append("\nImage Appendix Subheaders: \n");
      enun = getImageAppendixSubheaders();
      while ( enun.hasMoreElements() ) {
	contents.append( enun.nextElement() );
      }

      return contents.toString();
    }
}
