/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import edu.ucla.loni.pacs.PacsElements;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * UCLA PACS image stream.
 *
 * @version 6 April 2004
 */
public class PacsStreamMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_pacs_stream_1.0";

  /** Name of the UCLA PACS Header Node. */
  public static final String PACS_HEADER_NAME = "PACS_HEADER";

  /** Name of the UCLA PACS MR Subheader Node. */
  public static final String PACS_MR_SUBHEADER_NAME = "PACS_SUBHEADER_MR";

  /** Name of the UCLA PACS CT Subheader Node. */
  public static final String PACS_CT_SUBHEADER_NAME = "PACS_SUBHEADER_CT";

  /** Name of the UCLA PACS CR Subheader Node. */
  public static final String PACS_CR_SUBHEADER_NAME = "PACS_SUBHEADER_CR";

  /** Name of the UCLA PACS XA Subheader Node. */
  public static final String PACS_XA_SUBHEADER_NAME = "PACS_SUBHEADER_XA";

  /** Single instance of the PACS Stream Metadata Format. */
  private static PacsStreamMetadataFormat _format =
  new PacsStreamMetadataFormat();

  /** Constructs a PACS Stream Metadata Format. */
  private PacsStreamMetadataFormat()
    {
      // Create the root with required children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_SOME);

      // Add the main header child with no children
      addElement(PACS_HEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the main header attributes
      addAttribute(PACS_HEADER_NAME, PacsElements.FILE_TYPE, DATATYPE_STRING,
		   true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.IMG_DIM, DATATYPE_INTEGER,
		   true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.IMG_ROWS, DATATYPE_INTEGER,
		   true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.IMG_COLS, DATATYPE_INTEGER,
		   true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.NO_IMG_IN_FILE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PIX_BIT_ALLOCATED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PIX_BIT_STORED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PIX_BIT_HIGH_POS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.COMPR_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.COMPR_VERSION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.X_PIX_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.Y_PIX_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_SLICE_THICKNESS,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_SLICE_SPACING,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.FIELD_OF_VIEW,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.SUBHDR_BLK_SIZE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.SUBHDR_BLOCK_POSITION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.RID_PATH,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.RID_FILENAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.EXAM_REFERENCE_NO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_REFERENCE_NO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.SERIES_REFERENCE_NO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.RIS_STUDY_NO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PACS_STUDY_NO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PATIENT_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PATIENT_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PATIENT_BIRTH_DATE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PAT_SEX,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PATIENT_HISTORY,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PATIENT_STATUS,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.REASON_FOR_REQUEST,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_DESCRIPTION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_SUBDESCRIPTION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_ACQ_DATE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_ACQ_TIME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_RADIOLOGIST,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_REF_PHYSICIAN,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_MODALITY,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.STUDY_MISC_INFO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.PATIENT_PREPARATION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.INST_STATION_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.INST_ACQ_MANUF,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.INST_ACQ_MODEL,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.INST_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.RIS_STUDY_NO_EXT,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.IMG_ORIG_ID_NO,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_HEADER_NAME, PacsElements.FREE_TEXT,
		   DATATYPE_STRING, true, null);

      // Add the MR subheader child with no children
      addElement(PACS_MR_SUBHEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the MR subheader attributes
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_PAT_ORIENTATION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_PLANE_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_POSITION,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_LONGIT_REF,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_VERTIC_REF,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_START_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_END_X,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_END_Y,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_START_Z,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_END_Z,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_IMAGE_LOCATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_PULSE_SEQ_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_GATING_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_GAUSS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_COIL_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME,
		   PacsElements.MR_REPETITION_RECOVERY_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_SCAN_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_ECHO_DELAY,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_INVERSION_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_NO_ECHOES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_NO_EXCITATIONS,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_FLIP_ANGLE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_TRAVERSE_TIME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_DFLT_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_DFLT_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_STUDY_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_STUDY_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_MR_SUBHEADER_NAME, PacsElements.MR_FREE_TEXT,
		   DATATYPE_STRING, true, null);

      // Add the CT subheader child with no children
      addElement(PACS_CT_SUBHEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the CT subheader attributes
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_SERIES_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_EXPO_TIME,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_EXPO_RATE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_KVP,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_SCAN_SEQ,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_PAT_ORIENT,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_SRC_DET_DIST,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_SRC_PAT_DIST,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_POSIT_REF,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_SLICE_LOC,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_RECON_DIAM,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_CONV_KERN,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_RESCAL_INTER,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_RESCAL_SLOPE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_GRAY_SCALE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_DFLT_LEVEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_DFLT_WINDOW,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_STUDY_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_STUDY_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CT_SUBHEADER_NAME, PacsElements.CT_FREE_TEXT,
		   DATATYPE_STRING, true, null);

      // Add the CR subheader child with no children
      addElement(PACS_CR_SUBHEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the CR subheader attributes
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_SENSITIVITY,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_RANGE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_PRE_SAMPLE_MODE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_PREPROC_PARAMETERS,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_DISP_WIN_CENTER,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_DISP_WIN_WIDTH,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_STUDY_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_STUDY_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_CR_SUBHEADER_NAME, PacsElements.CR_FREE_TEXT,
		   DATATYPE_STRING, true, null);

      // Add the XA subheader child with no children
      addElement(PACS_XA_SUBHEADER_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the XA subheader attributes
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_IMG_FPS,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_IMG_FPS_VARIABLE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_IMG_PLANE_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_TABLE_MOTION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_POSITIONER_MOTION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_PRIM_ANGLE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_SEC_ANGLE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_FOV_SHAPE,
		   DATATYPE_STRING, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_SOURCE_TO_PATIENT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_SOURCE_TO_DETECTOR,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_EXPO_KVP,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_EXPO_CURRENT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_EXPO_TIME,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_EXPO_RATE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_FOCAL_SPOT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_CUT_FLAG,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_NUM_CURVES_ENCLOSED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(PACS_XA_SUBHEADER_NAME, PacsElements.XA_FREE_TEXT,
		   DATATYPE_STRING, true, null);

      // Set the resource base name for element descriptions
      setResourceBaseName( PacsMetadataFormatResources.class.getName() );
    }

  /**
   * Gets an instance of the PACS Stream Metadata Format.
   *
   * @return The single instance of the PACS Stream Metadata Format.
   */
  public static PacsStreamMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
