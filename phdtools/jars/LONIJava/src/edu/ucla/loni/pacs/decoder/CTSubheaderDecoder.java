/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes a CT PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public class CTSubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs an CT Subheader Decoder which uses the specified Image Input
   * Stream to decode the CT PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public CTSubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {

	// Series name
	String name = PacsElements.CT_SERIES_NAME;
	String desc = PacsElements.CT_SERIES_NAME_DESC;
	_readString(name, desc, 20);

	// Source characteristics
	name = PacsElements.CT_EXPO_TIME;
	desc = PacsElements.CT_EXPO_TIME_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_EXPO_RATE;
	desc = PacsElements.CT_EXPO_RATE_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_KVP;
	desc = PacsElements.CT_KVP_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_SCAN_SEQ;
	desc = PacsElements.CT_SCAN_SEQ_DESC;
	_readString(name, desc, 20);

	// Source, patient, detector geometry
	name = PacsElements.CT_PAT_ORIENT;
	desc = PacsElements.CT_PAT_ORIENT_DESC;
	_readString(name, desc, 12);

	name = PacsElements.CT_SRC_DET_DIST;
	desc = PacsElements.CT_SRC_DET_DIST_DESC;
	_readString(name, desc, 16);

	name = PacsElements.CT_SRC_PAT_DIST;
	desc = PacsElements.CT_SRC_PAT_DIST_DESC;
	_readString(name, desc, 16);

	// Series view
	name = PacsElements.CT_POSIT_REF;
	desc = PacsElements.CT_POSIT_REF_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_SLICE_LOC;
	desc = PacsElements.CT_SLICE_LOC_DESC;
	_readString(name, desc, 16);

	// Reconstruction parameters
	name = PacsElements.CT_RECON_DIAM;
	desc = PacsElements.CT_RECON_DIAM_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_CONV_KERN;
	desc = PacsElements.CT_CONV_KERN_DESC;
	_readString(name, desc, 16);
	
	// Calibration parameters
	name = PacsElements.CT_RESCAL_INTER;
	desc = PacsElements.CT_RESCAL_INTER_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_RESCAL_SLOPE;
	desc = PacsElements.CT_RESCAL_SLOPE_DESC;
	_readString(name, desc, 8);

	name = PacsElements.CT_GRAY_SCALE;
	desc = PacsElements.CT_GRAY_SCALE_DESC;
	_readString(name, desc, 4);

	// Display parameters
	name = PacsElements.CT_DFLT_LEVEL;
	desc = PacsElements.CT_DFLT_LEVEL_DESC;
	_readInt(name, desc);

	name = PacsElements.CT_DFLT_WINDOW;
	desc = PacsElements.CT_DFLT_WINDOW_DESC;
	_readInt(name, desc);

	// Max/min gray level in study
	name = PacsElements.CT_STUDY_MIN;
	desc = PacsElements.CT_STUDY_MIN_DESC;
	_readInt(name, desc);

	name = PacsElements.CT_STUDY_MAX;
	desc = PacsElements.CT_STUDY_MAX_DESC;
	_readInt(name, desc);

	// Extra filler space
	name = PacsElements.CT_FREE_TEXT;
	desc = PacsElements.CT_FREE_TEXT_DESC;
	_readString(name, desc, 320);
      }

      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode a CT subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
