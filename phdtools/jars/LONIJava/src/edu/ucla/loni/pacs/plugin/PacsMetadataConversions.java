/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.pacs.PacsElement;
import edu.ucla.loni.pacs.PacsElements;
import edu.ucla.loni.pacs.PacsSubheader;
import java.util.Enumeration;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between the PACS Header classes and
 * DOM Node representations of them.
 *
 * @version 10 May 2007
 */
public class PacsMetadataConversions
{
  /** Constructs a PACS Metadata Conversions. */
  private PacsMetadataConversions()
    {
    }

  /**
   * Converts the tree to a main PACS Subheader.
   *
   * @param root DOM Node representing the main PACS Subheader.
   *
   * @return Main PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toMainPacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsStreamMetadataFormat.PACS_HEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a main PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	// Header version
	String name = PacsElements.FILE_TYPE;
	String desc = PacsElements.FILE_TYPE_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Image format
	name = PacsElements.IMG_DIM;
	desc = PacsElements.IMG_DIM_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_ROWS;
	desc = PacsElements.IMG_ROWS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_COLS;
	desc = PacsElements.IMG_COLS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.NO_IMG_IN_FILE;
	desc = PacsElements.NO_IMG_IN_FILE_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Image storage
	name = PacsElements.PIX_BIT_ALLOCATED;
	desc = PacsElements.PIX_BIT_ALLOCATED_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.PIX_BIT_STORED;
	desc = PacsElements.PIX_BIT_STORED_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.PIX_BIT_HIGH_POS;
	desc = PacsElements.PIX_BIT_HIGH_POS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.COMPR_FLAG;
	desc = PacsElements.COMPR_FLAG_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.COMPR_VERSION;
	desc = PacsElements.COMPR_VERSION_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Voxel size
	name = PacsElements.X_PIX_SIZE;
	desc = PacsElements.X_PIX_SIZE_DESC;
        Float floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.Y_PIX_SIZE;
	desc = PacsElements.Y_PIX_SIZE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.STUDY_SLICE_THICKNESS;
	desc = PacsElements.STUDY_SLICE_THICKNESS_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.STUDY_SLICE_SPACING;
	desc = PacsElements.STUDY_SLICE_SPACING_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.FIELD_OF_VIEW;
	desc = PacsElements.FIELD_OF_VIEW_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Pointers to subheaders
	name = PacsElements.SUBHDR_BLK_SIZE;
	desc = PacsElements.SUBHDR_BLK_SIZE_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.SUBHDR_BLOCK_POSITION;
	desc = PacsElements.SUBHDR_BLOCK_POSITION_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Acquisition machine references
	name = PacsElements.RID_PATH;
	desc = PacsElements.RID_PATH_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.RID_FILENAME;
	desc = PacsElements.RID_FILENAME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.EXAM_REFERENCE_NO;
	desc = PacsElements.EXAM_REFERENCE_NO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_REFERENCE_NO;
	desc = PacsElements.STUDY_REFERENCE_NO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.SERIES_REFERENCE_NO;
	desc = PacsElements.SERIES_REFERENCE_NO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// RIS and PACS study reference ID
	name = PacsElements.RIS_STUDY_NO;
	desc = PacsElements.RIS_STUDY_NO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PACS_STUDY_NO;
	desc = PacsElements.PACS_STUDY_NO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Patient information
	name = PacsElements.PATIENT_ID;
	desc = PacsElements.PATIENT_ID_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PATIENT_NAME;
	desc = PacsElements.PATIENT_NAME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PATIENT_BIRTH_DATE;
	desc = PacsElements.PATIENT_BIRTH_DATE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PAT_SEX;
	desc = PacsElements.PAT_SEX_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PATIENT_HISTORY;
	desc = PacsElements.PATIENT_HISTORY_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PATIENT_STATUS;
	desc = PacsElements.PATIENT_STATUS_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Study information
	name = PacsElements.REASON_FOR_REQUEST;
	desc = PacsElements.REASON_FOR_REQUEST_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_DESCRIPTION;
	desc = PacsElements.STUDY_DESCRIPTION_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_SUBDESCRIPTION;
	desc = PacsElements.STUDY_SUBDESCRIPTION_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_ACQ_DATE;
	desc = PacsElements.STUDY_ACQ_DATE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_ACQ_TIME;
	desc = PacsElements.STUDY_ACQ_TIME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_RADIOLOGIST;
	desc = PacsElements.STUDY_RADIOLOGIST_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_REF_PHYSICIAN;
	desc = PacsElements.STUDY_REF_PHYSICIAN_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_MODALITY;
	desc = PacsElements.STUDY_MODALITY_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.STUDY_MISC_INFO;
	desc = PacsElements.STUDY_MISC_INFO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.PATIENT_PREPARATION;
	desc = PacsElements.PATIENT_PREPARATION_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Imaging device information
	name = PacsElements.INST_STATION_ID;
	desc = PacsElements.INST_STATION_ID_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.INST_ACQ_MANUF;
	desc = PacsElements.INST_ACQ_MANUF_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.INST_ACQ_MODEL;
	desc = PacsElements.INST_ACQ_MODEL_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.INST_ID;
	desc = PacsElements.INST_ID_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Extra IDs
	name = PacsElements.RIS_STUDY_NO_EXT;
	desc = PacsElements.RIS_STUDY_NO_EXT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.IMG_ORIG_ID_NO;
	desc = PacsElements.IMG_ORIG_ID_NO_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Extra filler space
	name = PacsElements.FREE_TEXT;
	desc = PacsElements.FREE_TEXT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Return the main PACS subheader
	return subheader;
      }

      // Unable to construct a main PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a main PACS subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to a CR PACS Subheader.
   *
   * @param root DOM Node representing the CR PACS Subheader.
   *
   * @return CR PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toCrPacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsStreamMetadataFormat.PACS_CR_SUBHEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a CR PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	// Source characteristics
	String name = PacsElements.CR_SENSITIVITY;
	String desc = PacsElements.CR_SENSITIVITY_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CR_RANGE;
        desc = PacsElements.CR_RANGE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Detector characteristics
	name = PacsElements.CR_PRE_SAMPLE_MODE;
        desc = PacsElements.CR_PRE_SAMPLE_MODE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Image pre-processing parameters
	name = PacsElements.CR_PREPROC_PARAMETERS;
        desc = PacsElements.CR_PREPROC_PARAMETERS_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Display parameters
	name = PacsElements.CR_DISP_WIN_CENTER;
        desc = PacsElements.CR_DISP_WIN_CENTER_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.CR_DISP_WIN_WIDTH;
        desc = PacsElements.CR_DISP_WIN_WIDTH_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Max/min gray level in study
	name = PacsElements.CR_STUDY_MIN;
        desc = PacsElements.CR_STUDY_MIN_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.CR_STUDY_MAX;
        desc = PacsElements.CR_STUDY_MAX_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Extra filler space
	name = PacsElements.CR_FREE_TEXT;
        desc = PacsElements.CR_FREE_TEXT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Return the CR PACS subheader
	return subheader;
      }

      // Unable to construct a CR PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a CR PACS subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to a CT PACS Subheader.
   *
   * @param root DOM Node representing the CT PACS Subheader.
   *
   * @return CT PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toCtPacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsStreamMetadataFormat.PACS_CT_SUBHEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a CT PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	// Series name
	String name = PacsElements.CT_SERIES_NAME;
	String desc = PacsElements.CT_SERIES_NAME_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Source characteristics
	name = PacsElements.CT_EXPO_TIME;
	desc = PacsElements.CT_EXPO_TIME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_EXPO_RATE;
	desc = PacsElements.CT_EXPO_RATE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_KVP;
	desc = PacsElements.CT_KVP_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_SCAN_SEQ;
	desc = PacsElements.CT_SCAN_SEQ_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Source, patient, detector geometry
	name = PacsElements.CT_PAT_ORIENT;
	desc = PacsElements.CT_PAT_ORIENT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_SRC_DET_DIST;
	desc = PacsElements.CT_SRC_DET_DIST_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_SRC_PAT_DIST;
	desc = PacsElements.CT_SRC_PAT_DIST_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Series view
	name = PacsElements.CT_POSIT_REF;
	desc = PacsElements.CT_POSIT_REF_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_SLICE_LOC;
	desc = PacsElements.CT_SLICE_LOC_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Reconstruction parameters
	name = PacsElements.CT_RECON_DIAM;
	desc = PacsElements.CT_RECON_DIAM_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_CONV_KERN;
	desc = PacsElements.CT_CONV_KERN_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));
	
	// Calibration parameters
	name = PacsElements.CT_RESCAL_INTER;
	desc = PacsElements.CT_RESCAL_INTER_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_RESCAL_SLOPE;
	desc = PacsElements.CT_RESCAL_SLOPE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.CT_GRAY_SCALE;
	desc = PacsElements.CT_GRAY_SCALE_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Display parameters
	name = PacsElements.CT_DFLT_LEVEL;
	desc = PacsElements.CT_DFLT_LEVEL_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.CT_DFLT_WINDOW;
	desc = PacsElements.CT_DFLT_WINDOW_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Max/min gray level in study
	name = PacsElements.CT_STUDY_MIN;
	desc = PacsElements.CT_STUDY_MIN_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.CT_STUDY_MAX;
	desc = PacsElements.CT_STUDY_MAX_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Extra filler space
	name = PacsElements.CT_FREE_TEXT;
	desc = PacsElements.CT_FREE_TEXT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Return the CT PACS subheader
	return subheader;
      }

      // Unable to construct a CT PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a CT PACS subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to an MR PACS Subheader.
   *
   * @param root DOM Node representing the MR PACS Subheader.
   *
   * @return MR PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toMrPacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsStreamMetadataFormat.PACS_MR_SUBHEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an MR PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	// Source, patient, detector geometry
	String name = PacsElements.MR_PAT_ORIENTATION;
	String desc = PacsElements.MR_PAT_ORIENTATION_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Sequence view
	name = PacsElements.MR_PLANE_NAME;
	desc = PacsElements.MR_PLANE_NAME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.MR_POSITION;
	desc = PacsElements.MR_POSITION_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.MR_LONGIT_REF;
	desc = PacsElements.MR_LONGIT_REF_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.MR_VERTIC_REF;
	desc = PacsElements.MR_VERTIC_REF_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.MR_START_X;
	desc = PacsElements.MR_START_X_DESC;
        Float floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_END_X;
	desc = PacsElements.MR_END_X_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_START_Y;
	desc = PacsElements.MR_START_Y_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_END_Y;
	desc = PacsElements.MR_END_Y_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_START_Z;
	desc = PacsElements.MR_START_Z_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_END_Z;
	desc = PacsElements.MR_END_Z_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_IMAGE_LOCATION;
	desc = PacsElements.MR_IMAGE_LOCATION_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// Imaging technique
	name = PacsElements.MR_PULSE_SEQ_NAME;
	desc = PacsElements.MR_PULSE_SEQ_NAME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.MR_GATING_NAME;
	desc = PacsElements.MR_GATING_NAME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Source characteristics
	name = PacsElements.MR_GAUSS;
	desc = PacsElements.MR_GAUSS_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.MR_COIL_NAME;
	desc = PacsElements.MR_COIL_NAME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Pulse sequence technique
	name = PacsElements.MR_REPETITION_RECOVERY_TIME;
	desc = PacsElements.MR_REPETITION_RECOVERY_TIME_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_SCAN_TIME;
	desc = PacsElements.MR_SCAN_TIME_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_ECHO_DELAY;
	desc = PacsElements.MR_ECHO_DELAY_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_INVERSION_TIME;
	desc = PacsElements.MR_INVERSION_TIME_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_NO_ECHOES;
	desc = PacsElements.MR_NO_ECHOES_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.MR_NO_EXCITATIONS;
	desc = PacsElements.MR_NO_EXCITATIONS_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_FLIP_ANGLE;
	desc = PacsElements.MR_FLIP_ANGLE_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.MR_TRAVERSE_TIME;
	desc = PacsElements.MR_TRAVERSE_TIME_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Display parameters
	name = PacsElements.MR_DFLT_WINDOW;
	desc = PacsElements.MR_DFLT_WINDOW_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.MR_DFLT_LEVEL;
	desc = PacsElements.MR_DFLT_LEVEL_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Max/min gray level in study
	name = PacsElements.MR_STUDY_MIN;
	desc = PacsElements.MR_STUDY_MIN_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.MR_STUDY_MAX;
	desc = PacsElements.MR_STUDY_MAX_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Extra filler space
	name = PacsElements.MR_FREE_TEXT;
	desc = PacsElements.MR_FREE_TEXT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Return the main MR subheader
	return subheader;
      }

      // Unable to construct an MR PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an MR PACS subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to an XA PACS Subheader.
   *
   * @param root DOM Node representing the XA PACS Subheader.
   *
   * @return XA PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toXaPacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsStreamMetadataFormat.PACS_XA_SUBHEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an XA PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	// XA Frame Rate
	String name = PacsElements.XA_IMG_FPS;
	String desc = PacsElements.XA_IMG_FPS_DESC;
        Float floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_IMG_FPS_VARIABLE;
	desc = PacsElements.XA_IMG_FPS_VARIABLE_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.XA_IMG_PLANE_TYPE;
	desc = PacsElements.XA_IMG_PLANE_TYPE_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.XA_TABLE_MOTION;
	desc = PacsElements.XA_TABLE_MOTION_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// XA positioner info
	name = PacsElements.XA_POSITIONER_MOTION;
	desc = PacsElements.XA_POSITIONER_MOTION_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.XA_PRIM_ANGLE;
	desc = PacsElements.XA_PRIM_ANGLE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_SEC_ANGLE;
	desc = PacsElements.XA_SEC_ANGLE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_FOV_SHAPE;
	desc = PacsElements.XA_FOV_SHAPE_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.XA_SOURCE_TO_PATIENT;
	desc = PacsElements.XA_SOURCE_TO_PATIENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_SOURCE_TO_DETECTOR;
	desc = PacsElements.XA_SOURCE_TO_DETECTOR_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// X-ray information
	name = PacsElements.XA_EXPO_KVP;
	desc = PacsElements.XA_EXPO_KVP_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_EXPO_CURRENT;
	desc = PacsElements.XA_EXPO_CURRENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_EXPO_TIME;
	desc = PacsElements.XA_EXPO_TIME_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_EXPO_RATE;
	desc = PacsElements.XA_EXPO_RATE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_FOCAL_SPOT;
	desc = PacsElements.XA_FOCAL_SPOT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// Special information for shortened (cut) sequences
	name = PacsElements.XA_CUT_FLAG;
	desc = PacsElements.XA_CUT_FLAG_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Curve presence flag
	name = PacsElements.XA_NUM_CURVES_ENCLOSED;
	desc = PacsElements.XA_NUM_CURVES_ENCLOSED_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Extra filler space
	name = PacsElements.XA_FREE_TEXT;
	desc = PacsElements.XA_FREE_TEXT_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	// Return the XA subheader
	return subheader;
      }

      // Unable to construct an XA PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an XA PACS subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to an Image PACS Subheader.
   *
   * @param root DOM Node representing the Image PACS Subheader.
   *
   * @return Image PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toImagePacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsImageMetadataFormat.PACS_IMG_SUBHEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an Image PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	String name = PacsElements.IMG_NUMBER;
	String desc = PacsElements.IMG_NUMBER_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_ROWS;
	desc = PacsElements.IMG_ROWS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_COLS;
	desc = PacsElements.IMG_COLS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.X_PIX_SIZE;
	desc = PacsElements.X_PIX_SIZE_DESC;
        Float floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.Y_PIX_SIZE;
	desc = PacsElements.Y_PIX_SIZE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.FIELD_OF_VIEW;
	desc = PacsElements.FIELD_OF_VIEW_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.IMG_MIN;
	desc = PacsElements.IMG_MIN_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_MAX;
	desc = PacsElements.IMG_MAX_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.RECON_ALGORITHM;
	desc = PacsElements.RECON_ALGORITHM_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.THICK;
	desc = PacsElements.THICK_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// Return the Image subheader
	return subheader;
      }

      // Unable to construct an Image PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an Image PACS " +
	             "subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the tree to an Image Appendix PACS Subheader.
   *
   * @param root DOM Node representing the Image Appendix PACS Subheader.
   *
   * @return Image Appendix PACS Subheader constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static PacsSubheader toImageAppendixPacsSubheader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = PacsImageMetadataFormat.PACS_IMGX_SUBHEADER_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an ImageX PACS subheader from the Node attributes
      try {
	PacsSubheader subheader = new PacsSubheader();
	NamedNodeMap atts = root.getAttributes();

	String name = PacsElements.VERSION;
	String desc = PacsElements.VERSION_DESC;
	String stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.WINDOW_LEVEL;
	desc = PacsElements.WINDOW_LEVEL_DESC;
	Integer intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.WINDOW_WIDTH;
	desc = PacsElements.WINDOW_WIDTH_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.MR_REPETITION_TIME;
	desc = PacsElements.MR_REPETITION_TIME_DESC;
        Float floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.MR_ECHO_TIME;
	desc = PacsElements.MR_ECHO_TIME_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION;
	desc = PacsElements.PAT_ORIENTATION_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.IMG_NUMBER;
	desc = PacsElements.IMG_NUMBER_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_ROWS;
	desc = PacsElements.IMG_ROWS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_COLS;
	desc = PacsElements.IMG_COLS_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.X_PIX_SIZE;
	desc = PacsElements.X_PIX_SIZE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.Y_PIX_SIZE;
	desc = PacsElements.Y_PIX_SIZE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.FIELD_OF_VIEW;
	desc = PacsElements.FIELD_OF_VIEW_DESC;
        stringValue = atts.getNamedItem(name).getNodeValue();
	subheader.addPacsElement(new PacsElement(name, desc, stringValue));

	name = PacsElements.IMG_MIN;
	desc = PacsElements.IMG_MIN_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.IMG_MAX;
	desc = PacsElements.IMG_MAX_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.RECON_ALGORITHM;
	desc = PacsElements.RECON_ALGORITHM_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.THICK;
	desc = PacsElements.THICK_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));
 
	// 3D rendering fields - VI03
	name = PacsElements.IMG_POS_PAT_X;
	desc = PacsElements.IMG_POS_PAT_X_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.IMG_POS_PAT_Y;
	desc = PacsElements.IMG_POS_PAT_Y_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.IMG_POS_PAT_Z;
	desc = PacsElements.IMG_POS_PAT_Z_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION_X1;
	desc = PacsElements.PAT_ORIENTATION_X1_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION_Y1;
	desc = PacsElements.PAT_ORIENTATION_Y1_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION_Z1;
	desc = PacsElements.PAT_ORIENTATION_Z1_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION_X2;
	desc = PacsElements.PAT_ORIENTATION_X2_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION_Y2;
	desc = PacsElements.PAT_ORIENTATION_Y2_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.PAT_ORIENTATION_Z2;
	desc = PacsElements.PAT_ORIENTATION_Z2_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// XA/RF compatibility fields - VI04
	name = PacsElements.XA_FRAME_NUMBER;
	desc = PacsElements.XA_FRAME_NUMBER_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	name = PacsElements.XA_ORIG_FRAME_NO;
	desc = PacsElements.XA_ORIG_FRAME_NO_DESC;
        intValue = new Integer(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, intValue));

	// Frame rate info - VI04
	name = PacsElements.XA_FRAME_INTERVAL;
	desc = PacsElements.XA_FRAME_INTERVAL_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// XA positioner info - VI04
	name = PacsElements.XA_PRIM_ANGLE_INCREMENT;
	desc = PacsElements.XA_PRIM_ANGLE_INCREMENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_SEC_ANGLE_INCREMENT;
	desc = PacsElements.XA_SEC_ANGLE_INCREMENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_CURRENT_PRIM_ANGLE;
	desc = PacsElements.XA_CURRENT_PRIM_ANGLE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_CURRENT_SEC_ANGLE;
	desc = PacsElements.XA_CURRENT_SEC_ANGLE_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// XA table info - VI04
	name = PacsElements.XA_TABLE_VERTIC_INCREMENT;
	desc = PacsElements.XA_TABLE_VERTIC_INCREMENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_TABLE_LATERAL_INCREMENT;
	desc = PacsElements.XA_TABLE_LATERAL_INCREMENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	name = PacsElements.XA_TABLE_LONGIT_INCREMENT;
	desc = PacsElements.XA_TABLE_LONGIT_INCREMENT_DESC;
        floatValue = new Float(atts.getNamedItem(name).getNodeValue());
	subheader.addPacsElement(new PacsElement(name, desc, floatValue));

	// Return the Image Appendix subheader
	return subheader;
      }

      // Unable to construct an Image Appendix PACS subheader
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an Image Appendix " +
	             "PACS subheader.";
	throw new IIOInvalidTreeException(msg, e, root);
      }
    }

  /**
   * Converts the main and modality PACS Subheaders to a tree of stream
   * metadata.
   *
   * @param mainHeader PACS main Subheader to convert.
   * @param modalityHeader PACS modality Subheader to convert.
   *
   * @return DOM Node representing the stream metadata.
   */
  public static Node toStreamTree(PacsSubheader mainHeader,
				  PacsSubheader modalityHeader)
    {
      String rootName = PacsStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      if (mainHeader != null && modalityHeader != null) {

	// Add the main Subheader
	String name = PacsStreamMetadataFormat.PACS_HEADER_NAME;
	Node node = _toTree(name, mainHeader);
	rootNode.appendChild(node);

	// Determine the name for the modality Subheader
	Enumeration enun = modalityHeader.getPacsElements();
	PacsElement firstElement = (PacsElement)enun.nextElement();
	if ( firstElement.getID().startsWith("cr") ) {
	  name = PacsStreamMetadataFormat.PACS_CR_SUBHEADER_NAME;
	}
	if ( firstElement.getID().startsWith("ct") ) {
	  name = PacsStreamMetadataFormat.PACS_CT_SUBHEADER_NAME;
	}
	if ( firstElement.getID().startsWith("mr") ) {
	  name = PacsStreamMetadataFormat.PACS_MR_SUBHEADER_NAME;
	}
	if ( firstElement.getID().startsWith("xa") ) {
	  name = PacsStreamMetadataFormat.PACS_XA_SUBHEADER_NAME;
	}

	// Add the modality Subheader
	node = _toTree(name, modalityHeader);
	rootNode.appendChild(node);
      }

      // Return the root Node
      return rootNode;
    }

  /**
   * Converts the Image and Image Appendix PACS Subheaders to a tree of image
   * metadata.
   *
   * @param imageHeader PACS image Subheader containing metadata.
   * @param imageAppendixHeader PACS image appendix Subheader containing
   *                            metadata.
   *
   * @return DOM Node representing the image metadata.
   */
  public static Node toImageTree(PacsSubheader imageHeader,
				 PacsSubheader imageAppendixHeader)
    {
      String rootName = PacsImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      if (imageHeader != null && imageAppendixHeader != null) {

	// Add the Image Subheader
	String name = PacsImageMetadataFormat.PACS_IMG_SUBHEADER_NAME;
	Node node = _toTree(name, imageHeader);
	rootNode.appendChild(node);

	// Add the Image Appendix Subheader
	name = PacsImageMetadataFormat.PACS_IMGX_SUBHEADER_NAME;
	node = _toTree(name, imageAppendixHeader);
	rootNode.appendChild(node);
      }

      // Return the root Node
      return rootNode;
    }

  /**
   * Converts the PACS Subheader to a tree.
   *
   * @param treeName Name of the tree.
   * @param pacsSubheader PACS Subheader from which to construct the tree.
   *
   * @return DOM Node representing the PACS Subheader.
   */
  private static Node _toTree(String treeName, PacsSubheader pacsSubheader)
    {
      // Create the root Node
      IIOMetadataNode rootNode = new IIOMetadataNode(treeName);

      // Add each PACS Element as an attribute
      Enumeration enun = pacsSubheader.getPacsElements();
      while ( enun.hasMoreElements() ) {
	PacsElement pacsElement = (PacsElement)enun.nextElement();

	// Set the attribute
	String elementName = pacsElement.getID();
	String elementValue = pacsElement.getValue().toString();
	rootNode.setAttribute(elementName, elementValue);
      }

      // Return the root Node
      return rootNode;
    }
}
