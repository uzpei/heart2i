/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.pacs.PacsHeader;
import edu.ucla.loni.pacs.PacsSubheader;
import edu.ucla.loni.pacs.decoder.PacsHeaderDecoder;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for parsing and decoding UCLA PACS images.
 *
 * @version 15 April 2005
 */
public class PacsImageReader extends ImageReader
{
  /** UCLA PACS Header decoded from the input source. */
  private PacsHeader _pacsHeader;

  /**
   * Constructs a PACS Image Reader.
   *
   * @param originatingProvider The Image Reader SPI that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not a
   *                                  PACS Image Reader Spi.
   */
  public PacsImageReader(ImageReaderSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be a PACS Image Reader Spi
      if ( !(originatingProvider instanceof PacsImageReaderSpi) ) {
	String msg = "Originating provider must be a PACS Image Reader SPI.";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Sets the input source to use to the given ImageInputStream or other
   * Object.  The input source must be set before any of the query or read
   * methods are used.  If the input is null, any currently set input source
   * will be removed.
   *
   * @param input The ImageInputStream or other Object to use for future
   *              decoding.
   * @param seekForwardOnly If true, images and metadata may only be read in
   *                        ascending order from this input source.
   * @param ignoreMetadata If true, metadata may be ignored during reads.
   *
   * @throws IllegalArgumentException If the input is not an instance of one of
   *                                  the classes returned by the originating
   *                                  service provider's getInputTypes method,
   *                                  or is not an ImageInputStream.
   * @throws IllegalStateException If the input source is an Input Stream that
   *                               doesn't support marking and seekForwardOnly
   *                               is false.
   */
  public void setInput(Object input, boolean seekForwardOnly,
		       boolean ignoreMetadata)
    {
      super.setInput(input, seekForwardOnly, ignoreMetadata);

      // Remove any decoded PACS Header
      _pacsHeader = null;
    }

  /**
   * Returns the number of images, not including thumbnails, available from the
   * current input source.
   * 
   * @param allowSearch If true, the true number of images will be returned
   *                    even if a search is required.  If false, the reader may
   *                    return -1 without performing the search.
   *
   * @return The number of images or -1 if allowSearch is false and a search
   *         would be required.
   *
   * @throws IllegalStateException If the input source has not been set.
   * @throws IOException If an error occurs reading the information from the
   *                     input source.
   */
  public int getNumImages(boolean allowSearch) throws IOException
    {
      PacsStreamMetadata metadata = _getStreamMetadata();
      int numberOfImages = metadata.getNumberOfImages();

      // Unable to determine the number of images
      if (numberOfImages == -1) {
	String msg = "Unable to determine the number of images.";
	throw new IOException(msg);
      }

      return numberOfImages;
    }

  /**
   * Returns the width in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The width of the image in pixels.
   *
   * @throws IOException If an error occurs reading the width information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getWidth(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the image width
      PacsStreamMetadata metadata = _getStreamMetadata();
      int width = metadata.getImageWidth();

      // Unable to determine the image width
      if (width == -1) {
	String msg = "Unable to determine the image width.";
	throw new IOException(msg);
      }

      return width;
    }

  /**
   * Returns the height in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The height of the image in pixels.
   *
   * @throws IOException If an error occurs reading the height information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getHeight(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the image height
      PacsStreamMetadata metadata = _getStreamMetadata();
      int height = metadata.getImageHeight();

      // Unable to determine the image height
      if (height == -1) {
	String msg = "Unable to determine the image height.";
	throw new IOException(msg);
      }

      return height;
    }

  /**
   * Returns an Iterator containing possible image types to which the given
   * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
   * legal image type will be returned.
   *
   * @param imageIndex The index of the image to be retrieved.
   *
   * @return An Iterator containing at least one ImageTypeSpecifier
   *         representing suggested image types for decoding the current given
   *         image.
   *
   * @throws IOException If an error occurs reading the format information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public Iterator getImageTypes(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the image parameters
      PacsStreamMetadata metadata = _getStreamMetadata();
      int bitsPerPixel = metadata.getBitsPerPixel();
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Check if the image data type is supported
      if (bitsPerPixel == -1) {
	String msg = "Unable to recognize the image type.";
	throw new IOException(msg);
      }

      // Return the Image Type Specifier
      ArrayList list = new ArrayList(1);
      list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
      return list.iterator();
    }

  /**
   * Returns an IIOMetadata object representing the metadata associated with
   * the input source as a whole (i.e., not associated with any particular
   * image), or null if the reader does not support reading metadata, is set
   * to ignore metadata, or if no metadata is available.
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   */
  public IIOMetadata getStreamMetadata() throws IOException
    {
      return _getStreamMetadata();
    }

  /**
   * Returns an IIOMetadata object containing metadata associated with the
   * given image, or null if the reader does not support reading metadata, is
   * set to ignore metadata, or if no metadata is available.
   *
   * @param imageIndex Index of the image whose metadata is to be retrieved. 
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the indexed Image and Image Appendix Subheaders
      PacsHeader pacsHeader = _getPacsHeader();
      PacsSubheader imgHdr = pacsHeader.getImageSubheader(imageIndex);
      PacsSubheader appHdr = pacsHeader.getImageAppendixSubheader(imageIndex);

      // Return a new PACS Image Metadata
      return new PacsImageMetadata(imgHdr, appHdr);
    }

  /**
   * Reads the image indexed by imageIndex and returns it as a complete
   * BufferedImage.  The supplied ImageReadParam is ignored.
   *
   * @param imageIndex The index of the image to be retrieved.
   * @param param An ImageReadParam used to control the reading process, or
   *              null.
   *
   * @return The desired portion of the image as a BufferedImage.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public BufferedImage read(int imageIndex, ImageReadParam param)
    throws IOException
    {
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Update the Listeners
      processImageStarted(imageIndex);

      // Compute the size of the image in bytes
      ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
      int dataType = spec.getSampleModel().getDataType();
      int imageSize = width*height;
      if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }

      // Seek to the indexed image
      int offset = 1024 + 512 + 512*getNumImages(false) + imageIndex*imageSize;
      ImageInputStream imageStream = _getInputStream();
      imageStream.seek(offset);

      // Return the raw image data if required
      if ( Utilities.returnRawBytes(param) ) {
	BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	imageStream.read( ((DataBufferByte)db).getData() );
	return bufferedImage;
      }

      // Create a Buffered Image for the image data
      Iterator iter = getImageTypes(0);
      BufferedImage bufferedImage = getDestination(null, iter, width, height);
      DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

      // Read the image data into an array
      byte[] srcBuffer = new byte[imageSize];
      imageStream.readFully(srcBuffer);

      // 8-bit gray image pixel data
      WritableRaster dstRaster = bufferedImage.getRaster();
      if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
      }

      // 16-bit gray image pixel data
      else {
	PacsStreamMetadata metadata = _getStreamMetadata();
	String modality = metadata.getModality();
	String manufacturer = metadata.getManufacturer();

	// Determine if a Hounsfield correction needs to be added
	int intercept = 0;
	if ( modality.equals("CT") && manufacturer.startsWith("Picker") ) {
	  intercept = 1024;
	}

	// Create the image
	int bitsPerPixel = metadata.getBitsPerPixel();
	bufferedImage = Utilities.getGrayUshortImage(srcBuffer, dstRaster,
						     bitsPerPixel, false,
						     false, 1, intercept);
      }

      // Update the Listeners
      if ( abortRequested() ) { processReadAborted(); }
      else { processImageComplete(); }

      // Return the requested image
      return bufferedImage;
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageReader.  Otherwise, the reader may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the input stream
      try { if (input != null) { ((ImageInputStream)input).close(); } }
      catch (Exception e) {}

      input = null;
    }

  /**
   * Checks whether or not the specified image index is valid.
   *
   * @param imageIndex The index of the image to check.
   *
   * @throws IOException If an error occurs reading the information from the
   *                     input source.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  private void _checkImageIndex(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      if ( imageIndex >= getNumImages(false) ) {
	String msg = "Image index of " + imageIndex + " >= the total number " +
	             "of images (" + getNumImages(false) + ")";
	throw new IndexOutOfBoundsException(msg);
      }
    }

  /**
   * Gets the input stream.
   *
   * @return Image Input Stream to read data from.
   *
   * @throws IllegalStateException If the input has not been set.
   */
  private ImageInputStream _getInputStream()
    {
      // No input has been set
      if (input == null) {
	String msg = "No input has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Input Stream
      return (ImageInputStream)input;
    }

  /**
   * Gets the PACS Stream Metadata.
   *
   * @return PACS Stream Metadata.
   * 
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   */
  private PacsStreamMetadata _getStreamMetadata() throws IOException
    {
      PacsHeader pacsHeader = _getPacsHeader();

      // Construct and return the PACS Stream Metadata
      PacsSubheader mainSubheader = pacsHeader.getMainSubheader();
      PacsSubheader modalityHeader = pacsHeader.getModalitySubheader();

      return new PacsStreamMetadata(mainSubheader, modalityHeader);
    }

  /**
   * Reads the PACS Header from the input source.  If the PACS Header has
   * already been decoded, no additional read is performed.
   *
   * @return PACS Header decoded from the input source.
   * 
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   */
  private PacsHeader _getPacsHeader() throws IOException
    {
      // Decode the PACS Header if needed
      if (_pacsHeader == null) {
	ImageInputStream imageStream = _getInputStream();
	PacsHeaderDecoder decoder = new PacsHeaderDecoder(imageStream);
	_pacsHeader = decoder.getPacsHeader();
      }

      // Return the PACS Header
      return _pacsHeader;
    }
}
