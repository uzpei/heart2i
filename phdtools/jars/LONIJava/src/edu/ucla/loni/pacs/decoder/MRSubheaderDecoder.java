/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes an MR PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public class MRSubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs an MR Subheader Decoder which uses the specified Image Input
   * Stream to decode the MR PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public MRSubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {

	// Source, patient, detector geometry
	String name = PacsElements.MR_PAT_ORIENTATION;
	String desc = PacsElements.MR_PAT_ORIENTATION_DESC;
	_readString(name, desc, 32);

	// Sequence view
	name = PacsElements.MR_PLANE_NAME;
	desc = PacsElements.MR_PLANE_NAME_DESC;
	_readString(name, desc, 16);

	name = PacsElements.MR_POSITION;
	desc = PacsElements.MR_POSITION_DESC;
	_readString(name, desc, 16);

	name = PacsElements.MR_LONGIT_REF;
	desc = PacsElements.MR_LONGIT_REF_DESC;
	_readString(name, desc, 32);

	name = PacsElements.MR_VERTIC_REF;
	desc = PacsElements.MR_VERTIC_REF_DESC;
	_readString(name, desc, 32);

	name = PacsElements.MR_START_X;
	desc = PacsElements.MR_START_X_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_END_X;
	desc = PacsElements.MR_END_X_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_START_Y;
	desc = PacsElements.MR_START_Y_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_END_Y;
	desc = PacsElements.MR_END_Y_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_START_Z;
	desc = PacsElements.MR_START_Z_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_END_Z;
	desc = PacsElements.MR_END_Z_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_IMAGE_LOCATION;
	desc = PacsElements.MR_IMAGE_LOCATION_DESC;
	_readFloat(name, desc);

	// Imaging technique
	name = PacsElements.MR_PULSE_SEQ_NAME;
	desc = PacsElements.MR_PULSE_SEQ_NAME_DESC;
	_readString(name, desc, 24);

	name = PacsElements.MR_GATING_NAME;
	desc = PacsElements.MR_GATING_NAME_DESC;
	_readString(name, desc, 8);

	// Source characteristics
	name = PacsElements.MR_GAUSS;
	desc = PacsElements.MR_GAUSS_DESC;
	_readInt(name, desc);

	name = PacsElements.MR_COIL_NAME;
	desc = PacsElements.MR_COIL_NAME_DESC;
	_readString(name, desc, 8);

	// Pulse sequence technique
	name = PacsElements.MR_REPETITION_RECOVERY_TIME;
	desc = PacsElements.MR_REPETITION_RECOVERY_TIME_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_SCAN_TIME;
	desc = PacsElements.MR_SCAN_TIME_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_ECHO_DELAY;
	desc = PacsElements.MR_ECHO_DELAY_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_INVERSION_TIME;
	desc = PacsElements.MR_INVERSION_TIME_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_NO_ECHOES;
	desc = PacsElements.MR_NO_ECHOES_DESC;
	_readInt(name, desc);

	name = PacsElements.MR_NO_EXCITATIONS;
	desc = PacsElements.MR_NO_EXCITATIONS_DESC;
	_readFloat(name, desc);

	name = PacsElements.MR_FLIP_ANGLE;
	desc = PacsElements.MR_FLIP_ANGLE_DESC;
	_readInt(name, desc);

	name = PacsElements.MR_TRAVERSE_TIME;
	desc = PacsElements.MR_TRAVERSE_TIME_DESC;
	_readString(name, desc, 4);

	// Display parameters
	name = PacsElements.MR_DFLT_WINDOW;
	desc = PacsElements.MR_DFLT_WINDOW_DESC;
	_readInt(name, desc);

	name = PacsElements.MR_DFLT_LEVEL;
	desc = PacsElements.MR_DFLT_LEVEL_DESC;
	_readInt(name, desc);

	// Max/min gray level in study
	name = PacsElements.MR_STUDY_MIN;
	desc = PacsElements.MR_STUDY_MIN_DESC;
	_readInt(name, desc);

	name = PacsElements.MR_STUDY_MAX;
	desc = PacsElements.MR_STUDY_MAX_DESC;
	_readInt(name, desc);

	// Extra filler space
	name = PacsElements.MR_FREE_TEXT;
	desc = PacsElements.MR_FREE_TEXT_DESC;
	_readString(name, desc, 264);
      }

      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode an MR subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
