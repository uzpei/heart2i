/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes an XA PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public class XASubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs an XA Subheader Decoder which uses the specified Image Input
   * Stream to decode the XA PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public XASubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {

	// XA Frame Rate
	String name = PacsElements.XA_IMG_FPS;
	String desc = PacsElements.XA_IMG_FPS_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_IMG_FPS_VARIABLE;
	desc = PacsElements.XA_IMG_FPS_VARIABLE_DESC;
	_readInt(name, desc);

	name = PacsElements.XA_IMG_PLANE_TYPE;
	desc = PacsElements.XA_IMG_PLANE_TYPE_DESC;
	_readInt(name, desc);

	name = PacsElements.XA_TABLE_MOTION;
	desc = PacsElements.XA_TABLE_MOTION_DESC;
	_readInt(name, desc);

	// XA positioner info
	name = PacsElements.XA_POSITIONER_MOTION;
	desc = PacsElements.XA_POSITIONER_MOTION_DESC;
	_readInt(name, desc);

	name = PacsElements.XA_PRIM_ANGLE;
	desc = PacsElements.XA_PRIM_ANGLE_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_SEC_ANGLE;
	desc = PacsElements.XA_SEC_ANGLE_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_FOV_SHAPE;
	desc = PacsElements.XA_FOV_SHAPE_DESC;
	_readString(name, desc, 10);

	name = PacsElements.XA_SOURCE_TO_PATIENT;
	desc = PacsElements.XA_SOURCE_TO_PATIENT_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_SOURCE_TO_DETECTOR;
	desc = PacsElements.XA_SOURCE_TO_DETECTOR_DESC;
	_readFloat(name, desc);

	// X-ray information
	name = PacsElements.XA_EXPO_KVP;
	desc = PacsElements.XA_EXPO_KVP_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_EXPO_CURRENT;
	desc = PacsElements.XA_EXPO_CURRENT_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_EXPO_TIME;
	desc = PacsElements.XA_EXPO_TIME_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_EXPO_RATE;
	desc = PacsElements.XA_EXPO_RATE_DESC;
	_readFloat(name, desc);

	name = PacsElements.XA_FOCAL_SPOT;
	desc = PacsElements.XA_FOCAL_SPOT_DESC;
	_readFloat(name, desc);

	// Special information for shortened (cut) sequences
	name = PacsElements.XA_CUT_FLAG;
	desc = PacsElements.XA_CUT_FLAG_DESC;
	_readInt(name, desc);

	// Curve presence flag
	name = PacsElements.XA_NUM_CURVES_ENCLOSED;
	desc = PacsElements.XA_NUM_CURVES_ENCLOSED_DESC;
	_readInt(name, desc);

	// Extra filler space
	name = PacsElements.XA_FREE_TEXT;
	desc = PacsElements.XA_FREE_TEXT_DESC;
	_readString(name, desc, 436);
      }

      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode an XA subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
