/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes an Image PacsSubheader from a DataInputStream.
 *
 * @version 6 April 2004
 */
public class ImageSubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs an Image Subheade rDecoder which uses the specified
   * Image Input Stream to decode the Image PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public ImageSubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {
	String name = PacsElements.IMG_NUMBER;
	String desc = PacsElements.IMG_NUMBER_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_ROWS;
	desc = PacsElements.IMG_ROWS_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_COLS;
	desc = PacsElements.IMG_COLS_DESC;
	_readInt(name, desc);

	name = PacsElements.X_PIX_SIZE;
	desc = PacsElements.X_PIX_SIZE_DESC;
	_readFloat(name, desc);

	name = PacsElements.Y_PIX_SIZE;
	desc = PacsElements.Y_PIX_SIZE_DESC;
	_readFloat(name, desc);

	name = PacsElements.FIELD_OF_VIEW;
	desc = PacsElements.FIELD_OF_VIEW_DESC;
	_readString(name, desc, 16);

	name = PacsElements.IMG_MIN;
	desc = PacsElements.IMG_MIN_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_MAX;
	desc = PacsElements.IMG_MAX_DESC;
	_readInt(name, desc);

	name = PacsElements.RECON_ALGORITHM;
	desc = PacsElements.RECON_ALGORITHM_DESC;
	_readInt(name, desc);

	name = PacsElements.THICK;
	desc = PacsElements.THICK_DESC;
	_readFloat(name, desc);
      }
      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode an image " +
					  "subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
