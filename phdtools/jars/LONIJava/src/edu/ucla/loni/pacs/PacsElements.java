/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs;

/**
 * Interface for the names and descriptions of UCLA PACS data elements.
 *
 * @version 31 March 2004
 */
public interface PacsElements
{
  /** ID of the header version. */
  public static final String FILE_TYPE = "file_type";

  /** Description of the ID of the header version. */
  public static final String FILE_TYPE_DESC = "Header Version";

  /** ID of the image dimension. */
  public static final String IMG_DIM = "img_dim";

  /** Description of the ID of the image dimension. */
  public static final String IMG_DIM_DESC = "Image Dimension";

  /** ID of the number of rows. */
  public static final String IMG_ROWS = "img_rows";

  /** Description of the ID of the number of rows. */
  public static final String IMG_ROWS_DESC = "Number of Rows";

  /** ID of the number of columns. */
  public static final String IMG_COLS = "img_cols";

  /** Description of the ID of the number of columns. */
  public static final String IMG_COLS_DESC = "Number of Columns";

  /** ID of the number of images. */
  public static final String NO_IMG_IN_FILE = "no_img_in_file";

  /** Description of the ID of the number of images. */
  public static final String NO_IMG_IN_FILE_DESC = "Number of Images";

  /** ID of the pixel bits allocated. */
  public static final String PIX_BIT_ALLOCATED = "pix_bit_allocated";

  /** Description of the ID of the pixel bits allocated. */
  public static final String PIX_BIT_ALLOCATED_DESC =
  "Bits Allocated for Each Pixel";

  /** ID of the pixel bits stored. */
  public static final String PIX_BIT_STORED = "pix_bit_stored";

  /** Description of the ID of the pixel bits stored. */
  public static final String PIX_BIT_STORED_DESC =
  "Bits Stored for Each Pixel";

  /** ID of the highest bit position. */
  public static final String PIX_BIT_HIGH_POS = "pix_bit_high_pos";

  /** Description of the ID of the highest bit position. */
  public static final String PIX_BIT_HIGH_POS_DESC = "Highest Bit Position";

  /** ID of the compression flag. */
  public static final String COMPR_FLAG = "compr_flag";

  /** Description of the ID of the compression flag. */
  public static final String COMPR_FLAG_DESC = "Compression Flag";

  /** ID of the compression version. */
  public static final String COMPR_VERSION = "compr_version";

  /** Description of the ID of the compression version. */
  public static final String COMPR_VERSION_DESC = "Compression Version";

  /** ID of the X pixel size. */
  public static final String X_PIX_SIZE = "x_pix_size";

  /** Description of the ID of the X pixel size. */
  public static final String X_PIX_SIZE_DESC = "Size of Pixel Along X, in mm";

  /** ID of the Y pixel size. */
  public static final String Y_PIX_SIZE = "y_pix_size";

  /** Description of the ID of the Y pixel size. */
  public static final String Y_PIX_SIZE_DESC = "Size of Pixel Along Y, in mm";

  /** ID of the study slice thickness. */
  public static final String STUDY_SLICE_THICKNESS = "study_slice_thickness";

  /** Description of the ID of the study slice thickness. */
  public static final String STUDY_SLICE_THICKNESS_DESC =
  "Study Slice Thickness, in mm";

  /** ID of the study slice spacing. */
  public static final String STUDY_SLICE_SPACING = "study_slice_spacing";

  /** Description of the ID of the . */
  public static final String STUDY_SLICE_SPACING_DESC =
  "Study Slice Spacing, in mm";

  /** ID of the field of view. */
  public static final String FIELD_OF_VIEW = "field_of_view";

  /** Description of the ID of the field of view. */
  public static final String FIELD_OF_VIEW_DESC = "Field of View, X mm x Y mm";

  /** ID of the subheader block size. */
  public static final String SUBHDR_BLK_SIZE = "subhdr_blk_size";

  /** Description of the ID of the subheader block size. */
  public static final String SUBHDR_BLK_SIZE_DESC = "Subheader Block Size";

  /** ID of the subheader block position. */
  public static final String SUBHDR_BLOCK_POSITION = "subhdr_blk_position";

  /** Description of the ID of the subheader block position. */
  public static final String SUBHDR_BLOCK_POSITION_DESC =
  "Subheader Block Position";

  /** ID of the path name on the capture computer. */
  public static final String RID_PATH = "rid_path";

  /** Description of the ID of the path name on the capture computer. */
  public static final String RID_PATH_DESC = "Path Name on Capture Computer";

  /** ID of the file name on the acquisition computer. */
  public static final String RID_FILENAME = "rid_filename";

  /** Description of the ID of the file name on the acquisition computer. */
  public static final String RID_FILENAME_DESC =
  "File Name on Acquisition Machine";

  /** ID of the exam reference number. */
  public static final String EXAM_REFERENCE_NO = "exam_reference_no";

  /** Description of the ID of the exam reference number. */
  public static final String EXAM_REFERENCE_NO_DESC =
  "Exam Reference Number Used by RID DBMS";

  /** ID of the study reference number. */
  public static final String STUDY_REFERENCE_NO = "study_reference_no";

  /** Description of the ID of the study reference number. */
  public static final String STUDY_REFERENCE_NO_DESC =
  "Study Reference Number Used by RID DBMS";

  /** ID of the series reference number. */
  public static final String SERIES_REFERENCE_NO = "series_reference_no";

  /** Description of the ID of the series reference number. */
  public static final String SERIES_REFERENCE_NO_DESC =
  "Series Reference Number Used by RID DBMS";

  /** ID of the RIS study number. */
  public static final String RIS_STUDY_NO = "RIS_study_no";

  /** Description of the ID of the RIS study number. */
  public static final String RIS_STUDY_NO_DESC =
  "Study Reference ID used by RIS DBMS";

  /** ID of the PACS study number. */
  public static final String PACS_STUDY_NO = "PACS_study_no";

  /** Description of the ID of the PACS study number. */
  public static final String PACS_STUDY_NO_DESC =
  "Patient Study Number Referenced by PACS";

  /** ID of the patient id. */
  public static final String PATIENT_ID = "pat_id";

  /** Description of the ID of the patient id. */
  public static final String PATIENT_ID_DESC = "Patient ID";

  /** ID of the patient name. */
  public static final String PATIENT_NAME = "pat_name";

  /** Description of the ID of the patient name. */
  public static final String PATIENT_NAME_DESC = "Patient Name";

  /** ID of the patient birth date. */
  public static final String PATIENT_BIRTH_DATE = "pat_birth_date";

  /** Description of the ID of the patient birth date. */
  public static final String PATIENT_BIRTH_DATE_DESC = "Patient Birth Date";

  /** ID of the patient sex. */
  public static final String PAT_SEX = "pat_sex";

  /** Description of the ID of the patient sex. */
  public static final String PAT_SEX_DESC = "Patient Sex";

  /** ID of the patient history. */
  public static final String PATIENT_HISTORY = "pat_history";

  /** Description of the ID of the patient history. */
  public static final String PATIENT_HISTORY_DESC = "Patient History";

  /** ID of the patient status. */
  public static final String PATIENT_STATUS = "pat_status";

  /** Description of the ID of the patient status. */
  public static final String PATIENT_STATUS_DESC = "Patient Status";

  /** ID of the reason for request. */
  public static final String REASON_FOR_REQUEST = "reason_for_request";

  /** Description of the ID of the reason for request. */
  public static final String REASON_FOR_REQUEST_DESC = "Reason for Request";

  /** ID of the study description. */
  public static final String STUDY_DESCRIPTION = "study_description";

  /** Description of the ID of the study description. */
  public static final String STUDY_DESCRIPTION_DESC = "Study Description";

  /** ID of the study subdescription. */
  public static final String STUDY_SUBDESCRIPTION = "study_subdescription";

  /** Description of the ID of the study subdescription. */
  public static final String STUDY_SUBDESCRIPTION_DESC =
  "Study Subdescription";

  /** ID of the study acquisition date. */
  public static final String STUDY_ACQ_DATE = "study_acq_date";

  /** Description of the ID of the study acquisition date. */
  public static final String STUDY_ACQ_DATE_DESC = "Study Acquisition Date";

  /** ID of the study acquisition_time. */
  public static final String STUDY_ACQ_TIME = "study_acq_time";

  /** Description of the ID of the study acquisition_time. */
  public static final String STUDY_ACQ_TIME_DESC = "Study Acquisition Time";

  /** ID of the study radiologist. */
  public static final String STUDY_RADIOLOGIST = "study_radiologist";

  /** Description of the ID of the study radiologist. */
  public static final String STUDY_RADIOLOGIST_DESC = "Study Radiologist";

  /** ID of the study referring physician. */
  public static final String STUDY_REF_PHYSICIAN = "study_ref_physician";

  /** Description of the ID of the study referring physician. */
  public static final String STUDY_REF_PHYSICIAN_DESC =
  "Study Referring Physician";

  /** ID of the study modality. */
  public static final String STUDY_MODALITY = "study_modality";

  /** Description of the ID of the study modality. */
  public static final String STUDY_MODALITY_DESC = "Study Modality";

  /** ID of the study miscellaneous information. */
  public static final String STUDY_MISC_INFO = "study_misc_info";

  /** Description of the ID of the study miscellaneous information. */
  public static final String STUDY_MISC_INFO_DESC =
  "Study Miscellaneous Information";

  /** ID of the patient preparation. */
  public static final String PATIENT_PREPARATION = "pat_preparation";

  /** Description of the ID of the patient preparation. */
  public static final String PATIENT_PREPARATION_DESC = "Patient Preparation";

  /** ID of the institutional station id. */
  public static final String INST_STATION_ID = "inst_station_id";

  /** Description of the ID of the institutional station id. */
  public static final String INST_STATION_ID_DESC = "Institutional Station ID";

  /** ID of the institutional acquisition manufacturer. */
  public static final String INST_ACQ_MANUF = "inst_acq_manuf";

  /** Description of the ID of the institutional acquisition manufacturer. */
  public static final String INST_ACQ_MANUF_DESC =
  "Institutional Acquisition Manufacturer";

  /** ID of the institutional acquisition model. */
  public static final String INST_ACQ_MODEL = "inst_acq_model";

  /** Description of the ID of the institutional acquisition model. */
  public static final String INST_ACQ_MODEL_DESC =
  "Institutional Acquisition Model";

  /** ID of the institutional id. */
  public static final String INST_ID = "inst_id";

  /** Description of the ID of the institutional id. */
  public static final String INST_ID_DESC = "Institutional ID";

  /** ID of the RIS study number extension. */
  public static final String RIS_STUDY_NO_EXT = "RIS_study_no_ext";

  /** Description of the ID of the RIS study number extension. */
  public static final String RIS_STUDY_NO_EXT_DESC =
  "RIS Study Number Extension";

  /** ID of the original image id number assigned. */
  public static final String IMG_ORIG_ID_NO = "img_orig_id_no";

  /** Description of the ID of the . */
  public static final String IMG_ORIG_ID_NO_DESC =
  "Original Image ID Number Assigned";

  /** ID of the free text. */
  public static final String FREE_TEXT = "free_text";

  /** Description of the ID of the free text. */
  public static final String FREE_TEXT_DESC = "Free Space for Future Use";

  /** ID of the MR patient orientation. */
  public static final String MR_PAT_ORIENTATION = "mr_pat_orientation";

  /** Description of the ID of the MR patient orientation. */
  public static final String MR_PAT_ORIENTATION_DESC =
  "MR Patient Orientation";

  /** ID of the MR plane name. */
  public static final String MR_PLANE_NAME = "mr_plane_name";

  /** Description of the ID of the MR plane name. */
  public static final String MR_PLANE_NAME_DESC = "MR Plane Name";

  /** ID of the MR position. */
  public static final String MR_POSITION = "mr_position";

  /** Description of the ID of the MR position. */
  public static final String MR_POSITION_DESC = "MR Position";

  /** ID of the MR longitudinal reference. */
  public static final String MR_LONGIT_REF = "mr_longit_ref";

  /** Description of the ID of the MR longitudinal reference. */
  public static final String MR_LONGIT_REF_DESC = "MR Longitudinal Reference";

  /** ID of the MR vertical reference. */
  public static final String MR_VERTIC_REF = "mr_vertic_ref";

  /** Description of the ID of the MR vertical reference. */
  public static final String MR_VERTIC_REF_DESC = "MR Vertical Reference";

  /** ID of the MR minimum right position. */
  public static final String MR_START_X = "mr_start_x";

  /** Description of the ID of the MR minimum right position. */
  public static final String MR_START_X_DESC = "MR Minimum Right Position";

  /** ID of the MR maximum right position. */
  public static final String MR_END_X = "mr_end_x";

  /** Description of the ID of the MR maximum right position. */
  public static final String MR_END_X_DESC = "MR Maximum Right Position";

  /** ID of the MR minimum anterior view. */
  public static final String MR_START_Y = "mr_start_y";

  /** Description of the ID of the MR minimum anterior view. */
  public static final String MR_START_Y_DESC = "MR Minimum Anterior View";

  /** ID of the MR maximum anterior view. */
  public static final String MR_END_Y = "mr_end_y";

  /** Description of the ID of the MR maximum anterior view. */
  public static final String MR_END_Y_DESC = "MR Maximum Anterior View";

  /** ID of the MR minimum superior view. */
  public static final String MR_START_Z = "mr_start_z";

  /** Description of the ID of the MR minimum superior view. */
  public static final String MR_START_Z_DESC = "MR Minimum Superior View";

  /** ID of the MR maximum superior view. */
  public static final String MR_END_Z = "mr_end_z";

  /** Description of the ID of the MR maximum superior view. */
  public static final String MR_END_Z_DESC = "MR Maximum Superior View";

  /** ID of the MR image location. */
  public static final String MR_IMAGE_LOCATION = "mr_image_location";

  /** Description of the ID of the MR image location. */
  public static final String MR_IMAGE_LOCATION_DESC =
  "MR Image Location Relative to Landmark";

  /** ID of the MR pulse sequence name. */
  public static final String MR_PULSE_SEQ_NAME = "mr_pulse_seq_name";

  /** Description of the ID of the MR pulse sequence name. */
  public static final String MR_PULSE_SEQ_NAME_DESC = "MR Pulse Sequence Name";

  /** ID of the MR gating name. */
  public static final String MR_GATING_NAME = "mr_gating_name";

  /** Description of the ID of the MR gating name. */
  public static final String MR_GATING_NAME_DESC = "MR Gating Name";

  /** ID of the MR gauss. */
  public static final String MR_GAUSS = "mr_gauss";

  /** Description of the ID of the MR gauss. */
  public static final String MR_GAUSS_DESC = "MR Magnetic Field Strength";

  /** ID of the MR coil name. */
  public static final String MR_COIL_NAME = "mr_coil_name";

  /** Description of the ID of the MR coil name. */
  public static final String MR_COIL_NAME_DESC = "MR Coil Name";

  /** ID of the MR repetition time. */
  public static final String MR_REPETITION_RECOVERY_TIME =
  "mr_repetition_recovery_time";

  /** Description of the ID of the MR repetition time. */
  public static final String MR_REPETITION_RECOVERY_TIME_DESC =
  "MR Repetition Recovery Time, in micro-sec";

  /** ID of the MR scan time. */
  public static final String MR_SCAN_TIME = "mr_scan_time";

  /** Description of the ID of the MR scan time. */
  public static final String MR_SCAN_TIME_DESC = "MR Active Scan Time";

  /** ID of the MR echo delay. */
  public static final String MR_ECHO_DELAY = "mr_echo_delay";

  /** Description of the ID of the MR echo delay. */
  public static final String MR_ECHO_DELAY_DESC = "MR Echo Delay";

  /** ID of the MR inversion time. */
  public static final String MR_INVERSION_TIME = "mr_inversion_time";

  /** Description of the ID of the MR inversion time. */
  public static final String MR_INVERSION_TIME_DESC =
  "MR Inversion Recovery Time";

  /** ID of the MR number of echoes. */
  public static final String MR_NO_ECHOES = "mr_no_echos";

  /** Description of the ID of the MR number of echoes. */
  public static final String MR_NO_ECHOES_DESC = "MR Number of Echoes";

  /** ID of the MR number of excitations. */
  public static final String MR_NO_EXCITATIONS = "mr_no_excitations";

  /** Description of the ID of the MR number of excitations. */
  public static final String MR_NO_EXCITATIONS_DESC =
  "MR Number of Excitations";

  /** ID of the MR flip angle. */
  public static final String MR_FLIP_ANGLE = "mr_flip_angle";

  /** Description of the ID of the MR flip angle. */
  public static final String MR_FLIP_ANGLE_DESC = "MR Flip Angle";

  /** ID of the MR traverse time. */
  public static final String MR_TRAVERSE_TIME = "mr_traverse_time";

  /** Description of the ID of the MR traverse time. */
  public static final String MR_TRAVERSE_TIME_DESC = "MR Traverse Time";

  /** ID of the MR default window. */
  public static final String MR_DFLT_WINDOW = "mr_dflt_window";

  /** Description of the ID of the MR default window. */
  public static final String MR_DFLT_WINDOW_DESC = "MR Default Window";

  /** ID of the MR default level. */
  public static final String MR_DFLT_LEVEL = "mr_dflt_level";

  /** Description of the ID of the MR default level. */
  public static final String MR_DFLT_LEVEL_DESC = "MR Default Level";

  /** ID of the MR minimum study z value. */
  public static final String MR_STUDY_MIN = "mr_study_min";

  /** Description of the ID of the MR minimum study z value. */
  public static final String MR_STUDY_MIN_DESC = "MR Minimum Study Z-Value";

  /** ID of the MR maximum study z value. */
  public static final String MR_STUDY_MAX = "mr_study_max";

  /** Description of the ID of the MR maximum study z value. */
  public static final String MR_STUDY_MAX_DESC = "MR Maximum Study Z-Value";

  /** ID of the MR free text. */
  public static final String MR_FREE_TEXT = "mr_free_text";

  /** Description of the ID of the MR free text. */
  public static final String MR_FREE_TEXT_DESC =
  "MR Free Space for Future Use";

  /** ID of the CR sensitivity. */
  public static final String CR_SENSITIVITY = "cr_sensitivity";

  /** Description of the ID of the CR sensitivity. */
  public static final String CR_SENSITIVITY_DESC = "CR Sensitivity Value";

  /** ID of the CR range. */
  public static final String CR_RANGE = "cr_range";

  /** Description of the ID of the CR range. */
  public static final String CR_RANGE_DESC = "CR Range Value";

  /** ID of the CR sample mode. */
  public static final String CR_PRE_SAMPLE_MODE = "cr_pre_sample_mode";

  /** Description of the ID of the CR sample mode. */
  public static final String CR_PRE_SAMPLE_MODE_DESC = "CR Sample Mode";

  /** ID of the CR preprocessing parameters. */
  public static final String CR_PREPROC_PARAMETERS = "cr_preproc_parameters";

  /** Description of the ID of the CR preprocessing parameters. */
  public static final String CR_PREPROC_PARAMETERS_DESC =
  "CR Preprocessing Parameters";

  /** ID of the CR display window center. */
  public static final String CR_DISP_WIN_CENTER = "cr_disp_win_center";

  /** Description of the ID of the CR display window center. */
  public static final String CR_DISP_WIN_CENTER_DESC =
  "CR Display Window Center";

  /** ID of the CR display window width. */
  public static final String CR_DISP_WIN_WIDTH = "cr_disp_win_width";

  /** Description of the ID of the CR display window width. */
  public static final String CR_DISP_WIN_WIDTH_DESC =
  "CR Display Window Width";

  /** ID of the CR minimum gray level in the study. */
  public static final String CR_STUDY_MIN = "cr_study_min";

  /** Description of the ID of the CR minimum gray level in the study. */
  public static final String CR_STUDY_MIN_DESC = "CR Minimum Study Z-Value";

  /** ID of the CR maximum gray level in the study. */
  public static final String CR_STUDY_MAX = "cr_study_max";

  /** Description of the ID of the CR maximum gray level in the study. */
  public static final String CR_STUDY_MAX_DESC = "CR Maximum Study Z-Value";

  /** ID of the CR extra filler space. */
  public static final String CR_FREE_TEXT = "cr_free_text";

  /** Description of the ID of the CR extra filler space. */
  public static final String CR_FREE_TEXT_DESC =
  "CR Free Space for Future Use";

  /** ID of the CT series name. */
  public static final String CT_SERIES_NAME = "ct_series_name";

  /** Description of the ID of the CT series name. */
  public static final String CT_SERIES_NAME_DESC = "CT Series Name";

  /** ID of the CT exposure time. */
  public static final String CT_EXPO_TIME = "ct_expo_time";

  /** Description of the ID of the CT exposure time. */
  public static final String CT_EXPO_TIME_DESC = "CT Exposure Time, in ms";

  /** ID of the CT rate of X-ray exposure. */
  public static final String CT_EXPO_RATE = "ct_expo_rate";

  /** Description of the ID of the CT rate of X-ray exposure. */
  public static final String CT_EXPO_RATE_DESC = "CT Rate of X-Ray Exposure";

  /** ID of the CT tube voltage. */
  public static final String CT_KVP = "ct_kvp";

  /** Description of the ID of the CT tube voltage. */
  public static final String CT_KVP_DESC = "CT Tube Voltage, in Kvp";

  /** ID of the CT scan sequence. */
  public static final String CT_SCAN_SEQ = "ct_scan_seq";

  /** Description of the ID of the CT scan sequence. */
  public static final String CT_SCAN_SEQ_DESC = "CT Scan Sequence";

  /** ID of the CT patient orientation. */
  public static final String CT_PAT_ORIENT = "ct_pat_orient";

  /** Description of the ID of the CT patient orientation. */
  public static final String CT_PAT_ORIENT_DESC = "CT Patient Orientation";

  /** ID of the CT tube-to-detector distance. */
  public static final String CT_SRC_DET_DIST = "ct_src_det_dist";

  /** Description of the ID of the CT tube-to-detector distance. */
  public static final String CT_SRC_DET_DIST_DESC =
  "CT Tube-to-Detector Distance";

  /** ID of the CT tube-to-patient distance. */
  public static final String CT_SRC_PAT_DIST = "ct_src_pat_dist";

  /** Description of the ID of the CT tube-to-patient distance. */
  public static final String CT_SRC_PAT_DIST_DESC =
  "CT Tube-to-Patient Distance";

  /** ID of the CT position reference. */
  public static final String CT_POSIT_REF = "ct_posit_ref";

  /** Description of the ID of the CT position reference. */
  public static final String CT_POSIT_REF_DESC = "CT Position Reference";

  /** ID of the CT slice location. */
  public static final String CT_SLICE_LOC = "ct_slice_loc";

  /** Description of the ID of the CT slice location. */
  public static final String CT_SLICE_LOC_DESC = "CT Slice Location";

  /** ID of the CT field of view. */
  public static final String CT_RECON_DIAM = "ct_recon_diam";

  /** Description of the ID of the CT field of view. */
  public static final String CT_RECON_DIAM_DESC =
  "CT Field of View, Reconst Diam";

  /** ID of the CT convolution kernal. */
  public static final String CT_CONV_KERN = "ct_conv_kern";

  /** Description of the ID of the CT convolution kernal. */
  public static final String CT_CONV_KERN_DESC = "CT Convolution Kernel";

  /** ID of the CT rescale intercept. */
  public static final String CT_RESCAL_INTER = "ct_rescal_inter";

  /** Description of the ID of the CT rescale intercept. */
  public static final String CT_RESCAL_INTER_DESC = "CT Rescale Intercept";

  /** ID of the CT rescale slope. */
  public static final String CT_RESCAL_SLOPE = "ct_rescal_slope";

  /** Description of the ID of the CT rescale slope. */
  public static final String CT_RESCAL_SLOPE_DESC = "CT Rescale Slope";

  /** ID of the CT gray scale. */
  public static final String CT_GRAY_SCALE = "ct_gray_scale";

  /** Description of the ID of the CT gray scale. */
  public static final String CT_GRAY_SCALE_DESC = "";

  /** ID of the CT default level. */
  public static final String CT_DFLT_LEVEL = "ct_dflt_level";

  /** Description of the ID of the CT default level. */
  public static final String CT_DFLT_LEVEL_DESC = "CT Default Level";

  /** ID of the CT default window. */
  public static final String CT_DFLT_WINDOW = "ct_dflt_window";

  /** Description of the ID of the CT default window. */
  public static final String CT_DFLT_WINDOW_DESC = "CT Default Window";

  /** ID of the CT study minimum z-value. */
  public static final String CT_STUDY_MIN = "ct_study_min";

  /** Description of the ID of the CT study minimum z-value. */
  public static final String CT_STUDY_MIN_DESC = "CT Minimum Study Z-Value";

  /** ID of the CT study maximum z-value. */
  public static final String CT_STUDY_MAX = "ct_study_max";

  /** Description of the ID of the CT study maximum z-value. */
  public static final String CT_STUDY_MAX_DESC = "CT Maximum Study Z-Value";

  /** ID of the CT extra filler space. */
  public static final String CT_FREE_TEXT = "ct_free_text";

  /** Description of the ID of the CT extra filler space. */
  public static final String CT_FREE_TEXT_DESC =
  "CT Free Space for Further Use";

  /** ID of the XA frame rate. */
  public static final String XA_IMG_FPS = "xa_img_fps";

  /** Description of the ID of the XA frame rate. */
  public static final String XA_IMG_FPS_DESC = "XA Frame Rate";

  /** ID of the XA frame rate variability. */
  public static final String XA_IMG_FPS_VARIABLE = "xa_img_fps_variable";

  /** Description of the ID of the XA frame rate variability. */
  public static final String XA_IMG_FPS_VARIABLE_DESC =
  "XA Frame Rate variability";

  /** ID of the XA image plane type. */
  public static final String XA_IMG_PLANE_TYPE = "xa_img_plane_type";

  /** Description of the ID of the XA image plane type. */
  public static final String XA_IMG_PLANE_TYPE_DESC = "XA Image Plane Type";

  /** ID of the XA table motion. */
  public static final String XA_TABLE_MOTION = "xa_table_motion";

  /** Description of the ID of the XA table motion. */
  public static final String XA_TABLE_MOTION_DESC = "XA Table Motion";

  /** ID of the XA positioner arm motion. */
  public static final String XA_POSITIONER_MOTION = "xa_positioner_motion";

  /** Description of the ID of the XA positioner arm motion. */
  public static final String XA_POSITIONER_MOTION_DESC =
  "XA Positioner Arm Motion";

  /** ID of the XA positioner primary angle. */
  public static final String XA_PRIM_ANGLE = "xa_prim_angle";

  /** Description of the ID of the XA positioner primary angle. */
  public static final String XA_PRIM_ANGLE_DESC =
  "XA Positioner Primary Angle";

  /** ID of the XA positioner secondary angle. */
  public static final String XA_SEC_ANGLE = "xa_sec_angle";

  /** Description of the ID of the XA positioner secondary angle. */
  public static final String XA_SEC_ANGLE_DESC =
  "XA Positioner Arm Secondary Angle";

  /** ID of the XA field of view. */
  public static final String XA_FOV_SHAPE = "xa_fov_shape";

  /** Description of the ID of the XA field of view. */
  public static final String XA_FOV_SHAPE_DESC = "XA Field of View Shape";

  /** ID of the XA source to patient distance. */
  public static final String XA_SOURCE_TO_PATIENT = "xa_source_to_patient";

  /** Description of the ID of the XA source to patient distance. */
  public static final String XA_SOURCE_TO_PATIENT_DESC =
  "XA Source to Patient Distance";

  /** ID of the XA source to detector distance. */
  public static final String XA_SOURCE_TO_DETECTOR = "xa_source_to_detector";

  /** Description of the ID of the XA source to detector distance. */
  public static final String XA_SOURCE_TO_DETECTOR_DESC =
  "XA Source to Detector Distance";

  /** ID of the XA X-ray tube voltage. */
  public static final String XA_EXPO_KVP = "xa_expo_kvp";

  /** Description of the ID of the XA X-ray tube voltage. */
  public static final String XA_EXPO_KVP_DESC = "XA X-Ray Tube Voltage in KVp";

  /** ID of the XA X-ray tube current. */
  public static final String XA_EXPO_CURRENT = "xa_expo_current";

  /** Description of the ID of the XA X-ray tube current. */
  public static final String XA_EXPO_CURRENT_DESC =
  "XA X-Ray Tube Current in mA";

  /** ID of the XA exposure time. */
  public static final String XA_EXPO_TIME = "xa_expo_time";

  /** Description of the ID of the XA exposure time. */
  public static final String XA_EXPO_TIME_DESC = "XA Exposure Time in msec";

  /** ID of the XA rate of X-ray exposure. */
  public static final String XA_EXPO_RATE = "xa_expo_rate";

  /** Description of the ID of the XA rate of X-ray exposure. */
  public static final String XA_EXPO_RATE_DESC = "XA Rate of X-Ray Exposure";

  /** ID of the XA focal spot. */
  public static final String XA_FOCAL_SPOT = "xa_focal_spot";

  /** Description of the ID of the XA focal spot. */
  public static final String XA_FOCAL_SPOT_DESC = "XA Focal Spot in mm";

  /** ID of the XA shortened flag. */
  public static final String XA_CUT_FLAG = "xa_cut_flag";

  /** Description of the ID of the XA shortened flag. */
  public static final String XA_CUT_FLAG_DESC = "XA Shortened Flag";

  /** ID of the XA number of curves. */
  public static final String XA_NUM_CURVES_ENCLOSED = "xa_num_curves_enclosed";

  /** Description of the ID of the XA number of curves. */
  public static final String XA_NUM_CURVES_ENCLOSED_DESC =
  "XA Number of Curves";

  /** ID of the XA free space. */
  public static final String XA_FREE_TEXT = "xa_free_text";

  /** Description of the ID of the XA free space. */
  public static final String XA_FREE_TEXT_DESC =
  "XA Free Space for Future Use";

  /** ID of the image number. */
  public static final String IMG_NUMBER = "img_number";

  /** Description of the ID of the image number. */
  public static final String IMG_NUMBER_DESC = "Image Number in the Series";

  /** ID of the image minimum. */
  public static final String IMG_MIN = "img_min";

  /** Description of the ID of the image minimum. */
  public static final String IMG_MIN_DESC = "Minimum Z-Value of Image";

  /** ID of the image maximum. */
  public static final String IMG_MAX = "img_max";

  /** Description of the ID of the . */
  public static final String IMG_MAX_DESC = "Maximum Z-Value of Image";

  /** ID of the reconstruction algorithm. */
  public static final String RECON_ALGORITHM = "recon_algorithm";

  /** Description of the ID of the reconstruction algorithm. */
  public static final String RECON_ALGORITHM_DESC = "Reconstruction Algorithm";

  /** ID of the thickness. */
  public static final String THICK = "thick";

  /** Description of the ID of the thickness. */
  public static final String THICK_DESC = "Thickness";

  /** ID of the version name. */
  public static final String VERSION = "version";

  /** Description of the ID of the version name. */
  public static final String VERSION_DESC = "Version Name";

  /** ID of the window level. */
  public static final String WINDOW_LEVEL = "window_level";

  /** Description of the ID of the window level. */
  public static final String WINDOW_LEVEL_DESC = "Window Level";

  /** ID of the window width. */
  public static final String WINDOW_WIDTH = "window_width";

  /** Description of the ID of the window width. */
  public static final String WINDOW_WIDTH_DESC = "Window Width";

  /** ID of the MR repetition time. */
  public static final String MR_REPETITION_TIME = "mr_repetition_time";

  /** Description of the ID of the MR repetition time. */
  public static final String MR_REPETITION_TIME_DESC = "MR TR Value";

  /** ID of the MR echo time. */
  public static final String MR_ECHO_TIME = "mr_echo_time";

  /** Description of the ID of the MR echo time. */
  public static final String MR_ECHO_TIME_DESC = "MR TE Value";

  /** ID of the patient orientation. */
  public static final String PAT_ORIENTATION = "pat_orientation";

  /** Description of the ID of the patient orientation. */
  public static final String PAT_ORIENTATION_DESC = "Patient Orientation";

  /** ID of the image position patient x. */
  public static final String IMG_POS_PAT_X = "img_pos_pat_x";

  /** Description of the ID of the image position patient x. */
  public static final String IMG_POS_PAT_X_DESC = "Image Position Patient X";

  /** ID of the image position patient y. */
  public static final String IMG_POS_PAT_Y = "img_pos_pat_y";

  /** Description of the ID of the image position patient y. */
  public static final String IMG_POS_PAT_Y_DESC = "Image Position Patient Y";

  /** ID of the image position patient z. */
  public static final String IMG_POS_PAT_Z = "img_pos_pat_z";

  /** Description of the ID of the image position patient z. */
  public static final String IMG_POS_PAT_Z_DESC = "Image Position Patient Z";

  /** ID of the patient orientation x1. */
  public static final String PAT_ORIENTATION_X1 = "pat_orientation_x1";

  /** Description of the ID of the patient orientation x1. */
  public static final String PAT_ORIENTATION_X1_DESC =
  "Patient Orientation X1";

  /** ID of the patient orientation y1. */
  public static final String PAT_ORIENTATION_Y1 = "pat_orientation_y1";

  /** Description of the ID of the patient orientation y1. */
  public static final String PAT_ORIENTATION_Y1_DESC =
  "Patient Orientation Y1";

  /** ID of the patient orientation z1. */
  public static final String PAT_ORIENTATION_Z1 = "pat_orientation_z1";

  /** Description of the ID of the patient orientation z1. */
  public static final String PAT_ORIENTATION_Z1_DESC =
  "Patient Orientation Z1";

  /** ID of the patient orientation x2. */
  public static final String PAT_ORIENTATION_X2 = "pat_orientation_x2";

  /** Description of the ID of the patient orientation x2. */
  public static final String PAT_ORIENTATION_X2_DESC =
  "Patient Orientation X2";

  /** ID of the patient orientation y2. */
  public static final String PAT_ORIENTATION_Y2 = "pat_orientation_y2";

  /** Description of the ID of the patient orientation y2. */
  public static final String PAT_ORIENTATION_Y2_DESC =
  "Patient Orientation Y2";

  /** ID of the patient orientation z2. */
  public static final String PAT_ORIENTATION_Z2 = "pat_orientation_z2";

  /** Description of the ID of the patient orientation z2. */
  public static final String PAT_ORIENTATION_Z2_DESC =
  "Patient Orientation Z2";

  /** ID of the XA frame number. */
  public static final String XA_FRAME_NUMBER = "xa_frame_number";

  /** Description of the ID of the XA frame number. */
  public static final String XA_FRAME_NUMBER_DESC = "XA Frame Number";

  /** ID of the XA original frame number. */
  public static final String XA_ORIG_FRAME_NO = "xa_orig_frame_no";

  /** Description of the ID of the XA original frame number. */
  public static final String XA_ORIG_FRAME_NO_DESC =
  "XA Original Frame Number";

  /** ID of the XA frame interval. */
  public static final String XA_FRAME_INTERVAL = "xa_frame_interval";

  /** Description of the ID of the XA frame interval. */
  public static final String XA_FRAME_INTERVAL_DESC =
  "XA Frame Interval, in ms";

  /** ID of the XA primary angle. */
  public static final String XA_PRIM_ANGLE_INCREMENT =
  "xa_prim_angle_increment";

  /** Description of the ID of the XA primary angle. */
  public static final String XA_PRIM_ANGLE_INCREMENT_DESC =
  "XA Positioner Primary Angle Increment";

  /** ID of the XA secondary angle. */
  public static final String XA_SEC_ANGLE_INCREMENT = "xa_sec_angle_increment";

  /** Description of the ID of the XA secondary angle. */
  public static final String XA_SEC_ANGLE_INCREMENT_DESC =
  "XA Positioner Secondary Angle Increment";

  /** ID of the XA current primary angle. */
  public static final String XA_CURRENT_PRIM_ANGLE = "xa_current_prim_angle";

  /** Description of the ID of the XA current primary angle. */
  public static final String XA_CURRENT_PRIM_ANGLE_DESC =
  "XA Positioner Current Primary Angle";

  /** ID of the XA current secondary angle. */
  public static final String XA_CURRENT_SEC_ANGLE = "xa_current_sec_angle";

  /** Description of the ID of the XA current secondary angle. */
  public static final String XA_CURRENT_SEC_ANGLE_DESC =
  "XA Positioner Current Secondary Angle";

  /** ID of the XA table vertical increment. */
  public static final String XA_TABLE_VERTIC_INCREMENT =
  "xa_table_vertic_increment";

  /** Description of the ID of the XA table vertical increment. */
  public static final String XA_TABLE_VERTIC_INCREMENT_DESC =
  "XA Table Vertical Increment";

  /** ID of the XA table lateral increment. */
  public static final String XA_TABLE_LATERAL_INCREMENT =
  "xa_table_lateral_increment";

  /** Description of the ID of the XA table lateral increment. */
  public static final String XA_TABLE_LATERAL_INCREMENT_DESC =
  "XA Table Lateral Increment";

  /** ID of the XA table longitudinal increment. */
  public static final String XA_TABLE_LONGIT_INCREMENT =
  "xa_table_longit_increment";

  /** Description of the ID of the XA table longitudinal increment. */
  public static final String XA_TABLE_LONGIT_INCREMENT_DESC =
  "XA Table Longitudinal Increment";
}
