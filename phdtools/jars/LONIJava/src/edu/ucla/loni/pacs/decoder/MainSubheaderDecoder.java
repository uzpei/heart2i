/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.pacs.decoder;

import edu.ucla.loni.pacs.PacsElements;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;

/**
 * Decodes a Main PACS Subheader from an Image Input Stream.
 *
 * @version 6 April 2004
 */
public class MainSubheaderDecoder extends SubheaderDecoder
{
  /**
   * Constructs a Main Subheader Decoder which uses the specified Image Input
   * Stream to decode the Main PACS Subheader.
   *
   * @param in Image Input Stream that is read from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public MainSubheaderDecoder(ImageInputStream in) throws IOException
    {
      super(in);

      try {

	// Header version
	String name = PacsElements.FILE_TYPE;
	String desc = PacsElements.FILE_TYPE_DESC;
	_readString(name, desc, 4);

	// Image format
	name = PacsElements.IMG_DIM;
	desc = PacsElements.IMG_DIM_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_ROWS;
	desc = PacsElements.IMG_ROWS_DESC;
	_readInt(name, desc);

	name = PacsElements.IMG_COLS;
	desc = PacsElements.IMG_COLS_DESC;
	_readInt(name, desc);

	name = PacsElements.NO_IMG_IN_FILE;
	desc = PacsElements.NO_IMG_IN_FILE_DESC;
	_readInt(name, desc);

	// Image storage
	name = PacsElements.PIX_BIT_ALLOCATED;
	desc = PacsElements.PIX_BIT_ALLOCATED_DESC;
	_readInt(name, desc);

	name = PacsElements.PIX_BIT_STORED;
	desc = PacsElements.PIX_BIT_STORED_DESC;
	_readInt(name, desc);

	name = PacsElements.PIX_BIT_HIGH_POS;
	desc = PacsElements.PIX_BIT_HIGH_POS_DESC;
	_readInt(name, desc);

	name = PacsElements.COMPR_FLAG;
	desc = PacsElements.COMPR_FLAG_DESC;
	_readInt(name, desc);

	name = PacsElements.COMPR_VERSION;
	desc = PacsElements.COMPR_VERSION_DESC;
	_readString(name, desc, 8);

	// Voxel size
	name = PacsElements.X_PIX_SIZE;
	desc = PacsElements.X_PIX_SIZE_DESC;
	_readFloat(name, desc);

	name = PacsElements.Y_PIX_SIZE;
	desc = PacsElements.Y_PIX_SIZE_DESC;
	_readFloat(name, desc);

	name = PacsElements.STUDY_SLICE_THICKNESS;
	desc = PacsElements.STUDY_SLICE_THICKNESS_DESC;
	_readFloat(name, desc);

	name = PacsElements.STUDY_SLICE_SPACING;
	desc = PacsElements.STUDY_SLICE_SPACING_DESC;
	_readFloat(name, desc);

	name = PacsElements.FIELD_OF_VIEW;
	desc = PacsElements.FIELD_OF_VIEW_DESC;
	_readString(name, desc, 16);

	// Pointers to subheaders
	name = PacsElements.SUBHDR_BLK_SIZE;
	desc = PacsElements.SUBHDR_BLK_SIZE_DESC;
	_readInt(name, desc);

	name = PacsElements.SUBHDR_BLOCK_POSITION;
	desc = PacsElements.SUBHDR_BLOCK_POSITION_DESC;
	_readInt(name, desc);

	// Acquisition machine references
	name = PacsElements.RID_PATH;
	desc = PacsElements.RID_PATH_DESC;
	_readString(name, desc, 40);

	name = PacsElements.RID_FILENAME;
	desc = PacsElements.RID_FILENAME_DESC;
	_readString(name, desc, 48);

	name = PacsElements.EXAM_REFERENCE_NO;
	desc = PacsElements.EXAM_REFERENCE_NO_DESC;
	_readString(name, desc, 20);

	name = PacsElements.STUDY_REFERENCE_NO;
	desc = PacsElements.STUDY_REFERENCE_NO_DESC;
	_readString(name, desc, 20);

	name = PacsElements.SERIES_REFERENCE_NO;
	desc = PacsElements.SERIES_REFERENCE_NO_DESC;
	_readString(name, desc, 20);

	// RIS and PACS study reference ID
	name = PacsElements.RIS_STUDY_NO;
	desc = PacsElements.RIS_STUDY_NO_DESC;
	_readString(name, desc, 12);

	name = PacsElements.PACS_STUDY_NO;
	desc = PacsElements.PACS_STUDY_NO_DESC;
	_readString(name, desc, 12);

	// Patient information
	name = PacsElements.PATIENT_ID;
	desc = PacsElements.PATIENT_ID_DESC;
	_readString(name, desc, 12);

	name = PacsElements.PATIENT_NAME;
	desc = PacsElements.PATIENT_NAME_DESC;
	_readString(name, desc, 48);

	name = PacsElements.PATIENT_BIRTH_DATE;
	desc = PacsElements.PATIENT_BIRTH_DATE_DESC;
	_readString(name, desc, 12);

	name = PacsElements.PAT_SEX;
	desc = PacsElements.PAT_SEX_DESC;
	_readString(name, desc, 4);

	name = PacsElements.PATIENT_HISTORY;
	desc = PacsElements.PATIENT_HISTORY_DESC;
	_readString(name, desc, 80);

	name = PacsElements.PATIENT_STATUS;
	desc = PacsElements.PATIENT_STATUS_DESC;
	_readString(name, desc, 4);

	// Study information
	name = PacsElements.REASON_FOR_REQUEST;
	desc = PacsElements.REASON_FOR_REQUEST_DESC;
	_readString(name, desc, 80);

	name = PacsElements.STUDY_DESCRIPTION;
	desc = PacsElements.STUDY_DESCRIPTION_DESC;
	_readString(name, desc, 80);

	name = PacsElements.STUDY_SUBDESCRIPTION;
	desc = PacsElements.STUDY_SUBDESCRIPTION_DESC;
	_readString(name, desc, 80);

	name = PacsElements.STUDY_ACQ_DATE;
	desc = PacsElements.STUDY_ACQ_DATE_DESC;
	_readString(name, desc, 12);

	name = PacsElements.STUDY_ACQ_TIME;
	desc = PacsElements.STUDY_ACQ_TIME_DESC;
	_readString(name, desc, 12);

	name = PacsElements.STUDY_RADIOLOGIST;
	desc = PacsElements.STUDY_RADIOLOGIST_DESC;
	_readString(name, desc, 40);

	name = PacsElements.STUDY_REF_PHYSICIAN;
	desc = PacsElements.STUDY_REF_PHYSICIAN_DESC;
	_readString(name, desc, 40);

	name = PacsElements.STUDY_MODALITY;
	desc = PacsElements.STUDY_MODALITY_DESC;
	_readString(name, desc, 4);

	name = PacsElements.STUDY_MISC_INFO;
	desc = PacsElements.STUDY_MISC_INFO_DESC;
	_readString(name, desc, 48);

	name = PacsElements.PATIENT_PREPARATION;
	desc = PacsElements.PATIENT_PREPARATION_DESC;
	_readString(name, desc, 32);

	// Imaging device information
	name = PacsElements.INST_STATION_ID;
	desc = PacsElements.INST_STATION_ID_DESC;
	_readString(name, desc, 24);

	name = PacsElements.INST_ACQ_MANUF;
	desc = PacsElements.INST_ACQ_MANUF_DESC;
	_readString(name, desc, 24);

	name = PacsElements.INST_ACQ_MODEL;
	desc = PacsElements.INST_ACQ_MODEL_DESC;
	_readString(name, desc, 24);

	name = PacsElements.INST_ID;
	desc = PacsElements.INST_ID_DESC;
	_readString(name, desc, 32);

	// Extra IDs
	name = PacsElements.RIS_STUDY_NO_EXT;
	desc = PacsElements.RIS_STUDY_NO_EXT_DESC;
	_readString(name, desc, 40);

	name = PacsElements.IMG_ORIG_ID_NO;
	desc = PacsElements.IMG_ORIG_ID_NO_DESC;
	_readString(name, desc, 8);

	// Extra filler space
	name = PacsElements.FREE_TEXT;
	desc = PacsElements.FREE_TEXT_DESC;
	_readString(name, desc, 28);
      }

      catch (Exception e) {
	IOException ioe = new IOException("Unable to decode the main " +
					  "subheader.");
	ioe.initCause(e);
	throw ioe;
      }
    }
}
