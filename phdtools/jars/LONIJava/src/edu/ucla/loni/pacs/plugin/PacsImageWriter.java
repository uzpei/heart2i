/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.pacs.PacsElement;
import edu.ucla.loni.pacs.PacsElements;
import edu.ucla.loni.pacs.PacsHeader;
import edu.ucla.loni.pacs.PacsSubheader;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Enumeration;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Image Writer for encoding UCLA PACS images.
 *
 * @version 10 May 2007
 */
public class PacsImageWriter extends ImageWriter
{
  /** UCLA PACS Header of the image sequence currently being written. */
  private PacsHeader _pacsHeader;

  /** Number of images in the sequence currently being written. */
  private int _numberOfImagesWritten;

  /**
   * Constructs a PACS Image Writer.
   *
   * @param originatingProvider The Image Writer Spi that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not an
   *                                  PACS Image Writer Spi.
   */
  public PacsImageWriter(ImageWriterSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be a PACS Image Writer Spi
      if ( !(originatingProvider instanceof PacsImageWriterSpi) ) {
	String msg = "Originating provider must a PACS Image Writer SPI.";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Returns an IIOMetadata object containing default values for encoding a
   * stream of images.
   *
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object.
   */
  public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
      // Return a blank metadata object
      return new PacsStreamMetadata(null, null);
    }

  /**
   * Returns an IIOMetadata object containing default values for encoding an
   * image of the given type.
   *
   * @param imageType An ImageTypeSpecifier indicating the format of the image
   *                  to be written later.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object.
   */
  public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					     ImageWriteParam param)
    {
      // Return a blank metadata object
      return new PacsImageMetadata(null, null);
    }

  /**
   * Returns an IIOMetadata object that may be used for encoding and optionally
   * modified using its document interfaces or other interfaces specific to the
   * writer plug-in that will be used for encoding.
   *
   * @param inData An IIOMetadata object representing stream metadata, used to
   *               initialize the state of the returned object.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object, or null if the plug-in does not provide
   *         metadata encoding capabilities.
   *
   * @throws IllegalArgumentException If inData is null.
   */
  public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					   ImageWriteParam param)
    {
      // Only recognize PACS Stream Metadata
      if (inData instanceof PacsStreamMetadata) { return inData; }

      // Otherwise perform no conversion
      return null;
    }

  /**
   * Returns an IIOMetadata object that may be used for encoding and optionally
   * modified using its document interfaces or other interfaces specific to the
   * writer plug-in that will be used for encoding.
   *
   * @param inData An IIOMetadata object representing image metadata, used to
   *               initialize the state of the returned object.
   * @param imageType An ImageTypeSpecifier indicating the layout and color
   *                  information of the image with which the metadata will be
   *                  associated.
   * @param param An ImageWriteParam that will be used to encode the image, or
   *              null.
   *
   * @return An IIOMetadata object, or null if the plug-in does not provide
   *         metadata encoding capabilities.
   *
   * @throws IllegalArgumentException If either of inData or imageType is null.
   */
  public IIOMetadata convertImageMetadata(IIOMetadata inData,
					  ImageTypeSpecifier imageType,
					  ImageWriteParam param)
    {
      // Only recognize PACS Image Metadata
      if (inData instanceof PacsImageMetadata) { return inData; }

      // Otherwise perform no conversion
      return null;
    }

  /**
   * Returns true if the methods that take an IIOImage parameter are capable
   * of dealing with a Raster (as opposed to RenderedImage) source image.
   *
   * @return True if Raster sources are supported.
   */
  public boolean canWriteRasters()
    {
      return true;
    }

  /**
   * Appends a complete image stream containing a single image and associated
   * stream and image metadata and thumbnails to the output.
   *
   * @param streamMetadata An IIOMetadata object representing stream metadata,
   *                       or null to use default values.
   * @param image An IIOImage object containing an image, thumbnails, and
   *              metadata to be written.
   * @param param An ImageWriteParam, or null to use a default ImageWriteParam.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the image is null, if the stream
   *                                  metadata cannot be converted, or if the
   *                                  image type is unrecognized.
   * @throws IllegalStateException If the output has not been set.
   */
  public void write(IIOMetadata streamMetadata, IIOImage image,
		    ImageWriteParam param) throws IOException
    {
      // Write a sequence with 1 image
      prepareWriteSequence(streamMetadata);
      writeToSequence(image, param);
      endWriteSequence();
    }

  /**
   * Returns true if the writer is able to append an image to an image
   * stream that already contains header information and possibly prior
   * images.
   *
   * @return True If images may be appended sequentially.
   */
  public boolean canWriteSequence()
    {
      return true;
    }

  /**
   * Prepares a stream to accept a series of subsequent writeToSequence calls,
   * using the provided stream metadata object.  The metadata will be written
   * to the stream if it should precede the image data.
   *
   * @param streamMetadata A stream metadata object.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the stream metadata is null or cannot
   *                                  be converted.
   * @throws IllegalStateException If the output has not been set.
   */
  public void prepareWriteSequence(IIOMetadata streamMetadata)
    throws IOException
    {
      // Convert the metadata to PACS Stream Metadata
      IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

      // Unable to convert the metadata
      if (metadata == null) {
	String msg = "Unable to convert the stream metadata for encoding.";
	throw new IllegalArgumentException(msg);
      }

      // Construct a PACS Header from the metadata
      try {
	String rootName = PacsStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	Node root = metadata.getAsTree(rootName);

	// Convert the main Subheader
	Node mainNode = root.getFirstChild();
	PacsSubheader mainSubheader =
	  PacsMetadataConversions.toMainPacsSubheader(mainNode);

	// Convert the modality Subheader
	PacsSubheader modSubheader = null;
	Node modNode = mainNode.getNextSibling();
	String name = modNode.getNodeName();
	if ( PacsStreamMetadataFormat.PACS_CR_SUBHEADER_NAME.equals(name) ) {
	  modSubheader = PacsMetadataConversions.toCrPacsSubheader(modNode);
	}
	if ( PacsStreamMetadataFormat.PACS_CT_SUBHEADER_NAME.equals(name) ) {
	  modSubheader = PacsMetadataConversions.toCtPacsSubheader(modNode);
	}
	if ( PacsStreamMetadataFormat.PACS_MR_SUBHEADER_NAME.equals(name) ) {
	  modSubheader = PacsMetadataConversions.toMrPacsSubheader(modNode);
	}
	if ( PacsStreamMetadataFormat.PACS_XA_SUBHEADER_NAME.equals(name) ) {
	  modSubheader = PacsMetadataConversions.toXaPacsSubheader(modNode);
	}
	if (modSubheader == null) {
	  String msg = "Unable to recognize the modality header type.";
	  throw new IllegalArgumentException(msg);
	}

	// Create the PACS Header
	_pacsHeader = new PacsHeader(mainSubheader, modSubheader);
      }
      
      catch (Exception e) {
	String msg = "Unable to make a PACS Header from the metadata tree.";
	IllegalArgumentException exception = new IllegalArgumentException(msg);
	exception.initCause(e);
	throw exception;
      }

      // Write the main and modality Subheaders to the output stream
      ImageOutputStream outputStream = _getOutputStream();
      _writeSubheader(_pacsHeader.getMainSubheader(), outputStream);
      _writeSubheader(_pacsHeader.getModalitySubheader(), outputStream);

      // Write empty image Subheaders to the output stream (fill in later)
      PacsStreamMetadata psm = (PacsStreamMetadata)streamMetadata;
      int numberOfImages = psm.getNumberOfImages();
      byte[] emptyBuffer = new byte[512];
      for (int i = 0; i < numberOfImages; i++) {
	outputStream.write(emptyBuffer);
      }

      _numberOfImagesWritten = 0;
    }

  /**
   * Appends a single image and possibly associated metadata and thumbnails,
   * to the output.  The supplied ImageReadParam is ignored.
   *
   * @param image An IIOImage object containing an image, thumbnails, and
   *              metadata to be written.
   * @param param An ImageWriteParam or null to use a default ImageWriteParam.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalArgumentException If the image is null, the image type is
   *                                  not recognized, or the image metadata
   *                                  cannot be converted.
   * @throws IllegalStateException If the output has not been set,
   *                               prepareWriteSequence has not been called,
   *                               or the maximum number of images has been
   *                               written.
   */
  public void writeToSequence(IIOImage image, ImageWriteParam param)
    throws IOException
    {
      // Check for the UCLA PACS Header
      if (_pacsHeader == null) {
	String msg = "The image sequence has not been prepared.";
	throw new IllegalStateException(msg);
      }

      // Check that there is room for more images
      PacsStreamMetadata streamMetadata =
	new PacsStreamMetadata(_pacsHeader.getMainSubheader(),
			       _pacsHeader.getModalitySubheader());

      if (streamMetadata.getNumberOfImages() <= _numberOfImagesWritten) {
	String msg = "Cannot write more than " + _numberOfImagesWritten +
	             " images.";
	throw new IllegalStateException(msg);
      }

      // Check for a null image
      if (image == null) {
	String msg = "Cannot write a null image.";
	throw new IllegalArgumentException(msg);
      }

      // Convert the metadata to PACS Image Metadata
      IIOMetadata metadata = convertImageMetadata(image.getMetadata(), null,
						  null);

      // Unable to convert the metadata
      if (metadata == null) {
	String msg = "Unable to convert the image metadata for encoding.";
	throw new IllegalArgumentException(msg);
      }

      // Construct the image Subheaders from the metadata
      try {
	String rootName = PacsImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	Node root = metadata.getAsTree(rootName);

	// Convert the image Subheader
	Node imageNode = root.getFirstChild();
	PacsSubheader imageSubheader =
	  PacsMetadataConversions.toImagePacsSubheader(imageNode);

	// Convert the image appendix Subheader
	Node imageAppNode = imageNode.getNextSibling();
	PacsSubheader imageAppSubheader =
	  PacsMetadataConversions.toImageAppendixPacsSubheader(imageAppNode);

	// Add the Subheaders to the PACS Header
	_pacsHeader.addImageSubheader(imageSubheader);
	_pacsHeader.addImageAppendixSubheader(imageAppSubheader);
      }
      
      catch (Exception e) {
	String msg = "Unable to make image subheaders from the metadata tree.";
	IllegalArgumentException exception = new IllegalArgumentException(msg);
	exception.initCause(e);
	throw exception;
      }

      // Get a Raster from the image
      Raster raster = image.getRaster();
      if (raster == null) {
	RenderedImage renderedImage = image.getRenderedImage();

	// If the Rendered Image is a Buffered Image, get the Raster directly
	if (renderedImage instanceof BufferedImage) {
	  raster = ((BufferedImage)renderedImage).getRaster();
	}

	// Otherwise get a copy of the Raster from the Rendered Image
	raster = renderedImage.getData();
      }

      // Update the Listeners
      processImageStarted(_numberOfImagesWritten);

      // Write an 8 bit grayscale image
      ImageOutputStream outputStream = _getOutputStream();
      DataBuffer dataBuffer = raster.getDataBuffer();
      if (dataBuffer instanceof DataBufferByte) {
	outputStream.write( ((DataBufferByte)dataBuffer).getData() );
      }

      // Write a 16 bit grayscale image
      else if (dataBuffer instanceof DataBufferUShort) {
	short[] data = ((DataBufferUShort)dataBuffer).getData();
	int offset = 0;
	int length = data.length;

	outputStream.writeShorts(data, offset, length);
      }

      // Unrecognized type
      else {
	String msg = "Unable to write the IIOImage.";
	throw new IllegalArgumentException(msg);
      }

      // Update the Listeners
      _numberOfImagesWritten++;
      if ( abortRequested() ) { processWriteAborted(); }
      else { processImageComplete(); }
    }

  /**
   * Completes the writing of a sequence of images begun with
   * prepareWriteSequence.  Any stream metadata that should come at the end of
   * the sequence of images is written out, and any header information at the
   * beginning of the sequence is patched up if necessary.
   *
   * @throws IOException If an error occurs during writing.
   * @throws IllegalStateException If the output has not been set, or
   *                               prepareWriteSequence has not been called.
   */
  public void endWriteSequence() throws IOException
    {
      // Check for the UCLA PACS Header
      if (_pacsHeader == null) {
	String msg = "The image sequence has not been prepared.";
	throw new IllegalStateException(msg);
      }

      // Reset the output stream to the beginning of the image Subheaders
      int offset = 1024 + 512;
      ImageOutputStream outputStream = _getOutputStream();
      outputStream.seek(offset);

      // Write the image Subheaders
      Enumeration imageEnum = _pacsHeader.getImageSubheaders();
      Enumeration imgApEnum = _pacsHeader.getImageAppendixSubheaders();
      while ( imageEnum.hasMoreElements() ) {
	PacsSubheader imageSubheader = (PacsSubheader)imageEnum.nextElement();
	PacsSubheader imgApSubheader = (PacsSubheader)imgApEnum.nextElement();

	_writeSubheader(imageSubheader, outputStream);
	_writeSubheader(imgApSubheader, outputStream);
      }

      // Reset the UCLA PACS Header
      _pacsHeader = null;
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageWriter.  Otherwise, the writer may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the output stream
      try { if (output != null) { ((ImageOutputStream)output).close(); } }
      catch (Exception e) {}

      output = null;
    }

  /**
   * Writes the PACS Subheader to the output stream.
   *
   * @param subheader PACS Subheader to write to the output stream.
   * @param outputStream Image Output Stream to write to.
   *
   * @throws IOException If an error occurs during the writing.
   */
  private void _writeSubheader(PacsSubheader subheader,
			       ImageOutputStream outputStream)
    throws IOException
    {
      Enumeration enun = subheader.getPacsElements();
      while ( enun.hasMoreElements() ) {
	PacsElement pacsElement = (PacsElement)enun.nextElement();
	String name = pacsElement.getID();
	Object value = pacsElement.getValue();

	// Element value is a String
	if (value instanceof String) {
	  int size = 0;

	  // CR Elements
	  if ( name.startsWith("cr") ) {
	    if ( PacsElements.CR_SENSITIVITY.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.CR_RANGE.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.CR_PRE_SAMPLE_MODE.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.CR_PREPROC_PARAMETERS.equals(name) ) {
	      size = 64;
	    }
	    else if ( PacsElements.CR_FREE_TEXT.equals(name) ) {
	      size = 392;
	    }
	  }

	  // CT Elements
	  else if ( name.startsWith("ct") ) {
	    if ( PacsElements.CT_SERIES_NAME.equals(name) ) {
	      size = 20;
	    }
	    else if ( PacsElements.CT_EXPO_TIME.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_EXPO_RATE.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_KVP.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_SCAN_SEQ.equals(name) ) {
	      size = 20;
	    }
	    else if ( PacsElements.CT_PAT_ORIENT.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.CT_SRC_DET_DIST.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.CT_SRC_PAT_DIST.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.CT_POSIT_REF.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_SLICE_LOC.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.CT_RECON_DIAM.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_CONV_KERN.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.CT_RESCAL_INTER.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_RESCAL_SLOPE.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.CT_GRAY_SCALE.equals(name) ) {
	      size = 4;
	    }
	    else if ( PacsElements.CT_FREE_TEXT.equals(name) ) {
	      size = 320;
	    }
	  }

	  // MR Elements
	  else if ( name.startsWith("mr") ) {
	    if ( PacsElements.MR_PAT_ORIENTATION.equals(name) ) {
	      size = 32;
	    }
	    else if ( PacsElements.MR_PLANE_NAME.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.MR_POSITION.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.MR_LONGIT_REF.equals(name) ) {
	      size = 32;
	    }
	    else if ( PacsElements.MR_VERTIC_REF.equals(name) ) {
	      size = 32;
	    }
	    else if ( PacsElements.MR_PULSE_SEQ_NAME.equals(name) ) {
	      size = 24;
	    }
	    else if ( PacsElements.MR_GATING_NAME.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.MR_COIL_NAME.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.MR_TRAVERSE_TIME.equals(name) ) {
	      size = 4;
	    }
	    else if ( PacsElements.MR_FREE_TEXT.equals(name) ) {
	      size = 264;
	    }
	  }

	  // XA Elements
	  else if ( name.startsWith("xa") ) {
	    if ( PacsElements.XA_FOV_SHAPE.equals(name) ) {
	      size = 10;
	    }
	    else if ( PacsElements.XA_FREE_TEXT.equals(name) ) {
	      size = 436;
	    }
	  }

	  // All other Elements
	  else {
	    if ( PacsElements.FILE_TYPE.equals(name) ) {
	      size = 4;
	    }
	    else if ( PacsElements.COMPR_VERSION.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.FIELD_OF_VIEW.equals(name) ) {
	      size = 16;
	    }
	    else if ( PacsElements.RID_PATH.equals(name) ) {
	      size = 40;
	    }
	    else if ( PacsElements.RID_FILENAME.equals(name) ) {
	      size = 48;
	    }
	    else if ( PacsElements.EXAM_REFERENCE_NO.equals(name) ) {
	      size = 20;
	    }
	    else if ( PacsElements.STUDY_REFERENCE_NO.equals(name) ) {
	      size = 20;
	    }
	    else if ( PacsElements.SERIES_REFERENCE_NO.equals(name) ) {
	      size = 20;
	    }
	    else if ( PacsElements.RIS_STUDY_NO.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.PACS_STUDY_NO.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.PATIENT_ID.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.PATIENT_NAME.equals(name) ) {
	      size = 48;
	    }
	    else if ( PacsElements.PATIENT_BIRTH_DATE.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.PAT_SEX.equals(name) ) {
	      size = 4;
	    }
	    else if ( PacsElements.PATIENT_HISTORY.equals(name) ) {
	      size = 80;
	    }
	    else if ( PacsElements.PATIENT_STATUS.equals(name) ) {
	      size = 4;
	    }
	    else if ( PacsElements.REASON_FOR_REQUEST.equals(name) ) {
	      size = 80;
	    }
	    else if ( PacsElements.STUDY_DESCRIPTION.equals(name) ) {
	      size = 80;
	    }
	    else if ( PacsElements.STUDY_SUBDESCRIPTION.equals(name) ) {
	      size = 80;
	    }
	    else if ( PacsElements.STUDY_ACQ_DATE.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.STUDY_ACQ_TIME.equals(name) ) {
	      size = 12;
	    }
	    else if ( PacsElements.STUDY_RADIOLOGIST.equals(name) ) {
	      size = 40;
	    }
	    else if ( PacsElements.STUDY_REF_PHYSICIAN.equals(name) ) {
	      size = 40;
	    }
	    else if ( PacsElements.STUDY_MODALITY.equals(name) ) {
	      size = 4;
	    }
	    else if ( PacsElements.STUDY_MISC_INFO.equals(name) ) {
	      size = 48;
	    }
	    else if ( PacsElements.PATIENT_PREPARATION.equals(name) ) {
	      size = 32;
	    }
	    else if ( PacsElements.INST_STATION_ID.equals(name) ) {
	      size = 24;
	    }
	    else if ( PacsElements.INST_ACQ_MANUF.equals(name) ) {
	      size = 24;
	    }
	    else if ( PacsElements.INST_ACQ_MODEL.equals(name) ) {
	      size = 24;
	    }
	    else if ( PacsElements.INST_ID.equals(name) ) {
	      size = 32;
	    }
	    else if ( PacsElements.RIS_STUDY_NO_EXT.equals(name) ) {
	      size = 40;
	    }
	    else if ( PacsElements.IMG_ORIG_ID_NO.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.FREE_TEXT.equals(name) ) {
	      size = 28;
	    }
	    else if ( PacsElements.VERSION.equals(name) ) {
	      size = 8;
	    }
	    else if ( PacsElements.PAT_ORIENTATION.equals(name) ) {
	      size = 16;
	    }
	  }

	  // Write the string value
	  _writeString( (String)value, size, outputStream );
	}

	// Element value is a Number
	else { _writeNumber( (Number)value, outputStream ); }
      }
    }

  /**
   * Writes the Number to the output stream.
   *
   * @param number Number to write to the output stream.
   * @param outputStream Image Output Stream to write to.
   *
   * @throws IOException If an error occurs during the writing.
   */
  private void _writeNumber(Number number, ImageOutputStream outputStream)
    throws IOException
    {
      // Integer
      if (number instanceof Integer) {
	outputStream.writeInt( ((Integer)number).intValue() );
      }

      // Float
      else if (number instanceof Float) {
	outputStream.writeFloat( ((Float)number).floatValue() );
      }

      // Unknown Number
      else {
	String msg = "Unable to write an Number of type \"" +
	             number.getClass().getName() + "\".";
	throw new IOException(msg);
      }
    }

  /**
   * Writes the String to the output stream.
   *
   * @param string String to write to the output stream.
   * @param length Length of the string to actually write.
   * @param outputStream Image Output Stream to write to.
   *
   * @throws IOException If an error occurs during the writing.
   */
  private void _writeString(String string, int length,
			    ImageOutputStream outputStream)
    throws IOException
    {
      // Create a byte buffer of the specified length
      byte[] buffer = new byte[length];

      // Copy the bytes of the String to the byte buffer
      byte[] stringBytes = string.getBytes();
      int min = Math.min( buffer.length, stringBytes.length );
      System.arraycopy(stringBytes, 0, buffer, 0, min);

      // Write the bytes
      outputStream.write(buffer);
    }

  /**
   * Gets the output stream.
   *
   * @return Image Output Stream to write to.
   *
   * @throws IllegalStateException If the output has not been set.
   */
  private ImageOutputStream _getOutputStream()
    {
      // No output has been set
      if (output == null) {
	String msg = "No output has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Output Stream
      return (ImageOutputStream)output;
    }
}
