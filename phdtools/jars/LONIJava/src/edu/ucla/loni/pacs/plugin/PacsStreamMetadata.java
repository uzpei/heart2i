/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.pacs.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.pacs.PacsElement;
import edu.ucla.loni.pacs.PacsElements;
import edu.ucla.loni.pacs.PacsSubheader;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for stream metadata of the UCLA PACS image format.
 *
 * @version 16 January 2009
 */
public class PacsStreamMetadata extends AppletFriendlyIIOMetadata
{
    /** PACS main Subheader containing original metadata. */
    private PacsSubheader _originalMainSubheader;

    /** PACS main Subheader containing current metadata. */
    private PacsSubheader _currentMainSubheader;

    /** PACS modality Subheader containing original metadata. */
    private PacsSubheader _originalModalitySubheader;

    /** PACS modality Subheader containing current metadata. */
    private PacsSubheader _currentModalitySubheader;

    /**
     * Constructs a PACS Stream Metadata from the given PACS Subheaders.
     *
     * @param mainHeader PACS main Subheader containing metadata.
     * @param modalityHeader PACS modality Subheader containing metadata.
     */
    public PacsStreamMetadata(PacsSubheader mainHeader,
			      PacsSubheader modalityHeader)
    {
	super(true, PacsStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      PacsStreamMetadataFormat.class.getName(),
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalMainSubheader = mainHeader;
	_currentMainSubheader = mainHeader;
	_originalModalitySubheader = modalityHeader;
	_currentModalitySubheader = modalityHeader;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return PacsMetadataConversions.toStreamTree(_currentMainSubheader,
						     _currentModalitySubheader);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current metadata, convert the root
	Node r;
	if (_currentMainSubheader == null || _currentModalitySubheader == null){
	    r = root;
	}

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Get the current metadata tree Nodes
	    Node streamNode = getAsTree(formatName);
	    Node mainNode = streamNode.getFirstChild();
	    Node modalityNode = mainNode.getNextSibling();

	    // Loop through the root Node children and merge their attributes
	    NodeList nodeList = root.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node childNode = nodeList.item(i);
		String childName = childNode.getNodeName();
		NamedNodeMap childAttrs = childNode.getAttributes();

		// Replace main Subheader attributes
		if (PacsStreamMetadataFormat.PACS_HEADER_NAME.equals(childName) ) {
		    mainNode = _getMergedNode(mainNode, childAttrs);
		}

		// Replace modality Subheader attributes
		if ( modalityNode.getNodeName().equals(childName) ) {
		    modalityNode = _getMergedNode(modalityNode, childAttrs);
		}
	    }

	    // Recreate the root tree
	    r = new IIOMetadataNode(nativeMetadataFormatName);
	    r.appendChild(mainNode);
	    r.appendChild(modalityNode);
	}

	// Set the new current main PACS Subheader
	Node r1 = r.getFirstChild();
	_currentMainSubheader = PacsMetadataConversions.toMainPacsSubheader(r1);

	// Set the new current modality PACS Subheader
	Node r2 = r1.getNextSibling();
	String name = r2.getNodeName();
	if (PacsStreamMetadataFormat.PACS_CR_SUBHEADER_NAME.equals(name)) {
	    _currentModalitySubheader =
		PacsMetadataConversions.toCrPacsSubheader(r2);
	}
	else if (PacsStreamMetadataFormat.PACS_CT_SUBHEADER_NAME.equals(name)) {
	    _currentModalitySubheader =
		PacsMetadataConversions.toCtPacsSubheader(r2);
	}
	else if (PacsStreamMetadataFormat.PACS_MR_SUBHEADER_NAME.equals(name)) {
	    _currentModalitySubheader =
		PacsMetadataConversions.toMrPacsSubheader(r2);
	}
	else if (PacsStreamMetadataFormat.PACS_XA_SUBHEADER_NAME.equals(name)) {
	    _currentModalitySubheader =
		PacsMetadataConversions.toXaPacsSubheader(r2);
	}
	else {
	    String msg = "Missing a modality Node.";
	    throw new IIOInvalidTreeException(msg, r2);
	}
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current PACS Subheaders to the originals
	_currentMainSubheader = _originalMainSubheader;
	_currentModalitySubheader = _originalModalitySubheader;
    }

    /**
     * Gets the number of images.
     *
     * @return Number of images, or -1 if it cannot be determined.
     */
    public int getNumberOfImages()
    {
	try {

	    // Get the number of images in the file
	    String name = PacsElements.NO_IMG_IN_FILE;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    Integer imageNumber = (Integer)pacsElement.getValue();

	    return imageNumber.intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the width of each image.
     *
     * @return Width of each image, or -1 if it cannot be determined.
     */
    public int getImageWidth()
    {
	try {

	    // Get the image width
	    String name = PacsElements.IMG_COLS;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    Integer imageWidth = (Integer)pacsElement.getValue();

	    return imageWidth.intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the height of each image.
     *
     * @return Height of each image, or -1 if it cannot be determined.
     */
    public int getImageHeight()
    {
	try {

	    // Get the image height
	    String name = PacsElements.IMG_ROWS;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    Integer imageHeight = (Integer)pacsElement.getValue();

	    return imageHeight.intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the number of bits per pixel used by each image.
     *
     * @return Number of bits per pixel used by each image, or -1 if it cannot
     *         be determined.
     */
    public int getBitsPerPixel()
    {
	try {

	    // Use the modality to determine the number of bits per pixel
	    String modality = getModality();
	    if ( modality.equals("CT") || modality.equals("MR") ||
		 modality.equals("LS") || modality.equals("XA") )
		{
		    return 12;
		}

	    else if ( modality.equals("CR") ) { return 10; }

	    // Unknown type
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the modality of the images.
     *
     * @return Modality of the images, or null if it cannot be determined.
     */
    public String getModality()
    {
	try {
	    String name = PacsElements.STUDY_MODALITY;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    return (String)pacsElement.getValue();
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the scanner manufacturer.
     *
     * @return Manufacturer, or null if it cannot be determined.
     */
    public String getManufacturer()
    {
	try {
	    String name = PacsElements.INST_ACQ_MANUF;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    return (String)pacsElement.getValue();
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// Only support non-inverted grayscale
	return Utilities.getGrayScaleChromaNode(false);
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	int bitsPerPixel = getBitsPerPixel();

	// No information available
	if (bitsPerPixel == -1) { return null; }

	// Otherwise support grayscale data
	return Utilities.getGrayScaleDataNode(bitsPerPixel);
    }

    /**
     * Returns an IIOMetadataNode representing the dimension format information
     * of the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the dimension information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Get the X pixel size
	    String name = PacsElements.X_PIX_SIZE;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    float pixelWidth = ((Float)pacsElement.getValue()).floatValue();

	    // Get the Y pixel size
	    name = PacsElements.Y_PIX_SIZE;
	    pacsElement = _currentMainSubheader.getPacsElement(name);
	    float pixelHeight = ((Float)pacsElement.getValue()).floatValue();

	    // Return the dimension node
	    return Utilities.getDimensionNode(pixelWidth, pixelHeight);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	// Image dimensions
	int imageWidth = getImageWidth();
	int imageHeight = getImageHeight();
	int numberOfImages = getNumberOfImages();

	// Pixel dimensions and slice thickness
	float pixelWidth = 0;
	float pixelHeight = 0;
	float sliceThickness = 0;
	try {

	    // Get the X pixel size
	    String name = PacsElements.X_PIX_SIZE;
	    PacsElement pacsElement=_currentMainSubheader.getPacsElement(name);
	    pixelWidth = ((Float)pacsElement.getValue()).floatValue();

	    // Get the Y pixel size
	    name = PacsElements.Y_PIX_SIZE;
	    pacsElement = _currentMainSubheader.getPacsElement(name);
	    pixelHeight = ((Float)pacsElement.getValue()).floatValue();

	    // Get the slice thickness
	    name = PacsElements.STUDY_SLICE_THICKNESS;
	    pacsElement = _currentMainSubheader.getPacsElement(name);
	    sliceThickness = ((Float)pacsElement.getValue()).floatValue();
	}
	catch (Exception e) {}

	// Bits per pixel
	int bitsPerPixel = getBitsPerPixel();

	// Data type
	String dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE;

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;

	// Return the basic tree based upon available information
	if (pixelWidth == 0 && pixelHeight == 0 && sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, dataType,
						  bitsPerPixel, colorType);
	}
	if (sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, pixelWidth,
						  pixelHeight, dataType,
						  bitsPerPixel, colorType);
	}
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }

    /**
     * Merges the attributes with the specified Node.  If there are attribute
     * names not present in the attributes of the node, the attributes are
     * ignored.
     *
     * @param node Node to merge the attributes with.
     * @param attributes Attributes to merge.
     *
     * @return New Node containing the merged attributes.
     */
    private Node _getMergedNode(Node node, NamedNodeMap attributes)
    {
	// Create a new Node with the same name
	IIOMetadataNode mergedNode = new IIOMetadataNode( node.getNodeName() );

	// Copy the Node attributes to the new Node
	NamedNodeMap map = node.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		mergedNode.setAttribute(attr.getNodeName(),attr.getNodeValue());
	    }
	}

	// Merge the new attributes with the old
	if (attributes != null) {
	    for (int i = 0; i < attributes.getLength(); i++) {
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName();

		// Attribute name must be present in the new Node
		if ( mergedNode.hasAttribute(attributeName) ) {
		    mergedNode.setAttribute(attributeName,
					    attribute.getNodeValue());
		}
	    }
	}

	// Return the new merged Node
	return mergedNode;
    }
}
