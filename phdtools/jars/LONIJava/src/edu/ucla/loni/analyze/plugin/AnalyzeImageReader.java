/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.analyze.plugin;

import edu.ucla.loni.Conversions;
import edu.ucla.loni.analyze.AnalyzeHeader;
import edu.ucla.loni.analyze.DataHistory;
import edu.ucla.loni.analyze.HeaderKey;
import edu.ucla.loni.analyze.ImageDimension;
import edu.ucla.loni.imageio.Utilities;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for parsing and decoding ANALYZE 7.5 images.
 *
 * @version 20 January 2009
 */
public class AnalyzeImageReader extends ImageReader
{
    /** Analyze Header decoded from the input source. */
    private AnalyzeHeader _analyzeHeader;

    /** True if the encoded ANALYZE Header is byte swapped; false otherwise. */
    private boolean _isByteSwapped;

    /** True if the pixel data is checked for GZIP compression; false o/w. */
    private boolean _isGZipChecked;

    /** True if the pixel data is GZIP compressed; false otherwise. */
    private boolean _isGZipped;

    /**
     * Constructs an AnalyzeImageReader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not a
     *                                  Analyze Image Reader Spi.
     */
    public AnalyzeImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be a Analyze Image Reader Spi
	if ( !(originatingProvider instanceof AnalyzeImageReaderSpi) ) {
	    String msg = "Originating provider must be an Analyze Image " +
		"Reader SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The ImageInputStream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of the classes returned by the
     *                                  originating service provider's
     *                                  getInputTypes method, or is not an
     *                                  ImageInputStream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	// Convert the input source into an Analyze Input Stream
	input = AnalyzeInputStream.getAnalyzeInputStream(input);

	super.setInput(input, seekForwardOnly, ignoreMetadata);

	// Remove any decoded Analyze Header
	_analyzeHeader = null;
	_isGZipChecked = false;
    }

    /**
     * Returns the number of images, not including thumbnails, available from
     * the current input source.
     * 
     * @param allowSearch If true, the true number of images will be returned
     *                    even if a search is required.  If false, the reader
     *                    may return -1 without performing the search.
     *
     * @return The number of images or -1 if allowSearch is false and a search
     *         would be required.
     *
     * @throws IllegalStateException If the input source has not been set.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    public int getNumImages(boolean allowSearch) throws IOException
    {
	AnalyzeMetadata metadata = new AnalyzeMetadata( _getAnalyzeHeader() );
	int numberOfImages = metadata.getNumberOfImages();

	// Unable to determine the number of images
	if (numberOfImages == -1) {
	    String msg = "Unable to determine the number of images.";
	    throw new IOException(msg);
	}

	return numberOfImages;
    }

    /**
     * Returns the width in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The width of the image in pixels.
     *
     * @throws IOException If an error occurs reading the width information from
     *                     the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getWidth(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image width
	AnalyzeMetadata metadata = new AnalyzeMetadata( _getAnalyzeHeader() );
	int width = metadata.getImageWidth();

	// Unable to determine the image width
	if (width == -1) {
	    String msg = "Unable to determine the image width.";
	    throw new IOException(msg);
	}

	return width;
    }

    /**
     * Returns the height in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The height of the image in pixels.
     *
     * @throws IOException If an error occurs reading the height information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getHeight(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image height
	AnalyzeMetadata metadata = new AnalyzeMetadata( _getAnalyzeHeader() );
	int height = metadata.getImageHeight();

	// Unable to determine the image height
	if (height == -1) {
	    String msg = "Unable to determine the image height.";
	    throw new IOException(msg);
	}

	return height;
    }

    /**
     * Returns an Iterator containing possible image types to which the given
     * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
     * legal image type will be returned.
     *
     * @param imageIndex The index of the image to be retrieved.
     *
     * @return An Iterator containing at least one ImageTypeSpecifier
     *         representing suggested image types for decoding the current given
     *         image.
     *
     * @throws IOException If an error occurs reading the format information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public Iterator getImageTypes(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Get the image parameters
	AnalyzeMetadata metadata = new AnalyzeMetadata( _getAnalyzeHeader() );
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);
	int bitsPerPixel = metadata.getBitsPerPixel();
	int type = metadata.getDataType();

	// Color images
	ArrayList list = new ArrayList(1);
	if ( metadata.hasColorImages() ) {
	    list.add( Utilities.getRgbImageType(width, height) );
	    return list.iterator();
	}

	// Determine the appropriate Image Type Specifier
	if (bitsPerPixel == 8 || bitsPerPixel == 16) {
	    list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
	}
	else if (type == 8 && bitsPerPixel == 32) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_INT) );
	}
	else if (type == 16 && bitsPerPixel == 32) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_FLOAT) );
	}
	else if (type == 64 && bitsPerPixel == 64) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_DOUBLE) );
	}

	// No data type recognized; try to infer it
	if (list.size() == 0) {

	    // Read the total pixel data size
	    long pixelDataSize = 0;
	    try {
		AnalyzeInputStream ais = (AnalyzeInputStream)input;
		ImageInputStream imageStream = ais.getPixelDataInputStream();
		pixelDataSize = imageStream.length();
	    }
	    catch (Exception e) {}

	    // Try to infer the number of bytes per voxel
	    if (pixelDataSize > 0) {
		int numOfImages = metadata.getNumberOfImages();

		// Match the volume to the file size
		long volume = width*height*numOfImages;
		if (volume == pixelDataSize) {
		    list.add( Utilities.getGrayImageType(width, height, 8) );
		}
		else if (2*volume == pixelDataSize) {
		    list.add( Utilities.getGrayImageType(width, height, 16) );
		}
	    }
	}

	// Check if the image data type is supported
	if (list.size() == 0) {
	    String msg = "Unsupported ANALYZE image type:  data type = " +
		type + "; bits per voxel = " + bitsPerPixel + ".";
	    throw new IOException(msg);
	}

	// Return the Image Type Specifier
	return list.iterator();
    }

    /**
     * Returns an IIOMetadata object representing the metadata associated with
     * the input source as a whole (i.e., not associated with any particular
     * image), or null if the reader does not support reading metadata, is set
     * to ignore metadata, or if no metadata is available.
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     */
    public IIOMetadata getStreamMetadata() throws IOException
    {
	return new AnalyzeMetadata( _getAnalyzeHeader() );
    }

    /**
     * Returns an IIOMetadata object containing metadata associated with the
     * given image, or null if the reader does not support reading metadata, is
     * set to ignore metadata, or if no metadata is available.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved. 
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
	// Not applicable
	return null;
    }

    /**
     * Reads the image indexed by imageIndex and returns it as a complete
     * BufferedImage.  The supplied ImageReadParam is ignored.
     *
     * @param imageIndex The index of the image to be retrieved.
     * @param param An ImageReadParam used to control the reading process, or
     *              null.
     *
     * @return The desired portion of the image as a BufferedImage.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public BufferedImage read(int imageIndex, ImageReadParam param)
	throws IOException
    {
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// Update the Listeners
	processImageStarted(imageIndex);

	// Compute the size of the image in bytes
	AnalyzeMetadata metadata = new AnalyzeMetadata( _getAnalyzeHeader() );
	ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
	int dataType = spec.getSampleModel().getDataType();
	int imageSize = width*height;

	if ( metadata.hasColorImages() ) { imageSize *= 3; }
	else if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }
	else if (dataType == DataBuffer.TYPE_INT) { imageSize *= 4; }
	else if (dataType == DataBuffer.TYPE_FLOAT) { imageSize *= 4; }
	else if (dataType == DataBuffer.TYPE_DOUBLE) { imageSize *= 8; }

	// Beginning of the image pixel data
	long imagePos = imageIndex*imageSize;

	// Return the raw image data if required
	if ( Utilities.returnRawBytes(param) ) {
	    BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	    DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	    byte[] data = ((DataBufferByte)db).getData();
	    _readPixelData(imagePos, data);

	    // Byte swap the data if the header is byte swapped
	    if (_isByteSwapped) { _byteSwap(data, dataType); }

	    return bufferedImage;
	}

	// Create a Buffered Image for the image data
	Iterator iter = getImageTypes(0);
	BufferedImage bufferedImage = getDestination(null, iter, width, height);
	DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

	// Read the image data into an array
	byte[] srcBuffer = new byte[imageSize];
	_readPixelData(imagePos, srcBuffer);

	// RGB color (3-byte) image pixel data
	WritableRaster dstRaster = bufferedImage.getRaster();
	if ( metadata.hasColorImages() ) {
	    bufferedImage = Utilities.getRgbImage(srcBuffer, dstRaster, true);
	}

	// 8-bit gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	    bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
	}

	// 16-bit gray image pixel data (byte swap if necessary)
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_USHORT) {
	    bufferedImage =
		Utilities.getGrayUshortImage(srcBuffer, dstRaster, 16, false,
					     _isByteSwapped, 1, 0);
	}

	// General gray image pixel data (byte swap if necessary)
	else {
	    bufferedImage = Utilities.getGrayImage(srcBuffer, dstRaster,
						   _isByteSwapped);
	}

	// Update the Listeners
	if ( abortRequested() ) { processReadAborted(); }
	else { processImageComplete(); }

	// Return the requested image
	return bufferedImage;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageReader.  Otherwise, the reader may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the input stream
	try { if (input != null) { ((AnalyzeInputStream)input).close(); } }
	catch (Exception e) {}

	input = null;
    }

    /**
     * Reads the Analyze Header from the input source.  If the Analyze Header
     * has already been decoded, no additional read is performed.
     *
     * @return Analyze Header decoded from the input source.
     * 
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     */
    private AnalyzeHeader _getAnalyzeHeader() throws IOException
    {
	// Decode the Analyze Header if needed
	if (_analyzeHeader == null) {

	    // No input has been set
	    if (input == null) {
		String msg = "No input has been set.";
		throw new IllegalStateException(msg);
	    }

	    // Get the metadata Image Input Stream
	    AnalyzeInputStream inputStream = (AnalyzeInputStream)input;
	    ImageInputStream imageStream = inputStream.getMetadataInputStream();

	    // Read the header size
	    imageStream.mark();
	    int sizeOfHdr = imageStream.readInt();
	    imageStream.reset();

	    // Determine if bytes should be swapped; find the header size
	    _isByteSwapped = false;
	    if (sizeOfHdr == 0x94000000) {
		_isByteSwapped = true;
		sizeOfHdr = 148;
	    }
	    if (sizeOfHdr == 0x5C010000) {
		_isByteSwapped = true;
		sizeOfHdr = 348;
	    }

	    // Read the encoded Analyze header into a byte array
	    byte[] headerBytes = new byte[sizeOfHdr];
	    imageStream.readFully(headerBytes);

	    // Create a new Image Input Stream that reads from the bytes
	    ByteArrayInputStream bStream =new ByteArrayInputStream(headerBytes);
	    MemoryCacheImageInputStream mStream =
		new MemoryCacheImageInputStream(bStream);

	    // Account for byte swapping if needed
	    if (_isByteSwapped) {mStream.setByteOrder(ByteOrder.LITTLE_ENDIAN);}

	    // Read Header Key elements
	    byte[] dataType = new byte[10];
	    byte[] dbName = new byte[18];

	    sizeOfHdr = mStream.readInt();
	    mStream.readFully(dataType);
	    mStream.readFully(dbName);
	    int extents = mStream.readInt();
	    short sessionError = mStream.readShort();
	    byte regular = mStream.readByte();
	    byte hKeyUn0 = mStream.readByte();

	    // Construct the Header Key
	    HeaderKey headerKey =
		new HeaderKey(sizeOfHdr, dataType, dbName, extents,
			      sessionError, regular, hKeyUn0);

	    // Read Image Dimension elements
	    short[] dim = new short[8];
	    byte[] voxUnits = new byte[4];
	    byte[] calUnits = new byte[8];
	    short[] shorts = new short[4];
	    float[] pixDim = new float[8];
	    float[] floats = new float[8];

	    mStream.readFully(dim, 0, dim.length);
	    mStream.readFully(voxUnits);
	    mStream.readFully(calUnits);
	    mStream.readFully(shorts, 0, shorts.length);
	    mStream.readFully(pixDim, 0, pixDim.length);
	    mStream.readFully(floats, 0, floats.length);
	    int glMax = mStream.readInt();
	    int glMin = mStream.readInt();

	    // Construct the Image Dimension
	    ImageDimension imageDimension =
		new ImageDimension(dim[0], dim[1], dim[2], dim[3], dim[4],
				   dim[5], dim[6], dim[7], voxUnits, calUnits,
				   shorts[0], shorts[1], shorts[2], shorts[3],
				   pixDim[0], pixDim[1], pixDim[2], pixDim[3],
				   pixDim[4], pixDim[5], pixDim[6], pixDim[7],
				   floats[0], floats[1], floats[2], floats[3],
				   floats[4], floats[5], floats[6], floats[7],
				   glMax, glMin);

	    // Read Data History elements if present
	    DataHistory dataHistory = null;
	    if (sizeOfHdr == 348) {
		byte[] descrip = new byte[80];
		byte[] auxFile = new byte[24];
		byte[] originator = new byte[10];
		byte[] generated = new byte[10];
		byte[] scannum = new byte[10];
		byte[] patientId = new byte[10];
		byte[] expDate = new byte[10];
		byte[] expTime = new byte[10];
		byte[] histUn0 = new byte[3];
		int[] ints = new int[8];

		mStream.readFully(descrip);
		mStream.readFully(auxFile);
		byte orient = mStream.readByte();

		// Try to convert the originator bytes into a hex string
		mStream.readFully(originator);
		try {
		    String string = Conversions.toHexString(originator);
		    originator = string.getBytes();
		}

		// If not possible, treat as normal
		catch (Exception e) {}

		mStream.readFully(generated);
		mStream.readFully(scannum);
		mStream.readFully(patientId);
		mStream.readFully(expDate);
		mStream.readFully(expTime);
		mStream.readFully(histUn0);
		mStream.readFully(ints, 0, ints.length);

		// Construct the Data History
		dataHistory =
		    new DataHistory(descrip, auxFile, orient, originator,
				    generated, scannum, patientId, expDate,
				    expTime, histUn0,
				    ints[0], ints[1], ints[2], ints[3], ints[4],
				    ints[5], ints[6], ints[7]);
	    }

	    // Construct the Analyze Header
	    _analyzeHeader = new AnalyzeHeader(headerKey, imageDimension,
					       dataHistory);
	}

	return _analyzeHeader;
    }

    /**
     * Checks whether or not the specified image index is valid.
     *
     * @param imageIndex The index of the image to check.
     *
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private void _checkImageIndex(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	if ( imageIndex >= getNumImages(false) ) {
	    String msg = "Image index of " + imageIndex + " >= the total " +
		"number of images (" + getNumImages(false) + ")";
	    throw new IndexOutOfBoundsException(msg);
	}
    }

    /**
     * Reads bytes of pixel data.
     *
     * @param pos Position in the pixel data stream to start reading at.
     * @param b Byte array to fill with pixel data.
     *
     * @throws IOException If an error occurs while reading from the pixel data
     *                     stream.
     * @throws IllegalStateException If the input has not been set.
     */
    private void _readPixelData(long pos, byte[] b) throws IOException
    {
	// No input has been set
	if (input == null) {
	    String msg = "No input has been set.";
	    throw new IllegalStateException(msg);
	}

	// Get the Image Input Stream for image pixel data
	AnalyzeInputStream ais = (AnalyzeInputStream)input;
	ImageInputStream imageStream = ais.getPixelDataInputStream();

	// Check for GZIP compression
	if (!_isGZipChecked) {
	    _isGZipChecked = true;

	    // Attempt to read a byte assuming GZIP compression
	    try {
		InputStream inputStream = new InputStreamAdapter(imageStream);
		GZIPInputStream gStream = new GZIPInputStream(inputStream);
		gStream.read();

		// Successful read
		_isGZipped = true;
	    }
	    catch (Exception e) { _isGZipped = false; }

	    // Reset the stream
	    imageStream.seek(0);
	}

	// Pixel data is GZIP compressed
	if (_isGZipped) {
	    imageStream.seek(0);
	    InputStream inputStream = new InputStreamAdapter(imageStream);
	    GZIPInputStream gStream = new GZIPInputStream(inputStream);

	    // Seek to the stream position
	    long bytesSeeked = 0;
	    while (bytesSeeked < pos) {
		long skipped = gStream.skip(pos - bytesSeeked);
		bytesSeeked += skipped;
	    }

	    // Read pixel data into the array
	    DataInputStream dStream = new DataInputStream(gStream);
	    dStream.readFully(b);
	    return;
	}

	// Pixel data is not compressed
	imageStream.seek(pos);
	imageStream.readFully(b);
    }

    /**
     * Byte swaps the specified array.
     *
     * @param b Byte array to swap.
     * @param type Data type to swap.
     */
    private void _byteSwap(byte[] b, int type)
    {
	// Byte swap shorts
	if (type == DataBuffer.TYPE_USHORT) {
	    for (int i = 0; i < b.length; i+=2) {
		byte b1 = b[i];

		b[i] = b[i+1];
		b[i+1] = b1;
	    }
	}

	// Byte swap integers or floats
	else if (type == DataBuffer.TYPE_INT || type == DataBuffer.TYPE_FLOAT) {
	    for (int i = 0; i < b.length; i+=4) {
		byte b1 = b[i];
		byte b2 = b[i+1];

		b[i] = b[i+3];
		b[i+1] = b[i+2];
		b[i+2] = b2;
		b[i+3] = b1;
	    }
	}

	// Byte swap doubles
	else if (type == DataBuffer.TYPE_DOUBLE) {
	    for (int i = 0; i < b.length; i+=8) {
		byte b1 = b[i];
		byte b2 = b[i+1];
		byte b3 = b[i+2];
		byte b4 = b[i+3];

		b[i] = b[i+7];
		b[i+1] = b[i+6];
		b[i+2] = b[i+5];
		b[i+3] = b[i+4];
		b[i+4] = b4;
		b[i+5] = b3;
		b[i+6] = b2;
		b[i+7] = b1;
	    }
	}
    }
}
