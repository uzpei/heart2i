/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.analyze.plugin;

import edu.ucla.loni.imageio.FileNameAssociator;

/**
 * File Name Associator for generating file name associations between ANALYZE
 * files.
 *
 * @version 25 May 2007
 */
public class AnalyzeNameAssociator extends FileNameAssociator
{
    /** Constructs an ANALYZE Name Associator. */
    public AnalyzeNameAssociator()
    {
    }

    /**
     * Determines whether or not this file contains metadata or image pixel
     * data.
     *
     * @return True if this file contains metadata, false otherwise.
     */
    public boolean isMetadataFile(String fileName)
    {
	// No name 
	if (fileName == null) { return false; }

	// ANALYZE metadata files end with ".hdr"
	return fileName.toLowerCase().endsWith(".hdr");
    }

    /**
     * Gets all the allowed file names that can be associated with the specified
     * file name.
     *
     * @param fileName Name of the file to get associated file names for.
     *
     * @return All the allowed file names that can be associated with the
     *         specified file name.  An empty array is returned if there are no
     *         associated file names.
     */
    public String[] getAssociatedFileNames(String fileName)
    {
	// No name 
	if (fileName == null) { return new String[0]; }

	// File is the metadata file
	if ( isMetadataFile(fileName) ) {
	    String[] names1 = _getSuffixReplacements(fileName, "hdr", "img");
	    String[] names2 = _getSuffixReplacements(fileName, "hdr", "img.gz");

	    String[] names = new String[names1.length + names2.length];
	    System.arraycopy(names1, 0, names, 0, names1.length);
	    System.arraycopy(names2, 0, names, names1.length, names2.length);
	    return names;
	}

	// File is the image pixel file
	else if ( fileName.toLowerCase().endsWith(".img") ) {
	    return _getSuffixReplacements(fileName, "img", "hdr");
	}

	// File is a GZIP compressed image pixel file
	else if ( fileName.toLowerCase().endsWith(".img.gz") ) {
	    return _getSuffixReplacements(fileName, "img.gz", "hdr");
	}

	// Not an ANALYZE file
	return new String[0];
    }
}
