/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze;

/**
 * Analyze Subheader that describes the image sizes.
 *
 * @version 27 November 2002
 */
public class ImageDimension extends AnalyzeSubheader
{
  /** ID of the number of dimensions in the file. */
  public static final String DIM_0 = "dim_0";

  /** ID of the image X dimension; number of pixels in an image row. */
  public static final String DIM_1 = "dim_1";

  /** ID of the image Y dimension; number of pixel rows in a slice. */
  public static final String DIM_2 = "dim_2";

  /** ID of the image Z dimension; number of slices in a volume. */
  public static final String DIM_3 = "dim_3";

  /** ID of the time points; number of volumes in the file. */
  public static final String DIM_4 = "dim_4";

  /** ID of the fifth file dimension. */
  public static final String DIM_5 = "dim_5";

  /** ID of the sixth file dimension. */
  public static final String DIM_6 = "dim_6";

  /** ID of the seventh file dimension. */
  public static final String DIM_7 = "dim_7";

  /** ID of the spatial units of measure for a voxel. */
  public static final String VOX_UNITS = "vox_units";

  /** ID of the name of the calibration unit. */
  public static final String CAL_UNITS = "cal_units";

  /** ID of the unused element. */
  public static final String UNUSED_1 = "unused1";

  /** ID of the data type for this image set. */
  public static final String DATA_TYPE = "dataType";

  /** ID of the number of bits per pixel. */
  public static final String BIT_PIX = "bitpix";

  /** ID of the Un0 unused element. */
  public static final String DIM_UN0 = "dim_un0";

  /** ID of the Dim 0 unused element. */
  public static final String PIX_DIM_0 = "pixdim_0";

  /** ID of the voxel width in millimeters. */
  public static final String PIX_DIM_1 = "pixdim_1";

  /** ID of the voxel height in millimeters. */
  public static final String PIX_DIM_2 = "pixdim_2";

  /** ID of the slice thickness in millimeters. */
  public static final String PIX_DIM_3 = "pixdim_3";

  /** ID of the time in milliseconds. */
  public static final String PIX_DIM_4 = "pixdim_4";

  /** ID of the real world measurement of the fifth file dimension. */
  public static final String PIX_DIM_5 = "pixdim_5";

  /** ID of the real world measurement of the sixth file dimension. */
  public static final String PIX_DIM_6 = "pixdim_6";

  /** ID of the real world measurement of the seventh file dimension. */
  public static final String PIX_DIM_7 = "pixdim_7";

  /** ID of the byte offset in the corresponding '.img' file. */
  public static final String VOX_OFFSET = "vox_offset";

  /** ID of the region-of-interest scale. */
  public static final String ROI_SCALE = "roi_scale";

  /** ID of the first float unused element. */
  public static final String F_UNUSED_1 = "funused1";

  /** ID of the second float unused element. */
  public static final String F_UNUSED_2 = "funused2";

  /** ID of the maximum of the range of calibration values. */
  public static final String CAL_MAX = "cal_max";

  /** ID of the minimum of the range of calibration values. */
  public static final String CAL_MIN = "cal_min";

  /** ID of the compressed. */
  public static final String COMPRESSED = "compressed";

  /** ID of the verified. */
  public static final String VERIFIED = "verified";

  /** ID of the maximum pixel value for the entire file. */
  public static final String GL_MAX = "glmax";

  /** ID of the minimum pixel value for the entire file. */
  public static final String GL_MIN = "glmin";

  /**
   * Constructs an ImageDimension.
   *
   * @param dim0 Number of dimensions in the file (usually 4).
   * @param dim1 Image X dimension; number of pixels in an image row.
   * @param dim2 Image Y dimension; number of pixel rows in a slice.
   * @param dim3 Image Z dimension; number of slices in a volume.
   * @param dim4 Time points; number of volumes in the file.
   * @param dim5 Fifth file dimension.
   * @param dim6 Sixth file dimension.
   * @param dim7 Seventh file dimension.
   * @param voxUnits The spatial units of measure for a voxel.  This must be 4
   *                 bytes or less.
   * @param calUnits The name of the calibration unit.  This must be 8 bytes or
   *                 less.
   * @param unused1 Unused element.
   * @param dataType Data type for this image set.
   * @param bitPix Number of bits per pixel.
   * @param dimUn0 Unused element.
   * @param pixDim0 Unused element.
   * @param pixDim1 Voxel width in millimeters.
   * @param pixDim2 Voxel height in millimeters.
   * @param pixDim3 Slice thickness in millimeters.
   * @param pixDim4 Time in milliseconds.
   * @param pixDim5 Real world measurement of the fifth file dimension.
   * @param pixDim6 Real world measurement of the sixth file dimension.
   * @param pixDim7 Real world measurement of the seventh file dimension.
   * @param voxOffset Byte offset in the corresponding '.img' file at which
   *                  the voxels start.
   * @param roiScale Region-of-interest scale.
   * @param fUnused1 Unused element.
   * @param fUnused2 Unused element.
   * @param calMax Maximum of the range of calibration values.
   * @param calMin Minimum of the range of calibration values.
   * @param compressed Compressed.
   * @param verified Verified.
   * @param glMax The maximum pixel value for the entire file.
   * @param glMin The minimum pixel value for the entire file.
   *
   * @throws IllegalArgumentException If any byte array is larger than its
   *                                  specified maximum length.
   */
  public ImageDimension(short dim0, short dim1, short dim2, short dim3,
			short dim4, short dim5, short dim6, short dim7,
			byte[] voxUnits, byte[] calUnits, short unused1,
			short dataType, short bitPix, short dimUn0,
			float pixDim0, float pixDim1, float pixDim2,
			float pixDim3, float pixDim4, float pixDim5,
			float pixDim6, float pixDim7, float voxOffset,
			float roiScale, float fUnused1, float fUnused2,
			float calMax, float calMin, float compressed,
			float verified, int glMax, int glMin)
    {
      super(32);

      _addElement(DIM_0, new Short(dim0));
      _addElement(DIM_1, new Short(dim1));
      _addElement(DIM_2, new Short(dim2));
      _addElement(DIM_3, new Short(dim3));
      _addElement(DIM_4, new Short(dim4));
      _addElement(DIM_5, new Short(dim5));
      _addElement(DIM_6, new Short(dim6));
      _addElement(DIM_7, new Short(dim7));
      _addElement(VOX_UNITS, _createString(voxUnits));
      _addElement(CAL_UNITS, _createString(calUnits));
      _addElement(UNUSED_1, new Short(unused1));
      _addElement(DATA_TYPE, new Short(dataType));
      _addElement(BIT_PIX, new Short(bitPix));
      _addElement(DIM_UN0, new Short(dimUn0));
      _addElement(PIX_DIM_0, new Float(pixDim0));
      _addElement(PIX_DIM_1, new Float(pixDim1));
      _addElement(PIX_DIM_2, new Float(pixDim2));
      _addElement(PIX_DIM_3, new Float(pixDim3));
      _addElement(PIX_DIM_4, new Float(pixDim4));
      _addElement(PIX_DIM_5, new Float(pixDim5));
      _addElement(PIX_DIM_6, new Float(pixDim6));
      _addElement(PIX_DIM_7, new Float(pixDim7));
      _addElement(VOX_OFFSET, new Float(voxOffset));
      _addElement(ROI_SCALE, new Float(roiScale));
      _addElement(F_UNUSED_1, new Float(fUnused1));
      _addElement(F_UNUSED_2, new Float(fUnused2));
      _addElement(CAL_MAX, new Float(calMax));
      _addElement(CAL_MIN, new Float(calMin));
      _addElement(COMPRESSED, new Float(compressed));
      _addElement(VERIFIED,  new Float(verified));
      _addElement(GL_MAX, new Integer(glMax));
      _addElement(GL_MIN, new Integer(glMin));
    }
}
