/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze;

import java.util.Vector;

/**
 * Analyze Subheader that describes the history of the data.
 *
 * @version 13 December 2002
 */
public class DataHistory extends AnalyzeSubheader
{
  /** ID of the description. */
  public static final String DESCRIP = "descrip";

  /** ID of the auxilary file name. */
  public static final String AUX_FILE = "aux_file";

  /** ID of the slice orientation for this dataset. */
  public static final String ORIENT = "orient";

  /** ID of the originator. */
  public static final String ORIGINATOR = "originator";

  /** ID of the generated. */
  public static final String GENERATED = "generated";

  /** ID of the number of the scan. */
  public static final String SCAN_NUM = "scannum";

  /** ID of the id of the patient. */
  public static final String PATIENT_ID = "patient_id";

  /** ID of the date. */
  public static final String EXP_DATE = "exp_date";

  /** ID of the time. */
  public static final String EXP_TIME = "exp_time";

  /** ID of the unused element. */
  public static final String HIST_UN0 = "hist_un0";

  /** ID of the views. */
  public static final String VIEWS = "views";

  /** ID of the volumes added. */
  public static final String VOLS_ADDED = "vols_added";

  /** ID of the start field. */
  public static final String START_FIELD = "start_field";

  /** ID of the skip field. */
  public static final String FIELD_SKIP = "field_skip";

  /** ID of the o maximum. */
  public static final String O_MAX = "omax";

  /** ID of the o minimum. */
  public static final String O_MIN = "omin";

  /** ID of the s maximum. */
  public static final String S_MAX = "smax";

  /** ID of the s minimum. */
  public static final String S_MIN = "smin";

  /**
   * Constructs a DataHistory.
   *
   * @param descrip Description.  This must be 80 bytes or less.
   * @param auxFile Auxilary file name.  This must be 24 bytes or less.
   * @param orient Slice orientation for this dataset.
   * @param originator Originator.  This must be 10 bytes or less.
   * @param generated Generated.  This must be 10 bytes or less.
   * @param scannum Number of the scan.  This must be 10 bytes or less.
   * @param patientId Id of the patient.  This must be 10 bytes or less.
   * @param expDate Date.  This must be 10 bytes or less.
   * @param expTime Time.  This must be 10 bytes or less.
   * @param histUn0 Unused element.  This must be 3 bytes or less.
   * @param views Views.
   * @param volsAdded Volumes added.
   * @param startField Start Field.
   * @param fieldSkip Skip Field.
   * @param omax O maximum.
   * @param omin O minimum.
   * @param smax S maximum.
   * @param smin S minimum.
   *
   * @throws IllegalArgumentException If any byte array is larger than its
   *                                  specified maximum length.
   */
  public DataHistory(byte[] descrip, byte[] auxFile, byte orient,
		     byte[] originator, byte[] generated, byte[] scannum,
		     byte[] patientId, byte[] expDate, byte[] expTime,
		     byte[] histUn0, int views, int volsAdded, int startField,
		     int fieldSkip, int omax, int omin, int smax, int smin)
    {
      super(18);

      _addElement(DESCRIP, _createString(descrip));
      _addElement(AUX_FILE, _createString(auxFile));
      _addElement(ORIENT, new Byte(orient));
      _addElement(ORIGINATOR, _createString(originator));
      _addElement(GENERATED, _createString(generated));
      _addElement(SCAN_NUM, _createString(scannum));
      _addElement(PATIENT_ID, _createString(patientId));
      _addElement(EXP_DATE, _createString(expDate));
      _addElement(EXP_TIME, _createString(expTime));
      _addElement(HIST_UN0, _createString(histUn0));
      _addElement(VIEWS, new Integer(views));
      _addElement(VOLS_ADDED, new Integer(volsAdded));
      _addElement(START_FIELD, new Integer(startField));
      _addElement(FIELD_SKIP, new Integer(fieldSkip));
      _addElement(O_MAX, new Integer(omax));
      _addElement(O_MIN, new Integer(omin));
      _addElement(S_MAX, new Integer(smax));
      _addElement(S_MIN, new Integer(smin));
    }
}
