/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.analyze.plugin;

import java.io.IOException;
import java.io.InputStream;
import javax.imageio.stream.ImageInputStream;

/**
 * Class that adapts the Image Input Stream interface into the Input Stream
 * class.
 *
 * @version 25 May 2007
 */
public class InputStreamAdapter extends InputStream
{
    /** Image Input Stream to adapt. */
    private ImageInputStream _imageInputStream;

    /** Stream position beyond which a mark becomes invalid. */
    private long _markLimitPosition;

    /**
     * Creates an InputStreamAdapter.
     *
     * @param imageInputStream Image Input Stream to adapt.
     *
     * @throws NullPointerException If the Image Input Stream is null.
     */
    public InputStreamAdapter(ImageInputStream imageInputStream)
    {
	super();

	// Check for a null stream
	if (imageInputStream == null) {
	    String msg = "A null Image Input Stream is not allowed.";
	    throw new NullPointerException(msg);
	}

	_imageInputStream = imageInputStream;

	// No mark has been set
	_markLimitPosition = -1;
    }

    /**
     * Reads the next byte of data from the input stream.
     *
     * @return The next byte of data, or -1 if the end of the stream is reached.
     *
     * @throws IOException If an I/O error occurs.
     */
    public int read() throws IOException
    {
	return _imageInputStream.read();
    }

    /**
     * Reads up to len bytes of data from the input stream into an array of
     * bytes.
     *
     * @param b The buffer into which the data is read.
     * @param off The start offset in array b at which the data is written.
     * @param len The maximum number of bytes to read.
     *
     * @return The total number of bytes read into the buffer, or -1 if there is
     *         no more data because the end of the stream has been reached.
     *
     * @throws IOException If an I/O error occurs.
     */
    public int read(byte b[], int off, int len) throws IOException
    {
	return _imageInputStream.read(b, off, len);
    }

    /**
     * Skips over and discards <code>n</code> bytes of data from this input
     * stream.
     *
     * @param n The number of bytes to be skipped.
     *
     * @return The actual number of bytes skipped.
     *
     * @throws IOException If an I/O error occurs.
     */
    public long skip(long n) throws IOException
    {
	return _imageInputStream.skipBytes(n);
    }

    /**
     * Closes this input stream and releases any system resources associated
     * with the stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void close() throws IOException
    {
	_imageInputStream.close();
    }

    /**
     * Marks the current position in this input stream.  A subsequent call to
     * the reset method repositions this stream at the last marked position so
     * that subsequent reads re-read the same bytes.
     *
     * @param readlimit The maximum limit of bytes that can be read before the
     *                  the mark position becomes invalid.
     */
    public synchronized void mark(int readlimit)
    {
	_imageInputStream.mark();
      
	// Record the current position of the stream
	try {
	    _markLimitPosition = _imageInputStream.getStreamPosition() +
		readlimit;
	}

	// On error, report the error in the reset method
	catch (IOException e) { _markLimitPosition = -1; }
    }

    /**
     * Repositions this stream to the position at the time the mark method was
     * last called on this input stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    public synchronized void reset() throws IOException
    {
	// Check for no marked stream position or if read too far
	if (_markLimitPosition == -1 ||
	    _imageInputStream.getStreamPosition() > _markLimitPosition)
	    {
		String msg = "Unable to reset because of an invalid mark.";
		throw new IOException(msg);
	    }

	// Reset the stream position
	_imageInputStream.reset();
    }

    /**
     * Tests if this input stream supports the mark and reset methods.
     *
     * @return True if this stream instance supports the mark and reset methods;
     *         false otherwise.
     */
    public boolean markSupported()
    {
	return true;
    }
}
