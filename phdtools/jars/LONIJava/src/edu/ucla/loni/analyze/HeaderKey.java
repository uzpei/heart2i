/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze;

/**
 * Analyze Subheader that describes the contents of the header.
 *
 * @version 27 November 2002
 */
public class HeaderKey extends AnalyzeSubheader
{
  /** ID of the size of the header in bytes. */
  public static final String SIZEOF_HDR = "sizeof_hdr";

  /** ID of the description of the data type. */
  public static final String DATA_TYPE = "data_type";

  /** ID of the name of the database. */
  public static final String DB_NAME = "db_name";

  /** ID of the minimum extent size. */
  public static final String EXTENTS = "extents";

  /** ID of the error in the session. */
  public static final String SESSION_ERROR = "session_error";

  /** ID of the regularity. */
  public static final String REGULARITY = "regular";

  /** ID of the unused element. */
  public static final String HKEY_UN0 = "hkey_un0";

  /**
   * Constructs a HeaderKey.
   *
   * @param sizeOfHdr The size of the header in bytes.
   * @param dataType Description of the data type.  This must be 10 bytes or 
   *                 less.
   * @param dbName Name of the database.  This must be 18 bytes or less.
   * @param extents Minimum extent size.
   * @param sessionError Error in the session.
   * @param regular If equal to 'r', then indicates that all images and volumes
   *                are the same size.
   * @param hKeyUn0 Unused element.
   *
   * @throws IllegalArgumentException If any byte array is larger than its
   *                                  specified maximum length.
   */
  public HeaderKey(int sizeOfHdr, byte[] dataType, byte[] dbName, int extents,
		   short sessionError, byte regular, byte hKeyUn0)
    {
      super(7);

      _addElement(SIZEOF_HDR, new Integer(sizeOfHdr));
      _addElement(DATA_TYPE, _createString(dataType));
      _addElement(DB_NAME, _createString(dbName));
      _addElement(EXTENTS, new Integer(extents));
      _addElement(SESSION_ERROR, new Short(sessionError));
      _addElement(REGULARITY, new Byte(regular));
      _addElement(HKEY_UN0, new Byte(hKeyUn0));
    }
}
