/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze.plugin;

import java.util.ListResourceBundle;
import edu.ucla.loni.analyze.DataHistory;
import edu.ucla.loni.analyze.HeaderKey;
import edu.ucla.loni.analyze.ImageDimension;

/**
 * List Resource Bundle that provides descriptions for ANALYZE 7.5 elements.
 *
 * @version 4 June 2002
 */
public class AnalyzeMetadataFormatResources extends ListResourceBundle
{
  /** Constructs a AnalyzeMetadataFormatResources. */
  public AnalyzeMetadataFormatResources()
    {
    }

  /**
   * Gets the contents of the Resource Bundle.
   *
   * @return Object array of the contents, where each item of the array is a
   *         pair of Objects.  The first element of each pair is the key, which
   *         must be a String, and the second element is the value associated
   *         with that key.
   */
  public Object[][] getContents()
    {
      String headerKeyName = AnalyzeMetadataFormat.HEADER_KEY_NAME;
      String imageDimName = AnalyzeMetadataFormat.IMAGE_DIMENSION_NAME;
      String dataHistName = AnalyzeMetadataFormat.DATA_HISTORY_NAME;

      Object[][] contents = {

	// Node name, followed by description
	{headerKeyName, "Describes the contents of the header."},
	{imageDimName, "Describes the image sizes."},
	{dataHistName, "Describes the history of the data."},

	// Node name + "/" + attribute name, followed by description
	{headerKeyName + "/" + HeaderKey.SIZEOF_HDR,
	 "The size of the header in bytes."},
	{headerKeyName + "/" + HeaderKey.DATA_TYPE,
	 "Description of the data type."},
	{headerKeyName + "/" + HeaderKey.DB_NAME,
	 "Name of the database."},
	{headerKeyName + "/" + HeaderKey.EXTENTS,
	 "Minimum extent size."},
	{headerKeyName + "/" + HeaderKey.SESSION_ERROR,
	 "Error in the session."},
	{headerKeyName + "/" + HeaderKey.REGULARITY,
	 "If equal to 'r', then all images and volumes are the same size."},
	{headerKeyName + "/" + HeaderKey.HKEY_UN0,
	 "Unused element."},

	{imageDimName + "/" + ImageDimension.DIM_0,
	 "Number of dimensions in the file (usually 4)."},
	{imageDimName + "/" + ImageDimension.DIM_1,
	 "Image X dimension; number of pixels in an image row."},
	{imageDimName + "/" + ImageDimension.DIM_2,
	 "Image Y dimension; number of pixel rows in a slice."},
	{imageDimName + "/" + ImageDimension.DIM_3,
	 "Image Z dimension; number of slices in a volume."},
	{imageDimName + "/" + ImageDimension.DIM_4,
	 "Time points; number of volumes in the file."},
	{imageDimName + "/" + ImageDimension.DIM_5,
	 "Fifth file dimension."},
	{imageDimName + "/" + ImageDimension.DIM_6,
	 "Sixth file dimension."},
	{imageDimName + "/" + ImageDimension.DIM_7,
	 "Seventh file dimension."},
	{imageDimName + "/" + ImageDimension.VOX_UNITS,
	 "The spatial units of measure for a voxel."},
	{imageDimName + "/" + ImageDimension.CAL_UNITS,
	 "The name of the calibration unit."},
	{imageDimName + "/" + ImageDimension.UNUSED_1,
	 "Unused element."},
	{imageDimName + "/" + ImageDimension.DATA_TYPE,
	 "Data type for this image set."},
	{imageDimName + "/" + ImageDimension.BIT_PIX,
	 "Number of bits per pixel."},
	{imageDimName + "/" + ImageDimension.DIM_UN0,
	 "Unused element."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_0,
	 "Unused element."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_1,
	 "Voxel width in millimeters."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_2,
	 "Voxel height in millimeters."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_3,
	 "Slice thickness in millimeters."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_4,
	 "Time in milliseconds."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_5,
	 "Real world measurement of the fifth file dimension."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_6,
	 "Real world measurement of the sixth file dimension."},
	{imageDimName + "/" + ImageDimension.PIX_DIM_7,
	 "Real world measurement of the seventh file dimension."},
	{imageDimName + "/" + ImageDimension.VOX_OFFSET,
	 "Byte offset in the corresponding '.img' file at which the voxels " +
	 "start."},
	{imageDimName + "/" + ImageDimension.ROI_SCALE,
	 "Region-of-interest scale."},
	{imageDimName + "/" + ImageDimension.F_UNUSED_1,
	 "Unused element."},
	{imageDimName + "/" + ImageDimension.F_UNUSED_2,
	 "Unused element."},
	{imageDimName + "/" + ImageDimension.CAL_MAX,
	 "Maximum of the range of calibration values."},
	{imageDimName + "/" + ImageDimension.CAL_MIN,
	 "Minimum of the range of calibration values."},
	{imageDimName + "/" + ImageDimension.COMPRESSED,
	 "Compressed."},
	{imageDimName + "/" + ImageDimension.VERIFIED,
	 "Verified."},
	{imageDimName + "/" + ImageDimension.GL_MAX,
	 "The maximum pixel value for the entire file."},
	{imageDimName + "/" + ImageDimension.GL_MIN,
	 "The minimum pixel value for the entire file."},

	{dataHistName + "/" + DataHistory.DESCRIP,
	 "Description."},
	{dataHistName + "/" + DataHistory.AUX_FILE,
	 "Auxilary file name."},
	{dataHistName + "/" + DataHistory.ORIENT,
	 "Slice orientation for this dataset."},
	{dataHistName + "/" + DataHistory.ORIGINATOR,
	 "Originator."},
	{dataHistName + "/" + DataHistory.GENERATED,
	 "Generated."},
	{dataHistName + "/" + DataHistory.SCAN_NUM,
	 "Number of the scan."},
	{dataHistName + "/" + DataHistory.PATIENT_ID,
	 "Id of the patient."},
	{dataHistName + "/" + DataHistory.EXP_DATE,
	 "Date."},
	{dataHistName + "/" + DataHistory.EXP_TIME,
	 "Time."},
	{dataHistName + "/" + DataHistory.HIST_UN0,
	 "Unused element."},
	{dataHistName + "/" + DataHistory.VIEWS,
	 "Views."},
	{dataHistName + "/" + DataHistory.VOLS_ADDED,
	 "Volumes added."},
	{dataHistName + "/" + DataHistory.START_FIELD,
	 "Start Field."},
	{dataHistName + "/" + DataHistory.FIELD_SKIP,
	 "Skip Field."},
	{dataHistName + "/" + DataHistory.O_MAX,
	 "O maximum."},
	{dataHistName + "/" + DataHistory.O_MIN,
	 "O minimum."},
	{dataHistName + "/" + DataHistory.S_MAX,
	 "S maximum."},
	{dataHistName + "/" + DataHistory.S_MIN,
	 "S minimum."}
	};

      return contents;
    }
}
