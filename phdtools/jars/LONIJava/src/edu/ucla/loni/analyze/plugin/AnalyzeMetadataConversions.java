/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze.plugin;

import edu.ucla.loni.analyze.AnalyzeHeader;
import edu.ucla.loni.analyze.AnalyzeSubheader;
import edu.ucla.loni.analyze.DataHistory;
import edu.ucla.loni.analyze.HeaderKey;
import edu.ucla.loni.analyze.ImageDimension;
import java.util.Iterator;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between the Analyze Header classes and
 * DOM Node representations of them.
 *
 * @version 26 June 2002
 */
public class AnalyzeMetadataConversions
{
  /** Constructs an AnalyzeMetadataConversions. */
  private AnalyzeMetadataConversions()
    {
    }

  /**
   * Converts the tree to an Analyze Header.
   *
   * @param root DOM Node representing the Analyze Header.
   *
   * @return Analyze Header constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static AnalyzeHeader toAnalyzeHeader(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the required name
      String rootName = AnalyzeMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Convert the Header Key Node
      Node headerKeyNode = root.getFirstChild();
      HeaderKey headerKey = toHeaderKey(headerKeyNode);

      // Convert the Image Dimension Node
      Node imageDimensionNode = headerKeyNode.getNextSibling();
      ImageDimension imageDimension = toImageDimension(imageDimensionNode);

      // Convert the Data History if applicable
      Node dataHistoryNode = imageDimensionNode.getNextSibling();
      DataHistory dataHistory = null;
      if (dataHistoryNode != null) {
	dataHistory = toDataHistory(dataHistoryNode);
      }

      // Return a new Analyze Header
      return new AnalyzeHeader(headerKey, imageDimension, dataHistory);
    }

  /**
   * Converts the tree to a Header Key.
   *
   * @param root DOM Node representing the Header Key.
   *
   * @return Header Key constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static HeaderKey toHeaderKey(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the Header Key Node name
      String headerKeyName = AnalyzeMetadataFormat.HEADER_KEY_NAME;
      if ( !root.getNodeName().equals(headerKeyName) ) {
	String msg = "Root node must be named \"" + headerKeyName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a Header Key from the Node attributes
      HeaderKey headerKey;
      try {
	NamedNodeMap atts = root.getAttributes();

	// ID of the size of the header in bytes
	String id = HeaderKey.SIZEOF_HDR;
	int sizeOfHdr = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the description of the data type
	id = HeaderKey.DATA_TYPE;
	byte[] dataType = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the name of the database
	id = HeaderKey.DB_NAME;
	byte[] dbName = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the minimum extent size
	id = HeaderKey.EXTENTS;
	int extents = Integer.parseInt( atts.getNamedItem(id).getNodeValue() );

	// ID of the error in the session
	id = HeaderKey.SESSION_ERROR;
	short error = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the regularity
	id = HeaderKey.REGULARITY;
	byte regular = Byte.parseByte( atts.getNamedItem(id).getNodeValue() );

	// ID of the unused element
	id = HeaderKey.HKEY_UN0;
	byte hKeyUn0 = Byte.parseByte( atts.getNamedItem(id).getNodeValue() );

	// Construct a Header Key
	headerKey = new HeaderKey(sizeOfHdr, dataType, dbName, extents, error,
				  regular, hKeyUn0);
      }

      // Unable to construct a Header Key
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a Header Key.";
	throw new IIOInvalidTreeException(msg, e, root);
      }

      // Return the Header Key
      return headerKey;
    }

  /**
   * Converts the tree to an Image Dimension.
   *
   * @param root DOM Node representing the Image Dimension.
   *
   * @return Image Dimension constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static ImageDimension toImageDimension(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the Image Dimension Node name
      String imageDimName = AnalyzeMetadataFormat.IMAGE_DIMENSION_NAME;
      if ( !root.getNodeName().equals(imageDimName) ) {
	String msg = "Root node must be named \"" + imageDimName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct an Image Dimension from the Node attributes
      ImageDimension imageDimension;
      try {
	NamedNodeMap atts = root.getAttributes();

	// ID of the number of dimensions in the file
	String id = ImageDimension.DIM_0;
	short dim0 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the image X dimension; number of pixels in an image row
	id = ImageDimension.DIM_1;
	short dim1 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the image Y dimension; number of pixel rows in a slice
	id = ImageDimension.DIM_2;
	short dim2 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the image Z dimension; number of slices in a volume
	id = ImageDimension.DIM_3;
	short dim3 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the time points; number of volumes in the file
	id = ImageDimension.DIM_4;
	short dim4 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the fifth file dimension
	id = ImageDimension.DIM_5;
	short dim5 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the sixth file dimension
	id = ImageDimension.DIM_6;
	short dim6 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the seventh file dimension
	id = ImageDimension.DIM_7;
	short dim7 = Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	// ID of the spatial units of measure for a voxel
	id = ImageDimension.VOX_UNITS;
	byte[] voxUnits = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the name of the calibration unit
	id = ImageDimension.CAL_UNITS;
	byte[] calUnits = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the unused element
	id = ImageDimension.UNUSED_1;
	short unused1 = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// ID of the data type for this image set
	id = ImageDimension.DATA_TYPE;
	short type = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// ID of the number of bits per pixel
	id = ImageDimension.BIT_PIX;
	short bitPix = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// ID of the Un0 unused element
	id = ImageDimension.DIM_UN0;
	short dimUn0 = Short.parseShort(atts.getNamedItem(id).getNodeValue());

	// ID of the Dim 0 unused element
	id = ImageDimension.PIX_DIM_0;
	float pixDim0 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the voxel width in millimeters
	id = ImageDimension.PIX_DIM_1;
	float pixDim1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the voxel height in millimeters
	id = ImageDimension.PIX_DIM_2;
	float pixDim2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the slice thickness in millimeters
	id = ImageDimension.PIX_DIM_3;
	float pixDim3 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the time in milliseconds
	id = ImageDimension.PIX_DIM_4;
	float pixDim4 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the real world measurement of the fifth file dimension
	id = ImageDimension.PIX_DIM_5;
	float pixDim5 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the real world measurement of the sixth file dimension
	id = ImageDimension.PIX_DIM_6;
	float pixDim6 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the real world measurement of the seventh file dimension
	id = ImageDimension.PIX_DIM_7;
	float pixDim7 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the byte offset in the corresponding '.img' file
	id = ImageDimension.VOX_OFFSET;
	float offset = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the region-of-interest scale
	id = ImageDimension.ROI_SCALE;
	float scale = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the first float unused element
	id = ImageDimension.F_UNUSED_1;
	float fU1 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the second float unused element
	id = ImageDimension.F_UNUSED_2;
	float fU2 = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the maximum of the range of calibration values
	id = ImageDimension.CAL_MAX;
	float calMax = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the minimum of the range of calibration values
	id = ImageDimension.CAL_MIN;
	float calMin = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the compressed
	id = ImageDimension.COMPRESSED;
	float compd = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the verified
	id = ImageDimension.VERIFIED;
	float verd = Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	// ID of the maximum pixel value for the entire file
	id = ImageDimension.GL_MAX;
	int glMax = Integer.parseInt( atts.getNamedItem(id).getNodeValue() );

	// ID of the minimum pixel value for the entire file
	id = ImageDimension.GL_MIN;
	int glMin = Integer.parseInt( atts.getNamedItem(id).getNodeValue() );

	// Construct an Image Dimension
	imageDimension = new ImageDimension(dim0, dim1, dim2, dim3, dim4, dim5,
					    dim6, dim7, voxUnits, calUnits,
					    unused1, type,  bitPix, dimUn0,
					    pixDim0, pixDim1, pixDim2, pixDim3,
					    pixDim4, pixDim5, pixDim6, pixDim7,
					    offset, scale, fU1, fU2, calMax,
					    calMin, compd, verd, glMax, glMin);
      }

      // Unable to construct an Image Dimension
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into an Image Dimension.";
	throw new  IIOInvalidTreeException(msg, e, root);
      }

      // Return the Image Dimension
      return imageDimension;
    }

  /**
   * Converts the tree to a Data History.
   *
   * @param root DOM Node representing the Data History.
   *
   * @return Data History constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static DataHistory toDataHistory(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the Data History Node name
      String dataHistoryName = AnalyzeMetadataFormat.DATA_HISTORY_NAME;
      if ( !root.getNodeName().equals(dataHistoryName) ) {
	String msg = "Root node must be named \"" + dataHistoryName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Attempt to construct a Data History from the Node attributes
      DataHistory dataHistory;
      try {
	NamedNodeMap atts = root.getAttributes();

	// ID of the description
	String id = DataHistory.DESCRIP;
	byte[] descrip = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the auxilary file name
	id = DataHistory.AUX_FILE;
	byte[] auxFile = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the slice orientation for this dataset
	id = DataHistory.ORIENT;
	byte orient = Byte.parseByte( atts.getNamedItem(id).getNodeValue() );

	// ID of the originator
	id = DataHistory.ORIGINATOR;
	byte[] originator = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the generated
	id = DataHistory.GENERATED;
	byte[] generated = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the number of the scan
	id = DataHistory.SCAN_NUM;
	byte[] scannum = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the id of the patient
	id = DataHistory.PATIENT_ID;
	byte[] patientId = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the date
	id = DataHistory.EXP_DATE;
	byte[] expDate = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the time
	id = DataHistory.EXP_TIME;
	byte[] expTime = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the unused element
	id = DataHistory.HIST_UN0;
	byte[] histUn0 = atts.getNamedItem(id).getNodeValue().getBytes();

	// ID of the views
	id = DataHistory.VIEWS;
	int views = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the volumes added
	id = DataHistory.VOLS_ADDED;
	int volsAdded = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the start field
	id = DataHistory.START_FIELD;
	int strtField = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the skip field
	id = DataHistory.FIELD_SKIP;
	int fieldSkip = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the o maximum
	id = DataHistory.O_MAX;
	int omax = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the o minimum
	id = DataHistory.O_MIN;
	int omin = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the s maximum
	id = DataHistory.S_MAX;
	int smax = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// ID of the s minimum
	id = DataHistory.S_MIN;
	int smin = Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	// Construct a Data History
	dataHistory = new DataHistory(descrip, auxFile, orient, originator,
				      generated, scannum, patientId, expDate,
				      expTime, histUn0, views, volsAdded,
				      strtField, fieldSkip, omax, omin, smax,
				      smin);
      }

      // Unable to construct a Data History
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node into a Data History.";
	throw new  IIOInvalidTreeException(msg, e, root);
      }

      // Return the Data History
      return dataHistory;
    }

  /**
   * Converts the Analyze Header to a tree.
   *
   * @param analyzeHeader Analyze Header to convert to a tree.
   *
   * @return DOM Node representing the Analyze Header.
   */
  public static Node toTree(AnalyzeHeader analyzeHeader)
    {
      // Create the root Node
      String rootName = AnalyzeMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      if (analyzeHeader != null) {

	// Add the Header Key
	String name = AnalyzeMetadataFormat.HEADER_KEY_NAME;
	Node node = _toTree(name, analyzeHeader.getHeaderKey());
	rootNode.appendChild(node);

	// Add the Image Dimension
	name = AnalyzeMetadataFormat.IMAGE_DIMENSION_NAME;
	node = _toTree(name, analyzeHeader.getImageDimension());
	rootNode.appendChild(node);

	// Add the Data History if it is available
	if ( analyzeHeader.getDataHistory() != null ) {
	  name = AnalyzeMetadataFormat.DATA_HISTORY_NAME;
	  node = _toTree(name, analyzeHeader.getDataHistory());
	  rootNode.appendChild(node);
	}
      }

      // Return the root Node
      return rootNode;
    }

  /**
   * Converts the Analyze Subheader to a tree.
   *
   * @param treeName Name of the tree.
   * @param analyzeSubheader Analyze Subheader from which to construct the
   *                         tree.
   *
   * @return DOM Node representing the Analyze Subheader.
   */
  private static Node _toTree(String treeName,
			      AnalyzeSubheader analyzeSubheader)
    {
      // Create the root Node
      IIOMetadataNode rootNode = new IIOMetadataNode(treeName);

      // Add each element as an attribute
      Iterator iter = analyzeSubheader.getElementNames();
      while ( iter.hasNext() ) {
	String elementName = (String)iter.next();
	Object elementValue = analyzeSubheader.getElementValue(elementName);

	// Set the attribute
	rootNode.setAttribute(elementName, elementValue.toString());
      }

      // Return the root Node
      return rootNode;
    }
}
