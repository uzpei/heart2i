/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.analyze.plugin;

import edu.ucla.loni.analyze.AnalyzeHeader;
import edu.ucla.loni.analyze.ImageDimension;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for metadata of the ANALYZE 7.5 image format.
 *
 * @version 16 January 2009
 */
public class AnalyzeMetadata extends AppletFriendlyIIOMetadata
{
    /** Analyze Header containing the original metadata. */
    private AnalyzeHeader _originalAnalyzeHeader;

    /** Analyze Header containing the current metadata. */
    private AnalyzeHeader _currentAnalyzeHeader;

    /**
     * Constructs an AnalyzeMetadata from the given Analyze Header.
     *
     * @param analyzeHeader Analyze Header containing the metadata.
     */
    public AnalyzeMetadata(AnalyzeHeader analyzeHeader)
    {
	super(true, AnalyzeMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      AnalyzeMetadataFormat.class.getName(),
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalAnalyzeHeader = analyzeHeader;
	_currentAnalyzeHeader = analyzeHeader;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return AnalyzeMetadataConversions.toTree(_currentAnalyzeHeader);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current Analyze Header, convert the root
	Node r;
	if (_currentAnalyzeHeader == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Get the current metadata tree Nodes
	    Node analyzeHeaderNode = getAsTree(formatName);
	    Node headerKeyNode = analyzeHeaderNode.getFirstChild();
	    Node imageDimNode = headerKeyNode.getNextSibling();
	    Node dataHistoryNode = imageDimNode.getNextSibling();

	    // Loop through the root Node children and merge their attributes
	    NodeList nodeList = root.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node childNode = nodeList.item(i);
		String cName = childNode.getNodeName();
		NamedNodeMap childAttrs = childNode.getAttributes();

		// Replace Header Key attributes
		if ( AnalyzeMetadataFormat.HEADER_KEY_NAME.equals(cName) ) {
		    headerKeyNode = _getMergedNode(headerKeyNode, childAttrs);
		}

		// Replace Image Dimension attributes
		if ( AnalyzeMetadataFormat.IMAGE_DIMENSION_NAME.equals(cName) ){
		    imageDimNode = _getMergedNode(imageDimNode, childAttrs);
		}

		// Replace Data History attributes
		if ( AnalyzeMetadataFormat.DATA_HISTORY_NAME.equals(cName) ) {

		    // Merge only if there currently exists a Data History Node
		    if (dataHistoryNode != null) {
			dataHistoryNode = _getMergedNode(dataHistoryNode,
							 childAttrs);
		    }

		    // Otherwise directly use the child Node
		    else { dataHistoryNode = childNode; }
		}
	    }

	    // Recreate the root tree
	    r = new IIOMetadataNode(nativeMetadataFormatName);
	    r.appendChild(headerKeyNode);
	    r.appendChild(imageDimNode);
	    r.appendChild(dataHistoryNode);
	}

	// Set a new current Analyze Header
	_currentAnalyzeHeader = AnalyzeMetadataConversions.toAnalyzeHeader(r);
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current Analyze Header to the original 
	_currentAnalyzeHeader = _originalAnalyzeHeader;
    }

    /**
     * Gets the number of images.
     *
     * @return Number of images, or -1 if it cannot be determined.
     */
    public int getNumberOfImages()
    {
	try {

	    // Number of images is the DIM_3 attribute of the Image Dimension
	    ImageDimension imageDim = _currentAnalyzeHeader.getImageDimension();
	    Object value = imageDim.getElementValue(ImageDimension.DIM_3);
	    return ((Number)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the width of each image.
     *
     * @return Width of each image, or -1 if it cannot be determined.
     */
    public int getImageWidth()
    {
	try {

	    // Width is the DIM_1 attribute of the Image Dimension
	    ImageDimension imageDim = _currentAnalyzeHeader.getImageDimension();
	    Object value = imageDim.getElementValue(ImageDimension.DIM_1);
	    return ((Number)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the height of each image.
     *
     * @return Height of each image, or -1 if it cannot be determined.
     */
    public int getImageHeight()
    {
	try {

	    // Height is the DIM_2 attribute of the Image Dimension
	    ImageDimension imageDim = _currentAnalyzeHeader.getImageDimension();
	    Object value = imageDim.getElementValue(ImageDimension.DIM_2);
	    return ((Number)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the number of bits per pixel used by each image.
     *
     * @return Number of bits per pixel used by each image, or -1 if it cannot
     *         be determined.
     */
    public int getBitsPerPixel()
    {
	try {

	    // Get the data type
	    int type = getDataType();

	    // Unsigned character (1 byte/voxel)
	    if (type == 2) { return 8; }

	    // Signed short (2 bytes/voxel)
	    if (type == 4) { return 16; }

	    // Signed integer (4 bytes/voxel)
	    if (type == 8) { return 32; }

	    // Float (4 bytes/voxel)
	    if (type == 16) { return 32; }

	    // Two floats (8 bytes/voxel)
	    if (type == 32) { return 64; }

	    // Double (8 bytes/voxel)
	    if (type == 64) { return 64; }

	    // RGB (3 bytes/voxel)
	    if (type == 128) { return 24; }

	    // Unknown type
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the data type of each image.
     *
     * @return Data type of each image (as defined by the ANALYZE file format),
     *         or -1 if it cannot be determined.
     */
    public int getDataType()
    {
	try {

	    // Get the data type attribute of the Image Dimension
	    ImageDimension imageDim = _currentAnalyzeHeader.getImageDimension();
	    Object value = imageDim.getElementValue(ImageDimension.DATA_TYPE);
	    return ((Number)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Determines whether or not each image is a color image.
     *
     * @return True if the images are color images; false otherwise.
     */
    public boolean hasColorImages()
    {
	// RGB
	try { return getDataType() == 128; }

	// Unable to make the determination
	catch (Exception e) { return false; }
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	try {

	    // RGB
	    if ( hasColorImages() ) { return Utilities.getRgbChromaNode(); }

	    // Only support non-inverted grayscale
	    return Utilities.getGrayScaleChromaNode(false);
	}

	// No information available
	catch (Exception e) { return null; }
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	try {

	    // RGB
	    if ( hasColorImages() ) { return Utilities.getRgbDataNode(); }

	    // Get the number of bits per pixel
	    int bitsPerPixel = getBitsPerPixel();
	    if (bitsPerPixel == -1) { return null; }

	    // Integral gray scale data
	    int type = getDataType();
	    if (type == 2 || type == 4 || type == 8) {
		return Utilities.getGrayScaleDataNode(bitsPerPixel);
	    }

	    // Real gray scale data
	    else { return Utilities.getGrayScaleDataNode(bitsPerPixel, false); }
	}
      
	// No information available
	catch (Exception e) { return null; }
    }

    /**
     * Returns an IIOMetadataNode representing the dimension format information
     * of the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the dimension information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Get the voxel width and height attributes of the Image Dimension
	    ImageDimension imageDim = _currentAnalyzeHeader.getImageDimension();
	    Object value = imageDim.getElementValue(ImageDimension.PIX_DIM_1);
	    float pixelWidth = ((Number)value).floatValue();

	    value = imageDim.getElementValue(ImageDimension.PIX_DIM_2);
	    float pixelHeight = ((Number)value).floatValue();

	    // Return the dimension node
	    return Utilities.getDimensionNode(pixelWidth, pixelHeight);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	// Image dimensions
	int imageWidth = getImageWidth();
	int imageHeight = getImageHeight();
	int numberOfImages = getNumberOfImages();

	// Pixel dimensions
	ImageDimension imageDim = _currentAnalyzeHeader.getImageDimension();
	Object value = imageDim.getElementValue(ImageDimension.PIX_DIM_1);
	float pixelWidth = ((Number)value).floatValue();
	value = imageDim.getElementValue(ImageDimension.PIX_DIM_2);
	float pixelHeight = ((Number)value).floatValue();
	value = imageDim.getElementValue(ImageDimension.PIX_DIM_3);
	float sliceThickness = ((Number)value).floatValue();

	// Data type
	int type = getDataType();
	String dataType = "Unknown";
	if (type == 2) { dataType = BasicMetadataFormat.BYTE_TYPE; }
	if (type == 4) { dataType = BasicMetadataFormat.SIGNED_SHORT_TYPE; }
	if (type == 8) { dataType = BasicMetadataFormat.SIGNED_INT_TYPE; }
	if (type == 16) { dataType = BasicMetadataFormat.FLOAT_TYPE; }
	if (type == 32) { dataType = BasicMetadataFormat.FLOAT_TYPE; }
	if (type == 64) { dataType = BasicMetadataFormat.DOUBLE_TYPE; }
	if (type == 128) { dataType = BasicMetadataFormat.BYTE_TYPE; }

	// Bits per pixel
	int bitsPerPixel = getBitsPerPixel();

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;
	if ( hasColorImages() ) {colorType = BasicMetadataFormat.RGB_TYPE;}

	// Return the basic tree
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }

    /**
     * Merges the attributes with the specified Node.  If there are attribute
     * names not present in the attributes of the node, the attributes are
     * ignored.
     *
     * @param node Node to merge the attributes with.
     * @param attributes Attributes to merge.
     *
     * @return New Node containing the merged attributes.
     */
    private Node _getMergedNode(Node node, NamedNodeMap attributes)
    {
	// Create a new Node with the same name
	IIOMetadataNode mergedNode = new IIOMetadataNode( node.getNodeName() );

	// Copy the Node attributes to the new Node
	NamedNodeMap map = node.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		mergedNode.setAttribute(attr.getNodeName(),attr.getNodeValue());
	    }
	}

	// Merge the new attributes with the old
	if (attributes != null) {
	    for (int i = 0; i < attributes.getLength(); i++) {
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName();

		// Attribute name must be present in the new Node
		if ( mergedNode.hasAttribute(attributeName) ) {
		    mergedNode.setAttribute(attributeName,
					    attribute.getNodeValue());
		}
	    }
	}

	// Return the new merged Node
	return mergedNode;
    }
}
