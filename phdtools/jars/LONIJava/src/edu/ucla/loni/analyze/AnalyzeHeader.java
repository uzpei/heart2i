/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze;

/**
 * Header that contains elements for the ANALYZE 7.5 format.
 *
 * @version 3 June 2002
 */
public class AnalyzeHeader
{
  /** Analyze Subheader that describes the contents of the header. */
  private HeaderKey _headerKey;

  /** Analyze Subheader that describes the image sizes. */
  private ImageDimension _imageDimension;

  /** Analyze Subheader that describes the history of the data. */
  private DataHistory _dataHistory;

  /**
   * Constructs an AnalyzeHeader without a Data History.
   *
   * @param headerKey Analyze Subheader that describes the contents of the
   *                  header.
   * @param imageDimension Analyze Subheader that describes the image sizes.
   *
   * @throws NullPointerException If the Header Key or Image Dimension is null.
   */
  public AnalyzeHeader(HeaderKey headerKey, ImageDimension imageDimension)
    {
      this(headerKey, imageDimension, null);
    }

  /**
   * Constructs an AnalyzeHeader.
   *
   * @param headerKey Analyze Subheader that describes the contents of the
   *                  header.
   * @param imageDimension Analyze Subheader that describes the image sizes.
   * @param dataHistory Analyze Subheader that describes the history of the
   *                    data; this may be null.
   *
   * @throws NullPointerException If the Header Key or Image Dimension is null.
   */
  public AnalyzeHeader(HeaderKey headerKey, ImageDimension imageDimension,
		       DataHistory dataHistory)
    {
      // Check for a null Header Key or Image Dimension
      if (headerKey == null || imageDimension == null) {
	String msg = "Header Key or Image Dimension is null.";
	throw new NullPointerException(msg);
      }

      _headerKey = headerKey;
      _imageDimension = imageDimension;
      _dataHistory = dataHistory;
    }

  /**
   * Gets the Header Key.
   *
   * @return Analyze Subheader that describes the contents of the header.
   */
  public HeaderKey getHeaderKey()
    {
      return _headerKey;
    }

  /**
   * Gets the Image Dimension.
   *
   * @return Analyze Subheader that describes the image sizes.
   */
  public ImageDimension getImageDimension()
    {
      return _imageDimension;
    }

  /**
   * Gets the Data History.
   *
   * @return Analyze Subheader that describes the history of the data, or null
   *         if it does not exist.
   */
  public DataHistory getDataHistory()
    {
      return _dataHistory;
    }

  /**
   * Gets the contents of the Analyze Header.
   *
   * @return String representation of the contents of the Analyze Header.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Write the Header Key
      buffer.append("Header Key:  \n");
      buffer.append( getHeaderKey().toString() );

      // Write the Image Dimension
      buffer.append("\nImage Dimension:  \n");
      buffer.append( getImageDimension().toString() );

      // Write the Data History if it exists
      if ( getDataHistory() != null ) {
	buffer.append("\nData History:  \n");
	buffer.append( getDataHistory().toString() );
      }

      return buffer.toString();
    }
}
