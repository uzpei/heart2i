/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.analyze.plugin;

import edu.ucla.loni.Conversions;
import edu.ucla.loni.analyze.AnalyzeHeader;
import edu.ucla.loni.analyze.AnalyzeSubheader;
import edu.ucla.loni.analyze.DataHistory;
import edu.ucla.loni.analyze.HeaderKey;
import edu.ucla.loni.analyze.ImageDimension;
import edu.ucla.loni.imageio.DualImageOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Image Writer for parsing and encoding ANALYZE 7.5 images.
 *
 * @version 17 November 2005
 */
public class AnalyzeImageWriter extends ImageWriter
{
    /** Analyze Header of the image sequence currently being written. */
    private AnalyzeHeader _streamHeader;

    /** Number of images in the sequence currently being written. */
    private int _numberOfImagesWritten;

    /**
     * Constructs an ANALYZE Image Writer.
     *
     * @param originatingProvider The ImageWriterSpi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  Analyze Image Writer Spi.
     */
    public AnalyzeImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an Analyze Image Writer Spi
	if ( !(originatingProvider instanceof AnalyzeImageWriterSpi) ) {
	    String msg="Originating provider must an Analyze Image Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the destination to the given Image Output Stream or other Object.
     * The destination is assumed to be ready to accept data, and will not be
     * closed at the end of each write. If output is null, any currently set
     * output will be removed.
     *
     * @param output A Dual Image Output Stream or a Vector of two Image Output
     *               Streams to use for future writing.
     *
     * @throws IllegalArgumentException If output is not an instance of one of
     *                                  the classes returned by the originating
     *                                  service provider's getOutputTypes
     *                                  method.
     */
    public void setOutput(Object output)
    {
	// Convert a Vector into a Dual Image Output Stream
	if (output instanceof Vector) {
	    output = new DualImageOutputStream( (Vector)output );
	}

	super.setOutput(output);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank metadata object
	return new AnalyzeMetadata(null);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding an
     * image of the given type.
     *
     * @param imageType An ImageTypeSpecifier indicating the format of the image
     *                  to be written later.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					       ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing stream metadata, used to
     *               initialize the state of the returned object.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize Analyze Metadata
	if (inData instanceof AnalyzeMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing image metadata, used to
     *               initialize the state of the returned object.
     * @param imageType An ImageTypeSpecifier indicating the layout and color
     *                  information of the image with which the metadata will be
     *                  associated.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If either of inData or imageType is
     *                                  null.
     */
    public IIOMetadata convertImageMetadata(IIOMetadata inData,
					    ImageTypeSpecifier imageType,
					    ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns true if the methods that take an IIOImage parameter are capable
     * of dealing with a Raster (as opposed to RenderedImage) source image.
     *
     * @return True if Raster sources are supported.
     */
    public boolean canWriteRasters()
    {
	return true;
    }

    /**
     * Appends a complete image stream containing a single image and associated
     * stream and image metadata and thumbnails to the output.
     *
     * @param streamMetadata An IIOMetadata object representing stream metadata,
     *                       or null to use default values.
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam, or null to use a default
     *              ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, if the stream
     *                                  metadata cannot be converted, or if the
     *                                  image type is unrecognized.
     * @throws IllegalStateException If the output has not been set.
     */
    public void write(IIOMetadata streamMetadata, IIOImage image,
		      ImageWriteParam param) throws IOException
    {
	// Write a sequence with 1 image
	prepareWriteSequence(streamMetadata);
	writeToSequence(image, param);
	endWriteSequence();
    }

    /**
     * Returns true if the writer is able to append an image to an image
     * stream that already contains header information and possibly prior
     * images.
     *
     * @return True If images may be appended sequentially.
     */
    public boolean canWriteSequence()
    {
	return true;
    }

    /**
     * Prepares a stream to accept a series of subsequent writeToSequence calls,
     * using the provided stream metadata object.  The metadata will be written
     * to the stream if it should precede the image data.
     *
     * @param streamMetadata A stream metadata object.
     *
     * @throws IllegalArgumentException If the stream metadata is null or cannot
     *                                  be converted.
     */
    public void prepareWriteSequence(IIOMetadata streamMetadata)
    {
	// Convert the metadata to Analyze Metadata
	IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the stream metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct a stream Analyze Header from the metadata
	try {
	    String rootName = AnalyzeMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	    Node tree = metadata.getAsTree(rootName);
	    _streamHeader = AnalyzeMetadataConversions.toAnalyzeHeader(tree);
	}
      
	catch (Exception e) {
	    String msg = "Unable to construct an Analyze Header from the " +
		"metadata.";
	    throw new IllegalArgumentException(msg);
	}

	_numberOfImagesWritten = 0;
    }

    /**
     * Appends a single image and possibly associated metadata and thumbnails,
     * to the output.  The supplied ImageReadParam is ignored.
     *
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam or null to use a default ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null or if the image
     *                                  type is not recognized.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void writeToSequence(IIOImage image, ImageWriteParam param)
	throws IOException
    { 
	// Check for the stream Analyze Header
	if (_streamHeader == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Check for a null image
	if (image == null) {
	    String msg = "Cannot write a null image.";
	    throw new IllegalArgumentException(msg);
	}

	// Get a Raster from the image
	Raster raster = image.getRaster();
	if (raster == null) {
	    RenderedImage renderedImage = image.getRenderedImage();

	    // If the Rendered Image is a Buffered Image, get it directly
	    if (renderedImage instanceof BufferedImage) {
		raster = ((BufferedImage)renderedImage).getRaster();
	    }

	    // Otherwise get a copy of the Raster from the Rendered Image
	    raster = renderedImage.getData();
	}

	// Update the Listeners
	processImageStarted(_numberOfImagesWritten);

	// Color image data
	ImageOutputStream outputStream = _getPixelDataOutputStream();
	DataBuffer dataBuffer = raster.getDataBuffer();
	AnalyzeMetadata metadata = new AnalyzeMetadata(_streamHeader);
	if ( metadata.hasColorImages() ) {
	    int[] intArray = ((DataBufferInt)dataBuffer).getData();

	    // Separate the RGB components of each integer
	    byte[] byteArray = new byte[3*intArray.length];
	    for (int i = 0; i < intArray.length; i++) {
		int v = intArray[i];
		byteArray[3*i]   = (byte)((v >>> 16) & 0xFF);
		byteArray[3*i+1] = (byte)((v >>> 8) & 0xFF);
		byteArray[3*i+2] = (byte)((v >>> 0) & 0xFF);
	    }

	    // Write byte data
	    outputStream.write(byteArray);
	}

	// Write byte data
	else if (dataBuffer instanceof DataBufferByte) {
	    outputStream.write( ((DataBufferByte)dataBuffer).getData() );
	}

	// Write short data
	else if (dataBuffer instanceof DataBufferShort) {
	    short[] data = ((DataBufferShort)dataBuffer).getData();
	    outputStream.writeShorts(data, 0, data.length);
	}

	// Write unsigned short data
	else if (dataBuffer instanceof DataBufferUShort) {
	    short[] data = ((DataBufferUShort)dataBuffer).getData();
	    outputStream.writeShorts(data, 0, data.length);
	}

	// Write integer data
	else if (dataBuffer instanceof DataBufferInt) {
	    int[] data = ((DataBufferInt)dataBuffer).getData();
	    outputStream.writeInts(data, 0, data.length);
	}

	// Write float data
	else if (dataBuffer instanceof DataBufferFloat) {
	    float[] data = ((DataBufferFloat)dataBuffer).getData();
	    outputStream.writeFloats(data, 0, data.length);
	}

	// Write double data
	else if (dataBuffer instanceof DataBufferDouble) {
	    double[] data = ((DataBufferDouble)dataBuffer).getData();
	    outputStream.writeDoubles(data, 0, data.length);
	}

	// Unrecognized type
	else {
	    String msg = "Unable to write the IIOImage.";
	    throw new IllegalArgumentException(msg);
	}

	// Update the Listeners
	_numberOfImagesWritten++;
	if ( abortRequested() ) { processWriteAborted(); }
	else { processImageComplete(); }
    }

    /**
     * Completes the writing of a sequence of images begun with
     * prepareWriteSequence.  Any stream metadata that should come at the end of
     * the sequence of images is written out, and any header information at the
     * beginning of the sequence is patched up if necessary.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void endWriteSequence() throws IOException
    {
	// Check for the stream Analyze Header
	if (_streamHeader == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Get the Analyze Header elements
	AnalyzeSubheader headerKey = _streamHeader.getHeaderKey();
	AnalyzeSubheader imageDimension = _streamHeader.getImageDimension();
	AnalyzeSubheader dataHistory = _streamHeader.getDataHistory();

	// Determine the size of the Analyze Header in bytes
	int headerSize = 348;
	if (dataHistory == null) { headerSize = 148; }

	// Write the Header Key to the metadata output stream
	ImageOutputStream outputStream = _getMetadataOutputStream();
	_writeHeaderKey(headerKey, outputStream, headerSize);

	// Write the Image Dimension to the metadata output stream
	_writeImageDimension(imageDimension, outputStream);

	// If applicable, write the Data History to the metadata output stream
	if (dataHistory != null) {
	    _writeDataHistory(dataHistory, outputStream);
	}

	// Reset the stream Analyze Header
	_streamHeader = null;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageWriter.  Otherwise, the writer may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the output stream
	try { if (output != null) { ((DualImageOutputStream)output).close(); } }
	catch (Exception e) {}

	output = null;
    }

    /**
     * Gets the output stream for the metadata.
     *
     * @return Image Output Stream to write Analyze metadata to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getMetadataOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream for the metadata
	return ((DualImageOutputStream)output).getMetadataOutputStream();
    }

    /**
     * Gets the output stream for the pixel data.
     *
     * @return Image Output Stream to write Analyze image pixel data to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getPixelDataOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream for image pixel data
	return ((DualImageOutputStream)output).getPixelDataOutputStream();
    }

    /**
     * Writes the Header Key to the output stream.
     *
     * @param headerKey Header Key to write to the output stream.
     * @param outputStream Image Output Stream to write to.
     * @param headerSize Size of the Analyze header in bytes.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeHeaderKey(AnalyzeSubheader headerKey,
				 ImageOutputStream outputStream, int headerSize)
	throws IOException
    {
	Iterator iter = headerKey.getElementNames();
	while ( iter.hasNext() ) {
	    String elementName = (String)iter.next();
	    Object elementValue = headerKey.getElementValue(elementName);

	    // Element value is a String
	    if (elementValue instanceof String) {
		int size = 0;
		if (HeaderKey.DATA_TYPE.equals(elementName) ) { size = 10; }
		if (HeaderKey.DB_NAME.equals(elementName) ) { size = 18; }

		// Write the string value
		_writeString( (String)elementValue, size, outputStream );
	    }

	    // Element value is a Number
	    else {

		// Overwrite any value for the size of the Analyze Header
		if ( HeaderKey.SIZEOF_HDR.equals(elementName) ) {
		    elementValue = new Integer(headerSize);
		}

		// Write the number value
		_writeNumber( (Number)elementValue, outputStream );
	    }
	}
    }

    /**
     * Writes the Image Dimension to the output stream.
     *
     * @param imageDimension Image Dimension to write to the output stream.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeImageDimension(AnalyzeSubheader imageDimension,
				      ImageOutputStream outputStream)
	throws IOException
    {
	Iterator iter = imageDimension.getElementNames();
	while ( iter.hasNext() ) {
	    String elementName = (String)iter.next();
	    Object elementValue = imageDimension.getElementValue(elementName);

	    // Element value is a String
	    if (elementValue instanceof String) {
		int size = 0;
		if (ImageDimension.VOX_UNITS.equals(elementName) ) { size = 4; }
		if (ImageDimension.CAL_UNITS.equals(elementName) ) { size = 8; }

		// Write the string value
		_writeString( (String)elementValue, size, outputStream );
	    }

	    // Element value is a Number
	    else { _writeNumber( (Number)elementValue, outputStream ); }
	}
    }

    /**
     * Writes the Data History to the output stream.
     *
     * @param dataHistory Data History to write to the output stream.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeDataHistory(AnalyzeSubheader dataHistory,
				   ImageOutputStream outputStream)
	throws IOException
    {
	Iterator iter = dataHistory.getElementNames();
	while ( iter.hasNext() ) {
	    String elementName = (String)iter.next();
	    Object elementValue = dataHistory.getElementValue(elementName);

	    // Element value is a String
	    if (elementValue instanceof String) {
		int size = 0;
		if (DataHistory.DESCRIP.equals(elementName) ) { size = 80; }
		if (DataHistory.AUX_FILE.equals(elementName) ) { size = 24; }
		if (DataHistory.ORIGINATOR.equals(elementName) ) {
		    size = 10;

		    // Try to convert from a hex string 
		    try {
			byte[] bytes =
			    Conversions.toByteArray( (String)elementValue );

			elementValue = new String(bytes);
		    }

		    // If not an encoded hex string, write as normal
		    catch (Exception e) {}
		}
		if (DataHistory.GENERATED.equals(elementName) ) { size = 10; }
		if (DataHistory.SCAN_NUM.equals(elementName) ) { size = 10; }
		if (DataHistory.PATIENT_ID.equals(elementName) ) { size = 10; }
		if (DataHistory.EXP_DATE.equals(elementName) ) { size = 10; }
		if (DataHistory.EXP_TIME.equals(elementName) ) { size = 10; }
		if (DataHistory.HIST_UN0.equals(elementName) ) { size = 3; }

		// Write the string value
		_writeString( (String)elementValue, size, outputStream );
	    }

	    // Element value is a Number
	    else { _writeNumber( (Number)elementValue, outputStream ); }
	}
    }

    /**
     * Writes the Number to the output stream.
     *
     * @param number Number to write to the output stream.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeNumber(Number number, ImageOutputStream outputStream)
	throws IOException
    {
	// Byte
	if (number instanceof Byte) {
	    int value = ((Byte)number).byteValue() & 0xffffffff;
	    outputStream.writeByte(value);
	}

	// Short
	else if (number instanceof Short) {
	    int value = ((Short)number).shortValue() & 0xffffffff;
	    outputStream.writeShort(value);
	}

	// Integer
	else if (number instanceof Integer) {
	    outputStream.writeInt( ((Integer)number).intValue() );
	}

	// Float
	else if (number instanceof Float) {
	    outputStream.writeFloat( ((Float)number).floatValue() );
	}

	// Unknown Number
	else {
	    String msg = "Unable to write an Number of type \"" +
		number.getClass().getName() + "\".";
	    throw new IOException(msg);
	}
    }

    /**
     * Writes the String to the output stream.
     *
     * @param string String to write to the output stream.
     * @param length Length of the string to actually write.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeString(String string, int length,
			      ImageOutputStream outputStream)
	throws IOException
    {
	// Create a byte buffer of the specified length
	byte[] buffer = new byte[length];

	// Copy the bytes of the String to the byte buffer
	byte[] stringBytes = string.getBytes();
	int min = Math.min( buffer.length, stringBytes.length );
	System.arraycopy(stringBytes, 0, buffer, 0, min);

	// Write the bytes
	outputStream.write(buffer);
    }
}
