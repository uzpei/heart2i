/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.analyze.plugin;

import edu.ucla.loni.analyze.DataHistory;
import edu.ucla.loni.analyze.HeaderKey;
import edu.ucla.loni.analyze.ImageDimension;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import java.util.Vector;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * Analyze Metadata class.
 *
 * @version 3 July 2003
 */
public class AnalyzeMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_analyze_1.0";

  /** Name of the Header Key Node. */
  public static final String HEADER_KEY_NAME = "HEADER_KEY";

  /** Name of the Image Dimension Node. */
  public static final String IMAGE_DIMENSION_NAME = "IMAGE_DIMENSION";

  /** Name of the Data History Node. */
  public static final String DATA_HISTORY_NAME = "DATA_HISTORY";

  /** Single instance of the AnalyzeMetadataFormat. */
  private static AnalyzeMetadataFormat _format = new AnalyzeMetadataFormat();

  /** Constructs a AnalyzeMetadataFormat. */
  private AnalyzeMetadataFormat()
    {
      // Create the root with required children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_ALL);

      // Add the Header Key child with no children
      addElement(HEADER_KEY_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the Header Key attributes
      addAttribute(HEADER_KEY_NAME, HeaderKey.SIZEOF_HDR, DATATYPE_INTEGER,
		   true, null);
      addAttribute(HEADER_KEY_NAME, HeaderKey.DATA_TYPE, DATATYPE_STRING,
		   true, null);
      addAttribute(HEADER_KEY_NAME, HeaderKey.DB_NAME, DATATYPE_STRING,
		   true, null);
      addAttribute(HEADER_KEY_NAME, HeaderKey.EXTENTS, DATATYPE_INTEGER,
		   true, null);
      addAttribute(HEADER_KEY_NAME, HeaderKey.SESSION_ERROR, DATATYPE_INTEGER,
		   true, null, "-32768", "32767", true, true);
      addAttribute(HEADER_KEY_NAME, HeaderKey.REGULARITY, DATATYPE_INTEGER,
		   true, null, "-128", "127", true, true);
      addAttribute(HEADER_KEY_NAME, HeaderKey.HKEY_UN0, DATATYPE_INTEGER,
		   true, null, "-128", "127", true, true);

      // Add the Image Dimension child with no children
      addElement(IMAGE_DIMENSION_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the Image Dimension attributes
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_0,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_1,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_2,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_3,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_4,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_5,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_6,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_7,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.VOX_UNITS,
		   DATATYPE_STRING, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.CAL_UNITS,
		   DATATYPE_STRING, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.UNUSED_1,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);

      Vector acceptableValues = new Vector(10);
      acceptableValues.addElement("0");
      acceptableValues.addElement("1");
      acceptableValues.addElement("2");
      acceptableValues.addElement("4");
      acceptableValues.addElement("8");
      acceptableValues.addElement("16");
      acceptableValues.addElement("32");
      acceptableValues.addElement("64");
      acceptableValues.addElement("128");
      acceptableValues.addElement("255");
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DATA_TYPE,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      acceptableValues = new Vector(5);
      acceptableValues.addElement("1");
      acceptableValues.addElement("8");
      acceptableValues.addElement("16");
      acceptableValues.addElement("32");
      acceptableValues.addElement("64");
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.BIT_PIX,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.DIM_UN0,
		   DATATYPE_INTEGER, true, null, "-32768", "32767", true,
		   true);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_0,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_5,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_6,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.PIX_DIM_7,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.VOX_OFFSET,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.ROI_SCALE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.F_UNUSED_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.F_UNUSED_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.CAL_MAX,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.CAL_MIN,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.COMPRESSED,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.VERIFIED,
		   DATATYPE_FLOAT, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.GL_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(IMAGE_DIMENSION_NAME, ImageDimension.GL_MIN,
		   DATATYPE_INTEGER, true, null);

      // Add the Data History child with no children
      addElement(DATA_HISTORY_NAME, NATIVE_METADATA_FORMAT_NAME,
		 CHILD_POLICY_EMPTY);

      // Add the Data History attributes
      addAttribute(DATA_HISTORY_NAME, DataHistory.DESCRIP,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.AUX_FILE,
		   DATATYPE_STRING, true, null);

      acceptableValues = new Vector(6);
      acceptableValues.addElement("0");
      acceptableValues.addElement("1");
      acceptableValues.addElement("2");
      acceptableValues.addElement("3");
      acceptableValues.addElement("4");
      acceptableValues.addElement("5");
      addAttribute(DATA_HISTORY_NAME, DataHistory.ORIENT,
		   DATATYPE_INTEGER, true, null, acceptableValues);

      addAttribute(DATA_HISTORY_NAME, DataHistory.ORIGINATOR,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.GENERATED,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.SCAN_NUM,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.PATIENT_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.EXP_DATE,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.EXP_TIME,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.HIST_UN0,
		   DATATYPE_STRING, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.VIEWS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.VOLS_ADDED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.START_FIELD,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.FIELD_SKIP,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.O_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.O_MIN,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.S_MAX,
		   DATATYPE_INTEGER, true, null);
      addAttribute(DATA_HISTORY_NAME, DataHistory.S_MIN,
		   DATATYPE_INTEGER, true, null);
    }

  /**
   * Gets an instance of the AnalyzeMetadataFormat.
   *
   * @return The single instance of the AnalyzeMetadataFormat.
   */
  public static AnalyzeMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
