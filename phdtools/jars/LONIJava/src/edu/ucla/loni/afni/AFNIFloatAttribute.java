/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni;

import java.util.Vector;

/**
 * Header that contains elements for the AFNI format.
 *
 * @version 23 January 2003
 */
public class AFNIFloatAttribute extends AFNIAttribute
{
  /** Integer values of the attribute. */
  private Vector _values;
  
  /**
   * Constructs an AFNIStringAttribute.
   * 
   * @param name Name of the attribute.
   */
  public AFNIFloatAttribute(String name)
    {
      super(name);

      _values = new Vector();
    }

  /**
   * Gets the number of values for this attribute.
   */
  public int getCount()
    {
      return _values.size();
    }

  /**
   * Adds a value to the attribute.
   *
   * @param value Value to add.
   */
  public void addValue(float value)
    {
      _values.addElement(new Float(value));
    }
  
  /**
   * Gets the value at a certain index.
   *
   * @param index Index of the value.
   *
   * @throws ArrayIndexOutOfBoundsException If the index does not exist.
   *
   * @return float Value of attribute.
   */
  public float getValue(int index)
    {
      // Check index bounds
      if( index >= _values.size() ){
	String msg = "No value at index " + index + ".";
	throw new ArrayIndexOutOfBoundsException(msg);
      }
      
      return ((Float)_values.elementAt(index)).floatValue();
    }
}
  
