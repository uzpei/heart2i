/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni;

import java.util.Vector;

/**
 * Header that contains elements for the AFNI format.
 *
 * @version 23 January 2003
 */
public class AFNIIntegerAttribute extends AFNIAttribute
{
  /** Value of the attribute as vector of integers. */
  private Vector _values;
  
  /**
   * Constructs an AFNIStringAttribute.
   * 
   * @param name Name of the attribute.
   */
  public AFNIIntegerAttribute(String name)
    {
      super(name);

      _values = new Vector();
    }

  /**
   * Gets the number of values for this attribute.
   */
  public int getCount()
    {
      return _values.size();
    }

  /**
   * Adds a value to the attribute.
   *
   * @param value Value to set the string to.
   */
  public void addValue(int value)
    {
      _values.addElement(new Integer(value));
    }
  
  /**
   * Gets the value at a certain index.
   *
   * @param index Index of the value.
   *
   * @throws ArrayIndexOutOfBoundsException If the index does not exist.
   *
   * @return int Value of attribute.
   */
  public int getValue(int index)
    {
      // Check index bounds
      if( index >= _values.size() ){
	String msg = "No value at index " + index + ".";
	throw new ArrayIndexOutOfBoundsException(msg);
      }
      
      return ((Integer)_values.elementAt(index)).intValue();
    }
}
  
