/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.imageio.DualStreamImageReaderSpi;
import edu.ucla.loni.imageio.FileNameAssociator;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

/**
 * Dual Stream Image Reader SPI (Service Provider Interface) for the AFNI
 * image format.
 *
 * @version 15 July 2005
 */
public class AFNIImageReaderSpi extends DualStreamImageReaderSpi
{
    /** Name of the vendor who supplied this plug-in. */
    private static String _vendorName = "Laboratory of Neuro Imaging (LONI)";

    /** Version of this plug-in. */
    private static String _version = "1.0";

    /** Names of the formats read by this plug-in. */
    private static String[] _formatNames = {"afni", "AFNI"};

    /** Names of commonly used suffixes for files in the supported formats. */
    private static String[] _fileSuffixes = {"head", "HEAD"};

    /** MIME types of supported formats. */
    private static String[] _mimeTypes = {"image/head"};

    /** Name of the associated Image Reader. */
    private static String _reader = AFNIImageReader.class.getName();

    /** Allowed input type classes for the plugin-in. */
    private static Class[] _inputTypes = {AFNIInputStream.class, File.class,
					  Vector.class};

    /** Names of the associated Image Writer SPI's. */
    private static String[] _writeSpis = {AFNIImageWriterSpi.class.getName()};

    /** Constructs an AFNIImageReaderSpi. */
    public AFNIImageReaderSpi()
    {
	super(_vendorName, _version, _formatNames, _fileSuffixes, _mimeTypes,
	      _reader, _inputTypes, _writeSpis,

	      // Support one native stream metatdata format
	      true, 
	      AFNIMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      AFNIMetadataFormat.class.getName(), null, null,

	      // Do not support native image metatdata formats
	      false, null, null, null, null);
    }

    /**
     * Determines if the supplied source object appears to be in a supported
     * format.  After the determination is made, the source is reset to its
     * original state if that operation is allowed.
     *
     * @param source Source Object to examine.
     *
     * @return True if the supplied source object appears to be in a supported
     *         format; false otherwise.
     *
     * @throws IOException If an I/O error occurs while reading a stream.
     * @throws IllegalArgumentException If the source is null.
     */
    public boolean canDecodeInput(Object source) throws IOException
    {
	// Check for a null argument
	if (source == null) {
	    String msg = "Cannot have a null source.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct an AFNI Input Stream from the source
	AFNIInputStream inputStream =
	    AFNIInputStream.getAFNIInputStream(source);

	if (inputStream == null) { return false; }

	// Mark the current position of the metadata Image Input Stream
	ImageInputStream imageStream = inputStream.getMetadataInputStream();
	imageStream.mark();

	// Read the first 10 characters
	byte[] b = new byte[10];
	imageStream.readFully(b);

	// Reset the position of the stream
	imageStream.reset();

	// Check if the word "type" occurs anywhere
	String firstChars = new String(b);
	if ( firstChars.indexOf("type") != -1 ) { return true; }
	return false;
    }

    /**
     * Returns a brief, human-readable description of this service provider and
     * its associated implementation.
     *
     * @param locale Locale for which the return value should be localized.
     *
     * @return Description of this service provider.
     */
    public String getDescription(Locale locale)
    {
	return "AFNI image reader";
    }

    /**
     * Returns an instance of the ImageReader implementation associated with
     * this service provider.  The returned object will be in an initial state
     * as if its reset method had been called.
     *
     * @param extension A plug-in specific extension object, which may be null.
     *
     * @return An ImageReader instance.
     *
     * @throws IllegalArgumentException If the ImageReader finds the extension
     *                                  object to be unsuitable.
     */
    public ImageReader createReaderInstance(Object extension)
    {
	return new AFNIImageReader(this);
    }

    /**
     * Gets the File Name Associator that generates file name associations
     * between metadata files and image pixel files.
     *
     * @return File Name Associator for this format.
     */
    public FileNameAssociator getFileNameAssociator()
    {
	return new AFNINameAssociator();
    }
}
