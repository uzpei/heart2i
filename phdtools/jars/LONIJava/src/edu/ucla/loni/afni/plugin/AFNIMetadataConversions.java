/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.afni.AFNIAttribute;
import edu.ucla.loni.afni.AFNIAttributes;
import edu.ucla.loni.afni.AFNIFloatAttribute;
import edu.ucla.loni.afni.AFNIIntegerAttribute;
import edu.ucla.loni.afni.AFNIStringAttribute;
import java.util.Enumeration;
import java.util.Vector;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides conversions between the AFNI Attributes classes and
 * DOM Node representations of them.
 *
 * @version 10 May 2007
 */
public class AFNIMetadataConversions
{
  /** Constructs an AFNIMetadataConversions. */
  private AFNIMetadataConversions()
    {
    }

  /**
   * Converts the tree to an AFNIAttributes.
   *
   * @param root DOM Node representing the AFNIAttributes.
   *
   * @return AFNI Attributes constructed from the DOM Node.
   *
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   * @throws IllegalArgumentException If the root Node is null.
   */
  public static AFNIAttributes toAFNIAttributes(Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check that the root Node name matches the AFNI Attributes Node name
      String rootName = AFNIMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      if ( !root.getNodeName().equals(rootName) ) {
	String msg = "Root node must be named \"" + rootName + "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // Reconstruct all the AFNI attributes from the Nodes
      AFNIAttributes attributes = new AFNIAttributes();
      try {
	NodeList nodes = root.getChildNodes();
	for (int i = 0; i < nodes.getLength(); i++) {
	  Node node = nodes.item(i);

	  // Get the AFNI attribute name
	  String attributeName = node.getNodeName();

	  // Get the AFNI attribute type
	  String typeName = AFNIMetadataFormat.TYPE_NAME;
	  NamedNodeMap atts = node.getAttributes();
	  String type = atts.getNamedItem(typeName).getNodeValue();

	  // AFNI String attribute
	  String valueName = AFNIMetadataFormat.VALUE_NAME;
	  if ( type.equals("string-attribute") ) {

	    // Get the first String value
	    Node valueNode = node.getFirstChild();
	    NamedNodeMap valueAtts = valueNode.getAttributes();
	    String value = valueAtts.getNamedItem(valueName).getNodeValue();

	    // Create a new AFNI String attribute
	    AFNIAttribute attr = new AFNIStringAttribute(attributeName, value);
	    attributes.addAttribute(attr);
	  }

	  // AFNI Integer attribute
	  else if ( type.equals("integer-attribute") ) {

	    // Create a new AFNI Integer attribute
	    AFNIIntegerAttribute attr =new AFNIIntegerAttribute(attributeName);
	    attributes.addAttribute(attr);

	    // Get the integer attribute values
	    NodeList valueNodes = node.getChildNodes();
	    for (int j = 0; j < valueNodes.getLength(); j++) {
	      Node valueNode = valueNodes.item(j);

	      // Convert the value to an integer
	      NamedNodeMap valueAtts = valueNode.getAttributes();
	      String value = valueAtts.getNamedItem(valueName).getNodeValue();
	      attr.addValue( Integer.parseInt(value) );
	    }
	  }

	  // AFNI Float attribute
	  else if ( type.equals("float-attribute") ) {

	    // Create a new AFNI Float attribute
	    AFNIFloatAttribute attr = new AFNIFloatAttribute(attributeName);
	    attributes.addAttribute(attr);

	    // Get the float attribute values
	    NodeList valueNodes = node.getChildNodes();
	    for (int j = 0; j < valueNodes.getLength(); j++) {
	      Node valueNode = valueNodes.item(j);

	      // Convert the value to a float
	      NamedNodeMap valueAtts = valueNode.getAttributes();
	      String value = valueAtts.getNamedItem(valueName).getNodeValue();
	      attr.addValue( Float.parseFloat(value) );
	    }
	  }

	  // Unknown attribute type
	  else {
	    String msg = "Unknown attribute type:  " + type;
	    throw new IIOInvalidTreeException(msg, root);
	  }
	}
      }

      // Unable to construct one or more AFNI attributes
      catch (Exception e) {
	String msg = "Cannot convert the DOM Node to AFNI attributes.";
	throw new IIOInvalidTreeException(msg, e, root);
      }

      // Return the AFNI attributes
      return attributes;
    }

  /**
   * Converts the AFNI Attributes to a tree.
   *
   * @param afniAttributes AFNI Attributes to convert to a tree.
   *
   * @return DOM Node representing the AFNI Attributes.
   */
  public static Node toTree(AFNIAttributes afniAttributes)
    {
      // Create the root Node
      String rootName = AFNIMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      IIOMetadataNode rootNode = new IIOMetadataNode(rootName);

      // Check for a null attribute list
      if (afniAttributes == null) { return rootNode; }

      // Add each element as an attribute
      Enumeration enun = afniAttributes.getAttributes();
      while ( enun.hasMoreElements() ) {
	AFNIAttribute attribute = (AFNIAttribute)enun.nextElement();

	// Create a child node for the attribute
	String name = attribute.getName();
	IIOMetadataNode node = new IIOMetadataNode(name);
	rootNode.appendChild(node);

	// String attribute
	String typeName = AFNIMetadataFormat.TYPE_NAME;
	String attValueName = AFNIMetadataFormat.ATTRIBUTE_VALUE;
	String valueName = AFNIMetadataFormat.VALUE_NAME;
	if (attribute instanceof AFNIStringAttribute) {

	  // Add the type
	  node.setAttribute(typeName, "string-attribute");

	  // Add the value as a child node
	  IIOMetadataNode valueNode = new IIOMetadataNode(attValueName);
	  node.appendChild(valueNode);
	  String value = ((AFNIStringAttribute)attribute).getValue();
	  valueNode.setAttribute(valueName, value);
	}

	// Integer attribute
	if (attribute instanceof AFNIIntegerAttribute) {
	  
	  // Add the type
	  node.setAttribute(typeName, "integer-attribute");

	  // Add the values as child nodes
	  for (int i = 0; i < attribute.getCount(); i++) {
	    IIOMetadataNode valueNode = new IIOMetadataNode(attValueName);
	    node.appendChild(valueNode);
	    int value = ((AFNIIntegerAttribute)attribute).getValue(i);
	    valueNode.setAttribute(valueName, Integer.toString(value));
	  }
	}

	// Float attribute
	if (attribute instanceof AFNIFloatAttribute) {

	  // Add the type
	  node.setAttribute(typeName, "float-attribute");

	  // Add the values as child nodes
	  for (int i = 0; i < attribute.getCount(); i++) {
	    IIOMetadataNode valueNode = new IIOMetadataNode(attValueName);
	    node.appendChild(valueNode);
	    float value = ((AFNIFloatAttribute)attribute).getValue(i);
	    valueNode.setAttribute(valueName, Float.toString(value));
	  }
	}
      }

      // Return the root Node
      return rootNode;
    }
}
