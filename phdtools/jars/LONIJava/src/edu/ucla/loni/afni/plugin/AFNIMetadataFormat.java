/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * AFNI Metadata class.
 *
 * @version 3 July 2003
 */
public class AFNIMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_afni_1.0";

  /** Name of the Type Attribute. */
  public static final String TYPE_NAME = "type";

  /** Name of the Attribute Value Node. */
  public static final String ATTRIBUTE_VALUE = "VALUE";

  /** Name of the data value Attribute. */
  public static final String VALUE_NAME = "value";

  /** Single instance of the AFNIMetadataFormat. */
  private static AFNIMetadataFormat _format = new AFNIMetadataFormat();

  /** Constructs a AFNIMetadataFormat. */
  private AFNIMetadataFormat()
    {
      // Create the root with required children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_SEQUENCE);
    }

  /**
   * Gets an instance of the AFNIMetadataFormat.
   *
   * @return The single instance of the AFNIMetadataFormat.
   */
  public static AFNIMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }


  /**
   * Returns a String containing a description of the named element, or null.
   *
   * @param elementName Name of the element.
   * @param locale Locale for which localization will be attempted.
   *
   * @return The element description, or null if it doesn't exist.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   */
  public String getElementDescription(String elementName, Locale locale)
    {
      // Rename the VOLREG_MATVEC and VOLREG_ROTCOM elements
      if ( elementName.startsWith("VOLREG_MATVEC_") ||
	   elementName.startsWith("VOLREG_ROTCOM_") )
	{
	  elementName = elementName.substring(0, 14).concat("xxxxxx");
	}

      return _getDescription(elementName);
    }

  /**
   * Returns a String containing a description of the named attribute, or
   * null.
   *
   * @param elementName Name of the element.
   * @param attrName Name of the attribute.
   * @param locale Locale for which localization will be attempted, or null.
   *
   * @return The attribute description, or null if it doesn't exist.
   *
   * @throws IllegalArgumentException If elementName is null, or is not a
   *                                  legal element name for this format.
   * @throws IllegalArgumentException If attrName is null, or is not a legal
   *                                  legal attribute name for this element.
   */
  public String getAttributeDescription(String elementName, String attrName,
					Locale locale)
    {
      // Allow any element to have the type attribute
      if ( attrName.equals(TYPE_NAME) ) { elementName = "*"; }

      // Get the description
      String key = elementName + "/" + attrName;
      return _getDescription(key);
    }

  /**
   * Gets a description for the key.
   *
   * @param key Key to find a description for.
   *
   * @return Description for the key, or null if there isn't one.
   */
  private String _getDescription(String key)
    {
      // Get the description from the resource bundle
      try {
	String name = getResourceBaseName();
	Locale locale = Locale.getDefault();	
	ResourceBundle bundle = ResourceBundle.getBundle(name, locale);

	// Find the description
	return bundle.getString(key);
      }

      // Return null if no resource bundle is available
      catch (MissingResourceException e) { return null; }
    }
}
