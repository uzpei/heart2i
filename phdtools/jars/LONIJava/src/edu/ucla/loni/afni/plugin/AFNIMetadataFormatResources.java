/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.afni.AFNIAttributes;
import java.util.ListResourceBundle;

/**
 * List Resource Bundle that provides descriptions for AFNI Header elements.
 *
 * @version 22 January 2003
 */
public class AFNIMetadataFormatResources extends ListResourceBundle
{
  /** Constructs a AFNIMetadataFormatResources. */
  public AFNIMetadataFormatResources()
    {
    }

  /**
   * Gets the contents of the Resource Bundle.
   *
   * @return Object array of the contents, where each item of the array is a
   *         pair of Objects.  The first element of each pair is the key, which
   *         must be a String, and the second element is the value associated
   *         with that key.
   */
  public Object[][] getContents()
    {
      String formatName = AFNIMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
      String typeName = AFNIMetadataFormat.TYPE_NAME;
      String attributeValueName = AFNIMetadataFormat.ATTRIBUTE_VALUE;
      String valueName = AFNIMetadataFormat.VALUE_NAME;

      Object[][] contents = {

	  // Node name, followed by description
	  {formatName, "AFNI stream metadata."},
	  {attributeValueName, "Attribute value."},

	  // Common attributes
	  {"DATASET_RANK",
	   "Two values that determine the dimensionality of the dataset."},
	  {"DATASET_DIMENSIONS",
	   "Three values that determine the size of each spatial axis " +
	   "of the dataset."},
	  {"TYPESTRING",
	   "Determines if the dataset is of Anat or Func type."},
	  {"SCENE_DATA",
	   "Three integer codes describing the dataset type."},
	  {"ORIENT_SPECIFIC",
	   "Three integer codes describing the spatial orientation " +
	   "of the dataset axes."},
	  {"ORIGIN",
	   "Three numbers giving the xyz-coordinates of the center of " +
	   "the (0,0,0) voxel in the dataset."},
	  {"DELTA",
	   "Three numbers giving the (x,y,z) voxel sizes, in the same order " +
	   "as ORIENT_SPECIFIC."},
	  {"TAXIS_NUMS",
	   "[0] = Number of points in time. "+
	   "[1] = Number of slices with time offsets. " +
	   "[2] = Units codes for TAXIS_FLOATS[1]."},
	  {"TAXIS_FLOATS",
	   "[0] = Time origin. "+
	   "[1] = Time step (TR). "+
	   "[2] = Duration of acquisition. "+
	   "[3] = If TAXIS_NUMS[1] > 0, then this is the z-axis offset "+
	   "for the slice-dependent time offsets. "+
	   "[4] = If TAXIS_NUMS[1] > 0, then this is the z-axis step "+
	   "for the slice-dependent time offsets."},
	  {"TAXIS_OFFSETS",
	   "If TAXIS_NUMS[1] > 0, then this array gives the time " +
	   "offsets of the slices defined by TAXIS_FLOATS[3..4]."},
	  {"IDCODE_STRING",
	   "15 character string (plus NUL) giving a (hopefully) "+
	   "unique identifier for the dataset, independent of the "+
	   "filename assigned by the user."},
	  {"IDCODE_DATE",
	   "Maximum of 47 characters giving the creation date for "+
	   "the dataset."},
	  {"BYTEORDER_STRING",
	   "If this attribute is present, describes the byte-ordering "+
	   "of the data in the .BRIK file."},
	  {"BRICK_STATS",
	   "For the p-th "+
	   "sub-brick, BRICK_STATS[2*p] is the minimum value stored "+
	   "in the brick, and BRICK_STATS[2*p+1] is the maximum value "+
	   "stored in the brick."},
	  {"BRICK_TYPES",
	   "For the p-th sub-brick, BRICK_TYPES[p] is a code that tells "+
	   "the type of data stored in the .BRIK file for that sub-brick."},
	  {"BRICK_FLOAT_FACS",
	   "For the p-th sub-brick, if f=BRICK_FLOAT_FACS[p] is positive, "+
	   "then the values in the .BRIK should be scaled by f "+
	   "to give their \"true\" values."},
	  {"BRICK_LABS",
	   "These are labels for the sub-bricks, and are used in the "+
	   "choosers for sub-brick display when the dataset is a "+
	   "bucket type."},
	  {"BRICK_STATAUX",
	   "his stores auxiliary statistical information about "+
	   "sub-bricks that contain statistical parameters."},
	  {"STAT_AUX",
	   "The BRICK_STATAUX attribute allows you to attach statistical "+
	   "distribution information to arbitrary sub-bricks of a bucket "+
           "dataset."},
	  {"HISTORY_NOTE",
	   "A multi-line string giving the history of the dataset."},
	  {"NOTES_COUNT",
	   "The number of auxiliary notes attached to the dataset(0 to 999)."},
	  {"NOTE_NUMBER_001",
	   "The first auxiliary note attached to the dataset."},
	  {"TAGALIGN_MATVEC",
	   "12 numbers giving the 3x3 matrix and 3-vector of the "+
	   "transformation derived in 3dTagalign."},
	  {"VOLREG_MATVEC_xxxxxx",
	   "For sub-brick #xxxxxx (so a max of 999,999 "+
	   "sub-bricks can be used), this stores the 12 numbers "+
	   "for the matrix-vector of the transformation from 3dvolreg."},
	  {"VOLREG_ROTCOM_xxxxxx",
	   "The -rotate/-ashift options to 3drotate that are equivalent "+
	   "to the above matrix-vector transformation."},
	  {"VOLREG_CENTER_OLD",
	   "The xyz-coordinates (Dicom order) of the center of "+
	   "the input dataset to 3dvolreg."},
	  {"VOLREG_CENTER_BASE",
	   "The xyz-coordinates (Dicom order) of the center of the base "+
	   "dataset to 3dvolreg."},
	  {"VOLREG_ROTPARENT_IDCODE",
	   "If a 3dvolreg run uses the -rotparent option, then this value "+
	   "in the header of the output "+
	   "dataset tells which dataset was the rotparent."},
	  {"VOLREG_ROTPARENT_NAME",
	   "The .HEAD filename of the -rotparent."},
	  {"VOLREG_GRIDPARENT_IDCODE",
	   "Similar to the above, but for a 3dvolreg "+
	   "output dataset that was created using a -gridparent option."},
	  {"VOLREG_GRIDPARENT_NAME",
	   "The .HEAD filename of the -gridparent."},
	  {"VOLREG_INPUT_IDCODE",
	   "In the 3dvolreg output dataset header, this "+
	   "tells which dataset was the input to 3dvolreg."},
	  {"VOLREG_INPUT_NAME",
	   "The .HEAD filename of the 3dvolreg input dataset."},
	  {"VOLREG_BASE_IDCODE",
	   "In the 3dvolreg output dataset header, this "+
	   "tells which dataset was the base for registration."},
	  {"VOLREG_BASE_NAME",
	   "The .HEAD filename of the 3dvolreg base dataset."},
	  {"VOLREG_ROTCOM_NUM",
	   "The single value in here tells how many sub-bricks "+
	   "were registered by 3dvolreg."},
	  {"IDCODE_ANAT_PARENT",
	   "ID code for the \"anatomy parent\" of this dataset."},
	  {"TO3D_ZPAD",
	   "3 integers specifying how much zero-padding to3d applied "+
	   "when it created the dataset (x,y,z axes)."},
	  {"IDCODE_WARP_PARENT",
	   "ID code for the \"warp parent\" of this dataset."},
	  {"WARP_TYPE",
	   "[0] = Integer code describing the type of warp."},
	  {"WARP_DATA",
	   "Data that define the transformation from the warp parent "+
	   "to the current dataset."},
	  {"MARKS_XYZ",
	   "30 values giving the xyz-coordinates (Dicom order) of "+
	   "the markers for this dataset."},
	  {"MARKS_LAB",
	   "200 characters giving the labels for the markers."},
	  {"MARKS_HELP",
	   "2560 characters giving the help strings for the markers."},
	  {"MARKS_FLAGS",
	   "[0] = Type of markers. "+
	   "[1] = This should always be 1 (it is an \"action code\", "+
	   "but the only action ever defined was warping)."},
	  {"TAGSET_NUM",
	   "[0] = ntag = number of tags defined in the dataset (max=100). "+
	   "[1] = nfper = number of floats stored per tag (should be 5)."},
	  {"TAGSET_FLOATS",
	   "ntag*nfper values; for tag #i."},
	  {"TAGSET_LABELS",
	   "ntag sub-strings (separated by NULs) with "+
	   "the labels for each tag."},
	  {"LABEL_1",
	   "A short label describing the dataset."},
	  {"LABEL_2",
	   "Another short label describing the dataset."},
	  {"DATASET_NAME",
	   "A longer name describing the dataset contents."},
	  {"DATASET_KEYWORDS",
	   "List of keywords for this dataset."},
	  {"BRICK_KEYWORDS",
	   "List of keywords for each sub-brick of the dataset."},

	  // Node name + "/" + attribute name, followed by description
	  {"*/" + typeName, "Data type."},
	  {attributeValueName + "/" + valueName, "Attribute value."}
      };	  

      return contents;
    }
}
