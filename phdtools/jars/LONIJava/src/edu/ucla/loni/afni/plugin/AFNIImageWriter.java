/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.afni.AFNIAttribute;
import edu.ucla.loni.afni.AFNIAttributes;
import edu.ucla.loni.afni.AFNIFloatAttribute;
import edu.ucla.loni.afni.AFNIIntegerAttribute;
import edu.ucla.loni.afni.AFNIStringAttribute;
import edu.ucla.loni.imageio.DualImageOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Image Writer for parsing and encoding AFNI images.
 *
 * @version 10 May 2007
 */
public class AFNIImageWriter extends ImageWriter
{
    /** AFNI Attributes of the image sequence currently being written. */
    private AFNIAttributes _streamAttributes;

    /** Number of images in the sequence currently being written. */
    private int _numberOfImagesWritten;

    /**
     * Constructs an AFNIImageWriter.
     *
     * @param originatingProvider The ImageWriterSpi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  AFNI Image Writer Spi.
     */
    public AFNIImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an AFNI Image Writer Spi
	if ( !(originatingProvider instanceof AFNIImageWriterSpi) ) {
	    String msg = "Originating provider must an AFNI Image Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the destination to the given ImageOutputStream or other Object.  The
     * destination is assumed to be ready to accept data, and will not be closed
     * at the end of each write. If output is null, any currently set output
     * will be removed.
     *
     * @param output An Dual Image Output Stream or a Vector of two Image Output
     *               Streams to use for future writing.
     *
     * @throws IllegalArgumentException If output is not an instance of one of
     *                                  the classes returned by the originating
     *                                  service provider's getOutputTypes
     *                                  method.
     */
    public void setOutput(Object output)
    {
	// Convert a Vector into an AFNI Output Stream
	if (output instanceof Vector) {
	    output = new DualImageOutputStream( (Vector)output );
	}

	super.setOutput(output);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank metadata object
	return new AFNIMetadata(null);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding an
     * image of the given type.
     *
     * @param imageType An ImageTypeSpecifier indicating the format of the image
     *                  to be written later.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					       ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing stream metadata, used to
     *               initialize the state of the returned object.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize AFNI Metadata
	if (inData instanceof AFNIMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing image metadata, used to
     *               initialize the state of the returned object.
     * @param imageType An ImageTypeSpecifier indicating the layout and color
     *                  information of the image with which the metadata will be
     *                  associated.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If either of inData or imageType is
     *                                  null.
     */
    public IIOMetadata convertImageMetadata(IIOMetadata inData,
					    ImageTypeSpecifier imageType,
					    ImageWriteParam param)
    {
	// Not applicable
	return null;
    }

    /**
     * Returns true if the methods that take an IIOImage parameter are capable
     * of dealing with a Raster (as opposed to RenderedImage) source image.
     *
     * @return True if Raster sources are supported.
     */
    public boolean canWriteRasters()
    {
	return true;
    }

    /**
     * Appends a complete image stream containing a single image and associated
     * stream and image metadata and thumbnails to the output.
     *
     * @param streamMetadata An IIOMetadata object representing stream metadata,
     *                       or null to use default values.
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam, or null to use a default
     *              ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, if the stream
     *                                  metadata cannot be converted, or if the
     *                                  image type is unrecognized.
     * @throws IllegalStateException If the output has not been set.
     */
    public void write(IIOMetadata streamMetadata, IIOImage image,
		      ImageWriteParam param) throws IOException
    {
	// Write a sequence with 1 image
	prepareWriteSequence(streamMetadata);
	writeToSequence(image, param);
	endWriteSequence();
    }

    /**
     * Returns true if the writer is able to append an image to an image
     * stream that already contains header information and possibly prior
     * images.
     *
     * @return True If images may be appended sequentially.
     */
    public boolean canWriteSequence()
    {
	return true;
    }

    /**
     * Prepares a stream to accept a series of subsequent writeToSequence calls,
     * using the provided stream metadata object.  The metadata will be written
     * to the stream if it should precede the image data.
     *
     * @param streamMetadata A stream metadata object.
     *
     * @throws IllegalArgumentException If the stream metadata is null or cannot
     *                                  be converted.
     */
    public void prepareWriteSequence(IIOMetadata streamMetadata)
    {
	// Convert the metadata to AFNI Metadata
	IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the stream metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct a stream AFNI Attributes from the metadata
	try {
	    String rootName = AFNIMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	    Node tree = metadata.getAsTree(rootName);
	    _streamAttributes = AFNIMetadataConversions.toAFNIAttributes(tree);
	}
      
	catch (Exception e) {
	    String msg = "Unable to construct an AFNI Attributes from the " +
		"metadata.";
	    throw new IllegalArgumentException(msg);
	}

	_numberOfImagesWritten = 0;
    }

    /**
     * Appends a single image and possibly associated metadata and thumbnails,
     * to the output.  The supplied ImageReadParam is ignored.
     *
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam or null to use a default ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null or if the image
     *                                  type is not recognized.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void writeToSequence(IIOImage image, ImageWriteParam param)
	throws IOException
    { 
	// Check for the stream AFNI Attributes
	if (_streamAttributes == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Check for a null image
	if (image == null) {
	    String msg = "Cannot write a null image.";
	    throw new IllegalArgumentException(msg);
	}

	// Get a Raster from the image
	Raster raster = image.getRaster();
	if (raster == null) {
	    RenderedImage renderedImage = image.getRenderedImage();

	    // If the Rendered Image is a Buffered Image, get it directly
	    if (renderedImage instanceof BufferedImage) {
		raster = ((BufferedImage)renderedImage).getRaster();
	    }

	    // Otherwise get a copy of the Raster from the Rendered Image
	    raster = renderedImage.getData();
	}

	// Update the Listeners
	processImageStarted(_numberOfImagesWritten);

	// Write an 8 bit grayscale image
	ImageOutputStream outputStream = _getPixelDataOutputStream();
	DataBuffer dataBuffer = raster.getDataBuffer();
	if (dataBuffer instanceof DataBufferByte) {
	    outputStream.write( ((DataBufferByte)dataBuffer).getData() );
	}

	// Write a 16 bit grayscale image
	else if (dataBuffer instanceof DataBufferUShort) {
	    short[] data = ((DataBufferUShort)dataBuffer).getData();
	    int offset = 0;
	    int length = data.length;

	    outputStream.writeShorts(data, offset, length);
	}

	// Unrecognized type
	else {
	    String msg = "Unable to write the IIOImage.";
	    throw new IllegalArgumentException(msg);
	}

	// Update the Listeners
	_numberOfImagesWritten++;
	if ( abortRequested() ) { processWriteAborted(); }
	else { processImageComplete(); }
    }

    /**
     * Completes the writing of a sequence of images begun with
     * prepareWriteSequence.  Any stream metadata that should come at the end of
     * the sequence of images is written out, and any header information at the
     * beginning of the sequence is patched up if necessary.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void endWriteSequence() throws IOException
    {
	// Check for the stream AFNI Attributes
	if (_streamAttributes == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Write the Attributes to the metadata output stream
	ImageOutputStream outputStream = _getMetadataOutputStream();
	Enumeration enun = _streamAttributes.getAttributes();
	while ( enun.hasMoreElements() ) {
	    AFNIAttribute attr = (AFNIAttribute)enun.nextElement();

	    // Write some filler space
	    outputStream.writeBytes("\n\n");

	    // Write the Attribute
	    if (attr instanceof AFNIStringAttribute) {
		_writeStringAttribute((AFNIStringAttribute)attr, outputStream);
	    }
	    else if (attr instanceof AFNIIntegerAttribute) {
		_writeIntegerAttribute((AFNIIntegerAttribute)attr,outputStream);
	    }
	    else if (attr instanceof AFNIFloatAttribute) {
		_writeFloatAttribute((AFNIFloatAttribute)attr, outputStream);
	    }
	}

	// Reset the stream Attributes
	_streamAttributes = null;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageWriter.  Otherwise, the writer may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the output stream
	try { if (output != null) { ((DualImageOutputStream)output).close(); } }
	catch (Exception e) {}

	output = null;
    }

    /**
     * Gets the output stream for the metadata.
     *
     * @return Image Output Stream to write AFNI metadata to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getMetadataOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream for the metadata
	return ((DualImageOutputStream)output).getMetadataOutputStream();
    }

    /**
     * Gets the output stream for the pixel data.
     *
     * @return Image Output Stream to write AFNI image pixel data to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getPixelDataOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream for image pixel data
	return ((DualImageOutputStream)output).getPixelDataOutputStream();
    }

    /**
     * Writes the String Attribute to the output stream.
     *
     * @param attribute String Attribute to write.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeStringAttribute(AFNIStringAttribute attribute,
				       ImageOutputStream outputStream)
	throws IOException
    {
	StringBuffer buffer = new StringBuffer();

	// Construct the string representation
	buffer.append("type = string-attribute\n");
	buffer.append("name = " + attribute.getName() + "\n");
	buffer.append("count = " + attribute.getCount() + "\n");
	buffer.append("'" + attribute.getValue());

	// Write the string representation to the output stream
	outputStream.writeBytes( buffer.toString() );
    }

    /**
     * Writes the Integer Attribute to the output stream.
     *
     * @param attribute Integer Attribute to write.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeIntegerAttribute(AFNIIntegerAttribute attribute,
					ImageOutputStream outputStream)
	throws IOException
    {
	StringBuffer buffer = new StringBuffer();

	// Construct the string representation
	buffer.append("type = integer-attribute\n");
	buffer.append("name = " + attribute.getName() + "\n");
	buffer.append("count = " + attribute.getCount() );

	// Add the integer values
	for (int i = 0; i < attribute.getCount(); i++) {

	    // Start a new line after every 5 values
	    if (i % 5 == 0) { buffer.append("\n"); }

	    // Add the integer value
	    buffer.append(" " + Integer.toString( attribute.getValue(i) ));
	}

	// Write the string representation to the output stream
	outputStream.writeBytes( buffer.toString() );
    }

    /**
     * Writes the Float Attribute to the output stream.
     *
     * @param attribute Float Attribute to write.
     * @param outputStream Image Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeFloatAttribute(AFNIFloatAttribute attribute,
				      ImageOutputStream outputStream)
	throws IOException
    {
	StringBuffer buffer = new StringBuffer();

	// Construct the string representation
	buffer.append("type = float-attribute\n");
	buffer.append("name = " + attribute.getName() + "\n");
	buffer.append("count = " + attribute.getCount() );

	// Add the float values
	for (int i = 0; i < attribute.getCount(); i++) {

	    // Start a new line after every 5 values
	    if (i % 5 == 0) { buffer.append("\n"); }

	    // Add the float value
	    buffer.append(" " + Float.toString( attribute.getValue(i) ));
	}

	// Write the string representation to the output stream
	outputStream.writeBytes( buffer.toString() );
    }
}
