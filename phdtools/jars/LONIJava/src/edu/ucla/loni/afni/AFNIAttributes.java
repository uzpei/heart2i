/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.afni;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Header that contains elements for the AFNI format.
 *
 * @version 10 May 2007
 */
public class AFNIAttributes
{
  /** Vector of AFNIAttribute's. */
  private Vector _attributes;
  
  /** Constructs an AFNIAttributes. */
  public AFNIAttributes()
    {
      _attributes = new Vector();
    }

  /**
   * Adds the Attribute to the end of the attributes.
   *
   * @param attribute Attribute to add.
   */
  public void addAttribute(AFNIAttribute attribute)
    {
      _attributes.addElement(attribute);
    }

  /**
   * Gets the named Attribute.
   *
   * @param name Name of the Attribute.
   *
   * @return Named Attribute, or null if it does not exist.
   */
  public AFNIAttribute getAttribute(String name)
    {
      // Search the Attributes for the name
      Enumeration enun = getAttributes();
      while ( enun.hasMoreElements() ) {
	AFNIAttribute attribute = (AFNIAttribute)enun.nextElement();
	if ( name.equals( attribute.getName() ) ) { return attribute; }
      }

      // Named Attribute not found
      return null;
    }
	
  /**
   * Gets the attributes.
   *
   * @return Enumeration of AFNI Attributes.
   */
  public Enumeration getAttributes()
    {
      return _attributes.elements();
    }
}
