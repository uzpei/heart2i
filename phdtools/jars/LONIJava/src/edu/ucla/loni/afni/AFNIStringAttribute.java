/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni;

/**
 * Header that contains elements for the AFNI format.
 *
 * @version 23 January 2003
 */
public class AFNIStringAttribute extends AFNIAttribute
{
  /** String value of the attribute. */
  private String _value;
  
  /**
   * Constructs an AFNIStringAttribute.
   * 
   * @param name Name of the attribute.
   * @param value String value of the attribute.
   *
   * @throws NullPointerException If the name is null.
   */
  public AFNIStringAttribute(String name, String value)
    {
      super(name);

      // Set a null String to the empty String
      _value = value;
      if (_value == null) { _value = ""; }
    }

  /**
   * Gets the number of values for this attribute.
   */
  public int getCount()
    {
      return _value.length();
    }

  /**
   * Gets the value string.
   *
   * @return String Value of the attribute.
   */
  public String getValue()
    {
      return _value;
    }
}
  
