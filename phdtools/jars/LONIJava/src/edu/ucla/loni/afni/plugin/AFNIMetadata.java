/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.afni.AFNIAttribute;
import edu.ucla.loni.afni.AFNIAttributes;
import edu.ucla.loni.afni.AFNIFloatAttribute;
import edu.ucla.loni.afni.AFNIIntegerAttribute;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * IIO Metadata for metadata of the AFNI volume format.
 *
 * @version 14 January 2009
 */
public class AFNIMetadata extends AppletFriendlyIIOMetadata
{
    /** AFNI Attributes containing the original metadata. */
    private AFNIAttributes _originalAFNIAttributes;

    /** AFNI Attributes containing the current metadata. */
    private AFNIAttributes _currentAFNIAttributes;

    /**
     * Constructs a AFNIMetadata from the given AFNI Attributes.
     *
     * @param afniAttributes AFNI Attributes containing the metadata.
     */
    public AFNIMetadata(AFNIAttributes afniAttributes)
    {
	super(true, AFNIMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      AFNIMetadataFormat.class.getName(),
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalAFNIAttributes = afniAttributes;
	_currentAFNIAttributes = afniAttributes;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object acafniding to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return AFNIMetadataConversions.toTree(_currentAFNIAttributes);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current AFNI Attributes, convert the root
	Node r;
	if (_currentAFNIAttributes == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Nodes
	else {

	    // Recreate the root tree
	    r = new IIOMetadataNode(nativeMetadataFormatName);

	    // Get the current metadata tree Nodes
	    Node currentRoot = getAsTree(formatName);
	    NodeList currentNodeList = currentRoot.getChildNodes();

	    // Get the new metadata tree Nodes
	    NodeList nodeList = root.getChildNodes();

	    // Add the current Nodes to the new root tree
	    for (int i = 0; i < currentNodeList.getLength(); i++) {
		Node currentNode = currentNodeList.item(i);

		// Replace the current Node if a new one exists
		Node node = _getNode(currentNode.getNodeName(), nodeList);
		if (node != null) { r.appendChild(node); }
		else { r.appendChild(currentNode); }
	    }
	}

	// Set a new current AFNI Attributes
	_currentAFNIAttributes = AFNIMetadataConversions.toAFNIAttributes(r);
    }

    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current AFNI Attributes to the original 
	_currentAFNIAttributes = _originalAFNIAttributes;
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// Only support non-inverted grayscale
	return Utilities.getGrayScaleChromaNode(false);
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	try {

	    // Get the brick types (default of short)
	    int bitsPerPixel = 16;
	    AFNIAttribute types
		= _currentAFNIAttributes.getAttribute("BRICK_TYPES");
	    if (types != null && types.getCount() > 0) {
		int type = ((AFNIIntegerAttribute)types).getValue(0);

		// Check for byte or short type
		if (type == 0) { bitsPerPixel = 8; }
		if (type == 1) { bitsPerPixel = 16; }

		// Unknown type
		else { return null; }
	    }

	    // Otherwise support grayscale data
	    return Utilities.getGrayScaleDataNode(bitsPerPixel);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Returns an IIOMetadataNode representing the dimension format information
     * of the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the dimension information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Get the delta values
	    AFNIAttribute deltas = _currentAFNIAttributes.getAttribute("DELTA");
	    float deltaX = ((AFNIFloatAttribute)deltas).getValue(0);
	    float deltaY = ((AFNIFloatAttribute)deltas).getValue(1);

	    // Return the dimension node
	    return Utilities.getDimensionNode(deltaX, deltaY);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	AFNIAttributes attributes = _currentAFNIAttributes;

	// Image dimensions
	int imageWidth = 0;
	int imageHeight = 0;
	int numberOfImages = 0;
	try {
	    AFNIAttribute rank = attributes.getAttribute("DATASET_RANK");
	    AFNIAttribute dim = attributes.getAttribute("DATASET_DIMENSIONS");

	    imageWidth = ((AFNIIntegerAttribute)dim).getValue(0);
	    imageHeight = ((AFNIIntegerAttribute)dim).getValue(1);

	    int numberOfSubBricks = ((AFNIIntegerAttribute)rank).getValue(1);
	    int dim3 = ((AFNIIntegerAttribute)dim).getValue(2);
	    numberOfImages = numberOfSubBricks*dim3;
	}
	catch (Exception e) {}

	// Pixel dimensions and slice thickness
	float pixelWidth = 0;
	float pixelHeight = 0;
	float sliceThickness = 0;
	try {
	    AFNIAttribute deltas = attributes.getAttribute("DELTA");

	    pixelWidth = ((AFNIFloatAttribute)deltas).getValue(0);
	    pixelHeight = ((AFNIFloatAttribute)deltas).getValue(1);
	    sliceThickness = ((AFNIFloatAttribute)deltas).getValue(2);
	}
	catch (Exception e) {}

	// Data type and bits per pixel
	String dataType = "Undetermined";
	int bitsPerPixel = 16;
	try {
	    AFNIAttribute types = attributes.getAttribute("BRICK_TYPES");
	    int type = ((AFNIIntegerAttribute)types).getValue(0);

	    if (type == 0) {
		dataType = BasicMetadataFormat.BYTE_TYPE;
		bitsPerPixel = 8;
	    }
	    if (type == 1) {
		dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE;
		bitsPerPixel = 16;
	    }
	}
	catch (Exception e) {}

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;

	// Return the basic tree based upon available information
	if (pixelWidth == 0 && pixelHeight == 0 && sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, dataType,
						  bitsPerPixel, colorType);
	}
	if (sliceThickness == 0) {
	    return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
						  numberOfImages, pixelWidth,
						  pixelHeight, dataType,
						  bitsPerPixel, colorType);
	}
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }

    /**
     * Gets the named Node in the given Node list.
     *
     * @param nodeName Name of the Node.
     * @param nodeList Node list to check for the named Node.
     *
     * @return Named Node in the given Node list, or null if the Node does not
     *         exist.
     */
    private Node _getNode(String nodeName, NodeList nodeList)
    {
	for (int i = 0; i < nodeList.getLength(); i++) {
	    Node node = nodeList.item(i);
	    if (nodeName.equals( node.getNodeName() )) { return node; }
	}

	// No Node exists with the name
	return null;
    }
}
