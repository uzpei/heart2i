/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.afni.AFNIAttribute;
import edu.ucla.loni.afni.AFNIAttributes;
import edu.ucla.loni.afni.AFNIFloatAttribute;
import edu.ucla.loni.afni.AFNIIntegerAttribute;
import edu.ucla.loni.afni.AFNIStringAttribute;
import edu.ucla.loni.imageio.Utilities;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for parsing and decoding AFNI images.
 *
 * @version 15 April 2005
 */
public class AFNIImageReader extends ImageReader
{
  /** AFNI Attributes decoded from the input source. */
  private AFNIAttributes _afniAttributes;

  /**
   * Constructs an AFNI Image Reader.
   *
   * @param originatingProvider The Image Reader SPI that instantiated this
   *                            Object.
   *
   * @throws IllegalArgumentException If the originating provider is not a
   *                                  AFNI Image Reader Spi.
   */
  public AFNIImageReader(ImageReaderSpi originatingProvider)
    {
      super(originatingProvider);

      // Originating provider must be an AFNI Image Reader Spi
      if ( !(originatingProvider instanceof AFNIImageReaderSpi) ) {
	String msg = "Originating provider must be an AFNI Image Reader " +
	             "SPI.";
	throw new IllegalArgumentException(msg);
      }
    }

  /**
   * Sets the input source to use to the given ImageInputStream or other
   * Object.  The input source must be set before any of the query or read
   * methods are used.  If the input is null, any currently set input source
   * will be removed.
   *
   * @param input The ImageInputStream or other Object to use for future
   *              decoding.
   * @param seekForwardOnly If true, images and metadata may only be read in
   *                        ascending order from this input source.
   * @param ignoreMetadata If true, metadata may be ignored during reads.
   *
   * @throws IllegalArgumentException If the input is not an instance of one of
   *                                  the classes returned by the originating
   *                                  service provider's getInputTypes method,
   *                                  or is not an ImageInputStream.
   * @throws IllegalStateException If the input source is an Input Stream that
   *                               doesn't support marking and seekForwardOnly
   *                               is false.
   */
  public void setInput(Object input, boolean seekForwardOnly,
		       boolean ignoreMetadata)
    {
      // Convert the input source into an AFNI Input Stream
      input = AFNIInputStream.getAFNIInputStream(input);

      super.setInput(input, seekForwardOnly, ignoreMetadata);

      // Remove any decoded AFNI Attributes
      _afniAttributes = null;
    }

  /**
   * Returns the number of images, not including thumbnails, available from the
   * current input source.
   * 
   * @param allowSearch If true, the true number of images will be returned
   *                    even if a search is required.  If false, the reader may
   *                    return -1 without performing the search.
   *
   * @return The number of images or -1 if allowSearch is false and a search
   *         would be required.
   *
   * @throws IllegalStateException If the input source has not been set.
   * @throws IOException If an error occurs reading the information from the
   *                     input source.
   */
  public int getNumImages(boolean allowSearch) throws IOException
    {
      AFNIAttributes attributes = _getAFNIAttributes();

      // Get the number of sub-bricks
      AFNIAttribute rank = attributes.getAttribute("DATASET_RANK");
      if (rank == null || rank.getCount() < 2) {
	String msg = "Cannot determine the number of images.";
	throw new IOException(msg);
      }
      int numberOfSubBricks = ((AFNIIntegerAttribute)rank).getValue(1);

      // Get the number of time steps
      AFNIAttribute dim = attributes.getAttribute("DATASET_DIMENSIONS");
      if (dim == null || dim.getCount() < 3) {
	String msg = "Cannot determine the number of images.";
	throw new IOException(msg);
      }
      int numberOfImages = ((AFNIIntegerAttribute)dim).getValue(2);

      // Return the total number of images
      return numberOfImages*numberOfSubBricks;
    }

  /**
   * Returns the width in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The width of the image in pixels.
   *
   * @throws IOException If an error occurs reading the width information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getWidth(int imageIndex) throws IOException
    {
      AFNIAttributes attributes = _getAFNIAttributes();

      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the number of voxels along the x-axis
      AFNIAttribute dim = attributes.getAttribute("DATASET_DIMENSIONS");
      if (dim == null || dim.getCount() < 1) {
	String msg = "Cannot determine the image width.";
	throw new IOException(msg);
      }

      // Return the width
      return ((AFNIIntegerAttribute)dim).getValue(0);
    }

  /**
   * Returns the height in pixels of the given image within the input source.
   *
   * @param imageIndex The index of the image to be queried.
   *
   * @return The height of the image in pixels.
   *
   * @throws IOException If an error occurs reading the height information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public int getHeight(int imageIndex) throws IOException
    {
      AFNIAttributes attributes = _getAFNIAttributes();

      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the number of voxels along the y-axis
      AFNIAttribute dim = attributes.getAttribute("DATASET_DIMENSIONS");
      if (dim == null || dim.getCount() < 2) {
	String msg = "Cannot determine the image height.";
	throw new IOException(msg);
      }

      // Return the height
      return ((AFNIIntegerAttribute)dim).getValue(1);
    }

  /**
   * Returns an Iterator containing possible image types to which the given
   * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
   * legal image type will be returned.
   *
   * @param imageIndex The index of the image to be retrieved.
   *
   * @return An Iterator containing at least one ImageTypeSpecifier
   *         representing suggested image types for decoding the current given
   *         image.
   *
   * @throws IOException If an error occurs reading the format information from
   *                     the input source.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public Iterator getImageTypes(int imageIndex) throws IOException
    {
      AFNIAttributes attributes = _getAFNIAttributes();

      // Check the bounds of the image index
      _checkImageIndex(imageIndex);

      // Get the brick types (default of short)
      int bitsPerPixel = 16;
      AFNIAttribute types = attributes.getAttribute("BRICK_TYPES");
      if (types != null && types.getCount() > 0) {
	int type = ((AFNIIntegerAttribute)types).getValue(0);

	// Check for byte or short type
	if (type == 0) { bitsPerPixel = 8; }
	if (type == 1) { bitsPerPixel = 16; }

	// Unknown type
	else {
	  String msg = "Unable to recognize the image type.";
	  throw new IOException(msg);
	}
      }

      // Get image parameters
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Return the Image Type Specifier
      ArrayList list = new ArrayList(1);
      list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
      return list.iterator();
    }

  /**
   * Returns an IIOMetadata object representing the metadata associated with
   * the input source as a whole (i.e., not associated with any particular
   * image), or null if the reader does not support reading metadata, is set
   * to ignore metadata, or if no metadata is available.
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   */
  public IIOMetadata getStreamMetadata() throws IOException
    {
      return new AFNIMetadata( _getAFNIAttributes() );
    }

  /**
   * Returns an IIOMetadata object containing metadata associated with the
   * given image, or null if the reader does not support reading metadata, is
   * set to ignore metadata, or if no metadata is available.
   *
   * @param imageIndex Index of the image whose metadata is to be retrieved. 
   *
   * @return An IIOMetadata object, or null.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
      // Not applicable
      return null;
    }

  /**
   * Reads the image indexed by imageIndex and returns it as a complete
   * BufferedImage.  The supplied ImageReadParam is ignored.
   *
   * @param imageIndex The index of the image to be retrieved.
   * @param param An ImageReadParam used to control the reading process, or
   *              null.
   *
   * @return The desired portion of the image as a BufferedImage.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  public BufferedImage read(int imageIndex, ImageReadParam param)
    throws IOException
    {
      int width = getWidth(imageIndex);
      int height = getHeight(imageIndex);

      // Update the Listeners
      processImageStarted(imageIndex);

      // Compute the size of the image in bytes
      ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
      int dataType = spec.getSampleModel().getDataType();
      int imageSize = width*height;
      if (dataType == DataBuffer.TYPE_USHORT) { imageSize *= 2; }

      // Set the pixel data Image Input Stream position to the image beginning
      ImageInputStream imageStream = _getPixelDataInputStream();
      imageStream.seek(imageIndex*imageSize);

      // Return the raw image data if required
      if ( Utilities.returnRawBytes(param) ) {
	BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	imageStream.read( ((DataBufferByte)db).getData() );
	return bufferedImage;
      }

      // Create a Buffered Image for the image data
      Iterator iter = getImageTypes(0);
      BufferedImage bufferedImage = getDestination(null, iter, width, height);
      DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

      // Read the image data into an array
      byte[] srcBuffer = new byte[imageSize];
      imageStream.readFully(srcBuffer);

      // 8-bit gray image pixel data
      WritableRaster dstRaster = bufferedImage.getRaster();
      if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
      }

      // 16-bit gray image pixel data
      else {

	// Determine if the bytes need to be swapped
	boolean isByteSwapped = false;
	AFNIAttributes attributes = _getAFNIAttributes();
	AFNIAttribute order = attributes.getAttribute("BYTEORDER_STRING");
	if (order != null) {

	  // See if the Most Signficant Bit is last
	  if ( ((AFNIStringAttribute)order).getValue().equals("MSB_LAST~") ) {
	    isByteSwapped = true;
	  }
	}

	bufferedImage = Utilities.getGrayUshortImage(srcBuffer, dstRaster, 16,
						     false, isByteSwapped,
						     1, 0);
      }

      // Update the Listeners
      if ( abortRequested() ) { processReadAborted(); }
      else { processImageComplete(); }

      // Return the requested image
      return bufferedImage;
    }

  /**
   * Allows any resources held by this object to be released.  It is important
   * for applications to call this method when they know they will no longer
   * be using this ImageReader.  Otherwise, the reader may continue to hold on
   * to resources indefinitely.
   */
  public void dispose()
    {
      // Attempt to close the input stream
      try { if (input != null) { ((AFNIInputStream)input).close(); } }
      catch (Exception e) {}

      input = null;
    }

  /**
   * Reads the AFNI Attributes from the input source.  If the AFNI Attributes 
   * has already been decoded, no additional read is performed.
   *
   * @return AFNI Attributes decoded from the input source.
   * 
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   */
  private AFNIAttributes _getAFNIAttributes() throws IOException
    {
      // Decode the AFNI Attributes if needed
      if (_afniAttributes == null) {
	_afniAttributes = new AFNIAttributes();

	// Get the AFNI Attributes
	AFNIAttribute attribute = _getAFNIAttribute();
	while (attribute != null) {
	  _afniAttributes.addAttribute(attribute);
	  attribute = _getAFNIAttribute();
	}
      }
      
      return _afniAttributes;
    }

  /**
   * Reads the next AFNI Attribute from the input source.
   *
   * @return AFNI Attribute decoded from the input source, or null if there
   *         isn't one to decode.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   */
  private AFNIAttribute _getAFNIAttribute() throws IOException
    {
      // Determine if there is an attribute to decode
      String nextLine = _getInputLine();
      if (nextLine == null) { return null; }

      try {

	// Get the attribute type
	StringTokenizer st = new StringTokenizer(nextLine);
	st.nextToken();
	st.nextToken();
	String type = st.nextToken();

	// Get the attribute name
	nextLine = _getInputLine();
	st = new StringTokenizer(nextLine);
	st.nextToken();
	st.nextToken();
	String name = st.nextToken();

	// Get the attribute count
	nextLine = _getInputLine();
	st = new StringTokenizer(nextLine);
	st.nextToken();
	st.nextToken();
	int count = Integer.parseInt( st.nextToken() );

	// AFNI String Attribute
	if ( type.equals("string-attribute") ) {

	  // Get the metadata Image Input Stream
	  ImageInputStream imageStream = _getMetadataInputStream();

	  // Read in the specified number of characters (plus initial ')
	  byte[] array = new byte[count+1];
	  imageStream.readFully(array);
	  String value = new String(array, 1, count);
	  return new AFNIStringAttribute(name, value);
	}

	// AFNI Integer Attribute
	else if ( type.equals("integer-attribute") ) {
	  AFNIIntegerAttribute attr = new AFNIIntegerAttribute(name);

	  // Read in the specified number of integers
	  while ( attr.getCount() < count ) {

	    // Read the next line
	    nextLine = _getInputLine();
	    st = new StringTokenizer(nextLine);

	    // Parse each token of the line into an integer
	    while ( st.hasMoreTokens() ) {
	      attr.addValue( Integer.parseInt( st.nextToken() ) );
	    }
	  }

	  return attr;
	}

	// AFNI Float Attribute
	else if ( type.equals("float-attribute") ) {
	  AFNIFloatAttribute attr = new AFNIFloatAttribute(name);

	  // Read in the specified number of floats
	  while ( attr.getCount() < count ) {

	    // Read the next line
	    nextLine = _getInputLine();
	    st = new StringTokenizer(nextLine);

	    // Parse each token of the line into a float
	    while ( st.hasMoreTokens() ) {
	      attr.addValue( Float.parseFloat( st.nextToken() ) );
	    }
	  }

	  return attr;
	}

	// Unknown Attribute
	else {
	  String msg = "Unknown Attribute type:  " + type;
	  throw new IOException(msg);
	}
      }

      catch (Exception e) {
	String msg = "Illegal attribute definition:  ";
	if (nextLine == null) { msg += "MISSING A LINE"; }
	else { msg += nextLine; }
	throw new IOException(msg);
      }
    }

  /**
   * Reads the next non-empty line from the input source.
   *
   * @return Next non-empty line from the input source, or null if no such line
   *         exists.
   *
   * @throws IOException If an error occurs during reading.
   * @throws IllegalStateException If the input source has not been set.
   */
  private String _getInputLine() throws IOException
    {
      // Get the metadata Image Input Stream
      ImageInputStream imageStream = _getMetadataInputStream();

      String line = "";
      while (line != null) {

	// Read the next line from the input source
	line = imageStream.readLine();
	if (line != null) {

	  // Determine if there are any parsed strings
	  StringTokenizer st = new StringTokenizer(line);
	  if ( st.hasMoreTokens() ) { return line; }
	}
      }

      // No empty line exists
      return null;
    }

  /**
   * Checks whether or not the specified image index is valid.
   *
   * @param imageIndex The index of the image to check.
   *
   * @throws IOException If an error occurs reading the information from the
   *                     input source.
   * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
   */
  private void _checkImageIndex(int imageIndex) throws IOException
    {
      // Check the bounds of the image index
      if ( imageIndex >= getNumImages(false) ) {
	String msg = "Image index of " + imageIndex + " >= the total number " +
	             "of images (" + getNumImages(false) + ")";
	throw new IndexOutOfBoundsException(msg);
      }
    }

  /**
   * Gets the input stream for the metadata.
   *
   * @return Image Input Stream to read AFNI metadata from.
   *
   * @throws IllegalStateException If the input has not been set.
   */
  private ImageInputStream _getMetadataInputStream()
    {
      // No input has been set
      if (input == null) {
	String msg = "No input has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Input Stream for image metadata
      return ((AFNIInputStream)input).getMetadataInputStream();
    }

  /**
   * Gets the input stream for the pixel data.
   *
   * @return Image Input Stream to write AFNI pixel data to.
   *
   * @throws IllegalStateException If the input has not been set.
   */
  private ImageInputStream _getPixelDataInputStream()
    {
      // No input has been set
      if (input == null) {
	String msg = "No input has been set.";
	throw new IllegalStateException(msg);
      }

      // Return the Image Input Stream for image pixel data
      return ((AFNIInputStream)input).getPixelDataInputStream();
    }
}
