/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.imageio.FileNameAssociator;

/**
 * File Name Associator for generating file name associations between AFNI
 * files.
 *
 * @version 19 July 2005
 */
public class AFNINameAssociator extends FileNameAssociator
{
    /** Constructs an AFNI Name Associator. */
    public AFNINameAssociator()
    {
    }

    /**
     * Determines whether or not this file contains metadata or image pixel
     * data.
     *
     * @return True if this file contains metadata, false otherwise.
     */
    public boolean isMetadataFile(String fileName)
    {
	// No name 
	if (fileName == null) { return false; }

	// AFNI metadata files end with ".head"
	return fileName.toLowerCase().endsWith(".head");
    }

    /**
     * Gets all the allowed file names that can be associated with the specified
     * file name.
     *
     * @param fileName Name of the file to get associated file names for.
     *
     * @return All the allowed file names that can be associated with the
     *         specified file name.  An empty array is returned if there are no
     *         associated file names.
     */
    public String[] getAssociatedFileNames(String fileName)
    {
	// No name 
	if (fileName == null) { return new String[0]; }

	// File is the metadata file
	if ( isMetadataFile(fileName) ) {
	    return _getSuffixReplacements(fileName, "head", "brik");
	}

	// File is the image pixel file
	else if ( fileName.toLowerCase().endsWith(".brik") ) {
	    return _getSuffixReplacements(fileName, "brik", "head");
	}

	// Not an AFNI file
	return new String[0];
    }
}
