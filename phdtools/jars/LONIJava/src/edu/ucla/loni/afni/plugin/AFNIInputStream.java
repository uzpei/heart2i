/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.afni.plugin;

import edu.ucla.loni.imageio.DualImageInputStream;
import java.io.File;
import java.util.Vector;
import javax.imageio.stream.ImageInputStream;

/**
 * Dual Image Input Stream for AFNI files.
 *
 * @version 15 July 2005
 */
public class AFNIInputStream extends DualImageInputStream
{
    /**
     * Constructs an AFNI Input Stream for the two Image Input Streams.
     *
     * @param metadataInputStream Image Input Stream for AFNI metadata.
     * @param pixelDataInputStream Image Input Stream for AFNI image pixel data.
     *
     * @throws NullPointerException If either Image Input Stream is null.
     */
    public AFNIInputStream(ImageInputStream metadataInputStream,
			   ImageInputStream pixelDataInputStream)
    {
	super(metadataInputStream, pixelDataInputStream);
    }

    /**
     * Constructs an AFNI Input Stream from the Vector of two Image Input
     * Streams.
     *
     * @param streams Vector of two Image Input Streams; the first is for the
     *                metadata and the second is for the image pixel data.
     *
     * @throws IllegalArgumentException If the Vector does not contain two Image
     *                                  Input Streams.
     */
    public AFNIInputStream(Vector streams)
    {
	super(streams);
    }

    /**
     * Constructs an AFNI Input Stream from the File.  The file name suffix
     * is used to identify whether the file contains metadata or image pixel
     * data, and its associated file is assumed to be in the same directory and
     * to have the same file name except for the file name suffix.
     *
     * @param file Metadata or image pixel data File.
     *
     * @throws IllegalArgumentException If the file associated with the given
     *                                  file cannot be determined, or if the
     *                                  files cannot be accessed.
     */
    public AFNIInputStream(File file)
    {
	super(file, new AFNINameAssociator());
    }

    /**
     * Gets an AFNI Input Stream from the supplied source object.
     *
     * @param source Source object to construct an AFNI Input Stream from.
     *
     * @return AFNI Input Stream constructed from the supplied source object,
     *         or null if one cannot be created.
     */
    public static AFNIInputStream getAFNIInputStream(Object source)
    {
	// Check for a null argument
	if (source == null) { return null; }

	// Check for an AFNI Input Stream
	else if (source instanceof AFNIInputStream) {
	    return (AFNIInputStream)source;
	}

	// Check for a Vector of Image Input Streams
	else if (source instanceof Vector) {
	    try { return new AFNIInputStream( (Vector)source ); }
	    catch (IllegalArgumentException iae) { return null; }
	}

	// Check for a File
	else if (source instanceof File) {
	    try { return new AFNIInputStream( (File)source ); }
	    catch (IllegalArgumentException iae) { return null; }
	}

	// Unsupported source
	return null;
    }
}
