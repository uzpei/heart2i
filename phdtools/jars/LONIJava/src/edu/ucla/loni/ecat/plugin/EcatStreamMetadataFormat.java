/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.ecat.Ecat7MainHeader;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata in the
 * ECAT image stream.
 *
 * @version 14 April 2005
 */
public class EcatStreamMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
  /** Name of the native metadata format. */
  public static final String NATIVE_METADATA_FORMAT_NAME =
  "edu_ucla_loni_ecat7_stream_1.0";

  /** Single instance of the ECAT Stream Metadata Format. */
  private static EcatStreamMetadataFormat _format =
  new EcatStreamMetadataFormat();

  /** Constructs an ECAT Stream Metadata Format. */
  private EcatStreamMetadataFormat()
    {
      // Create the root with no children
      super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_EMPTY);
      String rootName = NATIVE_METADATA_FORMAT_NAME;

      // Add the Main Header attributes
      addAttribute(rootName, Ecat7MainHeader.MAGIC_NUMBER,
                   DATATYPE_STRING, true, "MATRIX72v");
      addAttribute(rootName, Ecat7MainHeader.ORIGINAL_FILE_NAME,
                   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.SW_VERSION,
                   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.SYSTEM_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILE_TYPE,
                   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.SERIAL_NUMBER,
                   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.SCAN_START_TIME,
                   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.ISOTOPE_NAME, 
                   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.ISOTOPE_HALFLIFE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.RADIOPHARMACEUTICAL,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.GANTRY_TILT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.GANTRY_ROTATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_ELEVATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.INTRINSIC_TILT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.WOBBLE_SPEED,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.TRANSM_SOURCE_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.DISTANCE_SCANNED,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.TRANSAXIAL_FOV,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.ANGULAR_COMPRESSION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.COIN_SAMP_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.AXIAL_SAMP_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName,Ecat7MainHeader.ECAT_CALIBRATION_FACTOR,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.CALIBRATION_UNITS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName,Ecat7MainHeader.CALIBRATION_UNITS_LABEL,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.COMPRESSION_CODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.STUDY_TYPE,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_ID,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_SEX,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_DEXTERITY,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_AGE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_HEIGHT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_WEIGHT,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_BIRTH_DATE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.PHYSICIAN_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName,  Ecat7MainHeader.OPERATOR_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.STUDY_DESCRIPTION,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.ACQUISITION_TYPE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.PATIENT_ORIENTATION,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FACILITY_NAME,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.NUM_PLANES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.NUM_FRAMES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.NUM_GATES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.NUM_BED_POS,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.INIT_BED_POSITION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_0,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_1,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_2,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_3,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_4,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_5,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_6,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_7,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_8,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_9,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_10,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_11,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_12,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_13,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BED_POSITION_14,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.PLANE_SEPARATION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.LWR_SCTR_THRES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.LWR_TRUE_THRES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.UPR_TRUE_THRES,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.USER_PROCESS_CODE,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.ACQUISITION_MODE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.BIN_SIZE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.BRANCHING_FRACTION,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.DOSE_START_TIME,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.DOSAGE,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.WELL_COUNTER_CORR_FACTOR,
		   DATATYPE_FLOAT, true, null);
      addAttribute(rootName, Ecat7MainHeader.DATA_UNITS,
		   DATATYPE_STRING, true, null);
      addAttribute(rootName, Ecat7MainHeader.SEPTA_STATE,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILL_CTI_0,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILL_CTI_1,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILL_CTI_2,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILL_CTI_3,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILL_CTI_4,
		   DATATYPE_INTEGER, true, null);
      addAttribute(rootName, Ecat7MainHeader.FILL_CTI_5,
		   DATATYPE_INTEGER, true, null);

      // Set the resource base name for element descriptions
      setResourceBaseName( EcatMetadataFormatResources.class.getName() );
    }

  /**
   * Gets an instance of the ECAT Stream Metadata Format.
   *
   * @return The single instance of the ECAT Stream Metadata Format.
   */
  public static EcatStreamMetadataFormat getInstance()
    {
      return _format;
    }

  /**
   * Returns true if the element (and the subtree below it) is allowed to
   * appear in a metadata document for an image of the given type, defined by
   * an ImageTypeSpecifier.
   *
   * @param elementName The name of the element being queried.
   * @param imageType An ImageTypeSpecifier indicating the type of the image
   *                  that will be associated with the metadata.
   *
   * @return True if the node is meaningful for images of the given type.
   */
  public boolean canNodeAppear(String elementName,
			       ImageTypeSpecifier imageType)
    {
      // Not implemented
      return true;
    }
}
