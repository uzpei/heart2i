/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.ecat.Ecat7ImageHeader;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import edu.ucla.loni.imageio.BasicMetadataFormat;
import edu.ucla.loni.imageio.BasicMetadataTreeMaker;
import edu.ucla.loni.imageio.Utilities;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * IIO Metadata for image metadata of the ECAT image format.
 *
 * @version 18 February 2009
 */
public class EcatImageMetadata extends AppletFriendlyIIOMetadata
{
    /** ECAT 7 Image Header containing original metadata. */
    private Ecat7ImageHeader _originalImageHeader;

    /** ECAT 7 Image Header containing current metadata. */
    private Ecat7ImageHeader _currentImageHeader;

    /**
     * Constructs an ECAT Image Metadata.
     *
     * @param imageHeader ECAT 7 Image Header.
     */
    public EcatImageMetadata(Ecat7ImageHeader imageHeader)
    {
	super(true, EcatImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	      EcatImageMetadataFormat.class.getName(),
	      new String[]{BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME},
	      new String[]{BasicMetadataFormat.class.getName()});

	_originalImageHeader = imageHeader;
	_currentImageHeader = imageHeader;
    }

    /**
     * Returns true if this object does not support the mergeTree, setFromTree,
     * and reset methods.  
     *
     * @return True if this IIOMetadata object cannot be modified; false o/w.
     */
    public boolean isReadOnly()
    {
	return false;
    }

    /**
     * Returns an XML DOM Node object that represents the root of a tree of
     * metadata contained within this object according to the conventions
     * defined by a given metadata format.
     *
     * @param formatName Name of the desired metadata format.
     *
     * @return An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names.
     */
    public Node getAsTree(String formatName)
    {
	// Standard format name
	String standardName = IIOMetadataFormatImpl.standardMetadataFormatName;
	if ( standardName.equals(formatName) ) { return getStandardTree(); }

	// Native metadata format name
	if ( nativeMetadataFormatName.equals(formatName) ) {
	    return EcatMetadataConversions.toTree(_currentImageHeader);
	}

	// LONI Basic format name
	String basicName = BasicMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( basicName.equals(formatName) ) { return _getBasicTree(); }

	// Unknown format name
	String msg = "The format name \"" + formatName + "\" is not a " +
	    "supported metadata format name.";
	throw new IllegalArgumentException(msg);
    }

    /**
     * Alters the internal state of this IIOMetadata object from a tree of XML
     * DOM Nodes whose syntax is defined by the given metadata format.  The
     * previous state is altered only as necessary to accomodate the nodes that
     * are present in the given tree.
     *
     * @param formatName Name of the desired metadata format.
     * @param root An XML DOM Node object forming the root of a tree.
     *
     * @throws IllegalArgumentException If formatName is not one of the allowed
     *                                  metadata format names, or if the root is
     *                                  null.
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     */
    public void mergeTree(String formatName, Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check for a matching root Node
	if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	    String msg = "Root node must be named \"" +
		nativeMetadataFormatName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// If there is no current metadata, convert the root
	Node r;
	if (_currentImageHeader == null) { r = root; }

	// Otherwise merge the root with the current metadata tree Node
	else {
	    Node imageNode = getAsTree(formatName);
	    r = _getMergedNode(imageNode, root.getAttributes());
	}

	// Set the new current ECAT Main Header
	_currentImageHeader = EcatMetadataConversions.toEcat7ImageHeader(r);
    }


    /**
     * Resets all the data stored in this object to default values, usually to
     * the state this object was in immediately after construction, though the
     * precise semantics are plug-in specific.  Note that there are many
     * possible default values, depending on how the object was created.
     */
    public void reset()
    {
	// Reset the current Image Header
	_currentImageHeader = _originalImageHeader;
    }

    /**
     * Gets the width of each image.
     *
     * @return Width of each image.
     *
     * @return Number of images, or -1 if it cannot be determined.
     */
    public int getImageWidth()
    {
	try {

	    // Use the X Dimension for the image width
	    String name = Ecat7ImageHeader.X_DIMENSION;
	    Object value = _currentImageHeader.getElementValue(name);
	    return ((Short)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the height of each image.
     *
     * @return Height of each image, or -1 if it cannot be determined.
     */
    public int getImageHeight()
    {
	try {

	    // Use the Y Dimension for the image height
	    String name = Ecat7ImageHeader.Y_DIMENSION;
	    Object value = _currentImageHeader.getElementValue(name);
	    return ((Short)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Gets the number of bits per pixel used by each image.
     *
     * @return Number of bits per pixel used by each image, or -1 if it cannot
     *         be determined.
     */
    public int getBitsPerPixel()
    {
	try {
	    int ECAT7_SUNI2 = 6;

	    // Use the data type to determine the bits per pixel
	    String name = Ecat7ImageHeader.DATA_TYPE;
	    Object value = _currentImageHeader.getElementValue(name);
	    short dataType = ((Number)value).shortValue();

	    // Only support 16 bits per pixel
	    if (dataType == ECAT7_SUNI2) { return 16; }
	    return -1;
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Determines whether or not the image data has signed values.
     *
     * @return True if the image data has signed values; false otherwise.
     */
    public boolean hasSignedData()
    {
	try {

	    // Use the Image Min value to make the determination
	    String name = Ecat7ImageHeader.IMAGE_MIN;
	    Object value = _currentImageHeader.getElementValue(name);
	    short min = ((Number)value).shortValue();

	    // Mininum is negative
	    return min < 0;
	}

	// Unable to make the determination
	catch (Exception e) { return false; }
    }

    /**
     * Gets the matrix id of the image.
     *
     * @return Matrix id of the image, or -1 if it cannot be determined.
     */
    public int getMatrixId()
    {
	try {
	    String name = Ecat7ImageHeader.MATRIX_ID;
	    Object value = _currentImageHeader.getElementValue(name);
	    return ((Integer)value).intValue();
	}

	// Unable to make the determination
	catch (Exception e) { return -1; }
    }

    /**
     * Returns an IIOMetadataNode representing the chroma information of the
     * standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the chroma information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardChromaNode()
    {
	// Only support non-inverted grayscale
	return Utilities.getGrayScaleChromaNode(false);
    }

    /**
     * Returns an IIOMetadataNode representing the data format information of
     * the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the data information, or null if
     *         it does not exist.
     */
    protected IIOMetadataNode getStandardDataNode()
    {
	int bitsPerPixel = getBitsPerPixel();

	// No information available
	if (bitsPerPixel == -1) { return null; }

	// Otherwise support grayscale data
	return Utilities.getGrayScaleDataNode(bitsPerPixel);
    }

    /**
     * Returns an IIOMetadataNode representing the dimension format information
     * of the standard javax_imageio_1.0 metadata format, or null if no such
     * information is available.
     *
     * @return IIOMetadataNode representing the dimension information, or null
     *         if it does not exist.
     */
    protected IIOMetadataNode getStandardDimensionNode()
    {
	try {

	    // Get the X pixel size (cm -> mm)
	    String name = Ecat7ImageHeader.X_PIXEL_SIZE;
	    Object value = _currentImageHeader.getElementValue(name);
	    float pixelWidth = 10*((Number)value).floatValue();

	    // Get the Y pixel size (cm -> mm)
	    name = Ecat7ImageHeader.Y_PIXEL_SIZE;
	    value = _currentImageHeader.getElementValue(name);
	    float pixelHeight = 10*((Number)value).floatValue();

	    // Return the dimension node
	    return Utilities.getDimensionNode(pixelWidth, pixelHeight);
	}

	// Unable to make the determination
	catch (Exception e) { return null; }
    }

    /**
     * Gets the LONI Basic metadata tree.
     *
     * @return DOM Node representing the LONI Basic metadata, or null if it can
     *         not be determined.
     */
    private Node _getBasicTree()
    {
	// Image dimensions
	int imageWidth = getImageWidth();
	int imageHeight = getImageHeight();
	int numberOfImages = 1;

	// Pixel width (cm -> mm)
	String name = Ecat7ImageHeader.X_PIXEL_SIZE;
	Object value = _currentImageHeader.getElementValue(name);
	float pixelWidth = 10*((Number)value).floatValue();

	// Pixel height (cm -> mm)
	name = Ecat7ImageHeader.Y_PIXEL_SIZE;
	value = _currentImageHeader.getElementValue(name);
	float pixelHeight = 10*((Number)value).floatValue();

	// Slice thickness (cm -> mm)
	name = Ecat7ImageHeader.Z_PIXEL_SIZE;
	value = _currentImageHeader.getElementValue(name);
	float sliceThickness = 10*((Number)value).floatValue();

	// Bits per pixel
	int bitsPerPixel = getBitsPerPixel();

	// Data type
	String dataType = "Undetermined";
	if (bitsPerPixel == 8) { dataType = BasicMetadataFormat.BYTE_TYPE; }
	if (bitsPerPixel == 16) {
	    if ( hasSignedData() ) {
		dataType = BasicMetadataFormat.SIGNED_SHORT_TYPE;
	    }
	    else { dataType = BasicMetadataFormat.UNSIGNED_SHORT_TYPE; }
	}

	// Color type
	String colorType = BasicMetadataFormat.GRAY_TYPE;

	// Return the basic tree
	return BasicMetadataTreeMaker.getTree(imageWidth, imageHeight,
					      numberOfImages, pixelWidth,
					      pixelHeight, sliceThickness,
					      dataType, bitsPerPixel,
					      colorType);
    }

    /**
     * Merges the attributes with the specified Node.  If there are attribute
     * names not present in the attributes of the node, the attributes are
     * ignored.
     *
     * @param node Node to merge the attributes with.
     * @param attributes Attributes to merge.
     *
     * @return New Node containing the merged attributes.
     */
    private Node _getMergedNode(Node node, NamedNodeMap attributes)
    {
	// Create a new Node with the same name
	IIOMetadataNode mergedNode = new IIOMetadataNode( node.getNodeName() );

	// Copy the Node attributes to the new Node
	NamedNodeMap map = node.getAttributes();
	if (map != null) {
	    for (int i = 0; i < map.getLength(); i++) {
		Node attr = map.item(i);
		mergedNode.setAttribute(attr.getNodeName(),attr.getNodeValue());
	    }
	}

	// Merge the new attributes with the old
	if (attributes != null) {
	    for (int i = 0; i < attributes.getLength(); i++) {
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName();

		// Attribute name must be present in the new Node
		if ( mergedNode.hasAttribute(attributeName) ) {
		    mergedNode.setAttribute(attributeName,
					    attribute.getNodeValue());
		}
	    }
	}

	// Return the new merged Node
	return mergedNode;
    }
}
