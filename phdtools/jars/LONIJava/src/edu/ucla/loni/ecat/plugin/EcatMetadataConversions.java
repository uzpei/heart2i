/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.Conversions;
import edu.ucla.loni.ecat.Ecat7ImageHeader;
import edu.ucla.loni.ecat.Ecat7MainHeader;
import edu.ucla.loni.ecat.EcatElementMap;
import java.util.Iterator;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Class that provides conversions between the ECAT Header classes and the
 * DOM Node representations of them.
 *
 * @version 26 July 2006
 */
public class EcatMetadataConversions
{
    /** Constructs an ECAT Metadata Conversions. */
    private EcatMetadataConversions()
    {
    }

    /**
     * Converts the tree to an ECAT 7 Main Header.
     *
     * @param root DOM Node representing the ECAT 7 Main Header.
     *
     * @return ECAT 7 Main Header constructed from the DOM Node.
     *
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     * @throws IllegalArgumentException If the root Node is null.
     */
    public static Ecat7MainHeader toEcat7MainHeader(Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check that the root Node name matches the required name
	String rootName = EcatStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( !root.getNodeName().equals(rootName) ) {
	    String msg = "Root node must be named \"" + rootName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// Attempt to construct an ECAT 7 Main Header from the Node attributes
	try {
	    NamedNodeMap atts = root.getAttributes();

	    // Magic number.
	    String id = Ecat7MainHeader.MAGIC_NUMBER;
	    byte[] magicNumber =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Original file name.
	    id = Ecat7MainHeader.ORIGINAL_FILE_NAME;
	    byte[] originalFileName =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Sw version.
	    id = Ecat7MainHeader.SW_VERSION;
	    short swVersion =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // System type.
	    id = Ecat7MainHeader.SYSTEM_TYPE;
	    short systemType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // File type.
	    id = Ecat7MainHeader.FILE_TYPE;
	    short fileType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Serial number.
	    id = Ecat7MainHeader.SERIAL_NUMBER;
	    byte[] serialNumber =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Scan start time.
	    id = Ecat7MainHeader.SCAN_START_TIME;
	    int scanStartTime =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Isotope name.
	    id = Ecat7MainHeader.ISOTOPE_NAME;
	    byte[] isotopeName =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Isotope halflife.
	    id = Ecat7MainHeader.ISOTOPE_HALFLIFE;
	    float isotopeHalflife =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Radiopharmaceutical.
	    id = Ecat7MainHeader.RADIOPHARMACEUTICAL;
	    byte[] radiopharmaceutical =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Gantry tilt.
	    id = Ecat7MainHeader.GANTRY_TILT;
	    float gantryTilt =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Gantry rotation.
	    id = Ecat7MainHeader.GANTRY_ROTATION;
	    float gantryRotation =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed elevation.
	    id = Ecat7MainHeader.BED_ELEVATION;
	    float bedElevation =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Intrinsic tilt.
	    id = Ecat7MainHeader.INTRINSIC_TILT;
	    float intrinsicTilt =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Wobble speed.
	    id = Ecat7MainHeader.WOBBLE_SPEED;
	    short wobbleSpeed =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Transm source type.
	    id = Ecat7MainHeader.TRANSM_SOURCE_TYPE;
	    short transmSourceType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Distance scanned.
	    id = Ecat7MainHeader.DISTANCE_SCANNED;
	    float distanceScanned =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Transaxial fov.
	    id = Ecat7MainHeader.TRANSAXIAL_FOV;
	    float transaxialFov =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Angular compression.
	    id = Ecat7MainHeader.ANGULAR_COMPRESSION;
	    short angularCompression =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Coin samp mode.
	    id = Ecat7MainHeader.COIN_SAMP_MODE;
	    short coinSampMode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Axial samp mode.
	    id = Ecat7MainHeader.AXIAL_SAMP_MODE;
	    short axialSampMode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Ecat calibration factor.
	    id = Ecat7MainHeader.ECAT_CALIBRATION_FACTOR;
	    float ecatCalibrationFactor =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Calibration units.
	    id = Ecat7MainHeader.CALIBRATION_UNITS;
	    short calibrationUnits =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Calibration units label.
	    id = Ecat7MainHeader.CALIBRATION_UNITS_LABEL;
	    short calibrationUnitsLabel =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Compression code.
	    id = Ecat7MainHeader.COMPRESSION_CODE;
	    short compressionCode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Study type.
	    id = Ecat7MainHeader.STUDY_TYPE;
	    byte[] studyType = atts.getNamedItem(id).getNodeValue().getBytes();

	    // Patient id.
	    id = Ecat7MainHeader.PATIENT_ID;
	    byte[] patientId = atts.getNamedItem(id).getNodeValue().getBytes();

	    // Patient name.
	    id = Ecat7MainHeader.PATIENT_NAME;
	    byte[] patientName =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Patient sex.
	    id = Ecat7MainHeader.PATIENT_SEX;
	    byte[] patientSex = atts.getNamedItem(id).getNodeValue().getBytes();

	    // Patient dexterity.
	    id = Ecat7MainHeader.PATIENT_DEXTERITY;
	    byte[] patientDexterity =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Patient age.
	    id = Ecat7MainHeader.PATIENT_AGE;
	    float patientAge =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Patient height.
	    id = Ecat7MainHeader.PATIENT_HEIGHT;
	    float patientHeight =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Patient weight.
	    id = Ecat7MainHeader.PATIENT_WEIGHT;
	    float patientWeight =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Patient birth date.
	    id = Ecat7MainHeader.PATIENT_BIRTH_DATE;
	    int patientBirthDate =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Physician name.
	    id = Ecat7MainHeader.PHYSICIAN_NAME;
	    byte[] physicianName =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Operator name.
	    id = Ecat7MainHeader.OPERATOR_NAME;
	    byte[] operatorName =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Study description
	    id = Ecat7MainHeader.STUDY_DESCRIPTION;
	    byte[] studyDescription =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Acquisition type.
	    id = Ecat7MainHeader.ACQUISITION_TYPE;
	    short acquisitionType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Patient orientation.
	    id = Ecat7MainHeader.PATIENT_ORIENTATION;
	    short patientOrientation =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Facility name.
	    id = Ecat7MainHeader.FACILITY_NAME;
	    byte[] facilityName =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Num planes.
	    id = Ecat7MainHeader.NUM_PLANES;
	    short numPlanes =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Num frames.
	    id = Ecat7MainHeader.NUM_FRAMES;
	    short numFrames =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Num gates.
	    id = Ecat7MainHeader.NUM_GATES;
	    short numGates =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Num bed pos.
	    id = Ecat7MainHeader.NUM_BED_POS;
	    short numBedPos =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Init bed position.
	    id = Ecat7MainHeader.INIT_BED_POSITION;
	    float initBedPosition =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 0.
	    float[] bedPosition = new float[15];
	    id = Ecat7MainHeader.BED_POSITION_0;
	    bedPosition[0] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 1.
	    id = Ecat7MainHeader.BED_POSITION_1;
	    bedPosition[1] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 2.
	    id = Ecat7MainHeader.BED_POSITION_2;
	    bedPosition[2] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 3.
	    id = Ecat7MainHeader.BED_POSITION_3;
	    bedPosition[3] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 4.
	    id = Ecat7MainHeader.BED_POSITION_4;
	    bedPosition[4] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 5.
	    id = Ecat7MainHeader.BED_POSITION_5;
	    bedPosition[5] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 6.
	    id = Ecat7MainHeader.BED_POSITION_6;
	    bedPosition[6] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 7.
	    id = Ecat7MainHeader.BED_POSITION_7;
	    bedPosition[7] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 8.
	    id = Ecat7MainHeader.BED_POSITION_8;
	    bedPosition[8] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 9.
	    id = Ecat7MainHeader.BED_POSITION_9;
	    bedPosition[9] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 10.
	    id = Ecat7MainHeader.BED_POSITION_10;
	    bedPosition[10] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 11.
	    id = Ecat7MainHeader.BED_POSITION_11;
	    bedPosition[11] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 12.
	    id = Ecat7MainHeader.BED_POSITION_12;
	    bedPosition[12] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 13.
	    id = Ecat7MainHeader.BED_POSITION_13;
	    bedPosition[13] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Bed position 14.
	    id = Ecat7MainHeader.BED_POSITION_14;
	    bedPosition[14] =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Plane separation.
	    id = Ecat7MainHeader.PLANE_SEPARATION;
	    float planeSeparation =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Lwr sctr thres.
	    id = Ecat7MainHeader.LWR_SCTR_THRES;
	    short lwrSctrThres =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Lwr true thres.
	    id = Ecat7MainHeader.LWR_TRUE_THRES;
	    short lwrTrueThres =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Upr true thres.
	    id = Ecat7MainHeader.UPR_TRUE_THRES;
	    short uprTrueThres =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // User process code.
	    id = Ecat7MainHeader.USER_PROCESS_CODE;
	    byte[] userProcessCode =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Acquisition mode.
	    id = Ecat7MainHeader.ACQUISITION_MODE;
	    short acquisitionMode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Bin size.
	    id = Ecat7MainHeader.BIN_SIZE;
	    float binSize =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Branching fraction.
	    id = Ecat7MainHeader.BRANCHING_FRACTION;
	    float branchingFraction =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Dose start time.
	    id = Ecat7MainHeader.DOSE_START_TIME;
	    int doseStartTime =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Dosage.
	    id = Ecat7MainHeader.DOSAGE;
	    float dosage =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Well counter corr factor.
	    id = Ecat7MainHeader.WELL_COUNTER_CORR_FACTOR;
	    float wellCounterCorrFactor =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Data units.
	    id = Ecat7MainHeader.DATA_UNITS;
	    byte[] dataUnits = atts.getNamedItem(id).getNodeValue().getBytes();

	    // Septa state.
	    id = Ecat7MainHeader.SEPTA_STATE;
	    short septaState =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti 0.
	    short[] fillCti = new short[6];
	    id = Ecat7MainHeader.FILL_CTI_0;
	    fillCti[0] =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti 1.
	    id = Ecat7MainHeader.FILL_CTI_1;
	    fillCti[1] =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti 2.
	    id = Ecat7MainHeader.FILL_CTI_2;
	    fillCti[2] =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti 3.
	    id = Ecat7MainHeader.FILL_CTI_3;
	    fillCti[3] =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti 4.
	    id = Ecat7MainHeader.FILL_CTI_4;
	    fillCti[4] =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti 5.
	    id = Ecat7MainHeader.FILL_CTI_5;
	    fillCti[5] =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Return an ECAT 7 Main Header
	    return new Ecat7MainHeader(magicNumber, originalFileName, 
				       swVersion, systemType, fileType,
				       serialNumber, scanStartTime,
				       isotopeName, isotopeHalflife,
				       radiopharmaceutical, gantryTilt,
				       gantryRotation, bedElevation,
				       intrinsicTilt, wobbleSpeed,
				       transmSourceType, distanceScanned,
				       transaxialFov, angularCompression,
				       coinSampMode, axialSampMode,
				       ecatCalibrationFactor, calibrationUnits,
				       calibrationUnitsLabel, compressionCode,
				       studyType, patientId,
				       patientName, patientSex,
				       patientDexterity, patientAge,
				       patientHeight, patientWeight,
				       patientBirthDate, physicianName,
				       operatorName, studyDescription,
				       acquisitionType, patientOrientation,
				       facilityName, numPlanes, numFrames,
				       numGates, numBedPos,
				       initBedPosition, bedPosition,
				       planeSeparation, lwrSctrThres,
				       lwrTrueThres, uprTrueThres,
				       userProcessCode, acquisitionMode,
				       binSize, branchingFraction,
				       doseStartTime, dosage,
				       wellCounterCorrFactor, dataUnits,
				       septaState, fillCti);
	}

	// Unable to construct an ECAT 7 Main Header
	catch (Exception e) {
	    String msg = "Cannot convert a Node into an ECAT 7 Main Header.";
	    throw new IIOInvalidTreeException(msg, e, root);
	}
    }

    /**
     * Converts the tree to an ECAT 7 Image Header.
     *
     * @param root DOM Node representing the ECAT 7 Image Header.
     *
     * @return ECAT 7 Image Header constructed from the DOM Node.
     *
     * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
     *                                 using the rules of the given format.
     * @throws IllegalArgumentException If the root Node is null.
     */
    public static Ecat7ImageHeader toEcat7ImageHeader(Node root)
	throws IIOInvalidTreeException
    {
	// Check for a null root Node
	if (root == null) {
	    String msg = "A null root Node is not allowed.";
	    throw new IllegalArgumentException(msg);
	}

	// Check that the root Node name matches the required name
	String rootName = EcatImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	if ( !root.getNodeName().equals(rootName) ) {
	    String msg = "Root node must be named \"" + rootName + "\".";
	    throw new IIOInvalidTreeException(msg, root);
	}

	// Attempt to construct an ECAT 7 Image Header from the Node attributes
	try {
	    NamedNodeMap atts = root.getAttributes();

	    // Matrix id.
	    String id = Ecat7ImageHeader.MATRIX_ID;
	    int matrixId =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Data type.
	    id = Ecat7ImageHeader.DATA_TYPE;
	    short dataType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Number of dimensions.
	    id = Ecat7ImageHeader.NUM_DIMENSIONS;
	    short numDimensions =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // X dimension.
	    id = Ecat7ImageHeader.X_DIMENSION;
	    short xDimension =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Y dimension.
	    id = Ecat7ImageHeader.Y_DIMENSION;
	    short yDimension =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Z dimension.
	    id = Ecat7ImageHeader.Z_DIMENSION;
	    short zDimension =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // X offset (cm).
	    id = Ecat7ImageHeader.X_OFFSET;
	    float xOffset =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Y offset (cm).
	    id = Ecat7ImageHeader.Y_OFFSET;
	    float yOffset =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Z offset (cm).
	    id = Ecat7ImageHeader.Z_OFFSET;
	    float zOffset =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Reconstruction maginification factor.
	    id = Ecat7ImageHeader.RECON_ZOOM;
	    float reconZoom =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Scale factor.
	    id = Ecat7ImageHeader.SCALE_FACTOR;
	    float scaleFactor =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Image min.
	    id = Ecat7ImageHeader.IMAGE_MIN;
	    short imageMin =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Image max.
	    id = Ecat7ImageHeader.IMAGE_MAX;
	    short imageMax =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // X dimension pixel size (cm).
	    id = Ecat7ImageHeader.X_PIXEL_SIZE;
	    float xPixelSize =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Y dimension pixel size (cm).
	    id = Ecat7ImageHeader.Y_PIXEL_SIZE;
	    float yPixelSize =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Z dimension pixel size (cm).
	    id = Ecat7ImageHeader.Z_PIXEL_SIZE;
	    float zPixelSize =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Frame duration (msec).
	    id = Ecat7ImageHeader.FRAME_DURATION;
	    int frameDuration =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Offset from first frame (msec).
	    id = Ecat7ImageHeader.FRAME_START_TIME;
	    int frameStartTime =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Filter code.
	    id = Ecat7ImageHeader.FILTER_CODE;
	    short filterCode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // X resolution (cm).
	    id = Ecat7ImageHeader.X_RESOLUTION;
	    float xResolution =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Y resolution (cm).
	    id = Ecat7ImageHeader.Y_RESOLUTION;
	    float yResolution =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Z resolution (cm).
	    id = Ecat7ImageHeader.Z_RESOLUTION;
	    float zResolution =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Number R elements from sinogram.
	    id = Ecat7ImageHeader.NUM_R_ELEMENTS;
	    float numRElements =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Number of angles from sinogram.
	    id = Ecat7ImageHeader.NUM_ANGLES;
	    float numAngles =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Rotation in the xy plane (degrees).
	    id = Ecat7ImageHeader.Z_ROTATION_ANGLE;
	    float zRotationAngle =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Decay corr factor.
	    id = Ecat7ImageHeader.DECAY_CORR_FCTR;
	    float decayCorrFctr =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Processing code.
	    id = Ecat7ImageHeader.PROCESSING_CODE;
	    int processingCode =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Gate duration.
	    id = Ecat7ImageHeader.GATE_DURATION;
	    int gateDuration =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // R wave offset.
	    id = Ecat7ImageHeader.R_WAVE_OFFSET;
	    int rWaveOffset =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Number of accepted beats.
	    id = Ecat7ImageHeader.NUM_ACCEPTED_BEATS;
	    int numAcceptedBeats =
		Integer.parseInt(atts.getNamedItem(id).getNodeValue());

	    // Filter cutoff frequency.
	    id = Ecat7ImageHeader.FILTER_CUTOFF_FREQUENCY;
	    float filterCutoffFrequency =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Filter resolution.
	    id = Ecat7ImageHeader.FILTER_RESOLUTION;
	    float filterResolution =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Filter ramp slope.
	    id = Ecat7ImageHeader.FILTER_RAMP_SLOPE;
	    float filterRampSlope =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Filter order.
	    id = Ecat7ImageHeader.FILTER_ORDER;
	    short filterOrder =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Filter scatter fraction.
	    id = Ecat7ImageHeader.FILTER_SCATTER_FRACTION;
	    float filterScatterFraction =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Filter scatter slope.
	    id = Ecat7ImageHeader.FILTER_SCATTER_SLOPE;
	    float filterScatterSlope =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Annotation.
	    id = Ecat7ImageHeader.ANNOTATION;
	    byte[] annotation =
		atts.getNamedItem(id).getNodeValue().getBytes();

	    // Matrix 1 1.
	    id = Ecat7ImageHeader.MT_1_1;
	    float mt11 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 1 2.
	    id = Ecat7ImageHeader.MT_1_2;
	    float mt12 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 1 3.
	    id = Ecat7ImageHeader.MT_1_3;
	    float mt13 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 2 1.
	    id = Ecat7ImageHeader.MT_2_1;
	    float mt21 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 2 2.
	    id = Ecat7ImageHeader.MT_2_2;
	    float mt22 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 2 3.
	    id = Ecat7ImageHeader.MT_2_3;
	    float mt23 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 3 1.
	    id = Ecat7ImageHeader.MT_3_1;
	    float mt31 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 3 2.
	    id = Ecat7ImageHeader.MT_3_2;
	    float mt32 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 3 3.
	    id = Ecat7ImageHeader.MT_3_3;
	    float mt33 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // R filter cutoff.
	    id = Ecat7ImageHeader.RFILTER_CUTOFF;
	    float rFilterCutoff =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // R filter resolution.
	    id = Ecat7ImageHeader.RFILTER_RESOLUTION;
	    float rFilterResolution =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // R filter code.
	    id = Ecat7ImageHeader.RFILTER_CODE;
	    short rFilterCode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // R filter order.
	    id = Ecat7ImageHeader.RFILTER_ORDER;
	    short rFilterOrder =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Z filter cutoff.
	    id = Ecat7ImageHeader.ZFILTER_CUTOFF;
	    float zFilterCutoff =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Z filter resolution.
	    id = Ecat7ImageHeader.ZFILTER_RESOLUTION;
	    float zFilterResolution =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Z filter code.
	    id = Ecat7ImageHeader.ZFILTER_CODE;
	    short zFilterCode =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Z filter coder.
	    id = Ecat7ImageHeader.ZFILTER_ORDER;
	    short zFilterOrder =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Matrix 1 4.
	    id = Ecat7ImageHeader.MT_1_4;
	    float mt14 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 2 4.
	    id = Ecat7ImageHeader.MT_2_4;
	    float mt24 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Matrix 3 4.
	    id = Ecat7ImageHeader.MT_3_4;
	    float mt34 =
		Float.parseFloat(atts.getNamedItem(id).getNodeValue());

	    // Scatter type.
	    id = Ecat7ImageHeader.SCATTER_TYPE;
	    short scatterType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Recon type.
	    id = Ecat7ImageHeader.RECON_TYPE;
	    short reconType =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Recon views.
	    id = Ecat7ImageHeader.RECON_VIEWS;
	    short reconViews =
		Short.parseShort( atts.getNamedItem(id).getNodeValue() );

	    // Fill cti - convert from a hex string to bytes
	    id = Ecat7ImageHeader.FILL_CTI;
	    byte[] fillCti =
		Conversions.toByteArray(atts.getNamedItem(id).getNodeValue());

	    // Fill user - convert from a hex string to bytes
	    id = Ecat7ImageHeader.FILL_USER;
	    byte[] fillUser =
		Conversions.toByteArray(atts.getNamedItem(id).getNodeValue());

	    // Return an ECAT 7 Image Header
	    return new Ecat7ImageHeader(matrixId, dataType, numDimensions,
					xDimension, yDimension, zDimension,
					xOffset, yOffset, zOffset,
					reconZoom, scaleFactor, imageMin,
					imageMax, xPixelSize, yPixelSize,
					zPixelSize, frameDuration,
					frameStartTime, filterCode,
					xResolution, yResolution,
					zResolution, numRElements,
					numAngles, zRotationAngle,
					decayCorrFctr, processingCode,
					gateDuration, rWaveOffset,
					numAcceptedBeats, filterCutoffFrequency,
					filterResolution, filterRampSlope,
					filterOrder, filterScatterFraction,
					filterScatterSlope, annotation,
					mt11, mt12, mt13, mt21,
					mt22, mt23, mt31, mt32,
					mt33, rFilterCutoff,
					rFilterResolution, rFilterCode,
					rFilterOrder, zFilterCutoff,
					zFilterResolution, zFilterCode,
					zFilterOrder, mt14, mt24,
					mt34, scatterType, reconType,
					reconViews, fillCti, fillUser);
	}

	// Unable to construct an ECAT 7 Image Header
	catch (Exception e) {
	    String msg ="Cannot convert a Node into an ECAT 7 Image Header.";
	    throw new IIOInvalidTreeException(msg, e, root);
	}
    }

    /**
     * Converts the ECAT 7 Main Header to a tree.
     *
     * @param mainHeader ECAT 7 Main Header to convert to a tree.
     *
     * @return DOM Node representing the ECAT 7 Main Header.
     */
    public static Node toTree(Ecat7MainHeader mainHeader)
    {
	String rootName = EcatStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	return _toTree(rootName, mainHeader);
    }

    /**
     * Converts the ECAT 7 Image Header to a tree.
     *
     * @param imageHeader ECAT 7 Image Header to convert to a tree.
     *
     * @return DOM Node representing the ECAT 7 Image Header.
     */
    public static Node toTree(Ecat7ImageHeader imageHeader)
    {
	String rootName = EcatImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	return _toTree(rootName, imageHeader);
    }

    /**
     * Converts the ECAT Element Map to a tree.
     *
     * @param treeName Name of the tree.
     * @param map ECAT Element Map to convert to a tree.
     *
     * @return DOM Node representing the ECAT Element Map.
     */
    private static Node _toTree(String treeName, EcatElementMap map)
    {
	// Create the root Node
	IIOMetadataNode rootNode = new IIOMetadataNode(treeName);

	// Add each element as an attribute
	Iterator iter = map.getElementNames();
	while ( iter.hasNext() ) {
	    String elementName = (String)iter.next();
	    Object elementValue = map.getElementValue(elementName);

	    // Convert the bytes of special elements to hex strings
	    if (elementValue instanceof byte[]) {
		elementValue = Conversions.toHexString( (byte[])elementValue );
	    }

	    // Set the attribute
	    rootNode.setAttribute(elementName, elementValue.toString());
	}

	// Return the root Node
	return rootNode;
    }
}
