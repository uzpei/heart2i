/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.ecat.Ecat7ImageHeader;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadataFormat;
import javax.imageio.ImageTypeSpecifier;

/**
 * IIO Metadata Format that describes the tree structure of metadata of an
 * ECAT image.
 *
 * @version 26 July 2006
 */
public class EcatImageMetadataFormat extends AppletFriendlyIIOMetadataFormat
{
    /** Name of the native metadata format. */
    public static final String NATIVE_METADATA_FORMAT_NAME =
	"edu_ucla_loni_ecat7_image_1.0";

    /** Single instance of the ECAT Image Metadata Format. */
    private static EcatImageMetadataFormat _format =
	new EcatImageMetadataFormat();

    /** Constructs an ECAT Image Metadata Format. */
    private EcatImageMetadataFormat()
    {
	// Create the root with no children
	super(NATIVE_METADATA_FORMAT_NAME, CHILD_POLICY_EMPTY);
	String rootName = NATIVE_METADATA_FORMAT_NAME;

	// Add the image header attributes
	addAttribute(rootName, Ecat7ImageHeader.MATRIX_ID,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.DATA_TYPE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.NUM_DIMENSIONS,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.X_DIMENSION,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Y_DIMENSION,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Z_DIMENSION,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.X_OFFSET,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Y_OFFSET,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Z_OFFSET,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RECON_ZOOM,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.SCALE_FACTOR,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.IMAGE_MIN,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.IMAGE_MAX,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.X_PIXEL_SIZE,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Y_PIXEL_SIZE,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Z_PIXEL_SIZE,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FRAME_DURATION,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FRAME_START_TIME,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_CODE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.X_RESOLUTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Y_RESOLUTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Z_RESOLUTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.NUM_R_ELEMENTS,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.NUM_ANGLES,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.Z_ROTATION_ANGLE,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.DECAY_CORR_FCTR,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.PROCESSING_CODE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.GATE_DURATION,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.R_WAVE_OFFSET,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.NUM_ACCEPTED_BEATS,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_CUTOFF_FREQUENCY,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_RESOLUTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_RAMP_SLOPE,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_ORDER,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_SCATTER_FRACTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILTER_SCATTER_SLOPE,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.ANNOTATION,
		     DATATYPE_STRING, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_1_1,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_1_2,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_1_3,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_2_1,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_2_2,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_2_3,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_3_1,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_3_2,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_3_3,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RFILTER_CUTOFF,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RFILTER_RESOLUTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RFILTER_CODE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RFILTER_ORDER,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.ZFILTER_CUTOFF,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.ZFILTER_RESOLUTION,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.ZFILTER_CODE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.ZFILTER_ORDER,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_1_4,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_2_4,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.MT_3_4,
		     DATATYPE_FLOAT, true, null);
	addAttribute(rootName, Ecat7ImageHeader.SCATTER_TYPE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RECON_TYPE,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.RECON_VIEWS,
		     DATATYPE_INTEGER, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILL_CTI,
		     DATATYPE_STRING, true, null);
	addAttribute(rootName, Ecat7ImageHeader.FILL_USER,
		     DATATYPE_STRING, true, null);

	// Set the resource base name for element descriptions
	setResourceBaseName( EcatMetadataFormatResources.class.getName() );
    }

    /**
     * Gets an instance of the ECAT Image Metadata Format.
     *
     * @return The single instance of the ECAT Image Metadata Format.
     */
    public static EcatImageMetadataFormat getInstance()
    {
	return _format;
    }

    /**
     * Returns true if the element (and the subtree below it) is allowed to
     * appear in a metadata document for an image of the given type, defined by
     * an ImageTypeSpecifier.
     *
     * @param elementName The name of the element being queried.
     * @param imageType An ImageTypeSpecifier indicating the type of the image
     *                  that will be associated with the metadata.
     *
     * @return True if the node is meaningful for images of the given type.
     */
    public boolean canNodeAppear(String elementName,
				 ImageTypeSpecifier imageType)
    {
	// Not implemented
	return true;
    }
}
