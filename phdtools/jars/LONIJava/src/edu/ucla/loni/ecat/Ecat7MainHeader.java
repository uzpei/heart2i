/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ecat;

/**
 * ECAT Element Map for the main ECAT 7 section.
 *
 * @version 11 April 2005
 */
public class Ecat7MainHeader extends EcatElementMap
{
  /** Magic number. */
  public static final String MAGIC_NUMBER = "magic_number";

  /** Original file name. */
  public static final String ORIGINAL_FILE_NAME = "original_file_name";

  /** Sw version. */
  public static final String SW_VERSION = "sw_version";

  /** System type. */
  public static final String SYSTEM_TYPE = "system_type";

  /** File type. */
  public static final String FILE_TYPE = "file_type";

  /** Serial number. */
  public static final String SERIAL_NUMBER = "serial_number";

  /** Scan start time. */
  public static final String SCAN_START_TIME = "scan_start_time";

  /** Isotope name. */
  public static final String ISOTOPE_NAME = "isotope_name";

  /** Isotope halflife. */
  public static final String ISOTOPE_HALFLIFE = "isotope_halflife";

  /** Radiopharmaceutical. */
  public static final String RADIOPHARMACEUTICAL = "radiopharmaceutical";

  /** Gantry tilt. */
  public static final String GANTRY_TILT = "gantry_tilt";

  /** Gantry rotation. */
  public static final String GANTRY_ROTATION = "gantry_rotation";

  /** Bed elevation. */
  public static final String BED_ELEVATION = "bed_elevation";

  /** Intrinsic tile. */
  public static final String INTRINSIC_TILT = "intrinsic_tilt";

  /** Wobble speed. */
  public static final String WOBBLE_SPEED = "wobble_speed";

  /** Transm source type. */
  public static final String TRANSM_SOURCE_TYPE = "transm_source_type";

  /** Distance scanned. */
  public static final String DISTANCE_SCANNED = "distance_scanned";

  /** Transaxial fov. */
  public static final String TRANSAXIAL_FOV = "transaxial_fov";

  /** Angular compression. */
  public static final String ANGULAR_COMPRESSION = "angular_compression";

  /** Coin samp mode. */
  public static final String COIN_SAMP_MODE = "coin_samp_mode";

  /** Axial samp mode. */
  public static final String AXIAL_SAMP_MODE = "axial_samp_mode";

  /** Ecat calibration factor. */
  public static final String ECAT_CALIBRATION_FACTOR =
  "ecat_calibration_factor";

  /** Calibration units. */
  public static final String CALIBRATION_UNITS = "calibration_units";

  /** Calibration units label. */
  public static final String CALIBRATION_UNITS_LABEL =
  "calibration_units_label";

  /** Compression code. */
  public static final String COMPRESSION_CODE = "compression_code";

  /** Study type. */
  public static final String STUDY_TYPE = "study_type";

  /** Patient id. */
  public static final String PATIENT_ID = "patient_id";
  
  /** Patient name. */
  public static final String PATIENT_NAME = "patient_name";

  /** Patient sex. */
  public static final String PATIENT_SEX = "patient_sex";

  /** Patient dexterity. */
  public static final String PATIENT_DEXTERITY = "patient_dexterity";

  /** Patient age. */
  public static final String PATIENT_AGE = "patient_age";

  /** Patient height. */
  public static final String PATIENT_HEIGHT = "patient_height";

  /** Patient weight. */
  public static final String PATIENT_WEIGHT = "patient_weight";

  /** Patient birth date. */
  public static final String PATIENT_BIRTH_DATE = "patient_birth_date";

  /** Physician name. */
  public static final String PHYSICIAN_NAME = "physician_name";

  /** Operator name. */
  public static final String OPERATOR_NAME = "operator_name";

  /** Study description */
  public static final String STUDY_DESCRIPTION = "study_description";

  /** Acquisition type. */
  public static final String ACQUISITION_TYPE = "acquisition_type";

  /** Patient orientation. */
  public static final String PATIENT_ORIENTATION = "patient_orientation";

  /** Facility name. */
  public static final String FACILITY_NAME = "facility_name";

  /** Num planes. */
  public static final String NUM_PLANES = "num_planes";

  /** Num frames. */
  public static final String NUM_FRAMES = "num_frames";

  /** Num gates. */
  public static final String NUM_GATES = "num_gates";
  
  /** Num bed pos. */
  public static final String NUM_BED_POS = "num_bed_pos";

  /** Init bed position. */
  public static final String INIT_BED_POSITION = "init_bed_position";

  /** Bed position 0. */
  public static final String BED_POSITION_0 = "bed_position_0";

  /** Bed position 1. */
  public static final String BED_POSITION_1 = "bed_position_1";

  /** Bed position 2. */
  public static final String BED_POSITION_2 = "bed_position_2";

  /** Bed position 3. */
  public static final String BED_POSITION_3 = "bed_position_3";

  /** Bed position 4. */
  public static final String BED_POSITION_4 = "bed_position_4";

  /** Bed position 5. */
  public static final String BED_POSITION_5 = "bed_position_5";

  /** Bed position 6. */
  public static final String BED_POSITION_6 = "bed_position_6";

  /** Bed position 7. */
  public static final String BED_POSITION_7 = "bed_position_7";

  /** Bed position 8. */
  public static final String BED_POSITION_8 = "bed_position_8";

  /** Bed position 9. */
  public static final String BED_POSITION_9 = "bed_position_9";

  /** Bed position 10. */
  public static final String BED_POSITION_10 = "bed_position_10";

  /** Bed position 11. */
  public static final String BED_POSITION_11 = "bed_position_11";

  /** Bed position 12. */
  public static final String BED_POSITION_12 = "bed_position_12";

  /** Bed position 13. */
  public static final String BED_POSITION_13 = "bed_position_13";

  /** Bed position 14. */
  public static final String BED_POSITION_14 = "bed_position_14";

  /** Plane separation. */
  public static final String PLANE_SEPARATION = "plane_separation";

  /** Lwr sctr thres. */
  public static final String LWR_SCTR_THRES = "lwr_sctr_thres";

  /** Lwr true thres. */
  public static final String LWR_TRUE_THRES = "lwr_true_thres";

  /** Upr true thres. */
  public static final String UPR_TRUE_THRES = "upr_true_thres";

  /** User process code. */
  public static final String USER_PROCESS_CODE = "user_process_code";

  /** Acquisition mode. */
  public static final String ACQUISITION_MODE = "acquisition_mode";

  /** Bin size. */
  public static final String BIN_SIZE = "bin_size";

  /** Branching fraction. */
  public static final String BRANCHING_FRACTION = "branching_fraction";

  /** Dose start time. */
  public static final String DOSE_START_TIME = "dose_start_time";

  /** Dosage. */
  public static final String DOSAGE = "dosage";

  /** Well counter corr factor. */
  public static final String WELL_COUNTER_CORR_FACTOR =
  "well_counter_corr_factor";

  /** Data units. */
  public static final String DATA_UNITS = "data_units";

  /** Septa state. */
  public static final String SEPTA_STATE = "septa_state";

  /** Fill cti 0. */
  public static final String FILL_CTI_0 = "fill_cti_0";

  /** Fill cti 1. */
  public static final String FILL_CTI_1 = "fill_cti_1";

  /** Fill cti 2. */
  public static final String FILL_CTI_2 = "fill_cti_2";

  /** Fill cti 3. */
  public static final String FILL_CTI_3 = "fill_cti_3";

  /** Fill cti 4. */
  public static final String FILL_CTI_4 = "fill_cti_4";

  /** Fill cti 5. */
  public static final String FILL_CTI_5 = "fill_cti_5";

  /**
   * Constructs an ECAT 7 Main Header.
   *
   * @param magicNumber Unix file type indentification number.
   * @param originalFileName Scan file's creation number.
   * @param swVersion Software version.
   * @param systemType Scanner model.
   * @param fileType Matrix file type.
   * @param serialNumber Serial number of the gantry.
   * @param scanStartTime Date and time when acquisition was started (sec from
   *                      base time).
   * @param isotopeName String representation of the isotope.
   * @param isotopeHalflife Half-life of isotope (sec).
   * @param radiopharmaceutical String representation of the tracer name.
   * @param gantryTilt Angle (degrees).
   * @param gantryRotation Angle (degrees).
   * @param bedElevation Bed height from lowest point (cm).
   * @param intrinsicTilt Intrinsic tilt.
   * @param wobbleSpeed Wobble speed.
   * @param transmSourceType Transm source type.
   * @param distanceScanned Total distance scanned (cm).
   * @param transaxialFov Diameter of transaxial view (cm).
   * @param angularCompression 0=no mash, 1=mash of 2, 2=mash of 4.
   * @param coinSampMode 0=Net trues, 1=Prompts and Delayed, 3=Prompts,
   *                     Delayed, and Multiples.
   * @param axialSampMode 0=Normal, 1=2X, 2=3X.
   * @param ecatCalibrationFactor Ecat calibration factor.
   * @param calibrationUnits 0=Uncalibrated; 1=Calibrated; 2=Processed.
   * @param calibrationUnitsLabel Whether data_units is filled or not.
   * @param compressionCode Compression code.
   * @param studyType Study type.
   * @param patientId Patient id.
   * @param patientName Patient name.
   * @param patientSex Patient sex.
   * @param patientDexterity Patient dexterity.
   * @param patientAge Patient age (years).
   * @param patientHeight Patient height (cm).
   * @param patientWeight Patient weight (kg).
   * @param patientBirthDate YYYYMMDD (sec from time zero).
   * @param physicianName Physician name.
   * @param operatorName Operator name.
   * @param studyDescription Study description.
   * @param acquisitionType 0=Undefined; 1=Blank; 2=Transmission;
   *                        3=Static emission; 4=Dynamic emission;
   *                        5=Gated emission; 6=Transmission rectilinear;
   *                        7=Emission rectilinear.
   * @param patientOrientation Patient orientation.
   * @param facilityName Facility name.
   * @param numPlanes Physical distance between adjacent planes (cm).
   * @param numFrames Highest frame number in partially reconstruction files.
   * @param numGates Number of gates.
   * @param numBedPos Num of bed positions.
   * @param initBedPosition Initial bed position.
   * @param bedPosition Bed position.  This must have 15 elements.
   * @param planeSeparation Plane separation.
   * @param lwrSctrThres Lower sctr thres.
   * @param lwrTrueThres Lower true thres.
   * @param uprTrueThres Upper true thres.
   * @param userProcessCode User process code.
   * @param acquisitionMode Acquisition mode.
   * @param binSize Width of view sample (cm).
   * @param branchingFraction Fraction of decay by position emmision.
   * @param doseStartTime Time of injection.
   * @param dosage Radiopharmaceutical dosage at time of injection (Bq/cc).
   * @param wellCounterCorrFactor Well counter corr factor.
   * @param dataUnits Free text field; fixed strings: "ECAT counts/sec",
   *                  "Bq/cc".
   * @param septaState Septa state.
   * @param fillCti Fill cti.  This must have 6 elements.
   *
   * @throws IllegalArgumentException If bedPosition does not have 15 elements
   *                                  or if fillCti does not have 6 elements.
   */
  public Ecat7MainHeader(byte[] magicNumber, byte[] originalFileName, 
			 short swVersion, short systemType, short fileType,
			 byte[] serialNumber, int scanStartTime,
			 byte[] isotopeName, float isotopeHalflife,
			 byte[] radiopharmaceutical, float gantryTilt,
			 float gantryRotation, float bedElevation,
			 float intrinsicTilt, short wobbleSpeed,
			 short transmSourceType, float distanceScanned,
			 float transaxialFov, short angularCompression,
			 short coinSampMode, short axialSampMode,
			 float ecatCalibrationFactor, short calibrationUnits,
			 short calibrationUnitsLabel, short compressionCode,
			 byte[] studyType, byte[] patientId,
			 byte[] patientName, byte[] patientSex,
			 byte[] patientDexterity, float patientAge,
			 float patientHeight, float patientWeight,
			 int patientBirthDate, byte[] physicianName,
			 byte[] operatorName, byte[] studyDescription,
			 short acquisitionType, short patientOrientation,
			 byte[] facilityName, short numPlanes, short numFrames,
			 short numGates, short numBedPos,
			 float initBedPosition, float[] bedPosition,
			 float planeSeparation, short lwrSctrThres,
			 short lwrTrueThres, short uprTrueThres,
			 byte[] userProcessCode, short acquisitionMode,
			 float binSize, float branchingFraction,
			 int doseStartTime, float dosage,
			 float wellCounterCorrFactor, byte[] dataUnits,
			 short septaState, short[] fillCti)
    {
      super(60);

      // Check the bed position size
      if (bedPosition.length != 15) {
	throw new IllegalArgumentException("Bed Position must have size 15.");
      }

      // Check the fill CTI size
      if (fillCti.length != 6) {
	throw new IllegalArgumentException("Fill CTI must have size 6.");
      }

      _addElement(MAGIC_NUMBER, _createString(magicNumber));
      _addElement(ORIGINAL_FILE_NAME, _createString(originalFileName));
      _addElement(SW_VERSION, new Short(swVersion));
      _addElement(SYSTEM_TYPE, new Short(systemType));
      _addElement(FILE_TYPE, new Short(fileType));
      _addElement(SERIAL_NUMBER, _createString(serialNumber));
      _addElement(SCAN_START_TIME, new Integer(scanStartTime));
      _addElement(ISOTOPE_NAME, _createString(isotopeName));
      _addElement(ISOTOPE_HALFLIFE, new Float(isotopeHalflife));
      _addElement(RADIOPHARMACEUTICAL, _createString(radiopharmaceutical));
      _addElement(GANTRY_TILT, new Float(gantryTilt));
      _addElement(GANTRY_ROTATION, new Float(gantryRotation));
      _addElement(BED_ELEVATION, new Float(bedElevation));
      _addElement(INTRINSIC_TILT, new Float(intrinsicTilt));
      _addElement(WOBBLE_SPEED, new Short(wobbleSpeed));
      _addElement(TRANSM_SOURCE_TYPE, new Short(transmSourceType));
      _addElement(DISTANCE_SCANNED, new Float(distanceScanned));
      _addElement(TRANSAXIAL_FOV, new Float(transaxialFov));
      _addElement(ANGULAR_COMPRESSION, new Short(angularCompression));
      _addElement(COIN_SAMP_MODE, new Short(coinSampMode));
      _addElement(AXIAL_SAMP_MODE, new Short(axialSampMode));
      _addElement(ECAT_CALIBRATION_FACTOR, new Float(calibrationUnitsLabel));
      _addElement(CALIBRATION_UNITS, new Short(calibrationUnits));
      _addElement(CALIBRATION_UNITS_LABEL, new Short(calibrationUnitsLabel));
      _addElement(COMPRESSION_CODE, new Short(compressionCode));
      _addElement(STUDY_TYPE, _createString(studyType));
      _addElement(PATIENT_ID, _createString(patientId));
      _addElement(PATIENT_NAME, _createString(patientName));
      _addElement(PATIENT_SEX, _createString(patientSex));
      _addElement(PATIENT_DEXTERITY, _createString(patientDexterity));
      _addElement(PATIENT_AGE, new Float(patientAge));
      _addElement(PATIENT_HEIGHT, new Float(patientHeight));
      _addElement(PATIENT_WEIGHT, new Float(patientWeight));
      _addElement(PATIENT_BIRTH_DATE, new Integer(patientBirthDate));
      _addElement(PHYSICIAN_NAME, _createString(physicianName));
      _addElement(OPERATOR_NAME, _createString(operatorName));
      _addElement(STUDY_DESCRIPTION, _createString(studyDescription));
      _addElement(ACQUISITION_TYPE, new Short(acquisitionType));
      _addElement(PATIENT_ORIENTATION, new Short(patientOrientation));
      _addElement(FACILITY_NAME, _createString(facilityName));
      _addElement(NUM_PLANES, new Short(numPlanes));
      _addElement(NUM_FRAMES, new Short(numFrames));
      _addElement(NUM_GATES, new Short(numGates));
      _addElement(NUM_BED_POS, new Short(numBedPos));
      _addElement(INIT_BED_POSITION, new Float(initBedPosition));
      _addElement(BED_POSITION_0, new Float(bedPosition[0]));
      _addElement(BED_POSITION_1, new Float(bedPosition[1]));
      _addElement(BED_POSITION_2, new Float(bedPosition[2]));
      _addElement(BED_POSITION_3, new Float(bedPosition[3]));
      _addElement(BED_POSITION_4, new Float(bedPosition[4]));
      _addElement(BED_POSITION_5, new Float(bedPosition[5]));
      _addElement(BED_POSITION_6, new Float(bedPosition[6]));
      _addElement(BED_POSITION_7, new Float(bedPosition[7]));
      _addElement(BED_POSITION_8, new Float(bedPosition[8]));
      _addElement(BED_POSITION_9, new Float(bedPosition[9]));
      _addElement(BED_POSITION_10, new Float(bedPosition[10]));
      _addElement(BED_POSITION_11, new Float(bedPosition[11]));
      _addElement(BED_POSITION_12, new Float(bedPosition[12]));
      _addElement(BED_POSITION_13, new Float(bedPosition[13]));
      _addElement(BED_POSITION_14, new Float(bedPosition[14]));
      _addElement(PLANE_SEPARATION, new Float(planeSeparation));
      _addElement(LWR_SCTR_THRES, new Short(lwrSctrThres));
      _addElement(LWR_TRUE_THRES, new Short(lwrTrueThres));
      _addElement(UPR_TRUE_THRES, new Short(uprTrueThres));
      _addElement(USER_PROCESS_CODE, _createString(userProcessCode));
      _addElement(ACQUISITION_MODE, new Short(acquisitionMode));
      _addElement(BIN_SIZE, new Float(binSize));
      _addElement(BRANCHING_FRACTION, new Float(branchingFraction));
      _addElement(DOSE_START_TIME, new Integer(doseStartTime));
      _addElement(DOSAGE, new Float(dosage));
      _addElement(WELL_COUNTER_CORR_FACTOR, new Float(wellCounterCorrFactor));
      _addElement(DATA_UNITS, _createString(dataUnits));
      _addElement(SEPTA_STATE, new Short(septaState));
      _addElement(FILL_CTI_0, new Short(fillCti[0]));
      _addElement(FILL_CTI_1, new Short(fillCti[1]));
      _addElement(FILL_CTI_2, new Short(fillCti[2]));
      _addElement(FILL_CTI_3, new Short(fillCti[3]));
      _addElement(FILL_CTI_4, new Short(fillCti[4]));
      _addElement(FILL_CTI_5, new Short(fillCti[5]));
    }
}
