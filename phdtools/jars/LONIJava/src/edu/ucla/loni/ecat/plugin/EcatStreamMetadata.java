/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.ecat.Ecat7MainHeader;
import edu.ucla.loni.imageio.AppletFriendlyIIOMetadata;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * IIO Metadata for stream metadata of the ECAT image format.
 *
 * @version 7 April 2005
 */
public class EcatStreamMetadata extends AppletFriendlyIIOMetadata
{
  /** ECAT 7 Main Header containing original metadata. */
  private Ecat7MainHeader _originalMainHeader;

  /** ECAT 7 Main Header containing current metadata. */
  private Ecat7MainHeader _currentMainHeader;

  /**
   * Constructs an ECAT Stream Metadata.
   *
   * @param mainHeader ECAT 7 Main Header.
   */
  public EcatStreamMetadata(Ecat7MainHeader mainHeader)
    {
      super(false, EcatStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    EcatStreamMetadataFormat.class.getName(), null, null);

      _originalMainHeader = mainHeader;
      _currentMainHeader = mainHeader;
    }

  /**
   * Returns true if this object does not support the mergeTree, setFromTree,
   * and reset methods.  
   *
   * @return True if this IIOMetadata object cannot be modified; false o/w.
   */
  public boolean isReadOnly()
    {
      return false;
    }

  /**
   * Returns an XML DOM Node object that represents the root of a tree of
   * metadata contained within this object according to the conventions
   * defined by a given metadata format.
   *
   * @param formatName Name of the desired metadata format.
   *
   * @return An XML DOM Node object forming the root of a tree.
   *
   * @throws IllegalArgumentException If formatName is not one of the allowed
   *                                  metadata format names.
   */
  public Node getAsTree(String formatName)
    {
      // Native metadata format name
      if ( nativeMetadataFormatName.equals(formatName) ) {
	return EcatMetadataConversions.toTree(_currentMainHeader);
      }

      // Unknown format name
      String msg = "The format name \"" + formatName + "\" is not a " +
	           "supported metadata format name.";
      throw new IllegalArgumentException(msg);
    }

  /**
   * Alters the internal state of this IIOMetadata object from a tree of XML
   * DOM Nodes whose syntax is defined by the given metadata format.  The
   * previous state is altered only as necessary to accomodate the nodes that
   * are present in the given tree.
   *
   * @param formatName Name of the desired metadata format.
   * @param root An XML DOM Node object forming the root of a tree.
   *
   * @throws IllegalArgumentException If formatName is not one of the allowed
   *                                  metadata format names, or if the root is
   *                                  null.
   * @throws IIOInvalidTreeException If the tree cannot be parsed successfully
   *                                 using the rules of the given format.
   */
  public void mergeTree(String formatName, Node root)
    throws IIOInvalidTreeException
    {
      // Check for a null root Node
      if (root == null) {
	String msg = "A null root Node is not allowed.";
	throw new IllegalArgumentException(msg);
      }

      // Check for a matching root Node
      if ( !root.getNodeName().equals(nativeMetadataFormatName) ) {
	String msg = "Root node must be named \"" + nativeMetadataFormatName +
	             "\".";
	throw new IIOInvalidTreeException(msg, root);
      }

      // If there is no current metadata, convert the root
      Node r;
      if (_currentMainHeader == null) { r = root; }

      // Otherwise merge the root with the current metadata tree Node
      else {
	Node streamNode = getAsTree(formatName);
	r = _getMergedNode(streamNode, root.getAttributes());
      }

      // Set the new current ECAT Main Header
      _currentMainHeader = EcatMetadataConversions.toEcat7MainHeader(r);
    }

  /**
   * Resets all the data stored in this object to default values, usually to
   * the state this object was in immediately after construction, though the
   * precise semantics are plug-in specific.  Note that there are many possible
   * default values, depending on how the object was created.
   */
  public void reset()
    {
      // Reset the current Main Header
      _currentMainHeader = _originalMainHeader;
    }

  /**
   * Merges the attributes with the specified Node.  If there are attribute
   * names not present in the attributes of the node, the attributes are
   * ignored.
   *
   * @param node Node to merge the attributes with.
   * @param attributes Attributes to merge.
   *
   * @return New Node containing the merged attributes.
   */
  private Node _getMergedNode(Node node, NamedNodeMap attributes)
    {
      // Create a new Node with the same name
      IIOMetadataNode mergedNode = new IIOMetadataNode( node.getNodeName() );

      // Copy the Node attributes to the new Node
      NamedNodeMap map = node.getAttributes();
      if (map != null) {
	for (int i = 0; i < map.getLength(); i++) {
	  Node attr = map.item(i);
	  mergedNode.setAttribute( attr.getNodeName(), attr.getNodeValue() );
	}
      }

      // Merge the new attributes with the old
      if (attributes != null) {
	for (int i = 0; i < attributes.getLength(); i++) {
	  Node attribute = attributes.item(i);
	  String attributeName = attribute.getNodeName();

	  // Attribute name must be present in the new Node
	  if ( mergedNode.hasAttribute(attributeName) ) {
	    mergedNode.setAttribute(attributeName, attribute.getNodeValue());
	  }
	}
      }

      // Return the new merged Node
      return mergedNode;
    }
}
