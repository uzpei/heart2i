/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ecat;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * Structure that maps ECAT elements to their values.
 *
 * @version 7 April 2005
 */
public abstract class EcatElementMap
{
  /** ECAT element names mapped to their values. */
  private LinkedHashMap _elements;

  /**
   * Creates an ECAT Element Map that holds the specified number of elements.
   *
   * @param numberOfElements Number of elements in the ECAT Subheader.
   *
   * @throws IllegalArgumentException If the number of elements is negative.
   */
  protected EcatElementMap(int numberOfElements)
    {
      _elements = new LinkedHashMap(numberOfElements);
    }

  /**
   * Gets the names of all the elements in the ECAT Subheader.
   *
   * @return Names of all the elements in the ECAT Subheader.
   */
  public Iterator getElementNames()
    {
      return _elements.keySet().iterator();
    }

  /**
   * Gets the value of the specified element.
   *
   * @param elementName Name of the element.
   *
   * @return Object that represents the value of the named element.
   *
   * @throws IllegalArgumentException If the named element does not exist.
   */
  public Object getElementValue(String elementName)
    {
      Object value = _elements.get(elementName);

      // Check that the element exists
      if (value == null) {
	String msg = "Unable to find an element named \"" + elementName +
	             "\".";
	throw new IllegalArgumentException(msg);
      }

      // Return the value
      return value;
    }

  /**
   * Gets the contents of the ECAT Subheader.
   *
   * @return String representation of the contents of the ECAT Subheader.
   */
  public String toString()
    {
      StringBuffer buffer = new StringBuffer();

      // Append each element name and value to the buffer
      Iterator iter = getElementNames();
      while ( iter.hasNext() ) {
	String name = (String)iter.next();
	Object value = getElementValue(name);

	// Create a string representation
	buffer.append(name + " = \"" + value + "\" \n");
      }

      return buffer.toString();
    }

  /**
   * Adds an element to the ECAT Subheader.  If the element already exists,
   * it is replaced using the new element value.
   *
   * @param name Name of the element.
   * @param value Value of the element.
   */
  protected void _addElement(String name, Object value)
    {
      _elements.put(name, value);
    }

  /**
   * Creates a String from the byte array.
   *
   * @param byteArray Array of bytes to construct a String from.
   *
   * @return String created from the byte array.
   */
  protected String _createString(byte[] byteArray)
    {
      // Determine the location of the first null character
      int loc;
      for (loc = 0; loc < byteArray.length && byteArray[loc] != 0; loc++) {}

      // Return a String
      return new String(byteArray, 0, loc);
    }
}
