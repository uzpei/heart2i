/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.ecat.Ecat7MainHeader;
import edu.ucla.loni.ecat.Ecat7ImageHeader;
import edu.ucla.loni.ecat.EcatElementMap;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Node;

/**
 * Image Writer for encoding ECAT images.
 *
 * @version 27 July 2006
 */
public class EcatImageWriter extends ImageWriter
{
    /** Number of images in the sequence currently being written. */
    private int _numberOfImagesWritten;

    /** Number of planes in a frame. */
    private int _numberOfPlanesPerFrame;

    /** Number of 512k blocks to the start of the indexed frame. */
    private int[] _blockOffsets;

    /** Matrix ids for each frame. */
    private int[] _matrixIds;

    /**
     * Constructs an ECAT Image Writer.
     *
     * @param originatingProvider The Image Writer Spi that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not an
     *                                  ECAT Image Writer Spi.
     */
    public EcatImageWriter(ImageWriterSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an ECAT Image Writer Spi
	if ( !(originatingProvider instanceof EcatImageWriterSpi) ) {
	    String msg = "Originating provider must an ECAT Image Writer SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding a
     * stream of images.
     *
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
    {
	// Return a blank metadata object
	return new EcatStreamMetadata(null);
    }

    /**
     * Returns an IIOMetadata object containing default values for encoding an
     * image of the given type.
     *
     * @param imageType An ImageTypeSpecifier indicating the format of the image
     *                  to be written later.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object.
     */
    public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
					       ImageWriteParam param)
    {
	// Return a blank metadata object
	return new EcatImageMetadata(null);
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing stream metadata, used to
     *               initialize the state of the returned object.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If inData is null.
     */
    public IIOMetadata convertStreamMetadata(IIOMetadata inData,
					     ImageWriteParam param)
    {
	// Only recognize ECAT Stream Metadata
	if (inData instanceof EcatStreamMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns an IIOMetadata object that may be used for encoding and
     * optionally modified using its document interfaces or other interfaces
     * specific to the writer plug-in that will be used for encoding.
     *
     * @param inData An IIOMetadata object representing image metadata, used to
     *               initialize the state of the returned object.
     * @param imageType An ImageTypeSpecifier indicating the layout and color
     *                  information of the image with which the metadata will be
     *                  associated.
     * @param param An ImageWriteParam that will be used to encode the image, or
     *              null.
     *
     * @return An IIOMetadata object, or null if the plug-in does not provide
     *         metadata encoding capabilities.
     *
     * @throws IllegalArgumentException If either of inData or imageType is
     *                                  null.
     */
    public IIOMetadata convertImageMetadata(IIOMetadata inData,
					    ImageTypeSpecifier imageType,
					    ImageWriteParam param)
    {
	// Only recognize ECAT Image Metadata
	if (inData instanceof EcatImageMetadata) { return inData; }

	// Otherwise perform no conversion
	return null;
    }

    /**
     * Returns true if the methods that take an IIOImage parameter are capable
     * of dealing with a Raster (as opposed to RenderedImage) source image.
     *
     * @return True if Raster sources are supported.
     */
    public boolean canWriteRasters()
    {
	return true;
    }

    /**
     * Appends a complete image stream containing a single image and associated
     * stream and image metadata and thumbnails to the output.
     *
     * @param streamMetadata An IIOMetadata object representing stream metadata,
     *                       or null to use default values.
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam, or null to use a default
     *              ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, if the stream
     *                                  metadata cannot be converted, or if the
     *                                  image type is unrecognized.
     * @throws IllegalStateException If the output has not been set.
     */
    public void write(IIOMetadata streamMetadata, IIOImage image,
		      ImageWriteParam param) throws IOException
    {
	// Write a sequence with 1 image
	prepareWriteSequence(streamMetadata);
	writeToSequence(image, param);
	endWriteSequence();
    }

    /**
     * Returns true if the writer is able to append an image to an image
     * stream that already contains header information and possibly prior
     * images.
     *
     * @return True If images may be appended sequentially.
     */
    public boolean canWriteSequence()
    {
	return true;
    }

    /**
     * Prepares a stream to accept a series of subsequent writeToSequence calls,
     * using the provided stream metadata object.  The metadata will be written
     * to the stream if it should precede the image data.
     *
     * @param streamMetadata A stream metadata object.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the stream metadata is null or cannot
     *                                  be converted.
     * @throws IllegalStateException If the output has not been set, or if the
     *                               number of frames is not positive.
     */
    public void prepareWriteSequence(IIOMetadata streamMetadata)
	throws IOException
    {
	// Convert the metadata to ECAT Stream Metadata
	IIOMetadata metadata = convertStreamMetadata(streamMetadata, null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the stream metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Construct an ECAT Header from the metadata
	Ecat7MainHeader mainHeader;
	try {
	    String rootName =
		EcatStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	    Node root = metadata.getAsTree(rootName);
	    mainHeader = EcatMetadataConversions.toEcat7MainHeader(root);
	}
      
	catch (Exception e) {
	    String msg="Unable to make an ECAT Header from the metadata tree.";
	    IllegalArgumentException ex = new IllegalArgumentException(msg);
	    ex.initCause(e);
	    throw ex;
	}

	// Determine the number of planes per frame
	try {
	    Object value=mainHeader.getElementValue(Ecat7MainHeader.NUM_PLANES);
	    _numberOfPlanesPerFrame = ((Number)value).intValue();
	}

	catch (Exception e) {
	    String msg = "Unable to determine the number of frames.";
	    IllegalArgumentException ex = new IllegalArgumentException(msg);
	    ex.initCause(e);
	    throw ex;
	}

	// Make sure the number of planes per frame is valid
	if (_numberOfPlanesPerFrame < 1) {
	    String msg = "Illegal number of planes:  "+_numberOfPlanesPerFrame;
	    throw new IllegalStateException(msg);
	}

	// Encode the main header and write to the output stream
	ImageOutputStream outputStream = _getOutputStream();
	outputStream.write( _encode(mainHeader) );

	// Initialize
	_numberOfImagesWritten = 0;
	_blockOffsets = new int[0];
	_matrixIds = new int[0];
    }

    /**
     * Appends a single image and possibly associated metadata and thumbnails,
     * to the output.  The supplied ImageReadParam is ignored.
     *
     * @param image An IIOImage object containing an image, thumbnails, and
     *              metadata to be written.
     * @param param An ImageWriteParam or null to use a default ImageWriteParam.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalArgumentException If the image is null, the image type is
     *                                  not recognized, or the image metadata
     *                                  cannot be converted.
     * @throws IllegalStateException If the output has not been set,
     *                               prepareWriteSequence has not been called,
     *                               or the maximum number of images has been
     *                               written.
     */
    public void writeToSequence(IIOImage image, ImageWriteParam param)
	throws IOException
    {
	// Check to see if the sequence has been prepared
	if (_blockOffsets == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Check for a null image
	if (image == null) {
	    String msg = "Cannot write a null image.";
	    throw new IllegalArgumentException(msg);
	}

	// Convert the metadata to ECAT Image Metadata
	IIOMetadata metadata = convertImageMetadata(image.getMetadata(), null,
						    null);

	// Unable to convert the metadata
	if (metadata == null) {
	    String msg = "Unable to convert the image metadata for encoding.";
	    throw new IllegalArgumentException(msg);
	}

	// Frame metadata needs to be written at the start of each frame
	ImageOutputStream outputStream = _getOutputStream();
	if (_numberOfImagesWritten % _numberOfPlanesPerFrame == 0) {

	    // Determine the next 512k block
	    long currentPos = outputStream.getStreamPosition();
	    int nextBlock = (int)currentPos/512;
	    if (currentPos > 512*nextBlock) { nextBlock++; }

	    // Advance to the start of the next 512k block
	    outputStream.seek(512*nextBlock);

	    // Write an empty directory list block if needed
	    if (_numberOfImagesWritten/_numberOfPlanesPerFrame % 31 == 0) {
		outputStream.write( new byte[512] );
		nextBlock++;
	    }

	    // Update the frame blocks
	    int[] newOffsets = new int[_blockOffsets.length + 1];
	    System.arraycopy(_blockOffsets, 0, newOffsets, 0,
			     _blockOffsets.length);
	    newOffsets[_blockOffsets.length] = nextBlock+1;
	    _blockOffsets = newOffsets;

	    // Convert the frame metadata
	    Ecat7ImageHeader imageHeader;
	    try {
		String rt = EcatImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
		Node root = metadata.getAsTree(rt);
		imageHeader = EcatMetadataConversions.toEcat7ImageHeader(root);
	    }

	    catch (Exception e) {
		String msg = "Unable to convert frame metadata.";
		IllegalArgumentException ex = new IllegalArgumentException(msg);
		ex.initCause(e);
		throw ex;
	    }

	    // Update the matrix ids
	    try {
		int[] newMatrixIds = new int[_matrixIds.length + 1];
		System.arraycopy(_matrixIds, 0, newMatrixIds, 0,
				 _matrixIds.length);

		// Add the matrix id
		String name = Ecat7ImageHeader.MATRIX_ID;
		Object value = imageHeader.getElementValue(name);
		newMatrixIds[_matrixIds.length] = ((Integer)value).intValue();

		_matrixIds = newMatrixIds;
	    }

	    catch (Exception e) {
		String msg = "Unable to get a matrix id.";
		IllegalArgumentException ex = new IllegalArgumentException(msg);
		ex.initCause(e);
		throw ex;
	    }

	    // Write the frame metadata
	    outputStream.write( _encode(imageHeader) );
	}

	// Get a Raster from the image
	Raster raster = image.getRaster();
	if (raster == null) {
	    RenderedImage renderedImage = image.getRenderedImage();

	    // If the Rendered Image is a Buffered Image, get Raster directly
	    if (renderedImage instanceof BufferedImage) {
		raster = ((BufferedImage)renderedImage).getRaster();
	    }

	    // Otherwise get a copy of the Raster from the Rendered Image
	    raster = renderedImage.getData();
	}

	// Update the Listeners
	processImageStarted(_numberOfImagesWritten);

	// Write byte data
	DataBuffer dataBuffer = raster.getDataBuffer();
	if (dataBuffer instanceof DataBufferByte) {
	    outputStream.write( ((DataBufferByte)dataBuffer).getData() );
	}

	// Write short data
	else if (dataBuffer instanceof DataBufferShort) {
	    short[] data = ((DataBufferShort)dataBuffer).getData();
	    outputStream.writeShorts(data, 0, data.length);
	}

	// Write unsigned short data
	else if (dataBuffer instanceof DataBufferUShort) {
	    short[] data = ((DataBufferUShort)dataBuffer).getData();
	    outputStream.writeShorts(data, 0, data.length);
	}

	// Unrecognized type
	else {
	    String msg = "Unable to write the IIOImage (unknown data type).";
	    throw new IllegalArgumentException(msg);
	}

	// Update the Listeners
	_numberOfImagesWritten++;
	if ( abortRequested() ) { processWriteAborted(); }
	else { processImageComplete(); }
    }

    /**
     * Completes the writing of a sequence of images begun with
     * prepareWriteSequence.  Any stream metadata that should come at the end of
     * the sequence of images is written out, and any header information at the
     * beginning of the sequence is patched up if necessary.
     *
     * @throws IOException If an error occurs during writing.
     * @throws IllegalStateException If the output has not been set, or
     *                               prepareWriteSequence has not been called.
     */
    public void endWriteSequence() throws IOException
    {
	// Check to see if the sequence has been prepared
	if (_blockOffsets == null) {
	    String msg = "The image sequence has not been prepared.";
	    throw new IllegalStateException(msg);
	}

	// Compute the block offsets to the directory list blocks
	int numberOfDirLists = (_blockOffsets.length-1)/31 + 1;
	int[] thsblk = new int[numberOfDirLists];
	for (int i = 0; i < numberOfDirLists; i++) {
	    thsblk[i] = _blockOffsets[31*i] - 1;
	}

	// Add information about the directory list blocks
	int[] nfree = new int[numberOfDirLists];
	int[] nxtblk = new int[numberOfDirLists];
	int[] prvblk = new int[numberOfDirLists];
	int[] nDims = new int[numberOfDirLists];
	for (int i = 0; i < numberOfDirLists; i++) {

	    // Number of used entries in this directory list block
	    if (i < numberOfDirLists-1) { nDims[i] = 31; }
	    else { nDims[i] = _blockOffsets.length % 31; }

	    // Number of available entries in this directory list block
	    nfree[i] = 31 - nDims[i];

	    // Offset to previous directory list block (zero if 1st block)
	    if (i > 0) { prvblk[i] = thsblk[i-1]; }

	    // Offset to next directory list block (offset to 1st if last block)
	    if (i == numberOfDirLists-1) { nxtblk[i] = thsblk[0]; }
	    else { nxtblk[i] = thsblk[i+1]; }
	}

	// Record the end of the stream
	ImageOutputStream outputStream = _getOutputStream();
	long endPos = outputStream.getStreamPosition();

	// Write the directory list blocks
	int blockIndex = 0;
	for (int i = 0; i < numberOfDirLists; i++) {
	    ByteArrayOutputStream bStream = new ByteArrayOutputStream(512);
	    DataOutputStream listStream = new DataOutputStream(bStream);

	    // Write the first entry
	    listStream.writeInt(nfree[i]);
	    listStream.writeInt(nxtblk[i]);
	    listStream.writeInt(prvblk[i]);
	    listStream.writeInt(nDims[i]);

	    // Write the entries for the block offsets
	    for (int j = 0; j < nDims[i]; j++) {

		// Matrix ID
		listStream.writeInt( _matrixIds[blockIndex] );

		// Matrix Subheader block number of frame
		listStream.writeInt( _blockOffsets[blockIndex] );

		// Block number of last image data block of frame
		if (blockIndex == _blockOffsets.length-1) {
		    listStream.writeInt((int)endPos/512);
		}
		else { listStream.writeInt(_blockOffsets[blockIndex+1]-1); }

		// Status: 1=exists, 0=doesn't exist, -1=deleted
		listStream.writeInt(1);

		// Increase the index
		blockIndex++;
	    }

	    // Reset the output stream to the start of the directory list block
	    outputStream.seek(512*(thsblk[i]-1));

	    // Write the directory list block data
	    outputStream.write( bStream.toByteArray() );
	}

	// Signify an unprepared image sequence
	_blockOffsets = null;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageWriter.  Otherwise, the writer may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the output stream
	try { if (output != null) { ((ImageOutputStream)output).close(); } }
	catch (Exception e) {}

	output = null;
    }

    /**
     * Gets the output stream.
     *
     * @return Image Output Stream to write to.
     *
     * @throws IllegalStateException If the output has not been set.
     */
    private ImageOutputStream _getOutputStream()
    {
	// No output has been set
	if (output == null) {
	    String msg = "No output has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Output Stream
	return (ImageOutputStream)output;
    }

    /**
     * Writes the specified ECAT Element Map to a byte array.
     *
     * @param map ECAT Element Map to encode.
     *
     * @return Byte array of size 512 containing the encoded Element Map.
     * 
     * @throws IOException If an error occurs during reading.
     */
    private byte[] _encode(EcatElementMap map) throws IOException
    {
	ByteArrayOutputStream bStream = new ByteArrayOutputStream(512);
	DataOutputStream outputStream = new DataOutputStream(bStream);

	// Encode each element
	Iterator iter = map.getElementNames();
	while ( iter.hasNext() ) {
	    String name = (String)iter.next();
	    Object value = map.getElementValue(name);

	    // String value
	    if (value instanceof String) {
		int size = 0;

		// ECAT 7 Main Header string elements
		if ( Ecat7MainHeader.MAGIC_NUMBER.equals(name) ) {
		    size = 14;
		}
		else if ( Ecat7MainHeader.ORIGINAL_FILE_NAME.equals(name) ) {
		    size = 32;
		}
		else if ( Ecat7MainHeader.SERIAL_NUMBER.equals(name) ) {
		    size = 10;
		}
		else if ( Ecat7MainHeader.ISOTOPE_NAME.equals(name) ) {
		    size = 8;
		}
		else if ( Ecat7MainHeader.RADIOPHARMACEUTICAL.equals(name) ) {
		    size = 32;
		}
		else if ( Ecat7MainHeader.STUDY_TYPE.equals(name) ) {
		    size = 12;
		}
		else if ( Ecat7MainHeader.PATIENT_ID.equals(name) ) {
		    size = 16;
		}
		else if ( Ecat7MainHeader.PATIENT_NAME.equals(name) ) {
		    size = 32;
		}
		else if ( Ecat7MainHeader.PATIENT_SEX.equals(name) ) {
		    size = 1;
		}
		else if ( Ecat7MainHeader.PATIENT_DEXTERITY.equals(name) ) {
		    size = 1;
		}
		else if ( Ecat7MainHeader.PHYSICIAN_NAME.equals(name) ) {
		    size = 32;
		}
		else if ( Ecat7MainHeader.OPERATOR_NAME.equals(name) ) {
		    size = 32;
		}
		else if ( Ecat7MainHeader.STUDY_DESCRIPTION.equals(name) ) {
		    size = 32;
		}
		else if ( Ecat7MainHeader.FACILITY_NAME.equals(name) ) {
		    size = 20;
		}
		else if ( Ecat7MainHeader.USER_PROCESS_CODE.equals(name) ) {
		    size = 10;
		}
		else if ( Ecat7MainHeader.DATA_UNITS.equals(name) ) {
		    size = 32;
		}

		// ECAT 7 Image Header string elements
		else if ( Ecat7ImageHeader.ANNOTATION.equals(name) ) {
		    size = 40;
		}

		// Write the string value
		_writeString( (String)value, size, outputStream );
	    }

	    // Value is a byte array
	    else if (value instanceof byte[]) {
		int size = 0;

		// ECAT 7 Image Header string elements
		if ( Ecat7ImageHeader.FILL_CTI.equals(name) ) {
		    size = 2*87;
		}
		else if ( Ecat7ImageHeader.FILL_USER.equals(name) ) {
		    size = 2*49;
		}

		// Write the byte array
		_writeByteArray( (byte[])value, size, outputStream );
	    }

	    // Element value is a Number
	    else {

		// Don't write the matrix id (it is written in a separate blk)
		if ( !Ecat7ImageHeader.MATRIX_ID.equals(name) ) {
		    _writeNumber( (Number)value, outputStream );
		}
	    }
	}

	// Return a byte array that contains the encoded Element Map
	while (bStream.size() < 512) { bStream.write(0); }
	return bStream.toByteArray();
    }

    /**
     * Writes the byte array to the output stream.
     *
     * @param array Byte array to write to the output stream.
     * @param length Length of the array to actually write.
     * @param outputStream Data Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeByteArray(byte[] array, int length,
				 DataOutputStream outputStream)
	throws IOException
    {
	// Create a byte buffer of the specified length
	byte[] buffer = new byte[length];

	// Copy the specified bytes to the byte buffer
	int min = Math.min( buffer.length, array.length );
	System.arraycopy(array, 0, buffer, 0, min);

	// Write the bytes
	outputStream.write(buffer);
    }

    /**
     * Writes the Number to the output stream.
     *
     * @param number Number to write to the output stream.
     * @param outputStream Data Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeNumber(Number number, DataOutputStream outputStream)
	throws IOException
    {
	// Integer
	if (number instanceof Integer) {
	    outputStream.writeInt( ((Integer)number).intValue() );
	}

	// Short
	else if (number instanceof Short) {
	    outputStream.writeShort( ((Short)number).shortValue() );
	}

	// Float
	else if (number instanceof Float) {
	    outputStream.writeFloat( ((Float)number).floatValue() );
	}

	// Unknown Number
	else {
	    String msg = "Unable to write an Number of type \"" +
		number.getClass().getName() + "\".";
	    throw new IOException(msg);
	}
    }

    /**
     * Writes the String to the output stream.
     *
     * @param string String to write to the output stream.
     * @param length Length of the string to actually write.
     * @param outputStream Data Output Stream to write to.
     *
     * @throws IOException If an error occurs during the writing.
     */
    private void _writeString(String string, int length,
			      DataOutputStream outputStream)
	throws IOException
    {
	// Create a byte buffer of the specified length
	byte[] buffer = new byte[length];

	// Copy the bytes of the String to the byte buffer
	byte[] stringBytes = string.getBytes();
	int min = Math.min( buffer.length, stringBytes.length );
	System.arraycopy(stringBytes, 0, buffer, 0, min);

	// Write the bytes
	outputStream.write(buffer);
    }
}
