/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.imageio.Utilities;
import edu.ucla.loni.ecat.Ecat7Header;
import edu.ucla.loni.ecat.Ecat7ImageHeader;
import edu.ucla.loni.ecat.Ecat7MainHeader;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageReader;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Image Reader for parsing and decoding ECAT images.
 *
 * @version 24 April 2008
 */
public class EcatImageReader extends ImageReader
{
    /** ECAT header decoded from the input source. */
    private Ecat7Header _ecatHeader;

    /** Offsets (in bytes) to the start of each frame. */
    private long[] _frameOffsets;

    /**
     * Constructs an ECAT Image Reader.
     *
     * @param originatingProvider The Image Reader SPI that instantiated this
     *                            Object.
     *
     * @throws IllegalArgumentException If the originating provider is not a
     *                                  ECAT Image Reader Spi.
     */
    public EcatImageReader(ImageReaderSpi originatingProvider)
    {
	super(originatingProvider);

	// Originating provider must be an ECAT Image Reader Spi
	if ( !(originatingProvider instanceof EcatImageReaderSpi) ) {
	    String msg="Originating provider must be an ECAT Image Reader SPI.";
	    throw new IllegalArgumentException(msg);
	}
    }

    /**
     * Sets the input source to use to the given ImageInputStream or other
     * Object.  The input source must be set before any of the query or read
     * methods are used.  If the input is null, any currently set input source
     * will be removed.
     *
     * @param input The ImageInputStream or other Object to use for future
     *              decoding.
     * @param seekForwardOnly If true, images and metadata may only be read in
     *                        ascending order from this input source.
     * @param ignoreMetadata If true, metadata may be ignored during reads.
     *
     * @throws IllegalArgumentException If the input is not an instance of one
     *                                  of the classes returned by the
     *                                  originating service provider's
     *                                  getInputTypes method, or is not an
     *                                  ImageInputStream.
     * @throws IllegalStateException If the input source is an Input Stream that
     *                               doesn't support marking and seekForwardOnly
     *                               is false.
     */
    public void setInput(Object input, boolean seekForwardOnly,
			 boolean ignoreMetadata)
    {
	super.setInput(input, seekForwardOnly, ignoreMetadata);

	// Remove any decoded ECAT Header
	_ecatHeader = null;
    }

    /**
     * Returns the number of images, not including thumbnails, available from
     * the current input source.
     * 
     * @param allowSearch If true, the true number of images will be returned
     *                    even if a search is required.  If false, the reader
     *                    may return -1 without performing the search.
     *
     * @return The number of images or -1 if allowSearch is false and a search
     *         would be required.
     *
     * @throws IllegalStateException If the input source has not been set.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    public int getNumImages(boolean allowSearch) throws IOException
    {
	// Get the number of frames
	int numberOfFrames = _getEcatHeader().getNumberOfFrames();

	// Return the total number of images
	return numberOfFrames*_getNumberOfPlanes();
    }

    /**
     * Returns the width in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The width of the image in pixels.
     *
     * @throws IOException If an error occurs reading the width information from
     *                     the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getWidth(int imageIndex) throws IOException
    {
	int width = _getImageMetadata(imageIndex).getImageWidth();

	// Unable to determine the image width
	if (width == -1) {
	    String msg = "Unable to determine the image width.";
	    throw new IOException(msg);
	}

	return width;
    }

    /**
     * Returns the height in pixels of the given image within the input source.
     *
     * @param imageIndex The index of the image to be queried.
     *
     * @return The height of the image in pixels.
     *
     * @throws IOException If an error occurs reading the height information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public int getHeight(int imageIndex) throws IOException
    {
	int height = _getImageMetadata(imageIndex).getImageHeight();

	// Unable to determine the image height
	if (height == -1) {
	    String msg = "Unable to determine the image height.";
	    throw new IOException(msg);
	}

	return height;
    }

    /**
     * Returns an Iterator containing possible image types to which the given
     * image may be decoded, in the form of ImageTypeSpecifiers.  At least one
     * legal image type will be returned.
     *
     * @param imageIndex The index of the image to be retrieved.
     *
     * @return An Iterator containing at least one ImageTypeSpecifier
     *         representing suggested image types for decoding the current given
     *         image.
     *
     * @throws IOException If an error occurs reading the format information
     *                     from the input source.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public Iterator getImageTypes(int imageIndex) throws IOException
    {
	// Get the image parameters
	EcatImageMetadata metadata = _getImageMetadata(imageIndex);
	int bitsPerPixel = metadata.getBitsPerPixel();
	int width = metadata.getImageWidth();
	int height = metadata.getImageHeight();

	// Check if the image data type is supported
	if (bitsPerPixel == -1) {
	    String msg = "Unable to recognize the image type.";
	    throw new IOException(msg);
	}

	// Check for signed data
	ArrayList list = new ArrayList(1);
	if (bitsPerPixel == 16 && metadata.hasSignedData()) {
	    list.add( Utilities.getGrayImageType(DataBuffer.TYPE_SHORT) );
	}
	else {
	    list.add( Utilities.getGrayImageType(width, height, bitsPerPixel) );
	}

	// Return the Image Type Specifier
	return list.iterator();
    }

    /**
     * Returns an IIOMetadata object representing the metadata associated with
     * the input source as a whole (i.e., not associated with any particular
     * image), or null if the reader does not support reading metadata, is set
     * to ignore metadata, or if no metadata is available.
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     */
    public IIOMetadata getStreamMetadata() throws IOException
    {
	Ecat7MainHeader mainHeader = _getEcatHeader().getMainHeader();
	return new EcatStreamMetadata(mainHeader);
    }

    /**
     * Returns an IIOMetadata object containing metadata associated with the
     * given image, or null if the reader does not support reading metadata, is
     * set to ignore metadata, or if no metadata is available.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved. 
     *
     * @return An IIOMetadata object, or null.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public IIOMetadata getImageMetadata(int imageIndex) throws IOException
    {
	return _getImageMetadata(imageIndex);
    }

    /**
     * Reads the image indexed by imageIndex and returns it as a complete
     * BufferedImage.  The supplied ImageReadParam is ignored.
     *
     * @param imageIndex The index of the image to be retrieved.
     * @param param An ImageReadParam used to control the reading process, or
     *              null.
     *
     * @return The desired portion of the image as a BufferedImage.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    public BufferedImage read(int imageIndex, ImageReadParam param)
	throws IOException
    {
	int width = getWidth(imageIndex);
	int height = getHeight(imageIndex);

	// Update the Listeners
	processImageStarted(imageIndex);

	// Compute the size of the image in bytes
	ImageTypeSpecifier spec = (ImageTypeSpecifier)getImageTypes(0).next();
	int dataType = spec.getSampleModel().getDataType();
	int imageSize = width*height;
	if (dataType == DataBuffer.TYPE_USHORT ||
	    dataType == DataBuffer.TYPE_SHORT) { imageSize *= 2; }

	// Seek to the indexed image
	int frameIndex = imageIndex / _getNumberOfPlanes();
	int imageNumber = imageIndex % _getNumberOfPlanes();
	long offset = _frameOffsets[frameIndex] + 512 + imageNumber*imageSize;
	ImageInputStream imageStream = _getInputStream();
	imageStream.seek(offset);

	// Return the raw image data if required
	if ( Utilities.returnRawBytes(param) ) {
	    BufferedImage bufferedImage = Utilities.getRawByteImage(imageSize);
	    DataBuffer db = bufferedImage.getRaster().getDataBuffer();
	    imageStream.read( ((DataBufferByte)db).getData() );
	    return bufferedImage;
	}

	// Create a Buffered Image for the image data
	Iterator iter = getImageTypes(0);
	BufferedImage bufferedImage = getDestination(null, iter, width, height);
	DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();

	// Read the image data into an array
	byte[] srcBuffer = new byte[imageSize];
	imageStream.readFully(srcBuffer);

	// 8-bit gray image pixel data
	WritableRaster dstRaster = bufferedImage.getRaster();
	if (dataBuffer.getDataType() == DataBuffer.TYPE_BYTE) {
	    bufferedImage = Utilities.getGrayByteImage(srcBuffer, dstRaster);
	}

	// Signed 16-bit gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_SHORT) {
	    bufferedImage = Utilities.getGrayShortImage(srcBuffer, dstRaster,
							false);
	}

	// Unsigned 16-bit gray image pixel data
	else if (dataBuffer.getDataType() == DataBuffer.TYPE_USHORT) {
	    bufferedImage = Utilities.getGrayUshortImage(srcBuffer, dstRaster);
	}

	// Update the Listeners
	if ( abortRequested() ) { processReadAborted(); }
	else { processImageComplete(); }

	// Return the requested image
	return bufferedImage;
    }

    /**
     * Allows any resources held by this object to be released.  It is important
     * for applications to call this method when they know they will no longer
     * be using this ImageReader.  Otherwise, the reader may continue to hold on
     * to resources indefinitely.
     */
    public void dispose()
    {
	// Attempt to close the input stream
	try { if (input != null) { ((ImageInputStream)input).close(); } }
	catch (Exception e) {}

	input = null;
    }

    /**
     * Checks whether or not the specified image index is valid.
     *
     * @param imageIndex The index of the image to check.
     *
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private void _checkImageIndex(int imageIndex) throws IOException
    {
	// Check the bounds of the image index
	if ( imageIndex >= getNumImages(false) ) {
	    String msg = "Image index of " + imageIndex + " >= the total " +
		"number of images (" + getNumImages(false) + ")";
	    throw new IndexOutOfBoundsException(msg);
	}
    }

    /**
     * Gets the input stream.
     *
     * @return Image Input Stream to read data from.
     *
     * @throws IllegalStateException If the input has not been set.
     */
    private ImageInputStream _getInputStream()
    {
	// No input has been set
	if (input == null) {
	    String msg = "No input has been set.";
	    throw new IllegalStateException(msg);
	}

	// Return the Image Input Stream
	return (ImageInputStream)input;
    }

    /**
     * Gets the number of planes.
     *
     * @return Number of planes decoded from the input source.
     *
     * @throws IllegalStateException If the input source has not been set.
     * @throws IOException If an error occurs reading the information from the
     *                     input source.
     */
    private int _getNumberOfPlanes() throws IOException
    {
	int numberOfPlanes;

	// Get the number of planes
	try {
	    Ecat7MainHeader mainHeader = _getEcatHeader().getMainHeader();
	    Object val = mainHeader.getElementValue(Ecat7MainHeader.NUM_PLANES);
	    numberOfPlanes = ((Short)val).intValue();
	}
	catch (Exception e) {
	    String msg = "Unable to get the number of planes.";
	    throw new IOException(msg);
	}

	// Check for a positive number
	if (numberOfPlanes < 1) {
	    String msg = "Invalid number of planes:  " + numberOfPlanes;
	    throw new IOException(msg);
	}

	return numberOfPlanes;
    }

    /**
     * Gets the ECAT Image Metadata for the specified image.
     *
     * @param imageIndex Index of the image whose metadata is to be retrieved.
     *
     * @return ECAT Image Metadata for the specified image.
     *
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     * @throws IndexOutOfBoundsException If the supplied index is out of bounds.
     */
    private EcatImageMetadata _getImageMetadata(int imageIndex)
	throws IOException
    {
	// Check the bounds of the image index
	_checkImageIndex(imageIndex);

	// Determine the frame of the image
	int fIndex = imageIndex / _getNumberOfPlanes();

	// Get the frame subheader for the image
	Ecat7ImageHeader imageHeader = _getEcatHeader().getSubheader(fIndex);

	// Return a new ECAT Image Metadata object
	return new EcatImageMetadata(imageHeader);
    }

    /**
     * Reads the ECAT Header from the input source.  If the ECAT Header has
     * already been decoded, no additional read is performed.
     *
     * @return ECAT Header decoded from the input source.
     * 
     * @throws IOException If an error occurs during reading.
     * @throws IllegalStateException If the input source has not been set.
     */
    private Ecat7Header _getEcatHeader() throws IOException
    {
	// ECAT header is decoded
	if (_ecatHeader != null) { return _ecatHeader; }

	// Read the first metadata block
	byte[] mainHeaderBytes = new byte[512];
	ImageInputStream inputStream = _getInputStream();
	inputStream.readFully(mainHeaderBytes);

	// Decode the main header
	Ecat7MainHeader mainHeader = _getEcat7MainHeader(mainHeaderBytes);
	_ecatHeader = new Ecat7Header(mainHeader);

	// Determine the number of frames to decode
	int numberOfFrames = _ecatHeader.getNumberOfFrames();
	_frameOffsets = new long[numberOfFrames];
	int[] matrixIds = new int[numberOfFrames];

	// Decode all the frames
	int frameNumber = 0;
	while (frameNumber < numberOfFrames) {

	    // Read in the directory list block
	    byte[] listBytes = new byte[512];
	    inputStream.readFully(listBytes);
	    ByteArrayInputStream bStream = new ByteArrayInputStream(listBytes);
	    DataInputStream listStream = new DataInputStream(bStream);

	    // Decode the first entry of the directory list block
	    // (4 bytes) - Number of available entries in this dir list block
	    // (4 bytes) - Offset to next directory list block
	    // (4 bytes) - Offset to previous directory list block
	    // (4 bytes) - Number of used entries in this directory list block
	    int nfree = listStream.readInt();
	    int nxtblk = listStream.readInt();
	    int prvblk = listStream.readInt();
	    int nDims = listStream.readInt();

	    // Decode all the frame offsets in the directory list block
	    int numberOfEntries = Math.min(31, numberOfFrames-frameNumber);
	    for (int i = 0; i < numberOfEntries; i++) {

		// Decode the entry of the directory list block
		// (4 bytes) - Matrix ID
		// (4 bytes) - Matrix Subheader block number
		// (4 bytes) - Block number of last block of the image data
		// (4 bytes) - Status: 1=exists, 0=doesn't exist, -1=deleted
		int matnum = listStream.readInt();
		int strtblk = listStream.readInt();
		int endblk = listStream.readInt();
		int matsat = listStream.readInt();

		// Record the frame offset and matrix id
		_frameOffsets[frameNumber] = 512*(strtblk-1);
		matrixIds[frameNumber] = matnum;
		frameNumber++;
	    }

	    // Advance to the next directory list block
	    if (frameNumber < numberOfFrames) {
		inputStream.seek(512*(nxtblk-1));
	    }
	}

	// Decode the frame subheaders
	for (int i = 0; i < _frameOffsets.length; i++) {

	    // Move the beginning of the frame
	    inputStream.seek(_frameOffsets[i]);

	    // Read the frame block
	    byte[] imgHeaderBytes = new byte[512];
	    inputStream.readFully(imgHeaderBytes);

	    // Decode the frame subheader
	    Ecat7ImageHeader imageHeader = _getEcat7ImageHeader(matrixIds[i],
								imgHeaderBytes);
	    _ecatHeader.setSubheader(imageHeader, i);
	}

	// Return the ECAT Header
	return _ecatHeader;
    }

    /**
     * Reads an ECAT 7 Main Header from the specified byte source.
     *
     * @param mainHeaderBytes Byte array containing the encoded main header.
     *
     * @return ECAT 7 Main Header decoded from the byte source.
     * 
     * @throws IOException If an error occurs during reading.
     */
    private Ecat7MainHeader _getEcat7MainHeader(byte[] mainHeaderBytes)
	throws IOException
    {
	ByteArrayInputStream bStream=new ByteArrayInputStream(mainHeaderBytes);
	DataInputStream headerStream = new DataInputStream(bStream);

	// Define the byte arrays
	byte[] magicNumber = new byte[14];
	byte[] originalFileName = new byte[32];
	byte[] serialNumber = new byte[10];
	byte[] isotopeName = new byte[8];
	byte[] radiopharmaceutical = new byte[32];
	byte[] studyType = new byte[12];
	byte[] patientId = new byte[16];
	byte[] patientName = new byte[32];
	byte[] patientSex = new byte[1];
	byte[] patientDexterity = new byte[1];
	byte[] physicianName = new byte[32];
	byte[] operatorName = new byte[32];
	byte[] studyDescription = new byte[32];
	byte[] facilityName = new byte[20];
	byte[] userProcessCode = new byte[10];
	byte[] dataUnits = new byte[32];

	// Define other arrays
	float[] bedPosition = new float[15];
	short[] fillCti = new short[6];

	// Read info from the stream
	headerStream.readFully(magicNumber);
	headerStream.readFully(originalFileName);
	short swVersion = headerStream.readShort();
	short systemType = headerStream.readShort();
	short fileType = headerStream.readShort();
	headerStream.readFully(serialNumber);
	int scanStartTime = headerStream.readInt();
	headerStream.readFully(isotopeName);
	float isotopeHalflife = headerStream.readFloat();
	headerStream.readFully(radiopharmaceutical);
	float gantryTilt = headerStream.readFloat();
	float gantryRotation = headerStream.readFloat();
	float bedElevation = headerStream.readFloat();
	float intrinsicTilt = headerStream.readFloat();
	short wobbleSpeed = headerStream.readShort();
	short transmSourceType = headerStream.readShort();
	float distanceScanned = headerStream.readFloat();
	float transaxialFov = headerStream.readFloat();
	short angularCompression = headerStream.readShort();
	short coinSampMode = headerStream.readShort();
	short axialSampMode = headerStream.readShort();
	float ecatCalibrationFactor = headerStream.readFloat();
	short calibrationUnits = headerStream.readShort();
	short calibrationUnitsLabel = headerStream.readShort();
	short compressionCode = headerStream.readShort();
	headerStream.readFully(studyType);
	headerStream.readFully(patientId);
	headerStream.readFully(patientName);
	headerStream.readFully(patientSex);
	headerStream.readFully(patientDexterity);
	float patientAge = headerStream.readFloat();
	float patientHeight = headerStream.readFloat();
	float patientWeight = headerStream.readFloat();
	int patientBirthDate = headerStream.readInt();
	headerStream.readFully(physicianName);
	headerStream.readFully(operatorName);
	headerStream.readFully(studyDescription);
	short acquisitionType = headerStream.readShort();
	short patientOrientation = headerStream.readShort();
	headerStream.readFully(facilityName);
	short numPlanes = headerStream.readShort();
	short numFrames = headerStream.readShort();
	short numGates = headerStream.readShort();
	short numBedPos = headerStream.readShort();
	float initBedPosition = headerStream.readFloat();

	for (int i = 0; i < bedPosition.length; i++) {
	    bedPosition[i] = headerStream.readFloat();
	}
      
	float planeSeparation = headerStream.readFloat();
	short lwrSctrThres = headerStream.readShort();
	short lwrTrueThres = headerStream.readShort();
	short uprTrueThres = headerStream.readShort();
	headerStream.readFully(userProcessCode);
	short acquisitionMode = headerStream.readShort();
	float binSize = headerStream.readFloat();
	float branchingFraction = headerStream.readFloat();
	int doseStartTime = headerStream.readInt();
	float dosage = headerStream.readFloat();
	float wellCounterCorrFactor = headerStream.readFloat();
	headerStream.readFully(dataUnits);
	short septaState = headerStream.readShort();

	for (int i = 0; i < fillCti.length; i++) {
	    fillCti[i] = headerStream.readShort();
	}

	// Return an ECAT 7 Main Header
	return new Ecat7MainHeader(magicNumber, originalFileName, 
				   swVersion, systemType, fileType,
				   serialNumber, scanStartTime,
				   isotopeName, isotopeHalflife,
				   radiopharmaceutical, gantryTilt,
				   gantryRotation, bedElevation,
				   intrinsicTilt, wobbleSpeed,
				   transmSourceType, distanceScanned,
				   transaxialFov, angularCompression,
				   coinSampMode, axialSampMode,
				   ecatCalibrationFactor, calibrationUnits,
				   calibrationUnitsLabel, compressionCode,
				   studyType, patientId,
				   patientName, patientSex,
				   patientDexterity, patientAge,
				   patientHeight, patientWeight,
				   patientBirthDate, physicianName,
				   operatorName, studyDescription,
				   acquisitionType, patientOrientation,
				   facilityName, numPlanes, numFrames,
				   numGates, numBedPos,
				   initBedPosition, bedPosition,
				   planeSeparation, lwrSctrThres,
				   lwrTrueThres, uprTrueThres,
				   userProcessCode, acquisitionMode,
				   binSize, branchingFraction,
				   doseStartTime, dosage,
				   wellCounterCorrFactor, dataUnits,
				   septaState, fillCti);
    }

    /**
     * Reads an ECAT 7 Image Header from the specified byte source.
     *
     * @param matrixId Matrix id.
     * @param imageHeaderBytes Byte array containing the encoded image header.
     *
     * @return ECAT 7 Image Header decoded from the byte source.
     * 
     * @throws IOException If an error occurs during reading.
     */
    private Ecat7ImageHeader _getEcat7ImageHeader(int matrixId,
						  byte[] imageHeaderBytes)
	throws IOException
    {
	ByteArrayInputStream bStrm = new ByteArrayInputStream(imageHeaderBytes);
	DataInputStream headerStream = new DataInputStream(bStrm);

	// Define the byte arrays
	byte[] annotation = new byte[40];
	byte[] fillCti = new byte[2*87];
	byte[] fillUser = new byte[2*49];

	// Read info from the stream
	short dataType = headerStream.readShort();
	short numDimensions = headerStream.readShort();
	short xDimension = headerStream.readShort();
	short yDimension = headerStream.readShort();
	short zDimension = headerStream.readShort();
	float xOffset = headerStream.readFloat();
	float yOffset = headerStream.readFloat();
	float zOffset = headerStream.readFloat();
	float reconZoom = headerStream.readFloat();
	float scaleFactor = headerStream.readFloat();
	short imageMin = headerStream.readShort();
	short imageMax = headerStream.readShort();
	float xPixelSize = headerStream.readFloat();
	float yPixelSize = headerStream.readFloat();
	float zPixelSize = headerStream.readFloat();
	int frameDuration = headerStream.readInt();
	int frameStartTime = headerStream.readInt();
	short filterCode = headerStream.readShort();
	float xResolution = headerStream.readFloat();
	float yResolution = headerStream.readFloat();
	float zResolution = headerStream.readFloat();
	float numRElements = headerStream.readFloat();
	float numAngles = headerStream.readFloat();
	float zRotationAngle = headerStream.readFloat();
	float decayCorrFctr = headerStream.readFloat();
	int processingCode = headerStream.readInt();
	int gateDuration = headerStream.readInt();
	int rWaveOffset = headerStream.readInt();
	int numAcceptedBeats = headerStream.readInt();
	float filterCutoffFrequency = headerStream.readFloat();
	float filterResolution = headerStream.readFloat();
	float filterRampSlope = headerStream.readFloat();
	short filterOrder = headerStream.readShort();
	float filterScatterFraction = headerStream.readFloat();
	float filterScatterSlope = headerStream.readFloat();
	headerStream.readFully(annotation);
	float mt11 = headerStream.readFloat();
	float mt12 = headerStream.readFloat();
	float mt13 = headerStream.readFloat();
	float mt21 = headerStream.readFloat();
	float mt22 = headerStream.readFloat();
	float mt23 = headerStream.readFloat();
	float mt31 = headerStream.readFloat();
	float mt32 = headerStream.readFloat();
	float mt33 = headerStream.readFloat();
	float rFilterCutoff = headerStream.readFloat();
	float rFilterResolution = headerStream.readFloat();
	short rFilterCode = headerStream.readShort();
	short rFilterOrder = headerStream.readShort();
	float zFilterCutoff = headerStream.readFloat();
	float zFilterResolution = headerStream.readFloat();
	short zFilterCode = headerStream.readShort();
	short zFilterOrder = headerStream.readShort();
	float mt14 = headerStream.readFloat();
	float mt24 = headerStream.readFloat();
	float mt34 = headerStream.readFloat();
	short scatterType = headerStream.readShort();
	short reconType = headerStream.readShort();
	short reconViews = headerStream.readShort();
	headerStream.readFully(fillCti);
	headerStream.readFully(fillUser);

	// Return an ECAT 7 Image Header
	return new Ecat7ImageHeader(matrixId, dataType, numDimensions,
				    xDimension, yDimension, zDimension,
				    xOffset, yOffset, zOffset,
				    reconZoom, scaleFactor, imageMin,
				    imageMax, xPixelSize, yPixelSize,
				    zPixelSize, frameDuration,
				    frameStartTime, filterCode,
				    xResolution, yResolution,
				    zResolution, numRElements,
				    numAngles, zRotationAngle,
				    decayCorrFctr, processingCode,
				    gateDuration, rWaveOffset,
				    numAcceptedBeats, filterCutoffFrequency,
				    filterResolution, filterRampSlope,
				    filterOrder, filterScatterFraction,
				    filterScatterSlope, annotation,
				    mt11, mt12, mt13, mt21,
				    mt22, mt23, mt31, mt32,
				    mt33, rFilterCutoff,
				    rFilterResolution, rFilterCode,
				    rFilterOrder, zFilterCutoff,
				    zFilterResolution, zFilterCode,
				    zFilterOrder, mt14, mt24,
				    mt34, scatterType, reconType,
				    reconViews, fillCti, fillUser);
    }
}
