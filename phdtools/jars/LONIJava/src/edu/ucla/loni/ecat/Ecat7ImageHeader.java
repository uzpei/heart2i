/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat;

/**
 * ECAT Subheader for the image ECAT 7 section.
 *
 * @version 26 July 2006
 */
public class Ecat7ImageHeader extends EcatSubheader
{
    /** Matrix Id. */
    public static final String MATRIX_ID = "matrix_id";

    /** Data type. */
    public static final String DATA_TYPE = "data_type";

    /** Number of dimensions. */
    public static final String NUM_DIMENSIONS = "num_dimensions";

    /** X dimension. */
    public static final String X_DIMENSION = "x_dimension";

    /** Y dimension. */
    public static final String Y_DIMENSION = "y_dimension";

    /** Z dimension. */
    public static final String Z_DIMENSION = "z_dimension";

    /** X offset (cm). */
    public static final String X_OFFSET = "x_offset";

    /** Y offset (cm). */
    public static final String Y_OFFSET = "y_offset";

    /** Z offset (cm). */
    public static final String Z_OFFSET = "z_offset";

    /** Reconstruction maginification factor. */
    public static final String RECON_ZOOM = "recon_zoom";

    /** Scale factor. */
    public static final String SCALE_FACTOR = "scale_factor";

    /** Image min. */
    public static final String IMAGE_MIN = "image_min";

    /** Image max. */
    public static final String IMAGE_MAX = "image_max";

    /** X dimension pixel size (cm). */
    public static final String X_PIXEL_SIZE = "x_pixel_size";

    /** Y dimension pixel size (cm). */
    public static final String Y_PIXEL_SIZE = "y_pixel_size";

    /** Z dimension pixel size (cm). */
    public static final String Z_PIXEL_SIZE = "z_pixel_size";

    /** Frame duration (msec). */
    public static final String FRAME_DURATION = "frame_duration";

    /** Offset from first frame (msec). */
    public static final String FRAME_START_TIME = "frame_start_time";

    /** Filter code. */
    public static final String FILTER_CODE = "filter_code";

    /** X resolution (cm). */
    public static final String X_RESOLUTION = "x_resolution";

    /** Y resolution (cm). */
    public static final String Y_RESOLUTION = "y_resolution";

    /** Z resolution (cm). */
    public static final String Z_RESOLUTION = "z_resolution";

    /** Number R elements from sinogram. */
    public static final String NUM_R_ELEMENTS = "num_r_elements";

    /** Number of angles from sinogram. */
    public static final String NUM_ANGLES = "num_angles";

    /** Rotation in the xy plane (degrees). */
    public static final String Z_ROTATION_ANGLE = "z_rotation_angle";

    /** Decay corr factor. */
    public static final String DECAY_CORR_FCTR = "decay_corr_fctr";

    /** Processing code. */
    public static final String PROCESSING_CODE = "processing_code";

    /** Gate duration. */
    public static final String GATE_DURATION = "gate_duration";

    /** R wave offset. */
    public static final String R_WAVE_OFFSET = "r_wave_offset";

    /** Number of accepted beats. */
    public static final String NUM_ACCEPTED_BEATS = "num_accepted_beats";

    /** Filter cutoff frequency. */
    public static final String FILTER_CUTOFF_FREQUENCY =
	"filter_cutoff_frequency";

    /** Filter resolution. */
    public static final String FILTER_RESOLUTION = "filter_resolution";

    /** Filter ramp slope. */
    public static final String FILTER_RAMP_SLOPE = "filter_ramp_slope";

    /** Filter order. */
    public static final String FILTER_ORDER = "filter_order";

    /** Filter scatter fraction. */
    public static final String FILTER_SCATTER_FRACTION =
	"filter_scatter_fraction";

    /** Filter scatter slope. */
    public static final String FILTER_SCATTER_SLOPE = "filter_scatter_slope";

    /** Annotation. */
    public static final String ANNOTATION = "annotation";

    /** Matrix 1 1. */
    public static final String MT_1_1 = "mt_1_1";

    /** Matrix 1 2. */
    public static final String MT_1_2 = "mt_1_2";

    /** Matrix 1 3. */
    public static final String MT_1_3 = "mt_1_3";

    /** Matrix 2 1. */
    public static final String MT_2_1 = "mt_2_1";

    /** Matrix 2 2. */
    public static final String MT_2_2 = "mt_2_2";

    /** Matrix 2 3. */
    public static final String MT_2_3 = "mt_2_3";

    /** Matrix 3 1. */
    public static final String MT_3_1 = "mt_3_1";

    /** Matrix 3 2. */
    public static final String MT_3_2 = "mt_3_2";

    /** Matrix 3 3. */
    public static final String MT_3_3 = "mt_3_3";

    /** R filter cutoff. */
    public static final String RFILTER_CUTOFF = "rfilter_cutoff";

    /** R filter resolution. */
    public static final String RFILTER_RESOLUTION = "rfilter_resolution";

    /** R filter code. */
    public static final String RFILTER_CODE = "rfilter_code";

    /** R filter order. */
    public static final String RFILTER_ORDER = "rfilter_order";

    /** Z filter cutoff. */
    public static final String ZFILTER_CUTOFF = "zfilter_cutoff";

    /** Z filter resolution. */
    public static final String ZFILTER_RESOLUTION = "zfilter_resolution";

    /** Z filter code. */
    public static final String ZFILTER_CODE = "zfilter_code";

    /** Z filter coder. */
    public static final String ZFILTER_ORDER = "zfilter_order";

    /** Matrix 1 4. */
    public static final String MT_1_4 = "mt_1_4";

    /** Matrix 2 4. */
    public static final String MT_2_4 = "mt_2_4";

    /** Matrix 3 4. */
    public static final String MT_3_4 = "mt_3_4";

    /** Scatter type. */
    public static final String SCATTER_TYPE = "scatter_type";

    /** Recon type. */
    public static final String RECON_TYPE = "recon_type";

    /** Recon views. */
    public static final String RECON_VIEWS = "recon_views";

    /** Fill cti. */
    public static final String FILL_CTI = "fill_cti";

    /** Fill user. */
    public static final String FILL_USER = "fill_user";

    /**
     * Constructs an ECAT 7 Image Header.
     *
     * @param matrixId Matrix id.
     * @param dataType Data type.
     * @param numDimensions Number of dimensions.
     * @param xDimension X dimension.
     * @param yDimension Y dimension.
     * @param zDimension Z dimension.
     * @param xOffset X offset (cm).
     * @param yOffset Y offset (cm).
     * @param zOffset Z offset (cm).
     * @param reconZoom Reconstruction maginification factor.
     * @param scaleFactor Scale factor.
     * @param imageMin Image min.
     * @param imageMax Image max.
     * @param xPixelSize X dimension pixel size (cm).
     * @param yPixelSize Y dimension pixel size (cm).
     * @param zPixelSize Z dimension pixel size (cm).
     * @param frameDuration Frame duration (msec).
     * @param frameStartTime Offset from first frame (msec).
     * @param filterCode Filter code.
     * @param xResolution X resolution (cm).
     * @param yResolution Y resolution (cm).
     * @param zResolution Z resolution (cm).
     * @param numRElements Number R elements from sinogram.
     * @param numAngles Number of angles from sinogram.
     * @param zRotationAngle Rotation in the xy plane (degrees).
     * @param decayCorrFctr Decay corr factor.
     * @param processingCode Processing code.
     * @param gateDuration Gate duration.
     * @param rWaveOffset R wave offset.
     * @param numAcceptedBeats Number of accepted beats.
     * @param filterCutoffFrequency Filter cutoff frequency.
     * @param filterResolution Filter resolution.
     * @param filterRampSlope Filter ramp slope.
     * @param filterOrder Filter order.
     * @param filterScatterFraction Filter scatter fraction.
     * @param filterScatterSlope Filter scatter slope.
     * @param annotation Annotation.
     * @param mt11 Matrix 1 1.
     * @param mt12 Matrix 1 2.
     * @param mt13 Matrix 1 3.
     * @param mt21 Matrix 2 1.
     * @param mt22 Matrix 2 2.
     * @param mt23 Matrix 2 3.
     * @param mt31 Matrix 3 1.
     * @param mt32 Matrix 3 2.
     * @param mt33 Matrix 3 3.
     * @param rFilterCutoff R filter cutoff.
     * @param rFilterResolution R filter resolution.
     * @param rFilterCode R filter code.
     * @param rFilterOrder R filter order.
     * @param zFilterCutoff Z filter cutoff.
     * @param zFilterResolution Z filter resolution.
     * @param zFilterCode Z filter code.
     * @param zFilterOrder Z filter order.
     * @param mt14 Matrix 1 4.
     * @param mt24 Matrix 2 4.
     * @param mt34 Matrix 3 4.
     * @param scatterType Scatter type.
     * @param reconType Recon type.
     * @param reconViews Recon views.
     * @param fillCti Fill cti.
     * @param fillUser Fill user.
     */
    public Ecat7ImageHeader(int matrixId, short dataType, short numDimensions,
			    short xDimension, short yDimension,
			    short zDimension,
			    float xOffset, float yOffset, float zOffset,
			    float reconZoom, float scaleFactor, short imageMin,
			    short imageMax, float xPixelSize, float yPixelSize,
			    float zPixelSize, int frameDuration,
			    int frameStartTime, short filterCode,
			    float xResolution, float yResolution,
			    float zResolution, float numRElements,
			    float numAngles, float zRotationAngle,
			    float decayCorrFctr, int processingCode,
			    int gateDuration, int rWaveOffset,
			    int numAcceptedBeats, float filterCutoffFrequency,
			    float filterResolution, float filterRampSlope,
			    short filterOrder, float filterScatterFraction,
			    float filterScatterSlope, byte[] annotation,
			    float mt11, float mt12, float mt13, float mt21,
			    float mt22, float mt23, float mt31, float mt32,
			    float mt33, float rFilterCutoff,
			    float rFilterResolution, short rFilterCode,
			    short rFilterOrder, float zFilterCutoff,
			    float zFilterResolution, short zFilterCode,
			    short zFilterOrder, float mt14, float mt24,
			    float mt34, short scatterType, short reconType,
			    short reconViews, byte[] fillCti, byte[] fillUser)
    {
	super(62);

	_addElement(MATRIX_ID, new Integer(matrixId));
	_addElement(DATA_TYPE, new Short(dataType));
	_addElement(NUM_DIMENSIONS, new Short(numDimensions));
	_addElement(X_DIMENSION, new Short(xDimension));
	_addElement(Y_DIMENSION, new Short(yDimension));
	_addElement(Z_DIMENSION, new Short(zDimension));
	_addElement(X_OFFSET, new Float(xOffset));
	_addElement(Y_OFFSET, new Float(yOffset));
	_addElement(Z_OFFSET, new Float(zOffset));
	_addElement(RECON_ZOOM, new Float(reconZoom));
	_addElement(SCALE_FACTOR, new Float(scaleFactor));
	_addElement(IMAGE_MIN, new Short(imageMin));
	_addElement(IMAGE_MAX, new Short(imageMax));
	_addElement(X_PIXEL_SIZE, new Float(xPixelSize));
	_addElement(Y_PIXEL_SIZE, new Float(yPixelSize));
	_addElement(Z_PIXEL_SIZE, new Float(zPixelSize));
	_addElement(FRAME_DURATION, new Integer(frameDuration));
	_addElement(FRAME_START_TIME, new Integer(frameStartTime));
	_addElement(FILTER_CODE, new Short(filterCode));
	_addElement(X_RESOLUTION, new Float(xResolution));
	_addElement(Y_RESOLUTION, new Float(yResolution));
	_addElement(Z_RESOLUTION, new Float(zResolution));
	_addElement(NUM_R_ELEMENTS, new Float(numRElements));
	_addElement(NUM_ANGLES, new Float(numAngles));
	_addElement(Z_ROTATION_ANGLE, new Float(zRotationAngle));
	_addElement(DECAY_CORR_FCTR, new Float(decayCorrFctr));
	_addElement(PROCESSING_CODE, new Integer(processingCode));
	_addElement(GATE_DURATION, new Integer(gateDuration));
	_addElement(R_WAVE_OFFSET, new Integer(rWaveOffset));
	_addElement(NUM_ACCEPTED_BEATS, new Integer(numAcceptedBeats));
	_addElement(FILTER_CUTOFF_FREQUENCY, new Float(filterCutoffFrequency));
	_addElement(FILTER_RESOLUTION, new Float(filterResolution));
	_addElement(FILTER_RAMP_SLOPE, new Float(filterRampSlope));
	_addElement(FILTER_ORDER, new Short(filterOrder));
	_addElement(FILTER_SCATTER_FRACTION, new Float(filterScatterFraction));
	_addElement(FILTER_SCATTER_SLOPE, new Float(filterScatterSlope));
	_addElement(ANNOTATION, _createString(annotation));
	_addElement(MT_1_1, new Float(mt11));
	_addElement(MT_1_2, new Float(mt12));
	_addElement(MT_1_3, new Float(mt13));
	_addElement(MT_2_1, new Float(mt21));
	_addElement(MT_2_2, new Float(mt22));
	_addElement(MT_2_3, new Float(mt23));
	_addElement(MT_3_1, new Float(mt31));
	_addElement(MT_3_2, new Float(mt32));
	_addElement(MT_3_3, new Float(mt33));
	_addElement(RFILTER_CUTOFF, new Float(rFilterCutoff));
	_addElement(RFILTER_RESOLUTION, new Float(rFilterResolution));
	_addElement(RFILTER_CODE, new Short(rFilterCode));
	_addElement(RFILTER_ORDER, new Short(rFilterOrder));
	_addElement(ZFILTER_CUTOFF, new Float(zFilterCutoff));
	_addElement(ZFILTER_RESOLUTION, new Float(zFilterResolution));
	_addElement(ZFILTER_CODE, new Short(zFilterCode));
	_addElement(ZFILTER_ORDER, new Short(zFilterOrder));
	_addElement(MT_1_4, new Float(mt14));
	_addElement(MT_2_4, new Float(mt24));
	_addElement(MT_3_4, new Float(mt34));
	_addElement(SCATTER_TYPE, new Short(scatterType));
	_addElement(RECON_TYPE, new Short(reconType));
	_addElement(RECON_VIEWS, new Short(reconViews));
	_addElement(FILL_CTI, fillCti);
	_addElement(FILL_USER, fillUser);
    }
}
