/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ecat.plugin;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Locale;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;

/**
 * Image Writer SPI (Service Provider Interface) for the ECAT image format.
 *
 * @version 11 April 2005
 */
public class EcatImageWriterSpi extends ImageWriterSpi
{
  /** Name of the vendor who supplied this plug-in. */
  private static String _vendorName = "Laboratory of Neuro Imaging (LONI)";

  /** Version of this plug-in. */
  private static String _version = "1.0";

  /** Names of the formats written by this plug-in. */
  private static String[] _formatNames = {"ecat", "ECAT"};

  /** Names of commonly used suffixes for files in the supported formats. */
  private static String[] _fileSuffixes = {"v", "V"};

  /** MIME types of supported formats. */
  private static String[] _mimeTypes = {"image/ecat"};

  /** Name of the associated Image Writer. */
  private static String _writer = EcatImageWriter.class.getName();

  /** Allowed output type classes for the plugin-in. */
  private static Class[] _outputTypes = {ImageOutputStream.class};

  /** Names of the associated Image Reader SPI's. */
  private static String[] _readSpis = {EcatImageReaderSpi.class.getName()};

  /** Constructs an ECAT Image Writer SPI. */
  public EcatImageWriterSpi()
    {
      super(_vendorName, _version, _formatNames, _fileSuffixes, _mimeTypes,
	    _writer, _outputTypes, _readSpis,

	    // Support one native stream metadata format
	    false,
	    EcatStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    EcatStreamMetadataFormat.class.getName(), null, null,

	    // Support one native image metatdata format
	    true,
	    EcatImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME,
	    EcatImageMetadataFormat.class.getName(), null, null);
    }

  /**
   * Returns true if the ImageWriter implementation associated with this
   * service provider is able to encode an image with the given layout.  The
   * layout (i.e., the image's SampleModel and ColorModel) is described by an
   * ImageTypeSpecifier object.
   *
   * @param spec ImageTypeSpecifier specifying the layout of the image to be
   *             written.
   *
   * @return True if this writer is likely to be able to encode images with
   *         the given layout.
   *
   * @throws IllegalArgumentException If spec is null.
   */
  public boolean canEncodeImage(ImageTypeSpecifier spec)
    {
      int type = spec.getBufferedImageType();

      // Support standard 16 bit grayscale images
      if ( type == BufferedImage.TYPE_USHORT_GRAY ) { return true; }
      return false;
    }

  /**
   * Returns a brief, human-readable description of this service provider and
   * its associated implementation.
   *
   * @param locale Locale for which the return value should be localized.
   *
   * @return Description of this service provider.
   */
  public String getDescription(Locale locale)
    {
      return "ECAT 7 image writer";
    }

  /**
   * Returns an instance of the ImageWriter implementation associated with
   * this service provider.  The returned object will initially be in an
   * initial state as if its reset method had been called.
   *
   * @param extension A plug-in specific extension object, which may be null.
   *
   * @return An ImageWriter instance.
   *
   * @throws IOException If the attempt to instantiate the writer fails.
   * @throws IllegalArgumentException If the ImageWriter finds the extension
   *                                  object to be unsuitable.
   */
  public ImageWriter createWriterInstance(Object extension) throws IOException
    {
      return new EcatImageWriter(this);
    }
}
