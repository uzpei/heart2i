/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package edu.ucla.loni.ecat;

/**
 * Header for the ECAT 7 file format.
 *
 * @version 8 April 2005
 */
public class Ecat7Header
{
  /** ECAT Element Map for the main ECAT 7 section. */
  private Ecat7MainHeader _mainHeader;

  /** ECAT Subheaders for the image ECAT 7 sections. */
  private Ecat7ImageHeader[] _subheaders;

  /**
   * Constructs an ECAT 7 Header.
   *
   * @param mainHeader ECAT Element Map for the main ECAT 7 section.
   *
   * @throws IllegalArgumentException If the number of frames is non-positive.
   */
  public Ecat7Header(Ecat7MainHeader mainHeader)
    {
      _mainHeader = mainHeader;

      // Get the number of frames
      int numberOfFrames;
      try {
	Object value = _mainHeader.getElementValue(Ecat7MainHeader.NUM_FRAMES);
	numberOfFrames = ((Short)value).intValue();
      }
      catch (Exception e) {
	String msg = "Unable to get the number of frames.";
	throw new IllegalArgumentException(msg);
      }

      // Check for a positive number
      if (numberOfFrames < 1) {
	String msg = "Invalid number of frames:  " + numberOfFrames;
	throw new IllegalArgumentException(msg);
      }

      _subheaders = new Ecat7ImageHeader[numberOfFrames];
    }

  /**
   * Gets the main header.
   *
   * @return ECAT Element Map for the main ECAT 7 section.
   */
  public Ecat7MainHeader getMainHeader()
    {
      return _mainHeader;
    }

  /**
   * Gets the number of frames.
   *
   * @return Number of frames.
   */
  public int getNumberOfFrames()
    {
      return _subheaders.length;
    }

  /**
   * Sets the specified subheader.
   *
   * @param imageHeader ECAT Subheader for the image ECAT 7 section.
   * @param index Index of the subheader.
   *
   * @throws ArrayIndexOutOfBoundsException If the index is invalid.
   */
  public void setSubheader(Ecat7ImageHeader imageHeader, int index)
    {
      _subheaders[index] = imageHeader;
    }

  /**
   * Gets the indexed subheader.
   *
   * @param index Index of the subheader.
   *
   * @return ECAT Subheader for the image ECAT 7 section.
   *
   * @throws ArrayIndexOutOfBoundsException If the index is invalid.
   */
  public Ecat7ImageHeader getSubheader(int index)
    {
      return _subheaders[index];
    }
}
