/*
  COPYRIGHT NOTICE
  Copyright (c) 2009  Scott C. Neu and Arthur W. Toga

  This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package edu.ucla.loni.ecat.plugin;

import edu.ucla.loni.ecat.Ecat7ImageHeader;
import edu.ucla.loni.ecat.Ecat7MainHeader;
import java.util.ListResourceBundle;

/**
 * List Resource Bundle that provides descriptions for ECAT elements.
 *
 * @version 26 July 2006
 */
public class EcatMetadataFormatResources extends ListResourceBundle
{
    /** Constructs an ECAT Metadata Format Resources. */
    public EcatMetadataFormatResources()
    {
    }

    /**
     * Gets the contents of the Resource Bundle.
     *
     * @return Object array of the contents, where each item of the array is a
     *         pair of Objects.  The first element of each pair is the key,
     *         which must be a String, and the second element is the value
     *         associated with that key.
     */
    public Object[][] getContents()
    {
	String strmName = EcatStreamMetadataFormat.NATIVE_METADATA_FORMAT_NAME;
	String imageName = EcatImageMetadataFormat.NATIVE_METADATA_FORMAT_NAME;

	Object[][] contents = {

	    // Node name, followed by description
	    {strmName, "ECAT stream metadata."},
	    {imageName, "ECAT image metadata."},

	    // Node name + "/" + attribute name, followed by description
	    {strmName + "/" + Ecat7MainHeader.MAGIC_NUMBER,
	     "Unix file type indentification number."},
	    {strmName + "/" + Ecat7MainHeader.ORIGINAL_FILE_NAME,
	     "Scan file's creation name."},
	    {strmName + "/" + Ecat7MainHeader.SW_VERSION,
	     "Software version number."},
	    {strmName + "/" + Ecat7MainHeader.SYSTEM_TYPE,
	     "Scanner model."},
	    {strmName + "/" + Ecat7MainHeader.FILE_TYPE, 
	     "00=unknown, 01=Sinogram, 02=Image-16,\n" +
	     "03=Attenuation Correction, 04=Normalization, 05=Polar Map,\n" +
	     "06=Volume 8, 07=Volume 16, 08=Projection 8, 09=Projection 16,\n" +
	     "10=Image 8, 11=3D Sinogram 16, 12=3D Sinogram 8,\n" +
	     "13=3D Normalization, 14=3D Sinogram Fit."},
	    {strmName + "/" + Ecat7MainHeader.SERIAL_NUMBER,
	     "Serial number of the gantry."},
	    {strmName + "/" + Ecat7MainHeader.SCAN_START_TIME,
	     "Date and time when acquisition was started (sec from base " +
	     "time)."},
	    {strmName + "/" + Ecat7MainHeader.ISOTOPE_NAME,
	     "Isotope."},
	    {strmName + "/" + Ecat7MainHeader.ISOTOPE_HALFLIFE,
	     "Half-life of isotope specified (in sec.)."},
	    {strmName + "/" + Ecat7MainHeader.RADIOPHARMACEUTICAL,
	     "String representation of the tracer name."},
	    {strmName + "/" + Ecat7MainHeader.GANTRY_TILT,
	     "Angle (degrees)."},
	    {strmName + "/" + Ecat7MainHeader.GANTRY_ROTATION,
	     "Angle (degrees)."},
	    {strmName + "/" + Ecat7MainHeader.BED_ELEVATION,
	     "Bed height from lowest point (cm)."},
	    {strmName + "/" + Ecat7MainHeader.INTRINSIC_TILT,
	     "Angle that the first detector of Bucket 0 is offset from top\n" +
	     "center (in degrees)."},
	    {strmName + "/" + Ecat7MainHeader.WOBBLE_SPEED,
	     "Revolutions/minute (0 if not wobbled)."},
	    {strmName + "/" + Ecat7MainHeader.TRANSM_SOURCE_TYPE,
	     "Enumerated type (SRC_NONE, _RRING, _RING, _ROD, _RROD)."},
	    {strmName + "/" + Ecat7MainHeader.DISTANCE_SCANNED,
	     "Total distance scanned (cm)."},
	    {strmName + "/" + Ecat7MainHeader.TRANSAXIAL_FOV,
	     "Diameter of transaxial view (cm)."},
	    {strmName + "/" + Ecat7MainHeader.ANGULAR_COMPRESSION,
	     "0=no mash, 1=mash of 2, 2=mash of 4."},
	    {strmName + "/" + Ecat7MainHeader.COIN_SAMP_MODE,
	     "0=Net trues, 1=Prompts and Delayed, 3=Prompts, Delayed, and\n" +
	     "Multiples."},
	    {strmName + "/" + Ecat7MainHeader.AXIAL_SAMP_MODE,
	     "0=Normal, 1=2X, 2=3X."},
	    {strmName + "/" + Ecat7MainHeader.ECAT_CALIBRATION_FACTOR,
	     "Quantification scale factor (to convert from ECAT counts to\n" +
	     "activity counts)."},
	    {strmName + "/" + Ecat7MainHeader.CALIBRATION_UNITS,
	     "0=Uncalibrated; 1=Calibrated; 2=Processed."},
	    {strmName + "/" + Ecat7MainHeader.CALIBRATION_UNITS_LABEL,
	     "Enumerated type (BLOOD_FLOW, LMRGLU)."},
	    {strmName + "/" + Ecat7MainHeader.COMPRESSION_CODE,
	     "Enumerated type (COMP_NONE, (This is the only value))."},
	    {strmName + "/" + Ecat7MainHeader.STUDY_TYPE, "Study descriptor."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_ID,
	     "Patient identification descriptor."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_NAME,
	     "Patient name."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_SEX,
	     "Enumerated type (SEX_MALE, _FEMALE, _UNKNOWN)."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_DEXTERITY,
	     "Enumerated type (DEXT_RT, _LF, _UNKNOWN)."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_AGE,
	     "Patient age (years)."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_HEIGHT,
	     "Patient height (cm)."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_WEIGHT,
	     "Patient weight (kg)."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_BIRTH_DATE,
	     "YYYYMMDD (sec from time zero)."},
	    {strmName + "/" + Ecat7MainHeader.PHYSICIAN_NAME,
	     "Attending Physician name (free format)."},
	    {strmName + "/" + Ecat7MainHeader.OPERATOR_NAME,
	     "Operator name (free format)."},
	    {strmName + "/" + Ecat7MainHeader.STUDY_DESCRIPTION,
	     "Study description."},
	    {strmName + "/" + Ecat7MainHeader.ACQUISITION_TYPE,
	     "0=Undefined; 1=Blank; 2=Transmission; 3=Static emission;\n" +
	     "4=Dynamic emission; 5=Gated emission; " +
	     "6=Transmission rectilinear;\n7=Emission rectilinear."},
	    {strmName + "/" + Ecat7MainHeader.PATIENT_ORIENTATION,
	     "Enumerated Type (Bit 0 clear - Feet first, Bit 0 set - Head\n" +
	     "first, Bit 1-2 00 - Prone, Bit 1-2 01 - Supine,\n" +
	     "Bit 1-2 10 - Decubitus Right, Bit 1-2 11 - Decubitus Left)."},
	    {strmName + "/" + Ecat7MainHeader.FACILITY_NAME,
	     "Facility name."},
	    {strmName + "/" + Ecat7MainHeader.NUM_PLANES, 
	     "Number of planes of data collected."},
	    {strmName + "/" + Ecat7MainHeader.NUM_FRAMES, 
	     "Number of frames of data collected OR Highest frame number\n" +
	     "(in partially reconstructed files)."},
	    {strmName + "/" + Ecat7MainHeader.NUM_GATES, 
	     "Number of gates of data collected."},
	    {strmName + "/" + Ecat7MainHeader.NUM_BED_POS, 
	     "Number of bed positions of data collected."},
	    {strmName + "/" + Ecat7MainHeader.INIT_BED_POSITION, 
	     "Absolute location of initial bed position (in cm.)."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_0, 
	     "Absolute bed location (in cm.) 0."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_1,
	     "Absolute bed location (in cm.) 1."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_2,
	     "Absolute bed location (in cm.) 2."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_3,
	     "Absolute bed location (in cm.) 3."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_4,
	     "Absolute bed location (in cm.) 4."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_5,
	     "Absolute bed location (in cm.) 5."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_6,
	     "Absolute bed location (in cm.) 6."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_7,
	     "Absolute bed location (in cm.) 7."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_8,
	     "Absolute bed location (in cm.) 8."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_9,
	     "Absolute bed location (in cm.) 9."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_10,
	     "Absolute bed location (in cm.) 10."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_11,
	     "Absolute bed location (in cm.) 11."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_12,
	     "Absolute bed location (in cm.) 12."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_13,
	     "Absolute bed location (in cm.) 13."},
	    {strmName + "/" + Ecat7MainHeader.BED_POSITION_14,
	     "Absolute bed location (in cm.) 14."},
	    {strmName + "/" + Ecat7MainHeader.PLANE_SEPARATION, 
	     "Physical distance between adjacent planes (in cm.)."},
	    {strmName + "/" + Ecat7MainHeader.LWR_SCTR_THRES, 
	     "Lowest threshold setting for scatter (in KeV)."},
	    {strmName + "/" + Ecat7MainHeader.LWR_TRUE_THRES, 
	     "Lower threshold setting for trues in (in KeV)."},
	    {strmName + "/" + Ecat7MainHeader.UPR_TRUE_THRES, 
	     "Upper threshold setting for trues (in KeV)."},
	    {strmName + "/" + Ecat7MainHeader.USER_PROCESS_CODE, 
	     "Data processing code (defined by user)."},
	    {strmName + "/" + Ecat7MainHeader.ACQUISITION_MODE, 
	     "0=Normal, 1=Windowed, 2=Windowed & Nonwindowed, 3=Dual energy,\n"+
	     "4=Upper energy, 5=Emission and Transmission."},
	    {strmName + "/" + Ecat7MainHeader.BIN_SIZE, 
	     "Width of view sample (cm)."},
	    {strmName + "/" + Ecat7MainHeader.BRANCHING_FRACTION, 
	     "Fraction of decay by position emmision."},
	    {strmName + "/" + Ecat7MainHeader.DOSE_START_TIME, 
	     "Actual time radiopharmaceutical was injected or flow was " +
	     "started\n(in sec since base time)."},
	    {strmName + "/" + Ecat7MainHeader.DOSAGE, 
	     "Radiopharmaceutical dosage (in bequerels/cc) at time of " +
	     "injection."},
	    {strmName + "/" + Ecat7MainHeader.WELL_COUNTER_CORR_FACTOR, 
	     "Well counter corr factor."},
	    {strmName + "/" + Ecat7MainHeader.DATA_UNITS, 
	     "Free text field."},
	    {strmName + "/" + Ecat7MainHeader.SEPTA_STATE, 
	     "Septa position during scan (0=septa extended, 1=septa " +
	     "retracted)."},
	    {strmName + "/" + Ecat7MainHeader.FILL_CTI_0,
	     "CTI Reserved space 0."},
	    {strmName + "/" + Ecat7MainHeader.FILL_CTI_1,
	     "CTI Reserved space 1."},
	    {strmName + "/" + Ecat7MainHeader.FILL_CTI_2,
	     "CTI Reserved space 2."},
	    {strmName + "/" + Ecat7MainHeader.FILL_CTI_3,
	     "CTI Reserved space 3."},
	    {strmName + "/" + Ecat7MainHeader.FILL_CTI_4,
	     "CTI Reserved space 4."},
	    {strmName + "/" + Ecat7MainHeader.FILL_CTI_5,
	     "CTI Reserved space 5."},

	    {imageName + "/" + Ecat7ImageHeader.MATRIX_ID, "Matrix id."},
	    {imageName + "/" + Ecat7ImageHeader.DATA_TYPE,
	     "Enumerated type (0=Unknown Matrix Data Type, 1=Byte Data,\n" +
	     "2=VAX_Ix2, 3=VAX_Ix4, 4=VAX_Rx4, 5=IEEE Float, 6=Sun short,\n" +
	     "7=Sun long)."},
	    {imageName + "/" + Ecat7ImageHeader.NUM_DIMENSIONS,
	     "Number of dimensions."},
	    {imageName + "/" + Ecat7ImageHeader.X_DIMENSION,
	     "Dimension along x axis."},
	    {imageName + "/" + Ecat7ImageHeader.Y_DIMENSION,
	     "Dimension along y axis."},
	    {imageName + "/" + Ecat7ImageHeader.Z_DIMENSION,
	     "Dimension along z axis."},
	    {imageName + "/" + Ecat7ImageHeader.X_OFFSET,
	     "Offset in x axis for recon target (in cm)."},
	    {imageName + "/" + Ecat7ImageHeader.Y_OFFSET,
	     "Offset in y axis for recon target (in cm.)"},
	    {imageName + "/" + Ecat7ImageHeader.Z_OFFSET,
	     "Offset in z axis for recon target (in cm.)"},
	    {imageName + "/" + Ecat7ImageHeader.RECON_ZOOM,
	     "Reconstruction magnification factor (zoom)."},
	    {imageName + "/" + Ecat7ImageHeader.SCALE_FACTOR,
	     "Quantification scale factor (in Quant_units)."},
	    {imageName + "/" + Ecat7ImageHeader.IMAGE_MIN,
	     "Image minimum pixel value."},
	    {imageName + "/" + Ecat7ImageHeader.IMAGE_MAX,
	     "Image maximum pixel value."},
	    {imageName + "/" + Ecat7ImageHeader.X_PIXEL_SIZE,
	     "X dimension pixel size (cm)."},
	    {imageName + "/" + Ecat7ImageHeader.Y_PIXEL_SIZE,
	     "Y dimension pixel size (cm)."},
	    {imageName + "/" + Ecat7ImageHeader.Z_PIXEL_SIZE,
	     "Z dimension pixel size (cm)."},
	    {imageName + "/" + Ecat7ImageHeader.FRAME_DURATION,
	     "Total duration of current frame (in msec.)."},
	    {imageName + "/" + Ecat7ImageHeader.FRAME_START_TIME,
	     "Frame start time (offset from first frame, in msec)."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_CODE,
	     "0=all pass, 1=ramp, 2=Butterworth, 3=Hanning, 4=Hamming,\n" +
	     "5=Parzen, 6=Shepp, 7=Butterworthorder 2, 8=Gaussian, 9=Median,\n"+
	     "10=Boxcar."},
	    {imageName + "/" + Ecat7ImageHeader.X_RESOLUTION,
	     "Resolution in the x dimension (in cm)."},
	    {imageName + "/" + Ecat7ImageHeader.Y_RESOLUTION,
	     "Resolution in the y dimension (in cm)."},
	    {imageName + "/" + Ecat7ImageHeader.Z_RESOLUTION,
	     "Resolution in the z dimension (in cm)."},
	    {imageName + "/" + Ecat7ImageHeader.NUM_R_ELEMENTS,
	     "Number R elements from sinogram."},
	    {imageName + "/" + Ecat7ImageHeader.NUM_ANGLES,
	     "Number of angles from sinogram."},
	    {imageName + "/" + Ecat7ImageHeader.Z_ROTATION_ANGLE,
	     "Rotation in the xy plane (in degrees). Use righthand " +
	     "coordinate\n" +
	     "system for rotation angle sign."},
	    {imageName + "/" + Ecat7ImageHeader.DECAY_CORR_FCTR,
	     "Isotope decay compensation applied to data."},
	    {imageName + "/" + Ecat7ImageHeader.PROCESSING_CODE,
	     "Bit mask (0=Not Processed, 1=Normalized, 2=Measured " +
	     "Attenuation\n" +
	     "Correction, 4=Calculated Attenuation Correction, " +
	     "8=X smoothing,\n" +
	     "16=Y smoothing, 32=Z smoothing, 64=2D scatter correction,\n" +
	     "128=3D scatter correction, 256=Arc correction,\n" +
	     "512=Decay correction, 1024=Online compression,\n" +
	     "2048 Fourier Rebinning, 4096=Single Slice Rebinning,\n" +
	     "8192=Seg 0 Only, 16384=Randoms Smooth)."},
	    {imageName + "/" + Ecat7ImageHeader.GATE_DURATION,
	     "Gate duration (in msec)."},
	    {imageName + "/" + Ecat7ImageHeader.R_WAVE_OFFSET,
	     "R wave offset (For phase sliced studies, average, in msec)."},
	    {imageName + "/" + Ecat7ImageHeader.NUM_ACCEPTED_BEATS,
	     "Number of accepted beats for this gate."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_CUTOFF_FREQUENCY,
	     "Filter cutoff frequency."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_RESOLUTION,
	     "Filter resolution."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_RAMP_SLOPE,
	     "Filter ramp slope."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_ORDER,
	     "Filter order."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_SCATTER_FRACTION,
	     "Filter scatter fraction."},
	    {imageName + "/" + Ecat7ImageHeader.FILTER_SCATTER_SLOPE,
	     "Filter scatter slope."},
	    {imageName + "/" + Ecat7ImageHeader.ANNOTATION,
	     "Free format ASCII."},
	    {imageName + "/" + Ecat7ImageHeader.MT_1_1,
	     "Matrix transformation element (1,1)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_1_2,
	     "Matrix transformation element (1,2)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_1_3,
	     "Matrix transformation element (1,3)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_2_1,
	     "Matrix transformation element (2,1)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_2_2,
	     "Matrix transformation element (2,2)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_2_3,
	     "Matrix transformation element (2,3)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_3_1,
	     "Matrix transformation element (3,1)."},
	    {imageName + "/" + Ecat7ImageHeader.MT_3_2,
	     "Matrix transformation element (3,2)."},
	    {imageName + "/" +  Ecat7ImageHeader.MT_3_3,
	     "Matrix transformation element (3,3)."},
	    {imageName + "/" + Ecat7ImageHeader.RFILTER_CUTOFF,
	     "R filter cutoff."},
	    {imageName + "/" +  Ecat7ImageHeader.RFILTER_RESOLUTION,
	     "R filter resolution."},
	    {imageName + "/" + Ecat7ImageHeader.RFILTER_CODE,
	     "R filter code."},
	    {imageName + "/" + Ecat7ImageHeader.RFILTER_ORDER,
	     "R filter order."},
	    {imageName + "/" +  Ecat7ImageHeader.ZFILTER_CUTOFF,
	     "Z filter cutoff."},
	    {imageName + "/" + Ecat7ImageHeader.ZFILTER_RESOLUTION,
	     "Z filter resolution."},
	    {imageName + "/" + Ecat7ImageHeader.ZFILTER_CODE,
	     "Z filter code."},
	    {imageName + "/" + Ecat7ImageHeader.ZFILTER_ORDER,
	     "Z filter order."},
	    {imageName + "/" + Ecat7ImageHeader.MT_1_4,
	     "Matrix transformation element (1,4)."},
	    {imageName + "/" +  Ecat7ImageHeader.MT_2_4,
	     "Matrix transformation element (2,4)."},
	    {imageName + "/" +  Ecat7ImageHeader.MT_3_4,
	     "Matrix transformation element (3,4)."},
	    {imageName + "/" +  Ecat7ImageHeader.SCATTER_TYPE,
	     "0=None, 1=Deconvolution, 2=Simulated, 3=Dual Energy."},
	    {imageName + "/" +  Ecat7ImageHeader.RECON_TYPE,
	     "0=Filtered backprojection, 1=Forward projection 3D (PROMIS),\n" +
	     "2=Ramp 3D, 3=FAVOR 3D, 4=Single-slice Rebinning,\n" +
	     "5=Multi-slice rebinning, 6=FORE, 7=Basic OSEM, 8=CPU Base,\n" +
	     "9=CPU Fourier, 10=CPU FBP, 11=CPU terative, 12=CPU Fourier,\n" +
	     "13=CPU FBP, 14=CPU Iterative_SmallFOV."},
	    {imageName + "/" + Ecat7ImageHeader.RECON_VIEWS,
	     "Number of views used to reconstruct the data."},
	    {imageName + "/" + Ecat7ImageHeader.FILL_CTI,
	     "CTI Reserved space."},
	    {imageName + "/" + Ecat7ImageHeader.FILL_USER,
	     "User Reserved space.  Note: Usehighest bytes first."}
	};

	return contents;
    }
}
