/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package ucar.netcdf;

import java.io.IOException;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageInputStreamImpl;

/**
 * Modification of UCAR's RandomAccessFile class to handle Image Input Streams.
 *
 * @version 9 July 2002
 */
public class BufferedImageInputStream extends ImageInputStreamImpl
{
  /** The default buffer size, in bytes. */
  protected static final int defaultBufferSize = 4096;

  /** The underlying ImageInputStream. */
  protected ImageInputStream _inputStream;

  /** The buffer used to load the data. */
  protected byte buffer[];

  /**
   * The offset in bytes of the start of the buffer, from the start
   *  of the file.
   */
  protected long bufferStart;

  /**
   * The offset in bytes of the end of the data in the buffer, from
   *  the start of the file. This can be calculated from
   *  <code>bufferStart + dataSize</code>, but it is cached to speed
   *  up the read( ) method.
   */
  protected long dataEnd;

  /**
   * The size of the data stored in the buffer, in bytes. This may be
   * less than the size of the buffer.
   */
  protected int dataSize;

  /** True if we are at the end of the file. */
  protected boolean endOfFile;

  /** True if the data in the buffer has been modified. */
  boolean bufferModified = false;

  /**
   * Creates a new Buffered Image Input Stream.
   *
   * @param inputStream Image Input Stream to buffer.
   */
  public BufferedImageInputStream(ImageInputStream inputStream)
    {
      this(inputStream, defaultBufferSize);
    }

  /**
   * Creates a new Buffered Image Input Stream with the specified buffer size.
   *
   * @param inputStream Image Input Stream to buffer.
   * @param bufferSize Size of the temporary buffer, in bytes.
   */
  public BufferedImageInputStream(ImageInputStream inputStream,
				  int bufferSize)
    {
      _inputStream = inputStream;			   

      // Initialise the buffer;
      bufferStart = 0;
      dataEnd = 0;
      dataSize = 0;
      streamPos = 0;
      buffer = new byte[bufferSize];
      endOfFile = false;
    }

  /**
   * Close the file, and release any associated system resources.
   *
   * @exception IOException  if an I/O error occurrs.
   */
  public void close() throws IOException
    {
      super.close();

      _inputStream.close();
    }

  /**
   * Set the position in the file for the next read or write.
   *
   * @param pos  the offset (in bytes) from the start of the file.
   * @exception IOException  if an I/O error occurrs.
   */
  public void seek(long pos) throws IOException
    {
      // If the seek is into the buffer, just update the file pointer.
      if( pos >= bufferStart && pos < dataEnd ) {
	streamPos = pos;
	return;
      }

      // If the current buffer is modified, write it to disk.
      if( bufferModified )
	flush();

      // need new buffer
      bufferStart = pos;
      streamPos = pos;

      dataSize = read_( pos, buffer, 0, buffer.length);
      if( dataSize < 0 ) {
         dataSize = 0;
         endOfFile = true;
      } else {
         endOfFile = false;
      }

      // Cache the position of the buffer end.
      dataEnd = bufferStart + dataSize;
    }

  /**
   * Read a byte of data from the file, blocking until data is
   * available.
   *
   * @return the next byte of data, or -1 if the end of the file is reached.
   * @exception IOException  if an I/O error occurrs.
   */
  public final int read() throws IOException
    {
      // If the file position is within the data, return the byte...
      if( streamPos < dataEnd ) {
	return (int)(buffer[(int)(streamPos++ - bufferStart)] & 0xff);

	// ...or should we indicate EOF...
      } else if( endOfFile ) {
	return -1;

	// ...or seek to fill the buffer, and try again.
      } else {
	seek( streamPos );
	return read( );
      }
    }

  /**
   * Read up to <code>len</code> bytes into an array, at a specified
   * offset. This will block until at least one byte has been read.
   *
   * @param b    the byte array to receive the bytes.
   * @param off  the offset in the array where copying will start.
   * @param len  the number of bytes to copy.
   * @return the actual number of bytes read, or -1 if there is not
   *         more data due to the end of the file being reached.
   * @exception IOException  if an I/O error occurrs.
   */
  private int readBytes( byte b[], int off, int len ) throws IOException
    {
      // Check for end of file.
      if( endOfFile )
	return -1;

      // See how many bytes are available in the buffer - if none,
      // seek to the file position to update the buffer and try again.
      int bytesAvailable = (int)(dataEnd - streamPos);
      if( bytesAvailable < 1 ) {
         seek( streamPos );
         return readBytes( b, off, len );
      }

      // Copy as much as we can.
      int copyLength = (bytesAvailable >= len) ? len : bytesAvailable;
      System.arraycopy( buffer, (int)(streamPos - bufferStart),
                        b, off, copyLength );
      streamPos += copyLength;

      // If there is more to copy...
      if( copyLength < len ) {
         int extraCopy = len - copyLength;

         // If the amount remaining is more than a buffer's length, read it
         // directly from the file.
         if( extraCopy > buffer.length ) {
	   extraCopy = read_( streamPos, b, off + copyLength,
			      len - copyLength );

	   // ...or read a new buffer full, and copy as much as possible...
         } else {
	   seek( streamPos );
	   if( ! endOfFile ) {
	     extraCopy = (extraCopy > dataSize) ? dataSize : extraCopy;
	     System.arraycopy( buffer, 0, b, off + copyLength, extraCopy );
	   } else {
	     extraCopy = -1;
	   }
         }

         // If we did manage to copy any more, update the file position and
         // return the amount copied.
         if( extraCopy > 0 ) {
            streamPos += extraCopy;
            return copyLength + extraCopy;
         }
      }

      // Return the amount copied.
      return copyLength;
    }

  // read directly, without going through the buffer
  protected int read_(long pos, byte[] b, int offset, int len)
    throws IOException
    {
      _inputStream.seek(pos);
      return _inputStream.read(b, offset, len);
    }

  /**
   * Read up to <code>len</code> bytes into an array, at a specified
   * offset. This will block until at least one byte has been read.
   *
   * @param b    the byte array to receive the bytes.
   * @param off  the offset in the array where copying will start.
   * @param len  the number of bytes to copy.
   * @return the actual number of bytes read, or -1 if there is not
   *         more data due to the end of the file being reached.
   * @exception IOException  if an I/O error occurrs.
   */
  public int read( byte b[], int off, int len ) throws IOException
    {
      return readBytes( b, off, len );
    }
}

