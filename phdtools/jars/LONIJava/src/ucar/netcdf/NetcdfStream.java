/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package ucar.netcdf;

import ucar.multiarray.Accessor;
import ucar.multiarray.AbstractAccessor;
import ucar.multiarray.MultiArray;
import ucar.multiarray.MultiArrayImpl;
import ucar.multiarray.IndexIterator;
import ucar.multiarray.OffsetIndexIterator;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.Math;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

/**
 * Modification of UCAR's NetcdfFile class to handle Image Input and Output
 * Streams.
 *
 * @version 4 November 2004
 */
public class NetcdfStream extends AbstractNetcdf
{
  /** Image Input Stream to read encoded data from. */
  private ImageInputStream _inputStream;

  /** Image Output Stream to write encoded data to. */
  private ImageOutputStream _outputStream;

  private UnlimitedDimension recDim;
  private int recsize;
  private boolean doFill; // set to false to suppress data prefill.

  /** Netcdf file format version 1 "magic number". */
  private final static int v1magic = 0x43444601;

  /** Tag for signed 1 byte integer. */
  private final static int NC_BYTE = 1;

  /** Tag for ISO/ASCII characters. */
  private final static int NC_CHAR = 2;

  /** Tag for signed 2 byte integer. */
  private final static int NC_SHORT = 3;

  /** Tag for signed 4 byte integer. */
  private final static int NC_INT = 4;

  /** Tag for single precision floating point number. */
  private final static int NC_FLOAT = 5;

  /** Tag for double precision floating point number. */
  private final static int NC_DOUBLE = 6;

  /** Tag for array of netcdf Dimensions. */
  private final static int NC_DIMENSION = 10;

  /** Tag for array of netcdf Variables. */
  private final static int NC_VARIABLE = 11;

  /** Tag for array of netcdf attributes. */
  private final static int NC_ATTRIBUTE = 12;

  /** External representation aligns to 4 byte boundaries. */
  private final static int X_ALIGN = 4;

  /** External size of char.  Netcdf v1 file format uses 8 bit ASCII. */
  private final static int X_SIZEOF_CHAR = 1;

  /** External size of byte. */
  private final static int X_SIZEOF_BYTE = 1;

  /** External size of short. */
  private final static int X_SIZEOF_SHORT = 2;

  /** External size of int. */
  private final static int X_SIZEOF_INT = 4;

  /** External size of float. */
  private final static int X_SIZEOF_FLOAT = 4;

  /** External size of DOUBLE. */
  private final static int X_SIZEOF_DOUBLE = 8;

  /** Reserved name of Fill attribute. */
  private static final String _FillValue = "_FillValue";

  private static final byte NC_FILL_BYTE = -127;
  private static final byte NC_FILL_CHAR = 0;
  private static final short NC_FILL_SHORT = -32767;
  private static final int NC_FILL_INT = -2147483647;
  private static final float NC_FILL_FLOAT = 9.9692099683868690e+36F;
  private static final double NC_FILL_DOUBLE = 9.9692099683868690e+36;

  /**
   * Constructs a NetcdfStream that reads encoded data from the specified
   * Image Input Stream.
   *
   * @param inputStream Image Input Stream to read encoded data from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public NetcdfStream(ImageInputStream inputStream) throws IOException
    {
      super();

      _inputStream = new BufferedImageInputStream(inputStream);
      _outputStream = null;

      readV1(_inputStream);
      initRecSize();
      this.doFill = true;
    }

  /**
   * Constructs a NetcdfStream that writes encoded data to the specified
   * Image Output Stream.
   *
   * @param outputStream Image Output Stream to write encoded data to.
   * @param fill Variable prefill is suppressed if this is false; true o/w.
   * @param template Schema used as a construction template (may be empty).
   *
   * @throws IOException If an I/O error occurs.
   */
  public NetcdfStream(ImageOutputStream outputStream, boolean fill,
		      Schema template) throws IOException
    {
      super(new Schema(template), true);

      _inputStream = null;
      _outputStream = new BufferedImageOutputStream(outputStream);

      this.doFill = fill;
      compileBegins();
      initRecSize();
      writeV1();
      fillerup();
    }

  /**
   * Close this netcdf file.
   * The inquiry interface calls will continue to be available,
   * but I/O accesses will fail after this call.
   * @see RandomAccessFile#close
   */
  public void close() throws IOException
    {
      if (_inputStream != null) { _inputStream.close(); }
      if (_outputStream != null) { _outputStream.close(); }
    }

  /**
   * Flush anything written to disk.
   * @see RandomAccessFile#flush
   */
  public void flush() throws IOException
    {
      if (_inputStream != null) { _inputStream.flush(); }
      if (_outputStream != null) { _outputStream.flush(); }
    }

  /**
   * Sets the "fill mode" to the argument.
   * If true (the default), new storage is prefilled with the
   * appropriate fill value. Otherwise, this activity is suppressed
   * and the programmer should initialize all values.
   * @see #getFill
   * @param pleaseFill true to fill.
   */
  public synchronized void setFill(boolean pleaseFill)
    {
      doFill = pleaseFill;
    }

  /**
   * Get the current "fill mode".
   * @see #setFill
   * @return true iff we are prefilling new storage
   * with the appropriate fill value.
   */
  public final boolean getFill()
    {
      return doFill;
    }

    private int padsz(int xsz)
    {
      int rem = xsz % X_ALIGN;
      if(rem == 0)
	return 0;
      return (X_ALIGN - rem);
    }

    private int rndup(int xsz)
    {
      return (xsz + padsz(xsz));
    }

    private int xszofV1String(String str)
    {
      int xsz = X_SIZEOF_INT;
      xsz += rndup(str.length());
      return xsz;
    }

    private void writeV1String(String str) throws IOException
    {
      // Check for an Output Stream
      _checkOutputStream();

      int rndup = str.length() % X_ALIGN;
      if(rndup != 0)
	rndup = X_ALIGN - rndup;
      _outputStream.writeInt(str.length());
      _outputStream.writeBytes(str);
      while(rndup != 0) {
	_outputStream.writeByte(0);
	rndup--;
      }
    }

    private String readV1String(DataInput hgs, int size) throws IOException
    {
      int rndup = size % X_ALIGN;
      if(rndup != 0)
	rndup = X_ALIGN - rndup;
      byte [] bytes = new byte[size];
      hgs.readFully(bytes); // TODO: premature EOF detection provided?
      hgs.skipBytes(rndup);
      return new String(bytes).intern();
    }

    private String readV1String(DataInput hgs) throws IOException
    {
      return readV1String(hgs, hgs.readInt());
    }

    private void writeV1Bytes(byte [] bytes) throws IOException
    {
      // Check for an Output Stream
      _checkOutputStream();

      int rndup = bytes.length % X_ALIGN;
      if(rndup != 0)
	rndup = X_ALIGN - rndup;
      _outputStream.writeInt(bytes.length);
      _outputStream.write(bytes);
      while(rndup != 0) {
	_outputStream.writeByte(0);
	rndup--;
      }
    }

    private int v1TypeEncode(Class componentType)
    {
      if(componentType.isPrimitive()) {
	if(componentType.equals(Character.TYPE))
	  return NC_CHAR;
	if(componentType.equals(Byte.TYPE))
	  return NC_BYTE;
	if(componentType.equals(Short.TYPE))
	  return NC_SHORT;
	if(componentType.equals(Integer.TYPE))
	  return NC_INT;
	if(componentType.equals(Float.TYPE))
	  return NC_FLOAT;
	if(componentType.equals(Double.TYPE))
	  return NC_DOUBLE;
      }
      throw new IllegalArgumentException("Not a V1 type: " + componentType);
    }

    private Class v1TypeDecode(int v1type)
    {
      switch (v1type) {
      case NC_CHAR:
	return Character.TYPE;
      case NC_BYTE:
	return Byte.TYPE;
      case NC_SHORT:
	return Short.TYPE;
      case NC_INT:
	return Integer.TYPE;
      case NC_FLOAT:
	return Float.TYPE;
      case NC_DOUBLE:
	return Double.TYPE;
      }
      return null;
    }

    private int xszofElement(Class componentType)
    {
      if(componentType.equals(Short.TYPE))
	return 2;
      if(componentType.equals(Integer.TYPE))
	return 4;
      if(componentType.equals(Float.TYPE))
	return 4;
      if(componentType.equals(Double.TYPE))
	return 8;
      return 1;
    }

  /** Used to calculate V1Io.vsize and this.recsize. */
  private int initVsize(DimensionIterator ee, int xsz)
    {
      int size = 1;
      while ( ee.hasNext() ) {
	final Dimension dim = ee.next();
	if(dim instanceof UnlimitedDimension) {
	  continue;
	}
	// else
	size *= dim.getLength();
      }

      size *= xsz;
      return size; // N.B. not rounded up
    }

  private void writeV1(Dimension dim) throws IOException
    {
      writeV1String(dim.getName());
      if(dim instanceof UnlimitedDimension)
	_outputStream.writeInt(0);
      else
	_outputStream.writeInt(dim.getLength());
    }

  private int xszof(Dimension dim)
    {
      int xsz = xszofV1String(dim.getName());
      xsz += X_SIZEOF_INT;
      return xsz;
    }

  private void writeV1(Attribute attr) throws IOException
    {
      writeV1String(attr.getName());
      if(attr.isString())
	{
	  _outputStream.writeInt(NC_CHAR); // type
	  writeV1String((String)attr.getValue());
	  return;
	}
      // else
      final int v1type = v1TypeEncode(attr.getComponentType());
      _outputStream.writeInt(v1type); // type
      if(v1type == NC_CHAR)
	{
	  writeV1String((String)attr.getValue());
	  return;
	}
      if(v1type == NC_BYTE)
	{
	  writeV1Bytes((byte [])attr.getValue());
	  return;
	}
      // else
      final int length = Array.getLength(attr.getValue());
      _outputStream.writeInt(length); // nelems
      // TODO: invert so loop is inside switch?
      for(int ii = 0; ii < length; ii++) {
	switch (v1type) {
	case NC_SHORT:
	  _outputStream.writeShort(((short[])attr.getValue())[ii]);
	  if(length % 2 != 0)
	    _outputStream.writeShort(0); // pad to X_ALIGN
	  break;
	case NC_INT:
	  _outputStream.writeInt(((int[])attr.getValue())[ii]);
	  break;
	case NC_FLOAT:
	  _outputStream.writeFloat(((float[])attr.getValue())[ii]);
	  break;
	case NC_DOUBLE:
	  _outputStream.writeDouble(((double[])attr.getValue())[ii]);
	  break;
	}
      }
    }

  private int xszof(Attribute attr)
    {
      int xsz = xszofV1String(attr.getName());
      xsz += X_SIZEOF_INT; // tag
      if(attr.isString()) {
	return xsz + xszofV1String((String)attr.getValue());
      }
      // else
      xsz += X_SIZEOF_INT; // nelems
      final int v1type = v1TypeEncode(attr.getComponentType());
      final int length = Array.getLength(attr.getValue());
      switch (v1type) {
      case NC_BYTE:
      case NC_CHAR:
	xsz += rndup(length);
	break;
      case NC_SHORT:
	xsz += rndup(length * X_SIZEOF_SHORT);
	break;
      case NC_INT:
	xsz += length * X_SIZEOF_INT;
	break;
      case NC_FLOAT:
	xsz += length * X_SIZEOF_FLOAT;
	break;
      case NC_DOUBLE:
	xsz += length * X_SIZEOF_DOUBLE;
	break;
      }
      return xsz;
    }

  private Dimension [] readV1DimensionArray(DataInput hgs) throws IOException
    {
      final int numrecs = hgs.readInt();
      final int tag = hgs.readInt();
      if(tag != NC_DIMENSION && tag != 0)
	throw new IllegalArgumentException("Not a netcdf file (dimensions)");
      final int ndims = hgs.readInt();
      Dimension [] dimArray = new Dimension[ndims];
      for(int ii = 0; ii < ndims; ii++) {
	final String name = readV1String(hgs);
	final int length = hgs.readInt();
	if(length == 0) {
	  if(this.recDim != null)
	    throw new IllegalArgumentException(
					       "Multiple UnlimitedDimensions");
	  this.recDim = new UnlimitedDimension(name, numrecs);
	  dimArray[ii] = this.recDim;
	}
	else
	  dimArray[ii] = new Dimension(name, length);
      }
      return dimArray;
    }

  private void writeV1(DimensionSet ds) throws IOException
    {
      writeV1numrecs();
      final int size = ds.size();
      if(size != 0)
	_outputStream.writeInt(NC_DIMENSION);
      else
	_outputStream.writeInt(0); // bit for bit backward compat.
      _outputStream.writeInt(size);
      final DimensionIterator ee = ds.iterator();
      while(ee.hasNext()) {
	writeV1(ee.next());
      }
    }

  private int xszof(DimensionSet ds)
    {
      int xsz = X_SIZEOF_INT; // numrecs
      xsz += X_SIZEOF_INT; // tag
      xsz += X_SIZEOF_INT; // nelems
      final DimensionIterator ee = ds.iterator();
      while(ee.hasNext()) {
	xsz += xszof(ee.next());
      }
      return xsz;
    }

  private void writeV1numrecs() throws IOException
    {
      // Check for an Output Stream
      _checkOutputStream();

      if(recDim == null)
	_outputStream.writeInt(0);
      else
	_outputStream.writeInt(recDim.getLength());
    }

  private void writeV1(AttributeSet as) throws IOException
    {
      // Check for an Output Stream
      _checkOutputStream();

      final int size = as.size();
      if(size != 0)
	_outputStream.writeInt(NC_ATTRIBUTE);
      else
	_outputStream.writeInt(0); // bit for bit backward compat.
      _outputStream.writeInt(size);
      final AttributeIterator ee = as.iterator();
      while (ee.hasNext()) {
	writeV1(ee.next());
      }
    }

  private Object readV1AttrVal(DataInput hgs) throws IOException
    {
      final int v1type = hgs.readInt();
      final int nelems = hgs.readInt();

      switch (v1type) {
      case NC_CHAR:
	{
	  int rndup = nelems % X_ALIGN;
	  if(rndup != 0)
	    rndup = X_ALIGN - rndup;
	  char [] values = new char[nelems];
	  for(int ii = 0; ii < nelems; ii++)
	    values[ii] = (char) hgs.readUnsignedByte();
	  hgs.skipBytes(rndup);
	  return values;
	}
      case NC_BYTE:
	{
	  int rndup = nelems % X_ALIGN;
	  if(rndup != 0)
	    rndup = X_ALIGN - rndup;
	  byte [] values = new byte[nelems];
	  hgs.readFully(values); // TODO: premature EOF detection?
	  hgs.skipBytes(rndup);
	  return values;
	}
      case NC_SHORT:
	{
	  short [] values = new short[nelems];
	  for(int ii = 0; ii < nelems; ii++)
	    values[ii] = hgs.readShort();
	  if(nelems % 2 != 0)
	    hgs.skipBytes(2); // pad to X_ALIGN
	  return values;
	}
      case NC_INT:
	{
	  int [] values = new int[nelems];
	  for(int ii = 0; ii < nelems; ii++)
	    values[ii] = hgs.readInt();
	  return values;
	}
      case NC_FLOAT:
	{
	  float [] values = new float[nelems];
	  for(int ii = 0; ii < nelems; ii++)
	    values[ii] = hgs.readFloat();
	  return values;
	}
      case NC_DOUBLE:
	{
	  double [] values = new double[nelems];
	  for(int ii = 0; ii < nelems; ii++)
	    values[ii] = hgs.readDouble();
	  return values;
	}
      } // end switch
      /*NOTREACHED*/
      return null;
    }

  private Attribute [] readV1AttributeArray(DataInput hgs) throws IOException
    {
      final int tag = hgs.readInt();
      if(tag != NC_ATTRIBUTE && tag != 0)
	throw new IllegalArgumentException("Not a netcdf file (attributes)");
      final int nelems = hgs.readInt();
      Attribute [] attrArray = new Attribute[nelems];
      for(int ii = 0; ii < nelems; ii++) {
	final String name = readV1String(hgs);
	final Object value = readV1AttrVal(hgs);
	attrArray[ii] = new Attribute(name, value);
      }
      return attrArray;
    }

  private int xszof(AttributeSet as)
    {
      int xsz = X_SIZEOF_INT; // tag
      xsz += X_SIZEOF_INT; // nelems
      final AttributeIterator ee = as.iterator();
      while (ee.hasNext()) {
	xsz += xszof(ee.next());
      }
      return xsz;
    }

    abstract class V1Io extends AbstractAccessor
    {
      private final ProtoVariable meta; // sibling within the Variable.
      private final int [] lengths; // cache of meta.getLengths()
      byte [] fillbytes;
      int vsize;
      int begin;
      final boolean isUnlimited; // TODO factor this!!
      final int [] dsizes;
      int xsz; // TODO: Is this member needed?

      /*
       * This form of constructor used when creating.
       */
      protected V1Io(ProtoVariable proto)
	{
	  meta = proto;
	  lengths = proto.getLengths();
	  initFillValue(proto);
	  // TODO
	  this.vsize = rndup(initVsize(proto.getDimensionIterator(),
				      xszofElement(proto.getComponentType())));
	  this.begin = 0;
	  isUnlimited = proto.isUnlimited();
	  this.dsizes = compileDsizes(proto.getLengths());
	  this.xsz = xszofElement(proto.getComponentType());
        }

        abstract void readArray(long offset, Object dst, int dst_position,
				int nelems) throws IOException;

        private final int iocount(int [] origin, int [] shape)
        {
	  int product = 1;
	  int minIndex = 0;
	  if(isUnlimited)
	    minIndex = 1;
	  for(int ii = shape.length -1; ii >= minIndex; ii--)
	    {
	      final int si = shape[ii];
	      product *= si;
	      if(origin[ii] != 0 || si < lengths[ii] )
		break;
	    }
	  return product;
        }

        public MultiArray copyout(int [] origin, int [] shape)
	  throws IOException
	{
	  final int [] dimensions = (int []) shape.clone();
	  final int [] products = new int[dimensions.length];
	  final int product = MultiArrayImpl.numberOfElements(dimensions,
							      products);
	  final Object storage = Array.newInstance(meta.getComponentType(),
						   product);
	  final int contig = iocount(origin, shape);

	  // convert dimensions to limits
	  final int [] limits = (int []) dimensions.clone();
	  for(int ii = 0; ii < limits.length; ii++)
	    limits[ii] += origin[ii];

	  final OffsetIndexIterator odo = new OffsetIndexIterator(origin,
								  limits);
	  for(int begin = 0; odo.notDone(); odo.advance(contig),
		begin += contig)
	    {
	      final long offset = computeOffset(odo.value());
	      readArray(offset, storage, begin, contig);

	    }
	  return new MultiArrayImpl(dimensions, products, storage);
        }

      /*
       * @param data Array of byte which can be contiguously written.
       */
      abstract void writeArray(long offset, Object from, int begin, int nelems)
	throws IOException;

      public void copyin(int [] origin, MultiArray data) throws IOException
        {
	  /*
	   * The switch on subclass here is justified
	   * to make the specialized optimization available here
	   * without adding specializations to Accessor,
	   * AbstractAccessor, and Variable.
	   */
	  if(data instanceof MultiArrayImpl)
	    {
	      this.copyin(origin, (MultiArrayImpl) data);
	    }
	  else
	    {
	      // TODO checkfill
	      if(isUnlimited)
		checkfill(origin[0] + (data.getLengths())[0]);
	      super.copyin(origin, data); // AbstractAccessor.
	    }
        }

        public void copyin(int [] origin, MultiArrayImpl data) 
	  throws IOException
        {
	  final int [] dimensions = data.getLengths();
	  final int contig = iocount(origin, dimensions);
	  // convert dimensions to limits
	  for(int ii = 0; ii < dimensions.length; ii++)
	    dimensions[ii] += origin[ii];
	  if(isUnlimited)
	    checkfill(dimensions[0]);
	  final Object storage = data.storage;
	  final OffsetIndexIterator odo = new OffsetIndexIterator(origin,
								  dimensions);
	  for(int begin = 0; odo.notDone(); odo.advance(contig),
		begin += contig)
	    {
	      final long offset = computeOffset(odo.value());
	      writeArray(offset, storage, begin, contig);
	    }
        }

        public Object toArray() throws IOException
        {
	  return this.toArray(null, null, null);
        }

        public Object toArray(Object dst, int [] origin, int [] shape)
	  throws IOException
        {
	  final int rank = getRank();
	  if(origin == null)
	    origin = new int[rank];
	  else if(origin.length != rank)
	    throw new IllegalArgumentException("Rank Mismatch");

	  int [] shp = null;
	  if(shape == null)
	    shp = (int []) lengths.clone();
	  else if(shape.length == rank)
	    shp = (int []) shape.clone();
	  else
	    throw new IllegalArgumentException("Rank Mismatch");

	  final int product = MultiArrayImpl.numberOfElements(shp);
	  dst = MultiArrayImpl.fixDest(dst, product,
				       meta.getComponentType());
	  final int contig = iocount(origin, shp);

	  // convert dimensions to limits
	  final int [] limits = (int []) shp.clone();
	  for(int ii = 0; ii < limits.length; ii++)
	    limits[ii] += origin[ii];

	  final OffsetIndexIterator odo = new OffsetIndexIterator(origin,
								  limits);
	  for(int begin = 0; odo.notDone(); odo.advance(contig),
		begin += contig)
	    {
	      final long offset = computeOffset(odo.value());
	      readArray(offset, dst, begin, contig);
	    }

	  return dst;
        }

        private final int [] compileDsizes(int [] shape)
        {
	  final int [] ds = new int [shape.length];
	  int product = 1;
	  for(int ii = shape.length - 1; ii >= 0; ii--)
	    {
	      if(!(ii == 0 && isUnlimited))
		product *= shape[ii];
	      ds[ii] = product;
	    }
	  return ds;
        }

        public final void checkfill(int newLength) throws IOException
	{
	  // Check for an Output Stream
	  _checkOutputStream();

	  synchronized(recDim) {
	    int length = recDim.getLength();
	    if(newLength > length)
	      {
		if(doFill)
		  {
		    for(; length < newLength; length++)
		      {
			fillRec(length);
		      }
		  }
		recDim.setLength(newLength);
                                // TODO? allow caching? (NC_NSYNC)
		_outputStream.seek(((long)4)); // NC_NUMRECS_OFFSET
                                // writeV1numrecs();
		_outputStream.writeInt(recDim.getLength());
	      }
	  }
        }

        /* TODO */
      final int getRank() { return dsizes.length; }

      final boolean isScalar() { return 0 == getRank(); }

      final long computeOffset(int [] origin)
	{
	  if(isScalar())
	    return begin;
	  // else
	  if(getRank() == 1) {
	    if(isUnlimited) {
	      return (begin + origin[0] * recsize);
	    }
	    // else
	    return (begin + origin[0] * this.xsz);
	  }
	  // else
	  final int end = dsizes.length -1;
	  int lcoord = origin[end];
	  int index  = 0;
	  if(isUnlimited) {
	    index++;
	  }
	  for(; index < end ; index++) {
	    lcoord += dsizes[index +1] * origin[index];
	  }
	  lcoord *= this.xsz;
	  if(isUnlimited)
	    lcoord += origin[0] * recsize;
	  lcoord += begin;
	  return lcoord;
        }

      abstract void fill(DataOutput dos, int nbytes, Attribute fAttr)
	throws IOException;

      private final void initFillValue(Attribute fAttr)
	{
	  final int nbytes = 32; // tune here
	  ByteArrayOutputStream bos = new ByteArrayOutputStream(nbytes);
	  DataOutputStream dos = new DataOutputStream(bos);
	  try {
	    this.fill(dos, nbytes, fAttr);
	    dos.flush();
	  } catch (IOException ioe) {
	    // assert(cant happen);
	  }
	  fillbytes = bos.toByteArray();
        }

      private final void initFillValue(ProtoVariable proto)
        {
	  initFillValue(proto.getAttribute(_FillValue));
        }

      private final void initFillValue(Attribute [] attrArray)
        {
	  Attribute fAttr = null;
	  for(int ii = 0; ii < attrArray.length; ii++)
	    {
	      if(attrArray[ii].getName() == _FillValue)
		fAttr = attrArray[ii];
	    }
	  initFillValue(fAttr);
        }

      void fillO(long offset) throws IOException
	{
	  // Check for an Output Stream
	  _checkOutputStream();

	  _outputStream.seek(offset);
	  int remainder = vsize;
	  for(; remainder >= fillbytes.length;
	      remainder -= fillbytes.length)
	    _outputStream.write(fillbytes);
	  // handle any remainder;
	  if(remainder > 0)
	    for(int ii = 0; ii < remainder; ii++)
	      _outputStream.write(fillbytes[ii]);
        }

        void fill(int recno) throws IOException
	{
	  long offset = begin;
	  if(isUnlimited)
	    offset +=  (long)recno * recsize;
	  this.fillO(offset);
        }
    }

  private final class V1ByteIo extends V1Io
  {
    V1ByteIo(ProtoVariable var) {
      super(var);
    }

    void readArray(long offset, Object into, int begin, int nelems)
      throws IOException
      {
	final byte [] values = (byte []) into;
	_getInputStream().seek(offset);
	_getInputStream().read(values, begin, nelems);
      }

    public byte getByte(int [] index) throws IOException
      {
	_getInputStream().seek(computeOffset(index));
	return _getInputStream().readByte();
      }

    public Object get(int [] index) throws IOException
      {
	return new Byte(this.getByte(index));
      }

    void writeArray(long offset, Object from, int begin, int nelems)
      throws IOException
      {
	byte [] values = (byte []) from;
	_outputStream.seek(offset);
	_outputStream.write(values, begin, nelems);
      }

    public void setByte(int [] index, byte value) throws IOException
      {
	if(isUnlimited)
	  checkfill(index[0] +1);
	_outputStream.seek(computeOffset(index));
	_outputStream.writeByte(value);
      }

    public void set(int [] index, Object value) throws IOException
      {
	this.setByte(index, ((Number)value).byteValue());
      }

    final void fill(DataOutput dos, int nbytes, Attribute fAttr)
      throws IOException
      {
	byte fv = NC_FILL_BYTE;
	if(fAttr != null)
	  fv = fAttr.getNumericValue().byteValue();
	for(int ii = 0; ii < nbytes; ii++)
	  dos.write(fv);
      }
  }

  private final class V1CharacterIo extends V1Io
  {
    V1CharacterIo(ProtoVariable var)
      {
	super(var);
      }

    void readArray(long offset, Object into, int begin, int nelems)
      throws IOException
      {
	final char [] values = (char []) into;
	_getInputStream().seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  values[ii] = (char) _getInputStream().readUnsignedByte();
	}
      }

    public char getChar(int [] index) throws IOException
      {
	_getInputStream().seek(computeOffset(index));
	return (char) _getInputStream().readUnsignedByte();
      }

    public Object get(int [] index) throws IOException
      {
	return new Character(this.getChar(index));
      }

    void writeArray(long offset, Object from, int begin, int nelems)
      throws IOException
      {
	final char [] values = (char []) from;
	_outputStream.seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  _outputStream.writeByte((byte) values[ii]);
	}
      }

    public void setChar(int [] index, char value) throws IOException
      {
	if(isUnlimited)
	  checkfill(index[0] +1);
	_outputStream.seek(computeOffset(index));
	_outputStream.writeByte((byte) value);
      }

    public void set(int [] index, Object value) throws IOException
      {
	this.setChar(index, ((Character)value).charValue());
      }

    final void fill(DataOutput dos, int nbytes, Attribute fAttr)
      throws IOException
      {
	byte fv = NC_FILL_CHAR;
	if(fAttr != null)
	  fv = fAttr.getNumericValue().byteValue();
	for(int ii = 0; ii < nbytes; ii++)
	  dos.write(fv);
      }
  }

  private final class V1ShortIo extends V1Io
  {
    V1ShortIo(ProtoVariable var) {
      super(var);
    }

    void  readArray(long offset, Object into, int begin, int nelems)
      throws IOException
      {
	final short [] values = (short []) into;
	_getInputStream().seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  values[ii] = _getInputStream().readShort();
	}
      }

    public short getShort(int [] index) throws IOException
      {
	_getInputStream().seek(computeOffset(index));
	return _getInputStream().readShort();
      }

    public Object get(int [] index) throws IOException
      {
	return new Short(this.getShort(index));
      }

    void writeArray(long offset, Object from, int begin, int nelems)
      throws IOException
      {
	final short [] values = (short []) from;
	_outputStream.seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  _outputStream.writeShort(values[ii]);
	}
      }

    public void setShort(int [] index, short value) throws IOException
      {
	if(isUnlimited)
	  checkfill(index[0] +1);
	_outputStream.seek(computeOffset(index));
	_outputStream.writeShort(value);
      }

    public void set(int [] index, Object value) throws IOException
      {
	this.setShort(index, ((Number)value).shortValue());
      }

    final void fill(DataOutput dos, int nbytes, Attribute fAttr)
      throws IOException
      {
	short fv = NC_FILL_SHORT;
	if(fAttr != null)
	  fv = fAttr.getNumericValue().shortValue();
	for(int ii = 0; ii < nbytes; ii++)
	  dos.writeShort(fv);
      }
  }

  private final class V1IntegerIo extends V1Io
  {
    V1IntegerIo(ProtoVariable var) {
      super(var);
    }

    void readArray(long offset, Object into, int begin, int nelems)
      throws IOException
      {
	final int [] values = (int []) into;
	_getInputStream().seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  values[ii] = _getInputStream().readInt();
	}
      }

    public int getInt(int [] index) throws IOException
      {
	_getInputStream().seek(computeOffset(index));
	return _getInputStream().readInt();
      }

    public Object get(int [] index) throws IOException
      {
	return new Integer(this.getInt(index));
      }

    void writeArray(long offset, Object from, int begin, int nelems)
      throws IOException
      {
	final int [] values = (int []) from;
	_outputStream.seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  _outputStream.writeInt(values[ii]);
	}
      }

    public void setInt(int [] index, int value) throws IOException
      {
	if(isUnlimited)
	  checkfill(index[0] +1);
	_outputStream.seek(computeOffset(index));
	_outputStream.writeInt(value);
      }

    public void set(int [] index, Object value) throws IOException
      {
	this.setInt(index, ((Number)value).intValue());
      }

    final void fill(DataOutput dos, int nbytes, Attribute fAttr)
      throws IOException
      {
	int fv = NC_FILL_INT;
	if(fAttr != null)
	  fv = fAttr.getNumericValue().intValue();
	for(int ii = 0; ii < nbytes; ii++)
	  dos.writeInt(fv);
      }
  }

  private final class V1FloatIo extends V1Io
  {
    V1FloatIo(ProtoVariable var) {
      super(var);
    }

    void readArray(long offset, Object into, int begin, int nelems)
      throws IOException
      {
	float [] values = (float []) into;
	_getInputStream().seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  values[ii] = _getInputStream().readFloat();
	}
      }

    public float getFloat(int [] index) throws IOException
      {
	_getInputStream().seek(computeOffset(index));
	return _getInputStream().readFloat();
      }

    public Object get(int [] index) throws IOException
      {
	return new Float(this.getFloat(index));
      }

    void writeArray(long offset, Object from, int begin, int nelems)
      throws IOException
      {
	final float [] values = (float []) from;
	_outputStream.seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  _outputStream.writeFloat(values[ii]);
	}
      }

    public void setFloat(int [] index, float value) throws IOException
      {
	if(isUnlimited)
	  checkfill(index[0] +1);
	_outputStream.seek(computeOffset(index));
	_outputStream.writeFloat(value);
      }

    public void set(int [] index, Object value) throws IOException
      {
	this.setFloat(index, ((Number)value).floatValue());
      }

    final void fill(DataOutput dos, int nbytes, Attribute fAttr)
      throws IOException
      {
	float fv = NC_FILL_FLOAT;
	if(fAttr != null)
	  fv = fAttr.getNumericValue().floatValue();
	for(int ii = 0; ii < nbytes; ii++)
	  dos.writeFloat(fv);
      }
  }

  private final class V1DoubleIo extends V1Io
  {
    V1DoubleIo(ProtoVariable var) {
      super(var);
    }

    void readArray(long offset, Object into, int begin, int nelems)
      throws IOException
      {
	final double [] values = (double []) into;
	_getInputStream().seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  values[ii] = _getInputStream().readDouble();
	}
      }

    public double getDouble(int [] index) throws IOException
      {
	_getInputStream().seek(computeOffset(index));
	return _getInputStream().readDouble();
      }

    public Object get(int [] index) throws IOException
      {
	return new Double(this.getDouble(index));
      }

    void writeArray(long offset, Object from, int begin, int nelems)
      throws IOException
      {
	final double [] values = (double []) from;
	_outputStream.seek(offset);
	final int end = begin + nelems;
	for(int ii = begin; ii < end; ii++) {
	  _outputStream.writeDouble(values[ii]);
	}
      }

    public void setDouble(int [] index, double value) throws IOException
      {
	if(isUnlimited)
	  checkfill(index[0] +1);
	_outputStream.seek(computeOffset(index));
	_outputStream.writeDouble(value);
      }

    public void set(int [] index, Object value) throws IOException
      {
	this.setDouble(index, ((Number)value).doubleValue());
      }

    final void fill(DataOutput dos, int nbytes, Attribute fAttr)
      throws IOException
      {
	double fv = NC_FILL_DOUBLE;
	if(fAttr != null)
	  fv = fAttr.getNumericValue().doubleValue();
	for(int ii = 0; ii < nbytes; ii++)
	  dos.writeDouble(fv);
      }
  }

  private V1Io V1IoFactory(ProtoVariable proto)
    {
      final Class componentType = proto.getComponentType();
      V1Io io = null;

      if(componentType.equals(Character.TYPE)) {
	io =  new V1CharacterIo(proto);
      }
      else if (componentType.equals(Byte.TYPE)) {
	io =  new V1ByteIo(proto);
      }
      else if (componentType.equals(Short.TYPE)) {
	io =  new V1ShortIo(proto);
      }
      else if (componentType.equals(Integer.TYPE)) {
	io =  new V1IntegerIo(proto);
      }
      else if (componentType.equals(Float.TYPE)) {
	io =  new V1FloatIo(proto);
      }
      else if (componentType.equals(Double.TYPE)) {
	io =  new V1DoubleIo(proto);
      }

      return io;
    }

  protected Accessor ioFactory(ProtoVariable proto)
    {
      return V1IoFactory(proto);
    }

  private void writeV1(Variable var) throws IOException
    {
      writeV1String(var.getName());
      _outputStream.writeInt(var.getRank());

      DimensionIterator ee = var.getDimensionIterator();
      while(ee.hasNext())
	{
	  _outputStream.writeInt(indexOf(ee.next()));
	}

      writeV1(var.getAttributes());
      _outputStream.writeInt(v1TypeEncode(var.getComponentType()));
      final V1Io io = (V1Io) var.io;
      _outputStream.writeInt(io.vsize);
      _outputStream.writeInt(io.begin);
    }

  private int xszof(Variable var)
    {
      int xsz = xszofV1String(var.getName());
      xsz += X_SIZEOF_INT; // dimArray.length
      xsz += var.getRank() * X_SIZEOF_INT; // dim indexes
      xsz += xszof(var.getAttributes());
      xsz += X_SIZEOF_INT; // tag
      xsz += X_SIZEOF_INT; // vsize
      xsz += X_SIZEOF_INT; // begin
      return xsz;
    }

  private void readV1VarArray(DataInput hgs, Dimension [] allDims)
    throws IOException
    {
      int tag = hgs.readInt();
      if(tag != NC_VARIABLE && tag != 0)
	throw new IllegalArgumentException("Not a netcdf file (variables)");
      int nelems = hgs.readInt();
      for(int ii = 0; ii < nelems; ii++) {
	final String name = readV1String(hgs);
	final int ndims = hgs.readInt();
	final Dimension [] dimArray = new Dimension[ndims];
	for(int jj = 0; jj < ndims; jj++)
	  dimArray[jj] = allDims[hgs.readInt()];
	final Attribute [] attrArray =
	  readV1AttributeArray(hgs);
	final Class type =  v1TypeDecode(hgs.readInt());
	final ProtoVariable proto = new ProtoVariable(name, type, dimArray,
						      attrArray);

	final V1Io io = V1IoFactory(proto);
	io.vsize =  hgs.readInt();
	io.begin =  hgs.readInt();
			
	try {
	  add(proto, io);
	}
	catch (InstantiationException ie)
	  {
	    // Can't happen: Variable is concrete
	    throw new Error();
	  }
	catch (IllegalAccessException iae)
	  {
	    // Can't happen: Variable is accessable
	    throw new Error();
	  }
	catch (InvocationTargetException ite)
	  {
	    // all the possible target exceptions are RuntimeException
	    throw (RuntimeException)
	      ite.getTargetException();
	  }
      }
    }

  private void writeV1(int size, VariableIterator ee) throws IOException
    {
      // Check for an Output Stream
      _checkOutputStream();

      if(size != 0)
	_outputStream.writeInt(NC_VARIABLE);
      else
	_outputStream.writeInt(0); // bit for bit backward compat.
      _outputStream.writeInt(size);
      while( ee.hasNext()) {
	writeV1(ee.next());
      }
    }

  private int xszof(VariableIterator ee)
    {
      int xsz = X_SIZEOF_INT; // tag
      xsz += X_SIZEOF_INT; // nelems
      while(ee.hasNext()) {
	xsz += xszof(ee.next());
      }
      return xsz;
    }

  private void writeV1() throws IOException
    {
      // Check for an Output Stream
      _checkOutputStream();

      _outputStream.writeInt(v1magic);
      writeV1(getDimensions());
      writeV1(getAttributes());
      writeV1(size(), iterator());
    }

  private int xszof()
    {
      int xsz = X_SIZEOF_INT; // magic number
      xsz += xszof(getDimensions());
      xsz += xszof(getAttributes());
      xsz += xszof(iterator());
      return xsz;
    }

    private void readV1(DataInput hgs) throws IOException
    {
      final int magic = hgs.readInt();
      if(magic != v1magic)
	throw new IllegalArgumentException("Not a netcdf file");

      final Dimension [] dimArray = readV1DimensionArray(hgs);
      for(int ii = 0; ii < dimArray.length; ii++)
	putDimension(dimArray[ii]);

      {
	final Attribute [] gAttrArray = readV1AttributeArray(hgs);
	for(int ii = 0; ii < gAttrArray.length; ii++)
	  putAttribute(gAttrArray[ii]);
      }

      readV1VarArray(hgs, dimArray);
    }

  /**
   * In the C interface this is called NC_begins();
   */
  private void compileBegins()
    {
      int index = xszof();
      // loop thru vars, first pass is for the 'non-record' vars
      {
        final VariableIterator ee = iterator();
        while(ee.hasNext())
	  {
	    final Variable var = ee.next();
	    if(var.isUnlimited())
	      continue;
	    // else
	    final V1Io io = (V1Io) var.io;
	    io.begin = index;
	    index += io.vsize;
	  }
      }

      {
        // loop thru vars, 2nd pass is for the 'record' vars
        final VariableIterator ee = iterator();
        while(ee.hasNext())
	  {
	    final Variable var = ee.next();
	    if(!var.isUnlimited())
	      continue;
	    if(recDim == null)
	      {
		final Dimension dim0 =var.getDimensionIterator().next();
		if(!(dim0 instanceof UnlimitedDimension))
		  throw new IllegalArgumentException("Unlimited Dim not " +
						     "leftmost");
		recDim = (UnlimitedDimension)dim0;
	      }
	    final V1Io io = (V1Io) var.io;
	    io.begin = index;
	    index += io.vsize;
	  }
      }
    }

    private void initRecSize()
    {
      recsize = 0;
      // loop thru vars, 2nd pass is for the 'record' vars
      final VariableIterator ee = iterator();
      while(ee.hasNext())
        {
	  final Variable var = ee.next();
	  if(!var.isUnlimited())
	    continue;
	  final V1Io io = (V1Io) var.io;
	  if(recsize == 0 && !ee.hasNext())
	    {
	      // special case exactly one record variable
	      // pack value
	      recsize = initVsize(var.getDimensionIterator(), io.xsz);
	      break;
	    }
	  // else
	  recsize += io.vsize;
        }
    }

    // can't be private and still visible in inner class V1Var?
  void fillRec(int recno) throws IOException
    {
      // synchronized in caller
      // "only call when doFill set" checked in caller
      final VariableIterator ee = iterator();
      while(ee.hasNext())
        {
	  final Variable var = ee.next();
	  if(!var.isUnlimited())
	    continue;
	  // else
	  // var.fill(recno);
	  final V1Io io = (V1Io) var.io;
	  final long offset = (long)io.begin + (long)recno * recsize;
	  io.fillO(offset);
        }
    }

  private void fillerup() throws IOException
    {
      if(!this.doFill)
	return;
      final VariableIterator ee = iterator();
      while(ee.hasNext())
        {
	  final Variable var = ee.next();
	  if(var.isUnlimited())
	    continue;
	  // else
	  final V1Io io = (V1Io) var.io;
	  io.fillO((long)io.begin);
        }
      if(this.recDim != null) {
	final int nrecs = recDim.getLength();
	for(int recno = 0; recno < nrecs; recno++)
	  fillRec(recno);
      }
    }

  /**
   * Ensures that the close method of this file is called when
   * there are no more
   * references to it.
   * @exception Throwable The lack of covariance for exception specifications
   * dictates the specificed type;
   * it can actually only be <code>IOException</code> thrown
   * by <code>RandomAccessFile.close</code>.
   * @see NetcdfFile#close
   */
  protected void finalize() throws Throwable
    {
      super.finalize();
      close();
    }

  /**
   * Checks whether or not an Image Output Stream has been specified.
   *
   * @throws IOException If an Image Output Stream has not been specified.
   */
  private void _checkOutputStream() throws IOException
    {
      if (_outputStream == null) {
	String msg = "No Output Image Stream has been set for writing.";
	throw new IOException(msg);
      }
    }

  /**
   * Gets the Image Stream for reading data.
   *
   * @return Image Stream for reading data.
   */
  private ImageInputStream _getInputStream()
    {
      // Find the Image Stream that is not null
      if (_outputStream != null) { return _outputStream; }
      return _inputStream;
    }
}
