package ucar.netcdf;
/**
 * Title:        NetCDF v2 CompGeo Extension
 * Description:  This is Computational Geosciences, Inc.'s extension and modification of the UCAR NetCDF v2 library. Its major purpose is to extend the NetcdfFile class to handle in-memory representations of NetCDF version 1 files, instead of being tied to on-disk RandomAccessFile backings.
 * This project is based on (and composed mostly of) LGPL source code from UCAR/Unidata. As such, it must be released under the LGPL.
 *
 * Copyright:    Copyright (c) 2001 Computational Geosciences, Inc.
 * Company:      Computational Geosciences, Inc. <compgeo@compgeo.com>
 *
 * License:
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author Andrew Janke
 * @version $Revision: 1.1.1.1 $
 */

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileDescriptor;


/**
 * Class that adapts an in-memory byte array as a RandomAccessFile so
 * that NetCDF files can be read from core instead of being written out to
 * and read from temp files.
 *
 * This class is modeled on the HTTPRandomAccessFile class from UCAR.
 */
public class InMemRandomAccessFile extends RandomAccessFile {

	//------------------------------------------
	// Constants

	/** Limit superclass' buffer size */
	private static final int MAX_REDUNDANT_BUFFER_LEN = 50 * 1024; // 50K

	//------------------------------------------
	// Constructors and Initialization

	/**
	 * Creates new InMemRandomAccessFile from given byte array.
	 */
   public InMemRandomAccessFile(byte[] data) throws IOException {
		// Have to call superclass constructor first, so minimize size
		// of spuriously created buffer
		super(1);
		if (data == null) {
			throw new FileNotFoundException("Can't read from null array");
		}

		// Hold on to backing data
		backingBytes = data;

		// Here's the clever part... use backing data as superclass' buffer
		buffer = backingBytes;
		bufferStart = 0;
		dataSize = buffer.length;
		dataEnd = buffer.length;
   }

	//-------------------------------------------
	// Member methods

	/**
	 * Read directly from backing byte array, without going through ucar.netcdf.RandomAccessFile's
	 * buffer.
	 * Will read up to len bytes, or until the end of backing buffer is reached,
	 * or until the end of destination buffer is reached. Note that this behavior
	 * diverges from normal DataInput behavior, which will throw IndexOutOfBounds
	 * exceptions if you specify len that runs past the end of buf.
	 *
	 * This is something of a holdover from a previous implementation of this
	 * class where backingBytes was actually its own array and the superclass
	 * was allowed to buffer it as though it were a file. This method should
	 * never be called, because superclass' buffer should always be full
	 * @param pos long starting position of read
	 * @param buf byte[] array to read into
	 * @param off int offset into buff to start writing
	 * @param len int len maximum number of bytes to read
	 * @throws IOException if an error occurs, or if pos is past the end of the
	 * backing byte buffer
	 */
	protected int read_(long pos, byte[] buf, int off, int iLen) throws IOException {
		int len = iLen;
		int backing_len = backingBytes.length;
		int buf_len = buf.length;
		try {
			if (pos >= backing_len) {
				throw new IOException("Attempt to read past end of data array");
			}

			// Check read against length of source, resize read to not go past
			// end of source
			int max_avail = backing_len - (int) pos;
			if (max_avail < len) {
				len = max_avail;
			}

			// Check write against length of destination, resize read operation
			// to fit in destination buffer
			int max_read = buf_len - off;
			if (max_read < len) {
				len = max_read;
			}
//				throw new IOException("Request to write past end of destination buffer: "
//					+"pos="+pos+",off="+off+",len="+len+",backing_len="
//					+backing_len+",buf_len="+buf_len);

			// Copy data from backing array to output buffer
			System.arraycopy(backingBytes, (int) pos, buf, off, len );

			return len;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new ArrayIndexOutOfBoundsException(aioobe.toString()
				+": pos="+pos+",off="+off+",len="+len+",iLen="+iLen
				+",backing_len="+backing_len+",buf_len="+buf_len);
		}
	}

  //------
  // override the rest of the RandomAccessFile public methods


	/**
	 * Does nothing. At some point, this could null the reference to the backing
	 * byte array to allow it to be garbage collected, but not for now, since
	 * it avoids the need for null checks (backingBytes is never null).
	 */
	public void close() {
		// do nothing
	}

	/**
	 * Always returns null because there's no file actually attached to this
	 * @return FileDescriptor null
	 */
	public FileDescriptor getFD() {
	   return null;
	}

	/**
	 * Size of the byte array backing this file. Since the array cannot
	 * be written to, this will always be the same
	 * @return long size of byte array input comes from
	 */
	public long length() {
		return backingBytes.length;
	}

	//-------------------------------------------
	// Member data

	/** Byte array backing this file. This is the original source of all read data.
	 *  In current implementation, this should actually point to the same array
	 *  as super.buffer */
	private byte[] backingBytes;

}