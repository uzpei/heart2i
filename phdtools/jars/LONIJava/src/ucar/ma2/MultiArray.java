// $Id: MultiArray.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.ma2;

import java.io.IOException;

/**
 * Abstraction for multidimensional arrays of numeric values of any type of backing store
 * (memory, file, network).
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */
public interface MultiArray {
  /**
   * Get the type of the underlying data.
   * @return the Class object of the underlying data.
   */
  public Class getElementType();

  /**
   * Get the number of dimensions of the array.
   * @return int number of dimensions of the array
   */
  public int getRank();

  /**
   * Get the total number of elements in the array.
   * @return int total number of elements in the array
   */
  public long getSize();

   /**
   * Get the shape: length of array in each dimension.
   *
   * @return int array whose length is the rank of this
   * MultiArray and whose elements represent the length of each of its dimensions.
   */
  public int [] getShape();

  /**
   * Read the data, possibly from an IO or network device, and return a memory-resident
   * Array containing a section of the MultiArray.
   * The returned Array has the same element type as the MultiArray, and the requested
   * shape. Call Array.reduce() to eliminate dimensions with length = 1.
   * <p>
   * <code>assert(origin[ii] + shape[ii] <= MultiArray.shape[ii]);</code>
   * <p>
   * @param origin int array specifying the starting index.
   * @param shape  int array specifying the extents in each
   *	dimension. This becomes the shape of the returned Array.
   * @return the requested data in a memory-resident Array
   */
  public Array read(int [] origin, int [] shape) throws InvalidRangeException, IOException;

  /**
   * Read data, possibly from an IO or network device, and return a memory-resident Array
   * containing the MultiArray. The returned Array has the same element type and shape
   * as the MultiArray.
   * <p>
   * @return the requested data in a memory-resident Array
   */
  public Array read() throws IOException;

  /**
    * Create a new MultiArray as a "slice" of this MultiArray, which reduces the rank by one
    * by fixing the value of one of the dimensions at the given value.
    * @param which_dim : fix this dimension
    * @param index_value : at this value
    * @return the new MultiArray
    */
  public MultiArray sliceMA(int which_dim, int index_value);

  /**
    * Create a new MultiArray as a subsection of this MultiArray.
    * @param ranges array of Ranges that specify the array subset.
    *   Must be same rank as original Array.
    *   A particular Range: 1) may be a subset; 2) may be null, meaning use entire Range.
    *   If Range.length == 1, then the rank of the resulting Array is reduced.
    * @return the new MultiArray
    */
  //public MultiArray sectionMA( Range[] ranges) throws InvalidRangeException;

    /**
     * Create a new MultiArray by
     * flipping the index so that it runs from shape[index]-1 to 0.
     * @param dim dimension to flip
     * @return the new MultiArray
     * @exception IllegalArgumentException dim not valid
     */
 // public MultiArray flipMA( int dim);

    /**
     * Create a new MultiArray by eliminating any dimensions with length one.
     * @return the new MultiArray
     */
  //public MultiArray reduceMA();

    /**
     * Create a new MultiArray by transposing two of the indices.
     * @param dim1, dim2 transpose these two indices
     * @return the new MultiArray
     * @exception IllegalArgumentException dim1 or dim2 not valid
     */
  //public MultiArray transposeMA( int dim1, int dim2);
}
