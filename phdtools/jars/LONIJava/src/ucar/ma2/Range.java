/*
IBM SOFTWARE DISCLAIMER

Java array package (draft 0.2). Copyright (1998), International Business
Machines Corporation.

Permission to use, copy, modify and distribute this software for any
noncommercial purpose and without fee is hereby granted, provided that
this copyright and permission notice appear on all copies of the
software. The name of the IBM Corporation may not be used in any
advertising or publicity pertaining to the use of the software. IBM
makes no warranty or representations about the suitability of the
software for any purpose.  It is provided "AS IS" without any express
or implied warranty, including the implied warranties of
merchantability, fitness for a particular purpose and non-infringement.
IBM shall not be liable for any direct, indirect, special or
consequential damages resulting from the loss of use, data or projects,
whether in an action of contract or tort, arising out of or in
connection with the use or performance of this software.
*/

package ucar.ma2;

/**
   Represents a range of integers, used as an index set for arrays.
   <P>
   To extract array sections from arrays, it is necessary to define
   ranges of indices along axes of arrays. Range objects are used for
   that purpose.
   <P>
   Ranges are either monotonically increasing or monotonically decreasing.
   Elements must be nonnegative. Ranges can be empty.
*/

public final class Range {

    int n;
    int a;
    int d;

    /**
       Create a range with unit stride (+1 or -1).
       @param first	first value in range
       @param last	last value in range
       @exception InvalidRangeException elements must be nonnegative
    */
  public Range(int first, int last) throws InvalidRangeException {

    if (first < 0) throw new InvalidRangeException();
          if (last < 0) throw new InvalidRangeException();
          if (last < first) d = -1;
          else d = 1;

          a = first;
          n = Math.max((last-first)/d + 1, 1); // changed minimum value to 1 (jc 7/23)
        //System.out.println("Range "+first+" "+last+" "+n);
  }

    /**
       Create a range with a specified stride.
       @param first	first value in range
       @param last	last value in range
       @param step	stride between consecutive elements (positive or negative)
       @exception InvalidRangeException elements must be nonnegative
    */
  public Range(int first, int last, int step) throws InvalidRangeException {

          if (first < 0) throw new InvalidRangeException();
          if (last < 0) throw new InvalidRangeException();

          a = first;
          d = step;
          n = Math.max((last - first)/step + 1, 1); // changed minimum value to 1 (jc)
  }

  /**
   * Return the number of elements in a range.
   */
  public int length() { return n; }

    /**
       Return the i-th element of a range.
       @param i	index of the element
       @exception InvalidRangeElementException 0 <= i < length
    */
    public int element(int i) throws InvalidRangeException {
            if (i < 0) throw new InvalidRangeException();
            if (i >= n) throw new InvalidRangeException();
        return a + i*d;
    }

    /**
       Return the i-th element of a range, no check
       @param i	index of the element
    */
  protected int elementf(int i) { return a + i*d; }

  public int first() {	return a; }

  public int last() { return a + (n-1)*d; }

  public int step() { return d; }

  public int min() {
    if (n > 0) {
            if (d > 0) return a;
            else return a+(n-1)*d;
          } else {
            return a;
          }
  }

  public int max() {
    if (n > 0) {
            if (d > 0) return a+(n-1)*d;
            else return a;
          } else {
            if (d > 0) return a-1;
            else return a+1;
          }
  }
}


