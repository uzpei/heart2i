// $Id: MultiArrayAdapter.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.ma2;
import java.io.IOException;

/**
 * An adapter for slicing MultiArrays
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */
public class MultiArrayAdapter implements MultiArray {
  private MultiArray ma;
  private int [] aShape, aOrigin;
  private int [] shape, origin;
  private int rank;
  private int dim, value;
  private long size;

  public MultiArrayAdapter(MultiArray ma, int dim, int value) {
    this.ma = ma;
    this.dim = dim;
    this.value = value;

    //error checking
    int [] org_shape = ma.getShape();
    if ((dim < 0 ) || (dim >= ma.getRank()))
      throw new IllegalArgumentException("MultiArrayAdapter: bad dim "+dim);
    if ((value < 0 ) || (value >= org_shape[dim]))
      throw new IllegalArgumentException("MultiArrayAdapter: bad index value "+value);

    // compute new shape
    rank = ma.getRank()-1;
    shape = new int[rank];
    int count = 0;
    for (int i=0; i < rank+1; i++) {
      if (i != dim)
        shape[count++] = org_shape[i];
    }
    size = IndexImpl.computeSize(shape);

    // need this in read()
    origin = new int[rank]; // inited to zeroes
    aShape = new int[rank+1];
    aOrigin = new int[rank+1];
  }

  public Class getElementType() { return ma.getElementType(); }
  public int getRank() { return rank; }
  public long getSize() { return size; }
  public int [] getShape() { return shape; }

  public Array read(int [] origin, int [] shape) throws InvalidRangeException, IOException {
    int count = 0;
    for (int i=0; i < rank+1; i++) {
      aShape[i] = (i == dim) ? 1 : shape[count];
      aOrigin[i] = (i == dim) ? value : origin[count];
      if (i != dim)
        count++;
    }
    return ma.read( aOrigin, aShape).reduce(dim);
  }

  public Array read() throws IOException {
    try {
      return read(origin, shape);
    } catch (InvalidRangeException e) {
      throw new IllegalArgumentException();
    }
  }

  public MultiArray sliceMA(int which_dim, int index_value) {
    return new MultiArrayAdapter(this, which_dim, index_value);
  }

}