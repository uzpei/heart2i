// $Id: ArrayAbstract.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.ma2;

/**
 * Superclass for our implementations of Array.
 * Data storage is with 1D java arrays.
 *
 * @see Array
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */
public abstract class ArrayAbstract implements Array, Cloneable {

/* implementation notes.
   section:
     String rep eg : (1:3,*,5) would be eacy to do as Ranges[] parseRanges(String s);
     Also could create interface for Ranges, ScatterIndex and pass array of that (?)
 */

  /** generate new Array with given type and shape and zeroed storage.
   * @param type eg double.class.
   * @param shape shape of the array.
   * @return new Array<type> or Array<type>.D<rank> if 0 <= rank <= 7.
   * @exception runtime UnsupportedOperationException unsupported type
   */
  static public ArrayAbstract factory( Class type, int [] shape) {
    IndexImpl index = IndexImpl.factory(shape);
    return factory( type, index);
  }

  /* generate new Array with given type and shape and zeroed storage */
  static private ArrayAbstract factory( Class type, IndexImpl index) {
    if (type == double.class)
      return ArrayDouble.factory(index);
    else if (type == float.class)
      return ArrayFloat.factory(index);
    else if (type == long.class)
      return ArrayLong.factory(index);
    else if (type == int.class)
      return ArrayInt.factory(index);
    else if (type == short.class)
      return ArrayShort.factory(index);
    else if (type == byte.class)
      return ArrayByte.factory(index);
    else if (type == char.class)
      return ArrayChar.factory(index);
    else if (type == boolean.class)
      return ArrayBoolean.factory(index);
    else
      throw new UnsupportedOperationException("unsupported type = "+ type);
  }

  /** Generate new Array with given type, shape, storage.
   * This should be package private, but is exposed for now for ucar.nc2.Variable.
   * storage must be 1D array of correct type
   * storage.length must equal product of shapes
   * storage data needs to be in canonical order
   *
   * @param type element type
   * @param shape array shape
   * @param storage 1D java array of correct type
   * @exception runtime IllegalArgumentException storage.length != product of shapes
   * @exception runtime ClassCastException wrong storage type
   * @exception runtime UnsupportedOperationException unsupported type
   */
   static public ArrayAbstract factory( Class type, int [] shape, Object storage) {
    IndexImpl indexCalc = IndexImpl.factory(shape);

    if (type == double.class)
      return ArrayDouble.factory(indexCalc, (double []) storage);
    else if (type == float.class)
      return ArrayFloat.factory(indexCalc, (float []) storage);
    else if (type == long.class)
      return ArrayLong.factory(indexCalc, (long []) storage);
    else if (type == int.class)
      return ArrayInt.factory(indexCalc, (int []) storage);
    else if (type == short.class)
      return ArrayShort.factory(indexCalc, (short []) storage);
    else if (type == byte.class)
      return ArrayByte.factory(indexCalc, (byte []) storage);
    else if (type == char.class)
      return ArrayChar.factory(indexCalc, (char []) storage);
    else if (type == boolean.class)
      return ArrayBoolean.factory(indexCalc, (boolean []) storage);
    else
      throw new UnsupportedOperationException();
  }

  /** Generate a new Array from a java multidimensional array of primitives.
   *  This makes a COPY of the data values of javaArray.
   *
   * @param Object javaArray: java array of primitive type
   * @exception runtime IllegalArgumentException: not an array
   * @exception runtime ClassCastException: not a primitive type
   * @exception runtime UnsupportedOperationException: not a primitive array
   */
  static public ArrayAbstract factory( Object javaArray) {
      // get the rank and type
    int rank_ = 0;
    Class componentType = javaArray.getClass();
    while(componentType.isArray()) {
      rank_++;
      componentType = componentType.getComponentType();
    }
    if( rank_ == 0)
      throw new IllegalArgumentException("ArrayAbstract.factory: not an array");
    if( !componentType.isPrimitive())
      throw new UnsupportedOperationException("ArrayAbstract.factory: not a primitive array");

      // get the shape
    int count = 0;
    int [] shape = new int[rank_];
    Object jArray = javaArray;
    Class cType = jArray.getClass();
    while(cType.isArray()) {
      shape[ count++] = java.lang.reflect.Array.getLength(jArray);
      jArray = java.lang.reflect.Array.get(jArray, 0);
      cType = jArray.getClass();
    }

    // create the Array
    ArrayAbstract aa = factory( componentType, shape);

    // copy the original array
    IndexIterator aaIter = aa.getIndexIterator();
    reflectArrayCopyIn( javaArray, aa, aaIter);

    return aa;
  }

  static private void reflectArrayCopyIn(Object jArray, ArrayAbstract aa, IndexIterator aaIter) {
    Class cType = jArray.getClass().getComponentType();
    if (cType.isPrimitive()) {
      aa.copyFrom1DJavaArray( aaIter, jArray);  // subclass does type-specific copy
    } else {
      for (int i=0; i< java.lang.reflect.Array.getLength(jArray); i++)  // recurse
        reflectArrayCopyIn(java.lang.reflect.Array.get(jArray, i), aa, aaIter);
    }
  }

  static private void reflectArrayCopyOut(Object jArray, ArrayAbstract aa, IndexIterator aaIter) {
    Class cType = jArray.getClass().getComponentType();
    if (cType.isPrimitive()) {
      aa.copyTo1DJavaArray( aaIter, jArray);  // subclass does type-specific copy
    } else {
      for (int i=0; i< java.lang.reflect.Array.getLength(jArray); i++)  // recurse
        reflectArrayCopyOut(java.lang.reflect.Array.get(jArray, i), aa, aaIter);
    }
  }

  /////////////////////////////////////////////////////
  protected final IndexImpl indexCalc;
  protected final int rank;

  /** for subclasses only */
  protected ArrayAbstract(int [] shape) {
    rank = shape.length;
    indexCalc = IndexImpl.factory(shape);
  }

  protected ArrayAbstract(IndexImpl index) {
    rank = index.getRank();
    indexCalc = index;
  }

  public Index getIndex() { return (Index) indexCalc.clone(); }
  public IndexIterator getIndexIterator() { return indexCalc.getIndexIterator(this); }
  public int getRank() { return rank;}
  public int [] getShape() { return indexCalc.getShape(); }
  public long getSize() { return indexCalc.getSize(); }

  /** Get an index iterator for traversing a section of the array in canonical order.
   * This is equivalent to Array.section(ranges).getIterator();
   * @param ranges array of Ranges that specify the array subset.
   *   Must be same rank as original Array.
   *   A particular Range: 1) may be a subset, or 2) may be null, meaning use entire Range.
   *
   * @return an IndexIterator over the named range.
   */
  public IndexIterator getRangeIterator(Range[] ranges) throws InvalidRangeException {
    return section(ranges).getIndexIterator();
  }

  /** Get a fast index iterator for traversing the array in arbitrary order.
   * @see IndexIterator
   */
  public IndexIterator getIndexIteratorFast() {
    return indexCalc.getIndexIteratorFast(this);
  }

 /** @return the element type of this Array */
  abstract public Class getElementType();

  /** create new Array with given indexImpl and the same (or copy of) backing store
   * @param index defines the shape
   * @param copyStorage false: use the same backing storage; true: use a copy of the backing store
   */
  abstract ArrayAbstract createView( IndexImpl index);

 /** used by copyTo1DJavaArray. made public for DODS efficiency */
  abstract public Object getStorage();

 /** used to create Array from java array */
  abstract void copyFrom1DJavaArray(IndexIterator iter, Object javaArray);
  abstract void copyTo1DJavaArray(IndexIterator iter, Object javaArray);

   /**
    * Create a new Array as a subsection of this Array, with rank reduction.
    * No data is moved, so the new Array references the same backing store as the original.
    * @param ranges array of Ranges that specify the array subset.
    *   Must be same rank as original Array.
    *   A particular Range: 1) may be a subset, or 2) may be null, meaning use entire Range.
    *   If Range[dim].length == 1, then the rank of the resulting Array is reduced at that dimension.
    * @return the new Array
    */
  public Array section( Range[] ranges) throws InvalidRangeException {
    return createView( indexCalc.section(ranges));
  }

   /**
    * Create a new Array as a subsection of this Array, with rank reduction.
    * No data is moved, so the new Array references the same backing store as the original.
    * <p>
    * @param origin int array specifying the starting index. Must be same rank as original Array.
    * @param shape  int array specifying the extents in each dimension.
    *	dimension. This becomes the shape of the returned Array. Must be same rank as original Array.
    *   If shape[dim] == 1, then the rank of the resulting Array is reduced at that dimension.
    * @return the new Array
    */
  public Array section( int [] origin, int [] shape) throws InvalidRangeException {
    Range[] ranges = new Range[ origin.length];
    for (int i=0; i<origin.length; i++)
      ranges[i] = new Range(origin[i], origin[i]+shape[i]-1);
    return createView( indexCalc.section(ranges));
  }

   /**
    * Create a new Array as a subsection of this Array, without rank reduction.
    * No data is moved, so the new Array references the same backing store as the original.
    * @param ranges array of Ranges that specify the array subset.
    *   Must be same rank as original Array.
    *   A particular Range: 1) may be a subset, or 2) may be null, meaning use entire Range.
    * @return the new Array
    */
  public Array sectionNoReduce( Range[] ranges) throws InvalidRangeException {
    return createView( indexCalc.sectionNoReduce(ranges));
  }

   /**
    * Create a new Array as a subsection of this Array, without rank reduction.
    * No data is moved, so the new Array references the same backing store as the original.
    * @param origin int array specifying the starting index. Must be same rank as original Array.
    * @param shape  int array specifying the extents in each dimension.
    *	dimension. This becomes the shape of the returned Array. Must be same rank as original Array.
    * @return the new Array
    */
  public Array sectionNoReduce( int [] origin, int [] shape) throws InvalidRangeException {
    Range[] ranges = new Range[ origin.length];
    for (int i=0; i<origin.length; i++)
      ranges[i] = new Range(origin[i], origin[i]+shape[i]-1);
    return createView( indexCalc.sectionNoReduce(ranges));
  }

  public Array slice(int dim, int value) {
    int[] origin = new int[rank];
    int[] shape = getShape();
    origin[dim] = value;
    shape[dim] = 1;
    try {
      return sectionNoReduce(origin, shape).reduce(dim);  // preserve other dim 1
    } catch (InvalidRangeException e) {
      throw new IllegalArgumentException();
    }
  }

  /*
    * Create a new Array as a subsection of this Array, making a copy of the data.
    * The data is placed into "canonical order" (last index varies fastest) in the new
    * Array.
    * @param ranges array of Ranges that specify the array subset.
    *   Must be same rank as original Array.
    *   A particular Range: 1) may be a subset; 2) may be null, meaning use entire Range.
    *   If Range.length == 1, then the rank of the resulting Array is reduced.
    * @return the new Array
    *
    public Array sectionCopy( Range[] ranges) throws InvalidRangeException {
        // get section into same backing store
      ArrayAbstract secOrg = createView( indexCalc.section(ranges));
        // generate new array
      ArrayAbstract secCopy = ArrayAbstract.factory( getElementType(), IndexImpl.factory( secOrg.getShape()));
        //copy data in canonical order
      MAMath.copy(secCopy, secOrg);
        // return copy
      return secCopy;
    } */

   /**
    * Create a copy of this Array, copying the data so that physical order is the same as
    * logical order
    * @return the new Array
    */
    public Array copy() {
      ArrayAbstract newA = factory( getElementType(), getShape());
      MAMath.copy(newA, this);
      return newA;
    }

   /**
    * Copy this array to a 1D Java primitive array of type getElementType(), with the physical order
    * of the result the same as logical order.
    * @return a Java 1D array of type getElementType().
    */
  public Object copyTo1DJavaArray() {
    ArrayAbstract newA = (ArrayAbstract) copy();
    return newA.getStorage();
  }


   /**
    * Copy this array to a n-Dimensioanl Java primitive array of type getElementType()
    * and rank getRank(). Makes a copy of the data.
    * @return a Java ND array of type getElementType().
    */
  public Object copyToNDJavaArray() {
    Object javaArray;
    try {
      javaArray = java.lang.reflect.Array.newInstance(getElementType(), getShape());
    } catch (Exception e) {
      throw new IllegalArgumentException();
    } // cant happen

    // copy data
    IndexIterator iter = getIndexIterator();
    reflectArrayCopyOut( javaArray, this, iter);

    return javaArray;
  }

 /**
  * Create a new Array using same backing store as this Array, by
  * flipping the index so that it runs from shape[index]-1 to 0.
  * @param dim dimension to flip
  * @return the new Array
  */
  public Array flip( int dim) {
    return createView( indexCalc.flip(dim));
  }

  /**
  * Create a new Array using same backing store as this Array, by
  * transposing two of the indices.
  * @param dim1, dim2 transpose these two indices
  * @return the new Array
  */
  public Array transpose( int dim1, int dim2)  {
    return createView( indexCalc.transpose(dim1, dim2));
  }

   /**
     * Create a new Array using same backing store as this Array, by
     * permuting the indices.
     * @param dims: the old index dims[k] becomes the new kth index.
     * @return the new Array
     * @exception IllegalArgumentException: wrong rank or dim[k] not valid
     */
  public Array permute( int[] dims)  {
    return createView( indexCalc.permute(dims));
  }

  /**
   * Create a new Array by copying this Array to a new one with given shape
   * @param shape the new shape
   * @return the new Array
   * @exception IllegalArgumentException a and b are not conformable
   */
  public Array reshape( int [] shape) {
    ArrayAbstract result = factory( this.getElementType(), shape);
    MAMath.copy( result, this);
    return result;
  }

  /**
     * Create a new Array using same backing store as this Array, by
     * eliminating any dimensions with length one.
     * @return the new Array
     */
  public Array reduce() {
    return createView( indexCalc.reduce());
  }

  /**
    * Create a new Array using same backing store as this Array, by
    * eliminating the specified dimension.
    * @param dim: dimension to eliminate: must be of length one, else IllegalArgumentException
    * @return the new Array
    */
  public Array reduce(int dim) {
    return createView( indexCalc.reduce(dim));
  }

  /** This is present so that Array is-a MultiArray: equivalent to sectionNoReduce(). */
  public Array read(int [] origin, int [] shape) throws InvalidRangeException {
    return sectionNoReduce(origin, shape);
  }
  /** This is present so that Array is-a MultiArray: returns itself. */
  public Array read() { return this; }

      // this is irritating that the corresponding method that returns Array cant be used directly.
  public MultiArray sectionMA( Range[] ranges) throws InvalidRangeException {
    return sectionNoReduce( ranges);
  }
  public MultiArray flipMA( int dim) { return flip(dim); }
  public MultiArray reduceMA() { return reduce(); }
  public MultiArray sliceMA(int dim, int val) { return slice(dim, val); }
  public MultiArray transposeMA( int dim1, int dim2) { return transpose( dim1, dim2); }

    /**
     * Get the array element at index as an Object.
     * The returned value is wrapped in an object, eg Double for double
     * @param index element index
     * @return Object value at <code>index</code>
     * @exception ArrayIndexOutOfBoundsException if index incorrect rank or out of bounds
     */
    abstract public Object getObject(Index ima);

    /**
     * Set the array element at index to the specified value.
     * the value must be passed wrapped in the appropriate Object (eg Double for double)
     * @param ima Index with current element set
     * @param value the new value.
     * @exception ArrayIndexOutOfBoundsException if index incorrect rank or out of bounds
     * @exception ClassCastException if Object is incorrect type
     */
    abstract public void setObject(Index ima, Object value);

  /** package private */
  //// these are for optimized access with no need to check index values
  //// elem is the index into the backing data
  abstract double getDouble(int elem);
  abstract void setDouble(int elem, double val);
  abstract float getFloat(int elem);
  abstract void setFloat(int elem, float val);
  abstract long getLong(int elem);
  abstract void setLong(int elem, long value);
  abstract int getInt(int elem);
  abstract void setInt(int elem, int value);
  abstract short getShort(int elem);
  abstract void setShort(int elem, short value);
  abstract byte getByte(int elem);
  abstract void setByte(int elem, byte value);
  abstract char getChar(int elem);
  abstract void setChar(int elem, char value);
  abstract boolean getBoolean(int elem);
  abstract void setBoolean(int elem, boolean value);
}
