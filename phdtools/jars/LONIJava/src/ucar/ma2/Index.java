// $Id: Index.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
 */
package ucar.ma2;

/**
 * Indexes for Multidimensional arrays.
 *
 * This is a generalization of index as int []. Its main function is
 * to do the index arithmetic to translate an n-dim index into a 1-dim
 * index.
 * The user obtains this by calling getIndex() on a Array.
 * The set() and seti() routines are convenience routines for 1-7 dim arrays.
 *
 * @see Array
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */

 public interface Index {

  /** Get the number of dimensions in the array. */
  public int getRank();

  /** Get the shape: length of array in each dimension. */
  public int [] getShape();

  /** Get the current element's index as an int []
  public int [] getCurrentIndex(); */

  /** Use index[] to calculate the index into the 1D backing array.
   * Does not set the current element.
   * @exception runtime ArrayIndexOutOfBoundsException if index is out of bounds
   */
  //public int element(int [] index);

  /** Get the total number of elements in the array. */
  public long getSize();

  /** Get the current element's index into the 1D backing array. */
  public int currentElement();

  /** Set the current element's index. General-rank case.
   * @return this, so you can use A.get(i.set(i))
   * @exception runtime ArrayIndexOutOfBoundsException if index is out of bounds
   */
  public Index set(int[] index);

  /** Set the current element at dimension dim to v.
   * @exception runtime ArrayIndexOutOfBoundsException if index is out of bounds
   */
  public void setDim(int dim, int v);

    /* the following methods are optimizations for rank 1-7 arrays
     * All will throw runtime ArrayIndexOutOfBoundsException if index is out of bounds
     */

    /** set current index at dimension 0 to v 
      @return this, so you can use A.get(i.set0(i)) */
  public Index set0(int v);
    /** set current index at dimension 1 to v 
      @return this, so you can use A.get(i.set1(i)) */
  public Index set1(int v);
    /** set current index at dimension 2 to v 
      @return this, so you can use A.get(i.set2(i)) */
  public Index set2(int v);
    /** set current index at dimension 3 to v 
      @return this, so you can use A.get(i.set3(i)) */
  public Index set3(int v);
    /** set current index at dimension 4 to v 
      @return this, so you can use A.get(i.set4(i)) */
  public Index set4(int v);
    /** set current index at dimension 5 to v 
      @return this, so you can use A.get(i.set5(i)) */
  public Index set5(int v);
    /** set current index at dimension 6 to v 
      @return this, so you can use A.get(i.set6(i)) */
  public Index set6(int v);

  /** set current index at dimension 0 to v0
      @return this, so you can use A.get(i.set(i)) */
  public Index set(int v0);
  /** set current index at dimension 0,1 to v0,v1
      @return this, so you can use A.get(i.set(i,j)) */
  public Index set(int v0, int v1);
  /** set current index at dimension 0,1,2 to v0,v1,v2
      @return this, so you can use A.get(i.set(i,j,k)) */
  public Index set(int v0, int v1, int v2);
  /** set current index at dimension 0,1,2,3 to v0,v1,v2,v3
      @return this, so you can use A.get(i.set(i,j,k,l)) */
  public Index set(int v0, int v1, int v2, int v3);
  /** set current index at dimension 0,1,2,3,4 to v0,v1,v2,v3,v4
      @return this, so you can use A.get(i.set(i,j,k,l,m)) */
  public Index set(int v0, int v1, int v2, int v3, int v4);
  /** set current index at dimension 0,1,2,3,4,5 to v0,v1,v2,v4,v4,v5
      @return this, so you can use A.get(i.set(i,j,k,l,m,n)) */
  public Index set(int v0, int v1, int v2, int v3, int v4, int v5);
  /** set current index at dimension 0,1,2,3,4,5,6 to v0,v1,v2,v3,v4,v5,v6
      @return this, so you can use A.get(i.set(i,j,k,l,m,n,o)) */
  public Index set(int v0, int v1, int v2, int v3, int v4, int v5, int v6);
}
