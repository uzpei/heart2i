/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package ucar.nc2;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;
import ucar.ma2.Array;

/**
 * Modification of UCAR's NetcdfFile class to handle Image Input and Output
 * Streams.
 *
 * @version 20 July 2002
 */
public class NetcdfStream
{
  protected ucar.netcdf.NetcdfStream _ncStream;
  protected ArrayList variables = new ArrayList();
  protected ArrayList dimensions = new ArrayList();
  protected ArrayList gattributes = new ArrayList();
  protected Hashtable _cachedArrays = new Hashtable();
  protected boolean isClosed = false;

  /**
   * Constructs a NetcdfStream that reads encoded data from the specified
   * Image Input Stream.
   *
   * @param inputStream Image Input Stream to read encoded data from.
   *
   * @throws IOException If an I/O error occurs.
   */
  public NetcdfStream(ImageInputStream inputStream) throws IOException
    {
      _ncStream = new ucar.netcdf.NetcdfStream(inputStream);
      init(_ncStream);
    }

  /** Constructs a NetcdfStream. */
  protected NetcdfStream()
    {
    }

  /** close the file */
  public synchronized boolean isClosed() { return isClosed; }

  public synchronized void close() throws IOException
    {
      _ncStream.close();
      isClosed = true;
    }

  public void flush() throws IOException
    {
      _ncStream.flush();
    }

  protected void init(ucar.netcdf.NetcdfStream ncStream)
    {
      _ncStream = ncStream;

      // collect dimensions
      ucar.netcdf.DimensionIterator dtr = _ncStream.getDimensions().iterator();
      while (dtr.hasNext())
	dimensions.add(new Dimension(dtr.next()));

      // collect variables
      ucar.netcdf.VariableIterator viter = _ncStream.iterator();
      while (viter.hasNext())
	variables.add(new Variable(viter.next(), dimensions));

      // collect global attributes
      ucar.netcdf.AttributeIterator itr = _ncStream.getAttributes().iterator();
      while (itr.hasNext())
	gattributes.add(new Attribute(itr.next()));
    }

  /**
   * Reads the named Variable's data into memory and caches it.
   *
   * @param variableName Name of the Variable whose data is read in.
   *
   * @return Array that is created (and cached) to hold the Variable's data,
   *         or null if the data is not read in.
   *
   * @throws IOException If an I/O error occurs.
   */
  public Array cacheData(String variableName) throws IOException
    {
      // Find the named Variable
      Variable variable = findVariable(variableName);
      if (variable == null) { return null; }

      // Read in the data and cache the Array
      Array array = variable.read();
      _cachedArrays.put(variableName, array);

      return array;
    }

  /**
   * Gets the named Variable's cached data.
   *
   * @param variableName Name of the Variable to get cached data for.
   *
   * @return Array of cached data for the Variable, or null if no such Array
   *         exists in the cache.
   */
  public Array getCachedData(String variableName)
    {
      return (Array)_cachedArrays.get(variableName);
    }

  /** Empties the cache of Variable data. */
  public void removeCachedData()
    {
      _cachedArrays = null;
    }

  /**
   * Return Iterator for the variables.
   * CHANGE TO GENERIC.
   * @return Iterator objects are type Variable.
   */
  public Iterator getVariableIterator() { return variables.iterator(); }

  /**
   * Retrieve the Variable with the specified name.
   * @param name String which identifies the desired variable
   * @return the Variable, or null if not found
   */
  public Variable findVariable(String name)
    {
      for (int i=0; i<variables.size(); i++) {
	Variable v = (Variable) variables.get(i);
	if (name.equals(v.getName()))
	  return v;
      }
      return null;
    }

  /**
   * Returns an iterator over the Dimensions used in this file.
   * This is the union of Dimensions used by all of the Variables.
   * CHANGE TO GENERIC
   * @return Iterator objects are type Dimension.
   */
  public Iterator getDimensionIterator() { return dimensions.iterator(); }

  /**
   * Retrieve a dimension by name.
   * @param name dimension name
   * @return the dimension, or null if not found
   */
  public Dimension findDimension(String name)
    {
      for (int i=0; i<dimensions.size(); i++) {
	Dimension d = (Dimension) dimensions.get(i);
	if (name.equals(d.getName()))
	  return d;
      }
      return null;
    }

  /**
   * Returns the set of attributes associated with this file,
   * also know as the "global" attributes.
   * CHANGE TO GENERIC
   * @return Iterator objects are type Attribute
   */
  public Iterator getGlobalAttributeIterator()
    {
      return gattributes.iterator();
    }

  /**
   * Look up global Attribute by name.
   *
   * @param name the name of the attribute
   * @return the attribute, or null if not found
   */
  public Attribute findGlobalAttribute(String name)
    {
      for (int i=0; i<gattributes.size(); i++) {
	Attribute a = (Attribute) gattributes.get(i);
	if (name.equals(a.getName()))
	  return a;
      }
      return null;
    }

  /**
   * Look up global Attribute by name, ignore case.
   *
   * @param name the name of the attribute
   * @return the attribute, or null if not found
   */
  public Attribute findGlobalAttributeIgnoreCase(String name)
    {
      for (int i=0; i<gattributes.size(); i++) {
	Attribute a = (Attribute) gattributes.get(i);
	if (name.equalsIgnoreCase(a.getName()))
	  return a;
      }
      return null;
    }

  /**
   * Find a String-valued global or variable Attribute by
   * Attribute name (ignore case), return the Value of the Attribute.
   * If variable attribute not found, look for global attribute of same name.
   * If not found return defaultValue
   *
   * @param Variable v: the variable or null for global attribute
   * @param String name: the name of the attribute, case insensitive
   * @param String defaultValue: return this if attribute not found
   * @return the attribute value, or defaultValue if not found
   */
  public String findAttValueIgnoreCase(Variable v, String attName,
				       String defaultValue)
    {
      String attValue = null;
      Attribute att;

      if (v == null)
	att = findGlobalAttributeIgnoreCase( attName);
      else
	att = v.findAttributeIgnoreCase(attName);

      if ((att != null) && att.isString())
	attValue = att.getStringValue();

      if (null == attValue) {    // not found, look for global attribute
	att = findGlobalAttributeIgnoreCase(attName);
	if ((att != null) && att.isString())
	  attValue = att.getStringValue();
      }

      if (null == attValue)                     // not found, use default
	attValue = defaultValue;

      return attValue;
    }

  protected StringBuffer buff = new StringBuffer(2000);

  /** For debugging: dump out the underlying netcdf info. */
  public String toStringDebug()
    {
      buff.setLength(0);
      buff.append("NETCDF stream \n");
      buff.append( _ncStream.getDimensions().toString());
      buff.append(" Global Attributes:\n");
      buff.append( _ncStream.getAttributes().toString());
      buff.append(" Variables:\n");

      for (int i=0; i<variables.size(); i++) {
	Variable v = (Variable) variables.get(i);
	buff.append(v.toStringN());
      }
      return buff.toString();
    }

  /** nicely formatted string representation */
  public String toString()
    {
      buff.setLength(0);
      buff.append("netcdf stream {\n");
      buff.append(" dimensions:\n");
      for (int i=0; i<dimensions.size(); i++) {
	Dimension myd = (Dimension) dimensions.get(i);
	buff.append("    "+myd.getName());
	if (myd.isUnlimited())
	  buff.append(" = UNLIMITED;   // ("+myd.getLength()+" currently)");
	else
	  buff.append(" = "+myd.getLength() +";");
	Variable cv = myd.getCoordinateVariable();
	if (cv != null)
	  buff.append("   // (has coord.var)");
	buff.append("\n");
      }

      buff.append("\n variables:\n");
      for (int i=0; i<variables.size(); i++) {
	Variable v = (Variable) variables.get(i);
	buff.append(v.toString());
      }

      buff.append("\n // Global Attributes:\n");
      Iterator iter = getGlobalAttributeIterator();
      while (iter.hasNext()) {
	Attribute att = (Attribute) iter.next();
	buff.append("    :"+att.getName());
	//buff.append(" "+att.getValueType());
	if (att.isString())
	  buff.append(" = \""+att.getStringValue()+"\"");
	else if (att.isArray()) {
	  buff.append(" = ");
	  for (int i=0; i<att.getLength(); i++) {
	    if (i != 0) buff.append(", ");
	    buff.append(att.getNumericValue(i));
	  }
	} else
	  buff.append(" = "+att.getNumericValue());
	buff.append(";");
	if (att.getValueType() != String.class)
	  buff.append(" // "+att.getValueType().getName());
	buff.append("\n");
      }

      buff.append("\n}\n");
      return buff.toString();
    }
}
