/*
COPYRIGHT NOTICE
Copyright (c) 2009  Scott C. Neu, Daniel J. Valentino, and Arthur W. Toga

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package ucar.nc2;

import ucar.ma2.ArrayAbstract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.IOException;
import javax.imageio.stream.ImageOutputStream;

/**
 * Modification of UCAR's NetcdfFileWriteable class to handle Image Input and
 * Output Streams.
 *
 * @version 22 November 2002
 */
public class NetcdfStreamWriteable extends NetcdfStream
{
  private ucar.netcdf.Schema schema;
  private HashMap protoVarHash = new HashMap(50);
  private boolean defineMode;
  private boolean fill = false;

//   private ArrayList variables = null;
  private ArrayList dimensions = null;
  private ArrayList gattributes = null;

  /** Open a new Netcdf file, put it into define mode. */
  public NetcdfStreamWriteable()
    {
      super();
      schema = new ucar.netcdf.Schema();
      defineMode = true;
    }

  /**
   * Determines whether or not the stream is in define mode.
   *
   * @return True if the stream is in define mode; false otherwise.
   */
  public boolean isInDefineMode()
    {
      return defineMode;
    }

  /**
   * Set the fill flag: call before call to create()
   * Default fill = false
   */
  public void setFill(boolean fill)
    {
      this.fill = fill;
      if (!defineMode)
	_ncStream.setFill(fill);
    }

  /**
   * Add a Dimension to the file. Must be in define mode.
   * @param dimName: name of dimension
   * @param size: size of dimension, or -1 for unlimited dimension.
   */
  public Dimension addDimension(String dimName, int size)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");

      if (size < 0)
	return new Dimension( new ucar.netcdf.UnlimitedDimension( dimName));
      return new Dimension( new ucar.netcdf.Dimension( dimName, size));
    }

  /**
   * Add a Global attribute of type String to the file. Must be in define mode.
   *
   * @param String name: name of attribute.
   * @param String value: value of atribute.
   */
  public void addGlobalAttribute(String name, String value)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");

      ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( name, value);
      schema.putAttribute(attr);
    }

  /**
   * Add a Global attribute of type Number to the file. Must be in define mode.
   * Remark: this looks buggy; allowed types unclear.
   * @param String name: name of attribute.
   * @param Number value: must be of type Float, Double, Integer, Short or Byte
   */
  public void addGlobalAttribute(String name, Number value)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");

      ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( name, value);
      schema.putAttribute(attr);
    }
  
  /**
   * Add a Global attribute of type Array to the file. Must be in define mode.
   * Remark: may be buggy; allowed types unclear.
   *
   * @param String name: name of attribute.
   * @param Object value: must be 1D array of double, float, int, short, char,
   *        or byte
   */
  public void addGlobalAttribute(String name, Object value)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");

      ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( name, value);
      schema.putAttribute(attr);
    }

  /**
   * Add a variable to the file. Must be in define mode.
   * @param varName: name of Variable
   * @param componentType: type of underlying element.
   * @param Dimension dims[]: array of Dimensions for the variable
   */
  public void addVariable(String varName, java.lang.Class componentType,
			  Dimension[] dims)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");

      ucar.netcdf.Dimension [] d = new ucar.netcdf.Dimension[ dims.length];
      for (int i=0; i<dims.length; i++)
	d[i] = dims[i].getOrg();
      ucar.netcdf.ProtoVariable proto =
	new ucar.netcdf.ProtoVariable( varName, componentType, d);
      schema.put(proto);

      protoVarHash.put( varName, proto);
    }

  /**
   * Add an attribute of type String to the named Variable. Must be in define
   * mode.
   *
   * @param String varName: name of variable.
   *        IllegalArgumentException if not valid name.
   * @param String attName: name of attribute.
   * @param String value: value of atribute.
   */
  public void addVariableAttribute(String varName, String attName,
				   String value)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");
      ucar.netcdf.ProtoVariable proto =
	(ucar.netcdf.ProtoVariable) protoVarHash.get( varName);
      if (null == proto) {
	throw new IllegalArgumentException("addVariableAttribute variable " +
					   "name not found = <"+ varName+">");
      }

      ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( attName, value);
      proto.putAttribute(attr);
    }

  /**
   * Add an attribute of type Number to the named Variable. Must be in define
   * mode.
   *
   * @param String varName: name of attribute.
   *        IllegalArgumentException if not valid name.
   * @param String attName: name of attribute.
   * @param Number value: must be of type Float, Double, Integer, Short or Byte
   */
  public void addVariableAttribute(String varName, String attName,
				   Number value)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");
      ucar.netcdf.ProtoVariable proto =
	(ucar.netcdf.ProtoVariable) protoVarHash.get( varName);
      if (null == proto)
	throw new IllegalArgumentException("addVariableAttribute illegal " +
					   "variable name "+ varName);

      ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( attName, value);
      proto.putAttribute(attr);
    }

  /**
   * Add an attribute of type Array to the named Variable. Must be in define
   * mode.
   *
   * @param String varName: name of attribute.
   *        IllegalArgumentException if not valid name.
   * @param String attName: name of attribute.
   * @param Object value: must be 1D array of double, float, int, short, char,
   *        or byte
   */
  public void addVariableAttribute(String varName, String attName,
				   Object value)
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");
      ucar.netcdf.ProtoVariable proto =
	(ucar.netcdf.ProtoVariable) protoVarHash.get( varName);
      if (null == proto)
	throw new IllegalArgumentException("addVariableAttribute illegal " +
					   "variable name "+ varName);

      ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( attName, value);
      proto.putAttribute(attr);
    }

  /**
   * Adds the Array to the named Variable.  Must be in define mode.
   *
   * @param varName Name of the Variable.
   * @param array Array to add to the Variable.
   *
   * @throws IllegalArgumentException If the named Variable does not exist.
   * @throws UnsupportedOperationException If not in define mode.
   */
  public void addVariableArray(String varName, ArrayAbstract array)
    {
      if (!defineMode) {
	throw new UnsupportedOperationException("not in define mode");
      }

      // Check that the Variable exists
      ucar.netcdf.ProtoVariable proto =
	(ucar.netcdf.ProtoVariable) protoVarHash.get(varName);
      if (null == proto) {
	throw new IllegalArgumentException("addVariableArray illegal " +
					   "variable name "+ varName);
      }

      // Save the Array in the cache memory
      _cachedArrays.put(varName, array);
    }

  /**
   * After you have added all of the Dimensions, Variables, and Attributes,
   * call create() to actually create the file. You must be in define mode.
   * After this call, you are no longer in define mode, and cannot return to
   * it.
   *
   * @param outputStream Image Output Stream to write encoded data to.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void create(ImageOutputStream outputStream) throws IOException
    {
      if (!defineMode)
	throw new UnsupportedOperationException("not in define mode");

      _ncStream = new ucar.netcdf.NetcdfStream(outputStream, fill, schema);
      init(_ncStream);

      defineMode = false;

      // Write the cached Arrays (must be in same order as variable order!)
      Iterator iter = variables.iterator();
      while ( iter.hasNext() ) {
	String variableName = ((Variable)iter.next()).getName();
	ArrayAbstract array = (ArrayAbstract)_cachedArrays.get(variableName);
	if (array != null) { write(variableName, array); }
      }
    }

  /**
   * Write data to the named variable, origin assumed to be 0. Must not be in
   * define mode.
   * @param name: name of variable.
   * IllegalArgumentException if variable name does not exist.
   * @param ArrayAbstract values: write this array; must be same type as
   *        Variable
   * @return true on success.
   */
  public boolean write(String varName, ArrayAbstract values) throws IOException
    {
      int [] origin = new int[ values.getRank()];
      return write( varName, origin, values);
    }

  /**
   * Write data to the named variable. Must not be in define mode.
   * @param name: name of variable.
   * IllegalArgumentException if variable name does not exist.
   * @param int [] origin: offset within the variable to start writing.
   * @param ArrayAbstract values: write this array; must be same type as
   *        Variable
   * @return true on success.
   */
  public boolean write(String varName, int [] origin, ArrayAbstract values)
    throws IOException
    {
      if (defineMode)
	throw new UnsupportedOperationException("in define mode");

      Variable ncvar = findVariable( varName);
      if (ncvar == null)
	throw new IllegalArgumentException("NetcdfStreamWriteable.write " +
					   "illegal variable name "+ varName);

      ucar.netcdf.Variable realvar = ncvar.getNetcdfVariable();
      ArrayAdapter2 aa = new ArrayAdapter2( values);

      realvar.copyin(origin, aa);
      return true;
    }

  /**
   * Flush anything written to disk.
   */
  public void flush() throws IOException
    {
      _ncStream.flush();
    }

  // package private
  boolean write(ucar.netcdf.Variable ncvar, int [] origin,
		ArrayAbstract values) throws IOException
    {
      ArrayAdapter2 aa = new ArrayAdapter2( values);
      ncvar.copyin(origin, aa);
      return true;
    }

  ucar.netcdf.Variable getVarByName(String name)
    {
      Variable ncvar = findVariable( name);
      if (ncvar == null)
	return null;
      return ncvar.getNetcdfVariable();
    }
}
