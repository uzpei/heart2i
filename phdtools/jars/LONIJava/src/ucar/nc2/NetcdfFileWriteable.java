// $Id: NetcdfFileWriteable.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.nc2;

import ucar.ma2.ArrayAbstract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.IOException;

/**
 * Create/Write Netcdf files. <p>
 * Because of the limitations of the underlying implementation, netcdf
 * files can only have Dimensions, Attributes and Variables added to it
 * at creation time. Thus, when a file is first opened, it in is "define mode"
 * where these may added. Once create() is called, you can no longer add, delete, or modify
 * the Dimensions, Attributes or Variables. <p>
 * After create has been called you can then write the Variables' data values.
 *
 * @see NetcdfFile
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */

public class NetcdfFileWriteable extends NetcdfFile {
  private ucar.netcdf.Schema schema;
  private HashMap protoVarHash = new HashMap(50);
  private boolean defineMode;
  private boolean fill = false;

  private ArrayList variables = null;
  private ArrayList dimensions = null;
  private ArrayList gattributes = null;

  /** Open a new Netcdf file, put it into define mode.
  */
  public NetcdfFileWriteable() {
    super();
    schema = new ucar.netcdf.Schema();
    defineMode = true;
  }

  /** Open an existing Netcdf file for writing data.
   * Not in define mode, you can only write to existing Variables.
   */
  public NetcdfFileWriteable(String filename) throws IOException {
    super( filename, false);
    defineMode = false;
  }

  /** set the filename: call before call to create() */
  public void setName( String filename) { name = filename; }

  /** Set the fill flag: call before call to create()
   *  Default fill = false
   */
  public void setFill( boolean fill) {
    this.fill = fill;
    if (!defineMode)
      ncfile.setFill(fill);
  }


    ////////////////////////////////////////////
    //// use these calls to create a new file

  /**
   * Add a Dimension to the file. Must be in define mode.
   * @param dimName: name of dimension
   * @param size: size of dimension, or -1 for unlimited dimension.
   */
  public Dimension addDimension(String dimName, int size) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");

    if (size < 0)
      return new Dimension( new ucar.netcdf.UnlimitedDimension( dimName));
    return new Dimension( new ucar.netcdf.Dimension( dimName, size));
  }

  /**
   * Add a Global attribute of type String to the file. Must be in define mode.
   *
   * @param String name: name of attribute.
   * @param String value: value of atribute.
   */
  public void addGlobalAttribute(String name, String value) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");

    ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( name, value);
    schema.putAttribute(attr);
  }

  /**
   * Add a Global attribute of type Number to the file. Must be in define mode.
   * Remark: this looks buggy; allowed types unclear.
   * @param String name: name of attribute.
   * @param Number value: must be of type Float, Double, Integer, Short or Byte
   */
  public void addGlobalAttribute(String name, Number value) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");

    ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( name, value);
    schema.putAttribute(attr);
  }

  /**
   * Add a Global attribute of type Array to the file. Must be in define mode.
   * Remark: may be buggy; allowed types unclear.
   *
   * @param String name: name of attribute.
   * @param Object value: must be 1D array of double, float, int, short, char, or byte
   */
  public void addGlobalAttribute(String name, Object value) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");

    ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( name, value);
    schema.putAttribute(attr);
  }

  /**
   * Add a variable to the file. Must be in define mode.
   * @param varName: name of Variable
   * @param componentType: type of underlying element.
   * @param Dimension dims[]: array of Dimensions for the variable
   */
  public void addVariable(String varName, java.lang.Class componentType, Dimension[] dims) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");

    ucar.netcdf.Dimension [] d = new ucar.netcdf.Dimension[ dims.length];
    for (int i=0; i<dims.length; i++)
      d[i] = dims[i].getOrg();
    ucar.netcdf.ProtoVariable proto = new ucar.netcdf.ProtoVariable( varName, componentType, d);
    schema.put(proto);

    protoVarHash.put( varName, proto);
  }

  /**
   * Add an attribute of type String to the named Variable. Must be in define mode.
   *
   * @param String varName: name of variable. IllegalArgumentException if not valid name.
   * @param String attName: name of attribute.
   * @param String value: value of atribute.
   */
  public void addVariableAttribute(String varName, String attName, String value) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");
    ucar.netcdf.ProtoVariable proto = (ucar.netcdf.ProtoVariable) protoVarHash.get( varName);
    if (null == proto) {
      throw new IllegalArgumentException("addVariableAttribute variable name not found = <"+ varName+">");
    }

    ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( attName, value);
    proto.putAttribute(attr);
  }


  /**
   * Add an attribute of type Number to the named Variable. Must be in define mode.
   *
   * @param String varName: name of attribute. IllegalArgumentException if not valid name.
   * @param String attName: name of attribute.
   * @param Number value: must be of type Float, Double, Integer, Short or Byte
   */
  public void addVariableAttribute(String varName, String attName, Number value) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");
    ucar.netcdf.ProtoVariable proto = (ucar.netcdf.ProtoVariable) protoVarHash.get( varName);
    if (null == proto)
      throw new IllegalArgumentException("addVariableAttribute illegal variable name "+ varName);

    ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( attName, value);
    proto.putAttribute(attr);
  }

  /**
   * Add an attribute of type Array to the named Variable. Must be in define mode.
   *
   * @param String varName: name of attribute. IllegalArgumentException if not valid name.
   * @param String attName: name of attribute.
   * @param Object value: must be 1D array of double, float, int, short, char, or byte
   */
  public void addVariableAttribute(String varName, String attName, Object value) {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");
    ucar.netcdf.ProtoVariable proto = (ucar.netcdf.ProtoVariable) protoVarHash.get( varName);
    if (null == proto)
      throw new IllegalArgumentException("addVariableAttribute illegal variable name "+ varName);

    ucar.netcdf.Attribute attr = new ucar.netcdf.Attribute( attName, value);
    proto.putAttribute(attr);
  }


  /**
   * After you have added all of the Dimensions, Variables, and Attributes,
   * call create() to actually create the file. You must be in define mode.
   * After this call, you are no longer in define mode, and cannot return to it.
   */
  public void create() throws java.io.IOException {
    if (!defineMode)
      throw new UnsupportedOperationException("not in define mode");

    ncfile = new ucar.netcdf.NetcdfFile( name, true, fill, schema);
    if (ncfile == null) {    // can this happen ??
      throw new java.io.IOException("createNetcdfFile failed");
    }
    init(ncfile);

    defineMode = false;
  }

    ////////////////////////////////////////////
    //// use these calls to write to the file



  /** Write data to the named variable, origin assumed to be 0. Must not be in define mode.
   * @param name: name of variable. IllegalArgumentException if variable name does not exist.
   * @param ArrayAbstract values: write this array; must be same type as Variable
   * @return true on success.
   */
  public boolean write(String varName, ArrayAbstract values) throws java.io.IOException {
    int [] origin = new int[ values.getRank()];
    return write( varName, origin, values);
  }

  /** Write data to the named variable. Must not be in define mode.
   * @param name: name of variable. IllegalArgumentException if variable name does not exist.
   * @param int [] origin: offset within the variable to start writing.
   * @param ArrayAbstract values: write this array; must be same type as Variable
   * @return true on success.
   */
  public boolean write(String varName, int [] origin, ArrayAbstract values) throws java.io.IOException {
    if (defineMode)
      throw new UnsupportedOperationException("in define mode");

    Variable ncvar = findVariable( varName);
    if (ncvar == null)
      throw new IllegalArgumentException("NetcdfFileWriteable.write illegal variable name "+ varName);

    ucar.netcdf.Variable realvar = ncvar.getNetcdfVariable();
    ArrayAdapter2 aa = new ArrayAdapter2( values);

    realvar.copyin(origin, aa);
    return true;
  }

    /**
     * Flush anything written to disk.
     */
    public void flush() throws IOException {
      ncfile.flush();
    }


  // package private
  boolean write(ucar.netcdf.Variable ncvar, int [] origin, ArrayAbstract values) throws IOException {
    ArrayAdapter2 aa = new ArrayAdapter2( values);
    ncvar.copyin(origin, aa);
    return true;
  }

  ucar.netcdf.Variable getVarByName( String name) {
    Variable ncvar = findVariable( name);
    if (ncvar == null)
      return null;
    return ncvar.getNetcdfVariable();
  }

/*  public static void main(String[] args) throws java.io.IOException {
    NetcdfSchema sceme = new NetcdfSchema();
    sceme.setName("F:/temp/testNetcdfSchema");

    Dimension[] d = new Dimension[3];
    d[0] = sceme.addDimension("document", -1);
    d[1] = sceme.addDimension("term", 100);
    d[2] = sceme.addDimension("nVectors", 10);

    Dimension[] d1 = new Dimension[2];
    d1[0] = d[0];
    d1[1] = d[1];
    sceme.addVariable("score", float.class, d1);

    Dimension[] d4 = new Dimension[2];
    d4[0] = d[1];
    d4[1] = d[2];
    sceme.addVariable("eigenVector", float.class, d4);

    Dimension[] d5 = new Dimension[1];
    d5[0] = d[2];
    sceme.addVariable("eigenValue", float.class, d5);

    Dimension[] d2 = new Dimension[2];
    d2[0] = d[1];
    d2[1] = sceme.addDimension("termLen", 20);
    sceme.addVariable("term", char.class, d2);

    Dimension[] d3 = new Dimension[2];
    d3[0] = d[0];
    d3[1] = sceme.addDimension("documentLen", 50);
    sceme.addVariable("document", char.class, d3);

    NetcdfFile ncfile = sceme.createNetcdfFile();
  }  */
}

/* Change History:
   $Log: NetcdfFileWriteable.java,v $
   Revision 1.1.1.1  2003/12/17 02:06:59  cvswebdev
   MINC Image I/O Plugin

   Revision 1.13  2002/05/24 00:09:12  caron
   add flush()

   Revision 1.12  2002/04/22 22:54:56  caron
   use ArrayAdapter2.java

   Revision 1.11  2001/08/10 21:18:28  caron
   add close()

   Revision 1.10  2001/05/01 15:10:34  caron
   clean up VariableStandardized.java; add HTTP access

   Revision 1.9  2001/02/21 21:21:25  caron
   add VariableStandardized

   Revision 1.8  2001/02/06 22:18:33  caron
   final changes for beta .9

   Revision 1.7  2001/01/25 01:08:55  caron
   release nc2

   Revision 1.6  2000/09/19 16:25:52  caron
   NetcdfFileWriteable complete, better docs

   Revision 1.5  2000/08/18 04:14:37  russ
   Licensed under GNU LGPL.

   Revision 1.4  2000/08/17 23:44:47  caron
   prepare for c**right clobbering


   Revision 1.2  2000/06/30 16:47:40  caron
   minor revs for GDV release

   Revision 1.1  2000/04/26 21:02:41  caron
   writeable Netcdf files

 */

