// $Id: Dimension.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.nc2;
import java.io.Serializable;

/**
 * A Dimension object contains an array length which is
 * named for use in multiple netcdf variables.
 *
 * The only way to create a Dimension is in NetcdfFileWriteable.
 * @see NetcdfFileWriteable.addDimension
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */

public class Dimension {
  private ucar.netcdf.Dimension dim;
  private Variable coordVar;

        /**
         * Returns the name of this Dimension.
         * @return String which identifies this Dimension.
         */
  public String getName() { return dim.getName(); }

        /**
         * Retrieve the length.
         * @return int which is the length of this Dimension
         */
  public int getLength() { return dim.getLength(); }

  /** If the Dimension isUnlimited, then the length can increase; otherwise it is immutable.
  */
  public boolean isUnlimited() {
    return (dim instanceof ucar.netcdf.UnlimitedDimension);
  }

  /** return coordinate variable if it has one */
  public Variable getCoordinateVariable() { return coordVar; }

  // package private
  protected Dimension () {}
  Dimension (ucar.netcdf.Dimension dim) { this.dim = dim; }
  protected void setCoordinateVariable( Variable v) { coordVar = v; }
  ucar.netcdf.Dimension getOrg() { return dim; }
}

/* Change History:
   $Log: Dimension.java,v $
   Revision 1.1.1.1  2003/12/17 02:06:59  cvswebdev
   MINC Image I/O Plugin

   Revision 1.7  2001/08/10 21:18:27  caron
   add close()

 */
