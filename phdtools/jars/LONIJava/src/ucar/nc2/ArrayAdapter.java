// $Id: ArrayAdapter.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.nc2;

import ucar.ma2.*;
import java.io.IOException;

/**
 * (package private)
 * This adapts a ucar.ma2.ArrayAbstract into a ucar.multiarray.MultiArray.
 * DEPRECATED: severe performance problesm. Use Adapter2
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */
class ArrayAdapter implements ucar.multiarray.MultiArray {
  private ArrayAbstract arr;
  private Index ima;

  ArrayAdapter( ArrayAbstract arr) {
    this.arr = arr;
    this.ima = arr.getIndex();
  }

  public Class getComponentType() { return arr.getElementType(); }
  public int getRank() { return arr.getRank(); }
  public int [] getLengths() { return arr.getShape(); }
  public boolean isUnlimited() { return false; }
  public boolean isScalar() { return 1 == arr.getRank(); }

    // Variable.copyin() requires this crap
  public Object get(int [] index) {
    Class c = getComponentType();
    ima.set(index);
    if (c == Double.TYPE)
      return new Double( arr.getDouble( ima));
    else if (c == Float.TYPE)
      return new Float( arr.getFloat( ima));
    else if (c == Long.TYPE)
      return new Long( arr.getLong( ima));
    else if (c == Integer.TYPE)
      return new Integer( arr.getInt( ima));
    else if (c == Short.TYPE)
      return new Short( arr.getShort( ima));
    else if (c == Byte.TYPE)
      return new Byte( arr.getByte( ima));
    else if (c == Character.TYPE)
      return new Character( arr.getChar( ima));
    else if (c == Boolean.TYPE)
      return new Boolean( arr.getBoolean( ima));

    throw new UnsupportedOperationException("get Object "+c.getName());
  }
  public boolean getBoolean(int [] index) { return arr.getBoolean( ima.set(index)); }
  public char getChar(int [] index) { return arr.getChar( ima.set(index)); }
  public byte getByte(int [] index) { return arr.getByte( ima.set(index)); }
  public short getShort(int [] index) { return arr.getShort( ima.set(index)); }
  public int getInt(int [] index) { return arr.getInt( ima.set(index)); }
  public long getLong(int [] index) { return arr.getLong( ima.set(index)); }
  public float getFloat(int [] index) { return arr.getFloat( ima.set(index)); }
  public double getDouble(int [] index) { return arr.getDouble( ima.set(index)); }

  public void set(int [] index, Object value) { throw new UnsupportedOperationException(); }
  public void setBoolean(int [] index, boolean value) { arr.setBoolean( ima.set(index), value); }
  public void setChar(int [] index, char value) { arr.setChar( ima.set(index), value); }
  public void setByte(int [] index, byte value) { arr.setByte( ima.set(index), value); }
  public void setShort(int [] index, short value) { arr.setShort( ima.set(index), value); }
  public void setInt(int [] index, int value) { arr.setInt( ima.set(index), value); }
  public void setLong(int [] index, long value) { arr.setLong( ima.set(index), value); }
  public void setFloat(int [] index, float value) { arr.setFloat( ima.set(index), value); }
  public void setDouble(int [] index, double value) { arr.setDouble( ima.set(index), value); }

        /**
         * Aggregate read access.
         * Return a new MultiArray of the
         * same componentType as this, and with shape as specified,
         * which is initialized to the values of this, as
         * clipped to (origin, origin + shape).
         * <p>
         * It is easier to implement than to specify :-).
         * <p>
         * The main reason to implement this instead of using
         * the equivalent proxy is for remote or file access.
         * <p>
         * <code>assert(origin[ii] + shape[ii] <= lengths[ii]);</code>
         *
         * @param origin int array specifying the starting index.
         * @param shape  int array specifying the extents in each
	 *     	 *     	 *     	 *	dimension. This becomes the shape of the return.
         * @return the MultiArray with the specified shape
         */
  public ucar.multiarray.MultiArray copyout(int [] origin, int [] shape) throws IOException {
    Array newArr;
    try {
      newArr = arr.section(origin, shape);
    } catch (ucar.ma2.InvalidRangeException e) {
      throw new IOException("InvalidRangeException");  // no choice
    }
    return new ArrayAdapter( (ArrayAbstract) newArr.copy());
  }

        /**
         * Aggregate write access.
         * Given a MultiArray, copy it into this at the specified starting index.
         * TODO: clearer specification.
         * <p>
         * Hopefully this member can be optimized in various situations.
         * <p>
         * <code>assert(origin[ii] + (source.getLengths())[ii]
                <= (getLengths())[ii]);</code>
         *
         * @param origin int array specifying the starting index.
         * @param source  MultiArray with the same componentType as
         *      this and shape smaller than
	 *     	 *     	 *     	 *	<code>this.getLengths() - origin</code>
         */
  public void copyin(int [] origin, ucar.multiarray.MultiArray source) throws IOException {
    System.out.println("what the fuh...?");
    throw new UnsupportedOperationException("Arrayadapter copyin");
  }

        /**
         * Returns a new array containing all of the elements in this
         * MultiArray. The returned array is one dimensional.
         * The order of the elements in the result is natural,
         * as if we used an IndexIterator to step through the elements
         * of this MultiArray. The component type of the result is
         * the same as this.
         * <p>
         * This method acts as bridge between array-based and MultiArray-based
         * APIs.
         * <p>
         * This method is functionally equivalent to
         * <pre>
                Object anArray = Array.newInstance(getComponentType(), 1);
                int [] origin = new int[getRank()]
                int [] shape = getDimensions();
                return toArray(anArray, origin, shape);
         * </pre>
         *
         * @return a one dimensional Array containing all the elements
         * in this MultiArray
         */
  public Object	toArray() {
    return arr.copyTo1DJavaArray();
  }


        /**
         * Returns an array containing elements of this
         * MultiArray specified by origin and shape,
         * possibly converting the component type.
         * The returned array is one dimensional.
         * The order of the elements in the result is natural,
         * as if we used an IndexIterator to step through the elements
         * of this MultiArray.
         * <p>
         * The anArray argument should be an array.
         * If it is large enough to contain the output,
         * it is used and no new storage is allocated.
         * Otherwise, new storage is allocated with the
         * same component type as the argument, and the data
         * is copied into it.
         * <p>
         * This method acts as bridge between array-based and MultiArray-based
         * APIs.
         * <p>
         * This method is similar to copyout(origin, shape).toArray(),
         * but avoids a copy operation and (potentially) an allocation.
         * <p>
         * NOTE: Implementation of type conversion is deferred until
         * JDK 1.2. Currently, the componentType of <code>anArray</code>
         * must be the same as <code>this</code>
         *
         * @return a one dimensional Array containing the specified elements
         */
  public Object toArray(Object anArray, int [] origin, int [] shape) throws IOException {
    ArrayAbstract newArr;
    try {
      newArr = (ArrayAbstract) arr.section(origin, shape);
    } catch (ucar.ma2.InvalidRangeException e) {
      throw new IOException("InvalidRangeException");  // no choice
    }
    return newArr.copyTo1DJavaArray();
  }

}

/* Change History:
   $Log: ArrayAdapter.java,v $
   Revision 1.1.1.1  2003/12/17 02:06:59  cvswebdev
   MINC Image I/O Plugin

   Revision 1.9  2002/04/22 22:54:55  caron
   use ArrayAdapter2.java

   Revision 1.8  2001/08/10 21:18:27  caron
   add close()

 */
