// $Id: Attribute.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.nc2;

/**
 * A (name, value) pair, where name is a String, and
 * value is String, Number or Number[].
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */

/*
 * Implementation Notes:
 *   does this convert char[] to String?
 *   what Exceptions should be thrown?
 */

public class Attribute {
  private ucar.netcdf.Attribute att;

  protected Attribute() {;}

  /** constructor: adapter around ucar.netcdf */
  Attribute( ucar.netcdf.Attribute att) {
    this.att = att;
  }

  /** Return the name of this Attribute. */
  public String getName() { return att.getName(); }

  /** Returns the type of the value: String, Double, Float, Int, Short, Byte */
  public Class 	getValueType() {
    Object val = att.getValue();
    Class c = val.getClass();
    if (c.isArray())
      return c.getComponentType();
    else
      return c;
  }

  /** True if value is an instance of String
   * @return boolean value instanceof String
   */
  public boolean isString() { return att.isString(); }

  /** True if value is an array of Number */
  public boolean isArray() { return (getLength() > 1); }

  /**
   * Length of the array of values.
   * @return the length of the value array; = 1 if scaler
   */
  public int getLength() {
    if (isString())
      return 1;
    else
      return att.getLength();
  }

  /**
   * Retrieve the value in its most general form.
   * @return Object which is either a String, a Number or Number[]
   */
  public Object getValue() {
    if (att.isString())
      return getStringValue();
    else
      return att.getValue();
  }

  /**
   * Retrieve String value.
   * @return String if this is a String valued attribute.
   * @throws ClassCastException if this is not String valued.
   * @see Attribute#isString
   */
  public String getStringValue(){
    String v = att.getStringValue();

    if (v.length() == 0) return v;   //  fixed for zero length string - dwd

    // get rid of trailing zeroes
    if (v.charAt( v.length()-1) == 0)
      return v.substring(0, v.length()-1);
    else
      return v;
 }


  /**
   * Retrieve simple numeric value.
   * Equivalent to <code>getNumericValue(0)</code>
   * @return Number the first element of the value array
   */
  public Number getNumericValue(){ return att.getNumericValue(); }


  /// these deal with array-valued attributes

  /**
   * Retrieve numeric value by index.
   * @param index int which is the index into the value array.
   * @return Number <code>value[index]</code>
   */
  public Number getNumericValue(int index) { return att.getNumericValue(index); }

}

/* Change History:
   $Log: Attribute.java,v $
   Revision 1.1.1.1  2003/12/17 02:06:59  cvswebdev
   MINC Image I/O Plugin

   Revision 1.9  2001/09/14 21:32:02  caron
   check zero len string

   Revision 1.8  2001/08/10 21:18:27  caron
   add close()

 */
