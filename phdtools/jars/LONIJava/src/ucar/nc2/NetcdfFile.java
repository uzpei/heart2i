// $Id: NetcdfFile.java,v 1.1.1.1 2003/12/17 02:06:59 cvswebdev Exp $
/*
 * Copyright 1997-2000 Unidata Program Center/University Corporation for
 * Atmospheric Research, P.O. Box 3000, Boulder, CO 80307,
 * support@unidata.ucar.edu.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package ucar.nc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.io.IOException;
import java.net.URL;

/**
 * Read-only Netcdf files. This can access local netcdf files, or netcdf files through an http server.
 * These are collections of Dimensions, Attributes, and Variables.
 *
 * @author caron
 * @version $Revision: 1.1.1.1 $ $Date: 2003/12/17 02:06:59 $
 */

public class NetcdfFile {
  protected String name;
  protected ucar.netcdf.NetcdfFile ncfile;
  protected ArrayList variables = new ArrayList();
  protected ArrayList dimensions = new ArrayList();
  protected ArrayList gattributes = new ArrayList();
  protected boolean isClosed = false;

    // for subclasses
  protected NetcdfFile() { }
    // for NetcdfFileWriteable
  NetcdfFile(String filename, boolean readonly) throws IOException {
    ncfile = new ucar.netcdf.NetcdfFile( filename, readonly);
    init(ncfile);
  }

  /** Open a netcdf file (read only).
    @param String filename name of file
   */
  public NetcdfFile(String filename) throws IOException {
    ncfile = new ucar.netcdf.NetcdfFile( filename, true);
    init(ncfile);
  }

  /**
   * Open existing, read-only netcdf file through a URL. This may use either the
   * file: or http: protocol. If it uses the file protocol, it will be opened as a
   * read-only file using url.getPath(). If it uses the http protocol, it will be
   * read over http using HTTPRandomAccessFile.
   *
   * @param url       the file URL
   */
  public NetcdfFile(URL url) throws IOException {
    ncfile = new ucar.netcdf.NetcdfFile( url);
    init(ncfile);
  }

    /** close the file */
  public synchronized boolean isClosed() { return isClosed; }
  public synchronized void close() throws java.io.IOException {
    ncfile.close();
    isClosed = true;
  }

  protected void init(ucar.netcdf.NetcdfFile ncfile) {
    this.ncfile = ncfile;
    name = ncfile.getName();

    // collect dimensions
    ucar.netcdf.DimensionIterator diter = ncfile.getDimensions().iterator();
    while (diter.hasNext())
      dimensions.add(new Dimension(diter.next()));

    // collect variables
    ucar.netcdf.VariableIterator viter = ncfile.iterator();
    while (viter.hasNext())
      variables.add(new Variable(viter.next(), dimensions));

    // collect global attributes
    ucar.netcdf.AttributeIterator iter = ncfile.getAttributes().iterator();
    while (iter.hasNext())
      gattributes.add(new Attribute(iter.next()));
  }

   /** Return the full pathname of file. */
  public String getPathName() { return name; }

  /** Return Iterator for the variables.
   * CHANGE TO GENERIC.
   * @return Iterator objects are type Variable.
   */
  public Iterator getVariableIterator() { return variables.iterator(); }

  /**
   * Retrieve the Variable with the specified name.
   * @param name String which identifies the desired variable
   * @return the Variable, or null if not found
   */
  public Variable findVariable(String name) {
    for (int i=0; i<variables.size(); i++) {
      Variable v = (Variable) variables.get(i);
      if (name.equals(v.getName()))
        return v;
    }
    return null;
  }

  /**
   * Returns an iterator over the Dimensions used in this file.
   * This is the union of Dimensions used by all of the Variables.
   * CHANGE TO GENERIC
   * @return Iterator objects are type Dimension.
   */
  public Iterator getDimensionIterator() { return dimensions.iterator(); }

    /**
   * Retrieve a dimension by name.
   * @param name dimension name
   * @return the dimension, or null if not found
   */
  public Dimension findDimension(String name) {
    for (int i=0; i<dimensions.size(); i++) {
      Dimension d = (Dimension) dimensions.get(i);
      if (name.equals(d.getName()))
        return d;
    }
    return null;
  }

  /**
   * Returns the set of attributes associated with this file,
   * also know as the "global" attributes.
   * CHANGE TO GENERIC
   * @return Iterator objects are type Attribute
   */
  public Iterator getGlobalAttributeIterator() {
   return gattributes.iterator();
  }

    /**
     * Look up global Attribute by name.
     *
     * @param name the name of the attribute
     * @return the attribute, or null if not found
     */
  public Attribute findGlobalAttribute(String name) {
    for (int i=0; i<gattributes.size(); i++) {
      Attribute a = (Attribute) gattributes.get(i);
      if (name.equals(a.getName()))
        return a;
    }
    return null;
  }

    /**
     * Look up global Attribute by name, ignore case.
     *
     * @param name the name of the attribute
     * @return the attribute, or null if not found
     */
  public Attribute findGlobalAttributeIgnoreCase(String name) {
    for (int i=0; i<gattributes.size(); i++) {
      Attribute a = (Attribute) gattributes.get(i);
      if (name.equalsIgnoreCase(a.getName()))
        return a;
    }
    return null;
  }

    /**
     * Find a String-valued global or variable Attribute by
     * Attribute name (ignore case), return the Value of the Attribute.
     * If variable attribute not found, look for global attribute of same name.
     * If not found return defaultValue
     *
     * @param Variable v: the variable or null for global attribute
     * @param String name: the name of the attribute, case insensitive
     * @param String defaultValue: return this if attribute not found
     * @return the attribute value, or defaultValue if not found
     */
  public String findAttValueIgnoreCase( Variable v, String attName, String defaultValue) {
    String attValue = null;
    Attribute att;

    if (v == null)
      att = findGlobalAttributeIgnoreCase( attName);
    else
      att = v.findAttributeIgnoreCase(attName);

    if ((att != null) && att.isString())
      attValue = att.getStringValue();

    if (null == attValue) {                    // not found, look for global attribute
      att = findGlobalAttributeIgnoreCase(attName);
      if ((att != null) && att.isString())
        attValue = att.getStringValue();
    }

    if (null == attValue)                     // not found, use default
      attValue = defaultValue;

    return attValue;
  }

  protected StringBuffer buff = new StringBuffer(2000);

  /** For debugging: dump out the underlying netcdf info. */
  public String toStringDebug() {
    buff.setLength(0);
    buff.append("NETCDF file "+getPathName()+"\n");
    buff.append( ncfile.getDimensions().toString());
    buff.append(" Global Attributes:\n");
    buff.append( ncfile.getAttributes().toString());
    buff.append(" Variables:\n");

    for (int i=0; i<variables.size(); i++) {
      Variable v = (Variable) variables.get(i);
      buff.append(v.toStringN());
    }
    return buff.toString();
  }

  /** nicely formatted string representation */
  public String toString() {
    buff.setLength(0);
    buff.append("netcdf "+getPathName()+" {\n");
    buff.append(" dimensions:\n");
    for (int i=0; i<dimensions.size(); i++) {
      Dimension myd = (Dimension) dimensions.get(i);
      buff.append("    "+myd.getName());
      if (myd.isUnlimited())
        buff.append(" = UNLIMITED;   // ("+myd.getLength()+" currently)");
      else
        buff.append(" = "+myd.getLength() +";");
      Variable cv = myd.getCoordinateVariable();
      if (cv != null)
        buff.append("   // (has coord.var)");
      buff.append("\n");
    }

    buff.append("\n variables:\n");
    for (int i=0; i<variables.size(); i++) {
      Variable v = (Variable) variables.get(i);
      buff.append(v.toString());
    }

    buff.append("\n // Global Attributes:\n");
    Iterator iter = getGlobalAttributeIterator();
    while (iter.hasNext()) {
      Attribute att = (Attribute) iter.next();
      buff.append("    :"+att.getName());
      //buff.append(" "+att.getValueType());
      if (att.isString())
        buff.append(" = \""+att.getStringValue()+"\"");
      else if (att.isArray()) {
        buff.append(" = ");
        for (int i=0; i<att.getLength(); i++) {
          if (i != 0) buff.append(", ");
          buff.append(att.getNumericValue(i));
        }
      } else
        buff.append(" = "+att.getNumericValue());
      buff.append(";");
      if (att.getValueType() != String.class) buff.append(" // "+att.getValueType().getName());
      buff.append("\n");
    }

    buff.append("\n}\n");
    return buff.toString();
  }

}

/* Change History:
   $Log: NetcdfFile.java,v $
   Revision 1.1.1.1  2003/12/17 02:06:59  cvswebdev
   MINC Image I/O Plugin

   Revision 1.14  2002/04/22 22:54:56  caron
   use ArrayAdapter2.java

   Revision 1.13  2001/09/14 21:31:40  caron
   add isClosed()

   Revision 1.12  2001/08/10 21:18:28  caron
   add close()

   Revision 1.11  2001/05/01 15:10:33  caron
   clean up VariableStandardized.java; add HTTP access

   Revision 1.10  2001/02/21 21:21:25  caron
   add VariableStandardized

   Revision 1.9  2001/02/06 22:18:33  caron
   final changes for beta .9

   Revision 1.8  2000/12/14 20:49:29  caron
   minor

   Revision 1.7  2000/10/24 21:36:19  caron
   tweak for DODS

   Revision 1.6  2000/09/19 16:25:52  caron
   NetcdfFileWriteable complete, better docs

   Revision 1.5  2000/08/18 04:14:37  russ
   Licensed under GNU LGPL.

   Revision 1.4  2000/08/17 23:44:46  caron
   prepare for c**right clobbering


   Revision 1.2  2000/04/26 21:02:40  caron
   writeable Netcdf files

 */

