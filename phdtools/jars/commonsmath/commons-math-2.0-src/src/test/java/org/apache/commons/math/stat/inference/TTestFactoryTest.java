/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.commons.math.stat.inference;
import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Test cases for the TTestTestFactory.
 *
 * @version $Revision: 762087 $ $Date: 2009-04-05 10:20:18 -0400 (Sun, 05 Apr 2009) $
 */

public class TTestFactoryTest extends TTestTest {

    public TTestFactoryTest(String name) {
        super(name);
    }

    @Override
    public void setUp() {
    	super.setUp();
        testStatistic = TestUtils.getTTest();
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(TTestFactoryTest.class);
        suite.setName("TTestFactory Tests");
        return suite;
    }
}
