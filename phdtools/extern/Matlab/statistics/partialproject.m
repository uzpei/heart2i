function [xp, eigenmatrix, evalues, W] = partialproject(x, X, L)
    Xmean = mean(X, 2);
    [eigenmatrix, ~, evalues] = eigendecompose(X, Xmean);
    [xp, W] = pcaproject(X, Xmean, x, eigenmatrix, evalues, find(L));
end