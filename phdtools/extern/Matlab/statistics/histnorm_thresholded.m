function [heights, centers] = histnorm_thresholded(data, nbins, stdf, varargin)

    % Apply threshold
    if (nargin > 3)
        ngin = numel(varargin);
        for i=1:ngin
            if (strcmpi(varargin{i}, 'limits'))
                limits = varargin{i+1};
                
                data = data(data >= limits(1) & data <= limits(2));
                
                inds = 1:ngin;
                inds([i, i+1]) = [];
                varargin = varargin(inds);
                break;
            end
        end
    end

    [heights,centers] = hist(data(:), nbins);

    % Count bins to find bounds
%     bounds = [centers(1), centers(end)];
   
    thresh = 0.005 * max(heights(:));
    imin = 1;
    imax = numel(centers);
    
    % Find min bound from left
    for bin=1:numel(centers)
        val = heights(bin);
        if (val < thresh)
            imin = bin;
        else
            break;
        end
    end

    % Find max bound from right
    for bin=numel(centers):-1:1
        val = heights(bin);
        if (val < thresh)
            imax = bin;
        else
            break;
        end
    end
%     [imin, imax]
%     centers(imin)
%     centers(imax)    
    % Apply Sturges' formula to get # of bins
%     histnorm(data(imin:imax), nbins, stdf);
    [heights,centers] = histnorm(data(data > centers(imin) & data < centers(imax)), nbins, stdf, varargin{:});
    