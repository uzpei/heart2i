% function [xp, W] = pcaproject(X, x, V, mask)
% Project the (masked) data vector x onto the eigenbasis V
% (eigenvectors of the covariance matrix).
% mask is an array of indices to use
% X is the data matrix
% x is an observation
% Q is the eigenbasis
% xp is the projected observation onto the basis
% W are the projected weights
function [xp, W] = pcaproject(X, meanX, x, Q, D, varargin)

if (nargin == 6)
    nu = varargin{1};
    Lmask = ones(size(x));
elseif (nargin == 7)
    nu = varargin{1};
    Lmask = varargin{2};
else
    Lmask = ones(size(x));
    nu = 0;
end

% Data size
[m, n] = size(Q);

% Subtract mean and apply mask
x = x - meanX;

%Lmask
% size(x)
% size(Lmask)
x = x(Lmask);

%numel(x)

% Weights are found from an SVD of the partial eigenvector matrix
% see Blanz, Vetter, Freiburg 2001 (Partial Reconstruction Eigen...)
% size(V(mask,:))
% size(D)
%LQ = Q(Lmask, :) * diag(D);
LQ = Q(Lmask, :);

if (nu > eps)
%    fprintf('Computing regularized inverse of Q...\n');
    % Compute the SVD of Q
%    size(LQ)
    %tic
    [U, S, V] = svds(LQ, n);
    %toc

    % Extract singular values
    Sv = diag(S);

    % Compute smoothed inverse
%    Sv'
    Svs = Sv ./ (Sv .^ 2 + nu);
%    Svs'
    
    % Fill in Sin matrix
    Sinv = zeros(size(S'));
    Sinv(logical(eye(size(Sinv)))) = Svs(:);

%    Sinv
    
    W = (V * Sinv * U') * x;
else
%    fprintf('Computing inverse of Q...\n');
    W = pinv(LQ) * x;
end
% Compute reconstructed volume
% xp = zeros(m,1);
% for i=1:n
% %      xp = xp + W(i) * Q(:,i) * D(i);
%     xp = xp + W(i) * Q(:,i);
% end
%W = W ./ sum(W(:));
xp = Q * W;

% Add mean
xp = xp + meanX;
%xp = meanX;

