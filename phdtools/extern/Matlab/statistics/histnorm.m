function [heights, centers] = histnorm(data, nbins, stdf, varargin)

    [heights,centers] = hist(data(:), nbins);
    
    if (stdf > 0)
       npdf = normpdf(-3*stdf:3*stdf,0,stdf);
       heights = conv(heights, npdf, 'same');
    end
 
    % Normalize histogram such that area under the curve is exactly 1
    % (use trapz since it is more accurate than sum)
%   heights = heights/sum(heights);
%    trapz(centers, heights ./ trapz(centers, heights))
    heights = heights ./ trapz(centers,heights);
    
    if (nargin > 3)
        if (strcmpi(varargin{1}, 'DisplayOff') == true)
            return
        else
            plot(centers, heights, varargin{:});
        end
    else
        plot(centers, heights, varargin{:});
    end
 