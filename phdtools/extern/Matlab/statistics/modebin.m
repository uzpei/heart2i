function mde = modebin(data, nbins, stdf)
    [heights, centers] = histnorm(data, nbins, stdf, 'Display', false);
    
    % Return the max bin
    mde = max(heights(:));
end