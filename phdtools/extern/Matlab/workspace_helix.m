s = pi/100;
% s = pi/10;
t = 0:s:12*pi;
a = 0.5;
b = 0.5;
c = 0.05;
plot3(a*sin(b*t),a*cos(b*t),-0.5 + c*t, '-k', 'LineWidth',2)
%plot3t(a*sin(b*t),a*cos(b*t),-0.5 + c*t, 5)
%xlabel('sin(t)')
%ylabel('cos(t)')
%zlabel('t')
grid off
%axis square
axis off
axis([-1 1 -1 1 0 1]);
export_fig('helix.pdf', '-q101', '-transparent', '-nocrop');

%%
figure(1)
clf
s = pi/10;
% s = pi/10;
t = 0:s:12*pi;
a = 0.5;
b = 0.5;
c = 1;
h = plot3t(a*sin(b*t),a*cos(b*t),-0.5 + c*t, 0.05, 'k', 2)
set(h, 'FaceLighting','phong','SpecularColorReflectance', 0, 'SpecularExponent', 50, 'DiffuseStrength', 1);
%view(3); axis equal;
% Set the material to shiny and enable light
material shiny; camlight;

%shading interp
% plot the first line
%h=plot3t(x,y,z,5);
view(0, 30)