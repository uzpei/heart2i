%% CIJK COMPUTATION TIME


%% INPAINTING METHODS VS ERRORS
% vector interpolation (VI)
% affine inpainting (AI)
% rule-based (RB)
% vector diffusion (VD)

% This is only for the first heart
% AVI=[11.866217740117264, 16.99141117456842, 18.889894599900362, 19.55256104763306, 24.492348388156277, 25.82228277113456];
% ARB=[37.365804686153, 37.63099290614352, 37.48737010881727, 37.57943545521183, 37.41256512364614, 37.523872355832246];
% AVD=[10.40898343560662, 14.087376435069656, 15.873820622281508, 16.77495695363614, 21.351789793347805, 23.2870439053104];
% AAI=[10.326821611927615, 11.410947721694155, 12.69701331062186, 14.551492529712041, 19.68886756915775, 25.320591852298016];
% This is for all hearts
AVD=[10.40898343560662, 14.087376435069656, 15.87382062228151, 16.774956953636142, 21.351789793347805, 23.287043905310398];
AVI=[11.866217740117264, 16.991411174568423, 18.889894599900362, 19.55256104763306, 24.492348388156277, 25.822282771134564];
ARB=[37.365804686153, 37.630992906143526, 37.48737010881726, 37.57943545521183, 37.41256512364615, 37.523872355832246];
AAI=[10.326821611927617, 11.410947721694154, 12.697013310621859, 14.551492529712041, 19.688867569157747, 25.320591852298016];

PVI = [24.48530287443162 22.26204441815162 23.73319254073954 22.686763479024673 ];
PVD = [22.414309737478554 20.234125662920935 21.48109602270424 20.529607710584557 ];
PRB = [41.73125945783206 39.48114311703608 37.256209716930336 37.77828879163024 ];
PAI = [19.430066194354886 19.8843426784327 20.066551627942232 20.64504452427292 ];

NVI = [21.901778977719783 23.429472625753377 22.13405414333324 20.851364058757426 22.162961575248413 21.205826381329484 ];
NVD = [19.453828603090503 21.04103665357106 20.154594052569223 18.920177781530274 19.94400107730218 19.30799551544115 ];
NRB = [39.1423324872612 41.57141234901026 37.79111727339141 36.76731751372001 39.78182457072375 36.2060035316581 ];
NAI = [16.16755231881802 17.88749316552985 18.96196552147263 18.683948263617598 19.9668233686821 20.679433833467293 ];

%
clf
hold on
c{1} = colormap(bone);
c{2} = colormap(gray);
c{3} = colormap(pink);

clear data;
clear h;
num_noise = 6;
num_axial = 6;
num_poisson = 4;
ymax = 140;

%labels = { 'VI', 'VD', 'AI', 'RB' };
labels = { 'Vector Interpolation', 'Vector Diffusion', 'Frame Inpainting', 'Rule-Based' };

data{1}{1} = AVI(1:num_axial);
data{1}{2} = PVI(1:num_poisson);
data{1}{3} = NVI(1:num_noise);

data{3}{1} = AAI(1:num_axial);
data{3}{2} = PAI(1:num_poisson);
data{3}{3} = NAI(1:num_noise);

data{4}{1} = ARB(1:num_axial);
data{4}{2} = PRB(1:num_poisson);
data{4}{3} = NRB(1:num_noise);

data{2}{1} = AVD(1:num_axial);
data{2}{2} = PVD(1:num_poisson);
data{2}{3} = NVD(1:num_noise);

m = numel(data); % number of methods
n = numel(data{1}); % number of artifacts

legends = {'Axial', 'Poisson', 'Noise'};
%
% For each method
for j=1:m
    
    % Plot each artifact: poisson, axial, noise
    for i=1:n
        nstacks = numel(data{j}{i});
        
        % Create new stack
%        y = [1:nstacks; zeros(1, nstacks)];
        y = [data{j}{i}; zeros(1, nstacks)];
        h{j}{i} = bar(y,'stacked');      
        
        for k=1:nstacks
            % apply color
           h{j}{i}(k).FaceColor = c{i}(round(size(c{i},1) * k / nstacks),:);       
            
            % shift stacks
           h{j}{i}(k).XData = h{j}{i}(k).XData + i + j * (numel(data{1})+1);
        end
        
    end
end
x0 = h{1}{floor(n/2)+1}(1).XData(1);
dx = n + 1;
set(gca, 'xtick', x0 + [0:m] .* dx)
set(gca, 'xticklabel', labels);
xlabel('Reconstruction Method');
ylabel('Cumulative Error (degrees)');
ha = 'left';
va = 'middle';
angle = 90;
hw = h{1}{1}(1).BarWidth;
for j=1:m
for i=1:n
    x = h{j}{i}(1).XData(1);
    y = sum(data{j}{i});
    
    % clamp text at ymax
    tx = x;
    ty = min(ymax, y);
    
    mu = y / numel(data{j}{i});
    mu = ['$\epsilon_1=$', sprintf('%.2f',mu)];
    text(tx - hw/4,ty + 3, legends{i},'rotation', angle, 'horizontalalignment',ha, 'verticalalignment', va);
    text(tx + hw/4,ty + 3, mu, 'rotation', angle, 'interpreter', 'latex', 'horizontalalignment',ha, 'verticalalignment', va);
    
    if y > ymax
%        text(tx,ty - 30, '...', 'rotation', 90, 'interpreter', 'latex', 'horizontalalignment',ha, 'verticalalignment', 'middle');
        hold on
        lx = [tx-hw/2,tx+hw/2];
        numPts = 5;
        lx = lx(1):(lx(2)-lx(1))/numPts:lx(2);
        plot(lx, ty * ones(1,numel(lx)), 'k.');
        hold off
    end
end
end
ylim([0 ymax])
%
%
cratio = 1/3.5;
current = daspect;
daspect([cratio*current(1) current(2) current(3)]);
export_fig('result_all.pdf', '-transparent');
