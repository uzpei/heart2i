% What is needed:
% -mask (0,1) for visited, unvisited nodes (voxels)
% -fast indexing of 6-neighbors
% -only keep voxels for which mask == 0
% -Propagation step:
% For all available propagation nodes V
% 1 Fetch T, N, B, cijk at current voxel V_i
% 2 Extrapolate T_j at current neighboring point N_ij
%   (for now simply add T_j and then at the end normalize)
%   (eventually can start doing interpolation/weighting depending
%   (on distance from origin: will also need a tuple (distance, T estimate)
% 3 Fetch B from gradient of distance transform
% 4 Orthogonalize T, B
% 5 Compute N = B x T
% repeat 2-5 for each neighbor
% mask(estimated neighbors) = 1
% ---> Need a sample test volume: 2 slices with tight mask

clear all

%% Load data
useRats = true;

if useRats
    dirdata = '../../data/heart/rats/rathearts.mat';
     zbounds = [28, 90];
%    zbounds = [45, 55];

    % Number of z-slices
    zn = 2;
    
    % Compute z indices
    zinds = round(linspace(zbounds(1),zbounds(2), zn))

else
    dirdata = '../../data/heart/canine/doghearts.mat';
    zbounds = [1, 247];

    % Number of z-slices
    zn = 30;
    
    % Compute z indices
    zinds = round(linspace(zbounds(1),zbounds(2), zn))
end

fprintf('Loading heart data...\n');
tic
hearts = loadmat(dirdata);
toc
subjects = fieldnames(hearts);
fprintf('Initializing volumes...\n');

% Remove atlas
subjects = subjects(1:numel(subjects)-1);

%% Set reconstruction parameters
reconSubject = 1;

trainingIndices = 1:length(subjects);
trainingIndices(reconSubject) = [];
trainingSize = length(trainingIndices);

fprintf('Training data from: \n');
subjects(trainingIndices)'

fprintf('Reconstructing subject = %s\n', subjects{reconSubject});

%% Initialize computations
% This could be any heart... We are only interested in the mask at this
% point.
heart = hearts.(char(subjects(reconSubject)));
mask = heart.mask;
[sx, sy, sz] = size(mask);

% This is our ground truth
T = heart.T;
N = heart.N;
B = heart.B;

maskBound = ones(sx, sy, sz);
maskBound(:,:,1:zbounds(1)-1) = 0;
maskBound(:,:,(zbounds(2)+1):end) = 0;

% Get a tighter mask by making sure there is an orientation at each voxel
mask = mask & maskBound & (sqrt(T(:,:,:,1) .^ 2 + T(:,:,:,2) .^ 2 + T(:,:,:,3) .^ 2) > 0);
mask3 = repmat(mask, [1 1 1 3]);

%% Precompute ground truth frames
fprintf('Precomputing all frames...\n');
tic
frames = framefield(T, N, B, true, mask);
T(:,:,:,1) = frames(:,:,:,1);
T(:,:,:,2) = frames(:,:,:,2);
T(:,:,:,3) = frames(:,:,:,3);
N(:,:,:,1) = frames(:,:,:,4);
N(:,:,:,2) = frames(:,:,:,5);
N(:,:,:,3) = frames(:,:,:,6);
B(:,:,:,1) = frames(:,:,:,7);
B(:,:,:,2) = frames(:,:,:,8);
B(:,:,:,3) = frames(:,:,:,9);
orthogonality3d(frames, mask)
toc

%% Preompute curvatures
fprintf('Precomputing all one-forms...\n');
tic
for i=1:length(subjects)
    heart = hearts.(char(subjects(i)));
    hlbl = char(subjects(i));

    hearts.(hlbl).name = hlbl;
    
    % Apply mask
    hearts.(hlbl).T = heart.T .* mask3;
    hearts.(hlbl).N = heart.N .* mask3;
    hearts.(hlbl).B = heart.B .* mask3;
    
    % Compute Jacobian
    DT = jacobian(heart.T);

    % Compute stats
    hearts.(hlbl).TNT = cijv(DT, heart.N, heart.T);
    hearts.(hlbl).TNN = cijv(DT, heart.N, heart.N);
    hearts.(hlbl).TNB = cijv(DT, heart.N, heart.B);
    
    hearts.(hlbl).TBT = cijv(DT, heart.B, heart.T);
    hearts.(hlbl).TBN = cijv(DT, heart.B, heart.N);
    hearts.(hlbl).TBB = cijv(DT, heart.B, heart.B);
end
toc

% Store reconstructed heart
reconstructedHeart = hearts.(char(subjects(reconSubject)));

% Compute partial mask
interpolantMask = zeros(size(mask));
interpolantMask(:,:,zinds) = mask(:,:,zinds);
interpolantMask = logical(interpolantMask);

% Compute interpolation mask
interpolationMask = ~interpolantMask & mask;

dirout = './output/interpolation/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

figure(1)
image3d(interpolantMask)
supertitle('Interpolant');
colormap(customap('redwhiteblue'))
export_fig([dirout, sprintf('mask_interpolant_%i.pdf', zn)], '-transparent', '-q101');

figure(2)
image3d(interpolationMask)
supertitle('Interpolated');
colormap(customap('redwhiteblue'))
export_fig([dirout, sprintf('mask_interpolated_%i.pdf', zn)], '-transparent', '-q101');

%% Start propagation from interpolant mask
% Initial frame volume
Tp = T .* repmat(interpolantMask, [1 1 1 3]);

% TODO: compute B from gradient of distance transform and orthogonalize N
Bp = B;
Np = N .* repmat(interpolantMask, [1 1 1 3]);

% Initial curvature volume
% Order is: TNT, TNN, TNB, TBT, TBN, TBB
cijks = zeros(sx, sy, sz, 6);

% TODO: fill this from PCA instead
cijks(:,:,:,1) = reconstructedHeart.TNT(:,:,:);
cijks(:,:,:,2) = reconstructedHeart.TNN(:,:,:);
cijks(:,:,:,3) = reconstructedHeart.TNB(:,:,:);
cijks(:,:,:,4) = reconstructedHeart.TBT(:,:,:);
cijks(:,:,:,5) = reconstructedHeart.TBN(:,:,:);
cijks(:,:,:,6) = reconstructedHeart.TBB(:,:,:);

% Grid points for converting from indices to coordinates
% (make sure to swap ij -> yx)
[pi, pj, pk] = ndgrid(1:sx, 1:sy, 1:sz);

% 6-neighbor precomputations
volume_neighbors = sixneighbors(size(mask));
waveMask = zeros(size(mask));

for z=zinds
    running = true;
    numits = 40;
    it = 0;

    % Launch propagation
%    propagationMask = double(interpolantMask);
    propagationMask = zeros(size(mask));
    propagationMask(:,:,z) = double(mask(:,:,z));

    while (running)
        it = it + 1;
    %for it=1:numits

        front = find(propagationMask == it);

    %    front = front(1:10);
        % FIXME: Can we avoid for loops for all this indexing?

        % For each point on the wavefront
        tic
        nf = numel(front);

        fprintf('Running propagation iteration %i with %i nodes.\n', it, nf);
        for front_node = front'
            front_coord =  [pi(front_node),pj(front_node),pk(front_node)];

            % Fetch T,N,B, curvatures
            Tf = squeeze(Tp(front_coord(1), front_coord(2), front_coord(3), :));
            Nf = squeeze(Np(front_coord(1), front_coord(2), front_coord(3), :));
            Bf = squeeze(Bp(front_coord(1), front_coord(2), front_coord(3), :));

            % For each neighbor of the wavefront point
            neighbors = volume_neighbors(front_node,:);
            for n = neighbors
                % Only consider if not already part of the propagated volume
                % and not masked
                if ((propagationMask(n) == 0 || propagationMask(n) == it+1) && mask(n))
%                 if (propagationMask(n) == 0 && mask(n))
                    % Update propagation
                    propagationMask(n) = it + 1;
                    waveMask(n) = waveMask(n) + 1;
                    % Extrapolate frame

                    % p is the neighbor position
                    p = [pi(n),pj(n),pk(n)];

                    % Compute neighbor offset
                    h = p - front_coord;

                    % Need to swap XY-> JI
    %                 sw = h(1);
    %                 h(1) = h(2);
    %                 h(2) = sw;

                    TNT = cijks(p(1),p(2),p(3),1);
                    TNN = cijks(p(1),p(2),p(3),2);
                    TNB = cijks(p(1),p(2),p(3),3);
                    TBT = cijks(p(1),p(2),p(3),4);
                    TBN = cijks(p(1),p(2),p(3),5);
                    TBB = cijks(p(1),p(2),p(3),6);

                    sN1 = dot(h, Tf) .* TNT;
                    sN2 = dot(h, Nf) .* TNN;
                    sN3 = dot(h, Bf) .* TNB;
                    sN = sN1 + sN2 + sN3;

                    sB1 = dot(h, Tf) .* TBT;
                    sB2 = dot(h, Nf) .* TBN;
                    sB3 = dot(h, Bf) .* TBB;
                    sB = sB1 + sB2 + sB3;


                    % Compute the extrapolated tangent
                    Th = Tf + sN .* Nf + sB .* Bf;

                    % normalize
                    Th = Th ./ (eps + sqrt(sum(Th .^ 2)));

                    % Store result (use addition)
                    Th = Th + Tp(p(1), p(2), p(3));

                    % normalize
                    Th = Th ./ (eps + sqrt(sum(Th .^ 2)));

                    Tp(p(1), p(2), p(3),1:3) = Th(:);


                    % From now on assume B^ is known
                    % Make sure T, B^ are orthogonal
                    Bh = squeeze(B(p(1), p(2), p(3),1:3));
                    Bh = Bh - dot(Bh,Th) .* Th;
                    Bh = Bh ./ (eps + sqrt(sum(Bh .^ 2)));
                    Nh = cross(Bh, Th);

                    Np(p(1), p(2), p(3),1:3) = Nh(:);
                    Bp(p(1), p(2), p(3),1:3) = Bh(:);
                end
            end
        end
        toc

        if (nf == 0 || it >= numits)
            running = false;
        end
    end
end

% Disp result
figure(1)
image3d(propagationMask)

figure(2)
image3d(waveMask)

figure(3)
image3d(Tp)

figure(4)
image3d(T)
axis image

