function cijk = cijk(Fi, Fj, Fk)
    cijk = cJijv(jacobian(Fi), Fj, Fk);
end