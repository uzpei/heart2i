function [phi, dphi, t] = poisson_solver(V, mask)
%function [phi, t] =poisson_solver(n)

% Dimensions must be even numbers

% Solve in a symmetric grid
n = max(size(mask));

%n=20; 
e = ones(n,1);
L = spdiags([e -2*e e], -1:1, n, n);
I = speye(n);
L3d = kron(I,kron(I,L))+kron(I,kron(L,I))+kron(L,kron(I,I));

%rhs = spalloc(n*n*n,1,10);
%rhs(n*n*n/2-n*n/2-n/2) = -1; % source term
rhs = spalloc(numel(mask), 1, numel(find(mask==1)));
% Fill-in sparse matrix
rhs(mask) = V(mask);

% use biconjugate gradients method
tic
sol = bicg(L3d, rhs, 1e-8, 200);
t=toc;

phi = reshape(sol,[n,n,n]);
[phix, phiy, phiz] = gradient(phi);
dphi = assemble(phix, phiy, phiz);
