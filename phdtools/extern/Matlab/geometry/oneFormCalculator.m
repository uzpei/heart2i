clear all
close all

% One-form computations
% Compute the 9 one-forms c_{ijk} from 
% a frame field

useCurl = false;
useRegions = true;
useRat = true;

if (useCurl)
    elabel = 'curl_';
else
    elabel = 'norm_';
end

if (useRat)
    dirdata = '../../data/heart/rats/rat07052008/registered/processed/';
    dirout = './output/oneform_rat/';
    zrange = 25:85;
    smoothingParameters = {0.3, 1};
else
    dirdata = '../../data/heart/canine/CH06/registered/cropped/processed/';
    dirout = './output/oneform_dog/';
    smoothingParameters = {3, 1};
end

xlimits=[-0.5, 0.5];

if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

fprintf('Loading in matrices...\n');
tic

if (useCurl)
    [T, N, B, mask, C] = curlfield(dirdata);
else
    [T, N, B, mask] = opendtmri(dirdata);
end

% Crop all volumes
if (useRat)
    T = T(:,:,zrange,:);
    N = N(:,:,zrange,:);
    B = B(:,:,zrange,:);
    mask = mask(:,:,zrange);
end

sx = size(mask,1);
sy = size(mask,2);
sz = size(mask,3);


% Save axis
figure(1)
image3d(T);
colormap hot
export_fig([dirout, elabel, 'vecTcolor.pdf'], '-transparent', '-q101');
quiverslice(mask, T);
export_fig([dirout, elabel, 'vecT.pdf'], '-transparent', '-q101');

image3d(N);
export_fig([dirout, elabel, 'vecNcolor.pdf'], '-transparent', '-q101');
quiverslice(mask, N);
export_fig([dirout, elabel, 'vecN.pdf'], '-transparent', '-q101');

clf
image3d(B);
export_fig([dirout, elabel, 'vecBcolor.pdf'], '-transparent', '-q101');
quiverslice(mask, B);
export_fig([dirout, elabel, 'vecB.pdf'], '-transparent', '-q101');

toc

%% Compute Jacobian of T, N, B
fprintf('Computing Jacobian of T, N, B\n');
tic

% Apply mask
%T = T .* repmat(mask, [1 1 1 3]);
DT = jacobian(T);
DN = jacobian(N);
DB = jacobian(B);
%image3d(T(:,:,:,1));
toc;

%% Compute all 9 one-forms for each region
fprintf('Computing one-forms\n');

tic
cijk = { ...
    cijv(DT, N, T), cijv(DT, N, N), cijv(DT, N, B), ...
    cijv(DT, B, T), cijv(DT, B, N), cijv(DT, B, B), ...
    cijv(DN, B, T), cijv(DN, B, N), cijv(DN, B, B) ...
    };
clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
toc

%image3d(
%image3d(cijk.('TNB'));

%% Compute regions
if (useRegions)
    tic
    fprintf('Computing regions...\n');
    % Z-slices
    % Angular slices
%    m = 10;
%    n = 10;
    m = 15;
    n = 6;
    
    % subplot dimensions
    % subm * subn = m * n
    subm = 9;
    subn = 10;
    
    numregions = m * n;
    [regions, regionindices] = volumeregions3d(mask, m, n);
    image3d(regions);

    cData = colormap(hot(m*n));
    colormap(cData);
    cData = reshape(cData, [1, length(cData), 3]);
    export_fig([dirout, elabel, 'regions.pdf'], '-transparent', '-q101');
    toc
end

%% Draw hist
nbins = 500;
cbins = 100;

for i=1:9
    subplot(3,3,i);
    hdata = cijk{i};
    hdata = hdata(mask);
%    hist(hdata(:), 100)
    hdata = hdata(:);
    hdata(hdata < xlimits(1)) = [];
    hdata(hdata > xlimits(2)) = [];
    
    [heights,centers] = hist(hdata(:), nbins);
    heights = heights / trapz(centers,heights);
    plot(centers, heights, 'LineWidth', 1, 'Color', 'k', 'LineStyle', '-');

    xlim(xlimits)
    title(clabels{i});
end
export_fig([dirout, elabel, 'cijk.pdf'], '-transparent', '-q101');

clf
if (useRegions)
    % For each one-form
    for i=1:9
        figure(1);
        clf
        
        hdata = cijk{i};
        hdata = hdata(mask);
        hdata = hdata(:);
        hdata(hdata < xlimits(1)) = [];
        hdata(hdata > xlimits(2)) = [];
        cmean = mean(hdata);
        cstd = std(hdata);

        % Look at all regions
        for region = 1:numregions
            sh = subplot(subm, subn, numregions - region + 1);

            % Apply threshold based on region labelling
            regiondata = cijk{i};
            regiondata = regiondata(mask & regions == region);
            regiondata = regiondata(:);
            regiondata(isnan(regiondata)) = [];
            regiondata(regiondata < xlimits(1)) = [];
            regiondata(regiondata > xlimits(2)) = [];

%            numel(regiondata)

            % Plot current region
            % normalize
            [heights,centers] = hist(regiondata(:), cbins);
            heights = heights / trapz(centers,heights);
            plot(centers, heights, 'LineWidth', 1, 'Color', 'k', 'LineStyle', '-');

            xlim(xlimits)
            set(gca,'XTick',[])
            set(gca,'YTick',[])
%            whitebg(colormapvs(mod(region, size(colormapvs,1)),:));
%            title(region);
            set(sh,'Color', cData(1, region, :))
            xlimData = xlim;
            ylimData = ylim;
%             line([0 0], [0 max(heights)])

            % Compute some stats
            meanv = mean(regiondata);
            
            % Draw the zero, mean, and std lines
            line([0 0], [0 ylimData(2)], 'Color', 'k')
            line([cmean cmean], [0 ylimData(2)], 'Color', 'b')
            line([cstd cstd], [0 ylimData(2)], 'Color', 'm')
            line([-cstd -cstd], [0 ylimData(2)], 'Color', 'm')
            
            % Print text (mean, num voxels, etc.)
            th = text(0.8 * xlimData(1), 0.9 * ylimData(2), ['\mu:', num2str(meanv)]);
            set(th, 'FontSize', 6, 'Color', [0.4, 0.4, 0.4]);
            
            th = text(0.8 * xlimData(1), 0.7 * ylimData(2), ['n:', num2str(numel(regiondata(:)))]);
            set(th, 'FontSize', 6, 'Color', [0.4, 0.4, 0.4]);

            % Region ID
            th = text(0.8 * xlimData(2), 0.8 * ylimData(2), ['#', num2str(round(region / n))]);
            set(th, 'FontSize', 6, 'Color', [0.4, 0.4, 0.4]);
        end
        
        supertitle(clabels{i});
        export_fig([dirout, elabel, 'c_', clabels{i}, '.pdf'], '-transparent', '-q101');
    end
end

%% Test
%plot(centers, heights, 'LineWidth', 1, 'Color', 'k', 'LineStyle', '-');

%% Compute the full one-form extrapolation
% cTN = cij(DT, N);
% cTB = cij(DT, B);
% h = zeros(sx, sy, sz, 3);
% 
% % Compute the error in a n^3 neighborhood
% onesv = ones(sx, sy, sz);
% error_volume = zeros(sx, sy, sz);
% slice = 50;
% axisn = 2;
% cTNhN = zeros(sx, sy, sz, 3);
% cTBhB = zeros(sx, sy, sz, 3);
% 
% tic
% n = 1;
% num_neighbors = (2 * n + 1)^3;
% for i = -n:n
%     for j = -n:n
%         for k = -n:n
% %            [i, j, k]
%             
%             % 0.27 whole iteration
%             
%             % Compute the current offset for all voxels
%             
%             h(:,:,:,1) = i * onesv;
%             h(:,:,:,2) = j * onesv;
%             h(:,:,:,3) = k * onesv;
%             
%             % Compute extrapolation at neighbor offset
%             
%             cTNh = dot(cTN, h, 4);
%             cTBh = dot(cTB, h, 4);
% 
%             % Compute the extrapolated tangent
%              dt = T + repmat(cTNh, [1 1 1 3]) .* N + repmat(cTBh, [1 1 1 3]) .* B;
%              
%             % normalize
% %             dt = dt ./ sqrt(dt(:,:,:,1) .^ 2 + dt(:,:,:,2) .^ 2 + dt(:,:,:,3) .^ 2);
%             dt = dt ./ (eps + repmat(sqrt(sum(dt .^ 2, 4)), [1 1 1 3]));
% %             subplot(1,2,1);
% %             imagesc(T(:,:,slice,axisn));
% %             caxis([-1 1]);
% %             axis square
% %             subplot(1,2,2);
% %             imagesc(dt(:,:,slice, axisn));
% %             caxis([-1 1]);
% %             axis square
% %             pause(0.1);
% 
%             % Evaluate at the offset h
%             % Need to shift the values from T
%             % to bring it back to -h
%             % Careful: i,j is switched to y,x
%             
%             Th = circshift(T, -[j, i, k, 0]);
%             % Take the acos of the dot product
% %             acosv = acos(abs(dot(Th, dt, 4)));
% %            find(isnan(Th))
% %            find(isnan(dt))
%             
%             % Apply mask
% %            dt = dt .* repmat(mask, [1 1 1 3]);
% %            find(isnan(dt))
%             
% 
%             local_error = acos(abs(dot(Th, dt, 4)));
%             error_volume = error_volume + local_error;
%             
% %            numel(find(local_error > 0))
% %            neighbor_sum = sum(local_error(:)) / (num_voxels * num_neighbors);            
% %            tot_error = tot_error + neighbor_sum;
%         end
%     end
% end
% toc
% error_volume = error_volume / num_neighbors;
% 
% evoxels = rad2deg(abs(error_volume(mask)));
% 
% %% Display error_volume
% figure(2)
% %subplot(1,2,1);
% %imagesc(rad2deg(abs(error_volume(:,:,slice))));
% %colorbar
% %axis square
% 
% % Display histogram
% %subplot(1,2,2);
% hist(evoxels, 200);
% axis square
% 
% hmean = mean(evoxels);
% hstd = std(evoxels);
% title(sprintf('error: mu = %.2f, sigma = %.2f\n', hmean, hstd));
% export_fig([dirout, elabel, 'error_hist.pdf'], '-transparent', '-q101');
% 
% %%
% figure(3)
% image3d(abs(error_volume));
% export_fig([dirout, elabel, 'error.pdf'], '-transparent', '-q101');
