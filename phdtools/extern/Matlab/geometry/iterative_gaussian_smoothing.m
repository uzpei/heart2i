%%%%
% function V = iterative_gaussian_smoothing(V, varargin)
% parameters = iterations, std
function V = iterative_gaussian_smoothing(V, varargin)

options = parseInput(struct('mask', [], 'iterations', 5, 'std', 0.5), varargin{:});

if (options.iterations == 0 || options.std <= 0)
    return;
end

% Use 68%?95%?99.7% rule
gsize = 6 * options.std;

fprintf('-> Iterative smoothing (std %.2f), iteration ', options.std);
for it = 1:options.iterations
    fprintf('%d/%d ', it, options.iterations);
    if (~isempty(options.mask))
        V = normalize(smooth_vector_field(V, options.std, gsize, options.mask));
    else
        V = normalize(smooth_vector_field(V, options.std, gsize));
    end
end
fprintf('. Done\n');
