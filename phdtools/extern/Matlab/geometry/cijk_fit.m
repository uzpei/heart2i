clear all
species = {};
%species(1).path = '../../data/heart/rat/atlas_FIMH2013/';
species(1).path = '../../data/heart/rat/sample1/';
species(1).innerPath = 'registered';

experiment_options = struct2paramcell(struct('ElementOrder', [1 2 3], 'PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));
subjects = compute_frames_con_all('../../data/heart/rat/sample1/', experiment_options{:});

%%
subject = subjects(1);
[sx, sy, sz] = size(subject.mask);

% Precompute frame field
[T, B] = frame_from_distance_transform(subject.e1, subject.dt, subject.mask);
N = cross(B, T);
Fv0 = { T, N, B};

%F = framefield(subject.frame.T, subject.frame.N, subject.frame.B);

%% Construct enumerated frame axis
n = 1;
num_neighbors = (2 * n + 1)^3;
zerog = zeros(sx, sy, sz);

maskInds = find(subject.mask > 0);

% Aall = zeros(81, numel(maskInds));
% Ball = zeros(9, numel(maskInds));
Aall = zeros(81, numel(subject.mask));
Ball = zeros(9, numel(subject.mask));

for i = -n:n
    for j = -n:n
        for k = -n:n
            v = [i; j; k];
            vvt = v * v';

            % Construct offseted frame axes
            Fv{1} = circshift(Fv0{1}, -[v(1), v(2), v(3), 0]);
            Fv{2} = circshift(Fv0{2}, -[v(1), v(2), v(3), 0]);
            Fv{3} = circshift(Fv0{3}, -[v(1), v(2), v(3), 0]);

            % Compute gammas - the frame axis dot products
            gammas = {};
            for gi=1:3 
                for gj=1:3
                    gammas{gi}{gj} = dot(Fv{gi}, Fv0{gj}, 4);
                end
            end

            %% Assemble Gamma
            Gamma = {};
            Gamma{1}{1} = -gammas{1}{1};
            Gamma{1}{2} = gammas{1}{3};
            Gamma{1}{3} = zerog;
            Gamma{2}{1} = zerog;
            Gamma{2}{2} = -gammas{1}{1};
            Gamma{2}{3} = -gammas{1}{2};
            Gamma{3}{1} = zerog;
            Gamma{3}{2} = -gammas{2}{1};
            Gamma{3}{3} = gammas{2}{2};

            gamma = {};
            gamma{1} = gammas{1}{2};
            gamma{2} = gammas{1}{3};
            gamma{3} = gammas{2}{3};

            %% Begin per-voxel computations 
            tic
            % for ind = maskInds'
            for ind = maskInds'
                pGamma = [Gamma{1}{1}(ind), Gamma{1}{2}(ind), Gamma{1}{3}(ind); ...
                    Gamma{2}{1}(ind), Gamma{2}{2}(ind), Gamma{2}{3}(ind); ...
                    Gamma{3}{1}(ind), Gamma{3}{2}(ind), Gamma{3}{3}(ind)];
                A = kron(pGamma' * pGamma, vvt);

                pgamma = [gamma{1}(ind); gamma{2}(ind); gamma{3}(ind)];
                B = v * pgamma';
                B = B(:);
                
                % Sum up
                Aall(:, ind) = Aall(:, ind) + A(:);
                Ball(:, ind) = Ball(:, ind) + B(:);
            end
            toc
        end
    end
end

%% Compute solutions
C = zeros(9, numel(subject.mask));
for ind = maskInds'
    Asumv = Aall(:, ind);
    Bsumv = Ball(:, ind);
    
    C(:, ind) = -pinv(reshape(Asumv, [9 9])) * Bsumv;
end

%% Reassemble volumes
cijks = {};
for i=1:9
    cijk = zeros(sx, sy, sz);
    cijk(:) = C(i, :);
    cijks{i} = cijk; 
end

%%
hold on
for i=1:9
    subplot(3,3,i);
    image3dslice(cijks{i}, 'z');
    colorbar
end