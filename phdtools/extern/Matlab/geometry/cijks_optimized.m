function [cijks, cerrors, mask_used] = cijks_optimized(F, mask, sampling, varargin)

global clbls;
clbls = { 'c121', 'c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233'};

poptions = parseInput(struct('verbose', 'off', 'neighborhood', 1, 'cmask', [1,1,1,1,1,1,1,1,1]), varargin{:});

% global Copti;
% global n;
% global f;

Copti = logical(poptions.cmask);

n = poptions.neighborhood;

maskSize = size(mask);

% cijks optimization
F.N = double(cross(F.B, F.T));

%ierror = compute_connection_extrapolation_error(F, mask, cijks, model)
F11 = F.T(:,:,:,1);
F12 = F.T(:,:,:,2);
F13 = F.T(:,:,:,3);

F21 = F.N(:,:,:,1);
F22 = F.N(:,:,:,2);
F23 = F.N(:,:,:,3);

F31 = F.B(:,:,:,1);
F32 = F.B(:,:,:,2);
F33 = F.B(:,:,:,3);

% C0 = zeros(9, 1);
C0 = zeros(9, 1);

%    Options: Display, TolX, TolFun, MaxFunEvals, MaxIter, FunValCheck,
%    PlotFcns, and OutputFcn.
options.MaxIter = 50;

% error function tolerance
options.TolFun = 1e-4; 

% C_ijk precision to achieve
options.TolX = 1e-4; 

options.Display = poptions.verbose;
%options.Display = 'iter';
options.LargeScale = 'off';

%% Preprocess masked sampling
maskInds = find(mask > 0);
if (numel(sampling) == 1)
    inds = maskInds';
    inds = inds(1:round(sampling * numel(inds)));
else
    inds = sampling';
end

mask_used = logical(zeros(size(mask)));
mask_used(inds) = 1;

%% Precompute index to subindex matrix
ijks = zeros(3, numel(mask));
[is, js, ks] = ind2sub(size(mask), 1:numel(mask));
ijks(1,:) = is(:);
ijks(2,:) = js(:);
ijks(3,:) = ks(:);

%% Precompute subindex to index matrix
subinds = ones(size(mask));
subinds(:) = 1:numel(mask);

%% Precompute frames
Finds = zeros(9, numel(mask));
Finds(1, :) = F11(:);
Finds(2, :) = F12(:);
Finds(3, :) = F13(:);
Finds(4, :) = F21(:);
Finds(5, :) = F22(:);
Finds(6, :) = F23(:);
Finds(7, :) = F31(:);
Finds(8, :) = F32(:);
Finds(9, :) = F33(:);

% for index = 1:numel(mask)
%     Finds(1:3,index) = [F11(index); F12(index); F13(index)];
%     Finds(4:6,index) = [F21(index); F22(index); F23(index)];
%     Finds(7:9,index) = [F31(index); F32(index); F33(index)];
% end

%% Construct volumes
cijksind = zeros(numel(find(Copti)), numel(inds));
errorsind = zeros(1, numel(inds));

fprintf('Running optimization on [%i/%i] voxels...\n', numel(inds), numel(find(mask)));
fprintf('Using connection mask: %s\n', mat2str(Copti));
t_all = tic;

CoreNum = feature('numCores');
if(matlabpool('size'))<=0
    fprintf('Enabling parallel computing with %i threads.\n', CoreNum);
    matlabpool('open','local',CoreNum);
%else
%    fprintf('Parallel Computing enabled with %i threads.\n', matlabpool('size'));
end

C0 = C0(Copti);
fsize = size(F11);
Cv = zeros(1,9);
parfor parfor_index = 1:numel(inds)
    % Voxel index under optimization
    ind = inds(parfor_index);

%    [x2, fval] = fminsearch(@(C) cerror(C, Cv, ind, ijks, subinds, Copti, Finds, n, fsize), C0, options);
    [x2, fval] = fminunc(@(C) cerror(C, Cv, ind, ijks, subinds, Copti, Finds, n, fsize), C0, options);
    
    cijksind(:, parfor_index) = x2(:);
    errorsind(parfor_index) = fval;
%    [x1, x2]
end
topti = toc(t_all);
fprintf('Total time = %.2fs, %.4fs average per voxel\n', topti, topti / numel(inds));

% cijksind(3, :)

% Reconstruct final volumes
[cijks, cerrors] = reconstruct(mask, Copti, cijksind, errorsind, inds);

% Func end
end

function fsum = cerror(C, cijk, ind, ijks, subinds, Copti, Finds, n, fsize)
    % Assume cijks labeled as
    % C(1) = 121, C(2) = 122, C(3) = 123
    % C(4) = 131, C(5) = 132, C(6) = 133
    % C(7) = 231, C(8) = 232, C(3) = 233
    
    % Could also skip the assembling and use all of C
    % and then go C = C .* Copti
%     Cv = zeros(1,9);
    cijk(Copti) = C(:);
    
    % Index (coordinates) of voxel of expansion
    ijk0 = ijks(:, ind);

    % Fetch frame vectors f1, f2, f3 from the voxel of expansion
    f1 = Finds(1:3, ind);
    f2 = Finds(4:6, ind);
    f3 = Finds(7:9, ind);
    
    % function error accumulation
    fsum = 0;
    for ni = -n:n
    for nj = -n:n
    for nk = -n:n
        % The direction is computed as a matrix offset
%         offset = [ni; nj; nk];
%         v = [offset(1), offset(2), offset(3)];
        v = [ni; nj; nk];
        
        % Compute the position of the neighbor located in this direction
        ijk = ijk0 + v;

        % Fetch the corresponding neighboring matrix linear index
        vind = subinds(ijk(1), ijk(2), ijk(3));

        % Decompose the frame vectors onto the contracted direction
        % i.e. compute F1 . v, F2 . v, F3 .v
        vf1 = dot(v, f1);
        vf2 = dot(v, f2);
        vf3 = dot(v, f3);
        
        % Compute connection equations
        s12v = cijk(1) * vf1 + cijk(2) * vf2 + cijk(3) * vf3;
        s13v = cijk(4) * vf1 + cijk(5) * vf2 + cijk(6) * vf3;
        s23v = cijk(7) * vf1 + cijk(8) * vf2 + cijk(9) * vf3;
        
        % Compute extrapolated frame vectors
        f1v_tilde = f1 + s12v .* f2 + s13v .* f3;
        f2v_tilde = f2 - s12v .* f1 + s23v .* f3;
        f3v_tilde = f3 - s13v .* f1 - s23v .* f2;
        
        % Normalization step
        f1v_tilde = f1v_tilde ./ (eps + norm(f1v_tilde));
        f2v_tilde = f2v_tilde ./ (eps + norm(f2v_tilde));
        f3v_tilde = f3v_tilde ./ (eps + norm(f3v_tilde));
        
        % Fetch true neighboring frame vectors
        f1v = Finds(1:3, vind);
        f2v = Finds(4:6, vind);
        f3v = Finds(7:9, vind);

        eps1 = abs(dot(f1v_tilde, f1v));
        eps2 = abs(dot(f2v_tilde, f2v));
        eps3 = abs(dot(f3v_tilde, f3v));
        
        fsum = fsum + eps1 + eps2 + eps3;
%         fsum = fsum + 3 - abs(dot(f1v_tilde, f1v)) + abs(dot(f1v_tilde, f2v)) + abs(dot(f1v0, f3v));
%       fsum = fsum + abs(dot(f1v0, f2v)) + abs(dot(f1v0, f3v));
%         fsum = fsum + 1 - abs(dot(f1v_tilde, f1v));
    end
    end
    end
    fsum = fsum / (2 * n + 1)^3;

end

function [cijks, cerrors] = reconstruct(mask, Copti, cijksind, errorsind, inds)
    global clbls;
    cijks = {};
    cerrors = zeros(size(mask));
    cerrors(inds) = errorsind(:);
    zerov = zeros(size(mask));
    valid_C = find(Copti);
    for i = 1:numel(valid_C)
        cijk = zerov;
        cijk(inds) = cijksind(i, :);
        cijks.(clbls{valid_C(i)}) = cijk; 
    end
end

