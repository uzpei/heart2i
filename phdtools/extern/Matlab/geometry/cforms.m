% labels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
% labels = { '121', '122', '123', '131', '132', '133', '231', '232', '233' };
function cijks = cforms(F, varargin)

    % TODO: add option to store volumes as vectorized mask vectors
    options = parseInput(struct('MaskedVolume', [], 'SinglePrecision', false), varargin{:});

    %% Compute all connection forms
    % clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
    cijks = {};

    % Parse frame field and make sure we work with doubles (DGradient
    % requires this)
    if (strcmpi(class(F), 'double'))
        Fv(1).F = double(F(:,:,:,1:3));
        Fv(2).F = double(F(:,:,:,4:6));
        Fv(3).F = double(F(:,:,:,7:9));
    elseif (length(fieldnames(F)) == 1)
        Fv(1).F = double(F(:,:,:,1:3));
        Fv(2).F = double(F(:,:,:,4:6));
        Fv(3).F = double(F(:,:,:,7:9));
    else
        % Assume we only have T, B
        Fv(1).F = double(F.T);
        Fv(2).F = double(cross(F.B, F.T));
        Fv(3).F = double(F.B);
    end

    for i=1:3
        JFi = jacobian(Fv(i).F);
        for j=(i+1):3
            for k=1:3
                clbl = ind2lbl(i, j, k);
                if (isempty(options.MaskedVolume))
                    
                    if (options.SinglePrecision)
                        cijks.(clbl) = single(cJijv(JFi, Fv(j).F, Fv(k).F));
                    else
                        cijks.(clbl) = cJijv(JFi, Fv(j).F, Fv(k).F);
                    end
                else
                    cijkv = cJijv(JFi, Fv(j).F, Fv(k).F);
                    if (options.SinglePrecision)
                        cijks.(clbl) = single(cijkv(options.MaskedVolume));
                    else
                        cijks.(clbl) = cijkv(options.MaskedVolume);
                    end
                end
            end
        end
    end
end

function lbl = ind2lbl(i, j, k)
    lbl = ['c',c2lbl(i), c2lbl(j), c2lbl(k)];
end

function lbl = c2lbl(c)
%     if (c == 1)
%         lbl = 'T';
%     elseif (c == 2)
%         lbl = 'N';
%     elseif (c == 3);
%         lbl = 'B';
%     else
    if (c == 1)
        lbl = '1';
    elseif (c == 2)
        lbl = '2';
    elseif (c == 3);
        lbl = '3';
    else
        error('Undefined form index');
    end
end


