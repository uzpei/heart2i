function cijk2 = cijk2(Fi, Fj, Fk)
    % Compute c_ij<v>
    %cijv = cij1 .* v(:,:,:,1) + cij2 .* v(:,:,:,2) + cij3 .* v(:,:,:,3);
%    cijv = dot(cij(Ji, Fj), v, 4);

Ji = jacobian(Fi);
u1100 = Ji(:,:,:,1);
u1010 = Ji(:,:,:,2);
u1001 = Ji(:,:,:,3);
u2100 = Ji(:,:,:,4);
u2010 = Ji(:,:,:,5);
u2001 = Ji(:,:,:,6);
u3100 = Ji(:,:,:,7);
u3010 = Ji(:,:,:,8);
u3001 = Ji(:,:,:,9);

Jj = jacobian(Fj);
v1100 = Jj(:,:,:,1);
v1010 = Jj(:,:,:,2);
v1001 = Jj(:,:,:,3);
v2100 = Jj(:,:,:,4);
v2010 = Jj(:,:,:,5);
v2001 = Jj(:,:,:,6);
v3100 = Jj(:,:,:,7);
v3010 = Jj(:,:,:,8);
v3001 = Jj(:,:,:,9);

u1 = Fi(:,:,:,1);
u2 = Fi(:,:,:,2);
u3 = Fi(:,:,:,3);

v1 = Fj(:,:,:,1);
v2 = Fj(:,:,:,2);
v3 = Fj(:,:,:,3);

w1 = Fk(:,:,:,1);
w2 = Fk(:,:,:,2);
w3 = Fk(:,:,:,3);

w121=u3.*(v1.*u1001+v2.*u2001+v3.*u3001)+u2.*(v1.*u1010+v2.*u2010+v3.*u3010)+u1.*(v1.*u1100+v2.*u2100+v3.*u3100);
w122=v3.^2.*u3001+v2.^2.*u2010+v2.*v3.*(u2001+u3010)+v1.^2.*u1100+v1.*(v2.*(u1010+u2100)+v3.*(u1001+u3100));
w123=v1.*(w3.*u1001+w2.*u1010+w1.*u1100)+v2.*(w3.*u2001+w2.*u2010+w1.*u2100)+v3.*(w3.*u3001+w2.*u3010+w1.*u3100);
w131=u3.*(w1.*u1001+w2.*u2001+w3.*u3001)+u2.*(w1.*u1010+w2.*u2010+w3.*u3010)+u1.*(w1.*u1100+w2.*u2100+w3.*u3100);
w132=v3.*(w1.*u1001+w2.*u2001+w3.*u3001)+v2.*(w1.*u1010+w2.*u2010+w3.*u3010)+v1.*(w1.*u1100+w2.*u2100+w3.*u3100);
w133=w3.^2.*u3001+w2.^2.*u2010+w2.*w3.*(u2001+u3010)+w1.^2.*u1100+w1.*(w2.*(u1010+u2100)+w3.*(u1001+u3100));
w231=u3.*(w1.*v1001+w2.*v2001+w3.*v3001)+u2.*(w1.*v1010+w2.*v2010+w3.*v3010)+u1.*(w1.*v1100+w2.*v2100+w3.*v3100);
w232=v3.*(w1.*v1001+w2.*v2001+w3.*v3001)+v2.*(w1.*v1010+w2.*v2010+w3.*v3010)+v1.*(w1.*v1100+w2.*v2100+w3.*v3100);
w233=w3.^2.*v3001+w2.^2.*v2010+w2.*w3.*(v2001+v3010)+w1.^2.*v1100+w1.*(w2.*(v1010+v2100)+w3.*(v1001+v3100));

[sx, sy, sz, nd] = size(u1);
cijk2 = zeros(sx, sy, sz, 9);
cijk2(:,:,:,1) = w121;
cijk2(:,:,:,2) = w122;
cijk2(:,:,:,3) = w123;
cijk2(:,:,:,4) = w131;
cijk2(:,:,:,5) = w132;
cijk2(:,:,:,6) = w133;
cijk2(:,:,:,7) = w231;
cijk2(:,:,:,8) = w232;
cijk2(:,:,:,9) = w233;

% Negate
cijk2 = -cijk2;
end