% Ji is the Jacobian of Fi
% and Fj and v are frame vectors
function cJijv = cJijv(Ji, Fj, v)
    % Compute c_ij<v>
    cJijv = dot(cij(Ji, Fj), v, 4);
end