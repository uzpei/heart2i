function lbl = ind2clbl(i, j, k)
    lbl = [c2lbl(i), c2lbl(j), c2lbl(k)];
end

function lbl = c2lbl(c)
    if (c == 1)
        lbl = 'T';
    elseif (c == 2)
        lbl = 'N';
    elseif (c == 3);
        lbl = 'B';
    else
        error('Undefined form index');
    end
end
