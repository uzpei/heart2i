% order = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
% function cijks = cformplot(F, mask, varargin)
function cijks = cformplot(cijks, mask, varargin)

clf

if (nargin > 2)
    saxis = varargin{1};
    ioverlay = true;
else
%     saxis = 'z';
    ioverlay = false;
end

%% Compute all connection forms
% clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
clbls = { 'c121', 'c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233' };
% cijks = cforms(F);
nbins = 50;
stdf = 0;

for cind=1:9
    if (length(fieldnames(cijks)) < cind)
        continue;
    end
    
    cijkv = cijks.(clbls{cind});
    
    subplot(3,3,cind);
    
    % This call is for getting the figure limits
    [heights, centers] = histnorm_thresholded(cijkv(mask), nbins, stdf);
    
    if (ioverlay)
        cijkslice = volslice(cijkv, saxis);
        imagesc([min(centers(:)), max(centers(:))], [min(heights(:)), max(heights(:))], flipud(cijkslice));
    end
    
    % Use fixed color axis such that all axial volumes have same colormap
    if (ioverlay)
        caxis([min(centers(:)), max(centers(:))]);
    end
    
    hold on
    histnorm_thresholded(cijkv(mask), nbins, stdf, '--k', 'LineWidth',1);
    set(gca,'ydir','normal');
    axis square
%     axis tight
%     title(sprintf('%s, mean = %.2e', clbls{cind}, mean(cijkv(mask))));
    title(sprintf('%s, u = %.4f', clbls{cind}, mean(cijkv(mask))));
    
    if (ioverlay)
        colorbar
    end
end


