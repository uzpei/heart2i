% Data directory
dir = '../data/pig/';

% Set dims
sx = 128;
sy = 128;
sz = 91;

% Load normal pig heart
data = load([dir, 'SS_eigenvectors.mat']);
ne = data.Expression1;
e = reshape(ne, 3, 3, sx, sy, sz);
e1x = squeeze(e(2, 1, :, :, :));
e1y = squeeze(e(1, 1, :, :, :));
e1z = -squeeze(e(3, 1, :, :, :));

data = load([dir, 'SS_eigenvalues.mat']);
nl = data.Expression1;
l = reshape(nl, 3, sx, sy, sz);
l1 = squeeze(l(1,:,:,:));
l2 = squeeze(l(2,:,:,:));
l3 = squeeze(l(3,:,:,:));

data = load([dir, 'SS_binarymask.mat']);
nmask = data.Expression1;
mask = reshape(nmask, sx, sy, sz);

isotropy = [0.703, 0.703, 1];

figure(1)
imagesc(e1x(:,:,30));
colormap gray
colorbar
pause(0.5);

% Work in 2D
slice = 30;
ex = e1x(:,:,slice);
ey = e1y(:,:,slice);

% Taken from:
% http://stackoverflow.com/questions/3277541/construct-adjacency-matrix-in-matlab

% Compute horizontal adjacency 
mat = ex;
[r,c] = size(mat);
mat = [1 2 3; 4 5 6; 7 8 9];

tic
disp('Computing repmat + diagvec');
diagVec1 = repmat([ones(c-1,1); 0],r,1);
diagVec1 = diagVec1(1:end-1);

diagVec2 = ones(c*(r-1),1);

%adj = diag(diagVec1,1) + diag(diagVec2,c);
adjx = diag(diagVec1,1);
adjx = 0.5*adjx-0.5*adjx.';

adjy = diag(diagVec2,c);
adjy = 0.5*adjy-0.5*adjy.';

toc

%mat
%adjx
%adjy
n = numel(ex);
% Compute sparse matrices
[i,j,w] = find(adjx);
Dx = sparse(i,j,w,n,n);

[i,j,w] = find(adjy);
Dy = sparse(i,j,w,n,n);

% Assemble A = [ 0 0 dy;
%                0 0 -dx;
%              -dy dx 0 ]
D = [Dy; -Dx];
zero2n = sparse(2*n,2*n);
zeron = sparse(n, n);
A = [zero2n, D; D', zeron];

size(A)

% Assemble b
%b = [ex(:); ey(:); zeros(numel(ex),1)];
b = [ex(:); ey(:); sparse(n,1)];

tic
v = b \ A;
v = v';
toc

%vx = v(1:numel(ex));
%vy = v((numel(ex)+1):(2*numel(ex)));
%vz = v((2*numel(ex)+1):end);


Av = A * v;
cvx = Av(1:n);
cvy = Av((n+1):(2*n));
cvz = Av((2*n+1):end);
cvmag = sqrt(cvx .^2 + cvy .^2 + cvz .^2);
cvmag = reshape(cvmag,size(ex,1),size(ex,2));


figure(2);

%[idxi,idxj] = meshgrid(1:size(ex,1),1:size(ex,2));
[idxi,idxj] = meshgrid(1:size(ex,1),1:size(ex,2));

hold on; 
imagesc(cvmag);
%quiver(idxi(:),idxj(:),v(1:n),v(n+1:2*n), 'b'); 

quiver(idxi(:),idxj(:),ex(:),ey(:), 'r'); 
hold off;
set(gca,'YDir','normal')
daspect([1 ,1 ,1])
colorbar
colormap gray