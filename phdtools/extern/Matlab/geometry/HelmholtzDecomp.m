dir = '../data/newmouse/highresolution/';
evfile = 'eigenvectors_hr.mat';
lfile = 'eigenvalues_hr.mat';
mfile = 'binarymask_hr.mat';
dirout = 'processed/';

% Set dims
sx = 120;
sy = 120;
sz = 120;

% Load normal pig heart
data = load([dir, evfile]);
ne = data.Expression1;
e = reshape(ne, 3, 3, sx, sy, sz);
e1x = squeeze(e(2, 1, :, :, :));
e1y = squeeze(e(1, 1, :, :, :));

figure(1)
imagesc(e1x(:,:,30))

% Work in 2D
slice = 30;
ex = e1x(:,:,slice);
ey = e1y(:,:,slice);

% Taken from:
% http://stackoverflow.com/questions/3277541/construct-adjacency-matrix-in-matlab

% Compute horizontal adjacency 
mat = ex;
[r,c] = size(mat);
mat = [1 2 3; 4 5 6; 7 8 9];

tic
disp('Computing repmat + diagvec');
diagVec1 = repmat([ones(c-1,1); 0],r,1);
diagVec1 = diagVec1(1:end-1);

diagVec2 = ones(c*(r-1),1);

%adj = diag(diagVec1,1) + diag(diagVec2,c);
adjx = diag(diagVec1,1);
adjx = 0.5*adjx-0.5*adjx.';

adjy = diag(diagVec2,c);
adjy = 0.5*adjy-0.5*adjy.';

toc

%mat
%adjx
%adjy

% Compute sparse matrices
[i,j,w] = find(adjx);
Dx = sparse(i,j,w,numel(ex),numel(ex));

[i,j,w] = find(adjy);
Dy = sparse(i,j,w,numel(ey),numel(ey));

% Assemble A = [ Dx; Dy ]
A = [Dx; Dy];

size(A);

% Assemble b
b = [ex(:); ey(:)];

tic
u = b \ A;
toc

% Compute inverse
% tic
% disp('Computing pseudoinverse...');
% [u,s,v] = svds(A); 
% Ainv = u*s^-1*v';
% toc

% u = Ainv * b;

u = reshape(u,size(ex,1),size(ex,2));

[cx,cy] = gradient(u);

figure(2);

%[idxi,idxj] = meshgrid(1:size(ex,1),1:size(ex,2));
[idxi,idxj] = meshgrid(1:size(ex,1),1:size(ex,2));

hold on; 
imagesc(u);
quiver(idxi(:),idxj(:),cx(:),cy(:), 'b'); 

quiver(idxi(:),idxj(:),ex(:),ey(:), 'r'); 
hold off;
set(gca,'YDir','normal')
daspect([1 
