clear all
close all

r = -1 : .1 : 1;
[X, Y] = meshgrid(r, r);
ex = Y;
ey = -X;
z = zeros(size(ex));
%z = x.^2 - 3*sin(x.*y);
ez = curl(X,Y, ex, ey);
ex = zeros(size(ex));
ey = zeros(size(ex));

figure(1);
%[dx, dy] = gradient(z);
%quiver(X,Y,dx,dy);
quiver(X,Y,ex,ey);

% Taken from:
% http://stackoverflow.com/questions/3277541/construct-adjacency-matrix-in-matlab

% Compute horizontal adjacency 
mat = ex;
[r,c] = size(mat);
mat = [1 2 3; 4 5 6; 7 8 9];

tic
disp('Computing repmat + diagvec');
diagVec1 = repmat([ones(c-1,1); 0],r,1);
diagVec1 = diagVec1(1:end-1);

diagVec2 = ones(c*(r-1),1);

%adj = diag(diagVec1,1) + diag(diagVec2,c);
adjx = diag(diagVec1,1);
adjx = 0.5*adjx-0.5*adjx.';

adjy = diag(diagVec2,c);
adjy = 0.5*adjy-0.5*adjy.';

toc

%mat
%adjx
%adjy
n = numel(ex);
% Compute sparse matrices
[i,j,w] = find(adjx);
Dx = sparse(i,j,w,n,n);

[i,j,w] = find(adjy);
Dy = sparse(i,j,w,n,n);

% Assemble A = [ 0 0 dy;
%                0 0 -dx;
%              -dy dx 0 ]
D = [Dy; -Dx];
zero2n = sparse(2*n,2*n);
zeron = sparse(n, n);
A = [zero2n, D; D', zeron];

size(A)

% Assemble b
%b = [ex(:); ey(:); zeros(numel(ex),1)];
b = [ex(:); ey(:); ez(:)];

tic
v = b \ A;
v = v';
toc

%vx = v(1:numel(ex));
%vy = v((numel(ex)+1):(2*numel(ex)));
%vz = v((2*numel(ex)+1):end);


Av = A * v;
cvx = Av(1:n);
cvy = Av((n+1):(2*n));
cvz = Av((2*n+1):end);
cvmag = sqrt(cvx .^2 + cvy .^2 + cvz .^2);
cvmag = reshape(cvmag,size(ex,1),size(ex,2));

figure(2);

%[idxi,idxj] = meshgrid(1:size(ex,1),1:size(ex,2));
[idxi,idxj] = meshgrid(1:size(ex,1),1:size(ex,2));

hold on; 
imagesc(cvmag);
quiver(idxi(:),idxj(:),Av(1:n),Av(n+1:2*n), 'b'); 

quiver(idxi(:),idxj(:),ex(:),ey(:), 'r'); 
hold off;
set(gca,'YDir','normal')
daspect([1 ,1 ,1])
colorbar
colormap gray