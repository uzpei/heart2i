% Assume isotropic gaussian for now... 
% TODO: check whether gstd and gsize are 1-dim or 3-dim
function Vout = smooth_vector_field(V, gstd, gsize, varargin)

gsize = max(2, gsize);

if (nargin > 3)
    mask = double(varargin{1});
    h = fgaussian3([gsize,gsize,gsize], [gstd, gstd, gstd], 'normalize', false);

    % Use this to normalize over number of neighborhing voxels.
    Gmask = imfilter(mask,h,'replicate');
    
    Vx = imfilter(V(:,:,:,1), h, 'replicate') ./ Gmask;
    Vy = imfilter(V(:,:,:,2), h, 'replicate') ./ Gmask;
    Vz = imfilter(V(:,:,:,3), h, 'replicate') ./ Gmask;
    
else
    h = fgaussian3([gsize,gsize,gsize], [gstd, gstd, gstd], 'normalize', true);
    Vx = imfilter(V(:,:,:,1), h, 'replicate');
    Vy = imfilter(V(:,:,:,2), h, 'replicate');
    Vz = imfilter(V(:,:,:,3), h, 'replicate');
end

Vout = assemble(Vx,Vy,Vz);

