function cij = cij(Ji, Fj)
%     Fj1 = Fj(:,:,:,1);
%     Fj2 = Fj(:,:,:,2);
%     Fj3 = Fj(:,:,:,3);

    % Compute c_ij
    % Assuming J = 
    % (1) dFx/dx, (2) dFx/dy, (3) dFx/dz
    % (4) dFy/dx, (5) dFy/dy, (6) dFy/dz
    % (7) dFz/dx, (8) dFz/dy, (9) dFz/dz
     cij = zeros(size(Fj));
%      cij(:,:,:,1) = Fj1 .* Ji(:,:,:,1) + Fj2 .* Ji(:,:,:,4) + Fj3 .* Ji(:,:,:,7);
%      cij(:,:,:,2) = Fj1 .* Ji(:,:,:,2) + Fj2 .* Ji(:,:,:,5) + Fj3 .* Ji(:,:,:,8);
%      cij(:,:,:,3) = Fj1 .* Ji(:,:,:,3) + Fj2 .* Ji(:,:,:,6) + Fj3 .* Ji(:,:,:,9);

    % Need to be a little careful here. The Jacobian is computed in spatial
    % coordinates (xyz) but we are performing the dot product in matrix
    % coordinates (ijk) so we need to swap the volume.

     cij(:,:,:,1) = dot(Fj, Ji(:,:,:,[1 4 7]), 4);
     cij(:,:,:,2) = dot(Fj, Ji(:,:,:,[2 5 8]), 4);
     cij(:,:,:,3) = dot(Fj, Ji(:,:,:,[3 6 9]), 4);

end