%% Output histograms
figure(1)
nbins = 100;
stdf = 2;
xfig = get(gcf, 'Position');
xt = round(0.01 * xfig(3));
yt = round(0.79 * xfig(4));
tfsize = 28;
clbls = { 'TNB' };

for cindex = 1:length(clbls)
    hist_data = subjects_combined.(clbls{cindex});
    histnorm_thresholded(hist_data, nbins, stdf, '-k', 'LineWidth',2);
    
    t1 = text(xt, yt - 0,  sprintf('mean %.4f', mean(hist_data)), 'Units', 'Pixels');
    t2 = text(xt, yt - tfsize,  sprintf(' std %.4f', std(hist_data)), 'Units', 'Pixels');
    set(t1, 'FontName', 'Monospaced', 'FontSize', tfsize);
    set(t2, 'FontName', 'Monospaced', 'FontSize', tfsize);
    set(gca, 'FontSize', tfsize);
    pause(0.1);
    export_fig('.', ['c_', clbls{cindex}, '_histo_norm.pdf'], '-transparent', '-q101', '-nocrop');
end
