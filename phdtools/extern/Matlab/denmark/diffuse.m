function Id = diffuse(I_poluted, omega, stds, numIts)

%% Test diffusion
Id = I_poluted;

dilation = 0;

for t=1:numIts
    % Use a Gaussian smoothed before computing
    if (stds > 0)
        h = fspecial('gaussian', [3 3], stds);
        I_filtered = imfilter(Id, h, 'replicate');
    else
        I_filtered = Id;
    end

    % Fill omega + boundary with filtered data
%     omegaOut = imdilate(omega, strel_ball(dilation));
%     Id(omegaOut) = I_filtered(omegaOut);

    % Computation image
    Id(omega) = I_filtered(omega);

    % Compute differential quantities
    Ix = DGradient(Id, [], 1);
    Iy = DGradient(Id, [], 2);
    dI2 = Ix.^2 + Iy.^2;
%     Ixx = DGradient(Ix, [], 1);
%     Iyy = DGradient(Iy, [], 2);
% %     dI = sqrt(dI2);
%     
%     % Diffusion coefficient (based on Perona and Malik)
%     c = 1 ./ (1 + dI2);
% %     c = exp(-dI2);
%     cx = DGradient(c, [], 1);
%     cy = DGradient(c, [], 2);
%     
%     % Compute <dc, di>
%     dcdi = cx .* Ix + cy .* Iy;
%     
%     % Compute the anisotropic diffusion update
%     dIdt = dcdi + c .* (Ixx + Iyy);
    Nx = Iy;
    Ny = -Ix;
    Nxx = DGradient(Nx, [], 1);
    Nyy = DGradient(Ny, [], 2);
    kappa = sqrt(Nxx .^2 + Nyy .^2);
    dIdt = kappa .* dI2;

    [Ii, Ij, V] = find(omega);
    for p=1:length(V)
        i = Ii(p);
        j = Ij(p);
        Id(i,j) = Id(i,j) + dIdt(i,j);
    end
end
