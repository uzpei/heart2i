setpath;

%% Load the image
% I = double(rgb2gray(imread('peppers.png'))) / 255.0;
% [sx, sy] = size(I);
sx = 128;sy = sx;
dimLength = sqrt(sx^2+sy^2);
inner = 1/8 * dimLength;
outer = 1.05 * inner;
% Eccentricity
ecc = 1.5;
% Construct two ellipses
[X, Y] = meshgrid(1:sy, 1:sx);
e1 = sqrt((X - sy/4).^2+((Y - sx/2)/ecc).^2);
e2 = sqrt((X - 3*sy/4).^2+((Y - sx/2)/ecc).^2);
I = 0.5 * ((e1 > outer & e2 > outer));
clf;imshow(I)

% %% Polute our image
% paintmask = zeros(size(I));
% textobject = vision.TextInserter('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
% textobject.Color = [255, 255, 255]; % [red, green, blue]
% textobject.FontSize = 28;
% for y = 1:round(sy / 12):sy
%     textobject.Location = [10 y]; % [x y]
%     paintmask = step(textobject, paintmask);
% end
% paintmask = logical(paintmask);

dimLength = sqrt(sx^2+sy^2);
inner = 1/8 * dimLength;
outer = 1.2 * inner;
[X, Y] = meshgrid(1:sy, 1:sx);
paintmask = sqrt(((X - sy/2)/ecc).^2+(Y - sx/2).^2);
paintmask = paintmask > inner & paintmask < outer;

%
I_polluted = I + paintmask;

subplot(121);
imshow(I);
subplot(122);
imshow(I_polluted);

%% Test diffusion
tic
Id = diffuse(I_polluted, paintmask, stds, 25);
toc
subplot(121);imshow(Id)
subplot(122);imshow(Id .* paintmask)

%% Test inpainting
tic
It = inpaint(Id, paintmask, 0.05, 0, 100);
toc
subplot(121);imshow(It)
subplot(122);imshow(It .* paintmask)

%%
% TODO: need to normalize our gaussians!
% since we are not using the entire neighborhood (see 3D filtering code)
stds = 0.4;
h = fspecial('gaussian', [3 3], stds);
If = imfilter(I, h, 'replicate');
imshow(If);

%% Test pipeline
% Start by running a few diffusion iterations
stds = 0.5;
dt = 0.05;
Id = diffuse(I_polluted, paintmask, stds, 2);
error_previous = 0;
for it=1:500
    numDiff = 1;
    numInpaint = 15;
    
    Id = inpaint(Id, paintmask, dt, 0, numInpaint);
    Id = diffuse(Id, paintmask, stds, numDiff);
%    Id(Id > 1) = 1;
    
    if (mod(it, 10) == 0)
        subplot(121);imagesc(Id); axis image; caxis([0 1]);
%         subplot(122);imshow(Id .* paintmask)
        subplot(122);imagesc(abs(Id - I)); axis image; caxis([0 1]);
        supertitle(['Iteration ', num2str(it)]);
        pause(0.5);
    end
    
    error = norm(I(paintmask)-Id(paintmask)) / numel(find(paintmask));
    disp(error_previous - error);
%     if (abs(error_previous - error) < 1e-8)
%         break;
%     end
    
    error_previous = error;
end
%%
subplot(121);imagesc(bck-bck2);
subplot(122);imagesc(bck2);