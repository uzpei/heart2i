setpath;

%filepath = '../../data/heart/rat/atlas_FIMH2013/rat07052008/registered_clean/';
filepath = '../../data/heart/pig/dataset2/';
[e1, ~, ~, mask, dtEndo, dtEpi] = opendtmri(filepath);

% Compute volume labels
closingMagnitude = 7;
[exterior, muscle, interior, labels] = mask_processor(mask, closingMagnitude);
figure(1);clf;image3d(labels);
[sx, sy, sz] = size(mask);

%% Find principal axes
dt = double(bwdist(exterior));

% First find the principal axis of the whole muscle.
% This will be useful for selecting eigenvectors
[P_muscle, X0_muscle] = pca_heart(mask);
vscale = norm(X0_muscle)/(2*sqrt(2));

clf
hold on
for i=1:3
        Xi = [X0_muscle(1); X0_muscle(1) + vscale*P_muscle(1, i); X0_muscle(1) - vscale*P_muscle(1,i)];
        Yi = [X0_muscle(2); X0_muscle(2) + vscale*P_muscle(2, i); X0_muscle(2) - vscale*P_muscle(2,i)];
        Zi = [X0_muscle(3); X0_muscle(3) + vscale*P_muscle(3, i); X0_muscle(3) - vscale*P_muscle(3,i)];
        plot3(Xi, Yi, Zi, 'Color',colorset(i));
end
%isosurface(dt(1:round(sx/2),:,:), 1);
isosurface(dt(:,1:round(sy/2),:), 0);
isosurface(dt(:,1:round(sy/2),:), 5);
hold off
view(180,0);
camlight
lighting phong
axis image
%%

slab = 4;
P = {};
X0 = {};
for z=(1+slab):(sz-slab)
    mask_slab = mask(:,:,(z-slab):(z+slab));
    [Pz, X0z] = pca_heart(mask_slab);
   
    % As a first step pick direction closest to [0,0,1] for first principal
    % component
    Pz(:,1) = Pz(:,1) .* sign(dot(Pz(:,1), [0 0 1]));
    P{z} = Pz;
    X0{z} = X0z + [0,0,z - slab]';
end

% Set end points
for z=1:slab
    P{z} = P{1+slab}; 
    P{sz - slab + z} = P{sz-slab}; 
    
    X0{z} = X0{1+slab};
    X0{sz - slab + z} = X0{sz-slab}; 
end

% Fix the sign based on initial components
for z=1:sz
    for i=1:3
        P{z}(:,i) = P{z}(:,i) .* dot(P{z}(:,i), P{1}(:,i)); 
    end
end

%%
dt = double(bwdist(exterior));
% vscale = norm(X0_muscle)/(2*sqrt(2));
vscale = 10;
clf
hold on
for i=1:3
    for z=1:sz
        Xi = [X0{z}(1); X0{z}(1) + vscale*P{z}(1,i)];
        Yi = [X0{z}(2); X0{z}(2) + vscale*P{z}(2,i)];
        Zi = [X0{z}(3); X0{z}(3) + vscale*P{z}(3,i)];
%         Xi = [X0_muscle(1); X0_muscle(1) + vscale*P(1, i); X0_muscle(1) - vscale*P(1,i)];
%         Yi = [X0_muscle(2); X0_muscle(2) + vscale*P(2, i); X0_muscle(2) - vscale*P(2,i)];
%         Zi = [X0_muscle(3); X0_muscle(3) + vscale*P(3, i); X0_muscle(3) - vscale*P(3,i)];
        plot3(Xi, Yi, Zi, 'Color',colorset(i));
    end
end
% isosurface(dt(1:round(sx/2),:,:), 1);
isosurface(dt(:,1:round(sy/2),:), 1);
%isosurface(dt, 1);
hold off
view(180,0);
camlight
lighting phong
axis image

%% Assume the first principal component is the long-axis
pLong = P(:,1);

%% find apex as center of mass of the first slice
zlims = zbox(mask);
apexZ = zlims(1);
apexSlice = mask(:,:,apexZ);
apex = round(fcentroid(apexSlice));

% display apex as overlay over slice itself
hold on
clf;
hold on
imagesc(mat2gray(squeeze(apexSlice)))
plot(apex(1),apex(2), 'r.');
hold off
axis image
axis on

%% compute distance transform
dt = double(bwdist(exterior));
gsize = 5;
h = fspecial3('gaussian',[gsize, gsize, gsize]); 
dt = imfilter(dt,h,'replicate');
dt = dt(:,:,1:sz);

figure(1);image3d(exterior);
figure(2);image3d(dt);


%% compute gradient
dx = DGradient(dt, [], 1);
dy = DGradient(dt, [], 2);
dz = DGradient(dt, [], 3);
dg = sqrt(dx.^2+dy.^2+dz.^2);
T(:,:,:,1)=dx(:,:,:);
T(:,:,:,2)=dy(:,:,:);
T(:,:,:,3)=dz(:,:,:);

% integrate from apex
h = 1;
% numIts = 1 + floor((sz - apexZ)/h);
numIts = 500;

clear x;
x{1} = [apex(1), apex(2), apexZ];
x0 = round(x{1});

% Fix the principal component direction based on the initial gradient
d = normalize([dx(x0(1),x0(2),x0(3)),dy(x0(1),x0(2),x0(3)),dz(x0(1),x0(2),x0(3))]);
pLongDirected = pLong .* sign(dot(pLong, d));
r = 0.1;
%%
for i=1:numIts
    xi = round(x{i});
   
   di = [dx(xi(1),xi(2),xi(3)),dy(xi(1),xi(2),xi(3)),dz(xi(1),xi(2),xi(3))];
%    d = normalize((1-r) * d + r * di * sign(dot(d0, di)));
%     d = di * sign(dot(d0, di));
    d = normalize((1-r) * d + r * di);
%    d = di;
   x_temp = x{i} + h * d;
   if (checkBound(size(mask), x_temp))
       x{i+1} = x_temp;
   else
       i
       break;
   end
end

% plot xz course
P = zeros(length(x),3);
for i=1:length(x)
    P(i,1) = x{i}(1);
    P(i,2) = x{i}(2);
    P(i,3) = x{i}(3);
end

% Display centerline with outer wall
% figure(3);
% close(3);
% figure(3);
clf
hold on
plot3(P(:,1), P(:,2), P(:,3));
isosurface(dt(1:round(sx/2),:,:), 1);
hold off
view(180,0);
camlight
lighting phong
axis image
%%
clf
hold on
imagesc(mat2gray(squeeze(dt(:,70,:))));
%imagesc(mat2gray(squeeze(dy(:,50,:))))
plot(P(3,:), sx-P(1,:), 'w-.')
hold off

%%
image3d(dt);
P = zeros(sz, 3);
for i=1:sz
    dt_z = dt(:,:,i);
    [dval, dind] = max(dt_z(:));
    [mi, mj] = ind2sub(size(dt(:,:,1)), dind);
    P(i,:) = [mi, mj,i];
end
