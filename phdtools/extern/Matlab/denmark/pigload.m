
%%%%%%%%%%%%%%%%%
% - Contact: 
% Valentina Mazzoli (V.Mazzoli@tue.nl)
% 
% - DTI storage:
% 32 bits format
% 
% - Volume dimensions:
% Dataset 1: 55x101x101
% Dataset 2: 60x114x101
% Dataset 3: 50x101x96
setpath
clear all
%dimensions = {[55, 101, 101], [60, 114, 101], [50, 101, 96]};
dimensions = {};
binaryformat = 'float32';
datasetID = 2;
% filepath = ['../../data/heart/pig/dataset', num2str(datasetID), '/'];
% filespec = '-denoised.dat';
filepath = ['../../data/heart/pig/dataset', num2str(datasetID), '/raw/'];
filespec = '-rawdata.dat';
%%
fprintf('Processing [%s]\n', filepath);

% Load T_11, T_12, T_13, T_22, T_23, T_33
% assuming a symmetric matrix i.e. T_ij = T_ji
filenames = { 'XX', 'XY', 'XZ', 'YY','YZ','ZZ'};
tensorElementCount = length(filenames);

% Load-in dimensions from the first volume (could use any other)
% and assign to the current dataset
fileid = fopen([filepath, filenames{1}, filespec], 'r');
dimensions{datasetID} = fread(fileid, 3, 'uint16');

% Set computation variables
dimension = dimensions{datasetID};
voxelcount = dimension(1)*dimension(2)*dimension(3);

%
% Load-in diagonal + top-right part of the volume
volumes = zeros(tensorElementCount, voxelcount);
for index=1:tensorElementCount
    file = filenames{index}; 
    fileid = fopen([filepath, file, filespec], 'r');
    
    % Skip dimension header
    % (16 bits = 2 bytes per dimension for a total of 3x2 = 6 bytes)
    fseek(fileid,6,'bof');
    
    volume = fread(fileid, inf,binaryformat);
    volumes(index, :) = volume(:);
end

tensor = zeros([dimension(1), dimension(2), dimension(3), 6]);

% Process each tensor element
for element=1:tensorElementCount
    volume = volumes(element,:);
    index = 1;
    for z=1:dimension(3)
        for y=1:dimension(2)
            for x=1:dimension(1)
                % Set current voxel
                tensor(x, y, z, element) = volume(index);

                % Increment vectorized volume index
                index = index+1;
            end
        end
    end
end
image3d(tensor(:,:,:,1));

% Compute eigendecomposition
[eigfield, eigvalues] = tensoreigendecomp(tensor);

%
% Extract eigenvectors
e1(:,:,:,:) = squeeze(eigfield(:,:,:,1,:));
e2(:,:,:,:) = squeeze(eigfield(:,:,:,2,:));
e3(:,:,:,:) = squeeze(eigfield(:,:,:,3,:));
mask = logical(e1(:,:,:,1));
e1 = cardiac_cylindrical_consistency(e1, mask);
clf;quiverslice(mask, e1, 'z');
export_fig([filepath, 'e1_zplane.pdf'], '-q101', '-transparent');
%
e1x = e1(:,:,:,1);
e1y = e1(:,:,:,2);
e1z = e1(:,:,:,3);
e2x = e2(:,:,:,1);
e2y = e2(:,:,:,2);
e2z = e2(:,:,:,3);
e3x = e3(:,:,:,1);
e3y = e3(:,:,:,2);
e3z = e3(:,:,:,3);

%% Compute the FA
l1 = eigvalues(:,:,:,1);
l2 = eigvalues(:,:,:,2);
l3 = eigvalues(:,:,:,3);

frac = fractional_anisotropy(l1, l2, l3);
% clf;image3d(frac, [55, 55, 30], [0, 0.5]);
clf;image3dslice(frac,'z');
caxis([0, 0.5]); axis image; axis off;
colormap gray

%% Save eigenvectors/values
save([filepath, 'l1.mat'], 'l1');
save([filepath, 'l2.mat'], 'l2');
save([filepath, 'l3.mat'], 'l3');
save([filepath, 'e1x.mat'], 'e1x');
save([filepath, 'e1y.mat'], 'e1y');
save([filepath, 'e1z.mat'], 'e1z');
save([filepath, 'e2x.mat'], 'e2x');
save([filepath, 'e2y.mat'], 'e2y');
save([filepath, 'e2z.mat'], 'e2z');
save([filepath, 'e3x.mat'], 'e3x');
save([filepath, 'e3y.mat'], 'e3y');
save([filepath, 'e3z.mat'], 'e3z');
save([filepath, 'mask.mat'], 'mask');

%% Compute and save distance transforms
[e1, ~, ~, mask, dtEndo, dtEpi] = opendtmri(filepath);
figure(1);clf;image3d(dtEndo);
figure(2);clf;image3d(dtEpi);

%%
[exterior, muscle, interior, labels] = mask_processor(mask, 5);

%%
V = interior;
dt = bwdist(V);
figure(1);image3d(V);
figure(2);image3d(dt);

%% Compare with cardiac_lv
dt1 = cardiac_lv_distance(mask, 5, 5, 'epicardium');
dt2 = cardiac_lv_distance(mask, 5, 5, 'endocardium');