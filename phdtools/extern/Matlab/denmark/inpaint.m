function It = inpaint(It, omega, dt, stds, numIts)

dilation = 0;

%% Reconstruct
for t=1:numIts
    
    % Smooth omega and boundary using a Gaussian filter
    if (stds > 0)
        h = fspecial('gaussian', [3 3], stds);
        I_filtered = imfilter(It, h, 'replicate');
    else
        I_filtered = It;
    end

    % Computation image
    I_comp = It;
    I_comp(omega) = I_filtered(omega);
    
%     It(omegaOut) = I_filtered(omegaOut);
    
    % Compute differential quantities
    Ix = DGradient(I_comp, [], 1);
    Iy = DGradient(I_comp, [], 2);

    dI = sqrt(Ix.^2+Iy.^2);

    % Compute laplacian
    Ixx = DGradient(Ix, [], 1);
    Iyy = DGradient(Iy, [], 2);
    L = Ixx + Iyy;

    % Compute L, the gradient of the laplacian
    % (do we need to multiply by 2 to get rid of central diff's /2 ?)
    Lx = DGradient(L, [], 1);
    Ly = DGradient(L, [], 2);

    Nx = Iy;
    Ny = -Ix;
    [Nx, Ny] = normalize(Nx, Ny);
    
    % Compute the update rule
    dIdt = (Lx .* Nx + Ly .* Ny) .* dI;
    
    [Ii, Ij, V] = find(omega);
    for p=1:length(V)
        i = Ii(p);
        j = Ij(p);
        It(i,j) = It(i,j) + dt * dIdt(i,j);
    end
    
%     pause(0.1);
%     imagesc(It);
end
