% TODO: for now assumes the heart's principal axis is aligned
% with the world's z axis
% The "interior" voxels are most unstable labels due to 
% ventricular collapse and low resolution.
% closingMagnitude = 10 is a safe value to play around for typical hearts
% Labels are the following:
% exterior = 1;
% muscle = 2;
% interior = 3;
function [exterior, muscle, interior, labels] = mask_processor(mask, closingMagnitude)

% Set parameters
label_exterior = 1;
label_muscle = 2;
label_interior = 3;
z_cutfrac = 0.95;

% 1) close the mask so we have a nice and smooth ventricular boundary
padSize = closingMagnitude;
se = strel_ball(closingMagnitude);
maskPad = padarray(mask,[padSize padSize padSize]);
maskClose = imdilate(maskPad, se);
maskClose = imerode(maskClose, se);
%maskClose = imclose(maskPad, se);
maskClose = maskClose(padSize+1:end-padSize,padSize+1:end-padSize, padSize+1:end-padSize);

% Cut top portion
zbounds = zbox(maskClose);
maskCut = logical(maskClose(:,:,zbounds(1):round(z_cutfrac*zbounds(2))));
%figure(1);image3d(maskCut);

% 2) Find regions: left ventricle, right ventricle, exterior
% use 1 - mask such that "air" (chambers, exterior) voxels are labeled with a 1
airMask = logical(1 - maskCut);
components = bwconncomp(airMask);
components = components.PixelIdxList;
comp_count = length(components);

% Sort the components by number of elements
comp_length = zeros(comp_count,1);
for i=1:comp_count
    comp_length(i) = length(components{i});
end
[comp_sorted, comp_sorted_ind] = sortrows(comp_length);
components = components(comp_sorted_ind);

% Display components
component = zeros(size(airMask));
for i=1:comp_count    
    component(components{i}) = i;
end

% 3) Find the exterior using a flood fill
exterior_fill = imfill(maskCut, 1) - maskCut;

% Match connected component with the flood fill
% to determine the exterior
min_diff = Inf;
exterior_index = -1;
exterior_count = length(find(exterior_fill));
for c=1:comp_count
    diff = abs(length(components{c}) - exterior_count);
    if (diff < min_diff)
        min_diff = diff;
        exterior_index = c;
    end
end

% From this point on we assume that "exterior_index"
% holds the exterior component
exterior_cut = false(size(maskCut));
exterior_cut(components{exterior_index}) = 1;

% 4) Now determine the "interior" of the heart, which
% includes both the LV and RV
%interior_cut = (1-exterior_cut) - maskCut;

% 5) Now we need to recover labels based on the original (unclosed) mask)
% Process muscle: the mask itself
% not an exterior or an interior voxel will be treated as muscle
%muscle = logical(~or(exterior_final, interior_final) .* mask);
muscle = mask;

% Process exterior
exterior = false(size(mask));
exterior(:,:,1:size(exterior_cut,3)) = exterior_cut(:,:,:);
% Make z cuts all part of the exterior
%exterior(:,:,size(exterior_cut,3):size(exterior,3)) = 1;
exterior = logical(exterior .* (1-mask));

% Process interior
% interior = false(size(mask));
% interior(:,:,1:size(interior_cut,3)) = interior_cut(:,:,:);
% interior = logical(interior .* (1-mask));
interior = ~or(mask, exterior);

% Create initial labeled volume
labels = zeros(size(mask));
labels(exterior)=label_exterior;
labels(muscle)=label_muscle;
labels(interior)=label_interior;

% At this point, any "0" label corresponds to unassigned voxels
% 5.1) Run a distance transform from the exterior voxels
% and match any non-muscle voxels that are within the 
% morphological closing size
d_exterior = bwdist(exterior);
solitary_ext = (d_exterior <= round(closingMagnitude/2)) & (labels ~= label_muscle); 
exterior_eroded = exterior;
exterior_eroded(solitary_ext) = 1;
exterior = exterior_eroded;

% Update interior

%% 5.1) Run a distance transform from the interior voxels
% and match unassigned voxels that are within the 
% morphological closing size
closingMagnitude = 5;
interior = ~or(mask, exterior);
d_interior = bwdist(interior);
solitary_ext = (d_interior < closingMagnitude) & (labels ~= label_muscle); 
interior_eroded = interior;
interior_eroded(solitary_ext) = 1;
interior = interior_eroded;

% 6) Create finallabeled volume
labels = zeros(size(mask));
labels(exterior)=label_exterior;
labels(muscle)=label_muscle;
labels(interior)=label_interior;

%figure(1);clf;image3d(mask);
%figure(2);clf;image3d(labels);
