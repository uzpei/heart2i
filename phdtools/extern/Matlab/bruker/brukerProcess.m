function data=brukerProcess(dir_data, scannumber, reconumber)
%dir_data = '../bruker/110909.9X1_11-12782-01/';
	
	
%% Load all data, scale data, ...
%clear all
%close all
%clc

%scannumber = 6;       
%reconumber = 3;

% Directory containing data.
% This is the folder containing the (Bruker) data of mouse 11-12782-01.
% Change this to the appropriate location on your computer
% dir_data = 'J:\nmr\110909.9X1';

% Load data. The second argument should be the scan number of the DTI set.
% In this case # 6. This will load 3 reconstructions into matlab.
% Since my (rather old) computer is struggling with its limited amount of
% memory, row 124 is commented out in PV3read.m, thereby omitting data 
% other than a DTI reconstruction.
disp('LOADING DATA...')

global data
data     = PV3read(dir_data,scannumber);

% Once finished you have a structure element 'data'. Look here for the 3th
% (DTI) reconstruction:
size(data{1}.Scan{1})
data{1}.Scan{1}.Reconstruction{reconumber}.images;

% This will give you a matrix of 128x128 (in plane resolution) x (1408 = 
% 64x22; the number of slices in the z-direction multiplied by the number 
% of reconstructed DTI parameters)

disp('SCALING DATA...')

% Scale the MRI data with the appropriate scaling factors (in order to
% have, for example, FA scaled from 0 - 1 and not in A/U.
data = scaleDTI_heart(data, 1,3);

% The 3th reco now contains the scaled DTI reconstruction:
data{1}.Scan{1}.Reconstruction{reconumber}.imscaled;

% this matrix is filled with the following images:
% <DTI_FA> (first 64 slices): pos=0
% <DTI_TRACE> (next 64 slices, and so on): pos=1 
% <DTI_A0>     pos=2
% <DTI_TWI>    pos=3
% <DTI_DXX>    pos=4
% <DTI_DYY>    pos=5
% <DTI_DZZ>    pos=6
% <DTI_DXY>    pos=7
% <DTI_DXZ>    pos=8
% <DTI_DYZ>    pos=9
% <DTI_L1>     pos=10
% <DTI_L2>     pos=11
% <DTI_L3>     pos=12
% <DTI_L1X>    pos=13
% <DTI_L1Y>    pos=14
% <DTI_L1Z>    pos=15
% <DTI_L2X>    pos=16
% <DTI_L2Y>    pos=17
% <DTI_L2Z>    pos=18   
% <DTI_L3X>    pos=19
% <DTI_L3Y>    pos=20
% <DTI_L3Z>    pos=21

disp('PLOTTING DATA...')


%% Plot one of the derived properties from the tensor (FA, trace, ..., L1, ...)

%pos=11;

%for n=(1:64)+64*pos
%    figure(1); clf;
%    imagesc(data{1}.Scan{1}.Reconstruction{3}.imscaled(:,:,n))
%    % colormap('gray')
%    colorbar;
%    caxis([0 0.0025])
%    title(num2str(n),'FontSize',12,'FontWeight','bold')
%    axis off
%    axis square
%end

%% First eigen vector in one matrix
%L1 = data{1}.Scan{1}.Reconstruction{3}.imscaled(:,:,(1:64)+64*pos);

exportTensor(data{1}.Scan{1}.Reconstruction{3}.imscaled, dir_data)

end