function [data]=scaleDTI_heart(data, sc, reco)

% -------------------------------------------------------------------------
% function []=scaleDTIreco(sc)
% 
% Version 1.0   - Save DTI 2nd reconstruction scaled to structure
%                 element data. Reconstruction{2}.imscaled(:,:,:)
% Version 2.0   - 
%
% Options:   sc   - Defines the scannumber that will be analyzed
% 
% B.J. van Nierop
% September 2012
%
% TU/eindhoven
% Biomedical NMR
% -------------------------------------------------------------------------

% Get global params
%global data

% -------------------------------------------------------------------------
% Scale DTI reconstruction 
% -------------------------------------------------------------------------
% Build empty array
nslic    = str2num(data{1}.Scan{1}.Reconstruction{reco}.d3proc.im.siz);
imscaled = zeros(size(data{1}.Scan{sc}.Reconstruction{reco}.images));

% Determine scaled images
c1=1;
c2=1;
for n=1:nslic
    scale = data{1}.Scan{sc}.Reconstruction{reco}.visupars.VisuCoreDataSlope(c1);
    c2=c2+1;
    if c2 == 65; c1=c1+1; end
    
    image = data{1}.Scan{sc}.Reconstruction{reco}.images(:,:,n);
    imscaled(:,:,n)=(image.*scale);     
end

% Save data
data{1}.Scan{sc}.Reconstruction{reco}.imscaled = imscaled;