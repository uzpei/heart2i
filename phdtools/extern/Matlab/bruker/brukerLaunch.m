% Once done with this processing, will need to compute the two
% other frame axes (Y, Z) in Java.

% Control hearts
path = '../data/mouse/MouseData/bruker/110909.9X1_11-12782-01/'
disp('Control')
brukerProcess(path, 6, 3)


path = '../data/mouse/MouseData/bruker/110909.9Y1_11-12782-02/'
disp('Control')
brukerProcess(path, 5, 3)


path = '../data/mouse/MouseData/bruker/110909.9Z1_11-12782-03/'
disp('Control')
brukerProcess(path, 5, 3)


path = '../data/mouse/MouseData/bruker/110909.a01_11-12782-04/'
disp('Control')
brukerProcess(path, 5, 3)


% TAC hearts
path = '../data/mouse/MouseData/bruker/111110.b21_11-12657-01/'
disp('TAC')
brukerProcess(path, 6, 3)


disp('test')
path = '../data/mouse/MouseData/bruker/111110.aY1_11-12657-04/'
disp('TAC')
brukerProcess(path, 5, 3)


path = '../data/mouse/MouseData/bruker/111110.ba1_11-12985-02/'
disp('TAC')
brukerProcess(path, 5, 3)


path = '../data/mouse/MouseData/bruker/111110.b91_11-12985-03/'
disp('TAC')
brukerProcess(path, 5, 3)

