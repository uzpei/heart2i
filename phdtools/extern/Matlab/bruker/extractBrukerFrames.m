function mask = extractBrukerFrames(brukerFile, dtiScan, dtiReconstruction, a0Reconstruction, isotropy, zdim, dilationScale)

% Set default variables
animate = true;
export = true;

% Load data
disp(sprintf('Reading DTI data...'));
data = PV3read(brukerFile, dtiScan);

% Scale the MRI data with the appropriate scaling factors (in order to
% have, for example, FA scaled from 0 - 1 and not in A/U.
data = scaleDTI_heart(data, 1, 3);

% data is now filled with the following images.
% (dz = number of slices along z)
% image = pos * dz + (1:dz)
% <DTI_FA>     pos=0
% <DTI_TRACE>  pos=1 
% <DTI_A0>     pos=2
% <DTI_TWI>    pos=3
% <DTI_DXX>    pos=4
% <DTI_DYY>    pos=5
% <DTI_DZZ>    pos=6
% <DTI_DXY>    pos=7
% <DTI_DXZ>    pos=8
% <DTI_DYZ>    pos=9
% <DTI_L1>     pos=10
% <DTI_L2>     pos=11
% <DTI_L3>     pos=12
% <DTI_L1X>    pos=13
% <DTI_L1Y>    pos=14
% <DTI_L1Z>    pos=15
% <DTI_L2X>    pos=16
% <DTI_L2Y>    pos=17
% <DTI_L2Z>    pos=18   
% <DTI_L3X>    pos=19
% <DTI_L3Y>    pos=20
% <DTI_L3Z>    pos=21
dataDTI = data{1}.Scan{1}.Reconstruction{dtiReconstruction}.imscaled(:,:,:);

% Compute the FA mask
mask = estimateMask(dataDTI, zdim, true);
maski = find(mask == 0);

%%%%%%%%% DISTANCE TRANSFORM %%%%%%%%

disp(sprintf('Computing the distance transform...'));

[dt, dx, dy, dz, maskd] = dtransform3(1-mask, isotropy, dilationScale);

%%%%%%%%% LOAD EIGENVECTORS %%%%%%%%
% We want the first eigenvector so look at pos=L1X, L1Y, L1Z
% and normalize them
% Load first eigenvector and normalize
Tx = dataDTI(:, :, (1:zdim) + zdim * 13);
Ty = dataDTI(:, :, (1:zdim) + zdim * 14);
Tz = dataDTI(:, :, (1:zdim) + zdim * 15);
enorm = sqrt(Tx .^ 2 + Ty .^ 2 + Tz .^ 2);
Tx = Tx ./ enorm;
Ty = Ty ./ enorm;
Tz = Tz ./ enorm;

[nx, ny, nz] = orthogonalize(Tx, Ty, Tz, dx, dy, dz);

%%%%%%%%%%%%% EXPORT %%%%%%%%%%%%%%%
% Store the result of the distance transform gradient
if (export)
        % Save mask & eigenvectors
    disp(sprintf('Exporting mask and eigenvectors...'));
    mask = double(mask);
    save([brukerFile, 'mask.mat'], 'mask');
    save([brukerFile, 'Tx.mat'], 'Tx');
    save([brukerFile, 'Ty.mat'], 'Ty');
    save([brukerFile, 'Tz.mat'], 'Tz');
    save([brukerFile, 'nx.mat'], 'dx');
    save([brukerFile, 'ny.mat'], 'dy');
    save([brukerFile, 'nz.mat'], 'dz');

    filex = [brukerFile, 'projnormx.mat'];
    filey = [brukerFile, 'projnormy.mat'];
    filez = [brukerFile, 'projnormz.mat'];
    
    disp(sprintf('Exporting projection (x) to %s...', filex));
    save(filex, 'nx');
    
    disp(sprintf('Exporting projection (y) to %s...', filey));
    save(filey, 'ny');
    
    disp(sprintf('Exporting projection (z) to %s...', filez));
    save(filez, 'nz');

end

%%%%%%%%%%% ANIMATE %%%%%%%%%%%
if (animate)
    for m=1:10
    for n=23:46

       figure(1); clf;
       imagesc(mask(:,:, n));
       caxis([0 1])
       colormap hot
       title(n);

       figure(2); clf;
       imagesc(dx(:,:, n));
       caxis([0 1])
       colormap hot

       figure(3); clf;
       imagesc(dy(:,:, n));
       caxis([0 1])
       colormap hot

       pause(.1)
    end
    end
end
