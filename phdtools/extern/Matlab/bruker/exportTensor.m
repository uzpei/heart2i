function exportTensor(image, dir_data)
	dtitensor = [0:21];

	% Assume the order is DXX, DYY, DZZ, DXY, DXZ, DYZ
	j = 0;
	for i=dtitensor
		switch j
			case 0
				str = 'fa';
			case 1
				str = 'trace';
			case 2
				str = 'a0';
			case 3
				str = 'twi';
			case 4
				str = 'dxx';
			case 5
				str = 'dyy';
			case 6
				str = 'dzz';
			case 7
				str = 'dxy';
			case 8
				str = 'dxz';
			case 9
				str = 'dyz';
			case 10
				str = 'L1';
			case 11
				str = 'L2';
			case 12
				str = 'L3';
			case 13
				str = 'L1X';
			case 14
				str = 'L1Y';
			case 15
				str = 'L1Z';
			case 16
				str = 'L2X';
			case 17
				str = 'L2Y';
			case 18
				str = 'L2Z';
			case 19
				str = 'L3X';
			case 20
				str = 'L3Y';
			case 21
				str = 'L3Z';
			otherwise
				disp('ERROR: unknown data id')
		end
	
		filename = strcat(dir_data, str);
		filename = strcat(filename, '.txt');
	
		tensor = image(:, :, (1:64) + 64 * i);
	
		disp(sprintf('Exporting to %s...', filename));
	
%		header = sprintf('%s\n%ix%ix%i\n', filename, size(tensor,1), size(tensor,2), size(tensor,3));
%		fprecision = 8;
		fileID = fopen(filename,'w');
%		fprintf(fileID, '%s', header);
		fprintf(fileID, '%.10f\n', tensor(:));
		fclose(fileID);
%		dlmwrite(filename, tensor, '-append', 'delimiter', ',', 'precision', fprecision);
	
	
		j = j+1;
	end

end
