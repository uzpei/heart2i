clear all

setpath;

% Load all ghm volumes
searchPath = '../../data/heart/rat/sample/';
%subjects = openghm_folder(searchPath, 'CurvatureBits', [true true true, true], 'GHMpath', 'registered/processed/helicoidfit', 'MaskPath', 'registered', 'MaskName', 'mask.mat');
%subjects = opendtmri_folder(searchPath, 'PostPath', 'registered', 'ElementOrder', [2 1 3], 'LoadE1', true', 'LoadE2', false, 'LoadE3', false);

options = struct2paramcell(struct('PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', false, 'OptimizationFrameOnly', true, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));
subjects = compute_frames_con_all(searchPath, options{:});

%%
F = subjects(1).frames.Fnorm;
% frame = framefield(F.T, cross(F.B, F.T), F.B, 'Assemble', true);
T = F.T;
N = cross(F.B, F.T);
B = F.B;

%%
ctnb = cijk(T, N, B);
image3d(ctnb)

%% Test derivatives
% Test dx
n = 50;
Vx = rand(n,n,n);
Vy = rand(n,n,n);
Vz = rand(n,n,n);

% Vz(:,:,round(n /3):round(2*n/3)) = 1;

V = assemble(Vx, Vy, Vz);
J = jacobian(V);
Jx = squeeze(J(:,:,:,1:3));
Jy = squeeze(J(:,:,:,4:6));
Jz = squeeze(J(:,:,:,7:9));

image3d(Jz);
