%% Create a large matrix and test indexing speed
n = 100;
A = rand(n, n, n);
numinds = 100000;
samples = randsample(numel(A), numinds);

tic
v = 0;
for i=1:numinds
    ind = samples(i);
    
    [i0, j0, k0] = ind2sub(size(A), ind);
    v = v + A(i0, j0, k0);
end
toc

tic
v = 0;
for i=1:numinds
    ind = samples(i);
    
    [i0, j0, k0] = ind2sub(size(A), ind);
    v = v + A(ind);
end
toc



%%
clear all

setpath;

% Load all subjects, frames, and connection forms
% subjects = compute_frames_con_all('../../data/heart/rat/atlas_FIMH2013/', 'registered');
searchPath = '../../data/heart/rat/sample/';
%searchPath = '../../data/heart/rat/atlas_FIMH2013/';
%%
experiment_options = struct2paramcell(struct('PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', false, 'OptimizationFrameOnly', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));
subjects = compute_frames_con_all(searchPath, experiment_options{:});
frameFields = fieldnames(subjects(1).frames);
subject = subjects(1);

%% Test smoothing
nits = 100;
gstd = 0.5;
% delta_gstd = 0.5;

V = subject.e1;
mask = subject.mask;

for it = 1:nits
    gsize = 6 * gstd;
    gsize
    V = smooth_vector_field(V, gstd, gsize);
    V = normalize(V);
    quiverslice(mask, V, 'z');
    pause(0.1);
%     gstd = gstd + delta_gstd;
    it
end

%%
clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
cijks = subject.cijks.Fnorm;
nbins = 50;
stdf = 2;
saxis = 'z';

for cind=1:9            
    cijkv = reconstitute(cijks.(clbls{cind}), subject.mask);
    cijkslice = volslice(cijkv, saxis);
    
    subplot(3,3,cind);
    
    % This call is just for getting the figure limits
    [heights, centers] = histnorm_thresholded(cijkv(subject.mask), nbins, stdf);
    imagesc([min(centers(:)), max(centers(:))], [min(heights(:)), max(heights(:))], flipud(cijkslice));
    
    % Use fixed color axis such that all axial volumes have same colormap
    caxis([min(centers(:)), max(centers(:))]);
    
    hold on
    histnorm_thresholded(cijkv(subject.mask), nbins, stdf, '--k', 'LineWidth',1);
    set(gca,'ydir','normal');
    axis square
%     axis tight
    title(clbls{cind});
    colorbar
end

%image3d(reconstitute(subject.cijks.Fnorm.TNB, subject.mask));