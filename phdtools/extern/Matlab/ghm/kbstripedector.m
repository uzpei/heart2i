
%% Load data in
fprintf('Loading data...\n');
tic
% datapath = ['../../data/heart/rats/','rat07052008','/registeredHelicoidFit/', 'kb_n3_n3.mat'];
datapath = ['../../data/heart/rats/','rat07052008','/registered/processed/helicoidfit/', 'kb_n3_n3.mat'];

kbdata = cell2mat(struct2cell(load(datapath)));
toc

dirout = './output/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

%% Look at data
figure(1)
image3d(kbdata);

%% Compute gradient
%[gx, gy, gz] = gradient(kbdata);
%image3d(gx);
%colormap gray

%% LOG detector
%figure(1)
gsize = 2.5;
h = fspecial3('log',[gsize, gsize, gsize]); 
% h = fspecial3('average',[gsize, gsize, gsize]); 
%h = fspecial3('ellipsoid',[gsize, gsize, gsize]); 
% h = fspecial3('gaussian',[gsize, gsize, gsize]); 
%h = fspecial3('laplacian');

imf = imfilter(kbdata,h,'replicate');
%image3d(imf)

%% Extract zero-crossing
figure(2)
imft = imf / max(abs(imf(:)));

imft = imft .* (imft >= 0);
image3d(imft);
