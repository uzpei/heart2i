clear all
%close all

specimen = 'human/isotropic/'
%specimen = 'rats/rat07052008/isotropic/'
path = ['/Users/epiuze/Documents/workspace/PhD/phdtools/data/heartResults/',specimen];

% Global variables
targets = { '_n3', '_n5', '_n7' };
%targets = { '_n3' };
titles = { 'N_3', 'N_5', 'N_7' };
outputDir = 'output/';

% Error and standard parameters
errormag = [0, 0.3];
errorres = 0.05;
errorstd = 3;

xlabelSize = 22;
figSize = 18;
titleSize = 18;
legendSize = 24;

% Params
measures = { 'helicoidfit/error_n3', 'oneFormExtrapolation/extrapolationErrorTheta_ghm' };

labels = { 'GHM', 'GHMform'};
legends = { 'GHM', 'GHM-form'};

fprintf('%i measures found\n', length(measures));

measuresCount = length(measures);
targetsCount = length(targets);

% For debugging
%    m = 4;

% Load in data for this heart

%path = char(hearts(i).path);

% Load curvature values
% Load first to get the size of the garbage matrix
loaded = load([path, measures{1}, targets{1}, '.mat']);

% Filter
garbage = zeros(length(loaded.data(:)), 1);

heart = {};
% Load measures
for j=1:measuresCount
    % Load target for each measure
    for k=1:targetsCount
        sfield = [labels{j}, targets{k}];

        % Process theta
        filename = [path, measures{j}, targets{k}, '.mat'];
        fprintf('Loading %s...\n', filename);
        measure = load(filename);
        measure = measure.data;
        measure = measure(:);
        heart.(sfield) = measure;
        garbage(isnan(measure)) = NaN;
    end
end

% Apply filter
fprintf('Thresholding...\n');
for j=1:measuresCount
    for k=1:targetsCount
        sfield = [labels{j}, targets{k}];
        heart.(sfield)(isnan(garbage)) = [];
    end
end

% Plot error overlay for both models
errorbins = 500;
errormag = [0, 0.3];
errorres = 0.05;
errorstd = 3;
maxheights = zeros(1, targetsCount);
for k=1:targetsCount
    figure(k)
    clf
    for j=1:measuresCount
        if (j == 1)
            lineStyle = '-';
        else
            lineStyle = '-';
        end
        color = colorset(1+j);
        sfield = [labels{j}, targets{k}];
        data = heart.(sfield);
        hold on
        [hmax, modev] = plotsmooth(data, errorbins, '', '', color, false, false, errormag, errorres, errorstd, lineStyle);
        hold off
        
        if (hmax > maxheights(k))
            maxheights(k) = hmax;
        end
        
    end
end

for k=1:targetsCount
    figure(k)
    
    legend(legends)
    axis([errormag(1) errormag(2) 0 1.01 * max(maxheights(:))]);
    xhandle = xlabel('error (radians)');
    set(xhandle,'FontSize',22);
    set(gca,'FontSize',18);
    export_fig(['output/ghmFormHuman',targets{k},'.pdf'], '-transparent', '-q101');
end

