clear all

setpath;

% Load all ghm volumes
searchPath = '../../data/heart/rat/atlas_FIMH2013/';
subjects = openghm_folder(searchPath, 'CurvatureBits', [true true true, true], 'GHMpath', 'registered/processed/helicoidfit', 'MaskPath', 'registered', 'MaskName', 'mask.mat');

%% Export short axial slice for each
cscale = [-0.7, 0.7];
%% KT
figure(1)
colorbar
for subject_it = 1:length(subjects)
    subject = subjects(subject_it)
    vol = rad2deg(subject.kt_n3_n3) / 0.25;
    vol(isnan(vol)) = 0;
    image3dslice(vol, 'z');
%     image3d(subject.mask);
%     colorbar
    caxis(rad2deg(cscale) / 0.25);
    colormap(customap('redwhiteblue'))
    axis image
    axis off
    export_fig('.', [subject.name, '_kt_z.pdf'], '-transparent', '-q101', '-nocrop');
    pause(0.1);
end
colormap_export('.', 'colorbar_kt.pdf');


%% KN
figure(1)
colorbar
for subject_it = 1:length(subjects)
    subject = subjects(subject_it)
    vol = rad2deg(subject.kn_n3_n3) / 0.25;
    vol(isnan(vol)) = 0;
    image3dslice(vol, 'z');
%     image3d(subject.mask);
%     colorbar
    caxis(rad2deg(cscale) / 0.25);
    colormap(customap('redwhiteblue'))
    axis image
    axis off
    export_fig('.', [subject.name, '_kn_z.pdf'], '-transparent', '-q101', '-nocrop');
    pause(0.1);
end
colormap_export('.', 'colorbar_kn.pdf');

%% KB
figure(1)
colorbar
for subject_it = 1:length(subjects)
    subject = subjects(subject_it)
    vol = rad2deg(subject.kb_n3_n3) / 0.25;
    vol(isnan(vol)) = 0;
    image3dslice(vol, 'z');
%     image3d(subject.mask);
%     colorbar
    caxis(rad2deg(cscale) / 0.25);
    colormap(customap('redwhiteblue'))
    axis image
    axis off
    export_fig('.', [subject.name, '_kb_z.pdf'], '-transparent', '-q101', '-nocrop');
    pause(0.1);
end
colormap_export('.', 'colorbar_kb.pdf');

%% error
figure(1)
colorbar
for subject_it = 1:length(subjects)
    subject = subjects(subject_it)
    vol = rad2deg(subject.error_n3_n3);
    vol(isnan(vol)) = 0;
    image3dslice(vol, 'z');
    caxis(rad2deg([0, 0.8]));
    colormap(customap('hotinv'))
    axis image
    axis off
    export_fig('.', [subject.name, '_error_z.pdf'], '-transparent', '-q101', '-nocrop');
    pause(0.1);
end
colormap_export('.', 'colorbar_error.pdf');


    
    