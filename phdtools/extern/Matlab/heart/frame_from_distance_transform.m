function [T, D] = frame_from_distance_transform(T, dtEndocardium, dtEpicardium, mask)    

    D = zeros([size(mask) 3]);
    
    % Compute both gradient fields
    factor = 0;
    if (~isempty(dtEndocardium))
        Fendo = frame(dtEndocardium, mask);
        D = D + Fendo;
        factor = factor + 1;
    end
    
    if (~isempty(dtEpicardium))
        Fepi = frame(dtEpicardium, mask);
        D = D + Fepi;
        factor = factor + 1;
    end

    %% Orthogonalize frame field with respect to the first eigenvector
    D = orthogonalize(T, D ./ factor);

    function F = frame(dt, mask)
    %     [dx, dy, dz] = gradient(dt);
        dtd = double(dt);
        dx = DGradient(dtd, [], 1);
        dy = DGradient(dtd, [], 2);
        dz = DGradient(dtd, [], 3);

        F = zeros([size(mask) 3]);
        F(:,:,:,1) = dx(:,:,:) .* mask;
        F(:,:,:,2) = dy(:,:,:) .* mask;
        F(:,:,:,3) = dz(:,:,:) .* mask;
        F = normalize(F); 
    end

end