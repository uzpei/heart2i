% function [T, N, B, mask, C] = curlfield(path, varargin)
% ======================================================
% varargin = { gaussianStd, numFilteringIterations }
% ======================================================
% Returns the smoothed T data 
% with (T-smoothed) orthogonal B
% and orthogonal N
% and curl field C
function [T, N, B, mask, C] = curlfield(path, varargin)

    if (nargin > 1)
        smoothingParameters = varargin{1};
    else
        smoothingParameters = {0.3, 1};
    end

    fprintf('=== Computing curl frame field === \n');
    
    %% Load data
    fprintf('Loading data...\n');
    tic
    mask = cell2mat(struct2cell(load([path, 'mask.mat']))) > 0;
    
    [sx, sy, sz] = size(mask);
    
    T = zeros(size(mask));
    T(:,:,:,2) = cell2mat(struct2cell(load([path, 'e1x.mat'])));
    T(:,:,:,1) = cell2mat(struct2cell(load([path, 'e1y.mat'])));
    T(:,:,:,3) = cell2mat(struct2cell(load([path, 'e1z.mat'])));
    toc

    %% Smooth T
    fprintf('Smoothing T...\n');
    tic
    gstd = smoothingParameters{1};
    gsize = ceil(6 * gstd);
    %gscale = sqrt(smoothingParameters{2} * gstd ^ 2);

    for filterit=1:smoothingParameters{2};
        % Filter the tangent vector
        h = fgaussian3([gsize,gsize,gsize], [gstd, gstd, gstd]);
        T(:,:,:,1) = imfilter(T(:,:,:,1),h,'replicate');
        T(:,:,:,2) = imfilter(T(:,:,:,2),h,'replicate');
        T(:,:,:,3) = imfilter(T(:,:,:,3),h,'replicate');

        % Normalize the result
        [T(:,:,:,1), T(:,:,:,2), T(:,:,:,3)] = normalize(T(:,:,:,1), T(:,:,:,2), T(:,:,:,3));

        % Apply mask
        T = T .* repmat(mask, [1 1 1 3]);
    end
    toc

    %% Store smoothed T
%     T(:,:,:,1) = TX;
%     T(:,:,:,2) = TY;
%     T(:,:,:,3) = TZ;

    %% Compute the curl of T
    fprintf('Computing curl of T...\n');
    tic
    C = zeros(sx, sy, sz, 3);
    [C(:,:,:,1), C(:,:,:,2), C(:,:,:,3)] = curl(T(:,:,:,1), T(:,:,:,2), T(:,:,:,3));
    toc
    
    %% Compute B from the curl of T
    % (Make B orthogonal to T)
    fprintf('Computing remaining N, B...\n');
    tic
    B = zeros(sx, sy, sz, 3);
    [B(:,:,:,1), B(:,:,:,2) , B(:,:,:,3)] = orthogonalize(T(:,:,:,1), T(:,:,:,2), T(:,:,:,3), C(:,:,:,1), C(:,:,:,2), C(:,:,:,3));

    %% Compute N = T x B
    N = cross(T,B);

end
