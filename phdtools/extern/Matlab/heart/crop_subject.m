%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Crop a volume given the specified bounds. 
% The format is either 
% 1) a struct: { i0:iend, j0:jend, k0:kend }, e.g. { 20:30, 20:30, 30:35 }
% 2) or a double: [ ipercent, jpercent, kpercent ]
function subject = crop_subject(subject, bounds, varargin)
options = parseInput(struct('padding', [0 0 0]), varargin{:});

fnames = fieldnames(subject);

for fi = 1:length(fnames)
    fname = fnames{fi};
    field = subject.(fname);
    
    if ((isa(field,'numeric') || isa(field, 'logical')) && ~isempty(field))
        fprintf('Found matrix %s\n', fname);
        vcrop = apply_crop(field, bounds, options.padding);
%         size(field)
%         size(vcrop)
        subject.(fname) = vcrop;
    elseif (isa(field, 'struct'))
        % Visit structure
%        fprintf('Found structure %s\n', fname);
        subject.(fname) = crop_subject(field, bounds);
    end
end
end

function volume_crop = apply_crop(volume, bounds, padding)
    % If this is a vectorized volume, abort
    if (isvector(volume))
        volume_crop = volume;
        return;
    end
    
    [sx, sy, sz, nd] = size(volume);
    
    if (isa(bounds, 'struct') | isa(bounds, 'cell'))
        n1 = numel(bounds{1}) + 2 * padding(1);
        n2 = numel(bounds{2}) + 2 * padding(2);
        n3 = numel(bounds{3}) + 2 * padding(3);
    elseif (isa(bounds, 'double'))
        bounds = ceil(bounds .* [sx, sy, sz]);
        n1 = bounds(1) + 2 * padding(1) ;
        n2 = bounds(2) + 2 * padding(2);
        n3 = bounds(3) + 2 * padding(3);
        
        % Construct index array from absolute bounds
        pbounds = {};
        pbounds{1} = 1:bounds(1);
        pbounds{2} = 1:bounds(2);
        pbounds{3} = 1:bounds(3);
        bounds = pbounds;
    else
        class(bounds)
    end

    volume_crop = zeros(n1, n2, n3, nd);
    
    for fi=1:nd
        vc = padarray(volume(bounds{1}, bounds{2}, bounds{3}, fi),padding,0,'both');
        volume_crop(1:n1, 1:n2, 1:n3, fi) = vc(:,:,:);
%        volume_crop(1:n1, 1:n2, 1:n3, fi) = volume(bounds{1}, bounds{2}, bounds{3}, fi);
    end
    
    % Make sure we preserve volume class
    if (isa(volume, 'single'))
        volume_crop = single(volume_crop);
    elseif (isa(volume, 'logical'))
        volume_crop = logical(volume_crop);
    end

end

