clear all
species = {};
%species(1).path = '../../data/heart/rat/atlas_FIMH2013/';
species(1).path = '../../data/heart/rat/sample1/';
species(1).innerPath = 'registered';

% subjects = opendtmri_folder(species(1).path, 'PostPath', species(1).innerPath, 'ElementOrder', [1 2 3], 'LoadE1', true', 'LoadE2', false, 'LoadE3', false);
% subject = subjects(1);
experiment_options = struct2paramcell(struct('ElementOrder', [1 2 3], 'PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));
subjects = compute_frames_con_all('../../data/heart/rat/sample1/', experiment_options{:});
subject = subjects(1);

%%
% clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB', 'CONSTANT' };
clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB' };
elbls = [ clbls, '0' ];


%%
[sx, sy, sz] = size(subject.mask);
num_forms = length(clbls);

% Precompute frame field
subject = frame_subject(subject);
subject = crop_subject(subject, { 1:size(subject.mask,1), 1:size(subject.mask,2), 1:60 });

F = framefield(subject.frame.T, subject.frame.N, subject.frame.B);

%%
% opti_subject = crop_subject(subjects(1), { 1:size(subject.mask,1), 1:size(subject.mask,2), 30:33 }, 'padding', [1 1 1]);
opti_subject = crop_subject(subjects(1), { 1:15, 1:15, 30:33 }, 'padding', [1 1 1]);
opti_subject = frame_subject(opti_subject);
%
opti_time = tic;
copti = cijks_optimized(opti_subject.frame, opti_subject.mask, 0.1, 'cmask', [1 1 1 0 0 0 0 0 0]);
toc(opti_time)

%%
for i=1:9
    subplot(3,3,i);
    image3dslice(copti{i}, 'z');
end
%%
clear cforms;
cijks = cforms(F);

zerov = zeros(size(subject.mask));
for i=1:num_forms
    zerocform.(clbls{i}) = zerov;
end

errors = {};
for i=1:num_forms
    fprintf('%s\n', clbls{i});
    
    % Use single connection form
	cijks_sample = zerocform;
    cijks_sample.(clbls{i}) = cijks.(clbls{i});
    
	Terror = compute_connection_extrapolation_error(F, subject.mask, cijks_sample, 'all');
    
    errors{i} = real(Terror);
end

% Add constant model
Terror = compute_connection_extrapolation_error(F, subject.mask, [], 'constant');
errors{num_forms+1} = real(Terror);

%% Plot histograms
figure
clf
hold on
for i=1:length(errors)
%    histnorm(errors{i}, 200, 1, 'Color', colorset(i));
    histnorm_thresholded(errors{i}(:), 100, 2, 'Color', colorset(i));
end
legend(elbls);
hold off

%% Plot slices
for i=1:length(errors)
    subplot(3,3,i);
    image3dslice(errors{i}, 'x');
    title(elbls{i});
end

%% Sort errors and plot with labels
figure
merrors = [];
for i=1:length(errors)
    evol = errors{i};
%     merrors(i) = mean(evol(evol < 0.2));
    merrors(i) = mean(evol(:));
end

[errors_sorted, inds] = sort(merrors);
clbls_sorted = elbls(inds);
plot(1:numel(errors_sorted), rad2deg(errors_sorted));
set(gca,'XTickLabel', clbls_sorted)
xlabel('Connection form thresholded')
ylabel('mean volume error')

