% TODO: add possibility to choose other frame fields
% Species: struct with fieldnames(struct) = available species
% Each species has a target directory (species.path) and inner directory
% (species.innerPath).
%function connection_form_experiment(species, varargin)
species = {};
%species(1).path = '../../data/heart/rat/atlas_FIMH2013/';
species(1).path = '../../data/heart/rat/sample2/';
species(1).innerPath = 'registered';
%species(1).path = '../../data/heart/canine/sample1/';
% species(1).path = '../../data/heart/canine/';
% species(1).innerPath = 'original';
varargin = {};

% Construct defaults
% Setting intraspecies to false and interspecies to true requires a lot of
% memory since all subjects need to be stored explicitly.

defaults = {};
defaults.combineIntraSpecies = true;
defaults.combineInterSpecies = true;
defaults.elementOrder = [1 2 3];
% Parse input parameters
options = parseInput(defaults, varargin{:});

% Parse species info
species_count = length(species);

% Prepare experiment
connection_labels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };

% In order to minimize memory footprint, need to proceed carefull from
% here.
% 1) If we are combining intra-species, we don't need to keep track of each
% subject since they are merged with others.
% 2) If we are combining inter-species, we need to proceed depending on
% whether 1) is true or false.
% 3) If we are not combining inter-species, proceed with each species
% separetely.

if (options.combineIntraSpecies)
    %
    % Load subjects from each species and combine them on-the-fly.
    %
    for species_index=1:species_count
        species_current = species(species_index);

        subjects = opendtmri_folder(species_current.path, 'PostPath', species_current.innerPath, 'ElementOrder', options.elementOrder, 'LoadE1', true', 'LoadE2', false, 'LoadE3', false);

        % Precompute frames
        for subject_index=1:length(subjects)

            % Precompute frame field
            if (~isempty(subjects(subject_index).dt))
                fprintf('Loading distance transform from file...\n');
                [T, B] = frame_from_distance_transform(subjects(subject_index).e1, subjects(subject_index).dt, subjects(subject_index).mask);
                subjects(subject_index).frame.T = T;
                subjects(subject_index).frame.B = B;
                subjects(subject_index).frame.N = cross(B, T);
            else
                [frame, dt] = compute_frames(subjects(subject_index).e1, [], [], subjects(subject_index).mask);
                spath = [species_current.path, subjects(subject_index).name, '/', species_current.innerPath, '/'];
                fprintf('Saving distance transform to file [%sdt.mat]\n', spath);
                save([spath, 'dt.mat'], 'dt');                
                
                subjects(subject_index).frame.T = frame.Fnorm.T;
                subjects(subject_index).frame.B = frame.Fnorm.B;
                subjects(subject_index).frame.N = cross(frame.Fnorm.B, frame.Fnorm.T);
            end

            % Optimize memory footprint by indexing and using single
            % precision
            mask3 = repmat(subjects(subject_index).mask, [1 1 1 3]);
            subjects(subject_index).frame.T = single(subjects(subject_index).frame.T(mask3));
            subjects(subject_index).frame.N = single(subjects(subject_index).frame.N(mask3));
            subjects(subject_index).frame.B = single(subjects(subject_index).frame.B(mask3));
            
            % TODO: Precompute Jacobian of F_1 and F_2
%             JF1 = jacobian(subjects(subject_index).frame.T);
%             JF2 = jacobian(subjects(subject_index).frame.N);
            
            % Overwrite spurious information
            subjects(subject_index).e1 = [];
            subjects(subject_index).e2 = [];
            subjects(subject_index).e3 = [];
        end
        
        % Combine connection forms
        for i=1:3
            for j=(i+1):3
                for k=1:3
                    % Current connection-form
                    clbl = ind2clbl(i, j, k);
                    fprintf('Computing %s for all subjects...\n', clbl);
                    
                    % Compute subject volumes
                    tcomp = tic;
                    for subject_index=1:length(subjects)
%                         e{1} = subjects(subject_index).frame.T;
%                         e{2} = subjects(subject_index).frame.N;
%                         e{3} = subjects(subject_index).frame.B;
                        mask3 = repmat(subjects(subject_index).mask, [1 1 1 3]);
                        e{1} = reconstitute(subjects(subject_index).frame.T, mask3);
                        e{2} = reconstitute(subjects(subject_index).frame.N, mask3);
                        e{3} = reconstitute(subjects(subject_index).frame.B, mask3);
                        
                        cmat = cijk(e{i}, e{j}, e{k});
                        subjects(subject_index).(clbl) = cmat(subjects(subject_index).mask);
                    end
                    
                    % TODO: only store vectorized volume and 
                    % in combine_volumes, check if the volume is already
                    % linearized (i.e. it doesn't need to be indexed using
                    % the mask)
                    % Combine volumes
                    cforms.(clbl) = combine_volumes(subjects, clbl);
                    
                    % Free up space
                    for subject_index=1:length(subjects)
                        subjects(subject_index).(clbl) = [];
                    end
                    fprintf('Total time = %fs\n', toc(tcomp))
                end
            end
        end
        
    end
    
else
    
end

% Plot histograms
nbins = 200;
stdf = 2;
clbls = fieldnames(cforms);
for i=1:length(clbls)
    hdata = cforms.(clbls{i});
    subplot(3,3,i);
    %histnorm(rad2deg(hdata), nbins, stdf);
    histnorm_thresholded(rad2deg(hdata), nbins, stdf, 'LineWidth',2, 'Color', colorset(i));
    title(clbls{i});
    pause(0.1);
end


%%
%%%%%%%%%%%%%% Previous code %%%%%%%%%%%%%%
setpath;

% Load all subjects, frames, and connection forms
% subjects = compute_frames_con_all('../../data/heart/rat/atlas_FIMH2013/', 'registered');
%searchPath = '../../data/heart/rat/sample/';
% searchPath = '../../data/heart/rat/atlas_FIMH2013/';
%searchPath = '../../data/heart/canine/';
searchPath = '../../data/heart/rat/sample1/';

experiment_options = struct2paramcell(struct('PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', true, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));
subjects = compute_frames_con_all(searchPath, experiment_options{:});
frameFields = fieldnames(subjects(1).frames);

subject = subjects(1);
[sx, sy, sz] = size(subject_index.mask);

%%
subject_index = subjects(1);
%image3d(subject.frames.Fnorm.T);
quiverslice(subject_index.mask, subject_index.frames.Fnorm.T, 'z');

T = double(subject_index.frames.Fnorm.T);
B = double(subject_index.frames.Fnorm.B);
N = double(normalize(cross(B, T, 4)));
c123 = cijk(T, N, B);
image3d(c123);

%% 
smoothed = false;

if (smoothed)
    smstr = 'smoothed';
%     T = iterative_gaussian_smoothing(subject.frames.Fnorm.T, 'iterations', 3, 'std', 0.5);
    T = iterative_gaussian_smoothing(subject_index.frames.Fnorm.T, 'iterations', 10, 'std', 0.15);
    T = T .* repmat(subject_index.mask, [1 1 1 3]);
    B = orthogonalize(T, subject_index.frames.Fnorm.B);
    figure(1); image3d(subject_index.frames.Fnorm.T);
    figure(2); image3d(T);
    figure(3); image3d(abs(T-subject_index.frames.Fnorm.T));
else
    smstr = 'raw';
    T = subject_index.frames.Fnorm.T;
    B = subject_index.frames.Fnorm.B;
end

%
N = double(normalize(cross(B, T, 4)));
F = framefield(T, N, B);
cijks = cforms(F);

% Compute K-means clustering

% Assemble clustering data
% c-forms
clbls = fieldnames(subject.cijks.Fnorm);
kdata = zeros(numel(find(subject_index.mask)), length(clbls));
for i=1:length(clbls)
    c = cijks.(clbls{i});
    kdata(:, i) = c(subject_index.mask);
end
% spatial
sweight = 0.008;
[nx, ny, nz] = ndgrid(1:sx, 1:sy, 1:sz);
kdata(:, length(clbls) + 1) = sweight * nx(subject.mask);
kdata(:, length(clbls) + 2) = sweight * ny(subject.mask);
kdata(:, length(clbls) + 3) = sweight * nz(subject.mask);

%
distance = 'sqEuclidean';
options = {};
options.MaxIter = 200;
options.UseParallel = false;
options.replicates = 3;
options.start = 'sample';

for k=5:5
    klbl = zeros(sx, sy, sz);
    
    fprintf('Constructing %d clusters at %d iterations with %d replicates...\n', k, options.MaxIter, options.replicates);
    dirout = ['output/kmeans/', subject_index.name, '/', int2str(k), '/'];
    if ~exist(fullfile('.',dirout),'dir')
        mkdir(dirout)
    end

    [IDX, C] = kmeans(kdata, k, 'options', options);
    
    klbl(subject_index.mask) = IDX(:);
    image3d(klbl);
    supertitle(sprintf('Kmeans (%i) all clusters', k));
    export_fig([dirout, 'Kmeans_', int2str(k),'_', smstr, '.pdf'], '-transparent', '-q101', '-nocrop');
    
    % Print individual clusters
    for ki=1:k
        fprintf('%d ', ki);
        klbl = IDX == ki;
        cvol = zeros(sx, sy, sz);
        cvol(subject_index.mask) = klbl(:);
        
        image3d(cvol);
        supertitle(sprintf('Kmeans (%i) cluster %i\n%s\n%s', k, ki, cellstr2str(clbls), mat2str(C(ki,:),2)));
        export_fig([dirout, 'Kmeans_', int2str(k), '_', int2str(ki),'_', smstr, '.pdf'], '-transparent', '-q101', '-nocrop');
    end
    fprintf('\ndone.\n');
end


