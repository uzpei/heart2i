% function curlfield

path = '../../data/heart/rats/atlas/processed/';
fprintf('Loading orientation volumes...\n');

datax = cell2mat(struct2cell(load([path, 'e1x.mat'])));
datay = cell2mat(struct2cell(load([path, 'e1y.mat'])));

e1x = datax;
e1y = datay;
e1z = cell2mat(struct2cell(load([path, 'e1z.mat'])));

%mask = e1x | e1y | e1z;
mask = cell2mat(struct2cell(load([path, 'mask.mat']))) > 0;
%    mask = cell2mat(struct2cell(load([atlasPath, 'myocardium.mat']))) > 0;

sx = size(mask,1);
sy = size(mask,2);
sz = size(mask,3);

% Test dimensions
% e1x = e1x(1:end-1, :, :);
% e1y = e1y(1:end-1, :, :);
% e1z = e1z(1:end-1, :, :);
% mask = mask(1:end-1, :, :);

%% Quiver slices
slice = 50;

maskSlice = mask(:,:,slice);
e1xSlice = e1x(:,:,slice);
e1ySlice = e1y(:,:,slice);
e1zSlice = e1z(:,:,slice);

e1xSliceMasked = e1xSlice(maskSlice);
e1ySliceMasked = e1ySlice(maskSlice);
e1zSliceMasked = e1zSlice(maskSlice);

[X, Y] = ndgrid(1:sx, 1:sy);
X = X(maskSlice);
Y = Y(maskSlice);

figure(1)
clf
subplot(3,3,1);
quiver(X(:),Y(:),e1xSliceMasked, e1ySliceMasked);
axis square
xlim([0 sx/2])
ylim([0 sy/2])
title('e1');

% Compute curl
[cx, cy, cz] = curl(e1x, e1y, e1z);
cxm = cx(:,:,slice);
cym = cy(:,:,slice);
czm = cz(:,:,slice);
cxm = cx(maskSlice);
cym = cy(maskSlice);
czm = cz(maskSlice);

subplot(3,3,2);
imagesc(e1x(:,:,slice));
colormap jet
axis square
xlim([0 sx/2])
ylim([0 sy/2])
title('e1x');

subplot(3,3,3);
imagesc(e1z(:,:,slice));
colormap jet
axis square
xlim([0 sx/2])
ylim([0 sy/2])
title('e1z');

subplot(3,3,4);
%quiver(X(:),Y(:),cxm, cym);
imagesc(abs(cx(:,:,slice)));
caxis([0 0.5])
axis square
xlim([0 sx/2])
ylim([0 sy/2])
colorbar
title('|cx|');

subplot(3,3,5);
%quiver(X(:),Y(:),cxm, cym);
imagesc(abs(cy(:,:,slice)));
caxis([0 0.5])
axis square
xlim([0 sx/2])
ylim([0 sy/2])
colorbar
title('|cy|');

subplot(3,3,6);
%quiver(X(:),Y(:),cxm, cym);
imagesc(abs(cz(:,:,slice)));
caxis([0 0.5])
axis square
xlim([0 sx/2])
ylim([0 sy/2])
colorbar
title('|cz|');

%% Orthogonalize e1 and curl
[cx, cy, cz] = normalize(cx, cy, cz);
[cxo, cyo, czo] = orthogonalize(e1x, e1y, e1z, cx, cy, cz, mask);

% Plot

subplot(3,3,7);
%quiver(X(:),Y(:),cxm, cym);
imagesc(abs(cxo(:,:,slice)));
caxis([0 0.5])
axis square
xlim([0 sx/2])
ylim([0 sy/2])
colorbar
title('|cx-|');

subplot(3,3,8);
%quiver(X(:),Y(:),cxm, cym);
imagesc(abs(cyo(:,:,slice)));
caxis([0 0.5])
axis square
xlim([0 sx/2])
ylim([0 sy/2])
colorbar
title('|cy-|');

subplot(3,3,9);
%quiver(X(:),Y(:),cxm, cym);
imagesc(abs(czo(:,:,slice)));
caxis([0 0.5])
axis square
xlim([0 sx/2])
ylim([0 sy/2])
colorbar
title('|cz-|');

%% Plot slice by slice
[Y, X, Z] = ndgrid(1:sx,1:sy,1:sz);
zeroZ = zeros(sx, sy, 1);
vc = [0, 0, 1];

figure(2)

for gsize=1:9
%h = fspecial('gaussian',[gsizeNormals gsizeNormals], gstdNormals); 
h = fspecial3('gaussian',[gsize, gsize, gsize]); 
nx = imfilter(cxo,h,'replicate');
ny = imfilter(cyo,h,'replicate');
nz = imfilter(czo,h,'replicate');

% Normalize everything
[nx, ny, nz] = normalize(nx, ny, nz);

subplot(3,3,gsize);

for slice=50:50
    maskSlice = mask(:,:,slice);
    
    cxoSlice = nx(:,:,slice);
    cyoSlice = ny(:,:,slice);
    czoSlice = nz(:,:,slice);

    % Align vectors
    % Output centroid
    rp = regionprops(1-maskSlice, 'Centroid');
    centroid = rp(length(rp)).Centroid;

    % Compute the vector from the centroid to slice points
    Xs = X(:,:,slice);
    Ys = Y(:,:,slice);

    vrx = X(:,:,slice) - centroid(1);
    vry = Y(:,:,slice) - centroid(2);
    vrz = zeroZ;
    
    % Compute u = vr x v
    ux = vry .* czoSlice - vrz .* cyoSlice;
    uy = vrz .* cxoSlice - vrx .* czoSlice;
    uz = vrx .* cyoSlice - vry .* cxoSlice;
    
    % Compute the signum of u . vc
    % Use clockwise turning
    msigns = -sign(vc(1) .* ux + vc(2) .* uy + vc(3) .* uz); 
    
    % Flip e1
    cxoSlice = cxoSlice .* msigns;
    cyoSlice = cyoSlice .* msigns;
    czoSlice = czoSlice .* msigns;

    % Only output vectors part of the mask
    cxoSlice = cxoSlice(maskSlice);
    cyoSlice = cyoSlice(maskSlice);
    czoSlice = czoSlice(maskSlice);

    [Xs, Ys] = ndgrid(1:sx, 1:sy);
    Xs = Xs(maskSlice);
    Ys = Ys(maskSlice);
    hold on
    quiver(Xs(:),Ys(:),cxoSlice, cyoSlice);
    plot(centroid(1), centroid(2), 'ro', 'MarkerSize', 20);
    hold off
    
%     xlim([0 sx]);
%     ylim([0 sy]);
    xlim([0 sx/2])
    ylim([0 sy/2])
    title(slice);
   
    pause(0.5);
end
end
