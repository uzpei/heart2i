close all

% hearts = heartxml('../../pnashearts.xml');
%hearts = heartxml('../../heartsRats.xml');
%hearts = heartxml('../../heartsRatAverage.xml');
%hearts = heartxml('hearts.xml');
hearts = heartxml('heartsDogsCropped.xml');

n = length(hearts);
fprintf('%i hearts found\n', n);
for i=1:n
    %fi = 4*(i - 1);
    
    heart = hearts(i);
    path = char(heart.path);

    fprintf('Processing heart %s \n', path);

    % TODO: will eventually need to use the spacing
    % that is stored within the XML file...
    % for now it's hardcoded based on the order in which
    % hearts appear in the XML file.
    
%     isotropy = [];
%     
%     if (i >= 1 && i <= 4)
%         isotropy = [1 1 1];
%     elseif (i == 5)
%         isotropy = [0.43 0.43 1];
%     elseif (i >= 6 && i <= 8)
%         isotropy = [0.3125, 0.3125, 0.8];
%     end
%     axialTransform(path, isotropy);
    axialTransform(path, [1 1 1]);

end