% function [envelope, dx, dy, dz] = compute_outer_sampling(heartPath, zrange)
% zrange is the range of z-subscripts for which the envelope should be
% computed
function [envelope, dx, dy, dz] = compute_outer_sampling(heartPath, zrange)

    fprintf('Loading orientation volumes...\n');
    % Load and swap XY
    datax = cell2mat(struct2cell(load([heartPath, 'e1x.mat'])));
    datay = cell2mat(struct2cell(load([heartPath, 'e1y.mat'])));

    e1x = datay;
    e1y = datax;
    e1z = cell2mat(struct2cell(load([heartPath, 'e1z.mat'])));

    %mask = e1x | e1y | e1z;
    mask = cell2mat(struct2cell(load([heartPath, 'mask.mat']))) > 0;
%    mask = cell2mat(struct2cell(load([heartPath, 'myocardium.mat']))) > 0;

    sx = size(mask,1);
    sy = size(mask,2);
    sz = size(mask,3);

    % Free parameters
    closeSize = 3;

    % The gaussian smoothing kernel size for the DT gradient
    gsize = 5;

    % Normal smoothing
    % Kernel size
    gsizeNormals = 10;

    % Gaussian std
    gstdNormals = 2;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Preprocess Mask 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % First pad the mask to make sure we have enough room
    padSize = closeSize;
    mask2 = padarray(mask,[padSize padSize padSize]);
    %mask2 = mask;

    %%%%% Image closure %%%%%
    fprintf('Computing image closing...\n');
    [x,y,z] = ndgrid(-2*closeSize:2*closeSize);
    se = strel(sqrt(x.^2 + y.^2 + z.^2) <= closeSize);
    mask2 = imclose(mask2, se);

    % Determine the new volume dimension
    border = size(mask2) - size(mask);

    % Remove garbage in corners
    zeroPadSize = border / 2;
    mask2 = mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Compute centroid for each slice
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('Computing centroids...\n');

    % Position grid
    centroids = zeros(sz, 2);

    for z=1:sz
        maskz = mask(:,:,z);

        if (~(max(max(maskz)) > 0))
            continue;
        end

        % Extract orientation
        e1xs = e1x(:,:,z);
        e1ys = e1y(:,:,z);
        centroid = frameCentroid(e1xs, e1ys, maskz);
        centroids(z,:) = centroid(:);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Distance Transform
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fprintf('Computing distance transform...\n');

    % Compute the distance transform of mask2
    dtransform = bwdist(1-mask2);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Compute Gradient
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Smooth the distance transform with a 3D gaussian
    % This function automatically sets the standard deviation such that
    % the full width at half maximum (FWHM) is half the filter size
    h = fspecial3('gaussian',[gsize, gsize, gsize]); 
    fprintf('Combining gradient...\n');
    [dx, dy, dz] = gradient(imfilter(dtransform,h,'replicate'));
    dx = double(dx .* mask);
    dy = double(dy .* mask);
    dz = double(dz .* mask);

    % Normalize gradient
    [dx, dy, dz] = normalize(dx, dy, dz);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Compute Cylindrical Consistency
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Position grid
    [Y, X, Z] = ndgrid(1:sx,1:sy,1:sz);

    for z=zrange(1):zrange(2)
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end

        % Extract sliced quantities
        Xs = X(:,:,z);
        Ys = Y(:,:,z);
        zeroZ = zeros(size(Xs));

        % Extract DT gradient
        nxs = dx(:,:,z);
        nys = dy(:,:,z);
        nzs = dz(:,:,z);

        centroid = centroids(z,:);

        % Compute the vector from the centroid to slice points
        vrx = Xs - centroid(1);
        vry = Ys - centroid(2);
        vrz = zeroZ;

        % Compute the signum of b . vr
        % Normals will point towards the centroid
        msigns = -sign(vrx .* nxs + vry .* nys + vrz .* nzs); 

        % Flip b
        nxsf = nxs .* msigns;
        nysf = nys .* msigns;
        nzsf = nzs .* msigns;

        % Concatenate volumes
        dx(:,:,z) = nxsf(:,:);
        dy(:,:,z) = nysf(:,:);
        dz(:,:,z) = nzsf(:,:);
    end

    % Normalize 
    [dx, dy, dz] = normalize(dx, dy, dz);
    dx = dx .* mask;
    dy = dy .* mask;
    dz = dz .* mask;

    % Apply z-thresholding
    maskthresholded = mask;
    maskthresholded(:,:,1:zrange(1)) = 0;
    maskthresholded(:,:,zrange(2):end) = 0;

    % DT = 1 is on the boundary
    
    % Use exact distance transform
    dtransform = bwdist(1-mask);

    envelope = find(dtransform <= 1.5 & maskthresholded);

end
