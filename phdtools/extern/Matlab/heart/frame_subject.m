function subject = frame_subject(subject)

if (~isempty(subject.dt))
    fprintf('Loading distance transform from file...\n');
    [T, B] = frame_from_distance_transform(subject.e1, subject.dt, subject.mask);
    subject.frame.T = T;
    subject.frame.B = B;
    subject.frame.N = cross(B, T);
else
    [frame, dt] = compute_frames(subject.e1, [], [], subject.mask);
    spath = [species(1).path, subject.name, '/', species(1).innerPath, '/'];
    fprintf('Saving distance transform to file [%sdt.mat]\n', spath);
    save([spath, 'dt.mat'], 'dt');                

    subject.frame.T = frame.Fnorm.T;
    subject.frame.B = frame.Fnorm.B;
    subject.frame.N = cross(frame.Fnorm.B, frame.Fnorm.T);
end
