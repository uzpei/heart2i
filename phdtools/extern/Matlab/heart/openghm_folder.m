function subjects = openghm_folder(searchPath, varargin)

% Define defaults
defaults = struct('GHMpath','.', 'MaskPath', '.', 'MaskName', 'mask.mat', 'CurvatureBits', [true true true]);
defaults.Ignore = { '..' };
options = parseInput(defaults, varargin{:});

% Make sure we have mask and GHM volumes
pathsMask = list_folder(searchPath, 'InnerPath', options.MaskPath);
pathsGHM = list_folder(searchPath, 'InnerPath', options.GHMpath);

if (length(pathsMask) ~= length(pathsGHM))
    error('Invalid folder structure: mask and GHM paths do not correspond');
end

% Assemble paths
subjects = {};
for path_it = 1:length(pathsMask)
    subject_name = pathsMask{path_it};
    subjects(path_it).name = subject_name;
    subjects(path_it).pathMask = [searchPath, subject_name, '/', options.MaskPath];
    subjects(path_it).pathGHM = [searchPath, subject_name, '/', options.GHMpath];
end

%% Determine which GHM volumes we are loading
ghm_list = {};

if (options.CurvatureBits(1))
    ghm_list = [ ghm_list, 'kt_n3_n3' ];
end

if (options.CurvatureBits(2))
    ghm_list = [ ghm_list, 'kn_n3_n3' ];
end

if (options.CurvatureBits(3))
    ghm_list = [ ghm_list, 'kb_n3_n3' ];
end

if (options.CurvatureBits(4))
    ghm_list = [ ghm_list, 'error_n3_n3' ];
end

%% Load curvature volumes
for subject_it = 1:length(subjects)
    mask = loadmat([subjects(subject_it).pathMask, '/', options.MaskName]);
    subjects(subject_it).mask = mask;
    for ghm_it = 1:length(ghm_list)
        subjects(subject_it).(ghm_list{ghm_it}) = loadmat([subjects(subject_it).pathGHM, '/', ghm_list{ghm_it}, '.mat']);
    end
end
