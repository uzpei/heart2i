
% This function loads and preprocesses a heart volume
% The following operations are performed:
% 1) Cylindrical consistency is enforced for all vectors
% 2) Heart is interpolated in isotropic volume 
% 3) A (2D or 3D) distance transform (DT) is computed
% 4) The gradient of the DT is computed
% 5) The DT gradient is projected onto the normal component of the
% eigenvenctors.
function [e1x, e1y, e1z, projnormx, projnormy, projnormz] = axialTransform(dir, varargin)
if (nargin > 1)
    isotropy = varargin{1};
else
    isotropy = [1 1 1];
end
%axialTransform('../../data/heart/rats/rat24012008/mat/', [1 1 1]);
%dir = '../../data/heart/rats/rat24012008/mat/';
%dir = './data/';
%dir = '../../data/heart/canine/average/cropped/';
%isotropy = [1 1 1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Initial Setup
%   TODO: pass all of these as arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cut a border around the volume for faster debugging
% This should be 0 when not debugging
border = 0;

% Whether X-Y should be swapped. When passing from Java exported
% matrices, this should be set to true.
swapXY = true;

% The image closure size
closeSize = 5;

% The gaussian smoothing kernel size
gsize = 5;

% Distance transform gaussian smoothing standard deviation
dtStd = 1;

% Skeletal point threshold. Larger values are more restrictive
% i.e. more points will be considered as being skeletal
nthreshold = 0.8;

% Normal smoothing parameters
% Gaussian kernel size
gsizeNormals = 10;

% Gaussian std
gstdNormals = 2;

% Midline axis
% TODO: this doesn't have to be aligned with the z-axis
% would need to debug to make sure it's still valid
vc = [0, 0, 1];

% Whether the distance transform should be computed in a per-slice basis
% Doing this will result in DT gradients for each slice that lie in the XY
% plane.
perSliceDT = false;

% Whether we should remove the skeletal points from the gradient
% of the distance transform
removeSkeletalPoints = false;

% Image close the mask
closeImage = true;

% Export computations to dirout
exportData = true;

% Drawing
% drawMask = false;
% drawDT = false;
% drawGradient = false;
% drawFlips = false;
% drawSmooth = false;
% drawProjected = true;
drawMask = false;
drawDT = false;
drawGradient = false;
drawFlips = false;
drawSmooth = false;
drawProjected = true;

snapshotMod = 20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Load Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dir = '../../data/heart/rats/rat07052008';
dirout = [dir, 'processed/'];

if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

fprintf('Loading in matrices...\n');
tic
% Load and swap XY
datax = cell2mat(struct2cell(load([dir, 'e1x.mat'])));
datay = cell2mat(struct2cell(load([dir, 'e1y.mat'])));

if (swapXY)
    e1x = datay;
    e1y = datax;
else
    e1x = datax;
    e1y = datay;
end

e1z = cell2mat(struct2cell(load([dir, 'e1z.mat'])));

% mask = cell2mat(struct2cell(load([dir, 'mask.mat']))) > 0;
mask = cell2mat(struct2cell(load([dir, 'myocardium.mat']))) > 0;

sx = size(mask,1);
sy = size(mask,2);
sz = size(mask,3);

%%%% Cut smaller volume for debugging
% sz = sz - 2 * border;
% e1x = e1x(1:end,1:end, border+1:end-border);
% e1y = e1y(1:end,1:end, border+1:end-border);
% e1z = e1z(1:end,1:end, border+1:end-border);
% mask = mask(1:end,1:end, border+1:end-border);

fprintf('Found mask with size = %i,%i,%i\n', size(mask,1),size(mask,2), size(mask,3));
toc

% Show first slice
% figure(1)
% subplot(1,2,1);
% quiver(e1x(:,:,1), e1y(:,:,1), 'r');
% axis equal
% title('initial T');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Preprocess Mask 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First pad the mask to make sure we have enough room
padSize = closeSize;
mask2 = padarray(mask,[padSize padSize padSize]);
%mask2 = mask;

if (closeImage)
    
    %%%%% Image closure %%%%%
    fprintf('Computing image closing...\n');
    tic
    [x,y,z] = ndgrid(-2*closeSize:2*closeSize);
    se = strel(sqrt(x.^2 + y.^2 + z.^2) <= closeSize);
    mask2 = imclose(mask2, se);

    % Determine the new volume dimension
    border = size(mask2) - size(mask);
    %mask2 = mask2(border(1)+1:end-border(1),border(2)+1:end-border(2), border(3)+1:end-border(3));
    %mask = padarray(mask, border/2);

    % Remove garbage in corners
    %mask2 = mask2 .* mask;
    zeroPadSize = border / 2;
    %mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3)) = 0;
    mask2 = mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));
    toc

end    

if (drawMask)
   figure(1);
    clf
    %isosurface(mask,0.5), axis equal, view(3)
    size(mask)
    size(mask2)
    for z=1:sz
        subplot(1,2,1);
        imagesc(mask(:,:,z));
        axis equal
        title('original mask');
        colormap gray

        subplot(1,2,2);
        imagesc(mask2(:,:,z));
        title('closed mask');
        axis equal
        colormap gray


        pause(0.01);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute Cylindrical Consistency for T for each slice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Computing cylindrical consistency...\n');
tic
% Position grid
[Y, X, Z] = ndgrid(1:sx,1:sy,1:sz);

zeroZ = zeros(sx, sy, 1);
centroids = zeros(sz, 2);

for z=1:sz
    % Extract sliced quantities
    maskSlice = mask2(:,:,z);
    
    if (~(max(max(mask(:,:,z))) > 0))
        continue;
    end

    Xs = X(:,:,z);
    Ys = Y(:,:,z);

    % Extract orientation
    e1xs = e1x(:,:,z);
    e1ys = e1y(:,:,z);
    e1zs = e1z(:,:,z);
        
%     % Compute the centroid of this slice
%     rp = regionprops(maskz, 'Centroid');
%     % Skip this volume if no centroid was found
%     if (length(rp) < 1)
%         continue;
%     end
%     centroid = rp(length(rp)).Centroid;
%     centroid = [centroid(1), centroid(2), z];
%    centroid = frameCentroid(e1xs, e1ys, maskz);
    centroid = fcentroid(maskSlice);
    centroids(z,:) = centroid(:);
        
    % Compute the vector from the centroid to slice points
    vrx = X(:,:,z) - centroid(1);
    vry = Y(:,:,z) - centroid(2);
    vrz = zeroZ;
    
    % Compute u = vr x v
    ux = vry .* e1zs - vrz .* e1ys;
    uy = vrz .* e1xs - vrx .* e1zs;
    uz = vrx .* e1ys - vry .* e1xs;
    
    % Compute the signum of u . vc
    % Use clockwise turning
    msigns = -sign(vc(1) .* ux + vc(2) .* uy + vc(3) .* uz); 
    
    % Flip e1
    e1x(:,:,z) = e1xs .* msigns;
    e1y(:,:,z) = e1ys .* msigns;
    e1z(:,:,z) = e1zs .* msigns;
end
clear X;
clear Y;
clear Z;
clear e1xs;
clear e1ys;
clear e1zs;
clear vrx;
clear vry;
clear vrz;
clear ux;
clear uy
clear uz;
clear msigns;
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Interpolate mask and ET on an isotropic grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Assume isotropy in the XY plane
isoscale = isotropy(1) / isotropy(3);

if (abs(isoscale-1) > 0.001)
    fprintf('Computing interpolated isotropic volume...\n');
    tic
    [x,y,z] = ndgrid(1:sx,1:sy,1:sz);

    % New interpolatated volume size
    res = [1, 1, isoscale];
    [xi, yi, zi] = ndgrid(1:res(1):sx, 1:res(2):sy, 1:res(3):sz);

    % size(xi)
    % size(yi)
    % size(zi)
    fprintf('Size before interpolation = %i %i %i\n', size(mask));

    % Compute interpolation
    fprintf('Interpolating masks...\n');
    mask = interpn(x,y,z,double(mask),xi,yi,zi,'linear') > 0;
    mask2 = interpn(x,y,z,double(mask2),xi,yi,zi,'linear') > 0;

    fprintf('Interpolating first eigenvector...\n');
    e1xf = interpn(x,y,z,e1x,xi,yi,zi,'linear');
    e1yf = interpn(x,y,z,e1y,xi,yi,zi,'linear');
    e1zf = interpn(x,y,z,e1z,xi,yi,zi,'linear');

    % e1xf = mask .* e1xf;
    % e1yf = mask .* e1yf;
    % e1zf = mask .* e1zf;

    [e1xf, e1yf, e1zf] = normalize(e1xf, e1yf, e1zf);

    fprintf('Size after interpolation = %i %i %i\n', size(mask));

    % Recompute new dimensions
    sx = size(mask,1);
    sy = size(mask,2);
    sz = size(mask,3);

    % figure(1)
    % clf
    % subplot(1,2,1);
    % isosurface(mask,0.5), axis equal, view(3)
    % camlight 
    % lighting gouraud
    % subplot(1,2,2);
    % isosurface(maski,0.5), axis equal, view(3)
    % camlight 
    % lighting gouraud

    % subplot(1,2,2);
    % quiver(e1xf(:,:,1), e1yf(:,:,1), 'r');
    % axis equal
    % title('initial T');
    toc
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Distance Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Computing distance transform...\n');
tic
% Compute the distance transform of mask2
if (perSliceDT)
    dtransform = zeros(sx,sy,sz);
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end

        dtransform(:,:,z) = bwdist(1-mask2(:,:,z));
    end
else
    dtransform = bwdist(1-mask2);
end

if (perSliceDT)
    % Smooth the distance transform with a gaussian on each slice
    h = fspecial('gaussian',[gsize gsize], dtStd); 
    dtransformSmooth = imfilter(dtransform,h,'replicate');
else
    % Smooth the distance transform with a 3D gaussian
    % This function automatically sets the standard deviation such that
    % the full width at half maximum (FWHM) is half the filter size
    h = fspecial3('gaussian',[gsize, gsize, gsize]); 
    dtransformSmooth = imfilter(dtransform,h,'replicate');
end
toc;

if (drawDT)
    figure(2);
    clf;
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end
        
        subplot(1,2,1);
        imagesc(dtransform(:,:,z));
        axis equal
        title(sprintf('distance transform at %i', z));
        colormap gray

        subplot(1,2,2);
        imagesc(dtransformSmooth(:,:,z));
        axis equal
        title(sprintf('smoothed DT at %i', z));
        colormap gray

        pause(0.1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Compute Gradient
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (perSliceDT)
    dx = zeros(size(mask));
    dy = zeros(size(mask));
    dz = zeros(size(mask));
    
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end

        [dxs, dys] = gradient(dtransformSmooth(:,:,z));
        dx(:,:,z) = dxs(:,:);
        dy(:,:,z) = dys(:,:);
    end
else
    [dx, dy, dz] = gradient(dtransformSmooth);
    dx = double(dx);
    dy = double(dy);
    dz = double(dz);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Remove Skeletal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (removeSkeletalPoints)
    fprintf('Computing skeletal points...\n');
    tic
    % Threshold dx,dy,dz to get rid of skeleton
    normask = sqrt(dx.^2 + dy.^2 + dz.^2) >= nthreshold;
    dxt = dx .* normask;
    dyt = dy .* normask;
    dzt = dz .* normask;

    % Create a skeletal mask (only include voxels within the original mask)
    normask = 1 - ((normask == 0) & (mask == 1));

    % Find skeletal points (label 0 in the skeletal mask)
    dvals = find(normask ~= 0);
    svals = find(normask == 0);

    fprintf('%d skeletal points found out of %d (%f)...\n', length(svals), length(dvals), length(svals)/length(dvals));

    % Create separate position matrices for skeletal poinst and data points
    [X,Y,Z] = ind2sub(size(normask), dvals);
    [Xq,Yq,Zq] = ind2sub(size(normask), svals);

    % Data points
    vx = dx(dvals(:));
    vy = dy(dvals(:));
    vz = dz(dvals(:));

    % Interpolate skeletal point
    fprintf('Interpolating grid data (nearest neighbor)... \n');
    fprintf('This step can take a few minutes.');
    dxi = TriScatteredInterp(X,Y,Z,vx, 'nearest');
    dyi = TriScatteredInterp(X,Y,Z,vy, 'nearest');
    dzi = TriScatteredInterp(X,Y,Z,vz, 'nearest');
    fprintf(' Done!\n');
    toc

    fprintf('Filling in interpolated skeletal volume...\n');
    % Compute the interpolated volume
    fx=dxi(Xq(:),Yq(:),Zq(:));
    fy=dyi(Xq(:),Yq(:),Zq(:));
    fz=dzi(Xq(:),Yq(:),Zq(:));

    fprintf('Filling in matrices...\n');
    % The interpolation result is flat, need to fill matrices
    dxi3 = zeros(sx,sy,sz);
    dyi3 = zeros(sx,sy,sz);
    dzi3 = zeros(sx,sy,sz);
    dxi3(svals(:)) = fx(:);
    dyi3(svals(:)) = fy(:);
    dzi3(svals(:)) = fz(:);

    fprintf('Combining information...\n');
    % Combine the original distance transform gradient with the 
    % updated skeletal points and apply original mask
    nx = (dxi3 + dxt) .* mask;
    ny = (dyi3 + dyt) .* mask;
    nz = (dzi3 + dzt) .* mask;
else
    fprintf('Combining information...\n');
    nx = dx .* mask;
    ny = dy .* mask;
    nz = dz .* mask;
end

% Normalize gradient
[nx, ny, nz] = normalize(nx, ny, nz);
nx(isnan(nx)) = 0;
ny(isnan(ny)) = 0;
nz(isnan(nz)) = 0;

nx = nx .* mask;
ny = ny .* mask;
nz = nz .* mask;

% Look at the result
if (drawGradient)   
    figure(3);
    clf;
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end
        
        subplot(1,2,1);
        quiver(dx(:,:,z), dy(:,:,z), 'b');
        axis equal

        subplot(1,2,2);
        quiver(nx(:,:,z), ny(:,:,z), 'b');
        axis equal

        pause(0.1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute Cylindrical Consistency for normals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Position grid
[Y, X, Z] = ndgrid(1:sx,1:sy,1:sz);
% nxf = zeros(size(mask));
% nyf = zeros(size(mask));
% nzf = zeros(size(mask));

for z=1:sz
    if (~(max(max(mask(:,:,z))) > 0))
        continue;
    end
    
    % Extract sliced quantities
    Xs = X(:,:,z);
    Ys = Y(:,:,z);
    zeroZ = zeros(size(Xs));

    % Extract DT gradient
    nxs = nx(:,:,z);
    nys = ny(:,:,z);
    nzs = nz(:,:,z);
    
    centroid = centroids(z,:);
   
%     hold on
%     imagesc(maskz);
%     colormap gray
%     plot(centroid(1),centroid(2), 'r');
%     hold off
    
    % Compute the vector from the centroid to slice points
    vrx = Xs - centroid(1);
    vry = Ys - centroid(2);
    vrz = zeroZ;
    
    % Compute the signum of b . vr
    % Normals will point towards the centroid
    msigns = -sign(vrx .* nxs + vry .* nys + vrz .* nzs); 
    
    % Flip b
%     nxsf = nxs .* msigns;
%     nysf = nys .* msigns;
%     nzsf = nzs .* msigns;
%     
%     % Concatenate volumes
%     nxf(:,:,z) = nxsf(:,:);
%     nyf(:,:,z) = nysf(:,:);
%     nzf(:,:,z) = nzsf(:,:);
    nx(:,:,z) = nxs .* msigns;
    ny(:,:,z) = nys .* msigns;
    nz(:,:,z) = nzs .* msigns;
end
clear X;
clear Y;
clear Z;
clear nxs;
clear nys;
clear nzs;

if (drawFlips)
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end
        
        % Display quantities
        subplot(2,2,1);
        quiver(e1x(:,:,z), e1y(:,:,z), 'b');
        title(['z-slice ', int2str(z)]);

        subplot(2,2,2);
        quiver(e1xf(:,:,z), e1yf(:,:,z), 'b');

        subplot(2,2,3);
        quiver(nx(:,:,z), ny(:,:,z), 'b');

        subplot(2,2,4);
        quiver(nxf(:,:,z), nyf(:,:,z), 'b');

        pause(0.1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Smooth normals and normalize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h = fspecial('gaussian',[gsizeNormals gsizeNormals], gstdNormals); 
nx = imfilter(nx,h,'replicate');
ny = imfilter(ny,h,'replicate');
nz = imfilter(nz,h,'replicate');

% Normalize everything
[nx, ny, nz] = normalize(nx, ny, nz);

% Reapply mask
% (smoothed vectors can lie outside of the mask) 
nx = nx .* mask;
ny = ny .* mask;
nz = nz .* mask;

if (drawSmooth)
    clf
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end
        
        subplot(2,1,1);
        quiver(nxf(:,:,z), nyf(:,:,z), 'b');
        axis equal

        subplot(2,1,2);
        quiver(nxSmooth(:,:,z), nySmooth(:,:,z), 'b');
        axis equal

        if (z == 1 || mod(z, snapshotMod) == 0)
            export_fig([dirout, 'nslice_', int2str(z), '.pdf'], '-transparent', '-q101');
        else
            pause(0.1);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute Projected Normals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Computing projected normals...\n');

[projnormx, projnormy, projnormz] = orthogonalize(e1x, e1y, e1z, nx, ny, nz, mask);

if (drawProjected)
    [Y, X, Z] = ndgrid(1:sx,1:sy,1:sz);
    
    clf
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end
        
        if (~(z == 1 || mod(z, snapshotMod) == 0))
            continue;
        end
        
        slice = mask2(:,:,z);
        x = X(:,:,z);
        y = Y(:,:,z);
        x = x(slice);
        y = y(slice);

        e1xs = e1x(:,:,z);
        e1ys = e1y(:,:,z);
        e1xs = e1xs(slice);
        e1ys = e1ys(slice);
        
        e2xs = nx(:,:,z);
        e2ys = ny(:,:,z);
        e2xs = e2xs(slice);
        e2ys = e2ys(slice);

        e3xs = projnormx(:,:,z);
        e3ys = projnormy(:,:,z);
        e3xs = e3xs(slice);
        e3ys = e3ys(slice);

        clf
        hold on
%         quiver(e1x(:,:,z), e1y(:,:,z), 'r');
%         quiver(nx(:,:,z), ny(:,:,z), 'g');
%         quiver(projnormx(:,:,z), projnormy(:,:,z), 'b');
        quiver(x, y, e1xs, e1ys, 'r');
        quiver(x, y, e2xs, e2ys, 'g');
        quiver(x, y, e3xs, e3ys, 'b');
        centroid = centroids(z,:);
        plot(centroid(1),centroid(2),'m.', 'MarkerSize',20);
        hold off
        axis equal
        title(['z-slice ', int2str(z)]);

        export_fig([dirout, 'slice_', int2str(z), '.pdf'], '-transparent', '-q101');
%         if (z == 1 || mod(z, snapshotMod) == 0)
%             export_fig([dirout, 'slice_', int2str(z), '.pdf'], '-transparent', '-q101');
%         else
%             pause(0.1);
%         end
        
    end
end

if (exportData)
    if (swapXY)
        tmpe = e1x;
        e1x = e1y;
        e1y = tmpe;
    end
    
    fprintf('Exporting mask and eigenvectors to %s...\n', dirout);
    maskd = double(mask);
    save([dirout, 'mask.mat'], 'maskd');
    save([dirout, 'e1x.mat'], 'e1x');
    save([dirout, 'e1y.mat'], 'e1y');
    save([dirout, 'e1z.mat'], 'e1z');
    save([dirout, 'projnormx.mat'], 'projnormy');
    save([dirout, 'projnormy.mat'], 'projnormx');
    save([dirout, 'projnormz.mat'], 'projnormz');
 
%end

end

