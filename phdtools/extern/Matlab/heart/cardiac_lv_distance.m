%
% Smooth the mask using topological operations (dilation and erosition)
% and returns a smoothed distance transform.
% The last argument is either 'endocardium' or 'epicardium',
% depending on which boundary we are computing the distance transform from. 
function distance = cardiac_lv_distance(mask, varargin)

maskWork = mask;
verbose = true;

% Determine if we compute from epicardium to endocardium or vice-versa
%epiendo = true;

% The image closure size
closeSize = 5;

% IMPORTANT: Close ventricle such that imfill will not leak inside
% TODO: should this be passed as a prameter? The filling amount may vary
% depending on the species... in general better to be aggressive here.
zcut = 3;

% The gaussian smoothing kernel size
gsize = 5;

if (nargin > 1)
    closeSize = varargin{1};
end

if (nargin > 2)
    gsize = varargin{2};
end

boundary = 'epicardium';
if (nargin > 3) 
    boundary = varargin{3};
end

applyMask = true;
if (nargin > 4)
    applyMask = varargin{4};
end

if (verbose)
fprintf('Computing DT from [%s] of morphologically closed mask with dims = %i,%i,%i\n', boundary, size(maskWork,1),size(maskWork,2), size(maskWork,3));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Preprocess Mask 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (closeSize > 0)
    % Remove top slices
    maskProc = maskWork;
    padSize = 3 * closeSize;
%     padSize = 2;
    maskProc = padarray(maskProc,[padSize padSize padSize]);
%     maskProc = padarray(maskProc,[padSize padSize 0]);

    % maskTight = imdilate(logical(maskTight), strel_ball(1));
    % maskTight = imdilate(maskTight, strel_ball(2));
    maskProc = imclose(maskProc, strel_ball(closeSize));
    maskProc = maskProc(padSize+1:end-padSize,padSize+1:end-padSize, padSize+1:end-padSize);

    % Hardcoded sample threshold... any mask with more than 10 vertical
    % slices will be considered a complete volume.
    if (zcut > 0 && size(maskProc,3) > 10)
        maskProc(:,:,end-(zcut-1):end) = 1;
    else
        % Otherwise we need to do some copy and filling
        maskProc = fillslice(maskProc, 'bot');
        maskProc = fillslice(maskProc, 'top');
        image3d(maskProc);
    end
    
%    figure(1);image3d(maskProc);
%    animate3d(maskProc);
%    image3d(maskProc);
    maskFilled = ~imfill(boolean(maskProc), 'holes');
    
    
    if (strcmpi(boundary, 'epicardium'))
        maskProc = maskFilled;
    else
        maskProc = (maskProc - maskFilled)==0;
    end
    
%     figure(2);image3d((maskProc - maskBck)==0);
%    figure(3);image3d(maskProc);

    % figure(1);image3d(maskTight);
    % figure(2);image3d(smask);

    % Determine the new volume dimension
    border = size(maskProc) - size(maskWork);
    % Remove garbage in corners
    zeroPadSize = border / 2;
    maskWork = 1 - maskProc(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));

    % First pad the mask to make sure we have enough room
    padSize = closeSize;
    mask2 = padarray(maskWork,[padSize padSize padSize]);
%     mask2 = padarray(maskWork,[padSize padSize 0]);
    
    %%%%% Image closure %%%%%
    if (verbose)
    fprintf('Computing image closing...\n');
    tic
    end

    se = strel_ball(closeSize);
    mask2 = imdilate(mask2, se);
    mask2 = imerode(mask2, se);

    % Determine the new volume dimension
    border = size(mask2) - size(maskWork);
    zeroPadSize = border / 2;
    mask2 = mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));
else
    mask2 = maskWork;
end

if (verbose)
fprintf('Mask preprocessing time: %.2fs\n', toc)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Distance Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (verbose)
fprintf('Computing distance transform...\n');
tic
end

% Compute the distance transform of mask2
%figure(2);image3d(1-mask2)
%pause(0.1)
distance = double(bwdist(1-mask2));
% figure(2);
if (gsize > 0)
    % Smooth the distance transform with a 3D gaussian
    % This function automatically sets the standard deviation such that
    % the full width at half maximum (FWHM) is half the filter size
    h = fspecial3('gaussian',[gsize, gsize, gsize]); 
    distance = imfilter(distance,h,'replicate');
end

if (strcmpi(boundary, 'endocardium'))
    distance = -distance;
end

% Cannot just apply mask directly. Otherwise we will have issues when computing the
% gradient near the boundary for the endocardium computations since the
% boundary will suddently drop to zero.
% Therefore dilate the mask by one unit before applying it.
%% dilate mask
if (applyMask)
    dilated = imdilate(mask, strel_ball(1));
    distance = distance .* dilated;
end

if (verbose)
fprintf('Distance transform and filtering time: %.2fs\n\n', toc);
end

%image3d(distance);
end

function mask = fillslice(maskProc, part)
        % Otherwise we need to do some copy and filling
        % 1) get bottom slice
        [~, maxs] = cropimage(maskProc, false);
        
        if strcmpi(part, 'bot')
            slice_z = maxs(3,1);
            slice_target = slice_z - 1;
        elseif strcmpi(part, 'top')
            slice_z = maxs(3,2);
            slice_target = slice_z + 1;
        end
        
        slice = maskProc(:,:,slice_z);
        
        fx = round(size(slice,1)/2);
        fy = round(size(slice,2)/2);
        
        % fill from center
        crop_fill = imfill(slice, [fx fy]);
        padSize = 5;
        crop_fill = padarray(crop_fill,[padSize padSize 0]);
        crop_fill = imclose(crop_fill, strel_ball(1));
        
        % crop back
        crop_fill = crop_fill(padSize+1:end-padSize,padSize+1:end-padSize, :);
%         clf
%         image3dslice(crop_fill);
%         size(maskProc)
        maskProc(:,:,slice_target) = crop_fill(:,:,1);
        mask = imclose(maskProc, strel_ball(1));
end