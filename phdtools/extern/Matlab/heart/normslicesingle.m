%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute frame field using the atlas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

useRat = true;

if (useRat)
    % rats
    atlasPath = '../../data/heart/rats/atlas/processed/';
    subjectParentPath = '../../data/heart/rats/';
    subjects = { 'rat07052008/', 'rat17122007/', 'rat21012008/', 'rat22012008/', 'rat24012008/', 'rat24022008_17/', 'rat27022008/', 'rat28012008/' };
    zrange = [20, 85];
    tagname = 'rat_';
    maskname = 'mask.mat';
    dataPath = 'registered/processed/';
    fitPath = 'helicoidFit/';
    scaling = { 0.25, 0.25, 0.25, 1 };

    %%%%%%% IMPORTANT %%%%%%%%
    % This value provides an upper bound on the number of transmural
    % steps that should be taken. Paths requiring longer than this numbe of
    % steps will not be considered
    %%%%%%% IMPORTANT %%%%%%%%
    % For rat use <20
    maxsteps = 20;
else
    atlasPath = '../../data/heart/canine/average/cropped/processed/';
    subjectParentPath = '../../data/heart/canine/';
    subjects = { 'CH06/', 'CH09/', 'CH10/', 'CH11/', 'CH12/', 'CH13/', 'CH15/', 'CH16/' };
    dataPath = 'registered/cropped/processed/';
    fitPath = 'helicoidFit/';
    maskname = 'mask.mat';
    tagname = 'dog_';
    zrange = [];
    % For canine use <60 (about 40 myo samples)
    maxsteps = 60;
    scaling = { 0.3125, 0.3125, 0.3125, 1 };
end


dirout = ['./output/'];
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

% Parameter volumes
vfiles = { 'kt_n3_n3.mat', 'kn_n3_n3.mat', 'kb_n3_n3.mat', 'error_n3_n3.mat' };
nvols = length(vfiles);

numSubjects = length(subjects);

% Clear data points
for vol=1:nvols
    transmural{vol}.data = [];
    transmural{vol}.index = 1;
end

% Compute epicardial envelope from the atlas
mask = cell2mat(struct2cell(load([atlasPath, maskname])));

% Set sizes
isize = size(mask);
sx = isize(1);
sy = isize(2);
sz = isize(3);

if (isempty(zrange))
    zrange = [1, size(mask,3)];
end
[envelope, dx, dy, dz] = compute_outer_sampling(atlasPath, zrange);
fprintf('Found transmural sampling for %i voxels\n', length(envelope));

% Convert envelope indices to subscripts
[I,J,K] = ind2sub(isize, envelope);

%% Begin sampling

testAgainstVolume = 5;
slice = 50;

fprintf('--- Processing %s\n', subjects{testAgainstVolume});
tic

% Set paths
subjectPath = [subjectParentPath, subjects{testAgainstVolume}, dataPath];
parameterPath = [subjectParentPath, subjects{testAgainstVolume}, dataPath, fitPath];

transmural = {};

% Compute restrictive mask
maskstrong = mask & ~isnan(dx) & ~isnan(dy) & ~isnan(dz);

fprintf('Tracing normals...\n');
numpoints = numel(envelope);
for ipoint=1:numpoints

    if (mod(ipoint, round(numpoints / 10)) == 0)
        fprintf('%.2f%% completed\n', 100 * ipoint / numpoints);
    end

    % Get matrix index associated to this point
    i = envelope(ipoint);

    % Transmural direction
    nx = dx(i);
    ny = dy(i);
    nz = dz(i);
    n = [nx, ny, nz];

    % Continue if any component of the direction is NaN
    if (isnan(nx) || isnan(ny) || isnan(nz))
        continue;
    end

    % Follow direction until we are out of the mask
    posi0 = I(ipoint);
    posj0 = J(ipoint);
    posk0 = K(ipoint);
    
    % Only process selected slice
    if (posk0 ~= slice) 
        continue;
    end
    
    % First data is starting point
    posvec = zeros(maxsteps, 3);
    posvec(1, :) = [posi0, posj0, posk0];
    numsteps = 0;

    for step=2:maxsteps
        vstep = step * n;
        
        % Compute position in volume
        posi = round(posi0 + vstep(2));
        posj = round(posj0 + vstep(1));
        posk = round(posk0 + vstep(3));

        % Stop once we step outside of the myocardium
        if (~mask(posi, posj, posk) || posi > sx || posj > sy || posk > sz || posi < 1 || posj < 1 || posk < 1)
            break;
        end

        % Update position vector
        posvec(step, :) = [posi, posj, posk];

    end

    if (step > 4)
        % Only keep valid steps
        posvec(step:end, :) = [];

        % Store trajectory found
        transmural{length(transmural) + 1} = posvec;
    end

end
toc

fprintf('%i sampling found\n', length(transmural));

% Plot trajectories
figure(1)
clf

% First draw mask
hold on
% imgslice = maskstrong(:,:,slice);
imgslice = mask(:,:,slice);
imagesc(imgslice);
colormap gray    
hold off

hold on
for i=1:length(transmural)
%for i=1:1
sampling = transmural{i};
%    scatter3(sampling(1,2), sampling(1,1), sampling(1,3), 'ro');
%scatter(sampling(:,2), sampling(:,1), 'b');
plot(sampling(:,2), sampling(:,1), 'b-');
end
    hold off

[I,J,K] = ind2sub(isize, envelope);

numpoints = numel(envelope);
quiverdata = zeros(numpoints, 4);
for ipoint=1:numpoints

    % Get matrix index associated to this point
    i = envelope(ipoint);

    % Continue if any component of the direction is NaN
    if (isnan(nx) || isnan(ny) || isnan(nz))
        continue;
    end

    % Follow direction until we are out of the mask
    posi = I(ipoint);
    posj = J(ipoint);
    posk = K(ipoint);
    
    % Only process selected slice
    if (posk ~= slice) 
        continue;
    end
    
    % Transmural direction
    nx = dx(i);
    ny = dy(i);
    nz = dz(i);

%     hold on
%     scatter(posj, posi, 'ro');
%     quiver(posj, posi, nx*5, ny*5, 'r', 'AutoScale','off');
%     hold off
    quiverdata(ipoint, 1) = posj;
    quiverdata(ipoint, 2) = posi;
    quiverdata(ipoint, 3) = nx;
    quiverdata(ipoint, 4) = ny;
end

hold on
scatter(quiverdata(:,1), quiverdata(:,2), 'r');
quiver(quiverdata(:,1), quiverdata(:,2), quiverdata(:,3), quiverdata(:,4), 'r', 'AutoScale', 'off');
hold off

set(gca,'YDir','normal')
xlim([1 sx]);
ylim([1 sy]);
zlim([1 sz]);




