function centroid = frameCentroid(vx, vy, mask)

% Compute connected components
    cc = bwconncomp(1 - mask);
    carea = regionprops(cc, 'area');
    numr = cc.NumObjects;
    
    if (max(max(mask)) < 1)
        centroid = [0 0];
        return;
    end

%     for k=1:numr
%         fprintf('Area (voxels) for connected component %d = %d\n', k, carea(k).Area)
%     end

    % If we have a hole in the mask then we can compute its centroid
    % otherwise find max swirling location
    % Condition for computing centroid is two separate regions and
    % both of them have more than 1 voxel
    rsize1 = 0;
    rsize2 = 0;
    rindex1 = 0;
    rindex2 = 0;
    for i=1:numr
        if (carea(i).Area > rsize1)
            if (rsize1 >  rsize2)
                rsize2 = rsize1;
                rindex2 = rindex1;
            end
            
            rsize1 = carea(i).Area;
            rindex1 = i;
        elseif (carea(i).Area > rsize2)
            rsize2 = carea(i).Area;
            rindex2 = i;
        end
    end

    if (rsize1 > 0 && rsize2 > 0)
        % We have access to a hole in the chamber so use its centroid
        rp = regionprops(1-mask, 'Centroid');
%         centroid = rp(rindex2).Centroid;
        centroid = rp(length(rp)).Centroid;
%        fprintf('Region props yields: %f, %f\n', centroid(1), centroid(2));
    else
        % No hole was found. Estimate swirling location
        centroid = vcentroid(vx, vy, mask);
%        fprintf('Swirling yields: %f, %f\n', centroid(1), centroid(2));
    end
end
