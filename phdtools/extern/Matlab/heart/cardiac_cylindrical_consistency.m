function [Vout, centroids] = cardiac_cylindrical_consistency(Vin, mask)

[sx, sy, sz] = size(mask);

%% Apply cylindrical consistency to e1

% size(V)
% size(mask)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute Cylindrical Consistency of V within each slice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Precompute centroids along Z-axis
centroids = zeros(sz, 2);
for z=1:sz
    maskSlice = mask(:,:,z);
    centroid = fcentroid(maskSlice);
    centroids(z,:) = centroid(:);
end

% Compute re-alignment
[X, Y, Z] = ndgrid(1:sx,1:sy,1:sz);

% Midline axis
% TODO: this doesn't have to be aligned with the z-axis
% would need to debug to make sure it's still valid
vc = [0, 0, 1];

% Zero-vector
zeroZ = zeros(sx, sy);
Vout = zeros(size(Vin));
for z=1:sz
    Xs = X(:,:,z);
    Ys = Y(:,:,z);

    % Extract orientation
    e1xs = Vin(:,:,z,1);
    e1ys = Vin(:,:,z,2);
    e1zs = Vin(:,:,z,3);
        
    centroid = centroids(z, :);

    % Skip NaN slices (i.e. where there are no centroids)
    if (numel(centroid) < 2 || max(isnan(centroid)) > 0)
        continue;
    end
    
%     centroid
        
    % Compute the vector from the centroid to slice points
    vrx = X(:,:,z) - centroid(1);
    vry = Y(:,:,z) - centroid(2);
    vrz = zeroZ;
    
    % Compute u = vr x v
    ux = vry .* e1zs - vrz .* e1ys;
    uy = vrz .* e1xs - vrx .* e1zs;
    uz = vrx .* e1ys - vry .* e1xs;
    
    % Compute the signum of u . vc
    % Use clockwise turning
    msigns = -sign(vc(1) .* ux + vc(2) .* uy + vc(3) .* uz); 
    
    % Flip e1
    Vout(:,:,z,1) = e1xs(:,:) .* msigns;
    Vout(:,:,z,2) = e1ys(:,:) .* msigns;
    Vout(:,:,z,3) = e1zs(:,:) .* msigns;
end

