%n = 20;

for n=2:2:40
mask = logical(rand(n,n,n) > 0.5);
tic
poisson_solver(rand(n,n,n), mask);
t=toc
ctimes(n/2) = t;
end

%%
plot(2:2:40,ctimes)

%%
mask = logical(rand(n,n,n) > 0.5);
poisson_solver(rand(n,n,n), mask);

%%
ninds = 2:2:64;
figure(1);
clear ctimes
for n=ninds
%     [phi, t] = poisson_solver(n);
    mask = logical(rand(n,n,n) > 0.2);
    V = rand(n,n,n);
    [phi, dphi, t] = poisson_solver(V, mask);

    ctimes(n/2) = t;
    
    subplot(211)
    image3dslice(phi, 'z');
    colorbar
    axis image

    subplot(212)
    image3dslice(dphi(:,:,:,1), 'z');
    colorbar
    axis image

    pause(0.1)
end

%
figure(2);
clf
plot(ninds, ctimes, 'r');

%% Try DTI volume
[e1, e2, e3, mask] = opendtmri(path, 'mask.mat', [2 1 3]);
e1x = e1(:,:,:,1);
e1y = e1(:,:,:,2);
e1z = e1(:,:,:,3);

% Cut volumes
zcut = 25:(25+63);
e1x = e1x(:,:,zcut);
e1y = e1y(:,:,zcut);
e1z = e1z(:,:,zcut);
e1 = assemble(e1x, e1y, e1z);
mask = mask(:,:,zcut);

%%
psi = divergence(e1x, e1y, e1z);

%%
tic
[phi, dphi] = poisson_solver(psi, mask);
toc

% Curl-free component
u = dphi;

% Divergence-free component
v = normalize(e1 - dphi); 

%%
saxes = { 'x', 'y', 'z' };
for fi=1:3
    saxis = saxes{fi};
    
    subplot(221);quiverslice(mask, e1, saxis); title('original field'); axis image;
    subplot(222);image3dslice(phi, saxis); title('poisson solution'); colorbar;axis image;
    subplot(223);quiverslice(mask, v, saxis); title('divergence-free field'); axis image;
    [un, dnorm] = normalize(u);
    dnorm(:,:,:,1) = dnorm(:,:,:,1) .* mask;
    % subplot(224);quiverslice(mask, u, 'z'); title('curl-free');
    subplot(224);image3dslice(dnorm(:,:,:,1), saxis); title('curl-free magnitude'); colorbar; axis image;
    
    export_fig('.', ['divfree_', saxes{fi}, '.pdf'], '-transparent', '-q101');
end

