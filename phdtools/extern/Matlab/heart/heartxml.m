function hearts = loadHeartXML(xmlfile)

xDoc = xmlread(xmlfile);

% NOTE: this is an important call
% otherwise Matlab interprets whitespaces within the XML file
% in a strange way. This way we can keep human-readable XML files
removeIndentNodes(xDoc.getChildNodes );

%xmlwrite(xDoc)

% Get the "Hearts" node
heartsNode = xDoc.getDocumentElement;

% Loop over all hearts
heartNode = heartsNode.getFirstChild;

i = 1;
while ~isempty(heartNode)
    heart.path = heartNode.getAttribute('path');
    
    heart.id = int2str(i);
    
    hearts(i) = heart;
    
    heartNode = heartNode.getNextSibling;
    i = i + 1;
end
end

