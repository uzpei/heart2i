%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This experiment explores the direct computations of the cijk connection
% forms.
% Different models of the cijk are used for extrapolating neighborhoods and
% compared together on error histograms.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

setpath;

searchPath = '../../data/heart/rat/sample1/';
experiment_options.ElementOrder = [1 2 3];
experiment_options.PostPath = 'registered';
experiment_options.MaskedIndexing = true;
experiment_options.SinglePrecision = false;
experiment_options.OptimizationFrameOnly = false;
experiment_options.FrameNormal = true;
experiment_options.FrameCurl = false;
experiment_options.FrameEig = false;
experiment_options.PrecomputeDirectConnections = false;
experiment_options.Bounds = [1, 1, 1];
%experiment_options = struct2paramcell(struct('ElementOrder', [1 2 3], 'PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));

% Load all subjects, frames, and connection forms
experiment_options = struct2paramcell(experiment_options);
subjects = compute_frames_con_all(searchPath, experiment_options{:});
frameFields = fieldnames(subjects(1).frames);
image3d(subjects(1).mask)
subject = subjects(1);

%% Plot and export quivers
[sx, sy, sz] = size(subject.mask);
[x,y,z] = meshgrid(1:sy, 1:sx, 1:sz);

% Create a colormap based on the vector direction
Vx = subject.e1(:,:,:,1);
Vy = subject.e1(:,:,:,2);
Vz = subject.e1(:,:,:,3);
phi = asin(Vz);
theta = asin(Vy ./ Vx);
V = real(phi);
%V = resize(V, subsample);


% Extract euler angles
clf
%[verts averts] = streamslice(Vx,Vy,Vz,10,10,10); 
%streamline([verts averts], [0.1 1000])
hold on
slx = 10;sly = 10;slz = 20;
%slx = 1;sly = 1;slz = 2;
slice(x,y,z,V,slx,sly,slz)
colormap(customap('redwhiteblue'))
view(135, 30)
%view(0, 90)
%shading flat
shading interp
grid off

% vectors
sfactor = 1;
subsample = round(sfactor * [sx, sy, sz]);
mask = resize(subject.mask, subsample);
[sx, sy, sz] = size(mask);
Vx = resize(Vx, subsample);
Vy = resize(Vy, subsample);
Vz = resize(Vz, subsample);
[x,y,z] = meshgrid(1:sy, 1:sx, 1:sz);
x = x ./ sfactor;
y = y ./ sfactor;
z = z ./ sfactor;

Vxs = Vx(:,:,slz);
Vys = Vy(:,:,slz);
Vzs = Vz(:,:,slz);
xs = x(:,:,slz);
ys = y(:,:,slz);
zs = z(:,:,slz);
quiver3(xs, ys, 1+zs, Vys, Vxs, Vzs, 'Color', [0 0 0]);

Vxs = Vx(slx,:,:);
Vys = Vy(slx,:,:);
Vzs = Vz(slx,:,:);
xs = x(slx,:,:);
ys = y(slx,:,:);
zs = z(slx,:,:);
quiver3(xs, 1+ys, zs, Vys, Vxs, Vzs, 'Color', [0 0 0]);

Vxs = Vx(:,sly,:);
Vys = Vy(:,sly,:);
Vzs = Vz(:,sly,:);
xs = x(:,sly,:);
ys = y(:,sly,:);
zs = z(:,sly,:);
quiver3(1+xs, ys, 1+zs, Vys, Vxs, Vzs, 'Color', [0 0 0]);

hold off

axis image

export_fig('.', 'dtivec.png', '-transparent', '-q101', '-nocrop');
