testpath = '../../data/heart/pig/processed/SS/';
%%
filtering = [10, 0.2];
[e10, mask] = open_principal_direction(testpath, testpath, 'e1', 10,0.4);
e10c  = cardiac_cylindrical_consistency(e10, mask);
%%
figure(1);image3d(e10);colormap jet;
figure(2);image3d(e10c-e10);colormap jet

%%
[e1, mask] = open_principal_direction(testpath, testpath, 'e1', filtering(1), filtering(2));
% Swap e1x, e1y
e1(:,:,:,[1 2 3]) = e1(:,:,:,[2 1 3]);
e10(:,:,:,[1 2 3]) = e10(:,:,:,[2 1 3]);
% cylindrical consistency
[e1c, centroids] = cardiac_cylindrical_consistency(e1, mask);
[e10c, centroids] = cardiac_cylindrical_consistency(e10, mask);

figure(1);image3d(e10c);colormap jet;
figure(2);image3d(e1c);colormap jet
%%
distance = cardiac_lv_distance(mask);
[f1, f3] = frame_from_distance_transform(e1c, distance, [], mask);
f2 = cross(f3, f1);

%% Plot centroid
imagecentroidz(mask);

%%
figure(1);clf;quiverslice(mask, e10c, 'z');
figure(2);clf;quiverslice(mask, e1c, 'z');

%% Compute forms
F(:,:,:,1:3) = f1(:,:,:,:);
F(:,:,:,4:6) = f2(:,:,:,:);
F(:,:,:,7:9) = f3(:,:,:,:);
cijks = cforms(F);

%% Plot connection forms
cformplot(cijks, mask, 'z');

%%
t123 = 0.02;
t131 = 0.02;
c123t = mask + ((abs(cijks.c123) > t123) & (abs(cijks.c123) > t131));
animate3d(c123t);

%% k-means

% Assemble clustering data
% c-forms to use
cmask = [1:6];

clbls = fieldnames(cijks);
kdata = zeros(numel(find(mask)), length(cmask));
for i=1:length(cmask)
    c = cijks.(clbls{cmask(i)});
    kdata(:, i) = c(mask);
end

% Add another variable, spatial locality
% i.e. favor clusters that spatially are close to each
useSpatialWeight = true;
if (useSpatialWeight)
    sweight = mean(kdata(:)) / 5;
    [nx, ny, nz] = ndgrid(1:sx, 1:sy, 1:sz);
    kdata(:, size(kdata, 2) + 1) = sweight * nx(mask);
    kdata(:, size(kdata, 2) + 2) = sweight * ny(mask);
    kdata(:, size(kdata, 2) + 3) = sweight * nz(mask);
end

distance = 'sqEuclidean';
options = {};
options.MaxIter = 200;
options.UseParallel = true;
options.replicates = 1;
options.start = 'uniform';

for k=10
    klbl = zeros(sx, sy, sz);
    
    fprintf('Constructing %d clusters at %d iterations with %d replicates...\n', k, options.MaxIter, options.replicates);
%     dirout = ['output/kmeans/', subject_index.name, '/', int2str(k), '/'];
%     if ~exist(fullfile('.',dirout),'dir')
%         mkdir(dirout)
%     end

    [IDX, C] = kmeans(kdata, k, 'options', options);
    
    klbl(mask) = IDX(:);
    image3d(klbl);
end

%%
animate3d(klbl);