% Return the three principal components of the given heart mask
% as columns of the matrix P = [p1, p2, p3].
% In general, P(1) 
function [P, Xm] = pca_heart(mask)

pxlist = find(mask);
n = length(pxlist);
[I, J, K] = ind2sub(size(mask), pxlist);

% Write samples as columns and features as rows such that
%     [ x_00     x_n0 ]
% X = | ...  ... ...  |
%     [ x_0n     x_nn ]
%
X = [I,J,K]';

% center data
Xm = mean_masked(mask);
X0 = bsxfun(@minus,X,Xm); 

[V, D] = eig(X0*X0');

if ~issorted(diag(D))
    [V,D] = eig(A);
    [D,I] = sort(diag(D));
    V = V(:, I);
end

P(:,1) = normalize(V(:,1)');
P(:,2) = normalize(V(:,2)');
P(:,3) = normalize(V(:,3)');


