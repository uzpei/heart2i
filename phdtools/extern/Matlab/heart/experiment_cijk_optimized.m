%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This experiment explores the direct computations of the cijk connection
% forms.
% Different models of the cijk are used for extrapolating neighborhoods and
% compared together on error histograms.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

setpath;

searchPath = '../../data/heart/rat/sample1/';
%searchPath = '../jobs/data/heart/rats/sample1/';

experiment_options.ElementOrder = [1 2 3];
experiment_options.PostPath = 'registered';
experiment_options.MaskedIndexing = true;
experiment_options.SinglePrecision = false;
experiment_options.OptimizationFrameOnly = false;
experiment_options.FrameNormal = true;
experiment_options.FrameCurl = false;
experiment_options.FrameEig = false;
experiment_options.PrecomputeDirectConnections = false;
experiment_options.Bounds = [1, 1, 0.7];
%experiment_options = struct2paramcell(struct('ElementOrder', [1 2 3], 'PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));

% Load all subjects, frames, and connection forms
experiment_options = struct2paramcell(experiment_options);
subjects = compute_frames_con_all(searchPath, experiment_options{:});
image3d(subjects(1).mask)

%% Optimize
subject = subjects(1);
[sx, sy, sz] = size(subject.mask);
%subject = crop_subject(subject, { 1:sx, 1:sy, 30:33 }, 'padding', [2 2 2]);

F = framefield(subject.frames.Fnorm.T, [], subject.frames.Fnorm.B);

p = 0.01;

%% Test valid indices
% maskInds = find(subject.mask > 0);
% inds = maskInds';
% inds = inds(1:round(p * numel(inds)));
% f = {};
% n = 1;
% vi = 1;
% f{1}{1} = F.T(:,:,:,1);
% f{1}{2} = F.T(:,:,:,2);
% f{1}{3} = F.T(:,:,:,3);
% f{2}{1} = F.N(:,:,:,1);
% f{2}{2} = F.N(:,:,:,2);
% f{2}{3} = F.N(:,:,:,3);
% f{3}{1} = F.B(:,:,:,1);
% f{3}{2} = F.B(:,:,:,2);
% f{3}{3} = F.B(:,:,:,3);
% 
% for parfor_index = 1:numel(inds)
% %for parfor_index = round(0.95*numel(inds)):numel(inds)
%     ind = inds(parfor_index);
%     [i0, j0, k0] = ind2sub(size(f{1}{1}), ind);
%     ind0 = [i0; j0; k0];
%     for ni = -n:n
%     for nj = -n:n
%     for nk = -n:n
%         offset = [ni; nj; nk];
%         ijk = ind0 + offset;
%         f1v = [f{vi}{1}(ijk(1), ijk(2), ijk(3)), f{vi}{2}(ijk(1), ijk(2), ijk(3)), f{vi}{3}(ijk(1), ijk(2), ijk(3))]; 
%     end
%     end
%     end
% 
% end

copt = cijks_optimized(F, subject.mask, p, 'cmask', [0 0 1 0 0 0 0 0 0], 'verbose', 'off');

save('copt.mat', 'copt');

image3d(copt{3});

%%
figure(2);histnorm_thresholded(copt{3}(copt{3}~=0), 100, 1);

%%
figure(2);animate3d(copt{3});

%%
c123 = cijk(F.T, F.N, F.B);
figure; image3d(c123)
caxis([-0.5908    0.0111])

%%
data = load('copt.mat');
copt = data.copt;
A = copt{3};
% figure(1);image3d(A);
% figure(2);histnorm_thresholded(A(subject.mask), 100, 1);
%
%image3d(copt{1})
animate3d(copt{1}, 0.1, 1, [-0.3, 0.3]);
% for i=1:sz
% image3dslice(copt{3}, 'z', i);
% caxis([-0.3, 0.3]);
% colorbar
% pause(0.1)
% end
