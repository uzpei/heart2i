clear all

setpath;

% Load all subjects, frames, and connection forms
% subjects = compute_frames_con_all('../../data/heart/rat/atlas_FIMH2013/', 'registered');
%searchPath = '../../data/heart/rat/sample/';
% searchPath = '../../data/heart/rat/atlas_FIMH2013/';
%searchPath = '../../data/heart/canine/';
searchPath = '../../data/heart/rat/sample1/';

experiment_options = struct2paramcell(struct('PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', true, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));
subjects = compute_frames_con_all(searchPath, experiment_options{:});
frameFields = fieldnames(subjects(1).frames);

%%
subject = subjects(1);
%image3d(subject.frames.Fnorm.T);
quiverslice(subject.mask, subject.frames.Fnorm.T, 'z');

T = double(subject.frames.Fnorm.T);
B = double(subject.frames.Fnorm.B);
N = double(normalize(cross(B, T, 4)));
c123 = cijk(T, N, B);
image3d(c123);

%% Combine volumes
% Find the smallest volume in the population
voxels_per_volume = inf;
for subject_index = 1:length(subjects)
    subjects(subject_index).volume_count = numel(find(subjects(subject_index).mask > 0));
    if (subjects(subject_index).volume_count < voxels_per_volume)
        voxels_per_volume = subjects(subject_index).volume_count;
    end
end

clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
for frame_index = 1:length(frameFields);
    for cindex = 1:length(clbls)
        % Initialize empty array
        subjects_combined.(frameFields{frame_index}).(clbls{cindex}) = [];

        % Load in data from each subject's volume
        for subject_index = 1:length(subjects)
            subject = subjects(subject_index);

            % First load all voxels
            all_voxels = subject.cijks.(frameFields{frame_index}).(clbls{cindex});
            
            % Apply mask if necessary
            if (ndims(all_voxels) > 2)
                all_voxels = all_voxels(subject.mask);
            end
            
            % Then randomly and uniformly sample them
            samples = randi(numel(all_voxels), 1, voxels_per_volume);
            all_voxels = all_voxels(samples);

            subjects_combined.(frameFields{frame_index}).(clbls{cindex}) = [subjects_combined.(frameFields{frame_index}).(clbls{cindex}); all_voxels]; 
        end
    end
end

%% Output histograms
figure(1)
nbins = 200;
stdf = 2;
xfig = get(gcf, 'Position');
xt = round(0.01 * xfig(3));
yt = round(0.79 * xfig(4));
tfsize = 28;
%clbls = { 'TNB' };
clbls = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };

for cindex = 1:length(clbls)
    clf
    hold on
    for frame_index = 1:length(frameFields)
        hist_data = subjects_combined.(frameFields{frame_index}).(clbls{cindex});
        histnorm_thresholded(hist_data, nbins, stdf, '-k', 'LineWidth',2, 'Color', colorset(frame_index));

%         t1 = text(xt, yt - 0,  sprintf('mean %.4f', mean(hist_data)), 'Units', 'Pixels');
%         t2 = text(xt, yt - tfsize,  sprintf(' std %.4f', std(hist_data)), 'Units', 'Pixels');
%         set(t1, 'FontName', 'Monospaced', 'FontSize', tfsize);
%         set(t2, 'FontName', 'Monospaced', 'FontSize', tfsize);
        set(gca, 'FontSize', tfsize);

    end
    hold off 
    alims = axis;
    line([0 0], [alims(3) alims(4)], 'LineWidth', 1, 'LineStyle', '--', 'Color', [0 0 0])
    legend({frameFields{:}, 'zero'});
    title(clbls{cindex});
    pause(0.1);
    export_fig('.', ['c_', clbls{cindex}, '_histo.pdf'], '-transparent', '-q101', '-nocrop');
end

