
% This function loads and preprocesses a heart volume
% The following operations are performed:
% 1) Cylindrical consistency is enforced for all vectors
% 2) Heart is interpolated in isotropic volume 
% 3) A (2D or 3D) distance transform (DT) is computed
% 4) The gradient of the DT is computed
% 5) The DT gradient is projected onto the normal component of the
% eigenvenctors.
function [B, dtransformSmooth] = estimate_wall_normals(mask, varargin)

if (nargin > 1)
    drawGradient = varargin{1};
else
    drawGradient= false;
end

verbose = false;

% The image closure size
closeSize = 5;

% The gaussian smoothing kernel size
gsize = 5;

% Distance transform gaussian smoothing standard deviation
dtStd = 1;

% Skeletal point threshold. Larger values are more restrictive
% i.e. more points will be considered as being skeletal
nthreshold = 0.8;

% Normal smoothing parameters
% Gaussian kernel size
gsizeNormals = 10;

% Gaussian std
gstdNormals = 2;

% Whether we should remove the skeletal points from the gradient
% of the distance transform
removeSkeletalPoints = false;

% Image close the mask
closeImage = true;

sx = size(mask,1);
sy = size(mask,2);
sz = size(mask,3);

%%%% Cut smaller volume for debugging
% sz = sz - 2 * border;
% e1x = e1x(1:end,1:end, border+1:end-border);
% e1y = e1y(1:end,1:end, border+1:end-border);
% e1z = e1z(1:end,1:end, border+1:end-border);
% mask = mask(1:end,1:end, border+1:end-border);

if (verbose)
fprintf('Found mask with size = %i,%i,%i\n', size(mask,1),size(mask,2), size(mask,3));
end

% Show first slice
% figure(1)
% subplot(1,2,1);
% quiver(e1x(:,:,1), e1y(:,:,1), 'r');
% axis equal
% title('initial T');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Preprocess Mask 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First pad the mask to make sure we have enough room
padSize = closeSize;
mask2 = padarray(mask,[padSize padSize padSize]);
%mask2 = mask;

if (closeImage)
    
    %%%%% Image closure %%%%%
    if (verbose)
    fprintf('Computing image closing...\n');
    tic
    end
    
    [x,y,z] = ndgrid(-2*closeSize:2*closeSize);
    se = strel(sqrt(x.^2 + y.^2 + z.^2) <= closeSize);
    mask2 = imclose(mask2, se);

    % Determine the new volume dimension
    border = size(mask2) - size(mask);
    %mask2 = mask2(border(1)+1:end-border(1),border(2)+1:end-border(2), border(3)+1:end-border(3));
    %mask = padarray(mask, border/2);

    % Remove garbage in corners
    %mask2 = mask2 .* mask;
    zeroPadSize = border / 2;
    %mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3)) = 0;
    mask2 = mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));

    if (verbose)
    toc
    end

end    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Distance Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (verbose)
fprintf('Computing distance transform...\n');
tic
end

% Compute the distance transform of mask2
dtransform = bwdist(1-mask2);

% Smooth the distance transform with a 3D gaussian
% This function automatically sets the standard deviation such that
% the full width at half maximum (FWHM) is half the filter size
h = fspecial3('gaussian',[gsize, gsize, gsize]); 
dtransformSmooth = imfilter(dtransform,h,'replicate');
dtransformSmooth = dtransformSmooth .* mask;

if (verbose)
toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Compute Gradient
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[dx, dy, dz] = gradient(dtransformSmooth);
dx = double(dx);
dy = double(dy);
dz = double(dz);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Remove Skeletal points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (removeSkeletalPoints)
    fprintf('Computing skeletal points...\n');
    tic
    % Threshold dx,dy,dz to get rid of skeleton
    normask = sqrt(dx.^2 + dy.^2 + dz.^2) >= nthreshold;
    dxt = dx .* normask;
    dyt = dy .* normask;
    dzt = dz .* normask;

    % Create a skeletal mask (only include voxels within the original mask)
    normask = 1 - ((normask == 0) & (mask == 1));

    % Find skeletal points (label 0 in the skeletal mask)
    dvals = find(normask ~= 0);
    svals = find(normask == 0);

    fprintf('%d skeletal points found out of %d (%f)...\n', length(svals), length(dvals), length(svals)/length(dvals));

    % Create separate position matrices for skeletal poinst and data points
    [X,Y,Z] = ind2sub(size(normask), dvals);
    [Xq,Yq,Zq] = ind2sub(size(normask), svals);

    % Data points
    vx = dx(dvals(:));
    vy = dy(dvals(:));
    vz = dz(dvals(:));

    % Interpolate skeletal point
    fprintf('Interpolating grid data (nearest neighbor)... \n');
    fprintf('This step can take a few minutes.');
    dxi = TriScatteredInterp(X,Y,Z,vx, 'nearest');
    dyi = TriScatteredInterp(X,Y,Z,vy, 'nearest');
    dzi = TriScatteredInterp(X,Y,Z,vz, 'nearest');
    fprintf(' Done!\n');
    toc

    fprintf('Filling in interpolated skeletal volume...\n');
    % Compute the interpolated volume
    fx=dxi(Xq(:),Yq(:),Zq(:));
    fy=dyi(Xq(:),Yq(:),Zq(:));
    fz=dzi(Xq(:),Yq(:),Zq(:));

    fprintf('Filling in matrices...\n');
    % The interpolation result is flat, need to fill matrices
    dxi3 = zeros(sx,sy,sz);
    dyi3 = zeros(sx,sy,sz);
    dzi3 = zeros(sx,sy,sz);
    dxi3(svals(:)) = fx(:);
    dyi3(svals(:)) = fy(:);
    dzi3(svals(:)) = fz(:);

%    fprintf('Combining information...\n');
    % Combine the original distance transform gradient with the 
    % updated skeletal points and apply original mask
    nx = (dxi3 + dxt) .* mask;
    ny = (dyi3 + dyt) .* mask;
    nz = (dzi3 + dzt) .* mask;
else
%    fprintf('Combining information...\n');
    nx = dx .* mask;
    ny = dy .* mask;
    nz = dz .* mask;
end

% Normalize gradient
[nx, ny, nz] = normalize(nx, ny, nz);
nx(isnan(nx)) = 0;
ny(isnan(ny)) = 0;
nz(isnan(nz)) = 0;

nx = nx .* mask;
ny = ny .* mask;
nz = nz .* mask;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute Cylindrical Consistency for normals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Position grid
[Y, X, Z] = ndgrid(1:sx,1:sy,1:sz);
% nxf = zeros(size(mask));
% nyf = zeros(size(mask));
% nzf = zeros(size(mask));

for z=1:sz
    if (~(max(max(mask(:,:,z))) > 0))
        continue;
    end
    
    % Extract sliced quantities
    Xs = X(:,:,z);
    Ys = Y(:,:,z);
    zeroZ = zeros(size(Xs));

    % Extract DT gradient
    nxs = nx(:,:,z);
    nys = ny(:,:,z);
    nzs = nz(:,:,z);
    
    maskSlice = mask2(:,:,z);
    centroid = fcentroid(maskSlice);
    centroids(z,:) = centroid(:);
    centroid = centroids(z,:);
   
%     hold on
%     imagesc(maskz);
%     colormap gray
%     plot(centroid(1),centroid(2), 'r');
%     hold off
    
    % Compute the vector from the centroid to slice points
    vrx = Xs - centroid(1);
    vry = Ys - centroid(2);
    vrz = zeroZ;
    
    % Compute the signum of b . vr
    % Normals will point towards the centroid
    msigns = -sign(vrx .* nxs + vry .* nys + vrz .* nzs); 
    
    % Flip b
%     nxsf = nxs .* msigns;
%     nysf = nys .* msigns;
%     nzsf = nzs .* msigns;
%     
%     % Concatenate volumes
%     nxf(:,:,z) = nxsf(:,:);
%     nyf(:,:,z) = nysf(:,:);
%     nzf(:,:,z) = nzsf(:,:);
    nx(:,:,z) = nxs .* msigns;
    ny(:,:,z) = nys .* msigns;
    nz(:,:,z) = nzs .* msigns;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Smooth normals and normalize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h = fspecial('gaussian',[gsizeNormals gsizeNormals], gstdNormals); 
nx = imfilter(nx,h,'replicate');
ny = imfilter(ny,h,'replicate');
nz = imfilter(nz,h,'replicate');

% Normalize everything
[nx, ny, nz] = normalize(nx, ny, nz);

% Reapply mask
% (smoothed vectors can lie outside of the mask) 
nx = nx .* mask;
ny = ny .* mask;
nz = nz .* mask;

B = zeros(sx, sy, sz, 3);
B(:,:,:,1) = nx(:,:,:);
B(:,:,:,2) = ny(:,:,:);
B(:,:,:,3) = nz(:,:,:);

%% Look at the result
if (drawGradient)   
    figure(1);
    clf;
    for z=1:sz
        if (~(max(max(mask(:,:,z))) > 0))
            continue;
        end
        
        quiver(nx(:,:,z), ny(:,:,z), 'b');
        axis image

        pause(1);
    end
end
