function [frames, dt] = compute_frames(e1, ~, e3, mask, varargin)

% Parse options
options = parseInput(struct('SinglePrecision', false, 'SwapXY', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false), varargin{:});
framefield_options = struct2paramcell(struct('Mask', mask, 'ApplyMask', false, 'SwapXY', options.SwapXY, 'Assemble', false', 'StoreF2', false', 'SinglePrecision', options.SinglePrecision));
frames = {};

%%
[sx, sy, sz] = size(mask);

if (options.FrameEig)
    fprintf('===> Constructing eigen-framefield <===\n');
    
    %% Construct eigen-framefield
    Feig = framefield(e1, [], e3, framefield_options{:});
    frames.Feig = Feig;
end

if (options.FrameNormal)
    fprintf('===> Constructing myonormal-framefield <===\n');
    
    %% Compute heart normals with closing and DT smoothing of same magnitude
    nmag = 5;
    dt = cardiac_lv_distance(mask, nmag, nmag);

    %% Compute gradient of distance transform
    [Tnorm, Bnorm] = frame_from_distance_transform(T, dt, mask);

    %% Compute N
%     Nnorm = cross(Bnorm,Tnorm);

    % Construct normal-framefield
    Fnorm = framefield(Tnorm, [], Bnorm, framefield_options{:});

    frames.Fnorm = Fnorm;
end

if (options.FrameCurl)
    fprintf('===> Constructing curl-framefield <===\n');
    
    %% Compute curl field
    % Use smoothed e1 vector for curl computations
    gstd = 1.5;
    gsize = 6 * gstd;
    h = fgaussian3([gsize,gsize,gsize], [gstd, gstd, gstd]);
    Txsmooth = imfilter(T(:,:,:,1),h,'replicate');
    Tysmooth = imfilter(T(:,:,:,2),h,'replicate');
    Tzsmooth = imfilter(T(:,:,:,3),h,'replicate');
    Btcurl = zeros(sx, sy, sz, 3);

    % Compute curl
    [Btcurl(:,:,:,1), Btcurl(:,:,:,2), Btcurl(:,:,:,3)] = curl(Txsmooth, Tysmooth, Tzsmooth);

    % Apply mask
    Btcurl = Btcurl .* repmat(mask, [1 1 1 3]);

    % Btcurlnorm = sqrt(Btcurl(:,:,:,1).^2 + Btcurl(:,:,:,2).^2 + Btcurl(:,:,:,3).^2);

    % Orthogonalize frame field
    Tcurl = T;
    Bcurl = orthogonalize(Tcurl, Btcurl);

    %%
%     Ncurl = cross(Bcurl,Tcurl);

    % Construct curl-framefield
    Fcurl = framefield(Tcurl, [], Bcurl, framefield_options{:});
    
    frames.Fcurl = Fcurl;
end

