% TODO: add options (e.g. framenormal, framecurl, etc.)
function subjects = compute_frames_con_all(allpath, varargin)

t_all = tic;

defaults.Bounds = [];
defaults.PrecomputeDirectConnections = false;
defaults.ElementOrder = [1 2 3]; 
defaults.SinglePrecision = false;
defaults.MaskedIndexing = false; 
defaults.OptimizationFrameOnly = false; 
defaults.PostPath = '.'; 
defaults.FrameNormal = true;
defaults.FrameCurl = false;
defaults.FrameEig = false;
defaults.IterativeSmoothing = [ false, 0.1, 1 ];
defaults.CropVolumes = false;

options = parseInput(defaults, varargin{:});
option_cells = struct2paramcell(options);

cform_options = struct2paramcell(struct('SinglePrecision', options.SinglePrecision));

%%%%%%%% Load all subjects
subjects = opendtmri_folder(allpath, 'CropVolumes', options.CropVolumes, 'PostPath', options.PostPath, 'ElementOrder', options.ElementOrder, 'LoadE1', true', 'LoadE2', options.FrameEig, 'LoadE3', options.FrameEig);

%%%%%%%% Apply smoothing if necessary
% if (options.IterativeSmoothing(1))
%     pdisp('Applying iterative smoothing to first eigenvector...', 'style', 'box');
%     tsmooth = tic;
%     for subject_index = 1:length(subjects)
%         fprintf('(Subject %s)\n', subjects(subject_index).name);
%         subjects(subject_index).e1 = repmat(subjects(subject_index).mask, [1 1 1 3]) .* iterative_gaussian_smoothing(subjects(subject_index).e1, 'iterations', options.IterativeSmoothing(3), 'std', options.IterativeSmoothing(2));
%     end
%     pdisp(sprintf('Finished smoothing. Elapsed time = %.2fs', toc(tsmooth)), 'style', 'box')
% end

%%%%%%% Crop
if (~isempty(options.Bounds))
    subjects = crop_subjects(subjects, options.Bounds, 'padding', [1 1 1]);
end

% Precompute frames
%frame_options = { 'FrameNormal', true, 'FrameCurl', true, 'FrameEig', true };
pdisp('Processing frames', 'style', 'box')
tproc = tic;
for subject_index = 1:length(subjects)
    subject = subjects(subject_index);
    fprintf('-> Subject %s...\n', subject.name);
    
    % Only compute myocardium normal if necessary 
    if (options.FrameNormal && ~isempty(subject.dt_endo) && ~isempty(subject.dt_epi))
        pdisp('Loading distance transform from file.', 'style', '*');
        [T, B] = frame_from_distance_transform(subject.e1, subject.dt_endo, subject.dt_epi, subject.mask);
        Fnorm = {};
        Fnorm.T = T;
        Fnorm.B = B;
        
        % Do not compute normal
        options.FrameNormal = false;
        option_cells = struct2paramcell(options);
        
        %% Compute other frames if necessary
        frames = compute_frames(subject.e1, subject.e2, subject.e3, subject.mask, option_cells{:});
        
        %% Set the normal manually
        frames.Fnorm = Fnorm;
        
        % Reset parameters
        options.FrameNormal = true;
        option_cells = struct2paramcell(options);
    else
        frames = compute_frames(subject.e1, subject.e2, subject.e3, subject.mask, option_cells{:});
    end
    
    subjects(subject_index).frames = frames;
    
    % Only preserve frame if passed as argument
    if (options.OptimizationFrameOnly)
        % For now only set volumes to empty matrix
        subjects(subject_index).e1 = [];
        subjects(subject_index).e2 = [];
        subjects(subject_index).e3 = [];
    end
end

pdisp(sprintf('Finished processing frames. Elapsed time = %.2fs', toc(tproc)), 'style', 'box')
if (options.PrecomputeDirectConnections && any(strcmp('frames',fieldnames(subjects))))
frameFields = fieldnames(subjects(1).frames);
    %% Compute connection forms
    pdisp('Computing connection forms (direct)', 'style', 'box');
    tproc = tic;
    for frame_index = 1:length(frameFields);
        fprintf('---> Processing connection forms on frame field [%s]\n', frameFields{frame_index});
        for subject_index = 1:length(subjects)
            subject = subjects(subject_index);
            fprintf('-> Subject %s...\n', subject.name);
            
            if (options.MaskedIndexing)
                subjects(subject_index).cijks.(frameFields{frame_index}) = cforms(subject.frames.(frameFields{frame_index}), 'MaskedVolume', subject.mask, cform_options{:});
            else
                subjects(subject_index).cijks.(frameFields{frame_index}) = cforms(subject.frames.(frameFields{frame_index}), cform_options{:});
            end
        end
    end
    pdisp(sprintf('Total connection form processing time = %.2fs', toc(tproc)), 'style', 'box');
end


% fprintf('--------------------------------------------------\n');
% fprintf('  Memory footprint for all subjects: %.2fMB\n', memfootprint(subjects));
% fprintf('  Total preprocessing time = %.2fs\n', toc(t_all));
% fprintf('--------------------------------------------------\n');
% 
