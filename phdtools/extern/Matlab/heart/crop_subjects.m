function subjects = crop_subjects(subjects, bounds, varargin)

for i=1:length(subjects)
    subjects(i) = crop_subject(subjects(i), bounds, varargin{:});
end
