%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compute frame field using the atlas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

useRat = true;

if (useRat)
    % rats
    atlasPath = '../../data/heart/rats/atlas/processed/';
    subjectParentPath = '../../data/heart/rats/';
    subjects = { 'rat07052008/', 'rat17122007/', 'rat21012008/', 'rat22012008/', 'rat24012008/', 'rat24022008_17/', 'rat27022008/', 'rat28012008/' };
    zrange = [20, 85];
    tagname = 'rat_';
    maskname = 'mask.mat';
    dataPath = 'registered/processed/';
    fitPath = 'helicoidFit/';
    scaling = { 0.25, 0.25, 0.25, 1 };

    %%%%%%% IMPORTANT %%%%%%%%
    % This value provides an upper bound on the number of transmural
    % steps that should be taken. Paths requiring longer than this numbe of
    % steps will not be considered
    %%%%%%% IMPORTANT %%%%%%%%
    % For rat use <20
    steprange = [3, 20];
else
    atlasPath = '../../data/heart/canine/average/cropped/processed/';
    subjectParentPath = '../../data/heart/canine/';
    subjects = { 'CH06/', 'CH09/', 'CH10/', 'CH11/', 'CH12/', 'CH13/', 'CH15/', 'CH16/' };
    dataPath = 'registered/cropped/processed/';
    fitPath = 'helicoidFit/';
    maskname = 'mask.mat';
    tagname = 'dog_';
    zrange = [];
    % For canine use <60 (about 40 myo samples)
    steprange = [8, 60];
    scaling = { 0.3125, 0.3125, 0.3125, 1 };
end


dirout = ['./output/'];
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

% Parameter volumes
vfiles = { 'kt_n3_n3.mat', 'kn_n3_n3.mat', 'kb_n3_n3.mat', 'error_n3_n3.mat' };
nvols = length(vfiles);

numSubjects = length(subjects);

% Clear data points
for vol=1:nvols
    histodata{vol}.data = [];
    histodata{vol}.index = 1;
end

% Compute epicardial envelope from the atlas
mask = cell2mat(struct2cell(load([atlasPath, maskname])));

% Set sizes
isize = size(mask);
sx = isize(1);
sy = isize(2);
sz = isize(3);

if (isempty(zrange))
    zrange = [1, size(mask,3)];
end
[envelope, dx, dy, dz] = compute_outer_sampling(atlasPath, zrange);
fprintf('Found transmural sampling for %i voxels\n', length(envelope));

% Convert envelope indices to subscripts
[I,J,K] = ind2sub(isize, envelope);

%% Compute sampling

for subject=1:numSubjects
    fprintf('--- Processing %s\n', subjects{subject});
    tic

    % Set paths
    subjectPath = [subjectParentPath, subjects{subject}, dataPath];
    parameterPath = [subjectParentPath, subjects{subject}, dataPath, fitPath];

    % Load parameter volumes
    for vol=1:nvols
        volumes{vol} = cell2mat(struct2cell(load([parameterPath, vfiles{vol}])));
    end
    
    % preallocate space if needed
    % (the total number of voxels in this volume times number of hearts should be more than enough)
    for vol=1:nvols
        if (histodata{vol}.index >= numel(histodata{vol}.data))
            histodata{vol}.data = [histodata{vol}.data; zeros(numSubjects * numel(mask),2)];
        end
    end
   
    fprintf('Tracing normals...\n');
    numpoints = numel(envelope);

    % Holds datapoints for each sampling
    for vol=1:nvols
        volvec = zeros(nvols, steprange(2));
    end

    for ipoint=1:numpoints
        
        if (mod(ipoint, round(numpoints / 10)) == 0)
            fprintf('%.2f%% completed\n', 100 * ipoint / numpoints);
        end
        
        % Get matrix index associated to this point
        i = envelope(ipoint);
        
        % Transmural direction
        nx = dx(i);
        ny = dy(i);
        nz = dz(i);

        % Continue if any component of the direction is NaN
        if (isnan(nx) || isnan(ny) || isnan(nz))
            continue;
        end

        % Follow direction until we are out of the mask
        posi0 = I(ipoint);
        posj0 = J(ipoint);
        posk0 = K(ipoint);
        n = [nx, ny, nz];

        % Fill initial datapoint
        for vol=1:nvols
            voldata = volumes{vol}(posi0, posj0, posk0);
            volvec(vol, 1) = voldata;
        end
        
        for step=2:steprange(2)
            vstep = step * n;

            % Compute position in volume
            % J = X, I = Y
            posi = round(posi0 + vstep(2));
            posj = round(posj0 + vstep(1));
            posk = round(posk0 + vstep(3));

            % Stop once we step outside of the myocardium
            if (posi > sx || posj > sy || posk > sz || posi < 1 || posj < 1 || posk < 1 || ~mask(posi, posj, posk))
                step = step - 1;
                break;
            end

            for vol=1:nvols
                voldata = volumes{vol}(posi, posj, posk);
                volvec(vol, step) = voldata;
            end

        end

        % Only use paths with length > 2
        if (step >= steprange(1))
%            fprintf('Found %i samples\n', numsteps);
            
            % Add points to myo percentage histogram
            myopercent = ((1:step) / step)';

            % Remove NaNs
        %    garbage = find(isnan(kbvec));
        %    myopercent(garbage) = [];
        %    kbvec(garbage) = [];

        %    histodata = [histodata; [myopercent, kbvec]];
        %     size(histodata)
        %     size(myopercent)
        %     size(volvec)
            for vol=1:nvols
                histodata{vol}.data(histodata{vol}.index:histodata{vol}.index + step - 1, :) = [myopercent, volvec(vol,1:step)'];
                histodata{vol}.index = histodata{vol}.index + step;
            end
        end

    end
toc
end


%% Plot values
% Remove empty data
for vol=1:nvols
    indexLimit = histodata{vol}.index;
    histodata{vol}.data = histodata{vol}.data(1:indexLimit,:);
end

if (useRat)
    volimits = { [-0.2, 0.2], [-0.2, 0.2], [-0.6, 0.3], [0, pi/6] };
    volres = { 0.01, 0.01, 0.01, 0.01 };
    % Myocardium sampling points
    myores = 0.05;
    myocut = 3;
else
    volimits = { [-0.2, 0.2], [-0.2, 0.2], [-0.2, 0.2], [0, pi/12] };
%    volres = { 0.005, 0.005, 0.005, 0.01 };
    volres = { 0.005, 0.005, 0.005, 0.005 };
    myores = 0.05;
    myocut = 3;
end

labels = { 'K_T (deg/mm)', 'K_N (deg/mm)', 'K_B (deg/mm)', 'error (deg)' };
outlabels = { 'kt', 'kn', 'kb', 'error' };

xpoints = (0.0:myores:1);

fontSize = 40;
axisSize = 40;

for volume=1:length(labels)
%for volume=4:4
%    subplot(2,2,volume);
    figure(volume)
    clf
    
    % Myo
    myoValues = histodata{volume}.data(:,1);

    % Parameter values
    yvalues = histodata{volume}.data(:,2);

    % Remove points that lie outside of the histogram limit
    garbageMask = yvalues >= volimits{volume}(1) & yvalues <= volimits{volume}(2);
    
    % Apply garbage mask
    myoValues = myoValues(garbageMask);
    yvalues = yvalues(garbageMask);

    % Histogram bins
    volumePoints = (volimits{volume}(1):volres{volume}:volimits{volume}(2));

    dat = [myoValues,yvalues];
    [bincounts, bincenters] = hist3(dat, {xpoints, volumePoints});
    
    % Apply scaling
    if (volume ~= 4)
        bincenters{2} = rad2deg(bincenters{2}) / scaling{volume};
        volumePoints = rad2deg((volimits{volume}(1):volres{volume}:volimits{volume}(2))) / scaling{volume};
    else
        bincenters{2} = rad2deg(bincenters{2});
        volumePoints = rad2deg((volimits{volume}(1):volres{volume}:volimits{volume}(2)));
    end

    % Threshold borders
    bincounts(1:myocut, :) = 0;
    bincounts(end-myocut:end, :) = 0;
    
    % number of transmural samples
    pcount = size(bincounts,1);

    % Normalize counts
    for myosample=1:pcount;
        bincounts(myosample, :) = bincounts(myosample, :) / trapz(bincenters{2}, bincounts(myosample, :));
    end

%     [X,Y] = meshgrid(xpoints, volumePoints);
%     surf(X,Y, bincounts');
%    hist3([myoValues,yvalues], {xpoints, volumePoints});
    ph = pcolor(xpoints, volumePoints, bincounts');
    shading flat;
    set(ph,'edgecolor','none')
    set(ph, 'EdgeColor','none')
    
    % Plot ridge line
    xhandle = xlabel('myocardial percentage');
    yhandle = ylabel(labels{volume});
    set(xhandle,'FontSize', axisSize);
    set(yhandle,'FontSize', axisSize);

%    view(2)
%    shading flat
%    colorbar

    % Compute mode along each myo sample
    hold on
    
    % Upper limit on z-value
    pz = max(bincounts(:));
    
    % Plot the mean, max and mode of each bin
    maxPoints = zeros(pcount,1);
    for myosample=myocut:pcount;
        % Max
        [maxv, maxi] = max(bincounts(myosample, :));
%        plot3(bincenters{1}(myosample), bincenters{2}(maxi), pz, 'bo', 'MarkerSize',10);
        maxPoints(myosample) = maxi;
        % Mode
%         [maxv, maxi] = mode(bincounts(myosample, :));
%         plot3(bincenters{1}(myosample), bincenters{2}(maxi), pz, 'bo', 'MarkerSize',10);
    end
    
    % Repeat for borders
    maxPoints(1:myocut) = maxPoints(myocut+1);
    maxPoints(end-myocut:end) = maxPoints(end-myocut - 1);
%     
%     % This is the max of each bin
%     maxindices = zeros(pcount, 1);
%     for xv=1:pcount
%         % Extract data along this sample
%        [m, j] = max(bincounts(xv,:));
%        maxindices(xv) = j;
%        plot3(xpoints(xv), volumePoints(j), pz, 'bo', 'MarkerSize',10);
%     end
%     plot3(xpoints, volumePoints(maxindices(:)), repmat(pz, [pcount, 1]), 'b-', 'LineWidth',3);
%     
%     plot3(bincenters{1}(1:pcount), bincenters{2}(maxPoints(:)), repmat(pz, [pcount, 1]), 'b-', 'LineWidth',3);
%     plot(bincenters{1}(1:pcount), bincenters{2}(maxPoints(:)), 'b-', 'LineWidth',3);
    plot(bincenters{1}(2:pcount-2) + myores / 2, bincenters{2}(maxPoints(2:end-2)), 'b-', 'LineWidth',3);
    hold off
    grid off
    
    xlim([myocut * myores 1-myocut*myores])
    
    %ylim(volimits{volume});
    ylim(rad2deg(volimits{volume}) / scaling{volume});
    
    % Make sure we have only three labels on the x axis
    axlim = xlim;
    set(gca,'XTick',axlim(1):(axlim(2)-axlim(1))/2:axlim(2));
    set(gca, 'XTickLabel', { 'epi', 'mid', 'endo' } );
    xlabelh = xlabel('myocardial depth');
    
%    set(gcf,'units','normalized','outerposition',[0 0 0.8 0.8]);
%    set(gcf,'Position',get(0,'Screensize'))
    set(gca,'FontSize', fontSize);
    set(xlabelh,'FontSize', fontSize);

    cmap = colormap(hot);
    colormap(cmap(end:-1:1,:));
%    caxis(caxisl);
    
    colorbar off
    hidden off
    
    export_fig([dirout, 'transmural_', tagname, outlabels{volume}, '.pdf'], '-transparent', '-q101');
end
