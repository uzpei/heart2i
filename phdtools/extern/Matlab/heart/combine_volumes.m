function combined = combine_volumes(subjects, volume_name)

probe_voxels = getnestedfield(subjects(1), volume_name);
vectorized = isvector(squeeze(probe_voxels));

subject_count = length(subjects);

% Find the smallest volume in the population
% Check if we are dealing with a full volume or only voxels
if (vectorized)
    n = find_smallest_volume(subjects, volume_name);
else 
    n = find_smallest_volume(subjects);
end

% Combine volumes 
combined = zeros(1, n * length(subjects));

for index = 1:subject_count
    subject = subjects(index);

    % First load all voxels
    all_voxels = getnestedfield(subject, volume_name);
    
    % Apply mask if necessary
    if (~vectorized)
        all_voxels = all_voxels(subject.mask);
    end
    
    % Randomly and uniformly sample using the smallest volume
    %samples = randi(numel(all_voxels), 1, n);
    samples = randsample(1:numel(all_voxels), n);

    combined(((index-1) * n + 1):((index) * n)) = all_voxels(samples);
end
