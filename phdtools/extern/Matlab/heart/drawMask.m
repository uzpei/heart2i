
dir = '../../data/heart/rats/rat07052008/';
dirout = [dir, 'processed/'];

% Load and swap XY
datax = cell2mat(struct2cell(load([dir, 'e1x.mat'])));
datay = cell2mat(struct2cell(load([dir, 'e1y.mat'])));

swapXY = false;

if (swapXY)
    e1x = datay;
    e1y = datax;
else
    e1x = datax;
    e1y = datay;
end

e1z = cell2mat(struct2cell(load([dir, 'e1z.mat'])));

mask = cell2mat(struct2cell(load([dir, 'mask.mat']))) > 0;

sx = size(mask,1);
sy = size(mask,2);
sz = size(mask,3);

%%%% Cut smaller volume for debugging
sz = sz - 2 * border;
e1x = e1x(1:end,1:end, border+1:end-border);
e1y = e1y(1:end,1:end, border+1:end-border);
e1z = e1z(1:end,1:end, border+1:end-border);
mask = mask(1:end,1:end, border+1:end-border);


for z=1:sz
   imagesc(mask(:,:,z));
   
end
