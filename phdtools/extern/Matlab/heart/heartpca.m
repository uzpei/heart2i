 % PCA of curvature parameters
clear all

useRats = true;

if useRats
    dirdata = '../../data/heart/rats/rathearts.mat';
%     zbounds = [20, 85];
    zbounds = [1, 128];
else
    dirdata = '../../data/heart/canine/doghearts.mat';
    zbounds = [1, 300];
end

hearts = loadmat(dirdata);
subjects = fieldnames(hearts);

% Load hearts
atlas = hearts.atlas;

% Set dimensions
mask = atlas.mask;
[sx, sy, sz] = size(mask);

% Tighten mask
mask(:,:,[1:zbounds(1), zbounds(2):end]) = 0;

% 4D mask
mask3 = repmat(mask, [1 1 1 3]);
n = numel(find(mask));

% This matrix will store reconstruction errors
% for all leave-one-out subjects
allErrors = [];

%% Compute 2nd-order statistics
for i=1:length(subjects)-1
    heart = hearts.(char(subjects(i)));
    hlbl = char(subjects(i));

    % Apply mask
    hearts.(hlbl).T = heart.T .* mask3;
    hearts.(hlbl).N = heart.N .* mask3;
    hearts.(hlbl).B = heart.B .* mask3;

    % Compute Jacobian
    DT = jacobian(heart.T);

    % Compute stats
%     hearts.(hlbl).TNT = cijv(DT, heart.N, heart.T);
%     hearts.(hlbl).TNN = cijv(DT, heart.N, heart.N);
    hearts.(hlbl).TNB = cijv(DT, heart.N, heart.B);
    
%     hearts.(hlbl).TBT = cijv(DT, heart.B, heart.T);
%     hearts.(hlbl).TBN = cijv(DT, heart.B, heart.N);
%     hearts.(hlbl).TBB = cijv(DT, heart.B, heart.B);
end


%% Heart we are leaving out
for leaveOut=1:length(subjects)-1
    %leaveOut = 2;
    fprintf('Current subject = %s\n', subjects{leaveOut});
    dirout = ['./output/', subjects{leaveOut}, '/'];
    if ~exist(fullfile('.',dirout),'dir')
        mkdir(dirout)
    end

    % List hearts we are going to use
    heartIndices = 1:(length(subjects)-1);
    heartIndices(leaveOut) = [];
    m = length(heartIndices);

    fprintf('PCA training from: \n');
    subjects(heartIndices)

    %% Compute training data matrix X
    X = zeros(n, m);
    for hi=1:m
        i = heartIndices(hi);
        heart = hearts.(char(subjects(i)));

        data = heart.TNB;
        data = data(mask);
        X(:,hi) = data(:);
    end

    %% Output eigenmodes
    [eigenmatrix, evalues, Xmean] = eigendecompose(X);
    mn = size(eigenmatrix,2);

    figure(1)
    slices = round([sx/2, sy/2, sz/2]);
    %eigenmatrix = zeros(n, mn);
    
    % Xmean3d = reshape(Xmean, [sx sy sz]);
    evector = zeros(sx, sy, sz);
    for i=1:mn
        % Reconstruct eigenvector
         evector(mask) = evalues(i) * eigenmatrix(:,i);
%        evector(mask) = eigenmatrix(:,i);

        image3d(evector);
%        image3d(evector, slices, [-0.5, 0.5]);
        supertitle(['Principal Component ', num2str(i)]);
        colormap(customap('redwhiteblue'))
        export_fig([dirout, 'TNBmode_', num2str(i), '.pdf'], '-transparent', '-q101');
    end

    %
    colormap(customap('redwhiteblue'))
    Xmean3 = zeros(sx, sy, sz);
    Xmean3(mask) = Xmean(:);
    image3d(Xmean3, slices, [-0.5, 0.5]);
    supertitle('Principal Component \mu');
    export_fig([dirout, 'TNBmean.pdf'], '-transparent', '-q101');

    % Plot eigenvalue magnitude
    clf
    hold on

    plot(evalues ./ sum(evalues(:)), '-bo');

    % Plot cumulative energy
    plot(cumsum(evalues)./ sum(evalues(:)), '-ro');
    hold off

    legend({'magnitude', 'cumulative energy' });
    title('eigenvalue spectrum');
    xlabel('eigenvalue');
    ylabel('magnitude');
    export_fig([dirout, 'eigenvalues.pdf'], '-transparent', '-q101');

    %% Compute sampling
    % Pick heart that was left out
     heart = hearts.(char(subjects(leaveOut)));
%    heart = hearts.(char(subjects(2)));
    heart.T = heart.T .* mask3;
    heart.N = heart.N .* mask3;
    heart.B = heart.B .* mask3;
    DT = jacobian(heart.T);
    dataOriginal = cijv(DT, heart.N, heart.B);

    % Use a slice every
    %zerror = zeros(2, sz);
    zerror = [];

    zsnap = 5;

    dataMean = zeros(sx, sy, sz);
    dataMean(mask) = Xmean(:);
    nu = 0;
    
    % Only sample within volume
    for zcut=zbounds(1):zbounds(2)
        % Compute slice indices (+2 is for beginning, end)
        zslices = floor(linspace(zbounds(1), zbounds(2), zcut - zbounds(1) + 1 + 2));
        zslices = zslices(2:end-1);
        zslices(zslices>sz) = [];

        % Perform reconstruction
        data = dataOriginal(mask);

        % Compute the partial volume mask
        % Could make this faster... need to index directly
        partialMask = zeros(size(dataOriginal));
        partialMask(:,:,zslices(:)) = 1;
        partialMaskIndices = partialMask(mask);

    %    smask = find(smask);

        % Compute reconstruction and difference
        % Problem: need to fix indices within find(smasks)
        % i.e. we are adding another thresholding
        [xp, W] = pcaproject(X, Xmean, data(:), eigenmatrix, evalues, find(partialMaskIndices), nu);

        % Reconstruct projection
        dataRecon = zeros(sx, sy, sz);
        dataRecon(mask) = xp(:);

    %    data = reshape(data, [sx sy sz]);
        data = dataOriginal;
        data(~partialMask) = 0;

        dataDiff = dataRecon - dataOriginal;

        % Compute the masked norm
    %      dnorm = norm(dataDiff(mask), 2) / numel(find(mask));
    %    dnorm = norm(dataDiff(:), 2)
        dnorm = norm(dataDiff(:), 2);

        if (mod(zcut-zbounds(1), zsnap) == 0)
            % Plot: original data, sliced data, reconstructed data, error diff
            figure(1)
            clf
            customSubplot(dataOriginal, data, dataRecon, dataMean, dataDiff);
            text(5,5,['e = ', num2str(dnorm)])
            export_fig([dirout, subjects{leaveOut}, '_recon_', num2str(numel(zslices)), '_nu_', num2str(nu), '.pdf'], '-transparent', '-q101');
            pause(0.1);
        end

        % Store error
        zerror = [zerror, dnorm];

        zcut
    end

    allErrors = [allErrors; zerror];

    %% Plot error
    zerror
    figure(1)
    clf
    hold on
    % Plot error
    plot(1:numel(zerror), zerror, 'r');

    % Plot norm2 of original image
    onorm = norm(dataOriginal(mask), 2);
    line([0 numel(zerror)], [onorm onorm])

    xlabel('Axial reconstruction slices');
    ylabel('error (L^2 norm)');
    legend({'error (L^2 norm)', 'original (L^2 norm)'})
    xlim([0 numel(zerror)])
    export_fig([dirout, subjects{leaveOut}, '_recon_errorplot', '.pdf'], '-transparent', '-q101');
    hold off
end
