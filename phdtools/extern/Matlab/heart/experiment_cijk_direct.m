%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This experiment explores the direct computations of the cijk connection
% forms.
% Different models of the cijk are used for extrapolating neighborhoods and
% compared together on error histograms.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

setpath;

searchPath = '../../data/heart/rat/sample1/';
experiment_options.ElementOrder = [1 2 3];
experiment_options.PostPath = 'registered';
experiment_options.MaskedIndexing = true;
experiment_options.SinglePrecision = false;
experiment_options.OptimizationFrameOnly = false;
experiment_options.FrameNormal = true;
experiment_options.FrameCurl = false;
experiment_options.FrameEig = false;
experiment_options.PrecomputeDirectConnections = true;
experiment_options.Bounds = [1, 1, 0.7];
%experiment_options = struct2paramcell(struct('ElementOrder', [1 2 3], 'PostPath', 'registered', 'MaskedIndexing', true, 'SinglePrecision', true, 'OptimizationFrameOnly', false, 'FrameNormal', true, 'FrameCurl', false, 'FrameEig', false));

% Load all subjects, frames, and connection forms
experiment_options = struct2paramcell(experiment_options);
subjects = compute_frames_con_all(searchPath, experiment_options{:});
frameFields = fieldnames(subjects(1).frames);

%%% Apply cropping in percentage. 0.7 is a good value for thresholding the
%%% base and valves. NOTE: always validate by checking all masks.
image3d(subjects(1).mask)

%%
subject = subjects(1);
cformplot(framefield(double(subject.frames.Fnorm.T), [], double(subject.frames.Fnorm.B)), subject.mask);

%% Compute connection form extrapolation for all frames and all subjects
models = { 'all', 'homeoid', 'ghmform', 'constant' }; 
%models = { 'all', 'constant' }; 
cerror = {};
for subject_index = 1:length(subjects)
    subject = subjects(subject_index);
    
    fprintf('===> Computing extrapolation error for [%s]\n', subject.name);
    for frame_index=1:length(frameFields)
        fprintf('==> Using frame [%s]\n', frameFields{frame_index});
        for model_index = 1:length(models)
            cerror{model_index} = compute_connection_extrapolation_error(subject.frames.(frameFields{frame_index}), subject.mask, subject.cijks.Fnorm, models{model_index});
            subjects(subject_index).interpolation_error.(frameFields{frame_index}).(models{model_index}) = cerror{model_index};
%             mean(real(cerror(subject.mask)))
        end
    end
end
%

% 4 models: all, homeoid, ghmform, constant
% stats: error, model / m1, model / m2, model / m3, model / m4
% ---> 4 x 5
%%
for sl=1:3
figure(sl)
if (sl == 1)
    sl = 'x';
elseif (sl == 2)
    sl = 'y';
elseif (sl == 3)
    sl = 'z';
end

lmn = length(models);
for i=1:lmn
    subplot(lmn, lmn + 1, (i-1) * (lmn+1) + 1);
    image3dslice(cerror{i}, sl);
    title('error');
    ylabel(models{i});
    axis image
    set(gca,'xtick',[])
    set(gca,'ytick',[])

    for j=1:lmn
        cvolj = cerror{j};
        
        subplot(4, 5, (i-1) * (lmn+1) + j + 1);
%         image3dslice(cvoli ./ cvolj, 'z');
        image3dslice(sign(cerror{i} - cerror{j}), sl);
        title([models{i}, ' - ', models{j}]);
        axis off
        axis image
        colormap jet
    end
end
supercolorbar
end

% for divider_index=1:length(models)
%     figure(divider_index)
% for i=1:length(models)
%     cvol = cerror{i};
%     if (i ~= divider_index)
% %         cvol = cerror{mi} - cvol;
% %         cvol = abs(cerror{mi} - cvol) ./ cerror{mi};
%         cvol = cvol ./ cerror{divider_index};
%         tstr = [ models{i}, ' / ', models{divider_index}];
%     else
%         tstr = models{divider_index};
%     end
%     
%    subplot(2,2,i);
% %    image3dslice(rad2deg(real(cvol)), 'z');
%    image3dslice(cvol, 'z');
%    title(tstr);
% %         colormap(customap('redwhiteblue'));
%         colormap(customap('redblackblue'));
%    axis image
% %   caxis([0 rad2deg(max(real(cerror{4}(:))))]);
%     if (i ~= divider_index)
%         caxis([0, 2]);
%     else
%         caxis([0, 0.5]);
%     end
%    colorbar
% end
% supertitle([models{divider_index}, ' / models']);
% end

%%
% Combine results from all subjects
% TODO: also loop over frames (will need to update extrapolation code
% above as well)
% Find the smallest volume in the population
fprintf('Combining volumes...\n');
voxels_per_volume = find_smallest_volume(subjects);
for frame_index = 1:length(frameFields)
    for model_index = 1:length(models)
        slbl = ['interpolation_error.', frameFields{frame_index}, '.', models{model_index}];
        subjects_combined.interpolation_error.(frameFields{frame_index}).(models{model_index}) = combine_volumes(subjects, slbl);
    end
end

% Plot all subjects combined, comparing connection form models
nbins = 100;
stdf = 0;
for frame_index = 1:length(frameFields)
    figure(frame_index)
    clf
    
    hold on
    for model_index = 1:length(models)
        hdata = subjects_combined.interpolation_error.(frameFields{frame_index}).(models{model_index});
        histnorm_thresholded(rad2deg(hdata), nbins, stdf, 'LineWidth',2, 'Color', colorset(model_index));
        
    end
    title(sprintf('Frame field: %s', frameFields{frame_index}));
    xlabel('error (deg)')
    legend(models);
    
    export_fig('.', [frameFields{frame_index}, '_models_histo.pdf'], '-transparent', '-q101', '-nocrop');
end

%% Plot all subjects combined, comparing frame models for each connection model
nbins = 200;
stdf = 2;
clf
hold on
ltitles = {};

find = 1;
for frame_index = 1:length(frameFields)
    for model_index = 1:length(models)
        hdata = subjects_combined.interpolation_error.(frameFields{frame_index}).(models{model_index});
        histnorm_thresholded(rad2deg(hdata), nbins, stdf, 'LineWidth',2, 'Color', colorset(find));
        ltitles = { ltitles{:}, [frameFields{frame_index}, '-', models{model_index}] };
        find = find + 1;
    end
end
xlabel('error (deg)')
legend(ltitles);
export_fig('.', ['all_models_frames_histo.pdf'], '-transparent', '-q101', '-nocrop');


