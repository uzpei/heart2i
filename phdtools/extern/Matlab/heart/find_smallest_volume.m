function voxels_per_volume = find_smallest_volume(subjects, varargin)

if (nargin == 1)
    %% Find the smallest (masked) volume in the population
    voxels_per_volume = inf;
    for subject_index = 1:length(subjects)
        subjects(subject_index).volume_count = numel(find(subjects(subject_index).mask > 0));
        if (subjects(subject_index).volume_count < voxels_per_volume)
            voxels_per_volume = subjects(subject_index).volume_count;
        end
    end
else
    % Treat as a vectorized volume.
    volume_name = varargin{1};
    voxels_per_volume = inf;
    for subject_index = 1:length(subjects)
        subject = subjects(subject_index);
        all_voxels = getnestedfield(subject, volume_name);
        
        % Remove nans and vectorize
        all_voxels = all_voxels(~isnan(all_voxels));
        
        % Count number of clean voxels
        volume_count = numel(all_voxels);
        
        % Update volume count
        if (volume_count < voxels_per_volume)
            voxels_per_volume = volume_count;
        end
    end
end

