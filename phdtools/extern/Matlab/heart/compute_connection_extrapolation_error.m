function Terror = compute_connection_extrapolation_error(F, mask, cijks, model)

%% Parse the connection model
% Reconstitute vectorized volumes if needed
if (strcmpi(model, 'all'))
    % Use all connection forms
    TNT = validate_dims(cijks.TNT, mask);
    TNN = validate_dims(cijks.TNN, mask);
    TNB = validate_dims(cijks.TNB, mask);
    TBT = validate_dims(cijks.TBT, mask);
    TBN = validate_dims(cijks.TBN, mask);
    TBB = validate_dims(cijks.TBB, mask);
elseif (strcmpi(model, 'homeoid'))
    % Remove out-of-shell variation
    TNT = validate_dims(cijks.TNT, mask);
    TNN = validate_dims(cijks.TNN, mask);
    TNB = validate_dims(cijks.TNB, mask);
    TBT = validate_dims(cijks.TBT, mask);
    TBN = 0;
    TBB = validate_dims(cijks.TBB, mask);
elseif (strcmpi(model, 'ghmform'))
    % Remove out of plane variation
    TNT = validate_dims(cijks.TNT, mask);
    TNN = validate_dims(cijks.TNN, mask);
    TNB = validate_dims(cijks.TNB, mask);
    TBT = 0;
    TBN = 0;
    TBB = 0;
elseif (strcmpi(model, 'constant'))
    % Remove all variation
    TNT = 0;
    TNN = 0;
    TNB = 0;
    
    TBT = 0;
    TBN = 0;
    TBB = 0;
end

[sx, sy, sz] = size(mask);

if (length(fieldnames(F)) == 1)
    T = F(:,:,:,1:3);
    N = F(:,:,:,4:6);
    B = F(:,:,:,7:9);
else
    T = F.T;
    N = cross(F.B, F.T, 4);
    B = F.B;
end

T1 = T(:,:,:,1);
T2 = T(:,:,:,2);
T3 = T(:,:,:,3);

N1 = N(:,:,:,1);
N2 = N(:,:,:,2);
N3 = N(:,:,:,3);

B1 = B(:,:,:,1);
B2 = B(:,:,:,2);
B3 = B(:,:,:,3);

%% Iterate over neighbor offsets: [-1, 0, 1] in 3-dimensions
n = 1;
num_neighbors = (2 * n + 1)^3;

%% Create volumes
fprintf('Computing extrapolation error using [%s] model\n', model);

tall = tic;

%%
neighbors = zeros(3, num_neighbors);
neighbor_index = 1;
for i = -n:n
    for j = -n:n
        for k = -n:n
            % Need to slice volume for parfor indexing
            neighborsi(neighbor_index) = i;
            neighborsj(neighbor_index) = j;
            neighborsk(neighbor_index) = k;
            
            neighbor_index = neighbor_index + 1;
        end
    end
end

Terror = zeros(sx, sy, sz, num_neighbors);

% CoreNum = feature('numCores');
% if(matlabpool('size'))<=0
%     fprintf('Enabling parallel computing with %i threads.\n', CoreNum);
%     matlabpool('open','local',CoreNum);
% else
%     fprintf('Parallel Computing enabled with %i threads.\n', matlabpool('size'));
% end

onesv = ones(sx, sy, sz);
for par_index = 1:num_neighbors
%     onesv = ones(sx, sy, sz);
    i = neighborsi(par_index);
    j = neighborsj(par_index);
    k = neighborsk(par_index);

%         if (strcmpi(model, 'constant'))
%             Tout = T;
%         else
            % Compute h . T, h . N, h . B
            % Compute TN<h>N =  TNT h . T + TNN h . N + TNB h . B
            % Compute TB<h>B =  TBT h . T + TBN h . N + TBB h . B
%             hT = (j * onesv) .* T1 + (i * onesv) .* T2 + (k * onesv) .* T3;
%             hN = (j * onesv) .* N1 + (i * onesv) .* N2 + (k * onesv) .* N3;
%             hB = (j * onesv) .* B1 + (i * onesv) .* B2 + (k * onesv) .* B3;
%             sN = cijks.TNT .* hT + cijks.TNN .* hN + cijks.TNB .* hB;
%             sB = cijks.TBT .* hT + cijks.TBN .* hN + cijks.TBB .* hB;
%                 sN = cijks.TNT .* ((j * onesv) .* T1 + (i * onesv) .* T2 + (k * onesv) .* T3) + cijks.TNN .* ((j * onesv) .* N1 + (i * onesv) .* N2 + (k * onesv)) .* N3 + cijks.TNB .* ((j * onesv) .* B1 + (i * onesv) .* B2 + (k * onesv) .* B3);
%                 sB = cijks.TBT .* ((j * onesv) .* T1 + (i * onesv) .* T2 + (k * onesv) .* T3) + cijks.TBN .* ((j * onesv) .* N1 + (i * onesv) .* N2 + (k * onesv)) .* N3 + cijks.TBB .* ((j * onesv) .* B1 + (i * onesv) .* B2 + (k * onesv) .* B3);
            sN = TNT .* ((i * onesv) .* T1 + (j * onesv) .* T2 + (k * onesv) .* T3) + TNN .* ((i * onesv) .* N1 + (j * onesv) .* N2 + (k * onesv)) .* N3 + TNB .* ((i * onesv) .* B1 + (j * onesv) .* B2 + (k * onesv) .* B3);
            sB = TBT .* ((i * onesv) .* T1 + (j * onesv) .* T2 + (k * onesv) .* T3) + TBN .* ((i * onesv) .* N1 + (j * onesv) .* N2 + (k * onesv)) .* N3 + TBB .* ((i * onesv) .* B1 + (j * onesv) .* B2 + (k * onesv) .* B3);

%                 hx = j * onesv;
%                 hy = i * onesv;
%                 hz = k * onesv;
%                 sN = cijks.TNT .* (hx .* T1 + hy .* T2 + (k * onesv) .* T3) + cijks.TNN .* (hx .* N1 + hy .* N2 + hz) .* N3 + cijks.TNB .* (hx .* B1 + hy .* B2 + hz .* B3);
%                 sB = cijks.TBT .* (hx .* T1 + hy .* T2 + (k * onesv) .* T3) + cijks.TBN .* (hx .* N1 + hy .* N2 + hz) .* N3 + cijks.TBB .* (hx .* B1 + hy .* B2 + hz .* B3);

            %% Compute the extrapolated tangent
            % Use offset to compare difference to current neighbor
            % Accumulate prediction (contributions might not all sum to
            % same values do to boundary effects)
            Tout(:,:,:,1) = T1 + sN .* N1 + sB .* B1;
            Tout(:,:,:,2) = T2 + sN .* N2 + sB .* B2;
            Tout(:,:,:,3) = T3 + sN .* N3 + sB .* B3;
            Tout = normalize(Tout);
%         end

    %     Terror(:,:,:,offset_it) = acos(abs(dot(circshift(T, -[j, i, k, 0]), Tout, 4)));
%             Terror = Terror + acos(abs(dot(circshift(T, -[i, j, k, 0]), Tout, 4)));
        cshift = -[i, j, k];
%            Tv = circshift(T, cshift);
        Nv = circshift(N, cshift);
        Bv = circshift(B, cshift);
%         Terror = Terror + abs(dot(Tout, Nv, 4)) + abs(dot(Tout, Bv, 4));
        Terror(:,:,:,par_index) = abs(dot(Tout, Nv, 4)) + abs(dot(Tout, Bv, 4));
end

%% Sum error
Terror = sum(Terror, 4) / num_neighbors;
%Terror = Terror / num_neighbors;
Terror(~mask) = 0;

toc(tall)

end

%% Reconstruct volumes if needed
function cijk = validate_dims(cijk, mask)
    if (ndims(cijk) <= 2)
        cijk = reconstitute(cijk, mask);
    end
end

