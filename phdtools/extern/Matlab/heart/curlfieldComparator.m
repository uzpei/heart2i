%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Compare curl friend across different subjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

useRat = true;

if (useRat)
    % rats
    subjectParentPath = '../../data/heart/rats/';
    subjects = { 'rat07052008', 'rat17122007', 'rat21012008', 'rat22012008', 'rat24012008', 'rat24022008_17', 'rat27022008', 'rat28012008' };
    zrange = [20, 85];
    tagname = 'rat_';
    maskname = 'mask.mat';
    dataPath = '/registered/processed/';
    scaling = { 0.25, 0.25, 0.25, 1 };

    %%%%%%% IMPORTANT %%%%%%%%
    % This value provides an upper bound on the number of transmural
    % steps that should be taken. Paths requiring longer than this numbe of
    % steps will not be considered
    %%%%%%% IMPORTANT %%%%%%%%
    % For rat use <20
    maxsteps = 20;
    smoothingParameters = {0.3, 1};
else
    subjectParentPath = '../../data/heart/canine/';
    subjects = { 'CH06', 'CH09', 'CH10', 'CH11', 'CH12', 'CH13', 'CH15', 'CH16' };
    dataPath = '/registered/cropped/processed/';
    maskname = 'mask.mat';
    tagname = 'dog_';
    zrange = [];
    % For canine use <60 (about 40 myo samples)
    maxsteps = 60;
    scaling = { 0.3125, 0.3125, 0.3125, 1 };
    
    smoothingParameters = {3, 1};
end


dirout = ['./output/curl/', num2str(smoothingParameters{1}), 'x', num2str(smoothingParameters{2}), '/'];
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end
numSubjects = length(subjects);

% Process each heart
for subject=1:length(subjects)
   path = [subjectParentPath, subjects{subject}, dataPath];
   
    [T, N, B, mask, C] = curlfield(path, smoothingParameters);
    
    cnorm = sqrt(C(:,:,:,1).^2 + C(:,:,:,2).^2 + C(:,:,:,3).^2);
    image3d(cnorm);
    pause(0.1);
    export_fig([dirout, 'Cmag_', subjects{subject},  '.pdf'], '-transparent', '-q101');

    image3d(T);
    export_fig([dirout, 'T', subjects{subject},  '.pdf'], '-transparent', '-q101');
    image3d(N);
    export_fig([dirout, 'N', subjects{subject},  '.pdf'], '-transparent', '-q101');
    image3d(B);
    export_fig([dirout, 'B_', subjects{subject},  '.pdf'], '-transparent', '-q101');
end

