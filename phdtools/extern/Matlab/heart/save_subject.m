% Save a subject to its path.
function save_subject(subject)
    outpath = subject.path;
    
    disp(['Saving to ', outpath]);
    
    save_mkdir([outpath, 'e1x.mat'], subject.e1(:,:,:,1));
    save_mkdir([outpath, 'e1y.mat'], subject.e1(:,:,:,2));
    save_mkdir([outpath, 'e1z.mat'], subject.e1(:,:,:,3));
    save_mkdir([outpath, 'mask.mat'], subject.mask);
%     save_mkdir([outpath, 'dt_endo.mat'], subject.dt_endo);
%     save_mkdir([outpath, 'dt_epi.mat'], subject.dt_epi);
    
end