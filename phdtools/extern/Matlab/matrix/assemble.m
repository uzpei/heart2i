function T = assemble(Tx, Ty, Tz)
    [sx, sy, sz] = size(Tx);
    T = zeros(sx, sy, sz, 3);
    T(:,:,:,1) = Tx;
    T(:,:,:,2) = Ty;
    T(:,:,:,3) = Tz;
