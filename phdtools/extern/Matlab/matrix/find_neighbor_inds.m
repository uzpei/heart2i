% To get the linear neighbors of an index of vol, 
% add the linear index to the result from this function:
% lin_index = sub2ind(size(vol), 10,10,10);
% new_neigh = bsxfun(@plus,inds, lin_index);
% r = radius (=1 for 3x3x3 neighborhood, =2 for 5x5x5, etc.
% varargin: if a linear index is given, this function returns
% relative neighboring indices
function inds = find_neighbor_inds(vol, r, varargin)

%r = 1;
ns = 2*r+1;

[M,N,D] = size(vol);
rs = (1:ns)-(r+1);
% 2d generic coordinates:
neigh2d = bsxfun(@plus, M*rs,rs');

% 3d generic coordinates:
pages = (M*N)*rs;
pages = reshape(pages,1,1,length(pages));

inds = bsxfun(@plus,neigh2d,pages);

if (nargin > 2)
    inds = bsxfun(@plus,inds, varargin{1});
end

% 
% % Show slice
% volimg = zeros(size(vol));
% volimg(new_neigh) = 2;
% volimg(lin_index) = 1;
% image3d(volimg)

