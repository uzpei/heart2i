%% Test rotation matrix to euler angle to matrix conversion

A = rand(3,3); 
[M,R]=qr(A);
M

Mangles = SpinCalc('DCMtoEA313', M, eps, 0)
phi = deg2rad(Mangles(1));
theta = deg2rad(Mangles(2));
psi = deg2rad(Mangles(3));

Q = SpinCalc('EA313toDCM', Mangles, eps, 0);

%[phi, theta, psi] = anglem3d(M);

%[phi, theta, psi]

% Convert back
%Q = matangle3d(phi, theta, psi);
%Q = matangle3d(psi, theta, phi);
%Q = reshape(Q, [3 3])
fprintf('L^2 difference is %e\n', norm(Q-M,2));

%Q(2,:) = -Q(2,:);
%v = rand(3,1)
%M * v
%Q * v