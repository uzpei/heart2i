function eigendecomp(path)
%dxx = load(sprintf('%sdxx.txt','-ascii', path));
%path = '.'

disp('Loading data...');
dxx = load([path 'dxx.txt']);
dyy = load([path 'dyy.txt']);
dzz = load([path 'dzz.txt']);
dxy = load([path 'dxy.txt']);
dxz = load([path 'dxz.txt']);
dyz = load([path 'dyz.txt']);

disp('Filling tensor matrices...')
tensors(1,1,:) = dxx; 
tensors(1,2,:) = dxy; 
tensors(1,3,:) = dxz;
tensors(2,1,:) = dxy; 
tensors(2,2,:) = dyy; 
tensors(2,3,:) = dyz;
tensors(3,1,:) = dxz; 
tensors(3,2,:) = dyz; 
tensors(3,3,:) = dzz;

disp('Computing eigendecomposition...')

for i = 1:length(dxx)
  if(~((dxx(i) == 0.0) & (dxy(i) == 0.0) & (dxz(i) == 0.0) &  (dyy(i) == 0.0) & (dyz(i) == 0.0) & (dzz(i) == 0.0)))

    %tensor = [dxx(i) dxy(i) dxz(i);
    %          dxy(i) dyy(i) dyz(i);
    %          dxz(i) dyz(i) dzz(i)];

    [v,d] = eigs(tensors(:,:,i));
    
%    if(~((d(1,1) > 0) & (d(2,2) > 0) &  (d(3,3) > 0)))
%      fprintf(1, '%d: ',i);                                      
%      fprintf(1,'%f %f %f\n',d);
%    end
              
    lambda1(i) = d(1,1);
    lambda2(i) = d(2,2);
    lambda3(i) = d(3,3);
  
    e1x(i) = v(1,1);
    e1y(i) = v(2,1);
    e1z(i) = v(3,1);
    e2x(i) = v(1,2);
    e2y(i) = v(2,2);
    e2z(i) = v(3,2);
    e3x(i) = v(1,3);
    e3y(i) = v(2,3);
    e3z(i) = v(3,3);
  else
    lambda1(i) = 0;
    lambda2(i) = 0;
    lambda3(i) = 0;
  
    e1x(i) = 0;
    e1y(i) = 0;
    e1z(i) = 0;
    e2x(i) = 0;
    e2y(i) = 0;
    e2z(i) = 0;
    e3x(i) = 0;
    e3y(i) = 0;
    e3z(i) = 0;
  end
end




	l1 = fopen(sprintf('%s/lambda1.txt',path)','w');
	l2 = fopen(sprintf('%s/lambda2.txt',path)','w');
	l3 = fopen(sprintf('%s/lambda3.txt',path)','w');
	v1x = fopen(sprintf('%s/e1x.txt',path)','w');
	v1y = fopen(sprintf('%s/e1y.txt',path)','w');
	v1z = fopen(sprintf('%s/e1z.txt',path)','w');

	v2x = fopen(sprintf('%s/e2x.txt',path)','w');
	v2y = fopen(sprintf('%s/e2y.txt',path)','w');
	v2z = fopen(sprintf('%s/e2z.txt',path)','w');

	v3x = fopen(sprintf('%s/e3x.txt',path)','w');
	v3y = fopen(sprintf('%s/e3y.txt',path)','w');
	v3z = fopen(sprintf('%s/e3z.txt',path)','w');

	fprintf(l1, '%.10f\n',lambda1);
	fprintf(l2, '%.10f\n',lambda2);
	fprintf(l3, '%.10f\n',lambda3);

	fprintf(v1x, '%.10f\n',e1x);
	fprintf(v1y, '%.10f\n',e1y);
	fprintf(v1z, '%.10f\n',e1z);

	fprintf(v2x, '%.10f\n',e2x);
	fprintf(v2y, '%.10f\n',e2y);
	fprintf(v2z, '%.10f\n',e2z);

	fprintf(v3x, '%.10f\n',e3x);
	fprintf(v3y, '%.10f\n',e3y);
	fprintf(v3z, '%.10f\n',e3z);


fclose('all');
