% function [phi, theta, psi] = anglem3d(M)
% Returns the three euler angles phi, theta, psi from M
% This method uses the x-convention, the 3-1-3 Euler angles phi, theta, psi
% (around the Z, Y, Z axis)
function [phi, theta, psi] = anglem3d(M)

    sd = size(M, 4);
 
    % Check whether we have a simple 3D matrix or a field of matrices
    if (sd == 1)
        Qxx = M(1);
        Qyx = M(2);
        Qzx = M(3);
        Qxy = M(4);
        Qyy = M(5);
        Qzy = M(6);
        Qxz = M(7);
        Qyz = M(8);
        Qzz = M(9);
    else
        Qxx = M(:,:,:,1);
        Qyx = M(:,:,:,2);
        Qzx = M(:,:,:,3);
        Qxy = M(:,:,:,4);
        Qyy = M(:,:,:,5);
        Qzy = M(:,:,:,6);
        Qxz = M(:,:,:,7);
        Qyz = M(:,:,:,8);
        Qzz = M(:,:,:,9);
    end

%     phi = atan2(Qzx, Qzy);
%     theta = acos(Qzz);
%     psi = -atan2(Qxz, Qyz);

    phi = atan2(Qzx, -Qzy);
    theta = acos(Qzz);
    psi = atan2(Qxz, Qyz);
%     phi = atan2(Qzy, Qzz);
%     theta = atan2(-Qzx, sqrt(Qzy.^2 + Qzz.^2));
%     psi = atan2(Qyx, Qxx);
    