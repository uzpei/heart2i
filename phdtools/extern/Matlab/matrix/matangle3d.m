% function [phi, theta, psi] = anglem3d(M)
% Returns the three euler angles phi, theta, psi from M
% This method uses the x-convention, the 3-1-3 Euler angles phi, theta, psi
% (around the Z, Y, Z axis)
function M = matangle3d(phi, theta, psi)

    [sx, sy, sz, nd] = size(phi);
    
    M = zeros(sx, sy, sz, 9);
    
%     M(:,:,:,1) = cos(theta) .* cos(psi);
%     M(:,:,:,2) = -cos(theta) .* sin(psi);
%     M(:,:,:,3) = sin(theta);
%     M(:,:,:,4) = cos(phi) .* sin(psi) + sin(phi) .* sin(theta) .* cos(psi);
%     M(:,:,:,5) = cos(phi) .* cos(psi) - sin(phi) .* sin(theta) .* sin(psi);
%     M(:,:,:,6) = -sin(phi) .* cos(theta);
%     M(:,:,:,7) = sin(phi) .* sin(psi) - cos(phi) .* sin(theta) .* cos(psi);
%     M(:,:,:,8) = sin(phi) .* cos(psi) + cos(phi) .* sin(theta) .* sin(psi);
%     M(:,:,:,9) = cos(phi) .* cos(theta);
%     
% swap psi, phi
% phib = phi;
% phi = psi;
% psi = phib;

% M(:,:,:,1) = cos(phi) .* cos(psi) - sin(phi) .* cos(theta) .* sin(psi);
% M(:,:,:,2) = -sin(phi) .* cos(psi) - cos(phi) .* cos(theta) .* sin(psi);
% M(:,:,:,3) = sin(theta) .* sin(psi);
% 
% M(:,:,:,4) = cos(theta) .* sin(psi) + sin(phi) .* cos(theta) .* cos(psi);
% M(:,:,:,5) = -sin(phi) .* sin(psi) + cos(phi) .* cos(theta) .* cos(psi);
% M(:,:,:,6) = -sin(theta) .* cos(psi);
% 
% M(:,:,:,7) = sin(phi) .* sin(theta);
% M(:,:,:,8) = cos(phi) .* sin(theta);
% M(:,:,:,9) = cos(theta);
%     
% Z(phi) X(theta) Z(psi)
M(:,:,:,1) = cos(phi) .* cos(psi) - sin(phi) .* sin(psi) .* cos(theta);
M(:,:,:,2) = cos(phi) .* sin(psi) .* cos(theta) + sin(phi) .* cos(psi);
M(:,:,:,3) = sin(psi) .* sin(theta);
M(:,:,:,4) = -sin(phi) .* cos(psi) .* cos(theta) - cos(phi) .* sin(psi);
M(:,:,:,5) = cos(phi) .* cos(psi) .* cos(theta) - sin(phi) .* sin(psi);
M(:,:,:,6) = cos(psi) .* sin(theta);
M(:,:,:,7) = sin(phi) .* sin(theta);
M(:,:,:,8) = -cos(phi) .* sin(theta);
M(:,:,:,9) = cos(theta);

% Z(psi) Y(theta) X(phi)
%M(:,:,:,1) = cos(psi)
