function s = orthogonality3d(F, varargin)

% s12 = sum(F(:,:,:, 1:3) .* F(:,:,:, 4:6), 4);
% s13 = sum(F(:,:,:, 1:3) .* F(:,:,:, 7:9), 4);
% s23 = sum(F(:,:,:, 4:6) .* F(:,:,:, 7:9), 4);
s12 = dot(F(:,:,:, 1:3), F(:,:,:, 4:6), 4);
s13 = dot(F(:,:,:, 1:3), F(:,:,:, 7:9), 4);
s23 = dot(F(:,:,:, 4:6), F(:,:,:, 7:9), 4);

if (nargin > 1)
    mask = varargin{1};
    s12(~mask) = 0;
    s13(~mask) = 0;
    s23(~mask) = 0;
end

s = sum(s12(:) + s13(:) + s23(:));

end