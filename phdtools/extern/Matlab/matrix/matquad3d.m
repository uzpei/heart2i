% function [w, x, y, z] = quatm3d(M)
% M is an M*N*O*P matrix where P has 9 elements forming a rotation matrix.
% Indexing is 
% 1)Qxx 2)Qyx 3)Qzx
% 4)Qxy 5)Qyy 6)Qzy
% 7)Qxz 8)Qyz 9)Qzz
function M = matquad3d(varargin)

    if (nargin == 1)
        Q = varargin{1};
        x = Q(:,:,:,1);
        y = Q(:,:,:,2);
        z = Q(:,:,:,3);
        w = Q(:,:,:,4);
        
    elseif (nargin == 4)
        x = varargin{1};
        y = varargin{2};
        z = varargin{3};
        w = varargin{4};
    end

%    [sx, sy, sz, sd] = size(Q);
%     x2 = x.^2;
%     y2 = y.^2;
%     z2 = z.^2;
%     twox = 2 * x;
%     twoy = 2 * y;
%     twoz = 2 * z;
%     twox2 = 2 * x2;
%     twoy2 = 2 * y2;
%     twoz2 = 2 * z2;
    
    [sx, sy, sz] = size(w);
    M = zeros(sx, sy, sz, 9);
    
%     M(:,:,:,1) = 1 - twoy2 - twoz2;
%     M(:,:,:,2) = twox .* y + twoz .* w;
%     M(:,:,:,3) = twox .* z - twoy .* w;
%     M(:,:,:,4) = twox .* y - twoz .* w;
%     M(:,:,:,5) = 1 - twox2 - twoz2;
%     M(:,:,:,6) = twoy .* z + twox .* w;
%     M(:,:,:,7) = twox .* z + twoy .* w;
%     M(:,:,:,8) = twoy .* z - twox .* w;
%     M(:,:,:,9) = 1 - twox2 - twoy2;
    
    M(:,:,:,1) = x.^2 - y.^2 - z.^2 + w.^2;
    M(:,:,:,4) = 2 * (x .* y + z .* w);
    M(:,:,:,7) = 2 * (x .* z - y .* w);

    M(:,:,:,2) = 2 * (x .* y - z .* w);
    M(:,:,:,5) = -x.^2 + y.^2 - z.^2 + w.^2;
    M(:,:,:,8) = 2 * (y .* z + x .* w);

    M(:,:,:,3) = 2 * (x .* z + y .* w);
    M(:,:,:,6) = 2 * (y .* z - x .* w);
    M(:,:,:,9) = -x.^2 - y.^2 + z.^2 + w.^2;
    
    % Normalize
    n1 = sqrt(M(:,:,:,1) .^ 2 + M(:,:,:,4) .^ 2 + M(:,:,:,7) .^ 2);
    n2 = sqrt(M(:,:,:,2) .^ 2 + M(:,:,:,5) .^ 2 + M(:,:,:,8) .^ 2);
    n3 = sqrt(M(:,:,:,3) .^ 2 + M(:,:,:,6) .^ 2 + M(:,:,:,9) .^ 2);
    
    M(:,:,:,1) = M(:,:,:,1) ./ n1;
    M(:,:,:,4) = M(:,:,:,4) ./ n1;
    M(:,:,:,7) = M(:,:,:,7) ./ n1;

    M(:,:,:,2) = M(:,:,:,2) ./ n2;
    M(:,:,:,5) = M(:,:,:,5) ./ n2;
    M(:,:,:,8) = M(:,:,:,8) ./ n2;

    M(:,:,:,3) = M(:,:,:,3) ./ n3;
    M(:,:,:,6) = M(:,:,:,6) ./ n3;
    M(:,:,:,9) = M(:,:,:,9) ./ n3;
    
    % Remove NaNs
    M(isnan(M)) = 0;

    % 1 Q(1,1,:).^2 - Q(1,2,:).^2-Q(1,3,:).^2+Q(1,4,:).^2
    % 4 2*(Q(1,1,:).*Q(1,2,:)+Q(1,3,:).*Q(1,4,:))
    % 7 2*(Q(1,1,:).*Q(1,3,:)-Q(1,2,:).*Q(1,4,:))
        
    % 2 2*(Q(1,1,:).*Q(1,2,:)-Q(1,3,:).*Q(1,4,:)),
    % 5 -Q(1,1,:).^2+Q(1,2,:).^2-Q(1,3,:).^2+Q(1,4,:).^2
    % 8 2*(Q(1,2,:).*Q(1,3,:)+Q(1,1,:).*Q(1,4,:))
    
    % 3 2*(Q(1,1,:).*Q(1,3,:)+Q(1,2,:).*Q(1,4,:))
    % 6 2*(Q(1,2,:).*Q(1,3,:)-Q(1,1,:).*Q(1,4,:))
    % 9 -Q(1,1,:).^2-Q(1,2,:).^2+Q(1,3,:).^2+Q(1,4,:).^2]

%     tic
%     M(:,:,:,1) = 1 - 2*y.^2 - 2*z.^2;
%     M(:,:,:,2) = 2*x .* y + 2*z .* w;
%     M(:,:,:,3) = 2*x .* z - 2*y .* w;
%     M(:,:,:,4) = 2*x .* y - 2*z .* w;
%     M(:,:,:,5) = 1 - 2*x.^2 - 2*z.^2;
%     M(:,:,:,6) = 2*y .* z + 2*x .* w;
%     M(:,:,:,7) = 2*x .* z + 2*y .* w;
%     M(:,:,:,8) = 2*y .* z - 2*x .* w;
%     M(:,:,:,9) = 1 - 2*x.^2 - 2*y.^2;
%     toc
    
%     % From http://en.wikipedia.org/wiki/Rotation_matrix#Quaternion
%      Nq = w.^2 + x.^2 + y.^2 + z.^2;
%     
%     s = 2 ./ Nq;
%     s(Nq <= 0) = 0;
%     
%     X = x .* s;
%     Y = y .* s;
%     Z = z .* s;
%     
%     wX = w .* X;
%     wY = w .* Y;
%     wZ = w .* Z;
%     
%     xX = x .* X;
%     xY = x .* Y;
%     xZ = x .* Z;
%     
%     yY = y .* Y;
%     yZ = y .* Z;
%     zZ = z .* Z;
%     
%     M = zeros(sx, sy, sz, 9);
%     
%     M(:,:,:,1) = 1 - (yY + zZ);
%     M(:,:,:,2) = xY + wZ;
%     M(:,:,:,3) = xZ - wY;
%     
%     M(:,:,:,4) = xY - wZ;
%     M(:,:,:,5) = 1 - (xX + zZ);
%     M(:,:,:,6) = yZ + wX;
%     
%     M(:,:,:,7) = xZ + wY;
%     M(:,:,:,8) = yZ - wX;
%     M(:,:,:,9) = 1 - (xX + yY);
    
end