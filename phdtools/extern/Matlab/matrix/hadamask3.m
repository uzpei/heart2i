% Compute A(mask) .* B(mask)
function hadaproduct = hadamask3(A, B, mask, hadaproduct)
    dm = A(mask) .* B(mask);
%     d = zeros(size(mask));
    hadaproduct(mask) = dm(:);
end