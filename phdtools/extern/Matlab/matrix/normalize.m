% function [vx, vy, vz] = normalize(vx, vy, vz);
% Perform vector field normalization.
function varargout = normalize(varargin)

% Assume nx * ny * nz * 3 matrix
if (nargin == 1)
    V = varargin{1};
    nd = ndims(V);
    
    rep = ones(1, nd);
    rep(nd) = nd-1;
    dnorm = (repmat(sqrt(sum(V .^ 2, nd)), rep));
    
    % This is *not* faster
%     dnorm = sqrt(sum(V .^ 2, nd));
%     V = V ./ dnorm(:, :, :, ones(3,1));

    V = V ./ dnorm;
    
    V(isnan(V)) = 0;
    varargout{1} = V;
    varargout{2} = dnorm;
    
elseif (nargin == 2)
    vx = varargin{1};
    vy = varargin{2};
    
    dnorm = sqrt(vx .^ 2 + vy .^ 2);
    vx = vx ./ dnorm;
    vy = vy ./ dnorm;

    vx(isnan(vx)) = 0;
    vy(isnan(vy)) = 0;
    
    varargout{1} = vx;
    varargout{2} = vy;
    varargout{3} = dnorm;
    
elseif (nargin == 3)
    vx = varargin{1};
    vy = varargin{2};
    vz = varargin{3};
    
    dnorm = sqrt(vx .^ 2 + vy .^ 2 + vz .^ 2);
    vx = vx ./ dnorm;
    vy = vy ./ dnorm;
    vz = vz ./ dnorm;

    vx(isnan(vx)) = 0;
    vy(isnan(vy)) = 0;
    vz(isnan(vz)) = 0;
    
    varargout{1} = vx;
    varargout{2} = vy;
    varargout{3} = vz;
    varargout{4} = dnorm;
end

end

