function Xm = mean_masked(mask)

pxlist = find(mask);
[I, J, K] = ind2sub(size(mask), pxlist);

X = [I,J,K]';
Xm = mean(X,2);
