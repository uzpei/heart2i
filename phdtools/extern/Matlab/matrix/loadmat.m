% Hack for extracting the first field in a struct
% (i.e. when saving a single mat to a file)
function M = loadmat(path)
if (exist(path))
    data = load(path);
    dnames = fieldnames(data);
    M = data.(dnames{1});
else
    M = [];
end
