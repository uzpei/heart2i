function d = dotmask3(A, B, mask)
    A1 = A(:,:,:,1);
    A2 = A(:,:,:,2);
    A3 = A(:,:,:,3);

    B1 = B(:,:,:,1);
    B2 = B(:,:,:,2);
    B3 = B(:,:,:,3);

    dm = A1(mask) .* B1(mask) + A2(mask) .* B2(mask) + A3(mask) .* B3(mask);
    d = zeros(size(mask));
    d(mask) = dm(:);
end