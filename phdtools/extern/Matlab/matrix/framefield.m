% function frames = framefield(T, N, B, varargin)
% varargin = swap XY (bool), mask (logical)
function frames = framefield(T, N, B, varargin)

options = parseInput(struct('Assemble', false', 'StoreF2', true, 'SinglePrecision', false, 'ApplyMask', false, 'Mask', []), varargin{:});

if (options.StoreF2 && isempty(N))
    N = cross(B, T, 4);
end

if (options.SinglePrecision)
    T = single(T);
    N = single(N);
    B = single(B);
end

if (options.ApplyMask && ~isempty(options.Mask))
    fprintf('Applying mask in frame field construction...\n');
    T = T .* repmat(mask, [1 1 1 3]);
    B = B .* repmat(mask, [1 1 1 3]);
    
    if (~isempty(N))
        N = N .* repmat(mask, [1 1 1 3]);
    end
end

% Assemble frame
if (options.Assemble)
    % When using this option, N should never be empty
    [sx, sy, sz, nd] = size(T);
    frames = zeros(sx, sy, sz, 9);
    frames(:,:,:,1:3) = T(:,:,:,1:3);
    frames(:,:,:,4:6) = N(:,:,:,1:3);
    frames(:,:,:,7:9) = B(:,:,:,1:3);
else
    frames.T = T;
    frames.B = B;
    
    if (options.StoreF2 && ~isempty(N))
        frames.N = N;
    end
end
