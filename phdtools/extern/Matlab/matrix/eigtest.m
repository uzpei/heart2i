sliceid = 35;

mask1 = FA(:,:,:)<0.3;
mask2 = a0(:,:,:)>0.0005;

mask = mask1 .* mask2;
%mask = mask2;

figure(1)
imagesc(mask(:,:,sliceid));
colormap gray;

L1masked = (L1 .* mask) < 0;
L2masked = (L2 .* mask) < 0;
L3masked = (L3 .* mask) < 0;

disp('Eig X pos');
sum(L1masked(:)) == 0
disp('Eig Y pos');
sum(L2masked(:)) == 0
disp('Eig Z pos');
sum(L3masked(:)) == 0

%figure(1);
%imagesc(L1masked(:,:,36))
%figure(2);
%imagesc(L1masked(:,:,36))
%figure(3);
%imagesc(L1masked(:,:,36))

% Test for positive definiteness
M = zeros(3,3);
inc = 0;
maskels = sum(mask(:)>0);
pourri = zeros(size(L1));
for i=1:size(L1,1)
for j=1:size(L1,2)
for k=1:size(L1,3)
	if (mask(i,j,k) > 0)
		M(1,1) = image(i, j, k + 64 * 4);
		M(2,2) = image(i, j, k + 64 * 5);
		M(3,3) = image(i, j, k + 64 * 6);
		M(1,2) = image(i, j, k + 64 * 7);
		M(1,3) = image(i, j, k + 64 * 8);
		M(2,3) = image(i, j, k + 64 * 9);

		M(2,1) = M(1,2);
		M(3,1) = M(1,3);
		M(3,2) = M(2,3);
		
		if (~(all( all( M == M' ) ) & min( eig( M ) ) > 0))
			inc = inc + 1;
			pourri(i,j,k) = 1;
		end
	end
end
end
end
disp('Pos semidefinite');
maskels
inc
inc == 0

figure(2)
imagesc(pourri(:,:,sliceid));
colormap gray;

figure(3)
i = 1;
while (1)
	imagesc(squeeze(mask(i,:,:)));
	i = 1 + mod((i + 1), 127);
	pause
end