% function [w, x, y, z] = quatm3d(M)
% M is an M*N*O*P matrix where P has 9 elements forming a rotation matrix.
% Indexing is 
% 1)Qxx 2)Qyx 3)Qzx
% 4)Qxy 5)Qyy 6)Qzy
% 7)Qxz 8)Qyz 9)Qzz
function [x, y, z, w] = quatm3d(M)

    [sx, sy, sz, sd] = size(M);
 
    % Check whether we have a simple 3D matrix or a field of matrices
    if (sd == 1)
        Qxx = M(1);
        Qyx = M(2);
        Qzx = M(3);
        Qxy = M(4);
        Qyy = M(5);
        Qzy = M(6);
        Qxz = M(7);
        Qyz = M(8);
        Qzz = M(9);
    else
        Qxx = M(:,:,:,1);
        Qyx = M(:,:,:,2);
        Qzx = M(:,:,:,3);
        Qxy = M(:,:,:,4);
        Qyy = M(:,:,:,5);
        Qzy = M(:,:,:,6);
        Qxz = M(:,:,:,7);
        Qyz = M(:,:,:,8);
        Qzz = M(:,:,:,9);
    end

    if (sd == 1)
        denom=zeros(4,1);
        denom(1)=0.5*sqrt(1+Qxx-Qyy-Qzz);
        denom(2)=0.5*sqrt(1-Qxx+Qyy-Qzz);
        denom(3)=0.5*sqrt(1-Qxx-Qyy+Qzz);
        denom(4)=0.5*sqrt(1+Qxx+Qyy+Qzz);        
        
        switch find(denom==max(denom),1,'first')
            case 1
                x=denom(1);
                y=(Qxy+Qyx)/(4*x);
                z=(Qxz+Qzx)/(4*x);
                w=(Qyz-Qzy)/(4*x);
            case 2
                y=denom(2);
                x=(Qxy+Qyx)/(4*y);
                z=(Qyz+Qzy)/(4*y);
                w=(Qzx-Qxz)/(4*y);
            case 3
                z=denom(3);
                x=(Qxz+Qzx)/(4*z);
                y=(Qyz+Qzy)/(4*z);
                w=(Qxy-Qyx)/(4*z);
            case 4
                w=denom(4);
                x=(Qyz-Qzy)/(4*w);
                y=(Qzx-Qxz)/(4*w);
                z=(Qxy-Qyx)/(4*w);
        end
    else
        x1 = 0.5 * sqrt(1 + Qxx - Qyy - Qzz);
        y2 = 0.5 * sqrt(1 - Qxx + Qyy - Qzz);
        z3 = 0.5 * sqrt(1 - Qxx - Qyy + Qzz);
        w4 = 0.5 * sqrt(1 + Qxx + Qyy + Qzz);

        mask1 = (x1 > y2) & (x1 > z3) & (x1 > w4);
        mask2 = (y2 > x1) & (y2 > z3) & (y2 > w4);
        mask3 = (z3 > x1) & (z3 > y2) & (z3 > w4);
        mask4 = (w4 > x1) & (w4 > y2) & (w4 > z3);

        % x1
        y1 = (Qxy + Qyx) ./ (4 * x1);
        z1 = (Qxz + Qzx) ./ (4 * x1);
        w1 = (Qyz - Qzy) ./ (4 * x1);

        % y2
        x2 = (Qxy + Qyx) ./ (4 * y2);
        z2 = (Qyz + Qzy) ./ (4 * y2);
        w2 = (Qzx - Qxz) ./ (4 * y2);

        % z3
        x3 = (Qxz + Qzx) ./ (4 * z3);
        y3 = (Qyz + Qzy) ./ (4 * z3);
        w3 = (Qxy - Qyx) ./ (4 * z3);

        % w4
        x4 = (Qyz - Qzy) ./ (4 * w4);
        y4 = (Qzx - Qxz) ./ (4 * w4);
        z4 = (Qxy - Qyx) ./ (4 * w4);

        % Fill volumes
        x = zeros(sx, sy, sz);
        y = zeros(sx, sy, sz);
        z = zeros(sx, sy, sz);
        w = zeros(sx, sy, sz);

        x(mask1) = x1(mask1);
        x(mask2) = x2(mask2);
        x(mask3) = x3(mask3);
        x(mask4) = x4(mask4);

        y(mask1) = y1(mask1);
        y(mask2) = y2(mask2);
        y(mask3) = y3(mask3);
        y(mask4) = y4(mask4);

        z(mask1) = z1(mask1);
        z(mask2) = z2(mask2);
        z(mask3) = z3(mask3);
        z(mask4) = z4(mask4);

        w(mask1) = w1(mask1);
        w(mask2) = w2(mask2);
        w(mask3) = w3(mask3);
        w(mask4) = w4(mask4);

    end

    % Normalize quaternions
    dnorm = sqrt(w .^ 2 + x .^ 2 + y .^ 2 + z .^ 2);
    w = w ./ dnorm;
    x = x ./ dnorm;
    y = y ./ dnorm;
    z = z ./ dnorm;

%     w = 0.5 * sqrt(1 + Qxx + Qyy + Qzz);
%     x = (Qzy - Qyz) ./ (4 * w);
%     y = (Qxz - Qzx) ./ (4 * w);
%     z = (Qyx - Qxy) ./ (4 * w);
    
    % From http://en.wikipedia.org/wiki/Rotation_matrix#Quaternion
    % This is an efficient version but creates imaginary numbers
%     t = Qxx + Qyy + Qzz;
%     r = sqrt(1+t);
%     s = 0.5 ./ r;
%     w = 0.5 .* r;
%     x = (Qzy - Qyz) .* s;
%     y = (Qxz - Qzx) .* s;
%     z = (Qyx - Qxy) .* s;    

    % Test length of w, x, y, z
    
    % More stable
%     t = Qxx + Qyy + Qzz;
%     
%     mask1 = (Qxx > Qyy) & (Qxx > Qzz);
%     mask2 = (Qyy > Qxx) & (Qyy > Qzz);
%     mask3 = (Qzz > Qxx) & (Qzz > Qyy);
%     
%     r = zeros(sx, sy, sz);
%     r(mask1) = sqrt(1 + Qxx(mask1) - Qyy(mask1) - Qzz(mask1));
%     r(mask2) = sqrt(1 + Qyy(mask2) - Qxx(mask2) - Qzz(mask2));
%     r(mask3) = sqrt(1 + Qzz(mask3) - Qxx(mask3) - Qyy(mask3));
% 
%     s(mask1) = 0.5 ./ r(mask1);
%     s(mask2) = 0.5 ./ r(mask2);
%     s(mask3) = 0.5 ./ r(mask3);
% 
%     w(mask1) = (Qzy(mask1) - Qyz(mask1)) .* s(mask1);
%     w(mask2) = (Qzy(mask2) - Qyz(mask2)) .* s(mask2);
%     w(mask3) = (Qzy(mask3) - Qyz(mask3)) .* s(mask3);
% 
%     x(mask1) = 0.5 .* r(mask1);
%     x(mask2) = 0.5 .* r(mask2);
%     x(mask3) = 0.5 .* r(mask3);
% 
%     y(mask1) = (Qxy(mask1) + Qyx(mask1)) .* s(mask1);
%     y(mask2) = (Qxy(mask2) + Qyx(mask2)) .* s(mask2);
%     y(mask3) = (Qxy(mask3) + Qyx(mask3)) .* s(mask3);
% 
%     z(mask1) = (Qzx(mask1) + Qxz(mask1)) .* s(mask1);
%     z(mask2) = (Qzx(mask2) + Qxz(mask2)) .* s(mask2);
%     z(mask3) = (Qzx(mask3) + Qxz(mask3)) .* s(mask3);

    % Explicit from
    % http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
%     t = Qxx + Qyy + Qzz;
%     s1 = sqrt(t + 1.0) * 2;
%     s2 = sqrt(1.0 + Qxx - Qyy - Qzz) * 2;
%     s3 = sqrt(1.0 + Qyy - Qxx - Qzz) * 2;
%     s4 = sqrt(1.0 + Qzz - Qxx - Qyy) * 2;
%     
%     mask1 = t > 0;
%     mask2 = t < 0 & (Qxx > Qyy) & (Qxx > Qzz);
%     mask3 = t < 0 & (Qyy > Qxx) & (Qyy > Qzz);
%     mask4 = t < 0 & (Qzz > Qxx) & (Qzz > Qxx);
%     
%     w = zeros(sx, sy, sz);
%     x = zeros(sx, sy, sz);
%     y = zeros(sx, sy, sz);
%     z = zeros(sx, sy, sz);
%     
%     w(mask1) = 0.25 * s1(mask1);
%     x(mask1) = (Qzy(mask1) - Qyz(mask1)) ./ s2(mask1);
    
% if (tr > 0) { 
%   float S = sqrt(tr+1.0) * 2; // S=4*qw 
%   qw = 0.25 * S;
%   qx = (m21 - m12) / S;
%   qy = (m02 - m20) / S; 
%   qz = (m10 - m01) / S; 
% } else if ((m00 > m11)&(m00 > m22)) { 
%   float S = sqrt(1.0 + m00 - m11 - m22) * 2; // S=4*qx 
%   qw = (m21 - m12) / S;
%   qx = 0.25 * S;
%   qy = (m01 + m10) / S; 
%   qz = (m02 + m20) / S; 
% } else if (m11 > m22) { 
%   float S = sqrt(1.0 + m11 - m00 - m22) * 2; // S=4*qy
%   qw = (m02 - m20) / S;
%   qx = (m01 + m10) / S; 
%   qy = 0.25 * S;
%   qz = (m12 + m21) / S; 
% } else { 
%   float S = sqrt(1.0 + m22 - m00 - m11) * 2; // S=4*qz
%   qw = (m10 - m01) / S;
%   qx = (m02 + m20) / S;
%   qy = (m12 + m21) / S;
%   qz = 0.25 * S;
% }

end