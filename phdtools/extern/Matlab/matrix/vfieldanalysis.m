%function vfieldanalysis

% Generate some vector field
f = @(t,Y) [Y(2); -sin(Y(1))];
y1 = linspace(-2,8,20);
y2 = linspace(-2,2,20);

% creates two matrices one for all the x-values on the grid, and one for
% all the y-values on the grid. Note that x and y are matrices of the same
% size and shape, in this case 20 rows and 20 columns
[x,y] = meshgrid(y1,y2);
size(x)
size(y)
%end
u = zeros(size(x));
v = zeros(size(x));

% we can use a single loop over each element to compute the derivatives at
% each point (y1, y2)
t=0; % we want the derivatives at each point at t=0, i.e. the starting time
for i = 1:numel(x)
    Yprime = f(t,[x(i); y(i)]);
    u(i) = Yprime(1);
    v(i) = Yprime(2);
end

% Vector Field Analysis for Oriented Patterns 
% Chiao-Fe Shu and Ramesh C. Jain (93)

% Compute theta-field
theta = atan2(v, u);

% Compute Xi-field (tan theta)
xi = v ./ u;

% Compute cos-field (u' in Shu and Jain)
w = cos(theta);

figure(1)

subplot(3,1,1);
quiver(x,y,u,v,'r'); figure(gcf)
xlabel('y_1')
ylabel('y_2')
%axis tight equal;

subplot(3,1,2);
%imagesc(xi);
imagesc(curl(x, y, u, v));
%axis tight equal;
%set(gca,'YDir','normal')

subplot(3,1,3);
imagesc(w);
%set(gca,'YDir','normal')
%axis tight equal;
colormap jet
%colorbar

% Precompute curl and divergence
curlf = curl(u, v);
divf = divergence(u, v);

% Compute adjacency matrix
% use 8-nearest neighbors to estimate parameters 
nrc = numel(x);
[ic,icd] = ixneighbors(x);
S = sparse(ic,icd,ones(numel(icd),1),nrc,nrc);
%spy(S)

inds = 1:numel(x);
%[ic, icd] = ixneighbors(xi, 

A = zeros(2,2);

tic
flowpatterns1 = zeros(size(x));
flowpatterns2 = zeros(size(x));
flowpatterns3 = zeros(size(x));
flowpatterns4 = zeros(size(x));
flowpatterns5 = zeros(size(x));
flowpatterns6 = zeros(size(x));

flowlabels2 = zeros(size(x));
flowlabels3 = zeros(size(x));
flowlabels4 = zeros(size(x));
for i=inds
    % Find connected neighbors
    neighbors = find(S(i,:));
    n = length(neighbors);
    
    % Assemble sigma2
    % Col 1 is: w_i
    % Col 2 is: w_i * xi_i
    sigma2 = zeros(n, 2);
    
    w_i = w(neighbors);
    xi_i = xi(neighbors);
    sigma2(:, 1) = w_i(:);
    sigma2(:, 2) = xi_i(:) .* w_i(:);

    % Assemble sigma4
    % Col 1 is: x_i * w_i
    % Col 2 is: y_i * w_i
    % Col 3 is: -xi_i * x_i * w_i
    % Col 4 is: -xi_i * y_i * w_i
    sigma4 = zeros(n, 4);
    
    x_i = x(neighbors);
    y_i = y(neighbors);
    sigma4(:, 1) = x_i(:) .* w_i(:);
    sigma4(:, 2) = y_i(:) .* w_i(:);
    sigma4(:, 3) = -x_i(:) .* xi_i(:) .* w_i(:);
    sigma4(:, 4) = -y_i(:) .* xi_i(:) .* w_i(:);
    
%     if (~isempty(sigma2))
%         i
%         sigma2
%     end

    % Assemble Psi
    psi = -sigma4' * sigma4 + sigma4' * sigma2 * pinv(sigma2' * sigma2) * (sigma2' * sigma4);
    
    % Compute L4 = [a, b, c, d]
    [L4, L4v] = eigs(psi, 1);
    
    % Compute L2 = [e, f]
    L2 = -pinv(sigma2' * sigma2) * sigma2' * sigma4 * L4;
    
    % If necessary, we could compute the characteristic matrix A
    % elem 1: div(F) + (c-b)
    % elem 2: (a + d) + curl(F)
    % elem 3: (a + d) - curl(F)
    % elem 4: -(c - b) + div(F)
%     A(1) = -(L4(1) - L4(2)) + divf(i);
%     A(2) = (L4(1) + L4(4)) + curlf(i);
%     A(3) = (L4(1) + L4(4)) - curlf(i);
%     A(4) = -(L4(1) - L4(2)) + divf(i);
    
    % Compute two eigenvalues of A
%     [ev, ed] = eigs(A, 2)
%     l1 = ev(:,1)
%     l2 = ev(:,2)

    % We have all we need to classify pixels
    curl_i = L4(1)-L4(4);
    div_i = L4(2) + L4(3);
    def_i = sqrt((L4(3)-L4(2))^2 + (L4(1)+L4(4))^2);
    
    % Try to identify center flows
    epsi = 1e-2;
    flowpatterns1(i) = def_i^2 > curl_i ^2 && abs(div_i) > sqrt(def_i^2 - curl_i^2);
    flowpatterns2(i) = def_i^2 > curl_i ^2 && abs(div_i) <+ sqrt(def_i^2 - curl_i^2);

    flowpatterns3(i) = abs(def_i^2) <= epsi && abs(curl_i^2) <= epsi && abs(def_i^2 - curl_i^2) <= epsi && abs(div_i) > epsi;
    flowpatterns4(i) = abs(def_i^2) > epsi && abs(curl_i^2) > epsi   && abs(def_i^2 - curl_i^2) <= epsi && abs(div_i) > epsi;
    
    flowpatterns5(i) = def_i^2 < curl_i^2 && abs(div_i) <= epsi;
    flowpatterns6(i) = def_i^2 < curl_i^2 && abs(div_i) > epsi;
    
    flowlabels2(i) = curl_i;
    flowlabels3(i) = div_i;
    flowlabels4(i) = def_i;
end
toc

figure(4)
subplot(3,2,1);
imagesc(flowpatterns1);
title('node');

subplot(3,2,2);
imagesc(flowpatterns2);
title('saddle');

subplot(3,2,3);
imagesc(flowpatterns3);
title('star');

subplot(3,2,4);
imagesc(flowpatterns4);
title('improper');

subplot(3,2,5);
imagesc(flowpatterns5);
title('center');

subplot(3,2,6);
imagesc(flowpatterns6);
title('spirral');
colormap gray

figure(5)
subplot(3,1,1);
imagesc(flowlabels2);
%axis equal tight
title('curl');
colorbar

subplot(3,1,2);
imagesc(flowlabels3);
%axis equal tight
title('div');
colorbar

subplot(3,1,3);
imagesc(flowlabels4);
%axis equal tight
title('deformation');

colormap jet
colorbar


