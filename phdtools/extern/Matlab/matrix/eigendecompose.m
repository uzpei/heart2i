function [V, D, Xmean] = eigendecompose(X)

m = size(X, 2);

% Compute mean
Xmean = mean(X, 2);

% Subtract mean from each column
X = X - repmat(Xmean, [1 m]);

% The covariance matrix is C = X * X'
% Since the rank of X is at most the number of columns (m << n)
% we don't need to compute the evectors and evalues of the (huge) n*n matrix
% Instead we can find then directly from the eigenvectors of X' * X which
% is m * m
%
% Let eigenvectors v_i of X' * X be such that
% X'*X*v_i = l*v_i
% X*X'X*v_i = X*l*v_i (premultiply by X)
% X*X'(X*v_i) = l*(X*v_i)
% => The eigenvectors of XX' are the eigenvectors of X'X premultiplied by X


% Compute the eigenvectors of X'X
% Scaling is not important... can divide by m or not
%L = X'*X / m; 
L = X'*X; 

% Diagonal elements of D are eigenvalues of L=X'*X and C=X*X'.
[V, D] = eig(L); 

%V
%D
% Threshold eigenvalues of L based on cumulative energy
cenergy = cumsum(sum(D));
cmax = max(cenergy);
cthreshold = 1e-8;
thresholded = find(cenergy / cmax <= cthreshold);
V(:,thresholded) = [];
D(:,thresholded) = [];
D(thresholded,:) = [];

% The eigenvectors of XX' are the eigenvectors of X'X premultiplied by X
V = X * V;

% Normalize eigenvectors
% for i=1:size(V, 2)
%    V(:,i) = V(:,i) / (eps + sqrt(V(:,i)' * V(:,i)));
% end

D = diag(D);
end