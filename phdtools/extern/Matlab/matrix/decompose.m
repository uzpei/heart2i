function [Tx, Ty, Tz] = decompose(T)
    Tx = T(:,:,:,1);
    Ty = T(:,:,:,2);
    Tz = T(:,:,:,3);
