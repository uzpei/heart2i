% function V = normalizemulti(C);
% Perform vector field normalization at each row
function V = normalizemulti(V)

dim = size(V, 2);
Vnorm = V .^ 2;
Vnorm = sqrt(sum(Vnorm'))';
V = V ./ repmat(Vnorm, [1 dim]);
