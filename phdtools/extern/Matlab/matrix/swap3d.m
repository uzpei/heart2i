function A = swap3d(A, ordering)

B = A;
A(:,:,:,1) = B(:,:,:,ordering(1));
A(:,:,:,2) = B(:,:,:,ordering(2));
A(:,:,:,3) = B(:,:,:,ordering(3));
