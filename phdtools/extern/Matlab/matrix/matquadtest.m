%% Test field of frames quaternion to matrix conversion
sx = 64;
sy = 64;
sz = 128;

fprintf('Creating random volume...\n');
tic
frames = zeros(sx, sy, sz, 9);
for x=1:sx
    for y=1:sy
        for z=1:sz
        [M, R] = qr(rand(3,3));
        frames(x,y,z,:) = M(:);
        end
    end
end
toc

% q21 = SpinCalc('DCMtoQ', M1, eps, 0)
% q22 = SpinCalc('DCMtoQ', M2, eps, 0)
orthogonality3d(frames)

fprintf('Reconstructing matrix...\n');
tic
[x, y, z, w] = quatm3d(frames);
Q = matquad3d(x, y, z, w);
toc

% Output error
fprintf('||M - Q||  = %e\n', norm(frames(:)-Q(:),2));


%% Test single matrix quaternion to matrix conversion
%clear all
[M, R] = qr(rand(3,3));
M

% Compute quaternions
[x, y, z, w] = quatm3d(M);
q1 = [x y z w];
q2 = SpinCalc('DCMtoQ', M, eps, 0);

% Reconstruct matrix
Q1 = reshape(matquad3d(x, y, z, w), [3 3]);
Q2 = SpinCalc('QtoDCM', q2, eps, 0);

%Q3 = SpinCalc('QtoDCM', SpinCalc('DCMtoQ', M, eps, 0), eps, 0)

Q1
Q2
% Output error
fprintf('||q1 - q2|| = %e\n', norm(q1(:)-q2(:),2));
fprintf('||M - Q1||  = %e\n', norm(Q1(:)-M(:),2));
fprintf('||M - Q2||  = %e\n', norm(Q2(:)-M(:),2));
fprintf('||Q1 - Q2|| = %e\n', norm(Q1(:)-Q2(:),2));
