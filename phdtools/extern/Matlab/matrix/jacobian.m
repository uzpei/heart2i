% custom implementation of vector jacobian
% The jacobian is computed in a row-major fashion
% and uses matrix indexing (i,j,k) and *NOT* spatial indexing (x,y,z)
% i.e. D = [dFi/di, dFi/dj, dFi/dk; ...;  dFk/di,  dFk/dj,  dFk/dk]
function D = jacobian(V)

[sx, sy, sz, sv] = size(V);
D = zeros(sx, sy, sz, 9);

f1 = V(:,:,:,1);
f2 = V(:,:,:,2);
f3 = V(:,:,:,3);

% [D(:,:,:,1), D(:,:,:,2), D(:,:,:,3)] = gradient(fx);
% [D(:,:,:,4), D(:,:,:,5), D(:,:,:,6)] = gradient(fy);
% [D(:,:,:,7), D(:,:,:,8), D(:,:,:,9)] = gradient(fz);

d = [1, 2, 3];
D(:,:,:,1) = DGradient(f1, [], d(1));
D(:,:,:,2) = DGradient(f1, [], d(2));
D(:,:,:,3) = DGradient(f1, [], d(3));
D(:,:,:,4) = DGradient(f2, [], d(1));
D(:,:,:,5) = DGradient(f2, [], d(2));
D(:,:,:,6) = DGradient(f2, [], d(3));
D(:,:,:,7) = DGradient(f3, [], d(1));
D(:,:,:,8) = DGradient(f3, [], d(2));
D(:,:,:,9) = DGradient(f3, [], d(3));

%d = [2, 1, 3];
%f = [1, 2, 3];

% D(:,:,:,1) = DGradient(F(:,:,:,f(1)), [], d(1));
% D(:,:,:,2) = DGradient(F(:,:,:,f(1)), [], d(2));
% D(:,:,:,3) = DGradient(F(:,:,:,f(1)), [], d(3));
% D(:,:,:,4) = DGradient(F(:,:,:,f(2)), [], d(1));
% D(:,:,:,5) = DGradient(F(:,:,:,f(2)), [], d(2));
% D(:,:,:,6) = DGradient(F(:,:,:,f(2)), [], d(3));
% D(:,:,:,7) = DGradient(F(:,:,:,f(3)), [], d(1));
% D(:,:,:,8) = DGradient(F(:,:,:,f(3)), [], d(2));
% D(:,:,:,9) = DGradient(F(:,:,:,f(3)), [], d(3));

end