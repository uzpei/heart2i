% Orthogonalize the vector field V with respect to U
% Can call as 
% [Vx, Vy, Vz, Ux, Uy, Uz] = orthogonalize(Ux, Uy, Uz, Vx, Vy, Vz)
% V = orthogonalize(U, V)
% [V, option U] = orthogonalize(U, V)
%function varargout = orthogonalize(varargin)
function varargout = orthogonalize(varargin)

if (nargin == 2)
    U = varargin{1};
    V = varargin{2};
    
    U = normalize(U);
    V = V - repmat(dot(V, U, 4), [1 1 1 3]) .* U;
    V = normalize(V);
    
    varargout{2} = U;
    varargout{1} = V;
    
    % Test orthogonality
    M = dot(U, V, 4);
elseif (nargin == 6)
    Ux = varargin{1};
    Uy = varargin{2};
    Uz = varargin{3};
    
    Vx = varargin{4};
    Vy = varargin{5};
    Vz = varargin{6};

    % Make sure U is normalized
    [Ux, Uy, Uz] = normalize(Ux, Uy, Uz);

    % Compute projection
    ndot = (Ux .* Vx) + (Uy .* Vy) + (Uz .* Vz);
    
    % Compute orthogonal component
    Vx = Vx - ndot .* Ux;
    Vy = Vy - ndot .* Uy;
    Vz = Vz - ndot .* Uz;
    [Vx, Vy, Vz] = normalize(Vx, Vy, Vz);
    varargout{4} = Ux;
    varargout{5} = Uy;
    varargout{6} = Uz;
    varargout{1} = Vx;
    varargout{2} = Vy;
    varargout{3} = Vz;

    % Test orthogonality
    M = (Vx .* Ux + Vy .* Uy + Vz .* Uz);

end

% This should sum up to (near) zero
%numel(M) * eps;
orthogonalSum = abs(sum(M(:)));
if (orthogonalSum < numel(M) * eps)
%    fprintf('Orthogonality check PASSED: sum = %e\n', orthogonalSum);
else
    fprintf('Orthogonality check FAILED: sum = %e\n', orthogonalSum);
end

