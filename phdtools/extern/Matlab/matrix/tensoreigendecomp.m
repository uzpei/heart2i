% function eigfield = tensoreigendecomp(tensorVolume)
% tensorVolume is assumed to be a 4D image: 3D for volume + 1D for each of the 6
% (symmetric) tensor components. The components are assumed to be listed
% in column-major order
% i.e. txx = c1, txy = c2, tyy = c3, txz = c4, tyz = c5, tzz = c6.
% An eigenvector field is returned, with 3D for volume + 1D for 3
% eigenvectors + 1D for 3 components
function varargout = tensoreigendecomp(tensorVolume)

sx = size(tensorVolume,1);
sy = size(tensorVolume,2);
sz = size(tensorVolume,3);
tot = sx*sy*sz;
skip = round(tot / 10);

disp('Computing eigendecomposition...')

tensor = zeros(3,3);
eigfield = zeros(sx, sy, sz, 3, 3);
eigvfield = zeros(sx, sy, sz, 3);
tic
it = 0;
for x=1:sx
    for y=1:sy
        for z=1:sz
            it = it + 1;
            
            if (mod(it, skip) == 0)
                fprintf('Voxel element %i/%i, %3.0f%% completed\n', it, sx*sy*sz, 100 * it / tot);
            end

            % Extract 6 components
            tensorEls = tensorVolume(x, y, z, :);
            tensor(1,1) = tensorEls(1);
            tensor(1,2) = tensorEls(2);
            tensor(1,3) = tensorEls(3);
            
            tensor(2,2) = tensorEls(4);
            tensor(2,3) = tensorEls(5);
            tensor(3,3) = tensorEls(6);
            
            tensor(2,1) = tensor(1,2);
            tensor(3,1) = tensor(1,3);
            tensor(3,2) = tensor(2,3);
            
            if (norm(tensor, 1) < 1e-8)
                continue
            end
            
            [v,d] = eigs(tensor);
            
            eigfield(x, y, z, 1, :) = v(:, 1);
            eigfield(x, y, z, 2, :) = v(:, 2);
            eigfield(x, y, z, 3, :) = v(:, 3);
            eigvfield(x, y, z, :) = diag(d);
            
        end
    end
end

varargout{1} = eigfield;
varargout{2} = eigvfield;
toc