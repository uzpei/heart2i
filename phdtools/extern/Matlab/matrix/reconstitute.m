function Vout = reconstitute(Vin, mask)
Vout = zeros(size(mask));
Vout(mask) = Vin(:);