clear all
fprintf('Loading data...\n');
tic
dirdata = '../../data/heart/rats/rathearts.mat';
hearts = miccai_preprocess(loadmat(dirdata));
toc

subjects = fieldnames(hearts);
subject = subjects{1};
heart = hearts.(subject);

dirout = './output/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

measurements = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
clims = { [-0.1, 0.1], [-0.1, 0.1], [-0.5, 0.5], [-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1] };

%% 

zcut = [0, 20];
imethod = 'nearest';
useExtrapolation = false;

% Compute partial volume bounds
mask = heart.mask;
[sx, sy, sz] = size(mask);
zbounds = zbox(heart.mask, 15) - zcut;
zdiff = zbounds(2) - zbounds(1);
maskBound = ones(sx, sy, sz);
maskBound(:,:,1:zbounds(1)-1) = 0;
maskBound(:,:,(zbounds(2)+1):end) = 0;
mask = mask & maskBound;
mask3 = repmat(mask, [1 1 1 3]);

% Prepare interpolation
T = heart.T;
Tx = T(:,:,:,1);
Ty = T(:,:,:,2);
Tz = T(:,:,:,3);

image3d(T);
colormap(customap('redwhiteblue'))
title('Ground truth');
export_fig([dirout, 'interp_ground'], '-transparent', '-q101');

B_ground = estimate_wall_normals(mask);

%%
zerrors = {};
for zn=2:20
zinds = round(linspace(zbounds(1),zbounds(2), zn));

% Compute partial mask
interpolantMask = zeros(size(mask));
interpolantMask(:,:,zinds) = mask(:,:,zinds);
interpolantMask = logical(interpolantMask);

% Compute interpolation mask
interpolationMask = ~interpolantMask & mask;

figure(1)
image3d(interpolantMask + 2 * interpolationMask)
supertitle('Interpolation');
colormap(customap('redwhiteblue'))

%% Interpolate Tx, Ty, Tz, components
fprintf('Reconstructing from direction interpolation...\n');
useExtrapolation = false;
imethod = 'linear';
Txc = interpolatevolumemask(Tx, interpolantMask, interpolationMask, imethod, useExtrapolation);
Tyc = interpolatevolumemask(Ty, interpolantMask, interpolationMask, imethod, useExtrapolation);
Tzc = interpolatevolumemask(Tz, interpolantMask, interpolationMask, imethod, useExtrapolation);

Tc = assemble(Txc, Tyc, Tzc);
Tc = normalize(Tc);
Tc(~mask3) = 0;

figure(2)
image3d(Tc);
colormap(customap('redwhiteblue'))
terror = norm(T(:) - Tc(:), 2);
title(sprintf('Elements: E = %.2f', terror));
export_fig([dirout, 'interp_dir', num2str(useExtrapolation)], '-transparent', '-q101');

% Compute connections forms
connections.dinterp = miccai2013_compute_connections(Tc, B_ground);


% Interpolate connection forms
fprintf('Reconstructing connection forms...\n');

for cind=1:numel(measurements)
    connections.cinterp.(measurements{cind}) = interpolatevolumemask(heart.(measurements{cind}), interpolantMask, interpolationMask, imethod, useExtrapolation);
    connections.cinterp.(measurements{cind})(isnan(connections.cinterp.(measurements{cind}))) = 0;
end

%% Compare both
zerror.(['z', num2str(zn)]) = [];
for cind=1:numel(measurements)
    subplot(2,6,cind)
    ddiff = heart.(measurements{cind}) - connections.dinterp.(measurements{cind});
    image3dslice(connections.dinterp.(measurements{cind}), 'x');
    caxis(clims{cind});
    text_location(sprintf('%.3f', norm(ddiff(:),1) / numel(find(interpolationMask))), 'northeast');
    text_location(sprintf('%.3f', norm(heart.(measurements{cind})(:),1) / numel(find(interpolationMask))), 'southeast');

    subplot(2,6,6+cind)
    cdiff = heart.(measurements{cind}) - connections.cinterp.(measurements{cind});
    image3dslice(connections.cinterp.(measurements{cind}), 'x');
    caxis(clims{cind});
    text_location(sprintf('%.3f', norm(cdiff(:),1) / numel(find(interpolationMask))), 'northeast');
    text_location(sprintf('%.3f', norm(heart.(measurements{cind})(:),1) / numel(find(interpolationMask))), 'southeast');
    zerror.(['z', num2str(zn)]) = [zerror.(['z', num2str(zn)]); [norm(ddiff(:),1) / numel(find(interpolationMask)), norm(cdiff(:),1) / numel(find(interpolationMask))]];
end
export_fig([dirout, 'interp_', num2str(zn), '.pdf'], '-transparent', '-q101');
%pause()
end

%% Output plot
derrors = [];
cerrors = [];
for zn=2:20
   derrors = [derrors, zerror.(['z', num2str(zn)])(3, 1)];
   cerrors = [cerrors, zerror.(['z', num2str(zn)])(3, 2)];
end
figure(1)
clf
hold on
plot(derrors, 'ro-');
plot(cerrors, 'bo-');
hold off
legend({'component', 'connection'});
export_fig([dirout, 'interp.pdf'], '-transparent', '-q101');

%% Interpolate spherical coordinates
% [azimuth,elevation,r] = cart2sph(Tx,Ty,Tz);
[theta,rho,z] = cart2pol(Tx,Ty,Tz);

% map [-pi, pi] to [0, pi]
%azimuth(azimuth < 0) = pi + azimuth(azimuth < 0);

fprintf('Reconstructing from spherical interpolation...\n');
% interpolant_azimuth = interpolatevolumemask(azimuth, interpolantMask, interpolationMask, imethod, useExtrapolation);
% interpolant_elevation = interpolatevolumemask(elevation, interpolantMask, interpolationMask, imethod, useExtrapolation);
% [Txs,Tys,Tzs] = sph2cart(interpolant_azimuth,interpolant_elevation,ones(size(interpolant_azimuth)));
itheta = interpolatevolumemask(theta, interpolantMask, interpolationMask, imethod, useExtrapolation);
iz = interpolatevolumemask(z, interpolantMask, interpolationMask, imethod, useExtrapolation);
[Txs,Tys,Tzs] = pol2cart(itheta,ones(size(iz)), iz);
%[Txs,Tys,Tzs] = pol2cart(theta,ones(size(z)), z);

Ts = assemble(Txs, Tys, Tzs);
Ts = normalize(Ts);
Ts(~mask3) = 0;

figure(2)
image3d(Ts);
colormap(customap('redwhiteblue'))
terror = norm(T(:) - Ts(:), 2);
title(sprintf('Spherical: E = %.2f', terror));

figure(3)
image3d(azimuth);
colormap(customap('redwhiteblue'))

%% Interpolate with extrapolation
fprintf('Reconstructing from direction interpolation...\n');
Txc = interpolatevolumemask(Tx, interpolantMask, interpolationMask, imethod, true);
Tyc = interpolatevolumemask(Ty, interpolantMask, interpolationMask, imethod, true);
Tzc = interpolatevolumemask(Tz, interpolantMask, interpolationMask, imethod, true);

Tc = assemble(Txc, Tyc, Tzc);
Tc = normalize(Tc);
Tc(~mask3) = 0;

figure(3)
image3d(Tc);
colormap(customap('redwhiteblue'))
terror = norm(T(:) - Tc(:), 2);
title(sprintf('Elements (extr): E = %.2f', terror));

%%
figure(4)
image3d(T)
colormap(customap('redwhiteblue'))


% use interp3
% [azimuth,elevation,r] = cart2sph(Tx,Ty,Tz);
% 
% fprintf('Reconstructing from spherical interpolation...\n');
% % [X, Y, Z] = meshgrid(1:sx, 1:sy, zbounds(1):zbounds(2));
% [X, Y, Z] = meshgrid(1:sx, 1:sy, 1:sz);
% Xi = X;
% Yi = Y;
% Zi = Z;
% 
% %
% interp_inds = 1 + zinds - zbounds(1);
% X = X(:,:,zinds);
% Y = Y(:,:,zinds);
% Z = Z(:,:,zinds);
% interpolant_azimuth = azimuth(:,:,zinds);
% interpolant_elevation = elevation(:,:,zinds);
% 
% %
% interpolated_azimuth = interp3(X, Y, Z, interpolant_azimuth, Xi, Yi, Zi, imethod);
% interpolated_elevation = interp3(X, Y, Z, interpolant_elevation, Xi, Yi, Zi, imethod);
% 
% image3d(interpolated_azimuth);
% 
% %%
% [Txs,Tys,Tzs] = sph2cart(interpolated_azimuth,interpolated_elevation,ones(size(interpolant_azimuth)));
% 
% Ts = assemble(Txs, Tys, Tzs);
% Ts = normalize(Ts);
% %Ts(~mask3) = 0;
% 
% figure(1)
% image3d(Ts);
% colormap(customap('redwhiteblue'))
% %terror = norm(T(:) - Ts(:), 2);
% %title(sprintf('Spherical: E = %.2f', terror));

%% Run interpolation experiment
zn = 2;
zinds = linspace(30, 80, zn);
mask2 = zeros(sx, sy, sz);
mask2(:,:,zinds) = mask(:,:,zinds);
mask2 = logical(mask2);

[Xi, Yi, Zi] = meshgrid(1:sx, 1:sy, 1:sz);
Vx = Tx;
Vy = Ty;
Vz = Tz;

x = Xi(mask2);
y = Yi(mask2);
z = Zi(mask2);
vx = Vx(mask2);
vy = Vy(mask2);
vz = Vz(mask2);

%%
rconstant = 5;
rfunc = 'multiquadric';
mtic = tic;
vrbf = rbfcreate([x'; y'; z'], vx','RBFFunction', rfunc);
% vrbf = rbfcreate([x'; y'; z'], v','RBFFunction', rfunc, 'RBFConstant', rconstant);
Vivx = rbfinterp([Xi(:)'; Yi(:)'; Zi(:)'], vrbf);
Vix = reshape(Vivx, size(Xi));
toc(mtic)

%% Experiment 2 (shell by shell, slab by slab interpolation)
zcut = 80;
all_mask_shell = miccai2013_all_shells(mask, zcut);
%image3d(mask_shell == 1)

%%

zn = 4;
zinds = linspace(28, 70, zn);

%%
maskOuter = ~miccai2013_shell_mask(mask, 3, 90);
dthresh = 0.5;

% Run once to get dtransform
[shell, dtransform] = miccai2013_shell(maskOuter, 0, 0, 0);

%
Txi = zeros(sx, sy, sz);
Tyi = zeros(sx, sy, sz);
Tzi = zeros(sx, sy, sz);
%slab_bounds = [28, 70];
cumshells = zeros(size(mask));
%for zi=1:length(zinds)-1
for zi=1
% slab_bounds = [zinds(zi), zinds(zi+1)];
% slab_inds = [slab_bounds(1),slab_bounds(2)];
% slab_intra_inds = (slab_bounds(1)):(slab_bounds(2));
% mask_interpolant_full = zeros(sx, sy, sz);
% mask_interpolant_full(:,:,slab_inds) = mask(:,:,slab_inds);
% mask_interpolation_full = zeros(sx, sy, sz);
% mask_interpolation_full(:,:,slab_intra_inds) = mask(:,:,slab_intra_inds);
mask_interpolant_full = zeros(sx, sy, sz);
mask_interpolation_full = zeros(sx, sy, sz);
mask_interpolant_full(:,:,zinds) = mask(:,:,zinds);
mask_interpolation_full(:,:,zinds(1):zinds(end)) = mask(:,:,zinds(1):zinds(end));

for shell_index=1:15
    fprintf('Processing shell %d...\n', shell_index);
    [shell, dtransform] = miccai2013_shell(maskOuter, shell_index, dthresh, 0, dtransform);

    % Interpolant mask
    mask_shell = zeros(sx, sy, sz);
    mask_shell(shell) = shell_index;
    mask_interpolant = mask_interpolant_full & (mask_shell == shell_index);
    mask_interpolation = mask_interpolation_full & (mask_shell == shell_index);

    cumshells(mask_interpolation) = cumshells(mask_interpolation) + 1;
    
%     image3d(mask_shell)
%     pause(0.1)
    Txi = Txi + miccai2013_interpolate_shell_slab(Tx, mask_interpolant, mask_interpolation);
    Tyi = Tyi + miccai2013_interpolate_shell_slab(Ty, mask_interpolant, mask_interpolation);
    Tzi = Tzi + miccai2013_interpolate_shell_slab(Tz, mask_interpolant, mask_interpolation);
    
    subplot(121)
    % Show interpolants
    image3dslice(mask_interpolant, vaxis);
%    image3dslice(mask_shell, vaxis);
    caxis([-1, 1])

    subplot(122)
    % Show iterative reconstruction
    image3dslice(Txi, vaxis);
    caxis([-1, 1])
    pause(0.1)

end

cumshells(cumshells == 0) = 1;
Txi = Txi ./ cumshells;
Tyi = Tyi ./ cumshells;
Tzi = Tzi ./ cumshells;

%%
vaxis = 'y';
subplot(121)
image3dslice(Txi, vaxis);
caxis([-1, 1])
subplot(122)
image3dslice(Tx, vaxis);
caxis([-1, 1])

end

[Txi, Tyi, Tzi] = normalize(Txi, Tyi, Tzi);

%%
% Smooth tx
% gsize = 3;
% h = fspecial3('gaussian',[gsize, gsize, gsize]); 
% Txis = imfilter(Txi,h,'replicate');
% Tyis = imfilter(Tyi,h,'replicate');
% Tzis = imfilter(Tzi,h,'replicate');
% Txis(~mask) = 0;
% Tyis(~mask) = 0;
% Tzis(~mask) = 0;

%%
figure(1)
vaxis = 'y';
slice = 41;
subplot(121)
image3dslice(Txi, vaxis, slice);
caxis([-1, 1])
subplot(122)
image3dslice(Tx, vaxis, slice);
caxis([-1, 1])
