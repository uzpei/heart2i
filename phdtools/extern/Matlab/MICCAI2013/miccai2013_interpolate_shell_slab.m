function VI = miccai2013_interpolate_shell_slab(V, mask_interpolant, mask_interpolation)

[sx, sy, sz] = size(mask_interpolant);
[XI, YI, ZI] = meshgrid(1:sx, 1:sy, 1:sz);
%slab_inds = [slab_bounds(1),slab_bounds(2)];
%slab_intra_inds = (1+slab_bounds(1)):(slab_bounds(2)-1);

%% Compute mask of interpolant points
%mask_slabs = zeros(sx, sy, sz);
%mask_slabs(:,:,slab_inds) = mask(:,:,slab_inds);
%mask_interpolant = mask_slabs & (mask_shell == shell);

if (numel(find(mask_interpolant)) == 0)
    VI = zeros(sx, sy, sz);
    return
end

%% Compute mask of interpolation points
%mask_slabs = zeros(sx, sy, sz);
%mask_slabs(:,:,slab_intra_inds) = mask(:,:,slab_intra_inds);
%mask_interpolation = mask_slabs & (mask_shell == shell);

xi = XI(mask_interpolant);
yi = YI(mask_interpolant);
zi = ZI(mask_interpolant);
v = V(mask_interpolant);

xp = XI(mask_interpolation);
yp = YI(mask_interpolation);
zp = ZI(mask_interpolation);

rfunc = 'linear';
%rconstant = slab_bounds(2)-slab_bounds(1);

%mtic = tic;
vrbf = rbfcreate([xi'; yi'; zi'], v','RBFFunction', rfunc);
% vrbf = rbfcreate([xi'; yi'; zi'], v','RBFFunction', rfunc,'RBFSmooth', 5);
%vrbf = rbfcreate([xi'; yi'; zi'], v','RBFFunction', rfunc, 'RBFConstant', rconstant);
vp = rbfinterp([xp'; yp'; zp'], vrbf);
%VI = reshape(vp, size(mask));
VI = reconstitute(vp, mask_interpolation);
VI(isnan(VI)) = 0;
%VI(mask_interpolant) = V(mask_interpolant);
%toc(mtic)
