function miccai2013_form_output(mask, original, form, pca, interp, clbl, faxis, crange)

%% Compare methods
% clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
% cind = 3;
% clbl = clabels{cind};
% faxis = 'x';

%crange = [-0.5 0.5];
cijk = original.(clbl);
cijk(~mask) = 0;

subplot(241)
myimage(cijk,crange,'',faxis);
title('original')
mytext(sprintf('L^2 = %.2f',norm(cijk(:),2)));

subplot(242)
myimage(form.(clbl),crange,'',faxis);

subplot(243)
myimage(pca.(clbl),crange,'',faxis);

subplot(244)
myimage(interp.(clbl),crange,'',faxis);

%subplot(245)
% myimage(cijk,crange,'original',faxis);

subplot(246)
vdiff = abs(form.(clbl) - cijk);
myimage(vdiff,crange,'pca-one-form',faxis);
mytext(sprintf('E = %.2f',norm(vdiff(:),2)));

subplot(247)
vdiff = abs(pca.(clbl) - cijk);
myimage(vdiff,crange,'pca-direction',faxis);
mytext(sprintf('E = %.2f',norm(vdiff(:),2)));

subplot(248)
vdiff = abs(interp.(clbl) - cijk);
myimage(vdiff,crange,'interpolation-direction',faxis);
mytext(sprintf('E = %.2f',norm(vdiff(:),2)));

supertitle(sprintf('%s reconstruction (top) and error (bottom)', clbl));

end

function myimage(img, crange, title_text, faxis)
image3dslice(img, faxis);
caxis(crange)
axis image
title(title_text)
axis off
end

function mytext(some_text)
    alims = axis;
    text(0.05* (alims(2)-alims(1)), 0.05* (alims(4)-alims(3)), some_text);
end
