%% Use image flood fill to look only at outer volume
function smask = miccai2013_shell_mask(mask, closeSize, zcut)

% Remove top slices
maskTight = mask;
maskTight(:,:,zcut:end) = 1;

padSize = 2 * closeSize;
maskTight = padarray(maskTight,[padSize padSize padSize]);

% maskTight = imdilate(logical(maskTight), strel_ball(1));
% maskTight = imdilate(maskTight, strel_ball(2));
maskTight = imclose(maskTight, strel_ball(closeSize));

smask = ~imfill(double(maskTight), 'holes');

% Determine the new volume dimension
border = size(smask) - size(mask);
% Remove garbage in corners
zeroPadSize = border / 2;
smask = smask(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));
