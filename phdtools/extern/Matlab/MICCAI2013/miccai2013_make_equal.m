function equalHearts = miccai2013_make_equal(hearts, measurements, reconSubject, trainingIndices)

subjects = fieldnames(hearts);

test_heart = hearts.(subjects{reconSubject});
isize = size(test_heart.mask);
[sx, sy, sz] = size(test_heart.mask);

mask = test_heart.mask;

% Fill missing parts for each heart
equalHearts.(subjects{reconSubject}) = hearts.(subjects{reconSubject});

% measurements
% class(measurements)

for measurement=measurements
    measurement = char(measurement);

    % First compute mean
    for i=1:numel(trainingIndices)
        heart = hearts.(subjects{trainingIndices(i)});
        X(:,i) = heart.(measurement)(mask);
    end
    
    rmean = reconstitute(mean(X, 2), mask);

    for i=1:numel(trainingIndices)
        heart = hearts.(subjects{trainingIndices(i)});
        hmask = heart.mask;

        % Fill missing voxels with mean
        equalHearts.(subjects{trainingIndices(i)}).(measurement) = hearts.(subjects{trainingIndices(i)}).(measurement);
        equalHearts.(subjects{trainingIndices(i)}).(measurement)(~hmask & mask) = rmean(~hmask & mask); 
    end

end