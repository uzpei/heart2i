clear all

fprintf('Loading data...\n');
tic
dirdata = '../../data/heart/rats/rathearts.mat';
hearts = loadmat(dirdata);
toc

dirout = './output/interpolation/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

reconSubject = 1;
subjects = fieldnames(hearts);

heart = hearts.(subjects{1});
[sx, sy, sz] = size(heart.mask);

trainingIndices = 1:length(subjects)-1;
trainingIndices(reconSubject) = [];
trainingSize = length(trainingIndices);

hearts = miccai_preprocess(hearts);

%% Compute PCA
testheart = hearts.(char(subjects(reconSubject)));
testheart.name = char(subjects(reconSubject));
[sx, sy, sz] = size(testheart.mask);

% clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
clabels = { 'Tx', 'Ty', 'Tz', 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};

fprintf('Computing full PCA...\n');
tic

pmask = testheart.mask;
%pmask(:,:, [1:50, 70:end]) = 0;
%pmask(:, 40:end, :) = 0;

pmask2 = rand(sx, sy, sz) > 0.8;
%pmask2 = pmask;

% Test change in weighting
% for cind=1:numel(clabels)
nu = 0;
for cind=6
    measurement = clabels{cind};
    [recon, V, W, rmean ] = miccai_pca(hearts, pmask, trainingIndices, reconSubject, measurement, pmask2, nu);
    W'
    cmean{cind} = rmean;
    heartPCA.([clabels{cind}]) = recon;
end

toc

miccai_pca_result_comparator(testheart.TNB, heartPCA.TNB, cmean{6}, pmask, [-0.5, 0.5]);

%% Experiment with gridding
%sn = 150;
%vgrid = subdivide(zeros(sn, sn, sn), 2);
%[vgrid, grid_inds] = subdivide(mask, [3, 3, 3]);
%[vgrid, grid_inds] = subdivide(mask, [1, 1, 1]);
%[vgrid, inds] = volumeregions3d(testheart.mask, 3, 4);
%grid_inds = [inds(1), inds(end)];

[vgrid, grid_inds] = subdivide(mask, [1, 1, 3]);
image3d(vgrid);
colormap jet

% For each grid cell
clear heartPCA
for grid_index = grid_inds(1):grid_inds(2)
%for grid_index = grid_inds(1)+1:grid_inds(1)+1
    mask = testheart.mask & vgrid == grid_index;

    nu = 0;
    
    % Compute PCA within this cell for each connection form
    fprintf('Computing %d PCA for r%d\n', length(clabels), grid_index);
    for cind=1:numel(clabels)
%        dataRecon = miccai_pca(hearts, trainingIndices, reconSubject, clabels{cind}, mask, nu);
        [dataRecon, V, W, rmean] = miccai_pca(hearts, pmask, trainingIndices, reconSubject, measurement, pmask2, nu);

        heartPCA.([clabels{cind}]).(['r', num2str(grid_index)]) = dataRecon;
        heartPCA.([clabels{cind}]).(['m', num2str(grid_index)]) = rmean;
    end
    
end

%% Show comparison for each region
ny = range(vgrid(:)) + 1;
for grid_index = grid_inds(1):grid_inds(2)
    gi = 1 + grid_index - grid_inds(1);
    
    % Only look at TNB for now
    measurement = clabels{6};

    maskel = vgrid == grid_index;
    mask = testheart.mask & maskel;

    subplot(2, ny, gi)
     cdata = heartPCA.([clabels{cind}]).(['r', num2str(grid_index)]);
     image3dslice(cdata, 'x');
%    image3dslice(mask, 'z');
    caxis([-0.5, 0.5]);
    axis off

    subplot(2, ny, ny + gi)
    test_data = testheart.(measurement);
    test_data(~mask) = 0;
    image3dslice(test_data, 'x');
%    image3dslice(mask, 'z');
    caxis([-0.5, 0.5]);
    axis off
    
%    pause(0.1)
end