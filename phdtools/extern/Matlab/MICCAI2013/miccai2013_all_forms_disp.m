function miccai2013_all_forms_disp(currentMeasure, dataRecon, dataReconMean, ni, nj, clims, vs)

dataReconDiff = dataRecon - dataReconMean;

figure(1)
subplot(ni,nj,2*(cind-1) + 1)
image3dslice(currentMeasure, vs);
caxis(clims);
axis image
axis off
title(sprintf('%s',clbl));

subplot(ni,nj,2*(cind-1) + 2)
image3dslice(dataRecon, vs);
caxis(clims);
title(sprintf('%s-PCA',clbl));
axis image
axis off
colormap(customap('redwhiteblue'))

figure(2)
subplot(ni,nj,2*(cind-1) + 1)
image3dslice(dataReconDiff, vs);
caxis(clims);
axis image
axis off
title(sprintf('%s-mean',clbl));

subplot(ni,nj,2*(cind-1) + 2)
image3dslice(dataReconMean, vs);
caxis(clims);
title(sprintf('Mean %s', clbl));
axis image
axis off
colormap(customap('redwhiteblue'))
