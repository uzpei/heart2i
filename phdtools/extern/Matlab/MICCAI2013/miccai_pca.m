function [projected, eigenmatrix, W, tmean, evalues] = miccai_pca(hearts, interpolantMask, interpolationMask, trainingIndices, testIndex, measurement, varargin)

    % First make all hearts fit in the same mask
    hearts = miccai2013_make_equal(hearts, { measurement }, testIndex, trainingIndices);

    % This is the full volume we are trying to reconstruct
    % it contains both interpolant and interpolated slices
    mask = interpolationMask | interpolantMask;

    subjects = fieldnames(hearts);

    testheart = hearts.(subjects{testIndex});

    [sx, sy, sz] = size(mask);

    if (nargin == 7)
        nu = varargin{1};
    else
        nu = 0;
    end
    
    n = numel(find(mask));
    X = zeros(n, numel(trainingIndices));

    % Smoothing params
%    gsize = 3;
%    h = fspecial3('gaussian',[gsize, gsize, gsize]); 
    
    % Compute data matrix X
    for i=1:numel(trainingIndices)
        heart = hearts.(subjects{trainingIndices(i)});
%         ms = imfilter(heart.(measurement),h,'replicate');
%         X(:,i) = ms(mask);
       X(:,i) = heart.(measurement)(mask);
    end

    % Compute eigenmodes
    [eigenmatrix, evalues, Xmean] = eigendecompose(X);

    % Compute reconstruction and difference
%     ms = imfilter(testheart.(measurement),h,'replicate');
%     currentMeasure = ms(mask);
    currentMeasure = testheart.(measurement)(mask);

    % Lmap is a linear mask which is
    % 1: where the full mask hits interpolant points
    % 0: elsewhere
    partialLmap = zeros(size(mask));
    partialLmap(interpolantMask) = 1;
    partialLmap = logical(partialLmap(mask));
    [xp, W] = pcaproject(X, Xmean, currentMeasure, eigenmatrix, evalues, nu, partialLmap);

        % Reconstruct projection
    if (numel(xp) == 0)
        projected = [];
        tmean = reconstitute(Xmean, mask);
        return
    end
    
    projected = reconstitute(xp, mask);
    tmean = reconstitute(Xmean, mask);
    
%     %% Run some dev tests
%     x = currentMeasure;
% %     x = x - Xmean;
% %    norm(Xmean(:), 2)
% %    x = x(partialLmap) - Xmean(partialLmap);
%     x = x(partialLmap);
%     
% %    zslice = 54;
%      zslice = 67;
% %    zslice = 32;
%     xr = reconstitute(x, partialLmap);
%     xr = reconstitute(xr, mask);
%     subplot(121)
%     image3dslice(xr, 'z', zslice);
%     customap('redwhiteblue');
%     caxis([-0.5, 0.5]);
%     subplot(122)
% %     gtruth = testheart.(measurement);
% %     gtruth(~interpolantMask) = 0;
% %     image3dslice(gtruth, 'z', zslice);
% %    image3dslice(reconstitute(Xmean, mask), 'z', zslice);
% %    image3dslice(reconstitute(partialLmap, mask), 'z', zslice);
% %     image3dslice(xr - reconstitute(Xmean, mask), 'z', zslice);
% %    zbox(reconstitute(partialLmap, mask))
%     image3dslice(reconstitute(xp, mask), 'z');
%     colormap(customap('redwhiteblue'));
%     caxis([-0.5, 0.5]);
% %    caxis auto
%     text_location(num2str(norm(projected(:) - xr(:), 2)), 'northwest');
%     pause(0.5)


end
