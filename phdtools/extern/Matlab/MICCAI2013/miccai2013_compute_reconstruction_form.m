function heartPCA = miccai2013_compute_reconstruction_form(hearts, measurements, trainingIndices, testIndex, interpolantMask, interpolationMask)

%% Compute PCA
for mind=1:numel(measurements)

    measurement = measurements{mind};
    
    [dataRecon, V, W, dataReconMean, evalues] = miccai_pca(hearts, interpolantMask, interpolationMask, trainingIndices, testIndex, measurement);

    heartPCA.(measurement) = dataRecon;
    heartPCA.([measurement, '_mean']) = dataReconMean;
    heartPCA.([measurement, '_weights']) = W;
    heartPCA.([measurement, '_evalues']) = evalues;
end
