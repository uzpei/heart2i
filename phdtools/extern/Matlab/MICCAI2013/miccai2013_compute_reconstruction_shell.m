function heartPCA = miccai2013_compute_reconstruction_shell(hearts, trainingIndices, reconSubject, dthresh, interpolantMask, interpolationMask, measurements, invalidBounds, z_top)

subjects = fieldnames(hearts);
%measurements = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};

test_heart = hearts.(subjects{reconSubject});
isize = size(test_heart.mask);
[sx, sy, sz] = size(test_heart.mask);


%% Compute volume mask
% Either use the mask from the test heart (uncomment below) or use a common mask
% that is valid for all hearts. In this case voxels that are not within the
% mask of systematically *all* hearts will be left out. For this to work
% hearts need to be properly registered to one another (preferably
% non-linearly)

maskVolume = test_heart.mask;
% maskVolume = ones(isize);
% for i=[reconSubject, trainingIndices]
%     subj = subjects{i};
%     maskVolume = hearts.(subj).mask & maskVolume;
% end

% image3dslice(maskVolume, 'y');
% pause(1)

maskBound = maskVolume;

% TODO: pass both bottom zcut and upper zcut as arguments.
maskBound(:,:,invalidBounds) = 0;

% TODO: the '90' magical number is the z-index at which it is safe to 
% compute an outer mask. Otherwise the flood fill algorithm will leak
% inside the ventricle.
max(invalidBounds)
maskOuter = ~miccai2013_shell_mask(maskBound, 3, z_top);

% Preprocess 
heartPCA.shell_projected = {};
heartPCA.shell_mean = {};
for measurement = measurements
    measurement = char(measurement);
    heartPCA.shell_projected.(measurement) = zeros(size(test_heart.mask));
    heartPCA.shell_mean.(measurement) = zeros(size(test_heart.mask));
end

allshells = zeros(size(maskBound));
cumshells = zeros(size(maskBound));

shell_index = 1;
fprintf('PCA computations for shell ');
perrors = [];
while (true && shell_index < max(isize))
%while (shell_index < 4)
%        dthresh = 0.5;
    shell = miccai2013_shell(maskOuter, shell_index, dthresh, 0);

    maskShell = zeros(isize);
    maskShell(shell) = 1;
    maskShell(~maskVolume) = 0;
    maskShell = logical(maskShell);
    maskShell(:,:,invalidBounds) = 0;
    
    shell_count = numel(find(maskShell));

    fprintf('%i (%i voxels) ', shell_index, shell_count);

    if (shell_count == 0)
        break
    end

    allshells(maskShell) = shell_index;
    cumshells(maskShell) = cumshells(maskShell) + 1;

%    image3dslice(maskShell, 'x');
%     image3dslice(interpolantMask & maskShell, 'x');
%     image3dslice(interpolationMask & maskShell, 'x');
%     pause(0.5)

%        fprintf('Computing PCA...\n');
    
    for measurement=measurements
        measurement = char(measurement);
        
%         [dataRecon, V, W, rmean] = miccai_pca(hearts, interpolantMask & maskShell, interpolationMask & maskShell, trainingIndices, reconSubject, measurement);
%         % Fill missing parts for each heart
%         % First compute mean
%         heartsFilled.(subjects{reconSubject}) = hearts.(subjects{reconSubject});
%         for i=1:numel(trainingIndices)
%             heart = hearts.(subjects{trainingIndices(i)});
%             hmask = heart.mask;
% 
%             % Fill missing voxels with mean
%             heartsFilled.(subjects{trainingIndices(i)}).(measurement) = hearts.(subjects{trainingIndices(i)}).(measurement);
%             heartsFilled.(subjects{trainingIndices(i)}).(measurement)(~hmask & maskVolume) = rmean(~hmask & maskVolume); 
%         end
%         heartsFilled = miccai2013_make_equal(hearts, measurements, reconSubject, trainingIndices);
        
%         [dataRecon, V, W, rmean] = miccai_pca(heartsFilled, interpolantMask & maskShell, interpolationMask & maskShell, trainingIndices, reconSubject, measurement);
        [dataRecon, V, W, rmean] = miccai_pca(hearts, interpolantMask & maskShell, interpolationMask & maskShell, trainingIndices, reconSubject, measurement);


        if (isempty(dataRecon))
            continue
        end
        
        % Run some tests
        mask = (interpolantMask | interpolationMask) & maskShell;
        partialLmap = zeros(size(mask));
        partialLmap(interpolantMask) = 1;
        partialLmap = logical(partialLmap(mask));

        xr = test_heart.(measurement)(mask);
        xr = xr(partialLmap);
        xr = reconstitute(xr, partialLmap);
        xr = reconstitute(xr, mask);
        perror = norm(xr(:) - dataRecon(:), 2) / numel(find(interpolationMask & maskShell));
        perrors = [perrors, perror];

        % Store result
        heartPCA.shell_projected.(measurement)(maskShell) = heartPCA.shell_projected.(measurement)(maskShell) + dataRecon(maskShell);
        heartPCA.shell_mean.(measurement)(maskShell) = heartPCA.shell_mean.(measurement)(maskShell) + rmean(maskShell);
        
        
        image3dslice(heartPCA.shell_projected.(measurement), 'x');
        colormap(customap('redwhiteblue'));
        caxis([-0.5, 0.5]);
        axis image
        pause(0.1)
    end

    shell_index = shell_index  + 1;
end
fprintf('\n');
%perrors
norm(perrors(:),2)

heartPCA.shells = allshells;

% Make background voxels 1 to prevent NaNs
cumshells(cumshells == 0) = 1;

% Normalize using shell counting
for measurement=measurements
    measurement = char(measurement);
    heartPCA.shell_projected.(measurement) = heartPCA.shell_projected.(measurement) ./ cumshells;
    heartPCA.shell_mean.(measurement) = heartPCA.shell_mean.(measurement) ./ cumshells;
end

% image3d(allshells);
% pause(0.1)
%image3d(cumshells);

% Remember: this will be a full volume of 1's if shells are 1-unit thick
%image3d(cumshells);
%pause(0.5)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute shell-by-shell natural masking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nu = 0;
% 
% for reconSubject=1:8
% 
%     test_heart = hearts.(subjects{reconSubject});
%     maskBound = test_heart.mask;
%     maskBound(:,:,zcut) = 0;
% 
%     trainingIndices = 1:length(subjects)-1;
%     trainingIndices(reconSubject) = [];
%     trainingSize = length(trainingIndices);
% 
% %    for cind=1:numel(measurements)
%     for cind=3
%         heartPCA.(measurements{cind}).shell_projected_adaptive = zeros(sx, sy, sz);
%         heartPCA.(measurements{cind}).shell_mean_adaptive = zeros(sx, sy, sz);
%         
%         shell_index = 1;
%         while(true)
%             fprintf('Shell index %d\n', shell_index);
%             shell_mask = test_heart.maskShell == shell_index;
% %             image3d(shell_mask);
% %             pause(0.5)
% 
%             [projected, eigenmatrix, W, tmean] = miccai_pca_from_shell(hearts, trainingIndices, reconSubject, measurements{cind}, shell_index, nu);
% 
%             if (isempty(projected))
%                 break;
%             end
% 
% %            testv = zeros(sx, sy, sz);
% %            testv(shell_mask) = projected(shell_mask);
% %            image3d(testv);
% %            image3d(heartPCA.(measurements{cind}).shell_projected_adaptive);
% %            pause(0.5);
%             
%             heartPCA.(measurements{cind}).shell_projected_adaptive(shell_mask) = projected(shell_mask);
%             heartPCA.(measurements{cind}).shell_mean_adaptive(shell_mask) = tmean(shell_mask);
% 
%             shell_index = shell_index + 1;
%         end
%         %image3d(heartPCA.(measurements{cind}).shell_projected_adaptive)
%         miccai_pca_result_comparator(test_heart.(measurements{cind}), heartPCA.(measurements{cind}).shell_projected_adaptive, heartPCA.(measurements{cind}).shell_mean_adaptive, maskBound, clims{cind}, vaxis)
%         colormap(customap('redwhiteblue'))
%         export_fig([dirout, subjects{reconSubject}, '_', measurements{cind}, '_pca_shell_adaptive', '.pdf'], '-transparent', '-q101');
%     end
% end
% 
