%% Tests
subject = subjects(8);
imagevectormask(subject.e1, subject.mask, 'z', 30, 1);
axis off
colormap gray
ext = 'pdf';
export_fig(['./output/', 'miccai1.', ext], '-transparent', '-q101');
imagevectormask(subject.e1, subject.mask, 'z', 30, 3);
axis off
colormap gray
export_fig(['./output/', 'miccai2.',ext], '-transparent', '-q101');


