function miccai_pca_result_comparator(groundTruth, pca, eigmean, pmask, clims, vaxis)

n = numel(find(pmask));

pca(~pmask) = 0;
eigmean(~pmask) = 0;


% Display some results
%vaxis = 'x';
subplot(231)
image3dslice(groundTruth, vaxis);
title('original')
colormap(customap('redwhiteblue'))
caxis(clims)
text_location(num2str(norm(groundTruth(pmask), 1) / n), 'northeast');

subplot(232)
image3dslice(pca, vaxis);
title('projection')
colormap(customap('redwhiteblue'))
caxis(clims)
text_location(num2str(norm(pca(pmask), 1) / n), 'northeast');

subplot(233)
image3dslice(eigmean, vaxis);
title('mean')
colormap(customap('redwhiteblue'))
caxis(clims)
text_location(num2str(norm(eigmean(pmask), 1) / n), 'northeast');

subplot(234)
image3dslice((pca - eigmean), vaxis);
title('delta to mean')
colormap(customap('redwhiteblue'))
caxis(clims)
text_location(num2str(norm(pca(pmask) - eigmean(pmask), 1) / n), 'northeast');

subplot(235)
cdiff = pca - groundTruth .* pmask;
image3dslice(cdiff, vaxis);
title('error (projection)')
colormap(customap('redwhiteblue'))
caxis(clims)
text_location(num2str(norm(pca(pmask) - groundTruth(pmask), 1) / n), 'northeast');

subplot(236)
cdiff = eigmean .* pmask - groundTruth .* pmask;
image3dslice(cdiff, vaxis);
title('error (mean) ')
colormap(customap('redwhiteblue'))
caxis(clims)
text_location(num2str(norm(eigmean(pmask) - groundTruth(pmask), 1) / n), 'northeast');
