function [connections, T, N, B] = miccai2013_compute_connections(T, B)

    % Make sure T, B are orthogonal
    B = orthogonalize(T, B);
    B = normalize(B);

    % Make sure data is clean
    T(isnan(T)) = 0;
    B(isnan(B)) = 0;

    % Obtain N via cross product
    N = cross(B, T, 4);

    % Compute Jacobian of T
    DT = jacobian(T);

    % Compute connection forms
    connections.TNT = cijv(DT, N, T);
    connections.TNN = cijv(DT, N, N);
    connections.TNB = cijv(DT, N, B);
    connections.TBT = cijv(DT, B, T);
    connections.TBN = cijv(DT, B, N);
    connections.TBB = cijv(DT, B, B);