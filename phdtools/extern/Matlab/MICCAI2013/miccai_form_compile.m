clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
recons = {};
for i=1:8
    subj = subjects{i};
    load([dirout, subj, '_recon.mat'])
    recons{i}.data = recon;
    recons{i}.ground = hearts.(char(subjects(i)));
end

[sx, sy, sz] = size(recons{1}.data.form.TNT);
slices = recons{1}.data.form.slices;

%%
pdata = {};
gdata = {};
n = length(recons);
for j=1:length(clabels)
    clbl = clabels{j};
    pdata.form.(clbl) = zeros(sx, sy, sz);
    pdata.pca.(clbl) = zeros(sx, sy, sz);
    pdata.interp.(clbl) = zeros(sx, sy, sz);
    pdata.interp.(clbl) = zeros(sx, sy, sz);

    gdata.(clbl) = zeros(sx, sy, sz);
    
    for i=1:n
        pdata.form.(clbl) = pdata.form.(clbl) + recons{i}.data.form.(clbl) / n;
        pdata.pca.(clbl) = pdata.pca.(clbl) + recons{i}.data.pca.(clbl) / n;
        pdata.interp.(clbl) = pdata.interp.(clbl) + recons{i}.data.interp.(clbl) / n;

        gdata.(clbl) = gdata.(clbl) + recons{i}.ground.(clbl) / n;
    end
end

%% Plot
clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};

figure(1)
clf
msize = 2;
mtype = '-';
lsize = 12;
for i=1:length(clabels)
    subplot(2,3,i)
    hold on
    plot(slices, recon.form.errors.(clabels{i}), mtype, 'Color', colorset(1), 'MarkerSize', msize);
    plot(slices, recon.pca.errors.(clabels{i}), mtype, 'Color', colorset(2), 'MarkerSize', msize);
    plot(slices, recon.interp.errors.(clabels{i}), mtype, 'Color', colorset(3), 'MarkerSize', msize);
    cnorm = norm(gdata.(clabels{i})(:),2);
    
    xlim([min(slices) max(slices)]);
    
    alims = axis;
%     line([alims(1), alims(2)], [cnorm, cnorm], 'Color', colorset(1));
    hold off
    
    if (i==1)
        lhandle = legend({'form', 'pca', 'interp', 'ground'}, 'Location', 'northeast');
        set(lhandle, 'FontSize', lsize);
    end
    
    xlabel('slices');
    ylabel('L^2 error')
    title(clabels{i})
    set(gca, 'Xtick', round([min(slices):round(max(slices)-min(slices))/5:max(slices)]))
end
export_fig([dirout, 'cijk_avg.pdf'], '-transparent', '-q101');

%% Look at each connection form across all regions and hearts
% Combine hearts together

cmapLarge = [-0.6, 0.6];
cmapSmall = [-0.2, 0.2];
%cmaps = { cmapSmall, cmapSmall, cmapLarge, cmapSmall, cmapSmall, cmapSmall };
cmaps.TNT = cmapSmall;
cmaps.TNN = cmapSmall;
cmaps.TNB = cmapLarge;
cmaps.TBT = cmapSmall;
cmaps.TBN = cmapSmall;
cmaps.TBB = cmapSmall;

singlePlot = true;
vaxis = 'y';

chearts = {};
figure(1)
clf
for j=1:length(clabels)
    clbl = clabels{j};
    
    if (~singlePlot)
        subplot(2,3,j);
    else
        clf
    end
    
    hold on
    mask_cumul = zeros(sx, sy, sz);
    chearts.(clbl) = zeros(sx, sy, sz);
    
    for i=1:length(subjects)
        heart = hearts.(char(subjects(i)));
        mask = heart.mask;
        mask(:,:, longitudinal_z_cut:end) = 0;
        mask(:,:, 1:28) = 0;
        cheart = heart.(clbl);
        
        chearts.(clbl)(mask) = chearts.(clbl)(mask) + cheart(mask);
        mask_cumul(mask) = mask_cumul(mask) + 1;
    end
    
%     mask_cumul(mask_cumul == 0) = 1;
    chearts.(clbl) = chearts.(clbl) ./ mask_cumul;
    chearts.(clbl)(mask_cumul == 0) = 0;
    chearts.(clbl)(isnan(chearts.(clbl))) = 0;
    
    hold off
    title(clbl);
    xlim(cmaps.(clbl));
    if (singlePlot)
        image3dslice(chearts.(clbl), vaxis);
        set(gca, 'FontSize', 32)
        axis image
        axis off
        title('');
        colormap(customap('redwhiteblue'))
        caxis(cmaps.(clbl));
        export_fig(sprintf('%s%s_all_%s.pdf', dirout, clbl, vaxis), '-transparent', '-q101');
    end
end
if (~singlePlot)
    export_fig([dirout, 'one-forms_all_hearts.pdf'], '-transparent', '-q101');
end
%%
mask_cumul = logical(mask_cumul);
for j=1:length(clabels)
    clbl = clabels{j};
    cm = mean(chearts.(clbl)(mask_cumul));
    cs = std(chearts.(clbl)(mask_cumul));
    fprintf('%s: %.3f, %.3f\n', clbl, cm, cs);
end

%% Look at each connection form across all regions and hearts
% Combine hearts together
nbins = 100;
stdf = 1;

cmapLarge = [-0.7, 0.5];
cmapSmall = [-0.15, 0.15];
%cmaps = { cmapSmall, cmapSmall, cmapLarge, cmapSmall, cmapSmall, cmapSmall };
cmaps.TNT = cmapSmall;
cmaps.TNN = cmapSmall;
cmaps.TNB = cmapLarge;
cmaps.TBT = cmapSmall;
cmaps.TBN = cmapSmall;
cmaps.TBB = cmapSmall;

singlePlot = true;

chearts = {};
figure(1)
clf
for j=1:length(clabels)
    clbl = clabels{j};
    chearts.(clbl) = zeros(sx, sy, sz);
    
    if (~singlePlot)
        subplot(2,3,j);
    else
        clf
    end
    
    hold on
    mask_cumul = zeros(sx, sy, sz);
    
    for i=1:length(subjects)
        heart = hearts.(char(subjects(i)));
        mask = heart.mask & maskBound;
        cheart = heart.(clbl);
        chearts.(clbl) = chearts.(clbl) + cheart;
        
        mask_cumul = mask_cumul + mask;
        cheart = cheart(cheart > cmaps.(clbl)(1) & cheart < cmaps.(clbl)(2) & mask & ~isnan(cheart));
        histnorm(cheart, 100, stdf, 'Color', colorset(i));
    end
    
    chearts.(clbl) = chearts.(clbl) ./ mask_cumul;
    chearts.(clbl)(isnan(chearts.(clbl))) = 0;
    
    hold off
    title(clbl);
    xlim(cmaps.(clbl));
    if (singlePlot)
        set(gca, 'FontSize', 32)
        title('');
        export_fig(sprintf('%s%s_all_histo.pdf', dirout, clbl), '-transparent', '-q101');
    end
end
if (~singlePlot)
    export_fig([dirout, 'one-forms_all_hearts.pdf'], '-transparent', '-q101');
end
%% Compute volume regions
% Resize mask
mask = mask_cumul > 2;
maskb = zeros(sx, sy, 1+zbounds(2)-zbounds(1));
maskb(:) = mask(:,:,zbounds(1):zbounds(2));
zslices = 3;
xyslices = 4;
% vregions = volumeregions3d(maskb, zslices, xyslices) / (zslices * xyslices);
vregionsCrop = volumeregions3d(maskb, zslices, xyslices);
%vregions = volumeregions3d(mask, zslices, xyslices);
vregions = zeros(sx, sy, sz);
vregions(:,:, zbounds(1):zbounds(2)) = vregionsCrop(:,:,:);

% n = max(vregions(:)) - min(vregions(:));
% r=randperm(n);
% for vr=1+min(vregions(:)):max(vregions(:))
%     vregions(vregions == vr) = r(vr);
% end

figure(1)
image3d(vregions)
export_fig([dirout, 'regions.pdf'], '-transparent', '-q101');
save([dirout, 'regions', '.mat'], 'vregions');


%% Plot all regions for each connection form
figure(2)
clf
meanc = 0.7;
meanColor = [meanc, meanc, meanc];

for j=1:length(clabels)
    clbl = clabels{j};
    cijk = chearts.(clbl);
    nregions = max(vregions(:))-min(vregions(:));
    
    subplot(2,3,j)
    hold on
    
    % Plot mean
    histnorm(cijk(mask), nbins, stdf, 'LineWidth', 5, 'Color', meanColor);

    % Plot per-region
    for r=1:nregions
        histnorm(cijk(vregions == r & mask), nbins, stdf, 'LineWidth', 1, 'Color', colorset(r));
    end
    hold off
    xlim(cmaps{j});
    title(clbl);
end
export_fig([dirout, 'one-forms.pdf'], '-transparent', '-q101');

%% Show legend
figure(3)
clf
hold on
clear rlabels
for r=1:length(subjects)
    plot(0,0, 'Color', colorset(r));
    rlabels{r} = num2str(r);
end
hold off

[lh, obh] = legend(rlabels);
lpos = get(lh, 'Position');
set(gcf, 'position',[50 500 20 200])
set(lh, 'Position', [0, 0, 1, 1])
%set(lh, 'FontSize', 32)
set(obh,'linewidth',15);

set(gca,'box','off');
set(gca,'xcolor','w','ycolor','w','xtick',[],'ytick',[]);
set(gca,'visible','off')
axis off
legend boxoff
export_fig([dirout, 'legend_hearts.pdf'], '-transparent', '-q101');


%% Show legend
figure(3)
clf
hold on
clear rlabels
rlabels{1} = 'mean';
plot(0,0, 'Color', meanColor);
for r=1:nregions
    plot(0,0, 'Color', colorset(r));
    rlabels{r+1} = num2str(r);
end
hold off

[lh, obh] = legend(rlabels);
lpos = get(lh, 'Position');
set(gcf, 'position',[50 500 20 200])
set(lh, 'Position', [0, 0, 1, 1])
%set(lh, 'FontSize', 32)
set(obh,'linewidth',15);

set(gca,'box','off');
set(gca,'xcolor','w','ycolor','w','xtick',[],'ytick',[]);
set(gca,'visible','off')
axis off
legend boxoff
export_fig([dirout, 'legend_regions.pdf'], '-transparent', '-q101');


