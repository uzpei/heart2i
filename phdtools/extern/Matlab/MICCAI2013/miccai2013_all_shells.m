function allshells = miccai2013_all_shells(mask, zcut)

closeSize = 3;

maskOuter = ~miccai2013_shell_mask(mask, closeSize, zcut);

[shell, dtransform] = miccai2013_shell(maskOuter, 0, 0.5, 0);
isize = size(mask);

allshells = zeros(size(mask));

shell_index = 1;
dthresh = 0.5;
while (true && shell_index < max(isize))
    % set to 0.5 for exact shells
    shell = miccai2013_shell(maskOuter, shell_index, dthresh, 0, dtransform);
    allshells(shell) = shell_index;
    
    if (numel(shell) == 0)
        break
    end

    shell_index = shell_index  + 1;
end

allshells(~mask) = 0;
