% Return indices corresponding to voxels lying at a given distance from the boundary.
% function [shell, dtransform] = miccai2013_shell(mask, distance, distance_threshold, varargin)
function [shell, dtransform] = miccai2013_shell(mask, distance, distance_threshold, varargin)

    % Free parameters
    closeSize = 0;
    
    if (nargin == 4)
        closeSize = varargin{1};
    elseif (nargin == 5)
        closeSize = varargin{1};
        dtransform = varargin{2};
    end

    if (closeSize > 0)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %           Preprocess Mask 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % First pad the mask to make sure we have enough room
        padSize = closeSize;
        mask2 = padarray(mask,[padSize padSize padSize]);
        %mask2 = mask;

        %%%%% Image closure %%%%%
        fprintf('Computing image closing...\n');
%        mask2 = imclose(mask2, se);
        mask2 = imdilate(mask2, strel_ball(closeSize));
        mask2 = imerode(mask2, strel_ball(closeSize));

        % Determine the new volume dimension
        border = size(mask2) - size(mask);

        % Remove garbage in corners
        zeroPadSize = border / 2;
        maskClosed = mask2(zeroPadSize(1)+1:end-zeroPadSize(1),zeroPadSize(2)+1:end-zeroPadSize(2), zeroPadSize(3)+1:end-zeroPadSize(3));
    else
        maskClosed = mask;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Distance Transform
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Compute the distance transform of mask2
    if (~exist('dtransform', 'var'))
%        fprintf('Computing distance transform...\n');
        dtransform = bwdist(1-maskClosed);
    end

    shell = find((abs(dtransform - distance) <= distance_threshold) & mask);

end
