function VI = miccai2013_interpolate_volume(V, mask_interpolant_full, mask_interpolation_full, disp_iterations, zcut)

mask = mask_interpolation_full | mask_interpolant_full;

% We want to make sure interpolants are being hit
% mask = mask_interpolation_full | mask_interpolant_full;
mask_interpolation_full = mask;

% Outer mask used to compute the distance transform
maskOuter = ~miccai2013_shell_mask(mask, 3, zcut);
dthresh = 0.5;

% Run once to get dtransform
[shell, dtransform] = miccai2013_shell(maskOuter, 0, 0, 0);

%
[sx, sy, sz] = size(mask);
VI = zeros(sx, sy, sz);
%slab_bounds = [28, 70];
cumshells = zeros(size(mask));
% slab_bounds = [zinds(zi), zinds(zi+1)];
% slab_inds = [slab_bounds(1),slab_bounds(2)];
% slab_intra_inds = (slab_bounds(1)):(slab_bounds(2));
% mask_interpolant_full = zeros(sx, sy, sz);
% mask_interpolant_full(:,:,slab_inds) = mask(:,:,slab_inds);
% mask_interpolation_full = zeros(sx, sy, sz);
% mask_interpolation_full(:,:,slab_intra_inds) = mask(:,:,slab_intra_inds);
% mask_interpolant_full = zeros(sx, sy, sz);
% mask_interpolation_full = zeros(sx, sy, sz);
% mask_interpolant_full(:,:,zinds) = mask(:,:,zinds);
% mask_interpolation_full(:,:,zinds(1):zinds(end)) = mask(:,:,zinds(1):zinds(end));

fprintf('Processing shells: ');
for shell_index=1:max(dtransform(:))
    fprintf('%d ', shell_index);
    [shell, dtransform] = miccai2013_shell(maskOuter, shell_index, dthresh, 0, dtransform);

    % Interpolant mask
    mask_shell = zeros(sx, sy, sz);
    mask_shell(shell) = shell_index;
    mask_interpolant = mask_interpolant_full & (mask_shell == shell_index);
    mask_interpolation = mask_interpolation_full & (mask_shell == shell_index);

    if (numel(find(mask_interpolant)) == 0)
        break;
    end
    
    cumshells(mask_interpolation) = cumshells(mask_interpolation) + 1;
    
    VI = VI + miccai2013_interpolate_shell_slab(V, mask_interpolant, mask_interpolation);

    if (disp_iterations)
        vaxis = 'y';
        subplot(121)
        image3dslice(mask_interpolant, vaxis);
        caxis([-1, 1])
        subplot(122)
        image3dslice(VI, vaxis);
%        caxis([-1, 1])
        pause(0.1)
    end

end
fprintf('\n');

cumshells(cumshells == 0) = 1;
VI = VI ./ cumshells;

% TODO: this is a hack... need to be careful with this
% Remove NaN or excessively large values
VI(isnan(VI)) = 0;
VI(abs(VI) > 1e5) = 0;
