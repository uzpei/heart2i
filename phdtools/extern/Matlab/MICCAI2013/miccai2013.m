% Test different interpolation methods
clear all

%% Load data
useRats = true;

if useRats
    dirdata = '../../data/heart/rats/rathearts.mat';
    zbounds = [28, 90];
else
    dirdata = '../../data/heart/canine/doghearts.mat';
    zbounds = [1, 247];
end

hearts = loadmat(dirdata);
subjects = fieldnames(hearts);

% Remove atlas
subjects = subjects(1:numel(subjects)-1);

%% Set reconstruction parameters
reconSubject = 1;

trainingIndices = 1:length(subjects);
trainingIndices(reconSubject) = [];
trainingSize = length(trainingIndices);

fprintf('Training data from: \n');
subjects(trainingIndices)'

fprintf('Reconstructing subject = %s\n', subjects{reconSubject});
reconstructedHeart = hearts.(char(subjects(reconSubject)));

%% Start computations
useExtrapolation = false;
mask = reconstructedHeart.mask;
[sx, sy, sz] = size(mask);

% This is our ground truth
T = reconstructedHeart.T;
N = reconstructedHeart.N;
B = reconstructedHeart.B;

maskBound = ones(sx, sy, sz);
maskBound(:,:,1:zbounds(1)-1) = 0;
maskBound(:,:,(zbounds(2)+1):end) = 0;

mask = mask & maskBound & (sqrt(T(:,:,:,1) .^ 2 + T(:,:,:,2) .^ 2 + T(:,:,:,3) .^ 2) > 0);
mask3 = repmat(mask, [1 1 1 3]);

% Apply mask and swap all volumes
% T(:,:,:,1) = T(:,:,:,2) .* mask;
% T(:,:,:,2) = T(:,:,:,1) .* mask;
% T(:,:,:,3) = T(:,:,:,3) .* mask;
% N(:,:,:,1) = N(:,:,:,2) .* mask;
% N(:,:,:,2) = N(:,:,:,1) .* mask;
% N(:,:,:,3) = N(:,:,:,3) .* mask;
% B(:,:,:,1) = B(:,:,:,2) .* mask;
% B(:,:,:,2) = B(:,:,:,1) .* mask;
% B(:,:,:,3) = B(:,:,:,3) .* mask;

frames = framefield(T, N, B, true, mask);
T(:,:,:,1) = frames(:,:,:,1);
T(:,:,:,2) = frames(:,:,:,2);
T(:,:,:,3) = frames(:,:,:,3);
N(:,:,:,1) = frames(:,:,:,4);
N(:,:,:,2) = frames(:,:,:,5);
N(:,:,:,3) = frames(:,:,:,6);
B(:,:,:,1) = frames(:,:,:,7);
B(:,:,:,2) = frames(:,:,:,8);
B(:,:,:,3) = frames(:,:,:,9);
orthogonality3d(frames, mask)

% Number of z-slices
zn = 10;
zinds = round(linspace(zbounds(1),zbounds(2), zn))

% Compute partial mask
interpolantMask = zeros(size(mask));
interpolantMask(:,:,zinds) = mask(:,:,zinds);
interpolantMask = logical(interpolantMask);

% Compute interpolation mask
interpolationMask = ~interpolantMask & mask;

dirout = './output/interpolation/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

figure(1)
image3d(interpolantMask)
supertitle('Interpolant');
colormap(customap('redwhiteblue'))
export_fig([dirout, sprintf('mask_interpolant_%i.pdf', zn)], '-transparent', '-q101');

figure(2)
image3d(interpolationMask)
supertitle('Interpolated');
colormap(customap('redwhiteblue'))
export_fig([dirout, sprintf('mask_interpolated_%i.pdf', zn)], '-transparent', '-q101');


%% Compute vector interpolation
imethod = 'linear';
Vx = interpolatevolumemask(T(:,:,:,1), interpolantMask, interpolationMask, imethod, useExtrapolation);
Vy = interpolatevolumemask(T(:,:,:,2), interpolantMask, interpolationMask, imethod, useExtrapolation);
Vz = interpolatevolumemask(T(:,:,:,3), interpolantMask, interpolationMask, imethod, useExtrapolation);

[Vx, Vy, Vz, dnorm] = normalize(Vx, Vy, Vz);

Vx(isnan(Vx)) = 0;
Vy(isnan(Vy)) = 0;
Vz(isnan(Vz)) = 0;

Ti = zeros(sx, sy, sz, 3);
Ti(:,:,:,1) = Vx;
Ti(:,:,:,2) = Vy;
Ti(:,:,:,3) = Vz;

%% Disp output

figure(1)
clf
%image3dreconstructionerror(T, interpolantMask, Ti);
%caxis([-1 1])
%export_fig([dirout, sprintf('vector_%s_interp_%i.pdf', imethod, zn)], '-transparent', '-q101');
image3dreconstructionerror(T, interpolantMask, Ti, [dirout, sprintf('vector_%s_interp_%i', imethod, zn)]);

%image3d(T);

%% Rotation matrix interpolation
% Fill respective column of 3x3 matrices

%% Compute quaternions from frames
tic
[x,y,z,w] = quatm3d(frames);
toc

%% interpolate
imethod = 'linear';
% xi = zinterpolate(x, zinds, imethod);
% yi = zinterpolate(y, zinds, imethod);
% zi = zinterpolate(z, zinds, imethod);
% wi = zinterpolate(w, zinds, imethod);
xi = interpolatevolumemask(x, interpolantMask, interpolationMask, imethod, useExtrapolation);
yi = interpolatevolumemask(y, interpolantMask, interpolationMask, imethod, useExtrapolation);
zi = interpolatevolumemask(z, interpolantMask, interpolationMask, imethod, useExtrapolation);
wi = interpolatevolumemask(w, interpolantMask, interpolationMask, imethod, useExtrapolation);

% Normalize quaternions
% dnorm = sqrt(wi .^ 2 + xi .^ 2 + yi .^ 2 + zi .^ 2);
% wi = wi ./ dnorm;
% xi = xi ./ dnorm;
% yi = yi ./ dnorm;
% zi = zi ./ dnorm;

toc

%% Recreate rotations from quaternions
% M = matquad3d(x, y, z, w);
M = matquad3d(xi, yi, zi, wi);
orthogonality3d(M)

% Disp output
figure(1)
clf
image3dreconstructionerror(T, interpolantMask, M(:,:,:,1:3), [dirout, sprintf('quat_%s_interp_%i', imethod, zn)]);
%export_fig([dirout, sprintf('quat_%s_interp_%i.pdf', imethod, zn)], '-transparent', '-q101');


%% First compute 2nd-order statistics for all hearts (including reconstructed)
fprintf('Computing one-forms for all hearts...\n');
tic
for i=1:length(subjects)
    heart = hearts.(char(subjects(i)));
    hlbl = char(subjects(i));

    hearts.(hlbl).name = hlbl;
    
    % Apply mask
    hearts.(hlbl).T = heart.T .* mask3;
    hearts.(hlbl).N = heart.N .* mask3;
    hearts.(hlbl).B = heart.B .* mask3;

    hearts.(hlbl).Tx = heart.T(:,:,:,1);
    hearts.(hlbl).Ty = heart.T(:,:,:,2);
    hearts.(hlbl).Tz = heart.T(:,:,:,3);
    hearts.(hlbl).Bx = heart.B(:,:,:,1);
    hearts.(hlbl).By = heart.B(:,:,:,2);
    hearts.(hlbl).Bz = heart.B(:,:,:,3);

    % Compute Jacobian
    DT = jacobian(heart.T);

    % Compute stats
    hearts.(hlbl).TNT = cijv(DT, heart.N, heart.T);
    hearts.(hlbl).TNN = cijv(DT, heart.N, heart.N);
    hearts.(hlbl).TNB = cijv(DT, heart.N, heart.B);
    
    hearts.(hlbl).TBT = cijv(DT, heart.B, heart.T);
    hearts.(hlbl).TBN = cijv(DT, heart.B, heart.N);
    hearts.(hlbl).TBB = cijv(DT, heart.B, heart.B);
end
toc

%% Compute one-form reconstruction
testheart = hearts.(char(subjects(reconSubject)));
testheart.name = char(subjects(reconSubject));

%clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
clabels = { 'Tx','Ty','Tz','Bx','By','Bz','TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
%clabels = { 'Tx' };
nu = 0;
clear rheart
figure(1)
clf
cmap = [-0.6, 0.2];

n = numel(find(mask));
%trainingIndices = [2, 3];
X = zeros(n, numel(trainingIndices));

for cind=1:numel(clabels)
    clbl = clabels{cind};

    % Compute data matrix X
    for i=1:numel(trainingIndices)
        heart = hearts.(char(subjects(trainingIndices(i))));
        data = heart.(clbl);
        data = data(mask);
        X(:,i) = data(:);
    end

    % Compute eigenmodes
    [eigenmatrix, evalues, Xmean] = eigendecompose(X);
    mn = size(eigenmatrix,2);

    % Compute the reconstruction map in the reduced coordinate system
    % TODO: make sure this is working properly
    Lmap = mask(:,:,zinds);

    % Compute reconstruction and difference
    % Problem: need to fix indices within find(smasks)
    % i.e. we are adding another thresholding
    cijk = testheart.(clbl);
    cijkm = cijk(mask);
    [xp, W] = pcaproject(X, Xmean, cijkm(:), eigenmatrix, evalues, Lmap, nu);

    % Reconstruct projection
    dataRecon = zeros(sx, sy, sz);
    dataRecon(mask) = xp(:);
%    dataRecon(mask) = Xmean(:);
    
    rheart.([clabels{cind}]) = dataRecon;
    
    % Visu
    ni = 4;
    nj = ceil(2 * numel(clabels) / ni);
    vs = 'z';
    
    subplot(ni,nj,2*(cind-1) + 1)
    image3dslice(cijk, vs);
    caxis(cmap);
    axis image
    title(clbl);
    
    subplot(ni,nj,2*(cind-1) + 2)
    image3dslice(dataRecon, vs);
    caxis(cmap);
    title(clbl);
    axis image
    
end
export_fig([dirout, sprintf('pca_recon_%i.pdf', zn)], '-transparent', '-q101');

%% Reconstruct frames from PCA
Tp = zeros(sx,sy,sz,3);
Tp(:,:,:,1) = rheart.Ty(:,:,:);
Tp(:,:,:,2) = rheart.Tx(:,:,:);
Tp(:,:,:,3) = rheart.Tz(:,:,:);
Bp(:,:,:,1) = rheart.By(:,:,:);
Bp(:,:,:,2) = rheart.Bx(:,:,:);
Bp(:,:,:,3) = rheart.Bz(:,:,:);

% Normalize
Tp = Tp ./ (eps + repmat(sqrt(sum(Tp .^ 2, 4)), [1 1 1 3]));
Bp = Bp ./ (eps + repmat(sqrt(sum(Bp .^ 2, 4)), [1 1 1 3]));

%% Save PCA frame extrapolation
clf
%image3dreconstructionerror(T, interpolantMask, Tp);
%export_fig([dirout, sprintf('pca_exterp_%i.pdf', zn)], '-transparent', '-q101');
image3dreconstructionerror(T, interpolantMask, Tp, [dirout, sprintf('pca_exterp_%i', zn)]);


Np = cross(Bp, Tp, 4);

%% Reconstruct orientation from extrapolated mean
h = zeros(sx, sy, sz, 3);

% Compute the error in a n^3 neighborhood
onesv = ones(sx, sy, sz);
error_volume = zeros(sx, sy, sz);

tic
n = 1;
num_neighbors = (2 * n + 1)^3;

Tout = zeros(sx, sy, sz, 3);

TNT = rheart.TNT;
TNN = rheart.TNN;
TNB = rheart.TNB;
TBT = rheart.TBT;
TBN = rheart.TBN;
TBB = rheart.TBB;

for i = -n:n
    for j = -n:n
        for k = -n:n
%            [i, j, k]
            
            % Compute the current offset for all voxels
            h(:,:,:,1) = i * onesv;
            h(:,:,:,2) = j * onesv;
            h(:,:,:,3) = k * onesv;
            
            % Compute extrapolation at neighbor offset
            sN1 = dot(h, Tp, 4) .* TNT;
            sN2 = dot(h, Np, 4) .* TNN;
            sN3 = dot(h, Bp, 4) .* TNB;
            sN = repmat(sN1 + sN2 + sN3, [1 1 1 3]);

            sB1 = dot(h, Tp, 4) .* TBT;
            sB2 = dot(h, Np, 4) .* TBN;
            sB3 = dot(h, Bp, 4) .* TBB;
            sB = repmat(sB1 + sB2 + sB3, [1 1 1 3]);


            % Compute the extrapolated tangent
            Te = Tp + sN .* Np + sB .* Bp;
             
            % normalize
            Te = Te ./ (eps + repmat(sqrt(sum(Te .^ 2, 4)), [1 1 1 3]));
            
            % Store cumulative prediction
            Tout = Tout + circshift(Te, -[i, j, k, 0]);
        end
    end
end
toc
%s
Tout = Tout ./ (eps + repmat(sqrt(sum(Tout .^ 2, 4)), [1 1 1 3]));
Tout(~mask3) = 0;

%%
% figure(1)
% image3d(Tout)
% figure(2)
% image3d(T)
image3dreconstructionerror(T, interpolantMask, Tout, [dirout, sprintf('pca_exterp_form_%i', zn)]);
%export_fig([dirout, sprintf('pca_exterp_form_%i.pdf', zn)], '-transparent', '-q101');
