dirdata = '../../data/heart/rats/rathearts.mat';
zbounds = [28, 90];
tic
hearts = loadmat(dirdata);
toc

%%
subjects = fieldnames(hearts);
heart = hearts.(subjects{1});
mask = heart.mask;
Th = heart.T;
zind = 50;

%% Test some data
sx = 64;
sy = 64;
sz = 128;

[X, Y, Z] = ndgrid(1:sx, 1:sy, 1:sz);
x0 = round(sx/2);
y0 = round(sy/2);
z0 = round(sz/2);

%Tx = X - y0;
%Ty = -(Y - x0);
%Tz = 0;
Tx = ones(sx, sy, sz);
Ty = zeros(sx, sy, sz);
Tz = ones(sx, sy, sz);

[Tx, Ty, Tz] = normalize(Tx, Ty, Tz);
Tx(~mask) = 0;
Ty(~mask) = 0;

figure(1)
T = assemble(Tx, Ty, Tz);
quiverslice(mask, T);
axis image
supertitle('this is a XZ vector field');

figure(2)
quiverslice(mask, Th);
axis image
supertitle('this is the data');
 
%% Test connection forms
clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
subjects = fieldnames(hearts);
T = hearts.(subjects{1}).T;
N = hearts.(subjects{1}).N;
B = hearts.(subjects{1}).B;

%% Compute stats
tic
fprintf('Vectorized operations start\n');
fprintf('Computing Jacobian...\n');
DT = jacobian(T);
fprintf('Computing TNT...\n');
stats.TNT.c1 = cijv(DT, N, T);
fprintf('Computing TNN...\n');
stats.TNN.c1 = cijv(DT, N, N);
fprintf('Computing TNB...\n');
stats.TNB.c1 = cijv(DT, N, B);
fprintf('Computing TBT...\n');
stats.TBT.c1 = cijv(DT, B, T);
fprintf('Computing TBN...\n');
stats.TBN.c1 = cijv(DT, B, N);
fprintf('Computing TBB...\n');
stats.TBB.c1 = cijv(DT, B, B);
toc

%%
tic
fprintf('Elemental operations start\n');
cijks = cijk2(T, N, B);
toc

%%
figure(1)
clf
for i=1:length(clabels)
    subplot(2,6,i);
    image3dslice(stats.(clabels{i}).c1, 'z');
    title(clabels{i});
%    colorbar
     caxis(cmaps{i});
%    caxis([-0.5 0.5]);
    axis image

    subplot(2,6,6 + i)
    image3dslice(stats.(clabels{i}).c2, 'z');
    title(clabels{i});
%    colorbar
    caxis(cmaps{i});
%    caxis([-0.5 0.5]);
    axis image
end

    
