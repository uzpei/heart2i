clear all
fprintf('Loading data...\n');
tic
dirdata = '../../data/heart/rats/rathearts.mat';
hearts = miccai_preprocess(loadmat(dirdata));
toc

subjects = fieldnames(hearts);
subject = subjects{1};
heart = hearts.(subject);

dirout = './output/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

measurements = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
clims = { [-0.1, 0.1], [-0.1, 0.1], [-0.5, 0.5], [-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1] };

%% 

zcut = [0, 20];
imethod = 'nearest';
useExtrapolation = false;

% Compute partial volume bounds
mask = heart.mask;
[sx, sy, sz] = size(mask);
zbounds = zbox(heart.mask, 15) - zcut;
zdiff = zbounds(2) - zbounds(1);
maskBound = ones(sx, sy, sz);
maskBound(:,:,1:zbounds(1)-1) = 0;
maskBound(:,:,(zbounds(2)+1):end) = 0;
mask = mask & maskBound;
mask3 = repmat(mask, [1 1 1 3]);

% Prepare interpolation
T = heart.T;
Tx = T(:,:,:,1);
Ty = T(:,:,:,2);
Tz = T(:,:,:,3);

image3d(T);
colormap(customap('redwhiteblue'))
B_ground = estimate_wall_normals(mask);

%%
zerrors = {};
for zn=5
%for zn=2:20
zinds = round(linspace(zbounds(1),zbounds(2), zn));
zinds = [1, zinds, 128];

% Compute partial mask
interpolantMask = zeros(size(mask));
interpolantMask(:,:,zinds) = mask(:,:,zinds);
interpolantMask = logical(interpolantMask);

% Compute interpolation mask
interpolationMask = ~interpolantMask & mask;

% Display interpolation mask
figure(1)
image3d(interpolantMask + 2 * interpolationMask)
supertitle('Interpolation');
colormap(customap('redwhiteblue'))

%% Interpolate Tx, Ty, Tz, components
fprintf('Reconstructing from direction interpolation...\n');
useExtrapolation = false;
imethod = 'linear';
tic
Txc = interpolatevolumemask(Tx, interpolantMask, interpolationMask, imethod, useExtrapolation);
Tyc = interpolatevolumemask(Ty, interpolantMask, interpolationMask, imethod, useExtrapolation);
Tzc = interpolatevolumemask(Tz, interpolantMask, interpolationMask, imethod, useExtrapolation);
toc
Tc = assemble(Txc, Tyc, Tzc);
Tc = normalize(Tc);
Tc(~mask3) = 0;

%% Faster computation
tic
[Fx, Fy, Fz] = meshgrid(1:sx, 1:sy, zinds);
[X, Y, Z] = meshgrid(1:sx, 1:sy, 1:sz);
Tmesh = zeros(sx, sy, length(zinds));
Tmesh(:,:,:) = T(:,:,zinds,1);

Tc2 = ba_interp3(Fx, Fy, Fz, Tmesh, X, Y, Z, 'linear');
Tc2(~mask) = 0;
toc

%
figure(2)
image3d(Tc(:,:,:,1));
colormap(customap('redwhiteblue'))

figure(3)
image3d(Tc2(:,:,:,1));
colormap(customap('redwhiteblue'))

figure(4)
image3d(T(:,:,:,1));
colormap(customap('redwhiteblue'))

end
