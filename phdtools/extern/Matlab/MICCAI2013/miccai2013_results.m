%reconstructionIndices = 1:8;
reconstructionIndices = 1:7;
fontSize = 32;
slineWidth = 2;
connectionLabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};

% methods = { 'form', 'form_shell', 'pca', 'form_interp', 'interp' };
% methods = { 'form_interp', 'interp', 'form', 'form_shell', 'pca'};
% legends = { 'Cartan Interp', 'T Interp', 'Cartan PCA', 'Cartan Shell PCA', 'T PCA' };
%methods = { 'form_interp', 'interp', 'form', 'pca'};
%legends = { 'Cartan Interp', 'T Interp', 'Cartan PCA', 'T PCA', 'Volume' };
% methods = { 'form_interp', 'interp', 'form', 'pca'};
methods = { 'form_interp', 'form', 'interp', 'pca'};
legends = { 'Cartan Interpolation', 'Cartan PCA', 'Fiber Interpolation', 'Fiber PCA'};
lspecs = { '-', '--', ':', '-.' };

% First load data
avg = {};
for ci = 1:numel(connectionLabels)
    avg.ground.(connectionLabels{ci}).data = [];
end

for ri = reconstructionIndices
    fprintf('Loading %s...\n', subjects{ri});
    reconstructions.(subjects{ri}) = loadmat(sprintf('./output/interpolation/%s_reconstruction.mat', subjects{ri}));
    slices = reconstructions.(subjects{ri}).slices;

    for ci = 1:numel(connectionLabels)
        avg.ground.(connectionLabels{ci}).data = [avg.ground.(connectionLabels{ci}).data; norm(reconstructions.(subjects{ri}).ground.(connectionLabels{ci})(:), 1) / reconstructions.(subjects{ri}).interp.errors.n];
    end
end

for ci = 1:numel(connectionLabels)
    avg.ground.(connectionLabels{ci}).mean = mean(avg.ground.(connectionLabels{ci}).data);
end

%% For each connection form
tic
for ci = 1:numel(connectionLabels)
    clbl = connectionLabels{ci};
    
    % For each method
    for mi = 1:numel(methods)
        mlbl = methods{mi};
        
        % Compute mean and STD bars for this method by combining all results
        avg.(mlbl).(clbl).data = [];

        for ri = reconstructionIndices
            rlbl = subjects{ri};
            
            avg.(mlbl).(clbl).data = [avg.(mlbl).(clbl).data; reconstructions.(rlbl).(mlbl).errors.norm1.(clbl)];
        end
        
        avg.(mlbl).(clbl).means = mean(avg.(mlbl).(clbl).data, 1);
        avg.(mlbl).(clbl).stds = std(avg.(mlbl).(clbl).data, 1);

    end
end
toc

%% Plot output
figure(1)
connectionLabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
%connectionLabels = { 'TNB' };
for ci = 1:numel(connectionLabels)
%for ci = 1:3
    clbl = connectionLabels{ci};
    
    clf
    hold on

    groundm = avg.ground.(clbl).mean;

    % For each method
    for mi = 1:numel(methods)
        mlbl = methods{mi};

        hData = errorbar(100 * slices(2:end) / 50, 100*(avg.(mlbl).(clbl).means(2:end) / groundm), 100 * avg.(mlbl).(clbl).stds(2:end) / groundm,'.r-', 'Color', colorset(mi), 'LineWidth', slineWidth, 'LineStyle', lspecs{mi});
%        hData = errorbar(slices(2:end), 100*(1-avg.(mlbl).(clbl).means(2:end) / groundm), 100 * avg.(mlbl).(clbl).stds(2:end) / groundm,'.r-', 'Color', colorset(mi), 'LineWidth', slineWidth, 'LineStyle', lspecs{mi});
        set(hData                            , ...
        'LineWidth'       , 2           , ...
        'Marker'          , 'o'         , ...
        'MarkerSize'      , 6           , ...
        'MarkerEdgeColor' , [.2 .2 .2]  , ...
        'MarkerFaceColor' , [.7 .7 .7]  );
    
    end
%     alims = axis;
%     line([alims(1), alims(2)], [groundm, groundm], 'Color', [0, 0, 0], 'LineStyle','-', 'LineWidth',2);

    hold off

[h, childObjs] = legend(legends);
% [h, childObjs] = legend(legends, 'location', 'northeast');
% h = legend(legends, 'location', 'southeast');
%# find handles of lines inside legend that have a non-empty tag

set(h, 'FontSize', 38);

xlim([2*100/50, 100*(max(slices)+1) / 50])
ylims = ylim;
ylim([max(0,ylims(1)), ylims(2)]);

% xh = xlabel('Reconstruction slices');
xh = xlabel('Reconstruction (%)');
% yh = ylabel('L^1 error');
yh = ylabel('Reconsctruction error (%)');

set(xh, 'FontSize', fontSize);
set(yh, 'FontSize', fontSize);
set(gca, 'FontSize', fontSize);
%daspect('manual')
%set(gca,'XTick',slices)
%daspect([30 1 5])

grid off
box off

% lineObjs = findobj(childObjs, 'Type', 'line');
% xCoords = get(lineObjs, 'XData') ;
% hoffset = [0, 0.1];
% set(lineObjs(2), 'XData', xCoords{2} + hoffset)
% set(lineObjs(4), 'XData', xCoords{4} + hoffset)
% set(lineObjs(6), 'XData', xCoords{6} + hoffset)
% set(lineObjs(8), 'XData', xCoords{8} + hoffset)
legend boxoff
export_fig([dirout, sprintf('errors_%s.pdf', connectionLabels{ci})], '-transparent', '-q101');

end

%%
load('./output/interpolation/rat07052008_reconstruction.mat')
clbl = 'TNB';

%%

figure(1)
clf

hold on
plot(reconstruction.slices, reconstruction.form.errors.(clbl), '-o', 'LineWidth', slineWidth, 'Color', colorset(1)) %OK
plot(reconstruction.slices, reconstruction.form_shell.errors.(clbl), '-o', 'LineWidth', slineWidth, 'Color',  colorset(2))
plot(reconstruction.slices, reconstruction.pca.errors.(clbl), '-o', 'LineWidth', slineWidth, 'Color',  colorset(3))
plot(reconstruction.slices, reconstruction.form_interp.errors.(clbl), '-o', 'LineWidth', slineWidth, 'Color',  colorset(6)) %OK
plot(reconstruction.slices, reconstruction.interp.errors.(clbl), '-o', 'LineWidth', slineWidth, 'Color',  colorset(5))

groundm = norm(reconstruction.ground.TNB(:) / numel(find(abs(reconstruction.ground.TNB) > 0)), 1);
alims = axis;
line([alims(1), alims(2)], [groundm, groundm], 'LineStyle','--', 'LineWidth',4);

hold off

h = legend({'form PCA', 'form shell PCA', 'T pca', 'form interpolation', 'T interpolation', 'form magnitude'});
set(h, 'FontSize', fontSize);

xh = xlabel('reconstruction slices');
yh = ylabel('L^2 error');

set(xh, 'FontSize', fontSize);
set(yh, 'FontSize', fontSize);
set(gca, 'FontSize', fontSize);
export_fig([dirout, sprintf('%s_comparison.pdf', testSubjectLabel)], '-transparent', '-q101');

%%
figure(1)
%colormap(customap('redwhiteblue'))
colormap(jet)
test_heart = hearts.rat07052008;
dthresh = 0.5;
maskOuter = ~miccai2013_shell_mask(test_heart.mask, 3, 90);
[shell, dtransform] = miccai2013_shell(maskOuter, 1, dthresh, 0);
dtransform = dtransform .* test_heart.mask;
dtransform(:,:, 90:end) = 0;
image3d(dtransform .* test_heart.mask);
export_fig([dirout, sprintf('%s_shell.pdf', testSubjectLabel)], '-transparent', '-q101');
