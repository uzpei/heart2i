disp1 = false;
disp2 = true;
disp3 = false;

if (sumInterpolationErrorsOnly)
    errorMask = interpolationMask;
else
    errorMask = interpolationMask | interpolantMask;
end

errorIndex = reconIndex;
zn = numSlices;

normalization = numel(find(interpolationMask));
%normalization = numel(find(errorMask));
Lnorm = 1;

% mlbl = 'TNB';
mlbl = 'TNB';
vaxis = 'x';
clims = [-0.5, 0.5];
tloc = 'northeast';

cground = reconstruction.ground.(mlbl);

%% Plot selected results
if (disp1)
figure(2)
clf

%subplot(141)
hold on
image3dslice(crop(cground, [1, sx], [1, sy], zbounds), vaxis);

axis image
% Overlay interpolant slices over the ground volume
lwidth = 8;
alims = axis;
for i=1:numel(zinds)
   zi = zinds(i);
   xi = [alims(1), alims(2)];
   yi = [1 + zi - zbounds(1), 1 + zi - zbounds(1)];
%    line(xi, yi, 'Color', [0, 0, 0],'LineStyle','-', 'LineWidth',lwidth);
    p = patchline(xi, yi,'linestyle','-','edgecolor','k','linewidth',lwidth,'edgealpha',0.4);
end
hold off
caxis(clims);
%title('original');
%text_location(sprintf('%.4f', norm(cground(:), Lnorm) / normalization), tloc);
set(gca, 'YDir', 'reverse');
colormap(customap('redwhiteblue'))
axis off
axis image

export_fig([dirout2, sprintf('%s_results_TNB_original_%i.png', testSubjectLabel, zn)], '-transparent', '-q101');

%subplot(142)
image3dslice(crop(reconstruction.interp.(mlbl), [1, sx], [1, sy], zbounds), vaxis);
caxis(clims);
%title('T interp');
%text_location(sprintf('%.4f', reconstruction.interp.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))
axis off
export_fig([dirout2, sprintf('%s_results_TNB_interp_%i.png', testSubjectLabel, zn)], '-transparent', '-q101');

%subplot(143)
image3dslice(crop(reconstruction.form_interp.(mlbl), [1, sx], [1, sy], zbounds), vaxis);
caxis(clims);
%title('form interp');
%text_location(sprintf('%.4f', reconstruction.form_interp.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))
axis off
export_fig([dirout2, sprintf('%s_results_TNB_form_interp_%i.png', testSubjectLabel, zn)], '-transparent', '-q101');

%subplot(144)
image3dslice(crop(reconstruction.form.(mlbl), [1, sx], [1, sy], zbounds), vaxis);
caxis(clims);
%title('form PCA');
%text_location(sprintf('%.4f', reconstruction.form.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))
axis off
export_fig([dirout2, sprintf('%s_results_TNB_form_%i.png', testSubjectLabel, zn)], '-transparent', '-q101');

% export_fig([dirout2, sprintf('%s_results_TNB_%i.pdf', testSubjectLabel, zn)], '-transparent', '-q101');
%export_fig([dirout2, sprintf('%s_results_TNB_%i.pdf', testSubjectLabel, zn)], '-transparent');
end

%% All results
if (disp2)
figure(1)
clf
subplot(241)
image3dslice(cground, vaxis);
caxis(clims);
title('original');
text_location(sprintf('%.4f', norm(cground(:), Lnorm) / normalization), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(242)
image3dslice(interpolantMask, vaxis);
caxis(clims);
title('interpolant');
axis image
colormap(customap('redwhiteblue'))

subplot(243)
image3dslice(interpolationMask, vaxis);
caxis(clims);
title('interpolated');
axis image
colormap(customap('redwhiteblue'))

subplot(245)
image3dslice(reconstruction.form.(mlbl), vaxis);
caxis(clims);
title('form PCA');
text_location(sprintf('%.4f', reconstruction.form.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(246)
image3dslice(reconstruction.form_shell.(mlbl), vaxis);
caxis(clims);
title('form shell PCA');
text_location(sprintf('%.4f', reconstruction.form_shell.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(244)
image3dslice(reconstruction.pca.(mlbl), vaxis);
caxis(clims);
title('T pca');
text_location(sprintf('%.4f', reconstruction.pca.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(247)
image3dslice(reconstruction.form_interp.(mlbl), vaxis);
caxis(clims);
title('form interp');
text_location(sprintf('%.4f', reconstruction.form_interp.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(248)
image3dslice(reconstruction.interp.(mlbl), vaxis);
caxis(clims);
title('T interp');
text_location(sprintf('%.4f', reconstruction.interp.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

export_fig([dirout2, sprintf('%s_results_TNB_all_%i.pdf', testSubjectLabel, zn)], '-transparent', '-q101');
end

%% All results, errors
if (disp3)
figure(1)
clf

subplot(241)
image3dslice(cground, vaxis);
caxis(clims);
title('original');
text_location(sprintf('%.4f', norm(cground(:), Lnorm) / normalization), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(242)
image3dslice(interpolantMask, vaxis);
caxis(clims);
title('interpolant');
axis image
colormap(customap('redwhiteblue'))

subplot(243)
image3dslice(interpolationMask, vaxis);
caxis(clims);
title('interpolated');
axis image
colormap(customap('redwhiteblue'))

subplot(245)
image3dslice(abs(reconstruction.form.(mlbl) - cground), vaxis);
caxis(clims);
title('form PCA');
text_location(sprintf('%.4f', reconstruction.form.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(246)
image3dslice(abs(reconstruction.form_shell.(mlbl) - cground), vaxis);
caxis(clims);
title('form shell PCA');
axis image
text_location(sprintf('%.4f', reconstruction.form_shell.errors.norm1.(mlbl)(errorIndex)), tloc);
colormap(customap('redwhiteblue'))

subplot(244)
image3dslice(abs(reconstruction.pca.(mlbl) - cground), vaxis);
caxis(clims);
title('T pca');
text_location(sprintf('%.4f', reconstruction.pca.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(247)
image3dslice(abs(reconstruction.form_interp.(mlbl) - cground), vaxis);
caxis(clims);
title('form interp');
text_location(sprintf('%.4f', reconstruction.form_interp.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))

subplot(248)
image3dslice(abs(reconstruction.interp.(mlbl) - cground), vaxis);
caxis(clims);
title('T interp');
text_location(sprintf('%.4f', reconstruction.interp.errors.norm1.(mlbl)(errorIndex)), tloc);
axis image
colormap(customap('redwhiteblue'))
export_fig([dirout2, sprintf('%s_results_TNB_error_%i.pdf', testSubjectLabel, zn)], '-transparent', '-q101');
end