function [projected, eigenmatrix, W, tmean] = miccai_pca_from_shell(hearts, trainingIndices, testIndex, measurement, shell_index, nu)

    subjects = fieldnames(hearts);

    testheart = hearts.(subjects{testIndex});

    [sx, sy, sz] = size(testheart.mask);
    n = sx*sy*sz;
    X = zeros(n, numel(trainingIndices));

    % Compute data matrix X
    for i=1:numel(trainingIndices)
        heart = hearts.(subjects{trainingIndices(i)});
        x = heart.(measurement);
        
        %% Compute shell
        x(~(heart.maskShell == shell_index)) = 0;
        
%         image3d(x, [], [-0.5, 0.5]);
%         pause(0.1)
        X(:,i) = x(:);
    end

    if (norm(X(:), 1) == 0)
        projected = [];
        eigenmatrix = [];
        W = [];
        tmean = [];
        return;
    end
    
    % Compute eigenmodes
    [eigenmatrix, evalues, Xmean] = eigendecompose(X);

    % Compute reconstruction and difference
    testMeasure = testheart.(measurement);
    tmask = heart.maskShell == shell_index;
    testMeasure(~tmask) = 0;
    
    [xp, W] = pcaproject(X, Xmean, testMeasure(:), eigenmatrix, evalues, nu, tmask);
    W'
    
    % Reconstruct projection
    projected = zeros(sx, sy, sz);
%    projected(mask) = xp(:);
    
%    projected(~(heart.maskShell == shell_index)) = xp(:);
    projected(:) = xp(:);
    
%    image3d(projected, [], [-0.5, 0.5]);
    tmean = zeros(sx, sy, sz);
    tmean(:) = Xmean(:);
    
%     subplot(211)
%     image3dslice(projected, 'z');
%     caxis([-0.5, 0.5]);
%     subplot(212)
%     image3dslice(tmean, 'z');
%     caxis([-0.5, 0.5]);
%     pause(0.1)

end
