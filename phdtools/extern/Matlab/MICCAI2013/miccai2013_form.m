%function miccai2013_form(reconSubjects)
reconSubjects = 1;

%clear all
close all
%reconSubjects = 1:7;
%reconSubjects = 2;
%reconSubjects = 5;
path(path, 'rbf_1');
warning off
%set(0,'DefaultFigureWindowStyle','docked')

%% Load data
useRats = true;

% Should *not* be true since hearts from
% combined using opendtmri which already does the swapping
swapXY = false;

preserveControlSlices = false;

if useRats
    dirdata = '../../data/heart/rats/rathearts.mat';

    %    zbounds = [28, 90];
    longitudinal_z_cut_low = 25;
    longitudinal_z_cut_high = 80;

    % Number of z-slices
%     zn = 10;
else 
    dirdata = '../../data/heart/canine/doghearts.mat';
%    zbounds = [1, 247];
    longitudinal_z_cut_low = 1;
    longitudinal_z_cut_high = 200;

    % Number of z-slices
%     zn = 30;
end

% Axis limits
cmapVec = [-1, 1];
cmapLarge = [-0.5, 0.5];
cmapSmall = [-0.1, 0.1];
%cmaps = { cmapSmall, cmapSmall, cmapLarge, cmapSmall, cmapSmall, cmapSmall };
cmaps.TNT = cmapSmall;
cmaps.TNN = cmapSmall;
cmaps.TNB = cmapLarge;
cmaps.TBT = cmapSmall;
cmaps.TBN = cmapSmall;
cmaps.TBB = cmapSmall;

connectionLabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
pcaMeasurements = { 'Tx','Ty','Tz','TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
pcamaps = { cmapVec, cmapVec, cmapVec, cmapSmall, cmapSmall, cmapLarge, cmapSmall, cmapSmall, cmapSmall };

dirout = './output/interpolation/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

%%%%%%%%%%%%%%%%%%%%%%%%
%% PREPROCESS FIELD
%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Loading data...\n');
ctic=tic;
hearts = miccai_preprocess(loadmat(dirdata));
%hearts = loadmat(dirdata);
toc(ctic);

%% Set subjects (remove atlas
subjects = fieldnames(hearts);
subjects = subjects(1:numel(subjects)-1);

% Remove last heart (large right ventricle)
if (useRats)
    subjects = subjects(1:numel(subjects)-1);
end

% Process mask
sample = hearts.(subjects{4});
[sx, sy, sz] = size(sample.mask);

invalidBounds = [1:longitudinal_z_cut_low, longitudinal_z_cut_high:sz];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN PER-SUBJECT PCA PRECOMPUTATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%zperc = 0.01:0.05:1;
%zperc = 0.05:0.05:0.4;
minSlices = 2;
maxSlices = 21;
numSlices = 10;
reconSliceIndices = logspace_sampling(minSlices, maxSlices, numSlices);
%reconSliceIndices = 5;

%%
for reconSubject = reconSubjects 
    testSubjectLabel = subjects{reconSubject};
    testHeart = hearts.(char(subjects(reconSubject)));
    testheart.name = char(subjects(reconSubject));

    dirout2 = ['./output/interpolation/', testSubjectLabel, '/'];
    if ~exist(fullfile('.',dirout2),'dir')
        mkdir(dirout2)
    end

    fprintf('BEGINNING COMPUTATIONS for %s\n', testSubjectLabel);
    fprintf('Running reconstruction for the following samples: %s \n', strtrim(sprintf('%d ', reconSliceIndices)))
    %% Set reconstruction parameters
    trainingIndices = 1:length(subjects);
    trainingIndices(reconSubject) = [];
    trainingSize = length(trainingIndices);

    fprintf('Training data from: \n');
    subjects(trainingIndices)'
    fprintf('Reconstructing subject = %s\n', testSubjectLabel);

    % Compute partial volume bounds
%    zbounds = zbox(testHeart.mask, 15) - zcut;
    zb = zbox(testHeart.mask, 15);
    zbounds(1) = zb(1);
    zbounds(2) = longitudinal_z_cut_high;
    
    zdiff = zbounds(2) - zbounds(1);
    mask = ones(sx, sy, sz);
    mask(:,:,1:zbounds(1)-1) = 0;
    mask(:,:,(zbounds(2)+1):end) = 0;
    mask = mask & testHeart.mask;

    % Noise estimate (set to 0 for unregularized pseudoinverse)
    nu = 0;

    % Manually select slice percentage (disable this to automate)
%    zperc = 0.1;

    % Fill in partial volume indices and make sure we use all slices in the end
%    zninds = round(zdiff * zperc);
%    zninds(1+numel(zninds)) = zdiff;

    clear reconstruction
    reconstruction.slices = reconSliceIndices;

    % Compute ground truth
    T_ground = testHeart.T;
    B_ground = estimate_wall_normals(mask);

    %% Iterate volume sampling
    %recon_errors = zeros(numel(reconSliceIndices), 4);
     for reconIndex = 1:numel(reconSliceIndices)
%    for reconIndex = 1
        numSlices = reconSliceIndices(reconIndex);
        zinds = round(linspace(zbounds(1),zbounds(2), numSlices));

        fprintf('\n==========================================\n');
        fprintf('\n    UPDATING SLICING with %i/%i samples   \n', numSlices, reconSliceIndices(end));
        fprintf('\n==========================================\n');
        fprintf('\n');
        
        fprintf('---> Computing PCA...\n');
        
        % Compute partial g
        interpolantMask = zeros(size(mask));
        interpolantMask(:,:,zinds) = mask(:,:,zinds);
        interpolantMask = logical(interpolantMask);

        % Compute interpolation mask
        interpolationMask = ~interpolantMask & mask;

        %% Precompute PCA for selected measurements
        ctic=tic;
%        heartsFilled = miccai2013_make_equal(hearts, pcaMeasurements, reconSubject, trainingIndices);
%        heartPCA = miccai2013_compute_reconstruction_form(heartsFilled, pcaMeasurements, trainingIndices, reconSubject, interpolantMask, interpolationMask);
        heartPCA = miccai2013_compute_reconstruction_form(hearts, pcaMeasurements, trainingIndices, reconSubject, interpolantMask, interpolationMask);
        
        toc(ctic)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Begin comparing methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        interpolationMethod = 'linear';
        useExtrapolation = true;
        sumInterpolationErrorsOnly = true;
        
        % For debugging, look at single connection form
        %connectionLabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
%        connectionLabels = { 'TNB' };

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% GROUND TRUTH
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for cind=1:length(connectionLabels)
            reconstruction.ground.(connectionLabels{cind}) = testHeart.(connectionLabels{cind});
            reconstruction.ground.(connectionLabels{cind})(~mask) = 0;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% ONE-FORM PCA RECONSTRUCTION
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('---> Reconstructing from one-form PCA...\n');
        ctic=tic;
        reconstruction.form.TNT = heartPCA.TNT;
        reconstruction.form.TNN = heartPCA.TNN;
        reconstruction.form.TNB = heartPCA.TNB;
        reconstruction.form.TBT = heartPCA.TBT;
        reconstruction.form.TBN = heartPCA.TBN;
        reconstruction.form.TBB = heartPCA.TBB;
        toc(ctic)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% ONE-FORM SHELL PCA RECONSTRUCTION
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('\n---> Reconstructing from one-form shell PCA...\n');
        ctic=tic;
        shellDistance = 0.5;
%        trainingIndices = [2:7];
        connections = miccai2013_compute_reconstruction_shell(hearts, trainingIndices, reconSubject, shellDistance, interpolantMask, interpolationMask, connectionLabels, invalidBounds, longitudinal_z_cut_high);
        for cind=1:length(connectionLabels)
            reconstruction.form_shell.(connectionLabels{cind}) = connections.shell_projected.(connectionLabels{cind});
        end

        toc(ctic)
        %colormap(customap('redwhiteblue'));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% DIRECTION PCA -> ONE-FORM
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %image3dreconstructionerror(T, interpolantMask, Tp, [dirout, sprintf('pca_exterp_%i', zn)]);
        fprintf('\n---> Reconstructing from direction PCA...\n');
        ctic=tic;
        connections = miccai2013_compute_connections(assemble(heartPCA.Tx, heartPCA.Ty, heartPCA.Tz), B_ground);
%        reconstruction.pca = connections;  
        for cind=1:length(connectionLabels)
            reconstruction.pca.(connectionLabels{cind}) = connections.(connectionLabels{cind});
        end
        toc(ctic)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% ONE-FORM SHELL INTERPOLATION
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('\n---> Reconstructing from one-form interpolation...\n');
        ctic=tic;
        for cind=1:length(connectionLabels)
%            reconstruction.form_interp.(connectionLabels{cind}) = interpolatevolumemask(testHeart.(connectionLabels{cind}), interpolantMask, interpolationMask, interpolationMethod, useExtrapolation);
            reconstruction.form_interp.(connectionLabels{cind}) = miccai2013_interpolate_volume(testHeart.(connectionLabels{cind}), interpolantMask, interpolationMask, 'true', longitudinal_z_cut_high);
        end
        
        toc(ctic)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% VECTOR SHELL INTERPOLATION -> ONE-FORM
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('\nReconstructing from direction interpolation...\n');
        ctic=tic;
%         Tx = interpolatevolumemask(T_ground(:,:,:,1), interpolantMask, interpolationMask, interpolationMethod, useExtrapolation);
%         Ty = interpolatevolumemask(T_ground(:,:,:,2), interpolantMask, interpolationMask, interpolationMethod, useExtrapolation);
%         Tz = interpolatevolumemask(T_ground(:,:,:,3), interpolantMask, interpolationMask, interpolationMethod, useExtrapolation);
        Tx = miccai2013_interpolate_volume(T_ground(:,:,:,1), interpolantMask, interpolationMask, 'true', longitudinal_z_cut_high);
        Ty = miccai2013_interpolate_volume(T_ground(:,:,:,2), interpolantMask, interpolationMask, 'true', longitudinal_z_cut_high);
        Tz = miccai2013_interpolate_volume(T_ground(:,:,:,3), interpolantMask, interpolationMask, 'true', longitudinal_z_cut_high);

        T = assemble(Tx, Ty, Tz);
        T = normalize(T);
        connections = miccai2013_compute_connections(T, B_ground);
%        reconstruction.interp = connections;
        for cind=1:length(connectionLabels)
            reconstruction.interp.(connectionLabels{cind}) = connections.(connectionLabels{cind});
        end
        toc(ctic)
        
        %% Compute errors
        fprintf('Computing errors...\n');
        reconstruction = miccai2013_compute_all_errors(testHeart, reconstruction, connectionLabels, interpolantMask, interpolationMask, sumInterpolationErrorsOnly, reconIndex);

        %% Display all results for a single parameter
        miccai2013_reconstruction_comparison

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Done processing realizations
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('\n==============================================\n');
    fprintf('\n  DONE COMPUTING REALIZATIONS FOR %s\n', testHeart.name);
    fprintf('\n==============================================\n');
        
    %% Save
    save([dirout, testSubjectLabel, '_reconstruction', '.mat'], 'reconstruction');
end
