function reconstruction = miccai2013_compute_all_errors(testHeart, reconstruction, measurements, interpolantMask, interpolationMask, evaluateInterpolationOnly, uniqueReconstructionID)

% if we are evaluating interpolation only, only sum errors within the
% interpolation mask.
if (evaluateInterpolationOnly)
    errorMask = interpolationMask;
else
    errorMask = interpolationMask | interpolantMask;
end

% Normalization factor: by what number of voxels do we divide the error?
% This is one of the two following: 
% 1) by all voxels in the mask. Combined with evaluateInterpolationOnly
% = true this will yield a linear decrease for any kind of method (so it
% can be misleading) but it goes to zero.
% 2) by all voxels that are interpolated. Since this is an average, it will
% "never" go exactly to zero. This is the preferable scenario as it gives a
% better indication of the performance of each method.

%normalization = numel(find(interpolationMask | interpolantMask));
normalization = numel(find(interpolationMask));


%Lnorm = 1;
% Evaluate both 1-norm and 2-norm
for Lnorm = 1:2
    normLbl = sprintf('norm%i', Lnorm);
    
    for mind=1:length(measurements)
        measurement = measurements{mind};

        measurementGround = testHeart.(measurement);

        % Compute errors
        rdiff = reconstruction.form.(measurement) - measurementGround;
        reconstruction.form.errors.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask), Lnorm) .^ Lnorm / normalization;
        reconstruction.form.errors.n = normalization; 
%         eratio = rdiff(errorMask & abs(measurementGround) > 0) ./ measurementGround(errorMask & abs(measurementGround) > 0);
%         eratio(isnan(eratio)) = 0;
%         max(eratio(:))
%         reconstruction.form.ratios.(normLbl).(measurement)(uniqueReconstructionID) = norm(eratio, Lnorm) .^ Lnorm / normalization;

        rdiff = reconstruction.pca.(measurement) - measurementGround;
        reconstruction.pca.errors.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask), Lnorm) .^ Lnorm / normalization;
        reconstruction.pca.errors.n = normalization; 
%         reconstruction.pca.ratios.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask)./ measurementGround(errorMask), Lnorm) .^ Lnorm / normalization;

        rdiff = reconstruction.interp.(measurement) - measurementGround;
        reconstruction.interp.errors.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask), Lnorm) .^ Lnorm / normalization;
        reconstruction.interp.errors.n = normalization; 
%         reconstruction.interp.ratios.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask)./ measurementGround(errorMask), Lnorm) .^ Lnorm / normalization;

        rdiff = reconstruction.form_interp.(measurement) - measurementGround;
        reconstruction.form_interp.errors.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask), Lnorm) .^ Lnorm / normalization;
        reconstruction.form_interp.errors.n = normalization; 
%         reconstruction.form_interp.ratios.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask)./ measurementGround(errorMask), Lnorm) .^ Lnorm / normalization;

        rdiff = reconstruction.form_shell.(measurement) - measurementGround;
        reconstruction.form_shell.errors.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask), Lnorm) .^ Lnorm / normalization;
        reconstruction.form_shell.errors.n = normalization; 
%         reconstruction.form_shell.ratios.(normLbl).(measurement)(uniqueReconstructionID) = norm(rdiff(errorMask)./ measurementGround(errorMask), Lnorm) .^ Lnorm / normalization;
    end
end

% %% Output
% if (displayOn || displayLittle)
% for measurement=1:length(measurements)
%     figure(1)
%     clf
%     miccai2013_form_output(mask, testHeart, reconstruction.form, reconstruction.pca, reconstruction.interp, measurement, 'x', cmaps{measurement});
%     colormap(customap('redwhiteblue'))
%     export_fig([dirout2, sprintf('%s_comp_%s_%i_%s.pdf', testSubjectLabel,measurement,zn, num2str(round(nu)))], '-transparent', '-q101');
% end
% end

    