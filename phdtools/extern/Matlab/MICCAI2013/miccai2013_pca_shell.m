fprintf('Loading data...\n');
tic
dirdata = '../../data/heart/rats/rathearts.mat';
hearts = loadmat(dirdata);
toc

subjects = fieldnames(hearts);
subject = subjects{1};

dirout = './output/shell/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end
%
vaxis = 'z';
zcut = 80;
hearts = miccai_preprocess(hearts, zcut, true);
subjects = fieldnames(hearts);

% clabels = { 'Tx', 'Ty', 'Tz', 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
measurements = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
clims = { [-0.1, 0.1], [-0.1, 0.1], [-0.5, 0.5], [-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1] };
%
[sx, sy, sz] = size(hearts.(subjects{1}).mask);
zcut = [1:25, zcut:sz];
%%
for reconSubject=1:8

    test_heart = hearts.(subjects{reconSubject});
    maskBound = test_heart.mask;
    maskBound(:,:,zcut) = 0;
    [sx, sy, sz] = size(test_heart.mask);
    isize = size(test_heart.mask);

    trainingIndices = 1:length(subjects)-1;
    trainingIndices(reconSubject) = [];
    trainingSize = length(trainingIndices);

    %
    maskOuter = ~miccai2013_shell_mask(test_heart.mask, 3, 90);
    %image3d(maskOuter);
    %
    % Preprocess 
    clear heartPCA;
    for cind=1:numel(measurements)
        heartPCA.([measurements{cind}]).shell_projected = zeros(size(test_heart.mask));
        heartPCA.([measurements{cind}]).shell_mean = zeros(size(test_heart.mask));
    end

    %for shell_index = 1:15
    allshells = zeros(size(maskBound));
    cumshells = zeros(size(maskBound));
    for shell_index = 1:100
        tic
%         [shell, dtransform] = miccai2013_shell(maskOuter, shell_index, 0.5, 0);
        dthresh = 0.5;
        [shell, dtransform] = miccai2013_shell(maskOuter, shell_index, dthresh, 0);
        toc
        %[shell2, dtransform] = miccai2013_shell(maskOuter, 1, 0.5, 3);
        %[shell3, dtransform] = miccai2013_shell(heart.mask, 2, 1.5, 3);

        maskShell = zeros(isize);
        maskShell(shell) = 1;
        maskShell(~test_heart.mask) = 0;
        maskShell = logical(maskShell);
        maskShell(:,:,zcut) = 0;

    %    shell_count = length(shell);
        shell_count = numel(find(maskShell));

        fprintf('Found shell %i containing %i voxels\n', shell_index, shell_count);

        if (shell_count == 0)
            break
        end

        allshells(maskShell) = shell_index;
        cumshells(maskShell) = cumshells(maskShell) + 1;
%        image3d(cumshells);
%        pause(0.5)

        % Compute PCA 
        nu = 0;

        fprintf('Computing PCA...\n');
        tic
        for cind=1:numel(measurements)
            [dataRecon, V, W, rmean] = miccai_pca(hearts, maskShell, trainingIndices, reconSubject, measurements{cind});

            heartPCA.([measurements{cind}]).shell_projected(maskShell) = heartPCA.([measurements{cind}]).shell_projected(maskShell) + dataRecon(maskShell);
            heartPCA.([measurements{cind}]).shell_mean(maskShell) = heartPCA.([measurements{cind}]).shell_mean(maskShell) + rmean(maskShell);
        end
        toc
    end

    cumshells(cumshells == 0) = 1;
    image3d(cumshells);
    pause(0.5)

    % Scale using shells
    image3d(cumshells);

    for cind=1:numel(measurements)
        heartPCA.([measurements{cind}]).shell_projected = heartPCA.([measurements{cind}]).shell_projected ./ cumshells;
        heartPCA.([measurements{cind}]).shell_mean = heartPCA.([measurements{cind}]).shell_mean ./ cumshells;
    end
    
    % TODO: video with iterative reconstruction
    figure(2)
    for cind=1:numel(measurements)
        view_measure = cind;
        miccai_pca_result_comparator(test_heart.(measurements{view_measure}), heartPCA.(measurements{view_measure}).shell_projected, heartPCA.(measurements{view_measure}).shell_mean, maskBound, [-0.5, 0.5], vaxis)
        colormap(customap('redwhiteblue'))
        export_fig([dirout, subjects{reconSubject}, '_', measurements{view_measure}, '_shell_', num2str(dthresh), '.pdf'], '-transparent', '-q101');
    end
    figure(3)
    image3d(allshells);
    colormap jet
    export_fig([dirout, subjects{reconSubject}, '_shells.pdf'], '-transparent', '-q101');
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% COMPUTE FULL PCA IN ONE GO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    nu = 0;

    fprintf('Computing PCA...\n');
    tic
    for cind=1:numel(measurements)
        [dataRecon, V, W, rmean] = miccai_pca(hearts, maskBound, trainingIndices, reconSubject, measurements{cind});

        heartPCA.(measurements{cind}).projected = dataRecon;
        heartPCA.(measurements{cind}).mean = rmean;
        
%    figure(2)
    view_measure = cind;
    miccai_pca_result_comparator(test_heart.(measurements{view_measure}), heartPCA.(measurements{view_measure}).projected, heartPCA.(measurements{view_measure}).mean, maskBound, [-0.5, 0.5], vaxis)
    colormap(customap('redwhiteblue'))
    export_fig([dirout, subjects{reconSubject}, '_', measurements{view_measure}, '_pca', '.pdf'], '-transparent', '-q101');

    end
    toc
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute shell-by-shell natural masking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nu = 0;

for reconSubject=1:8

    test_heart = hearts.(subjects{reconSubject});
    maskBound = test_heart.mask;
    maskBound(:,:,zcut) = 0;

    trainingIndices = 1:length(subjects)-1;
    trainingIndices(reconSubject) = [];
    trainingSize = length(trainingIndices);

%    for cind=1:numel(measurements)
    for cind=3
        heartPCA.(measurements{cind}).shell_projected_adaptive = zeros(sx, sy, sz);
        heartPCA.(measurements{cind}).shell_mean_adaptive = zeros(sx, sy, sz);
        
        shell_index = 1;
        while(true)
            fprintf('Shell index %d\n', shell_index);
            shell_mask = test_heart.maskShell == shell_index;
%             image3d(shell_mask);
%             pause(0.5)

            [projected, eigenmatrix, W, tmean] = miccai_pca_from_shell(hearts, trainingIndices, reconSubject, measurements{cind}, shell_index, nu);

            if (isempty(projected))
                break;
            end

%            testv = zeros(sx, sy, sz);
%            testv(shell_mask) = projected(shell_mask);
%            image3d(testv);
%            image3d(heartPCA.(measurements{cind}).shell_projected_adaptive);
%            pause(0.5);
            
            heartPCA.(measurements{cind}).shell_projected_adaptive(shell_mask) = projected(shell_mask);
            heartPCA.(measurements{cind}).shell_mean_adaptive(shell_mask) = tmean(shell_mask);

            shell_index = shell_index + 1;
        end
        %image3d(heartPCA.(measurements{cind}).shell_projected_adaptive)
        miccai_pca_result_comparator(test_heart.(measurements{cind}), heartPCA.(measurements{cind}).shell_projected_adaptive, heartPCA.(measurements{cind}).shell_mean_adaptive, maskBound, clims{cind}, vaxis)
        colormap(customap('redwhiteblue'))
        export_fig([dirout, subjects{reconSubject}, '_', measurements{cind}, '_pca_shell_adaptive', '.pdf'], '-transparent', '-q101');
    end
end
%%
% figure(1)
% view_measure = 3;
% miccai_pca_result_comparator(test_heart.(measurements{view_measure}), heartPCA.(measurements{view_measure}).shell_projected_adaptive, heartPCA.(measurements{view_measure}).shell_mean_adaptive, maskBound, [-0.5, 0.5], vaxis)
% colormap(customap('redwhiteblue'))
% export_fig([dirout, subjects{reconSubject}, '_', measurements{view_measure}, '_pca_shell_adaptive', '.pdf'], '-transparent', '-q101');


%% Look at a few slices
figure(2)
for i=2
    sv = heartPCA.(measurements{view_measure}).shell_projected_adaptive;
    sv(~(test_heart.maskShell == i)) = 0;
    image3d(sv, [], [-0.5, 0.5]);
    pause(0.1);
end
colormap(customap('redwhiteblue'))
