function hearts = miccai_preprocess(hearts, varargin)

subjects = fieldnames(hearts);

if (nargin == 4)
    zcut = varargin{1};
    computeShells = varargin{2};
else
    computeShells = false;
end

if (computeShells)
    %% Precompute shells
    tic
    fprintf('Precomputing distance transform shells for all (%d) hearts...\n', length(subjects)-1);
    for i=1:length(subjects)-1
        heart = hearts.(subjects{i});
        mask = heart.mask;
        maskOuter = ~miccai2013_shell_mask(mask, 3, zcut);

        cumshell = zeros(size(mask));
        shell_index = 1;
        [shell, dtransform] = miccai2013_shell(maskOuter, shell_index, 0.5, 0);
        while (true)
            shell = miccai2013_shell(maskOuter, shell_index, 0.5, 0, dtransform);

            maskShell = zeros(size(mask));
            maskShell(shell) = 1;
            maskShell(~mask) = 0;
            maskShell = logical(maskShell);
            maskShell(:,:,zcut:end) = 0;

            shell_count = numel(find(maskShell));

            if (shell_count == 0 || shell_index >= max(size(mask)))
                break
            end

            cumshell(maskShell) = shell_index;
            shell_index = shell_index + 1;
        end

        hearts.(subjects{i}).maskShell = cumshell;
    end
    toc
end


%% Precompute frames
tic
fprintf('Precomputing wall normals for all (%d) hearts...\n', length(subjects)-1);
for i=1:length(subjects)-1
    hlbl = char(subjects(i));

    hearts.(hlbl).name = hlbl;

    fprintf('Estimating wall normals...\n');
    B = estimate_wall_normals(hearts.(hlbl).mask);

    % Apply mask, swap axis if necessary
    T = hearts.(hlbl).T;
    
    % Recompute B, N from wall estimate
    B = orthogonalize(T, B);
    N = cross(B,T,4);

    hearts.(hlbl).T = T;
    hearts.(hlbl).B = B;
    hearts.(hlbl).N = N;
    hearts.(hlbl).Tx = T(:,:,:,1);
    hearts.(hlbl).Ty = T(:,:,:,2);
    hearts.(hlbl).Tz = T(:,:,:,3);

end
clear T;
clear B;
clear N;

toc

%% Precompute connection forms
fprintf('Precomputing connection forms for all (%d) hearts...\n', length(subjects)-1);
tic
for i=1:length(subjects)-1
    hlbl = char(subjects(i));

    T = hearts.(hlbl).T;
    N = hearts.(hlbl).N;
    B = hearts.(hlbl).B;
    
%     T(~mask3) = 0;
%     N(~mask3) = 0;
%     B(~mask3) = 0;
    
    % Compute Jacobian
    DT = jacobian(T);
%     DT(~mask3) = 0;

    % Compute stats
    hearts.(hlbl).TNT = cijv(DT, N, T);
    hearts.(hlbl).TNN = cijv(DT, N, N);
    hearts.(hlbl).TNB = cijv(DT, N, B);
    hearts.(hlbl).TBT = cijv(DT, B, T);
    hearts.(hlbl).TBN = cijv(DT, B, N);
    hearts.(hlbl).TBB = cijv(DT, B, B);
end
toc

