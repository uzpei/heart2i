clear all

%specimen = 'canine/dog042604/'
%specimen = 'canine/dog080803/'
%specimen = 'canine/dog101003/'
%specimen = 'human/'
specimen = 'rats/rat07052008/'
%specimen = 'rats/rat21012008/'
%specimen = 'rats/rat24012008/'
%specimen = 'rats/rat27022008/'
% path = ['/Users/epiuze/Documents/workspace/PhD/phdtools/data/heart/',specimen, 'isotropic/oneFormExtrapolation/'];
path = ['/Volumes/Cortex/Users/piuze/Documents/workspace_PhD/phdtools/data/heart/',specimen, 'isotropic/oneFormExtrapolation/'];

oneForms = { 'cTNT', 'cTNN', 'cTNB', 'cTBT', 'cTBN', 'cTBB', 'cNBT', 'cNBN', 'cNBB' };

% Load error
error = load([path, 'extrapolationError_n3.mat']);
error = error.data;
heart.error = error(:);

vsize = length(error);
m = length(oneForms);

hmag = [-0.8 0.8];
errormag = [0 pi/4];
hres = 0.1;
std = 5;
bins = 1000;
onebins = 1000;

% Filter
garbage = zeros(vsize, 1);

% Load one-forms
for j=1:m
    data = load([path, char(oneForms(j)), '.mat']);
    data = data.data;
    oneFormLabel = char(oneForms(j));
    heart.(oneFormLabel) = data(:);

    fprintf('Setting garbage at %i\n', j);
    garbage(isnan(heart.(oneFormLabel))) = NaN;
    garbage(heart.(oneFormLabel) < hmag(1)) = NaN;
    garbage(heart.(oneFormLabel) > hmag(2)) = NaN;
end

garbage(heart.error > errormag(2)) = NaN;
garbage(isnan(heart.error)) = NaN;

% Apply filter
fprintf('Thresholding...\n');
for j=1:m
    oneFormLabel = char(oneForms(j));
    heart.(oneFormLabel)(isnan(garbage)) = [];
end

length(heart.error)
heart.error(isnan(garbage)) = [];
length(heart.error)

figure(1);
clf
color = 'r';
[hmax, mode] = plotsmooth(heart.error, bins, '', ['values'], color, false, true, errormag, hres, std);
axis([errormag(1) errormag(2) 0 hmax]);
mean(heart.error)

% Begin drawing
for j=1:m
    figure(1+j);
    clf
    oneFormLabel = oneForms(j);
    clabel = char(oneFormLabel);
    [hmax, mode] = plotsmooth(heart.(clabel), onebins, '', ['c_{',clabel(2:4),'}'], color, false, true, hmag, hres, std);
    axis([hmag(1) hmag(2) 0 hmax]);

    fontSize = 18;
    set(gca,'FontSize',fontSize);

end

