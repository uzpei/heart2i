labels = { 'extrapolationError', 'extrapolationError_spherical', 'helicoidError', 'extrapolationError_repeated'};

target = 3;

for j=1:4
vmean=0;
vstd=0;
for i=1:4
    sfield = [labels{j}, targets{target}];

    vmean = vmean + mean(hearts(i).(sfield));
    vstd = vstd + std(hearts(i).(sfield));
end
vmean = vmean / 4;
vstd = vstd / 4;

sprintf('%s: %f +- %f\n', labels{j}, vmean, vstd)
end

for j=1:4
vmean=0;
vstd=0;
for i=6:8
    sfield = [labels{j}, targets{target}];

    vmean = vmean + mean(hearts(i).(sfield));
    vstd = vstd + std(hearts(i).(sfield));
end
vmean = vmean / 3;
vstd = vstd / 3;

sprintf('%s: %f +- %f\n', labels{j}, vmean, vstd)
end

rowLabels = {'one-form', 'homeoid', 'ghm-form', 'constant'};
columnLabels = {'rat', 'human', 'dog'};

matrix2latex(matrix, 'out.tex', 'rowLabels', rowLabels, 'columnLabels', columnLabels, 'alignment', 'c', 'format', '%-6.2f', 'size', 'tiny');
