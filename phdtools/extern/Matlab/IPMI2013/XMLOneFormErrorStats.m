%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function measurements = XMLOneFormErrorStats
close all
clear all

% Global variables
heartsPath = '../../heartsAllIso.xml';
targets = { '_n3', '_n5', '_n7' };
%targets = { '_n3'};
outputDir = 'output/';

% Error and standard parameters
errormag = [0, pi/4];
errorres = 0.1;

species = { 'rat', 'human', 'dog' };
%specimens = { 'rat 1', 'rat 2', 'rat 3', 'rat 4', 'human', 'dog 1', 'dog 2', 'dog 3' };
speciesIndices = { 1:4, 5:5, 6:8 };

measure = 'extrapolationErrorTheta_ghm';
fitdir = 'oneFormExtrapolation/';

outputDir = [outputDir, 'oneform/'];
    
hearts = heartxml(heartsPath);
heartCount = length(hearts);
targetCount = length(targets);

fprintf('%i hearts found\n', heartCount);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Load and preprocess data
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:heartCount
    path = [char(hearts(i).path), fitdir];
        
    % Filter
    for j=1:targetCount
        sfield = [measure, targets{j}];
        loadpath = [path, sfield, '.mat'];

        % Load curvature values
        fprintf('Loading %s...\n', loadpath);

      	% Load first to get the right size
        loaded = load(loadpath);
        loaded = loaded.data;
        loaded = loaded(:);

        % Normalize
        hearts(i).(sfield) = loaded;

        garbage = zeros(length(loaded), 1);
        garbage(isnan(loaded)) = NaN;
        garbage(loaded < errormag(1)) = NaN;
        garbage(loaded > errormag(2)) = NaN;
        
        % Apply filter
        fprintf('Thresholding...\n');
        hearts(i).(sfield)(isnan(garbage)) = [];
    end
end

% Fill in species
measurements = { };
%measurements = zeros(length(targets), 1, length(species));
for j=1:targetCount

    % Fill in merged data for this species
    for s=1:length(species)

        % Count total voxel count for this species
        voxelCount = 0;
        sfield = [measure, targets{j}];
        for i = speciesIndices{s}
            voxelCount = voxelCount + length(hearts(i).(sfield));
        end

        fprintf('Voxel count for species %i = %i\n', s, voxelCount);
        
        mlabel = ['species', int2str(s), 'target', int2str(j)];
        measurements.(mlabel) = zeros(voxelCount, 1);

        prevIndex = 1;
        for i = speciesIndices{s}
            sfield = [measure, targets{j}];

            hdata = hearts(i).(sfield);
            currentLength = length(hdata);
            toIndex = prevIndex + currentLength - 1;

 %           measurements.(['s', s]).(['t', j])(prevIndex:toIndex,j) = hdata; 
            measurements.(mlabel)(prevIndex:toIndex) = hdata; 

            prevIndex = prevIndex + currentLength;
        end

    end
end

% Draw overlay
fprintf('Drawing histograms...\n');
maxHeight = 0;
for j=1:length(targets)
    figure(j)
    clf
    for s=1:length(species)
        color = colorset(s);

        errorbins = 200;
        if (s==1)
            % Rat std
            errorStd = 4;
        elseif (s==2)
            % Humanstd
            errorStd = 1;
        elseif (s==3)
            % Dog std
            errorStd = 1;
        end
        
        mlabel = ['species', int2str(s), 'target', int2str(j)];
        hdata = measurements.(mlabel);

        hold on
        [hmax, mode] = plotsmooth(hdata(:), errorbins, '', 'error (radians)', color, false, false, errormag, errorres, errorStd);
        if (hmax > maxHeight)
            maxHeight = hmax;
        end
        length(hdata);
        hold off
    end
end

% Export figures
for j=1:length(targets)
    h = figure(j);
    hlegend = legend(species);
    set(hlegend,'FontSize',24);

    xhandle = xlabel('error (radians)');
    set(xhandle,'FontSize',22);

    axis([errormag(1) errormag(2) 0 1.1 * maxHeight]);

    formatAndExport([outputDir,'heartsSpeciesOverlayed',targets{j},'.pdf']);
end

% End func
end

function formatAndExport(filename)

    [outputFolder, filenameOnly, ext] = fileparts(filename);
    if ~exist(fullfile('.',outputFolder),'dir')
        fprintf('Creating %s\n', outputFolder);
        mkdir(outputFolder)
    end

    fprintf('Saving %s...\n', filename);
    fontSize = 24;
    grid off
    box off
%	set(gca,'ytick',[])
    set(gca,'FontSize',fontSize);
    export_fig(filename, '-transparent', '-q101');
end

