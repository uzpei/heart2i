%%
clear all

species = {};
species.labels = { 'rat', 'dog', 'human' };
species.rat = {};
species.dog = {};
species.human = {};

% Set computation results
species.measures = { 'cTNT', 'cTNN', 'cTNB', 'cTBT', 'cTBN', 'cTBB', 'cNBT', 'cNBN', 'cNBB' };
species.measures_labels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
species.measures_limits = { [-0.2, 0.2], [-0.2, 0.2], [-0.5, 0.2], [-0.2, 0.2], [-0.2, 0.2], [-0.2, 0.2], [-0.1, 0.1], [-0.2, 0.2], [-0.1, 0.1] }; 

% Path to data
% species.rat.path_data = '../../data/heart/rats/ipmi/';
species.rat.path_data = '../../data/heartResults/rats/';
species.dog.path_data = '../../data/heartResults/canine/';
species.human.path_data = '../../data/heartResults/human/';

% Computation post_paths
species.rat.path_data_post = 'isotropic/oneFormExtrapolation/';
species.dog.path_data_post = 'isotropic/oneFormExtrapolation/';
species.human.path_data_post = 'isotropic/oneFormExtrapolation/';

% Subject paths
species.rat.subjects_path = { 'rat07052008/', 'rat21012008/', 'rat24012008/', 'rat27022008/' };
species.dog.subjects_path = { 'dog042604/', 'dog080803/', 'dog101003/' };
species.human.subjects_path = { './' };

% Subject labels (can be the same as paths)
species.rat.subjects_label = { 'rat07052008', 'rat21012008', 'rat24012008', 'rat27022008' };
species.dog.subjects_label = { 'dog042604', 'dog080803', 'dog101003' };
species.human.subjects_label = { 'human' };

% Subject mean diameter
species.rat.subjects_dm = { 13.25, 11.75, 12.25, 12.25 };
species.dog.subjects_dm = { 48.75, 44.7, 47.8 };
species.human.subjects_dm = { 63.2 };
species.normalization = species.human.subjects_dm{1};

% Species resolution
species.rat.resolution = 0.25;
species.dog.resolution = 0.3125;
species.human.resolution = 0.43;

% Output directory
species.outdir = '../../data/heart/ipmi/';

%% Load all species
fprintf('Loading species...\n');
tic

% For each species
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[SPECIES: %s ]\n', spec);
    
    % For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};
        
        % For each measure
        for measure_it = 1:numel(species.measures)
            measure = species.measures{measure_it};
            lpath = [species.(spec).path_data, species.(spec).subjects_path{subject_it},species.(spec).path_data_post, measure, '.mat'];
            fprintf('Loading from %s...\n', lpath);
            ldata = loadmat(lpath);
            ldata = ldata(:);
            ldata(isnan(ldata)) = [];

            % Apply normalization and save
            species.(spec).(subject).(measure) = ipmi2013r_adjust_measure(species, spec, subject_it, species.normalization, ldata, false);
        end
    end
end
toc

%% Combine species
fprintf('Combining species...\n');
tic
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[PROCESSING SPECIES: %s ]\n', spec);

    % Prepare measure volumes
    for measure_it = 1:numel(species.measures)
        species.(spec).(species.measures{measure_it}) = [];
    end
        
    %% For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};
        
        % For each measure
        for measure_it = 1:numel(species.measures)
            measure = species.measures{measure_it};
            
            measure_data = species.(spec).(subject).(measure);
            measure_data = measure_data(:);
            
            % Remove NaNs
            measure_data(isnan(measure_data)) = [];
            
            % This is a quick hack to mask the data. We should 
            % never have *exactly* (within eps) zero values in any case
%             measure_data(abs(measure_data) <= 0) = [];

            species.(spec).(measure) = [species.(spec).(measure); measure_data];
        end
    end
end
toc

%% Each one-form, plot histogram for each species
figure(2)
clf
mlim1 = [-0.4, 0.4];
mlim2 = [-0.7, 0.4];
mlim3 = [-0.1, 0.1];
linewidth = 3;
nbins = 200;
stdf = 2;
fontSize = 28;

species.measures_limits = { mlim1, mlim1, mlim2, mlim1, mlim1, mlim1, mlim3, mlim3, mlim3 }; 
for measure_it = 1:numel(species.measures)
%for measure_it = 3
    measure_label = species.measures{measure_it};
    
%     subplot(3,3,measure_it)
    clf
    hold on
    alldata = [];
    for spec_it = 1:numel(species.labels)
        hdata = species.(species.labels{spec_it}).(measure_label);
        xlimits = species.measures_limits{measure_it};
        
        hdata(hdata < xlimits(1)) = [];
        hdata(hdata > xlimits(2)) = [];
        
        histnorm(hdata, nbins, stdf, 'LineWidth', linewidth, 'LineStyle', lineset(spec_it), 'Color', colorset(spec_it));
        xlim(xlimits);
        
        alldata = [alldata; hdata];
    end
    hold off
%    title(measure_label);
    legend(species.labels);
    set(gca, 'FontSize', fontSize)
    
    % Add text measures
    xfig = get(gcf, 'Position');
    xt = round(0.01 * xfig(3));
    yt = round(0.79 * xfig(4));
    tfsize = 28;
    lims = axis;
    
    t1 = text(xt, yt - 0,  sprintf('mean %.4f', mean(alldata)), 'Units', 'Pixels');
    t2 = text(xt, yt - tfsize,  sprintf(' std %.4f', std(alldata)), 'Units', 'Pixels');
%     t3 = text(xt, yt - 2*tfsize,  sprintf('median = %.4f', median(alldata)), 'Units', 'Pixels');
%     t3 = text(xt, yt - 2*tfsize,  sprintf('median = %.4f', modebin(alldata, nbins, stdf)), 'Units', 'Pixels');
    
	set(t1, 'FontName', 'Monospaced', 'FontSize', tfsize);
	set(t2, 'FontName', 'Monospaced', 'FontSize', tfsize);
% 	set(t3, 'FontName', 'Monospaced', 'FontSize', tfsize);
    
    export_fig([species.outdir, 'heartsSpeciesOverlayed_', measure_label, '.pdf'], '-transparent', '-q101');
end

%% Load GHM and compare
species.ghm.path = 'isotropic/helicoidfit/';
species.ghm.measures = { 'kt_n3', 'kn_n3', 'kb_n3' };

%% Load all species
fprintf('Loading species...\n');
tic

% For each species
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[SPECIES: %s ]\n', spec);
    
    % For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};
        
        % For each measure
        for measure_it = 1:numel(species.ghm.measures)
            measure = species.ghm.measures{measure_it};
            lpath = [species.(spec).path_data, species.(spec).subjects_path{subject_it},species.ghm.path, measure, '.mat'];
            fprintf('Loading from %s...\n', lpath);
            ldata = loadmat(lpath);
            ldata = ldata(:);
            ldata(isnan(ldata)) = [];

            % Apply normalization and save
            species.(spec).(subject).(measure) = ipmi2013r_adjust_measure(species, spec, subject_it, species.normalization, ldata, false);
        end
    end
end
toc

%% Combine species
fprintf('Combining species...\n');
tic
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[PROCESSING SPECIES: %s ]\n', spec);

    % Prepare measure volumes
    for measure_it = 1:numel(species.ghm.measures)
        species.(spec).(species.ghm.measures{measure_it}) = [];
    end
        
    %% For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};
        
        % For each measure
        for measure_it = 1:numel(species.ghm.measures)
            measure = species.ghm.measures{measure_it};
            
            measure_data = species.(spec).(subject).(measure);
            measure_data = measure_data(:);

            % Remove NaNs
            measure_data(isnan(measure_data)) = [];

            % This is a quick hack to mask the data. We should 
            % never have *exactly* (within eps) zero values in any case
 %             measure_data(abs(measure_data) <= 0) = [];

            species.(spec).(measure) = [species.(spec).(measure); measure_data];
        end
    end
end
toc

%% Each GHM parameter, plot histogram for each species
figure(3)
clf

species.measures_limits = { mlim1, mlim1, mlim2, mlim1, mlim1, mlim1, mlim3, mlim3, mlim3 }; 
for measure_it = 1:numel(species.ghm.measures)
%for measure_it = 3
    measure_label = species.ghm.measures{measure_it};
    
%     subplot(3,3,measure_it)
    clf
    hold on
    alldata = [];
    for spec_it = 1:numel(species.labels)
        hdata = species.(species.labels{spec_it}).(measure_label);
        xlimits = species.measures_limits{measure_it};
        
        hdata(hdata < xlimits(1)) = [];
        hdata(hdata > xlimits(2)) = [];
        
        histnorm(hdata, nbins, stdf, 'LineWidth', linewidth, 'LineStyle', lineset(spec_it), 'Color', colorset(spec_it));
        xlim(xlimits);
        
        alldata = [alldata; hdata];
    end
    hold off
%    title(measure_label);
    legend(species.labels);
    set(gca, 'FontSize', fontSize)
    
    % Add text measures
    xfig = get(gcf, 'Position');
    xt = round(0.01 * xfig(3));
    yt = round(0.79 * xfig(4));
    lims = axis;
    
    t1 = text(xt, yt - 0,  sprintf('mean %.4f', mean(alldata)), 'Units', 'Pixels');
    t2 = text(xt, yt - tfsize,  sprintf(' std %.4f', std(alldata)), 'Units', 'Pixels');
%     t3 = text(xt, yt - 2*tfsize,  sprintf('median = %.4f', median(alldata)), 'Units', 'Pixels');
    
	set(t1, 'FontName', 'Monospaced', 'FontSize', tfsize);
	set(t2, 'FontName', 'Monospaced', 'FontSize', tfsize);
% 	set(t3, 'FontName', 'Monospaced', 'FontSize', tfsize);
    
    export_fig([species.outdir, 'heartsSpeciesOverlayed_', measure_label, '.pdf'], '-transparent', '-q101');
end

%% Load errors
species.ghm.errors = { 'error_n3_n3', 'error_n3_n5', 'error_n3_n7' };

species.form.errors = { 'extrapolationErrorTheta_n3', 'extrapolationErrorTheta_n5', 'extrapolationErrorTheta_n7' };
species.ghmform.errors = { 'extrapolationErrorTheta_ghm_n3', 'extrapolationErrorTheta_ghm_n5', 'extrapolationErrorTheta_ghm_n7' };
species.homeoid.errors = { 'extrapolationErrorTheta_spherical_n3', 'extrapolationErrorTheta_spherical_n5', 'extrapolationErrorTheta_spherical_n7' };
species.constant.errors = { 'extrapolationErrorTheta_repeated_n3', 'extrapolationErrorTheta_repeated_n5', 'extrapolationErrorTheta_repeated_n7' };

%% Load all species
fprintf('Loading species...\n');
tic

elbls = { 'form', 'homeoid', 'ghmform', 'constant' };

% For each species
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[SPECIES: %s ]\n', spec);
    
    % For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};

        % Load all errors
        for error_it = 1:numel(species.(elbl).errors)
            
            % Process GHM
            elbl = 'ghm';
            error = species.(elbl).errors{error_it};
            lpath = [species.(spec).path_data, species.(spec).subjects_path{subject_it},species.ghm.path, error, '.mat'];
            fprintf('Loading from %s...\n', lpath);
            ldata = loadmat(lpath);
            ldata = ldata(:);
            ldata(isnan(ldata)) = [];
            species.(spec).(subject).(elbl).(error) = ldata;
            
            % Process forms
            for model_it = 1:numel(elbls)
                elbl = elbls{model_it};
            
                error = species.(elbl).errors{error_it};
                lpath = [species.(spec).path_data, species.(spec).subjects_path{subject_it},species.(spec).path_data_post, error, '.mat'];
                fprintf('Loading from %s...\n', lpath);
                ldata = loadmat(lpath);
                ldata = ldata(:);
                ldata(isnan(ldata)) = [];
                species.(spec).(subject).(elbl).(error) = ldata;
            end
        end
    end
end
toc

%% Combine species
fprintf('Combining species...\n');
tic
elbls = { 'ghm', 'form', 'homeoid', 'ghmform', 'constant' };

for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[PROCESSING SPECIES: %s ]\n', spec);

    % Prepare measure volumes
    for error_it = 1:numel(species.form.errors)
        for model_it = 1:numel(elbls)
            elbl = elbls{model_it};
            species.(spec).(elbl).(species.(elbl).errors{error_it}) = [];
        end
    end
        
    %% For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};

        for model_it = 1:numel(elbls)
            elbl = elbls{model_it};
            for error_it = 1:numel(species.(elbl).errors)
                error = species.(elbl).errors{error_it};
                measure_data = species.(spec).(subject).(elbl).(error);
                measure_data = measure_data(:);
                species.(spec).(elbl).(error) = [species.(spec).(elbl).(error); measure_data];
            end
        end
    end
end
toc

%% Plot GHM vs GHM-form errors
figure(1)
clf
errorFontSize = 24;
% errorLegendSize = 28;
errorLegendSize = 22;
errorBins = { 100, 200, 200 };
errorStdfs = { 3, 1, 1} ;
errorLineWidth = 2;

species.error_limits = [0, 0.8]; 
error_label = { 'n3', 'n5', 'n7' };
%for error_it = 3
for error_it = 1:numel(species.form.errors)
    
    xlimits = species.error_limits;
    
    clf
    
    hold on
    for spec_it=1:numel(species.labels)
        
        % Load GHM
        error = species.ghm.errors{error_it};
        hdata = species.(species.labels{spec_it}).ghm.(error);
        hdata(hdata < xlimits(1)) = [];
        hdata(hdata > xlimits(2)) = [];
        histnorm(hdata, errorBins{spec_it}, errorStdfs{spec_it}, 'LineWidth', errorLineWidth, 'LineStyle', lineset(1), 'Color', colorset(spec_it));

        % Load GHM-form
        error = species.ghmform.errors{error_it};
        hdata = species.(species.labels{spec_it}).ghmform.(error);
        hdata(hdata < xlimits(1)) = [];
        hdata(hdata > xlimits(2)) = [];
        histnorm(hdata, errorBins{spec_it}, errorStdfs{spec_it}, 'LineWidth', errorLineWidth, 'LineStyle', lineset(2), 'Color', colorset(spec_it));
        
        xlim(xlimits);
        set(gca,'yticklabel',[]);
        set(gca,'YColor','w')
    end
    hold off
    
%     lh = legend({'GHM', 'GHM-form'});
    lh = legend({'rat GHM', 'rat GHM-form', 'dog GHM', 'dog GHM-form', 'human GHM', 'human GHM-form'});
    xh = xlabel('error (radians)');
    set(gca, 'FontSize', errorFontSize)
    set(xh, 'FontSize', errorFontSize)
    set(lh, 'FontSize', errorLegendSize)
    
    export_fig([species.outdir, 'ghmFormHuman_', error_label{error_it}, '.pdf'], '-transparent', '-q101');
end

%% Plot one-form errors
figure(1)
clf
errorLineWidth = 3;
errorLegendSize = 28;

species.error_limits = { [0, 0.7], [0, 0.7], [0, 0.7] }; 
error_label = { 'n3', 'n5', 'n7' };
for error_it = 1:numel(species.form.errors)
    
    xlimits = species.error_limits{error_it};
    error = species.form.errors{error_it};
    
    clf
    
    hold on
    for spec_it=1:numel(species.labels)
        
        % Load one-form
        hdata = species.(species.labels{spec_it}).form.(error);
        hdata(hdata < xlimits(1)) = [];
        hdata(hdata > xlimits(2)) = [];
        histnorm(hdata, errorBins{spec_it}, errorStdfs{spec_it}, 'LineWidth', errorLineWidth, 'LineStyle', lineset(spec_it), 'Color', colorset(spec_it));
        
        xlim(xlimits);
        set(gca,'yticklabel',[]);
        set(gca,'YColor','w')
    end
    hold off
    
    lh = legend({'rat', 'dog', 'human'});
    xh = xlabel('error (radians)');
    set(gca, 'FontSize', errorFontSize)
    set(xh, 'FontSize', errorFontSize)
    set(lh, 'FontSize', errorLegendSize)
    
    export_fig([species.outdir, 'heartsSpeciesOverlayed_extrapolationError_', error_label{error_it}, '.pdf'], '-transparent', '-q101');
end

%% Plot all one-form models
figure(1)
clf
errorLineWidth = 2;
errorLegendSize = 28;

% species.error_limits = { [0, 0.7], [0, 0.7], [0, 0.7] }; 
species.error_limits = { [0, 0.3], [0, 0.3], [0, 0.3] }; 
error_label = { 'n3', 'n5', 'n7' };
elbls = { 'form', 'homeoid', 'ghmform', 'constant' };

for error_it = 1:numel(species.form.errors)
%for error_it = 1
    
    xlimits = species.error_limits{error_it};
    
    clf
    
    hold on
    % Only show human
%     for spec_it=1:numel(species.labels)
    for spec_it = 3
        
        for model_it=1:numel(elbls)
            elbl = elbls{model_it};
            error = species.(elbl).errors{error_it};
                                                                                      
            hdata = species.(species.labels{spec_it}).(elbl).(error);
            hdata(hdata < xlimits(1)) = [];
            hdata(hdata > xlimits(2)) = [];
            histnorm(hdata, errorBins{spec_it}, errorStdfs{spec_it}, 'LineWidth', errorLineWidth, 'LineStyle', lineset(model_it), 'Color', colorset(model_it));
        end
        
        xlim(xlimits);
        set(gca,'yticklabel',[]);
        set(gca,'YColor','w')
    end
    hold off
    
    lh = legend({'one-form', 'homeoid', 'ghm', 'constant'});
    xh = xlabel('error (radians)');
    set(gca, 'FontSize', errorFontSize)
    set(xh, 'FontSize', errorFontSize)
    set(lh, 'FontSize', errorLegendSize)
    
    export_fig([species.outdir, 'humanError_', error_label{error_it}, '.pdf'], '-transparent', '-q101');
end

