clear all
%close all

specimen = 'human/isotropic/'
%specimen = 'rats/rat07052008/isotropic/'
path = ['/Users/epiuze/Documents/workspace/PhD/phdtools/data/heartResults/',specimen];

% Global variables
targets = { '_n3', '_n5', '_n7' };
%targets = { '_n3' };
titles = { 'N_3', 'N_5', 'N_7' };
outputDir = 'output/';

% Error and standard parameters
errormag = [0, 0.3];
errorres = 0.05;
errorstd = 3;

xlabelSize = 22;
figSize = 18;
titleSize = 18;
legendSize = 24;

% Params
measuresPhi = { 'oneFormExtrapolation/extrapolationPhi', 'oneFormExtrapolation/extrapolationPhi_spherical', 'oneFormExtrapolation/extrapolationPhi_ghm', 'oneFormExtrapolation/extrapolationPhi_repeated' };
measuresTheta = { 'oneFormExtrapolation/extrapolationErrorTheta', 'oneFormExtrapolation/extrapolationErrorTheta_spherical', 'oneFormExtrapolation/extrapolationErrorTheta_ghm', 'oneFormExtrapolation/extrapolationErrorTheta_repeated' };

labels = { 'extrapolationError_form', 'extrapolationError_homeoid', 'extrapolationError_ghm', 'extrapolationError_constant'};
legends = { 'one-form', 'homeoid', 'ghm', 'constant'};

fprintf('%i measures found\n', length(measuresPhi));

measuresCount = length(measuresPhi);
targetsCount = length(targets);

% For debugging
%    m = 4;

% Load in data for this heart

%path = char(hearts(i).path);

% Load curvature values
% Load first to get the size of the garbage matrix
loaded = load([path, measuresPhi{1}, targets{1}, '.mat']);

% Filter
garbage = zeros(length(loaded.data(:)), 1);

heart = {};
% Load measures
for j=1:measuresCount
    % Load target for each measure
    for k=1:targetsCount
        sfield = [labels{j}, targets{k}];

        % Process theta
        filename = [path, measuresTheta{j}, targets{k}, '.mat'];
        fprintf('Loading %s...\n', filename);
        measure = load(filename);
        measure = measure.data;
        measure = measure(:);
        length(measure)
        heart.theta.(sfield) = measure;
        garbage(isnan(measure)) = NaN;

        % Process phi
        filename = [path, measuresPhi{j}, targets{k}, '.mat'];
        fprintf('Loading %s...\n', filename);
        measure = load(filename);
        measure = measure.data;
        measure = measure(:);
        heart.phi.(sfield) = measure;
        garbage(isnan(measure)) = NaN;
    end
end

% Apply filter
fprintf('Thresholding...\n');
for j=1:measuresCount
    for k=1:targetsCount
        sfield = [labels{j}, targets{k}];
        heart.theta.(sfield)(isnan(garbage)) = [];
        heart.phi.(sfield)(isnan(garbage)) = [];
    end
end

% Draw a scatter plot for each model
figure(1)
clf

% Normalization factor for all 3D histogram
% 
maxheights = zeros(1, targetsCount);
for k=1:targetsCount
    for j=1:measuresCount
%    subplot(2,2,j);
    figure(j)
    clf
        sfield = [labels{j}, targets{k}];
        dtheta = heart.theta.(sfield);
        dphi = heart.phi.(sfield);
        
        % Draw a scatter plot
%         plot(dtheta, dphi, 'b.', 'MarkerSize', 1);
%         z = sqrt(dtheta .* dtheta + dphi .* dphi);
%         scatter(dtheta, dphi, 1, z);
        x = dtheta;
        y = dphi;
%        z = sqrt(dtheta .* dtheta + dphi .* dphi);

        errorbins = 500;
        [N, C] = hist3([x,y], [errorbins, errorbins]);
        nmax = max(N(:));
        
        if (nmax > maxheights(k))
            maxheights(k) = nmax;
        end
        
        imagesc(N);
%        set(gcf,'renderer','opengl');
%        set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
%        view(270, 90);
%        shading interp
%        grid off

        xlim([75, 425]);
        ylim([1, 75]);
        set(gca,'XTickLabel',sprintf('%.2f|', C{2}(75:(425-75) / 6:425)))
        set(gca,'YTickLabel',sprintf('%.2f|', C{1}(1:(75-1) / 6:75)));
        
        if (j==1 && k == 1)
            Nbck = N;
            Cbck = C;
        end
    end
    
    for j=1:measuresCount
    figure(j)
%    subplot(2,2,j);
        % Set the color map based on the highest bin seen
        caxis([0 maxheights(k)])
%        colorbar
        axis on
        
        yhandle = ylabel('error (radians)');
        xhandle = xlabel('\phi (radians)');
%        thandle = title(legends{j});
        set(xhandle,'FontSize',22);
        set(yhandle,'FontSize',22);
        set(gca,'FontSize',18);
        colormap jet
        set(gca,'YDir','normal')

        set(gca,'XTickLabel',sprintf('%.2f|', Cbck{2}(75:(425-75) / 6:425)))
        set(gca,'YTickLabel',sprintf('%.2f|', Cbck{1}(1:(75-1) / 6:75)));
        
        % Hack for getting the zero
%        if (k==1)
        set(gca, 'XTickLabel', [-2.1, -1.4, -0.7, 0, 0.7, 1.4, 2.1])        
%        end

%        set(thandle,'FontSize',22);
        % For 200 human bins
%         xlim([30, 170]);
%         ylim([1, 50]);
        % For 500 human bins
        
        export_fig(['output/thetaphi_',legends{j},targets{k},'.pdf'], '-transparent', '-q101');

    end
end

% % Plot phi for each model
% errorbins = 200;
% errormag = [-3, 3];
% errorres = 1;
% errorstd = 3;
% maxheights = zeros(1, targetsCount);
% for k=1:targetsCount
%     figure(targetsCount + k)
%     clf
%     for j=1:measuresCount
%         if (j == 1 || j == 4)
%             lineStyle = '--';
%         else
%             lineStyle = '-';
%         end
%         color = colorset(j);
%         sfield = [labels{j}, targets{k}];
%         dphi = heart.phi.(sfield);
%         hold on
%         [hmax, modev] = plotsmooth(dphi, errorbins, '', '', color, false, false, errormag, errorres, errorstd, lineStyle);
%         hold off
%         
%         if (hmax > maxheights(k))
%             maxheights(k) = hmax;
%         end
%         
%     end
% end
% 
% for k=1:targetsCount
%     figure(targetsCount + k)
%     
%     legend(legends)
%     axis([errormag(1) errormag(2) 0 1.01 * max(maxheights(k))]);
%     xhandle = xlabel('\phi (radians)');
%     set(xhandle,'FontSize',22);
%     set(gca,'FontSize',18);
%     export_fig(['output/phiHuman',targets{k},'.pdf'], '-transparent', '-q101');
% end

