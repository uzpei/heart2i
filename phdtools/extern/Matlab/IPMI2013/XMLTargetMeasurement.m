%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function hearts = XMLTargetMeasurement
    close all
    clear all

    % Global variables
    heartsPath = '../../heartsAllIsoProcessed.xml';
    hearts = heartxml(heartsPath);
    targets = { '_n3', '_n5', '_n7' };
    titles = { 'N_3', 'N_5', 'N_7' };
    outputDir = 'output/';

    % Error and standard parameters
    errormag = [0, 0.5];
    errorbins = 500;
    errorres = 0.1;
    errorstd = 1;

    % Params
%     measures = { 'helicoidfit/error', 'oneFormExtrapolation/extrapolationError', 'oneFormExtrapolation/extrapolationError_spherical', 'oneFormExtrapolation/extrapolationError_repeated' };
%     labels = { 'helicoidError', 'extrapolationError', 'extrapolationError_spherical', 'extrapolationError_repeated'};
%     legends = { 'helicoidal', 'one-form', 'focaloid', 'central'};
    measures = { 'oneFormExtrapolation/extrapolationErrorTheta', 'oneFormExtrapolation/extrapolationErrorTheta_spherical', 'oneFormExtrapolation/extrapolationErrorTheta_ghm', 'oneFormExtrapolation/extrapolationErrorTheta_repeated' };
    labels = { 'extrapolationError', 'extrapolationError_spherical', 'helicoidError', 'extrapolationError_repeated'};
    legends = { 'one-form', 'focaloid', 'ghm', 'constant'};

    fprintf('%i hearts found\n', length(hearts));
    fprintf('%i measures found\n', length(measures));

    m = length(hearts);
    n = length(measures);
    o = length(targets);

    % For debugging
%    m = 4;

    for i=1:m

        path = char(hearts(i).path);

        % Load curvature values
        % Load first to get the size of the garbage matrix
        loaded = load([path, measures{1}, targets{1}, '.mat']);

        % Filter
        garbage = zeros(length(loaded.data(:)), 1);

        % Load measures
        for j=1:n
            % Load target for each measure
            for k=1:o
                sfield = [labels{j}, targets{k}];
                
                filename = [path, measures{j}, targets{k}, '.mat'];
                fprintf('Loading %s...\n', filename);

                measure = load(filename);
                measure = measure.data;
                measure = measure(:);
                hearts(i).(sfield) = measure;

                garbage(isnan(measure)) = NaN;
                garbage(measure < errormag(1)) = NaN;
                garbage(measure > errormag(2)) = NaN;
            end
        end

        % Apply filter
        fprintf('Thresholding...\n');
        for j=1:n
            for k=1:o
                sfield = [labels{j}, targets{k}];
                hearts(i).(sfield)(isnan(garbage)) = [];
            end
        end
    end

    % Combine species together
    merged = {};
    
    % Merge measure / target pairs for all hearts
    fprintf('Merging species...\n');
    % For each measure
    for j=1:n
        % For each target
        for k=1:o
            % Merge all hearts
            sfield = [labels{j}, targets{k}];
            merged.(sfield) = [];
            for i=1:m
                hdata = hearts(i).(sfield);
                merged.(sfield) = [merged.(sfield); hdata];
            end
        end
    end
    
    % Overlay one target at a time for all measures
    maxheight = 0;
    fprintf('Drawing histograms...\n');
    for k=1:o
        figure(k)
        clf
        for j=1:n
            color = colorset(j);
            sfield = [labels{j}, targets{k}];
            
            if (j == 1 || j == 4)
                lineStyle = '--';
            else
                lineStyle = '-';
            end
            
            hold on
            [hmax, mode] = plotsmooth(merged.(sfield), errorbins, '', '', color, false, false, errormag, errorres, errorstd, lineStyle);
            hold off
            if (hmax > maxheight)
                maxheight = hmax;
            end
        end
    end
    
    for k=1:o
        figure(k)
        axis([errormag(1) errormag(2) 0 maxheight]);
        hlegend = legend(legends);
        set(hlegend,'FontSize',24);
        formatAndExport([outputDir, titles{k}, '.pdf']);
    end

end

function formatAndExport(filename)

    [outputFolder, filenameOnly, ext] = fileparts(filename);
    if ~exist(fullfile('.',outputFolder),'dir')
        fprintf('Creating %s\n', outputFolder);
        mkdir(outputFolder)
    end

    fprintf('Saving %s...\n', filename);
    fontSize = 18;
    grid off
    box on
%	set(gca,'ytick',[])
    set(gca,'FontSize',fontSize);
    export_fig(filename, '-transparent', '-q101');
end
