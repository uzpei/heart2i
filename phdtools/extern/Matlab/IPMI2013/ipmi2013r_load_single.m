tic
% [T, mask] = opendtmri('../../data/heart/rats/ipmi/rat07052008/isotropic/', 'mask', [2, 1, 3], true);
% [T, mask] = opendtmri('../../data/heart/human/isotropic/', 'mask', [2, 1, 3], true);
%[T, mask] = opendtmri('../../data/heart/rats/rat07052008/isotropic/', 'mask', [2, 1, 3], true);
[T, mask] = opendtmri('../../data/heart/rats/rat21012008/mat/processed/', 'mask', [1, 2, 3], true);
B = estimate_wall_normals(mask);
B = orthogonalize(T, B);
N = cross(B,T,4);
toc

%%
tic
DT = jacobian(T);
DN = jacobian(N);
toc
tic
TNT = cijv(DT, N, T);
TNN = cijv(DT, N, N);
TNB = cijv(DT, N, B);

TBT = cijv(DT, B, T);
TBN = cijv(DT, B, N);
TBB = cijv(DT, B, B);
toc

NBT = cijv(DN, B, T);
NBN = cijv(DN, B, N);
NBB = cijv(DN, B, B);

%%
hdata.T = T;
hdata.N = N;
hdata.B = B;
hdata.TNT = TNT;
hdata.TNN = TNN;
hdata.TNB = TNB;
hdata.TBT = TBT;
hdata.TBN = TBN;
hdata.TBB = TBB;
hdata.mask = mask;
[sx, sy, sz] = size(mask);

%%
% dxTx = DGradient(T(:,:,:,1), [], 2);
% dxTy = DGradient(T(:,:,:,1), [], 1);
% dxTz = DGradient(T(:,:,:,1), [], 3);
[dxTx, dyTx, dzTx] = gradient(T(:,:,:,1));

%%
javaTNB = loadmat('cTNB_rat21012008.mat');
javaNBT = loadmat('cNBT_rat21012008.mat');
javaTBB = loadmat('cTBB_rat21012008.mat');

TXX = loadmat('J00_rat21012008.mat');
TXY = loadmat('J01_rat21012008.mat');
TXZ = loadmat('J02_rat21012008.mat');

TYX = loadmat('J10_rat21012008.mat');
TYY = loadmat('J11_rat21012008.mat');
TYZ = loadmat('J12_rat21012008.mat');

TZX = loadmat('J20_rat21012008.mat');
TZY = loadmat('J21_rat21012008.mat');
TZZ = loadmat('J22_rat21012008.mat');

javadxTx = loadmat('dxTxrat21012008.mat');
javadyTx = loadmat('dyTxrat21012008.mat');
javadzTx = loadmat('dzTxrat21012008.mat');

javaTx = loadmat('Tx_rat21012008.mat');

slicez = 40;

%%
figure(1)
image_diff(TNB, javaTNB, slicez);

%%
figure(1)
image_diff(NBT, javaNBT, slicez);

%%
figure(1)
image_diff(TBB, javaTBB, slicez);

%%
figure(1)
image_diff(dxTz, javadzTx, slicez);

%%
figure(1)
image_diff(T(:,:,:,2), javaTx, slicez);

%% Look at derivatives of T
figure(1)
%DT = jacobian(swap3d(T, [1 2 3]));
image_diff(DT(:,:,:,5), TXX, slicez);

