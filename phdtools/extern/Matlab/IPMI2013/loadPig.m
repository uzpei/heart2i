% Data directory
dir = '../../data/heart/pig/';

% Set dims
sx = 128;
sy = 128;
sz = 91;

% Load normal pig heart
data = load([dir, 'SS_eigenvectors.mat']);
ne = data.Expression1;
e = reshape(ne, 3, 3, sx, sy, sz);
e1x = squeeze(e(2, 1, :, :, :));
e1y = squeeze(e(1, 1, :, :, :));
e1z = -squeeze(e(3, 1, :, :, :));

% Load the eigenvalues
data = load([dir, 'SS_eigenvalues.mat']);
nl = data.Expression1;
l = reshape(nl, 3, sx, sy, sz);
l1 = squeeze(l(1,:,:,:));
l2 = squeeze(l(2,:,:,:));
l3 = squeeze(l(3,:,:,:));

% Load mask
data = load([dir, 'SS_binarymask.mat']);
nmask = data.Expression1;
mask = reshape(nmask, sx, sy, sz);

% Load the fractional anisotropy
fa=reshape(loadmat([dir, 'SS_fa.mat']), sx, sy, sz);
figure(1); image3d(fa);
t = 0.05;
figure(2); image3d(fa .* (fa > t)));

% Set the isotropy
isotropy = [0.703, 0.703, 1];
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Initial Setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
slice = 60;
zcut = 10;
animateConnectedComponents = false;
animateDistance = false;
animateGradient = false;
animateMask = false;

morphoSizeE = 1;
morphoSizeD = 2;

% NOTE: these components only work for data where
% Component size goes in the following order: background -> right
% ventricle -> left ventricle
% Use 1 to get a distance transform that points inwards everywhere
% Use 2 to also include left and right ventricles
% Use 3 to only include left ventricle
% Use 4 to only use left ventricle
componentType = 4;

if componentType == 1
    components = 1;
elseif componentType == 2
    components = 1:3;
elseif componentType == 3
    components = [1,3];
elseif componentType == 4
    components = 3;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Preprocess Mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
maskBounded = mask;

% Bury top 10 slices and bottom 5
for z=sz-zcut:sz
   maskBounded(:,:,z) = 1; 
end
for z=1:2
   maskBounded(:,:,z) = 1; 
end

% Need to run on the negative of the mask
% since connected components are assumed to have intensity 1
maskConnected = 1 - maskBounded;

fprintf('Closing the mask: erosion = %d, dilation = %d\n', morphoSizeE, morphoSizeD);
% First erode the mask
[x,y,z] = ndgrid(-2*morphoSizeE:2*morphoSizeE);
se = strel(sqrt(x.^2 + y.^2 + z.^2) <= morphoSizeE);
%maske = imerode(mask2, se, 'same');
%maske = imclose(mask2, se);
maskConnected = imerode(maskConnected, se, 'same');

% Then dilate
[x,y,z] = ndgrid(-2*morphoSizeD:2*morphoSizeD);
se = strel(sqrt(x.^2 + y.^2 + z.^2) <= morphoSizeD);
maskConnected = imdilate(maskConnected, se, 'same');

% Animate
if (animateMask)
    for i=zstart-1:sz-zcut+1
        figure(1);
        clf
        imagesc(mask(:,:,i));
        colormap gray
        title(['Original mask, Slice = ', int2str(i)]);
        caxis([0 1]);

        figure(2);
        clf
        imagesc(maskBounded(:,:,i));
        colormap gray
        title(['Bounded mask, Slice = ', int2str(i)]);
        caxis([0 1]);

        figure(3);
        clf
        imagesc(maskConnected(:,:,i));
        colormap gray
        title(['Closed mask, Slice = ', int2str(i)]);
        caxis([0 1]);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Compute Connected Components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Computing connected components...\n');

% Compute connected components
cc = bwconncomp(maskConnected);
cpixel = regionprops(cc,'pixellist');
clist = regionprops(cc,'PixelIdxList');
carea = regionprops(cc, 'area');
numr = cc.NumObjects;
fprintf('Found %d connected components\n', numr);

for k=components
    fprintf('Area (voxels) for connected component %d = %d\n', k, carea(k).Area)
end

% Process selected connected components
cc1 = zeros(size(mask));
dtmask = zeros(size(mask));
for k=components
    pxls = clist(k).PixelIdxList;
    cc1(pxls) = 2 * k / numr;
    dtmask(pxls) = 1;
end

% Look at mask of largest components
% and show eroded/dilated mask
if (animateConnectedComponents)

    % Process all connected components
    % Use a unique color for different regions
    ccr = zeros(size(mask));
    for k=1:numr
        pxls = clist(k).PixelIdxList;
        ccr(pxls) = 2 * k / numr;
    end

    for i=1:sz-zcut+2
        figure(1);
        clf
        imagesc(maskBounded(:,:,i));
        title(['Bounded mask at slice ', int2str(i)]);
        colormap gray

        figure(2);
        clf
        imagesc(ccr(:,:,i));
        colormap hsv
        caxis([0 2]);
        title(['All components at slice ', int2str(i)]);

        figure(3);
        clf
        imagesc(cc1(:,:,i));
        colormap hsv
        caxis([0 2]);
        title(['Selected components at slice', int2str(i)]);
    end
    figure(2)
    imagesc(maskBounded(:,:,slice));
    figure(2)
    imagesc(ccr(:,:,slice));
    figure(3);
    imagesc(cc1(:,:,slice));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Compute Distance Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Computing distance transform... This step might take a few minutes.');

% Move back to conventional binary mask (0 = background, 1 = shape)
dtmask = mat2gray(dtmask);
% Compute 3D distance transform
%[dt2, dx, dy, dz, maskd] = dtransform3(mat2gray(1-dtmask),isotropy);
[dt, dx, dy, dz, maskd, dto] = dtransform3(dtmask,isotropy);
fprintf(' Done!\n');

if (animateDistance)
    for i=2:sz-zcut
        figure(2);
        clf
        imagesc(dto(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        caxis auto
        colorbar
        colormap gray
        title(['Original distance transform at slice ', int2str(i)]);

        figure(3);
        clf
        imagesc(dt(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        caxis auto
        colorbar
        colormap gray
        title(['Smoothed distance transform at slice ', int2str(i)]);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Compute Skeletal Points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Computing skeletal points...\n');

% Threshold dx,dy,dz to get rid of skeleton
nthreshold = 0.5;
normask = sqrt(dx.^2 + dy.^2 + dz.^2) >= nthreshold;
dxt = dx .* normask;
dyt = dy .* normask;
dzt = dz .* normask;

% Create a skeletal mask (only include voxels within the original mask)
normask = 1 - ((normask == 0) & (mask == 1));

% Find skeletal points (label 0 in the skeletal mask)
dvals = find(normask ~= 0);
svals = find(normask == 0);

fprintf('%d skeletal points found...', length(svals));

% Create separate position matrices for skeletal poinst and data points
[X,Y,Z] = ind2sub(size(normask), dvals);
[Xq,Yq,Zq] = ind2sub(size(normask), svals);

% Data points
vx = dx(dvals(:));
vy = dy(dvals(:));
vz = dz(dvals(:));

% Interpolate skeletal point
fprintf('Interpolating grid data (nearest neighbor)... This step can take a few minutes.');
dxi = griddata(X,Y,Z,vx,Xq,Yq,Zq, 'nearest');
dyi = griddata(X,Y,Z,vy,Xq,Yq,Zq, 'nearest');
dzi = griddata(X,Y,Z,vz,Xq,Yq,Zq, 'nearest');
% dxi = griddata_lite(X,Y,Z,vx,Xq,Yq,Zq);
% dyi = griddata_lite(X,Y,Z,vy,Xq,Yq,Zq);
% dzi = griddata_lite(X,Y,Z,vz,Xq,Yq,Zq);
fprintf(' Done!\n');

% The interpolation result is flat, need to fill matrices
dxi3 = zeros(sx,sy,sz);
dyi3 = zeros(sx,sy,sz);
dzi3 = zeros(sx,sy,sz);
dxi3(svals(:)) = dxi(:);
dyi3(svals(:)) = dyi(:);
dzi3(svals(:)) = dzi(:);

% Combine the original distance transform gradient with the 
% updated skeletal points and apply original mask
nx = (dxi3 + dxt) .* mask;
ny = (dyi3 + dyt) .* mask;
nz = (dzi3 + dzt) .* mask;

% Negate vectors such that they all point towards the interior
nx = -nx;
ny = -ny;
nz = -nz;

% Normalize the result
[nx, ny, nz] = normalize(nx,ny,nz);

% Orthogonalize the result with respect to the first eigenvector
[projnormx, projnormy, projnormz] = orthogonalize(e1x, e1y, e1z, nx, ny, nz, mask);

dirout = 'processed/';
fprintf('Exporting mask and eigenvectors to %s...', [dir, dirout]);
maskd = -double(mask);
save([dir, dirout, 'mask.mat'], 'maskd');
save([dir, dirout, 'e1x.mat'], 'e1x');
save([dir, dirout, 'e1y.mat'], 'e1y');
save([dir, dirout, 'e1z.mat'], 'e1z');
save([dir, dirout, 'nx.mat'], 'nx');
save([dir, dirout, 'ny.mat'], 'ny');
save([dir, dirout, 'nz.mat'], 'nz');
save([dir, dirout, 'projnormx.mat'], 'projnormx');
save([dir, dirout, 'projnormy.mat'], 'projnormy');
save([dir, dirout, 'projnormz.mat'], 'projnormz');

if (animateGradient)
    for i=1:sz
        figure(4);
        clf
        imagesc(dt(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        colormap gray
        colorbar
        caxis auto
        title(['Original gradient at slice ', int2str(i)]);

        figure(5);
        clf
        quiver(dxt(:,:,i) .* mask(:,:,i),dyt(:,:,i).* mask(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        colormap gray
        colorbar
        caxis auto
        title(['Thresholded gradient at slice ', int2str(i)]);
    %    axis([52.4407   73.5445   55.8323   78.0668]);

        figure(6);
        clf
        quiver(dxi3(:,:,i) .* mask(:,:,i),dyi3(:,:,i).* mask(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        colormap gray
        colorbar
        caxis auto
        title(['Reconstructed gradient at slice ', int2str(i)]);
    %    axis([52.4407   73.5445   55.8323   78.0668]);

        figure(7);
        clf
        quiver(nx(:,:,i), ny(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        colormap gray
        colorbar
        caxis auto
        title(['Reconstructed skeletal + gradient at slice ', int2str(i)]);

    end 
end

