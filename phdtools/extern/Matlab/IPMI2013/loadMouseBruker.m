% Load mouse 12782-01
dtiScan = 6;
dtiReconstruction = 3;
a0Reconstruction = 1;
isotropy = [1 1 2];
zdim = 64;
dilationScale = 2;
brukerFile = '../data/mouse/MouseData/bruker/110909.9X1_11-12782-01/';

% TODO: load and pass mask
mask = extractBrukerFrames(brukerFile, dtiScan, dtiReconstruction, a0Reconstruction, isotropy, zdim, dilationScale)

