function data = ipmi2013r_adjust_measure(species, selected_species, subject_id, normalization, data, resolutionOnly)

    spec = species.(selected_species);

    if (resolutionOnly)
        factor = 1 / (spec.resolution);
    else
        factor = spec.subjects_dm{subject_id} / (spec.resolution * normalization);
%        factor = (1/normalization) / (spec.resolution * (1/spec.subjects_dm{subject_id}));
    end
    
    data = data * factor;

end