%% IPMI 2013 camera-ready revision
clear all

%% Load data
hdata = '../../data/heart/ipmi/species.mat';
fprintf('Loading data...\n');
tic
species = loadmat(hdata);
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute one-form histograms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Fetch a single subject to determine what measures will be used
% measures = fieldnames(species.(species.labels{1}).(species.(species.labels{1}).subjects_label{1}));
measures = {};

measures.one_form.labels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB', 'NBT', 'NBN', 'NBB' };
measures.one_form.limits = { [-0.2, 0.2], [-0.2, 0.2], [-0.5, 0.2], [-0.2, 0.2], [-0.2, 0.2], [-0.2, 0.2], [-0.1, 0.1], [-0.2, 0.2], [-0.1, 0.1] }; 
% Set normalization to bring computations in mm (what is the scale?)
% measures.one_form.normalization = { 1 / (0.25 * 63.2), 1 / (0.3125 * 63.2), 1 / (0.43 * 63.2) };
%measures.one_form.normalization = { 12.5 / (0.25 * 12.5), 47 / (0.8 * 12.5), 63.2 / (1 * 12.5) };
%measures.one_form.normalization = { 1, 1, 1 };

% Select over what subject to perform the normalization
measures.one_form.normalization = species.human.subjects_dm{1};

measures.ghm.labels = { 'kt_n3_n3', 'kn_n3_n3', 'kb_n3_n3' };
measures.ghm.limits = { measures.one_form.limits{1}, measures.one_form.limits{2}, measures.one_form.limits{3} };
measures.ghm.normalization = measures.one_form.normalization;

% Combine all measures
measures.all.labels = [ measures.one_form.labels, measures.ghm.labels ];
measures.all.normalization = [ measures.one_form.normalization, measures.ghm.normalization ];
measures.all.limits = [ measures.one_form.limits, measures.ghm.limits ];

%% Initialize combined volumes
for spec_it = 1:numel(species.labels)
    spec = species.labels{spec_it};
    
    for measure_it = 1:numel(measures.one_form.labels)
        measure_label = measures.one_form.labels{measure_it};
        species.(spec).all.(measure_label) = [];
    end
end

% 1) combine rat, dog, human for 9 one-form
for spec_it = 1:numel(species.labels)
    spec = species.labels{spec_it};
    specie = species.(spec);
    
    subjects = species.(spec).subjects_label;
    for sub_it = 1:numel(subjects)
        subject_label = subjects{sub_it};

        subject = species.(spec).(subject_label);
        mask = subject.mask;

%         normalization = specie.subjects_dm{sub_it} / (specie.resolution * measures.one_form.normalization)

        % Combine measurement with others from species
        for measure_it = 1:numel(measures.one_form.labels)
            measure_label = measures.one_form.labels{measure_it};

            measure = ipmi2013r_adjust_measure(species, spec, sub_it, measures.one_form.normalization, subject.(measure_label));
%             measure = subject.(measure_label) * normalization;
            
            measure(measure < measures.one_form.limits{measure_it}(1)) = [];
            measure(measure > measures.one_form.limits{measure_it}(2)) = [];
            measure(measure == 0) = [];
            
%             % Only try to reconstruct if one-dimensional
%             if (size(measure, 2) == 1)
%                 measure = reconstitute(measure, mask);
%             end
            species.(spec).all.(measure_label) = [species.(spec).all.(measure_label); measure(:)];
        end
    end
end

%% 2) each one-form, plot histogram for each species
figure(2)
clf
for measure_it = 1:numel(measures.one_form.labels)
    measure_label = measures.one_form.labels{measure_it};
    
    subplot(3,3,measure_it)
    hold on
    for spec_it = 1:numel(species.labels)
        histnorm(species.(species.labels{spec_it}).all.(measure_label), 100, 0, 'Color', colorset(spec_it));
    end
    hold off
    title(measure_label);
    legend(species.labels);
end

%% 3) each one-form, plot histogram for species
for spec_it = 1:numel(species.labels)
    spec = species.labels{spec_it};
    
figure
clf
for measure_it = 1:numel(measures.one_form.labels)
    measure_label = measures.one_form.labels{measure_it};
    
    subplot(3,3,measure_it)
    hold on
    for sub_it = 1:numel(species.(spec).subjects_label)
        measure = species.(spec).(species.(spec).subjects_label{sub_it}).(measure_label);
        measure(measure < measures.one_form.limits{measure_it}(1)) = [];
        measure(measure > measures.one_form.limits{measure_it}(2)) = [];
        measure(measure == 0) = [];

        histnorm(measure, 50, 1, 'Color', colorset(sub_it));
    end
    hold off
    title(measure_label);
    legend(species.(spec).subjects_label);
end
end

% figure(3)
% clf
% measure_label = 'kt_n3_n3';
% hold on
% for spec_it = 1:numel(species.labels)
%     histnorm(species.(species.labels{spec_it}).all.(measure_label), 100, 0, 'Color', colorset(spec_it));
% end
% hold off

