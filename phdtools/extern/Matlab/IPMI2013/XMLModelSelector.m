    close all
    clear all

    % Global variables
    heartsPath = '../../heartsAllIsoProcessed.xml';
    hearts = heartxml(heartsPath);
    targets = { '_n3', '_n5', '_n7' };
    titles = { 'N_3', 'N_5', 'N_7' };
    outputDir = 'output/';

    % Error and standard parameters
    errormag = [0, 0.5];
    errorbins = 500;
    errorres = 0.1;
    errorstd = 1;

    % Params
    models = { 'oneFormExtrapolation/extrapolationError', 'oneFormExtrapolation/extrapolationError_spherical', 'helicoidfit/error_n3', 'oneFormExtrapolation/extrapolationError_repeated' };
    modelDimensions = { 9, 5, 3, 1 };
    labels = { 'extrapolationError', 'extrapolationError_spherical', 'helicoidError', 'extrapolationError_repeated'};
    legends = { 'one-form', 'focaloid', 'ghm', 'central'};

    fprintf('%i hearts found\n', length(hearts));
    fprintf('%i measures found\n', length(models));

    m = length(hearts);
    n = length(models);
    o = length(targets);

    % For debugging
%    m = 4;

    for i=1:m

        path = char(hearts(i).path);

        % Load curvature values
        % Load first to get the size of the garbage matrix
        loaded = load([path, models{1}, targets{1}, '.mat']);

        % Filter
        garbage = zeros(length(loaded.data(:)), 1);

        % Load measures
        for j=1:n
            % Load target for each measure
            for k=1:o
                filename = [path, models{j}, targets{k}, '.mat'];
                fprintf('Loading %s...\n', filename);

                measure = load([path, models{j}, targets{k}, '.mat']);
                measure = measure.data;
                measure = measure(:);
                hearts(i).([labels{j}, targets{k}]) = measure;

                garbage(isnan(measure)) = NaN;
                garbage(measure < errormag(1)) = NaN;
                garbage(measure > errormag(2)) = NaN;
            end
        end

        % Apply filter
        fprintf('Thresholding...\n');
        for j=1:n
            for k=1:o
                sfield = [labels{j}, targets{k}];
                hearts(i).(sfield)(isnan(garbage)) = [];
            end
        end
    end

    % Combine species together
    merged = {};
    
    % Merge measure / target pairs for all hearts
    fprintf('Merging species...\n');
    % For each measure
    for j=1:n
        % For each target
        for k=1:o
            % Merge all hearts
            sfield = [labels{j}, targets{k}];
            merged.(sfield) = [];
            for i=1:m
                hdata = hearts(i).(sfield);
                merged.(sfield) = [merged.(sfield); hdata];
            end
        end
    end
    
    % Estimate the dimension of the model
    % We have four models: one-form, spherical, ghm, central
    
    % For each model
    schwarzSelect = zeros(n, k);
    for j=1:n
        % For each target (i.e. neighborhood size)
        for k=1:o
            % Fetch data
            sfield = [labels{j}, targets{k}];
            data = merged.(sfield);

            % Compute and store histogram info
            [heights,centers] = hist(data, errorbins);
            
            % Normalize the histogram
            %heights = heights/trapz(centers,heights);
            heights = heights / sum(heights);
            
            % Bin width
            bwidth = (errormag(2) - errormag(1)) / errorbins;
            
            % Compute the likelihood function
            likelihood = bwidth * sum(centers .* heights);
            
            % data size
            %dataSize = length(data);
            dataSize = 3 * 3 * 3;
            
            % Model dimension
            dimension = modelDimensions{j};
            
%            -log(likelihood)
%            0.5 * dimension * log(dataSize)
            
            % Compute the Schwarz criterion
            schwarzSelect(j, k) = -log(likelihood) - 0.5 * dimension * log(dataSize);
          
        end
    end


