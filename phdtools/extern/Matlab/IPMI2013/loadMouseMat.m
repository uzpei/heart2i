clear all

% hid = '11-12782-02/';
% dir = ['../../data/heart/mouse/control_mice/', hid];
%hid = '11-12657-01/';
%hid = '11-12657-04/';
%hid = '11-12985-02/';
hid = '11-12986-03/';
dir = ['../../data/heart/mouse/TAC_mice/', hid];

% Set dims (assume isotropic, otherwise need to be more careful)
sx = 128;
sy = sx;
sz = sx;

evfile = 'eigenvectors_reg.mat';
lfile = 'eigenvalues_reg.mat';
mfile = 'mask_reg.mat';
dirout = [dir, 'processed', hid];

if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Initial Setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
slice = 40;

% Use a large number for component filling
animateMask = false;
animateConnectedComponents = true;
animateDistance = false;
animateGradient = false;
useSkeletalThresholding = false;

morphoSizeE = 1;
morphoSizeD = 2;

isotropy = [1, 1, 1];

% In general (must be careful, this can change), the following components
% are found:
% Component 1: outside volume (blank)
% Component 2: heart interior
% Component 3: left ventricle
% Component 4: right ventricle
% Component > 4: noise
% If the heart ventricle leaks to the outside volume, use a larger cut
% i.e. trial and error until the third component is large enough.
components = 1;
zcut = 25;
zstart = 5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Load Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = load([dir, evfile]);
ne = data.Expression1;
e = reshape(ne, 3, 3, sx, sy, sz);
e1x = squeeze(e(2, 1, :, :, :));
e1y = squeeze(e(1, 1, :, :, :));
e1z = -squeeze(e(3, 1, :, :, :));

data = load([dir, lfile]);
nl = data.Expression1;
l = reshape(nl, 3, sx, sy, sz);
l1 = squeeze(l(1,:,:,:));
l2 = squeeze(l(2,:,:,:));
l3 = squeeze(l(3,:,:,:));

data = load([dir, mfile]);
nmask = data.Expression1;
mask = reshape(nmask, sx, sy, sz);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Preprocess Mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
maskBounded = mask;

% Bury top 10 slices and bottom 5
% for z=sz-zcut:sz
%    maskBounded(:,:,z) = 1; 
% end
% for z=1:zstart
%    maskBounded(:,:,z) = 1; 
% end

% Need to run on the negative of the mask
% since connected components are assumed to have intensity 1
maskConnected = 1 - maskBounded;

fprintf('Closing the mask: erosion = %d, dilation = %d\n', morphoSizeE, morphoSizeD);
% First erode the mask
[x,y,z] = ndgrid(-2*morphoSizeE:2*morphoSizeE);
se = strel(sqrt(x.^2 + y.^2 + z.^2) <= morphoSizeE);
maskConnected = imerode(maskConnected, se, 'same');

% Then dilate
[x,y,z] = ndgrid(-2*morphoSizeD:2*morphoSizeD);
se = strel(sqrt(x.^2 + y.^2 + z.^2) <= morphoSizeD);
maskConnected = imdilate(maskConnected, se, 'same');

% Animate
if (animateMask)
    for i=1:sz
%         figure(1);
%         clf
%         imagesc(mask(:,:,i));
%         colormap gray
%         title(['Original mask, Slice = ', int2str(i)]);
%         caxis([0 1]);
% 
%         figure(2);
%         clf
%         imagesc(maskBounded(:,:,i));
%         colormap gray
%         title(['Bounded mask, Slice = ', int2str(i)]);
%         caxis([0 1]);

        figure(3);
        clf
        imagesc(maskConnected(:,:,i));
        colormap gray
        title(['Closed mask, Slice = ', int2str(i)]);
        caxis([0 1]);
        pause(0.1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Compute Connected Components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Computing connected components...\n');
tic
% Compute connected components
cc = bwconncomp(maskConnected);
toc
cpixel = regionprops(cc,'pixellist');
clist = regionprops(cc,'PixelIdxList');
carea = regionprops(cc, 'area');
numr = cc.NumObjects;
fprintf('Found %d connected components\n', numr);
for k=1:numr
    fprintf('Area (voxels) for connected component %d = %d\n', k, carea(k).Area)
end
fprintf('Keeping only components\n');
components(:)

% Process selected connected components
cc1 = zeros(size(mask));
dtmask = zeros(size(mask));
for k=components
    pxls = clist(k).PixelIdxList;
    % Fill mask with selected components
    cc1(pxls) = 2 * k / numr;
    dtmask(pxls) = 1;
end

% Dilate the mask
fprintf('Dilating the distance transform mask...\n');
morphoSizeD = 5;
[x,y,z] = ndgrid(-morphoSizeD:morphoSizeD);
se = strel(sqrt(x.^2 + y.^2 + z.^2) <= morphoSizeD);
dtmask = imerode(dtmask, se, 'same');

% Look at mask of largest components
% and show eroded/dilated mask
if (animateConnectedComponents)

    % Process all connected components
    % Use a unique color for different regions
    ccr = zeros(size(mask));
    for k=1:numr
        pxls = clist(k).PixelIdxList;
        ccr(pxls) = 2 * k / numr;
    end

for i=zstart-1:sz-zcut+2
    figure(1);
    clf
    imagesc(dtmask(:,:,i));
    title(['Transform mask ', int2str(i)]);
    colormap gray
    pause(0.05);
    
%     figure(2);
%     clf
%     imagesc(ccr(:,:,i));
%     colormap hsv
%     caxis([0 2]);
%     title(['All components at slice ', int2str(i)]);

%     figure(3);
%     clf
%     imagesc(cc1(:,:,i));
%     colormap hsv
%     caxis([0 2]);
%     title(['Selected components at slice', int2str(i)]);
end
figure(2)
imagesc(maskBounded(:,:,slice));
figure(2)
imagesc(ccr(:,:,slice));
figure(3);
imagesc(cc1(:,:,slice));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Compute Distance Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Computing distance transform... This step might take a few minutes.\n');
tic
% Move back to conventional binary mask (0 = background, 1 = shape)
dtmask = mat2gray(dtmask);

% Compute 3D distance transform
%[dt2, dx, dy, dz, maskd] = dtransform3(mat2gray(1-dtmask),isotropy);
[dt, dx, dy, dz, maskd, dto] = dtransform3(dtmask,isotropy);
fprintf(' Done!\n');
toc

if (animateDistance)
    for i=2:sz-zcut
        figure(2);
        clf
        imagesc(dto(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        caxis auto
        colorbar
        colormap gray
        title(['Original distance transform at slice ', int2str(i)]);

        figure(3);
        clf
        imagesc(dt(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        caxis auto
        colorbar
        colormap gray
        title(['Smoothed distance transform at slice ', int2str(i)]);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Compute Skeletal Points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (useSkeletalThresholding)
    fprintf('Computing skeletal points...\n');
    tic
    % Threshold dx,dy,dz to get rid of skeleton
    nthreshold = 0.5;
    normask = sqrt(dx.^2 + dy.^2 + dz.^2) >= nthreshold;
    dxt = dx .* normask;
    dyt = dy .* normask;
    dzt = dz .* normask;

    % Create a skeletal mask (only include voxels within the original mask)
    normask = 1 - ((normask == 0) & (mask == 1));

    % Find skeletal points (label 0 in the skeletal mask)
    dvals = find(normask ~= 0);
    svals = find(normask == 0);

    fprintf('%d skeletal points found...', length(svals));

    % Create separate position matrices for skeletal poinst and data points
    [X,Y,Z] = ind2sub(size(normask), dvals);
    [Xq,Yq,Zq] = ind2sub(size(normask), svals);

    % Data points
    vx = dx(dvals(:));
    vy = dy(dvals(:));
    vz = dz(dvals(:));

    % Interpolate skeletal point
    fprintf('Interpolating grid data (nearest neighbor)... This step can take a few minutes.');
    dxi = griddata(X,Y,Z,vx,Xq,Yq,Zq, 'nearest');
    dyi = griddata(X,Y,Z,vy,Xq,Yq,Zq, 'nearest');
    dzi = griddata(X,Y,Z,vz,Xq,Yq,Zq, 'nearest');
    % dxi = griddata_lite(X,Y,Z,vx,Xq,Yq,Zq);
    % dyi = griddata_lite(X,Y,Z,vy,Xq,Yq,Zq);
    % dzi = griddata_lite(X,Y,Z,vz,Xq,Yq,Zq);
    fprintf(' Done!\n');
    toc

    % The interpolation result is flat, need to fill matrices
    dxi3 = zeros(sx,sy,sz);
    dyi3 = zeros(sx,sy,sz);
    dzi3 = zeros(sx,sy,sz);
    dxi3(svals(:)) = dxi(:);
    dyi3(svals(:)) = dyi(:);
    dzi3(svals(:)) = dzi(:);

    % Combine the original distance transform gradient with the 
    % updated skeletal points and apply original mask
    nx = (dxi3 + dxt) .* mask;
    ny = (dyi3 + dyt) .* mask;
    nz = (dzi3 + dzt) .* mask;
    
%     % Negate vectors such that they all point towards the interior
%     nx = -nx;
%     ny = -ny;
%     nz = -nz;
else
    nx = dx .* mask;
    ny = dy .* mask;
    nz = dz .* mask;
end

% Normalize the result
[nx, ny, nz] = normalize(nx,ny,nz);

% Orthogonalize the result with respect to the first eigenvector
[projnormx, projnormy, projnormz] = orthogonalize(e1x, e1y, e1z, nx, ny, nz, mask);

fprintf('Exporting mask and eigenvectors to %s...\n', dirout);
maskd = double(mask);
save([dirout, 'mask.mat'], 'maskd');
save([dirout, 'e1x.mat'], 'e1x');
save([dirout, 'e1y.mat'], 'e1y');
save([dirout, 'e1z.mat'], 'e1z');
save([dirout, 'nx.mat'], 'nx');
save([dirout, 'ny.mat'], 'ny');
save([dirout, 'nz.mat'], 'nz');
save([dirout, 'projnormx.mat'], 'projnormx');
save([dirout, 'projnormy.mat'], 'projnormy');
save([dirout, 'projnormz.mat'], 'projnormz');

if (animateGradient)
    for i=1:sz
        figure(4);
        clf
        imagesc(dt(:,:,i));
        axis square
        axis tight
        set(gca,'YDir','normal')
        colormap gray
        colorbar
        caxis auto
        title(['Original gradient at slice ', int2str(i)]);

        figure(5);
        clf
        if (useSkeletalThresholding)
            hold on
            quiver(dxt(:,:,i) .* mask(:,:,i),dyt(:,:,i).* mask(:,:,i), 'r');
            quiver(dxi3(:,:,i) .* mask(:,:,i),dyi3(:,:,i).* mask(:,:,i), 'g');
            quiver(nx(:,:,i), ny(:,:,i), 'b');
            hold off
        else
            quiver(nx(:,:,i), ny(:,:,i), 'b');
        end
        axis square
        axis tight
        set(gca,'YDir','normal')
        colormap gray
        colorbar
        caxis auto
        title(['Thresholded gradient at slice ', int2str(i)]);
    %    axis([52.4407   73.5445   55.8323   78.0668]);

    end 
end

if ~exist(fullfile('.',[dirout, '/img']),'dir')
    mkdir([dirout, '/img'])
end

close all
nims = 10;
%for slice=1:floor(sz / nims):sz
for slice=1:10:sz
    slice
    
    imagesc(mask(:,:,slice));
    daspect([1 1 1]);
    colormap gray;
    title(['slice = ', int2str(slice)]);
    set(gca,'YDir','normal')
    export_fig([dirout, '/img/mask', int2str(slice), '.pdf'], '-q101');
 
    imagesc(dt(:,:,slice));
    daspect([1 1 1]);
    colormap gray;
    title(['slice = ', int2str(slice)]);
    set(gca,'YDir','normal')
    export_fig([dirout, '/img/dt', int2str(slice), '.pdf'], '-q101');
    
    quiver(nx(:,:,slice), ny(:,:,slice), 'b');
    daspect([1 1 1]);
    colormap gray;
    title(['slice = ', int2str(slice)]);
    set(gca,'YDir','normal')
    export_fig([dirout, '/img/projnormal', int2str(slice), '.pdf'], '-q101');
end

