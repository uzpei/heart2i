function XMLHeartcollector(HeartXML, outputBaseFolder, zip)

copyFolders = { 'helicoidfit/', 'oneFormExtrapolation/' };

hearts = heartxml(HeartXML);

%[outputFolder, filename, ext] = fileparts(HeartXML);
%outputFolder = './output';

n = length(hearts);
fprintf('Output directory = %s\n', outputBaseFolder);
fprintf('Collecting helicoid fit and extrapolation from %i hearts\n', n);

% if ~exist(fullfile('.',outputFolder),'dir')
%     mkdir(outputFolder)
% end

for i=1:n
    heart = hearts(i);
    path = char(heart.path);

    fprintf('Processing heart %s \n', path);

    % Extract relative path since we want to preserve that structure
    relpath = relativepath(path);
    
    % Remove ../ and
    relpath = regexprep(relpath, '\.\.\/', '');
    
    % Construct final output folder
    relpath = [outputBaseFolder, relpath];
    
    for i=1:length(copyFolders)
        % Copy fit and extrapolation data
%        [path, copyFolders{i}]
%        [relpath, copyFolders{i}]
    folderFrom = [path, copyFolders{i}];
    folderTo = [relpath, copyFolders{i}];
    
    if ~exist(fullfile('.',folderTo),'dir')
        mkdir(folderTo)
    end

         copyfile(folderFrom, folderTo);
    end
    
    
end

if (zip)
% Compress the output foler
    %[outputFolder, filename, ext] = fileparts(outputBaseFolder);
    zip([regexprep(outputBaseFolder, '\/', ''), '.zip'], outputBaseFolder); 
end

end
