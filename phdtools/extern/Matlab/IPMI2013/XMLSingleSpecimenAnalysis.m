clear all
%close all

%specimen = 'canine/dog042604/'
%specimen = 'canine/dog080803/'
%specimen = 'canine/dog101003/'
specimen = 'human/isotropic/'
%specimen = 'rats/rat07052008/isotropic/'
%specimen = 'rats/rat21012008/'
%specimen = 'rats/rat24012008/'
%specimen = 'rats/rat27022008/'
% path = ['/Users/epiuze/Documents/workspace/PhD/phdtools/data/heart/',specimen, 'isotropic/oneFormExtrapolation/'];
path = ['/Users/epiuze/Documents/workspace/PhD/phdtools/data/heartResults/',specimen];

% Global variables
targets = { '_n3', '_n5', '_n7', '_n9' };
%targets = { '_n3' };
titles = { 'N_3', 'N_5', 'N_7' , 'N_9'};
outputDir = 'output/';

% Error and standard parameters
errormag = [0, 0.3];
errorres = 0.05;
% errormag = [-pi/2, pi/2];
% errorres = 0.1;
 errorstd = 3;

 % Use Sturge's formula for selecting the number of bins
errorbins = 200;
%errorbins = log2(length(heart.([labels{1}, targets{1}]))) + 1

xlabelSize = 22;
figSize = 18;
titleSize = 18;
legendSize = 24;

% Params
% measures = { 'oneFormExtrapolation/extrapolationError', 'oneFormExtrapolation/extrapolationError_spherical', 'helicoidfit/error_n3', 'oneFormExtrapolation/extrapolationError_repeated' };
measures = { 'oneFormExtrapolation/extrapolationErrorTheta', 'oneFormExtrapolation/extrapolationErrorTheta_spherical', 'oneFormExtrapolation/extrapolationErrorTheta_ghm', 'oneFormExtrapolation/extrapolationErrorTheta_repeated' };
%measures = { 'oneFormExtrapolation/extrapolationPhi', 'oneFormExtrapolation/extrapolationPhi_spherical', 'oneFormExtrapolation/extrapolationPhi_ghm', 'oneFormExtrapolation/extrapolationPhi_repeated' };
labels = { 'extrapolationError_form', 'extrapolationError_homeoid', 'extrapolationError_ghm', 'extrapolationError_constant'};
legends = { 'one-form', 'homeoid', 'ghm', 'constant'};

fprintf('%i measures found\n', length(measures));

n = length(measures);
o = length(targets);

% For debugging
%    m = 4;

% Load in data for this heart

%path = char(hearts(i).path);

% Load curvature values
% Load first to get the size of the garbage matrix
loaded = load([path, measures{1}, targets{1}, '.mat']);

% Filter
garbage = zeros(length(loaded.data(:)), 1);

heart = {};
% Load measures
for j=1:n
    % Load target for each measure
    for k=1:o
        sfield = [labels{j}, targets{k}];

        filename = [path, measures{j}, targets{k}, '.mat'];
        fprintf('Loading %s...\n', filename);
        measure = load(filename);
        measure = measure.data;
        measure = measure(:);
        heart.(sfield) = measure;
        garbage(isnan(measure)) = NaN;
        garbage(measure < errormag(1)) = NaN;
        garbage(measure > errormag(2)) = NaN;
    end
end

% Apply filter
fprintf('Thresholding...\n');
for j=1:n
    for k=1:o
        sfield = [labels{j}, targets{k}];
        heart.(sfield)(isnan(garbage)) = [];
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Start by displaying measures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
maxheight = 0;
fprintf('Drawing histograms...\n');

% For each target (neighborhood size)
for k=1:o
    figure(k)
    clf
    % For each measure (model)
    for j=1:n
        color = colorset(j);
        sfield = [labels{j}, targets{k}];

        if (j == 1 || j == 4)
            lineStyle = '--';
        else
            lineStyle = '-';
        end

        hold on
        [hmax, modev] = plotsmooth(heart.(sfield), errorbins, '', '', color, false, false, errormag, errorres, errorstd, lineStyle);
        hold off
        if (hmax > maxheight)
            maxheight = hmax;
        end
    end
end

% Process figures
for k=1:o
        figure(k)
        set(gca,'FontSize',figSize);
        axis([errormag(1) errormag(2) 0 1.01 * maxheight]);
        hlegend = legend(legends);
        xhandle = xlabel('error (radians)');
        set(hlegend,'FontSize',legendSize);
        set(xhandle,'FontSize',xlabelSize);
        
        export_fig(['output/humanError', targets{k}, '.pdf'], '-transparent', '-q101');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Compute model simplex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test error models
% dists = { 'beta', 'birnbaumsaunders', 'gamma', 'inversegaussian', 'lognormal', 'nakagami', 'rayleigh', 'rician', 'weibull' };
% figure(1)
% clf
% For each model
% for j=1:n
%     
%     % Only look at 3-neighbor for now
%     data = heart.([labels{j}, targets{1}]);
% 
%     % For each distribution
%     for d=1:length(dists)
%         subplot(n, length(dists), d + (j-1) * length(dists));
%         dist = dists{d};
%         fprintf('Computing distribution: %s\n', dist);
%         histfit(data,errorbins, dist);
%         title(dist);
%     end
% end

errorModel = 'lognormal';
logdata = zeros(n, o, 2);
meandata = zeros(n, o, 2);
modedata = zeros(n, o, 2);
bres = (errormag(2)-errormag(1))/errorbins;
for j=1:n
    for k=1:o
        data = heart.([labels{j}, targets{k}]);
        
        % Compute measures
        dstd = std(data);
        dmean = mean(data);
        logfit = fitdist(data, errorModel);

        % Geometric mean
        logdata(j, k, 1) = exp(logfit.mu);

        % Geometric std
        logdata(j, k, 2) = exp(logfit.sigma) * logdata(j, k, 1);

        meandata(j, k, 1) = dmean;
        meandata(j, k, 2) = dstd;
        
        % Trick to get the mode of a continuous distribution
        [heights,centers] = hist(data, errorbins);
        lheight = max(heights(:));
        [Y, I] = max(heights(:));
        mde = centers(I);
        modedata(j, k, 1) = mde;
        modedata(j, k, 2) = std(data);

    end
end

% Plot resulting points for models and error distribution parameters
descriptors = {{ '\mu (logNormal)', '\mu', 'mode' }, { '\sigma (logNormal)', '\sigma', '\sigma' }};
%descriptors = {{ '\mu (logNormal)', '\mu', 'mode' }};

for p=1:2
    figure(o+p);
    clf
    for d=1:length(descriptors{p})
        subplot(1, 3, d);

        if (d == 1)
            data = logdata(:,:,p)';
        elseif (d==2)
            data = meandata(:,:,p)';
        elseif (d==3)
            data = modedata(:,:,p)';
        end

        % Maybe use 3*3*3, 5*5*5, 7*7*7
        %xvals = [3^3; 5^3; 7^3];
        xvals = [3; 5; 7; 9];
    %    xvals = [0.001; 0.002; 0.003];
        x = repmat(xvals, [4,1]);
        y = data(:);
        y = y(:);

        for j=1:n
            color = colorset(j);
            hold on
            %'MarkerEdgeColor','k'
             plot(xvals, data(:, j, 1), '-s', 'Color', color,'LineWidth',1,'MarkerFaceColor',color,'MarkerSize',10);
%             plot(xvals, data(:, j, 1), '--s', 'Color', color,'LineWidth',1,'MarkerFaceColor',color,'MarkerSize',10);
%            plot(xvals, data(:, j, 1), 's', 'Color', color,'LineWidth',2,'MarkerFaceColor',color,'MarkerSize',5);
            hold off
        end
%        thandle = title(descriptors{p}{d});
        set(gca,'XTick',[3, 5, 7, 9])
        xlabel('Cubic neighborhood size')
        hlegend = legend(legends);
        set(hlegend,'FontSize',legendSize);
        set(hlegend,'Location','northwest')
        
        % Plot convex hull
        k = convhull(x, y);
%        plot(x(k),y(k),'k-',x,y,'b+')
        hold on
        plot(x(k),y(k),'k--', 'LineWidth', 1)
        hold off
        set(gca,'XTick',[3, 5, 7, 9])
        xhandle = xlabel('Cubic neighborhood size');

        set(gca,'FontSize',figSize);
 %       set(thandle,'FontSize',titleSize);
        set(xhandle,'FontSize',xlabelSize);
        
        % Add some space to the left and right (10% should be enough)
        dx = [xvals(1), xvals(length(xvals))];
        drangex = dx(2) - dx(1);
        xlim(dx + 0.1 * [-drangex, drangex]);
    end
end    

% Copy log normal

% Create figures
figure(o+3);
clf
figure(o+4);
clf

% Extract subplot
figure(o+1);
ahmean = copyobj(subplot(1,3,1),figure(o+3));
figure(o+2);
ahstd = copyobj(subplot(1,3,1),figure(o+4));

figure(o+3)
set(gca,'un','n','pos',[0.1,0.1,0.8,0.8]);
legend(legends, 'Location', 'northwest');

figure(o+4)
set(gca,'un','n','pos',[0.1,0.1,0.8,0.8]);
legend(legends, 'Location', 'northwest');

figure(o+3);
export_fig('output/modelSpaceMean.pdf', '-transparent', '-q101');
figure(o+4);
export_fig('output/modelSpaceStd.pdf', '-transparent', '-q101');


