% hpath = '../../data/heart/ipmi/species.mat';
% fprintf('Loading data...\n');
% tic
% species = loadmat(hpath);
% toc

%%

%hdata = species.rat.rat07052008;
%hdata = species.human.human;
%save hdata
% [T, mask] = opendtmri('../../data/heart/rats/rat07052008/isotropic/', 'mask', [2, 1, 3], true);
% hdata.T = T;

% T = reconstitute(hdata.T, mask3);
% N = reconstitute(hdata.N, mask3);
% B = reconstitute(hdata.B, mask3);
% 
% TNT = reconstitute(hdata.TNT, mask);
% TNN = reconstitute(hdata.TNN, mask);
% TNB = reconstitute(hdata.TNB, mask);
% 
% TBT = reconstitute(hdata.TBT, mask);
% TBN = reconstitute(hdata.TBN, mask);
% TBB = reconstitute(hdata.TBB, mask);

%%
mask = hdata.mask;
mask3 = repmat(mask, [1 1 1 3]);

[sx, sy, sz] = size(mask);

%% Reconstruct orientation from extrapolated mean
h = zeros(sx, sy, sz, 3);

% Compute the error in a n^3 neighborhood
onesv = ones(sx, sy, sz);
error_volume = zeros(sx, sy, sz);

n = 1;
num_neighbors = (2 * n + 1)^3;

T = normalize(T);
T1 = T(:,:,:,1);
T2 = T(:,:,:,2);
T3 = T(:,:,:,3);

N1 = N(:,:,:,1);
N2 = N(:,:,:,2);
N3 = N(:,:,:,3);

B1 = B(:,:,:,1);
B2 = B(:,:,:,2);
B3 = B(:,:,:,3);

% T1m = T1(mask);
% T2m = T2(mask);
% T3m = T3(mask);
% 
% N1m = N(:,:,:,1);
% N1m = N1m(mask);
% N2m = N(:,:,:,2);
% N2m = N2m(mask);
% N3m = N(:,:,:,3);
% N3m = N3m(mask);
% 
% B1m = B(:,:,:,1);
% B1m = B1m(mask);
% B2m = B(:,:,:,2);
% B2m = B2m(mask);
% B3m = B(:,:,:,3);
% B3m = B3m(mask);
% 
% TNTm = TNT(mask);
% TNNm = TNN(mask);
% TNBm = TNB(mask);
% 
% TBTm = TBT(mask);
% TBNm = TBN(mask);
% TBBm = TBB(mask);

% model_labels = { 'oneform', 'homeoid', 'ghmform' };
model_labels = { 'oneform' };
for mod_it = 1:numel(model_labels)
tall = tic;

model = model_labels{mod_it};
    
% Apply threshold based on model selection
% model = one_form, ghm_form, homeoid
%model = 'one_form';
%model = 'one_form';

if (strcmpi(model, 'oneform'))
    % Keep all forms
elseif (strcmpi(model, 'homeoid'))
    TBN = 0;
elseif (strcmpi(model, 'ghmform'))
    TBT = 0;
    TBN = 0;
    TBB = 0;
end

    Tout = zeros(sx, sy, sz, 3);
    Te = zeros(sx, sy, sz, 3);
    sNm = zeros(sx, sy, sz, 3);
    sBm = zeros(sx, sy, sz, 3);
    dummyDot = zeros(sx, sy, sz);
    
    sN1 = zeros(sx, sy, sz);
    sN2 = zeros(sx, sy, sz);
    sN3 = zeros(sx, sy, sz);
    sB1 = zeros(sx, sy, sz);
    sB2 = zeros(sx, sy, sz);
    sB3 = zeros(sx, sy, sz);

%% Iterate over neighbor offsets: [-1, 0, 1] in 3-dimensions
offsets = {};
for i = -n:n
    for j = -n:n
        for k = -n:n
            offsets = [ offsets; [i, j, k] ];
        end
    end
end

Terror = zeros(sx, sy, sz, numel(offsets));

%%
% for i = -n:n
%     for j = -n:n
%         for k = -n:n
tic
parfor offset_it=1:numel(offsets)
    
    offset = offsets{offset_it};
    i = offset(1);
    j = offset(2);
    k = offset(3);
    
    Te = zeros(sx, sy, sz, 3);
%     i = offset(1);
%     j = offset(2);
%     k = offset(3);
    
            % For each neighbor (i,j,k), do:

            %%
%            fprintf('Assembling products...\n');
%            tic
            
            % Compute the current offset for all voxels
%             h(:,:,:,1) = i * onesv;
%             h(:,:,:,2) = j * onesv;
%             h(:,:,:,3) = k * onesv;
            
            % Compute inner products
%             hT = dot(h, T, 4);
%             hN = dot(h, N, 4);
%             hB = dot(h, B, 4);
            hT = (i * onesv) .* T1 + (j * onesv) .* T2 + (k * onesv) .* T3;
            hN = (i * onesv) .* N1 + (j * onesv) .* N2 + (k * onesv) .* N3;
            hB = (i * onesv) .* B1 + (j * onesv) .* B2 + (k * onesv) .* B3;
            
            %% Compute extrapolation at neighbor offset
            sN1 = hT .* TNT;
            sN2 = hN .* TNN;
            sN3 = hB .* TNB;

            sB1 = hT .* TBT;
            sB2 = hN .* TBN;
            sB3 = hB .* TBB;
            
            sN = sN1 + sN2 + sN3;
            sB = sB1 + sB2 + sB3;
%             sN = repmat(sN1 + sN2 + sN3, [1 1 1 3]);
%             sB = repmat(sB1 + sB2 + sB3, [1 1 1 3]);
            
            %% Compute the extrapolated tangent
            Te(:,:,:,1) = sN .* N1 + sB .* B1;
            Te(:,:,:,2) = sN .* N2 + sB .* B2;
 
            Te(:,:,:,3) = sN .* N3 + sB .* B3;
%             Te = normalize(sN .* N + sB .* B);

%            fprintf('Circshifting to neighbor offset...\n');
            
            % Use offset to compare difference to current neighbor
            % Accumulate prediction (contributions might not all sum to
            % same values do to boundary effects)
            
            Tout = T + circshift(Te, -[i, j, k, 0]);
            Tout = normalize(Tout);
            
%             Terror = Terror + 1-abs(dot(T, Tout, 4));
%            fprintf('Computing error...\n');
%             Terror = Terror + acos(abs(dot(T, Tout, 4)));
            Terror(:,:,:,offset_it) = acos(abs(dot(T, Tout, 4)));
end
%         end
%     end
% end
toc

%% Sum error
Terror = sum(abs(Terror), 4) / numel(offsets);
%Terror = Terror / num_neighbors;
Terror(~mask) = 0;
results.(model).error = Terror;

toc(tall)

end

%% Plot results

% % Ground truth
% figure(1)
% image3d(T)
% 
% % Extrapolated
% figure(2)
% image3d(Tout)
% 
% % Error
% figure(3)
% Tdiff = abs(T - Tout);
% image3d(Tdiff);
% Terror = norm(Tdiff(:), 1) / numel(find(mask));

figure(4)
clf
xlimits = [0, 0.7];
for mod_it = 1:3
    error = results.(model_labels{mod_it}).error;
    error = error(:);
    
    error(error == 0) = [];
    error(error < xlimits(1)) = [];
    error(error > xlimits(2)) = [];
    
    size(error)
    
    hold on
    histnorm(error, 100, 1, 'Color', colorset(mod_it));
    hold off
end
% title(sprintf('average error = %.4f', Terror));
legend(model_labels);



