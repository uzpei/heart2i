%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function XMLMeasurementStats
close all
clear all

% Global variables
heartsPath = '../../heartsAllIso.xml';
voxelResolution = { 0.25, 0.25, 0.25, 0.25, 0.43, 0.3125, 0.3125, 0.3125 };
doNormalize = true;
target = '_n7';
outputDir = 'output/';
type = 1;

% Error and standard parameters
errormag = [0, pi / 4];
errorbins = 500;
errorres = 0.1;
errorStd = 2;
xmag = [-0.2, 0.2];
vbins = 1000;
xres = 0.1;

species = { 'rat', 'human', 'dog' };
specimens = { 'rat 1', 'rat 2', 'rat 3', 'rat 4', 'human', 'dog 1', 'dog 2', 'dog 3' };
speciesIndices = { 1:4, 5:5, 6:8 };

exportOverlayed = false;
exportMerged = true;
exportSpeciesOverlayed = true;

% Measurement specific variables
if (type == 1)
    % Set up dirs
    fitdir = 'helicoidfit/';
    outputDir = [outputDir, 'ghm/'];

    % Params
    measures = { 'kt', 'kn', 'kb', 'error' };
    measuredStd = { 3, 3, 3, errorStd }; 
    
    % Set up histogram variables
    mags = { xmag, xmag, xmag, errormag };
    bins = { vbins, vbins, vbins, errorbins };
    res = { xres, xres, xres, errorres };
    
    % Normalize by voxel size / diameter?
    normalizeMeasure = { true * doNormalize, true * doNormalize, true * doNormalize, false * doNormalize};
    
    % Add target?
    targets = { true, true, true, true };

elseif (type == 2)
    % Set up dirs
    fitdir = 'oneFormExtrapolation/';
    outputDir = [outputDir, 'oneform/'];
    
    % Params
    measures = { 'cTNT', 'cTNN', 'cTNB', 'cTBT', 'cTBN', 'cTBB', 'cNBT', 'cNBN', 'cNBB', 'extrapolationError', 'extrapolationError_spherical', 'extrapolationError_repeated' }
    measuredStd = { 1, 2, 2, 1, 1, 1, 0, 0, 0, errorStd, errorStd, errorStd }; 
    
    % Set up histogram variables
    mags = { xmag, xmag, xmag, xmag, xmag, xmag, xmag, xmag, xmag, errormag, errormag, errormag };
    bins = { vbins, vbins, vbins, vbins, vbins, vbins, vbins, vbins, vbins, errorbins, errorbins, errorbins };
    res = { xres, xres, xres, xres, xres, xres, xres, xres, xres, errorres, errorres, errorres };
        
    % Normalize by voxel size / diameter?
    normalizeMeasure = { true, true, true, true, true, true, true, true, true, false, false, false };

    % Add target?
    targets = { false, false, false, false, false, false, false, false, false, true, true, true };

elseif (type == 3)
    % Set up dirs
    fitdir = 'oneFormExtrapolation/';
    outputDir = [outputDir, 'oneform/'];
    
    % Params
    measures = { 'extrapolationError', 'extrapolationError_spherical', 'extrapolationError_repeated' };
    stdt = 4;
    measuredStd = { errorStd, errorStd, errorStd }; 
    
    % Set up histogram variables
    mags = { errormag, errormag, errormag };
    bins = { errorbins, errorbins, errorbins };
    res = { errorres, errorres, errorres };
        
    % Normalize by voxel size / diameter?
    normalizeMeasure = { true, true, true };

    % Add target?
    targets = { false, false, false };

end

m = length(measures);

hearts = heartxml(heartsPath);
n = length(hearts);
%n = 4;

fprintf('%i hearts found\n', n);
fprintf('%i measures found\n', m);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Load and preprocess data
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:n

    path = [char(hearts(i).path), fitdir];

    % Load curvature values
    fprintf('Loading %s...\n', path);

	% Load first to get the right size
    if (targets{1})
        loaded = load([path, measures{1}, target, '.mat']);
    else
        loaded = load([path, measures{1}, '.mat']);
    end

    % Filter
    garbage = zeros(length(loaded.data(:)), 1);

    % Load measures
    for j=1:m
        if (targets{j})
            measure = load([path, measures{j}, target, '.mat']);
        else
            measure = load([path, measures{j}, '.mat']);
        end
        measure = measure.data;
        measure = measure(:);
        
        % Normalize
        if (normalizeMeasure{j})
            hearts(i).(measures{j}) = measure * voxelResolution{i};
        else
            hearts(i).(measures{j}) = measure;
        end
        
        % Print out number of NaN voxels found
        fprintf('Number of NaN voxels = %i\n',length(isnan(hearts(i).(char(measures(j))))));
        
        garbage(isnan(hearts(i).(char(measures(j))))) = NaN;
        garbage(hearts(i).(measures{j}) < mags{j}(1)) = NaN;
        garbage(hearts(i).(measures{j}) > mags{j}(2)) = NaN;
    end

    % Apply filter
    fprintf('Thresholding...\n');
    for j=1:m
        hearts(i).(char(measures(j)))(isnan(garbage)) = [];
    end
end

% Fill in species
measurements = { };
for s=1:length(species)
    indices = speciesIndices{s};
    
    % Count total voxel count for this species
    voxelCount = 0;
    for i=indices
        voxelCount = voxelCount + length(hearts(i).(measures{1}));
    end
    measurements{s} = zeros(voxelCount, m);

    prevIndex = 1;
    for i=indices
        measure = hearts(i).(measures{1});
        currentLength = length(measure);
        toIndex = prevIndex + currentLength - 1;

        for j=1:m
           measurements{s}(prevIndex:toIndex,j) = hearts(i).(measures{j}); 
        end

        prevIndex = prevIndex + currentLength;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    
% 
%          Preprocessing Complete.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        All hearts merged
%       (also computes stats)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

measuresMean = zeros(m, 1);
measuresStd = zeros(m, 1);
measuresMode = zeros(m, 1);

if (exportMerged)
    close all;

    maxHeights = zeros(m,1);

    % For each measurement
    for j=1:m
        figure(j);
        stdh = measuredStd{j};

        fprintf('Drawing %s...\n', measures{j});
        label = '';

        measure = [];
        % Merge hearts
        for i=1:n
            measure = [measure; hearts(i).(measures{j})];
        end
                
        color = colorset(1);
        [hmax, mode] = plotsmooth(measure(:), bins{j}, '', label, color, false, false, mags{j}, res{j}, stdh);
        maxHeights(j) = hmax;

        measuresMean(j) = mean(measure(:));
        measuresStd(j) = std(measure(:));
        measuresMode(j) = mode;
    end

    % Export figures
    % One forms
    for j=1:m
        figure(j);
        axis([mags{j}(1) mags{j}(2) 0 maxHeights(j)]);
%        legend('All hearts');
        if (targets{j})
            formatAndExport([outputDir, 'heartsMerged',char(measures(j)),target,'.pdf']);
        else
            formatAndExport([outputDir, 'heartsMerged',char(measures(j)),'.pdf']);
        end
    end
end % end if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        All hearts overlayed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (exportOverlayed)
    close all;

    maxHeights = zeros(m,1);

%    for i=1:n
    for j=1:m
        figure(j);
        stdh = measuredStd{j};

        hold on

        % Load measuremenets
        fprintf('Drawing %s...\n', measures{j});

        label = '';

        % Begin drawing
        for i=1:n
            color = colorset(i);
            measure = hearts(i).(measures{j});
            [hmax, mode] = plotsmooth(measure, bins{j}, '', label, color, false, false, mags{j}, res{j}, stdh);

            if (hmax > maxHeights(j))
                maxHeights(j) = hmax;
            end
        end
        
        hold off;
    end

    % Export figures
    % One forms
    for j=1:m
        figure(j);
        axis([mags{j}(1) mags{j}(2) 0 maxHeights(j)]);
        legend(specimens);
        
        if (targets{j})
            formatAndExport([outputDir, 'heartsOverlayed_',char(measures(j)),target,'.pdf']);
        else
            formatAndExport([outputDir, 'heartsOverlayed_',char(measures(j)),false,'.pdf']);
        end
    end
end % end if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Hearts per species
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (exportSpeciesOverlayed)
    close all

    % Mean hearts
    maxHeights = zeros(m,1);

    % Measures
    for j=1:m
        figure(j);
        fprintf('Current measurement = %s\n', measures{j});
        label = '';
        
        % Process species
        hold on
        for s=1:length(species)
            sp = species{s};
            color = colorset(s);
            [hmax, mode] = plotsmooth(measurements{s}(:,j), bins{j}, '', label, color, false, false, mags{j}, res{j}, measuredStd{j});

            if (hmax > maxHeights(j))
                maxHeights(j) = hmax;
            end
        end
        hold off
    end

    % Export figures
    % One forms
    for j=1:m
        h = figure(j);
        if (type == 2 && j > 9)
            axis([mags{j}(1) mags{j}(2) 0 max(maxHeights(10:12))]);
        else
            axis([mags{j}(1) mags{j}(2) 0 maxHeights(j)]);
        end
        
        legend(species);

        if (j < 10 && exist('measuresMean', 'var'))
            text = {};
            text{1} = sprintf('mean = %.4f', measuresMean(j));
            text{2} = sprintf('std  = %.4f', measuresStd(j));
            text{3} = sprintf('mode = %.4f', measuresMode(j));
            addTextInfo(h, text);
        end

        if (targets{j})
            formatAndExport([outputDir,'heartsSpeciesOverlayed_',measures{j},target,'.pdf']);
        else
            formatAndExport([outputDir,'heartsSpeciesOverlayed_',measures{j},'.pdf']);
        end
    end

end % end if
end

function formatAndExport(filename)

    [outputFolder, filenameOnly, ext] = fileparts(filename);
    if ~exist(fullfile('.',outputFolder),'dir')
        fprintf('Creating %s\n', outputFolder);
        mkdir(outputFolder)
    end

    fprintf('Saving %s...\n', filename);
    fontSize = 24;
    grid off
    box on
%	set(gca,'ytick',[])
    set(gca,'FontSize',fontSize);
    export_fig(filename, '-transparent', '-q101');
end

function addTextInfo(handle, labels)
    figure(handle)
    xfig = get(handle, 'Position');
    xt = round(0.01 * xfig(3));
    yt = round(0.79 * xfig(4));
    
    n = length(labels);
    fontSize = 22;
    
    for i=1:n
        thandle = text(xt, yt - 0,  labels{i}, 'Units', 'Pixels');
        yt = yt - fontSize;
        set(thandle, 'FontName', 'Monospaced', 'FontSize', fontSize);
    end
end