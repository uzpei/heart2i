%% Load errors
species.form.errors = { 'extrapolationPhi_n3', 'extrapolationPhi_n5', 'extrapolationPhi_n7' };
species.ghmform.errors = { 'extrapolationPhi_ghm_n3', 'extrapolationPhi_ghm_n5', 'extrapolationPhi_ghm_n7' };
species.homeoid.errors = { 'extrapolationPhi_spherical_n3', 'extrapolationPhi_spherical_n5', 'extrapolationPhi_spherical_n7' };
species.constant.errors = { 'extrapolationPhi_repeated_n3', 'extrapolationPhi_repeated_n5', 'extrapolationPhi_repeated_n7' };

%% Load all species
fprintf('Loading species...\n');
tic

elbls = { 'form', 'homeoid', 'ghmform', 'constant' };

% For each species
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[SPECIES: %s ]\n', spec);
    
    % For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};

        % Load all errors
        for error_it = 1:numel(species.(elbl).errors)
            
            % Process GHM
            elbl = 'ghm';
            error = species.(elbl).errors{error_it};
            lpath = [species.(spec).path_data, species.(spec).subjects_path{subject_it},species.ghm.path, error, '.mat'];
            fprintf('Loading from %s...\n', lpath);
            ldata = loadmat(lpath);
            ldata = ldata(:);
            ldata(isnan(ldata)) = [];
            species.(spec).(subject).(elbl).(error) = ldata;
            
            % Process forms
            for model_it = 1:numel(elbls)
                elbl = elbls{model_it};
            
                error = species.(elbl).errors{error_it};
                lpath = [species.(spec).path_data, species.(spec).subjects_path{subject_it},species.(spec).path_data_post, error, '.mat'];
                fprintf('Loading from %s...\n', lpath);
                ldata = loadmat(lpath);
                ldata = ldata(:);
                ldata(isnan(ldata)) = [];
                species.(spec).(subject).(elbl).(error) = ldata;
            end
        end
    end
end
toc

%% Combine species
fprintf('Combining species...\n');
tic
elbls = { 'form', 'homeoid', 'ghmform', 'constant' };

for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[PROCESSING SPECIES: %s ]\n', spec);

    % Prepare measure volumes
    for error_it = 1:numel(species.form.errors)
        for model_it = 1:numel(elbls)
            elbl = elbls{model_it};
            species.(spec).(elbl).(species.(elbl).errors{error_it}) = [];
        end
    end
        
    %% For each specimen
    for subject_it = 1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};

        for model_it = 1:numel(elbls)
            elbl = elbls{model_it};
            for error_it = 1:numel(species.(elbl).errors)
                error = species.(elbl).errors{error_it};
                measure_data = species.(spec).(subject).(elbl).(error);
                measure_data = measure_data(:);
                species.(spec).(elbl).(error) = [species.(spec).(elbl).(error); measure_data];
            end
        end
    end
end
toc

%% Plot all one-form models
figure(1)
clf
errorLineWidth = 2;
errorLegendSize = 28;

% species.error_limits = { [0, 0.7], [0, 0.7], [0, 0.7] }; 
species.error_limits = { [0, 0.3], [0, 0.3], [0, 0.3] }; 
error_label = { 'n3', 'n5', 'n7' };
elbls = { 'form', 'homeoid', 'ghmform', 'constant' };

for error_it = 1:numel(species.form.errors)
%for error_it = 1
    
    xlimits = species.error_limits{error_it};
    
    clf
    
    hold on
    % Only show human
%     for spec_it=1:numel(species.labels)
    for spec_it = 3
        
        for model_it=1:numel(elbls)
            elbl = elbls{model_it};
            error = species.(elbl).errors{error_it};
                                                                                      
            hdata = species.(species.labels{spec_it}).(elbl).(error);
            hdata(hdata < xlimits(1)) = [];
            hdata(hdata > xlimits(2)) = [];
            histnorm(hdata, errorBins{spec_it}, errorStdfs{spec_it}, 'LineWidth', errorLineWidth, 'LineStyle', lineset(model_it), 'Color', colorset(model_it));
        end
        
        xlim(xlimits);
        set(gca,'yticklabel',[]);
        set(gca,'YColor','w')
    end
    hold off
    
    lh = legend({'one-form', 'homeoid', 'ghm', 'constant'});
    xh = xlabel('error (radians)');
    set(gca, 'FontSize', errorFontSize)
    set(xh, 'FontSize', errorFontSize)
    set(lh, 'FontSize', errorLegendSize)
    
    export_fig([species.outdir, 'humanError_', error_label{error_it}, '.pdf'], '-transparent', '-q101');
end