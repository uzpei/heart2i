clear all

%% Load tangent data and mask (T, mask)
%% Load ghm fits
%% Compute connection forms
%% Save result in single matrix for each species

save_frame = true;
save_T = false;

species = {};
species.labels = { 'rat', 'dog', 'human' };
species.computations.labels = { 'ghm' };
species.rat = {};
species.dog = {};
species.human = {};

% Set computation results
species.computations.ghm.measures = { 'error_n3_n3', 'error_n3_n5', 'error_n3_n7', 'kt_n3_n3', 'kt_n3_n5', 'kt_n3_n7', 'kn_n3_n3', 'kn_n3_n5', 'kn_n3_n7', 'kb_n3_n3', 'kb_n3_n5', 'kb_n3_n7' };

% Path to DT-MRI data
species.rat.path_data = '../../data/heart/rats/';
species.dog.path_data = '../../data/heart/canine/';
species.human.path_data = '../../data/heart/human/';

% Path to computation results (can also be the same as DT-MRI data)
species.rat.path_comp = '../../data/heartResults/rats/';
species.dog.path_comp = '../../data/heartResults/canine/';
species.human.path_comp = '../../data/heartResults/human/';

% Set all computation post_paths
species.rat.path_data_post = 'isotropic/';
species.dog.path_data_post = 'isotropic/';
species.human.path_data_post = 'isotropic/';

% Set all computation post_paths
species.rat.ghm_path = 'isotropic/helicoidfit/';
species.dog.ghm_path = 'isotropic/helicoidfit/';
species.human.ghm_path = 'isotropic/helicoidfit/';

% Subject paths
species.rat.subjects_path = { 'rat07052008/', 'rat21012008/', 'rat24012008/', 'rat27022008/' };
species.dog.subjects_path = { 'dog042604/', 'dog080803/', 'dog101003/' };
species.human.subjects_path = { './' };

% Subject labels (can be the same as paths)
species.rat.subjects_label = { 'rat07052008', 'rat21012008', 'rat24012008', 'rat27022008' };
species.dog.subjects_label = { 'dog042604', 'dog080803', 'dog101003' };
species.human.subjects_label = { 'human' };

% Subject mean diameter
species.rat.subjects_dm = { 13.25, 11.75, 12.25, 12.25 };
species.dog.subjects_dm = { 48.75, 44.7, 47.8 };
species.human.subjects_dm = { 63.2 };

% Species resolution
species.rat.resolution = 0.25;
species.dog.resolution = 0.3125;
species.human.resolution = 0.43;

% Specify location where results will be stored
% unique_path = 'ipmi/';
% for i=1:numel(species.labels)
%     spec = species.labels{i};
%     species.(spec).outdir = [species.(spec).path_results, unique_path];
% end
species.outdir = '../../data/heart/ipmi/';

%%
for spec_it=1:numel(species.labels)
    spec = species.labels{spec_it};
    subjects = species.(spec).subjects_label;
    
    fprintf('\n[SPECIES: %s ]\n', spec);
    
    for subject_it=1:numel(species.(spec).subjects_label)
        subject = subjects{subject_it};

        fprintf('--- Processing subject: %s \n', subject);

        % Process DT-MRI data
        fprintf('Loading data\n...');
        tic
        dtmri_path = [species.(spec).path_data, species.(spec).subjects_path{subject_it}, species.(spec).path_data_post];
        [T, mask] = opendtmri(dtmri_path, 'mask', [2, 1, 3], true);
        
        mask3 = repmat(mask, [1 1 1 3]);
        
        image3d(mask)
        pause(0.1)
        toc
        
        
        if (save_frame || save_T)
            species.(spec).(subject).T = T(mask3);
        end
        
        species.(spec).(subject).mask = mask;
        
        % Estimate wall normals
        fprintf('Computing local frame...\n');
        tic
        B = estimate_wall_normals(mask);

        % Compute orthogonal component B and N
        B = orthogonalize(T, B);
        if (save_frame)
            species.(spec).(subject).B = B(mask3);
        end
        
        N = cross(B,T,4);

        if (save_frame)
            species.(spec).(subject).N = N(mask3);
        end
        
        toc
        
        % Compute one-forms
        DT = jacobian(T);
        DN = jacobian(N);

        % Compute stats
        TNT = cijv(DT, N, T);
        TNN = cijv(DT, N, N);
        TNB = cijv(DT, N, B);
        
        TBT = cijv(DT, B, T);
        TBN = cijv(DT, B, N);
        TBB = cijv(DT, B, B);

        NBT = cijv(DN, B, T);
        NBN = cijv(DN, B, N);
        NBB = cijv(DN, B, B);

        species.(spec).(subject).TNT = TNT(mask);
        species.(spec).(subject).TNN = TNN(mask);
        species.(spec).(subject).TNB = TNB(mask);
        species.(spec).(subject).TBT = TBT(mask);
        species.(spec).(subject).TBN = TBN(mask);
        species.(spec).(subject).TBB = TBB(mask);
        species.(spec).(subject).NBT = NBT(mask);
        species.(spec).(subject).NBN = NBN(mask);
        species.(spec).(subject).NBB = NBB(mask);
        
        % Process computations
%         cpath = species.(spec).path_comp;
%         
%         for comp_it=1:numel(species.computations.labels)
%            comp = species.computations.labels{comp_it};
%            
%            comp_path = species.(spec).([comp, '_path']);
%            
%            loadpath = [cpath, species.(spec).subjects_path{subject_it}, comp_path];
%            
%            measures = species.computations.(comp).measures;
%            
%            for measure_it=1:numel(measures)
%                measure = measures{measure_it};
%                measure_path = [loadpath, measure, '.mat'];
%                measure_data = loadmat(measure_path);
%                
%                species.(spec).(subject).(measure) = measure_data(mask);
%            end
%         end
        
    end
end

%% Done processing species, save
% Need to use -v7.3 option since mat file might be larger than 1GB
% (this might be unnecessary in future versions of MATLAB 
outdir = species.outdir;
if ~exist(fullfile('.',outdir),'dir')
    mkdir(outdir)
end

outname = [outdir, 'species.mat'];

fprintf('Saving to %s...\n', outname);

tic
save(outname, 'species', '-v7.3');
toc

%% Test loading back data
clear species
fprintf('Loading data back in...\n');
tic
species = loadmat(outname);
toc
