function z = itocurv(curv)

curv = lower(curv);

if (strcmp(curv, 'k_t'))
    z = 1;
elseif (strcmp(curv, 'k_n'))
    z = 2; 
elseif (strcmp(curv, 'k_b'))
    z = 3; 
elseif (strcmp(curv, 'error'))
    z = 4; 
end