% Example use:
% pnaslaunch('K_B', true, true, false, false);
%function p = pnaslaunch(parameter, showcurv, showextras, showerror, export)
clear all;
close all

parameter = 'K_B';
showcurv = true;
showextras = true;
showerror = true;
export = true;

exportp = '../data/results/mousetest/'


%Human:       63.2
%Dog042604:   48.75
%Dog080803:   44.7
%Dog101003:   47.8
%Rat07052008: 13.25
%Rat21012008: 11.75
%Rat24012008: 12.25
%Rat27022008: 12.25
%To perform the normalization, I used the human diameter as a unit
%i.e. the curvature values obtained for the human were not normalized. 
%All other allDataets were normalized by a factor obtained by dividing their MD
%by that of the human, e.g. the normalization factor is 13.25/63.2 for Rat07052008.

%hearts = '../data/results/fitresult.txt';
heart1 = '../data/results/mousetest/mouse_hr.txt';
heart2 = '../data/results/mousetest/mouse_hr_reg.txt';
hearts = {heart1 heart2};

voxelSpacing = 1;
heartDiameter = 1;
mdref = (1 / voxelSpacing) / 63.2;
mds = heartDiameter .* mdref;

% Once values above are corrected, remove this
mds = [1, 1];

%showcurv = true;
%showextras = false;
%showerror = false;
%export = false;

%export_options = '-nocrop';
export_options = '-q101';

% Determine curvature parameter index
parameter = lower(parameter);
if (strcmp(parameter, 'k_t'))
    pindex = 1;
elseif (strcmp(parameter, 'k_n'))
    pindex = 2; 
elseif (strcmp(parameter, 'k_b'))
    pindex = 3; 
elseif (strcmp(parameter, 'error'))
    pindex = 4; 
end

% Keep track of histogram height
mh = 0;
eh = 0;
mde = 0;
mdc = 0;

% Error plot smoothing (standard deviation)
stdE = 5;

% Default histogram axis resolution
kmag = [-0.7, 0.7];
kres = 0.1;

% Error histogram bins
%bins = round(size(y, 1) / 50);
ebins = 500;

% Determine curvature histogram parameters
% and curvature plot smoothing (standard deviation)
% Use smaller STD for K_T and K_N since we 
% are looking at a sharper peak.
if (pindex == 1)
	stdc = 1;
	bins = 2000;
elseif (pindex == 2)
	stdc = 1;
	bins = 2000;
elseif (pindex == 3)
	kmag = [-0.8, 0.4];
	stdc = 3;
	bins = 1000;
end

% Load data in
allData = [];
allDatae = [];
for i=1:length(hearts)
	heart = hearts{i};

	if (i == 1)
		color = 'k';
	elseif (i == 2)
		color = 'r';
	elseif (i == 3)
		color = 'g';
	elseif (i == 4)
		color = 'b';
    end
	
    heart
	dataf = importdata(heart, ',', 2);
	data = dataf.data(:, pindex) * mds(i);
	%interp(data, 10);
		
 	allData = [allData; data];
 
	figure(i);
	if (showcurv)
		plotsmooth(data, bins, ['rat ', int2str(i), ' -- ', heart], parameter, 'r', true, true, kmag, kres, stdc);
	end
	
    % Clean up name
%    fprintf('Cleaning up name...\n');
    lastslash = 0;
    nsize = length(hearts{i});
    ht = hearts{i};
    for j=1:nsize
       if (strcmp(ht(j), '/') == 1)
           lastslash = j + 1;
       end
    end
	nameout = [exportp, ht(lastslash:nsize - 4), parameter, '.pdf']

	if (export) 
		export_fig(nameout, export_options);
	end
	
	% Error plot 
 %   fprintf('Updating error...\n');
	if (showerror)
		hold on
		figure(length(hearts) + 3);
		datae = dataf.data(:, itocurv('error'));
	 	allDatae = [allDatae; datae];
		[h, mode] = plotsmooth(datae, ebins, 'Error plot for rat hearts', 'error (radians)', color, false, false, [0, 1], kres, stdE);
		mde = mde + mode / length(hearts);
		hold off
		if (h > eh)
			eh = h;
		end
    end
		
 %   fprintf('Updating composite...\n');

	% Show composite figure of all rat histograms
	if (showextras)
		% Composite figure
		figure(length(hearts) + 2)
		hold on
		[h, mode] = plotsmooth(data, bins, 'Overlay of rat hearts', parameter, color, false, false, kmag, kres, stdc);
		hold off
			
		mdc = mdc + mode / length(hearts);
		if (h > mh)
			mh = h;
		end
	end
end

%fprintf('Combining multiple hearts...\n');
% Normalize histograms
if (showextras)

	% Compute average
	figure(length(hearts) + 1);
	plotsmooth(allData, bins, 'rat average', parameter, 'r', true, true, kmag, kres, stdc);

	if (export)
		export_fig([exportp, 'meanRat', parameter,'.pdf'], export_options);
	end
	
	% Adjust and export composite figure height
	figure(length(hearts) + 2)
	legend('rat 1', 'rat 2', 'rat 3', 'rat 4');
	axis([kmag(1) kmag(2) 0 1.1 * mh]);
	
	% Display error stats
	xfig = get(gcf, 'Position');
	xt = round(0.01 * xfig(3));
	yt = round(0.79 * xfig(4));
	t1 = text(xt, yt - 0,  ['mode = ', num2str(mdc), ' rad (', num2str(radtodeg(mdc), '%.4f'), ' deg)'], 'Units', 'Pixels');
	
	errorMean = mean(allData)
	t2 = text(xt, yt - 10, ['mean = ', num2str(errorMean), ' rad (', num2str(radtodeg(errorMean), '%.4f'), ' deg)'], 'Units', 'Pixels');

	errorStd = std(allData);
	t3 = text(xt, yt - 20, [' std = ', num2str(errorStd), ' rad (', num2str(radtodeg(errorStd), '%.4f'), ' deg)'], 'Units', 'Pixels');

	set(t1, 'FontName', 'Monospaced');
	set(t2, 'FontName', 'Monospaced');
	set(t3, 'FontName', 'Monospaced');
	
	if (export)
		export_fig([exportp, 'ratOverlay', parameter, '.pdf'], export_options);
	end
end

if (showerror)
	% Adjust and export error figure height
	figure(length(hearts) + 3)
	legend('rat 1', 'rat 2', 'rat 3', 'rat 4');
	axis([0 1 0 1.1 * eh]);
	grid on
    grid minor
    set(gca, 'XTick', [0, pi/8, 2*pi/8]);

    lticks = [1:8] * pi / 32
    
    [hx,hy] = format_ticks(gca,{'$\frac{\pi}{32}$','$\frac{\pi}{16}$','$\frac{3\pi}{32}$','$\frac{\pi}{8}$','$\frac{5\pi}{32}$','$\frac{3\pi}{16}$','$\frac{7\pi}{32}$','$\frac{\pi}{4}$'},[],lticks);
	% Display error stats
	xfig = get(gcf, 'Position');
	xt = round(0.01 * xfig(3));
	yt = round(0.79 * xfig(4));
	t1 = text(xt, yt - 0,  ['mode = ', num2str(mde), ' rad (', num2str(radtodeg(mde), '%.4f'), ' deg)'], 'Units', 'Pixels');
	
	errorMean = mean(allDatae)
	t2 = text(xt, yt - 10, ['mean = ', num2str(errorMean), ' rad (', num2str(radtodeg(errorMean), '%.4f'), ' deg)'], 'Units', 'Pixels');

	errorStd = std(allDatae);
	t3 = text(xt, yt - 20, [' std = ', num2str(errorStd), ' rad (', num2str(radtodeg(errorStd), '%.4f'), ' deg)'], 'Units', 'Pixels');

	set(t1, 'FontName', 'Monospaced');
	set(t2, 'FontName', 'Monospaced');
	set(t3, 'FontName', 'Monospaced');
	
	if (export)
		export_fig([exportp, 'ratError.pdf'], export_options);
	end	
end
