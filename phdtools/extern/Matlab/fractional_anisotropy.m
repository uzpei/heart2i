function fa = fractional_anisotropy(l1, l2, l3) 

fa = sqrt(0.5) * sqrt((l1-l2).^2 + (l2-l3).^2 + (l3-l1).^2) ./ sqrt(l1.^2+l2.^2+l3.^2);