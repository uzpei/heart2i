function N = sixneighbors(Isize)
% A = rand(3,3,2);
% Isize = size(A);

sx = Isize(1);
sy = Isize(2);
sz = Isize(3);

vol = zeros(sx, sy, sz);
vol(:) = 1:numel(vol);
N = zeros(numel(vol), 6);

n1 = circshift(vol, [1 0 0]);
n2 = circshift(vol, [0 -1 0]);
n3 = circshift(vol, [-1 0 0]);
n4 = circshift(vol, [0 1 0]);
n5 = circshift(vol, [0 0 1]);
n6 = circshift(vol, [0 0 -1]);

N(:,1) = n1(:);
N(:,2) = n2(:);
N(:,3) = n3(:);
N(:,4) = n4(:);
N(:,5) = n5(:);
N(:,6) = n6(:);


