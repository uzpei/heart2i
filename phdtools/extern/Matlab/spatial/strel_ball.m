function se = strel_ball(bsize);

[x,y,z] = ndgrid(-bsize:bsize);
se = strel(sqrt(x.^2 + y.^2 + z.^2) <= bsize);
