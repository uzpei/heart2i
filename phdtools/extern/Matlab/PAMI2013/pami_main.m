clear all
setpath;

% Setup paths and parameters
frame = 'frames.Fnorm';
outdir = './output/cijks/';
lims = [-1, 1];
specieslabels = {'rat', 'dog'};

% Experiment parameters
experiment_options.Bounds = [1, 1, 1];
experiment_options.ElementOrder = [1 2 3];
% experiment_options.PostPath = '';
experiment_options.PostPath = 'registered_clean';
% experiment_options.PostPath = '.';
experiment_options.MaskedIndexing = true;
experiment_options.SinglePrecision = false;
experiment_options.OptimizationFrameOnly = false;
experiment_options.FrameNormal = false;
experiment_options.FrameCurl = false;
experiment_options.FrameEig = false;
experiment_options.PrecomputeDirectConnections = false;
experiment_options.IterativeSmoothing = [false, 0, 0];
experiment_options.CropVolumes = false;

% Compute cell
experiment_options_cells = struct2paramcell(experiment_options);

% Load all subjects, frames, and connection forms
%ratpath = '../../data/heart/rat/atlas_FIMH2013_cropped/';
ratpath = '../../data/heart/rat/atlas_FIMH2013/';
% ratpath = '../../data/heart/rat/';
%ratpath = '../../data/heart/rat/sample2/rat07052008/';

subjects = compute_frames_con_all(ratpath, experiment_options_cells{:});


%% 0. This is needed to fix a corrupted mask for every subject
for i=1:length(subjects)
    % threshold = 0.3;
    % mask = loadmat([subject.path, 'b0.mat']);
    % mask = (mask ./ max(mask(:))) > threshold;
    figure(1);image3d(subjects(i).mask);
    subjects(i).mask = (abs(subjects(i).e1(:,:,:,1)) > 0);
    figure(2);image3d(subjects(i).mask);
    pause(0.2);
%     save_subject(subjects(i));
end

%%
image3d(subjects(1).mask)

%% Apply cropping
subjects(1).bounds = { 1:53, 1:66, 1:61};
subjects(2).bounds = { 1:54, 1:66, 2:62};
subjects(3).bounds = { 1:59, 1:66, 25:84};
subjects(4).bounds = { 1:54, 1:66, 18:75};
subjects(5).bounds = { 1:64, 1:66, 1:70};
subjects(6).bounds = { 1:53, 1:66, 19:70};
subjects(7).bounds = { 1:54, 1:66, 17:69};
subjects(8).bounds = { 1:61, 1:68, 17:68};

for sindex=1:length(subjects)
    subject_crop = crop_subject(subjects(sindex), subjects(sindex).bounds);
    figure(1); image3d(subjects(sindex).mask);figure(2);image3d(subject_crop.mask);
    subject_crop.path = [subject_crop.path(1:end-1), '_clean/'];
    save_subject(subject_crop);
end

%%
testsub = 6;
figure(1);image3d(subjects(testsub).mask);
testpath = [subjects(testsub).path(1:end-1), '_clean/'];
[V, mask] = open_principal_direction(testpath, testpath, 'e1', 0, 0);
figure(2);image3d(mask);

%% Recompute all (including distance transforms)
experiment_options.PostPath = 'registered_clean';
experiment_options_cells = struct2paramcell(experiment_options);
subjects = compute_frames_con_all(ratpath, experiment_options_cells{:});

%%
for sindex=1:length(subjects)
    image3d(subjects(sindex).dt_endo);
    pause(0.1);
end
%%%%%%%%%%%%%%%%%%%%%%%%
%% Smoothing experiment
%%%%%%%%%%%%%%%%%%%%%%%%
for testsub=1:8
    vpath = subjects(testsub).path;
    its = 5;
    stds = 0.4;
    maskpath = vpath;
    mask = openmask(maskpath);
    mask3 = repmat(mask, [1 1 1 3]);
    V = open_principal_direction(vpath, maskpath, 'e1', 0, 0);
    Vs = open_principal_direction(vpath, maskpath, 'e1', its, stds);
    figure(1);image3d(subjects(testsub).e1);
    figure(2);image3d(V);
    figure(3);image3d(Vs);
    waitforbuttonpress;
end

%%

[e1, ~, ~, mask, dtEndo, dtEpi] = opendtmri('../../data/heart/rat/atlas_FIMH2013/');

%% Check mask
maskS = (abs(Vs) > 1e-8);
figure(1);image3d(maskS);
figure(2);image3d(maskS - mask3); supertitle(['mask difference (', int2str(numel(find(maskS - mask3))), ' voxels)']);
numel(find(maskS - mask3))
%%
experiment_smoothing(ratpath, experiment_options_cells{:});


%% Cropping
% bounds = [1, 1, 0.2];
subject = subjects(1);
[sx, sy, sz] = size(subject.mask);
bounds = { round(sx/2):sx, 1:sy, round(0.4 * sz):round(0.46 * sz) };
subject = crop_subject(subject, bounds);
image3d(subject.mask)
numel(find(subject.mask))
% save
outpath = [ratpath, 'sampleSmall/'];
mkdir(outpath);
V = subject.mask;
save([outpath, 'mask.mat'], 'V');
V = subject.dtEndo;
save([outpath, 'dt_endo.mat'], 'V');
V = subject.dtEpi;
save([outpath, 'dt_epi.mat'], 'V');
V = squeeze(subject.e1(:,:,:,1));
save([outpath, 'e1x.mat'], 'V');
V = squeeze(subject.e1(:,:,:,2));
save([outpath, 'e1y.mat'], 'V');
V = squeeze(subject.e1(:,:,:,3));
save([outpath, 'e1z.mat'], 'V');
    
%% Test distance transform epi-endo merging
mask = subjects(1).mask;

T = subjects(1).e1;
nmag = 5; 
DTendo = cardiac_lv_distance(mask, nmag, nmag, 'endocardium');
DTepi = cardiac_lv_distance(mask, nmag, nmag, 'epicardium');

figure(1);image3d(DTendo);
figure(2);image3d(DTepi);

%% Display gradient
[T, Nendo] = frame_from_distance_transform(T, DTendo, mask);
[T, Nepi] = frame_from_distance_transform(T, DTepi, mask);

% Align the endocardium gradient to the epi
% Nendo = Nendo .* repmat(sign(dot(DTendo, DTepi,4)), [1 1 1 3]);
figure(1); clf; quiverslice(mask, Nendo, 'z');
figure(2); clf; quiverslice(mask, Nepi, 'z');

%% Average gradients
% Navg = orthogonalize(T, 0.5 * (Nendo + Nepi));
Navg = 0.5 * (Nendo + Nepi);
figure(3); clf; quiverslice(mask, Navg, 'z');

%%
dogpath = '../../data/heart/rat/smooth/';
experiment_options.PostPath = '.';
% dogpath = '../../data/heart/canine/sample2/';
% experiment_options.PostPath = 'mat';
experiment_options_cells = struct2paramcell(experiment_options);
subjects2 = compute_frames_con_all(dogpath, experiment_options_cells{:});
subjects2.normalization = 0.3125;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 5.4 Direct parameter computations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
result_direct = experiment_direct_computations(outdir, frame, specieslabels, subjects, subjects2);
form_multivolume_plotter(outdir, specieslabels, result_direct{:}); 
save_mkdir([outdir, 'cijks_direct.mat'], result_direct);
%cformplot(result_direct(1).cijks, subjects(1).mask);

%%%%%%%%%%%%%%%%%%
%% OPTIMIZATION %%
%%%%%%%%%%%%%%%%%%

result_optimized = pipeline_experiment(outdir, frame, specieslabels, subjects, subjects2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Beginning of direct computations and colsed-form solution %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Beginning of connection parameter method comparison experiments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%image3d(subjects(1).e1)

% TODO: 
% -Smoothing + direct computation experiment
% -Neighborhood size
% -Model selection: leave one out
% -Fix e1 flipping: implement automatically in open-dtmri?
% -Image-close mask at runtime?

%%
image3d(subjects(1).e1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Can preload using this %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = load([outdir, 'cijk_direct.mat']);
result_direct = data.data;
data = load([outdir, 'cijk_optimized.mat']);
result_optimized = data.data;

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Optimized Computation
%%%%%%%%%%%%%%%%%%%%%%%%%
result_optimized = experiment_optimized_computations(subjects, frame);
save_mkdir([outdir, 'cijk_optimized.mat'], result_optimized);

%%
clf
cformplot(result_optimized(1).cijks, subjects(1).mask);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compare direct and optimized histograms
%% TODO: merge all subjects together
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clf
sub_ind = 1;
fdirect = fieldnames(result_direct(sub_ind).cijks);
foptimized = fieldnames(result_optimized(sub_ind).cijks);
nbins = 200;
stdf = 1;
numc = min(numel(fdirect), numel(foptimized));
for i=1:numc
    subplot(2,3,i);
    hold on
    vol = result_direct(sub_ind).cijks.(fdirect{i});
    histnorm_thresholded(vol(subjects(sub_ind).mask), nbins, stdf, 'Color', colorset(1), 'limits', lims);
    
    vol = result_optimized(sub_ind).cijks.(foptimized{i});
    histnorm_thresholded(vol(subjects(sub_ind).mask), nbins, stdf, 'Color', colorset(2), 'limits', lims);
    hold off
    axis tight
    th = title(foptimized{i});
    legend({'direct', 'optimized'});
    set(gca, 'FontSize', 20)
    set(th, 'FontSize', 20)
end
export_fig([outdir, 'cforms_computations.pdf'], '-transparent', '-q101', '-nocrop');

%%
animate3d(result.cijks.c123);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Closed-form Computation
%%%%%%%%%%%%%%%%%%%%%%%%%%%
experiment_closedform_computations(subjects, frame);

% result = experiment_optimized_computations(subjects, frame);
% save_mkdir([outdir, 'cijk_closed.mat'], result);

%%%%%%%%%%%%%%%%%%%%%
% Model selection
%%%%%%%%%%%%%%%%%%%%%
computeModel = false;
if (computeModel)
    experiment_model_selection(subjects, frame);
end

%%%%%%%%%%%%%%%%%%%%%%%%
%% Smoothing experiment
%%%%%%%%%%%%%%%%%%%%%%%%

%%
experiment_smoothing(ratpath, experiment_options_cells{:});

