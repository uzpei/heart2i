function result = experiment_model_selection(subjects, frame)   

    % clabels = { '121', '122', '123', '131', '132', '133', '231', '232', '233' };
    elbls = { '121', '122', '123', '131', '132', '133'};
    cijks_opti = {};

        [sx, sy, sz] = size(subject.mask);
        F = framefield(subject.frames.Fnorm.T, [], subject.frames.Fnorm.B);
        % sampling = 0.01;
        sampling = 1;
        inds = find(subject.mask > 0);
        if (sampling < 1)
            samples = randsample(inds, round(sampling * numel(inds)));
            maskSample = zeros(size(subject.mask));
            maskSample(samples) = 1;
            image3d(maskSample);
        else
            maskSample = subject.mask;
            samples = inds;
        end

    errors = {};
    for opti_cijk = 1:numel(elbls)
        lbl = ['c', elbls{opti_cijk}];
        disp(lbl)
        cmask = zeros(1, 9);
        cmask(opti_cijk) = 1;
        disp(cmask)
        [copt, cerror, mask_sample] = cijks_optimized(F, subject.mask, samples, 'cmask', cmask, 'verbose', 'off');
        cijks_opti.(lbl) = copt;
        errors{opti_cijk} = cerror;
    end

    % Use all
    [copt, cerror, mask_sample] = cijks_optimized(F, subject.mask, samples, 'cmask', [1 1 1 1 1 1 0 0 0], 'verbose', 'off');
    cijks_opti.all = copt;
    errors{numel(elbls)+1} = cerror;

    % Save
    save('opti_errors.mat', 'errors');
    save('opti_cijks.mat', 'cijks_opti');

    % Sort errors and plot with labels
    elbls{numel(elbls)+1} = 'all';

    figure(3)
    clf
    merrors = [];
    for i=1:length(errors)
        evol = errors{i};
    %     merrors(i) = mean(evol(evol < 0.2));
        merrors(i) = mean(evol(:));
    end

    %%
    [errors_sorted, inds] = sort(merrors);
    clbls_sorted = elbls(inds);
    plot(1:numel(errors_sorted), rad2deg(errors_sorted), '.-k', 'MarkerSize', 20);
    set(gca,'XLim',[0.9 0.1+numel(errors)])
    set(gca,'XTick',1:1:numel(errors))
    set(gca,'XTickLabel', clbls_sorted)
    xlabel('Connection parameter used')
    ylabel('mean volume error (deg)')
    title('rat07052008');
    %set(gcf, 'FontSize', 32);
    set(gca, 'FontSize', 24);
    export_fig('cforms_importance.pdf', '-transparent', '-q101', '-nocrop');

    % % Plot histograms
    % figure(1)
    % clf
    % hold on
    % for i=1:length(errors)
    % %    histnorm(errors{i}, 200, 1, 'Color', colorset(i));
    %     histnorm_thresholded(errors{i}(:), 5, 2, 'Color', colorset(i));
    % end
    % legend(elbls);
    % hold off
    % 
    %% Plot slices
    figure(2)
    cmin = Inf;
    cmax = -Inf;
    for i=1:length(errors)
        if (max(errors{i}(:)) > cmax)
            cmax = max(errors{i}(:));
        end
        if (min(errors{i}(:)) < cmin)
            cmin = min(errors{i}(:));
        end
    end
    for saxis = 1:3
    clf
    for i=1:length(errors)
        subplot(3,3,i);
        image3dslice(errors{i}, saxis);
        caxis([cmin cmax]);
        axis image
        axis off
        title(elbls{i});
    end
    supercolorbar
    export_fig(['cforms_slice', num2str(saxis),'_rat07052008.pdf'], '-transparent', '-q101', '-nocrop');
    end

    %%
    % figure(2)
    % cmin = Inf;
    % cmax = -Inf;
    % for i=1:length(errors)
    %     if (max(errors{i}(:) ./ errors{numel(elbls)}(:)) > cmax)
    %         cmax = max(errors{i}(:) ./ errors{numel(elbls)}(:));
    %     end
    %     if (min(errors{i}(:) ./ errors{numel(elbls)}(:)) < cmin)
    %         cmin = min(errors{i}(:) ./ errors{numel(elbls)}(:));
    %     end
    % end
    % for saxis = 1:3
    % clf
    % for i=1:length(errors)
    %     subplot(3,3,i);
    %     image3dslice(errors{i} ./ errors{numel(elbls)}, saxis);
    %     caxis([cmin 10]);
    %     axis image
    %     axis off
    %     title(elbls{i});
    % end
    % supercolorbar
    % export_fig(['cforms_slice', num2str(saxis),'.pdf'], '-transparent', '-q101', '-nocrop');
    % end
end

