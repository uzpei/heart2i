filepath = '../../data/heart/rat/sampleSubset/';
maskpath = [filepath, 'b0.mat'];
volumeName = 'e1';
%F_raw = open_principal_direction(filepath, maskpath, volumeName, 0, 0);

mask = loadmat(maskPath);
[sx, sy, sz] = size(mask);

[path, volumeName, 'x.mat']

elementOrder = [1, 2, 3];
F1 = zeros([sx, sy, sz, 3]);
F1(:,:,:,elementOrder(1)) = loadmat([path, volumeName, 'x.mat']);
F1(:,:,:,elementOrder(2)) = loadmat([path, volumeName, 'y.mat']);
F1(:,:,:,elementOrder(3)) = loadmat([path, volumeName, 'z.mat']);
F1 = normalize(F1);

%% Apply smoothing
iterations = 10;
stdev = 0.4;
F_smooth = open_principal_direction(filepath, maskpath, volumeName, iterations, stdev);

%%
F_rawx = squeeze(F_raw(:,:,:,1));
F_smoothx = squeeze(F_smooth(:,:,:,1));
figure(1);
image3dslice(F_rawx, 'z');
figure(2);
image3dslice(F_smoothx, 'z');
