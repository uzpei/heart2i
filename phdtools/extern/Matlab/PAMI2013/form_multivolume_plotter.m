% Each arg is a datastructure containing subjects for a species
% and where each subject has a .cijks(121, 122, 123,..., 233) field
function form_multivolume_plotter(outdir, species_labels, varargin)

% Histogram parameters
clims = [-0.6, 0.6];
nbins = 150;
stdf = 1;
legendSize = 32;
legendSizeSmall = 6;
axisSize = legendSize;
linewidth = 2;

clbls = { 'c121', 'c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233'};

ncijks = length(clbls);
numspecies = nargin - 2;

% figure(1);
clf;
% figure(2);clf;
for cindex = 1:ncijks
    
    % Combine species
    for spindex = 1:numspecies
        subjects = varargin{spindex}.subjects;
        
        cijks = combine_volumes(subjects, ['cijks.', clbls{cindex}]);
        cijks = cijks(cijks < clims(2) & cijks > clims(1));
        
%         figure(1);
%         subplot(3,3,cindex);
%         hplot(cijks, nbins, stdf, colorset(spindex), styleset(spindex), linewidth);
%         figure(2);
        hplot(cijks, clims, nbins, stdf, colorset(spindex), styleset(spindex), linewidth);
    end
    
    % Could add a title for each cijk
%     title(clbls{cindex});
    
%     figure(1);
%     subplot(3,3,cindex);
%     lh = legend(species_labels);
%     set(lh, 'FontSize', legendSizeSmall);
%     figure(2);
    lh = legend(species_labels);
    set(lh, 'FontSize', legendSize);
    set(gca, 'FontSize', axisSize);
    export_fig([outdir, 'cforms_direct_species_', clbls{cindex}, '.pdf'], '-transparent', '-q101', '-nocrop');
    clf
end

% figure(1)
% export_fig([outdir, 'cforms_direct_species.pdf'], '-transparent', '-q101', '-nocrop');

end

function hplot(v, clims, nbins, stdf, color, linestyle, linewidth)
    hold on
    histnorm_thresholded(v, nbins, stdf, 'color', color, 'linestyle', linestyle, 'linewidth', linewidth);
    xlim(clims)
    set(gca,'YTick',[])
    set(gca,'YColor',[1 1 1])
    hold off
end