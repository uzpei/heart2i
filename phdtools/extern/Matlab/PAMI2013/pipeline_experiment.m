function pipeline_experiment(subjects)

% clear all
% setpath;
% % searchPath = '../../data/heart/rat/sample3/';
% searchPath = '../../data/heart/rat/';
searchPath = '../../data/heart/rat/atlas_FIMH2013/';
clbls = { 'c121', 'c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233'};

if (isempty(subjects))
    experiment_options.Bounds = [1, 1, 0.7];
    experiment_options.ElementOrder = [1 2 3];
    experiment_options.PostPath = 'registered';
    experiment_options.MaskedIndexing = true;
    experiment_options.SinglePrecision = false;
    experiment_options.OptimizationFrameOnly = false;
    experiment_options.FrameNormal = true;
    experiment_options.FrameCurl = false;
    experiment_options.FrameEig = false;
    experiment_options.PrecomputeDirectConnections = false;
    experiment_options.IterativeSmoothing = [false, 0, 0];
    experiment_options_cells = struct2paramcell(experiment_options);

    % Load all subjects, frames, and connection forms
    subjects = compute_frames_con_all(searchPath, experiment_options_cells{:});
end

%% Process subjects
for subject_index = 1:length(subjects)
    subject = subjects(subject_index);
    outdir = [subject.path, 'cartanfitting/'];
    model = 'full'; 
    cijks = cijks_optimized_java(subject.e1, subject.frames.Fnorm.B, subject.mask, model, outdir);
    subjects(subject_index).cijks = cijks;
    
    %% Flatten and remove nans
    for cindex = 1:length(clbls)
        cijk = cijks.(clbls{cindex})(subject.mask);
        cijk = cijk(~isnan(cijk));
        subjects(subject_index).cijks_flat.(clbls{cindex}) = cijk;
    end
end

%%
ncijks = numel(fieldnames(subjects(1).cijks));
clf
for cindex = 1:ncijks
    hold on
    cijks = combine_volumes(subjects, ['cijks_flat.', clbls{cindex}]);
    cijks = cijks(~isnan(cijks) & cijks < 1 & cijks > -1);
    histnorm_thresholded(cijks, 200, 1, 'color', colorset(cindex));
end
legend(clbls)
% axis([-1, 1, 0, 1])
