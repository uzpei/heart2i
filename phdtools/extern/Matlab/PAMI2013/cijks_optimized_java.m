% Choice of cmodel: full, homeoid, helicoid
function cijks = cijks_optimized_java(e1, B, mask, cmodel, opath)

% Preprocessing step
% 1. Load data (takes care of fixing cylindrical consistency)
% [e1, ~, ~, mask, dt] = opendtmri(hpath, 'CylindricalConsistency', true, 'CloseMask', true, 'CropVolumes', true, 'EpsilonSmoothing', false);

% 3. choice of frame field (for now we use epicardium-based)
% [e1, B] = frame_from_distance_transform(e1, dt, mask);

% 4. Save preprocessing results to temporary folder
tdir = tempdir;
save_mkdir([tdir, 'e1x.mat'], e1(:,:,:,1));
save_mkdir([tdir, 'e1y.mat'], e1(:,:,:,2));
save_mkdir([tdir, 'e1z.mat'], e1(:,:,:,3));
save_mkdir([tdir, 'projnormx.mat'], B(:,:,:,1));
save_mkdir([tdir, 'projnormy.mat'], B(:,:,:,2));
save_mkdir([tdir, 'projnormz.mat'], B(:,:,:,3));
save_mkdir([tdir, 'mask.mat'], double(mask));


%% Fit (optimize) selected heart
% 5. Select output directory
% opath = [hpath, 'cartanfit/'];

clbls = { 'c121', 'c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233'};
cpath = '../../cartanfitter.jar';
cmodel = 'full';
% cplane = 'transverse';
cplane = 'fit';
lambda = 1e-6;
errorThreshold = 1e-5;
errorDelta = 1e-6;
verboseLevel = 2;
command = ['java -jar ', cpath, ' ' , tdir, ' ', opath, ' ', cplane, ' 3 150 ', num2str(errorThreshold), ' ', num2str(errorDelta), ' ', num2str(lambda), ' 0 ', num2str(verboseLevel),' ', cmodel];
system(command);

%
% Load optimization results back
for i=1:length(clbls)
    lbl = clbls{i};
    cmat = loadmat([opath, cmodel, '/', cmodel, '_', lbl, '.mat']);
    cijks.(lbl) = cmat;
end

