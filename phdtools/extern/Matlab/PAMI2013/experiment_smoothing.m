function experiment_smoothing(searchPath, varargin)

defaults.Bounds = [1, 1, 0.7];
defaults.ElementOrder = [1 2 3];
%experiment_options.PostPath = 'registered';
defaults.PostPath = 'registered';
defaults.MaskedIndexing = true;
defaults.SinglePrecision = false;
defaults.OptimizationFrameOnly = false;
defaults.FrameNormal = true;
defaults.FrameCurl = false;
defaults.FrameEig = false;
defaults.PrecomputeDirectConnections = false;
defaults.IterativeSmoothing = [true, 0.2, 5];

options = parseInput(defaults, varargin{:});

cform_options = struct2paramcell(struct('SinglePrecision', true));

frameLabel = 'Fnorm';

%%%%%%%%%%%%%%%%%%
%% Set parameters
%%%%%%%%%%%%%%%%%%
% stds = [0.1, 0.2, 0.5];
% its = [1];
% stds = [0:0.1:0.3, 0.5];
% its = [1, 3, 5, 10];
% stds = [0,0.3, 0.5];
% its = [1, 3, 5];
stds = [0, sqrt(0.5)];
its = [1, 2, 4, 8];

realization = 1;

clf
legends = {};
for std = stds
    for it = its
        pdisp(sprintf('Smoothing | %d iterations @ STD = %.2f', it, std), 'style', 'title');
        
        % Set parameters
        options.IterativeSmoothing = [true, std, it];
        options_cells = struct2paramcell(options);
        
        % Format output directory
%         outdir = sprintf('./output/smoothing/std_%s_its_%d/', strrep(num2str(std), '.', 'd'), it)
        rlbl = sprintf('std_%.2f_its_%d', std, it);
        legends = [legends, sprintf('std[%.1f] its[%d]', std, it)];
        outdir = sprintf('./output/smoothing/%s/', rlbl)

        % Save experiment parameters
        save_mkdir([outdir, 'experiment.mat'], options); 

        % Load data and compute frames
        subjects = compute_frames_con_all(searchPath, options_cells{:});
       
        % Assemble result volume
        sred = {};
        for subject_index = 1:length(subjects)
            subject = subjects(subject_index);
            sred(subject_index).e1 = subject.e1;
            sred(subject_index).mask = subject.mask;
            sred(subject_index).name = subject.name;
            
            % Compute connection forms and use masked indexing to save some
            % space.
            sred(subject_index).cijks = cforms(subject.frames.(frameLabel), 'MaskedVolume', subject.mask, cform_options{:});
        end
        clear subjects;
        
        save_mkdir([outdir, 'results.mat'], sred);
        
        %% Update plot
        cnames = fieldnames(sred(1).cijks);
        
        nbins = 500;
        stdf = 1;
        lims = [-1, 1];
        for cindex = 1:length(cnames)
%         for cindex = 6
            c = combine_volumes(sred, ['cijks.', cnames{cindex}]);
            hold on
            subplot(3,3,cindex);
            histnorm_thresholded(c, nbins, stdf, 'Color', colorset(realization), 'limits', lims);
%             hist(c(:), nbins, 'Color', colorset(realization))
%             [heights,centers] = hist(c(:), nbins);
%             heights = heights ./ sum(heights);
%             plot(centers, heights, 'Color', colorset(realization));
            
            legend(legends);
            hold off
            title(cnames{cindex});
        end
        
        realization = realization+1;
        
        pause(0.1);
    end
end

%%
export_fig(['./output/smoothing/smoothing.pdf'], '-transparent', '-q101', '-nocrop');

