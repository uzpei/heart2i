subjects = compute_frames_con_all(searchPath, experiment_options_cells{:});

subject = subjects(1);

closeSize = 1;
iaxis = 'x';

% its = 4; stdi = 0.5;
% its = 1; stdi = 1;
% its = 2; stdi = 0.7;   
its = 2; stdi = 0.7;
%[T, centroids] = cardiac_cylindrical_consistency(subjects(1).e1, subjects(1).mask);
%mask = subjects(1).mask;
mask3 = repmat(subject.mask, [1 1 1 3]);
Ts = mask3 .* iterative_gaussian_smoothing(subjects(1).e1, 'iterations', its, 'std', stdi, 'mask', subjects(1).mask);
Ts2 = mask3 .* iterative_gaussian_smoothing(subjects(1).e1, 'iterations', its, 'std', stdi);
%Ts = subjects(1).e1;
figure(1);image3d(Ts);
figure(2);image3d(Ts2);
figure(3);image3d(Ts2 - Ts);

%%
% [e1,e2,e3,mask,dt] = opendtmri('../../data/heart/rat/smooth/');
% figure(1);image3d(mask);
% figure(2);image3d(dt);

%% Save smoothed frame
[T, B] = frame_from_distance_transform(Ts, subject.dt, subject.mask);
outdir = './output/frame_smooth/';
save_mkdir([outdir, 'dt.mat'], double(subject.dt));
save_mkdir([outdir, 'e1x.mat'], -T(:,:,:,1));
save_mkdir([outdir, 'e1y.mat'], -T(:,:,:,2));
save_mkdir([outdir, 'e1z.mat'], -T(:,:,:,3));
save_mkdir([outdir, 'projnormx.mat'], B(:,:,:,1));
save_mkdir([outdir, 'projnormy.mat'], B(:,:,:,2));
save_mkdir([outdir, 'projnormz.mat'], B(:,:,:,3));
save_mkdir([outdir, 'mask.mat'], double(subject.mask));

%%
%
%Ts = mask3 .* subjects(1).e1;
% figure(1);
% % subplot(131);image3dslice(subjects(1).e1,iaxis); axis image;
% % CC
% subplot(131);image3dslice(T,iaxis); axis image;
% title('original + CC');
% % Use a tiny bit of smoothing for filling holes
% % Ts = iterative_gaussian_smoothing(T, 'iterations', its, 'std', stdi);
% %Ts = T;
% subplot(132);image3dslice(Ts,iaxis); axis image;
% %subplot(133);image3dslice(mask3 & ~Ts,iaxis); axis image;
% title('smoothed');
% 
% subplot(133);image3dslice(Ts - T,iaxis); axis image;
% title('diff');
% 
% Td = Ts - T;
% mean(abs(Td(Td ~= 0)))
% % numel(find(T ~= 0))
% % numel(find(Ts ~= 0))
% % numel(find(mask3 == 1))

%
% Display c
subject = subjects(1);
%Ts = subject.e1;
[T, B] = frame_from_distance_transform(Ts, subject.dt, subject.mask);
frame = {};
F.T = T;
F.B = B;
cijks = cforms(F);
[sx, sy, sz] = size(subject.mask);
%image3d(cijks.c123, round([sx/2, sy/2, sz/2]), [-0.5, 0.5]);
%histnorm(cijks.c123(subject.mask), 100, 0);
%supertitle(sprintf('c123 with std=[%.2f]', stdi));
%cformplot(F, subject.mask);

%
figure(3);image3d(abs(cijks.c123) < 1e-2 & subjects(1).mask == 1)

%% Check with centroid
v = double(mask);
[sx, sy, sz] = size(mask);
rcs = round(centroids);
hold on
for z=1:sz
    if (isnan(rcs(z,1)))
        continue
    end
%     v(rcs(z,1), rcs(z,2), z) = 2;
    hold on
    image3dslice(mask(:,:,z));
    plot(rcs(z,1), rcs(z,2), '--rs', 'MarkerSize', 10);
    hold off
    pause(0.1);
end



%% Experiment Gaussian normalization
stdi = 1;
gsize = 6 * stdi;
h = fgaussian3([gsize,gsize,gsize], [stdi, stdi, stdi]);
Gmask = imfilter(double(subjects(1).mask),h,'replicate');

image3d(Gmask);


