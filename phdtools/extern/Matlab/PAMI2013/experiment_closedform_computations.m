function result = experiment_closedform_computations(subjects, frame)

% Experiment details:
% 1) Apply smoothing 
% 2) Compute c-forms
% 3) Compute error

for sub_index=1:length(subjects)
    subject = subjects(sub_index);
    F = getnestedfield(subject, frame);
    result(sub_index).cijks = cforms(F); 
end