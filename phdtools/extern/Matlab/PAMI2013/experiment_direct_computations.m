%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code is for Section 5.4 of the paper 
% The first output is 9 histograms combined for all species and subjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function species = experiment_direct_computations(outdir, frame, specieslabel, varargin)

%frame = 'frames.Fnorm';

% Experiment details:
% 1) Apply smoothing 
% 2) Compute c-forms
% 3) Compute error

numspecies = nargin - 3;
prettyprint(sprintf('Direct computations for %d species', numspecies)); 

for spindex = 1:numspecies
    % Compute cijks
    subjects = varargin{spindex};
    species{spindex} = {};
    species{spindex}.name = specieslabel{spindex};
    species{spindex}.names = {};
    
    for sub_index=1:length(subjects)
        subject = subjects(sub_index);

        species{spindex}.names = {species{spindex}.names{:}, subject.name};
        
        F = getnestedfield(subject, frame);
        cijks = cforms(F);
%         result(sub_index).cijks = cijks;
        
        clbls = fieldnames(cijks);
        
        for cindex = 1:length(clbls)
            cijk = cijks.(clbls{cindex});
            cijk = cijk(subject.mask & ~isnan(cijk));
            species{spindex}.subjects(sub_index).cijks.(clbls{cindex}) = cijk;
            
        end
%         species{1}.subjects(sub_index).cijks = cijks;
%         species{1}.subjects(sub_index).mask = subject.mask & ~isnan(cijks.c123);
    end
end
