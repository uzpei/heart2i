%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                                         %
%    MATLAB auto-generated script. Created by user [epiuze] on [helicoid.cim.mcgill.ca, x86_64] at 2013/11/26 16:25:32    %
%                                                                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd /Users/epiuze/Documents/workspace/PhD/phdtools/./extern/Matlab;
setpath;
clf;

outdir = '/Users/epiuze/Documents/workspace/PhD/phdtools/./data/PAMI/images/Rat/rat07052008/';

mask = loadmat('../../data/heart/rat/atlas_FIMH2013/rat07052008/registered/mask.mat');

%% Plot the distance transform
set(gcf, 'Position', get(0,'Screensize'));

vol = loadmat('../../data/heart/rat/atlas_FIMH2013/rat07052008/registered/dt_epi.mat');
image3d(vol);
%colormap(b2r(min(dt_endo(:)),max(dt_endo(:))));
vol(~mask) = NaN;
image3dvolumetric(vol, 7, 0.8);
colormap(flipud(hot));
caxis([0 20])
export_fig([outdir, 'epi.png'], '-q101', '-transparent');

vol = -loadmat('../../data/heart/rat/atlas_FIMH2013/rat07052008/registered/dt_endo.mat');
image3d(vol);
%colormap(b2r(min(dt_endo(:)),max(dt_endo(:))));
vol(~mask) = NaN;
image3dvolumetric(vol, 7, 0.8);
colormap(flipud(hot));
caxis([0 20])
export_fig([outdir, 'endo.png'], '-q101', '-transparent');

%%
e1x = loadmat('../../data/heart/rat/atlas_FIMH2013/rat07052008/registered/e1x.mat');
e1y = loadmat('../../data/heart/rat/atlas_FIMH2013/rat07052008/registered/e1y.mat');
e1z = loadmat('../../data/heart/rat/atlas_FIMH2013/rat07052008/registered/e1z.mat');
e1 = assemble(e1x, e1y, e1z);

it_stds = [0, 0.2, 0.4];
it_counts = [0, 5, 10];

for it_std = it_stds
    for it_count = it_counts
        if (it_std > 0 && it_count > 0)
            e1s = iterative_gaussian_smoothing(e1, 'mask', mask, 'iterations', it_count, 'std', it_std);
        elseif (it_std == 0 && it_count == 0)
            e1s = e1;
        else
            continue;
        end
        
        e1s = e1s - e1;
        
        for saxis = 1:3
            vol = e1s(:,:,:,saxis);
            vol(~mask) = NaN;

            if (saxis == 1)
                slbl = 'x';
            elseif (saxis == 2)
                slbl = 'y';
            elseif (saxis == 3)
                slbl = 'z';
            end

            clf
            hold on;
            [sx, sy, sz] = size(vol);
            for z = 2:round(0.7 * sz / 7):round(0.7 * sz)
                slice = image3dslice(vol, 'z', z);

                % Increase resolution
                res = 1;
                [X,Y] = meshgrid(1:res:sy, 1:res:sx);
                sliceHigh = interp2(slice,X,Y);
                Z = ones(numel(1:res:sx), numel(1:res:sy)) + z;
                surf(X, Y, Z, sliceHigh, 'EdgeColor','None');
            end
            hold off;


            camlight(-30, 0); lighting phong;
            view(-45.0,20.0);
            grid off;
            hidden off;
            axis off;
            axis square;
            zoom(1.3);
            set(gcf, 'Color', 'white');
            colormap jet;
            h=colorbar;
            set(h, 'FontSize', 36);
            hsize = 0.7;
            set(h, 'Position', [.8 (1-hsize) / 2 .04 hsize]);
            caxis([-1, 1]);
            set(gcf, 'Position', get(0,'Screensize'));

            mkdir(outdir);
            export_fig([outdir, 'e1diff', slbl, '_', num2str(it_count), '_', num2str(it_std), '.png'], '-q101', '-transparent');
        end
    end
end
