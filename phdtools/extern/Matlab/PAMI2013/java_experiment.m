% Select target heart
hpath = '/usr/local/data/heart_sample/';
[e1, ~, ~, mask, dt] = opendtmri(hpath, 'CylindricalConsistency', true, 'CloseMask', true, 'CropVolumes', true, 'EpsilonSmoothing', false);
[e1, B] = frame_from_distance_transform(e1, dt, mask);
subject.e1 = e1;
subject.mask = mask;
subject.B = B;

outdir = [hpath, 'cartanfitting/'];
cijks = cijks_optimized_java(e1, B, mask, 'full', outdir);


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preprocessing step
% 1. Load data (takes care of fixing cylindrical consistency)
hpath = '/usr/local/data/heart_sample/';
[e1, ~, ~, mask, dt] = opendtmri(hpath, 'CylindricalConsistency', true, 'CloseMask', true, 'CropVolumes', true, 'EpsilonSmoothing', false);

cit = 0;
clf
% for numits = [1, 2, 4, 8]
for numits = 0
    cit = cit + 1;
    
    istd = sqrt(0.5);

    % 2. apply smoothing
    e1 = iterative_gaussian_smoothing(e1, 'mask', mask, 'iterations', numits, 'std', istd);

    cijks = cijks_optimized_java(hpath, 'full');

    %
    % Plot results
    % c123 = rad2deg(cijks.c123(~isnan(cijks.c123))) / 0.25;
    c123 = rad2deg(cijks.c123(~isnan(cijks.c123) & cijks.c123 < 1 & cijks.c123 > -1)) / 0.25;
    % c123 = c123(c123 < 1e2);
    % c123 = c123(c123 > -1e2);
    % min(c123)
    % max(c123)
    fprintf('Mean c123 = %.5f\n', mean(c123(:)))
    fprintf('Std c123 = %.5f\n', std(c123(:)))

    %%
    hold on
    histnorm_thresholded(c123, 50, 0, 'color', colorset(cit));

end

%%%%%%%%%%
%%
c123 = loadmat('c123.mat');
c123 = rad2deg(c123(~isnan(c123))) / 0.25;
fprintf('Mean c123 = %.5f\n', mean(c123(:)))
fprintf('Std c123 = %.5f\n', std(c123(:)))

histnorm_thresholded(c123, 100, 0);
% histnorm(c123(:), 150, 0);

cerror = loadmat('error.mat');
cerror = cerror(~isnan(cerror));
fprintf('Mean error = %.5f\n', rad2deg(acos(1-nanmean(cerror(:)))))

%%
cmodel = 'full';
cd(['/usr/local/data/heart_sample/fitting/', cmodel, '/']);
clbls = { 'c121', 'c122', 'c123', 'c131', 'c132', 'c133', 'c231', 'c232', 'c233'};
% clf
for i=1:length(clbls)
    lbl = clbls{i};
    cmat = rad2deg(loadmat([cmodel, '_', lbl, '.mat']));
    cijks.(lbl) = cmat;
    mask = ~isnan(cmat);
    cmat = cmat(mask);
    
    subplot(3,3,i);
    hold on
    histnorm_thresholded(cmat, 100, 0);
    title(lbl);
end
% clf
% cformplot(cijks, mask);

