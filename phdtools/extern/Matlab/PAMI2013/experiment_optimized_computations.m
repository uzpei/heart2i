% Sampling: the percentage of (masked) volume voxels to consider.
function result = experiment_optimized_computations(subjects, frame)

testslice = true;

% elbls = { '121', '122', '123', '131', '132', '133', '231', '232', '233'};
cmask = [1 1 1 1 1 1 1 1 1];
% cmask = [1 1 1 1 1 1 0 0 0];

if (testslice)
    sampling = 0.1;
else
    sampling = 1;
end

% TODO: compute intersection of all mask volumes
% and subsample randomly
for sub_index = 1:length(subjects)
    subject = subjects(sub_index);
    
    if (testslice)
        [sx, sy, sz] = size(subject.mask);
        subject.mask(:,:,[1:(round(sz/2)-1),(round(sz/2)+1):end]) = 0;
        image3d(subject.mask);
    end
    
    F = getnestedfield(subject, frame);

    inds = find(subject.mask > 0);
    numel(inds)
    samples = randsample(inds, round(sampling * numel(inds)));

    [sx, sy, sz] = size(subject.mask);
    cijks = cijks_optimized(F, subject.mask, samples, 'cmask', cmask, 'verbose', 'off');
    result(sub_index).cijks = cijks; 
end
