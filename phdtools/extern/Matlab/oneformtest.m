% One-form unit testing
% Create synthetic volumes and checks Cartan one-forms.

% First create a frame field
% Use asymetrical volume
n = 16;
dims = [n, 2*n, 3*n];

F1 = zeros(dims(1), dims(2), dims(3), 3);
F2 = zeros(dims(1), dims(2), dims(3), 3);
F3 = zeros(dims(1), dims(2), dims(3), 3);

% Natural frame field
F1(:,:,:,1) = 1;
F2(:,:,:,2) = 1;
F3(:,:,:,3) = 1;

%% Construct cF1,F2 <F3> forms
k123 = 2 * pi / dims(3);
for z=1:dims(3)
    F1(:,:,z,1) = cos(k123 * z);
    F1(:,:,z,2) = sin(k123 * z);
    F1(:,:,z,3) = 0;

    F2(:,:,z,1) = -sin(k123 * z);
    F2(:,:,z,2) = cos(k123 * z);
    F2(:,:,z,3) = 0;
end

% normalize
F1 = normalize(F1);

% Plot
for sz=1:dims(3)
    clf
    hold on
    quiver(F1(:,:,sz,1),F1(:,:,sz,2), 'r');
    quiver(F2(:,:,sz,1),F2(:,:,sz,2), 'g');
    hold off
    
axis([1 dims(2) 1 dims(1)])
pause(0.01)
end

%% Compute C123
c123 = cijv(jacobian(F1), F2, F3);
fprintf('Error = %.6f (%.2e%%)\n', abs(k123-mean(c123(:))), abs((k123-mean(c123(:)))/k123));

%% Construct cF1,F2,F1 forms
k121 = 2 * pi / (dims(2)-1);
% for y=1:dims(2)
%     F1(:,y,:,1) = cos(k121 * (y-1));
%     F1(:,y,:,2) = sin(k121 * (y-1));
%     F1(:,y,:,3) = 0;
% 
%     F2(:,y,:,1) = -sin(k121 * (y-1));
%     F2(:,y,:,2) = cos(k121 * (y-1));
%     F2(:,y,:,3) = 0;
% end
[x1, x2] = ndgrid(1:dims(1), 1:dims(2));
x1 = repmat(x1, [1 1 dims(3)]);
x2 = repmat(x2, [1 1 dims(3)]);

F1(:,:,:,1) = -x2(:, :, :);
F1(:,:,:,2) = x1(:, :, :);
F2(:,:,:,1) = x1(:, :, :);
F2(:,:,:,2) = x2(:, :, :);

F1 = normalize(F1);
F2 = normalize(F2);

% Plot
clf
hold on
quiver(F1(:,:,1,1),F1(:,:,1,2), 'r');
quiver(F2(:,:,1,1),F2(:,:,1,2), 'g');
hold off
    
axis([1 dims(2) 1 dims(1)])

%% Compute C123
c121 = cijv(jacobian(F1), F2, F1);
k121
mean(c121(:))
fprintf('Error = %.6f (%.2e%%)\n', abs(k121-mean(c121(:))), abs((k121-mean(c121(:)))/k121));

