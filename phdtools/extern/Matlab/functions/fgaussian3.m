function h = fgaussian3(gsize, gstd, varargin)
    options = parseInput(struct('normalize', 'true'), varargin{:});

    if (sum(gsize) <= eps)
        gsize = ones(1,3);
    end
    
    if (sum(gstd) <= eps)
        h = 1;
        return;
    end
    
    % Parse varargin
    % i.e. use mask for normalizing the function
    
    
    % Use half-width and include center
    halfWidth = ceil(gsize / 2);
    
    kx = linspace(-halfWidth(1), halfWidth(1), 2 * ceil(gsize(1)) + 1);
    ky = linspace(-halfWidth(2), halfWidth(2), 2 * ceil(gsize(2)) + 1);
    kz = linspace(-halfWidth(3), halfWidth(3), 2 * ceil(gsize(3)) + 1);
    
    [x,y,z] = ndgrid(kx, ky, kz);
    
    h = exp(-((x.^2)/(2*gstd(1)^2) + y.^2/(2*gstd(2)^2) + z.^2/(2*gstd(3)^2)));
%     cx = -(x.^2)/(2*gstd(1)^2)
%     cy = -(y.^2)/(2*gstd(2)^2)
%     cz = -(z.^2)/(2*gstd(3)^2)
%     h = exp(cx + cy + cz);
    h = h/sum(h(:));
end


        