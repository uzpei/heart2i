function [dts, dx, dy, dz, mask, dt] = dtransform3(mask, isotropy, morphoSize)
animate = false;

if (nargin > 2)
    % Close the mask by dilating it
    [x,y,z] = ndgrid(-morphoSize:morphoSize);
    se = strel(sqrt(x.^2 + y.^2 + z.^2) <= morphoSize);
    mask = 1-imdilate(1-mask,se);
    mask = 1-imerode(1-mask,se);
end
 
% Compute the Euclidean distance transform on the mask
dt = bwdistsc1(mask, isotropy);

% Smooth the distance transform with a 3D gaussian
% This function automatically sets the standard deviation such that
% the full width at half maximum (FWHM) is half the filter size
gsize = 5;
h = fspecial3('gaussian',[gsize, gsize, gsize]); 
dts = imfilter(dt,h,'replicate');
[dx, dy, dz] = gradient(dts);

% Find gradient vector norms that fall below a certain threshold
% these are assumed to lie within a transform singularity
% tval = 0.6;
% dthreshold = sqrt(dx .^ 2 + dy .^ 2 + dz .^ 2) < tval;
% inds = find(dthreshold == 1);

% Look at one slice
if (animate)

[drawx, drawy, drawz] = gradient(dt);

for j=20:40
%for j=slice:slice

vx = drawx(:,:,j);
vy = drawy(:,:,j);

vxs = dx(:,:,j);
vys = dy(:,:,j);

figure(1);
clf
hold on
imagesc(dt(:,:,j));
hold off
title(j);
colormap gray

figure(2);
clf
quiver(vx, vy, 'k');
axis square
title(j);
colormap gray

figure(3);
clf
hold on
quiver(vxs, vys, 'r');
hold off
title(j);
colormap gray

pause(0.1);
end
end
