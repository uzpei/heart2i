function y = logspace_sampling(minSlice, maxSlice, numSlices)

% minSlice = 2;
% maxSlice = 21;
% numSlices = 10;
%numel(logspace_sampling(2, 21, 10))

n = numSlices + 1;
y = unique(round(logspace(log10(minSlice), log10(maxSlice),n)));
%y
%y = y(1:numSlices);

% inds = ones(1, numel(y));
% plot(y, inds, 'o');
% y
