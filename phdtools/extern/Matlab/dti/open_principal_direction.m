%path = '/Users/epiuze/Documents/workspace/PhD/phdtools/./data/heart/rat/atlas_FIMH2013/rat07052008/registered/';
function [F1g, mask] = open_principal_direction(path, maskPath, volumeName, iterations, stdev)

mask = openmask(maskPath);
[sx, sy, sz] = size(mask);

disp(['Vx = ', path, volumeName, 'x.mat']);
%size(loadmat([path, volumeName, 'x.mat']))
%size(mask)

elementOrder = [1, 2, 3];
F1 = zeros([sx, sy, sz, 3]);
F1(:,:,:,elementOrder(1)) = loadmat([path, volumeName, 'x.mat']);
F1(:,:,:,elementOrder(2)) = loadmat([path, volumeName, 'y.mat']);
F1(:,:,:,elementOrder(3)) = loadmat([path, volumeName, 'z.mat']);
F1 = normalize(F1);

% TODO: apply cylindrical consistency
F1 = cardiac_cylindrical_consistency(F1, mask);

F1g = iterative_gaussian_smoothing(F1, 'mask', mask, 'iterations', iterations, 'std', stdev);
F1g = normalize(F1g);
F1g = F1g .* repmat(mask, [1 1 1 3]);
% figure(1);image3d(F1);
%figure(2);image3d(e1g);
