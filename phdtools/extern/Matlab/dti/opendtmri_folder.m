function [subjects, options] = opendtmri_folder(searchPath, varargin)

t_loading = tic;

% Define defaults
defaults = struct('PostPath','.', 'ElementOrder', [1 2 3], 'CropVolumes', false, 'LoadMask', true, 'MaskName', 'mask.mat', 'LoadE1',true,'LoadE2',false,'LoadE3',false);
defaults.Ignore = { '..' };
options = parseInput(defaults, varargin{:});

opendtmri_options = struct2paramcell(struct('CropVolumes', options.CropVolumes, 'MaskName', options.MaskName, 'ElementOrder', options.ElementOrder, 'E1Only', ~options.LoadE2 && ~options.LoadE3));

subjects = {};

listing = dir(searchPath);
fprintf('--------------------------------------------------------------\n');
fprintf('  Searching [%s] for DT-MRI folders with inner path [%s]...\n', searchPath, options.PostPath);
fprintf('--------------------------------------------------------------\n');

% Go through all files
for file_index = 1:length(listing)
    % Get the filename
    filename = listing(file_index).name;
    
    % Ignore hidden files
    if (1 == strfind(filename, '.'))
        fprintf('Ignoring hidden file: [%s] ', filename);
        continue;
    end
    
    % Go through ignore list
    ignoredFlag = false;
    for ign = 1:length(options.Ignore)
        k = strfind(filename, options.Ignore{ign});
        if (k > 0)
            fprintf('Ignoring (flagged) file: [%s] ', filename);
            ignoredFlag = true;
            break;
        end
    end
    
    if (ignoredFlag)
        continue;
    end
    
    % Finally, look for the existence of the post-path
    % (if it is not "." or empty
%     strfind(options.PostPath, '.') 
%     isempty(options.PostPath)
    predot = strfind(options.PostPath, '.');
    
    if (~isempty(predot) && predot(1) == 1 && length(options.PostPath) == 1)
        postPathFound = true;
    elseif ((isempty(predot) || 1 ~= predot(1)) && ~isempty(options.PostPath))
        inner_listing = dir([searchPath, filename]);
        postPathFound = false;
        for inner_index = 1:length(inner_listing)
    %        inner_listing(inner_index).name
            if (strcmpi(inner_listing(inner_index).name, options.PostPath))
                postPathFound = true;
            end
        end
    else
        postPathFound = false;
    end
    
    if (~postPathFound)
        fprintf('Ignoring file: [%s] since inner path [%s] was not found ', filename, options.PostPath);
        continue;
    end
    
    % From this point on the file is valid
    subjects(length(subjects)+1).name = filename;
end

fprintf('\n');

allFiles = '';
for file_index = 1:length(subjects)
    allFiles = [allFiles, subjects(file_index).name, ', '];
end

% List folders that were found
fprintf('Will attempt to load the following [%i] subjects: %s\n', length(subjects), allFiles);

% Begin loading volumes
for subject_index = 1:length(subjects)
    subject_path = [searchPath, subjects(subject_index).name, '/', options.PostPath, '/'];
    fprintf('-> [%i/%i] Found volumes %s...\n', subject_index, length(subjects), subject_path);
    [e1, e2, e3, mask, dtEndo, dtEpi] = opendtmri(subject_path, opendtmri_options{:});
    subjects(subject_index).path = subject_path;
    subjects(subject_index).e1 = e1;
    subjects(subject_index).e2 = e2;
    subjects(subject_index).e3 = e3;
    subjects(subject_index).mask = mask;
    subjects(subject_index).dt_endo = dtEndo;
    subjects(subject_index).dt_epi = dtEpi;
end
fprintf('---------------------------------------\n');
fprintf('  Done loading [%i] subjects in %.2fs.\n', length(subjects), toc(t_loading));
fprintf('---------------------------------------\n');
end