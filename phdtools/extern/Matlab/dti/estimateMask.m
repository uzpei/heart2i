% Assume data is filled with the following images.
% (dz = number of slices along z)
% image = pos * dz + (1:dz)
% <DTI_FA>     pos=0
% <DTI_TRACE>  pos=1 
% <DTI_A0>     pos=2
% <DTI_TWI>    pos=3
% <DTI_DXX>    pos=4
% <DTI_DYY>    pos=5
% <DTI_DZZ>    pos=6
% <DTI_DXY>    pos=7
% <DTI_DXZ>    pos=8
% <DTI_DYZ>    pos=9
% <DTI_L1>     pos=10
% <DTI_L2>     pos=11
% <DTI_L3>     pos=12
% <DTI_L1X>    pos=13
% <DTI_L1Y>    pos=14
% <DTI_L1Z>    pos=15
% <DTI_L2X>    pos=16
% <DTI_L2Y>    pos=17
% <DTI_L2Z>    pos=18   
% <DTI_L3X>    pos=19
% <DTI_L3Y>    pos=20
% <DTI_L3Z>    pos=21
function mask = estimateMask(dataDTI, dz, animate)

disp(sprintf('Computing the mask...'));

% Compute the a0 image
a0 = dataDTI(:, :, (1:dz) + dz * 2);
a0 = a0 ./ max(a0(:));

l1 = dataDTI(:, :, (1:dz) + dz * 10);
l2 = dataDTI(:, :, (1:dz) + dz * 11);
l3 = dataDTI(:, :, (1:dz) + dz * 12);

%fa = dataDTI(:,:,1:dz + 0 * dz);
fa = sqrt(0.5) * sqrt((l1 - l2) .^ 2 + (l2 - l3) .^ 2 + (l3 - l1) .^ 2) ./ (sqrt(l1 .^ 2 + l2 .^ 2 + l3 .^ 2));
fa = fa ./ max(fa(:));

faMin = 0.5;
a0Min = 0.4;
a0Max = 0.8;
mask = (fa <= faMin) & (a0 >= a0Min) & (a0 <= a0Max);

if (animate)
    for m=1:1
    for n=23:46

       figure(1); clf;
       imagesc(mask(:,:, n));
       caxis([0 1])
       colormap gray
       title(n);
       pause(.05)
    end
    end
end
