function mask = openmask(path, varargin)

maskNames = { 'mask.mat', 'b0.mat' };
defaultMaskID = 1;

if (nargin > 1)
    maskName = varargin{1};
else
    maskName = maskNames{defaultMaskID};
end

% Skip if the path ends with .mat
if (strfind(path, '.mat') == length(path) - 3)
    maskPath = path;
else
    % Try other names if the mask cannot be found
maskFound = true;
if ~exist([path, maskName], 'file')
    maskFound = false;
    for i=1:length(maskNames)
        if exist([path, maskNames{i}], 'file')
            maskName = maskNames{i};
            maskFound = true;
            break;
        end
    end
end
maskPath = [path, maskName];
end

disp(['Loading mask ', maskPath]);
% First load the mask volume
mask = logical(loadmat(maskPath));

% Apply automatic threshold if this is a b0 image
% TODO: this has to be done in a better fashion
% Load the mask and make sure it's a logical (boolean) volume
mask = logical(loadmat(maskPath));
