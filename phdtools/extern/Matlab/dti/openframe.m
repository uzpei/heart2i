% function [T, N, B, mask] = openframe(path, varargin)
% where varargin = maskName (string)
function varargout = openframe(path, varargin)

% Defaults
maskName = 'mask.mat';
Tonly = false;

% Parse arguments
if (nargin > 1)    
    maskName = varargin{1};
end

if (nargin > 2)
    Tonly = varargin{2};
end

% Load mask
mask = openmask(path, maskName);

%% Load T
T = loadmat([path, 'T.mat']);
T = normalize(T);

if (Tonly)
    varargout{1} = T;
    varargout{2} = mask;
else
    % Load B
    B = loadmat([path, 'B.mat']);
    B = normalize(B);
    
    % Compute N = B x T
    N = cross(B,T);
    
    varargout{1} = T;
    varargout{2} = N;
    varargout{3} = B;
    varargout{4} = mask;
end

end
