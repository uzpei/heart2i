% function [e1, e2, e3, mask, endo, epi] = opendtmri(path, varargin)
% options = 'MaskName', 'mask.mat', 'ElementOrdering', [1 2 3], 'E1Only', false);
function varargout = opendtmri(path, varargin)
defaults = {};
defaults.CylindricalConsistency = true;
defaults.CloseMask = false;
defaults.CropVolumes = false;
defaults.MaskName = 'mask.mat';
defaults.ElementOrder = [1 2 3];
defaults.E1Only = false;
defaults.EpsilonSmoothing = false;
options = parseInput(defaults, varargin{:});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% NOTE: FOR DEBUGGING PURPOSES LEAVE TO "TRUE" %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Render the volume anisotropic (i.e. sx != sy != sz)
options.anisotropic = false;

%%%%%%%%%%%%%%%%%%
% Preprocess mask
%%%%%%%%%%%%%%%%%%
mask = openmask(path, options.MaskName);

if (options.CropVolumes)
    pdisp('Cropping the mask...', 'style', '*');
    maskCrop = cropimage(double(mask), true);
else
    pdisp('NOT cropping the mask.', 'style', '*');
end

if (options.CloseMask)
    pdisp('Closing the mask...', 'style', '*');
    closeSize = 1;
    mask = imclose(mask, strel_ball(closeSize));
else
    pdisp('NOT closing the mask.', 'style', '*');
end

% Crop volumes (tighten bound) if necessary
if (options.CropVolumes && any(size(maskCrop) ~= size(mask)))
    pdisp('Performing cropping on path...', 'style', '*');
    cropimage_folder(path); 
    mask = openmask(path, options.MaskName);
else
    pdisp('No need to perform cropping on other volumes: envelopes are already tight enough.', 'style', '*');
end

% Obtain volume size from mask
[sx, sy, sz] = size(mask);

% Load distance transform but update if it does not match the mask
% dimensions
dtEndo = loadmat([path, 'dt_endo.mat']);
dtEpi = loadmat([path, 'dt_epi.mat']);
if (~valid(dtEndo, mask) || ~valid(dtEpi, mask))
    %% Compute heart normals with closing and DT smoothing of same magnitude
    nmag = 5;
    dtEndo = cardiac_lv_distance(mask, nmag, nmag, 'endocardium');
    dtEpi = cardiac_lv_distance(mask, nmag, nmag, 'epicardium');
    
    spath = [path, 'dt_endo.mat'];
    fprintf('Saving distance transform to file [%s]\n', spath);
    save(spath, 'dtEndo');                
    
    spath = [path, 'dt_epi.mat'];
    fprintf('Saving distance transform to file [%s]\n', spath);
    save(spath, 'dtEpi');                

end

pdisp(sprintf('Using vector field element order = %s', mat2str(options.ElementOrder)), 'style', '*');

%%%%%%%%%%%%%%%%%%
% Preprocess e1
%%%%%%%%%%%%%%%%%%
e1 = zeros([sx, sy, sz, 3]);
e1(:,:,:,options.ElementOrder(1)) = loadmat([path, 'e1x.mat']);
e1(:,:,:,options.ElementOrder(2)) = loadmat([path, 'e1y.mat']);
e1(:,:,:,options.ElementOrder(3)) = loadmat([path, 'e1z.mat']);
e1 = normalize(e1);

if (options.anisotropic)
    e1 = anisotropic_volume(e1);
    mask = anisotropic_volume(mask);
    dtEndo = anisotropic_volume(dtEndo);
    dtEpi = anisotropic_volume(dtEpi);
end

if (options.CylindricalConsistency)
    e1 = cardiac_cylindrical_consistency(e1, mask);
end

% % Use epsilon-smoothing to fill gaps
% if (options.EpsilonSmoothing)
%     pdisp('Applying epsilon-iterative gaussian smoothing on E1...', 'style', '*');
%     its = 1;
%     stdi = 0.1;
%     e1 = repmat(mask, [1 1 1 3]) .* iterative_gaussian_smoothing(e1, 'iterations', its, 'std', stdi, 'mask', mask);
% else
%     pdisp('NOT applying epsilon-iterative gaussian smoothing on E1.', 'style', '*');
% end

varargout{1} = e1;
varargout{2} = [];
varargout{3} = [];
varargout{4} = mask;
varargout{5} = dtEndo;
varargout{6} = dtEpi;
    
pdisp('Done.', 'style', '*');

if (~options.E1Only)
    % Load e2
    e2(:,:,:,options.ElementOrder(1)) = loadmat([path, 'e2x.mat']);
    e2(:,:,:,options.ElementOrder(2)) = loadmat([path, 'e2y.mat']);
    e2(:,:,:,options.ElementOrder(3)) = loadmat([path, 'e2z.mat']);
    e2 = normalize(e2);

    % Load e3
    e3(:,:,:,options.ElementOrder(1)) = loadmat([path, 'e3x.mat']);
    e3(:,:,:,options.ElementOrder(2)) = loadmat([path, 'e3y.mat']);
    e3(:,:,:,options.ElementOrder(3)) = loadmat([path, 'e3z.mat']);
    e3 = normalize(e3);

    if (options.anisotropic)
        e2 = anisotropic_volume(e2);
        e3 = anisotropic_volume(e3);
    end
    
    varargout{2} = e2;
    varargout{3} = e3;
end

function b = valid(volume, mask) 
    if (isempty(volume))
        b = false;
    elseif (length(volume) ~= length(mask))
        b = false;
    else
        b = ~any(size(volume) ~= size(mask));
    end
end
end

