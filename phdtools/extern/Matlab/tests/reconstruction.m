pth = fileparts(which('cameraman.tif'));
D = dir(pth);
c = {'.tif';'.jp';'.png';'.bmp'};
idx = false(size(D));
for ii = 1:length(c)
    idx = idx | (arrayfun(@(x) any(strfind(x.name,c{ii})),D));
end
D = D(idx);
clf
for ii = 1:numel(D)
    img = sprintf('%s',D(ii).name);
    disp(img)
    I=imread(img);
    size(I)
    subplot(8, ceil(numel(D)/8), ii);
    imagesc(I);
    axis off
    axis image
    title(img);
    pause(0.1);
end
    
%%
I = double(imread('testpat1.png'));
[m, n] = size(I);
I = I ./ max(I(:));
imshow(I);
%%
pad = 200;
Ipad = padarray(I, [pad pad], 'replicate');
sigmas = [1.5:0.5:10];
c = zeros(length(sigmas), 1);
F = zeros(m*n, length(sigmas));
%%
for i=1:length(sigmas)
    sigma = sigmas(i)
    
    % Apply and return filter here
    f_i = fspecial('gaussian',round(3*[sigma sigma]),sigma);
    f_i = del2(f_i);
    If_i = double(imfilter(Ipad,f_i,'same'));

    % Crop
    If_i = If_i(pad:end-pad-1, pad:end-pad-1);

    % Normalize
    If_i = If_i ./ double(max(If_i(:)));

    c(i) = sum(If_i(:));
    F(:,i) = If_i(:);
end

%% Reconstruct image
figure(1);
clf
numSamples = 10;

% Sample volume
samples = unique(round(linspace(1,m*n, round(0.1 * m * n))));
%%
subm = 3;
subn = ceil((1+numSamples) / subm);
subplot(subm,subn,1);
I_crop = double(I(pad:end-pad, pad:end-pad));
imshow(I_crop);
colormap gray
title('original');
mindices = 1:(m*n);
mindices(samples) = [];
for i=1:(numSamples)
    subplot(subm,subn,i+1);
    index = i * floor(length(sigmas) / numSamples);
    
    % Construct sub-volumes
    c_sub = c(1:index);
    F_sub = F(:, 1:index);
    
    % Subsample
%     F_sub(samples, :) = [];
     F_sub(samples, :) = 0;
   
    % Reconstruct
    J = F_sub * pinv(F_sub' * F_sub) * c_sub;
    J = J ./ max(J(:));
    
    J_full = zeros(m*n,1);
    J_full(mindices) = J(:);
    
    J_full = reshape(J_full, [m, n]);
%     imshow(I_crop - J_crop);
    imshow(J_full);
    colormap gray
    title(num2str(index));
    pause(0.1);
end
