
% Create histogram from a normal distribution.
[f,x] = hist(randn(1000,1),50);

% Smooth
stdf = 2;
npdf = normpdf(-3*stdf:3*stdf,0,stdf);
f3 = conv(f, npdf, 'same');
f2 = smooth(f, 15, 'sgolay');

% Pdf of the normal distribution
g=1/sqrt(2*pi)*exp(-0.5*x.^2);

figure(1)
clf
hold on
%bar(x,f/sum(f));
plot(x,g,'k--');
plot(x, f ./ trapz(x, f), 'b');
plot(x, f2 ./ trapz(x, f2), 'g');
plot(x, f3 ./ trapz(x, f3), 'y');
hold off
legend({'pdf', 'rand', 'smooth', 'conv'});

