function h = quivermask(mask, varargin)

X = [];
Y = [];

if (nargin == 3)
    U = varargin{1};
    V = varargin{2};
    res = 1;
elseif (nargin == 5)
    X = varargin{1};
    Y = varargin{2}; 
    U = varargin{3};
    V = varargin{4}; 
    res = 1;
elseif (nargin == 6)
    X = varargin{1};
    Y = varargin{2}; 
    U = varargin{3};
    V = varargin{4};
    res = varargin{5};
else
    exception = MException('VerifyOutput:OutOfBounds', ...
       'Invalid number of function arguments');
    throw(exception);
end

% Make sure mask is a logical volume
mask = mask > 0;

% Construct grid if needed
if (isempty(X) || isempty(Y))
    sx = size(U, 1);
    sy = size(U, 2);
    [X, Y] = ndgrid(1:sx, 1:sy);
end

% Apply mask
X = X(mask);
Y = Y(mask);
% X = X(:);
% Y = Y(:);

% Apply mask
U = U(mask);
V = V(mask);
% U = U(:);
% V = V(:);

%res = 1;
% h = quiver(X(1:res:end),Y(1:res:end),U(1:res:end),V(1:res:end),'k','AutoScale','off');
%max(X)
%max(Y)
% h = quiver(X(1:res:end),Y(1:res:end),U(1:res:end),V(1:res:end),0.5,'k');
%h = quiver(Y(1:res:end),X(1:res:end),V(1:res:end),U(1:res:end),0.5,'k');
% h = quiver(Y(1:res:end),X(1:res:end),V(1:res:end),U(1:res:end),0.7, 'b','AutoScale','on');
% h = quiver(Y(1:res:end),X(1:res:end),-V(1:res:end),-U(1:res:end),0.5, 'b','AutoScale','off');
% set(h, 'linewidth', 3);
h = quiver(Y(1:res:end),X(1:res:end),V(1:res:end),U(1:res:end),'b','AutoScale','on');

