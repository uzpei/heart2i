% function h = quiverslice(mask, varargin)
% varargin =
% mask, 4D matrix T(:,:,:,3)
% mask, 4D matrix T(:,:,:,3), slice
% mask, U, V, W
% mask, U, V, W, slice
function [h1, h2, h3] = quiverslice(mask, F, sliceAxis, varargin)

%    F = swap3d(F, [2 1 3]);

    [sx, sy, sz] = size(mask);

    if (nargin == 4)
        slice = varargin{1};
        res = 1;
    elseif (nargin == 5)
        slice = varargin{1};
        res = varargin{2};
    else
        res = 1;
    end

    if (strcmpi(sliceAxis, 'z'))
        if (nargin <= 3)
            slice = round(sz/2);
        end
        
        maskSlice = mask(:,:,slice);
        VX = squeeze(F(:,:,slice, 1));
        VY = squeeze(F(:,:,slice, 2));
        [X, Y] = ndgrid(1:sx, 1:sy);
        h1 = quivermask(maskSlice, X, Y, VX, VY, res);
        xlabel('x');
        ylabel('y');
        formatFigs(h1);
        axis([1 sx 1 sy])

    elseif (strcmpi(sliceAxis, 'x'))
        if (nargin <= 3)
            slice = round(sx/2);
        end

        % Long axis view 1 (YZ)
        maskSlice = mask(slice,:,:);
        VY = squeeze(F(slice,:,:,2));
        VZ = squeeze(F(slice,:,:,3));
        [Y, Z] = ndgrid(1:sy, 1:sz);

        h2 = quivermask(maskSlice, Z, Y, VY, VZ, res);
        xlabel('y');
        ylabel('z');
        formatFigs(h2);
        axis([1 sy 1 sz])

    elseif (strcmpi(sliceAxis, 'y'))
        if (nargin <= 3)
            slice = round(sy/2);
        end
        
        % Long axis view 2 (XZ)
        maskSlice = mask(:,slice,:);
        VX = squeeze(F(:,slice,:,1));
        VZ = squeeze(F(:,slice,:,3));
        [X, Z] = ndgrid(1:sx, 1:sz);
        h3 = quivermask(maskSlice, Z, X, VX, VZ, res);
        xlabel('x');
        ylabel('z');
        formatFigs(h3);
        axis([1 sx 1 sz])
    end

end

function formatFigs(h)
    set(h,'linewidth',0.2);
    axis image
%    axis off
end

