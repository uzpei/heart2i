function aspect(ratio)
xlims = xlim;
ylims = ylim;
dx = xlims(2)-xlims(1);
dy = ylims(2)-ylims(1);

daspect([ratio * dx / dy 1 1]);
