function imageplanes(img, animate, delay)

imsize = size(img);

if (nargin == 1)
    % Mid long-axis slice (Coronal)
    subplot(2,2,1);
    imagesc(rot90(squeeze(img(imsize(1)/2,:,:))));
    title('long-axis coronal');
    axis equal
    colormap gray

    % Mid long-axis slice (Sagittal)
    subplot(2,2,2);
    imagesc(rot90(squeeze(img(:,imsize(2)/2,:))));
    title('long-axis sagittal');
    axis equal
    colormap gray

    % Mid short-axis slice (Transverse)
    subplot(2,2,3);
    imagesc(squeeze(img(:,:,imsize(3)/2)));
    title('short-axis');
    axis equal
    colormap gray
elseif (animate)
    if (nargin == 2)
        delay = 0.02;
    end
    
    % Pick smallest increment
    smin = min(imsize);
    imsize2 = [0, 0, 0]; 
    for i=1:smin
        imsize2 = imsize2 + [imsize(1) / smin, imsize(2) / smin, imsize(3) / smin];
        
        % Mid long-axis slice (Coronal)
        subplot(2,2,1);
        imagesc(rot90(squeeze(img(imsize2(1),:,:))));
        title('long-axis coronal');
        axis equal
        colormap gray

        % Mid long-axis slice (Sagittal)
        subplot(2,2,2);
        imagesc(rot90(squeeze(img(:,imsize2(2),:))));
        title('long-axis sagittal');
        axis equal
        colormap gray

        % Mid short-axis slice (Transverse)
        subplot(2,2,3);
        imagesc(squeeze(img(:,:,imsize2(3))));
        title('short-axis');
        axis equal
        colormap gray

        pause(delay);
    end
end


end