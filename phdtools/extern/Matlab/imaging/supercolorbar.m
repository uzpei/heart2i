function h = supercolorbar(varargin)

h = colorbar; 
hsize = 0.7;
set(h, 'Position', [.92 (1-hsize) / 2 .04 hsize]) 

if (nargin > 0)
    m = varargin{1};
    
    % Shift all subplots to the left
    for i=1:m
        h = subplot(1,m,i);
        pos = get(h, 'Position');
        set(h, 'Position', [pos(1) - 0.05, pos(2), pos(3), pos(4)]) 
    end
end
