function h = subcolorbar(m, varargin)

if (nargin > 1) 
    n = varargin{1};
    num = varargin{2};
end

h = colorbar; 
hsize = 0.7;
set(h, 'Position', [.88 (1-hsize) / 2 .04 hsize]) 

if (nargin > 1)
    % Shift all subplots to the left
    for i=1:num
        h = subplot(m,n,i);
        pos = get(h, 'Position');
        set(h, 'Position', [pos(1) - 0.05, pos(2), pos(3), pos(4)]) 
    end
else
    % Shift all subplots to the left
    for i=1:m
        h = subplot(1,m,i);
        pos = get(h, 'Position');
        set(h, 'Position', [pos(1) - 0.05, pos(2), pos(3), pos(4)]) 
    end
end