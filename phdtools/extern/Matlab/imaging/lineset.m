function l = lineset(i)

line_styles = { '-', '--', '-.', ':' };

l = line_styles{1 + mod(i - 1, numel(line_styles))};

end