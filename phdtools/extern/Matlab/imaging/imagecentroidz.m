function imagecentroidz(V)

[sx, sy, sz] = size(V);

% Precompute centroids along Z-axis
centroids = zeros(sz, 2);
for z=1:sz
    maskSlice = V(:,:,z);
    centroid = fcentroid(maskSlice);
    centroids(z,:) = centroid(:);
end

%% Plot centroid
clf
colormap gray
for z=1:sz
    hold on
    image3dslice(V, 'z', z);
    centroid = centroids(z, :);
    plot(centroid(1), centroid(2), '-ro', 'MarkerSize', 20);
    text(centroid(1), centroid(2), sprintf('C = (%d, %d)', round(centroid(1)), round(centroid(2))), 'FontSize', 32, 'Color', [1 0 0]);
    hold off    
    pause(0.1);
end