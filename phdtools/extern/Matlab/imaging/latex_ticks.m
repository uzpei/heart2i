function latex_ticks(selectedAxis, latexLabels, fontSize)

stickLabel = [selectedAxis, 'ticklabel'];
stick = [selectedAxis, 'tick'];
xShiftRatio = 0.01;

%Remove tick labels
set(gca,stickLabel,[]) 
set(gca,stick,1:numel(latexLabels));

ax = axis; 
yTicks = get(gca,'ytick');
xTicks = get(gca, 'xtick');

if (strcmpi(selectedAxis, 'y'))
    HorizontalOffset = 0.1;
    for i = 1:length(latexLabels)
        text(ax(1) - HorizontalOffset,yTicks(i),['$' num2str( yTicks(i)) '$'], 'HorizontalAlignment','Right','interpreter', 'latex');   
    end
end

if (strcmpi(selectedAxis, 'x'))
    % Shift downwards
    ylims = ylim;
    xlims = xlim;
    yOffset = ylims(1) - 0.05 * (ylims(2) - ylims(1));
    
    % Shift to the right
    xOffset = xShiftRatio * (xlims(2) - xlims(1));
    for i = 1:length(latexLabels)
        text(xTicks(i) + xOffset, yOffset, latexLabels(i),'HorizontalAlignment','center','interpreter', 'latex', 'FontSize', fontSize);   
    end
end

