% Crop an image foler based on a mask
% All volumes will be croped to the tightest bounding box around the mask.
function cropimage_folder(searchPath, varargin)

% Define defaults
options = parseInput(struct('MaskName', 'mask.mat'), varargin{:});

listing = dir(searchPath);

fprintf('---> Searching [%s] 3D MAT files to crop using mask name [%s]...\n', searchPath, options.MaskName);

% First load the mask
mask = openmask(searchPath, options.MaskName);

if (isempty(mask))
    error('Mask [%s] not found. Aborting\n', options.MaskName);
end

foundFiles = [];

% Go through all files and search for MAT volumes and mask
for file_index = 1:length(listing)
    % Get the filename
    filename = listing(file_index).name;
    
    % Search for mat files
    if (~isempty(strfind(filename, '.mat')))
        foundFiles(length(foundFiles)+1).name = filename;
    end    
end

allFiles = '';
for file_index = 1:length(foundFiles)
    allFiles = [allFiles, foundFiles(file_index).name, ', '];
end

% List folders that were found
fprintf('Loading volumes for MAT files: %s\n', allFiles);

% Apply cropping on the mask to get the bounding box
[~, maxima] = cropimage(mask);

% Begin loading volumes
for file_index = 1:length(foundFiles)
    file_path = [searchPath, foundFiles(file_index).name];
    fprintf('-> Loading %s...\n', file_path);
    V = loadmat(file_path);
    
    % Only consider volumes which have the same size as the mask
    if ((ndims(V) ~= ndims(mask)) | any(size(V) ~= size(mask)))
        fprintf('Dimensions do not match, skipping [%s]...\n', foundFiles(file_index).name);
        continue;
    end
    
    % Crop the volume
%     V = cropimage(V);
    V = V(maxima(1,1):maxima(1,2), maxima(2,1):maxima(2,2), maxima(3,1):maxima(3,2));
    V = padarray(V,[1 1 1],0,'both');
    
    % Save
    save(file_path, 'V');
end

end