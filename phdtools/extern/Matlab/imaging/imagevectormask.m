function imagevectormask(v, mask, sliceAxis, varargin)

    if (nargin == 4)
        slice = varargin{1};
        res = 1;
    elseif (nargin == 5)
        slice = varargin{1};
        res = varargin{2};
    else
        res = 1;
        slice = 'z';
    end
    
    clf
    hold on
    image3dslice(1-mask, sliceAxis, slice);
    colormap jet
    quiverslice(mask, v, sliceAxis, slice, res);
    axis image
    hold off
    