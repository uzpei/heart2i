function colormap_export(out_dir, out_name, varargin)
limits = caxis

clf
h = colorbar([0.05 0.1 0.2 0.85]);
axis off
box off
%set(allchild(gca),'visible','off');
%set(gca,'visible','off')
%set(gcf, 'Position', [500 500 350 600])
%set(gcf, 'OuterPosition', [100, 100, 200, 500]);
if (nargin > 2)
    limits = varargin{1};
end

fontSize = 32;
if (nargin > 3)
    fontSize = varargin{2};
end

caxis(limits);

pos = get(gcf, 'position');
set(gcf, 'Position', [pos(1) pos(2) 250 500])

%set(h,'OuterPosition',[0 0 0.2 1]);
set(h,'fontsize', fontSize); 
outpath = [out_dir, out_name, '.pdf']
mkdir(fileparts(outpath));

export_fig(outpath, '-transparent', '-q101', '-nocrop');

    