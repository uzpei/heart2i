function frame_export(F, mask, label, outpath)

clf

quiverslice(mask, F(:,:,:,1:3), 'x');axis off
export_fig(outpath, [label,'_yz.pdf'], '-transparent', '-q101');

quiverslice(mask, F(:,:,:,4:6), 'y');axis off
export_fig(outpath, [label,'_xz.pdf'], '-transparent', '-q101');

quiverslice(mask, F(:,:,:,7:9), 'z');axis off
export_fig(outpath, [label,'_xy.pdf'], '-transparent', '-q101');

