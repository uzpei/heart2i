% function h = animate3d(volume, speed, numcycles)
function h = animate3d(volume, varargin)

[sx, sy, sz] = size(volume);

if (nargin == 1)
    speed = 0.1;
    numcycles = 1;
    clims(1) = min(volume(:));
    clims(2) = max(volume(:));
elseif (nargin == 2)
    speed = varargin{1};
    numcycles = 1;
    clims(1) = min(volume(:));
    clims(2) = max(volume(:));
elseif (nargin == 3)
    speed = varargin{1};
    numcycles = varargin{2};
    clims(1) = min(volume(:));
    clims(2) = max(volume(:));
elseif (nargin == 4)
    speed = varargin{1};
    numcycles = varargin{2};
    clims = varargin{3};
end

% Fix color limits
%clims = 0.25 * [min(volume(:)), max(volume(:))];

index = 0;
cyclecount = 0;

while (true)
    slices = 1 + [mod(index, sx), mod(index, sy), mod(index, sz)];
%    image3d(volume, slices, clims);
    image3d(volume, slices, clims);
    
    index = index + 1;
    
    if (mod(index, max(size(volume))) == 0)
        cyclecount = cyclecount + 1;
    end

    if (cyclecount >= numcycles)
        break;
    end
    
%    caxis(clims);
    
    % Pause animation
    pause(speed);
end

fprintf('Animation terminated after %i cycles\n', numcycles);

end