% Crop a 3D volume by removing spurious 0-voxels and leaving out only the
% outer rectangular non-zero envelope.
function [maskOut, maxima] = cropimage(mask, varargin)

if (nargin > 1)
    zeropad = varargin{1};
else
    zeropad = true;
end

sx = size(mask,1);
sy = size(mask,2);
sz = size(mask,3);

bounds.x.min = [];
bounds.x.max = [];
bounds.y.min = [];
bounds.y.max = [];
bounds.z.min = [];
bounds.z.max = [];

for z=1:sz
    slice = squeeze(mask(:,:,z));
%    slice = slice';
%     size(mask)
%     z
%     size(slice)

    % Find minimum/max row
    [rowIdx,colIdx] = find(slice);
    
    minValues = accumarray(colIdx,rowIdx,[],@min);
    minValues(minValues==0) = [];
    minValue = min(minValues);
    
    if (numel(minValue) > 0)
        bounds.x.min = [bounds.x.min; minValue];
    end

    maxValue = max(accumarray(colIdx,rowIdx,[],@max));
    if (numel(maxValue) > 0)
        bounds.x.max = [bounds.x.max; maxValue];
    end

    % Find minimum/max column
    [rowIdx,colIdx] = find(slice);
    minValues = accumarray(rowIdx,colIdx,[],@min);
    minValues(minValues==0) = [];
    minValue = min(minValues);
    
    if (numel(minValue) > 0)
        minValue;
        bounds.y.min = [bounds.y.min; minValue];
    end

    maxValue = max(accumarray(rowIdx,colIdx,[],@max));
    if (numel(maxValue) > 0)
        bounds.y.max = [bounds.y.max; maxValue];
    end

end

maxima = zeros(3,2);
maxima(1,1) = min(bounds.x.min);
maxima(1,2) = max(bounds.x.max);
maxima(2,1) = min(bounds.y.min);
maxima(2,2) = max(bounds.y.max);

% Z span
for x=maxima(1,1):maxima(1,2)
    slice = squeeze(mask(x,maxima(2,1):maxima(2,2),:));

    % Find minimum/max z-slice
%     [rowIdx,colIdx] = find(slice);
    [colIdx,rowIdx] = find(slice);
    
    minValues = accumarray(colIdx,rowIdx,[],@min);
    minValues(minValues==0) = [];
    minValue = min(minValues);
    
    if (numel(minValue) > 0)
        minValue;
        bounds.z.min = [bounds.z.min; minValue];
    end

    maxValue = max(accumarray(colIdx,rowIdx,[],@max));
    if (numel(maxValue) > 0)
        bounds.z.max = [bounds.z.max; maxValue];
    end
end

maxima(3,1) = min(bounds.z.min);
maxima(3,2) = max(bounds.z.max);

% Crop volume and zero-pad by one.
maskOut = mask(maxima(1,1):maxima(1,2), maxima(2,1):maxima(2,2), maxima(3,1):maxima(3,2));

if (zeropad)
    fprintf('* Zero-padding cropped image\n');
    maskOut = padarray(maskOut,[1 1 1],0,'both');
end

% Print info if necessary
vinit = numel(mask);
vout = numel(maskOut);
if (vinit ~= vout)
    [sxOut, syOut, szOut] = size(maskOut);
    fprintf('Initial size = %i, %i, %i\n', sx, sy, sz);
    fprintf('Cropped size = %i, %i, %i\n',  sxOut, syOut, szOut);
    fprintf('Initial number of voxels = %i\n', vinit);
    fprintf('Cropped number of voxels = %i\n', vout);
    fprintf('%.2f %% of original\n', 100 * vout / vinit);
end
end