% Taken from http://www.mathworks.com/products/curvefitting/examples.html?file=/products/demos/shipping/curvefit/histodem.html
function [p, mde] = plotsmooth(y, bins, titleText, xLabel, color, showHist, showExtras, kmag, kres, stdf, lineStyle)

if (nargin < 11)
    lineStyle = '-';
end

interpolate = false;
showLines = false;

% Compute stats
mu = mean(y);
stdv = std(y);

% Compute and store histogram info
[heights,centers] = hist(y, bins);

if (interpolate)
	%We would like to derive from this histogram a smoother approximation to the underlying distribution. We do this by constructing a spline function f whose average value over each bar interval equals the height of that bar.
	%If h is the height of one of these bars, and its left and right edges are at L and R, then we want the spline f to satisfy
	%integral {f(x) : L < x < R}/(R - L) = h,

	hold on
	n = length(centers);
	w = centers(2)-centers(1);
	t = linspace(centers(1)-w/2,centers(end)+w/2,n+1);
	hold off

	dt = diff(t);
	Fvals = cumsum([0,heights.*dt]);

	% Adjust interval
	%size(t)
	%size(Fvals)

	F = spline(t, [0, Fvals, 0]);

	DF = fnder(F);  % computes its first derivative

	% Plot the resulting spline
	hold on
	fnplt(DF, 'r', 2)
	hold off
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot filtered enveloppe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Smooth the histogram (low pass filter)

% Use a 10-bin wide filter
% centered at 0 and with standard deviation 2
if (stdf > 0)
    npdf = normpdf(-3*stdf:3*stdf,0,stdf);
    heights = conv(heights, npdf, 'same');
end

% Normalize histogram by area
heights = abs(centers(2)-centers(1)) * heights/trapz(centers,heights);
%heights = heights / sum(heights);

% Add the left and right boundaries
%centers = [kmag(1), centers, kmag(2)];
%heights = [0, heights, 0];

% Draw the histogram bins
% (comment this for enveloppe only)
if (showHist)
%	hist(y, bins);
	h = bar(centers,heights, 'LineStyle', '-', 'BarLayout', 'stacked', 'EdgeColor', [1 0 0], 'FaceColor', 'b');
    set(h,'FaceColor',[0.7 0.7 0.7],'EdgeColor','k');

end

lheight = max(heights(:));

% Find mode (max height)
[Y, I] = max(heights(:));
mde = centers(I);

hold on
%plot(centers, heights, 'r');
plot(centers, heights, 'LineWidth', 3, 'Color', color, 'LineStyle', lineStyle);
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot mean and zero-line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (showLines)
	H = line([mu mu], [0 1.1 * lheight], 'LineWidth',1, 'Color', [0 0.8 0]);
	H = line([0 0], [0 1.1 * lheight], 'LineWidth',1, 'Color', [0 0 0]);
	%set(H, 'color', 'green');
end	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(gca, 'XTick', [kmag(1):kres:kmag(2)]);
axis([kmag(1) kmag(2) 0 1.1 * lheight]);

% Find text limits in pixels
xfig = get(gcf, 'Position');
xt = round(0.01 * xfig(3));
yt = round(0.79 * xfig(4));
XL = xlim;
limx = XL(2);

if (showExtras) 
    fontSize = 20;
	t1=text(xt, yt - 0,  ['count = ', num2str(size(y,1))], 'Units', 'Pixels');
%    yt = yt - 10;
%	t2=text(xt, yt - 10, [' bin count = ', num2str(bins)], 'Units', 'Pixels');
    yt = yt - fontSize;
	t3=text(xt, yt,      [' mean = ', num2str(mu, '%.4f')], 'Units', 'Pixels');
    yt = yt - fontSize;
	t4=text(xt, yt,      [' mode = ', num2str(mde, '%.4f')], 'Units', 'Pixels');
    yt = yt - fontSize;
	t5=text(xt, yt,      [' std = ', num2str(stdv, '%.4f')], 'Units', 'Pixels');

	set(t1, 'FontName', 'Monospaced', 'FontSize', fontSize);
%	set(t2, 'FontName', 'Monospaced');
	set(t3, 'FontName', 'Monospaced', 'FontSize', fontSize);
	set(t4, 'FontName', 'Monospaced', 'FontSize', fontSize);
	set(t5, 'FontName', 'Monospaced', 'FontSize', fontSize);

	if (showLines)
		legend('Histogram', 'low-pass filter', '\mu', 'zero')
% 	else
% 		legend('Histogram', 'low-pass filter')
	end
end

if (length(titleText > 0))
    title(titleText);
end

if (length(xLabel) > 0)
    xlabel(xLabel);
end
%ylabel('normalized count');

p = lheight;