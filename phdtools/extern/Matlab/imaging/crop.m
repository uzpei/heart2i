function Vc = crop(V, cx, cy, cz)

%cz(1) = cz(1) - 1;
%cz(2) = cz(2) + 1;

dx = 1 + cx(2) - cx(1);
dy = 1 + cy(2) - cy(1);
dz = 1 + cz(2) - cz(1);

Vc = zeros(dx, dy, dz);

Vc(:,:,:) = V(cx(1):cx(2), cy(1):cy(2), cz(1):cz(2));