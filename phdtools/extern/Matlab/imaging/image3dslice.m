function imgslice = image3dslice(volume, varargin)

[sx, sy, sz, ndim] = size(volume);

if (nargin > 1)
    sliceAxis = varargin{1};
else
    sliceAxis = 'z';
end

if (isnumeric(sliceAxis))
    if (sliceAxis == 1)
        sliceAxis = 'x';
    elseif (sliceAxis == 2)
        sliceAxis = 'y';
    elseif (sliceAxis == 3)
        sliceAxis = 'z';
    end
end

if (strcmpi(sliceAxis, 'x'))
    if (nargin > 2)
        slice = varargin{2};
    else
        slice = round(sx/2);
    end

    imgslice = squeeze(volume(slice,:,:))';
    for factor=0:ndim - 1
        ibeg = factor * sz + 1;
        iend = (factor+1) * sz;
        imgslice(ibeg:iend, :) = imgslice(iend:-1:ibeg,:);
    end
    
elseif (strcmpi(sliceAxis, 'y'))
    if (nargin > 2)
        slice = varargin{2};
    else
        slice = round(sy/2);
    end
    
    imgslice = squeeze(volume(:,slice,:))';
    
    for factor=0:ndim - 1
        ibeg = factor * sz + 1;
        iend = (factor+1) * sz;
        imgslice(ibeg:iend, :) = imgslice(iend:-1:ibeg,:);
    end


elseif (strcmpi(sliceAxis, 'z'))
    if (nargin > 2)
        slice = varargin{2};
    else
        slice = round(sz/2);
    end

    imgslice = squeeze(volume(:,:,slice,:));
    imgslice = ipermute(imgslice, [1 3 2]);
    imgslice = reshape(imgslice, ndim * sx, sy);
    
    if (ndim > 1)
        imgslice = ipermute(imgslice, [1 3 2]);
        imgslice = reshape(imgslice, ndim * sx, sy);
    end
%     imgslice = flipud(imgslice);
%     imgslice = fliplr(imgslice);

    
else
    imgslice = [];
end

imagesc(imgslice);
