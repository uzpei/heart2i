function image3dreconstructionerror(Voriginal, mask, Vreconstruction, varargin)

fexport = false;
sliceAxis = 'x';

if (nargin >= 4)
    
    if (strcmpi(varargin{1},'x') || strcmpi(varargin{1},'y') || strcmpi(varargin{1},'z'))
        sliceAxis = varargin{1};
    else
        % Assume we are exporting
        fexport = true;
        exportfile = varargin{1};
        
        image3dreconstructionerror(Voriginal, mask, Vreconstruction, 'x');
        export_fig([exportfile, '_x.pdf'], '-transparent', '-q101'); 
        image3dreconstructionerror(Voriginal, mask, Vreconstruction, 'y');
        export_fig([exportfile, '_y.pdf'], '-transparent', '-q101'); 
        image3dreconstructionerror(Voriginal, mask, Vreconstruction, 'z');
        export_fig([exportfile, '_z.pdf'], '-transparent', '-q101'); 
    end
end

% Make sure mask is of the right dimension
[sx sy sz nd] = size(Voriginal);

if size(mask,4) ~= size(Voriginal,4)
    mask = repmat(mask, [1 1 1 nd]);
end

Vmask = Voriginal;
Vmask(~mask) = 0;

hold on
subplot(141);
image3dslice(Voriginal, sliceAxis);
title('original')
axis image

subplot(142);
image3dslice(Vmask, sliceAxis);
title('mask');
axis image

subplot(143);
image3dslice(Vreconstruction, sliceAxis);
title('interpolated');
axis image

subplot(144);
vdiff = Voriginal-Vreconstruction;
%vdiff(~Vmask) = 0;
image3dslice(vdiff, sliceAxis);
alims = axis;
text(0.05* (alims(2)-alims(1)), 0.05* (alims(4)-alims(3)), sprintf('E = %.2f',norm(vdiff(:),2)));
title('error');
axis image

colormap(customap('redwhiteblue'))

if (nargin == 5)
    caxis(varargin{2});
end

subcolorbar(4)
hold off
