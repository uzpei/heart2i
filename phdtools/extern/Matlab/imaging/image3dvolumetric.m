function image3dvolumetric(vol,n,ratio)
clf
%set(gcf, 'Position', get(0,'Screensize'));
hold on;
[sx, sy, sz] = size(vol);
zo = 2;
dz = round(ratio * sz / n)
zf = round(ratio * sz)
for z = zo:dz:zf
    slice = image3dslice(vol, 'z', z);

    % Increase resolution
    res = 1;
    [X,Y] = meshgrid(1:res:sy, 1:res:sx);
    sliceHigh = interp2(slice,X,Y);
    Z = ones(numel(1:res:sx), numel(1:res:sy)) + z;
    surf(X, Y, Z, sliceHigh, 'EdgeColor','None');
end
hold off;

camlight(-30, 0); lighting phong;
view(-45.0,20.0);
grid off;
hidden off;
axis off;
axis square;
zoom(1.3);
set(gcf, 'Color', 'white');
h=colorbar;
set(h, 'FontSize', 72);
hsize = 0.7;
set(h, 'Position', [.8 (1-hsize) / 2 .04 hsize]);
