function image_diff(A, B, slice)

cmin = min(A(:));
cmax = max(A(:));
clims = [cmin, cmax];

alims = axis;

Al = A(:);
Al(Al == 0) = [];
Bl = B(:);
Bl(Bl == 0) = [];

nbins = 10 * round(log(numel((Al)) + 1));
%nbins = numel(A) / 10;

clf
%%
subplot(211)
hold on
% imagesc(A(:,:,slice));
histnorm(Al, nbins, 0, 'Color', colorset(1));
% text(0.1 * (alims(2)-alims(1)), 0.95 * (alims(4)-alims(3)), sprintf('M=%.4f', max(abs(A(:)))));
% text(0.1 * (alims(2)-alims(1)), 0.85 * (alims(4)-alims(3)), sprintf('A=%.4f', mean(abs(A(:)))));
%title('Volume 1');

%subplot(132)
histnorm(Bl, nbins, 0, 'Color', colorset(2));
%title('Volume 2');
% text(0.1 * (alims(2)-alims(1)), 0.95 * (alims(4)-alims(3)), sprintf('M=%.4f', max(abs(B(:)))));
% text(0.1 * (alims(2)-alims(1)), 0.85 * (alims(4)-alims(3)), sprintf('A=%.4f', mean(abs(B(:)))));
legend({'A', 'B'})
hold off
xlims = xlim;

subplot(212)
cdiff = A - B;
cdiff = cdiff(:);
histnorm(cdiff, nbins, 0, 'Color', [0 0 0]);
xlim(xlims);
% caxis(clims);
%title('Diff');
% text(0.1 * (alims(2)-alims(1)), 0.95 * (alims(4)-alims(3)), sprintf('M=%.4f', max(abs(cdiff(:)))));
% text(0.1 * (alims(2)-alims(1)), 0.85 * (alims(4)-alims(3)), sprintf('A=%.4f', mean(abs(cdiff(:)))));
