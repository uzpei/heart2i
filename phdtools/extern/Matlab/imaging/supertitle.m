function supertitle(text)
    set(gcf,'NextPlot','add');
    axes;
    h = title(text);
    set(gca,'Visible','off');
    set(h,'Visible','on');
end