% % jetsmooth
% 
% % Define built-in colormaps
% 
% % Number of color levels to create
% nLevels = 40;
% 
% figure(1);
% clf
% 
% % Color map
% cData = rand(nLevels,3);
% 
% % Number of color samples to display
% numSamples = nLevels;
% %xData = linspace(1, nLevels, nLevels);
% xData = [linspace(0, 15, nLevels); linspace(1, 16, nLevels); ...
%     linspace(1, 16, nLevels); linspace(0, 15, nLevels)];
% 
% 
% % Make each line of height 1
% yData = ones(1, numel(xData));
% 
% % Display colormap chart
% patch('XData', xData, 'YData', yData, ...
%     'EdgeColor', 'none', ...
%     'FaceColor', 'flat', ...
%     'FaceVertexCData', cData);
% rectangle('Position', [0, offset, 16, 1.5], 'Curvature', [0 0])
%     
% axis equal off;
% title('Built-in Colormaps');
cData = rand(1, 32, 3);
cData = [


cData = repmat(cData, [5, 1, 1]);
image(cData)
zoom(2.5)
axis image