function customSubplot(dataOriginal, data, dataRecon, dataMean, dataDiff)
    dsize = size(data);
    
    subplot(351)
    ximage(dataOriginal, 'original', dsize);
    subplot(352)
    ximage(data, 'sample', dsize);
    subplot(353)
    ximage(dataRecon, 'reconstruction', dsize);
    subplot(354)
    ximage(dataMean, 'mean', dsize);
    subplot(355)
    ximage(dataDiff, 'error', dsize);

    subplot(356)
    yimage(dataOriginal, 'original', dsize);
    subplot(357)
    yimage(data, 'sample', dsize);
    subplot(358)
    yimage(dataRecon, 'reconstruction', dsize);
    subplot(359)
    yimage(dataMean, 'mean', dsize);
    subplot(3,5,10)
    yimage(dataDiff, 'error', dsize);

    subplot(3,5,11)
    zimage(dataOriginal, 'original', dsize);
    subplot(3,5,12)
    zimage(data, 'sample', dsize);
    subplot(3,5,13)
    zimage(dataRecon, 'reconstruction', dsize);
    subplot(3,5,14)
    zimage(dataMean, 'mean', dsize);
    subplot(3,5,15)
    zimage(dataDiff, 'error', dsize);

end

function ximage(data, ttext, dsize)
    selSlice = round(dsize(1)/2);
    dslice = squeeze(data(selSlice,:, :))';
    dslice(1:end, :) = dslice(end:-1:1,:);
    myImagesc(dslice, ttext);
end

function yimage(data, ttext, dsize)
    selSlice = round(dsize(2)/2);
    dslice = squeeze(data(:,selSlice, :))';
    dslice(1:end, :) = dslice(end:-1:1,:);
    myImagesc(dslice, ttext);
end

function zimage(data, ttext, dsize)
    selSlice = round(dsize(3)/2);
    dslice = squeeze(data(:,:, selSlice))';
%    dslice(1:end, :) = dslice(end:-1:1,:);
    myImagesc(dslice, ttext);
end


function myImagesc(img, ttext)
    imagesc(img);
    title(ttext);
    axis image
    caxis([-0.5, 0.5]);
    colorbar;
end
