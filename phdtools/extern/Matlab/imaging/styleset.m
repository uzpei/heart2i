function s = styleset(i)

styles = { '-', '--', ':', '-.' };

s = styles{1 + mod(i - 1, length(styles))};

end