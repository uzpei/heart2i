% function c = colorset(i)

function c = colorset(i)
% Could also use predefined color maps
%colormap hsv;
%colors = colormap;
%colors = ['k'; 'r'; 'b'; 'm'];

colors = ...
[ 1 0 0 % 1 RED
   0 0 1 % 2 BLUE
   0 0.75 0.75 % 8 TURQUOISE
   0 0.5 0 % 9 GREEN (dark)
   0 1 1 % 5 CYAN
   0.75 0 0.75 % 12 MAGENTA (dark)
   1 0.50 0.25 % 11 ORANGE
   0 1 0 % 4 GREEN (pale)
   1 0 1 % 6 MAGENTA (pale)
   1 1 0 % 3 YELLOW (pale)
   0 0 0 % 7 BLACK
   0.75 0.75 0 % 10 YELLOW (dark)
   0.7 0.7 0.7 % 13 GREY
   0.8 0.7 0.6 % 14 BROWN (pale)
   0.6 0.5 0.4 ]; % 15 BROWN (dark)

c = colors(1 + mod(i - 1, size(colors, 1)), :);

end