% function h = quiverslice(mask, varargin)
% varargin =
% mask, 4D matrix T(:,:,:,3)
% mask, 4D matrix T(:,:,:,3), slice
% mask, U, V, W
% mask, U, V, W, slice
function [h1, h2, h3] = quiverslice3d(mask, varargin)

    [sx, sy, sz] = size(mask);

    slicez = round(sz/2);
    slicex = round(sx/2);
    slicey = round(sy/2);
    use_subplot = true;

    if (nargin == 2)
        % mask, 4D matrix T(:,:,:,3)
        F = varargin{1};
    %    U = F(:,:,:,1);
    %    V = F(:,:,:,2);
    %    W = F(:,:,:,3);

    elseif (nargin == 3)
        % mask, 4D matrix T(:,:,:,3), slice
        F = varargin{1};
    %    U = F(:,:,:,1);
    %    V = F(:,:,:,2);
    %    W = F(:,:,:,3);
        slicez = varargin{2};

    % elseif (nargin == 4)
    %     % mask, U, V, W
    %     U = varargin{1};
    %     V = varargin{2};
    %     W = varargin{3};
    % 
    % elseif (nargin == 5)
    %     % mask, U, V, W, slice
    %     U = varargin{1};
    %     V = varargin{2};
    %     W = varargin{3};
    %     slicez = varargin{4};

    elseif (nargin == 4)
        F = varargin{1};
        use_subplot = varargin{3};
    end

    % Short-axis view
    if (use_subplot)
        subplot(2,2,1);
    else
        figure(1)
    end
    quiverslice(mask, F, 'z', slicez);

    % Long axis view 1 (YZ)
    if (use_subplot)
        subplot(2,2,3);
    else
        figure(2)
    end
    quiverslice(mask, F, 'x', slicex);

    % Long axis view 2 (XZ)
    if (use_subplot)
        subplot(2,2,4);
    else
        figure(3)
    end
    quiverslice(mask, F, 'y', slicey);

end


