% function cmap = customap(mapname, clength)
function cmap = customap(mapname, varargin)

% Brightness fraction/value (color), brightness level (intensity)
clength = 256;

if (nargin > 1)
    clength = varargin{1};
end

if (strcmp(mapname, 'redblackblue'))
    % Black spread
    bs = 0.05;
    
    % Black intensity
    bi = 0.3;
    
    % Scale
    bc = 1.2;
    
    MB = [0,1;
%        0.25, 0.75;
        0.5 - bs,bi;
        0.5, 0;
        ];
    MR =[0,0;
        0.5, 0;
        0.5 + bs,bi;
        1,1];
    % MB=[0,0; 
    %     0.7,0;
    %     1,1];
    MG = [0,0; 1,0];
    cmap = min(bc * colormapRGBmatrices(clength,MR,MG,MB),1);
elseif (strcmp(mapname, 'redwhiteblue2'))
    MB = [0,1; 1,1; 0,0;];
    MG =[0,0; 1,1; 0,0];
    MR =[0,0; 1,1; 1,1];
    cmap = colormapRGBmatrices(clength,MR,MG,MB);
elseif (strcmp(mapname, 'redwhiteblue'))
    % Black spread
    bs = 0.45;
    
    MB = [0,1;
        0.5,1;
        0.5 + 0.1,0;
        ];
    MG =[0,0;
        0.5 - bs,0;
        0.5,1;
        0.5 + bs,0];
    MR =[
        0.5 - 0.1,0;
        0.5,1;
        1,1];
    cmap = colormapRGBmatrices(clength,MR,MG,MB);
elseif (strcmp(mapname, 'hotinv'))
    cmap = hot(clength);
    cmap(1:end,:) = cmap(end:-1:1,:);
else
    rand(5)
    cmap = customap('redblackblue');
end
end

function mymap = colormapRGBmatrices( N, rm, gm, bm)
  
  x = linspace(0,1, N);
%   rv = interp1( unique(rm(:,1)), unique(rm(:,2)), x);
%   gv = interp1( unique(gm(:,1)), unique(gm(:,2)), x);
%   mv = interp1( unique(bm(:,1)), unique(bm(:,2)), x);

  rv = interp1( rm(:,1), rm(:,2), x);
  gv = interp1( gm(:,1), gm(:,2), x);
  mv = interp1( bm(:,1), bm(:,2), x);
  mymap = [ rv', gv', mv'];
  %exclude invalid values that could appear
  mymap( isnan(mymap) ) = 0;
  mymap( (mymap>1) ) = 1;
  mymap( (mymap<0) ) = 0;
end
