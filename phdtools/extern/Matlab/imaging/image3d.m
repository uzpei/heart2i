% Todo: add histogram?
% varargin: slices, clims
function image3d(volume, varargin)

[sx, sy, sz, sv] = size(volume);

% Volume and slice selection
if (nargin == 2)
    slices = varargin{1};
% Volume, slice selection and caxis
elseif (nargin == 3)
    slices = varargin{1};
    clims = varargin{2};
    minv = clims(1);
    maxv = clims(2);
else
    slices = round([sx/2, sy/2, sz/2]);
end

if (isempty(slices))
    slices = round([sx/2, sy/2, sz/2]);
end

[vx, vy, vz] = computevolumes(volume, slices);

% Color axis
if (nargin ~= 3)
    allv = [vx(:); vy(:); vz(:)];
    minv = min(allv);
    maxv = max(allv);
end

if (minv == maxv)
    maxv = minv + 1;
end

usecolorbar = true;
clims = [minv, maxv];

% Short-axis view
subplot(1,3,1);
formatfig(vz, 'y', 'x', sprintf('z=%i',slices(3)), clims);

% Long axis view 1
subplot(1,3,2);
formatfig(vx, 'y', 'z', sprintf('x=%i',slices(1)), clims);

% Long axis view 2
subplot(1,3,3);
formatfig(vy, 'x', 'z', sprintf('y=%i',slices(2)), clims);

if (usecolorbar)
    subcolorbar(3);
end

end

function formatfig(img, xlbl, ylbl, titlelbl, clims)
    fontSize = 32;
    
    imagesc(img);

    xh = xlabel(xlbl);
    yh = ylabel(ylbl);
    axis image
    %set(gcf, 'Ydir', 'normal');
    
    set(gca,'XTick',[])
    set(gca,'YTick',[])
%    set(gca, 'FontSize', 32);
    
    set(xh,'FontSize', fontSize);
    set(yh,'FontSize', fontSize);
%    colorbar

    title(titlelbl);

    if (~(any(isnan(clims) | isinf(clims))))
        caxis([clims(1) clims(2)+eps])
    end

end

function [vx, vy, vz] = computevolumes(volume, slices)

% Check if this is a 4D volume
% if (size(volume, 4) > 1)
% end
% size(volume)
% slices

% Compute volumes
[sx, sy, sz, ndim] = size(volume);
vx = squeeze(volume(slices(1),:,:))';
vy = squeeze(volume(:,slices(2),:))';
vz = squeeze(volume(:,:,slices(3),:));

% Flip volumes
for factor=0:ndim - 1
    ibeg = factor * sz + 1;
    iend = (factor+1) * sz;
    vx(ibeg:iend, :) = vx(iend:-1:ibeg,:);
    vy(ibeg:iend, :) = vy(iend:-1:ibeg,:);
end
if (ndim > 1)
    vz = ipermute(vz, [1 3 2]);
    vz = reshape(vz, ndim * sx, sy);
end
end