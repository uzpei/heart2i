function harray = bardelta(x, dx, spacing)
Y = [x-dx, dx, dx];
harray = bar(Y,spacing, 'stacked');

%% Set delta colors
set(harray(2), 'FaceColor', [1 1 1]);
set(harray(3), 'FaceColor', [1 1 1]);

set(harray(2), 'LineStyle', '-');
set(harray(3), 'LineStyle', '-');

%% Hide bottom
set(harray(1), 'FaceColor', 'none');
set(harray(1), 'LineStyle', 'none');

%% Insert line at x
lwidth = 0.3;
for i=1:length(x)        
    lh = line([(1-lwidth)+i-1,1+lwidth+i-1],[x(i),x(i)]);
    set(lh, 'Color', [0 0 0]);
    set(lh, 'LineWidth', 2);
end

%% Set y labels
% The current ylim should be [0 max], where max
% is the x-dx + dx + dx = x + dx
ymin = min(x-dx);
ymax = max(x+dx);
ylim([ymin ymax]);

%% Draw baseline at 0 if it falls between ymin and ymax
if (ymin < 0 && ymax >0)
    lh = line([0, length(x)+1],[0, 0]);
%     set(lh, 'Color', [0.4 0.4 0.4]);
    set(lh, 'Color', [0 0 0]);
    set(lh, 'LineWidth', 1);
    set(lh,'LineStyle','-');
end

hBaseline = get(harray(1),'BaseLine');
% set(hBaseline,'LineStyle',':','Color','red','LineWidth',2);
set(hBaseline,'LineStyle','none');
          