function text_location(str, location, varargin)

fsize = 12;
if (nargin == 3)
    fsize = varargin{1};
end

lims = axis;

if (strcmpi(location, 'northwest'))
    text(0.75,0.90,str,'Units','normalized', 'FontSize', fsize);
elseif (strcmpi(location, 'northeast'))
    text(0.1,0.90,str,'Units','normalized', 'FontSize', fsize);
elseif (strcmpi(location, 'southeast'))
    text(0.1,0.1,str,'Units','normalized', 'FontSize', fsize);
elseif (strcmpi(location, 'southwest'))
    text(0.75,0.1,str,'Units','normalized', 'FontSize', fsize);
end
