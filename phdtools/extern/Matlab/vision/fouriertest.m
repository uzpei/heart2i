img = double(rgb2gray(imread('image.png')));

subplot(3,3,1)
imagesc(img);
title('original image');
colormap gray

% Compute Fourier transform
fimg = fft2(img);

subplot(3,3,2)
imagesc(angle(fftshift(fimg)));
title('phase angle');

subplot(3,3,3)
imagesc(log(abs(fftshift(fimg))));
title('phase amplitude');

% Convolution is just multiplication in the Fourier domain
% Embed the kernel in an image that is the same size as the
% the image (otherwise cannot use hadamard product in frequency space)
kernel = fspecial('gaussian', [20 20], 3);
[sx, sy] = size(img);
kernel = [kernel, zeros(


subplot(3,3,4);
imagesc(kernel);
title('filtered image');

% Bring kernel to Fourier domain
fkernel = fft2(kernel);
subplot(3,3,5)
imagesc(angle(fftshift(fkernel)));
title('phase angle');

%Set all zero values to minimum value
%fftkernel(find(fftkernel == 0)) = 1e-6;

subplot(3,3,6)
imagesc(log(abs(fftshift(fkernel))));
title('phase amplitude');

% Bring back to spatial domain
fimgi = ifft2(fimg);
subplot(3,3,7);
imagesc(fimgi);
title('filtered image');

subplot(3,3,8)
imagesc(angle(fftshift(fimg)));
title('phase angle');

subplot(3,3,9)
imagesc(log(abs(fftshift(fimg))));
title('phase amplitude');
