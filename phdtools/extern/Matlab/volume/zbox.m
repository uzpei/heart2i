function zbounds = zbox(mask, varargin)

if (nargin == 1)
    count_threshold = 1;
else
    count_threshold = varargin{1};
end

[sx sy sz] = size(mask);
z1 = NaN;
z2 = NaN;

for z=1:sz
    scount = numel(find(mask(:,:,z) == 1));
    
    if (scount >= count_threshold )
        z1 = z;
        break;
    end
end

for z=sz:-1:1
    scount = numel(find(mask(:,:,z) == 1));
    
    if (scount >= count_threshold)
        z2 = z;
        break;
    end
end

zbounds = [z1, z2];