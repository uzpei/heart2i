function Vr = reorder_vector_volume(V, order)

Vr(:,:,:,1) = V(:,:,:,order(1));
Vr(:,:,:,2) = V(:,:,:,order(2));
Vr(:,:,:,3) = V(:,:,:,order(3));

