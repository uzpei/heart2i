
% Split volume
clear all

%% Load data
dirdata = '../../data/heart/rats/rathearts.mat';
hearts = loadmat(dirdata);
mask = hearts.atlas.mask;

zcut = 10;
[sx, sy, sz] = size(mask);

%% Compute sampling
maskc = zeros(sx, sy, sz);
for z=28 + zcut:zcut:sz - 20
    maskc(:,:,z) = mask(:,:,z);
end

image3d(maskc);

% Pick some data


% Subtract mean from new volume


% Project onto curvature space 
