% function Xc = centroid(M)
function Xc = fcentroid(M)

%    idx = find(~isnan(M));
%    [X, Y] = ind2sub(size(M), idx);
%    Xsub = [X, Y];
%    %wX = sum(log(M(idx)+1));
%    wX = sum(M(idx));
%    Xc = sum(bsxfun(@times,Xsub,M(idx))) / wX; % center of mass

    bw = ~isnan(M);
    %bw = imfill(bw, 'holes');
    L = logical(bw);
    s = regionprops(L, 'PixelIdxList', 'PixelList');
    
    if (~isempty(s))
        idx = s(1).PixelIdxList;
        sum_region1 = sum(M(idx));
        x = s(1).PixelList(:, 1);
        y = s(1).PixelList(:, 2);

        xbar = sum(x .* double(M(idx))) / sum_region1;
        ybar = sum(y .* double(M(idx))) / sum_region1;

        Xc = [xbar, ybar];
    else
        Xc = [0 0];
    end
    
end

