function vslice = volslice(volume, sliceAxis, varargin)

[sx, sy, sz, ndim] = size(volume);

if (strcmpi(sliceAxis, 'x'))
    if (nargin > 2)
        slice = varargin{1};
    else
        slice = round(sx/2);
    end

    vslice = squeeze(volume(slice,:,:))';
    for factor=0:ndim - 1
        ibeg = factor * sz + 1;
        iend = (factor+1) * sz;
        vslice(ibeg:iend, :) = vslice(iend:-1:ibeg,:);
    end
    
elseif (strcmpi(sliceAxis, 'y'))
    if (nargin > 2)
        slice = varargin{1};
    else
        slice = round(sy/2);
    end
    
    vslice = squeeze(volume(:,slice,:))';
    
    for factor=0:ndim - 1
        ibeg = factor * sz + 1;
        iend = (factor+1) * sz;
        vslice(ibeg:iend, :) = vslice(iend:-1:ibeg,:);
    end


elseif (strcmpi(sliceAxis, 'z'))
    if (nargin > 2)
        slice = varargin{1};
    else
        slice = round(sz/2);
    end

    vslice = squeeze(volume(:,:,slice,:));
    vslice = ipermute(vslice, [1 3 2]);
    vslice = reshape(vslice, ndim * sx, sy);
    
    if (ndim > 1)
        vslice = ipermute(vslice, [1 3 2]);
        vslice = reshape(vslice, ndim * sx, sy);
    end

    
else
    vslice = [];
end
