% Render a volume anisotropic by increasing one or more dimensions.
% The original copied is copied within
function U = anisotropic_volume(V)

[sx, sy, sz, sn] = size(V);
[sx2, sy2, sz2, sn] = size(V);

% Find matching dimensions
if (sx2 == sy2)
    sy2 = sy2 + 1;
end

if (sy2 == sz2)
    sz2 = sz2 + 1;
end

U = zeros(sx2, sy2, sz2, sn);
U = cast(U, class(V));
U(1:sx, 1:sy, 1:sz, 1:sn) = V(1:sx, 1:sy, 1:sz, 1:sn);

