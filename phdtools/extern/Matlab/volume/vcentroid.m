% function Xc = vcentroid(vx, vy, mask)
% Estimate centroid of swirling location
function Xc = vcentroid(vx, vy, mask)

        % Compute adjacency matrix
        % use 8-connected neighbors
        nrc = numel(vx);
        [ic,icd] = ixneighbors(vx);
        S = sparse(ic,icd,ones(numel(icd),1),nrc,nrc);
        %spy(S)

        inds = 1:numel(vx);

        % Compute position matrix 
        sx = size(vx,1);
        sy = size(vx,2);
        [x,y] = meshgrid(1:sx, 1:sy);

        curlm = zeros(sx, sy);
        for i=inds

            if (~mask(i))
                continue
            end

            neighbors = find(S(i,:));

            % Compute r_ij for neighbors
            r = [(x(neighbors) - x(i))', (y(neighbors) - y(i))'];

            % Compute | r_ij x v_j | using Lagrange's identity
            v = [vx(neighbors)', vy(neighbors)'];
            v = normalizemulti(v);

            r2 = r(:,1).^2 + r(:,2).^2;
            v2 = v(:,1).^2 + v(:,2).^2;
            rv = dot(r', v')';

            curlm(i) = sum(r2 .* v2 - rv.^2);
        end

        % Could use this to find the max
%         [the_mx, idx] = max(curlm(:));
%         [rowmax, colmax] = ind2sub(size(curlm),idx);
        curlmp = exp(curlm .^ 1.5);
        curlmp(mask <= 0) = NaN;
        Xc = fcentroid(curlmp);
end

