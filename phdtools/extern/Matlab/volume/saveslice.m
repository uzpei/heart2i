% function saveslice(img, slice, filename, path, slicemod)
% Optional argument "slicemod" for saving only slice moduli
%
function saveslice(img, slice, filename, path, slicemod)
    if (nargin < 5)
        slicemod = 1;
    end
    
    if (mod(slice, slicemod) ~= 0)
        return
    end
    
    imgsliced = img(:,:,slice);
    save([path, filename, '_', int2str(slice)], 'imgsliced');
end