% ==================================================
% function labelVolume = volumeregions3d(volume, m, n)
% ==================================================
% 3D volume axial segmentation
% 1) Volume will be sliced in m regions along z-dimension:
% i.e. 0-1/m, 1/m-2/m, 2/m-3/m,..., (m-1) - 1
% 2) Volume will be sliced in n regions in the XY plane
% each region contains an arc of 2*pi / n
% path = ['../../data/heart/rats/', 'rat07052008', '/registered/processed/'];
% path = ['../../data/heart/canine/', 'CH06', '/registered/cropped/processed/'];
% volume = cell2mat(struct2cell(load([path, 'mask.mat']))) > 0;
% m=n=6
function [labelVolume, labelindices] = volumeregions3d(volume, m, n, varargin)
    [sx, sy, sz] = size(volume);

    
    if (nargin > 3)
        perSlice = varargin{1};
    else
        perSlice = true;
    end
    
    [Y, X, Z] = ndgrid(1:sx, 1:sy, 1:sz);

    labelVolume = zeros(sx, sy, sz);
    
    labelindices = 1:m*n;

%     dz = round(sz / m);
    dz = floor(sz / m);
    for zslice=1:m
        if (perSlice)
            % Compute centroid
            % of a few slices
            Xc = fcentroid(volume(:,:, 1 + (zslice-1) * dz : zslice * dz));
            X0 = X - Xc(1);
            Y0 = Y - Xc(2);
        else
            % Move X, Y to center
            X0 = X - sx / 2;
            Y0 = Y - sy / 2;
        end

        atanv = atan2(Y0, X0);
        
%         figure(1)
%         hold on
%         vol = atanv .* volume;
%         imagesc(vol(:,:,zslice * dz));
%         plot(Xc(1), Xc(2), 'wo', 'MarkerSize', 30);
%         hold off
%         pause(0.1)
        
        for angularslice=1:n
            
            theta1 = -pi + 2 * (angularslice-1) / n * pi;
            theta2 = -pi + 2 * (angularslice) / n * pi;

            % Compute Z-mask
            mask = Z >= (zslice-1) * dz & Z <= zslice * dz;
            
            % Compute angular mask
            if (angularslice == n)
                mask = mask & atanv >= theta1 & atanv <= theta2;
            else
                mask = mask & atanv >= theta1 & atanv < theta2;
            end
            
            labelVolume(mask & volume) = (zslice - 1) * n + angularslice;
        end
    end
    
    lavelVolume = mask;
    
%     % Z-slicing
%     figure(1)
%     mslice = round(sz / m);
%     labelVolume = zeros(size(volume));
%     for zindex=1:m
%         slice = Z >= (zindex-1) * mslice & Z <= zindex * mslice;
%         
%         labelVolume(slice) = volume(slice) * zindex * n;
%     end
% 
%     % XY angular slicing
%     for aindex=1:n
%         % Move X, Y to center
%         X0 = X - sy / 2;
%         Y0 = Y - sy / 2;
% 
%         atanv = atan2(Y0, X0);
% 
%         theta1 = -pi + 2 * (aindex-1) / n * pi;
%         theta2 = -pi + 2 * (aindex) / n * pi;
% 
%         % Include 0 for last pie
%         if (aindex == n)
%             slice = atanv >= theta1 & atanv <= theta2;
%         else
%             slice = atanv >= theta1 & atanv < theta2;
%         end
% 
%         % Apply threshold on mask
%         labelVolume(slice) = labelVolume(slice) + volume(slice) * (aindex - 1);
%     end

end
    
% % ==================================================
% % function labelVolume = volumeregions3d(volume, m, n)
% % ==================================================
% % 3D volume axial segmentation
% % 1) Volume will be sliced in m regions along z-dimension:
% % i.e. 0-1/m, 1/m-2/m, 2/m-3/m,..., (m-1) - 1
% % 2) Volume will be sliced in n regions in the XY plane
% % each region contains an arc of 2*pi / n
% % path = ['../../data/heart/rats/', 'rat07052008', '/registered/processed/'];
% % path = ['../../data/heart/canine/', 'CH06', '/registered/cropped/processed/'];
% % volume = cell2mat(struct2cell(load([path, 'mask.mat']))) > 0;
% % m=n=6
% function [labelVolume, labelindices] = volumeregions3d(volume, m, n, varargin)
%     [sx, sy, sz] = size(volume);
% 
%     
%     if (nargin > 3)
%         perSlice = varargin{1};
%     else
%         perSlice = false;
%     end
%     
%     [Y, X, Z] = ndgrid(1:sx, 1:sy, 1:sz);
% 
%     % TODO: use fcentroid
%     % and process each slice individually
%     % TODO: use growing number of regions 
%     % (i.e. less regions towards apex)
% 
%     labelindices = int8(zeros(m*n,1));
% 
%     % fill-in indices
%     index = 1;
%     for zindex=1:m
%         for aindex=1:n
%             labelindices(zindex * n + aindex) = index;
%             index = index + 1;
%         end
%     end
%     
%     % Z-slicing
%     figure(1)
%     mslice = round(sz / m);
%     labelVolume = zeros(size(volume));
%     for zindex=1:m
%         slice = Z >= (zindex-1) * mslice & Z <= zindex * mslice;
%         
%         labelVolume(slice) = volume(slice) * zindex * n;
%     end
% 
%     % XY angular slicing
%     for aindex=1:n
%         % Move X, Y to center
%         X0 = X - sy / 2;
%         Y0 = Y - sy / 2;
% 
%         atanv = atan2(Y0, X0);
% 
%         theta1 = -pi + 2 * (aindex-1) / n * pi;
%         theta2 = -pi + 2 * (aindex) / n * pi;
% 
%         % Include 0 for last pie
%         if (aindex == n)
%             slice = atanv >= theta1 & atanv <= theta2;
%         else
%             slice = atanv >= theta1 & atanv < theta2;
%         end
% 
%         % Apply threshold on mask
%         labelVolume(slice) = labelVolume(slice) + volume(slice) * (aindex - 1);
%     end
% 
%     % Display volume
% %     figure(1);
% %     for z=30:sz-30
% %         imagesc(labelVolume(:,:,z));
% %         caxis([1 m*n]);
% %         title(z);
% %         axis square
% %         colorbar
% %         pause(0.1);
% %     end
% end
%     