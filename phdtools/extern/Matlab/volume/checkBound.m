function inBounds = checkBound(dims,location)

    if (any(location <= 0))
        inBounds = false;
        return;
    end
    
    if (any(location > dims))
        inBounds = false;
        return;
    end
    
    inBounds = true;

    
    