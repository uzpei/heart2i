function [vgrid, inds] = subdivide(volume, dsize)

if (numel(dsize) == 1)
    dsize = repmat(dsize, [1 3]);
end

[sx, sy, sz] = size(volume);

if (dsize(1) == dsize(2) == dsize(3) == 1)
    vgrid = ones(sx, sy, sz);
    inds = [0 0];
    return
end

vgrid = zeros(sx, sy, sz);

dcols = 1:prod(dsize);
lx = round(linspace(1, sx, 1+dsize(1)));
ly = round(linspace(1, sy, 1+dsize(2)));
lz = round(linspace(1, sz, 1+dsize(3)));

% Fill in values along each dimension
% Make sure last row includes the boundary

for i=1:dsize(1)
    vgrid(lx(i):lx(i+1)-1, :, :) = dcols(i);
end
vgrid(lx(i+1), :, :) = dsize(1);

for i=1:dsize(2)
    vgrid(:, ly(i):ly(i+1)-1, :) = dsize(2) * vgrid(:, ly(i):ly(i+1)-1, :) + dcols(i);
end
vgrid(:, ly(i+1), :) = dsize(2) * vgrid(:, ly(i+1), :) + dcols(dsize(2));

for i=1:dsize(3)
    vgrid(:, :, lz(i):lz(i+1)-1) = dsize(3) * vgrid(:, :, lz(i):lz(i+1)-1) + dcols(i);
end
vgrid(:, :, lz(i+1)) = dsize(3) * vgrid(:, :, lz(i+1)) + dcols(i);

% Remap image to make sure individual labels were assigned
% for i=min(vgrid(:)):max(vgrid(:))
%     vgrid(vgrid == i) = rand(1);
% end

%
%image3d(vgrid);
% image3d(vgrid, round([sx/2, sy/2, sz/2]), [1 3*ndiv]);
% image3d(vgrid, round([sx/2, sy/2, sz/2]), [1 numel(dcols)]);

inds = [min(vgrid(:)), max(vgrid(:))];