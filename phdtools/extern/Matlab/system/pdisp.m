% TODO: parse strings for newlines
function pdisp(s, varargin)
options = parseInput(struct('level', 1, 'style', '%'), varargin{:});
c = options.style;
n = length(s);

if (strcmpi(c, '%'))
    filling = repmat(c, [1 n+6]);
    mtext = [c, c, ' ', s, ' ', c, c];
    s = [filling; mtext; filling];
    disp(s);
elseif  (strcmpi(c, 'box'))
    c = '-';
    filling = repmat(c, [1 n+6]);
    mtext = ['   ', s, '   '];
    s = [filling; mtext; filling];
    disp(s);
elseif  (strcmpi(c, '*'))
    s = ['(*) ', s, '  '];
    disp(s);
elseif  (strcmpi(c, 'title'))
    c = '-';
    fspace = ['|', repmat(' ', [1 n+4]), '|'];
    filling = [ ' ', repmat(c, [1 n+4]), ' '];
    mtext = ['|  ', s, '  |'];
    s = [filling; fspace; mtext; fspace; filling];
    disp(s);
end

