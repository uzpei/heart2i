data = importdata('2dseq.src');

fnames = fieldnames(data);
vsize = data.dimension;
%%
rdata = {};
for i=3:(length(fnames)-1)
    field = fnames{i}
    rvol = reshape(data.(field), vsize);
    rvol = ipermute(rvol, [1 2 3]);
    image3d(rvol)
    rdata{i-2} = rvol;
%     pause(0.5)
end
