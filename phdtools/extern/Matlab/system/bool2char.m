function c = bool2char(b)

tf_words = {'false','true'};
c = tf_words{b+1};
