function prettyprint(s, varargin)
options = parseInput(struct('level', 1, 'style', '%'), varargin{:});
c = options.style;

n = length(s);
filling = repmat(c, [1 n+6]);
mtext = [c, c, ' ', s, ' ', c, c];
disp([filling; mtext; filling])

