function pcell = struct2paramcell(s)

structnames = fieldnames(s);
cellvals = struct2cell(s);

pcell = { };
for i = 1:length(structnames)
    pcell(2*(i-1) + 1) = structnames(i);
    pcell(2*(i-1) + 2) = cellvals(i);
end

