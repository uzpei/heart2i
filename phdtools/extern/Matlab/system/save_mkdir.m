function save_mkdir(filename, data)

[pathstr, name, ext] = fileparts(filename);

% if ~exist(fullfile('.',pathstr),'dir')
%     mkdir(pathstr)
% end
if ~isequal(exist(pathstr, 'dir'),7)
    mkdir(pathstr);
end

save(filename, 'data');