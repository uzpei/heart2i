function r = getnestedfield(a, fieldname)

dot = strfind(fieldname, '.');
if isempty(dot)
    r = [a.(fieldname)];
else
    dot = strfind(fieldname, '.');
    dot = dot(1);
    r = getnestedfield([a.(fieldname(1:dot-1))], fieldname(dot+1:end));
end

