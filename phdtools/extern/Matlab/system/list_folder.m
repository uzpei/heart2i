% Search a path for folders containing the inner path specified and
% ignoring others matching the specified names.
% varargin: 'InnerPath', '...', 'Ignore', { ... }
%
function paths = list_folder(searchPath, varargin)

% Define defaults
defaults = struct('InnerPath','.');
defaults.Ignore =  { '..' };
options = parseInput(defaults, varargin{:});

paths = {};

listing = dir(searchPath);

fprintf('---> Searching [%s] for folders with inner path [%s]...\n', searchPath, options.InnerPath);

% Go through all files
for file_index = 1:length(listing)
    % Get the filename
    filename = listing(file_index).name;
    
    % Ignore hidden files
    if (1 == strfind(filename, '.'))
        fprintf('Ignoring hidden file: [%s]\n', filename);
        continue;
    end
    
    % Go through ignore list
    ignoredFlag = false;
    for ign = 1:length(options.Ignore)
        k = strfind(filename, options.Ignore{ign});
        if (k > 0)
            fprintf('Ignoring (flagged) file: [%s]\n', filename);
            ignoredFlag = true;
            break;
        end
    end
    
    if (ignoredFlag)
        continue;
    end
    
    % Finally, look for the existence of the inner path
%     inner_listing = dir([searchPath, filename]);
%     postPathFound = false;
%     for inner_index = 1:length(inner_listing)
% %        inner_listing(inner_index).name
%         if (strcmpi(inner_listing(inner_index).name, options.InnerPath))
%             postPathFound = true;
%         end
%     end
    fullPath = [searchPath, filename, '/', options.InnerPath];
    if (exist(fullPath) == 7)
        postPathFound = true;
    else
        postPathFound = false;
    end
    
    if (~postPathFound)
        fprintf('Ignoring file: [%s] since inner path [%s] was not found\n', filename, fullPath);
        continue;
    end
    
    % From this point on the file is valid
    paths = [ paths, filename ];
end
end