% Return the memory footprint of a variable, in MB
function vargout = memfootprint(var, varargin)
options = parseInput(struct('quiet', true), varargin{:});

S = whos('var');
mem = S.bytes * 9.53674e-7;

if (~options.quiet)
    fprintf('Variable footprint = %.2f MB\n', mem);
else
    vargout{1} = mem;
end


