n = 10;
si = n; sj = 2*n; sk = 4*n;

i = 1; j = 2; k = 3;

close all

% Test x (j)-derivative
Ux = repmat(sin(2 * pi * (1:sj) / sj), [si 1 sk]);
Vx = DGradient(Ux, [], j);
%figure; image3d(Ux); supertitle('x-derivative');
figure; image3dslice(Ux, 'z');

% Test x (j)-derivative
Uy = repmat((sin(2 * pi * (1:si) / si))', [1 sj sk]);
Vy = DGradient(Uy, [], i);
figure; image3d(Vy); supertitle('y-derivative');

%%%%%%%%
%%
[Ty, Tx, Tz] = decompose(T);
[By, Bx, Bz] = decompose(B);
T = assemble(Tx, Ty, Tz);
B = assemble(Bx, By, Bz);
N = double(normalize(cross(B, T, 4)));

%% Create synthetic volume to test cross product
[sx, sy, sz] = size(subject.mask);
onesV = ones(sx, sy, sz);
zerosV = zeros(sx, sy, sz);
Tx = onesV;
Ty = zerosV;
Tz = zerosV;
Bx = zerosV;
By = zerosV;
Bz = onesV;
T = assemble(Ty, Tx, Tz);
B = assemble(By, Bx, Bz);
N = double(normalize(cross(B, T, 4)));
sum(sum(sum(N(:,:,:,1))))

%% Swapping
T = swap3d(T, [2 1 3]);
N = swap3d(N, [2 1 3]);
B = swap3d(B, [2 1 3]);

%% Permutation
T = permute(T, [2 1 3 4]);
N = permute(N, [2 1 3 4]);
B = permute(B, [2 1 3 4]);

%%
figure; image3d(permute(T, [2 1 3 4]))
figure; image3d(T)
