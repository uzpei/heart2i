% Parse and return propertyName/propertyValue pair options
% All parameter names are converted to lower case during string compare.
function options = parseInput(defaults, varargin)

% Define defaults
options = defaults;

% Read all option names
optionNames = fieldnames(options);

% Count input arguments
nArgs = length(varargin);
if round(nArgs/2)~=nArgs/2
   error(sprintf('Input needs propertyName/propertyValue pairs. Found %d arguments.', nArgs))
end

for pair = reshape(varargin,2,[]) %# pair is {propName;propValue}
   propertyName = pair{1};
    
   % Search for property name
   if any(strcmpi(propertyName,optionNames))
      options.(propertyName) = pair{2};
%   else
%      error('%s is not a recognized parameter name',propertyName)
  end
end
