function s = cellstr2str(c)

s = [];
for i=1:length(c)
    s = [s, ' ', c{i}];
end