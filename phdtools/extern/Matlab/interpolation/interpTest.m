% 1D test
m = [1, 2, 3, 0];
n = numel(m);
m2 = interp1(1:n, m, linspace(1, n, 1.5*n), 'linear')

% 2D test
% Interpolate along Y
n = 3;
[x,y] = meshgrid(1:n,1:n);
%v = rand(size(x))
v = x + y

% Compute finer grid
res = [0.2, 1];

% Need to flip coordinates (XY --> YX)
[xi,yi] = meshgrid(1:res(2):n, 1:res(1):n);

% Compute interpolation
vi = interpn(x,y,v,xi,yi,'linear')

% 3D test
sx = 4;
sy = 3;
sz = 2;
[x,y,z] = ndgrid(1:sx,1:sy,1:sz);
%v = rand(size(x))
v = x + y + z

% Compute finer grid
res = [1, 1, 0.5];
[xi, yi, zi] = ndgrid(1:res(1):sx, 1:res(2):sy, 1:res(3):sz);

% Compute interpolation
vi = interpn(x,y,z,v,xi,yi,zi,'linear')
