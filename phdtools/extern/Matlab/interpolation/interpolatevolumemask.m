% function interpolatevolumemask(volume, mask, minterp)
% volume is a 3D volume
% mask is a logical volume. voxels with a value of 1 are interpolants.
% values with a value of 0 will be interpolated.
%

% For test purposes
% Create a random volume
%    volume = rand(64, 64, 128);
%    [sx, sy, sz] = size(volume);
% Create a cylindrical hole
%     [x, y, z] = meshgrid(1:sy, 1:sx, 1:sz);
%     dsize = 2;
%     mask = sqrt((x-sx/2).^2 + (y-sy/2).^2) > dsize;
%     volume(~mask) = 0;
%     figure(1)
%     image3d(volume)
%     pause(0.1)
% minterp = 'nearest';
%%    
function volumeInterpolated = interpolatevolumemask(volume, mask, varargin)

    % volume, mask of interpolant
    if (nargin == 2) 
        % Choose from natural, linear, nearest
        interpMethod = 'natural';
        maskInterpolant = logical(mask);
        maskInterpolated = ~maskInterpolant;
        extrapolate = false;
        
    % volume, interpolant mask, interpolated mask
    elseif (nargin == 3)
        maskInterpolant = logical(mask);
        maskInterpolated = logical(varargin{1});
        interpMethod = 'natural';
        extrapolate = false;

    % volume, interpolant mask, interpolated mask, method
    elseif (nargin == 4) 
        maskInterpolant = logical(mask);
        maskInterpolated = logical(varargin{1});
        interpMethod = varargin{2};
        extrapolate = false;
        
    % volume, interpolant mask, interpolated mask, method, extrapolate
    elseif (nargin == 5)
        maskInterpolant = logical(mask);
        maskInterpolated = logical(varargin{1});
        interpMethod = varargin{2};
        extrapolate = varargin{3};
    end

    [sx, sy, sz] = size(volume);

    % Create interpolant from points within the mask
    [x, y, z] = meshgrid(1:sy, 1:sx, 1:sz);
    xm = x(maskInterpolant);
    ym = y(maskInterpolant);
    zm = z(maskInterpolant);
    v = volume(maskInterpolant);
    
    tic
    fprintf('Creating (%s) interpolant from %i points...\n', interpMethod, numel(v));
    F = TriScatteredInterp(xm,ym,zm,v,interpMethod);
%    toc

    % Interpolate to non-masked points
%    tic
    xnm = x(maskInterpolated);
    ynm = y(maskInterpolated);
    znm = z(maskInterpolated);
    
    fprintf('Interpolating %i points...\n', numel(xnm));
    vinterp = F(xnm, ynm, znm);
    
    volumeInterpolated = zeros(size(volume));
    volumeInterpolated(maskInterpolant) = volume(maskInterpolant);
    
    % Fill in interpolated values
    volumeInterpolated(maskInterpolated) = vinterp(:);
    
    toc
    
    % Next we need to fill in points that are outside of the convex hull
    % By default TriScatteredInterp has returned NaN at those points
    % Use nearest neighbor to extrapolate those points.
    maskNan = isnan(volumeInterpolated);
    numNan = numel(find(maskNan));
    
    % This prevents infinite recursion
    if (extrapolate && (numNan > 0))
        maskClean = ~maskNan & (maskInterpolant | maskInterpolated);
        maskDirty = maskNan & (maskInterpolant | maskInterpolated);
        
        fprintf('%i interpolated points lie outside of the convex hull. Extrapolating these points...\n', numel(find(maskNan)));
        volumeInterpolated = interpolatevolumemask(volumeInterpolated, maskClean, maskDirty, 'nearest');
    elseif (numNan > 0)
        volumeInterpolated(maskNan) = 0;
    end
    
