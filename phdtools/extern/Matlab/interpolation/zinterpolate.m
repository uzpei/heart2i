function P = zinterpolate(Q, zinds, imeth)

    [sx, sy, sz] = size(Q);

    % Compute the coordinates of the interpolated volume
    [xq, yq, zq] = ndgrid(1:sx,1:sy,1:sz);

    % Compute the coordinates of the available data points
    [x, y, z] = ndgrid(1:sx,1:sy,zinds);

    P = zeros(sx, sy, numel(zinds));
    P(:,:,:) = Q(:,:,zinds,1);
    
    fprintf('Interpolating components...\n');
    P = interpn(x,y,z,P,xq,yq,zq,imeth);
 
%end

