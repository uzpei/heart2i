%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Interpolate mask and ET on an isotropic grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Assume isotropy in the XY plane
isoscale = isotropy(1) / isotropy(3);

if (abs(isoscale-1) > 0.001)
    fprintf('Computing interpolated isotropic volume...\n');
    tic
    [x,y,z] = ndgrid(1:sx,1:sy,1:sz);

    % New interpolatated volume size
    res = [1, 1, isoscale];
    [xi, yi, zi] = ndgrid(1:res(1):sx, 1:res(2):sy, 1:res(3):sz);

    % size(xi)
    % size(yi)
    % size(zi)
    fprintf('Size before interpolation = %i %i %i\n', size(mask));

    % Compute interpolation
    fprintf('Interpolating masks...\n');
    mask = interpn(x,y,z,double(mask),xi,yi,zi,'linear') > 0;
    mask2 = interpn(x,y,z,double(mask2),xi,yi,zi,'linear') > 0;

    fprintf('Interpolating first eigenvector...\n');
    e1xf = interpn(x,y,z,e1x,xi,yi,zi,'linear');
    e1yf = interpn(x,y,z,e1y,xi,yi,zi,'linear');
    e1zf = interpn(x,y,z,e1z,xi,yi,zi,'linear');

    % e1xf = mask .* e1xf;
    % e1yf = mask .* e1yf;
    % e1zf = mask .* e1zf;

    [e1xf, e1yf, e1zf] = normalize(e1xf, e1yf, e1zf);

    fprintf('Size after interpolation = %i %i %i\n', size(mask));

    % Recompute new dimensions
    sx = size(mask,1);
    sy = size(mask,2);
    sz = size(mask,3);

    % figure(1)
    % clf
    % subplot(1,2,1);
    % isosurface(mask,0.5), axis equal, view(3)
    % camlight 
    % lighting gouraud
    % subplot(1,2,2);
    % isosurface(maski,0.5), axis equal, view(3)
    % camlight 
    % lighting gouraud

    % subplot(1,2,2);
    % quiver(e1xf(:,:,1), e1yf(:,:,1), 'r');
    % axis equal
    % title('initial T');
    toc
end
