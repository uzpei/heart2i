% Test different interpolation methods

%% Load data
hearts = loadmat('../../data/heart/rats/rathearts.mat');
subjects = fieldnames(hearts);

reconSubject = 1;
fprintf('Reconstructing subject = %s\n', subjects{reconSubject});
reconstructedHeart = hearts.(char(subjects(reconSubject)));

%% Start computations
useExtrapolation = false;
mask = hearts.rat07052008.mask;
[sx, sy, sz] = size(mask);

% This is our ground truth
T = reconstructedHeart.T;
N = reconstructedHeart.N;
B = reconstructedHeart.B;

zbounds = [28, 90];
maskBound = ones(sx, sy, sz);
maskBound(:,:,1:zbounds(1)) = 0;
maskBound(:,:,zbounds(2):end) = 0;

mask = mask & maskBound & (sqrt(T(:,:,:,1) .^ 2 + T(:,:,:,2) .^ 2 + T(:,:,:,3) .^ 2) > 0);
mask3 = repmat(mask, [1 1 1 3]);

% Apply mask and swap all volumes
% T(:,:,:,1) = T(:,:,:,2) .* mask;
% T(:,:,:,2) = T(:,:,:,1) .* mask;
% T(:,:,:,3) = T(:,:,:,3) .* mask;
% N(:,:,:,1) = N(:,:,:,2) .* mask;
% N(:,:,:,2) = N(:,:,:,1) .* mask;
% N(:,:,:,3) = N(:,:,:,3) .* mask;
% B(:,:,:,1) = B(:,:,:,2) .* mask;
% B(:,:,:,2) = B(:,:,:,1) .* mask;
% B(:,:,:,3) = B(:,:,:,3) .* mask;

frames = framefield(T, N, B, true, mask);
T(:,:,:,1) = frames(:,:,:,1);
T(:,:,:,2) = frames(:,:,:,2);
T(:,:,:,3) = frames(:,:,:,3);
N(:,:,:,1) = frames(:,:,:,4);
N(:,:,:,2) = frames(:,:,:,5);
N(:,:,:,3) = frames(:,:,:,6);
B(:,:,:,1) = frames(:,:,:,7);
B(:,:,:,2) = frames(:,:,:,8);
B(:,:,:,3) = frames(:,:,:,9);
orthogonality3d(frames, mask)

% Number of z-slices
zn = 10;
zinds = round(linspace(zbounds(1),zbounds(2), zn));

% Compute partial mask
interpolantMask = zeros(size(mask));
interpolantMask(:,:,zinds) = mask(:,:,zinds);
interpolantMask = logical(interpolantMask);

% Compute interpolation mask
interpolationMask = ~interpolantMask & mask;

dirout = './output/interpolation/';
if ~exist(fullfile('.',dirout),'dir')
    mkdir(dirout)
end

figure(1)
image3d(interpolantMask)
supertitle('Interpolant');
colormap(customap('redwhiteblue'))
export_fig([dirout, sprintf('mask_interpolant_%i.pdf', zn)], '-transparent', '-q101');

figure(2)
image3d(interpolationMask)
supertitle('Interpolated');
colormap(customap('redwhiteblue'))
export_fig([dirout, sprintf('mask_interpolated_%i.pdf', zn)], '-transparent', '-q101');


%% Compute vector interpolation
imethod = 'linear';
Vx = interpolatevolumemask(T(:,:,:,1), interpolantMask, interpolationMask, imethod, useExtrapolation);
Vy = interpolatevolumemask(T(:,:,:,2), interpolantMask, interpolationMask, imethod, useExtrapolation);
Vz = interpolatevolumemask(T(:,:,:,3), interpolantMask, interpolationMask, imethod, useExtrapolation);

[Vx, Vy, Vz, dnorm] = normalize(Vx, Vy, Vz);

Vx(isnan(Vx)) = 0;
Vy(isnan(Vy)) = 0;
Vz(isnan(Vz)) = 0;

Ti = zeros(sx, sy, sz, 3);
Ti(:,:,:,1) = Vx;
Ti(:,:,:,2) = Vy;
Ti(:,:,:,3) = Vz;

%% Disp output

figure(1)
clf
image3dreconstructionerror(T, interpolantMask, Ti);
caxis([-1 1])
export_fig([dirout, sprintf('vector_%s_interp_%i.pdf', imethod, zn)], '-transparent', '-q101');

%image3d(T);

%% Rotation matrix interpolation
% Fill respective column of 3x3 matrices

%% Compute quaternions from frames
tic
[x,y,z,w] = quatm3d(frames);
toc

%% interpolate
imethod = 'linear';
% xi = zinterpolate(x, zinds, imethod);
% yi = zinterpolate(y, zinds, imethod);
% zi = zinterpolate(z, zinds, imethod);
% wi = zinterpolate(w, zinds, imethod);
xi = interpolatevolumemask(x, interpolantMask, interpolationMask, imethod, useExtrapolation);
yi = interpolatevolumemask(y, interpolantMask, interpolationMask, imethod, useExtrapolation);
zi = interpolatevolumemask(z, interpolantMask, interpolationMask, imethod, useExtrapolation);
wi = interpolatevolumemask(w, interpolantMask, interpolationMask, imethod, useExtrapolation);

% Normalize quaternions
% dnorm = sqrt(wi .^ 2 + xi .^ 2 + yi .^ 2 + zi .^ 2);
% wi = wi ./ dnorm;
% xi = xi ./ dnorm;
% yi = yi ./ dnorm;
% zi = zi ./ dnorm;

toc

%% Recreate rotations from quaternions
% M = matquad3d(x, y, z, w);
M = matquad3d(xi, yi, zi, wi);
orthogonality3d(M)

% Disp output
figure(1)
clf
image3dreconstructionerror(T, interpolantMask, M(:,:,:,1:3));
export_fig([dirout, sprintf('quat_%s_interp_%i.pdf', imethod, zn)], '-transparent', '-q101');


%% Recreate orientation from connection forms

heartIndices = 1:(length(subjects)-1);
heartIndices(reconSubject) = [];
trainingSize = length(heartIndices);

fprintf('PCA training from: \n');
subjects(heartIndices)

%% First compute 2nd-order statistics
fprintf('Computing one-forms for all hearts...\n');
tic
for i=1:length(subjects)-1
    heart = hearts.(char(subjects(i)))
    hlbl = char(subjects(i));

    % Apply mask
    hearts.(hlbl).T = heart.T .* mask3;
    hearts.(hlbl).N = heart.N .* mask3;
    hearts.(hlbl).B = heart.B .* mask3;

    % Compute Jacobian
    DT = jacobian(heart.T);

    % Compute stats
    hearts.(hlbl).TNT = cijv(DT, heart.N, heart.T);
    hearts.(hlbl).TNN = cijv(DT, heart.N, heart.N);
    hearts.(hlbl).TNB = cijv(DT, heart.N, heart.B);
    
    hearts.(hlbl).TBT = cijv(DT, heart.B, heart.T);
    hearts.(hlbl).TBN = cijv(DT, heart.B, heart.N);
    hearts.(hlbl).TBB = cijv(DT, heart.B, heart.B);
end
toc

%% Compute data matrix X
n = numel(find(mask));
X = zeros(n, trainingSize);
for hi=1:trainingSize
    i = heartIndices(hi);
    heart = hearts.(char(subjects(i)));

    data = heart.TNB;
    data = data(mask);
    X(:,hi) = data(:);
end

%% Output eigenmodes
[eigenmatrix, evalues, Xmean] = eigendecompose(X);
mn = size(eigenmatrix,2);

%% Compute one-form reconstruction
heart = hearts.(char(subjects(reconSubject)));
clabels = { 'TNT', 'TNN', 'TNB', 'TBT', 'TBN', 'TBB'};
nu = 0;
clear rheart
for cind=1:numel(clabels)
    cijk = heart.(clabels{cind});
    cijk = cijk(mask);
    
    % Compute the reconstruction map in the reduced coordinate system
    % TODO: make sure this is working properly
    Lmap = mask(:,:,zinds);

    % Compute reconstruction and difference
    % Problem: need to fix indices within find(smasks)
    % i.e. we are adding another thresholding
    [xp, W] = pcaproject(X, Xmean, cTNB(:), eigenmatrix, evalues, Lmap, nu);

    % Reconstruct projection
    dataRecon = zeros(sx, sy, sz);
    dataRecon(mask) = xp(:);
    
    rheart.([clabels{cind}]) = dataRecon;
end

% Visu
figure(1)
image3d(heart.TNB)
figure(2)
image3d(rheart.TNB)

%% Reconstruct orientation from extrapolated mean
h = zeros(sx, sy, sz, 3);

% Compute the error in a n^3 neighborhood
onesv = ones(sx, sy, sz);
error_volume = zeros(sx, sy, sz);

tic
n = 1;
num_neighbors = (2 * n + 1)^3;

Tout = zeros(sx, sy, sz, 3);

for i = -n:n
    for j = -n:n
        for k = -n:n
%            [i, j, k]
            
            % 0.27 whole iteration
            
            % Compute the current offset for all voxels
            
            h(:,:,:,1) = i * onesv;
            h(:,:,:,2) = j * onesv;
            h(:,:,:,3) = k * onesv;
            
            % Compute extrapolation at neighbor offset
            sN1 = dot(h, T, 4) .* rheart.TNT;
            sN2 = dot(h, N, 4) .* rheart.TNN;
            sN3 = dot(h, B, 4) .* rheart.TNB;
            sN = repmat(sN1 + sN2 + sN3, [1 1 1 3]);

            sB1 = dot(h, T, 4) .* rheart.TBT;
            sB2 = dot(h, N, 4) .* rheart.TBN;
            sB3 = dot(h, B, 4) .* rheart.TBB;
            sB = repmat(sB1 + sB2 + sB3, [1 1 1 3]);


            % Compute the extrapolated tangent
            Te = T + sN .* N + sB .* B;
             
            % normalize
            Te = Te ./ (eps + repmat(sqrt(sum(Te .^ 2, 4)), [1 1 1 3]));
            
            % Store cumulative prediction
            Tout = Tout + circshift(Te, -[i, j, k, 0]);
        end
    end
end
toc
%%
Tout = Tout ./ (eps + repmat(sqrt(sum(Tout .^ 2, 4)), [1 1 1 3]));
Tout(~mask3) = 0;

%
figure(1)
image3d(Tout)

figure(2)
image3d(T)
