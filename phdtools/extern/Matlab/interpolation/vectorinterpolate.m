function [Vx, Vy, Vz] = vectorinterpolate(U, zinds, mask, imeth)

    [sx, sy, sz] = size(mask);

    fprintf('Computing interpolated isotropic volume...\n');
    tic

    % Compute the coordinates of the interpolated volume
    [xq, yq, zq] = ndgrid(1:sx,1:sy,1:sz);

    % Compute the coordinates of the available data points
    %[y,x,z] = ind2sub(size(maskout), find(maskin));
%     x = xi(maskin);
%     y = yi(maskin);
%     z = zi(maskin);
    [x, y, z] = ndgrid(1:sx,1:sy,zinds);

%    size(x)
%    size(xq)

    Ux = zeros(sx, sy, numel(zinds));
    Uy = zeros(sx, sy, numel(zinds));
    Uz = zeros(sx, sy, numel(zinds));
    
%     Ux(:,:,:) = U(:,:,:,1);
%     Uy(:,:,:) = U(:,:,:,2);
%     Uz(:,:,:) = U(:,:,:,3);
    Ux(:,:,:) = U(:,:,zinds,1);
    Uy(:,:,:) = U(:,:,zinds,2);
    Uz(:,:,:) = U(:,:,zinds,3);
    
%     
%     figure(3)
%     image3d(x);
    
    fprintf('Interpolating components...\n');
     Vx = interpn(x,y,z,Ux,xq,yq,zq,imeth);
     Vy = interpn(x,y,z,Uy,xq,yq,zq,imeth);
     Vz = interpn(x,y,z,Uz,xq,yq,zq,imeth);
 
   [Vx, Vy, Vz] = normalize(Vx, Vy, Vz);
   Vx(~mask) = 0;
   Vy(~mask) = 0;
   Vz(~mask) = 0;
    
    toc
%end

