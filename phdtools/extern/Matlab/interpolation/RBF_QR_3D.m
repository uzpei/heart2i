function [u]=RBF_QR_3D(ep,xk,xe,f)
%--- ep (scalar)  : The shape parameter
%--- xk(1:N,1:2)  : The center points in spherical coordinates
%                      (r_k,theta_k, fi_k)
%--- xe(1:Ne,1:2) : The evaluation points in spherical coordinates
%--- f(1:N,1:Nf)  : The data to interpolate, Nf different functions
%--- u(1:Ne,1:Nf) : The RBF interpolant evaluated at xe for each function
  N = size(xk,1);  Ne = size(xe,1);
  mp = eps; % machine precision
%--- Find out how many columns are needed. 
  jN=degree(N,3); % 
  jmax = 1;  fac = ep^2/6; ratio = fac*(jmax+1); 
  while (jmax<jN & ratio > 1) % See if d_00 is smaller than d_j0
    jmax = jmax + 1;
    fac = fac*ep^2;
    if (mod(jmax,2)==0)
      ratio = fac;
    else
      fac = fac/(jmax+1)/(jmax+2);
      ratio = fac*(jmax+1);
    end  
  end
%
% d_00 was not the smallest, so we can compare with jN
%
  if (ratio < 1)
    % Adjust fac so that we start with computing the ratio testing jN+1
    jmax = jN;  fac = 1;
    if (mod(jN,2)==1)
      fac = fac/(jN+1);
    end  
  end  
  %
  % Compute the ratio for checking jN+1
  %
  fac = fac*ep^2;
  if (mod(jmax+1,2)==1)
    fac = fac/(jmax+2)/(jmax+3);
    ratio = fac*(jmax+2);
  end  
  
  while (ratio*exp(0.223*(jmax+1)-0.012-0.649*mod(jmax+1,2)) > mp)
    jmax = jmax + 1;
    fac = fac*ep^2;
    if (mod(jmax+1,2)==1)
      fac = fac/(jmax+2)/(jmax+3);
      ratio = fac*(jmax+2);
    else
      ratio = fac;
    end  
  end
%  
% Figure out what indices we need for the 3D-case. We order the
% functions as j first then m, then nu. I think there may be many
% zeros, but this is something we can perhaps gradually improve as
% it goes.
%
  j = zeros(0,1); m=zeros(0,1); nu=zeros(0,1); p=zeros(0,1); odd=1;
  for k=0:jmax
    odd = 1-odd;
    nk = (k+1)*(k+2)/2;
    j = [j; k*ones(nk,1)];   p = [p; odd*ones(nk,1)];
    for mm=0:(k-odd)/2
      nn=-(2*mm+odd):(2*mm+odd);
      nu = [nu; nn']; m = [m; mm*ones(size(nn'))];
    end  
  end
  p2=1:length(j);
  p1=ones(size(p2));
  

  %scale = EvalD(ep,p1,p2,j,m,p);

 %--- Precompute T_j(r), Y_mu^nu, and powers of r
 %--- Note the usage of outer products in the arguments
  Tk = cos(acos(xk(:,1))*(0:jmax));  Te = cos(acos(xe(:,1))*(0:jmax));
  
  Yk = Y(jmax,xk(:,2),xk(:,3));  Ye = Y(jmax,xe(:,2),xe(:,3));
  
  Pk = ones(N,jmax+1);
  for k=1:jmax
    Pk(:,k+1) = xk(:,1).*Pk(:,k);
  end
  re2 = xe(:,1).^2;  % Only even powers are needed for evaluation points
  Pe = ones(Ne,(jmax-p(end))/2+1);
  for k=1:(jmax-p(end))/2
    Pe(:,k+1) = re2.*Pe(:,k);
  end  
%--- Compute the coefficient matrix
  M = length(j);
  rscale = exp(-ep^2*xk(:,1).^2); % Row scaling of C = exp(-ep^2*r_k^2)
  cscale = ones(1,M);           % Column scaling of C = y_{nu}t_{j-2m}
  pos = find(nu == 0);   cscale(pos) = 0.5*cscale(pos);
  pos = find(j-2*m == 0);   cscale(pos) = 0.5*cscale(pos);
  C = Pk(:,j+1); % The powers of r_k and then the trig part
  for k=1:M
    mu = 2*m(k)+p(k);
    if (nu(k)>=0)
      C(:,k) = C(:,k).*Yk(:,mu-nu(k)+1,mu+1);
    else
      C(:,k) = C(:,k).*Yk(:,mu+1,mu+nu(k)+1);
    end
  end
  C = C.*(rscale*cscale);

  a = [(j-2*m+1)/2 (j-2*m+2)/2]; b=[j-2*m+1 (j-2*m-p+2)/2 (j+2*m+p+3)/2];
  z = ep.^4*xk(:,1).^2;
  for k=1:M
    C(:,k) = C(:,k).*hypergeom3(a(k,:),b(k,:),z);
  end  

%--- QR-factorize the coefficient matrix and compute \tilde{R}
  [Q,R] = qr(C);
  Rt = R(1:N,1:N)\R(1:N,N+1:M);
  p1 = (1:N);    p2 = (N+1):M;   [pp2,pp1]=meshgrid(p2,p1);
  if (M>N)
    D = EvalD(ep,pp1,pp2,j,m,p);
    Rt = D.*Rt;
  end  
%--- Evaluate the basis functions and compute the interpolation matrix. 
  V = exp(-ep^2*xk(:,1).^2)*ones(1,M).*Pk(:,2*m+1).*Tk(:,j-2*m+1);
  for k=1:M
    mu = 2*m(k)+p(k);
    if (nu(k)>=0)
      V(:,k) = V(:,k).*Yk(:,mu-nu(k)+1,mu+1);
    else
      V(:,k) = V(:,k).*Yk(:,mu+1,mu+nu(k)+1);
    end
  end

  A = V(:,1:N) + V(:,N+1:M)*Rt.';
%--- Solve the interpolation problem to obtain the coefficients lambda
  lambda = A\f;
  clear A V
%--- Compute basis functions at evaluation points
  Ve = exp(-ep^2*xe(:,1).^2)*ones(1,M).*Pe(:,m+1).*Te(:,j-2*m+1);
  for k=1:M
    mu = 2*m(k)+p(k);
    if (nu(k)>=0)
      Ve(:,k) = Ve(:,k).*Ye(:,mu-nu(k)+1,mu+1);
    else
      Ve(:,k) = Ve(:,k).*Ye(:,mu+1,mu+nu(k)+1);
    end
  end
%  Phi = (C*Ve')';

%--- Evaluate the solution
  B = Ve(:,1:N) + Ve(:,N+1:M)*Rt.';
  u = B*lambda;
  
function D=EvalD(ep,p1,p2,j,m,p)
%--- Compute the scaling effect of D_1^{-1} and D_2
  sz = size(p1);
  p1 = p1(:);  p2 = p2(:);
  D = ep.^(2*(j(p2)-j(p1))).*2.^(4*(m(p2)-m(p1))+p(p2)-p(p1));  
  f1a = (j(p2)+2*m(p2)+p(p2))/2;  f2a = (j(p1)-2*m(p1)-p(p1))/2;
  f1b = (j(p1)+2*m(p1)+p(p1))/2;  f2b = (j(p2)-2*m(p2)-p(p2))/2;

  f3a = j(p1)+2*m(p1)+p(p1)+1;
  f3b = j(p2)+2*m(p2)+p(p2)+1;
  
  for k=1:length(D)
    v1 = sort([(f1b(k)+1):f1a(k) (f2b(k)+1):f2a(k) (f3b(k)+1):f3a(k)]);
    v2 = sort([(f1a(k)+1):f1b(k) (f2a(k)+1):f2b(k) (f3a(k)+1):f3b(k)]);
    l1 = length(v1);            l2 = length(v2);
    v1 = [ones(1,l2-l1) v1];    v2 = [ones(1,l1-l2) v2];    
    D(k) = D(k)*prod(v1./v2);
  end  
  D = reshape(D,sz);

