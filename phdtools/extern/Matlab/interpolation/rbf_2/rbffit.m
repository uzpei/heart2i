%
% — rbffit( x. f, [ basis, shape parameter ] )
%
%        Return the expansion coefficients of a Radial Basis Function
%        approximation for the given function values f using the centers x
%
%
%    'basis'
%        The selected RBF basis. See rbfbasis.m for appropriate values
%
%    'shape parameter'
%        See rbfbasis.m. Defaults to 0.1. Adjust with care
%

function lambda = rbffit ( x, f, basis, epsilon )

default_basis = 'mq';
default_epsilon = 0.1;

m = size( x, 2);

% By default, use a multiquadratic basis
switch nargin
case 2
basis = default_basis;
epsilon = default_epsilon;
case 3
epsilon = default_epsilon;
end
%  if ( nargin == 2 )
%
%  else if (nargin == 3)
%      epsilon = default_epsilon;
%  end

phi = rbfbasis(basis, 0, epsilon);
H = rbfinter(x, basis, 0, epsilon);
% lambda = H\eye(m) * f;
lambda = pinv(H) * f;

end