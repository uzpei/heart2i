%
% — rbfbasis( basis, [ n, shape parameter ] )
%
%    Returns a handle for the nth derivative of a specific Radial Basis Function
%
%    The obviates the need for a messy (and expensive!) symbolic solver for
%    functions that hardly change at all.
%
%    basis = 'mq'
%        Use a multiquadric basis (default)
%
%    basis = 'iq'
%        Use inverse quadrics
%
%    basis = 'imq'
%        Use inverse multiquadric
%
%    basis = 'ga'
%        Use a gaussian basis
%
%
%    n = 0…as high as defined
%        Specify the nth derivative of the basis function. I've only needed
%        first derivatives, so that's all that I've defined.
%
%
%    shape parameter = value
%        specify value as the shape parameter (epsilon). In general, smaller
%        shape parameters yield more accurate approximations [1], but experience
%        shows round-off error becomes more significant. Default is 0.1.
%        Adjust with caution.
%
%
% [1] “Adaptive Radial Basis Function Method for Time Dependent Partial
% Differential Equations” by Scott Sarra

function phi = rbfbasis(r,n, epsilon)

defaulteps = 0.1;
defaultn = 0;

switch nargin
case 1
n = defaultn;
epsilon = defaulteps;
case 2
epsilon = defaulteps;
otherwise
end

% Multiquadric
mq{1} = @(r)sqrt(r.^2.+epsilon.^2);
mq{2} = @(r)r./(sqrt(r.^2.+epsilon.^2));

% Inverse Quadratic
iq{1} = @(r)1./(1.+(epsilon.*r).^2);
iq{2} = @(r)(-2.*epsilon.^2.*r)./(1+epsilon.^2.*r.^2).^2;

% Inverse Multiquadric
imq{1} = @(r)1./(sqrt(1.+(epsilon.*r).^2 ) );
imq{2} = @(r)(-1./2).*(2.*epsilon.^2.*r)./sqrt((1.+epsilon.^2.*r.^2).^3);

% Gaussian
ga{1} = @(r)exp(-1.*(epsilon.*r).^2);
ga{2} = @(r)-2.*epsilon.^2.*r.*exp(-epsilon.^2*r.^2);

switch r
case 'mq'
phi = mq{n+1};
case 'iq'
phi = iq{n+1};
case 'imq'
phi = imq{n+1};
case 'ga'
phi = ga{n+1};
end

end

