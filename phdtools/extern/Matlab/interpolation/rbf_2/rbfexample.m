clear all; close all;

% Let's start with a grid of 32 points. At each of these points, we know
% the value of a function.

x = linspace(-1, 1, 32);
u = sin(pi*x+pi)';

% We want to interpolate this function using Radial Basis Functions. Here I
% compute the expansion coefficients for a multiquadric basis

lambda = rbffit(x, u, 'mq');

% Now I want to refine my grid by choosing additional points between the ones I
% already have.

xRefined = linspace(-1, 1, 64);

% I use the expansion coefficient and my original mesh to get values for these
% additional points.

newU = rbfval(lambda, x, xRefined, 'mq');

% I can interpolate using a different RBF basis function. Here I choose a
% gaussian (ga), but I can also choose inverse quadratic (iq) or inverse
% multiquadric (imq)

lambda = rbffit(x, u, 'ga');
uRefined = rbfval(lambda, x, xRefined, 'ga');

% Now instead of interpolating the function values on a refined grid, I want to
% get the values of the first derivative on my original mesh. I construct a differential
% operator like this.

D = rbfdiff(x, 'mq', 1);
uPrime = D*u;

% Maybe I want to change the value of the shape parameter (epsilon) I'm using.

D = rbfdiff(x, 'mq', 1, 0.0001);

lambda = rbffit(x, u, 'mq', 0.0001);
uRefined = rbfval(lambda, x, xRefined, 'mq', 0.0001);