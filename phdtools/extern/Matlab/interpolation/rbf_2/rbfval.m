%
% — rbfval ( expansion coefficients, centers, x, [basis, shape parameter] )
%
%        Returns the value of a Radial Basis Function approximation at x. The
%    Radial Basis Function approximation is defined by the expansion coefficients
%    and a list of centers.
%
%    'expansion coefficients'
%
%    'centers'
%        The grid on which the RBF approximation was made.
%
%    'basis'
%        See rbfbasis.m for a list of acceptable basis functions
%
%    'shape parameter'
%        See rbfbasis.m

function f = rbfval(lambda, centers, x, basis, epsilon)

default_basis = 'mq';
default_epsilon = 0.1;

m = length(centers);
n = length(x);

switch nargin
case 3
basis = default_basis;
epsilon = default_epsilon;
case 4
epsilon = default_epsilon;
end

% Define our basis function
phi = rbfbasis(basis, 0, epsilon);

A = repmat(centers', 1, n);
B = repmat(x, m, 1);

% Find the difference between each point and all the RBF centers
C = abs( A - B );

% Evaluate the Radial Basis Function for this difference
rbf = phi(C);

% Get the RBF interpolated values for each point
f = rbf'*lambda;

end