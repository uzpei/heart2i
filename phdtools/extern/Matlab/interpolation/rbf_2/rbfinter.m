% — rbfinter( centers, [basis, n, epsilon] )
%
%        Return a Radial Basis Function interpolation matrix for the given centers
%
%        This isn't the function you're looking for.
%        Look at rbffit.m or rbfdiff.m
%

function H = rbfinter ( x, basis, n, epsilon)

default_basis = 'mq';
default_n = 0;
default_epsilon = 0.1;

m = length(x);

switch nargin
case 1
basis = default_basis;
n = default_n;
epsilon = default_epsilon;
case 2
n = default_n;
epsilon = default_epsilon;
case 3
epsilon = default_epsilon;
end

phi = rbfbasis(basis, n);
[c, r] = meshgrid(x, x);

if (n == 0)
H = phi( abs( c - r) );
else
H = phi( c - r );
end

end