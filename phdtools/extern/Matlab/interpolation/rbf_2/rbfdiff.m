% — rbfdiff( x, [basis, n, epsilon] )
%
%        Return a Radial Basis Function differential matrix to approximate
%    the n-th derivative at the given centers, x.
%
%    'basis’, 'n’, 'epsilon’
%        See rbfbasis.m for more information
%
function D = rbfdiff(x, basis, n, epsilon)

default_eps = 0.1;
default_n = 1;

m = length(x);

switch nargin
case 1
basis = 'mq';
epsilon = default_eps;
n = default_n;
case 2
epsilon = default_eps;
n = default_n;
case 3
epsilon = default_eps;
end

H = rbfinter(x, basis, 0, epsilon);
HN = rbfinter(x, basis, n, epsilon);

% Form the differential operator matrix
D = HN*(H\eye(m));

end