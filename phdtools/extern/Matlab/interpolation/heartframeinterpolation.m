%% Interpolate vectors
U = hearts.rat07052008.T;

maskout = hearts.rat07052008.mask;

maskin = zeros(size(maskout));
zn = 10;
zinds = round(linspace(zbounds(1),zbounds(2), zn));

maskin(:,:,zinds) = maskout(:,:,zinds);
maskin = logical(maskin);

[Vx, Vy, Vz] = vectorinterpolate(U, zinds, maskout, 'cubic');

figure(1)
image3d(maskin)

figure(2)
image3d(U)
%image3d(Vx);
%animate3d(Vx);
V = zeros(sx, sy, sz, 3);
V(:,:,:,1) = Vx(:,:,:);
V(:,:,:,2) = Vy(:,:,:);
V(:,:,:,3) = Vz(:,:,:);
image3d(V);

%% Interpolate matrices
T = hearts.rat07052008.T;
N = hearts.rat07052008.N;
B = hearts.rat07052008.B;

n = numel(mask);

%% Linearize rotation matrices
% First two dims are 3x3, third dim is number of elements in flatten
% array (9)

% First reshape T, N, B into 3 by n matrices
% Ts = reshape(T, [n, 3])';
% Ns = reshape(N, [n, 3])';
% Bs = reshape(B, [n, 3])';

% Fill respective column of 3x3 matrices
frames = zeros(sx, sy, sz, 9);
frames(:,:,:,1) = T(:,:,:,1);
frames(:,:,:,2) = T(:,:,:,2);
frames(:,:,:,3) = T(:,:,:,3);

frames(:,:,:,4) = N(:,:,:,1);
frames(:,:,:,5) = N(:,:,:,2);
frames(:,:,:,6) = N(:,:,:,3);

frames(:,:,:,7) = B(:,:,:,1);
frames(:,:,:,8) = B(:,:,:,2);
frames(:,:,:,9) = B(:,:,:,3);

%% Compute quaternions
tic
[w,x,y,z] = quatm3d(frames);
toc

%% Interpolate
zn = 10;
zinds = round(linspace(zbounds(1),zbounds(2), zn));
wi = zinterpolate(w, zinds, 'linear');
xi = zinterpolate(x, zinds, 'linear');
yi = zinterpolate(y, zinds, 'linear');
zi = zinterpolate(z, zinds, 'linear');

%% Recreate rotations from quaternions
Q = zeros(sx, sy, sz, 4);
Q(:,:,:,1) = wi(:,:,:);
Q(:,:,:,2) = xi(:,:,:);
Q(:,:,:,3) = yi(:,:,:);
Q(:,:,:,4) = zi(:,:,:);
M = real(matquad3d(Q));

%% Extract orientation
Ti = zeros(sx, sy, sz, 3);
Ti(:,:,:,1) = M(:,:,:,1) .* mask;
Ti(:,:,:,2) = M(:,:,:,2) .* mask;
Ti(:,:,:,3) = M(:,:,:,3) .* mask;
[Ti(:,:,:,1), Ti(:,:,:,2), Ti(:,:,:,3)] = normalize(Ti(:,:,:,1), Ti(:,:,:,2), Ti(:,:,:,3));

Ni = zeros(sx, sy, sz, 3);
Ni(:,:,:,1) = M(:,:,:,4) .* mask;
Ni(:,:,:,2) = M(:,:,:,5) .* mask;
Ni(:,:,:,3) = M(:,:,:,6) .* mask;
[Ni(:,:,:,1), Ni(:,:,:,2), Ni(:,:,:,3)] = normalize(Ni(:,:,:,1), Ni(:,:,:,2), Ni(:,:,:,3));

Bi = zeros(sx, sy, sz, 3);
Bi(:,:,:,1) = M(:,:,:,7) .* mask;
Bi(:,:,:,2) = M(:,:,:,8) .* mask;
Bi(:,:,:,3) = M(:,:,:,9) .* mask;
[Bi(:,:,:,1), Bi(:,:,:,2), Bi(:,:,:,3)] = normalize(Bi(:,:,:,1), Bi(:,:,:,2), Bi(:,:,:,3));

image3d(Ti);
colormap(customap('redwhiteblue'));

% Compute curvatures


