function [lambdas,alpha,err,k,errs]=FGP05_iterator_web(c,rset,fset,q,tol,Nitermax,printscreen)
% Faul-Goodsell-Powell (FGP05) iterator, written by Nail Gumerov and Ramani Duraiswami, 2005-2006
% arguments: 
% c - the parameter of the multiquadric kernel phi(r)=sqrt(r^2+c^2);
% rset - array of sources of size [dim,N], where dim is the space dimensionality, N is the number of points)
% fset - array of right hand side of size [N,1] 
% q - the power of the L-sets
% tol - tolerance
% Nitermax - max number of iterations
% printscreen=0 - no screen output, otherwise outputs error for each iteration step
% values:
% lambdas - array of size [N,1] of coefficients lambda (solution)
% alpha - constant of solution 
% err - the error of the solution (in terms of the Linf of the residual)
% k - number of iterations
% errs - array of size [k,1] the Linf-norm residual error before each iteration
% example of call:
% [lambdas,alpha,err,k,errs]=FGP05_iterator_web(0.1,rand(2,100),rand(100,1),30,1e-10,100,1);
% gumerov@umiacs.umd.edu; ramani@umiacs.umd.edu

N=length(fset);
[lambdas,alpha,res]=Step1_FGP05_web(fset);
[Omega,m]=Step2_FGP05_web(N);

Lsets=zeros(N,q);
Lsetspow=zeros(N,1);
zetas=zeros(N,q);
delta=zeros(N,1);

Phi=zeros(N,N);
SysMx=zeros(N+1,N+1);

for i=1:N
    ri(:,1)=rset(:,i);
    for j=1:i-1
        rj(:,1)=rset(:,j);
        r=sqrt(dot(ri-rj,ri-rj));
        Phi(j,i)=r;
        SysMx(j,i)=sqrt(r*r+c*c);
    end;
end;
Phi=Phi+Phi';
SysMx(N+1,1:N)=1;
SysMx=SysMx+SysMx';
for i=1:N
    SysMx(i,i)=abs(c);
end;

for m=1:N-1
    [Lset,Omega,ell]=Step3_FGP05_web(Omega,Phi,q,m);
    ells(m)=ell;
    pow=length(Lset);
    Lsetspow(ell)=pow;
    Lsets(ell,1:pow)=Lset(1,1:pow);
    zeta=Step4_FGP05_web(c,Lset,rset);
    zetas(ell,1:pow)=zeta(1,1:pow);
end;

err=max(abs(max(res)),abs(min(res)));
k=0;

while err > tol & k < Nitermax
    k=k+1;
    if printscreen~=0
        fprintf('k = %4.0f,  err= %10.3e \n',k,err);
    end;
    errs(k)=err;
    
    tau=zeros(N,1);
    tau1=zeros(N,1);

    for m=1:N-1
        ell=ells(m);
        pow=Lsetspow(ell);
        zeta(1,1:pow)=zetas(ell,1:pow);
        zeta1=zeta';
        Lset(1,1:pow)=Lsets(ell,1:pow);
        tau1=Step5_FGP05_web(zeta1,Lset,res,pow);
        tau=tau+tau1;
    end;
    
    if k==1
        delta=tau;
    else   
        delta=Step6_FGP05_web(tau,delta,d);
    end;
    
    d=Step7_FGP05_web(SysMx,delta);
    
    [lambdas,alpha,res,err]=Step8_FGP05_web(lambdas,alpha,res,d,delta);
end;
        
    