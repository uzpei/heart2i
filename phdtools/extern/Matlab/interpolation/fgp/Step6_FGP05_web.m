function delta1=Step6_FGP05_web(tau,delta,d)

beta=dot(tau,d)/dot(delta,d);
delta1=tau-beta*delta;
