function tau1=Step5_FGP05_web(zeta,Lset,res,pow)

N=length(res);
tau1=zeros(N,1);

sum=0;

for i=1:pow
    i_=Lset(i);
    sum=sum+zeta(i,1)*res(i_);
end;
myu=sum/zeta(1,1);

for j=1:pow
    j_=Lset(j);
    tau1(j_)=myu*zeta(j,1);
end;