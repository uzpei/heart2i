function [lambdas,alpha,res]=Step1_FGP05_web(fset)

N=length(fset);
lambdas=zeros(N,1);
alpha=.5*(min(fset)+max(fset));
res=fset-alpha;