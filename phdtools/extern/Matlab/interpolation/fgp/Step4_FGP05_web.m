function zeta=Step4_FGP05_web(c,Lset,rset)

nq=length(Lset);
SysMx=zeros(nq+1,nq+1);
for i=1:nq
    i_=Lset(i);
    ri(:,1)=rset(:,i_);
    for j=i+1:nq
        j_=Lset(j);
        rj(:,1)=rset(:,j_);
        SysMx(i,j)=sqrt(dot(ri-rj,ri-rj)+c*c);
    end;
end;
SysMx(nq+1,1:nq)=1;
SysMx=SysMx+SysMx';
for i=1:nq
    SysMx(i,i)=abs(c);
end;
rhs(1:nq+1,1)=0;
rhs(1,1)=1;
zeta=(SysMx\rhs)';  