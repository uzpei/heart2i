function d=Step7_FGP05_web(SysMx,delta)
N=length(delta);
d=squeeze(SysMx(1:N,1:N))*delta;