function [Lset,Omega1,ell]=Step3_FGP05_web(Omega,Phi,q,m)

Omega1=Omega;
N=length(Omega);
ell=Omega(m);

loopcount=0;

if N-m+1 > q
    flag=1;
    
    while flag==1
        
        loopcount=loopcount+1;
        
        for j=m:N
            jj=Omega1(j);
            dist2ell(j-m+1)=Phi(ell,jj);       
        end;
        
        [dist2ell,PermIndx]=sort(dist2ell);
        Lset(1:q)=Omega1(PermIndx(1:q)+m-1);
        
        mindist2=.5*dist2ell(2);
        
        minbeta=1;
        mingamma=2;
        mindist=1e30;
        
        for beta=1:q
            jbeta=Lset(beta);
                   
            for gamma=beta+1:q
                jgamma=Lset(gamma); 
                dist2betagamma=Phi(jbeta,jgamma);
                mindistnew=min(mindist,dist2betagamma);
                
                if mindistnew ~=mindist
                    minbeta=jbeta;
                    mingamma=jgamma;
                    mindist=mindistnew;
                end;
            end;
        end;
        
        if mindist < mindist2
            
            dist2gammaell=Phi(mingamma,ell);
            dist2betaell=Phi(minbeta,ell);
            
            if dist2gammaell<dist2betaell
                dum=minbeta;
                minbeta=mingamma;
                mingamma=dum;
            end;
            
            mhat=find(Omega1==minbeta);
            
            ell=minbeta;
            dum=Omega1(m);
            Omega1(m)=Omega1(mhat);
            Omega1(mhat)=dum;
            
        else
            
            flag=0;
            
        end;
        
    end;
    
else
    Lset(1:N-m+1)=Omega1(m:N);
end;
