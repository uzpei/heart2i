for i=1:8
%subplot(2,4,i);
b0 = loadmat([searchPath, subjects(i).name, '/', experiment_options.PostPath, '/', 'b0.mat']);
b0 = b0 ./ max(b0(:));
image3dslice(b0); colormap gray; axis image; axis off;
export_fig(['output/mask_', num2str(i), '.png'], '-transparent', '-q101');
%pause(0.1);
end
% export_fig(['output/masks.pdf'], '-transparent', '-q101', '-nocrop');
