% Only plot KB and error
limits = { [-20 20], [-20 20], [0 60], [0 20] };
ylabelTxt = '\sigma distribution';
xresk = 0.1;
xresr = 0.1;
stdf = 2;
for j=3:4
    figure(j);
    clf
    
    % Load non-regis
    datah = ratHeartsNonReg.sigmas{j}(:);
    datah = datah(datah > limits{j}(1) & datah < limits{j}(2));

    % Compute histogram
    hcenters = limits{j}(1):xresk:limits{j}(2);
    [heightsk,centersk] = hist(datah, hcenters);
    heightsk = heightsk/trapz(centersk,heightsk);
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        heightsk = conv(heightsk, npdf, 'same');
    end

    % Load rat
    datah = ratHeartsReg.sigmas{j}(:);
    datah = datah(datah > limits{j}(1) & datah < limits{j}(2));

    % Compute histogram
    hcenters = limits{j}(1):xresr:limits{j}(2);
    [heightsr,centersr] = hist(datah, hcenters);
     heightsr = heightsr/trapz(centersr,heightsr);
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        heightsr = conv(heightsr, npdf, 'same');
    end

    hold on
    plot(centersk, heightsk, 'LineWidth', 1, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2);
    plot(centersr, heightsr, 'LineWidth', 1, 'Color', 'b', 'LineStyle', '-', 'LineWidth', 2);
    hold off

    % Setting before exporting
%    xlim([limits{j}(1) limits{j}(2)]);
    set(gca,'FontSize',axisFontSize);
    axis tight
    axis square
    
    legend({ 'non-reg', 'reg' }, 'Location','NorthEast');
%     xlim([limits{j}(1) + 4*resolutions{j}; limits{j}(2) - 4*resolutions{j}]);
    xlim([limits{j}(1); limits{j}(2)]);
    xlabel(xlabels{j});
    ylabel(ylabelTxt);
    
    export_fig([outputDir, 'registeredFit_sigma_',legends{j}, '.pdf'], '-transparent', '-q101');
end
