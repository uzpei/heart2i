% Crop all dog hearts onto the atlas
hearts = heartxml('heartsDogs.xml');

n = length(hearts);
fprintf('%i hearts found\n', n);

maskName = 'myocardium.mat';
%volumes = { 'myocardium', 'b0', 'e1x', 'e1y', 'e1z', 'e2x', 'e2y', 'e2z', 'e3x', 'e3y', 'e3z', 'lambda1', 'lambda2', 'lambda3' };
volumes = { 'myocardium', 'e1x', 'e1y', 'e1z' };

for i=1:n
    
    heart = hearts(i);
    path = char(heart.path);
    dirout = [path, 'cropped/'];
    if ~exist(fullfile('.',dirout),'dir')
        mkdir(dirout)
    end

    if (i == 1)
        fprintf('---> Found ATLAS %s \n', path);
        mask = cell2mat(struct2cell(load([path, 'myocardium.mat']))) > 0;

        % Crop by considering only non-zero voxels
        [mask, maxima] = cropimage(mask);

        % Further cropping by chopping off base of the heart
        % NOTE: this z-index will vary from population to population
        % for JH hearts this is a conservative value
        %zchop = 60;
        maxima(3,2) = maxima(3,2) - 60;
        
%        mask = mask(:,:,1:end-zchop);
%        fprintf('Saving mask to %s \n', dirout);
%        save([dirout, 'mask.mat'], 'mask');
    end
    
    fprintf('---> Processing %s \n', path);
    mask = cell2mat(struct2cell(load([path, 'myocardium.mat']))) > 0;
    mask = mask(maxima(1,1):maxima(1,2), maxima(2,1):maxima(2,2), maxima(3,1):maxima(3,2));

    % Load other volumes and apply cropping
    for j=1:length(volumes)
        vfile = [volumes{j}, '.mat'];
        fprintf('Processing %s...\n', vfile);
        volume = cell2mat(struct2cell(load([path, vfile])));

        % Crop
        volume = volume(maxima(1,1):maxima(1,2), maxima(2,1):maxima(2,2), maxima(3,1):maxima(3,2));
        volume = volume .* mask;

        save([dirout, vfile], 'volume');
    end
end