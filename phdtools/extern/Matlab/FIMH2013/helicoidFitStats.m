%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function helicoidFitStats
close all
clear all

% hearts = heartxml('../../pnashearts.xml');
fitdir = 'helicoidfit/';
heartsPath = '../../';
hearts = heartxml([heartsPath, 'heartsAllIso.xml']);
n = length(hearts);
%n = 4;

fprintf('%i hearts found\n', n);

target = '_n7';

% Params
%xmag = [-0.8, 0.8];
xmag = [-0.3, 0.3];
errormag = [0, pi / 4];
xres = 0.1;
errorres = 0.1;
vbins = 1000;
errorbins = 500;

% Enumerate measures
measures = { 'kt', 'kn', 'kb', 'error' };
measuredStd = { 3, 3, 3, 2 }; 
m = length(measures);
mags = { xmag, xmag, xmag, errormag };
bins = { vbins, vbins, vbins, errorbins };
res = { xres, xres, xres, errorres };
mscale = { 1, 1, 1, 0 };

vres = { 0.25, 0.25, 0.25, 0.25, 0.43, 0.3125, 0.3125, 0.3125 };

exportOverlayed = true;
exportSpeciesOverlayed = true;

%colormap hsv;
%colors = colormap;
%colors = ['k'; 'r'; 'b'; 'm'];
colors = ...
[ 1 0 0 % 1 RED
   0 0 1 % 3 BLUE
   1 1 0 % 6 YELLOW (pale)
   0 1 0 % 2 GREEN (pale)
   0 1 1 % 4 CYAN
   1 0 1 % 5 MAGENTA (pale)
   0 0 0 % 7 BLACK
   0 0.75 0.75 % 8 TURQUOISE
   0 0.5 0 % 9 GREEN (dark)
   0.75 0.75 0 % 10 YELLOW (dark)
   1 0.50 0.25 % 11 ORANGE
   0.75 0 0.75 % 12 MAGENTA (dark)
   0.7 0.7 0.7 % 13 GREY
   0.8 0.7 0.6 % 14 BROWN (pale)
   0.6 0.5 0.4 ]; % 15 BROWN (dark)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Load and preprocess data
% NOTE: all volumes are assumed to be of 
% the same size. If not, all the following
% code will need to be reorganized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:n

    path = [char(hearts(i).path), fitdir];

    % Load curvature values
    fprintf('Loading %s...\n', path);

	% Load first to get the right size
	loaded = load([path, measures{1}, target, '.mat']);

    % Filter
    garbage = zeros(length(loaded.data(:)), 1);

    % Load measures
    for j=1:m
        measure = load([path, measures{j}, target, '.mat']);
        measure = measure.data;
        measure = measure(:);
        
        % Normalize
        if (mscale{j})
            hearts(i).(measures{j}) = measure * vres{i};
        else
            hearts(i).(measures{j}) = measure;
        end
        
        garbage(isnan(hearts(i).(char(measures(j))))) = NaN;
        garbage(hearts(i).(measures{j}) < mags{j}(1)) = NaN;
        garbage(hearts(i).(measures{j}) > mags{j}(2)) = NaN;
    end

    % Apply filter
    fprintf('Thresholding...\n');
    for j=1:m
        hearts(i).(char(measures(j)))(isnan(garbage)) = [];
    end
end

% Fill in species
rats = {};
dogs = {};
human = {};

% Rats
indexes = 1:4;
ratsVoxelCount = 0;
for i=indexes
    ratsVoxelCount = ratsVoxelCount + length(hearts(i).(measures{1}));
end
measurements = zeros(ratsVoxelCount, m);

prevIndex = 1;
for i=indexes
    measure = hearts(i).(measures{1});
    currentLength = length(measure);
    toIndex = prevIndex + currentLength - 1;

    for j=1:m
       measurements(prevIndex:toIndex,j) = hearts(i).(measures{j}); 
    end
    
    prevIndex = prevIndex + currentLength;
end
rats.measures = measurements;

% Human
indexes = 5:5;
humanVoxelCount = 0;
for i=indexes
    humanVoxelCount = humanVoxelCount +  length(hearts(i).(measures{1}));
end
measurements = zeros(humanVoxelCount, m);

prevIndex = 1;
for i=indexes
    measure = hearts(i).(measures{1});
    currentLength = length(measure);
    toIndex = prevIndex + currentLength - 1;

    for j=1:m
       measurements(prevIndex:toIndex,j) = hearts(i).(measures{j}); 
    end
    
    prevIndex = prevIndex + currentLength;
end
human.measures = measurements;

% Dogs
indexes = 6:8;
dogsVoxelCount = 0;
for i=indexes
    dogsVoxelCount = dogsVoxelCount +  + length(hearts(i).(measures{1}));
end
measurements = zeros(dogsVoxelCount, m);

prevIndex = 1;
for i=indexes
    measure = hearts(i).(measures{1});
    currentLength = length(measure);
    toIndex = prevIndex + currentLength - 1;

    for j=1:m
       measurements(prevIndex:toIndex,j) = hearts(i).(measures{j}); 
    end
    
    prevIndex = prevIndex + currentLength;
end
dogs.measures = measurements;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        All hearts overlayed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (exportOverlayed)
    close all;

    maxHeights = zeros(m,1);

%    for i=1:n
    for j=1:m
        figure(j);
        stdh = measuredStd{j};

        hold on

        % Load measuremenets
        fprintf('Drawing %s...\n', measures{j});

%         if (mscale{j})
%             label = upper([measures{j}(1), '_', measures{j}(2)]);
%         else
%             label = measures{j};
%         end
        label = '';

        % Begin drawing
        for i=1:n
            color = colors(i, :);
            measure = hearts(i).(measures{j});
            [hmax, mode] = plotsmooth(measure, bins{j}, '', label, color, false, false, mags{j}, res{j}, stdh);

            if (hmax > maxHeights(j))
                maxHeights(j) = hmax;
            end
        end
        
        hold off;
    end

    % Export figures
    % One forms
    for j=1:m
        figure(j);
        axis([mags{j}(1) mags{j}(2) 0 maxHeights(j)]);
        legend('rat 1', 'rat 2', 'rat 3', 'rat 4', 'human', 'dog 1', 'dog 2', 'dog 3');
        formatAndExport(['output/ghm/heartsOverlayed_',char(measures(j)),target,'.pdf']);
    end
end % end if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Hearts per species
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (exportSpeciesOverlayed)
    close all

    % Mean hearts
    maxHeights = zeros(m,1);

    % oneforms
    for j=1:m
        figure(j)
        hold on
        
%         if (mscale{j})
%             label = upper([measures{j}(1), '_', measures{j}(2)]);
%         else
%             label = measures{j};
%         end
        label = '';
        
        % Process rats
        color = colors(1, :);
        [hmax, mode] = plotsmooth(rats.measures(:,j), bins{j}, '', label, color, false, false, mags{j}, res{j}, measuredStd{j});

        if (hmax > maxHeights(j))
            maxHeights(j) = hmax;
        end
        
        % Process human
        color = colors(2, :);
        [hmax, mode] = plotsmooth(human.measures(:,j), bins{j}, '', label, color, false, false, mags{j}, res{j}, measuredStd{j});
        if (hmax > maxHeights(j))
            maxHeights(j) = hmax;
        end

        % Process dogs
        color = colors(3, :);
        [hmax, mode] = plotsmooth(dogs.measures(:,j), bins{j}, '', label, color, false, false, mags{j}, res{j}, measuredStd{j});
        if (hmax > maxHeights(j))
            maxHeights(j) = hmax;
        end
        hold off
    end


    % Export figures
    % One forms
    for j=1:m
        figure(j);
        axis([mags{j}(1) mags{j}(2) 0 maxHeights(j)]);
        legend('rat', 'human', 'canine');
        formatAndExport(['output/ghm/heartsSpeciesOverlayed_',char(measures(j)),target,'.pdf']);
    end

end % end if

end

function formatAndExport(filename)
    fprintf('Saving %s...\n', filename);
    fontSize = 18;
    grid off
    box on
%	set(gca,'ytick',[])
    set(gca,'FontSize',fontSize);
    export_fig(filename, '-transparent', '-q101');
end
