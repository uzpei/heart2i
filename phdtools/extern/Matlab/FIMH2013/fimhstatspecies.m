fprintf('Beginning plotting...\n');

% This file assumes dog hearts have been precomputed and stored in dogHearts
% and rats stored in heartsrat
outputDir = 'output/';
olabel = 'reg_';

xlabels = { '\mu_(K_T) (degrees / mm)', '\mu(K_N) (degrees / mm)', '\mu(K_B) (degrees / mm)', '\mu(error) (degrees / mm)' };
xlabelsStd = { '\sigma_(K_T) (degrees / mm)', '\sigma(K_N) (degrees / mm)', '\sigma(K_B) (degrees / mm)', '\sigma(error) (degrees / mm)' };

xresk = 0.25;
xresr = 0.5;
axisFontSize = 64;

% Only plot KB and error
limits = { [-20 20], [-20 20], [-120 40], [0 60] };
%ylabelTxt = '\mu distribution';
ylabelTxt = 'distribution';

xsize = 1000;
yratio = 2/3;

for j=3:4
    figure(j-2);
    clf
    
    % Load dog
    datah = dogHearts.means{j}(:);
    datah = datah(datah > limits{j}(1) & datah < limits{j}(2));

    % Compute histogram
    hcenters = limits{j}(1):xresk:limits{j}(2);
    [heightsk,centersk] = hist(datah, hcenters);
%     heightsk = abs(centersk(2)-centersk(1)) * heightsk/trapz(centersk,heightsk);
    heightsk = heightsk/trapz(centersk,heightsk);
    stdf = 1;
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        heightsk = conv(heightsk, npdf, 'same');
    end

    % Load rat
    datah = ratHearts.means{j}(:);
    datah = datah(datah > limits{j}(1) & datah < limits{j}(2));

    % Compute histogram
    hcenters = limits{j}(1):xresr:limits{j}(2);
    [heightsr,centersr] = hist(datah, hcenters);
     heightsr = heightsr/trapz(centersr,heightsr);
    stdf = 2;
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        heightsr = conv(heightsr, npdf, 'same');
    end

    hold on
    plot(centersk, heightsk, 'LineWidth', 1, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2);
    plot(centersr, heightsr, 'LineWidth', 1, 'Color', 'b', 'LineStyle', '-', 'LineWidth', 2);
    hold off

    % Setting before exporting
%    xlim([limits{j}(1) limits{j}(2)]);
    axis tight
%    axis square
    
    if (j==4)
        legend({ 'canine', 'rat' }, 'Location','NorthEast');
    else
        legend({ 'canine', 'rat' }, 'Location','NorthWest');
    end
%     xlim([limits{j}(1) + 4*resolutions{j}; limits{j}(2) - 4*resolutions{j}]);
    xlim([limits{j}(1); limits{j}(2)]);
    xlabel(xlabels{j});
    ylabel(ylabelTxt);
    
    fpos = get(gcf,'Position');
    set(gcf, 'Position', [fpos(1) fpos(2) xsize yratio * xsize]);

    set(gca,'FontSize',axisFontSize);
    set(get(gca,'XLabel'),'FontSize',axisFontSize);    
    set(get(gca,'YLabel'),'FontSize',axisFontSize);    
    
    export_fig([outputDir, 'registeredFit_mu_',legends{j}, '.pdf'], '-transparent', '-q101');
end

% Only plot KB and error
limits = { [-20 20], [-20 20], [0 60], [0 20] };
%ylabelTxt = '\sigma distribution';
ylabelTxt = 'distribution';
xresk = 0.03;
xresr = 0.1;
for j=3:4
    figure(j);
    clf
    
    % Load dog
    datah = dogHearts.sigmas{j}(:);
    datah = datah(datah > limits{j}(1) & datah < limits{j}(2));

    % Compute histogram 
    hcenters = limits{j}(1):xresk:limits{j}(2);
    [heightsk,centersk] = hist(datah, hcenters);
    heightsk = heightsk/trapz(centersk,heightsk);
    stdf = 1;
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        heightsk = conv(heightsk, npdf, 'same');
    end

    % Load rat
    datah = ratHearts.sigmas{j}(:);
    datah = datah(datah > limits{j}(1) & datah < limits{j}(2));

    % Compute histogram
    hcenters = limits{j}(1):xresr:limits{j}(2);
    [heightsr,centersr] = hist(datah, hcenters);
     heightsr = heightsr/trapz(centersr,heightsr);
    stdf = 1;
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        heightsr = conv(heightsr, npdf, 'same');
    end

    hold on
    plot(centersk, heightsk, 'LineWidth', 1, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2);
    plot(centersr, heightsr, 'LineWidth', 1, 'Color', 'b', 'LineStyle', '-', 'LineWidth', 2);
    hold off

    % Setting before exporting
%    xlim([limits{j}(1) limits{j}(2)]);
%    set(gca,'FontSize',axisFontSize);
    axis tight
%    axis square
    
    legend({ 'canine', 'rat' }, 'Location','NorthEast');
%     xlim([limits{j}(1) + 4*resolutions{j}; limits{j}(2) - 4*resolutions{j}]);
    xlim([limits{j}(1); limits{j}(2)]);
    xlabel(xlabelsStd{j});
    ylabel(ylabelTxt);
    
    fpos = get(gcf,'Position');
    set(gcf, 'Position', [fpos(1) fpos(2) xsize yratio * xsize]);
    set(gca,'FontSize',axisFontSize);
    set(get(gca,'XLabel'),'FontSize',axisFontSize);    
    set(get(gca,'YLabel'),'FontSize',axisFontSize);    

    export_fig([outputDir, 'registeredFit_sigma_',legends{j}, '.pdf'], '-transparent', '-q101');
end
