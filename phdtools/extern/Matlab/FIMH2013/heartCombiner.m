%% Process rats

subjects = { 'rat07052008', 'rat17122007', 'rat21012008', 'rat22012008', 'rat24012008', 'rat24022008_17', 'rat27022008', 'rat28012008' };
prepath = '../../data/heart/rats/';
postpath = '/registered/processed/';
outname = 'rathearts.mat';

%fitpath = '/registered/processed/helicoidfit/';
fitpath = '/registeredHelicoidFit/';

clear hearts

Tonly = true;

% Add subject frame and helicoid data
tic
for i=1:numel(subjects)
    fprintf('Loading %s...\n', subjects{i});
    path = [prepath, subjects{i}, postpath];
    [T, N, B, mask] = opendtmri(path);
    hearts.(subjects{i}).T = T;
    hearts.(subjects{i}).mask = mask;
    
    if (~Tonly)
        hearts.(subjects{i}).N = N;
        hearts.(subjects{i}).B = B;

        path = [prepath, subjects{i}, fitpath];
        hearts.(subjects{i}).kt = loadmat([path, 'kt_n3_n3.mat']);
        hearts.(subjects{i}).kn = loadmat([path, 'kn_n3_n3.mat']);
        hearts.(subjects{i}).kb = loadmat([path, 'kb_n3_n3.mat']);
    end
end

% Add atlas fiber and frame data
fprintf('Loading atlas...\n');
path = [prepath, 'atlas', '/processed/'];
[T, N, B, mask] = opendtmri(path);
hearts.('atlas').T = T;
hearts.('atlas').mask = mask;

if (~Tonly)
    hearts.('atlas').N = N;
    hearts.('atlas').B = B;
    %hearts.('atlas').mask = loadmat([path, 'mask.mat']);
end
toc

%
fprintf('Saving to %s...\n', outname);
save([prepath, outname], 'hearts', '-v7.3');

%% Process dogs
subjects = { 'CH06', 'CH09', 'CH10', 'CH11', 'CH12', 'CH13', 'CH15', 'CH16' };
prepath = '../../data/heart/canine/';
postpath = '/registered/cropped/processed/';
outname = 'doghearts.mat';

clear hearts

tic

% Load specimens
for i=1:numel(subjects)
    fprintf('Loading %s...\n', subjects{i});
    path = [prepath, subjects{i}, postpath];
    [T, N, B, mask] = opendtmri(path);
    hearts.(subjects{i}).T = T;
    hearts.(subjects{i}).mask = mask;
    
    if (~Tonly)
        hearts.(subjects{i}).N = N;
        hearts.(subjects{i}).B = B;
    end
end

% Load atlas
fprintf('Loading atlas...\n');
path = [prepath, 'atlas', '/cropped/processed/'];
[T, N, B, mask] = opendtmri(path);
hearts.('atlas').T = T;
hearts.('atlas').mask = mask;
if (~Tonly)
    hearts.('atlas').N = N;
    hearts.('atlas').B = B;
end

toc

%
fprintf('Saving to %s...\n', outname);
%%
save([prepath, outname], 'hearts', '-v7.3');
