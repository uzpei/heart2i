%close all
%clear all

% Global variables
%heartsPath = 'heartsRatsFit.xml';
%heartsPath = 'heartsRatsFitReg.xml';

heartsPath = 'heartsDogsFit.xml';
%olabel = 'ratfit_nonreg_';
%olabel = 'ratfit_reg_';
olabel = 'dogfit_reg_';

%heartsPath = 'heartsDogsFit.xml';
datamean = heartxml(heartsPath);
hearts.data = datamean;
outputDir = 'output/';
stdBins = 200;
axisFontSize = 32;
overlayMean = false;
computeSigma = false;

clear hcenters;

% Params
%subjects = { 'rat07052008', 'rat17122007', 'rat21012008', 'rat22012008', 'rat24012008', 'rat24022008-17', 'rat27022008', 'rat28012008' };
%subjects = { 'rat07052008', 'rat17122007', 'rat21012008', 'rat22012008', 'rat24012008', 'rat24022008-17', 'rat27022008', 'rat28012008' };
targets = { 'kt_n3_n3', 'kn_n3_n3', 'kb_n3_n3', 'error_n3_n3'};
legends = { 'K_T', 'K_N', 'K_B', 'error'};
xlabels = { 'K_T (degrees / mm)', 'K_N (degrees / mm)', 'K_B (degrees / mm)', 'error (degrees)' };

% Limits in radians
%limits = { [-0.5 0.5], [-0.5 0.5], [-0.5 0.5], [0 pi/4] };

% Histogram limits in degrees
% Rats
limits = { [-40 40], [-40 40], [-140 30], [-4 60] };
%limits = { [-0.5 0.5], [-0.5 0.5], [-0.5 0.5], [0 pi/4] };
%ylimits = { 0.1, 0.1, 0.03, 0.17 };
ylimits = { 0.06, 0.06, 0.018, 0.13 };
stdylimits = { 0.07, 0.07, 0.04, 0.4 };
stdlimits = { [0 40], [0 40], [0 150], [0 20] };
spacings = { 0.25, 0.25, 0.25, 1 };
%spacings = { 1, 1, 1, 1 };
xres = 1;
%xres = 0.05;
resolutions = { xres, xres, xres, xres / 2};
stdf = 3;
useMask = true;

% This should always be one
stdw = 1;

% Dogs
%limits = { [-20 20], [-20 20], [-40 20], [0 60] };
%spacings = { 0.3125, 0.3125, 0.3125, 1 };
% stdlimits = { [-20 20], [-20 20], [-40 20], [0 60] };
%xres = 0.25;
%resolutions = { xres, xres, xres, xres };
%useMask = true;

% Use this to convert from RAD to DEG
convertUnits = { true, true, true, true };

fprintf('%i hearts found\n', length(datamean));

heartCount = length(hearts.data);
targetCount = length(targets);

% Load in data for each heart
for i=1:heartCount

    path = char(hearts.data(i).path);

    % Load measures
    for j=1:targetCount
        filename = [path, targets{j}, '.mat'];
        fprintf('Loading %s...\n', filename);

        measure = load(filename);
        if (convertUnits{j})
             measure.data = rad2deg(measure.data) / spacings{j};
        end
        hearts.data(i).(targets{j}) = measure.data;
    end
end

% Assemble voxel-wise volumes (i.e. each voxel contains M values where M =
% # hearts)
sampleVolume = hearts.data(1).(targets{1});
sx = size(sampleVolume,1);
sy = size(sampleVolume,2);
sz = size(sampleVolume,3);

% Construct volumes
means = zeros(sx,sy,sz,targetCount);
sigmas = zeros(sx,sy,sz,targetCount);

% Determine mask from valid voxels
mask = logical(ones(sx, sy, sz));
if (useMask)
    for j=1:targetCount
        for i=1:heartCount
            mask(isnan(hearts.data(i).(targets{j}))) = 0;
        end
    end
end
fprintf('Found %i valid voxels out of %i\n', numel(mask(mask > 0)), sx * sy * sz);

% Assemble target volumes
fprintf('Assembling volumes...\n');
tic
for j=1:targetCount
    hearts.targetVolumes{j} = zeros(heartCount, numel(mask(mask>0)));
    for i=1:heartCount
        dataij = hearts.data(i).(targets{j})(mask);
        hearts.targetVolumes{j}(i, :) = dataij(:);
    end
end
toc

% Compute voxel-wise means
fprintf('Computing means and sigmas...\n');
tic
for j=1:targetCount
    hearts.means{j} = mean(hearts.targetVolumes{j}, 1);
    hearts.sigmas{j} = std(hearts.targetVolumes{j}, 1);
end
toc

for j=1:targetCount
    hcenters{j} = limits{j}(1):resolutions{j}:limits{j}(2);
end

% Output mean values
for j=1:targetCount
    mhcenter = limits{j}(1):resolutions{j}:limits{j}(2);
    [mheights,mcenters] = hist(hearts.means{j}(:), mhcenter);
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        mheights = conv(mheights, npdf, 'same');
    end
    [maxv, maxi] = max(mheights(:));

    stdcenter = stdlimits{j}(1):resolutions{j}:stdlimits{j}(2);
    [stdheights,stdcenters] = hist(hearts.sigmas{j}(:), stdcenter);
    if (stdf > 0)
        npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
        stdheights = conv(stdheights, npdf, 'same');
    end

    [stdmaxv, stdmaxi] = max(stdheights(:));
    fprintf('For %s, mean = %.2f and std = %.2f\n', legends{j}, mhcenter(maxi), stdcenter(stdmaxi));
end

%% Begin plotting
fprintf('Beginning plotting...\n');

% Plot each target for each heart 
figure(1)
clf
lineColor = [0.1 0.1 0.1];
lineStyle = '-';
for j=1:targetCount
    
    % Mean
    figure(j);
    clf;
    
    hold on
    for i=1:heartCount
        datat = hearts.data(i).(targets{j})(:);
        
        % Apply threshold
        datat = datat(datat > limits{j}(1) & datat < limits{j}(2));
        
        [heights,centers] = hist(datat, hcenters{j});
        
        % Smooth histograms
        if (stdf > 0)
            npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
            heights = conv(heights, npdf, 'same');
        end

        heights = heights/trapz(centers,heights);
%         plot(centers, heights, 'LineWidth', 1, 'Color', colorset(i), 'LineStyle', styleset(i));
        plot(centers, heights, 'LineWidth', 1, 'Color', lineColor, 'LineStyle', lineStyle);
    end
    hold off
    
    if (~overlayMean)
        set(gca,'FontSize',axisFontSize);
        axis tight
%        axis square

        xlim([limits{j}(1) + 4*resolutions{j}; limits{j}(2) - 4*resolutions{j}]);
        xlabel(xlabels{j});
        ylim([0 ylimits{j}]);
        ylabel('\mu distribution');
        set(gcf, 'Position', [1000 200 600 400]);
        export_fig([outputDir, 'parameters_',olabel, legends{j}, '.pdf'], '-transparent', '-q101');

    end
end

if (overlayMean)
    % Overlay per-voxel mean
    for j=1:targetCount

        datat = hearts.means{j}(:);
        datat = datat(datat > limits{j}(1) & datat < limits{j}(2));

        % Mean
        figure(j);

        [heights,centers] = hist(datat, hcenters{j});
        heights = heights/trapz(centers,heights);

        % Smooth histograms
        if (stdf > 0)
            npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
            heights = conv(heights, npdf, 'same');
        end

        hold on
        plot(centers, heights, 'LineWidth', 1, 'Color', 'b', 'LineStyle', '-', 'LineWidth', 2);
        hold off

        % Setting before exporting
    %    xlim([limits{j}(1) limits{j}(2)]);
        set(gca,'FontSize',axisFontSize);
        axis tight
%         axis square

        xlim([limits{j}(1) + 4*resolutions{j}; limits{j}(2) - 4*resolutions{j}]);
        xlabel(xlabels{j});
        ylim([0 ylimits{j}]);
        ylabel('\mu distribution');
        set(gcf, 'Position', [1000 200 600 400]);
        export_fig([outputDir, 'parameters_',olabel, legends{j}, '.pdf'], '-transparent', '-q101');
    end

end    

if (computeSigma)
    %% Per-voxel STD
    for j=1:targetCount

        datat = hearts.sigmas{j}(:);
        datat = datat(:);

        figure(j);
        clf

        hcenter = stdlimits{j}(1):resolutions{j}:stdlimits{j}(2);
        [heights,centers] = hist(datat, stdBins);
        heights = heights/trapz(centers,heights);

        % Smooth histograms
        if (stdf > 0)
            npdf = normpdf(-3*stdf:stdw:3*stdf,0,stdf);
            heights = conv(heights, npdf, 'same');
        end

        plot(centers, heights, 'LineWidth', 1, 'Color', 'k', 'LineStyle', '-', 'LineWidth', 1);

        % Setting before exporting
        set(gca,'FontSize',axisFontSize);
        axis tight
%         axis square
        xlim([stdlimits{j}(1); stdlimits{j}(2)]);

        xlabel(xlabels{j});
        ylabel('\sigma distribution');
        ylim([0 stdylimits{j}]);
        set(gcf, 'Position', [1000 200 600 400]);

        export_fig([outputDir, 'parameters_std_',olabel, legends{j}, '.pdf'], '-transparent', '-q101');
    end
end

