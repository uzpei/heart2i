cpath = pwd;

addpath([cpath, '/denmark']);

addpath([cpath, '/.']);
addpath([cpath, '/dti']);
addpath([cpath, '/export_fig']);
addpath([cpath, '/functions']);
addpath([cpath, '/geometry']);
addpath([cpath, '/ghm']);
addpath([cpath, '/heart']);
addpath([cpath, '/imaging']);
addpath([cpath, '/interpolation']);
addpath([cpath, '/matrix']);
addpath([cpath, '/spatial']);
addpath([cpath, '/statistics']);
addpath([cpath, '/system']);
addpath([cpath, '/vision']);
addpath([cpath, '/vision/graphcuts']);
addpath([cpath, '/volume']);
addpath([cpath, '/xml']);

addpath([cpath, '/functions/distrib']);
addpath([cpath, '/PAMI2013']);

addpath([cpath, '/export_fig']);


