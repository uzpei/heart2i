function z = histocurve(datan, titleText, xLabel)

bins = round(size(datan, 1) / 5);

% histfit(datan, bins, 'normal');
% histfit(datan, bins, 'logistic');
H = histfit(datan, bins, 'tlocationscale');

%[muhat,sigmahat] = normfit(datan)
%muhat = muhat + shift

%http://www.mathworks.com/help/toolbox/stats/mle.html
params = mle(datan,'distribution','tlocationscale');

% location
mu = params(1)

% scale > 0
sigma = params(2)

% shape
v = params(3)

% Show zero line
histd = hist(datan, bins);
lheight = max(histd);

H = line([0 0], [0 lheight]);
set(H, 'color', 'green')

% Show mean line
H = line([mu mu], [0 lheight]);
set(H, 'color', 'yellow')

sd = 3*std(datan);
axis([-sd sd 0 lheight])

title(titleText);
xlabel(xLabel);
ylabel('count');

legend('Histogram', 'fit (t location-scale)', 'zero bin', '\mu bin')

% Find text limits in pixels
xfig = get(gcf, 'Position');
xt = round(0.01 * xfig(3));
yt = round(0.79 * xfig(4));
XL = xlim;
limx = XL(2);

t1=text(xt, yt - 0,  horzcat('node count = ', num2str(size(datan,1))), 'Units', 'Pixels');
t2=text(xt, yt - 10, horzcat(' bin count = ', num2str(bins)), 'Units', 'Pixels');
t3=text(xt, yt - 20, horzcat('      mean = ', num2str(mu, '%.4f')), 'Units', 'Pixels');
t4=text(xt, yt - 30, horzcat('             (', num2str(100*abs(mu / limx), '%.2f'), '%)'), 'Units', 'Pixels');

set(t1, 'FontName', 'Monospaced')
set(t2, 'FontName', 'Monospaced')
set(t3, 'FontName', 'Monospaced')
set(t4, 'FontName', 'Monospaced')

end
