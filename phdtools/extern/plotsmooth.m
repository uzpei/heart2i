% Taken from http://www.mathworks.com/products/curvefitting/examples.html?file=/products/demos/shipping/curvefit/histodem.html
function p = plotsmooth(y, bins, titleText, xLabel, color, showHist, showExtras, kmag, kres)

interpolate = false;
showLines = false;

% Draw the histogram bins
% (comment this for enveloppe only)
if (showHist)
	hist(y, bins);
end

% Compute and store histogram info
[heights,centers] = hist(y, bins);
lheight = max(heights(:));
mu = mean(y);

% Find mode (max height)
[Y, I] = max(heights);
mde = centers(I);


if (interpolate)
	%We would like to derive from this histogram a smoother approximation to the underlying distribution. We do this by constructing a spline function f whose average value over each bar interval equals the height of that bar.
	%If h is the height of one of these bars, and its left and right edges are at L and R, then we want the spline f to satisfy
	%integral {f(x) : L < x < R}/(R - L) = h,

	hold on
	n = length(centers);
	w = centers(2)-centers(1);
	t = linspace(centers(1)-w/2,centers(end)+w/2,n+1);
	hold off

	dt = diff(t);
	Fvals = cumsum([0,heights.*dt]);

	% Adjust interval
	%size(t)
	%size(Fvals)

	F = spline(t, [0, Fvals, 0]);

	DF = fnder(F);  % computes its first derivative

	% Plot the resulting spline
	hold on
	fnplt(DF, 'r', 2)
	hold off
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot enveloppe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Smooth the histogram (low pass filter)
npdf = normpdf(-100:100,0,2);
smooth_h = conv(heights, npdf, 'same');
hold on
%plot(centers, heights, 'r');
plot(centers, smooth_h, 'LineWidth', 2, 'Color', color);
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot mean and zero-line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (showLines)
	H = line([mu mu], [0 1.1 * lheight], 'LineWidth',1, 'Color', [0 0.8 0]);
	H = line([0 0], [0 1.1 * lheight], 'LineWidth',1, 'Color', [0 0 0]);
	%set(H, 'color', 'green');
end	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(gca, 'XTick', [kmag(1):kres:kmag(2)]);
axis([kmag(1) kmag(2) 0 1.1 * lheight]);

% Find text limits in pixels
xfig = get(gcf, 'Position');
xt = round(0.01 * xfig(3));
yt = round(0.79 * xfig(4));
XL = xlim;
limx = XL(2);

if (showExtras) 
	t1=text(xt, yt - 0,  horzcat('node count = ', num2str(size(y,1))), 'Units', 'Pixels');
	t2=text(xt, yt - 10, horzcat(' bin count = ', num2str(bins)), 'Units', 'Pixels');
	t3=text(xt, yt - 20, horzcat('      mean = ', num2str(mu, '%.4f')), 'Units', 'Pixels');
	t4=text(xt, yt - 30, horzcat('      mode = ', num2str(mde, '%.4f')), 'Units', 'Pixels');

	set(t1, 'FontName', 'Monospaced');
	set(t2, 'FontName', 'Monospaced');
	set(t3, 'FontName', 'Monospaced');
	set(t4, 'FontName', 'Monospaced');

	if (showLines)
		legend('Histogram', 'low-pass filter', '\mu', 'zero')
	else
		legend('Histogram', 'low-pass filter')
	end
	title(titleText);
end

xlabel(xLabel);
ylabel('count');

p = lheight;