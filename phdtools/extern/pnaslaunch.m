function p = pnaslaunch(parameter)
%clear all;

close all

%Human:       63.2
%Dog042604:   48.75
%Dog080803:   44.7
%Dog101003:   47.8
%Rat07052008: 13.25
%Rat21012008: 11.75
%Rat24012008: 12.25
%Rat27022008: 12.25
%To perform the normalization, I used the human diameter as a unit
%i.e. the curvature values obtained for the human were not normalized. 
%All other datasets were normalized by a factor obtained by dividing their MD
%by that of the human, e.g. the normalization factor is 13.25/63.2 for Rat07052008.

heart1 = './data/results/pnas/rat07052008.txt';
heart2 = './data/results/pnas/rat21012008.txt';
heart3 = './data/results/pnas/rat24012008.txt';
heart4 = './data/results/pnas/rat27022008.txt';
hearts = [heart1; heart2; heart3; heart4]
%hearts = [heart1];

exportp = './data/results/pnas/'

exportextras = false;

showerror = true;

%parameter = 'K_B';
pindex = itocurv(parameter);
mh = 0;
kmag = [-0.7, 0.7];
kres = 0.1;
%bins = round(size(y, 1) / 50);
bins = 500;

if (parameter == 'K_T')
	bins = 5000;	
elseif (parameter == 'K_N')
	bins = 5000;	
elseif (parameter == 'K_B')
	bins = 500;	
end

% Load data in
datas = [];
for i=1:size(hearts,1)
	heart = hearts(i,:);

	if (i == 1)
		color = 'k';
	elseif (i == 2)
		color = 'r';
	elseif (i == 3)
		color = 'g';
	elseif (i == 4)
		color = 'b';
	end
	
	dataf = importdata(heart, ',', 2);
	data = dataf.data(:, pindex);
	%interp(data, 10);
		
 	datas = [datas; data];
 
	figure(i);
	plotsmooth(data, bins, ['rat ', int2str(i), ' -- ', heart], parameter, 'r', true, true, kmag, kres);

    % Clean up name
    lastslash = 0;
    nsize = size(hearts(i,:), 2);
    nameout = hearts(i, :);
    for j=1:nsize
       if (strcmp(hearts(i, j), '/') == 1)
           lastslash = j + 1;
       end
    end
	nameout = [exportp,hearts(i, lastslash:nsize - 4), parameter, '.pdf']
	export_fig(nameout, '-q101');
		
	if (showerror)
		hold on
		figure(size(hearts,1) + 3);
		data = dataf.data(:, itocurv('error'));
		plotsmooth(data, bins, 'error', parameter, color, false, true, [-0.1, 1], kres);
		hold off
	end
		
	if (exportextras)
		% Composite figure
		figure(size(hearts,1) + 2)
		hold on
		h = plotsmooth(data, bins, ['rat ', int2str(i), ' -- ', heart], parameter, color, false, false, kmag, kres);
		hold off
		if (h > mh)
			mh = h;
		end
	end
end

if (exportextras)
	figure(size(hearts,1) + 2)
	legend('rat 1', 'rat 2', 'rat 3', 'rat 4');
	axis([kmag(1) kmag(2) 0 1.1 * mh]);
	export_fig([exportp, 'ratOverlay', parameter, '.pdf'], '-q101');

	% Compute average
	figure(size(hearts,1) + 1);
	plotsmooth(datas, bins, 'rat average', parameter, 'r', true, true, kmag, kres);
	export_fig([exportp, 'meanRat', parameter,'.pdf'], '-q101');
end
