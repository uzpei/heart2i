Using visualization:

java -cp "../jars/jogl/jogl-1.1.1-macosx-universal/lib/jogl.jar; ../jars/jogl/jogl-1.1.1-macosx-universal/lib/gluegen-rt.jar" -Djava.library.path="../jars/jogl/jogl-1.1.1-macosx-universal/lib/" -jar -Xmx1024m hearfitter.jar

java -cp "./jars/jogl/jogl-1.1.1-macosx-universal/lib/jogl.jar; ./jars/jogl/jogl-1.1.1-macosx-universal/lib/gluegen-rt.jar" -Djava.library.path="./jars/jogl/jogl-1.1.1-macosx-universal/lib/" -Xmx1024m -jar hearfitter.jar true


No visualization:
Increase -Xmx1024m when running on axon.cim.mcgill.ca
It has 48gb available… Is 2048m more than enough?
java -jar -Xmx2048m hearfitter.jar false

java -cp "/ajars/j3d/vecmath.jar" -Xmx2048m -jar hearfitter.jar false

on Axon:
ssh epiuze@axon -X
(if exported properly from Eclipse, no need to include classpath)
java -Xmx2048m -jar hearfitter.jar HeartFilename useVisualization numprocs CuttingPlane
java -jar -Xmx2048m hearfitter.jar data/rat/pnas/rat07052008/ 24 false

