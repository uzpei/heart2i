function histolaunch(varargin)

% system('./launcher.sh', '-echo')
launchall = false;

if nargin < 1
    launchall = true;
    parameter = 'K_B';
elseif nargin < 2
%     filename = 'data/results/fitresult.txt';
%     parameter = 'K_T';
    launchall = true;
    parameter = varargin{1};
else
    filename = varargin{1};
    parameter = varargin{2};
end

close all
pindex = itocurv(parameter);

% M = importdata('data/fitresult.txt', ',', 2);
% datakt = M.data(:, 1);
% datakn = M.data(:, 2);
% datakb = M.data(:, 3);
% Uncomment if necessary
% datakb = abs(datakb);
% figure(1);
% histocurve(datakt, 'K_T');
% 
% figure(2);
% histocurve(datakn, 'K_N');
% figure(3);
% histocurve(datakb, 'K_B');

    data = [];
    name = [];
    count = 1;
    
    if (launchall)
        name = vertcat(name, 'data/results/rat07052008');
    else
        name = vertcat(name, filename);
    end
    
    data = vertcat(data, importdata(name(count, :), ',', 2));
    count = count+1;

    if (launchall)
        name = vertcat(name, 'data/results/rat21012008');
        data = vertcat(data, importdata(name(count, :), ',', 2));
        count = count+1;

        name = vertcat(name, 'data/results/rat24012008');
        data = vertcat(data, importdata(name(count, :), ',', 2));
        count = count+1;

        name = vertcat(name, 'data/results/rat27022008');
        data = vertcat(data, importdata(name(count, :), ',', 2));
        count = count+1;
    end

    for i=1:count-1
        figure(i)
        histocurve(data(i).data(:, pindex), name(i, :), parameter);
        
        % Clean up name
        lastslash = 0;
        nsize = size(name(i,:), 2);
        nameout = name(i, :);
        for j=1:nsize
           if (strcmp(name(i, j), '/') == 1)
               lastslash = j + 1;
           end
        end
        
        if (lastslash > 0)
            nameout = name(i, lastslash:nsize);
        end
        
        outstr = num2str(horzcat('data/results/output_', parameter, '_', nameout))
        saveas(gcf, outstr, 'epsc')
    end
